package com.google.android.exoplayer2.source.dash.manifest;

import android.net.Uri;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.util.Pair;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.ParserException;
import com.google.android.exoplayer2.drm.DrmInitData;
import com.google.android.exoplayer2.drm.DrmInitData.SchemeData;
import com.google.android.exoplayer2.extractor.mp4.PsshAtomUtil;
import com.google.android.exoplayer2.source.dash.manifest.SegmentBase.SegmentList;
import com.google.android.exoplayer2.source.dash.manifest.SegmentBase.SegmentTemplate;
import com.google.android.exoplayer2.source.dash.manifest.SegmentBase.SegmentTimelineElement;
import com.google.android.exoplayer2.source.dash.manifest.SegmentBase.SingleSegmentBase;
import com.google.android.exoplayer2.upstream.ParsingLoadable.Parser;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.MimeTypes;
import com.google.android.exoplayer2.util.UriUtil;
import com.google.android.exoplayer2.util.Util;
import com.google.android.exoplayer2.util.XmlPullParserUtil;
import io.fabric.sdk.android.services.network.HttpRequest;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.xml.sax.helpers.DefaultHandler;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

public class DashManifestParser extends DefaultHandler implements Parser<DashManifest> {
    private static final Pattern CEA_608_ACCESSIBILITY_PATTERN = Pattern.compile("CC([1-4])=.*");
    private static final Pattern CEA_708_ACCESSIBILITY_PATTERN = Pattern.compile("([1-9]|[1-5][0-9]|6[0-3])=.*");
    private static final Pattern FRAME_RATE_PATTERN = Pattern.compile("(\\d+)(?:/(\\d+))?");
    private static final String TAG = "MpdParser";
    private final String contentId;
    private final XmlPullParserFactory xmlParserFactory;

    private static final class RepresentationInfo {
        public final String baseUrl;
        public final ArrayList<SchemeData> drmSchemeDatas;
        public final Format format;
        public final ArrayList<SchemeValuePair> inbandEventStreams;
        public final SegmentBase segmentBase;

        public RepresentationInfo(Format format2, String str, SegmentBase segmentBase2, ArrayList<SchemeData> arrayList, ArrayList<SchemeValuePair> arrayList2) {
            this.format = format2;
            this.baseUrl = str;
            this.segmentBase = segmentBase2;
            this.drmSchemeDatas = arrayList;
            this.inbandEventStreams = arrayList2;
        }
    }

    /* access modifiers changed from: protected */
    public void parseAdaptationSetChild(XmlPullParser xmlPullParser) throws XmlPullParserException, IOException {
    }

    public DashManifestParser() {
        this(null);
    }

    public DashManifestParser(String str) {
        this.contentId = str;
        try {
            this.xmlParserFactory = XmlPullParserFactory.newInstance();
        } catch (XmlPullParserException e) {
            throw new RuntimeException("Couldn't create XmlPullParserFactory instance", e);
        }
    }

    public DashManifest parse(Uri uri, InputStream inputStream) throws IOException {
        try {
            XmlPullParser newPullParser = this.xmlParserFactory.newPullParser();
            newPullParser.setInput(inputStream, null);
            if (newPullParser.next() == 2) {
                if ("MPD".equals(newPullParser.getName())) {
                    return parseMediaPresentationDescription(newPullParser, uri.toString());
                }
            }
            throw new ParserException("inputStream does not contain a valid media presentation description");
        } catch (XmlPullParserException e) {
            throw new ParserException((Throwable) e);
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x016b A[LOOP:0: B:20:0x0063->B:64:0x016b, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x012f A[SYNTHETIC] */
    public DashManifest parseMediaPresentationDescription(XmlPullParser xmlPullParser, String str) throws XmlPullParserException, IOException {
        long j;
        DashManifestParser dashManifestParser;
        String str2;
        long j2;
        XmlPullParser xmlPullParser2 = xmlPullParser;
        long parseDateTime = parseDateTime(xmlPullParser2, "availabilityStartTime", C.TIME_UNSET);
        long parseDuration = parseDuration(xmlPullParser2, "mediaPresentationDuration", C.TIME_UNSET);
        long parseDuration2 = parseDuration(xmlPullParser2, "minBufferTime", C.TIME_UNSET);
        String attributeValue = xmlPullParser2.getAttributeValue(null, "type");
        boolean z = false;
        boolean z2 = attributeValue != null && attributeValue.equals("dynamic");
        long parseDuration3 = z2 ? parseDuration(xmlPullParser2, "minimumUpdatePeriod", C.TIME_UNSET) : -9223372036854775807L;
        long parseDuration4 = z2 ? parseDuration(xmlPullParser2, "timeShiftBufferDepth", C.TIME_UNSET) : -9223372036854775807L;
        long parseDuration5 = z2 ? parseDuration(xmlPullParser2, "suggestedPresentationDelay", C.TIME_UNSET) : -9223372036854775807L;
        ArrayList arrayList = new ArrayList();
        String str3 = str;
        long j3 = z2 ? -9223372036854775807L : 0;
        Uri uri = null;
        UtcTimingElement utcTimingElement = null;
        boolean z3 = false;
        while (true) {
            xmlPullParser.next();
            long j4 = parseDuration4;
            if (!XmlPullParserUtil.isStartTag(xmlPullParser2, "BaseURL")) {
                if (XmlPullParserUtil.isStartTag(xmlPullParser2, "UTCTiming")) {
                    j = parseDuration3;
                    utcTimingElement = parseUtcTiming(xmlPullParser);
                } else if (XmlPullParserUtil.isStartTag(xmlPullParser2, HttpRequest.HEADER_LOCATION)) {
                    j = parseDuration3;
                    uri = Uri.parse(xmlPullParser.nextText());
                } else if (XmlPullParserUtil.isStartTag(xmlPullParser2, "Period") && !z3) {
                    dashManifestParser = this;
                    boolean z4 = z;
                    Pair parsePeriod = dashManifestParser.parsePeriod(xmlPullParser2, str3, j3);
                    long j5 = j3;
                    Period period = (Period) parsePeriod.first;
                    str2 = str3;
                    j = parseDuration3;
                    if (period.startMs != C.TIME_UNSET) {
                        long longValue = ((Long) parsePeriod.second).longValue();
                        if (longValue == C.TIME_UNSET) {
                            j2 = C.TIME_UNSET;
                        } else {
                            j2 = longValue + period.startMs;
                        }
                        arrayList.add(period);
                        j3 = j2;
                        z = z4;
                        str3 = str2;
                        if (XmlPullParserUtil.isEndTag(xmlPullParser2, "MPD")) {
                        }
                    } else if (z2) {
                        z = z4;
                        j3 = j5;
                        str3 = str2;
                        z3 = true;
                        if (XmlPullParserUtil.isEndTag(xmlPullParser2, "MPD")) {
                        }
                    } else {
                        StringBuilder sb = new StringBuilder();
                        sb.append("Unable to determine start of period ");
                        sb.append(arrayList.size());
                        throw new ParserException(sb.toString());
                    }
                }
                dashManifestParser = this;
                if (XmlPullParserUtil.isEndTag(xmlPullParser2, "MPD")) {
                }
            } else if (!z) {
                dashManifestParser = this;
                str3 = parseBaseUrl(xmlPullParser2, str3);
                j = parseDuration3;
                z = true;
                if (XmlPullParserUtil.isEndTag(xmlPullParser2, "MPD")) {
                    if (parseDuration == C.TIME_UNSET) {
                        if (j3 != C.TIME_UNSET) {
                            parseDuration = j3;
                        } else if (!z2) {
                            throw new ParserException("Unable to determine duration of static manifest.");
                        }
                    }
                    if (arrayList.isEmpty()) {
                        throw new ParserException("No periods found.");
                    }
                    return dashManifestParser.buildMediaPresentationDescription(parseDateTime, parseDuration, parseDuration2, z2, j, j4, parseDuration5, utcTimingElement, uri, arrayList);
                }
                parseDuration4 = j4;
                parseDuration3 = j;
            }
            dashManifestParser = this;
            str2 = str3;
            j = parseDuration3;
            z = z;
            j3 = j3;
            str3 = str2;
            if (XmlPullParserUtil.isEndTag(xmlPullParser2, "MPD")) {
            }
        }
    }

    /* access modifiers changed from: protected */
    public DashManifest buildMediaPresentationDescription(long j, long j2, long j3, boolean z, long j4, long j5, long j6, UtcTimingElement utcTimingElement, Uri uri, List<Period> list) {
        DashManifest dashManifest = new DashManifest(j, j2, j3, z, j4, j5, j6, utcTimingElement, uri, list);
        return dashManifest;
    }

    /* access modifiers changed from: protected */
    public UtcTimingElement parseUtcTiming(XmlPullParser xmlPullParser) {
        return buildUtcTimingElement(xmlPullParser.getAttributeValue(null, "schemeIdUri"), xmlPullParser.getAttributeValue(null, "value"));
    }

    /* access modifiers changed from: protected */
    public UtcTimingElement buildUtcTimingElement(String str, String str2) {
        return new UtcTimingElement(str, str2);
    }

    /* access modifiers changed from: protected */
    public Pair<Period, Long> parsePeriod(XmlPullParser xmlPullParser, String str, long j) throws XmlPullParserException, IOException {
        String attributeValue = xmlPullParser.getAttributeValue(null, "id");
        long parseDuration = parseDuration(xmlPullParser, TtmlNode.START, j);
        long parseDuration2 = parseDuration(xmlPullParser, "duration", C.TIME_UNSET);
        ArrayList arrayList = new ArrayList();
        boolean z = false;
        SegmentBase segmentBase = null;
        do {
            xmlPullParser.next();
            if (XmlPullParserUtil.isStartTag(xmlPullParser, "BaseURL")) {
                if (!z) {
                    str = parseBaseUrl(xmlPullParser, str);
                    z = true;
                }
            } else if (XmlPullParserUtil.isStartTag(xmlPullParser, "AdaptationSet")) {
                arrayList.add(parseAdaptationSet(xmlPullParser, str, segmentBase));
            } else if (XmlPullParserUtil.isStartTag(xmlPullParser, "SegmentBase")) {
                segmentBase = parseSegmentBase(xmlPullParser, null);
            } else if (XmlPullParserUtil.isStartTag(xmlPullParser, "SegmentList")) {
                segmentBase = parseSegmentList(xmlPullParser, null);
            } else if (XmlPullParserUtil.isStartTag(xmlPullParser, "SegmentTemplate")) {
                segmentBase = parseSegmentTemplate(xmlPullParser, null);
            }
        } while (!XmlPullParserUtil.isEndTag(xmlPullParser, "Period"));
        return Pair.create(buildPeriod(attributeValue, parseDuration, arrayList), Long.valueOf(parseDuration2));
    }

    /* access modifiers changed from: protected */
    public Period buildPeriod(String str, long j, List<AdaptationSet> list) {
        return new Period(str, j, list);
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x01e1 A[LOOP:0: B:1:0x005c->B:52:0x01e1, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x01b3 A[EDGE_INSN: B:53:0x01b3->B:46:0x01b3 ?: BREAK  , SYNTHETIC] */
    public AdaptationSet parseAdaptationSet(XmlPullParser xmlPullParser, String str, SegmentBase segmentBase) throws XmlPullParserException, IOException {
        int i;
        String str2;
        ArrayList arrayList;
        ArrayList arrayList2;
        ArrayList arrayList3;
        ArrayList arrayList4;
        XmlPullParser xmlPullParser2;
        String str3;
        String str4;
        int i2;
        SegmentBase parseSegmentTemplate;
        XmlPullParser xmlPullParser3 = xmlPullParser;
        int parseInt = parseInt(xmlPullParser3, "id", -1);
        int parseContentType = parseContentType(xmlPullParser);
        String str5 = null;
        String attributeValue = xmlPullParser3.getAttributeValue(null, "mimeType");
        String attributeValue2 = xmlPullParser3.getAttributeValue(null, "codecs");
        int parseInt2 = parseInt(xmlPullParser3, SettingsJsonConstants.ICON_WIDTH_KEY, -1);
        int parseInt3 = parseInt(xmlPullParser3, SettingsJsonConstants.ICON_HEIGHT_KEY, -1);
        float parseFrameRate = parseFrameRate(xmlPullParser3, -1.0f);
        int parseInt4 = parseInt(xmlPullParser3, "audioSamplingRate", -1);
        String attributeValue3 = xmlPullParser3.getAttributeValue(null, "lang");
        ArrayList arrayList5 = new ArrayList();
        ArrayList arrayList6 = new ArrayList();
        ArrayList arrayList7 = new ArrayList();
        ArrayList arrayList8 = new ArrayList();
        String str6 = str;
        SegmentBase segmentBase2 = segmentBase;
        int i3 = parseContentType;
        int i4 = -1;
        String str7 = attributeValue3;
        boolean z = false;
        int i5 = 0;
        while (true) {
            xmlPullParser.next();
            if (XmlPullParserUtil.isStartTag(xmlPullParser3, "BaseURL")) {
                if (!z) {
                    str6 = parseBaseUrl(xmlPullParser3, str6);
                    z = true;
                }
                i2 = i3;
                str4 = str7;
                str3 = str6;
                arrayList4 = arrayList8;
                arrayList2 = arrayList7;
                arrayList3 = arrayList6;
                arrayList = arrayList5;
                str2 = str5;
                i = parseInt;
                xmlPullParser2 = xmlPullParser3;
                i3 = i2;
                str7 = str4;
                str6 = str3;
                if (!XmlPullParserUtil.isEndTag(xmlPullParser2, "AdaptationSet")) {
                    break;
                }
                xmlPullParser3 = xmlPullParser2;
                arrayList8 = arrayList4;
                arrayList6 = arrayList3;
                arrayList7 = arrayList2;
                arrayList5 = arrayList;
                str5 = str2;
                parseInt = i;
            } else {
                if (XmlPullParserUtil.isStartTag(xmlPullParser3, "ContentProtection")) {
                    SchemeData parseContentProtection = parseContentProtection(xmlPullParser);
                    if (parseContentProtection != null) {
                        arrayList5.add(parseContentProtection);
                    }
                } else if (XmlPullParserUtil.isStartTag(xmlPullParser3, "ContentComponent")) {
                    str7 = checkLanguageConsistency(str7, xmlPullParser3.getAttributeValue(str5, "lang"));
                    i3 = checkContentTypeConsistency(i3, parseContentType(xmlPullParser));
                } else if (XmlPullParserUtil.isStartTag(xmlPullParser3, "Role")) {
                    i5 |= parseRole(xmlPullParser);
                } else if (XmlPullParserUtil.isStartTag(xmlPullParser3, "AudioChannelConfiguration")) {
                    i4 = parseAudioChannelConfiguration(xmlPullParser);
                } else if (XmlPullParserUtil.isStartTag(xmlPullParser3, "Accessibility")) {
                    arrayList7.add(parseAccessibility(xmlPullParser));
                } else {
                    if (XmlPullParserUtil.isStartTag(xmlPullParser3, "Representation")) {
                        int i6 = i3;
                        String str8 = str7;
                        String str9 = str6;
                        ArrayList arrayList9 = arrayList8;
                        arrayList2 = arrayList7;
                        ArrayList arrayList10 = arrayList6;
                        arrayList = arrayList5;
                        str2 = str5;
                        i = parseInt;
                        RepresentationInfo parseRepresentation = parseRepresentation(xmlPullParser3, str6, attributeValue, attributeValue2, parseInt2, parseInt3, parseFrameRate, i4, parseInt4, str8, i5, arrayList2, segmentBase2);
                        int checkContentTypeConsistency = checkContentTypeConsistency(i6, getContentType(parseRepresentation.format));
                        arrayList4 = arrayList9;
                        arrayList4.add(parseRepresentation);
                        i3 = checkContentTypeConsistency;
                        str7 = str8;
                        str6 = str9;
                        arrayList3 = arrayList10;
                        xmlPullParser2 = xmlPullParser;
                    } else {
                        str4 = str7;
                        str3 = str6;
                        arrayList4 = arrayList8;
                        arrayList2 = arrayList7;
                        ArrayList arrayList11 = arrayList6;
                        arrayList = arrayList5;
                        str2 = str5;
                        i = parseInt;
                        i2 = i3;
                        xmlPullParser2 = xmlPullParser;
                        if (XmlPullParserUtil.isStartTag(xmlPullParser2, "SegmentBase")) {
                            parseSegmentTemplate = parseSegmentBase(xmlPullParser2, (SingleSegmentBase) segmentBase2);
                        } else if (XmlPullParserUtil.isStartTag(xmlPullParser2, "SegmentList")) {
                            parseSegmentTemplate = parseSegmentList(xmlPullParser2, (SegmentList) segmentBase2);
                        } else if (XmlPullParserUtil.isStartTag(xmlPullParser2, "SegmentTemplate")) {
                            parseSegmentTemplate = parseSegmentTemplate(xmlPullParser2, (SegmentTemplate) segmentBase2);
                        } else {
                            if (XmlPullParserUtil.isStartTag(xmlPullParser2, "InbandEventStream")) {
                                arrayList3 = arrayList11;
                                arrayList3.add(parseInbandEventStream(xmlPullParser));
                            } else {
                                arrayList3 = arrayList11;
                                if (XmlPullParserUtil.isStartTag(xmlPullParser)) {
                                    parseAdaptationSetChild(xmlPullParser);
                                }
                            }
                            i3 = i2;
                            str7 = str4;
                            str6 = str3;
                        }
                        segmentBase2 = parseSegmentTemplate;
                        i3 = i2;
                        str7 = str4;
                        str6 = str3;
                        arrayList3 = arrayList11;
                    }
                    if (!XmlPullParserUtil.isEndTag(xmlPullParser2, "AdaptationSet")) {
                    }
                }
                i2 = i3;
                str4 = str7;
                str3 = str6;
                arrayList4 = arrayList8;
                arrayList2 = arrayList7;
                arrayList3 = arrayList6;
                arrayList = arrayList5;
                str2 = str5;
                i = parseInt;
                xmlPullParser2 = xmlPullParser3;
                i3 = i2;
                str7 = str4;
                str6 = str3;
                if (!XmlPullParserUtil.isEndTag(xmlPullParser2, "AdaptationSet")) {
                }
            }
            arrayList4 = arrayList8;
            arrayList2 = arrayList7;
            arrayList3 = arrayList6;
            arrayList = arrayList5;
            str2 = str5;
            i = parseInt;
            xmlPullParser2 = xmlPullParser3;
            if (!XmlPullParserUtil.isEndTag(xmlPullParser2, "AdaptationSet")) {
            }
        }
        ArrayList arrayList12 = new ArrayList(arrayList4.size());
        for (int i7 = 0; i7 < arrayList4.size(); i7++) {
            arrayList12.add(buildRepresentation((RepresentationInfo) arrayList4.get(i7), this.contentId, arrayList, arrayList3));
        }
        return buildAdaptationSet(i, i3, arrayList12, arrayList2);
    }

    /* access modifiers changed from: protected */
    public AdaptationSet buildAdaptationSet(int i, int i2, List<Representation> list, List<SchemeValuePair> list2) {
        return new AdaptationSet(i, i2, list, list2);
    }

    /* access modifiers changed from: protected */
    public int parseContentType(XmlPullParser xmlPullParser) {
        String attributeValue = xmlPullParser.getAttributeValue(null, "contentType");
        if (TextUtils.isEmpty(attributeValue)) {
            return -1;
        }
        if (MimeTypes.BASE_TYPE_AUDIO.equals(attributeValue)) {
            return 1;
        }
        if ("video".equals(attributeValue)) {
            return 2;
        }
        if (MimeTypes.BASE_TYPE_TEXT.equals(attributeValue)) {
            return 3;
        }
        return -1;
    }

    /* access modifiers changed from: protected */
    public int getContentType(Format format) {
        String str = format.sampleMimeType;
        if (TextUtils.isEmpty(str)) {
            return -1;
        }
        if (MimeTypes.isVideo(str)) {
            return 2;
        }
        if (MimeTypes.isAudio(str)) {
            return 1;
        }
        if (mimeTypeIsRawText(str)) {
            return 3;
        }
        return -1;
    }

    /* access modifiers changed from: protected */
    public SchemeData parseContentProtection(XmlPullParser xmlPullParser) throws XmlPullParserException, IOException {
        boolean equals = "urn:uuid:9a04f079-9840-4286-ab92-e65be0885f95".equals(xmlPullParser.getAttributeValue(null, "schemeIdUri"));
        byte[] bArr = null;
        UUID uuid = null;
        boolean z = false;
        do {
            xmlPullParser.next();
            if (bArr == null && XmlPullParserUtil.isStartTag(xmlPullParser, "cenc:pssh") && xmlPullParser.next() == 4) {
                bArr = Base64.decode(xmlPullParser.getText(), 0);
                uuid = PsshAtomUtil.parseUuid(bArr);
                if (uuid == null) {
                    Log.w(TAG, "Skipping malformed cenc:pssh data");
                    bArr = null;
                }
            } else if (bArr == null && equals && XmlPullParserUtil.isStartTag(xmlPullParser, "mspr:pro") && xmlPullParser.next() == 4) {
                bArr = PsshAtomUtil.buildPsshAtom(C.PLAYREADY_UUID, Base64.decode(xmlPullParser.getText(), 0));
                uuid = C.PLAYREADY_UUID;
            } else if (XmlPullParserUtil.isStartTag(xmlPullParser, "widevine:license")) {
                String attributeValue = xmlPullParser.getAttributeValue(null, "robustness_level");
                z = attributeValue != null && attributeValue.startsWith("HW");
            }
        } while (!XmlPullParserUtil.isEndTag(xmlPullParser, "ContentProtection"));
        if (bArr != null) {
            return new SchemeData(uuid, MimeTypes.VIDEO_MP4, bArr, z);
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public SchemeValuePair parseInbandEventStream(XmlPullParser xmlPullParser) throws XmlPullParserException, IOException {
        return parseSchemeValuePair(xmlPullParser, "InbandEventStream");
    }

    /* access modifiers changed from: protected */
    public SchemeValuePair parseAccessibility(XmlPullParser xmlPullParser) throws XmlPullParserException, IOException {
        return parseSchemeValuePair(xmlPullParser, "Accessibility");
    }

    /* access modifiers changed from: protected */
    public int parseRole(XmlPullParser xmlPullParser) throws XmlPullParserException, IOException {
        String parseString = parseString(xmlPullParser, "schemeIdUri", null);
        String parseString2 = parseString(xmlPullParser, "value", null);
        do {
            xmlPullParser.next();
        } while (!XmlPullParserUtil.isEndTag(xmlPullParser, "Role"));
        return (!"urn:mpeg:dash:role:2011".equals(parseString) || !"main".equals(parseString2)) ? 0 : 1;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00fa A[LOOP:0: B:1:0x0051->B:34:0x00fa, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00d6 A[EDGE_INSN: B:35:0x00d6->B:29:0x00d6 ?: BREAK  , SYNTHETIC] */
    public RepresentationInfo parseRepresentation(XmlPullParser xmlPullParser, String str, String str2, String str3, int i, int i2, float f, int i3, int i4, String str4, int i5, List<SchemeValuePair> list, SegmentBase segmentBase) throws XmlPullParserException, IOException {
        SegmentBase segmentBase2;
        String str5;
        XmlPullParser xmlPullParser2 = xmlPullParser;
        String attributeValue = xmlPullParser2.getAttributeValue(null, "id");
        int parseInt = parseInt(xmlPullParser2, "bandwidth", -1);
        String parseString = parseString(xmlPullParser2, "mimeType", str2);
        String parseString2 = parseString(xmlPullParser2, "codecs", str3);
        int parseInt2 = parseInt(xmlPullParser2, SettingsJsonConstants.ICON_WIDTH_KEY, i);
        int parseInt3 = parseInt(xmlPullParser2, SettingsJsonConstants.ICON_HEIGHT_KEY, i2);
        float parseFrameRate = parseFrameRate(xmlPullParser2, f);
        int parseInt4 = parseInt(xmlPullParser2, "audioSamplingRate", i4);
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        boolean z = false;
        String str6 = str;
        int i6 = i3;
        SegmentBase segmentBase3 = segmentBase;
        while (true) {
            xmlPullParser.next();
            int i7 = i6;
            if (XmlPullParserUtil.isStartTag(xmlPullParser2, "BaseURL")) {
                if (!z) {
                    str5 = parseBaseUrl(xmlPullParser2, str6);
                    z = true;
                    segmentBase2 = segmentBase3;
                    i6 = i7;
                    if (XmlPullParserUtil.isEndTag(xmlPullParser2, "Representation")) {
                        break;
                    }
                    str6 = str5;
                    segmentBase3 = segmentBase2;
                }
            } else if (XmlPullParserUtil.isStartTag(xmlPullParser2, "AudioChannelConfiguration")) {
                i6 = parseAudioChannelConfiguration(xmlPullParser);
                str5 = str6;
                segmentBase2 = segmentBase3;
                if (XmlPullParserUtil.isEndTag(xmlPullParser2, "Representation")) {
                }
            } else if (XmlPullParserUtil.isStartTag(xmlPullParser2, "SegmentBase")) {
                segmentBase3 = parseSegmentBase(xmlPullParser2, (SingleSegmentBase) segmentBase3);
            } else if (XmlPullParserUtil.isStartTag(xmlPullParser2, "SegmentList")) {
                segmentBase3 = parseSegmentList(xmlPullParser2, (SegmentList) segmentBase3);
            } else if (XmlPullParserUtil.isStartTag(xmlPullParser2, "SegmentTemplate")) {
                segmentBase3 = parseSegmentTemplate(xmlPullParser2, (SegmentTemplate) segmentBase3);
            } else if (XmlPullParserUtil.isStartTag(xmlPullParser2, "ContentProtection")) {
                SchemeData parseContentProtection = parseContentProtection(xmlPullParser);
                if (parseContentProtection != null) {
                    arrayList.add(parseContentProtection);
                }
            } else if (XmlPullParserUtil.isStartTag(xmlPullParser2, "InbandEventStream")) {
                arrayList2.add(parseInbandEventStream(xmlPullParser));
            }
            str5 = str6;
            segmentBase2 = segmentBase3;
            i6 = i7;
            if (XmlPullParserUtil.isEndTag(xmlPullParser2, "Representation")) {
            }
        }
        Format buildFormat = buildFormat(attributeValue, parseString, parseInt2, parseInt3, parseFrameRate, i6, parseInt4, parseInt, str4, i5, list, parseString2);
        if (segmentBase2 == null) {
            segmentBase2 = new SingleSegmentBase();
        }
        RepresentationInfo representationInfo = new RepresentationInfo(buildFormat, str5, segmentBase2, arrayList, arrayList2);
        return representationInfo;
    }

    /* access modifiers changed from: protected */
    public Format buildFormat(String str, String str2, int i, int i2, float f, int i3, int i4, int i5, String str3, int i6, List<SchemeValuePair> list, String str4) {
        String str5 = str2;
        String str6 = str4;
        String sampleMimeType = getSampleMimeType(str5, str6);
        if (sampleMimeType != null) {
            if (MimeTypes.isVideo(sampleMimeType)) {
                return Format.createVideoContainerFormat(str, str5, sampleMimeType, str6, i5, i, i2, f, null, i6);
            }
            if (MimeTypes.isAudio(sampleMimeType)) {
                return Format.createAudioContainerFormat(str, str5, sampleMimeType, str6, i5, i3, i4, null, i6, str3);
            }
            if (mimeTypeIsRawText(sampleMimeType)) {
                int i7 = MimeTypes.APPLICATION_CEA608.equals(sampleMimeType) ? parseCea608AccessibilityChannel(list) : MimeTypes.APPLICATION_CEA708.equals(sampleMimeType) ? parseCea708AccessibilityChannel(list) : -1;
                return Format.createTextContainerFormat(str, str5, sampleMimeType, str6, i5, i6, str3, i7);
            }
        }
        return Format.createContainerFormat(str, str5, sampleMimeType, str6, i5, i6, str3);
    }

    /* access modifiers changed from: protected */
    public Representation buildRepresentation(RepresentationInfo representationInfo, String str, ArrayList<SchemeData> arrayList, ArrayList<SchemeValuePair> arrayList2) {
        Format format = representationInfo.format;
        ArrayList<SchemeData> arrayList3 = representationInfo.drmSchemeDatas;
        arrayList3.addAll(arrayList);
        if (!arrayList3.isEmpty()) {
            format = format.copyWithDrmInitData(new DrmInitData((List<SchemeData>) arrayList3));
        }
        Format format2 = format;
        ArrayList<SchemeValuePair> arrayList4 = representationInfo.inbandEventStreams;
        arrayList4.addAll(arrayList2);
        return Representation.newInstance(str, -1, format2, representationInfo.baseUrl, representationInfo.segmentBase, arrayList4);
    }

    /* access modifiers changed from: protected */
    public SingleSegmentBase parseSegmentBase(XmlPullParser xmlPullParser, SingleSegmentBase singleSegmentBase) throws XmlPullParserException, IOException {
        long j;
        long j2;
        XmlPullParser xmlPullParser2 = xmlPullParser;
        SingleSegmentBase singleSegmentBase2 = singleSegmentBase;
        long parseLong = parseLong(xmlPullParser2, "timescale", singleSegmentBase2 != null ? singleSegmentBase2.timescale : 1);
        long j3 = 0;
        long parseLong2 = parseLong(xmlPullParser2, "presentationTimeOffset", singleSegmentBase2 != null ? singleSegmentBase2.presentationTimeOffset : 0);
        long j4 = singleSegmentBase2 != null ? singleSegmentBase2.indexStart : 0;
        if (singleSegmentBase2 != null) {
            j3 = singleSegmentBase2.indexLength;
        }
        RangedUri rangedUri = null;
        String attributeValue = xmlPullParser2.getAttributeValue(null, "indexRange");
        if (attributeValue != null) {
            String[] split = attributeValue.split("-");
            j2 = Long.parseLong(split[0]);
            j = (Long.parseLong(split[1]) - j2) + 1;
        } else {
            j = j3;
            j2 = j4;
        }
        if (singleSegmentBase2 != null) {
            rangedUri = singleSegmentBase2.initialization;
        }
        do {
            xmlPullParser.next();
            if (XmlPullParserUtil.isStartTag(xmlPullParser2, "Initialization")) {
                rangedUri = parseInitialization(xmlPullParser);
            }
        } while (!XmlPullParserUtil.isEndTag(xmlPullParser2, "SegmentBase"));
        return buildSingleSegmentBase(rangedUri, parseLong, parseLong2, j2, j);
    }

    /* access modifiers changed from: protected */
    public SingleSegmentBase buildSingleSegmentBase(RangedUri rangedUri, long j, long j2, long j3, long j4) {
        SingleSegmentBase singleSegmentBase = new SingleSegmentBase(rangedUri, j, j2, j3, j4);
        return singleSegmentBase;
    }

    /* access modifiers changed from: protected */
    public SegmentList parseSegmentList(XmlPullParser xmlPullParser, SegmentList segmentList) throws XmlPullParserException, IOException {
        XmlPullParser xmlPullParser2 = xmlPullParser;
        SegmentList segmentList2 = segmentList;
        long parseLong = parseLong(xmlPullParser2, "timescale", segmentList2 != null ? segmentList2.timescale : 1);
        long parseLong2 = parseLong(xmlPullParser2, "presentationTimeOffset", segmentList2 != null ? segmentList2.presentationTimeOffset : 0);
        long parseLong3 = parseLong(xmlPullParser2, "duration", segmentList2 != null ? segmentList2.duration : C.TIME_UNSET);
        int parseInt = parseInt(xmlPullParser2, "startNumber", segmentList2 != null ? segmentList2.startNumber : 1);
        List list = null;
        RangedUri rangedUri = null;
        List list2 = null;
        do {
            xmlPullParser.next();
            if (XmlPullParserUtil.isStartTag(xmlPullParser2, "Initialization")) {
                rangedUri = parseInitialization(xmlPullParser);
            } else if (XmlPullParserUtil.isStartTag(xmlPullParser2, "SegmentTimeline")) {
                list2 = parseSegmentTimeline(xmlPullParser);
            } else if (XmlPullParserUtil.isStartTag(xmlPullParser2, "SegmentURL")) {
                if (list == null) {
                    list = new ArrayList();
                }
                list.add(parseSegmentUrl(xmlPullParser));
            }
        } while (!XmlPullParserUtil.isEndTag(xmlPullParser2, "SegmentList"));
        if (segmentList2 != null) {
            if (rangedUri == null) {
                rangedUri = segmentList2.initialization;
            }
            if (list2 == null) {
                list2 = segmentList2.segmentTimeline;
            }
            if (list == null) {
                list = segmentList2.mediaSegments;
            }
        }
        return buildSegmentList(rangedUri, parseLong, parseLong2, parseInt, parseLong3, list2, list);
    }

    /* access modifiers changed from: protected */
    public SegmentList buildSegmentList(RangedUri rangedUri, long j, long j2, int i, long j3, List<SegmentTimelineElement> list, List<RangedUri> list2) {
        SegmentList segmentList = new SegmentList(rangedUri, j, j2, i, j3, list, list2);
        return segmentList;
    }

    /* access modifiers changed from: protected */
    public SegmentTemplate parseSegmentTemplate(XmlPullParser xmlPullParser, SegmentTemplate segmentTemplate) throws XmlPullParserException, IOException {
        XmlPullParser xmlPullParser2 = xmlPullParser;
        SegmentTemplate segmentTemplate2 = segmentTemplate;
        long parseLong = parseLong(xmlPullParser2, "timescale", segmentTemplate2 != null ? segmentTemplate2.timescale : 1);
        long parseLong2 = parseLong(xmlPullParser2, "presentationTimeOffset", segmentTemplate2 != null ? segmentTemplate2.presentationTimeOffset : 0);
        long parseLong3 = parseLong(xmlPullParser2, "duration", segmentTemplate2 != null ? segmentTemplate2.duration : C.TIME_UNSET);
        int parseInt = parseInt(xmlPullParser2, "startNumber", segmentTemplate2 != null ? segmentTemplate2.startNumber : 1);
        RangedUri rangedUri = null;
        UrlTemplate parseUrlTemplate = parseUrlTemplate(xmlPullParser2, "media", segmentTemplate2 != null ? segmentTemplate2.mediaTemplate : null);
        UrlTemplate parseUrlTemplate2 = parseUrlTemplate(xmlPullParser2, "initialization", segmentTemplate2 != null ? segmentTemplate2.initializationTemplate : null);
        List list = null;
        do {
            xmlPullParser.next();
            if (XmlPullParserUtil.isStartTag(xmlPullParser2, "Initialization")) {
                rangedUri = parseInitialization(xmlPullParser);
            } else if (XmlPullParserUtil.isStartTag(xmlPullParser2, "SegmentTimeline")) {
                list = parseSegmentTimeline(xmlPullParser);
            }
        } while (!XmlPullParserUtil.isEndTag(xmlPullParser2, "SegmentTemplate"));
        if (segmentTemplate2 != null) {
            if (rangedUri == null) {
                rangedUri = segmentTemplate2.initialization;
            }
            if (list == null) {
                list = segmentTemplate2.segmentTimeline;
            }
        }
        return buildSegmentTemplate(rangedUri, parseLong, parseLong2, parseInt, parseLong3, list, parseUrlTemplate2, parseUrlTemplate);
    }

    /* access modifiers changed from: protected */
    public SegmentTemplate buildSegmentTemplate(RangedUri rangedUri, long j, long j2, int i, long j3, List<SegmentTimelineElement> list, UrlTemplate urlTemplate, UrlTemplate urlTemplate2) {
        SegmentTemplate segmentTemplate = new SegmentTemplate(rangedUri, j, j2, i, j3, list, urlTemplate, urlTemplate2);
        return segmentTemplate;
    }

    /* access modifiers changed from: protected */
    public List<SegmentTimelineElement> parseSegmentTimeline(XmlPullParser xmlPullParser) throws XmlPullParserException, IOException {
        ArrayList arrayList = new ArrayList();
        long j = 0;
        do {
            xmlPullParser.next();
            if (XmlPullParserUtil.isStartTag(xmlPullParser, "S")) {
                j = parseLong(xmlPullParser, "t", j);
                long parseLong = parseLong(xmlPullParser, "d", C.TIME_UNSET);
                int parseInt = 1 + parseInt(xmlPullParser, "r", 0);
                for (int i = 0; i < parseInt; i++) {
                    arrayList.add(buildSegmentTimelineElement(j, parseLong));
                    j += parseLong;
                }
            }
        } while (!XmlPullParserUtil.isEndTag(xmlPullParser, "SegmentTimeline"));
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public SegmentTimelineElement buildSegmentTimelineElement(long j, long j2) {
        return new SegmentTimelineElement(j, j2);
    }

    /* access modifiers changed from: protected */
    public UrlTemplate parseUrlTemplate(XmlPullParser xmlPullParser, String str, UrlTemplate urlTemplate) {
        String attributeValue = xmlPullParser.getAttributeValue(null, str);
        return attributeValue != null ? UrlTemplate.compile(attributeValue) : urlTemplate;
    }

    /* access modifiers changed from: protected */
    public RangedUri parseInitialization(XmlPullParser xmlPullParser) {
        return parseRangedUrl(xmlPullParser, "sourceURL", "range");
    }

    /* access modifiers changed from: protected */
    public RangedUri parseSegmentUrl(XmlPullParser xmlPullParser) {
        return parseRangedUrl(xmlPullParser, "media", "mediaRange");
    }

    /* access modifiers changed from: protected */
    public RangedUri parseRangedUrl(XmlPullParser xmlPullParser, String str, String str2) {
        long j;
        long j2;
        String attributeValue = xmlPullParser.getAttributeValue(null, str);
        String attributeValue2 = xmlPullParser.getAttributeValue(null, str2);
        if (attributeValue2 != null) {
            String[] split = attributeValue2.split("-");
            j2 = Long.parseLong(split[0]);
            if (split.length == 2) {
                j = (Long.parseLong(split[1]) - j2) + 1;
                return buildRangedUri(attributeValue, j2, j);
            }
        } else {
            j2 = 0;
        }
        j = -1;
        return buildRangedUri(attributeValue, j2, j);
    }

    /* access modifiers changed from: protected */
    public RangedUri buildRangedUri(String str, long j, long j2) {
        RangedUri rangedUri = new RangedUri(str, j, j2);
        return rangedUri;
    }

    /* access modifiers changed from: protected */
    public int parseAudioChannelConfiguration(XmlPullParser xmlPullParser) throws XmlPullParserException, IOException {
        int i = -1;
        if ("urn:mpeg:dash:23003:3:audio_channel_configuration:2011".equals(parseString(xmlPullParser, "schemeIdUri", null))) {
            i = parseInt(xmlPullParser, "value", -1);
        }
        do {
            xmlPullParser.next();
        } while (!XmlPullParserUtil.isEndTag(xmlPullParser, "AudioChannelConfiguration"));
        return i;
    }

    private static String getSampleMimeType(String str, String str2) {
        if (MimeTypes.isAudio(str)) {
            return MimeTypes.getAudioMediaMimeType(str2);
        }
        if (MimeTypes.isVideo(str)) {
            return MimeTypes.getVideoMediaMimeType(str2);
        }
        if (mimeTypeIsRawText(str)) {
            return str;
        }
        if (!MimeTypes.APPLICATION_MP4.equals(str)) {
            if (MimeTypes.APPLICATION_RAWCC.equals(str) && str2 != null) {
                if (str2.contains("cea708")) {
                    return MimeTypes.APPLICATION_CEA708;
                }
                if (str2.contains("eia608") || str2.contains("cea608")) {
                    return MimeTypes.APPLICATION_CEA608;
                }
            }
            return null;
        } else if ("stpp".equals(str2)) {
            return MimeTypes.APPLICATION_TTML;
        } else {
            if ("wvtt".equals(str2)) {
                return MimeTypes.APPLICATION_MP4VTT;
            }
        }
        return null;
    }

    private static boolean mimeTypeIsRawText(String str) {
        return MimeTypes.isText(str) || MimeTypes.APPLICATION_TTML.equals(str) || MimeTypes.APPLICATION_MP4VTT.equals(str) || MimeTypes.APPLICATION_CEA708.equals(str) || MimeTypes.APPLICATION_CEA608.equals(str);
    }

    private static String checkLanguageConsistency(String str, String str2) {
        if (str == null) {
            return str2;
        }
        if (str2 == null) {
            return str;
        }
        Assertions.checkState(str.equals(str2));
        return str;
    }

    private static int checkContentTypeConsistency(int i, int i2) {
        if (i == -1) {
            return i2;
        }
        if (i2 == -1) {
            return i;
        }
        Assertions.checkState(i == i2);
        return i;
    }

    protected static SchemeValuePair parseSchemeValuePair(XmlPullParser xmlPullParser, String str) throws XmlPullParserException, IOException {
        String parseString = parseString(xmlPullParser, "schemeIdUri", null);
        String parseString2 = parseString(xmlPullParser, "value", null);
        do {
            xmlPullParser.next();
        } while (!XmlPullParserUtil.isEndTag(xmlPullParser, str));
        return new SchemeValuePair(parseString, parseString2);
    }

    protected static int parseCea608AccessibilityChannel(List<SchemeValuePair> list) {
        for (int i = 0; i < list.size(); i++) {
            SchemeValuePair schemeValuePair = (SchemeValuePair) list.get(i);
            if ("urn:scte:dash:cc:cea-608:2015".equals(schemeValuePair.schemeIdUri) && schemeValuePair.value != null) {
                Matcher matcher = CEA_608_ACCESSIBILITY_PATTERN.matcher(schemeValuePair.value);
                if (matcher.matches()) {
                    return Integer.parseInt(matcher.group(1));
                }
                String str = TAG;
                StringBuilder sb = new StringBuilder();
                sb.append("Unable to parse CEA-608 channel number from: ");
                sb.append(schemeValuePair.value);
                Log.w(str, sb.toString());
            }
        }
        return -1;
    }

    protected static int parseCea708AccessibilityChannel(List<SchemeValuePair> list) {
        for (int i = 0; i < list.size(); i++) {
            SchemeValuePair schemeValuePair = (SchemeValuePair) list.get(i);
            if ("urn:scte:dash:cc:cea-708:2015".equals(schemeValuePair.schemeIdUri) && schemeValuePair.value != null) {
                Matcher matcher = CEA_708_ACCESSIBILITY_PATTERN.matcher(schemeValuePair.value);
                if (matcher.matches()) {
                    return Integer.parseInt(matcher.group(1));
                }
                String str = TAG;
                StringBuilder sb = new StringBuilder();
                sb.append("Unable to parse CEA-708 service block number from: ");
                sb.append(schemeValuePair.value);
                Log.w(str, sb.toString());
            }
        }
        return -1;
    }

    protected static float parseFrameRate(XmlPullParser xmlPullParser, float f) {
        String attributeValue = xmlPullParser.getAttributeValue(null, "frameRate");
        if (attributeValue == null) {
            return f;
        }
        Matcher matcher = FRAME_RATE_PATTERN.matcher(attributeValue);
        if (!matcher.matches()) {
            return f;
        }
        int parseInt = Integer.parseInt(matcher.group(1));
        String group = matcher.group(2);
        return !TextUtils.isEmpty(group) ? ((float) parseInt) / ((float) Integer.parseInt(group)) : (float) parseInt;
    }

    protected static long parseDuration(XmlPullParser xmlPullParser, String str, long j) {
        String attributeValue = xmlPullParser.getAttributeValue(null, str);
        if (attributeValue == null) {
            return j;
        }
        return Util.parseXsDuration(attributeValue);
    }

    protected static long parseDateTime(XmlPullParser xmlPullParser, String str, long j) throws ParserException {
        String attributeValue = xmlPullParser.getAttributeValue(null, str);
        if (attributeValue == null) {
            return j;
        }
        return Util.parseXsDateTime(attributeValue);
    }

    protected static String parseBaseUrl(XmlPullParser xmlPullParser, String str) throws XmlPullParserException, IOException {
        xmlPullParser.next();
        return UriUtil.resolve(str, xmlPullParser.getText());
    }

    protected static int parseInt(XmlPullParser xmlPullParser, String str, int i) {
        String attributeValue = xmlPullParser.getAttributeValue(null, str);
        return attributeValue == null ? i : Integer.parseInt(attributeValue);
    }

    protected static long parseLong(XmlPullParser xmlPullParser, String str, long j) {
        String attributeValue = xmlPullParser.getAttributeValue(null, str);
        return attributeValue == null ? j : Long.parseLong(attributeValue);
    }

    protected static String parseString(XmlPullParser xmlPullParser, String str, String str2) {
        String attributeValue = xmlPullParser.getAttributeValue(null, str);
        return attributeValue == null ? str2 : attributeValue;
    }
}
