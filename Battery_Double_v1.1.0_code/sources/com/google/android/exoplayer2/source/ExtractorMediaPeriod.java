package com.google.android.exoplayer2.source;

import android.net.Uri;
import android.os.Handler;
import android.support.v4.media.session.PlaybackStateCompat;
import android.util.SparseArray;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.FormatHolder;
import com.google.android.exoplayer2.decoder.DecoderInputBuffer;
import com.google.android.exoplayer2.extractor.DefaultExtractorInput;
import com.google.android.exoplayer2.extractor.DefaultTrackOutput;
import com.google.android.exoplayer2.extractor.DefaultTrackOutput.UpstreamFormatChangedListener;
import com.google.android.exoplayer2.extractor.Extractor;
import com.google.android.exoplayer2.extractor.ExtractorInput;
import com.google.android.exoplayer2.extractor.ExtractorOutput;
import com.google.android.exoplayer2.extractor.PositionHolder;
import com.google.android.exoplayer2.extractor.SeekMap;
import com.google.android.exoplayer2.extractor.TrackOutput;
import com.google.android.exoplayer2.source.ExtractorMediaSource.EventListener;
import com.google.android.exoplayer2.source.MediaSource.Listener;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.upstream.Allocator;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DataSpec;
import com.google.android.exoplayer2.upstream.Loader;
import com.google.android.exoplayer2.upstream.Loader.Callback;
import com.google.android.exoplayer2.upstream.Loader.Loadable;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.ConditionVariable;
import com.google.android.exoplayer2.util.MimeTypes;
import com.google.android.exoplayer2.util.Util;
import java.io.EOFException;
import java.io.IOException;

final class ExtractorMediaPeriod implements MediaPeriod, ExtractorOutput, Callback<ExtractingLoadable>, UpstreamFormatChangedListener {
    private static final long DEFAULT_LAST_SAMPLE_DURATION_US = 10000;
    private final Allocator allocator;
    /* access modifiers changed from: private */
    public MediaPeriod.Callback callback;
    /* access modifiers changed from: private */
    public final String customCacheKey;
    private final DataSource dataSource;
    private long durationUs;
    private int enabledTrackCount;
    private final Handler eventHandler;
    /* access modifiers changed from: private */
    public final EventListener eventListener;
    private int extractedSamplesCountAtStartOfLoad;
    private final ExtractorHolder extractorHolder;
    /* access modifiers changed from: private */
    public final Handler handler;
    private boolean haveAudioVideoTracks;
    private long lastSeekPositionUs;
    private long length;
    private final ConditionVariable loadCondition;
    private final Loader loader = new Loader("Loader:ExtractorMediaPeriod");
    private boolean loadingFinished;
    private final Runnable maybeFinishPrepareRunnable;
    private final int minLoadableRetryCount;
    private boolean notifyReset;
    /* access modifiers changed from: private */
    public final Runnable onContinueLoadingRequestedRunnable;
    private long pendingResetPositionUs;
    private boolean prepared;
    /* access modifiers changed from: private */
    public boolean released;
    /* access modifiers changed from: private */
    public final SparseArray<DefaultTrackOutput> sampleQueues;
    private SeekMap seekMap;
    private boolean seenFirstTrackSelection;
    private final Listener sourceListener;
    private boolean[] trackEnabledStates;
    private boolean[] trackIsAudioVideoFlags;
    private TrackGroupArray tracks;
    private boolean tracksBuilt;
    private final Uri uri;

    final class ExtractingLoadable implements Loadable {
        private static final int CONTINUE_LOADING_CHECK_INTERVAL_BYTES = 1048576;
        private final DataSource dataSource;
        private final ExtractorHolder extractorHolder;
        /* access modifiers changed from: private */
        public long length = -1;
        private volatile boolean loadCanceled;
        private final ConditionVariable loadCondition;
        private boolean pendingExtractorSeek = true;
        private final PositionHolder positionHolder = new PositionHolder();
        private long seekTimeUs;
        private final Uri uri;

        public ExtractingLoadable(Uri uri2, DataSource dataSource2, ExtractorHolder extractorHolder2, ConditionVariable conditionVariable) {
            this.uri = (Uri) Assertions.checkNotNull(uri2);
            this.dataSource = (DataSource) Assertions.checkNotNull(dataSource2);
            this.extractorHolder = (ExtractorHolder) Assertions.checkNotNull(extractorHolder2);
            this.loadCondition = conditionVariable;
        }

        public void setLoadPosition(long j, long j2) {
            this.positionHolder.position = j;
            this.seekTimeUs = j2;
            this.pendingExtractorSeek = true;
        }

        public void cancelLoad() {
            this.loadCanceled = true;
        }

        public boolean isLoadCanceled() {
            return this.loadCanceled;
        }

        public void load() throws IOException, InterruptedException {
            DefaultExtractorInput defaultExtractorInput;
            int i = 0;
            while (i == 0 && !this.loadCanceled) {
                try {
                    long j = this.positionHolder.position;
                    DataSource dataSource2 = this.dataSource;
                    DataSpec dataSpec = new DataSpec(this.uri, j, -1, ExtractorMediaPeriod.this.customCacheKey);
                    this.length = dataSource2.open(dataSpec);
                    if (this.length != -1) {
                        this.length += j;
                    }
                    defaultExtractorInput = new DefaultExtractorInput(this.dataSource, j, this.length);
                    try {
                        Extractor selectExtractor = this.extractorHolder.selectExtractor(defaultExtractorInput, this.dataSource.getUri());
                        if (this.pendingExtractorSeek) {
                            selectExtractor.seek(j, this.seekTimeUs);
                            this.pendingExtractorSeek = false;
                        }
                        while (i == 0 && !this.loadCanceled) {
                            this.loadCondition.block();
                            int read = selectExtractor.read(defaultExtractorInput, this.positionHolder);
                            try {
                                if (defaultExtractorInput.getPosition() > PlaybackStateCompat.ACTION_SET_CAPTIONING_ENABLED + j) {
                                    j = defaultExtractorInput.getPosition();
                                    this.loadCondition.close();
                                    ExtractorMediaPeriod.this.handler.post(ExtractorMediaPeriod.this.onContinueLoadingRequestedRunnable);
                                }
                                i = read;
                            } catch (Throwable th) {
                                th = th;
                                i = read;
                                this.positionHolder.position = defaultExtractorInput.getPosition();
                                Util.closeQuietly(this.dataSource);
                                throw th;
                            }
                        }
                        if (i == 1) {
                            i = 0;
                        } else if (defaultExtractorInput != null) {
                            this.positionHolder.position = defaultExtractorInput.getPosition();
                        }
                        Util.closeQuietly(this.dataSource);
                    } catch (Throwable th2) {
                        th = th2;
                        if (!(i == 1 || defaultExtractorInput == null)) {
                            this.positionHolder.position = defaultExtractorInput.getPosition();
                        }
                        Util.closeQuietly(this.dataSource);
                        throw th;
                    }
                } catch (Throwable th3) {
                    th = th3;
                    defaultExtractorInput = null;
                    this.positionHolder.position = defaultExtractorInput.getPosition();
                    Util.closeQuietly(this.dataSource);
                    throw th;
                }
            }
        }
    }

    private static final class ExtractorHolder {
        private Extractor extractor;
        private final ExtractorOutput extractorOutput;
        private final Extractor[] extractors;

        public ExtractorHolder(Extractor[] extractorArr, ExtractorOutput extractorOutput2) {
            this.extractors = extractorArr;
            this.extractorOutput = extractorOutput2;
        }

        public Extractor selectExtractor(ExtractorInput extractorInput, Uri uri) throws IOException, InterruptedException {
            if (this.extractor != null) {
                return this.extractor;
            }
            Extractor[] extractorArr = this.extractors;
            int length = extractorArr.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    break;
                }
                Extractor extractor2 = extractorArr[i];
                try {
                    if (extractor2.sniff(extractorInput)) {
                        this.extractor = extractor2;
                        extractorInput.resetPeekPosition();
                        break;
                    }
                    extractorInput.resetPeekPosition();
                    i++;
                } catch (EOFException unused) {
                } catch (Throwable th) {
                    extractorInput.resetPeekPosition();
                    throw th;
                }
            }
            if (this.extractor == null) {
                StringBuilder sb = new StringBuilder();
                sb.append("None of the available extractors (");
                sb.append(Util.getCommaDelimitedSimpleClassNames(this.extractors));
                sb.append(") could read the stream.");
                throw new UnrecognizedInputFormatException(sb.toString(), uri);
            }
            this.extractor.init(this.extractorOutput);
            return this.extractor;
        }

        public void release() {
            if (this.extractor != null) {
                this.extractor.release();
                this.extractor = null;
            }
        }
    }

    private final class SampleStreamImpl implements SampleStream {
        /* access modifiers changed from: private */
        public final int track;

        public SampleStreamImpl(int i) {
            this.track = i;
        }

        public boolean isReady() {
            return ExtractorMediaPeriod.this.isReady(this.track);
        }

        public void maybeThrowError() throws IOException {
            ExtractorMediaPeriod.this.maybeThrowError();
        }

        public int readData(FormatHolder formatHolder, DecoderInputBuffer decoderInputBuffer, boolean z) {
            return ExtractorMediaPeriod.this.readData(this.track, formatHolder, decoderInputBuffer, z);
        }

        public void skipData(long j) {
            ExtractorMediaPeriod.this.skipData(this.track, j);
        }
    }

    public void discardBuffer(long j) {
    }

    public ExtractorMediaPeriod(Uri uri2, DataSource dataSource2, Extractor[] extractorArr, int i, Handler handler2, EventListener eventListener2, Listener listener, Allocator allocator2, String str) {
        this.uri = uri2;
        this.dataSource = dataSource2;
        this.minLoadableRetryCount = i;
        this.eventHandler = handler2;
        this.eventListener = eventListener2;
        this.sourceListener = listener;
        this.allocator = allocator2;
        this.customCacheKey = str;
        this.extractorHolder = new ExtractorHolder(extractorArr, this);
        this.loadCondition = new ConditionVariable();
        this.maybeFinishPrepareRunnable = new Runnable() {
            public void run() {
                ExtractorMediaPeriod.this.maybeFinishPrepare();
            }
        };
        this.onContinueLoadingRequestedRunnable = new Runnable() {
            public void run() {
                if (!ExtractorMediaPeriod.this.released) {
                    ExtractorMediaPeriod.this.callback.onContinueLoadingRequested(ExtractorMediaPeriod.this);
                }
            }
        };
        this.handler = new Handler();
        this.pendingResetPositionUs = C.TIME_UNSET;
        this.sampleQueues = new SparseArray<>();
        this.length = -1;
    }

    public void release() {
        final ExtractorHolder extractorHolder2 = this.extractorHolder;
        this.loader.release(new Runnable() {
            public void run() {
                extractorHolder2.release();
                int size = ExtractorMediaPeriod.this.sampleQueues.size();
                for (int i = 0; i < size; i++) {
                    ((DefaultTrackOutput) ExtractorMediaPeriod.this.sampleQueues.valueAt(i)).disable();
                }
            }
        });
        this.handler.removeCallbacksAndMessages(null);
        this.released = true;
    }

    public void prepare(MediaPeriod.Callback callback2) {
        this.callback = callback2;
        this.loadCondition.open();
        startLoading();
    }

    public void maybeThrowPrepareError() throws IOException {
        maybeThrowError();
    }

    public TrackGroupArray getTrackGroups() {
        return this.tracks;
    }

    public long selectTracks(TrackSelection[] trackSelectionArr, boolean[] zArr, SampleStream[] sampleStreamArr, boolean[] zArr2, long j) {
        Assertions.checkState(this.prepared);
        for (int i = 0; i < trackSelectionArr.length; i++) {
            if (sampleStreamArr[i] != null && (trackSelectionArr[i] == null || !zArr[i])) {
                int access$400 = sampleStreamArr[i].track;
                Assertions.checkState(this.trackEnabledStates[access$400]);
                this.enabledTrackCount--;
                this.trackEnabledStates[access$400] = false;
                ((DefaultTrackOutput) this.sampleQueues.valueAt(access$400)).disable();
                sampleStreamArr[i] = null;
            }
        }
        boolean z = false;
        for (int i2 = 0; i2 < trackSelectionArr.length; i2++) {
            if (sampleStreamArr[i2] == null && trackSelectionArr[i2] != null) {
                TrackSelection trackSelection = trackSelectionArr[i2];
                Assertions.checkState(trackSelection.length() == 1);
                Assertions.checkState(trackSelection.getIndexInTrackGroup(0) == 0);
                int indexOf = this.tracks.indexOf(trackSelection.getTrackGroup());
                Assertions.checkState(!this.trackEnabledStates[indexOf]);
                this.enabledTrackCount++;
                this.trackEnabledStates[indexOf] = true;
                sampleStreamArr[i2] = new SampleStreamImpl(indexOf);
                zArr2[i2] = true;
                z = true;
            }
        }
        if (!this.seenFirstTrackSelection) {
            int size = this.sampleQueues.size();
            for (int i3 = 0; i3 < size; i3++) {
                if (!this.trackEnabledStates[i3]) {
                    ((DefaultTrackOutput) this.sampleQueues.valueAt(i3)).disable();
                }
            }
        }
        if (this.enabledTrackCount == 0) {
            this.notifyReset = false;
            if (this.loader.isLoading()) {
                this.loader.cancelLoading();
            }
        } else if (!this.seenFirstTrackSelection ? j != 0 : z) {
            j = seekToUs(j);
            for (int i4 = 0; i4 < sampleStreamArr.length; i4++) {
                if (sampleStreamArr[i4] != null) {
                    zArr2[i4] = true;
                }
            }
        }
        this.seenFirstTrackSelection = true;
        return j;
    }

    public boolean continueLoading(long j) {
        if (this.loadingFinished || (this.prepared && this.enabledTrackCount == 0)) {
            return false;
        }
        boolean open = this.loadCondition.open();
        if (!this.loader.isLoading()) {
            startLoading();
            open = true;
        }
        return open;
    }

    public long getNextLoadPositionUs() {
        if (this.enabledTrackCount == 0) {
            return Long.MIN_VALUE;
        }
        return getBufferedPositionUs();
    }

    public long readDiscontinuity() {
        if (!this.notifyReset) {
            return C.TIME_UNSET;
        }
        this.notifyReset = false;
        return this.lastSeekPositionUs;
    }

    public long getBufferedPositionUs() {
        long j;
        if (this.loadingFinished) {
            return Long.MIN_VALUE;
        }
        if (isPendingReset()) {
            return this.pendingResetPositionUs;
        }
        if (this.haveAudioVideoTracks) {
            j = Long.MAX_VALUE;
            int size = this.sampleQueues.size();
            for (int i = 0; i < size; i++) {
                if (this.trackIsAudioVideoFlags[i]) {
                    j = Math.min(j, ((DefaultTrackOutput) this.sampleQueues.valueAt(i)).getLargestQueuedTimestampUs());
                }
            }
        } else {
            j = getLargestQueuedTimestampUs();
        }
        if (j == Long.MIN_VALUE) {
            j = this.lastSeekPositionUs;
        }
        return j;
    }

    public long seekToUs(long j) {
        if (!this.seekMap.isSeekable()) {
            j = 0;
        }
        this.lastSeekPositionUs = j;
        int size = this.sampleQueues.size();
        boolean z = !isPendingReset();
        int i = 0;
        while (z && i < size) {
            if (this.trackEnabledStates[i]) {
                z = ((DefaultTrackOutput) this.sampleQueues.valueAt(i)).skipToKeyframeBefore(j, false);
            }
            i++;
        }
        if (!z) {
            this.pendingResetPositionUs = j;
            this.loadingFinished = false;
            if (this.loader.isLoading()) {
                this.loader.cancelLoading();
            } else {
                for (int i2 = 0; i2 < size; i2++) {
                    ((DefaultTrackOutput) this.sampleQueues.valueAt(i2)).reset(this.trackEnabledStates[i2]);
                }
            }
        }
        this.notifyReset = false;
        return j;
    }

    /* access modifiers changed from: 0000 */
    public boolean isReady(int i) {
        return this.loadingFinished || (!isPendingReset() && !((DefaultTrackOutput) this.sampleQueues.valueAt(i)).isEmpty());
    }

    /* access modifiers changed from: 0000 */
    public void maybeThrowError() throws IOException {
        this.loader.maybeThrowError();
    }

    /* access modifiers changed from: 0000 */
    public int readData(int i, FormatHolder formatHolder, DecoderInputBuffer decoderInputBuffer, boolean z) {
        if (this.notifyReset || isPendingReset()) {
            return -3;
        }
        return ((DefaultTrackOutput) this.sampleQueues.valueAt(i)).readData(formatHolder, decoderInputBuffer, z, this.loadingFinished, this.lastSeekPositionUs);
    }

    /* access modifiers changed from: 0000 */
    public void skipData(int i, long j) {
        DefaultTrackOutput defaultTrackOutput = (DefaultTrackOutput) this.sampleQueues.valueAt(i);
        if (!this.loadingFinished || j <= defaultTrackOutput.getLargestQueuedTimestampUs()) {
            defaultTrackOutput.skipToKeyframeBefore(j, true);
        } else {
            defaultTrackOutput.skipAll();
        }
    }

    public void onLoadCompleted(ExtractingLoadable extractingLoadable, long j, long j2) {
        copyLengthFromLoader(extractingLoadable);
        this.loadingFinished = true;
        if (this.durationUs == C.TIME_UNSET) {
            long largestQueuedTimestampUs = getLargestQueuedTimestampUs();
            this.durationUs = largestQueuedTimestampUs == Long.MIN_VALUE ? 0 : largestQueuedTimestampUs + 10000;
            this.sourceListener.onSourceInfoRefreshed(new SinglePeriodTimeline(this.durationUs, this.seekMap.isSeekable()), null);
        }
        this.callback.onContinueLoadingRequested(this);
    }

    public void onLoadCanceled(ExtractingLoadable extractingLoadable, long j, long j2, boolean z) {
        copyLengthFromLoader(extractingLoadable);
        if (!z && this.enabledTrackCount > 0) {
            int size = this.sampleQueues.size();
            for (int i = 0; i < size; i++) {
                ((DefaultTrackOutput) this.sampleQueues.valueAt(i)).reset(this.trackEnabledStates[i]);
            }
            this.callback.onContinueLoadingRequested(this);
        }
    }

    public int onLoadError(ExtractingLoadable extractingLoadable, long j, long j2, IOException iOException) {
        copyLengthFromLoader(extractingLoadable);
        notifyLoadError(iOException);
        if (isLoadableExceptionFatal(iOException)) {
            return 3;
        }
        int i = getExtractedSamplesCount() > this.extractedSamplesCountAtStartOfLoad ? 1 : 0;
        configureRetry(extractingLoadable);
        this.extractedSamplesCountAtStartOfLoad = getExtractedSamplesCount();
        return i;
    }

    public TrackOutput track(int i, int i2) {
        DefaultTrackOutput defaultTrackOutput = (DefaultTrackOutput) this.sampleQueues.get(i);
        if (defaultTrackOutput != null) {
            return defaultTrackOutput;
        }
        DefaultTrackOutput defaultTrackOutput2 = new DefaultTrackOutput(this.allocator);
        defaultTrackOutput2.setUpstreamFormatChangeListener(this);
        this.sampleQueues.put(i, defaultTrackOutput2);
        return defaultTrackOutput2;
    }

    public void endTracks() {
        this.tracksBuilt = true;
        this.handler.post(this.maybeFinishPrepareRunnable);
    }

    public void seekMap(SeekMap seekMap2) {
        this.seekMap = seekMap2;
        this.handler.post(this.maybeFinishPrepareRunnable);
    }

    public void onUpstreamFormatChanged(Format format) {
        this.handler.post(this.maybeFinishPrepareRunnable);
    }

    /* access modifiers changed from: private */
    public void maybeFinishPrepare() {
        if (!this.released && !this.prepared && this.seekMap != null && this.tracksBuilt) {
            int size = this.sampleQueues.size();
            int i = 0;
            while (i < size) {
                if (((DefaultTrackOutput) this.sampleQueues.valueAt(i)).getUpstreamFormat() != null) {
                    i++;
                } else {
                    return;
                }
            }
            this.loadCondition.close();
            TrackGroup[] trackGroupArr = new TrackGroup[size];
            this.trackIsAudioVideoFlags = new boolean[size];
            this.trackEnabledStates = new boolean[size];
            this.durationUs = this.seekMap.getDurationUs();
            int i2 = 0;
            while (true) {
                boolean z = true;
                if (i2 < size) {
                    Format upstreamFormat = ((DefaultTrackOutput) this.sampleQueues.valueAt(i2)).getUpstreamFormat();
                    trackGroupArr[i2] = new TrackGroup(upstreamFormat);
                    String str = upstreamFormat.sampleMimeType;
                    if (!MimeTypes.isVideo(str) && !MimeTypes.isAudio(str)) {
                        z = false;
                    }
                    this.trackIsAudioVideoFlags[i2] = z;
                    this.haveAudioVideoTracks = z | this.haveAudioVideoTracks;
                    i2++;
                } else {
                    this.tracks = new TrackGroupArray(trackGroupArr);
                    this.prepared = true;
                    this.sourceListener.onSourceInfoRefreshed(new SinglePeriodTimeline(this.durationUs, this.seekMap.isSeekable()), null);
                    this.callback.onPrepared(this);
                    return;
                }
            }
        }
    }

    private void copyLengthFromLoader(ExtractingLoadable extractingLoadable) {
        if (this.length == -1) {
            this.length = extractingLoadable.length;
        }
    }

    private void startLoading() {
        ExtractingLoadable extractingLoadable = new ExtractingLoadable(this.uri, this.dataSource, this.extractorHolder, this.loadCondition);
        if (this.prepared) {
            Assertions.checkState(isPendingReset());
            if (this.durationUs == C.TIME_UNSET || this.pendingResetPositionUs < this.durationUs) {
                extractingLoadable.setLoadPosition(this.seekMap.getPosition(this.pendingResetPositionUs), this.pendingResetPositionUs);
                this.pendingResetPositionUs = C.TIME_UNSET;
            } else {
                this.loadingFinished = true;
                this.pendingResetPositionUs = C.TIME_UNSET;
                return;
            }
        }
        this.extractedSamplesCountAtStartOfLoad = getExtractedSamplesCount();
        int i = this.minLoadableRetryCount;
        if (i == -1) {
            i = (this.prepared && this.length == -1 && (this.seekMap == null || this.seekMap.getDurationUs() == C.TIME_UNSET)) ? 6 : 3;
        }
        this.loader.startLoading(extractingLoadable, this, i);
    }

    private void configureRetry(ExtractingLoadable extractingLoadable) {
        if (this.length != -1) {
            return;
        }
        if (this.seekMap == null || this.seekMap.getDurationUs() == C.TIME_UNSET) {
            this.lastSeekPositionUs = 0;
            this.notifyReset = this.prepared;
            int size = this.sampleQueues.size();
            for (int i = 0; i < size; i++) {
                ((DefaultTrackOutput) this.sampleQueues.valueAt(i)).reset(!this.prepared || this.trackEnabledStates[i]);
            }
            extractingLoadable.setLoadPosition(0, 0);
        }
    }

    private int getExtractedSamplesCount() {
        int i = 0;
        for (int i2 = 0; i2 < this.sampleQueues.size(); i2++) {
            i += ((DefaultTrackOutput) this.sampleQueues.valueAt(i2)).getWriteIndex();
        }
        return i;
    }

    private long getLargestQueuedTimestampUs() {
        int size = this.sampleQueues.size();
        long j = Long.MIN_VALUE;
        for (int i = 0; i < size; i++) {
            j = Math.max(j, ((DefaultTrackOutput) this.sampleQueues.valueAt(i)).getLargestQueuedTimestampUs());
        }
        return j;
    }

    private boolean isPendingReset() {
        return this.pendingResetPositionUs != C.TIME_UNSET;
    }

    private boolean isLoadableExceptionFatal(IOException iOException) {
        return iOException instanceof UnrecognizedInputFormatException;
    }

    private void notifyLoadError(final IOException iOException) {
        if (this.eventHandler != null && this.eventListener != null) {
            this.eventHandler.post(new Runnable() {
                public void run() {
                    ExtractorMediaPeriod.this.eventListener.onLoadError(iOException);
                }
            });
        }
    }
}
