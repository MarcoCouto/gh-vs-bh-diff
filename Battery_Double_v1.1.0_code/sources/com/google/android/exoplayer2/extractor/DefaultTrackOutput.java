package com.google.android.exoplayer2.extractor;

import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.FormatHolder;
import com.google.android.exoplayer2.decoder.DecoderInputBuffer;
import com.google.android.exoplayer2.upstream.Allocation;
import com.google.android.exoplayer2.upstream.Allocator;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.ParsableByteArray;
import com.google.android.exoplayer2.util.Util;
import java.io.EOFException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.atomic.AtomicInteger;

public final class DefaultTrackOutput implements TrackOutput {
    private static final int INITIAL_SCRATCH_SIZE = 32;
    private static final int STATE_DISABLED = 2;
    private static final int STATE_ENABLED = 0;
    private static final int STATE_ENABLED_WRITING = 1;
    private final int allocationLength;
    private final Allocator allocator;
    private final LinkedBlockingDeque<Allocation> dataQueue = new LinkedBlockingDeque<>();
    private Format downstreamFormat;
    private final BufferExtrasHolder extrasHolder = new BufferExtrasHolder();
    private final InfoQueue infoQueue = new InfoQueue();
    private Allocation lastAllocation;
    private int lastAllocationOffset = this.allocationLength;
    private Format lastUnadjustedFormat;
    private boolean pendingFormatAdjustment;
    private boolean pendingSplice;
    private long sampleOffsetUs;
    private final ParsableByteArray scratch = new ParsableByteArray(32);
    private final AtomicInteger state = new AtomicInteger();
    private long totalBytesDropped;
    private long totalBytesWritten;
    private UpstreamFormatChangedListener upstreamFormatChangeListener;

    private static final class BufferExtrasHolder {
        public byte[] encryptionKeyId;
        public long nextOffset;
        public long offset;
        public int size;

        private BufferExtrasHolder() {
        }
    }

    private static final class InfoQueue {
        private static final int SAMPLE_CAPACITY_INCREMENT = 1000;
        private int absoluteReadIndex;
        private int capacity = 1000;
        private byte[][] encryptionKeys = new byte[this.capacity][];
        private int[] flags = new int[this.capacity];
        private Format[] formats = new Format[this.capacity];
        private long largestDequeuedTimestampUs = Long.MIN_VALUE;
        private long largestQueuedTimestampUs = Long.MIN_VALUE;
        private long[] offsets = new long[this.capacity];
        private int queueSize;
        private int relativeReadIndex;
        private int relativeWriteIndex;
        private int[] sizes = new int[this.capacity];
        private int[] sourceIds = new int[this.capacity];
        private long[] timesUs = new long[this.capacity];
        private Format upstreamFormat;
        private boolean upstreamFormatRequired = true;
        private boolean upstreamKeyframeRequired = true;
        private int upstreamSourceId;

        public void clearSampleData() {
            this.absoluteReadIndex = 0;
            this.relativeReadIndex = 0;
            this.relativeWriteIndex = 0;
            this.queueSize = 0;
            this.upstreamKeyframeRequired = true;
        }

        public void resetLargestParsedTimestamps() {
            this.largestDequeuedTimestampUs = Long.MIN_VALUE;
            this.largestQueuedTimestampUs = Long.MIN_VALUE;
        }

        public int getWriteIndex() {
            return this.absoluteReadIndex + this.queueSize;
        }

        public long discardUpstreamSamples(int i) {
            int writeIndex = getWriteIndex() - i;
            Assertions.checkArgument(writeIndex >= 0 && writeIndex <= this.queueSize);
            if (writeIndex != 0) {
                this.queueSize -= writeIndex;
                this.relativeWriteIndex = ((this.relativeWriteIndex + this.capacity) - writeIndex) % this.capacity;
                this.largestQueuedTimestampUs = Long.MIN_VALUE;
                for (int i2 = this.queueSize - 1; i2 >= 0; i2--) {
                    int i3 = (this.relativeReadIndex + i2) % this.capacity;
                    this.largestQueuedTimestampUs = Math.max(this.largestQueuedTimestampUs, this.timesUs[i3]);
                    if ((this.flags[i3] & 1) != 0) {
                        break;
                    }
                }
                return this.offsets[this.relativeWriteIndex];
            } else if (this.absoluteReadIndex == 0) {
                return 0;
            } else {
                int i4 = (this.relativeWriteIndex == 0 ? this.capacity : this.relativeWriteIndex) - 1;
                return this.offsets[i4] + ((long) this.sizes[i4]);
            }
        }

        public void sourceId(int i) {
            this.upstreamSourceId = i;
        }

        public int getReadIndex() {
            return this.absoluteReadIndex;
        }

        public int peekSourceId() {
            return this.queueSize == 0 ? this.upstreamSourceId : this.sourceIds[this.relativeReadIndex];
        }

        public synchronized boolean isEmpty() {
            return this.queueSize == 0;
        }

        public synchronized Format getUpstreamFormat() {
            return this.upstreamFormatRequired ? null : this.upstreamFormat;
        }

        public synchronized long getLargestQueuedTimestampUs() {
            return Math.max(this.largestDequeuedTimestampUs, this.largestQueuedTimestampUs);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:18:0x0021, code lost:
            return -3;
         */
        public synchronized int readData(FormatHolder formatHolder, DecoderInputBuffer decoderInputBuffer, boolean z, boolean z2, Format format, BufferExtrasHolder bufferExtrasHolder) {
            if (this.queueSize != 0) {
                if (!z) {
                    if (this.formats[this.relativeReadIndex] == format) {
                        if (decoderInputBuffer.isFlagsOnly()) {
                            return -3;
                        }
                        decoderInputBuffer.timeUs = this.timesUs[this.relativeReadIndex];
                        decoderInputBuffer.setFlags(this.flags[this.relativeReadIndex]);
                        bufferExtrasHolder.size = this.sizes[this.relativeReadIndex];
                        bufferExtrasHolder.offset = this.offsets[this.relativeReadIndex];
                        bufferExtrasHolder.encryptionKeyId = this.encryptionKeys[this.relativeReadIndex];
                        this.largestDequeuedTimestampUs = Math.max(this.largestDequeuedTimestampUs, decoderInputBuffer.timeUs);
                        this.queueSize--;
                        this.relativeReadIndex++;
                        this.absoluteReadIndex++;
                        if (this.relativeReadIndex == this.capacity) {
                            this.relativeReadIndex = 0;
                        }
                        bufferExtrasHolder.nextOffset = this.queueSize > 0 ? this.offsets[this.relativeReadIndex] : bufferExtrasHolder.offset + ((long) bufferExtrasHolder.size);
                        return -4;
                    }
                }
                formatHolder.format = this.formats[this.relativeReadIndex];
                return -5;
            } else if (z2) {
                decoderInputBuffer.setFlags(4);
                return -4;
            } else if (this.upstreamFormat != null && (z || this.upstreamFormat != format)) {
                formatHolder.format = this.upstreamFormat;
                return -5;
            }
        }

        public synchronized long skipAll() {
            if (this.queueSize == 0) {
                return -1;
            }
            int i = ((this.relativeReadIndex + this.queueSize) - 1) % this.capacity;
            this.relativeReadIndex = (this.relativeReadIndex + this.queueSize) % this.capacity;
            this.absoluteReadIndex += this.queueSize;
            this.queueSize = 0;
            return this.offsets[i] + ((long) this.sizes[i]);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:34:0x005f, code lost:
            return -1;
         */
        public synchronized long skipToKeyframeBefore(long j, boolean z) {
            if (this.queueSize != 0) {
                if (j >= this.timesUs[this.relativeReadIndex]) {
                    if (j > this.largestQueuedTimestampUs && !z) {
                        return -1;
                    }
                    int i = this.relativeReadIndex;
                    int i2 = 0;
                    int i3 = -1;
                    while (true) {
                        if (i == this.relativeWriteIndex) {
                            break;
                        } else if (this.timesUs[i] > j) {
                            break;
                        } else {
                            if ((this.flags[i] & 1) != 0) {
                                i3 = i2;
                            }
                            i = (i + 1) % this.capacity;
                            i2++;
                        }
                    }
                    if (i3 == -1) {
                        return -1;
                    }
                    this.relativeReadIndex = (this.relativeReadIndex + i3) % this.capacity;
                    this.absoluteReadIndex += i3;
                    this.queueSize -= i3;
                    return this.offsets[this.relativeReadIndex];
                }
            }
        }

        public synchronized boolean format(Format format) {
            if (format == null) {
                this.upstreamFormatRequired = true;
                return false;
            }
            this.upstreamFormatRequired = false;
            if (Util.areEqual(format, this.upstreamFormat)) {
                return false;
            }
            this.upstreamFormat = format;
            return true;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:18:0x00e9, code lost:
            return;
         */
        public synchronized void commitSample(long j, int i, long j2, int i2, byte[] bArr) {
            if (this.upstreamKeyframeRequired) {
                if ((i & 1) != 0) {
                    this.upstreamKeyframeRequired = false;
                } else {
                    return;
                }
            }
            Assertions.checkState(!this.upstreamFormatRequired);
            commitSampleTimestamp(j);
            this.timesUs[this.relativeWriteIndex] = j;
            this.offsets[this.relativeWriteIndex] = j2;
            this.sizes[this.relativeWriteIndex] = i2;
            this.flags[this.relativeWriteIndex] = i;
            this.encryptionKeys[this.relativeWriteIndex] = bArr;
            this.formats[this.relativeWriteIndex] = this.upstreamFormat;
            this.sourceIds[this.relativeWriteIndex] = this.upstreamSourceId;
            this.queueSize++;
            if (this.queueSize == this.capacity) {
                int i3 = this.capacity + 1000;
                int[] iArr = new int[i3];
                long[] jArr = new long[i3];
                long[] jArr2 = new long[i3];
                int[] iArr2 = new int[i3];
                int[] iArr3 = new int[i3];
                byte[][] bArr2 = new byte[i3][];
                Format[] formatArr = new Format[i3];
                int i4 = this.capacity - this.relativeReadIndex;
                System.arraycopy(this.offsets, this.relativeReadIndex, jArr, 0, i4);
                System.arraycopy(this.timesUs, this.relativeReadIndex, jArr2, 0, i4);
                System.arraycopy(this.flags, this.relativeReadIndex, iArr2, 0, i4);
                System.arraycopy(this.sizes, this.relativeReadIndex, iArr3, 0, i4);
                System.arraycopy(this.encryptionKeys, this.relativeReadIndex, bArr2, 0, i4);
                System.arraycopy(this.formats, this.relativeReadIndex, formatArr, 0, i4);
                System.arraycopy(this.sourceIds, this.relativeReadIndex, iArr, 0, i4);
                int i5 = this.relativeReadIndex;
                System.arraycopy(this.offsets, 0, jArr, i4, i5);
                System.arraycopy(this.timesUs, 0, jArr2, i4, i5);
                System.arraycopy(this.flags, 0, iArr2, i4, i5);
                System.arraycopy(this.sizes, 0, iArr3, i4, i5);
                System.arraycopy(this.encryptionKeys, 0, bArr2, i4, i5);
                System.arraycopy(this.formats, 0, formatArr, i4, i5);
                System.arraycopy(this.sourceIds, 0, iArr, i4, i5);
                this.offsets = jArr;
                this.timesUs = jArr2;
                this.flags = iArr2;
                this.sizes = iArr3;
                this.encryptionKeys = bArr2;
                this.formats = formatArr;
                this.sourceIds = iArr;
                this.relativeReadIndex = 0;
                this.relativeWriteIndex = this.capacity;
                this.queueSize = this.capacity;
                this.capacity = i3;
            } else {
                this.relativeWriteIndex++;
                if (this.relativeWriteIndex == this.capacity) {
                    this.relativeWriteIndex = 0;
                }
            }
        }

        public synchronized void commitSampleTimestamp(long j) {
            this.largestQueuedTimestampUs = Math.max(this.largestQueuedTimestampUs, j);
        }

        public synchronized boolean attemptSplice(long j) {
            if (this.largestDequeuedTimestampUs >= j) {
                return false;
            }
            int i = this.queueSize;
            while (i > 0 && this.timesUs[((this.relativeReadIndex + i) - 1) % this.capacity] >= j) {
                i--;
            }
            discardUpstreamSamples(this.absoluteReadIndex + i);
            return true;
        }
    }

    public interface UpstreamFormatChangedListener {
        void onUpstreamFormatChanged(Format format);
    }

    public DefaultTrackOutput(Allocator allocator2) {
        this.allocator = allocator2;
        this.allocationLength = allocator2.getIndividualAllocationLength();
    }

    public void reset(boolean z) {
        int andSet = this.state.getAndSet(z ? 0 : 2);
        clearSampleData();
        this.infoQueue.resetLargestParsedTimestamps();
        if (andSet == 2) {
            this.downstreamFormat = null;
        }
    }

    public void sourceId(int i) {
        this.infoQueue.sourceId(i);
    }

    public void splice() {
        this.pendingSplice = true;
    }

    public int getWriteIndex() {
        return this.infoQueue.getWriteIndex();
    }

    public void discardUpstreamSamples(int i) {
        this.totalBytesWritten = this.infoQueue.discardUpstreamSamples(i);
        dropUpstreamFrom(this.totalBytesWritten);
    }

    private void dropUpstreamFrom(long j) {
        int i = (int) (j - this.totalBytesDropped);
        int i2 = i / this.allocationLength;
        int i3 = i % this.allocationLength;
        int size = (this.dataQueue.size() - i2) - 1;
        if (i3 == 0) {
            size++;
        }
        for (int i4 = 0; i4 < size; i4++) {
            this.allocator.release((Allocation) this.dataQueue.removeLast());
        }
        this.lastAllocation = (Allocation) this.dataQueue.peekLast();
        if (i3 == 0) {
            i3 = this.allocationLength;
        }
        this.lastAllocationOffset = i3;
    }

    public void disable() {
        if (this.state.getAndSet(2) == 0) {
            clearSampleData();
        }
    }

    public boolean isEmpty() {
        return this.infoQueue.isEmpty();
    }

    public int getReadIndex() {
        return this.infoQueue.getReadIndex();
    }

    public int peekSourceId() {
        return this.infoQueue.peekSourceId();
    }

    public Format getUpstreamFormat() {
        return this.infoQueue.getUpstreamFormat();
    }

    public long getLargestQueuedTimestampUs() {
        return this.infoQueue.getLargestQueuedTimestampUs();
    }

    public void skipAll() {
        long skipAll = this.infoQueue.skipAll();
        if (skipAll != -1) {
            dropDownstreamTo(skipAll);
        }
    }

    public boolean skipToKeyframeBefore(long j, boolean z) {
        long skipToKeyframeBefore = this.infoQueue.skipToKeyframeBefore(j, z);
        if (skipToKeyframeBefore == -1) {
            return false;
        }
        dropDownstreamTo(skipToKeyframeBefore);
        return true;
    }

    public int readData(FormatHolder formatHolder, DecoderInputBuffer decoderInputBuffer, boolean z, boolean z2, long j) {
        switch (this.infoQueue.readData(formatHolder, decoderInputBuffer, z, z2, this.downstreamFormat, this.extrasHolder)) {
            case C.RESULT_FORMAT_READ /*-5*/:
                this.downstreamFormat = formatHolder.format;
                return -5;
            case -4:
                if (!decoderInputBuffer.isEndOfStream()) {
                    if (decoderInputBuffer.timeUs < j) {
                        decoderInputBuffer.addFlag(Integer.MIN_VALUE);
                    }
                    if (decoderInputBuffer.isEncrypted()) {
                        readEncryptionData(decoderInputBuffer, this.extrasHolder);
                    }
                    decoderInputBuffer.ensureSpaceForWrite(this.extrasHolder.size);
                    readData(this.extrasHolder.offset, decoderInputBuffer.data, this.extrasHolder.size);
                    dropDownstreamTo(this.extrasHolder.nextOffset);
                }
                return -4;
            case -3:
                return -3;
            default:
                throw new IllegalStateException();
        }
    }

    private void readEncryptionData(DecoderInputBuffer decoderInputBuffer, BufferExtrasHolder bufferExtrasHolder) {
        long j = bufferExtrasHolder.offset;
        int i = 1;
        this.scratch.reset(1);
        readData(j, this.scratch.data, 1);
        long j2 = j + 1;
        byte b = this.scratch.data[0];
        boolean z = (b & 128) != 0;
        byte b2 = b & Byte.MAX_VALUE;
        if (decoderInputBuffer.cryptoInfo.iv == null) {
            decoderInputBuffer.cryptoInfo.iv = new byte[16];
        }
        readData(j2, decoderInputBuffer.cryptoInfo.iv, (int) b2);
        long j3 = j2 + ((long) b2);
        if (z) {
            this.scratch.reset(2);
            readData(j3, this.scratch.data, 2);
            j3 += 2;
            i = this.scratch.readUnsignedShort();
        }
        int i2 = i;
        int[] iArr = decoderInputBuffer.cryptoInfo.numBytesOfClearData;
        if (iArr == null || iArr.length < i2) {
            iArr = new int[i2];
        }
        int[] iArr2 = iArr;
        int[] iArr3 = decoderInputBuffer.cryptoInfo.numBytesOfEncryptedData;
        if (iArr3 == null || iArr3.length < i2) {
            iArr3 = new int[i2];
        }
        int[] iArr4 = iArr3;
        if (z) {
            int i3 = 6 * i2;
            this.scratch.reset(i3);
            readData(j3, this.scratch.data, i3);
            j3 += (long) i3;
            this.scratch.setPosition(0);
            for (int i4 = 0; i4 < i2; i4++) {
                iArr2[i4] = this.scratch.readUnsignedShort();
                iArr4[i4] = this.scratch.readUnsignedIntToInt();
            }
        } else {
            iArr2[0] = 0;
            iArr4[0] = bufferExtrasHolder.size - ((int) (j3 - bufferExtrasHolder.offset));
        }
        decoderInputBuffer.cryptoInfo.set(i2, iArr2, iArr4, bufferExtrasHolder.encryptionKeyId, decoderInputBuffer.cryptoInfo.iv, 1);
        int i5 = (int) (j3 - bufferExtrasHolder.offset);
        bufferExtrasHolder.offset += (long) i5;
        bufferExtrasHolder.size -= i5;
    }

    private void readData(long j, ByteBuffer byteBuffer, int i) {
        while (i > 0) {
            dropDownstreamTo(j);
            int i2 = (int) (j - this.totalBytesDropped);
            int min = Math.min(i, this.allocationLength - i2);
            Allocation allocation = (Allocation) this.dataQueue.peek();
            byteBuffer.put(allocation.data, allocation.translateOffset(i2), min);
            j += (long) min;
            i -= min;
        }
    }

    private void readData(long j, byte[] bArr, int i) {
        int i2 = 0;
        while (i2 < i) {
            dropDownstreamTo(j);
            int i3 = (int) (j - this.totalBytesDropped);
            int min = Math.min(i - i2, this.allocationLength - i3);
            Allocation allocation = (Allocation) this.dataQueue.peek();
            System.arraycopy(allocation.data, allocation.translateOffset(i3), bArr, i2, min);
            j += (long) min;
            i2 += min;
        }
    }

    private void dropDownstreamTo(long j) {
        int i = ((int) (j - this.totalBytesDropped)) / this.allocationLength;
        for (int i2 = 0; i2 < i; i2++) {
            this.allocator.release((Allocation) this.dataQueue.remove());
            this.totalBytesDropped += (long) this.allocationLength;
        }
    }

    public void setUpstreamFormatChangeListener(UpstreamFormatChangedListener upstreamFormatChangedListener) {
        this.upstreamFormatChangeListener = upstreamFormatChangedListener;
    }

    public void setSampleOffsetUs(long j) {
        if (this.sampleOffsetUs != j) {
            this.sampleOffsetUs = j;
            this.pendingFormatAdjustment = true;
        }
    }

    public void format(Format format) {
        Format adjustedSampleFormat = getAdjustedSampleFormat(format, this.sampleOffsetUs);
        boolean format2 = this.infoQueue.format(adjustedSampleFormat);
        this.lastUnadjustedFormat = format;
        this.pendingFormatAdjustment = false;
        if (this.upstreamFormatChangeListener != null && format2) {
            this.upstreamFormatChangeListener.onUpstreamFormatChanged(adjustedSampleFormat);
        }
    }

    public int sampleData(ExtractorInput extractorInput, int i, boolean z) throws IOException, InterruptedException {
        if (!startWriteOperation()) {
            int skip = extractorInput.skip(i);
            if (skip != -1) {
                return skip;
            }
            if (z) {
                return -1;
            }
            throw new EOFException();
        }
        try {
            int read = extractorInput.read(this.lastAllocation.data, this.lastAllocation.translateOffset(this.lastAllocationOffset), prepareForAppend(i));
            if (read != -1) {
                this.lastAllocationOffset += read;
                this.totalBytesWritten += (long) read;
                endWriteOperation();
                return read;
            } else if (z) {
                return -1;
            } else {
                throw new EOFException();
            }
        } finally {
            endWriteOperation();
        }
    }

    public void sampleData(ParsableByteArray parsableByteArray, int i) {
        if (!startWriteOperation()) {
            parsableByteArray.skipBytes(i);
            return;
        }
        while (i > 0) {
            int prepareForAppend = prepareForAppend(i);
            parsableByteArray.readBytes(this.lastAllocation.data, this.lastAllocation.translateOffset(this.lastAllocationOffset), prepareForAppend);
            this.lastAllocationOffset += prepareForAppend;
            this.totalBytesWritten += (long) prepareForAppend;
            i -= prepareForAppend;
        }
        endWriteOperation();
    }

    public void sampleMetadata(long j, int i, int i2, int i3, byte[] bArr) {
        long j2 = j;
        if (this.pendingFormatAdjustment) {
            format(this.lastUnadjustedFormat);
        }
        if (!startWriteOperation()) {
            this.infoQueue.commitSampleTimestamp(j2);
            return;
        }
        try {
            if (this.pendingSplice) {
                if ((i & 1) != 0) {
                    if (this.infoQueue.attemptSplice(j2)) {
                        this.pendingSplice = false;
                    }
                }
                endWriteOperation();
                return;
            }
            int i4 = i2;
            this.infoQueue.commitSample(j2 + this.sampleOffsetUs, i, (this.totalBytesWritten - ((long) i4)) - ((long) i3), i4, bArr);
            endWriteOperation();
        } catch (Throwable th) {
            Throwable th2 = th;
            endWriteOperation();
            throw th2;
        }
    }

    private boolean startWriteOperation() {
        return this.state.compareAndSet(0, 1);
    }

    private void endWriteOperation() {
        if (!this.state.compareAndSet(1, 0)) {
            clearSampleData();
        }
    }

    private void clearSampleData() {
        this.infoQueue.clearSampleData();
        this.allocator.release((Allocation[]) this.dataQueue.toArray(new Allocation[this.dataQueue.size()]));
        this.dataQueue.clear();
        this.allocator.trim();
        this.totalBytesDropped = 0;
        this.totalBytesWritten = 0;
        this.lastAllocation = null;
        this.lastAllocationOffset = this.allocationLength;
    }

    private int prepareForAppend(int i) {
        if (this.lastAllocationOffset == this.allocationLength) {
            this.lastAllocationOffset = 0;
            this.lastAllocation = this.allocator.allocate();
            this.dataQueue.add(this.lastAllocation);
        }
        return Math.min(i, this.allocationLength - this.lastAllocationOffset);
    }

    private static Format getAdjustedSampleFormat(Format format, long j) {
        if (format == null) {
            return null;
        }
        if (!(j == 0 || format.subsampleOffsetUs == Long.MAX_VALUE)) {
            format = format.copyWithSubsampleOffsetUs(format.subsampleOffsetUs + j);
        }
        return format;
    }
}
