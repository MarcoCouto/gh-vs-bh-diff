package com.google.android.exoplayer2.upstream.cache;

import android.support.annotation.NonNull;
import android.util.Log;
import com.google.android.exoplayer2.extractor.ChunkIndex;
import com.google.android.exoplayer2.upstream.cache.Cache.Listener;
import java.util.Arrays;
import java.util.Iterator;
import java.util.NavigableSet;
import java.util.TreeSet;

public final class CachedRegionTracker implements Listener {
    public static final int CACHED_TO_END = -2;
    public static final int NOT_CACHED = -1;
    private static final String TAG = "CachedRegionTracker";
    private final Cache cache;
    private final String cacheKey;
    private final ChunkIndex chunkIndex;
    private final Region lookupRegion = new Region(0, 0);
    private final TreeSet<Region> regions = new TreeSet<>();

    private static class Region implements Comparable<Region> {
        public long endOffset;
        public int endOffsetIndex;
        public long startOffset;

        public Region(long j, long j2) {
            this.startOffset = j;
            this.endOffset = j2;
        }

        public int compareTo(@NonNull Region region) {
            if (this.startOffset < region.startOffset) {
                return -1;
            }
            return this.startOffset == region.startOffset ? 0 : 1;
        }
    }

    public void onSpanTouched(Cache cache2, CacheSpan cacheSpan, CacheSpan cacheSpan2) {
    }

    public CachedRegionTracker(Cache cache2, String str, ChunkIndex chunkIndex2) {
        this.cache = cache2;
        this.cacheKey = str;
        this.chunkIndex = chunkIndex2;
        synchronized (this) {
            NavigableSet addListener = cache2.addListener(str, this);
            if (addListener != null) {
                Iterator descendingIterator = addListener.descendingIterator();
                while (descendingIterator.hasNext()) {
                    mergeSpan((CacheSpan) descendingIterator.next());
                }
            }
        }
    }

    public void release() {
        this.cache.removeListener(this.cacheKey, this);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0064, code lost:
        return -1;
     */
    public synchronized int getRegionEndTimeMs(long j) {
        this.lookupRegion.startOffset = j;
        Region region = (Region) this.regions.floor(this.lookupRegion);
        if (region != null && j <= region.endOffset) {
            if (region.endOffsetIndex != -1) {
                int i = region.endOffsetIndex;
                if (i == this.chunkIndex.length - 1 && region.endOffset == this.chunkIndex.offsets[i] + ((long) this.chunkIndex.sizes[i])) {
                    return -2;
                }
                return (int) ((this.chunkIndex.timesUs[i] + ((this.chunkIndex.durationsUs[i] * (region.endOffset - this.chunkIndex.offsets[i])) / ((long) this.chunkIndex.sizes[i]))) / 1000);
            }
        }
    }

    public synchronized void onSpanAdded(Cache cache2, CacheSpan cacheSpan) {
        mergeSpan(cacheSpan);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x006a, code lost:
        return;
     */
    public synchronized void onSpanRemoved(Cache cache2, CacheSpan cacheSpan) {
        Region region = new Region(cacheSpan.position, cacheSpan.position + cacheSpan.length);
        Region region2 = (Region) this.regions.floor(region);
        if (region2 == null) {
            Log.e(TAG, "Removed a span we were not aware of");
            return;
        }
        this.regions.remove(region2);
        if (region2.startOffset < region.startOffset) {
            Region region3 = new Region(region2.startOffset, region.startOffset);
            int binarySearch = Arrays.binarySearch(this.chunkIndex.offsets, region3.endOffset);
            if (binarySearch < 0) {
                binarySearch = (-binarySearch) - 2;
            }
            region3.endOffsetIndex = binarySearch;
            this.regions.add(region3);
        }
        if (region2.endOffset > region.endOffset) {
            Region region4 = new Region(region.endOffset + 1, region2.endOffset);
            region4.endOffsetIndex = region2.endOffsetIndex;
            this.regions.add(region4);
        }
    }

    private void mergeSpan(CacheSpan cacheSpan) {
        Region region = new Region(cacheSpan.position, cacheSpan.position + cacheSpan.length);
        Region region2 = (Region) this.regions.floor(region);
        Region region3 = (Region) this.regions.ceiling(region);
        boolean regionsConnect = regionsConnect(region2, region);
        if (regionsConnect(region, region3)) {
            if (regionsConnect) {
                region2.endOffset = region3.endOffset;
                region2.endOffsetIndex = region3.endOffsetIndex;
            } else {
                region.endOffset = region3.endOffset;
                region.endOffsetIndex = region3.endOffsetIndex;
                this.regions.add(region);
            }
            this.regions.remove(region3);
        } else if (regionsConnect) {
            region2.endOffset = region.endOffset;
            int i = region2.endOffsetIndex;
            while (i < this.chunkIndex.length - 1) {
                int i2 = i + 1;
                if (this.chunkIndex.offsets[i2] > region2.endOffset) {
                    break;
                }
                i = i2;
            }
            region2.endOffsetIndex = i;
        } else {
            int binarySearch = Arrays.binarySearch(this.chunkIndex.offsets, region.endOffset);
            if (binarySearch < 0) {
                binarySearch = (-binarySearch) - 2;
            }
            region.endOffsetIndex = binarySearch;
            this.regions.add(region);
        }
    }

    private boolean regionsConnect(Region region, Region region2) {
        return (region == null || region2 == null || region.endOffset != region2.startOffset) ? false : true;
    }
}
