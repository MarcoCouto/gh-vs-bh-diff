package com.google.android.exoplayer2.text;

import android.support.annotation.NonNull;
import com.google.android.exoplayer2.decoder.DecoderInputBuffer;

public final class SubtitleInputBuffer extends DecoderInputBuffer implements Comparable<SubtitleInputBuffer> {
    public long subsampleOffsetUs;

    public SubtitleInputBuffer() {
        super(1);
    }

    public int compareTo(@NonNull SubtitleInputBuffer subtitleInputBuffer) {
        int i = ((this.timeUs - subtitleInputBuffer.timeUs) > 0 ? 1 : ((this.timeUs - subtitleInputBuffer.timeUs) == 0 ? 0 : -1));
        if (i == 0) {
            return 0;
        }
        return i > 0 ? 1 : -1;
    }
}
