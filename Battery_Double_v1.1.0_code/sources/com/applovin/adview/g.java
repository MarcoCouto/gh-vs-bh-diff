package com.applovin.adview;

import com.applovin.impl.sdk.AppLovinAdInternal;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdDisplayListener;

class g implements AppLovinAdDisplayListener {
    final /* synthetic */ AppLovinInterstitialActivity a;

    g(AppLovinInterstitialActivity appLovinInterstitialActivity) {
        this.a = appLovinInterstitialActivity;
    }

    public void adDisplayed(AppLovinAd appLovinAd) {
        this.a.g = (AppLovinAdInternal) appLovinAd;
        if (!this.a.i) {
            this.a.a(appLovinAd);
        }
    }

    public void adHidden(AppLovinAd appLovinAd) {
        this.a.b(appLovinAd);
    }
}
