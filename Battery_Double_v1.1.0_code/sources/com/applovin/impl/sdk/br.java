package com.applovin.impl.sdk;

import com.applovin.sdk.AppLovinLogger;

class br implements Runnable {
    final /* synthetic */ bn a;
    private final String b;
    private final ba c;
    private final bo d;

    br(bn bnVar, ba baVar, bo boVar) {
        this.a = bnVar;
        this.b = baVar.a();
        this.c = baVar;
        this.d = boVar;
    }

    public void run() {
        long currentTimeMillis = System.currentTimeMillis();
        try {
            m.a();
            if (this.a.a.c()) {
                this.a.b.i(this.b, "Task re-scheduled...");
                this.a.a(this.c, this.d, 2000);
            } else if (this.a.a.isEnabled()) {
                this.a.b.i(this.b, "Task started execution...");
                this.c.run();
                long currentTimeMillis2 = System.currentTimeMillis() - currentTimeMillis;
                if (this.c instanceof cc) {
                    bx.a().a(((cc) this.c).f(), currentTimeMillis2, p.a(this.a.a));
                }
                AppLovinLogger b2 = this.a.b;
                String str = this.b;
                StringBuilder sb = new StringBuilder();
                sb.append("Task executed successfully in ");
                sb.append(currentTimeMillis2);
                sb.append("ms.");
                b2.i(str, sb.toString());
                bg b3 = this.a.a.b();
                StringBuilder sb2 = new StringBuilder();
                sb2.append(this.b);
                sb2.append("_count");
                b3.a(sb2.toString());
                StringBuilder sb3 = new StringBuilder();
                sb3.append(this.b);
                sb3.append("_time");
                b3.a(sb3.toString(), currentTimeMillis2);
            } else {
                if (this.a.a.d()) {
                    this.a.a.e();
                } else {
                    this.a.b.w(this.b, "Task not executed, SDK is disabled");
                }
                this.c.b();
            }
        } catch (Throwable th) {
            AppLovinLogger b4 = this.a.b;
            String str2 = this.b;
            StringBuilder sb4 = new StringBuilder();
            sb4.append("Task failed execution in ");
            sb4.append(System.currentTimeMillis() - currentTimeMillis);
            sb4.append("ms.");
            b4.e(str2, sb4.toString(), th);
        }
    }
}
