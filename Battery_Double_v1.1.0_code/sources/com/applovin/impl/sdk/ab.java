package com.applovin.impl.sdk;

import android.app.Activity;
import android.os.Handler;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdClickListener;
import com.applovin.sdk.AppLovinAdDisplayListener;
import com.applovin.sdk.AppLovinAdRewardListener;
import com.applovin.sdk.AppLovinAdVideoPlaybackListener;
import com.applovin.sdk.AppLovinErrorCodes;
import java.util.Map;

class ab implements AppLovinAdClickListener, AppLovinAdDisplayListener, AppLovinAdRewardListener, AppLovinAdVideoPlaybackListener {
    final /* synthetic */ w a;
    private final Activity b;
    /* access modifiers changed from: private */
    public final AppLovinAdDisplayListener c;
    /* access modifiers changed from: private */
    public final AppLovinAdClickListener d;
    /* access modifiers changed from: private */
    public final AppLovinAdVideoPlaybackListener e;
    /* access modifiers changed from: private */
    public final AppLovinAdRewardListener f;

    private ab(w wVar, Activity activity, AppLovinAdRewardListener appLovinAdRewardListener, AppLovinAdVideoPlaybackListener appLovinAdVideoPlaybackListener, AppLovinAdDisplayListener appLovinAdDisplayListener, AppLovinAdClickListener appLovinAdClickListener) {
        this.a = wVar;
        this.c = appLovinAdDisplayListener;
        this.d = appLovinAdClickListener;
        this.e = appLovinAdVideoPlaybackListener;
        this.f = appLovinAdRewardListener;
        this.b = activity;
    }

    public void adClicked(AppLovinAd appLovinAd) {
        if (this.d != null) {
            this.a.e.post(new ae(this, appLovinAd));
        }
    }

    public void adDisplayed(AppLovinAd appLovinAd) {
        if (this.c != null) {
            this.c.adDisplayed(appLovinAd);
        }
    }

    public void adHidden(AppLovinAd appLovinAd) {
        int i;
        String str;
        String c2 = this.a.c();
        if (c2 == null || !this.a.i) {
            this.a.h.a(true);
            if (this.a.i) {
                str = "network_timeout";
                i = AppLovinErrorCodes.INCENTIVIZED_SERVER_TIMEOUT;
            } else {
                str = "user_closed_video";
                i = AppLovinErrorCodes.INCENTIVIZED_USER_CLOSED_VIDEO;
            }
            av.a().a(appLovinAd, str);
            if (this.a.i) {
                this.a.a(c2, this.b);
            }
            this.a.e.post(new ac(this, appLovinAd, i));
        } else {
            this.a.a(c2, this.b);
        }
        if (this.c != null) {
            this.a.e.post(new ad(this, appLovinAd));
        }
        this.a.a.a().a((ba) new bv(this.a.a, appLovinAd), bo.BACKGROUND);
        this.a.c = null;
    }

    public void userDeclinedToViewAd(AppLovinAd appLovinAd) {
    }

    public void userOverQuota(AppLovinAd appLovinAd, Map map) {
        this.a.b("quota_exceeded");
        if (this.f != null) {
            this.a.e.post(new ai(this, appLovinAd, map));
        }
    }

    public void userRewardRejected(AppLovinAd appLovinAd, Map map) {
        this.a.b("rejected");
        if (this.f != null) {
            this.a.e.post(new aj(this, appLovinAd, map));
        }
    }

    public void userRewardVerified(AppLovinAd appLovinAd, Map map) {
        this.a.b("accepted");
        if (this.f != null) {
            this.a.e.post(new ah(this, appLovinAd, map));
        }
    }

    public void validationRequestFailed(AppLovinAd appLovinAd, int i) {
        this.a.b("network_timeout");
        if (this.f != null) {
            this.a.e.post(new ak(this, appLovinAd, i));
        }
    }

    public void videoPlaybackBegan(AppLovinAd appLovinAd) {
        if (this.e != null) {
            this.a.e.post(new af(this, appLovinAd));
        }
    }

    public void videoPlaybackEnded(AppLovinAd appLovinAd, double d2, boolean z) {
        if (this.e != null) {
            Handler c2 = this.a.e;
            ag agVar = new ag(this, appLovinAd, d2, z);
            c2.post(agVar);
        }
        this.a.i = z;
    }
}
