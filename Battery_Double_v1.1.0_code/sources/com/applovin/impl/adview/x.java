package com.applovin.impl.adview;

import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdClickListener;

class x implements AppLovinAdClickListener {
    final /* synthetic */ ad a;
    final /* synthetic */ v b;

    x(v vVar, ad adVar) {
        this.b = vVar;
        this.a = adVar;
    }

    public void adClicked(AppLovinAd appLovinAd) {
        AppLovinAdClickListener e = this.a.e();
        if (e != null) {
            e.adClicked(appLovinAd);
        }
    }
}
