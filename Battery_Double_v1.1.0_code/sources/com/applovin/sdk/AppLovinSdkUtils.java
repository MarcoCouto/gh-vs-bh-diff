package com.applovin.sdk;

import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.util.Log;

public class AppLovinSdkUtils {
    private static boolean a(Context context) {
        Bundle e = e(context);
        return e != null && e.getBoolean("applovin.sdk.verbose_logging", false);
    }

    private static long b(Context context) {
        Bundle e = e(context);
        if (e != null) {
            return (long) e.getInt("applovin.sdk.ad_refresh_seconds", -100);
        }
        return -100;
    }

    private static String c(Context context) {
        Bundle e = e(context);
        if (e != null) {
            String string = e.getString("applovin.sdk.auto_preload_ad_sizes");
            if (string != null) {
                return string;
            }
        }
        return "BANNER,INTER";
    }

    private static String d(Context context) {
        Bundle e = e(context);
        if (e != null) {
            String string = e.getString("applovin.sdk.auto_preload_ad_types");
            if (string != null) {
                return string;
            }
        }
        StringBuilder sb = new StringBuilder();
        sb.append(AppLovinAdType.REGULAR.getLabel());
        sb.append(",");
        sb.append(AppLovinAdType.INCENTIVIZED.getLabel());
        return sb.toString();
    }

    private static Bundle e(Context context) {
        try {
            return context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData;
        } catch (NameNotFoundException e) {
            Log.e(AppLovinLogger.SDK_TAG, "Unable to retrieve application metadata", e);
            return null;
        }
    }

    public static String retrieveSdkKey(Context context) {
        Bundle e = e(context);
        if (e == null) {
            return null;
        }
        String string = e.getString("applovin.sdk.key");
        return string != null ? string : "";
    }

    public static AppLovinSdkSettings retrieveUserSettings(Context context) {
        AppLovinSdkSettings appLovinSdkSettings = new AppLovinSdkSettings();
        appLovinSdkSettings.setVerboseLogging(a(context));
        appLovinSdkSettings.setBannerAdRefreshSeconds(b(context));
        appLovinSdkSettings.setAutoPreloadSizes(c(context));
        appLovinSdkSettings.setAutoPreloadTypes(d(context));
        return appLovinSdkSettings;
    }
}
