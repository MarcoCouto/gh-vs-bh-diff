package com.mansoon.BatteryDouble.models;

import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.app.ActivityManager.MemoryInfo;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.Context;
import android.os.Debug;
import com.mansoon.BatteryDouble.util.LogUtils;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.List;
import java.util.Locale;

public class Memory {
    public static final int ACTIVE = 2;
    public static final int FREE = 1;
    public static final int INACTIVE = 3;
    private static final String TAG = LogUtils.makeLogTag(Memory.class);
    public static final int TOTAL = 0;

    @TargetApi(21)
    public static int[] readMemory(Context context) {
        ActivityManager activityManager = (ActivityManager) context.getSystemService("activity");
        MemoryInfo memoryInfo = new MemoryInfo();
        activityManager.getMemoryInfo(memoryInfo);
        int i = (int) memoryInfo.availMem;
        List<RunningAppProcessInfo> runningAppProcesses = activityManager.getRunningAppProcesses();
        List<RunningServiceInfo> runningServices = activityManager.getRunningServices(Integer.MAX_VALUE);
        if (runningAppProcesses == null || runningServices == null) {
            return new int[]{i, 0};
        }
        int[] iArr = new int[(runningAppProcesses.size() + runningServices.size())];
        int i2 = 0;
        for (RunningAppProcessInfo runningAppProcessInfo : runningAppProcesses) {
            iArr[i2] = runningAppProcessInfo.pid;
            i2++;
        }
        for (RunningServiceInfo runningServiceInfo : runningServices) {
            iArr[i2] = runningServiceInfo.pid;
            i2++;
        }
        activityManager.getMemoryInfo(memoryInfo);
        LogUtils.LOGI(TAG, String.format(Locale.US, "%d availMem, %b lowMemory, %d threshold, %d total", new Object[]{Long.valueOf(memoryInfo.availMem), Boolean.valueOf(memoryInfo.lowMemory), Long.valueOf(memoryInfo.threshold), Long.valueOf(memoryInfo.totalMem)}));
        for (Debug.MemoryInfo totalPss : activityManager.getProcessMemoryInfo(iArr)) {
            totalPss.getTotalPss();
        }
        return new int[]{i, 0};
    }

    public static synchronized int[] readMemoryInfo() {
        int i;
        int i2;
        int i3;
        int i4;
        synchronized (Memory.class) {
            try {
                RandomAccessFile randomAccessFile = new RandomAccessFile("/proc/meminfo", "r");
                i4 = Integer.parseInt(randomAccessFile.readLine().split("\\s+")[1]);
                try {
                    String readLine = randomAccessFile.readLine();
                    i2 = Integer.parseInt(readLine.split("\\s+")[1]);
                    String str = readLine;
                    int i5 = 0;
                    while (i5 < 4) {
                        try {
                            str = randomAccessFile.readLine();
                            i5++;
                        } catch (IOException e) {
                            e = e;
                            i3 = 0;
                            i = 0;
                            e.printStackTrace();
                            int[] iArr = {i2, i4, i3, i};
                            return iArr;
                        }
                    }
                    i3 = Integer.parseInt(str.split("\\s+")[1]);
                } catch (IOException e2) {
                    e = e2;
                    i3 = 0;
                    i2 = i3;
                    i = i2;
                    e.printStackTrace();
                    int[] iArr2 = {i2, i4, i3, i};
                    return iArr2;
                }
                try {
                    i = Integer.parseInt(randomAccessFile.readLine().split("\\s+")[1]);
                    try {
                        randomAccessFile.close();
                    } catch (IOException e3) {
                        e = e3;
                    }
                } catch (IOException e4) {
                    e = e4;
                    i = 0;
                    e.printStackTrace();
                    int[] iArr22 = {i2, i4, i3, i};
                    return iArr22;
                }
            } catch (IOException e5) {
                e = e5;
                i4 = 0;
                i3 = 0;
                i2 = i3;
                i = i2;
                e.printStackTrace();
                int[] iArr222 = {i2, i4, i3, i};
                return iArr222;
            }
            int[] iArr2222 = {i2, i4, i3, i};
        }
        return iArr2222;
    }
}
