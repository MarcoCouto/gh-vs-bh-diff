package com.mansoon.BatteryDouble.models.data;

import io.realm.ProcessInfoRealmProxyInterface;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.internal.RealmObjectProxy;

public class ProcessInfo extends RealmObject implements ProcessInfoRealmProxyInterface {
    public RealmList<AppPermission> appPermissions;
    public RealmList<AppSignature> appSignatures;
    public String applicationLabel;
    public String importance;
    public String installationPkg;
    public boolean isSystemApp;
    public String name;
    public int processId;
    public int versionCode;
    public String versionName;

    public RealmList realmGet$appPermissions() {
        return this.appPermissions;
    }

    public RealmList realmGet$appSignatures() {
        return this.appSignatures;
    }

    public String realmGet$applicationLabel() {
        return this.applicationLabel;
    }

    public String realmGet$importance() {
        return this.importance;
    }

    public String realmGet$installationPkg() {
        return this.installationPkg;
    }

    public boolean realmGet$isSystemApp() {
        return this.isSystemApp;
    }

    public String realmGet$name() {
        return this.name;
    }

    public int realmGet$processId() {
        return this.processId;
    }

    public int realmGet$versionCode() {
        return this.versionCode;
    }

    public String realmGet$versionName() {
        return this.versionName;
    }

    public void realmSet$appPermissions(RealmList realmList) {
        this.appPermissions = realmList;
    }

    public void realmSet$appSignatures(RealmList realmList) {
        this.appSignatures = realmList;
    }

    public void realmSet$applicationLabel(String str) {
        this.applicationLabel = str;
    }

    public void realmSet$importance(String str) {
        this.importance = str;
    }

    public void realmSet$installationPkg(String str) {
        this.installationPkg = str;
    }

    public void realmSet$isSystemApp(boolean z) {
        this.isSystemApp = z;
    }

    public void realmSet$name(String str) {
        this.name = str;
    }

    public void realmSet$processId(int i) {
        this.processId = i;
    }

    public void realmSet$versionCode(int i) {
        this.versionCode = i;
    }

    public void realmSet$versionName(String str) {
        this.versionName = str;
    }

    public ProcessInfo() {
        if (this instanceof RealmObjectProxy) {
            ((RealmObjectProxy) this).realm$injectObjectContext();
        }
    }
}
