package com.mansoon.BatteryDouble.models.data;

import io.realm.CpuStatusRealmProxyInterface;
import io.realm.RealmObject;
import io.realm.internal.RealmObjectProxy;

public class CpuStatus extends RealmObject implements CpuStatusRealmProxyInterface {
    public double cpuUsage;
    public long sleepTime;
    public long upTime;

    public double realmGet$cpuUsage() {
        return this.cpuUsage;
    }

    public long realmGet$sleepTime() {
        return this.sleepTime;
    }

    public long realmGet$upTime() {
        return this.upTime;
    }

    public void realmSet$cpuUsage(double d) {
        this.cpuUsage = d;
    }

    public void realmSet$sleepTime(long j) {
        this.sleepTime = j;
    }

    public void realmSet$upTime(long j) {
        this.upTime = j;
    }

    public CpuStatus() {
        if (this instanceof RealmObjectProxy) {
            ((RealmObjectProxy) this).realm$injectObjectContext();
        }
    }
}
