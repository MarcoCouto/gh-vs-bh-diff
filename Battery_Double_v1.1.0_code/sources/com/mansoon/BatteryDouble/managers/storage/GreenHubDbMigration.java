package com.mansoon.BatteryDouble.managers.storage;

import android.support.annotation.NonNull;
import com.facebook.internal.ServerProtocol;
import com.mansoon.BatteryDouble.util.LogUtils;
import io.realm.DynamicRealm;
import io.realm.FieldAttribute;
import io.realm.RealmMigration;
import io.realm.RealmSchema;

public class GreenHubDbMigration implements RealmMigration {
    private static final String TAG = LogUtils.makeLogTag(GreenHubDbMigration.class);

    public void migrate(@NonNull DynamicRealm dynamicRealm, long j, long j2) {
        RealmSchema schema = dynamicRealm.getSchema();
        if (schema != null) {
            if (j == 1) {
                try {
                    schema.get("Sample").addField(ServerProtocol.FALLBACK_DIALOG_PARAM_VERSION, Integer.TYPE, new FieldAttribute[0]);
                    j++;
                } catch (NullPointerException e) {
                    LogUtils.LOGE(TAG, "Schema is null!");
                    e.printStackTrace();
                }
            }
            if (j == 2) {
                schema.get("Device").removeField("serialNumber");
                schema.get("Sample").addField("database", Integer.TYPE, new FieldAttribute[0]);
            }
        }
    }
}
