package com.mansoon.BatteryDouble.managers.sampling;

import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;

public class SignalListener extends PhoneStateListener {
    private int cdmaDbm = 0;
    private int evdoDbm = 0;
    private int gsmSignal = 0;

    public void onSignalStrengthsChanged(SignalStrength signalStrength) {
        super.onSignalStrengthsChanged(signalStrength);
        this.gsmSignal = signalStrength.getGsmSignalStrength();
        this.cdmaDbm = signalStrength.getCdmaDbm();
        this.evdoDbm = signalStrength.getEvdoDbm();
    }

    public int getGsmSignal() {
        return this.gsmSignal;
    }

    public int getEvdoDbm() {
        return this.evdoDbm;
    }

    public int getCdmaDbm() {
        return this.cdmaDbm;
    }
}
