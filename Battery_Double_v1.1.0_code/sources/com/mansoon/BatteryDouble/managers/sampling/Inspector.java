package com.mansoon.BatteryDouble.managers.sampling;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PermissionInfo;
import android.preference.PreferenceManager;
import android.util.Log;
import com.facebook.internal.AnalyticsEvents;
import com.mansoon.BatteryDouble.Config;
import com.mansoon.BatteryDouble.R;
import com.mansoon.BatteryDouble.events.StatusEvent;
import com.mansoon.BatteryDouble.models.Application;
import com.mansoon.BatteryDouble.models.Battery;
import com.mansoon.BatteryDouble.models.Bluetooth;
import com.mansoon.BatteryDouble.models.Cpu;
import com.mansoon.BatteryDouble.models.Gps;
import com.mansoon.BatteryDouble.models.LocationInfo;
import com.mansoon.BatteryDouble.models.Memory;
import com.mansoon.BatteryDouble.models.Network;
import com.mansoon.BatteryDouble.models.Package;
import com.mansoon.BatteryDouble.models.Phone;
import com.mansoon.BatteryDouble.models.Process;
import com.mansoon.BatteryDouble.models.Screen;
import com.mansoon.BatteryDouble.models.SettingsInfo;
import com.mansoon.BatteryDouble.models.SimCard;
import com.mansoon.BatteryDouble.models.Specifications;
import com.mansoon.BatteryDouble.models.Storage;
import com.mansoon.BatteryDouble.models.Wifi;
import com.mansoon.BatteryDouble.models.data.AppPermission;
import com.mansoon.BatteryDouble.models.data.BatteryDetails;
import com.mansoon.BatteryDouble.models.data.BatterySession;
import com.mansoon.BatteryDouble.models.data.BatteryUsage;
import com.mansoon.BatteryDouble.models.data.CpuStatus;
import com.mansoon.BatteryDouble.models.data.Feature;
import com.mansoon.BatteryDouble.models.data.NetworkDetails;
import com.mansoon.BatteryDouble.models.data.ProcessInfo;
import com.mansoon.BatteryDouble.models.data.Sample;
import com.mansoon.BatteryDouble.models.data.Settings;
import com.mansoon.BatteryDouble.util.LogUtils;
import com.mansoon.BatteryDouble.util.SettingsUtils;
import io.realm.RealmList;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.greenrobot.eventbus.EventBus;

public final class Inspector {
    private static final String DISABLED = "disabled:";
    private static final String INSTALLED = "installed:";
    private static final String REPLACED = "replaced:";
    private static final String TAG = LogUtils.makeLogTag(Inspector.class);
    private static final String UNINSTALLED = "uninstalled:";
    private static double sCurrentBatteryLevel;
    private static double sLastBatteryLevel;

    private Inspector() {
    }

    public static void setLastBatteryLevel(double d) {
        sLastBatteryLevel = d;
    }

    public static double getLastBatteryLevel() {
        return sLastBatteryLevel;
    }

    public static double getCurrentBatteryLevel() {
        return sCurrentBatteryLevel;
    }

    public static void setCurrentBatteryLevel(double d, double d2) {
        double d3 = d / d2;
        if (d3 != sCurrentBatteryLevel) {
            sCurrentBatteryLevel = d3;
        }
    }

    public static boolean killApp(Context context, String str) {
        ActivityManager activityManager = (ActivityManager) context.getSystemService("activity");
        if (activityManager != null) {
            try {
                activityManager.killBackgroundProcesses(str);
                return true;
            } catch (Throwable th) {
                String str2 = TAG;
                StringBuilder sb = new StringBuilder();
                sb.append("Could not kill process: ");
                sb.append(str);
                Log.e(str2, sb.toString(), th);
            }
        }
        return false;
    }

    /* JADX WARNING: Removed duplicated region for block: B:38:0x00f1  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x00f3 A[SYNTHETIC] */
    private static List<ProcessInfo> getRunningProcessInfoForSample(Context context) {
        String str;
        Context context2 = context;
        Process.clear();
        ArrayList<ProcessInfo> runningAppInfo = Application.getRunningAppInfo(context);
        ArrayList arrayList = new ArrayList();
        PackageManager packageManager = context.getPackageManager();
        int[] iArr = new int[runningAppInfo.size()];
        HashSet hashSet = new HashSet();
        Map installedPackages = SettingsUtils.isInstalledPackagesIncluded(context) ? Package.getInstalledPackages(context2, false) : null;
        for (ProcessInfo processInfo : runningAppInfo) {
            String realmGet$name = processInfo.realmGet$name();
            if (installedPackages != null && installedPackages.containsKey(realmGet$name)) {
                installedPackages.remove(realmGet$name);
            }
            hashSet.add(realmGet$name);
            ProcessInfo processInfo2 = new ProcessInfo();
            processInfo2.realmSet$appPermissions(new RealmList());
            processInfo2.realmSet$appSignatures(new RealmList());
            PackageInfo packageInfo = Package.getPackageInfo(context2, realmGet$name);
            if (packageInfo != null) {
                processInfo2.realmSet$versionName(packageInfo.versionName);
                processInfo2.realmSet$versionCode(packageInfo.versionCode);
                String charSequence = packageManager.getApplicationLabel(packageInfo.applicationInfo).toString();
                if (charSequence.length() > 0) {
                    processInfo2.realmSet$applicationLabel(charSequence);
                }
                int i = packageInfo.applicationInfo.flags;
                boolean z = true;
                if (!((i & 1) > 0) && (i & 128) <= 0) {
                    z = false;
                }
                processInfo2.realmSet$isSystemApp(z);
                if (packageInfo.permissions != null) {
                    for (PermissionInfo permissionInfo : packageInfo.permissions) {
                        processInfo2.realmGet$appPermissions().add(new AppPermission(permissionInfo.name));
                    }
                }
            }
            processInfo2.realmSet$importance(processInfo.realmGet$importance());
            processInfo2.realmSet$processId(processInfo.realmGet$processId());
            processInfo2.realmSet$name(processInfo.realmGet$name());
            if (!processInfo.realmGet$isSystemApp()) {
                try {
                    str = packageManager.getInstallerPackageName(realmGet$name);
                } catch (IllegalArgumentException unused) {
                    String str2 = TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append("Could not get installer for ");
                    sb.append(realmGet$name);
                    Log.e(str2, sb.toString());
                }
                if (str != null) {
                    str = "null";
                }
                processInfo2.realmSet$installationPkg(str);
                arrayList.add(processInfo2);
            }
            str = null;
            if (str != null) {
            }
            processInfo2.realmSet$installationPkg(str);
            arrayList.add(processInfo2);
        }
        if (installedPackages != null && installedPackages.size() > 0) {
            arrayList.addAll(installedPackages.values());
            SettingsUtils.markInstalledPackagesIncluded(context2, false);
        }
        updatePackagePreferences(context2, arrayList);
        return arrayList;
    }

    private static void updatePackagePreferences(Context context, List<ProcessInfo> list) {
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        Set<String> keySet = defaultSharedPreferences.getAll().keySet();
        Editor edit = defaultSharedPreferences.edit();
        boolean z = false;
        for (String str : keySet) {
            if (str.startsWith(INSTALLED)) {
                String substring = str.substring(INSTALLED.length());
                if (defaultSharedPreferences.getBoolean(str, false)) {
                    String str2 = TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append("Installed:");
                    sb.append(substring);
                    Log.i(str2, sb.toString());
                    ProcessInfo installedPackage = Package.getInstalledPackage(context, substring);
                    if (installedPackage != null) {
                        installedPackage.realmSet$importance(Config.IMPORTANCE_INSTALLED);
                        list.add(installedPackage);
                        edit.remove(str);
                    }
                }
            } else if (str.startsWith(REPLACED)) {
                String substring2 = str.substring(REPLACED.length());
                if (defaultSharedPreferences.getBoolean(str, false)) {
                    String str3 = TAG;
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("Replaced:");
                    sb2.append(substring2);
                    Log.i(str3, sb2.toString());
                    ProcessInfo installedPackage2 = Package.getInstalledPackage(context, substring2);
                    if (installedPackage2 != null) {
                        installedPackage2.realmSet$importance(Config.IMPORTANCE_REPLACED);
                        list.add(installedPackage2);
                        edit.remove(str);
                    }
                }
            } else if (str.startsWith(UNINSTALLED)) {
                String substring3 = str.substring(UNINSTALLED.length());
                if (defaultSharedPreferences.getBoolean(str, false)) {
                    String str4 = TAG;
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append("Uninstalled:");
                    sb3.append(substring3);
                    Log.i(str4, sb3.toString());
                    list.add(Process.uninstalledItem(substring3, str, edit));
                }
            } else if (str.startsWith(DISABLED)) {
                String substring4 = str.substring(DISABLED.length());
                if (defaultSharedPreferences.getBoolean(str, false)) {
                    String str5 = TAG;
                    StringBuilder sb4 = new StringBuilder();
                    sb4.append("Disabled app:");
                    sb4.append(substring4);
                    Log.i(str5, sb4.toString());
                    list.add(Process.disabledItem(substring4, str, edit));
                }
            }
            z = true;
        }
        if (z) {
            edit.apply();
        }
    }

    private static List<Feature> getExtras() {
        LinkedList linkedList = new LinkedList();
        linkedList.add(Specifications.getVmVersion());
        return linkedList;
    }

    static Sample getSample(Context context, Intent intent, String str) {
        Sample sample = new Sample();
        NetworkDetails networkDetails = new NetworkDetails();
        BatteryDetails batteryDetails = new BatteryDetails();
        CpuStatus cpuStatus = new CpuStatus();
        Settings settings = new Settings();
        sample.realmSet$processInfos(new RealmList());
        sample.realmSet$locationProviders(new RealmList());
        sample.realmSet$features(new RealmList());
        LogUtils.LOGI(TAG, "getSample() was invoked.");
        String action = intent.getAction();
        String str2 = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("action = ");
        sb.append(action);
        LogUtils.LOGI(str2, sb.toString());
        sample.realmSet$uuId(Specifications.getAndroidId(context));
        sample.realmSet$triggeredBy(action);
        sample.realmSet$timestamp(System.currentTimeMillis());
        sample.realmSet$id(String.valueOf(sample.realmGet$timestamp()).hashCode());
        sample.realmSet$version(17);
        sample.realmSet$database(3);
        long[] readUsagePoint = Cpu.readUsagePoint();
        EventBus.getDefault().post(new StatusEvent(context.getString(R.string.event_get_processes)));
        sample.realmGet$processInfos().addAll(getRunningProcessInfoForSample(context));
        sample.realmSet$screenBrightness(Screen.getBrightness(context));
        if (Screen.isAutoBrightness(context)) {
            sample.realmSet$screenBrightness(-1);
        }
        sample.realmGet$locationProviders().addAll(LocationInfo.getEnabledLocationProviders(context));
        String status = Network.getStatus(context);
        String type = Network.getType(context);
        String mobileNetworkType = Network.getMobileNetworkType(context);
        if (!status.equals(Network.NETWORKSTATUS_CONNECTED)) {
            sample.realmSet$networkStatus(status);
        } else if (type.equals("WIFI")) {
            sample.realmSet$networkStatus(type);
        } else {
            sample.realmSet$networkStatus(mobileNetworkType);
        }
        networkDetails.realmSet$networkType(type);
        networkDetails.realmSet$mobileNetworkType(mobileNetworkType);
        networkDetails.realmSet$roamingEnabled(Network.getRoamingStatus(context));
        networkDetails.realmSet$mobileDataStatus(Network.getDataState(context));
        networkDetails.realmSet$mobileDataActivity(Network.getDataActivity(context));
        networkDetails.realmSet$simOperator(SimCard.getSIMOperator(context));
        networkDetails.realmSet$networkOperator(Phone.getNetworkOperator(context));
        networkDetails.realmSet$mcc(Phone.getMcc(context));
        networkDetails.realmSet$mnc(Phone.getMnc(context));
        networkDetails.realmSet$wifiStatus(Wifi.getState(context));
        networkDetails.realmSet$wifiSignalStrength(Wifi.getSignalStrength(context));
        networkDetails.realmSet$wifiLinkSpeed(Wifi.getLinkSpeed(context));
        networkDetails.realmSet$wifiApStatus(Wifi.getHotspotState(context));
        sample.realmSet$networkDetails(networkDetails);
        sample.realmSet$callInfo(null);
        int intExtra = intent.getIntExtra("health", 0);
        int intExtra2 = intent.getIntExtra("status", 0);
        int intExtra3 = intent.getIntExtra("plugged", 0);
        String string = intent.getExtras().getString("technology");
        String str3 = AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN;
        switch (intExtra) {
            case 1:
                str3 = AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN;
                break;
            case 2:
                str3 = "Good";
                break;
            case 3:
                str3 = "Overheat";
                break;
            case 4:
                str3 = "Dead";
                break;
            case 5:
                str3 = "Over voltage";
                break;
            case 6:
                str3 = "Unspecified failure";
                break;
        }
        switch (intExtra2) {
            case 1:
                str = AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN;
                break;
            case 2:
                str = "Charging";
                break;
            case 3:
                str = "Discharging";
                break;
            case 4:
                str = "Not charging";
                break;
            case 5:
                str = "Full";
                break;
            default:
                if (str == null) {
                    str = AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN;
                    break;
                }
                break;
        }
        String str4 = "unplugged";
        switch (intExtra3) {
            case 1:
                str4 = "ac";
                break;
            case 2:
                str4 = "usb";
                break;
        }
        batteryDetails.realmSet$temperature((double) (((float) intent.getIntExtra("temperature", 0)) / 10.0f));
        batteryDetails.realmSet$voltage((double) (((float) intent.getIntExtra("voltage", 0)) / 1000.0f));
        batteryDetails.realmSet$charger(str4);
        batteryDetails.realmSet$health(str3);
        batteryDetails.realmSet$technology(string);
        batteryDetails.realmSet$capacity(Battery.getActualBatteryCapacity(context));
        batteryDetails.realmSet$chargeCounter(Battery.getBatteryChargeCounter(context));
        batteryDetails.realmSet$currentAverage(Battery.getBatteryCurrentAverage(context));
        batteryDetails.realmSet$currentNow(Battery.getBatteryCurrentNow(context));
        batteryDetails.realmSet$energyCounter(Battery.getBatteryEnergyCounter(context));
        sample.realmSet$batteryDetails(batteryDetails);
        sample.realmSet$batteryLevel(sCurrentBatteryLevel);
        sample.realmSet$batteryState(str);
        int[] readMemoryInfo = Memory.readMemoryInfo();
        if (readMemoryInfo != null && readMemoryInfo.length == 4) {
            sample.realmSet$memoryUser(readMemoryInfo[0]);
            sample.realmSet$memoryFree(readMemoryInfo[1]);
            sample.realmSet$memoryActive(readMemoryInfo[2]);
            sample.realmSet$memoryInactive(readMemoryInfo[3]);
        }
        long[] readUsagePoint2 = Cpu.readUsagePoint();
        long uptime = Cpu.getUptime();
        long sleepTime = Cpu.getSleepTime();
        cpuStatus.realmSet$cpuUsage(Cpu.getUsage(readUsagePoint, readUsagePoint2));
        cpuStatus.realmSet$upTime(uptime);
        cpuStatus.realmSet$sleepTime(sleepTime);
        sample.realmSet$cpuStatus(cpuStatus);
        sample.realmSet$storageDetails(Storage.getStorageDetails());
        settings.realmSet$bluetoothEnabled(Bluetooth.isEnabled());
        settings.realmSet$locationEnabled(Gps.isEnabled(context));
        settings.realmSet$powersaverEnabled(SettingsInfo.isPowerSaveEnabled(context));
        settings.realmSet$flashlightEnabled(false);
        settings.realmSet$nfcEnabled(SettingsInfo.isNfcEnabled(context));
        settings.realmSet$developerMode(SettingsInfo.isDeveloperModeOn(context));
        settings.realmSet$unknownSources(SettingsInfo.allowUnknownSources(context));
        sample.realmSet$settings(settings);
        sample.realmSet$screenOn(Screen.isOn(context));
        sample.realmSet$timeZone(SettingsInfo.getTimeZone());
        sample.realmSet$countryCode(LocationInfo.getCountryCode(context));
        List extras = getExtras();
        if (extras != null && extras.size() > 0) {
            sample.realmGet$features().addAll(extras);
        }
        return sample;
    }

    static BatteryUsage getBatteryUsage(Context context, Intent intent) {
        String str;
        BatteryUsage batteryUsage = new BatteryUsage();
        BatteryDetails batteryDetails = new BatteryDetails();
        int intExtra = intent.getIntExtra("health", 0);
        int intExtra2 = intent.getIntExtra("status", 0);
        int intExtra3 = intent.getIntExtra("plugged", 0);
        String string = intent.getExtras().getString("technology");
        String str2 = AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN;
        String str3 = "unplugged";
        batteryUsage.realmSet$timestamp(System.currentTimeMillis());
        batteryUsage.realmSet$id(String.valueOf(batteryUsage.realmGet$timestamp()).hashCode());
        switch (intExtra) {
            case 1:
                str2 = AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN;
                break;
            case 2:
                str2 = "Good";
                break;
            case 3:
                str2 = "Overheat";
                break;
            case 4:
                str2 = "Dead";
                break;
            case 5:
                str2 = "Over voltage";
                break;
            case 6:
                str2 = "Unspecified failure";
                break;
        }
        switch (intExtra2) {
            case 1:
                str = AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN;
                break;
            case 2:
                str = "Charging";
                break;
            case 3:
                str = "Discharging";
                break;
            case 4:
                str = "Not charging";
                break;
            case 5:
                str = "Full";
                break;
            default:
                str = AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN;
                break;
        }
        switch (intExtra3) {
            case 1:
                str3 = "ac";
                break;
            case 2:
                str3 = "usb";
                break;
        }
        batteryDetails.realmSet$temperature((double) (((float) intent.getIntExtra("temperature", 0)) / 10.0f));
        batteryDetails.realmSet$voltage((double) (((float) intent.getIntExtra("voltage", 0)) / 1000.0f));
        batteryDetails.realmSet$charger(str3);
        batteryDetails.realmSet$health(str2);
        batteryDetails.realmSet$technology(string);
        batteryDetails.realmSet$capacity(Battery.getActualBatteryCapacity(context));
        batteryDetails.realmSet$chargeCounter(Battery.getBatteryChargeCounter(context));
        batteryDetails.realmSet$currentAverage(Battery.getBatteryCurrentAverage(context));
        batteryDetails.realmSet$currentNow(Battery.getBatteryCurrentNow(context));
        batteryDetails.realmSet$energyCounter(Battery.getBatteryEnergyCounter(context));
        batteryUsage.realmSet$level((float) sCurrentBatteryLevel);
        batteryUsage.realmSet$state(str);
        batteryUsage.realmSet$screenOn(Screen.isOn(context));
        batteryUsage.realmSet$triggeredBy(intent.getAction());
        batteryUsage.realmSet$details(batteryDetails);
        return batteryUsage;
    }

    public static BatterySession getBatterySession(Context context, Intent intent) {
        BatterySession batterySession = new BatterySession();
        batterySession.realmSet$timestamp(System.currentTimeMillis());
        batterySession.realmSet$id(String.valueOf(batterySession.realmGet$timestamp()).hashCode());
        batterySession.realmSet$level((float) sCurrentBatteryLevel);
        batterySession.realmSet$screenOn(Screen.isOn(context));
        batterySession.realmSet$triggeredBy(intent.getAction());
        return batterySession;
    }
}
