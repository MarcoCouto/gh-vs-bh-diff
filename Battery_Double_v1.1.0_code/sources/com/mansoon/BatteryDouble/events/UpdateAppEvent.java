package com.mansoon.BatteryDouble.events;

public class UpdateAppEvent {
    public final int version;

    public UpdateAppEvent(int i) {
        this.version = i;
    }
}
