package com.mansoon.BatteryDouble.ui;

import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.app.ActivityManager.MemoryInfo;
import android.app.AppOpsManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Process;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ItemDecoration;
import android.support.v7.widget.RecyclerView.State;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.support.v7.widget.helper.ItemTouchHelper.SimpleCallback;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.github.mikephil.charting.utils.Utils;
import com.mansoon.BatteryDouble.R;
import com.mansoon.BatteryDouble.events.OpenTaskDetailsEvent;
import com.mansoon.BatteryDouble.events.TaskRemovedEvent;
import com.mansoon.BatteryDouble.managers.TaskController;
import com.mansoon.BatteryDouble.models.ui.Task;
import com.mansoon.BatteryDouble.ui.adapters.TaskAdapter;
import com.mansoon.BatteryDouble.util.SettingsUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class TaskListActivity extends BaseActivity {
    private TaskAdapter mAdapter;
    /* access modifiers changed from: private */
    public boolean mIsUpdating;
    private Task mLastKilledApp;
    private long mLastKilledTimestamp;
    /* access modifiers changed from: private */
    public ProgressBar mLoader;
    /* access modifiers changed from: private */
    public RecyclerView mRecyclerView;
    private int mSortOrderMemory;
    private int mSortOrderName;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    /* access modifiers changed from: private */
    public ArrayList<Task> mTaskList;

    private class LoadRunningProcessesTask extends AsyncTask<Context, Void, List<Task>> {
        private LoadRunningProcessesTask() {
        }

        /* access modifiers changed from: protected */
        public List<Task> doInBackground(Context... contextArr) {
            return new TaskController(contextArr[0]).getRunningTasks();
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(List<Task> list) {
            super.onPostExecute(list);
            TaskListActivity.this.onRefreshComplete(list);
            TaskListActivity.this.checkIfLastAppIsKilled();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (!SettingsUtils.isTosAccepted(getApplicationContext())) {
            startActivity(new Intent(this, WelcomeActivity.class));
            finish();
            return;
        }
        setContentView(R.layout.activity_task_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
        }
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setDisplayHomeAsUpEnabled(true);
        }
        loadComponents();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_task_list, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        int itemId = menuItem.getItemId();
        if (itemId == R.id.action_settings) {
            startActivity(new Intent(this, SettingsActivity.class));
            return true;
        } else if (itemId == R.id.action_sort_memory) {
            sortTasksBy(1, this.mSortOrderMemory);
            this.mSortOrderMemory = -this.mSortOrderMemory;
            return true;
        } else if (itemId != R.id.action_sort_name) {
            return super.onOptionsItemSelected(menuItem);
        } else {
            sortTasksBy(2, this.mSortOrderName);
            this.mSortOrderName = -this.mSortOrderName;
            return true;
        }
    }

    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    public void onResume() {
        super.onResume();
        if (!this.mIsUpdating) {
            initiateRefresh();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onTaskRemovedEvent(TaskRemovedEvent taskRemovedEvent) {
        updateHeaderInfo();
        this.mLastKilledApp = taskRemovedEvent.task;
        this.mLastKilledTimestamp = System.currentTimeMillis();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onOpenTaskDetailsEvent(OpenTaskDetailsEvent openTaskDetailsEvent) {
        StringBuilder sb = new StringBuilder();
        sb.append("package:");
        sb.append(openTaskDetailsEvent.task.getPackageInfo().packageName);
        startActivity(new Intent("android.settings.APPLICATION_DETAILS_SETTINGS", Uri.parse(sb.toString())));
    }

    private void loadComponents() {
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar_actionbar));
        this.mLoader = (ProgressBar) findViewById(R.id.loader);
        this.mLastKilledApp = null;
        this.mSortOrderName = 1;
        this.mSortOrderMemory = 1;
        ((FloatingActionButton) findViewById(R.id.fab)).setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                String str;
                if (TaskListActivity.this.mTaskList.isEmpty()) {
                    Snackbar.make(view, (CharSequence) TaskListActivity.this.getString(R.string.task_no_apps_running), 0).show();
                    return;
                }
                double d = Utils.DOUBLE_EPSILON;
                TaskController taskController = new TaskController(TaskListActivity.this.getApplicationContext());
                Iterator it = TaskListActivity.this.mTaskList.iterator();
                int i = 0;
                while (it.hasNext()) {
                    Task task = (Task) it.next();
                    if (task.isChecked()) {
                        taskController.killApp(task);
                        d += task.getMemory();
                        i++;
                    }
                }
                double round = ((double) Math.round(d * 100.0d)) / 100.0d;
                TaskListActivity.this.mRecyclerView.setVisibility(8);
                TaskListActivity.this.mLoader.setVisibility(0);
                TaskListActivity.this.initiateRefresh();
                if (VERSION.SDK_INT >= 24) {
                    if (i > 0) {
                        str = TaskListActivity.this.makeMessage(i);
                    } else {
                        str = TaskListActivity.this.getString(R.string.task_no_apps_killed);
                    }
                } else if (i > 0) {
                    str = TaskListActivity.this.makeMessage(i, round);
                } else {
                    str = TaskListActivity.this.getString(R.string.task_no_apps_killed);
                }
                Snackbar.make(view, (CharSequence) str, 0).show();
            }
        });
        if (VERSION.SDK_INT >= 24 && !hasSpecialPermission(getApplicationContext())) {
            showPermissionInfoDialog();
        }
        this.mTaskList = new ArrayList<>();
        this.mIsUpdating = false;
        setupRefreshLayout();
        setupRecyclerView();
    }

    private void sortTasksBy(int i, final int i2) {
        if (i == 1) {
            Collections.sort(this.mTaskList, new Comparator<Task>() {
                public int compare(Task task, Task task2) {
                    int i = task.getMemory() < task2.getMemory() ? -1 : task.getMemory() == task2.getMemory() ? 0 : 1;
                    return i2 * i;
                }
            });
        } else if (i == 2) {
            Collections.sort(this.mTaskList, new Comparator<Task>() {
                public int compare(Task task, Task task2) {
                    return i2 * task.getLabel().compareTo(task2.getLabel());
                }
            });
        }
        this.mAdapter.notifyDataSetChanged();
    }

    /* access modifiers changed from: private */
    public String makeMessage(int i) {
        StringBuilder sb = new StringBuilder();
        sb.append(getString(R.string.task_killed));
        sb.append(" ");
        sb.append(i);
        sb.append(" apps!");
        return sb.toString();
    }

    /* access modifiers changed from: private */
    public String makeMessage(int i, double d) {
        StringBuilder sb = new StringBuilder();
        sb.append(getString(R.string.task_killed));
        sb.append(" ");
        sb.append(i);
        sb.append(" apps! ");
        sb.append(getString(R.string.task_cleared));
        sb.append(" ");
        sb.append(d);
        sb.append(" MB");
        return sb.toString();
    }

    private void setupRecyclerView() {
        this.mRecyclerView = (RecyclerView) findViewById(R.id.rv);
        this.mRecyclerView.setHasFixedSize(true);
        this.mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        this.mAdapter = new TaskAdapter(getApplicationContext(), this.mTaskList);
        this.mRecyclerView.setAdapter(this.mAdapter);
        setUpItemTouchHelper();
        setUpAnimationDecoratorHelper();
    }

    private void setupRefreshLayout() {
        this.mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_layout);
        if (VERSION.SDK_INT >= 23) {
            this.mSwipeRefreshLayout.setColorSchemeColors(getColor(R.color.color_accent), getColor(R.color.color_primary_dark));
        } else {
            Context applicationContext = getApplicationContext();
            this.mSwipeRefreshLayout.setColorSchemeColors(ContextCompat.getColor(applicationContext, R.color.color_accent), ContextCompat.getColor(applicationContext, R.color.color_primary_dark));
        }
        this.mSwipeRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
            public void onRefresh() {
                if (!TaskListActivity.this.mIsUpdating) {
                    TaskListActivity.this.initiateRefresh();
                }
            }
        });
    }

    private void setUpItemTouchHelper() {
        new ItemTouchHelper(new SimpleCallback(0, 4) {
            Drawable background;
            boolean initiated;
            Drawable xMark;
            int xMarkMargin;

            public boolean onMove(RecyclerView recyclerView, ViewHolder viewHolder, ViewHolder viewHolder2) {
                return false;
            }

            private void init() {
                this.background = new ColorDrawable(-12303292);
                this.xMark = ContextCompat.getDrawable(TaskListActivity.this, R.drawable.ic_delete_white_24dp);
                this.xMark.setColorFilter(-1, Mode.SRC_ATOP);
                this.xMarkMargin = (int) TaskListActivity.this.getResources().getDimension(R.dimen.fab_margin);
                this.initiated = true;
            }

            public int getSwipeDirs(RecyclerView recyclerView, ViewHolder viewHolder) {
                int adapterPosition = viewHolder.getAdapterPosition();
                TaskAdapter taskAdapter = (TaskAdapter) recyclerView.getAdapter();
                if (!taskAdapter.isUndoOn() || !taskAdapter.isPendingRemoval(adapterPosition)) {
                    return super.getSwipeDirs(recyclerView, viewHolder);
                }
                return 0;
            }

            public void onSwiped(ViewHolder viewHolder, int i) {
                int adapterPosition = viewHolder.getAdapterPosition();
                TaskAdapter taskAdapter = (TaskAdapter) TaskListActivity.this.mRecyclerView.getAdapter();
                if (taskAdapter.isUndoOn()) {
                    taskAdapter.pendingRemoval(adapterPosition);
                } else {
                    taskAdapter.remove(adapterPosition);
                }
            }

            public void onChildDraw(Canvas canvas, RecyclerView recyclerView, ViewHolder viewHolder, float f, float f2, int i, boolean z) {
                View view = viewHolder.itemView;
                if (viewHolder.getAdapterPosition() != -1) {
                    if (!this.initiated) {
                        init();
                    }
                    this.background.setBounds(view.getRight() + ((int) f), view.getTop(), view.getRight(), view.getBottom());
                    this.background.draw(canvas);
                    int bottom = view.getBottom() - view.getTop();
                    int intrinsicWidth = this.xMark.getIntrinsicWidth();
                    int intrinsicWidth2 = this.xMark.getIntrinsicWidth();
                    int right = (view.getRight() - this.xMarkMargin) - intrinsicWidth;
                    int right2 = view.getRight() - this.xMarkMargin;
                    int top = view.getTop() + ((bottom - intrinsicWidth2) / 2);
                    this.xMark.setBounds(right, top, right2, intrinsicWidth2 + top);
                    this.xMark.draw(canvas);
                    super.onChildDraw(canvas, recyclerView, viewHolder, f, f2, i, z);
                }
            }
        }).attachToRecyclerView(this.mRecyclerView);
    }

    private void setUpAnimationDecoratorHelper() {
        this.mRecyclerView.addItemDecoration(new ItemDecoration() {
            Drawable background;
            boolean initiated;

            private void init() {
                this.background = new ColorDrawable(-12303292);
                this.initiated = true;
            }

            public void onDraw(Canvas canvas, RecyclerView recyclerView, State state) {
                int i;
                int i2;
                if (!this.initiated) {
                    init();
                }
                if (recyclerView.getItemAnimator().isRunning()) {
                    int width = recyclerView.getWidth();
                    int childCount = recyclerView.getLayoutManager().getChildCount();
                    View view = null;
                    View view2 = null;
                    for (int i3 = 0; i3 < childCount; i3++) {
                        View childAt = recyclerView.getLayoutManager().getChildAt(i3);
                        if (childAt.getTranslationY() < 0.0f) {
                            view = childAt;
                        } else if (childAt.getTranslationY() > 0.0f && view2 == null) {
                            view2 = childAt;
                        }
                    }
                    if (view != null && view2 != null) {
                        i2 = view.getBottom() + ((int) view.getTranslationY());
                        i = view2.getTop() + ((int) view2.getTranslationY());
                    } else if (view != null) {
                        i2 = view.getBottom() + ((int) view.getTranslationY());
                        i = view.getBottom();
                    } else if (view2 != null) {
                        i2 = view2.getTop();
                        i = view2.getTop() + ((int) view2.getTranslationY());
                    } else {
                        i2 = 0;
                        i = 0;
                    }
                    this.background.setBounds(0, i2, width, i);
                    this.background.draw(canvas);
                }
                super.onDraw(canvas, recyclerView, state);
            }
        });
    }

    /* access modifiers changed from: private */
    public void initiateRefresh() {
        this.mIsUpdating = true;
        setHeaderToRefresh();
        new LoadRunningProcessesTask().execute(new Context[]{getApplicationContext()});
    }

    /* access modifiers changed from: private */
    public void onRefreshComplete(List<Task> list) {
        if (this.mLoader.getVisibility() == 0) {
            this.mLoader.setVisibility(8);
            this.mRecyclerView.setVisibility(0);
        }
        this.mAdapter.swap(list);
        this.mIsUpdating = false;
        updateHeaderInfo();
        this.mSwipeRefreshLayout.setRefreshing(false);
    }

    private void updateHeaderInfo() {
        String str;
        TextView textView = (TextView) findViewById(R.id.count);
        StringBuilder sb = new StringBuilder();
        sb.append("Apps ");
        sb.append(this.mTaskList.size());
        textView.setText(sb.toString());
        TextView textView2 = (TextView) findViewById(R.id.usage);
        double availableMemory = getAvailableMemory();
        if (availableMemory > 1000.0d) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append(getString(R.string.task_free_ram));
            sb2.append(" ");
            sb2.append(((double) Math.round((availableMemory / 1024.0d) * 100.0d)) / 100.0d);
            sb2.append(" GB");
            str = sb2.toString();
        } else {
            StringBuilder sb3 = new StringBuilder();
            sb3.append(getString(R.string.task_free_ram));
            sb3.append(" ");
            sb3.append(availableMemory);
            sb3.append(" MB");
            str = sb3.toString();
        }
        textView2.setText(str);
    }

    private void setHeaderToRefresh() {
        ((TextView) findViewById(R.id.count)).setText(getString(R.string.header_status_loading));
        ((TextView) findViewById(R.id.usage)).setText("");
    }

    private double getAvailableMemory() {
        MemoryInfo memoryInfo = new MemoryInfo();
        ((ActivityManager) getApplicationContext().getSystemService("activity")).getMemoryInfo(memoryInfo);
        return ((double) Math.round((((double) memoryInfo.availMem) / 1048576.0d) * 100.0d)) / 100.0d;
    }

    private double getTotalUsage(List<Task> list) {
        double d = Utils.DOUBLE_EPSILON;
        for (Task memory : list) {
            d += memory.getMemory();
        }
        return ((double) Math.round(d * 100.0d)) / 100.0d;
    }

    private boolean isKilledAppAlive(String str) {
        if (this.mLastKilledTimestamp < System.currentTimeMillis() - 15000) {
            this.mLastKilledApp = null;
            return false;
        }
        Iterator it = this.mTaskList.iterator();
        while (it.hasNext()) {
            if (((Task) it.next()).getLabel().equals(str)) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: private */
    public void checkIfLastAppIsKilled() {
        if (this.mLastKilledApp != null && isKilledAppAlive(this.mLastKilledApp.getLabel())) {
            final String str = this.mLastKilledApp.getPackageInfo().packageName;
            Builder builder = new Builder(this);
            builder.setMessage((CharSequence) getString(R.string.kill_app_dialog_text)).setTitle((CharSequence) this.mLastKilledApp.getLabel());
            builder.setPositiveButton((int) R.string.force_close, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    TaskListActivity taskListActivity = TaskListActivity.this;
                    StringBuilder sb = new StringBuilder();
                    sb.append("package:");
                    sb.append(str);
                    taskListActivity.startActivity(new Intent("android.settings.APPLICATION_DETAILS_SETTINGS", Uri.parse(sb.toString())));
                }
            });
            builder.setNegativeButton((int) R.string.cancel, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.cancel();
                }
            });
            builder.create().show();
        }
        this.mLastKilledApp = null;
    }

    @TargetApi(21)
    private boolean hasSpecialPermission(Context context) {
        return ((AppOpsManager) context.getSystemService("appops")).checkOpNoThrow("android:get_usage_stats", Process.myUid(), context.getPackageName()) == 0;
    }

    @TargetApi(21)
    private void showPermissionInfoDialog() {
        Builder builder = new Builder(this);
        builder.setMessage((CharSequence) getString(R.string.package_usage_permission_text)).setTitle((CharSequence) getString(R.string.package_usage_permission_title));
        builder.setPositiveButton((int) R.string.open_settings, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                TaskListActivity.this.startActivity(new Intent("android.settings.USAGE_ACCESS_SETTINGS"));
            }
        });
        builder.setNegativeButton((int) R.string.cancel, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        builder.create().show();
    }
}
