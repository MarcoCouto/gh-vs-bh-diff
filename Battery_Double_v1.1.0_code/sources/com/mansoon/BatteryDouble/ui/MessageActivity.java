package com.mansoon.BatteryDouble.ui;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import com.mansoon.BatteryDouble.R;
import com.mansoon.BatteryDouble.managers.storage.GreenHubDb;

public class MessageActivity extends BaseActivity {
    /* access modifiers changed from: private */
    public int mMessageId;

    /* access modifiers changed from: protected */
    public void onCreate(@Nullable Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_message);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
        }
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setDisplayHomeAsUpEnabled(true);
        }
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.mMessageId = extras.getInt("id");
            ((TextView) findViewById(R.id.message_title)).setText(extras.getString("title"));
            ((TextView) findViewById(R.id.message_body)).setText(extras.getString(TtmlNode.TAG_BODY));
            TextView textView = (TextView) findViewById(R.id.message_date);
            try {
                String string = extras.getString("date");
                if (string != null) {
                    string = string.substring(0, 16);
                }
                if (string == null) {
                    string = getString(R.string.not_available);
                }
                textView.setText(string);
            } catch (NullPointerException e) {
                textView.setText(getString(R.string.not_available));
                e.printStackTrace();
            }
            FloatingActionButton floatingActionButton = (FloatingActionButton) findViewById(R.id.fabDeleteMessage);
            if (floatingActionButton != null) {
                floatingActionButton.setOnClickListener(new OnClickListener() {
                    public void onClick(View view) {
                        Builder builder = new Builder(view.getContext());
                        builder.setMessage((int) R.string.dialog_message_text).setTitle((int) R.string.dialog_message_title);
                        builder.setPositiveButton((int) R.string.ok, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogInterface, int i) {
                                GreenHubDb greenHubDb = new GreenHubDb();
                                greenHubDb.deleteMessage(MessageActivity.this.mMessageId);
                                greenHubDb.close();
                                dialogInterface.dismiss();
                                MessageActivity.this.finish();
                            }
                        });
                        builder.setNegativeButton((int) R.string.cancel, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                        builder.create().show();
                    }
                });
            }
        }
    }
}
