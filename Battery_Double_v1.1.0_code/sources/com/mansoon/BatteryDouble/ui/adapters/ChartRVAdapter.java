package com.mansoon.BatteryDouble.ui.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.github.mikephil.charting.animation.Easing.EasingOption;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.data.LineDataSet.Mode;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.mansoon.BatteryDouble.R;
import com.mansoon.BatteryDouble.models.ui.ChartCard;
import com.mansoon.BatteryDouble.ui.views.ChartMarkerView;
import com.mansoon.BatteryDouble.util.DateUtils;
import com.mansoon.BatteryDouble.util.StringHelper;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import java.util.ArrayList;

public class ChartRVAdapter extends Adapter<DashboardViewHolder> {
    public static final int BATTERY_LEVEL = 1;
    public static final int BATTERY_TEMPERATURE = 2;
    public static final int BATTERY_VOLTAGE = 3;
    private ArrayList<ChartCard> mChartCards;
    private Context mContext;
    private int mInterval;

    static class DashboardViewHolder extends ViewHolder {
        TextView avg;
        LineChart chart;
        CardView cv;
        RelativeLayout extras;
        TextView interval;
        TextView label;
        TextView max;
        TextView min;

        DashboardViewHolder(View view) {
            super(view);
            this.cv = (CardView) view.findViewById(R.id.cv);
            this.label = (TextView) view.findViewById(R.id.label);
            this.interval = (TextView) view.findViewById(R.id.interval);
            this.chart = (LineChart) view.findViewById(R.id.chart);
            this.extras = (RelativeLayout) view.findViewById(R.id.extra_details);
            this.min = (TextView) view.findViewById(R.id.minValue);
            this.avg = (TextView) view.findViewById(R.id.avgValue);
            this.max = (TextView) view.findViewById(R.id.maxValue);
        }
    }

    public ChartRVAdapter(ArrayList<ChartCard> arrayList, int i, Context context) {
        this.mChartCards = arrayList;
        this.mInterval = i;
        this.mContext = context;
    }

    public void setInterval(int i) {
        this.mInterval = i;
    }

    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public DashboardViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new DashboardViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.chart_card_view, viewGroup, false));
    }

    public void onBindViewHolder(DashboardViewHolder dashboardViewHolder, int i) {
        ChartCard chartCard = (ChartCard) this.mChartCards.get(i);
        setup(dashboardViewHolder, chartCard);
        dashboardViewHolder.chart.setData(loadData(chartCard));
        dashboardViewHolder.chart.invalidate();
        dashboardViewHolder.label.setText(chartCard.label);
        if (chartCard.extras != null) {
            if (chartCard.type == 2) {
                StringBuilder sb = new StringBuilder();
                sb.append("Min: ");
                sb.append(StringHelper.formatNumber(chartCard.extras[0]));
                sb.append(" ºC");
                dashboardViewHolder.min.setText(sb.toString());
                StringBuilder sb2 = new StringBuilder();
                sb2.append(this.mContext.getString(R.string.chart_average_title));
                sb2.append(": ");
                sb2.append(StringHelper.formatNumber(chartCard.extras[1]));
                sb2.append(" ºC");
                dashboardViewHolder.avg.setText(sb2.toString());
                StringBuilder sb3 = new StringBuilder();
                sb3.append("Max: ");
                sb3.append(StringHelper.formatNumber(chartCard.extras[2]));
                sb3.append(" ºC");
                dashboardViewHolder.max.setText(sb3.toString());
            } else if (chartCard.type == 3) {
                StringBuilder sb4 = new StringBuilder();
                sb4.append("Min: ");
                sb4.append(StringHelper.formatNumber(chartCard.extras[0]));
                sb4.append(" V");
                dashboardViewHolder.min.setText(sb4.toString());
                StringBuilder sb5 = new StringBuilder();
                sb5.append(this.mContext.getString(R.string.chart_average_title));
                sb5.append(": ");
                sb5.append(StringHelper.formatNumber(chartCard.extras[1]));
                sb5.append(" V");
                dashboardViewHolder.avg.setText(sb5.toString());
                StringBuilder sb6 = new StringBuilder();
                sb6.append("Max: ");
                sb6.append(StringHelper.formatNumber(chartCard.extras[2]));
                sb6.append(" V");
                dashboardViewHolder.max.setText(sb6.toString());
            }
            dashboardViewHolder.extras.setVisibility(0);
        }
        if (this.mInterval == 1) {
            dashboardViewHolder.interval.setText("Last 24h");
        } else if (this.mInterval == 2) {
            dashboardViewHolder.interval.setText("Last 3 days");
        } else if (this.mInterval == 3) {
            dashboardViewHolder.interval.setText("Last 5 days");
        }
    }

    public int getItemCount() {
        return this.mChartCards.size();
    }

    public void swap(ArrayList<ChartCard> arrayList) {
        if (this.mChartCards != null) {
            this.mChartCards.clear();
            this.mChartCards.addAll(arrayList);
        } else {
            this.mChartCards = arrayList;
        }
        notifyDataSetChanged();
    }

    private LineData loadData(ChartCard chartCard) {
        LineDataSet lineDataSet = new LineDataSet(chartCard.entries, null);
        lineDataSet.setMode(Mode.LINEAR);
        lineDataSet.setDrawValues(false);
        lineDataSet.setDrawCircleHole(false);
        lineDataSet.setColor(chartCard.color);
        lineDataSet.setCircleColor(chartCard.color);
        lineDataSet.setLineWidth(1.8f);
        lineDataSet.setDrawFilled(true);
        lineDataSet.setFillColor(chartCard.color);
        return new LineData(lineDataSet);
    }

    private void setup(DashboardViewHolder dashboardViewHolder, final ChartCard chartCard) {
        AnonymousClass1 r0 = new IAxisValueFormatter() {
            public String getFormattedValue(float f, AxisBase axisBase) {
                return DateUtils.convertMilliSecondsToFormattedDate(Long.valueOf((long) f));
            }
        };
        AnonymousClass2 r1 = new IAxisValueFormatter() {
            public String getFormattedValue(float f, AxisBase axisBase) {
                switch (chartCard.type) {
                    case 1:
                        return StringHelper.formatPercentageNumber(f);
                    case 2:
                        StringBuilder sb = new StringBuilder();
                        sb.append(StringHelper.formatNumber(f));
                        sb.append(" ºC");
                        return sb.toString();
                    case 3:
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append(StringHelper.formatNumber(f));
                        sb2.append(" V");
                        return sb2.toString();
                    default:
                        return String.valueOf(f);
                }
            }
        };
        dashboardViewHolder.chart.getXAxis().setValueFormatter(r0);
        if (chartCard.type == 1) {
            dashboardViewHolder.chart.getAxisLeft().setAxisMaximum(1.0f);
        }
        dashboardViewHolder.chart.setExtraBottomOffset(5.0f);
        dashboardViewHolder.chart.getAxisLeft().setDrawGridLines(false);
        dashboardViewHolder.chart.getAxisLeft().setValueFormatter(r1);
        dashboardViewHolder.chart.getAxisRight().setDrawGridLines(false);
        dashboardViewHolder.chart.getAxisRight().setDrawLabels(false);
        dashboardViewHolder.chart.getXAxis().setDrawGridLines(false);
        dashboardViewHolder.chart.getXAxis().setLabelCount(3);
        dashboardViewHolder.chart.getXAxis().setGranularity(1.0f);
        dashboardViewHolder.chart.getLegend().setEnabled(false);
        dashboardViewHolder.chart.getDescription().setEnabled(false);
        dashboardViewHolder.chart.setNoDataText(this.mContext.getString(R.string.chart_loading_data));
        dashboardViewHolder.chart.setMarker(new ChartMarkerView(dashboardViewHolder.itemView.getContext(), R.layout.item_marker, chartCard.type));
        dashboardViewHolder.chart.animateY((int) SettingsJsonConstants.ANALYTICS_FLUSH_INTERVAL_SECS_DEFAULT, EasingOption.Linear);
    }
}
