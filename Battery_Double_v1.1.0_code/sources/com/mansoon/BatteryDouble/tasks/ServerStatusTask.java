package com.mansoon.BatteryDouble.tasks;

import android.content.Context;
import android.os.AsyncTask;
import com.mansoon.BatteryDouble.network.handlers.ServerStatusHandler;

public class ServerStatusTask extends AsyncTask<Context, Void, Void> {
    private static final String TAG = "ServerStatusTask";

    /* access modifiers changed from: protected */
    public Void doInBackground(Context... contextArr) {
        new ServerStatusHandler().callGetStatus(contextArr[0]);
        return null;
    }
}
