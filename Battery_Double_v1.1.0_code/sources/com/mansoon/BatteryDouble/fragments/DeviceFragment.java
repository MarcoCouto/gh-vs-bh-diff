package com.mansoon.BatteryDouble.fragments;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;
import com.mansoon.BatteryDouble.R;
import com.mansoon.BatteryDouble.events.RefreshEvent;
import com.mansoon.BatteryDouble.models.Bluetooth;
import com.mansoon.BatteryDouble.models.Memory;
import com.mansoon.BatteryDouble.models.Network;
import com.mansoon.BatteryDouble.models.Phone;
import com.mansoon.BatteryDouble.models.Specifications;
import com.mansoon.BatteryDouble.models.Storage;
import com.mansoon.BatteryDouble.models.Wifi;
import com.mansoon.BatteryDouble.models.data.StorageDetails;
import com.mansoon.BatteryDouble.ui.TaskListActivity;
import org.altbeacon.beacon.BeaconManager;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class DeviceFragment extends Fragment {
    private Context mContext = null;
    /* access modifiers changed from: private */
    public Handler mHandler;
    /* access modifiers changed from: private */
    public ProgressBar mMemoryBar;
    /* access modifiers changed from: private */
    public TextView mMemoryFree;
    /* access modifiers changed from: private */
    public TextView mMemoryUsed;
    private View mParentView = null;
    /* access modifiers changed from: private */
    public ProgressBar mStorageBar;
    /* access modifiers changed from: private */
    public TextView mStorageFree;
    /* access modifiers changed from: private */
    public TextView mStorageUsed;
    private Runnable runnable = new Runnable() {
        public void run() {
            int[] readMemoryInfo = Memory.readMemoryInfo();
            StorageDetails storageDetails = Storage.getStorageDetails();
            DeviceFragment.this.mMemoryBar.setMax(readMemoryInfo[1]);
            DeviceFragment.this.mMemoryBar.setProgress(readMemoryInfo[1] - readMemoryInfo[2]);
            StringBuilder sb = new StringBuilder();
            sb.append((readMemoryInfo[1] - readMemoryInfo[2]) / 1024);
            sb.append(" MB");
            DeviceFragment.this.mMemoryUsed.setText(sb.toString());
            StringBuilder sb2 = new StringBuilder();
            sb2.append(readMemoryInfo[2] / 1024);
            sb2.append(" MB");
            DeviceFragment.this.mMemoryFree.setText(sb2.toString());
            DeviceFragment.this.mStorageBar.setMax(storageDetails.realmGet$total());
            DeviceFragment.this.mStorageBar.setProgress(storageDetails.realmGet$total() - storageDetails.realmGet$free());
            StringBuilder sb3 = new StringBuilder();
            sb3.append(storageDetails.realmGet$total() - storageDetails.realmGet$free());
            sb3.append(" MB");
            DeviceFragment.this.mStorageUsed.setText(sb3.toString());
            StringBuilder sb4 = new StringBuilder();
            sb4.append(storageDetails.realmGet$free());
            sb4.append(" MB");
            DeviceFragment.this.mStorageFree.setText(sb4.toString());
            DeviceFragment.this.mHandler.postDelayed(this, BeaconManager.DEFAULT_BACKGROUND_SCAN_PERIOD);
        }
    };

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(R.layout.fragment_device, viewGroup, false);
        this.mParentView = inflate;
        this.mContext = inflate.getContext();
        this.mHandler = new Handler();
        loadComponents(inflate);
        return inflate;
    }

    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    public static DeviceFragment newInstance() {
        return new DeviceFragment();
    }

    private void loadComponents(View view) {
        int i;
        StringBuilder sb = new StringBuilder();
        sb.append(Specifications.getBrand());
        sb.append(" ");
        sb.append(Specifications.getModel());
        ((TextView) view.findViewById(R.id.androidVersion)).setText(Specifications.getOsVersion());
        ((TextView) view.findViewById(R.id.androidModel)).setText(sb);
        TextView textView = (TextView) view.findViewById(R.id.androidImei);
        String deviceId = Phone.getDeviceId(this.mContext);
        if (deviceId == null) {
            deviceId = getString(R.string.not_available);
        }
        textView.setText(deviceId);
        TextView textView2 = (TextView) view.findViewById(R.id.androidRoot);
        if (Specifications.isRooted()) {
            i = R.string.yes;
        } else {
            i = R.string.no;
        }
        textView2.setText(getString(i));
        updateWifiData(view, Wifi.isEnabled(this.mContext));
        updateBluetoothData(view, Bluetooth.isEnabled());
        updateMobileData(view, Network.isMobileDataEnabled(this.mContext));
        this.mMemoryBar = (ProgressBar) view.findViewById(R.id.memoryBar);
        this.mMemoryUsed = (TextView) view.findViewById(R.id.memoryUsed);
        this.mMemoryFree = (TextView) view.findViewById(R.id.memoryFree);
        ((Button) view.findViewById(R.id.buttonViewMore)).setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                Answers.getInstance().logContentView(new ContentViewEvent().putContentName("Enters Task Manager").putContentType("Page visit").putContentId("page-task-manager"));
                DeviceFragment.this.startActivity(new Intent(view.getContext(), TaskListActivity.class));
            }
        });
        this.mStorageBar = (ProgressBar) view.findViewById(R.id.storageBar);
        this.mStorageUsed = (TextView) view.findViewById(R.id.storageUsed);
        this.mStorageFree = (TextView) view.findViewById(R.id.storageFree);
        this.mHandler.post(this.runnable);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void refreshData(RefreshEvent refreshEvent) {
        if (this.mParentView != null) {
            String str = refreshEvent.field;
            char c = 65535;
            int hashCode = str.hashCode();
            if (hashCode != -1068855134) {
                if (hashCode != 3649301) {
                    if (hashCode == 1968882350 && str.equals("bluetooth")) {
                        c = 1;
                    }
                } else if (str.equals("wifi")) {
                    c = 0;
                }
            } else if (str.equals("mobile")) {
                c = 2;
            }
            switch (c) {
                case 0:
                    updateWifiData(this.mParentView, refreshEvent.value);
                    break;
                case 1:
                    updateBluetoothData(this.mParentView, refreshEvent.value);
                    break;
                case 2:
                    updateMobileData(this.mParentView, refreshEvent.value);
                    break;
            }
        }
    }

    private void updateWifiData(View view, boolean z) {
        int i;
        TextView textView = (TextView) view.findViewById(R.id.wifi);
        if (z) {
            i = R.string.yes;
        } else {
            i = R.string.no;
        }
        textView.setText(getString(i));
        int i2 = 8;
        ((TextView) view.findViewById(R.id.ipAddressLabel)).setVisibility(z ? 0 : 8);
        TextView textView2 = (TextView) view.findViewById(R.id.ipAddress);
        if (z) {
            textView2.setText(Wifi.getIpAddress(this.mContext));
        }
        textView2.setVisibility(z ? 0 : 8);
        ((TextView) view.findViewById(R.id.macAddressLabel)).setVisibility(z ? 0 : 8);
        TextView textView3 = (TextView) view.findViewById(R.id.macAddress);
        if (z) {
            textView3.setText(Wifi.getMacAddress(this.mContext));
        }
        textView3.setVisibility(z ? 0 : 8);
        ((TextView) view.findViewById(R.id.ssidLabel)).setVisibility(z ? 0 : 8);
        TextView textView4 = (TextView) view.findViewById(R.id.ssid);
        if (z) {
            textView4.setText(Wifi.getInfo(this.mContext).getSSID());
        }
        if (z) {
            i2 = 0;
        }
        textView4.setVisibility(i2);
    }

    private void updateBluetoothData(View view, boolean z) {
        int i;
        TextView textView = (TextView) view.findViewById(R.id.bluetooth);
        if (z) {
            i = R.string.yes;
        } else {
            i = R.string.no;
        }
        textView.setText(getString(i));
        TextView textView2 = (TextView) view.findViewById(R.id.bluetoothAddress);
        if (z) {
            textView2.setText(Bluetooth.getAddress());
        }
        int i2 = 8;
        textView2.setVisibility(z ? 0 : 8);
        TextView textView3 = (TextView) view.findViewById(R.id.bluetoothAddressLabel);
        if (z) {
            i2 = 0;
        }
        textView3.setVisibility(i2);
    }

    private void updateMobileData(View view, boolean z) {
        int i;
        TextView textView = (TextView) view.findViewById(R.id.mobileData);
        if (z) {
            i = R.string.yes;
        } else {
            i = R.string.no;
        }
        textView.setText(getString(i));
        TextView textView2 = (TextView) view.findViewById(R.id.networkType);
        if (z) {
            textView2.setText(Network.getMobileNetworkType(this.mContext));
        }
        int i2 = 8;
        textView2.setVisibility(z ? 0 : 8);
        TextView textView3 = (TextView) view.findViewById(R.id.networkTypeLabel);
        if (z) {
            i2 = 0;
        }
        textView3.setVisibility(i2);
    }
}
