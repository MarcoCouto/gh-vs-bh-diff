package com.jaredrummler.android.processes.models;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

public final class Cgroup extends ProcFile {
    public static final Creator<Cgroup> CREATOR = new Creator<Cgroup>() {
        public Cgroup createFromParcel(Parcel parcel) {
            return new Cgroup(parcel);
        }

        public Cgroup[] newArray(int i) {
            return new Cgroup[i];
        }
    };
    public final ArrayList<ControlGroup> groups;

    public static Cgroup get(int i) throws IOException {
        return new Cgroup(String.format("/proc/%d/cgroup", new Object[]{Integer.valueOf(i)}));
    }

    private Cgroup(String str) throws IOException {
        super(str);
        String[] split = this.content.split("\n");
        this.groups = new ArrayList<>();
        for (String controlGroup : split) {
            try {
                this.groups.add(new ControlGroup(controlGroup));
            } catch (Exception unused) {
            }
        }
    }

    private Cgroup(Parcel parcel) {
        super(parcel);
        this.groups = parcel.createTypedArrayList(ControlGroup.CREATOR);
    }

    public ControlGroup getGroup(String str) {
        Iterator it = this.groups.iterator();
        while (it.hasNext()) {
            ControlGroup controlGroup = (ControlGroup) it.next();
            String[] split = controlGroup.subsystems.split(",");
            int length = split.length;
            int i = 0;
            while (true) {
                if (i < length) {
                    if (split[i].equals(str)) {
                        return controlGroup;
                    }
                    i++;
                }
            }
        }
        return null;
    }

    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeTypedList(this.groups);
    }
}
