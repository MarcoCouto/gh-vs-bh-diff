package com.revmob.ads.link.client;

import com.revmob.RevMobAdsListener;
import com.revmob.ads.internal.Ad;
import com.revmob.ads.internal.AdRevMobClientListener;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class LinkClientListener extends AdRevMobClientListener {
    public LinkClientListener(Ad ad, RevMobAdsListener revMobAdsListener) {
        super(ad, revMobAdsListener);
    }

    public void handleResponse(String str) throws JSONException {
        this.ad.updateWithData(createData(str));
    }

    public static LinkData createData(String str) throws JSONException {
        JSONObject jSONObject = new JSONObject(str).getJSONObject("anchor");
        String appOrSite = getAppOrSite(jSONObject);
        boolean openInside = getOpenInside(jSONObject);
        JSONArray jSONArray = jSONObject.getJSONArray("links");
        LinkData linkData = new LinkData(getImpressionUrl(jSONArray), getClickUrl(jSONArray), getFollowRedirect(jSONObject), appOrSite, openInside);
        return linkData;
    }
}
