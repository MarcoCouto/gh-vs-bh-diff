package com.revmob.ads.interstitial.internal;

import android.content.Context;
import com.revmob.internal.RevMobWebView;
import com.revmob.internal.RevMobWebViewClient;

public class FullscreenWebview extends RevMobWebView implements FullscreenView {
    public void update() {
    }

    public FullscreenWebview(Context context, RevMobWebViewClient revMobWebViewClient) {
        super(context, revMobWebViewClient);
    }

    public FullscreenWebview(Context context, String str, String str2, RevMobWebViewClient revMobWebViewClient) {
        super(context, str, str2, revMobWebViewClient);
    }
}
