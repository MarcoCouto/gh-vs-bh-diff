package com.revmob;

public enum RevMobTestingMode {
    WITH_ADS("with_ads"),
    WITHOUT_ADS("without_ads"),
    DISABLED(null);
    
    private String value;

    private RevMobTestingMode(String str) {
        this.value = str;
    }

    public String getValue() {
        return this.value;
    }
}
