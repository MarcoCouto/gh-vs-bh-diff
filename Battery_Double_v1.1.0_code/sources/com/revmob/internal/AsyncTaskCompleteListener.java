package com.revmob.internal;

interface AsyncTaskCompleteListener {
    void onTaskComplete(String str);
}
