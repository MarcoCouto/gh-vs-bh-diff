package com.revmob.internal;

import io.fabric.sdk.android.services.network.HttpRequest;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.ProtocolException;
import org.apache.http.client.RedirectHandler;
import org.apache.http.impl.client.AbstractHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.DefaultRedirectHandler;
import org.apache.http.protocol.HttpContext;

public class MarketRedirector {
    private static Pattern AMAZON_URL_PATTERN = Pattern.compile("android\\?p=[a-zA-Z0-9\\.]+");
    private static Pattern GOOGLE_PLAY_URL_PATTERN = Pattern.compile("details\\?id=[a-zA-Z0-9\\.]+");
    private static int TIMEOUT_IN_SECONDS = 30;
    private String entity;
    private AbstractHttpClient httpclient;
    private RedirectHandler redirectHandler;
    private String url;

    static class MarketRedirectHandler extends DefaultRedirectHandler {
        private URI lastRedirectedUri;

        MarketRedirectHandler() {
        }

        public boolean isRedirectRequested(HttpResponse httpResponse, HttpContext httpContext) {
            Header lastHeader = httpResponse.getLastHeader(HttpRequest.HEADER_LOCATION);
            if (lastHeader != null) {
                String value = lastHeader.getValue();
                if (MarketRedirector.isGooglePlayUri(value) || MarketRedirector.isAmazonAppStoreUri(value)) {
                    try {
                        this.lastRedirectedUri = new URI(value);
                        return false;
                    } catch (URISyntaxException unused) {
                        return MarketRedirector.super.isRedirectRequested(httpResponse, httpContext);
                    }
                }
            }
            return MarketRedirector.super.isRedirectRequested(httpResponse, httpContext);
        }

        public URI getLocationURI(HttpResponse httpResponse, HttpContext httpContext) {
            try {
                RMLog.d("Get location URI...");
                this.lastRedirectedUri = MarketRedirector.super.getLocationURI(httpResponse, httpContext);
                StringBuilder sb = new StringBuilder();
                sb.append("getLocationURI: ");
                sb.append(this.lastRedirectedUri);
                RMLog.d(sb.toString());
                return this.lastRedirectedUri;
            } catch (ProtocolException e) {
                e.printStackTrace();
                return null;
            }
        }

        public URI getLastRedirectedUrl() {
            return this.lastRedirectedUri;
        }
    }

    public static boolean isGooglePlayUri(String str) {
        return str.startsWith("market://");
    }

    public static boolean isAmazonAppStoreUri(String str) {
        return str.startsWith("amzn://");
    }

    static String rewriteMarketUrl(String str) {
        Matcher matcher = GOOGLE_PLAY_URL_PATTERN.matcher(str);
        Matcher matcher2 = AMAZON_URL_PATTERN.matcher(str);
        if (matcher.find()) {
            String group = matcher.group();
            StringBuilder sb = new StringBuilder();
            sb.append("market://");
            sb.append(group);
            return sb.toString();
        } else if (!matcher2.find()) {
            return str;
        } else {
            String group2 = matcher2.group();
            StringBuilder sb2 = new StringBuilder();
            sb2.append("amzn://apps/");
            sb2.append(group2);
            return sb2.toString();
        }
    }

    public MarketRedirector(String str) {
        this(str, "", new DefaultHttpClient(), new MarketRedirectHandler());
    }

    public MarketRedirector(String str, String str2) {
        this(str, str2, new DefaultHttpClient(), new MarketRedirectHandler());
    }

    MarketRedirector(String str, String str2, AbstractHttpClient abstractHttpClient, RedirectHandler redirectHandler2) {
        this.url = str;
        this.entity = str2;
        this.httpclient = abstractHttpClient;
        this.redirectHandler = redirectHandler2;
        this.httpclient.setRedirectHandler(this.redirectHandler);
    }

    public String getMarketUrl() {
        return getMarketUrl(null);
    }

    public String getMarketUrl(String str) {
        if (str != null && !str.endsWith("#click")) {
            return str;
        }
        if (isGooglePlayUri(this.url) || isAmazonAppStoreUri(this.url)) {
            return this.url;
        }
        post();
        URI lastRedirectedUrl = this.redirectHandler.getLastRedirectedUrl();
        if (lastRedirectedUrl != null) {
            return rewriteMarketUrl(lastRedirectedUrl.toString());
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    public HttpResponse post() {
        HTTPHelper hTTPHelper = new HTTPHelper(this.httpclient);
        hTTPHelper.setTimeout(TIMEOUT_IN_SECONDS);
        return hTTPHelper.post(this.url, this.entity);
    }
}
