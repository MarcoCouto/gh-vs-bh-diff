package com.revmob.android;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Build.VERSION;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;
import com.facebook.internal.NativeProtocol;
import com.facebook.internal.ServerProtocol;
import com.revmob.RevMobAdsListener;
import com.revmob.RevMobTestingMode;
import com.revmob.ads.banner.RevMobBanner;
import com.revmob.android.AdvertisingIdClient.AdInfo;
import com.revmob.client.RevMobClient;
import com.revmob.client.RevMobClientListener;
import com.revmob.internal.HTTPHelper;
import com.revmob.internal.RMLog;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import java.util.Arrays;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class RevMobContext {
    public static Context activity = null;
    public static Thread adThread = null;
    /* access modifiers changed from: private */
    public static String adTrackingEnabled = null;
    /* access modifiers changed from: private */
    public static String advertisingId = null;
    private static JSONObject age = new JSONObject();
    /* access modifiers changed from: private */
    public static String appId = null;
    private static String birthday = null;
    private static boolean facebook = false;
    private static boolean facebookSent = false;
    public static boolean getInstalledApps = false;
    public static boolean getRunningApps = false;
    private static boolean hasFacebookSDK = false;
    private static String lastName;
    /* access modifiers changed from: private */
    public static RevMobClientListener listener;
    private static DisplayMetrics metrics = new DisplayMetrics();
    private static String name;
    private static String[] permissions;
    public static RevMobAdsListener publisherListener;
    public static RUNNING_APPS_STATUS runningAppsStatus = RUNNING_APPS_STATUS.PAUSED;
    private static String token;
    private static JSONObject userInformation = new JSONObject();
    private static String user_ID;
    private final int MAX_INSTALLED_APPS = 40;

    public enum RUNNING_APPS_STATUS {
        PAUSED,
        ONLY_NOTIFY,
        NOTIFY_AND_FETCH
    }

    public static String getAndroidID() {
        return "";
    }

    private static String getSerial() {
        return "";
    }

    public static String toPayload(Activity activity2) {
        activity = activity2;
        ((Activity) activity).getWindowManager().getDefaultDisplay().getMetrics(metrics);
        return revmobJSONString();
    }

    public static String revmobJSONString() {
        return revmobJSON().toString();
    }

    public static JSONArray getAppPermissions() {
        try {
            permissions = activity.getPackageManager().getPackageInfo(activity.getApplicationContext().getPackageName(), 4096).requestedPermissions;
            return new JSONArray(Arrays.asList(permissions));
        } catch (NameNotFoundException unused) {
            return null;
        }
    }

    private static String getFacebookToken() {
        try {
            Object newInstance = Class.forName("com.revmob.internal.RevMobSocialInfo").getConstructor(new Class[0]).newInstance(new Object[0]);
            return (String) Class.forName("com.revmob.internal.RevMobSocialInfo").getMethod("getFacebookToken", new Class[]{Context.class}).invoke(newInstance, new Object[]{activity.getApplicationContext()});
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static JSONObject revmobJSON() {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("device", getDeviceJSON());
            jSONObject.put("sdk", getSDKJSON());
            jSONObject.put(SettingsJsonConstants.APP_KEY, getAppJSON());
            jSONObject.put(NotificationCompat.CATEGORY_SOCIAL, getSocialJSON());
            if (getInstalledApps) {
                getInstalledApps = false;
                jSONObject.put("installedApps", getInstalledApps());
            }
            if (runningAppsStatus == RUNNING_APPS_STATUS.NOTIFY_AND_FETCH && getRunningApps) {
                getRunningApps = false;
                jSONObject.put("runningApps", getRunningApps());
            }
            if (!(RevMobClient.t0 == 0 || RevMobClient.t1 == 0 || RevMobClient.t2 == 0 || RevMobClient.t3 == 0)) {
                jSONObject.put("time", getFetchTimeInfo());
            }
            if (RevMobBanner.isBannerImpression) {
                jSONObject.put("bannerImpressions", getBannerJSON());
                RevMobBanner.setBannerImpression(false);
            }
            if (RevMobClient.getInstance().getTestingMode() != RevMobTestingMode.DISABLED) {
                JSONObject jSONObject2 = new JSONObject();
                jSONObject2.put("response", RevMobClient.getInstance().getTestingMode().getValue());
                jSONObject.put("testing", jSONObject2);
            }
            return jSONObject;
        } catch (JSONException unused) {
            return null;
        }
    }

    public static JSONObject deviceIdentifierJSON() {
        try {
            JSONObject jSONObject = new JSONObject();
            JSONObject jSONObject2 = new JSONObject();
            putIfNotEmpty(jSONObject, "android_id", getAndroidID());
            putIfNotEmpty(jSONObject, "serial", getSerial());
            putIfNotEmpty(jSONObject, "identifier_for_advertising", getAdvertisingId());
            jSONObject2.put("identifiers", jSONObject);
            return jSONObject2;
        } catch (JSONException unused) {
            return null;
        }
    }

    private static JSONObject getSocialJSON() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        UserData.addUserInfo(jSONObject);
        try {
            Class.forName("com.facebook.Session");
            RMLog.i("Has Facebook SDK!");
            jSONObject.put("facebook_token", getFacebookToken());
        } catch (ClassNotFoundException unused) {
            RMLog.i("Facebook SDK not found.");
        }
        return jSONObject;
    }

    public static JSONObject getSDKJSON() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("name", RevMobClient.SDK_NAME);
        jSONObject.put(ServerProtocol.FALLBACK_DIALOG_PARAM_VERSION, RevMobClient.SDK_VERSION);
        jSONObject.put("testing_mode", RevMobClient.getInstance().getTestingMode().getValue());
        return jSONObject;
    }

    public static JSONObject getDeviceJSON() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("screen", getDeviceScreenJSON());
        putIfNotEmpty(jSONObject, "model", getModel());
        StringBuilder sb = new StringBuilder();
        sb.append("");
        sb.append(getApiVersion());
        putIfNotEmpty(jSONObject, "api", sb.toString());
        putIfNotEmpty(jSONObject, "manufacturer", getManufacturer());
        putIfNotEmpty(jSONObject, "os_version", getOsVersion());
        putIfNotEmpty(jSONObject, "orientation", getOrientation());
        putIfNotEmpty(jSONObject, "locale", getLocale());
        putIfNotEmpty(jSONObject, "ua", HTTPHelper.getUserAgent());
        if (HTTPHelper.getShouldExtractGeolocation()) {
            jSONObject.put("location", getUserLocation());
        }
        putIfNotEmpty(jSONObject, "android_id", getAndroidID());
        putIfNotEmpty(jSONObject, "serial", getSerial());
        putIfNotEmpty(jSONObject, "identifier_for_advertising", getAdvertisingId());
        putIfNotEmpty(jSONObject, "limit_ad_tracking", getAdTrackingEnabled());
        return jSONObject;
    }

    @TargetApi(8)
    private static String getOrientation() {
        Display defaultDisplay = ((WindowManager) activity.getSystemService("window")).getDefaultDisplay();
        int rotation = api8OrNewer() ? defaultDisplay.getRotation() : defaultDisplay.getOrientation();
        if (rotation == 0) {
            return "0";
        }
        if (rotation == 1) {
            return "90";
        }
        if (rotation == 2) {
            return "180";
        }
        return rotation == 3 ? "270" : "-1";
    }

    private static boolean api8OrNewer() {
        return !VERSION.RELEASE.startsWith("1.") && !VERSION.RELEASE.startsWith("2.0") && !VERSION.RELEASE.startsWith("2.1");
    }

    private static JSONObject getIdentitiesJSON() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        putIfNotEmpty(jSONObject, "android_id", getAndroidID());
        putIfNotEmpty(jSONObject, "serial", getSerial());
        putIfNotEmpty(jSONObject, "identifier_for_advertising", getAdvertisingId());
        putIfNotEmpty(jSONObject, "limit_ad_tracking", getAdTrackingEnabled());
        return jSONObject;
    }

    public static JSONObject getDeviceScreenJSON() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put(SettingsJsonConstants.ICON_WIDTH_KEY, metrics.widthPixels);
        jSONObject.put(SettingsJsonConstants.ICON_HEIGHT_KEY, metrics.heightPixels);
        jSONObject.put("scale", (double) metrics.density);
        jSONObject.put("density_dpi", metrics.densityDpi);
        return jSONObject;
    }

    public static JSONObject getUserLocation() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        try {
            LocationManager locationManager = (LocationManager) activity.getSystemService("location");
            if (locationManager != null && (activity.checkCallingOrSelfPermission("android.permission.ACCESS_FINE_LOCATION") == 0 || activity.checkCallingOrSelfPermission("android.permission.ACCESS_COARSE_LOCATION") == 0)) {
                Location lastKnownLocation = locationManager.getLastKnownLocation("gps");
                Location lastKnownLocation2 = locationManager.getLastKnownLocation("network");
                if (lastKnownLocation == null || lastKnownLocation2 == null) {
                    if (lastKnownLocation != null) {
                        jSONObject.put("latitude", lastKnownLocation.getLatitude());
                        jSONObject.put("longitude", lastKnownLocation.getLongitude());
                        jSONObject.put("accuracy", (double) lastKnownLocation.getAccuracy());
                    } else if (lastKnownLocation2 != null) {
                        jSONObject.put("latitude", lastKnownLocation2.getLatitude());
                        jSONObject.put("longitude", lastKnownLocation2.getLongitude());
                        jSONObject.put("accuracy", (double) lastKnownLocation2.getAccuracy());
                    }
                } else if (lastKnownLocation.getTime() > lastKnownLocation2.getTime()) {
                    jSONObject.put("latitude", lastKnownLocation.getLatitude());
                    jSONObject.put("longitude", lastKnownLocation.getLongitude());
                    jSONObject.put("accuracy", (double) lastKnownLocation.getAccuracy());
                } else {
                    jSONObject.put("latitude", lastKnownLocation2.getLatitude());
                    jSONObject.put("longitude", lastKnownLocation2.getLongitude());
                    jSONObject.put("accuracy", (double) lastKnownLocation2.getAccuracy());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jSONObject;
    }

    private static JSONObject getFetchTimeInfo() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        double d = (double) (RevMobClient.t2 - RevMobClient.t1);
        double d2 = (double) (RevMobClient.t3 - RevMobClient.t2);
        jSONObject.put("fetchTime", ((double) (RevMobClient.t1 - RevMobClient.t0)) / 1000.0d);
        jSONObject.put("sdkTime", d / 1000.0d);
        jSONObject.put("creativeTime", d2 / 1000.0d);
        return jSONObject;
    }

    private static JSONObject getBannerJSON() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("bannerCount", RevMobBanner.bannerCount);
        JSONObject jSONObject2 = new JSONObject();
        int i = 0;
        while (i < RevMobBanner.usedCampaigns.size()) {
            int i2 = i + 1;
            jSONObject2.put(String.valueOf(i2), RevMobBanner.usedCampaigns.get(i));
            i = i2;
        }
        jSONObject.put("campaigns", jSONObject2);
        return jSONObject;
    }

    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0031 */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x007f  */
    /* JADX WARNING: Removed duplicated region for block: B:6:0x0060 A[Catch:{ Exception -> 0x0070 }] */
    private static JSONObject getAppJSON() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        putIfNotEmpty(jSONObject, "bundle_identifier", activity.getPackageName());
        Resources resources = activity.getResources();
        putIfNotEmpty(jSONObject, NativeProtocol.BRIDGE_ARG_APP_NAME_STRING, resources.getText(resources.getIdentifier(NativeProtocol.BRIDGE_ARG_APP_NAME_STRING, "string", activity.getPackageName())).toString());
        try {
            PackageInfo packageInfo = activity.getPackageManager().getPackageInfo(activity.getPackageName(), 0);
            int i = packageInfo.versionCode;
            StringBuilder sb = new StringBuilder();
            sb.append("");
            sb.append(i);
            putIfNotEmpty(jSONObject, "app_version", sb.toString());
            if (getAppPermissions() != null) {
                putArrayIfNotEmpty(jSONObject, NativeProtocol.RESULT_ARGS_PERMISSIONS, getAppPermissions());
            }
            putIfNotEmpty(jSONObject, "app_version_name", packageInfo.versionName);
        } catch (Exception unused) {
        }
        if (!new StoredData(activity).isAlreadyTracked()) {
            putIfNotEmpty(jSONObject, "install_not_registered", ServerProtocol.DIALOG_RETURN_SCOPES_TRUE);
        }
        return jSONObject;
    }

    public static void printEnvironmentInformation(String str, Activity activity2) {
        activity = activity2;
        if (RevMobClient.SDK_SOURCE_NAME != null) {
            StringBuilder sb = new StringBuilder();
            sb.append("RevMob SDK Version: ");
            sb.append(RevMobClient.SDK_VERSION);
            sb.append(" (");
            sb.append(RevMobClient.SDK_SOURCE_NAME);
            sb.append("-");
            sb.append(RevMobClient.SDK_SOURCE_VERSION);
            sb.append(")");
            RMLog.i(sb.toString());
        } else {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("RevMob SDK Version: ");
            sb2.append(RevMobClient.SDK_VERSION);
            RMLog.i(sb2.toString());
        }
        StringBuilder sb3 = new StringBuilder();
        sb3.append("App ID: ");
        sb3.append(str);
        RMLog.i(sb3.toString());
        StringBuilder sb4 = new StringBuilder();
        sb4.append("IP Address: ");
        sb4.append(HTTPHelper.getIpAddress());
        RMLog.i(sb4.toString());
        StringBuilder sb5 = new StringBuilder();
        sb5.append("Simulator: ");
        sb5.append(isSimulator());
        RMLog.i(sb5.toString());
        StringBuilder sb6 = new StringBuilder();
        sb6.append("OS Version: ");
        sb6.append(getOsVersion());
        RMLog.i(sb6.toString());
        StringBuilder sb7 = new StringBuilder();
        sb7.append("Android API: ");
        sb7.append(getApiVersion());
        RMLog.i(sb7.toString());
        StringBuilder sb8 = new StringBuilder();
        sb8.append("Manufacturer: ");
        sb8.append(getManufacturer());
        RMLog.i(sb8.toString());
        StringBuilder sb9 = new StringBuilder();
        sb9.append("Model: ");
        sb9.append(getModel());
        RMLog.i(sb9.toString());
        StringBuilder sb10 = new StringBuilder();
        sb10.append("Android ID: ");
        sb10.append(getAndroidID());
        RMLog.i(sb10.toString());
        StringBuilder sb11 = new StringBuilder();
        sb11.append("Serial number: ");
        sb11.append(getSerial());
        RMLog.i(sb11.toString());
        StringBuilder sb12 = new StringBuilder();
        sb12.append("ID for Advertising: ");
        sb12.append(getAdvertisingId());
        RMLog.i(sb12.toString());
        StringBuilder sb13 = new StringBuilder();
        sb13.append("Limit Ad Tracking: ");
        sb13.append(getAdTrackingEnabled());
        RMLog.i(sb13.toString());
        StringBuilder sb14 = new StringBuilder();
        sb14.append("Language: ");
        sb14.append(getLanguage());
        RMLog.i(sb14.toString());
        StringBuilder sb15 = new StringBuilder();
        sb15.append("Locale: ");
        sb15.append(getLocale());
        RMLog.i(sb15.toString());
        StringBuilder sb16 = new StringBuilder();
        sb16.append("User Agent: ");
        sb16.append(HTTPHelper.getUserAgent());
        RMLog.i(sb16.toString());
        StringBuilder sb17 = new StringBuilder();
        sb17.append("Screen size: ");
        sb17.append(metrics.widthPixels);
        sb17.append(",");
        sb17.append(metrics.heightPixels);
        RMLog.i(sb17.toString());
        StringBuilder sb18 = new StringBuilder();
        sb18.append("Density scale: ");
        sb18.append(metrics.density);
        RMLog.i(sb18.toString());
        StringBuilder sb19 = new StringBuilder();
        sb19.append("Density dpi: ");
        sb19.append(metrics.densityDpi);
        RMLog.i(sb19.toString());
        try {
            StringBuilder sb20 = new StringBuilder();
            sb20.append("Installed Apps: ");
            sb20.append(getInstalledApps());
            RMLog.i(sb20.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            StringBuilder sb21 = new StringBuilder();
            sb21.append("User Location: ");
            sb21.append(getUserLocation());
            RMLog.i(sb21.toString());
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
    }

    public static String getModel() {
        return Build.MODEL;
    }

    public static String getManufacturer() {
        return Build.MANUFACTURER;
    }

    public static String getOsVersion() {
        return VERSION.RELEASE;
    }

    public static int getApiVersion() {
        return VERSION.SDK_INT;
    }

    public static boolean isSimulator() {
        return getModel().contains("sdk") || getModel().contains("Emulator");
    }

    public static String getLocale() {
        return Locale.getDefault().toString().replace('_', '-');
    }

    public static String getLanguage() {
        return Locale.getDefault().getLanguage();
    }

    private static void putIfNotEmpty(JSONObject jSONObject, String str, String str2) throws JSONException {
        if (str2 != null && !str2.equals("")) {
            jSONObject.put(str, str2);
        }
    }

    private static void putArrayIfNotEmpty(JSONObject jSONObject, String str, JSONArray jSONArray) throws JSONException {
        if (jSONArray != null && !jSONArray.equals("")) {
            jSONObject.put(str, jSONArray);
        }
    }

    private static JSONArray getInstalledApps() throws JSONException {
        JSONArray jSONArray = new JSONArray();
        HTTPHelper.setShouldExtractOtherAppsData(true);
        if (!HTTPHelper.getShouldExtractOtherAppsData()) {
            return null;
        }
        PackageManager packageManager = activity.getPackageManager();
        for (ApplicationInfo applicationInfo : packageManager.getInstalledApplications(128)) {
            if ((applicationInfo.flags & 1) != 1) {
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("packageName", applicationInfo.packageName);
                jSONObject.put("name", applicationInfo.loadLabel(packageManager));
                jSONArray.put(jSONObject);
            }
        }
        return jSONArray;
    }

    private static JSONObject getRunningApps() throws JSONException {
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(activity.getApplicationContext());
        new JSONArray();
        JSONArray jSONArray = new JSONArray(defaultSharedPreferences.getString("runningApps", new JSONArray().toString()));
        return (JSONObject) jSONArray.get(jSONArray.length() - 1);
    }

    private static String getAdTrackingEnabled() {
        return adTrackingEnabled;
    }

    private static String getAdvertisingId() {
        return advertisingId;
    }

    public static void loadAdvertisingInfo(String str, RevMobClientListener revMobClientListener, RevMobAdsListener revMobAdsListener, Activity activity2) {
        try {
            appId = str;
            listener = revMobClientListener;
            publisherListener = revMobAdsListener;
            activity = activity2;
            if (adThread == null) {
                adThread = new Thread(new Runnable() {
                    public void run() {
                        try {
                            AdInfo advertisingIdInfo = AdvertisingIdClient.getAdvertisingIdInfo(RevMobContext.activity);
                            RevMobContext.advertisingId = advertisingIdInfo.getId();
                            RevMobContext.adTrackingEnabled = advertisingIdInfo.isLimitAdTrackingEnabled() ? ServerProtocol.DIALOG_RETURN_SCOPES_TRUE : "false";
                            RevMobClient.getInstance().startSession(RevMobContext.appId, RevMobContext.toPayload((Activity) RevMobContext.activity), RevMobContext.listener, RevMobContext.publisherListener);
                        } catch (Exception e) {
                            StringBuilder sb = new StringBuilder();
                            sb.append("Error with Google Play Services: ");
                            sb.append(e.getMessage());
                            RMLog.e(sb.toString());
                            e.printStackTrace();
                        }
                    }
                });
                adThread.start();
            }
        } catch (Exception e) {
            StringBuilder sb = new StringBuilder();
            sb.append("Error loading advertising info: ");
            sb.append(e.getMessage());
            RMLog.e(sb.toString());
        }
    }

    public static boolean isApplicationStarted() {
        return activity != null;
    }

    public static Context getApplicationContext() {
        return activity;
    }
}
