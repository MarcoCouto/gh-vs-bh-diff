package com.revmob.android;

import android.app.Activity;
import android.util.DisplayMetrics;

public class RevMobScreen {
    static int densityDpi;
    static float scale;
    static int screenHeight;
    static int screenWidth;

    public static void load(Activity activity) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        screenWidth = displayMetrics.widthPixels;
        screenHeight = displayMetrics.heightPixels;
        scale = displayMetrics.density;
        densityDpi = displayMetrics.densityDpi;
    }

    public static int getScreenWidth() {
        return screenWidth;
    }

    public static int getScreenHeight() {
        return screenHeight;
    }

    public static float getScale() {
        return scale;
    }

    public static int getDensityDpi() {
        return densityDpi;
    }
}
