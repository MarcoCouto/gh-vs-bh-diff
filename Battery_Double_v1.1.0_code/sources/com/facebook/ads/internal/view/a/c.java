package com.facebook.ads.internal.view.a;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.widget.FrameLayout;
import com.facebook.ads.internal.f.b;
import com.facebook.ads.internal.view.a;
import com.facebook.ads.internal.view.a.C0012a;
import com.facebook.ads.internal.w.e.g;

public abstract class c extends FrameLayout {
    boolean a;
    protected final e b;
    private final com.facebook.ads.internal.s.c c;
    /* access modifiers changed from: private */
    public final String d;
    /* access modifiers changed from: private */
    @Nullable
    public final a e;
    /* access modifiers changed from: private */
    @Nullable
    public final C0012a f;
    /* access modifiers changed from: private */
    @Nullable
    public b g;
    /* access modifiers changed from: private */
    public int h;
    /* access modifiers changed from: private */
    public b i;
    /* access modifiers changed from: private */
    public b.a j;
    /* access modifiers changed from: private */
    public com.facebook.ads.internal.f.c k;

    public c(Context context, com.facebook.ads.internal.s.c cVar, String str) {
        this(context, cVar, str, null, null);
    }

    public c(Context context, com.facebook.ads.internal.s.c cVar, String str, @Nullable a aVar, @Nullable C0012a aVar2) {
        super(context);
        this.h = 0;
        this.j = b.a.NONE;
        this.k = null;
        this.b = new e() {
            public void a() {
                if (c.this.k == null) {
                    a(false);
                    return;
                }
                c.this.h = c.this.h - 1;
                if (c.this.k.e() == null) {
                    c.this.g();
                } else {
                    c.a(c.this, c.this.k.e());
                }
            }

            public void a(b.a aVar) {
                c.this.h = c.this.h + 1;
                c.this.j = aVar;
                c.a(c.this, c.this.j == b.a.HIDE ? com.facebook.ads.internal.f.a.d(c.this.getContext()) : com.facebook.ads.internal.f.a.g(c.this.getContext()));
            }

            public void a(com.facebook.ads.internal.f.c cVar) {
                c.this.h = c.this.h + 1;
                c.this.i.a(cVar.a());
                if (cVar.d().isEmpty()) {
                    c.b(c.this, cVar);
                    if (c.this.g != null) {
                        c.this.g.a(cVar, c.this.j);
                    }
                } else {
                    c.a(c.this, cVar);
                }
            }

            public void a(boolean z) {
                c.this.c();
                if (c.this.e != null) {
                    c.this.e.b(true);
                }
                if (c.this.g != null) {
                    c.this.g.a(z);
                }
                if (!z) {
                    c.this.f();
                }
            }

            public void b() {
                if (c.this.f != null) {
                    c.this.f.a("com.facebook.ads.adreporting.FINISH_AD_REPORTING_FLOW");
                }
            }

            public void c() {
                if (!TextUtils.isEmpty(com.facebook.ads.internal.f.a.n(c.this.getContext()))) {
                    g.a(new g(), c.this.getContext(), Uri.parse(com.facebook.ads.internal.f.a.n(c.this.getContext())), c.this.d);
                }
                c.this.i.c();
            }

            public void d() {
                c.this.c();
                if (c.this.e != null) {
                    c.this.e.b(true);
                }
                if (!TextUtils.isEmpty(com.facebook.ads.internal.f.a.m(c.this.getContext()))) {
                    g.a(new g(), c.this.getContext(), Uri.parse(com.facebook.ads.internal.f.a.m(c.this.getContext())), c.this.d);
                }
                c.this.i.b();
                c.this.f();
            }
        };
        this.c = cVar;
        this.e = aVar;
        this.f = aVar2;
        this.d = str;
    }

    static /* synthetic */ void a(c cVar, com.facebook.ads.internal.f.c cVar2) {
        cVar.k = cVar2;
        cVar.i.a(cVar.j, cVar.h);
        cVar.a(cVar2, cVar.j);
    }

    static /* synthetic */ void b(c cVar, com.facebook.ads.internal.f.c cVar2) {
        cVar.i.a(cVar.j);
        cVar.b(cVar2, cVar.j);
        if (cVar.e()) {
            cVar.f();
        }
    }

    /* access modifiers changed from: private */
    public void f() {
        if (this.i.e()) {
            this.c.n(this.d, this.i.d());
            this.i.f();
        }
    }

    /* access modifiers changed from: private */
    public void g() {
        this.k = null;
        this.i.a();
        d();
    }

    public void a() {
        this.i = new b();
        if (this.e != null) {
            this.e.a_(true);
        }
        g();
        if (this.g != null) {
            this.g.a();
        }
    }

    /* access modifiers changed from: 0000 */
    public abstract void a(com.facebook.ads.internal.f.c cVar, b.a aVar);

    public void a(boolean z) {
        this.a = z;
    }

    public void b() {
        f();
    }

    /* access modifiers changed from: 0000 */
    public abstract void b(com.facebook.ads.internal.f.c cVar, b.a aVar);

    /* access modifiers changed from: 0000 */
    public abstract void c();

    /* access modifiers changed from: 0000 */
    public abstract void d();

    /* access modifiers changed from: 0000 */
    public abstract boolean e();

    public void setAdReportingFlowListener(@Nullable b bVar) {
        this.g = bVar;
    }
}
