package com.facebook.ads.internal.h;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.Log;
import com.facebook.ads.internal.v.b.a.b;
import com.facebook.ads.internal.v.b.a.f;
import com.facebook.ads.internal.v.b.a.g;
import com.facebook.ads.internal.v.b.l;
import com.facebook.ads.internal.v.b.o;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class c {
    /* access modifiers changed from: private */
    public static final String a = "c";
    private static c b;
    private final Future<a> c;

    private static class a {
        private static final Map<String, File> a = new HashMap();
        private final Context b;

        a(Context context) {
            this.b = context;
        }

        /* access modifiers changed from: 0000 */
        @Nullable
        public String a(String str) {
            File file = (File) a.get(str);
            if (file == null) {
                return null;
            }
            StringBuilder sb = new StringBuilder();
            sb.append("file://");
            sb.append(file.getPath());
            return sb.toString();
        }

        /* access modifiers changed from: 0000 */
        /* JADX WARNING: Removed duplicated region for block: B:31:0x0084 A[SYNTHETIC, Splitter:B:31:0x0084] */
        /* JADX WARNING: Removed duplicated region for block: B:38:0x0095 A[SYNTHETIC, Splitter:B:38:0x0095] */
        public boolean b(String str) {
            InputStream inputStream = null;
            try {
                File file = new File(o.a(this.b), new f().a(str));
                b bVar = new b(file, new g(67108864));
                if (bVar.d()) {
                    a.put(str, file);
                    bVar.b();
                    return true;
                }
                URLConnection openConnection = new URL(str).openConnection();
                openConnection.connect();
                BufferedInputStream bufferedInputStream = new BufferedInputStream(openConnection.getInputStream());
                try {
                    byte[] bArr = new byte[8192];
                    while (true) {
                        int read = bufferedInputStream.read(bArr);
                        if (read == -1) {
                            break;
                        }
                        bVar.a(bArr, read);
                    }
                    bVar.c();
                    a.put(str, file);
                    if (bufferedInputStream != null) {
                        try {
                            bufferedInputStream.close();
                            return true;
                        } catch (IOException e) {
                            Log.e(c.a, "Error closing the file", e);
                        }
                    }
                    return true;
                } catch (l | IOException e2) {
                    e = e2;
                    inputStream = bufferedInputStream;
                    try {
                        Log.e(c.a, "Error caching the file", e);
                        if (inputStream != null) {
                        }
                        return false;
                    } catch (Throwable th) {
                        th = th;
                        if (inputStream != null) {
                            try {
                                inputStream.close();
                            } catch (IOException e3) {
                                Log.e(c.a, "Error closing the file", e3);
                            }
                        }
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    inputStream = bufferedInputStream;
                    if (inputStream != null) {
                    }
                    throw th;
                }
            } catch (l | IOException e4) {
                e = e4;
                Log.e(c.a, "Error caching the file", e);
                if (inputStream != null) {
                    try {
                        inputStream.close();
                        return false;
                    } catch (IOException e5) {
                        Log.e(c.a, "Error closing the file", e5);
                        return false;
                    }
                }
                return false;
            }
        }
    }

    private c(final Context context) {
        this.c = Executors.newSingleThreadExecutor().submit(new Callable<a>() {
            /* renamed from: a */
            public a call() {
                return new a(context);
            }
        });
    }

    public static c a(Context context) {
        if (b == null) {
            synchronized (e.class) {
                if (b == null) {
                    b = new c(context.getApplicationContext());
                }
            }
        }
        return b;
    }

    @Nullable
    private a b() {
        try {
            return (a) this.c.get(500, TimeUnit.MILLISECONDS);
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            Log.e(a, "Timed out waiting for cache server.", e);
            return null;
        }
    }

    public boolean a(String str) {
        a b2 = b();
        return b2 != null && b2.b(str);
    }

    @Nullable
    public String b(String str) {
        a b2 = b();
        if (b2 == null) {
            return null;
        }
        return b2.a(str);
    }
}
