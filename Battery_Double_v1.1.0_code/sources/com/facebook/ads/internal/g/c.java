package com.facebook.ads.internal.g;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;

public class c {

    public static class a {
        public String a;
        public String b;
        public boolean c;

        public a(String str, String str2, boolean z) {
            this.a = str;
            this.b = str2;
            this.c = z;
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(4:21|22|(1:24)|25) */
    /* JADX WARNING: Code restructure failed: missing block: B:22:?, code lost:
        r2 = new com.facebook.ads.internal.g.c.a(null, null, false);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0068, code lost:
        if (r11 != null) goto L_0x006a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x006a, code lost:
        r11.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x006d, code lost:
        return r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x006e, code lost:
        r0 = th;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:21:0x0063 */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0071  */
    public static a a(ContentResolver contentResolver) {
        Cursor cursor;
        try {
            ContentResolver contentResolver2 = contentResolver;
            cursor = contentResolver2.query(Uri.parse("content://com.facebook.katana.provider.AttributionIdProvider"), new String[]{"aid", "androidid", "limit_tracking"}, null, null, null);
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    a aVar = new a(cursor.getString(cursor.getColumnIndex("aid")), cursor.getString(cursor.getColumnIndex("androidid")), Boolean.valueOf(cursor.getString(cursor.getColumnIndex("limit_tracking"))).booleanValue());
                    if (cursor != null) {
                        cursor.close();
                    }
                    return aVar;
                }
            }
            a aVar2 = new a(null, null, false);
            if (cursor != null) {
                cursor.close();
            }
            return aVar2;
        } catch (Exception unused) {
            cursor = null;
        } catch (Throwable th) {
            th = th;
            cursor = null;
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }
}
