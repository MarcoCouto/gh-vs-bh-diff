package com.facebook.ads.internal.w.b;

import android.content.Context;
import android.content.pm.Signature;
import android.os.Build;
import android.support.annotation.Nullable;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.security.MessageDigest;
import java.security.cert.CertificateFactory;

public class f {
    private static final String a = "f";

    public enum a {
        UNKNOWN(0),
        UNROOTED(1),
        ROOTED(2);
        
        public final int d;

        private a(int i) {
            this.d = i;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0026, code lost:
        if (a("su") == false) goto L_0x0029;
     */
    public static a a() {
        try {
            boolean z = false;
            if (!new File("/system/app/Superuser.apk").exists()) {
                String str = Build.TAGS;
                if (!(str != null && str.contains("test-keys"))) {
                }
            }
            z = true;
            return z ? a.ROOTED : a.UNROOTED;
        } catch (Throwable unused) {
            return a.UNKNOWN;
        }
    }

    @Nullable
    public static String a(Context context) {
        try {
            StringBuilder sb = new StringBuilder();
            for (Signature byteArray : context.getPackageManager().getPackageInfo(context.getPackageName(), 64).signatures) {
                sb.append(i.a(MessageDigest.getInstance("SHA1").digest(CertificateFactory.getInstance("X.509").generateCertificate(new ByteArrayInputStream(byteArray.toByteArray())).getPublicKey().getEncoded())));
                sb.append(";");
            }
            return sb.toString();
        } catch (Exception unused) {
            return null;
        }
    }

    private static boolean a(String str) {
        for (String file : System.getenv("PATH").split(":")) {
            File file2 = new File(file);
            if (file2.exists() && file2.isDirectory()) {
                File[] listFiles = file2.listFiles();
                if (listFiles == null) {
                    continue;
                } else {
                    for (File name : listFiles) {
                        if (name.getName().equals(str)) {
                            return true;
                        }
                    }
                    continue;
                }
            }
        }
        return false;
    }
}
