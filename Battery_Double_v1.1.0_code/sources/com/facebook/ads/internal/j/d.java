package com.facebook.ads.internal.j;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;
import android.os.Build.VERSION;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import android.text.TextUtils;
import com.facebook.ads.internal.w.b.p;
import com.facebook.ads.internal.w.h.b;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class d {
    private static final String a;
    private static final ReentrantReadWriteLock b = new ReentrantReadWriteLock();
    private static final Lock c = b.readLock();
    /* access modifiers changed from: private */
    public static final Lock d = b.writeLock();
    /* access modifiers changed from: private */
    public final Context e;
    /* access modifiers changed from: private */
    public final h f = new h(this);
    /* access modifiers changed from: private */
    public final c g = new c(this);
    private SQLiteOpenHelper h;

    private static class a<T> extends AsyncTask<Void, Void, T> {
        private final f<T> a;
        private final a<T> b;
        private final Context c;
        private com.facebook.ads.internal.j.f.a d;

        a(Context context, f<T> fVar, a<T> aVar) {
            this.a = fVar;
            this.b = aVar;
            this.c = context;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public T doInBackground(Void... voidArr) {
            Exception e;
            T t;
            try {
                t = this.a.b();
                try {
                    this.d = this.a.c();
                    return t;
                } catch (Exception e2) {
                    e = e2;
                    com.facebook.ads.internal.w.h.a.b(this.c, "database", b.x, e);
                    this.d = com.facebook.ads.internal.j.f.a.UNKNOWN;
                    return t;
                }
            } catch (Exception e3) {
                Exception exc = e3;
                t = null;
                e = exc;
                com.facebook.ads.internal.w.h.a.b(this.c, "database", b.x, e);
                this.d = com.facebook.ads.internal.j.f.a.UNKNOWN;
                return t;
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(T t) {
            if (this.d == null) {
                this.b.a(t);
            } else {
                this.b.a(this.d.a(), this.d.b());
            }
            this.b.a();
        }
    }

    static {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT tokens.");
        sb.append(h.a.b);
        sb.append(", ");
        sb.append("tokens");
        sb.append(".");
        sb.append(h.b.b);
        sb.append(", ");
        sb.append("events");
        sb.append(".");
        sb.append(c.a.b);
        sb.append(", ");
        sb.append("events");
        sb.append(".");
        sb.append(c.c.b);
        sb.append(", ");
        sb.append("events");
        sb.append(".");
        sb.append(c.d.b);
        sb.append(", ");
        sb.append("events");
        sb.append(".");
        sb.append(c.e.b);
        sb.append(", ");
        sb.append("events");
        sb.append(".");
        sb.append(c.f.b);
        sb.append(", ");
        sb.append("events");
        sb.append(".");
        sb.append(c.g.b);
        sb.append(", ");
        sb.append("events");
        sb.append(".");
        sb.append(c.h.b);
        sb.append(", ");
        sb.append("events");
        sb.append(".");
        sb.append(c.i.b);
        sb.append(" FROM ");
        sb.append("events");
        sb.append(" JOIN ");
        sb.append("tokens");
        sb.append(" ON ");
        sb.append("events");
        sb.append(".");
        sb.append(c.b.b);
        sb.append(" = ");
        sb.append("tokens");
        sb.append(".");
        sb.append(h.a.b);
        sb.append(" ORDER BY ");
        sb.append("events");
        sb.append(".");
        sb.append(c.e.b);
        sb.append(" ASC");
        a = sb.toString();
    }

    public d(Context context) {
        this.e = context;
    }

    private synchronized SQLiteDatabase j() {
        if (this.h == null) {
            this.h = new e(this.e, this);
        }
        return this.h.getWritableDatabase();
    }

    @WorkerThread
    public Cursor a(int i) {
        c.lock();
        try {
            SQLiteDatabase a2 = a();
            StringBuilder sb = new StringBuilder();
            sb.append(a);
            sb.append(" LIMIT ");
            sb.append(String.valueOf(i));
            Cursor rawQuery = a2.rawQuery(sb.toString(), null);
            return rawQuery;
        } finally {
            c.unlock();
        }
    }

    public SQLiteDatabase a() {
        if (Looper.myLooper() != Looper.getMainLooper()) {
            return j();
        }
        throw new IllegalStateException("Cannot call getDatabase from the UI thread!");
    }

    public <T> AsyncTask a(f<T> fVar, a<T> aVar) {
        Executor executor = p.b;
        a aVar2 = new a(this.e.getApplicationContext(), fVar, aVar);
        Void[] voidArr = new Void[0];
        if (VERSION.SDK_INT >= 11) {
            aVar2.executeOnExecutor(executor, voidArr);
            return aVar2;
        }
        aVar2.execute(voidArr);
        return aVar2;
    }

    public AsyncTask a(String str, int i, String str2, double d2, double d3, String str3, Map<String, String> map, a<String> aVar) {
        final String str4 = str;
        final int i2 = i;
        final String str5 = str2;
        final double d4 = d2;
        final double d5 = d3;
        final String str6 = str3;
        final Map<String, String> map2 = map;
        AnonymousClass1 r0 = new i<String>() {
            /* JADX WARNING: Removed duplicated region for block: B:32:0x0090 A[Catch:{ Exception -> 0x0094 }] */
            /* JADX WARNING: Removed duplicated region for block: B:44:0x00b9 A[Catch:{ Exception -> 0x00bd }] */
            @Nullable
            /* renamed from: a */
            public String b() {
                SQLiteDatabase sQLiteDatabase;
                if (TextUtils.isEmpty(str4)) {
                    return null;
                }
                d.d.lock();
                try {
                    sQLiteDatabase = d.this.a();
                    try {
                        sQLiteDatabase.beginTransaction();
                        String a2 = d.this.g.a(d.this.f.a(str4), i2, str5, d4, d5, str6, map2);
                        sQLiteDatabase.setTransactionSuccessful();
                        if (sQLiteDatabase != null && sQLiteDatabase.isOpen()) {
                            try {
                                if (sQLiteDatabase.inTransaction()) {
                                    sQLiteDatabase.endTransaction();
                                }
                            } catch (Exception e2) {
                                com.facebook.ads.internal.w.h.a.b(d.this.e, "database", b.w, e2);
                            }
                        }
                        d.d.unlock();
                        return a2;
                    } catch (Exception e3) {
                        e = e3;
                        try {
                            a(com.facebook.ads.internal.j.f.a.DATABASE_INSERT);
                            com.facebook.ads.internal.w.h.a.b(d.this.e, "database", b.u, e);
                            try {
                                if (sQLiteDatabase.inTransaction()) {
                                }
                            } catch (Exception e4) {
                                com.facebook.ads.internal.w.h.a.b(d.this.e, "database", b.w, e4);
                            }
                            d.d.unlock();
                            return null;
                        } catch (Throwable th) {
                            th = th;
                            try {
                                if (sQLiteDatabase.inTransaction()) {
                                }
                            } catch (Exception e5) {
                                com.facebook.ads.internal.w.h.a.b(d.this.e, "database", b.w, e5);
                            }
                            d.d.unlock();
                            throw th;
                        }
                    }
                } catch (Exception e6) {
                    e = e6;
                    sQLiteDatabase = null;
                    a(com.facebook.ads.internal.j.f.a.DATABASE_INSERT);
                    com.facebook.ads.internal.w.h.a.b(d.this.e, "database", b.u, e);
                    if (sQLiteDatabase != null && sQLiteDatabase.isOpen()) {
                        if (sQLiteDatabase.inTransaction()) {
                            sQLiteDatabase.endTransaction();
                        }
                    }
                    d.d.unlock();
                    return null;
                } catch (Throwable th2) {
                    th = th2;
                    sQLiteDatabase = null;
                    if (sQLiteDatabase != null && sQLiteDatabase.isOpen()) {
                        if (sQLiteDatabase.inTransaction()) {
                            sQLiteDatabase.endTransaction();
                        }
                    }
                    d.d.unlock();
                    throw th;
                }
            }
        };
        return a(r0, aVar);
    }

    @WorkerThread
    public boolean a(String str) {
        d.lock();
        boolean z = false;
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("UPDATE ");
            sb.append("events");
            sb.append(" SET ");
            sb.append(c.i.b);
            sb.append("=");
            sb.append(c.i.b);
            sb.append("+1");
            sb.append(" WHERE ");
            sb.append(c.a.b);
            sb.append("=?");
            a().execSQL(sb.toString(), new String[]{str});
            z = true;
        } catch (SQLiteException unused) {
        }
        d.unlock();
        return z;
    }

    public synchronized void b() {
        for (g e2 : c()) {
            e2.e();
        }
        if (this.h != null) {
            this.h.close();
            this.h = null;
        }
    }

    @WorkerThread
    public boolean b(String str) {
        d.lock();
        try {
            return this.g.a(str);
        } finally {
            d.unlock();
        }
    }

    public g[] c() {
        return new g[]{this.f, this.g};
    }

    public Cursor d() {
        c.lock();
        try {
            return this.g.c();
        } finally {
            c.unlock();
        }
    }

    @WorkerThread
    public Cursor e() {
        c.lock();
        try {
            return this.g.d();
        } finally {
            c.unlock();
        }
    }

    @WorkerThread
    public Cursor f() {
        c.lock();
        try {
            return this.f.c();
        } finally {
            c.unlock();
        }
    }

    @WorkerThread
    public void g() {
        d.lock();
        try {
            this.f.d();
        } finally {
            d.unlock();
        }
    }

    @WorkerThread
    public void h() {
        d.lock();
        try {
            this.g.g();
            this.f.g();
        } finally {
            d.unlock();
        }
    }
}
