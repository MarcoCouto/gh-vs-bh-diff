package com.facebook.ads.internal.v.b;

import android.util.Log;
import java.lang.Thread.State;
import java.util.concurrent.atomic.AtomicInteger;

class k {
    private final n a;
    private final a b;
    private final Object c = new Object();
    private final Object d = new Object();
    private final AtomicInteger e;
    private volatile Thread f;
    private volatile boolean g;
    private volatile int h = -1;

    private class a implements Runnable {
        private a() {
        }

        public void run() {
            k.a(k.this);
        }
    }

    public k(n nVar, a aVar) {
        this.a = (n) j.a(nVar);
        this.b = (a) j.a(aVar);
        this.e = new AtomicInteger();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0038, code lost:
        r2 = r2 + r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0042, code lost:
        r0 = r9.d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0044, code lost:
        monitor-enter(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0049, code lost:
        if (r9.c() != false) goto L_0x005e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0057, code lost:
        if (r9.b.a() != r9.a.a()) goto L_0x005e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0059, code lost:
        r9.b.c();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x005e, code lost:
        monitor-exit(r0);
     */
    static /* synthetic */ void a(k kVar) {
        int i;
        int i2;
        int i3;
        Throwable th;
        int i4;
        int i5 = 0;
        try {
            i = kVar.b.a();
            try {
                kVar.a.a(i);
                i2 = kVar.a.a();
                try {
                    byte[] bArr = new byte[8192];
                    while (true) {
                        int a2 = kVar.a.a(bArr);
                        if (a2 == -1) {
                            break;
                        }
                        synchronized (kVar.d) {
                            if (!kVar.c()) {
                                kVar.b.a(bArr, a2);
                            }
                        }
                        kVar.b((long) i, (long) i2);
                    }
                    kVar.d();
                    kVar.b((long) i, (long) i2);
                } catch (Throwable th2) {
                    th = th2;
                    kVar.d();
                    kVar.b((long) i, (long) i2);
                    throw th;
                }
            } catch (Throwable th3) {
                Throwable th4 = th3;
                i2 = -1;
                th = th4;
                kVar.d();
                kVar.b((long) i, (long) i2);
                throw th;
            }
        } catch (Throwable th5) {
            i2 = -1;
            th = th5;
            i = 0;
            kVar.d();
            kVar.b((long) i, (long) i2);
            throw th;
        }
    }

    private synchronized void b() {
        boolean z = (this.f == null || this.f.getState() == State.TERMINATED) ? false : true;
        if (!this.g && !this.b.d() && !z) {
            a aVar = new a();
            StringBuilder sb = new StringBuilder();
            sb.append("Source reader for ");
            sb.append(this.a);
            this.f = new Thread(aVar, sb.toString());
            this.f.start();
        }
    }

    private void b(long j, long j2) {
        a(j, j2);
        synchronized (this.c) {
            this.c.notifyAll();
        }
    }

    private boolean c() {
        return Thread.currentThread().isInterrupted() || this.g;
    }

    private void d() {
        try {
            this.a.b();
        } catch (l | IllegalArgumentException e2) {
            StringBuilder sb = new StringBuilder();
            sb.append("Error closing source ");
            sb.append(this.a);
            a((Throwable) new l(sb.toString(), e2));
        }
    }

    public int a(byte[] bArr, long j, int i) {
        String str = "Buffer must be not null!";
        if (bArr == null) {
            throw new NullPointerException(str);
        }
        j.a(j >= 0, "Data offset must be positive!");
        j.a(i >= 0 && i <= bArr.length, "Length must be in range [0..buffer.length]");
        while (!this.b.d() && ((long) this.b.a()) < ((long) i) + j && !this.g) {
            b();
            synchronized (this.c) {
                try {
                    this.c.wait(1000);
                } catch (InterruptedException e2) {
                    throw new l("Waiting source data is interrupted!", e2);
                }
            }
            int i2 = this.e.get();
            if (i2 >= 1) {
                this.e.set(0);
                StringBuilder sb = new StringBuilder();
                sb.append("Error reading source ");
                sb.append(i2);
                sb.append(" times");
                throw new l(sb.toString());
            }
        }
        int a2 = this.b.a(bArr, j, i);
        if (this.b.d() && this.h != 100) {
            this.h = 100;
            a(100);
        }
        return a2;
    }

    public void a() {
        synchronized (this.d) {
            String str = "ProxyCache";
            StringBuilder sb = new StringBuilder();
            sb.append("Shutdown proxy for ");
            sb.append(this.a);
            Log.d(str, sb.toString());
            try {
                this.g = true;
                if (this.f != null) {
                    this.f.interrupt();
                }
                this.b.b();
            } catch (l e2) {
                a((Throwable) e2);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a(int i) {
    }

    /* access modifiers changed from: protected */
    public void a(long j, long j2) {
        int i = (j2 > 0 ? 1 : (j2 == 0 ? 0 : -1));
        boolean z = false;
        int i2 = i == 0 ? 100 : (int) ((j * 100) / j2);
        boolean z2 = i2 != this.h;
        if (i >= 0) {
            z = true;
        }
        if (z && z2) {
            a(i2);
        }
        this.h = i2;
    }

    /* access modifiers changed from: protected */
    public final void a(Throwable th) {
        if (th instanceof i) {
            Log.d("ProxyCache", "ProxyCache is interrupted");
        } else {
            Log.e("ProxyCache", "ProxyCache error", th);
        }
    }
}
