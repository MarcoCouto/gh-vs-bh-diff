package com.facebook.ads.internal.n;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build.VERSION;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import android.text.TextUtils;
import android.util.Base64OutputStream;
import android.webkit.WebSettings;
import android.webkit.WebView;
import com.facebook.ads.internal.l.b;
import com.facebook.ads.internal.settings.AdInternalSettings;
import com.facebook.ads.internal.v.a.a.C0011a;
import com.facebook.ads.internal.w.b.f;
import com.facebook.ads.internal.w.b.f.a;
import com.facebook.ads.internal.w.b.h;
import com.facebook.ads.internal.w.b.i;
import com.facebook.ads.internal.w.b.k;
import com.facebook.ads.internal.w.b.o;
import com.facebook.ads.internal.w.b.u;
import com.facebook.ads.internal.w.b.v;
import com.facebook.ads.internal.w.b.x;
import com.facebook.internal.AnalyticsEvents;
import com.facebook.share.internal.MessengerShareContentUtility;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.zip.DeflaterOutputStream;
import org.json.JSONObject;

public class d {
    private static final AtomicBoolean a = new AtomicBoolean();
    /* access modifiers changed from: private */
    public static final AtomicInteger b = new AtomicInteger(0);
    /* access modifiers changed from: private */
    public static String c;
    private static final a d = f.a();
    @Nullable
    private static String e;
    private final Context f;
    private final b g;

    public d(Context context, boolean z) {
        this.f = context;
        this.g = new b(context);
        a(context, z);
    }

    private static String a(Context context, String str, String str2) {
        Class cls = Class.forName(str);
        Constructor declaredConstructor = cls.getDeclaredConstructor(new Class[]{Context.class, Class.forName(str2)});
        declaredConstructor.setAccessible(true);
        try {
            return (String) cls.getMethod("getUserAgentString", new Class[0]).invoke(declaredConstructor.newInstance(new Object[]{context, null}), new Object[0]);
        } finally {
            declaredConstructor.setAccessible(false);
        }
    }

    public static String a(b bVar, Context context, boolean z) {
        StringBuilder sb = new StringBuilder();
        sb.append(b(context, z));
        sb.append(" [FBAN/AudienceNetworkForAndroid;FBSN/");
        sb.append("Android");
        sb.append(";FBSV/");
        sb.append(b.a);
        sb.append(";FBAB/");
        sb.append(bVar.f());
        sb.append(";FBAV/");
        sb.append(bVar.g());
        sb.append(";FBBV/");
        sb.append(bVar.h());
        sb.append(";FBVS/");
        sb.append("5.1.0");
        sb.append(";FBLC/");
        sb.append(Locale.getDefault().toString());
        sb.append("]");
        return sb.toString();
    }

    @WorkerThread
    public static void a(Context context) {
        if (context != null) {
            final Context applicationContext = context.getApplicationContext();
            com.facebook.ads.internal.v.a.a.a((C0011a) new C0011a() {
                public Map<String, String> a() {
                    HashMap hashMap = new HashMap();
                    if (!com.facebook.ads.internal.g.b.c) {
                        hashMap.put("X-FB-Pool-Routing-Token", new d(applicationContext, true).a());
                    }
                    return hashMap;
                }
            });
        }
    }

    private static void a(final Context context, boolean z) {
        if (b.compareAndSet(0, 1)) {
            try {
                o.a();
                final SharedPreferences sharedPreferences = context.getSharedPreferences(com.facebook.ads.internal.w.f.a.a("FBAdPrefs", context), 0);
                b bVar = new b(context);
                StringBuilder sb = new StringBuilder();
                sb.append("AFP;");
                sb.append(bVar.g());
                final String sb2 = sb.toString();
                c = sharedPreferences.getString(sb2, null);
                FutureTask futureTask = new FutureTask(new Callable<Boolean>() {
                    /* renamed from: a */
                    public Boolean call() {
                        d.c = d.b(context, context.getPackageName());
                        sharedPreferences.edit().putString(sb2, d.c).apply();
                        d.b.set(2);
                        return Boolean.valueOf(true);
                    }
                });
                Executors.newSingleThreadExecutor().submit(futureTask);
                if (z) {
                    futureTask.get();
                }
            } catch (Exception unused) {
                b.set(0);
            }
        }
    }

    /* access modifiers changed from: private */
    @Nullable
    public static String b(Context context, String str) {
        try {
            return i.a(new File(context.getPackageManager().getApplicationInfo(str, 0).sourceDir));
        } catch (Exception e2) {
            if (a.compareAndSet(false, true)) {
                com.facebook.ads.internal.w.h.a.b(context.getApplicationContext(), MessengerShareContentUtility.TEMPLATE_GENERIC_TYPE, com.facebook.ads.internal.w.h.b.A, e2);
            }
            return null;
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(6:31|30|32|33|34|35) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:34:0x0046 */
    @Nullable
    private static String b(Context context, boolean z) {
        if (context == null) {
            return AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN;
        }
        if (z) {
            return System.getProperty("http.agent");
        }
        if (e != null) {
            return e;
        }
        synchronized (d.class) {
            if (e != null) {
                String str = e;
                return str;
            }
            if (VERSION.SDK_INT >= 17) {
                try {
                    e = WebSettings.getDefaultUserAgent(context);
                    String str2 = e;
                    return str2;
                } catch (Exception unused) {
                }
            }
            try {
                e = a(context, "android.webkit.WebSettings", "android.webkit.WebView");
            } catch (Exception unused2) {
                e = a(context, "android.webkit.WebSettingsClassic", "android.webkit.WebViewClassic");
                WebView webView = new WebView(context.getApplicationContext());
                e = webView.getSettings().getUserAgentString();
                webView.destroy();
            }
        }
        return e;
    }

    public static Map<String, String> b(Context context) {
        try {
            return new d(context, false).b();
        } catch (Throwable th) {
            com.facebook.ads.internal.w.h.a.a(th);
            return new HashMap();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:39:0x009a A[SYNTHETIC, Splitter:B:39:0x009a] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x009f A[Catch:{ IOException -> 0x00a7 }] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00a4 A[Catch:{ IOException -> 0x00a7 }] */
    @WorkerThread
    public String a() {
        DeflaterOutputStream deflaterOutputStream;
        Base64OutputStream base64OutputStream;
        ByteArrayOutputStream byteArrayOutputStream;
        IOException e2;
        a(this.f, true);
        try {
            byteArrayOutputStream = new ByteArrayOutputStream();
            try {
                base64OutputStream = new Base64OutputStream(byteArrayOutputStream, 0);
            } catch (IOException e3) {
                deflaterOutputStream = null;
                e2 = e3;
                base64OutputStream = null;
                try {
                    throw new RuntimeException("Failed to build user token", e2);
                } catch (Throwable th) {
                    th = th;
                    if (deflaterOutputStream != null) {
                        try {
                            deflaterOutputStream.close();
                        } catch (IOException unused) {
                            throw th;
                        }
                    }
                    if (base64OutputStream != null) {
                        base64OutputStream.close();
                    }
                    if (byteArrayOutputStream != null) {
                        byteArrayOutputStream.close();
                    }
                    throw th;
                }
            } catch (Throwable th2) {
                deflaterOutputStream = null;
                th = th2;
                base64OutputStream = null;
                if (deflaterOutputStream != null) {
                }
                if (base64OutputStream != null) {
                }
                if (byteArrayOutputStream != null) {
                }
                throw th;
            }
            try {
                deflaterOutputStream = new DeflaterOutputStream(base64OutputStream);
                try {
                    Map b2 = b();
                    if (TextUtils.isEmpty(com.facebook.ads.internal.g.b.b)) {
                        com.facebook.ads.internal.g.b.a(this.f);
                        a(this.f);
                    }
                    b2.put("IDFA", com.facebook.ads.internal.g.b.b);
                    b2.put("USER_AGENT", a(this.g, this.f, false));
                    deflaterOutputStream.write(new JSONObject(b2).toString().getBytes());
                    deflaterOutputStream.close();
                    String replaceAll = byteArrayOutputStream.toString().replaceAll("\n", "");
                    if (deflaterOutputStream != null) {
                        try {
                            deflaterOutputStream.close();
                        } catch (IOException unused2) {
                        }
                    }
                    if (base64OutputStream != null) {
                        base64OutputStream.close();
                    }
                    if (byteArrayOutputStream != null) {
                        byteArrayOutputStream.close();
                    }
                    return replaceAll;
                } catch (IOException e4) {
                    e2 = e4;
                    throw new RuntimeException("Failed to build user token", e2);
                }
            } catch (IOException e5) {
                deflaterOutputStream = null;
                e2 = e5;
                throw new RuntimeException("Failed to build user token", e2);
            } catch (Throwable th3) {
                deflaterOutputStream = null;
                th = th3;
                if (deflaterOutputStream != null) {
                }
                if (base64OutputStream != null) {
                }
                if (byteArrayOutputStream != null) {
                }
                throw th;
            }
        } catch (IOException e6) {
            base64OutputStream = null;
            deflaterOutputStream = null;
            e2 = e6;
            byteArrayOutputStream = null;
            throw new RuntimeException("Failed to build user token", e2);
        } catch (Throwable th4) {
            base64OutputStream = null;
            deflaterOutputStream = null;
            th = th4;
            byteArrayOutputStream = null;
            if (deflaterOutputStream != null) {
            }
            if (base64OutputStream != null) {
            }
            if (byteArrayOutputStream != null) {
            }
            throw th;
        }
    }

    public Map<String, String> b() {
        a(this.f, false);
        com.facebook.ads.internal.l.a.a(this.f);
        HashMap hashMap = new HashMap();
        hashMap.put("SDK", "android");
        hashMap.put("SDK_VERSION", "5.1.0");
        hashMap.put("LOCALE", Locale.getDefault().toString());
        float f2 = x.b;
        int i = this.f.getResources().getDisplayMetrics().widthPixels;
        int i2 = this.f.getResources().getDisplayMetrics().heightPixels;
        hashMap.put("DENSITY", String.valueOf(f2));
        hashMap.put("SCREEN_WIDTH", String.valueOf((int) (((float) i) / f2)));
        hashMap.put("SCREEN_HEIGHT", String.valueOf((int) (((float) i2) / f2)));
        hashMap.put("ATTRIBUTION_ID", com.facebook.ads.internal.g.b.a);
        hashMap.put("ID_SOURCE", com.facebook.ads.internal.g.b.d);
        hashMap.put("OS", "Android");
        hashMap.put("OSVERS", b.a);
        hashMap.put("BUNDLE", this.g.f());
        hashMap.put("APPNAME", this.g.d());
        hashMap.put("APPVERS", this.g.g());
        hashMap.put("APPBUILD", String.valueOf(this.g.h()));
        hashMap.put("CARRIER", this.g.c());
        hashMap.put("MAKE", this.g.a());
        hashMap.put("MODEL", this.g.b());
        hashMap.put("ROOTED", String.valueOf(d.d));
        hashMap.put("INSTALLER", this.g.e());
        hashMap.put("SDK_CAPABILITY", com.facebook.ads.internal.w.b.b.b());
        hashMap.put("NETWORK_TYPE", String.valueOf(u.a(this.f).g));
        hashMap.put("SESSION_TIME", v.a(o.b()));
        hashMap.put("SESSION_ID", o.c());
        if (c != null) {
            hashMap.put("AFP", c);
        }
        String a2 = f.a(this.f);
        if (a2 != null) {
            hashMap.put("ASHAS", a2);
        }
        hashMap.put("UNITY", String.valueOf(h.a(this.f)));
        String mediationService = AdInternalSettings.getMediationService();
        if (mediationService != null) {
            hashMap.put("MEDIATION_SERVICE", mediationService);
        }
        hashMap.put("ACCESSIBILITY_ENABLED", String.valueOf(this.g.i()));
        if (this.g.j() != -1) {
            hashMap.put("APP_MIN_SDK_VERSION", String.valueOf(this.g.j()));
        }
        hashMap.put("VALPARAMS", b.a(this.f));
        hashMap.put("ANALOG", k.a(com.facebook.ads.internal.l.a.a()));
        hashMap.put("PROCESS", c.a(this.f));
        return hashMap;
    }
}
