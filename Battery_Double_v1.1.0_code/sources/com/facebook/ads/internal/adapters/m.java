package com.facebook.ads.internal.adapters;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import com.facebook.ads.internal.s.c;
import com.facebook.ads.internal.w.e.a;
import java.util.Map;

public class m extends b {
    /* access modifiers changed from: private */
    public static final String c = "m";
    /* access modifiers changed from: private */
    public final a d;
    private final c e;
    /* access modifiers changed from: private */
    public l f;
    private boolean g;

    public m(Context context, c cVar, a aVar, com.facebook.ads.internal.x.a aVar2, c cVar2) {
        super(context, cVar2, aVar2);
        this.e = cVar;
        this.d = aVar;
    }

    public void a(l lVar) {
        this.f = lVar;
    }

    /* access modifiers changed from: protected */
    public void a(Map<String, String> map) {
        if (this.f != null && !TextUtils.isEmpty(this.f.getClientToken())) {
            this.e.a(this.f.getClientToken(), map);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0028, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002a, code lost:
        return;
     */
    public synchronized void b() {
        if (!this.g) {
            if (this.f != null) {
                this.g = true;
                if (this.d != null && !TextUtils.isEmpty(this.f.d())) {
                    this.d.post(new Runnable() {
                        public void run() {
                            if (m.this.d.c()) {
                                Log.w(m.c, "Webview already destroyed, cannot activate");
                                return;
                            }
                            a a2 = m.this.d;
                            StringBuilder sb = new StringBuilder();
                            sb.append("javascript:");
                            sb.append(m.this.f.d());
                            a2.loadUrl(sb.toString());
                        }
                    });
                }
            }
        }
    }
}
