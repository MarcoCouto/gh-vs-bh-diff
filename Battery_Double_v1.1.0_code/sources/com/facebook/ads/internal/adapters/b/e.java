package com.facebook.ads.internal.adapters.b;

import java.io.Serializable;

public class e implements Serializable {
    private static final long serialVersionUID = 5306126965868117466L;
    private final String a;
    private final String b;
    private final String c;
    private final String d;
    private final String e;
    private final String f;
    private final a g;

    public enum a {
        CONTEXTUAL_APP("contextual_app"),
        PAGE_POST("page_post");
        
        private final String c;

        private a(String str) {
            this.c = str;
        }

        /* JADX WARNING: Removed duplicated region for block: B:12:0x0026  */
        /* JADX WARNING: Removed duplicated region for block: B:14:0x0029  */
        public static a a(String str) {
            char c2;
            int hashCode = str.hashCode();
            if (hashCode != 883765328) {
                if (hashCode == 1434358835 && str.equals("contextual_app")) {
                    c2 = 0;
                    return c2 != 0 ? PAGE_POST : CONTEXTUAL_APP;
                }
            } else if (str.equals("page_post")) {
                c2 = 1;
                if (c2 != 0) {
                }
            }
            c2 = 65535;
            if (c2 != 0) {
            }
        }
    }

    public static class b {
        /* access modifiers changed from: private */
        public String a;
        /* access modifiers changed from: private */
        public String b;
        /* access modifiers changed from: private */
        public String c;
        /* access modifiers changed from: private */
        public String d;
        /* access modifiers changed from: private */
        public String e;
        /* access modifiers changed from: private */
        public String f;
        /* access modifiers changed from: private */
        public a g;

        /* access modifiers changed from: 0000 */
        public b a(String str) {
            this.a = str;
            return this;
        }

        /* access modifiers changed from: 0000 */
        public e a() {
            return new e(this);
        }

        /* access modifiers changed from: 0000 */
        public b b(String str) {
            this.b = str;
            return this;
        }

        /* access modifiers changed from: 0000 */
        public b c(String str) {
            this.c = str;
            return this;
        }

        /* access modifiers changed from: 0000 */
        public b d(String str) {
            this.d = str;
            return this;
        }

        /* access modifiers changed from: 0000 */
        public b e(String str) {
            this.e = str;
            return this;
        }

        /* access modifiers changed from: 0000 */
        public b f(String str) {
            this.f = str;
            return this;
        }

        /* access modifiers changed from: 0000 */
        public b g(String str) {
            this.g = a.a(str);
            return this;
        }
    }

    private e(b bVar) {
        this.a = bVar.a;
        this.b = bVar.b;
        this.c = bVar.c;
        this.d = bVar.d;
        this.e = bVar.e;
        this.f = bVar.f;
        this.g = bVar.g;
    }

    public String a() {
        return this.a;
    }

    public String b() {
        return this.b;
    }

    public String c() {
        return this.c;
    }

    public String d() {
        return this.d;
    }

    public String e() {
        return this.e;
    }

    public a f() {
        return this.g;
    }

    public String g() {
        return this.f;
    }
}
