package com.facebook.ads.internal.adapters.b;

import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import java.io.Serializable;
import org.json.JSONObject;

public class c implements Serializable {
    private static final long serialVersionUID = -1165646029762217510L;
    private final int a;
    private final int b;
    private final int c;
    private final boolean d;

    private c(int i, int i2, int i3, boolean z) {
        this.a = i;
        this.b = i2;
        this.c = i3;
        this.d = z;
    }

    public static c a(JSONObject jSONObject) {
        return new c(jSONObject.optInt("countdown_time_ms", 6000), jSONObject.optInt("pulse_animation_duration_ms", SettingsJsonConstants.ANALYTICS_FLUSH_INTERVAL_SECS_DEFAULT), jSONObject.optInt("default_ad_index"), jSONObject.optBoolean("should_show_rating", false));
    }

    public int a() {
        return this.a;
    }

    public int b() {
        return this.c;
    }

    public int c() {
        return this.b;
    }

    public boolean d() {
        return this.d;
    }
}
