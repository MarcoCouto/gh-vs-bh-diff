package com.facebook.ads.internal.adapters;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.GradientDrawable.Orientation;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewCompat;
import android.text.TextUtils;
import android.text.TextUtils.TruncateAt;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import com.facebook.ads.AudienceNetworkActivity;
import com.facebook.ads.AudienceNetworkActivity.BackButtonInterceptor;
import com.facebook.ads.internal.a.c;
import com.facebook.ads.internal.view.a;
import com.facebook.ads.internal.view.a.C0012a;
import com.facebook.ads.internal.view.c.f;
import com.facebook.ads.internal.view.i.a.b;
import com.facebook.ads.internal.view.i.b.u;
import com.facebook.ads.internal.view.i.c.a.C0020a;
import com.facebook.ads.internal.view.i.c.d;
import com.facebook.ads.internal.view.i.c.g;
import com.facebook.ads.internal.view.i.c.j;
import com.facebook.ads.internal.view.i.c.k;
import com.facebook.ads.internal.view.i.c.l;
import com.facebook.ads.internal.view.i.c.n;
import com.facebook.ads.internal.w.b.w;
import com.facebook.ads.internal.w.b.x;
import com.facebook.appevents.UserDataStore;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.google.android.exoplayer2.util.MimeTypes;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import java.util.HashMap;
import org.json.JSONObject;

public class h extends f implements OnTouchListener, a {
    static final /* synthetic */ boolean i = true;
    private static final String j = "h";
    private int A = -10525069;
    private int B = -12286980;
    private boolean C = false;
    @Nullable
    private com.facebook.ads.internal.view.i.a.a D;
    final int f = 64;
    final int g = 64;
    final int h = 16;
    @Nullable
    private C0012a k;
    /* access modifiers changed from: private */
    @Nullable
    public Activity l;
    private BackButtonInterceptor m = new BackButtonInterceptor() {
        public boolean interceptBackButton() {
            if (h.this.y == null) {
                return false;
            }
            if (!h.this.y.a()) {
                return true;
            }
            if (!(h.this.y.getSkipSeconds() == 0 || h.this.b == null)) {
                h.this.b.f();
            }
            if (h.this.b != null) {
                h.this.b.g();
            }
            return false;
        }
    };
    private final OnTouchListener n = new OnTouchListener() {
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (motionEvent.getAction() != 1) {
                return true;
            }
            if (h.this.y != null) {
                if (!h.this.y.a()) {
                    return true;
                }
                if (!(h.this.y.getSkipSeconds() == 0 || h.this.b == null)) {
                    h.this.b.f();
                }
                if (h.this.b != null) {
                    h.this.b.g();
                }
            }
            h.this.l.finish();
            return true;
        }
    };
    private f o = f.UNSPECIFIED;
    private final w p = new w();
    private com.facebook.ads.internal.view.e.a q;
    private TextView r;
    private TextView s;
    private ImageView t;
    @Nullable
    private C0020a u;
    private n v;
    private ViewGroup w;
    private d x;
    /* access modifiers changed from: private */
    public j y;
    private int z = -1;

    /* JADX WARNING: Removed duplicated region for block: B:136:0x060f  */
    /* JADX WARNING: Removed duplicated region for block: B:138:? A[RETURN, SYNTHETIC] */
    private void a(int i2) {
        View view;
        int i3;
        View rootView;
        int i4;
        int i5;
        int i6;
        int i7;
        View view2;
        int i8 = i2;
        float f2 = x.b;
        int i9 = (int) (56.0f * f2);
        LayoutParams layoutParams = new LayoutParams(i9, i9);
        layoutParams.addRule(10);
        layoutParams.addRule(11);
        int i10 = (int) (16.0f * f2);
        this.y.setPadding(i10, i10, i10, i10);
        this.y.setLayoutParams(layoutParams);
        d.a aVar = h() ? d.a.FADE_OUT_ON_PLAY : d.a.VISIBLE;
        int id = this.b.getId();
        if (i8 == 1) {
            if ((((double) (this.b.getVideoHeight() > 0 ? ((float) this.b.getVideoWidth()) / ((float) this.b.getVideoHeight()) : -1.0f)) <= 0.9d) || k()) {
                GradientDrawable gradientDrawable = new GradientDrawable(Orientation.TOP_BOTTOM, new int[]{0, -15658735});
                gradientDrawable.setCornerRadius(0.0f);
                LayoutParams layoutParams2 = new LayoutParams(-1, -2);
                layoutParams2.addRule(10);
                this.b.setLayoutParams(layoutParams2);
                a((View) this.b);
                a((View) this.y);
                if (this.u != null) {
                    a((View) this.u);
                }
                LayoutParams layoutParams3 = new LayoutParams(-1, (int) (((float) ((this.q != null ? 64 : 0) + 60 + 16 + 16 + 16)) * f2));
                layoutParams3.addRule(12);
                RelativeLayout relativeLayout = new RelativeLayout(this.d);
                x.a((View) relativeLayout, (Drawable) gradientDrawable);
                relativeLayout.setLayoutParams(layoutParams3);
                relativeLayout.setPadding(i10, 0, i10, (int) (((float) ((this.q != null ? 64 : 0) + 16 + 16)) * f2));
                this.w = relativeLayout;
                if (!this.C) {
                    this.x.a((View) relativeLayout, aVar);
                }
                a((View) relativeLayout);
                if (this.v != null) {
                    LayoutParams layoutParams4 = new LayoutParams(-1, (int) (6.0f * f2));
                    layoutParams4.addRule(12);
                    layoutParams4.topMargin = (int) (-6.0f * f2);
                    this.v.setLayoutParams(layoutParams4);
                    a((View) this.v);
                }
                if (this.q != null) {
                    LayoutParams layoutParams5 = new LayoutParams(-1, (int) (64.0f * f2));
                    layoutParams5.bottomMargin = i10;
                    layoutParams5.leftMargin = i10;
                    layoutParams5.rightMargin = i10;
                    layoutParams5.addRule(1);
                    layoutParams5.addRule(12);
                    this.q.setLayoutParams(layoutParams5);
                    a((View) this.q);
                }
                if (this.t != null) {
                    int i11 = (int) (60.0f * f2);
                    LayoutParams layoutParams6 = new LayoutParams(i11, i11);
                    layoutParams6.addRule(12);
                    layoutParams6.addRule(9);
                    this.t.setLayoutParams(layoutParams6);
                    a(relativeLayout, this.t);
                }
                if (this.r != null) {
                    LayoutParams layoutParams7 = new LayoutParams(-1, -2);
                    layoutParams7.bottomMargin = (int) (36.0f * f2);
                    layoutParams7.addRule(12);
                    layoutParams7.addRule(9);
                    this.r.setEllipsize(TruncateAt.END);
                    this.r.setGravity(GravityCompat.START);
                    this.r.setLayoutParams(layoutParams7);
                    this.r.setMaxLines(1);
                    this.r.setPadding((int) (72.0f * f2), 0, 0, 0);
                    this.r.setTextColor(-1);
                    this.r.setTextSize(18.0f);
                    a(relativeLayout, this.r);
                }
                if (this.s != null) {
                    LayoutParams layoutParams8 = new LayoutParams(-1, -2);
                    layoutParams8.addRule(12);
                    layoutParams8.addRule(9);
                    layoutParams8.bottomMargin = (int) (4.0f * f2);
                    this.s.setEllipsize(TruncateAt.END);
                    this.s.setGravity(GravityCompat.START);
                    this.s.setLayoutParams(layoutParams8);
                    this.s.setMaxLines(1);
                    this.s.setPadding((int) (72.0f * f2), 0, 0, 0);
                    this.s.setTextColor(-1);
                    a(relativeLayout, this.s);
                }
                view = (View) this.b.getParent();
                i3 = ViewCompat.MEASURED_STATE_MASK;
                x.a(view, i3);
                rootView = this.b.getRootView();
                if (rootView != null) {
                    rootView.setOnTouchListener(this);
                    return;
                }
                return;
            }
        }
        if (i8 == 1) {
            LayoutParams layoutParams9 = new LayoutParams(-1, -2);
            layoutParams9.addRule(10);
            this.b.setLayoutParams(layoutParams9);
            a((View) this.b);
            a((View) this.y);
            if (this.u != null) {
                a((View) this.u);
            }
            LinearLayout linearLayout = new LinearLayout(this.d);
            this.w = linearLayout;
            linearLayout.setGravity(112);
            linearLayout.setOrientation(1);
            LayoutParams layoutParams10 = new LayoutParams(-1, -1);
            int i12 = (int) (33.0f * f2);
            layoutParams10.leftMargin = i12;
            layoutParams10.rightMargin = i12;
            layoutParams10.topMargin = (int) (8.0f * f2);
            if (this.q == null) {
                layoutParams10.bottomMargin = i10;
            } else {
                layoutParams10.bottomMargin = (int) (80.0f * f2);
            }
            layoutParams10.addRule(3, id);
            linearLayout.setLayoutParams(layoutParams10);
            a((View) linearLayout);
            if (this.q != null) {
                LayoutParams layoutParams11 = new LayoutParams(-1, (int) (64.0f * f2));
                layoutParams11.bottomMargin = i10;
                layoutParams11.leftMargin = i12;
                layoutParams11.rightMargin = i12;
                layoutParams11.addRule(1);
                layoutParams11.addRule(12);
                this.q.setLayoutParams(layoutParams11);
                a((View) this.q);
            }
            if (this.r != null) {
                LinearLayout.LayoutParams layoutParams12 = new LinearLayout.LayoutParams(-2, -2);
                layoutParams12.weight = 2.0f;
                layoutParams12.gravity = 17;
                this.r.setEllipsize(TruncateAt.END);
                this.r.setGravity(17);
                this.r.setLayoutParams(layoutParams12);
                this.r.setMaxLines(2);
                this.r.setPadding(0, 0, 0, 0);
                this.r.setTextColor(this.A);
                this.r.setTextSize(24.0f);
                a(linearLayout, this.r);
            }
            if (this.t != null) {
                int i13 = (int) (64.0f * f2);
                LinearLayout.LayoutParams layoutParams13 = new LinearLayout.LayoutParams(i13, i13);
                layoutParams13.weight = 0.0f;
                layoutParams13.gravity = 17;
                this.t.setLayoutParams(layoutParams13);
                a(linearLayout, this.t);
            }
            if (this.s != null) {
                LinearLayout.LayoutParams layoutParams14 = new LinearLayout.LayoutParams(-1, -2);
                layoutParams14.weight = 2.0f;
                layoutParams14.gravity = 16;
                this.s.setEllipsize(TruncateAt.END);
                this.s.setGravity(16);
                this.s.setLayoutParams(layoutParams14);
                this.s.setMaxLines(2);
                this.s.setPadding(0, 0, 0, 0);
                this.s.setTextColor(this.A);
                a(linearLayout, this.s);
            }
            if (this.v != null) {
                LayoutParams layoutParams15 = new LayoutParams(-1, (int) (6.0f * f2));
                layoutParams15.addRule(3, id);
                this.v.setLayoutParams(layoutParams15);
                view2 = this.v;
            }
            view = (View) this.b.getParent();
            i3 = this.z;
            x.a(view, i3);
            rootView = this.b.getRootView();
            if (rootView != null) {
            }
        } else {
            double videoWidth = (double) (this.b.getVideoHeight() > 0 ? ((float) this.b.getVideoWidth()) / ((float) this.b.getVideoHeight()) : -1.0f);
            if (!(videoWidth > 0.9d && videoWidth < 1.1d) || k()) {
                GradientDrawable gradientDrawable2 = new GradientDrawable(Orientation.TOP_BOTTOM, new int[]{0, -15658735});
                gradientDrawable2.setCornerRadius(0.0f);
                this.b.setLayoutParams(new LayoutParams(-1, -1));
                a((View) this.b);
                a((View) this.y);
                if (this.u != null) {
                    a((View) this.u);
                }
                LayoutParams layoutParams16 = new LayoutParams(-1, (int) (124.0f * f2));
                layoutParams16.addRule(12);
                RelativeLayout relativeLayout2 = new RelativeLayout(this.d);
                x.a((View) relativeLayout2, (Drawable) gradientDrawable2);
                relativeLayout2.setLayoutParams(layoutParams16);
                relativeLayout2.setPadding(i10, 0, i10, i10);
                this.w = relativeLayout2;
                if (!this.C) {
                    this.x.a((View) relativeLayout2, aVar);
                }
                a((View) relativeLayout2);
                if (this.q != null) {
                    LayoutParams layoutParams17 = new LayoutParams((int) (110.0f * f2), i9);
                    layoutParams17.rightMargin = i10;
                    layoutParams17.bottomMargin = i10;
                    layoutParams17.addRule(12);
                    layoutParams17.addRule(11);
                    this.q.setLayoutParams(layoutParams17);
                    a((View) this.q);
                }
                if (this.t != null) {
                    int i14 = (int) (64.0f * f2);
                    LayoutParams layoutParams18 = new LayoutParams(i14, i14);
                    layoutParams18.addRule(12);
                    layoutParams18.addRule(9);
                    layoutParams18.bottomMargin = (int) (8.0f * f2);
                    this.t.setLayoutParams(layoutParams18);
                    a(relativeLayout2, this.t);
                }
                if (this.r != null) {
                    LayoutParams layoutParams19 = new LayoutParams(-1, -2);
                    layoutParams19.bottomMargin = (int) (48.0f * f2);
                    layoutParams19.addRule(12);
                    layoutParams19.addRule(9);
                    this.r.setEllipsize(TruncateAt.END);
                    this.r.setGravity(GravityCompat.START);
                    this.r.setLayoutParams(layoutParams19);
                    this.r.setMaxLines(1);
                    TextView textView = this.r;
                    int i15 = (int) (80.0f * f2);
                    if (this.q != null) {
                        i6 = (int) (126.0f * f2);
                        i7 = 0;
                    } else {
                        i7 = 0;
                        i6 = 0;
                    }
                    textView.setPadding(i15, i7, i6, i7);
                    this.r.setTextColor(-1);
                    this.r.setTextSize(24.0f);
                    a(relativeLayout2, this.r);
                }
                if (this.s != null) {
                    LayoutParams layoutParams20 = new LayoutParams(-1, -2);
                    layoutParams20.addRule(12);
                    layoutParams20.addRule(9);
                    this.s.setEllipsize(TruncateAt.END);
                    this.s.setGravity(GravityCompat.START);
                    this.s.setLayoutParams(layoutParams20);
                    this.s.setMaxLines(2);
                    this.s.setTextColor(-1);
                    TextView textView2 = this.s;
                    int i16 = (int) (80.0f * f2);
                    if (this.q != null) {
                        i4 = (int) (126.0f * f2);
                        i5 = 0;
                    } else {
                        i5 = 0;
                        i4 = 0;
                    }
                    textView2.setPadding(i16, i5, i4, i5);
                    a(relativeLayout2, this.s);
                }
                if (this.v != null) {
                    LayoutParams layoutParams21 = new LayoutParams(-1, (int) (6.0f * f2));
                    layoutParams21.addRule(12);
                    this.v.setLayoutParams(layoutParams21);
                    a((View) this.v);
                }
                view = (View) this.b.getParent();
                i3 = ViewCompat.MEASURED_STATE_MASK;
                x.a(view, i3);
                rootView = this.b.getRootView();
                if (rootView != null) {
                }
            } else {
                LayoutParams layoutParams22 = new LayoutParams(-2, -1);
                layoutParams22.addRule(9);
                this.b.setLayoutParams(layoutParams22);
                a((View) this.b);
                a((View) this.y);
                if (this.u != null) {
                    a((View) this.u);
                }
                LinearLayout linearLayout2 = new LinearLayout(this.d);
                this.w = linearLayout2;
                linearLayout2.setGravity(112);
                linearLayout2.setOrientation(1);
                LayoutParams layoutParams23 = new LayoutParams(-1, -1);
                layoutParams23.leftMargin = i10;
                layoutParams23.rightMargin = i10;
                layoutParams23.topMargin = (int) (8.0f * f2);
                layoutParams23.bottomMargin = (int) (80.0f * f2);
                layoutParams23.addRule(1, id);
                linearLayout2.setLayoutParams(layoutParams23);
                a((View) linearLayout2);
                if (this.v != null) {
                    LayoutParams layoutParams24 = new LayoutParams(-1, (int) (6.0f * f2));
                    layoutParams24.addRule(5, id);
                    layoutParams24.addRule(7, id);
                    layoutParams24.addRule(3, id);
                    layoutParams24.topMargin = (int) (-6.0f * f2);
                    this.v.setLayoutParams(layoutParams24);
                    a((View) this.v);
                }
                if (this.r != null) {
                    LinearLayout.LayoutParams layoutParams25 = new LinearLayout.LayoutParams(-2, -2);
                    layoutParams25.weight = 2.0f;
                    layoutParams25.gravity = 17;
                    this.r.setEllipsize(TruncateAt.END);
                    this.r.setGravity(17);
                    this.r.setLayoutParams(layoutParams25);
                    this.r.setMaxLines(10);
                    this.r.setPadding(0, 0, 0, 0);
                    this.r.setTextColor(this.A);
                    this.r.setTextSize(24.0f);
                    a(linearLayout2, this.r);
                }
                if (this.t != null) {
                    int i17 = (int) (64.0f * f2);
                    LinearLayout.LayoutParams layoutParams26 = new LinearLayout.LayoutParams(i17, i17);
                    layoutParams26.weight = 0.0f;
                    layoutParams26.gravity = 17;
                    this.t.setLayoutParams(layoutParams26);
                    a(linearLayout2, this.t);
                }
                if (this.s != null) {
                    LinearLayout.LayoutParams layoutParams27 = new LinearLayout.LayoutParams(-1, -2);
                    layoutParams27.weight = 2.0f;
                    layoutParams27.gravity = 16;
                    this.s.setEllipsize(TruncateAt.END);
                    this.s.setGravity(17);
                    this.s.setLayoutParams(layoutParams27);
                    this.s.setMaxLines(10);
                    this.s.setPadding(0, 0, 0, 0);
                    this.s.setTextColor(this.A);
                    a(linearLayout2, this.s);
                }
                if (this.q != null) {
                    LayoutParams layoutParams28 = new LayoutParams(-1, (int) (f2 * 64.0f));
                    layoutParams28.bottomMargin = i10;
                    layoutParams28.leftMargin = i10;
                    layoutParams28.rightMargin = i10;
                    layoutParams28.addRule(1);
                    layoutParams28.addRule(12);
                    layoutParams28.addRule(1, id);
                    this.q.setLayoutParams(layoutParams28);
                    view2 = this.q;
                }
                view = (View) this.b.getParent();
                i3 = this.z;
                x.a(view, i3);
                rootView = this.b.getRootView();
                if (rootView != null) {
                }
            }
        }
        a(view2);
        view = (View) this.b.getParent();
        i3 = this.z;
        x.a(view, i3);
        rootView = this.b.getRootView();
        if (rootView != null) {
        }
    }

    private void a(View view) {
        if (this.k != null) {
            this.k.a(view);
        }
    }

    private void a(@Nullable ViewGroup viewGroup, @Nullable View view) {
        if (viewGroup != null) {
            viewGroup.addView(view);
        }
    }

    private void b(View view) {
        if (view != null) {
            ViewGroup viewGroup = (ViewGroup) view.getParent();
            if (viewGroup != null) {
                viewGroup.removeView(view);
            }
        }
    }

    private boolean k() {
        boolean z2 = false;
        if (this.b.getVideoHeight() <= 0) {
            return false;
        }
        Rect rect = new Rect();
        this.l.getWindow().getDecorView().getWindowVisibleDisplayFrame(rect);
        if (rect.width() > rect.height()) {
            if (((float) (rect.width() - ((rect.height() * this.b.getVideoWidth()) / this.b.getVideoHeight()))) - (192.0f * x.b) < 0.0f) {
                z2 = true;
            }
            return z2;
        }
        if (((((float) (rect.height() - ((rect.width() * this.b.getVideoHeight()) / this.b.getVideoWidth()))) - (x.b * 64.0f)) - (64.0f * x.b)) - (40.0f * x.b) < 0.0f) {
            z2 = true;
        }
        return z2;
    }

    private void l() {
        b((View) this.b);
        b((View) this.q);
        b((View) this.r);
        b((View) this.s);
        b((View) this.t);
        b((View) this.v);
        b((View) this.w);
        b((View) this.y);
        if (this.u != null) {
            b((View) this.u);
        }
    }

    /* access modifiers changed from: protected */
    public void a() {
        if (this.c == null) {
            Log.e(j, "Unable to add UI without a valid ad response.");
            return;
        }
        String string = this.c.getString(UserDataStore.CITY);
        String optString = this.c.getJSONObject("context").optString("orientation");
        if (!optString.isEmpty()) {
            this.o = f.a(Integer.parseInt(optString));
        }
        if (this.c.has(TtmlNode.TAG_LAYOUT) && !this.c.isNull(TtmlNode.TAG_LAYOUT)) {
            JSONObject jSONObject = this.c.getJSONObject(TtmlNode.TAG_LAYOUT);
            this.z = (int) jSONObject.optLong("bgColor", (long) this.z);
            this.A = (int) jSONObject.optLong("textColor", (long) this.A);
            this.B = (int) jSONObject.optLong("accentColor", (long) this.B);
            this.C = jSONObject.optBoolean("persistentAdDetails", this.C);
        }
        JSONObject jSONObject2 = this.c.getJSONObject(MimeTypes.BASE_TYPE_TEXT);
        this.b.setId(VERSION.SDK_INT >= 17 ? View.generateViewId() : x.a());
        int c = c();
        Context context = this.d;
        if (c < 0) {
            c = 0;
        }
        this.y = new j(context, c, this.B);
        this.y.setOnTouchListener(this.n);
        this.b.a((b) this.y);
        if (this.c.has("cta") && !this.c.isNull("cta")) {
            JSONObject jSONObject3 = this.c.getJSONObject("cta");
            com.facebook.ads.internal.view.e.a aVar = new com.facebook.ads.internal.view.e.a(this.d, this.p, jSONObject3.getString("url"), jSONObject3.getString(MimeTypes.BASE_TYPE_TEXT), this.B, this.b, this.a, string);
            this.q = aVar;
            c.a(this.d, this.a, string, Uri.parse(jSONObject3.getString("url")), new HashMap());
        }
        if (this.c.has(SettingsJsonConstants.APP_ICON_KEY) && !this.c.isNull(SettingsJsonConstants.APP_ICON_KEY)) {
            JSONObject jSONObject4 = this.c.getJSONObject(SettingsJsonConstants.APP_ICON_KEY);
            this.t = new ImageView(this.d);
            new com.facebook.ads.internal.view.c.d(this.t).a((int) (x.b * 64.0f), (int) (64.0f * x.b)).a(jSONObject4.getString("url"));
        }
        if (this.c.has(MessengerShareContentUtility.MEDIA_IMAGE) && !this.c.isNull(MessengerShareContentUtility.MEDIA_IMAGE)) {
            JSONObject jSONObject5 = this.c.getJSONObject(MessengerShareContentUtility.MEDIA_IMAGE);
            g gVar = new g(this.d);
            this.b.a((b) gVar);
            gVar.setImage(jSONObject5.getString("url"));
        }
        String optString2 = jSONObject2.optString("title");
        if (!optString2.isEmpty()) {
            this.r = new TextView(this.d);
            this.r.setText(optString2);
            this.r.setTypeface(Typeface.defaultFromStyle(1));
        }
        String optString3 = jSONObject2.optString("subtitle");
        if (!optString3.isEmpty()) {
            this.s = new TextView(this.d);
            this.s.setText(optString3);
            this.s.setTextSize(16.0f);
        }
        this.v = new n(this.d);
        this.b.a((b) this.v);
        String d = d();
        if (!TextUtils.isEmpty(d)) {
            C0020a aVar2 = new C0020a(this.d, "AdChoices", d, new float[]{0.0f, 0.0f, 8.0f, 0.0f}, string);
            this.u = aVar2;
            LayoutParams layoutParams = new LayoutParams(-2, -2);
            layoutParams.addRule(10);
            layoutParams.addRule(9);
            this.u.setLayoutParams(layoutParams);
        }
        this.b.a((b) new k(this.d));
        l lVar = new l(this.d);
        this.b.a((b) lVar);
        d.a aVar3 = h() ? d.a.FADE_OUT_ON_PLAY : d.a.VISIBLE;
        this.b.a((b) new d(lVar, aVar3));
        this.x = new d(new RelativeLayout(this.d), aVar3);
        this.b.a((b) this.x);
    }

    @TargetApi(17)
    public void a(Intent intent, Bundle bundle, AudienceNetworkActivity audienceNetworkActivity) {
        this.l = audienceNetworkActivity;
        if (i || this.k != null) {
            audienceNetworkActivity.addBackButtonInterceptor(this.m);
            l();
            a(this.l.getResources().getConfiguration().orientation);
            if (h()) {
                e();
            } else {
                f();
            }
        } else {
            throw new AssertionError();
        }
    }

    public void a(Configuration configuration) {
        l();
        a(configuration.orientation);
    }

    public void a(Bundle bundle) {
    }

    public void a_(boolean z2) {
        if (this.b != null && this.b.getState() == com.facebook.ads.internal.view.i.d.d.STARTED) {
            this.D = this.b.getVideoStartReason();
            this.b.a(false);
        }
    }

    public void b(boolean z2) {
        if (this.b != null && this.D != null) {
            this.b.a(this.D);
        }
    }

    /* access modifiers changed from: protected */
    public boolean h() {
        if (i || this.c != null) {
            try {
                return this.c.getJSONObject("video").getBoolean("autoplay");
            } catch (Exception e) {
                Log.w(String.valueOf(h.class), "Invalid JSON", e);
                return true;
            }
        } else {
            throw new AssertionError();
        }
    }

    public f i() {
        return this.o;
    }

    public void j() {
        if (this.l != null) {
            this.l.finish();
        }
    }

    public void onDestroy() {
        if (!(this.c == null || this.a == null)) {
            String optString = this.c.optString(UserDataStore.CITY);
            if (!TextUtils.isEmpty(optString)) {
                this.a.l(optString, new HashMap());
            }
        }
        if (this.b != null) {
            this.b.g();
        }
        g.a((a) this);
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        this.p.a(motionEvent, view.getRootView(), view);
        if (this.b != null) {
            this.b.getEventBus().a(new u(view, motionEvent));
        }
        return true;
    }

    public void setListener(C0012a aVar) {
        this.k = aVar;
    }
}
