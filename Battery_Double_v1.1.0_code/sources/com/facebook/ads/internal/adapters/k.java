package com.facebook.ads.internal.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.net.Uri.Builder;
import android.provider.Settings.System;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import com.facebook.ads.AdError;
import com.facebook.ads.AdSettings;
import com.facebook.ads.AudienceNetworkActivity;
import com.facebook.ads.internal.adapters.b.d;
import com.facebook.ads.internal.adapters.b.f;
import com.facebook.ads.internal.adapters.b.n;
import com.facebook.ads.internal.adapters.b.o;
import com.facebook.ads.internal.adapters.b.q;
import com.facebook.ads.internal.settings.a.C0009a;
import com.facebook.ads.internal.view.e.c;
import com.facebook.ads.internal.w.b.x;
import com.facebook.share.internal.ShareConstants;
import io.fabric.sdk.android.services.events.EventsFilesManager;
import java.lang.ref.WeakReference;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONObject;

public class k extends s {
    private final String c = UUID.randomUUID().toString();
    private final AtomicBoolean d = new AtomicBoolean();
    /* access modifiers changed from: private */
    public Context e;
    private t f;
    private String g;
    private String h;
    private long i;
    /* access modifiers changed from: private */
    public com.facebook.ads.internal.adapters.b.a j;
    private u k;
    private C0009a l;
    private String m;
    private a n;

    private static class a implements com.facebook.ads.internal.adapters.c.a.b {
        final WeakReference<k> a;
        final WeakReference<t> b;
        final AtomicBoolean c;

        a(k kVar, t tVar, AtomicBoolean atomicBoolean) {
            this.a = new WeakReference<>(kVar);
            this.b = new WeakReference<>(tVar);
            this.c = atomicBoolean;
        }

        public void a() {
            this.c.set(true);
            if (this.b.get() != null && this.a.get() != null) {
                ((t) this.b.get()).a((s) this.a.get());
            }
        }

        public void a(AdError adError) {
            if (this.b.get() != null && this.a.get() != null) {
                ((t) this.b.get()).a((s) this.a.get(), adError);
            }
        }
    }

    private static abstract class b implements com.facebook.ads.internal.h.a {
        final WeakReference<k> d;
        final WeakReference<t> e;
        final com.facebook.ads.internal.h.b f;
        final AtomicBoolean g;
        final boolean h;

        private b(k kVar, t tVar, com.facebook.ads.internal.h.b bVar, AtomicBoolean atomicBoolean, boolean z) {
            this.d = new WeakReference<>(kVar);
            this.e = new WeakReference<>(tVar);
            this.f = bVar;
            this.g = atomicBoolean;
            this.h = z;
        }

        public void a() {
            a(true, (k) this.d.get(), (t) this.e.get());
        }

        /* access modifiers changed from: 0000 */
        public abstract void a(boolean z, @Nullable k kVar, @Nullable t tVar);

        public void b() {
            if (this.e.get() != null && this.d.get() != null) {
                if (this.h) {
                    ((t) this.e.get()).a((s) this.d.get(), AdError.CACHE_ERROR);
                } else {
                    a(false, (k) this.d.get(), (t) this.e.get());
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(Context context, boolean z, q qVar) {
        this.n = new a(this, this.f, this.d);
        com.facebook.ads.internal.adapters.c.a.a(context, o.a(qVar), z, this.n);
    }

    private void a(com.facebook.ads.internal.h.b bVar, q qVar) {
        bVar.a(qVar.f().b(), c.a, c.a);
        bVar.a(qVar.j().a());
        String g2 = qVar.j().g();
        Context context = this.e;
        d j2 = qVar.j();
        int min = com.facebook.ads.internal.r.a.S(context) ? Math.min(x.a.heightPixels, j2.i()) : j2.i();
        Context context2 = this.e;
        d j3 = qVar.j();
        bVar.a(g2, min, com.facebook.ads.internal.r.a.S(context2) ? Math.min(x.a.widthPixels, j3.h()) : j3.h());
        for (String a2 : qVar.k().d()) {
            bVar.a(a2, -1, -1);
        }
    }

    /* access modifiers changed from: private */
    public static boolean b(q qVar, boolean z) {
        n j2 = qVar.j().j();
        return j2 != null && (!z || !j2.i());
    }

    public int a() {
        if (this.j == null) {
            return -1;
        }
        if (this.l != C0009a.REWARDED_VIDEO_CHOOSE_YOUR_OWN_AD) {
            return ((q) this.j).j().d();
        }
        int i2 = 0;
        for (q j2 : ((f) this.j).j()) {
            int d2 = j2.j().d();
            if (i2 < d2) {
                i2 = d2;
            }
        }
        return i2;
    }

    /* JADX WARNING: type inference failed for: r0v11, types: [com.facebook.ads.internal.adapters.k$2] */
    /* JADX WARNING: type inference failed for: r10v5, types: [com.facebook.ads.internal.h.a] */
    /* JADX WARNING: type inference failed for: r0v12, types: [com.facebook.ads.internal.adapters.k$1] */
    /* JADX WARNING: type inference failed for: r0v13, types: [com.facebook.ads.internal.adapters.k$2] */
    /* JADX WARNING: type inference failed for: r0v14, types: [com.facebook.ads.internal.adapters.k$1] */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r0v13, types: [com.facebook.ads.internal.adapters.k$2]
  assigns: [com.facebook.ads.internal.adapters.k$2, com.facebook.ads.internal.adapters.k$1]
  uses: [com.facebook.ads.internal.adapters.k$2, com.facebook.ads.internal.h.a, com.facebook.ads.internal.adapters.k$1]
  mth insns count: 116
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
    	at jadx.core.ProcessClass.process(ProcessClass.java:35)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Unknown variable types count: 3 */
    public void a(Context context, t tVar, Map<String, Object> map, boolean z, String str) {
        com.facebook.ads.internal.h.b bVar;
        ? r10;
        this.e = context;
        this.f = tVar;
        this.d.set(false);
        this.h = (String) map.get(AudienceNetworkActivity.PLACEMENT_ID);
        this.i = ((Long) map.get(AudienceNetworkActivity.REQUEST_TIME)).longValue();
        int k2 = ((com.facebook.ads.internal.m.d) map.get("definition")).k();
        this.m = str;
        this.g = this.h != null ? this.h.split(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR)[0] : "";
        boolean equals = "choose_your_own_ad_rewarded_video".equals(map.get("data_model_type"));
        this.j = com.facebook.ads.internal.adapters.b.a.a(equals, (JSONObject) map.get(ShareConstants.WEB_DIALOG_PARAM_DATA));
        C0009a aVar = equals ? C0009a.REWARDED_VIDEO_CHOOSE_YOUR_OWN_AD : b((q) this.j, true) ? C0009a.REWARDED_PLAYABLE : C0009a.REWARDED_VIDEO;
        this.l = aVar;
        this.j.b(this.m);
        this.j.a(k2);
        if (this.l != C0009a.REWARDED_VIDEO || !TextUtils.isEmpty(((q) this.j).j().a())) {
            this.k = new u(this.c, this, tVar);
            LocalBroadcastManager.getInstance(this.e).registerReceiver(this.k, this.k.a());
            if (this.l == C0009a.REWARDED_VIDEO) {
                bVar = new com.facebook.ads.internal.h.b(context);
                final q qVar = (q) this.j;
                a(bVar, qVar);
                final boolean z2 = z;
                ? r0 = new b(this, this.f, bVar, this.d, z) {
                    /* access modifiers changed from: 0000 */
                    public void a(boolean z, @Nullable k kVar, @Nullable t tVar) {
                        if (!(kVar == null || tVar == null)) {
                            this.g.set(true);
                            qVar.c(z ? this.f.c(qVar.j().a()) : qVar.j().a());
                            if (k.b((q) k.this.j, false)) {
                                k.this.a(k.this.e, z2, qVar);
                                return;
                            }
                            tVar.a(kVar);
                        }
                    }
                };
                r10 = r0;
            } else if (this.l == C0009a.REWARDED_PLAYABLE) {
                a(context, z, (q) this.j);
                return;
            } else {
                bVar = new com.facebook.ads.internal.h.b(context);
                final f fVar = (f) this.j;
                for (q a2 : fVar.j()) {
                    a(bVar, a2);
                }
                ? r02 = new b(this, this.f, bVar, this.d, z) {
                    /* access modifiers changed from: 0000 */
                    public void a(boolean z, @Nullable k kVar, @Nullable t tVar) {
                        if (kVar != null && tVar != null) {
                            this.g.set(true);
                            for (q qVar : fVar.j()) {
                                qVar.c(z ? this.f.c(qVar.j().a()) : qVar.j().a());
                                k.b(qVar, false);
                            }
                            tVar.a(kVar);
                        }
                    }
                };
                r10 = r02;
            }
            bVar.a((com.facebook.ads.internal.h.a) r10);
            return;
        }
        this.f.a(this, AdError.internalError(AdError.INTERNAL_ERROR_2003));
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x00ea  */
    public boolean b() {
        String str;
        String str2;
        int i2;
        String str3;
        if (!this.d.get()) {
            return false;
        }
        if (this.a != null) {
            String urlPrefix = AdSettings.getUrlPrefix();
            if (urlPrefix == null || urlPrefix.isEmpty()) {
                str3 = "https://www.facebook.com/audience_network/server_side_reward";
            } else {
                str3 = String.format(Locale.US, "https://www.%s.facebook.com/audience_network/server_side_reward", new Object[]{urlPrefix});
            }
            Uri parse = Uri.parse(str3);
            Builder builder = new Builder();
            builder.scheme(parse.getScheme());
            builder.authority(parse.getAuthority());
            builder.path(parse.getPath());
            builder.query(parse.getQuery());
            builder.fragment(parse.getFragment());
            builder.appendQueryParameter("puid", this.a.getUserID());
            builder.appendQueryParameter("pc", this.a.getCurrency());
            builder.appendQueryParameter("ptid", this.c);
            builder.appendQueryParameter("appid", this.g);
            str = builder.build().toString();
        } else {
            str = null;
        }
        this.j.a(str);
        Intent intent = new Intent(this.e, AudienceNetworkActivity.getAdActivity());
        intent.putExtra(AudienceNetworkActivity.VIEW_TYPE, this.l);
        intent.putExtra("rewardedVideoAdDataBundle", this.j);
        intent.putExtra(AudienceNetworkActivity.AUDIENCE_NETWORK_UNIQUE_ID_EXTRA, this.c);
        intent.putExtra(AudienceNetworkActivity.REWARD_SERVER_URL, str);
        intent.putExtra(AudienceNetworkActivity.PLACEMENT_ID, this.h);
        intent.putExtra(AudienceNetworkActivity.REQUEST_TIME, this.i);
        if (this.b == -1 || System.getInt(this.e.getContentResolver(), "accelerometer_rotation", 0) == 1) {
            if (!com.facebook.ads.internal.r.a.y(this.e)) {
                str2 = AudienceNetworkActivity.PREDEFINED_ORIENTATION_KEY;
                i2 = 6;
            }
            if (!(this.e instanceof Activity)) {
                intent.setFlags(intent.getFlags() | 268435456);
            }
            this.e.startActivity(intent);
            return true;
        }
        str2 = AudienceNetworkActivity.PREDEFINED_ORIENTATION_KEY;
        i2 = this.b;
        intent.putExtra(str2, i2);
        if (!(this.e instanceof Activity)) {
        }
        this.e.startActivity(intent);
        return true;
    }

    public String getClientToken() {
        return this.j.a();
    }

    public void onDestroy() {
        if (this.k != null) {
            try {
                LocalBroadcastManager.getInstance(this.e).unregisterReceiver(this.k);
            } catch (Exception unused) {
            }
        }
    }
}
