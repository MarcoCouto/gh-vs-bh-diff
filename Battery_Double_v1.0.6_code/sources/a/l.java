package a;

import a.a.b.f;
import com.hmatalonga.greenhub.Config;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class l {

    /* renamed from: a reason: collision with root package name */
    private static final Pattern f179a = Pattern.compile("(\\d{2,4})[^\\d]*");

    /* renamed from: b reason: collision with root package name */
    private static final Pattern f180b = Pattern.compile("(?i)(jan|feb|mar|apr|may|jun|jul|aug|sep|oct|nov|dec).*");
    private static final Pattern c = Pattern.compile("(\\d{1,2})[^\\d]*");
    private static final Pattern d = Pattern.compile("(\\d{1,2}):(\\d{1,2}):(\\d{1,2})[^\\d]*");
    private final String e;
    private final String f;
    private final long g;
    private final String h;
    private final String i;
    private final boolean j;
    private final boolean k;
    private final boolean l;
    private final boolean m;

    private l(String str, String str2, long j2, String str3, String str4, boolean z, boolean z2, boolean z3, boolean z4) {
        this.e = str;
        this.f = str2;
        this.g = j2;
        this.h = str3;
        this.i = str4;
        this.j = z;
        this.k = z2;
        this.m = z3;
        this.l = z4;
    }

    private static int a(String str, int i2, int i3, boolean z) {
        for (int i4 = i2; i4 < i3; i4++) {
            char charAt = str.charAt(i4);
            if (((charAt < ' ' && charAt != 9) || charAt >= 127 || (charAt >= '0' && charAt <= '9') || ((charAt >= 'a' && charAt <= 'z') || ((charAt >= 'A' && charAt <= 'Z') || charAt == ':'))) == (!z)) {
                return i4;
            }
        }
        return i3;
    }

    private static long a(String str) {
        try {
            long parseLong = Long.parseLong(str);
            if (parseLong <= 0) {
                return Long.MIN_VALUE;
            }
            return parseLong;
        } catch (NumberFormatException e2) {
            if (str.matches("-?\\d+")) {
                return !str.startsWith("-") ? Long.MAX_VALUE : Long.MIN_VALUE;
            }
            throw e2;
        }
    }

    private static long a(String str, int i2, int i3) {
        int a2 = a(str, i2, i3, false);
        int i4 = -1;
        int i5 = -1;
        int i6 = -1;
        int i7 = -1;
        int i8 = -1;
        int i9 = -1;
        Matcher matcher = d.matcher(str);
        while (a2 < i3) {
            int a3 = a(str, a2 + 1, i3, true);
            matcher.region(a2, a3);
            if (i4 == -1 && matcher.usePattern(d).matches()) {
                i4 = Integer.parseInt(matcher.group(1));
                i5 = Integer.parseInt(matcher.group(2));
                i6 = Integer.parseInt(matcher.group(3));
            } else if (i7 == -1 && matcher.usePattern(c).matches()) {
                i7 = Integer.parseInt(matcher.group(1));
            } else if (i8 == -1 && matcher.usePattern(f180b).matches()) {
                i8 = f180b.pattern().indexOf(matcher.group(1).toLowerCase(Locale.US)) / 4;
            } else if (i9 == -1 && matcher.usePattern(f179a).matches()) {
                i9 = Integer.parseInt(matcher.group(1));
            }
            a2 = a(str, a3 + 1, i3, false);
        }
        if (i9 >= 70 && i9 <= 99) {
            i9 += 1900;
        }
        if (i9 >= 0 && i9 <= 69) {
            i9 += Config.STARTUP_CURRENT_INTERVAL;
        }
        if (i9 < 1601) {
            throw new IllegalArgumentException();
        } else if (i8 == -1) {
            throw new IllegalArgumentException();
        } else if (i7 < 1 || i7 > 31) {
            throw new IllegalArgumentException();
        } else if (i4 < 0 || i4 > 23) {
            throw new IllegalArgumentException();
        } else if (i5 < 0 || i5 > 59) {
            throw new IllegalArgumentException();
        } else if (i6 < 0 || i6 > 59) {
            throw new IllegalArgumentException();
        } else {
            GregorianCalendar gregorianCalendar = new GregorianCalendar(a.a.l.d);
            gregorianCalendar.setLenient(false);
            gregorianCalendar.set(1, i9);
            gregorianCalendar.set(2, i8 - 1);
            gregorianCalendar.set(5, i7);
            gregorianCalendar.set(11, i4);
            gregorianCalendar.set(12, i5);
            gregorianCalendar.set(13, i6);
            gregorianCalendar.set(14, 0);
            return gregorianCalendar.getTimeInMillis();
        }
    }

    static l a(long j2, s sVar, String str) {
        long j3;
        String str2;
        String str3;
        int length = str.length();
        int a2 = a.a.l.a(str, 0, length, ';');
        int a3 = a.a.l.a(str, 0, a2, '=');
        if (a3 == a2) {
            return null;
        }
        String c2 = a.a.l.c(str, 0, a3);
        if (c2.isEmpty()) {
            return null;
        }
        String c3 = a.a.l.c(str, a3 + 1, a2);
        long j4 = 253402300799999L;
        long j5 = -1;
        String str4 = null;
        String str5 = null;
        boolean z = false;
        boolean z2 = false;
        boolean z3 = true;
        boolean z4 = false;
        int i2 = a2 + 1;
        while (i2 < length) {
            int a4 = a.a.l.a(str, i2, length, ';');
            int a5 = a.a.l.a(str, i2, a4, '=');
            String c4 = a.a.l.c(str, i2, a5);
            String str6 = a5 < a4 ? a.a.l.c(str, a5 + 1, a4) : "";
            if (c4.equalsIgnoreCase("expires")) {
                try {
                    j4 = a(str6, 0, str6.length());
                    z4 = true;
                    str3 = str4;
                } catch (IllegalArgumentException e2) {
                    str3 = str4;
                }
            } else {
                if (c4.equalsIgnoreCase("max-age")) {
                    try {
                        j5 = a(str6);
                        z4 = true;
                        str3 = str4;
                    } catch (NumberFormatException e3) {
                        str3 = str4;
                    }
                } else {
                    if (c4.equalsIgnoreCase("domain")) {
                        try {
                            str3 = b(str6);
                            z3 = false;
                        } catch (IllegalArgumentException e4) {
                            str3 = str4;
                        }
                    } else {
                        if (c4.equalsIgnoreCase("path")) {
                            str5 = str6;
                            str3 = str4;
                        } else {
                            if (c4.equalsIgnoreCase("secure")) {
                                z = true;
                                str3 = str4;
                            } else {
                                if (c4.equalsIgnoreCase("httponly")) {
                                    z2 = true;
                                    str3 = str4;
                                } else {
                                    str3 = str4;
                                }
                            }
                        }
                    }
                }
            }
            String str7 = str3;
            i2 = a4 + 1;
            j4 = j4;
            str4 = str7;
        }
        if (j5 == Long.MIN_VALUE) {
            j3 = Long.MIN_VALUE;
        } else if (j5 != -1) {
            j3 = (j5 <= 9223372036854775L ? j5 * 1000 : Long.MAX_VALUE) + j2;
            if (j3 < j2 || j3 > 253402300799999L) {
                j3 = 253402300799999L;
            }
        } else {
            j3 = j4;
        }
        if (str4 == null) {
            str4 = sVar.f();
        } else if (!b(sVar, str4)) {
            return null;
        }
        if (str5 == null || !str5.startsWith("/")) {
            String h2 = sVar.h();
            int lastIndexOf = h2.lastIndexOf(47);
            str2 = lastIndexOf != 0 ? h2.substring(0, lastIndexOf) : "/";
        } else {
            str2 = str5;
        }
        return new l(c2, c3, j3, str4, str2, z, z2, z3, z4);
    }

    public static l a(s sVar, String str) {
        return a(System.currentTimeMillis(), sVar, str);
    }

    public static List<l> a(s sVar, r rVar) {
        List c2 = rVar.c("Set-Cookie");
        List list = null;
        int size = c2.size();
        for (int i2 = 0; i2 < size; i2++) {
            l a2 = a(sVar, (String) c2.get(i2));
            if (a2 != null) {
                List list2 = list == null ? new ArrayList() : list;
                list2.add(a2);
                list = list2;
            }
        }
        return list != null ? Collections.unmodifiableList(list) : Collections.emptyList();
    }

    private static String b(String str) {
        if (str.endsWith(".")) {
            throw new IllegalArgumentException();
        }
        if (str.startsWith(".")) {
            str = str.substring(1);
        }
        String a2 = a.a.l.a(str);
        if (a2 != null) {
            return a2;
        }
        throw new IllegalArgumentException();
    }

    private static boolean b(s sVar, String str) {
        String f2 = sVar.f();
        if (f2.equals(str)) {
            return true;
        }
        return f2.endsWith(str) && f2.charAt((f2.length() - str.length()) + -1) == '.' && !a.a.l.b(f2);
    }

    public String a() {
        return this.e;
    }

    public String b() {
        return this.f;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof l)) {
            return false;
        }
        l lVar = (l) obj;
        return lVar.e.equals(this.e) && lVar.f.equals(this.f) && lVar.h.equals(this.h) && lVar.i.equals(this.i) && lVar.g == this.g && lVar.j == this.j && lVar.k == this.k && lVar.l == this.l && lVar.m == this.m;
    }

    public int hashCode() {
        int i2 = 0;
        int hashCode = ((this.l ? 0 : 1) + (((this.k ? 0 : 1) + (((this.j ? 0 : 1) + ((((((((((this.e.hashCode() + 527) * 31) + this.f.hashCode()) * 31) + this.h.hashCode()) * 31) + this.i.hashCode()) * 31) + ((int) (this.g ^ (this.g >>> 32)))) * 31)) * 31)) * 31)) * 31;
        if (!this.m) {
            i2 = 1;
        }
        return hashCode + i2;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.e);
        sb.append('=');
        sb.append(this.f);
        if (this.l) {
            if (this.g == Long.MIN_VALUE) {
                sb.append("; max-age=0");
            } else {
                sb.append("; expires=").append(f.a(new Date(this.g)));
            }
        }
        if (!this.m) {
            sb.append("; domain=").append(this.h);
        }
        sb.append("; path=").append(this.i);
        if (this.j) {
            sb.append("; secure");
        }
        if (this.k) {
            sb.append("; httponly");
        }
        return sb.toString();
    }
}
