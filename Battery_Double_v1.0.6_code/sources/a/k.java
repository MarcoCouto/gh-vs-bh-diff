package a;

import a.a.l;
import java.util.Arrays;
import java.util.List;
import javax.net.ssl.SSLSocket;

public final class k {

    /* renamed from: a reason: collision with root package name */
    public static final k f175a = new a(true).a(d).a(ae.TLS_1_2, ae.TLS_1_1, ae.TLS_1_0).a(true).a();

    /* renamed from: b reason: collision with root package name */
    public static final k f176b = new a(f175a).a(ae.TLS_1_0).a(true).a();
    public static final k c = new a(false).a();
    private static final h[] d = {h.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256, h.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256, h.TLS_DHE_RSA_WITH_AES_128_GCM_SHA256, h.TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA, h.TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA, h.TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA, h.TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA, h.TLS_DHE_RSA_WITH_AES_128_CBC_SHA, h.TLS_DHE_RSA_WITH_AES_256_CBC_SHA, h.TLS_RSA_WITH_AES_128_GCM_SHA256, h.TLS_RSA_WITH_AES_128_CBC_SHA, h.TLS_RSA_WITH_AES_256_CBC_SHA, h.TLS_RSA_WITH_3DES_EDE_CBC_SHA};
    /* access modifiers changed from: private */
    public final boolean e;
    /* access modifiers changed from: private */
    public final boolean f;
    /* access modifiers changed from: private */
    public final String[] g;
    /* access modifiers changed from: private */
    public final String[] h;

    public static final class a {
        /* access modifiers changed from: private */

        /* renamed from: a reason: collision with root package name */
        public boolean f177a;
        /* access modifiers changed from: private */

        /* renamed from: b reason: collision with root package name */
        public String[] f178b;
        /* access modifiers changed from: private */
        public String[] c;
        /* access modifiers changed from: private */
        public boolean d;

        public a(k kVar) {
            this.f177a = kVar.e;
            this.f178b = kVar.g;
            this.c = kVar.h;
            this.d = kVar.f;
        }

        a(boolean z) {
            this.f177a = z;
        }

        public a a(boolean z) {
            if (!this.f177a) {
                throw new IllegalStateException("no TLS extensions for cleartext connections");
            }
            this.d = z;
            return this;
        }

        public a a(ae... aeVarArr) {
            if (!this.f177a) {
                throw new IllegalStateException("no TLS versions for cleartext connections");
            }
            String[] strArr = new String[aeVarArr.length];
            for (int i = 0; i < aeVarArr.length; i++) {
                strArr[i] = aeVarArr[i].e;
            }
            return b(strArr);
        }

        public a a(h... hVarArr) {
            if (!this.f177a) {
                throw new IllegalStateException("no cipher suites for cleartext connections");
            }
            String[] strArr = new String[hVarArr.length];
            for (int i = 0; i < hVarArr.length; i++) {
                strArr[i] = hVarArr[i].aS;
            }
            return a(strArr);
        }

        public a a(String... strArr) {
            if (!this.f177a) {
                throw new IllegalStateException("no cipher suites for cleartext connections");
            } else if (strArr.length == 0) {
                throw new IllegalArgumentException("At least one cipher suite is required");
            } else {
                this.f178b = (String[]) strArr.clone();
                return this;
            }
        }

        public k a() {
            return new k(this);
        }

        public a b(String... strArr) {
            if (!this.f177a) {
                throw new IllegalStateException("no TLS versions for cleartext connections");
            } else if (strArr.length == 0) {
                throw new IllegalArgumentException("At least one TLS version is required");
            } else {
                this.c = (String[]) strArr.clone();
                return this;
            }
        }
    }

    private k(a aVar) {
        this.e = aVar.f177a;
        this.g = aVar.f178b;
        this.h = aVar.c;
        this.f = aVar.d;
    }

    private static boolean a(String[] strArr, String[] strArr2) {
        if (strArr == null || strArr2 == null || strArr.length == 0 || strArr2.length == 0) {
            return false;
        }
        for (String a2 : strArr) {
            if (l.a(strArr2, a2)) {
                return true;
            }
        }
        return false;
    }

    private k b(SSLSocket sSLSocket, boolean z) {
        String[] enabledCipherSuites = this.g != null ? (String[]) l.a(String.class, (T[]) this.g, (T[]) sSLSocket.getEnabledCipherSuites()) : sSLSocket.getEnabledCipherSuites();
        String[] enabledProtocols = this.h != null ? (String[]) l.a(String.class, (T[]) this.h, (T[]) sSLSocket.getEnabledProtocols()) : sSLSocket.getEnabledProtocols();
        if (z && l.a(sSLSocket.getSupportedCipherSuites(), "TLS_FALLBACK_SCSV")) {
            enabledCipherSuites = l.b(enabledCipherSuites, "TLS_FALLBACK_SCSV");
        }
        return new a(this).a(enabledCipherSuites).b(enabledProtocols).a();
    }

    /* access modifiers changed from: 0000 */
    public void a(SSLSocket sSLSocket, boolean z) {
        k b2 = b(sSLSocket, z);
        if (b2.h != null) {
            sSLSocket.setEnabledProtocols(b2.h);
        }
        if (b2.g != null) {
            sSLSocket.setEnabledCipherSuites(b2.g);
        }
    }

    public boolean a() {
        return this.e;
    }

    public boolean a(SSLSocket sSLSocket) {
        if (!this.e) {
            return false;
        }
        if (this.h == null || a(this.h, sSLSocket.getEnabledProtocols())) {
            return this.g == null || a(this.g, sSLSocket.getEnabledCipherSuites());
        }
        return false;
    }

    public List<h> b() {
        if (this.g == null) {
            return null;
        }
        h[] hVarArr = new h[this.g.length];
        for (int i = 0; i < this.g.length; i++) {
            hVarArr[i] = h.a(this.g[i]);
        }
        return l.a((T[]) hVarArr);
    }

    public List<ae> c() {
        if (this.h == null) {
            return null;
        }
        ae[] aeVarArr = new ae[this.h.length];
        for (int i = 0; i < this.h.length; i++) {
            aeVarArr[i] = ae.a(this.h[i]);
        }
        return l.a((T[]) aeVarArr);
    }

    public boolean d() {
        return this.f;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof k)) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        k kVar = (k) obj;
        if (this.e == kVar.e) {
            return !this.e || (Arrays.equals(this.g, kVar.g) && Arrays.equals(this.h, kVar.h) && this.f == kVar.f);
        }
        return false;
    }

    public int hashCode() {
        if (!this.e) {
            return 17;
        }
        return (this.f ? 0 : 1) + ((((Arrays.hashCode(this.g) + 527) * 31) + Arrays.hashCode(this.h)) * 31);
    }

    public String toString() {
        if (!this.e) {
            return "ConnectionSpec()";
        }
        return "ConnectionSpec(cipherSuites=" + (this.g != null ? b().toString() : "[all enabled]") + ", tlsVersions=" + (this.h != null ? c().toString() : "[all enabled]") + ", supportsTlsExtensions=" + this.f + ")";
    }
}
