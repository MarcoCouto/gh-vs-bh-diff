package a;

import a.a.l;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Iterator;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public final class n {

    /* renamed from: a reason: collision with root package name */
    private int f182a = 64;

    /* renamed from: b reason: collision with root package name */
    private int f183b = 5;
    private ExecutorService c;
    private final Deque<b> d = new ArrayDeque();
    private final Deque<b> e = new ArrayDeque();
    private final Deque<y> f = new ArrayDeque();

    private void b() {
        if (this.e.size() < this.f182a && !this.d.isEmpty()) {
            Iterator it = this.d.iterator();
            while (it.hasNext()) {
                b bVar = (b) it.next();
                if (c(bVar) < this.f183b) {
                    it.remove();
                    this.e.add(bVar);
                    a().execute(bVar);
                }
                if (this.e.size() >= this.f182a) {
                    return;
                }
            }
        }
    }

    private int c(b bVar) {
        int i = 0;
        Iterator it = this.e.iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                return i2;
            }
            i = ((b) it.next()).a().equals(bVar.a()) ? i2 + 1 : i2;
        }
    }

    public synchronized ExecutorService a() {
        if (this.c == null) {
            this.c = new ThreadPoolExecutor(0, Integer.MAX_VALUE, 60, TimeUnit.SECONDS, new SynchronousQueue(), l.a("OkHttp Dispatcher", false));
        }
        return this.c;
    }

    /* access modifiers changed from: 0000 */
    public synchronized void a(e eVar) {
        if (!this.f.remove(eVar)) {
            throw new AssertionError("Call wasn't in-flight!");
        }
    }

    /* access modifiers changed from: 0000 */
    public synchronized void a(b bVar) {
        if (this.e.size() >= this.f182a || c(bVar) >= this.f183b) {
            this.d.add(bVar);
        } else {
            this.e.add(bVar);
            a().execute(bVar);
        }
    }

    /* access modifiers changed from: 0000 */
    public synchronized void a(y yVar) {
        this.f.add(yVar);
    }

    /* access modifiers changed from: 0000 */
    public synchronized void b(b bVar) {
        if (!this.e.remove(bVar)) {
            throw new AssertionError("AsyncCall wasn't running!");
        }
        b();
    }
}
