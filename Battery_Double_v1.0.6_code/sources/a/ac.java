package a;

import a.a.l;
import b.c;
import b.e;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;

public abstract class ac implements Closeable {
    private Reader reader;

    private Charset charset() {
        u contentType = contentType();
        return contentType != null ? contentType.a(l.c) : l.c;
    }

    public static ac create(final u uVar, final long j, final e eVar) {
        if (eVar != null) {
            return new ac() {
                public long contentLength() {
                    return j;
                }

                public u contentType() {
                    return uVar;
                }

                public e source() {
                    return eVar;
                }
            };
        }
        throw new NullPointerException("source == null");
    }

    public static ac create(u uVar, String str) {
        Charset charset = l.c;
        if (uVar != null) {
            charset = uVar.b();
            if (charset == null) {
                charset = l.c;
                uVar = u.a(uVar + "; charset=utf-8");
            }
        }
        c a2 = new c().a(str, charset);
        return create(uVar, a2.a(), a2);
    }

    public static ac create(u uVar, byte[] bArr) {
        return create(uVar, (long) bArr.length, new c().c(bArr));
    }

    public final InputStream byteStream() {
        return source().g();
    }

    /* JADX INFO: finally extract failed */
    public final byte[] bytes() throws IOException {
        long contentLength = contentLength();
        if (contentLength > 2147483647L) {
            throw new IOException("Cannot buffer entire body for content length: " + contentLength);
        }
        e source = source();
        try {
            byte[] r = source.r();
            l.a((Closeable) source);
            if (contentLength == -1 || contentLength == ((long) r.length)) {
                return r;
            }
            throw new IOException("Content-Length and stream length disagree");
        } catch (Throwable th) {
            l.a((Closeable) source);
            throw th;
        }
    }

    public final Reader charStream() {
        Reader reader2 = this.reader;
        if (reader2 != null) {
            return reader2;
        }
        InputStreamReader inputStreamReader = new InputStreamReader(byteStream(), charset());
        this.reader = inputStreamReader;
        return inputStreamReader;
    }

    public void close() {
        l.a((Closeable) source());
    }

    public abstract long contentLength();

    public abstract u contentType();

    public abstract e source();

    public final String string() throws IOException {
        return new String(bytes(), charset().name());
    }
}
