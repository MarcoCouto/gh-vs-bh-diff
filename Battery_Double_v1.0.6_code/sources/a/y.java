package a;

import a.a.b.g;
import a.a.b.l;
import a.a.b.o;
import a.a.b.r;
import a.a.h;
import a.a.j;
import java.io.IOException;
import java.net.ProtocolException;

final class y implements e {

    /* renamed from: a reason: collision with root package name */
    volatile boolean f214a;

    /* renamed from: b reason: collision with root package name */
    z f215b;
    g c;
    /* access modifiers changed from: private */
    public final w d;
    private boolean e;

    class a implements a.t.a {

        /* renamed from: b reason: collision with root package name */
        private final int f217b;
        private final z c;
        private final boolean d;

        a(int i, z zVar, boolean z) {
            this.f217b = i;
            this.c = zVar;
            this.d = z;
        }

        public ab a(z zVar) throws IOException {
            if (this.f217b >= y.this.d.v().size()) {
                return y.this.a(zVar, this.d);
            }
            t tVar = (t) y.this.d.v().get(this.f217b);
            ab a2 = tVar.a(new a(this.f217b + 1, zVar, this.d));
            if (a2 != null) {
                return a2;
            }
            throw new NullPointerException("application interceptor " + tVar + " returned null");
        }
    }

    final class b extends h {
        private final f c;
        private final boolean d;

        private b(f fVar, boolean z) {
            super("OkHttp %s", y.this.d().toString());
            this.c = fVar;
            this.d = z;
        }

        /* access modifiers changed from: 0000 */
        public String a() {
            return y.this.f215b.a().f();
        }

        /* access modifiers changed from: protected */
        public void b() {
            boolean z = true;
            boolean z2 = false;
            try {
                ab a2 = y.this.a(this.d);
                if (y.this.f214a) {
                    try {
                        this.c.onFailure(y.this, new IOException("Canceled"));
                    } catch (IOException e) {
                        e = e;
                    }
                } else {
                    this.c.onResponse(y.this, a2);
                }
                y.this.d.s().b(this);
            } catch (IOException e2) {
                e = e2;
                z = z2;
                if (z) {
                    try {
                        j.c().a(4, "Callback failure for " + y.this.e(), (Throwable) e);
                    } catch (Throwable th) {
                        y.this.d.s().b(this);
                        throw th;
                    }
                } else {
                    this.c.onFailure(y.this, e);
                }
                y.this.d.s().b(this);
            }
        }
    }

    protected y(w wVar, z zVar) {
        this.d = wVar;
        this.f215b = zVar;
    }

    /* access modifiers changed from: private */
    public ab a(boolean z) throws IOException {
        return new a(0, this.f215b, z).a(this.f215b);
    }

    /* access modifiers changed from: private */
    public String e() {
        return (this.f214a ? "canceled call" : "call") + " to " + d();
    }

    /* access modifiers changed from: 0000 */
    public ab a(z zVar, boolean z) throws IOException {
        z zVar2;
        boolean z2;
        aa d2 = zVar.d();
        if (d2 != null) {
            a.z.a e2 = zVar.e();
            u contentType = d2.contentType();
            if (contentType != null) {
                e2.a("Content-Type", contentType.toString());
            }
            long contentLength = d2.contentLength();
            if (contentLength != -1) {
                e2.a("Content-Length", Long.toString(contentLength));
                e2.b("Transfer-Encoding");
            } else {
                e2.a("Transfer-Encoding", "chunked");
                e2.b("Content-Length");
            }
            zVar2 = e2.a();
        } else {
            zVar2 = zVar;
        }
        this.c = new g(this.d, zVar2, false, false, z, null, null, null);
        int i = 0;
        while (!this.f214a) {
            try {
                this.c.a();
                this.c.h();
                ab c2 = this.c.c();
                z i2 = this.c.i();
                if (i2 == null) {
                    if (!z) {
                        this.c.e();
                    }
                    return c2;
                }
                r g = this.c.g();
                int i3 = i + 1;
                if (i3 > 20) {
                    g.c();
                    throw new ProtocolException("Too many follow-up requests: " + i3);
                }
                if (!this.c.a(i2.a())) {
                    g.c();
                    g = null;
                } else if (g.a() != null) {
                    throw new IllegalStateException("Closing the body of " + c2 + " didn't close its backing stream. Bad interceptor?");
                }
                this.c = new g(this.d, i2, false, false, z, g, null, c2);
                i = i3;
            } catch (l e3) {
                throw e3.getCause();
            } catch (o e4) {
                g a2 = this.c.a(e4.a(), true, null);
                if (a2 != null) {
                    z2 = false;
                    this.c = a2;
                } else {
                    throw e4.a();
                }
            } catch (IOException e5) {
                g a3 = this.c.a(e5, false, null);
                if (a3 != null) {
                    z2 = false;
                    this.c = a3;
                } else {
                    throw e5;
                }
            } catch (Throwable th) {
                th = th;
            }
        }
        this.c.e();
        throw new IOException("Canceled");
        if (z2) {
            this.c.g().c();
        }
        throw th;
    }

    public z a() {
        return this.f215b;
    }

    public void a(f fVar) {
        a(fVar, false);
    }

    /* access modifiers changed from: 0000 */
    public void a(f fVar, boolean z) {
        synchronized (this) {
            if (this.e) {
                throw new IllegalStateException("Already Executed");
            }
            this.e = true;
        }
        this.d.s().a(new b(fVar, z));
    }

    public ab b() throws IOException {
        synchronized (this) {
            if (this.e) {
                throw new IllegalStateException("Already Executed");
            }
            this.e = true;
        }
        try {
            this.d.s().a(this);
            ab a2 = a(false);
            if (a2 != null) {
                return a2;
            }
            throw new IOException("Canceled");
        } finally {
            this.d.s().a((e) this);
        }
    }

    public void c() {
        this.f214a = true;
        if (this.c != null) {
            this.c.f();
        }
    }

    /* access modifiers changed from: 0000 */
    public s d() {
        return this.f215b.a().c("/...");
    }
}
