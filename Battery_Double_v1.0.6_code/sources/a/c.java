package a;

import a.a.e;
import java.io.Closeable;
import java.io.Flushable;
import java.io.IOException;

public final class c implements Closeable, Flushable {

    /* renamed from: a reason: collision with root package name */
    final e f159a;

    /* renamed from: b reason: collision with root package name */
    private final a.a.c f160b;

    public void close() throws IOException {
        this.f160b.close();
    }

    public void flush() throws IOException {
        this.f160b.flush();
    }
}
