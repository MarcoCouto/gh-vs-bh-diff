package a;

import a.a.b.r;
import a.a.c.b;
import a.a.k;
import a.a.l;
import java.lang.ref.Reference;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public final class j {
    static final /* synthetic */ boolean c = (!j.class.desiredAssertionStatus());
    private static final Executor d = new ThreadPoolExecutor(0, Integer.MAX_VALUE, 60, TimeUnit.SECONDS, new SynchronousQueue(), l.a("OkHttp ConnectionPool", true));

    /* renamed from: a reason: collision with root package name */
    final k f172a;

    /* renamed from: b reason: collision with root package name */
    boolean f173b;
    private final int e;
    private final long f;
    private final Runnable g;
    private final Deque<b> h;

    public j() {
        this(5, 5, TimeUnit.MINUTES);
    }

    public j(int i, long j, TimeUnit timeUnit) {
        this.g = new Runnable() {
            public void run() {
                while (true) {
                    long a2 = j.this.a(System.nanoTime());
                    if (a2 != -1) {
                        if (a2 > 0) {
                            long j = a2 / 1000000;
                            long j2 = a2 - (j * 1000000);
                            synchronized (j.this) {
                                try {
                                    j.this.wait(j, (int) j2);
                                } catch (InterruptedException e) {
                                }
                            }
                        }
                    } else {
                        return;
                    }
                }
            }
        };
        this.h = new ArrayDeque();
        this.f172a = new k();
        this.e = i;
        this.f = timeUnit.toNanos(j);
        if (j <= 0) {
            throw new IllegalArgumentException("keepAliveDuration <= 0: " + j);
        }
    }

    private int a(b bVar, long j) {
        List<Reference<r>> list = bVar.h;
        int i = 0;
        while (i < list.size()) {
            if (((Reference) list.get(i)).get() != null) {
                i++;
            } else {
                a.a.j.c().a(5, "A connection to " + bVar.a().a().a() + " was leaked. Did you forget to close a response body?", (Throwable) null);
                list.remove(i);
                bVar.i = true;
                if (list.isEmpty()) {
                    bVar.j = j - this.f;
                    return 0;
                }
            }
        }
        return list.size();
    }

    /* access modifiers changed from: 0000 */
    public long a(long j) {
        b bVar;
        long j2;
        b bVar2 = null;
        long j3 = Long.MIN_VALUE;
        synchronized (this) {
            int i = 0;
            int i2 = 0;
            for (b bVar3 : this.h) {
                if (a(bVar3, j) > 0) {
                    i2++;
                } else {
                    int i3 = i + 1;
                    long j4 = j - bVar3.j;
                    if (j4 > j3) {
                        long j5 = j4;
                        bVar = bVar3;
                        j2 = j5;
                    } else {
                        bVar = bVar2;
                        j2 = j3;
                    }
                    j3 = j2;
                    bVar2 = bVar;
                    i = i3;
                }
            }
            if (j3 >= this.f || i > this.e) {
                this.h.remove(bVar2);
                l.a(bVar2.c());
                return 0;
            } else if (i > 0) {
                long j6 = this.f - j3;
                return j6;
            } else if (i2 > 0) {
                long j7 = this.f;
                return j7;
            } else {
                this.f173b = false;
                return -1;
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public b a(a aVar, r rVar) {
        if (c || Thread.holdsLock(this)) {
            for (b bVar : this.h) {
                if (bVar.h.size() < bVar.g && aVar.equals(bVar.a().f154a) && !bVar.i) {
                    rVar.a(bVar);
                    return bVar;
                }
            }
            return null;
        }
        throw new AssertionError();
    }

    /* access modifiers changed from: 0000 */
    public void a(b bVar) {
        if (c || Thread.holdsLock(this)) {
            if (!this.f173b) {
                this.f173b = true;
                d.execute(this.g);
            }
            this.h.add(bVar);
            return;
        }
        throw new AssertionError();
    }

    /* access modifiers changed from: 0000 */
    public boolean b(b bVar) {
        if (!c && !Thread.holdsLock(this)) {
            throw new AssertionError();
        } else if (bVar.i || this.e == 0) {
            this.h.remove(bVar);
            return true;
        } else {
            notifyAll();
            return false;
        }
    }
}
