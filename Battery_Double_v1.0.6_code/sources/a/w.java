package a;

import a.a.b.r;
import a.a.c.b;
import a.a.d;
import a.a.d.c;
import a.a.e;
import a.a.j;
import a.a.k;
import a.a.l;
import java.net.Proxy;
import java.net.ProxySelector;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.net.SocketFactory;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

public class w implements a.e.a, Cloneable {
    /* access modifiers changed from: private */
    public static final List<k> A;
    /* access modifiers changed from: private */
    public static final List<x> z = l.a((T[]) new x[]{x.HTTP_2, x.SPDY_3, x.HTTP_1_1});

    /* renamed from: a reason: collision with root package name */
    final n f208a;

    /* renamed from: b reason: collision with root package name */
    final Proxy f209b;
    final List<x> c;
    final List<k> d;
    final List<t> e;
    final List<t> f;
    final ProxySelector g;
    final m h;
    final c i;
    final e j;
    final SocketFactory k;
    final SSLSocketFactory l;
    final a.a.d.a m;
    final HostnameVerifier n;
    final g o;
    final b p;
    final b q;
    final j r;
    final o s;
    final boolean t;
    final boolean u;
    final boolean v;
    final int w;
    final int x;
    final int y;

    public static final class a {

        /* renamed from: a reason: collision with root package name */
        n f210a = new n();

        /* renamed from: b reason: collision with root package name */
        Proxy f211b;
        List<x> c = w.z;
        List<k> d = w.A;
        final List<t> e = new ArrayList();
        final List<t> f = new ArrayList();
        ProxySelector g = ProxySelector.getDefault();
        m h = m.f181a;
        c i;
        e j;
        SocketFactory k = SocketFactory.getDefault();
        SSLSocketFactory l;
        a.a.d.a m;
        HostnameVerifier n = c.f122a;
        g o = g.f165a;
        b p = b.f158a;
        b q = b.f158a;
        j r = new j();
        o s = o.f184a;
        boolean t = true;
        boolean u = true;
        boolean v = true;
        int w = 10000;
        int x = 10000;
        int y = 10000;
    }

    static {
        ArrayList arrayList = new ArrayList(Arrays.asList(new k[]{k.f175a, k.f176b}));
        if (j.c().a()) {
            arrayList.add(k.c);
        }
        A = l.a((List<T>) arrayList);
        d.f116a = new d() {
            public b a(j jVar, a aVar, r rVar) {
                return jVar.a(aVar, rVar);
            }

            public e a(w wVar) {
                return wVar.g();
            }

            public k a(j jVar) {
                return jVar.f172a;
            }

            public void a(k kVar, SSLSocket sSLSocket, boolean z) {
                kVar.a(sSLSocket, z);
            }

            public void a(a.r.a aVar, String str) {
                aVar.a(str);
            }

            public boolean a(j jVar, b bVar) {
                return jVar.b(bVar);
            }

            public void b(j jVar, b bVar) {
                jVar.a(bVar);
            }
        };
    }

    public w() {
        this(new a());
    }

    private w(a aVar) {
        this.f208a = aVar.f210a;
        this.f209b = aVar.f211b;
        this.c = aVar.c;
        this.d = aVar.d;
        this.e = l.a(aVar.e);
        this.f = l.a(aVar.f);
        this.g = aVar.g;
        this.h = aVar.h;
        this.i = aVar.i;
        this.j = aVar.j;
        this.k = aVar.k;
        boolean z2 = false;
        for (k a2 : this.d) {
            z2 = z2 || a2.a();
        }
        if (aVar.l != null || !z2) {
            this.l = aVar.l;
            this.m = aVar.m;
        } else {
            X509TrustManager z3 = z();
            this.l = a(z3);
            this.m = a.a.d.a.a(z3);
        }
        this.n = aVar.n;
        this.o = aVar.o.a(this.m);
        this.p = aVar.p;
        this.q = aVar.q;
        this.r = aVar.r;
        this.s = aVar.s;
        this.t = aVar.t;
        this.u = aVar.u;
        this.v = aVar.v;
        this.w = aVar.w;
        this.x = aVar.x;
        this.y = aVar.y;
    }

    private SSLSocketFactory a(X509TrustManager x509TrustManager) {
        try {
            SSLContext instance = SSLContext.getInstance("TLS");
            instance.init(null, new TrustManager[]{x509TrustManager}, null);
            return instance.getSocketFactory();
        } catch (GeneralSecurityException e2) {
            throw new AssertionError();
        }
    }

    private X509TrustManager z() {
        try {
            TrustManagerFactory instance = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            instance.init(null);
            TrustManager[] trustManagers = instance.getTrustManagers();
            if (trustManagers.length == 1 && (trustManagers[0] instanceof X509TrustManager)) {
                return (X509TrustManager) trustManagers[0];
            }
            throw new IllegalStateException("Unexpected default trust managers:" + Arrays.toString(trustManagers));
        } catch (GeneralSecurityException e2) {
            throw new AssertionError();
        }
    }

    public int a() {
        return this.w;
    }

    public e a(z zVar) {
        return new y(this, zVar);
    }

    public int b() {
        return this.x;
    }

    public int c() {
        return this.y;
    }

    public Proxy d() {
        return this.f209b;
    }

    public ProxySelector e() {
        return this.g;
    }

    public m f() {
        return this.h;
    }

    /* access modifiers changed from: 0000 */
    public e g() {
        return this.i != null ? this.i.f159a : this.j;
    }

    public o h() {
        return this.s;
    }

    public SocketFactory i() {
        return this.k;
    }

    public SSLSocketFactory j() {
        return this.l;
    }

    public HostnameVerifier k() {
        return this.n;
    }

    public g l() {
        return this.o;
    }

    public b m() {
        return this.q;
    }

    public b n() {
        return this.p;
    }

    public j o() {
        return this.r;
    }

    public boolean p() {
        return this.t;
    }

    public boolean q() {
        return this.u;
    }

    public boolean r() {
        return this.v;
    }

    public n s() {
        return this.f208a;
    }

    public List<x> t() {
        return this.c;
    }

    public List<k> u() {
        return this.d;
    }

    public List<t> v() {
        return this.e;
    }

    public List<t> w() {
        return this.f;
    }
}
