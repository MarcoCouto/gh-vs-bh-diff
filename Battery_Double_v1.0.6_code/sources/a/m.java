package a;

import java.util.Collections;
import java.util.List;

public interface m {

    /* renamed from: a reason: collision with root package name */
    public static final m f181a = new m() {
        public List<l> a(s sVar) {
            return Collections.emptyList();
        }

        public void a(s sVar, List<l> list) {
        }
    };

    List<l> a(s sVar);

    void a(s sVar, List<l> list);
}
