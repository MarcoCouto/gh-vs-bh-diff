package a;

import a.a.l;
import b.f;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.net.ssl.SSLPeerUnverifiedException;

public final class g {

    /* renamed from: a reason: collision with root package name */
    public static final g f165a = new a().a();

    /* renamed from: b reason: collision with root package name */
    private final List<b> f166b;
    private final a.a.d.a c;

    public static final class a {

        /* renamed from: a reason: collision with root package name */
        private final List<b> f167a = new ArrayList();

        public g a() {
            return new g(l.a(this.f167a), null);
        }
    }

    static final class b {

        /* renamed from: a reason: collision with root package name */
        final String f168a;

        /* renamed from: b reason: collision with root package name */
        final String f169b;
        final f c;

        /* access modifiers changed from: 0000 */
        public boolean a(String str) {
            boolean z = false;
            if (this.f168a.equals(str)) {
                return true;
            }
            int indexOf = str.indexOf(46);
            if (this.f168a.startsWith("*.")) {
                if (str.regionMatches(false, indexOf + 1, this.f168a, 2, this.f168a.length() - 2)) {
                    z = true;
                }
            }
            return z;
        }

        public boolean equals(Object obj) {
            return (obj instanceof b) && this.f168a.equals(((b) obj).f168a) && this.f169b.equals(((b) obj).f169b) && this.c.equals(((b) obj).c);
        }

        public int hashCode() {
            return ((((this.f168a.hashCode() + 527) * 31) + this.f169b.hashCode()) * 31) + this.c.hashCode();
        }

        public String toString() {
            return this.f169b + this.c.b();
        }
    }

    private g(List<b> list, a.a.d.a aVar) {
        this.f166b = list;
        this.c = aVar;
    }

    static f a(X509Certificate x509Certificate) {
        return l.a(f.a(x509Certificate.getPublicKey().getEncoded()));
    }

    public static String a(Certificate certificate) {
        if (certificate instanceof X509Certificate) {
            return "sha256/" + b((X509Certificate) certificate).b();
        }
        throw new IllegalArgumentException("Certificate pinning requires X509 certificates");
    }

    static f b(X509Certificate x509Certificate) {
        return l.b(f.a(x509Certificate.getPublicKey().getEncoded()));
    }

    /* access modifiers changed from: 0000 */
    public g a(a.a.d.a aVar) {
        return this.c != aVar ? new g(this.f166b, aVar) : this;
    }

    /* access modifiers changed from: 0000 */
    public List<b> a(String str) {
        List<b> emptyList = Collections.emptyList();
        for (b bVar : this.f166b) {
            if (bVar.a(str)) {
                if (emptyList.isEmpty()) {
                    emptyList = new ArrayList<>();
                }
                emptyList.add(bVar);
            }
        }
        return emptyList;
    }

    public void a(String str, List<Certificate> list) throws SSLPeerUnverifiedException {
        List a2 = a(str);
        if (!a2.isEmpty()) {
            if (this.c != null) {
                list = this.c.a(list, str);
            }
            int size = list.size();
            for (int i = 0; i < size; i++) {
                X509Certificate x509Certificate = (X509Certificate) list.get(i);
                int size2 = a2.size();
                int i2 = 0;
                f fVar = null;
                f fVar2 = null;
                while (i2 < size2) {
                    b bVar = (b) a2.get(i2);
                    if (bVar.f169b.equals("sha256/")) {
                        if (fVar == null) {
                            fVar = b(x509Certificate);
                        }
                        if (bVar.c.equals(fVar)) {
                            return;
                        }
                    } else if (bVar.f169b.equals("sha1/")) {
                        if (fVar2 == null) {
                            fVar2 = a(x509Certificate);
                        }
                        if (bVar.c.equals(fVar2)) {
                            return;
                        }
                    } else {
                        throw new AssertionError();
                    }
                    f fVar3 = fVar;
                    i2++;
                    fVar2 = fVar2;
                    fVar = fVar3;
                }
            }
            StringBuilder append = new StringBuilder().append("Certificate pinning failure!").append("\n  Peer certificate chain:");
            int size3 = list.size();
            for (int i3 = 0; i3 < size3; i3++) {
                X509Certificate x509Certificate2 = (X509Certificate) list.get(i3);
                append.append("\n    ").append(a((Certificate) x509Certificate2)).append(": ").append(x509Certificate2.getSubjectDN().getName());
            }
            append.append("\n  Pinned certificates for ").append(str).append(":");
            int size4 = a2.size();
            for (int i4 = 0; i4 < size4; i4++) {
                append.append("\n    ").append((b) a2.get(i4));
            }
            throw new SSLPeerUnverifiedException(append.toString());
        }
    }
}
