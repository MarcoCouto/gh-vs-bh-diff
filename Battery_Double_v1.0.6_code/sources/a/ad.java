package a;

import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.Proxy.Type;

public final class ad {

    /* renamed from: a reason: collision with root package name */
    final a f154a;

    /* renamed from: b reason: collision with root package name */
    final Proxy f155b;
    final InetSocketAddress c;

    public ad(a aVar, Proxy proxy, InetSocketAddress inetSocketAddress) {
        if (aVar == null) {
            throw new NullPointerException("address == null");
        } else if (proxy == null) {
            throw new NullPointerException("proxy == null");
        } else if (inetSocketAddress == null) {
            throw new NullPointerException("inetSocketAddress == null");
        } else {
            this.f154a = aVar;
            this.f155b = proxy;
            this.c = inetSocketAddress;
        }
    }

    public a a() {
        return this.f154a;
    }

    public Proxy b() {
        return this.f155b;
    }

    public InetSocketAddress c() {
        return this.c;
    }

    public boolean d() {
        return this.f154a.i != null && this.f155b.type() == Type.HTTP;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof ad)) {
            return false;
        }
        ad adVar = (ad) obj;
        return this.f154a.equals(adVar.f154a) && this.f155b.equals(adVar.f155b) && this.c.equals(adVar.c);
    }

    public int hashCode() {
        return ((((this.f154a.hashCode() + 527) * 31) + this.f155b.hashCode()) * 31) + this.c.hashCode();
    }
}
