package a.a;

import a.a.c.a;
import b.d;
import b.r;
import b.t;
import java.io.Closeable;
import java.io.File;
import java.io.Flushable;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.concurrent.Executor;
import java.util.regex.Pattern;

public final class c implements Closeable, Flushable {

    /* renamed from: a reason: collision with root package name */
    static final Pattern f108a = Pattern.compile("[a-z0-9_-]{1,120}");

    /* renamed from: b reason: collision with root package name */
    static final /* synthetic */ boolean f109b = (!c.class.desiredAssertionStatus());
    private static final r p = new r() {
        public void a(b.c cVar, long j) throws IOException {
            cVar.g(j);
        }

        public void close() throws IOException {
        }

        public void flush() throws IOException {
        }

        public t timeout() {
            return t.f1394b;
        }
    };
    /* access modifiers changed from: private */
    public final a c;
    private long d;
    /* access modifiers changed from: private */
    public final int e;
    private long f;
    private d g;
    private final LinkedHashMap<String, b> h;
    private int i;
    private boolean j;
    private boolean k;
    private boolean l;
    private long m;
    private final Executor n;
    private final Runnable o;

    public final class a {

        /* renamed from: a reason: collision with root package name */
        final /* synthetic */ c f110a;
        /* access modifiers changed from: private */

        /* renamed from: b reason: collision with root package name */
        public final b f111b;
        /* access modifiers changed from: private */
        public final boolean[] c;
        private boolean d;

        /* access modifiers changed from: 0000 */
        public void a() {
            if (this.f111b.f == this) {
                for (int i = 0; i < this.f110a.e; i++) {
                    try {
                        this.f110a.c.a(this.f111b.d[i]);
                    } catch (IOException e) {
                    }
                }
                this.f111b.f = null;
            }
        }

        public void b() throws IOException {
            synchronized (this.f110a) {
                if (this.d) {
                    throw new IllegalStateException();
                }
                if (this.f111b.f == this) {
                    this.f110a.a(this, false);
                }
                this.d = true;
            }
        }
    }

    private final class b {
        /* access modifiers changed from: private */

        /* renamed from: a reason: collision with root package name */
        public final String f113a;
        /* access modifiers changed from: private */

        /* renamed from: b reason: collision with root package name */
        public final long[] f114b;
        /* access modifiers changed from: private */
        public final File[] c;
        /* access modifiers changed from: private */
        public final File[] d;
        /* access modifiers changed from: private */
        public boolean e;
        /* access modifiers changed from: private */
        public a f;
        /* access modifiers changed from: private */
        public long g;

        /* access modifiers changed from: 0000 */
        public void a(d dVar) throws IOException {
            for (long k : this.f114b) {
                dVar.i(32).k(k);
            }
        }
    }

    /* access modifiers changed from: private */
    public synchronized void a(a aVar, boolean z) throws IOException {
        synchronized (this) {
            b a2 = aVar.f111b;
            if (a2.f != aVar) {
                throw new IllegalStateException();
            }
            if (z) {
                if (!a2.e) {
                    int i2 = 0;
                    while (true) {
                        if (i2 < this.e) {
                            if (!aVar.c[i2]) {
                                aVar.b();
                                throw new IllegalStateException("Newly created entry didn't create value for index " + i2);
                            } else if (!this.c.b(a2.d[i2])) {
                                aVar.b();
                                break;
                            } else {
                                i2++;
                            }
                        }
                    }
                }
            }
            for (int i3 = 0; i3 < this.e; i3++) {
                File file = a2.d[i3];
                if (!z) {
                    this.c.a(file);
                } else if (this.c.b(file)) {
                    File file2 = a2.c[i3];
                    this.c.a(file, file2);
                    long j2 = a2.f114b[i3];
                    long c2 = this.c.c(file2);
                    a2.f114b[i3] = c2;
                    this.f = (this.f - j2) + c2;
                }
            }
            this.i++;
            a2.f = null;
            if (a2.e || z) {
                a2.e = true;
                this.g.b("CLEAN").i(32);
                this.g.b(a2.f113a);
                a2.a(this.g);
                this.g.i(10);
                if (z) {
                    long j3 = this.m;
                    this.m = 1 + j3;
                    a2.g = j3;
                }
            } else {
                this.h.remove(a2.f113a);
                this.g.b("REMOVE").i(32);
                this.g.b(a2.f113a);
                this.g.i(10);
            }
            this.g.flush();
            if (this.f > this.d || b()) {
                this.n.execute(this.o);
            }
        }
    }

    private boolean a(b bVar) throws IOException {
        if (bVar.f != null) {
            bVar.f.a();
        }
        for (int i2 = 0; i2 < this.e; i2++) {
            this.c.a(bVar.c[i2]);
            this.f -= bVar.f114b[i2];
            bVar.f114b[i2] = 0;
        }
        this.i++;
        this.g.b("REMOVE").i(32).b(bVar.f113a).i(10);
        this.h.remove(bVar.f113a);
        if (b()) {
            this.n.execute(this.o);
        }
        return true;
    }

    private boolean b() {
        return this.i >= 2000 && this.i >= this.h.size();
    }

    private synchronized void c() {
        if (a()) {
            throw new IllegalStateException("cache is closed");
        }
    }

    private void d() throws IOException {
        while (this.f > this.d) {
            a((b) this.h.values().iterator().next());
        }
        this.l = false;
    }

    public synchronized boolean a() {
        return this.k;
    }

    public synchronized void close() throws IOException {
        b[] bVarArr;
        if (!this.j || this.k) {
            this.k = true;
        } else {
            for (b bVar : (b[]) this.h.values().toArray(new b[this.h.size()])) {
                if (bVar.f != null) {
                    bVar.f.b();
                }
            }
            d();
            this.g.close();
            this.g = null;
            this.k = true;
        }
    }

    public synchronized void flush() throws IOException {
        if (this.j) {
            c();
            d();
            this.g.flush();
        }
    }
}
