package a.a;

import a.a;
import a.a.b.r;
import a.a.c.b;
import a.j;
import a.k;
import a.w;
import javax.net.ssl.SSLSocket;

public abstract class d {

    /* renamed from: a reason: collision with root package name */
    public static d f116a;

    public abstract b a(j jVar, a aVar, r rVar);

    public abstract e a(w wVar);

    public abstract k a(j jVar);

    public abstract void a(k kVar, SSLSocket sSLSocket, boolean z);

    public abstract void a(a.r.a aVar, String str);

    public abstract boolean a(j jVar, b bVar);

    public abstract void b(j jVar, b bVar);
}
