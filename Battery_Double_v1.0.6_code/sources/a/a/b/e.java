package a.a.b;

import a.a.a.d;
import a.a.l;
import a.ab;
import a.ac;
import a.r;
import a.x;
import a.z;
import b.f;
import b.h;
import b.s;
import java.io.IOException;
import java.net.ProtocolException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public final class e implements i {

    /* renamed from: a reason: collision with root package name */
    private static final f f83a = f.a("connection");

    /* renamed from: b reason: collision with root package name */
    private static final f f84b = f.a("host");
    private static final f c = f.a("keep-alive");
    private static final f d = f.a("proxy-connection");
    private static final f e = f.a("transfer-encoding");
    private static final f f = f.a("te");
    private static final f g = f.a("encoding");
    private static final f h = f.a("upgrade");
    private static final List<f> i = l.a((T[]) new f[]{f83a, f84b, c, d, e, a.a.a.f.f32b, a.a.a.f.c, a.a.a.f.d, a.a.a.f.e, a.a.a.f.f, a.a.a.f.g});
    private static final List<f> j = l.a((T[]) new f[]{f83a, f84b, c, d, e});
    private static final List<f> k = l.a((T[]) new f[]{f83a, f84b, c, d, f, e, g, h, a.a.a.f.f32b, a.a.a.f.c, a.a.a.f.d, a.a.a.f.e, a.a.a.f.f, a.a.a.f.g});
    private static final List<f> l = l.a((T[]) new f[]{f83a, f84b, c, d, f, e, g, h});
    /* access modifiers changed from: private */
    public final r m;
    private final d n;
    private g o;
    private a.a.a.e p;

    class a extends h {
        public a(s sVar) {
            super(sVar);
        }

        public void close() throws IOException {
            e.this.m.a(false, e.this);
            super.close();
        }
    }

    public e(r rVar, d dVar) {
        this.m = rVar;
        this.n = dVar;
    }

    public static a.ab.a a(List<a.a.a.f> list) throws IOException {
        String str = null;
        String str2 = "HTTP/1.1";
        a.r.a aVar = new a.r.a();
        int size = list.size();
        int i2 = 0;
        while (i2 < size) {
            f fVar = ((a.a.a.f) list.get(i2)).h;
            String a2 = ((a.a.a.f) list.get(i2)).i.a();
            String str3 = str2;
            int i3 = 0;
            while (i3 < a2.length()) {
                int indexOf = a2.indexOf(0, i3);
                if (indexOf == -1) {
                    indexOf = a2.length();
                }
                String substring = a2.substring(i3, indexOf);
                if (!fVar.equals(a.a.a.f.f31a)) {
                    if (fVar.equals(a.a.a.f.g)) {
                        str3 = substring;
                        substring = str;
                    } else {
                        if (!j.contains(fVar)) {
                            aVar.a(fVar.a(), substring);
                        }
                        substring = str;
                    }
                }
                str = substring;
                i3 = indexOf + 1;
            }
            i2++;
            str2 = str3;
        }
        if (str == null) {
            throw new ProtocolException("Expected ':status' header not present");
        }
        q a3 = q.a(str2 + " " + str);
        return new a.ab.a().a(x.SPDY_3).a(a3.f105b).a(a3.c).a(aVar.a());
    }

    private static String a(String str, String str2) {
        return 0 + str2;
    }

    public static a.ab.a b(List<a.a.a.f> list) throws IOException {
        String str = null;
        a.r.a aVar = new a.r.a();
        int size = list.size();
        int i2 = 0;
        while (i2 < size) {
            f fVar = ((a.a.a.f) list.get(i2)).h;
            String a2 = ((a.a.a.f) list.get(i2)).i.a();
            if (!fVar.equals(a.a.a.f.f31a)) {
                if (!l.contains(fVar)) {
                    aVar.a(fVar.a(), a2);
                }
                a2 = str;
            }
            i2++;
            str = a2;
        }
        if (str == null) {
            throw new ProtocolException("Expected ':status' header not present");
        }
        q a3 = q.a("HTTP/1.1 " + str);
        return new a.ab.a().a(x.HTTP_2).a(a3.f105b).a(a3.c).a(aVar.a());
    }

    public static List<a.a.a.f> b(z zVar) {
        r c2 = zVar.c();
        ArrayList arrayList = new ArrayList(c2.a() + 5);
        arrayList.add(new a.a.a.f(a.a.a.f.f32b, zVar.b()));
        arrayList.add(new a.a.a.f(a.a.a.f.c, m.a(zVar.a())));
        arrayList.add(new a.a.a.f(a.a.a.f.g, "HTTP/1.1"));
        arrayList.add(new a.a.a.f(a.a.a.f.f, l.a(zVar.a(), false)));
        arrayList.add(new a.a.a.f(a.a.a.f.d, zVar.a().b()));
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        int a2 = c2.a();
        for (int i2 = 0; i2 < a2; i2++) {
            f a3 = f.a(c2.a(i2).toLowerCase(Locale.US));
            if (!i.contains(a3)) {
                String b2 = c2.b(i2);
                if (linkedHashSet.add(a3)) {
                    arrayList.add(new a.a.a.f(a3, b2));
                } else {
                    int i3 = 0;
                    while (true) {
                        if (i3 >= arrayList.size()) {
                            break;
                        } else if (((a.a.a.f) arrayList.get(i3)).h.equals(a3)) {
                            arrayList.set(i3, new a.a.a.f(a3, a(((a.a.a.f) arrayList.get(i3)).i.a(), b2)));
                            break;
                        } else {
                            i3++;
                        }
                    }
                }
            }
        }
        return arrayList;
    }

    public static List<a.a.a.f> c(z zVar) {
        r c2 = zVar.c();
        ArrayList arrayList = new ArrayList(c2.a() + 4);
        arrayList.add(new a.a.a.f(a.a.a.f.f32b, zVar.b()));
        arrayList.add(new a.a.a.f(a.a.a.f.c, m.a(zVar.a())));
        arrayList.add(new a.a.a.f(a.a.a.f.e, l.a(zVar.a(), false)));
        arrayList.add(new a.a.a.f(a.a.a.f.d, zVar.a().b()));
        int a2 = c2.a();
        for (int i2 = 0; i2 < a2; i2++) {
            f a3 = f.a(c2.a(i2).toLowerCase(Locale.US));
            if (!k.contains(a3)) {
                arrayList.add(new a.a.a.f(a3, c2.b(i2)));
            }
        }
        return arrayList;
    }

    public ac a(ab abVar) throws IOException {
        return new k(abVar.f(), b.l.a((s) new a(this.p.g())));
    }

    public b.r a(z zVar, long j2) throws IOException {
        return this.p.h();
    }

    public void a() {
        if (this.p != null) {
            this.p.b(a.a.a.a.CANCEL);
        }
    }

    public void a(g gVar) {
        this.o = gVar;
    }

    public void a(n nVar) throws IOException {
        nVar.a(this.p.h());
    }

    public void a(z zVar) throws IOException {
        if (this.p == null) {
            this.o.b();
            this.p = this.n.a(this.n.a() == x.HTTP_2 ? c(zVar) : b(zVar), this.o.a(zVar), true);
            this.p.e().a((long) this.o.f88a.b(), TimeUnit.MILLISECONDS);
            this.p.f().a((long) this.o.f88a.c(), TimeUnit.MILLISECONDS);
        }
    }

    public a.ab.a b() throws IOException {
        return this.n.a() == x.HTTP_2 ? b(this.p.d()) : a(this.p.d());
    }

    public void c() throws IOException {
        this.p.h().close();
    }
}
