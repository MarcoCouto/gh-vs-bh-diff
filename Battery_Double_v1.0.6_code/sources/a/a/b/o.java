package a.a.b;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public final class o extends Exception {

    /* renamed from: a reason: collision with root package name */
    private static final Method f100a;

    /* renamed from: b reason: collision with root package name */
    private IOException f101b;

    static {
        Method method;
        try {
            method = Throwable.class.getDeclaredMethod("addSuppressed", new Class[]{Throwable.class});
        } catch (Exception e) {
            method = null;
        }
        f100a = method;
    }

    public o(IOException iOException) {
        super(iOException);
        this.f101b = iOException;
    }

    private void a(IOException iOException, IOException iOException2) {
        if (f100a != null) {
            try {
                f100a.invoke(iOException, new Object[]{iOException2});
            } catch (IllegalAccessException | InvocationTargetException e) {
            }
        }
    }

    public IOException a() {
        return this.f101b;
    }

    public void a(IOException iOException) {
        a(iOException, this.f101b);
        this.f101b = iOException;
    }
}
