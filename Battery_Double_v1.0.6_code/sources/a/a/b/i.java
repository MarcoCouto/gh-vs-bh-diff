package a.a.b;

import a.ab;
import a.ab.a;
import a.ac;
import a.z;
import b.r;
import java.io.IOException;

public interface i {
    ac a(ab abVar) throws IOException;

    r a(z zVar, long j) throws IOException;

    void a();

    void a(g gVar);

    void a(n nVar) throws IOException;

    void a(z zVar) throws IOException;

    a b() throws IOException;

    void c() throws IOException;
}
