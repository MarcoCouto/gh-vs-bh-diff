package a.a.b;

import a.ab;
import a.r;
import a.z;

public final class j {

    /* renamed from: a reason: collision with root package name */
    static final String f94a = a.a.j.c().d();

    /* renamed from: b reason: collision with root package name */
    public static final String f95b = (f94a + "-Sent-Millis");
    public static final String c = (f94a + "-Received-Millis");
    public static final String d = (f94a + "-Selected-Protocol");
    public static final String e = (f94a + "-Response-Source");

    public static long a(ab abVar) {
        return a(abVar.f());
    }

    public static long a(r rVar) {
        return b(rVar.a("Content-Length"));
    }

    public static long a(z zVar) {
        return a(zVar.c());
    }

    static boolean a(String str) {
        return !"Connection".equalsIgnoreCase(str) && !"Keep-Alive".equalsIgnoreCase(str) && !"Proxy-Authenticate".equalsIgnoreCase(str) && !"Proxy-Authorization".equalsIgnoreCase(str) && !"TE".equalsIgnoreCase(str) && !"Trailers".equalsIgnoreCase(str) && !"Transfer-Encoding".equalsIgnoreCase(str) && !"Upgrade".equalsIgnoreCase(str);
    }

    private static long b(String str) {
        long j = -1;
        if (str == null) {
            return j;
        }
        try {
            return Long.parseLong(str);
        } catch (NumberFormatException e2) {
            return j;
        }
    }
}
