package a.a.b;

import a.a;
import a.a.k;
import a.ad;
import a.s;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.Proxy.Type;
import java.net.SocketAddress;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;

public final class p {

    /* renamed from: a reason: collision with root package name */
    private final a f102a;

    /* renamed from: b reason: collision with root package name */
    private final k f103b;
    private Proxy c;
    private InetSocketAddress d;
    private List<Proxy> e = Collections.emptyList();
    private int f;
    private List<InetSocketAddress> g = Collections.emptyList();
    private int h;
    private final List<ad> i = new ArrayList();

    public p(a aVar, k kVar) {
        this.f102a = aVar;
        this.f103b = kVar;
        a(aVar.a(), aVar.h());
    }

    static String a(InetSocketAddress inetSocketAddress) {
        InetAddress address = inetSocketAddress.getAddress();
        return address == null ? inetSocketAddress.getHostName() : address.getHostAddress();
    }

    private void a(s sVar, Proxy proxy) {
        if (proxy != null) {
            this.e = Collections.singletonList(proxy);
        } else {
            this.e = new ArrayList();
            List select = this.f102a.g().select(sVar.a());
            if (select != null) {
                this.e.addAll(select);
            }
            this.e.removeAll(Collections.singleton(Proxy.NO_PROXY));
            this.e.add(Proxy.NO_PROXY);
        }
        this.f = 0;
    }

    private void a(Proxy proxy) throws IOException {
        int i2;
        String str;
        this.g = new ArrayList();
        if (proxy.type() == Type.DIRECT || proxy.type() == Type.SOCKS) {
            String f2 = this.f102a.a().f();
            i2 = this.f102a.a().g();
            str = f2;
        } else {
            SocketAddress address = proxy.address();
            if (!(address instanceof InetSocketAddress)) {
                throw new IllegalArgumentException("Proxy.address() is not an InetSocketAddress: " + address.getClass());
            }
            InetSocketAddress inetSocketAddress = (InetSocketAddress) address;
            String a2 = a(inetSocketAddress);
            i2 = inetSocketAddress.getPort();
            str = a2;
        }
        if (i2 < 1 || i2 > 65535) {
            throw new SocketException("No route to " + str + ":" + i2 + "; port is out of range");
        }
        if (proxy.type() == Type.SOCKS) {
            this.g.add(InetSocketAddress.createUnresolved(str, i2));
        } else {
            List a3 = this.f102a.b().a(str);
            int size = a3.size();
            for (int i3 = 0; i3 < size; i3++) {
                this.g.add(new InetSocketAddress((InetAddress) a3.get(i3), i2));
            }
        }
        this.h = 0;
    }

    private boolean c() {
        return this.f < this.e.size();
    }

    private Proxy d() throws IOException {
        if (!c()) {
            throw new SocketException("No route to " + this.f102a.a().f() + "; exhausted proxy configurations: " + this.e);
        }
        List<Proxy> list = this.e;
        int i2 = this.f;
        this.f = i2 + 1;
        Proxy proxy = (Proxy) list.get(i2);
        a(proxy);
        return proxy;
    }

    private boolean e() {
        return this.h < this.g.size();
    }

    private InetSocketAddress f() throws IOException {
        if (!e()) {
            throw new SocketException("No route to " + this.f102a.a().f() + "; exhausted inet socket addresses: " + this.g);
        }
        List<InetSocketAddress> list = this.g;
        int i2 = this.h;
        this.h = i2 + 1;
        return (InetSocketAddress) list.get(i2);
    }

    private boolean g() {
        return !this.i.isEmpty();
    }

    private ad h() {
        return (ad) this.i.remove(0);
    }

    public void a(ad adVar, IOException iOException) {
        if (!(adVar.b().type() == Type.DIRECT || this.f102a.g() == null)) {
            this.f102a.g().connectFailed(this.f102a.a().a(), adVar.b().address(), iOException);
        }
        this.f103b.a(adVar);
    }

    public boolean a() {
        return e() || c() || g();
    }

    public ad b() throws IOException {
        if (!e()) {
            if (c()) {
                this.c = d();
            } else if (g()) {
                return h();
            } else {
                throw new NoSuchElementException();
            }
        }
        this.d = f();
        ad adVar = new ad(this.f102a, this.c, this.d);
        if (!this.f103b.c(adVar)) {
            return adVar;
        }
        this.i.add(adVar);
        return b();
    }
}
