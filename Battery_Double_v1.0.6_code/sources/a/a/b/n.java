package a.a.b;

import a.a.l;
import b.c;
import b.r;
import b.t;
import java.io.IOException;
import java.net.ProtocolException;

public final class n implements r {

    /* renamed from: a reason: collision with root package name */
    private boolean f98a;

    /* renamed from: b reason: collision with root package name */
    private final int f99b;
    private final c c;

    public n() {
        this(-1);
    }

    public n(int i) {
        this.c = new c();
        this.f99b = i;
    }

    public long a() throws IOException {
        return this.c.a();
    }

    public void a(c cVar, long j) throws IOException {
        if (this.f98a) {
            throw new IllegalStateException("closed");
        }
        l.a(cVar.a(), 0, j);
        if (this.f99b == -1 || this.c.a() <= ((long) this.f99b) - j) {
            this.c.a(cVar, j);
            return;
        }
        throw new ProtocolException("exceeded content-length limit of " + this.f99b + " bytes");
    }

    public void a(r rVar) throws IOException {
        c cVar = new c();
        this.c.a(cVar, 0, this.c.a());
        rVar.a(cVar, cVar.a());
    }

    public void close() throws IOException {
        if (!this.f98a) {
            this.f98a = true;
            if (this.c.a() < ((long) this.f99b)) {
                throw new ProtocolException("content-length promised " + this.f99b + " bytes, but received " + this.c.a());
            }
        }
    }

    public void flush() throws IOException {
    }

    public t timeout() {
        return t.f1394b;
    }
}
