package a.a.b;

import a.s;
import a.z;
import java.net.Proxy.Type;

public final class m {
    public static String a(s sVar) {
        String h = sVar.h();
        String k = sVar.k();
        return k != null ? h + '?' + k : h;
    }

    static String a(z zVar, Type type) {
        StringBuilder sb = new StringBuilder();
        sb.append(zVar.b());
        sb.append(' ');
        if (b(zVar, type)) {
            sb.append(zVar.a());
        } else {
            sb.append(a(zVar.a()));
        }
        sb.append(" HTTP/1.1");
        return sb.toString();
    }

    private static boolean b(z zVar, Type type) {
        return !zVar.g() && type == Type.HTTP;
    }
}
