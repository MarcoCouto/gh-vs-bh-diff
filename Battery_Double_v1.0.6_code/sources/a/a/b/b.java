package a.a.b;

import a.ab;
import a.d;
import a.r;
import a.z;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public final class b {

    /* renamed from: a reason: collision with root package name */
    public final z f71a;

    /* renamed from: b reason: collision with root package name */
    public final ab f72b;

    public static class a {

        /* renamed from: a reason: collision with root package name */
        final long f73a;

        /* renamed from: b reason: collision with root package name */
        final z f74b;
        final ab c;
        private Date d;
        private String e;
        private Date f;
        private String g;
        private Date h;
        private long i;
        private long j;
        private String k;
        private int l = -1;

        public a(long j2, z zVar, ab abVar) {
            this.f73a = j2;
            this.f74b = zVar;
            this.c = abVar;
            if (abVar != null) {
                this.i = abVar.j();
                this.j = abVar.k();
                r f2 = abVar.f();
                int a2 = f2.a();
                for (int i2 = 0; i2 < a2; i2++) {
                    String a3 = f2.a(i2);
                    String b2 = f2.b(i2);
                    if ("Date".equalsIgnoreCase(a3)) {
                        this.d = f.a(b2);
                        this.e = b2;
                    } else if ("Expires".equalsIgnoreCase(a3)) {
                        this.h = f.a(b2);
                    } else if ("Last-Modified".equalsIgnoreCase(a3)) {
                        this.f = f.a(b2);
                        this.g = b2;
                    } else if ("ETag".equalsIgnoreCase(a3)) {
                        this.k = b2;
                    } else if ("Age".equalsIgnoreCase(a3)) {
                        this.l = c.b(b2, -1);
                    }
                }
            }
        }

        private static boolean a(z zVar) {
            return (zVar.a("If-Modified-Since") == null && zVar.a("If-None-Match") == null) ? false : true;
        }

        private b b() {
            long j2 = 0;
            if (this.c == null) {
                return new b(this.f74b, null);
            }
            if (this.f74b.g() && this.c.e() == null) {
                return new b(this.f74b, null);
            }
            if (!b.a(this.c, this.f74b)) {
                return new b(this.f74b, null);
            }
            d f2 = this.f74b.f();
            if (f2.a() || a(this.f74b)) {
                return new b(this.f74b, null);
            }
            long d2 = d();
            long c2 = c();
            if (f2.c() != -1) {
                c2 = Math.min(c2, TimeUnit.SECONDS.toMillis((long) f2.c()));
            }
            long j3 = f2.h() != -1 ? TimeUnit.SECONDS.toMillis((long) f2.h()) : 0;
            d i2 = this.c.i();
            if (!i2.f() && f2.g() != -1) {
                j2 = TimeUnit.SECONDS.toMillis((long) f2.g());
            }
            if (i2.a() || d2 + j3 >= j2 + c2) {
                a.z.a e2 = this.f74b.e();
                if (this.k != null) {
                    e2.a("If-None-Match", this.k);
                } else if (this.f != null) {
                    e2.a("If-Modified-Since", this.g);
                } else if (this.d != null) {
                    e2.a("If-Modified-Since", this.e);
                }
                z a2 = e2.a();
                return a(a2) ? new b(a2, this.c) : new b(a2, null);
            }
            a.ab.a h2 = this.c.h();
            if (j3 + d2 >= c2) {
                h2.a("Warning", "110 HttpURLConnection \"Response is stale\"");
            }
            if (d2 > 86400000 && e()) {
                h2.a("Warning", "113 HttpURLConnection \"Heuristic expiration\"");
            }
            return new b(null, h2.a());
        }

        private long c() {
            d i2 = this.c.i();
            if (i2.c() != -1) {
                return TimeUnit.SECONDS.toMillis((long) i2.c());
            }
            if (this.h != null) {
                long time = this.h.getTime() - (this.d != null ? this.d.getTime() : this.j);
                if (time <= 0) {
                    time = 0;
                }
                return time;
            } else if (this.f == null || this.c.a().a().l() != null) {
                return 0;
            } else {
                long time2 = (this.d != null ? this.d.getTime() : this.i) - this.f.getTime();
                if (time2 > 0) {
                    return time2 / 10;
                }
                return 0;
            }
        }

        private long d() {
            long j2 = 0;
            if (this.d != null) {
                j2 = Math.max(0, this.j - this.d.getTime());
            }
            if (this.l != -1) {
                j2 = Math.max(j2, TimeUnit.SECONDS.toMillis((long) this.l));
            }
            return j2 + (this.j - this.i) + (this.f73a - this.j);
        }

        private boolean e() {
            return this.c.i().c() == -1 && this.h == null;
        }

        public b a() {
            b b2 = b();
            return (b2.f71a == null || !this.f74b.f().i()) ? b2 : new b(null, null);
        }
    }

    private b(z zVar, ab abVar) {
        this.f71a = zVar;
        this.f72b = abVar;
    }

    public static boolean a(ab abVar, z zVar) {
        switch (abVar.b()) {
            case 200:
            case 203:
            case 204:
            case 300:
            case 301:
            case 308:
            case 404:
            case 405:
            case 410:
            case 414:
            case 501:
                break;
            case 302:
            case 307:
                if (abVar.a("Expires") == null && abVar.i().c() == -1 && !abVar.i().e() && !abVar.i().d()) {
                    return false;
                }
            default:
                return false;
        }
        return !abVar.i().b() && !zVar.f().b();
    }
}
