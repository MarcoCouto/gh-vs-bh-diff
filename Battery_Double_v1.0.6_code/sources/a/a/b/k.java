package a.a.b;

import a.ac;
import a.r;
import a.u;
import b.e;

public final class k extends ac {

    /* renamed from: a reason: collision with root package name */
    private final r f96a;

    /* renamed from: b reason: collision with root package name */
    private final e f97b;

    public k(r rVar, e eVar) {
        this.f96a = rVar;
        this.f97b = eVar;
    }

    public long contentLength() {
        return j.a(this.f96a);
    }

    public u contentType() {
        String a2 = this.f96a.a("Content-Type");
        if (a2 != null) {
            return u.a(a2);
        }
        return null;
    }

    public e source() {
        return this.f97b;
    }
}
