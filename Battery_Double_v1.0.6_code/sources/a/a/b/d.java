package a.a.b;

import a.a.l;
import a.ab;
import a.ac;
import a.z;
import b.i;
import b.r;
import b.s;
import b.t;
import java.io.EOFException;
import java.io.IOException;
import java.net.ProtocolException;
import java.util.concurrent.TimeUnit;

public final class d implements i {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public final r f75a;
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public final b.e f76b;
    /* access modifiers changed from: private */
    public final b.d c;
    private g d;
    /* access modifiers changed from: private */
    public int e = 0;

    private abstract class a implements s {

        /* renamed from: a reason: collision with root package name */
        protected final i f77a;

        /* renamed from: b reason: collision with root package name */
        protected boolean f78b;

        private a() {
            this.f77a = new i(d.this.f76b.timeout());
        }

        /* access modifiers changed from: protected */
        public final void a(boolean z) throws IOException {
            if (d.this.e != 6) {
                if (d.this.e != 5) {
                    throw new IllegalStateException("state: " + d.this.e);
                }
                d.this.a(this.f77a);
                d.this.e = 6;
                if (d.this.f75a != null) {
                    d.this.f75a.a(!z, d.this);
                }
            }
        }

        public t timeout() {
            return this.f77a;
        }
    }

    private final class b implements r {

        /* renamed from: b reason: collision with root package name */
        private final i f80b;
        private boolean c;

        private b() {
            this.f80b = new i(d.this.c.timeout());
        }

        public void a(b.c cVar, long j) throws IOException {
            if (this.c) {
                throw new IllegalStateException("closed");
            } else if (j != 0) {
                d.this.c.j(j);
                d.this.c.b("\r\n");
                d.this.c.a(cVar, j);
                d.this.c.b("\r\n");
            }
        }

        public synchronized void close() throws IOException {
            if (!this.c) {
                this.c = true;
                d.this.c.b("0\r\n\r\n");
                d.this.a(this.f80b);
                d.this.e = 3;
            }
        }

        public synchronized void flush() throws IOException {
            if (!this.c) {
                d.this.c.flush();
            }
        }

        public t timeout() {
            return this.f80b;
        }
    }

    private class c extends a {
        private long e = -1;
        private boolean f = true;
        private final g g;

        c(g gVar) throws IOException {
            super();
            this.g = gVar;
        }

        private void a() throws IOException {
            if (this.e != -1) {
                d.this.f76b.q();
            }
            try {
                this.e = d.this.f76b.n();
                String trim = d.this.f76b.q().trim();
                if (this.e < 0 || (!trim.isEmpty() && !trim.startsWith(";"))) {
                    throw new ProtocolException("expected chunk size and optional extensions but was \"" + this.e + trim + "\"");
                } else if (this.e == 0) {
                    this.f = false;
                    this.g.a(d.this.e());
                    a(true);
                }
            } catch (NumberFormatException e2) {
                throw new ProtocolException(e2.getMessage());
            }
        }

        public void close() throws IOException {
            if (!this.f78b) {
                if (this.f && !l.a((s) this, 100, TimeUnit.MILLISECONDS)) {
                    a(false);
                }
                this.f78b = true;
            }
        }

        public long read(b.c cVar, long j) throws IOException {
            if (j < 0) {
                throw new IllegalArgumentException("byteCount < 0: " + j);
            } else if (this.f78b) {
                throw new IllegalStateException("closed");
            } else if (!this.f) {
                return -1;
            } else {
                if (this.e == 0 || this.e == -1) {
                    a();
                    if (!this.f) {
                        return -1;
                    }
                }
                long read = d.this.f76b.read(cVar, Math.min(j, this.e));
                if (read == -1) {
                    a(false);
                    throw new ProtocolException("unexpected end of stream");
                }
                this.e -= read;
                return read;
            }
        }
    }

    /* renamed from: a.a.b.d$d reason: collision with other inner class name */
    private final class C0000d implements r {

        /* renamed from: b reason: collision with root package name */
        private final i f82b;
        private boolean c;
        private long d;

        private C0000d(long j) {
            this.f82b = new i(d.this.c.timeout());
            this.d = j;
        }

        public void a(b.c cVar, long j) throws IOException {
            if (this.c) {
                throw new IllegalStateException("closed");
            }
            l.a(cVar.a(), 0, j);
            if (j > this.d) {
                throw new ProtocolException("expected " + this.d + " bytes but received " + j);
            }
            d.this.c.a(cVar, j);
            this.d -= j;
        }

        public void close() throws IOException {
            if (!this.c) {
                this.c = true;
                if (this.d > 0) {
                    throw new ProtocolException("unexpected end of stream");
                }
                d.this.a(this.f82b);
                d.this.e = 3;
            }
        }

        public void flush() throws IOException {
            if (!this.c) {
                d.this.c.flush();
            }
        }

        public t timeout() {
            return this.f82b;
        }
    }

    private class e extends a {
        private long e;

        public e(long j) throws IOException {
            super();
            this.e = j;
            if (this.e == 0) {
                a(true);
            }
        }

        public void close() throws IOException {
            if (!this.f78b) {
                if (this.e != 0 && !l.a((s) this, 100, TimeUnit.MILLISECONDS)) {
                    a(false);
                }
                this.f78b = true;
            }
        }

        public long read(b.c cVar, long j) throws IOException {
            if (j < 0) {
                throw new IllegalArgumentException("byteCount < 0: " + j);
            } else if (this.f78b) {
                throw new IllegalStateException("closed");
            } else if (this.e == 0) {
                return -1;
            } else {
                long read = d.this.f76b.read(cVar, Math.min(this.e, j));
                if (read == -1) {
                    a(false);
                    throw new ProtocolException("unexpected end of stream");
                }
                this.e -= read;
                if (this.e == 0) {
                    a(true);
                }
                return read;
            }
        }
    }

    private class f extends a {
        private boolean e;

        private f() {
            super();
        }

        public void close() throws IOException {
            if (!this.f78b) {
                if (!this.e) {
                    a(false);
                }
                this.f78b = true;
            }
        }

        public long read(b.c cVar, long j) throws IOException {
            if (j < 0) {
                throw new IllegalArgumentException("byteCount < 0: " + j);
            } else if (this.f78b) {
                throw new IllegalStateException("closed");
            } else if (this.e) {
                return -1;
            } else {
                long read = d.this.f76b.read(cVar, j);
                if (read != -1) {
                    return read;
                }
                this.e = true;
                a(true);
                return -1;
            }
        }
    }

    public d(r rVar, b.e eVar, b.d dVar) {
        this.f75a = rVar;
        this.f76b = eVar;
        this.c = dVar;
    }

    /* access modifiers changed from: private */
    public void a(i iVar) {
        t a2 = iVar.a();
        iVar.a(t.f1394b);
        a2.f();
        a2.E_();
    }

    private s b(ab abVar) throws IOException {
        if (!g.a(abVar)) {
            return b(0);
        }
        if ("chunked".equalsIgnoreCase(abVar.a("Transfer-Encoding"))) {
            return b(this.d);
        }
        long a2 = j.a(abVar);
        return a2 != -1 ? b(a2) : g();
    }

    public ac a(ab abVar) throws IOException {
        return new k(abVar.f(), b.l.a(b(abVar)));
    }

    public r a(long j) {
        if (this.e != 1) {
            throw new IllegalStateException("state: " + this.e);
        }
        this.e = 2;
        return new C0000d(j);
    }

    public r a(z zVar, long j) throws IOException {
        if ("chunked".equalsIgnoreCase(zVar.a("Transfer-Encoding"))) {
            return f();
        }
        if (j != -1) {
            return a(j);
        }
        throw new IllegalStateException("Cannot stream a request body without chunked encoding or a known content length!");
    }

    public void a() {
        a.a.c.b b2 = this.f75a.b();
        if (b2 != null) {
            b2.b();
        }
    }

    public void a(g gVar) {
        this.d = gVar;
    }

    public void a(n nVar) throws IOException {
        if (this.e != 1) {
            throw new IllegalStateException("state: " + this.e);
        }
        this.e = 3;
        nVar.a(this.c);
    }

    public void a(a.r rVar, String str) throws IOException {
        if (this.e != 0) {
            throw new IllegalStateException("state: " + this.e);
        }
        this.c.b(str).b("\r\n");
        int a2 = rVar.a();
        for (int i = 0; i < a2; i++) {
            this.c.b(rVar.a(i)).b(": ").b(rVar.b(i)).b("\r\n");
        }
        this.c.b("\r\n");
        this.e = 1;
    }

    public void a(z zVar) throws IOException {
        this.d.b();
        a(zVar.c(), m.a(zVar, this.d.d().a().b().type()));
    }

    public a.ab.a b() throws IOException {
        return d();
    }

    public s b(long j) throws IOException {
        if (this.e != 4) {
            throw new IllegalStateException("state: " + this.e);
        }
        this.e = 5;
        return new e(j);
    }

    public s b(g gVar) throws IOException {
        if (this.e != 4) {
            throw new IllegalStateException("state: " + this.e);
        }
        this.e = 5;
        return new c(gVar);
    }

    public void c() throws IOException {
        this.c.flush();
    }

    public a.ab.a d() throws IOException {
        q a2;
        a.ab.a a3;
        if (this.e == 1 || this.e == 3) {
            do {
                try {
                    a2 = q.a(this.f76b.q());
                    a3 = new a.ab.a().a(a2.f104a).a(a2.f105b).a(a2.c).a(e());
                } catch (EOFException e2) {
                    IOException iOException = new IOException("unexpected end of stream on " + this.f75a);
                    iOException.initCause(e2);
                    throw iOException;
                }
            } while (a2.f105b == 100);
            this.e = 4;
            return a3;
        }
        throw new IllegalStateException("state: " + this.e);
    }

    public a.r e() throws IOException {
        a.r.a aVar = new a.r.a();
        while (true) {
            String q = this.f76b.q();
            if (q.length() == 0) {
                return aVar.a();
            }
            a.a.d.f116a.a(aVar, q);
        }
    }

    public r f() {
        if (this.e != 1) {
            throw new IllegalStateException("state: " + this.e);
        }
        this.e = 2;
        return new b();
    }

    public s g() throws IOException {
        if (this.e != 4) {
            throw new IllegalStateException("state: " + this.e);
        } else if (this.f75a == null) {
            throw new IllegalStateException("streamAllocation == null");
        } else {
            this.e = 5;
            this.f75a.d();
            return new f();
        }
    }
}
