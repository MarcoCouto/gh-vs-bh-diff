package a.a.b;

import a.a.c.b;
import a.a.m;
import a.aa;
import a.ab;
import a.ac;
import a.ad;
import a.i;
import a.t;
import a.u;
import a.w;
import a.x;
import a.z;
import b.c;
import b.d;
import b.e;
import b.j;
import b.l;
import b.r;
import b.s;
import java.io.Closeable;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.ProtocolException;
import java.net.Proxy.Type;
import java.net.SocketTimeoutException;
import java.security.cert.CertificateException;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSocketFactory;

public final class g {
    private static final ac e = new ac() {
        public long contentLength() {
            return 0;
        }

        public u contentType() {
            return null;
        }

        public e source() {
            return new c();
        }
    };

    /* renamed from: a reason: collision with root package name */
    final w f88a;

    /* renamed from: b reason: collision with root package name */
    public final r f89b;
    long c = -1;
    public final boolean d;
    private final ab f;
    /* access modifiers changed from: private */
    public i g;
    private boolean h;
    private final z i;
    /* access modifiers changed from: private */
    public z j;
    private ab k;
    private ab l;
    private r m;
    private d n;
    private final boolean o;
    private final boolean p;
    private a q;
    private b r;

    class a implements a.t.a {

        /* renamed from: b reason: collision with root package name */
        private final int f93b;
        private final z c;
        private final i d;
        private int e;

        a(int i, z zVar, i iVar) {
            this.f93b = i;
            this.c = zVar;
            this.d = iVar;
        }

        public ab a(z zVar) throws IOException {
            this.e++;
            if (this.f93b > 0) {
                t tVar = (t) g.this.f88a.w().get(this.f93b - 1);
                a.a a2 = a().a().a();
                if (!zVar.a().f().equals(a2.a().f()) || zVar.a().g() != a2.a().g()) {
                    throw new IllegalStateException("network interceptor " + tVar + " must retain the same host and port");
                } else if (this.e > 1) {
                    throw new IllegalStateException("network interceptor " + tVar + " must call proceed() exactly once");
                }
            }
            if (this.f93b < g.this.f88a.w().size()) {
                a aVar = new a(this.f93b + 1, zVar, this.d);
                t tVar2 = (t) g.this.f88a.w().get(this.f93b);
                ab a3 = tVar2.a(aVar);
                if (aVar.e != 1) {
                    throw new IllegalStateException("network interceptor " + tVar2 + " must call proceed() exactly once");
                } else if (a3 != null) {
                    return a3;
                } else {
                    throw new NullPointerException("network interceptor " + tVar2 + " returned null");
                }
            } else {
                g.this.g.a(zVar);
                g.this.j = zVar;
                if (g.this.a(zVar) && zVar.d() != null) {
                    d a4 = l.a(g.this.g.a(zVar, zVar.d().contentLength()));
                    zVar.d().writeTo(a4);
                    a4.close();
                }
                ab b2 = g.this.m();
                int b3 = b2.b();
                if ((b3 != 204 && b3 != 205) || b2.g().contentLength() <= 0) {
                    return b2;
                }
                throw new ProtocolException("HTTP " + b3 + " had non-zero Content-Length: " + b2.g().contentLength());
            }
        }

        public i a() {
            return this.d;
        }
    }

    public g(w wVar, z zVar, boolean z, boolean z2, boolean z3, r rVar, n nVar, ab abVar) {
        this.f88a = wVar;
        this.i = zVar;
        this.d = z;
        this.o = z2;
        this.p = z3;
        if (rVar == null) {
            rVar = new r(wVar.o(), a(wVar, zVar));
        }
        this.f89b = rVar;
        this.m = nVar;
        this.f = abVar;
    }

    private static a.a a(w wVar, z zVar) {
        HostnameVerifier hostnameVerifier;
        SSLSocketFactory sSLSocketFactory;
        a.g gVar = null;
        if (zVar.g()) {
            sSLSocketFactory = wVar.j();
            hostnameVerifier = wVar.k();
            gVar = wVar.l();
        } else {
            hostnameVerifier = null;
            sSLSocketFactory = null;
        }
        return new a.a(zVar.a().f(), zVar.a().g(), wVar.h(), wVar.i(), sSLSocketFactory, hostnameVerifier, gVar, wVar.n(), wVar.d(), wVar.t(), wVar.u(), wVar.e());
    }

    private ab a(final a aVar, ab abVar) throws IOException {
        if (aVar == null) {
            return abVar;
        }
        r a2 = aVar.a();
        if (a2 == null) {
            return abVar;
        }
        final e source = abVar.g().source();
        final d a3 = l.a(a2);
        return abVar.h().a((ac) new k(abVar.f(), l.a((s) new s() {

            /* renamed from: a reason: collision with root package name */
            boolean f90a;

            public void close() throws IOException {
                if (!this.f90a && !a.a.l.a((s) this, 100, TimeUnit.MILLISECONDS)) {
                    this.f90a = true;
                    aVar.b();
                }
                source.close();
            }

            public long read(c cVar, long j) throws IOException {
                try {
                    long read = source.read(cVar, j);
                    if (read == -1) {
                        if (!this.f90a) {
                            this.f90a = true;
                            a3.close();
                        }
                        return -1;
                    }
                    cVar.a(a3.b(), cVar.a() - read, read);
                    a3.v();
                    return read;
                } catch (IOException e2) {
                    if (!this.f90a) {
                        this.f90a = true;
                        aVar.b();
                    }
                    throw e2;
                }
            }

            public b.t timeout() {
                return source.timeout();
            }
        }))).a();
    }

    private static a.r a(a.r rVar, a.r rVar2) throws IOException {
        a.r.a aVar = new a.r.a();
        int a2 = rVar.a();
        for (int i2 = 0; i2 < a2; i2++) {
            String a3 = rVar.a(i2);
            String b2 = rVar.b(i2);
            if ((!"Warning".equalsIgnoreCase(a3) || !b2.startsWith("1")) && (!j.a(a3) || rVar2.a(a3) == null)) {
                aVar.a(a3, b2);
            }
        }
        int a4 = rVar2.a();
        for (int i3 = 0; i3 < a4; i3++) {
            String a5 = rVar2.a(i3);
            if (!"Content-Length".equalsIgnoreCase(a5) && j.a(a5)) {
                aVar.a(a5, rVar2.b(i3));
            }
        }
        return aVar.a();
    }

    private String a(List<a.l> list) {
        StringBuilder sb = new StringBuilder();
        int size = list.size();
        for (int i2 = 0; i2 < size; i2++) {
            if (i2 > 0) {
                sb.append("; ");
            }
            a.l lVar = (a.l) list.get(i2);
            sb.append(lVar.a()).append('=').append(lVar.b());
        }
        return sb.toString();
    }

    public static boolean a(ab abVar) {
        if (abVar.a().b().equals("HEAD")) {
            return false;
        }
        int b2 = abVar.b();
        if ((b2 >= 100 && b2 < 200) || b2 == 204 || b2 == 304) {
            return j.a(abVar) != -1 || "chunked".equalsIgnoreCase(abVar.a("Transfer-Encoding"));
        }
        return true;
    }

    private static boolean a(ab abVar, ab abVar2) {
        if (abVar2.b() == 304) {
            return true;
        }
        Date b2 = abVar.f().b("Last-Modified");
        if (b2 != null) {
            Date b3 = abVar2.f().b("Last-Modified");
            if (b3 != null && b3.getTime() < b2.getTime()) {
                return true;
            }
        }
        return false;
    }

    private boolean a(IOException iOException, boolean z) {
        boolean z2 = true;
        if (iOException instanceof ProtocolException) {
            return false;
        }
        if (!(iOException instanceof InterruptedIOException)) {
            return (!(iOException instanceof SSLHandshakeException) || !(iOException.getCause() instanceof CertificateException)) && !(iOException instanceof SSLPeerUnverifiedException);
        }
        if (!(iOException instanceof SocketTimeoutException) || !z) {
            z2 = false;
        }
        return z2;
    }

    private static ab b(ab abVar) {
        return (abVar == null || abVar.g() == null) ? abVar : abVar.h().a((ac) null).a();
    }

    private z b(z zVar) throws IOException {
        a.z.a e2 = zVar.e();
        if (zVar.a("Host") == null) {
            e2.a("Host", a.a.l.a(zVar.a(), false));
        }
        if (zVar.a("Connection") == null) {
            e2.a("Connection", "Keep-Alive");
        }
        if (zVar.a("Accept-Encoding") == null) {
            this.h = true;
            e2.a("Accept-Encoding", "gzip");
        }
        List a2 = this.f88a.f().a(zVar.a());
        if (!a2.isEmpty()) {
            e2.a("Cookie", a(a2));
        }
        if (zVar.a("User-Agent") == null) {
            e2.a("User-Agent", m.a());
        }
        return e2.a();
    }

    private ab c(ab abVar) throws IOException {
        if (!this.h || !"gzip".equalsIgnoreCase(this.l.a("Content-Encoding")) || abVar.g() == null) {
            return abVar;
        }
        j jVar = new j(abVar.g().source());
        a.r a2 = abVar.f().b().b("Content-Encoding").b("Content-Length").a();
        return abVar.h().a(a2).a((ac) new k(a2, l.a((s) jVar))).a();
    }

    private boolean j() {
        return this.o && a(this.j) && this.m == null;
    }

    private i k() throws o, l, IOException {
        return this.f89b.a(this.f88a.a(), this.f88a.b(), this.f88a.c(), this.f88a.r(), !this.j.b().equals("GET"));
    }

    private void l() throws IOException {
        a.a.e a2 = a.a.d.f116a.a(this.f88a);
        if (a2 != null) {
            if (b.a(this.l, this.j)) {
                this.q = a2.a(this.l);
            } else if (h.a(this.j.b())) {
                try {
                    a2.b(this.j);
                } catch (IOException e2) {
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public ab m() throws IOException {
        this.g.c();
        ab a2 = this.g.b().a(this.j).a(this.f89b.b().d()).a(this.c).b(System.currentTimeMillis()).a();
        if (!this.p) {
            a2 = a2.h().a(this.g.a(a2)).a();
        }
        if ("close".equalsIgnoreCase(a2.a().a("Connection")) || "close".equalsIgnoreCase(a2.a("Connection"))) {
            this.f89b.d();
        }
        return a2;
    }

    public g a(IOException iOException, boolean z, r rVar) {
        this.f89b.a(iOException);
        if (!this.f88a.r()) {
            return null;
        }
        if ((rVar != null && !(rVar instanceof n)) || !a(iOException, z) || !this.f89b.f()) {
            return null;
        }
        return new g(this.f88a, this.i, this.d, this.o, this.p, g(), (n) rVar, this.f);
    }

    public void a() throws l, o, IOException {
        if (this.r == null) {
            if (this.g != null) {
                throw new IllegalStateException();
            }
            z b2 = b(this.i);
            a.a.e a2 = a.a.d.f116a.a(this.f88a);
            ab abVar = a2 != null ? a2.a(b2) : null;
            this.r = new a.a.b.b.a(System.currentTimeMillis(), b2, abVar).a();
            this.j = this.r.f71a;
            this.k = this.r.f72b;
            if (a2 != null) {
                a2.a(this.r);
            }
            if (abVar != null && this.k == null) {
                a.a.l.a((Closeable) abVar.g());
            }
            if (this.j == null && this.k == null) {
                this.l = new a.ab.a().a(this.i).c(b(this.f)).a(x.HTTP_1_1).a(504).a("Unsatisfiable Request (only-if-cached)").a(e).a(this.c).b(System.currentTimeMillis()).a();
            } else if (this.j == null) {
                this.l = this.k.h().a(this.i).c(b(this.f)).b(b(this.k)).a();
                this.l = c(this.l);
            } else {
                try {
                    this.g = k();
                    this.g.a(this);
                    if (j()) {
                        long a3 = j.a(b2);
                        if (!this.d) {
                            this.g.a(this.j);
                            this.m = this.g.a(this.j, a3);
                        } else if (a3 > 2147483647L) {
                            throw new IllegalStateException("Use setFixedLengthStreamingMode() or setChunkedStreamingMode() for requests larger than 2 GiB.");
                        } else if (a3 != -1) {
                            this.g.a(this.j);
                            this.m = new n((int) a3);
                        } else {
                            this.m = new n();
                        }
                    }
                } catch (Throwable th) {
                    if (abVar != null) {
                        a.a.l.a((Closeable) abVar.g());
                    }
                    throw th;
                }
            }
        }
    }

    public void a(a.r rVar) throws IOException {
        if (this.f88a.f() != a.m.f181a) {
            List a2 = a.l.a(this.i.a(), rVar);
            if (!a2.isEmpty()) {
                this.f88a.f().a(this.i.a(), a2);
            }
        }
    }

    public boolean a(a.s sVar) {
        a.s a2 = this.i.a();
        return a2.f().equals(sVar.f()) && a2.g() == sVar.g() && a2.b().equals(sVar.b());
    }

    /* access modifiers changed from: 0000 */
    public boolean a(z zVar) {
        return h.c(zVar.b());
    }

    public void b() {
        if (this.c != -1) {
            throw new IllegalStateException();
        }
        this.c = System.currentTimeMillis();
    }

    public ab c() {
        if (this.l != null) {
            return this.l;
        }
        throw new IllegalStateException();
    }

    public i d() {
        return this.f89b.b();
    }

    public void e() throws IOException {
        this.f89b.c();
    }

    public void f() {
        this.f89b.e();
    }

    public r g() {
        if (this.n != null) {
            a.a.l.a((Closeable) this.n);
        } else if (this.m != null) {
            a.a.l.a((Closeable) this.m);
        }
        if (this.l != null) {
            a.a.l.a((Closeable) this.l.g());
        } else {
            this.f89b.a((IOException) null);
        }
        return this.f89b;
    }

    public void h() throws IOException {
        ab m2;
        if (this.l == null) {
            if (this.j == null && this.k == null) {
                throw new IllegalStateException("call sendRequest() first!");
            } else if (this.j != null) {
                if (this.p) {
                    this.g.a(this.j);
                    m2 = m();
                } else if (!this.o) {
                    m2 = new a(0, this.j, this.f89b.b()).a(this.j);
                } else {
                    if (this.n != null && this.n.b().a() > 0) {
                        this.n.e();
                    }
                    if (this.c == -1) {
                        if (j.a(this.j) == -1 && (this.m instanceof n)) {
                            this.j = this.j.e().a("Content-Length", Long.toString(((n) this.m).a())).a();
                        }
                        this.g.a(this.j);
                    }
                    if (this.m != null) {
                        if (this.n != null) {
                            this.n.close();
                        } else {
                            this.m.close();
                        }
                        if (this.m instanceof n) {
                            this.g.a((n) this.m);
                        }
                    }
                    m2 = m();
                }
                a(m2.f());
                if (this.k != null) {
                    if (a(this.k, m2)) {
                        this.l = this.k.h().a(this.i).c(b(this.f)).a(a(this.k.f(), m2.f())).b(b(this.k)).a(b(m2)).a();
                        m2.g().close();
                        e();
                        a.a.e a2 = a.a.d.f116a.a(this.f88a);
                        a2.a();
                        a2.a(this.k, this.l);
                        this.l = c(this.l);
                        return;
                    }
                    a.a.l.a((Closeable) this.k.g());
                }
                this.l = m2.h().a(this.i).c(b(this.f)).b(b(this.k)).a(b(m2)).a();
                if (a(this.l)) {
                    l();
                    this.l = c(a(this.q, this.l));
                }
            }
        }
    }

    public z i() throws IOException {
        if (this.l == null) {
            throw new IllegalStateException();
        }
        b b2 = this.f89b.b();
        ad adVar = b2 != null ? b2.a() : null;
        int b3 = this.l.b();
        String b4 = this.i.b();
        switch (b3) {
            case 300:
            case 301:
            case 302:
            case 303:
                break;
            case 307:
            case 308:
                if (!b4.equals("GET") && !b4.equals("HEAD")) {
                    return null;
                }
            case 401:
                return this.f88a.m().a(adVar, this.l);
            case 407:
                if ((adVar != null ? adVar.b() : this.f88a.d()).type() == Type.HTTP) {
                    return this.f88a.n().a(adVar, this.l);
                }
                throw new ProtocolException("Received HTTP_PROXY_AUTH (407) code while not using proxy");
            case 408:
                boolean z = this.m == null || (this.m instanceof n);
                if (!this.o || z) {
                    return this.i;
                }
                return null;
            default:
                return null;
        }
        if (!this.f88a.q()) {
            return null;
        }
        String a2 = this.l.a("Location");
        if (a2 == null) {
            return null;
        }
        a.s c2 = this.i.a().c(a2);
        if (c2 == null) {
            return null;
        }
        if (!c2.b().equals(this.i.a().b()) && !this.f88a.p()) {
            return null;
        }
        a.z.a e2 = this.i.e();
        if (h.c(b4)) {
            if (h.d(b4)) {
                e2.a("GET", (aa) null);
            } else {
                e2.a(b4, (aa) null);
            }
            e2.b("Transfer-Encoding");
            e2.b("Content-Length");
            e2.b("Content-Type");
        }
        if (!a(c2)) {
            e2.b("Authorization");
        }
        return e2.a(c2).a();
    }
}
