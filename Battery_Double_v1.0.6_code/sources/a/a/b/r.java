package a.a.b;

import a.a;
import a.a.a.p;
import a.a.c.b;
import a.a.d;
import a.a.k;
import a.a.l;
import a.ad;
import a.j;
import java.io.IOException;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.concurrent.TimeUnit;

public final class r {

    /* renamed from: a reason: collision with root package name */
    public final a f106a;

    /* renamed from: b reason: collision with root package name */
    private ad f107b;
    private final j c;
    private final p d;
    private int e;
    private b f;
    private boolean g;
    private boolean h;
    private i i;

    public r(j jVar, a aVar) {
        this.c = jVar;
        this.f106a = aVar;
        this.d = new p(aVar, g());
    }

    private b a(int i2, int i3, int i4, boolean z) throws IOException, o {
        b bVar;
        ad adVar;
        synchronized (this.c) {
            if (this.g) {
                throw new IllegalStateException("released");
            } else if (this.i != null) {
                throw new IllegalStateException("stream != null");
            } else if (this.h) {
                throw new IOException("Canceled");
            } else {
                bVar = this.f;
                if (bVar == null || bVar.i) {
                    bVar = d.f116a.a(this.c, this.f106a, this);
                    if (bVar != null) {
                        this.f = bVar;
                    } else {
                        ad adVar2 = this.f107b;
                        if (adVar2 == null) {
                            ad b2 = this.d.b();
                            synchronized (this.c) {
                                this.f107b = b2;
                                this.e = 0;
                            }
                            adVar = b2;
                        } else {
                            adVar = adVar2;
                        }
                        bVar = new b(adVar);
                        a(bVar);
                        synchronized (this.c) {
                            d.f116a.b(this.c, bVar);
                            this.f = bVar;
                            if (this.h) {
                                throw new IOException("Canceled");
                            }
                        }
                        bVar.a(i2, i3, i4, this.f106a.f(), z);
                        g().b(bVar.a());
                    }
                }
            }
        }
        return bVar;
    }

    private void a(boolean z, boolean z2, boolean z3) {
        b bVar = null;
        synchronized (this.c) {
            if (z3) {
                this.i = null;
            }
            if (z2) {
                this.g = true;
            }
            if (this.f != null) {
                if (z) {
                    this.f.i = true;
                }
                if (this.i == null && (this.g || this.f.i)) {
                    b(this.f);
                    if (this.f.h.isEmpty()) {
                        this.f.j = System.nanoTime();
                        if (d.f116a.a(this.c, this.f)) {
                            bVar = this.f;
                        }
                    }
                    this.f = null;
                }
            }
        }
        if (bVar != null) {
            l.a(bVar.c());
        }
    }

    private b b(int i2, int i3, int i4, boolean z, boolean z2) throws IOException, o {
        b a2;
        while (true) {
            a2 = a(i2, i3, i4, z);
            synchronized (this.c) {
                if (a2.d != 0) {
                    if (a2.a(z2)) {
                        break;
                    }
                    d();
                } else {
                    break;
                }
            }
        }
        return a2;
    }

    private void b(b bVar) {
        int size = bVar.h.size();
        for (int i2 = 0; i2 < size; i2++) {
            if (((Reference) bVar.h.get(i2)).get() == this) {
                bVar.h.remove(i2);
                return;
            }
        }
        throw new IllegalStateException();
    }

    private k g() {
        return d.f116a.a(this.c);
    }

    public i a() {
        i iVar;
        synchronized (this.c) {
            iVar = this.i;
        }
        return iVar;
    }

    public i a(int i2, int i3, int i4, boolean z, boolean z2) throws o, IOException {
        i dVar;
        try {
            b b2 = b(i2, i3, i4, z, z2);
            if (b2.c != null) {
                dVar = new e(this, b2.c);
            } else {
                b2.c().setSoTimeout(i3);
                b2.e.timeout().a((long) i3, TimeUnit.MILLISECONDS);
                b2.f.timeout().a((long) i4, TimeUnit.MILLISECONDS);
                dVar = new d(this, b2.e, b2.f);
            }
            synchronized (this.c) {
                this.i = dVar;
            }
            return dVar;
        } catch (IOException e2) {
            throw new o(e2);
        }
    }

    public void a(b bVar) {
        bVar.h.add(new WeakReference(this));
    }

    public void a(IOException iOException) {
        boolean z;
        synchronized (this.c) {
            if (iOException instanceof p) {
                p pVar = (p) iOException;
                if (pVar.f68a == a.a.a.a.REFUSED_STREAM) {
                    this.e++;
                }
                if (pVar.f68a != a.a.a.a.REFUSED_STREAM || this.e > 1) {
                    this.f107b = null;
                }
                z = false;
            } else {
                if (this.f != null && !this.f.e()) {
                    if (this.f.d == 0) {
                        if (!(this.f107b == null || iOException == null)) {
                            this.d.a(this.f107b, iOException);
                        }
                        this.f107b = null;
                    }
                }
                z = false;
            }
            z = true;
        }
        a(z, false, true);
    }

    public void a(boolean z, i iVar) {
        synchronized (this.c) {
            if (iVar != null) {
                if (iVar == this.i) {
                    if (!z) {
                        this.f.d++;
                    }
                }
            }
            throw new IllegalStateException("expected " + this.i + " but was " + iVar);
        }
        a(z, false, true);
    }

    public synchronized b b() {
        return this.f;
    }

    public void c() {
        a(false, true, false);
    }

    public void d() {
        a(true, false, false);
    }

    public void e() {
        i iVar;
        b bVar;
        synchronized (this.c) {
            this.h = true;
            iVar = this.i;
            bVar = this.f;
        }
        if (iVar != null) {
            iVar.a();
        } else if (bVar != null) {
            bVar.b();
        }
    }

    public boolean f() {
        return this.f107b != null || this.d.a();
    }

    public String toString() {
        return this.f106a.toString();
    }
}
