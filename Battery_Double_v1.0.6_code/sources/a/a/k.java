package a.a;

import a.ad;
import java.util.LinkedHashSet;
import java.util.Set;

public final class k {

    /* renamed from: a reason: collision with root package name */
    private final Set<ad> f137a = new LinkedHashSet();

    public synchronized void a(ad adVar) {
        this.f137a.add(adVar);
    }

    public synchronized void b(ad adVar) {
        this.f137a.remove(adVar);
    }

    public synchronized boolean c(ad adVar) {
        return this.f137a.contains(adVar);
    }
}
