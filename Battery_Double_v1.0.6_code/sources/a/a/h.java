package a.a;

public abstract class h implements Runnable {

    /* renamed from: b reason: collision with root package name */
    protected final String f132b;

    public h(String str, Object... objArr) {
        this.f132b = l.a(str, objArr);
    }

    /* access modifiers changed from: protected */
    public abstract void b();

    public final void run() {
        String name = Thread.currentThread().getName();
        Thread.currentThread().setName(this.f132b);
        try {
            b();
        } finally {
            Thread.currentThread().setName(name);
        }
    }
}
