package a.a;

import a.w;
import a.x;
import b.c;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.SSLSocket;

public class j {

    /* renamed from: a reason: collision with root package name */
    private static final j f135a = b();

    /* renamed from: b reason: collision with root package name */
    private static final Logger f136b = Logger.getLogger(w.class.getName());

    public static List<String> a(List<x> list) {
        ArrayList arrayList = new ArrayList(list.size());
        int size = list.size();
        for (int i = 0; i < size; i++) {
            x xVar = (x) list.get(i);
            if (xVar != x.HTTP_1_0) {
                arrayList.add(xVar.toString());
            }
        }
        return arrayList;
    }

    private static j b() {
        j b2 = a.b();
        if (b2 != null) {
            return b2;
        }
        f b3 = f.b();
        if (b3 != null) {
            return b3;
        }
        j b4 = g.b();
        return b4 == null ? new j() : b4;
    }

    static byte[] b(List<x> list) {
        c cVar = new c();
        int size = list.size();
        for (int i = 0; i < size; i++) {
            x xVar = (x) list.get(i);
            if (xVar != x.HTTP_1_0) {
                cVar.i(xVar.toString().length());
                cVar.b(xVar.toString());
            }
        }
        return cVar.r();
    }

    public static j c() {
        return f135a;
    }

    public String a(SSLSocket sSLSocket) {
        return null;
    }

    public void a(int i, String str, Throwable th) {
        f136b.log(i == 5 ? Level.WARNING : Level.INFO, str, th);
    }

    public void a(Socket socket, InetSocketAddress inetSocketAddress, int i) throws IOException {
        socket.connect(inetSocketAddress, i);
    }

    public void a(SSLSocket sSLSocket, String str, List<x> list) {
    }

    public boolean a() {
        return true;
    }

    public void b(SSLSocket sSLSocket) {
    }

    public String d() {
        return "OkHttp";
    }
}
