package a.a;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

class i<T> {

    /* renamed from: a reason: collision with root package name */
    private final Class<?> f133a;

    /* renamed from: b reason: collision with root package name */
    private final String f134b;
    private final Class[] c;

    public i(Class<?> cls, String str, Class... clsArr) {
        this.f133a = cls;
        this.f134b = str;
        this.c = clsArr;
    }

    private Method a(Class<?> cls) {
        if (this.f134b == null) {
            return null;
        }
        Method a2 = a(cls, this.f134b, this.c);
        if (a2 == null || this.f133a == null || this.f133a.isAssignableFrom(a2.getReturnType())) {
            return a2;
        }
        return null;
    }

    private static Method a(Class<?> cls, String str, Class[] clsArr) {
        try {
            Method method = cls.getMethod(str, clsArr);
            try {
                if ((method.getModifiers() & 1) == 0) {
                    return null;
                }
                return method;
            } catch (NoSuchMethodException e) {
                return method;
            }
        } catch (NoSuchMethodException e2) {
            return null;
        }
    }

    public Object a(T t, Object... objArr) throws InvocationTargetException {
        Object obj = null;
        Method a2 = a(t.getClass());
        if (a2 == null) {
            return obj;
        }
        try {
            return a2.invoke(t, objArr);
        } catch (IllegalAccessException e) {
            return obj;
        }
    }

    public boolean a(T t) {
        return a(t.getClass()) != null;
    }

    public Object b(T t, Object... objArr) {
        try {
            return a(t, objArr);
        } catch (InvocationTargetException e) {
            Throwable targetException = e.getTargetException();
            if (targetException instanceof RuntimeException) {
                throw ((RuntimeException) targetException);
            }
            AssertionError assertionError = new AssertionError("Unexpected exception");
            assertionError.initCause(targetException);
            throw assertionError;
        }
    }

    public Object c(T t, Object... objArr) throws InvocationTargetException {
        Method a2 = a(t.getClass());
        if (a2 == null) {
            throw new AssertionError("Method " + this.f134b + " not supported for object " + t);
        }
        try {
            return a2.invoke(t, objArr);
        } catch (IllegalAccessException e) {
            AssertionError assertionError = new AssertionError("Unexpectedly could not call: " + a2);
            assertionError.initCause(e);
            throw assertionError;
        }
    }

    public Object d(T t, Object... objArr) {
        try {
            return c(t, objArr);
        } catch (InvocationTargetException e) {
            Throwable targetException = e.getTargetException();
            if (targetException instanceof RuntimeException) {
                throw ((RuntimeException) targetException);
            }
            AssertionError assertionError = new AssertionError("Unexpected exception");
            assertionError.initCause(targetException);
            throw assertionError;
        }
    }
}
