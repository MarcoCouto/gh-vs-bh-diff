package a.a.d;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.security.GeneralSecurityException;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.X509TrustManager;

public abstract class a {

    /* renamed from: a.a.d.a$a reason: collision with other inner class name */
    static final class C0001a extends a {

        /* renamed from: a reason: collision with root package name */
        private final Object f117a;

        /* renamed from: b reason: collision with root package name */
        private final Method f118b;

        C0001a(Object obj, Method method) {
            this.f117a = obj;
            this.f118b = method;
        }

        public List<Certificate> a(List<Certificate> list, String str) throws SSLPeerUnverifiedException {
            try {
                X509Certificate[] x509CertificateArr = (X509Certificate[]) list.toArray(new X509Certificate[list.size()]);
                return (List) this.f118b.invoke(this.f117a, new Object[]{x509CertificateArr, "RSA", str});
            } catch (InvocationTargetException e) {
                SSLPeerUnverifiedException sSLPeerUnverifiedException = new SSLPeerUnverifiedException(e.getMessage());
                sSLPeerUnverifiedException.initCause(e);
                throw sSLPeerUnverifiedException;
            } catch (IllegalAccessException e2) {
                throw new AssertionError(e2);
            }
        }
    }

    static final class b extends a {

        /* renamed from: a reason: collision with root package name */
        private final d f119a;

        public b(d dVar) {
            this.f119a = dVar;
        }

        private boolean a(X509Certificate x509Certificate, X509Certificate x509Certificate2) {
            if (!x509Certificate.getIssuerDN().equals(x509Certificate2.getSubjectDN())) {
                return false;
            }
            try {
                x509Certificate.verify(x509Certificate2.getPublicKey());
                return true;
            } catch (GeneralSecurityException e) {
                return false;
            }
        }

        public List<Certificate> a(List<Certificate> list, String str) throws SSLPeerUnverifiedException {
            boolean z = false;
            ArrayDeque arrayDeque = new ArrayDeque(list);
            ArrayList arrayList = new ArrayList();
            arrayList.add(arrayDeque.removeFirst());
            int i = 0;
            while (true) {
                boolean z2 = z;
                if (i < 9) {
                    X509Certificate x509Certificate = (X509Certificate) arrayList.get(arrayList.size() - 1);
                    X509Certificate a2 = this.f119a.a(x509Certificate);
                    if (a2 != null) {
                        if (arrayList.size() > 1 || !x509Certificate.equals(a2)) {
                            arrayList.add(a2);
                        }
                        if (a(a2, a2)) {
                            return arrayList;
                        }
                        z = true;
                    } else {
                        Iterator it = arrayDeque.iterator();
                        while (it.hasNext()) {
                            X509Certificate x509Certificate2 = (X509Certificate) it.next();
                            if (a(x509Certificate, x509Certificate2)) {
                                it.remove();
                                arrayList.add(x509Certificate2);
                                z = z2;
                            }
                        }
                        if (z2) {
                            return arrayList;
                        }
                        throw new SSLPeerUnverifiedException("Failed to find a trusted cert that signed " + x509Certificate);
                    }
                    i++;
                } else {
                    throw new SSLPeerUnverifiedException("Certificate chain too long: " + arrayList);
                }
            }
        }
    }

    public static a a(X509TrustManager x509TrustManager) {
        try {
            Class cls = Class.forName("android.net.http.X509TrustManagerExtensions");
            return new C0001a(cls.getConstructor(new Class[]{X509TrustManager.class}).newInstance(new Object[]{x509TrustManager}), cls.getMethod("checkServerTrusted", new Class[]{X509Certificate[].class, String.class, String.class}));
        } catch (Exception e) {
            return new b(d.a(x509TrustManager));
        }
    }

    public abstract List<Certificate> a(List<Certificate> list, String str) throws SSLPeerUnverifiedException;
}
