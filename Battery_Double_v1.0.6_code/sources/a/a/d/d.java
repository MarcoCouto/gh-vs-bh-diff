package a.a.d;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.security.cert.TrustAnchor;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.net.ssl.X509TrustManager;
import javax.security.auth.x500.X500Principal;

public abstract class d {

    static final class a extends d {

        /* renamed from: a reason: collision with root package name */
        private final X509TrustManager f123a;

        /* renamed from: b reason: collision with root package name */
        private final Method f124b;

        a(X509TrustManager x509TrustManager, Method method) {
            this.f124b = method;
            this.f123a = x509TrustManager;
        }

        public X509Certificate a(X509Certificate x509Certificate) {
            try {
                TrustAnchor trustAnchor = (TrustAnchor) this.f124b.invoke(this.f123a, new Object[]{x509Certificate});
                if (trustAnchor != null) {
                    return trustAnchor.getTrustedCert();
                }
                return null;
            } catch (IllegalAccessException e) {
                throw new AssertionError();
            } catch (InvocationTargetException e2) {
                return null;
            }
        }
    }

    static final class b extends d {

        /* renamed from: a reason: collision with root package name */
        private final Map<X500Principal, List<X509Certificate>> f125a = new LinkedHashMap();

        public b(X509Certificate... x509CertificateArr) {
            for (X509Certificate x509Certificate : x509CertificateArr) {
                X500Principal subjectX500Principal = x509Certificate.getSubjectX500Principal();
                List list = (List) this.f125a.get(subjectX500Principal);
                if (list == null) {
                    list = new ArrayList(1);
                    this.f125a.put(subjectX500Principal, list);
                }
                list.add(x509Certificate);
            }
        }

        public X509Certificate a(X509Certificate x509Certificate) {
            List<X509Certificate> list = (List) this.f125a.get(x509Certificate.getIssuerX500Principal());
            if (list == null) {
                return null;
            }
            for (X509Certificate x509Certificate2 : list) {
                try {
                    x509Certificate.verify(x509Certificate2.getPublicKey());
                    return x509Certificate2;
                } catch (Exception e) {
                }
            }
            return null;
        }
    }

    public static d a(X509TrustManager x509TrustManager) {
        try {
            Method declaredMethod = x509TrustManager.getClass().getDeclaredMethod("findTrustAnchorByIssuerAndSignature", new Class[]{X509Certificate.class});
            declaredMethod.setAccessible(true);
            return new a(x509TrustManager, declaredMethod);
        } catch (NoSuchMethodException e) {
            return a(x509TrustManager.getAcceptedIssuers());
        }
    }

    public static d a(X509Certificate... x509CertificateArr) {
        return new b(x509CertificateArr);
    }

    /* access modifiers changed from: 0000 */
    public abstract X509Certificate a(X509Certificate x509Certificate);
}
