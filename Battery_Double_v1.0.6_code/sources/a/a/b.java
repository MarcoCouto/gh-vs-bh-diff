package a.a;

import a.k;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.ProtocolException;
import java.net.UnknownServiceException;
import java.security.cert.CertificateException;
import java.util.Arrays;
import java.util.List;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLProtocolException;
import javax.net.ssl.SSLSocket;

public final class b {

    /* renamed from: a reason: collision with root package name */
    private final List<k> f69a;

    /* renamed from: b reason: collision with root package name */
    private int f70b = 0;
    private boolean c;
    private boolean d;

    public b(List<k> list) {
        this.f69a = list;
    }

    private boolean b(SSLSocket sSLSocket) {
        int i = this.f70b;
        while (true) {
            int i2 = i;
            if (i2 >= this.f69a.size()) {
                return false;
            }
            if (((k) this.f69a.get(i2)).a(sSLSocket)) {
                return true;
            }
            i = i2 + 1;
        }
    }

    public k a(SSLSocket sSLSocket) throws IOException {
        k kVar;
        int i = this.f70b;
        int size = this.f69a.size();
        int i2 = i;
        while (true) {
            if (i2 >= size) {
                kVar = null;
                break;
            }
            kVar = (k) this.f69a.get(i2);
            if (kVar.a(sSLSocket)) {
                this.f70b = i2 + 1;
                break;
            }
            i2++;
        }
        if (kVar == null) {
            throw new UnknownServiceException("Unable to find acceptable protocols. isFallback=" + this.d + ", modes=" + this.f69a + ", supported protocols=" + Arrays.toString(sSLSocket.getEnabledProtocols()));
        }
        this.c = b(sSLSocket);
        d.f116a.a(kVar, sSLSocket, this.d);
        return kVar;
    }

    public boolean a(IOException iOException) {
        this.d = true;
        if (!this.c || (iOException instanceof ProtocolException) || (iOException instanceof InterruptedIOException)) {
            return false;
        }
        if ((!(iOException instanceof SSLHandshakeException) || !(iOException.getCause() instanceof CertificateException)) && !(iOException instanceof SSLPeerUnverifiedException)) {
            return (iOException instanceof SSLHandshakeException) || (iOException instanceof SSLProtocolException);
        }
        return false;
    }
}
