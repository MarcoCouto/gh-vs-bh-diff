package a.a;

import a.x;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.List;
import javax.net.ssl.SSLSocket;

class g extends j {

    /* renamed from: a reason: collision with root package name */
    private final Method f128a;

    /* renamed from: b reason: collision with root package name */
    private final Method f129b;
    private final Method c;
    private final Class<?> d;
    private final Class<?> e;

    private static class a implements InvocationHandler {

        /* renamed from: a reason: collision with root package name */
        private final List<String> f130a;
        /* access modifiers changed from: private */

        /* renamed from: b reason: collision with root package name */
        public boolean f131b;
        /* access modifiers changed from: private */
        public String c;

        public a(List<String> list) {
            this.f130a = list;
        }

        public Object invoke(Object obj, Method method, Object[] objArr) throws Throwable {
            String name = method.getName();
            Class<String> returnType = method.getReturnType();
            if (objArr == null) {
                objArr = l.f139b;
            }
            if (name.equals("supports") && Boolean.TYPE == returnType) {
                return Boolean.valueOf(true);
            }
            if (name.equals("unsupported") && Void.TYPE == returnType) {
                this.f131b = true;
                return null;
            } else if (name.equals("protocols") && objArr.length == 0) {
                return this.f130a;
            } else {
                if ((name.equals("selectProtocol") || name.equals("select")) && String.class == returnType && objArr.length == 1 && (objArr[0] instanceof List)) {
                    List list = (List) objArr[0];
                    int size = list.size();
                    for (int i = 0; i < size; i++) {
                        if (this.f130a.contains(list.get(i))) {
                            String str = (String) list.get(i);
                            this.c = str;
                            return str;
                        }
                    }
                    String str2 = (String) this.f130a.get(0);
                    this.c = str2;
                    return str2;
                } else if ((!name.equals("protocolSelected") && !name.equals("selected")) || objArr.length != 1) {
                    return method.invoke(this, objArr);
                } else {
                    this.c = (String) objArr[0];
                    return null;
                }
            }
        }
    }

    public g(Method method, Method method2, Method method3, Class<?> cls, Class<?> cls2) {
        this.f128a = method;
        this.f129b = method2;
        this.c = method3;
        this.d = cls;
        this.e = cls2;
    }

    public static j b() {
        String str = "org.eclipse.jetty.alpn.ALPN";
        try {
            Class cls = Class.forName(str);
            Class cls2 = Class.forName(str + "$Provider");
            Class cls3 = Class.forName(str + "$ClientProvider");
            Class cls4 = Class.forName(str + "$ServerProvider");
            return new g(cls.getMethod("put", new Class[]{SSLSocket.class, cls2}), cls.getMethod("get", new Class[]{SSLSocket.class}), cls.getMethod("remove", new Class[]{SSLSocket.class}), cls3, cls4);
        } catch (ClassNotFoundException | NoSuchMethodException e2) {
            return null;
        }
    }

    public String a(SSLSocket sSLSocket) {
        try {
            a aVar = (a) Proxy.getInvocationHandler(this.f129b.invoke(null, new Object[]{sSLSocket}));
            if (aVar.f131b || aVar.c != null) {
                return aVar.f131b ? null : aVar.c;
            }
            j.c().a(4, "ALPN callback dropped: SPDY and HTTP/2 are disabled. Is alpn-boot on the boot class path?", (Throwable) null);
            return null;
        } catch (IllegalAccessException | InvocationTargetException e2) {
            throw new AssertionError();
        }
    }

    public void a(SSLSocket sSLSocket, String str, List<x> list) {
        List a2 = a(list);
        try {
            Object newProxyInstance = Proxy.newProxyInstance(j.class.getClassLoader(), new Class[]{this.d, this.e}, new a(a2));
            this.f128a.invoke(null, new Object[]{sSLSocket, newProxyInstance});
        } catch (IllegalAccessException | InvocationTargetException e2) {
            throw new AssertionError(e2);
        }
    }

    public void b(SSLSocket sSLSocket) {
        try {
            this.c.invoke(null, new Object[]{sSLSocket});
        } catch (IllegalAccessException | InvocationTargetException e2) {
            throw new AssertionError();
        }
    }
}
