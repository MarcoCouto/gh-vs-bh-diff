package a.a.a;

import a.a.l;
import b.c;
import b.d;
import b.e;
import b.f;
import b.g;
import b.r;
import java.io.Closeable;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.ProtocolException;
import java.util.List;
import java.util.zip.Deflater;

public final class o implements q {

    /* renamed from: a reason: collision with root package name */
    static final byte[] f63a;

    static final class a implements b {

        /* renamed from: a reason: collision with root package name */
        private final e f64a;

        /* renamed from: b reason: collision with root package name */
        private final boolean f65b;
        private final k c = new k(this.f64a);

        a(e eVar, boolean z) {
            this.f64a = eVar;
            this.f65b = z;
        }

        private static IOException a(String str, Object... objArr) throws IOException {
            throw new IOException(l.a(str, objArr));
        }

        private void a(a.a.a.b.a aVar, int i, int i2) throws IOException {
            boolean z = true;
            int k = this.f64a.k() & Integer.MAX_VALUE;
            int k2 = this.f64a.k() & Integer.MAX_VALUE;
            this.f64a.j();
            List a2 = this.c.a(i2 - 10);
            boolean z2 = (i & 1) != 0;
            if ((i & 2) == 0) {
                z = false;
            }
            aVar.a(z, z2, k, k2, a2, g.SPDY_SYN_STREAM);
        }

        private void b(a.a.a.b.a aVar, int i, int i2) throws IOException {
            aVar.a(false, (i & 1) != 0, this.f64a.k() & Integer.MAX_VALUE, -1, this.c.a(i2 - 4), g.SPDY_REPLY);
        }

        private void c(a.a.a.b.a aVar, int i, int i2) throws IOException {
            if (i2 != 8) {
                throw a("TYPE_RST_STREAM length: %d != 8", Integer.valueOf(i2));
            }
            int k = this.f64a.k() & Integer.MAX_VALUE;
            int k2 = this.f64a.k();
            a a2 = a.a(k2);
            if (a2 == null) {
                throw a("TYPE_RST_STREAM unexpected error code: %d", Integer.valueOf(k2));
            } else {
                aVar.a(k, a2);
            }
        }

        private void d(a.a.a.b.a aVar, int i, int i2) throws IOException {
            aVar.a(false, false, this.f64a.k() & Integer.MAX_VALUE, -1, this.c.a(i2 - 4), g.SPDY_HEADERS);
        }

        private void e(a.a.a.b.a aVar, int i, int i2) throws IOException {
            if (i2 != 8) {
                throw a("TYPE_WINDOW_UPDATE length: %d != 8", Integer.valueOf(i2));
            }
            int k = this.f64a.k() & Integer.MAX_VALUE;
            long k2 = (long) (this.f64a.k() & Integer.MAX_VALUE);
            if (k2 == 0) {
                throw a("windowSizeIncrement was 0", Long.valueOf(k2));
            } else {
                aVar.a(k, k2);
            }
        }

        private void f(a.a.a.b.a aVar, int i, int i2) throws IOException {
            boolean z = true;
            if (i2 != 4) {
                throw a("TYPE_PING length: %d != 4", Integer.valueOf(i2));
            }
            int k = this.f64a.k();
            if (this.f65b != ((k & 1) == 1)) {
                z = false;
            }
            aVar.a(z, k, 0);
        }

        private void g(a.a.a.b.a aVar, int i, int i2) throws IOException {
            if (i2 != 8) {
                throw a("TYPE_GOAWAY length: %d != 8", Integer.valueOf(i2));
            }
            int k = this.f64a.k() & Integer.MAX_VALUE;
            int k2 = this.f64a.k();
            a c2 = a.c(k2);
            if (c2 == null) {
                throw a("TYPE_GOAWAY unexpected error code: %d", Integer.valueOf(k2));
            } else {
                aVar.a(k, c2, f.f1371b);
            }
        }

        private void h(a.a.a.b.a aVar, int i, int i2) throws IOException {
            boolean z = true;
            int k = this.f64a.k();
            if (i2 != (k * 8) + 4) {
                throw a("TYPE_SETTINGS length: %d != 4 + 8 * %d", Integer.valueOf(i2), Integer.valueOf(k));
            }
            n nVar = new n();
            for (int i3 = 0; i3 < k; i3++) {
                int k2 = this.f64a.k();
                int i4 = (-16777216 & k2) >>> 24;
                nVar.a(k2 & 16777215, i4, this.f64a.k());
            }
            if ((i & 1) == 0) {
                z = false;
            }
            aVar.a(z, nVar);
        }

        public void a() {
        }

        public boolean a(a.a.a.b.a aVar) throws IOException {
            boolean z = false;
            try {
                int k = this.f64a.k();
                int k2 = this.f64a.k();
                int i = (-16777216 & k2) >>> 24;
                int i2 = k2 & 16777215;
                if ((Integer.MIN_VALUE & k) != 0) {
                    int i3 = (2147418112 & k) >>> 16;
                    int i4 = 65535 & k;
                    if (i3 != 3) {
                        throw new ProtocolException("version != 3: " + i3);
                    }
                    switch (i4) {
                        case 1:
                            a(aVar, i, i2);
                            return true;
                        case 2:
                            b(aVar, i, i2);
                            return true;
                        case 3:
                            c(aVar, i, i2);
                            return true;
                        case 4:
                            h(aVar, i, i2);
                            return true;
                        case 6:
                            f(aVar, i, i2);
                            return true;
                        case 7:
                            g(aVar, i, i2);
                            return true;
                        case 8:
                            d(aVar, i, i2);
                            return true;
                        case 9:
                            e(aVar, i, i2);
                            return true;
                        default:
                            this.f64a.g((long) i2);
                            return true;
                    }
                } else {
                    int i5 = Integer.MAX_VALUE & k;
                    if ((i & 1) != 0) {
                        z = true;
                    }
                    aVar.a(z, i5, this.f64a, i2);
                    return true;
                }
            } catch (IOException e) {
                return false;
            }
        }

        public void close() throws IOException {
            this.c.a();
        }
    }

    static final class b implements c {

        /* renamed from: a reason: collision with root package name */
        private final d f66a;

        /* renamed from: b reason: collision with root package name */
        private final c f67b = new c();
        private final d c;
        private final boolean d;
        private boolean e;

        b(d dVar, boolean z) {
            this.f66a = dVar;
            this.d = z;
            Deflater deflater = new Deflater();
            deflater.setDictionary(o.f63a);
            this.c = b.l.a((r) new g((r) this.f67b, deflater));
        }

        private void a(List<f> list) throws IOException {
            this.c.g(list.size());
            int size = list.size();
            for (int i = 0; i < size; i++) {
                f fVar = ((f) list.get(i)).h;
                this.c.g(fVar.e());
                this.c.b(fVar);
                f fVar2 = ((f) list.get(i)).i;
                this.c.g(fVar2.e());
                this.c.b(fVar2);
            }
            this.c.flush();
        }

        public synchronized void a() {
        }

        /* access modifiers changed from: 0000 */
        public void a(int i, int i2, c cVar, int i3) throws IOException {
            if (this.e) {
                throw new IOException("closed");
            } else if (((long) i3) > 16777215) {
                throw new IllegalArgumentException("FRAME_TOO_LARGE max size is 16Mib: " + i3);
            } else {
                this.f66a.g(Integer.MAX_VALUE & i);
                this.f66a.g(((i2 & 255) << 24) | (16777215 & i3));
                if (i3 > 0) {
                    this.f66a.a(cVar, (long) i3);
                }
            }
        }

        public void a(int i, int i2, List<f> list) throws IOException {
        }

        public synchronized void a(int i, long j) throws IOException {
            if (this.e) {
                throw new IOException("closed");
            } else if (j == 0 || j > 2147483647L) {
                throw new IllegalArgumentException("windowSizeIncrement must be between 1 and 0x7fffffff: " + j);
            } else {
                this.f66a.g(-2147287031);
                this.f66a.g(8);
                this.f66a.g(i);
                this.f66a.g((int) j);
                this.f66a.flush();
            }
        }

        public synchronized void a(int i, a aVar) throws IOException {
            if (this.e) {
                throw new IOException("closed");
            } else if (aVar.t == -1) {
                throw new IllegalArgumentException();
            } else {
                this.f66a.g(-2147287037);
                this.f66a.g(8);
                this.f66a.g(Integer.MAX_VALUE & i);
                this.f66a.g(aVar.t);
                this.f66a.flush();
            }
        }

        public synchronized void a(int i, a aVar, byte[] bArr) throws IOException {
            if (this.e) {
                throw new IOException("closed");
            } else if (aVar.u == -1) {
                throw new IllegalArgumentException("errorCode.spdyGoAwayCode == -1");
            } else {
                this.f66a.g(-2147287033);
                this.f66a.g(8);
                this.f66a.g(i);
                this.f66a.g(aVar.u);
                this.f66a.flush();
            }
        }

        public void a(n nVar) {
        }

        public synchronized void a(boolean z, int i, int i2) throws IOException {
            boolean z2 = true;
            synchronized (this) {
                if (this.e) {
                    throw new IOException("closed");
                }
                if (this.d == ((i & 1) == 1)) {
                    z2 = false;
                }
                if (z != z2) {
                    throw new IllegalArgumentException("payload != reply");
                }
                this.f66a.g(-2147287034);
                this.f66a.g(4);
                this.f66a.g(i);
                this.f66a.flush();
            }
        }

        public synchronized void a(boolean z, int i, c cVar, int i2) throws IOException {
            a(i, z ? 1 : 0, cVar, i2);
        }

        public synchronized void a(boolean z, boolean z2, int i, int i2, List<f> list) throws IOException {
            int i3 = 0;
            synchronized (this) {
                if (this.e) {
                    throw new IOException("closed");
                }
                a(list);
                int a2 = (int) (10 + this.f67b.a());
                int i4 = z ? 1 : 0;
                if (z2) {
                    i3 = 2;
                }
                int i5 = i3 | i4;
                this.f66a.g(-2147287039);
                this.f66a.g(((i5 & 255) << 24) | (a2 & 16777215));
                this.f66a.g(i & Integer.MAX_VALUE);
                this.f66a.g(i2 & Integer.MAX_VALUE);
                this.f66a.h(0);
                this.f66a.a(this.f67b);
                this.f66a.flush();
            }
        }

        public synchronized void b() throws IOException {
            if (this.e) {
                throw new IOException("closed");
            }
            this.f66a.flush();
        }

        public synchronized void b(n nVar) throws IOException {
            if (this.e) {
                throw new IOException("closed");
            }
            int b2 = nVar.b();
            int i = (b2 * 8) + 4;
            this.f66a.g(-2147287036);
            this.f66a.g((i & 16777215) | 0);
            this.f66a.g(b2);
            for (int i2 = 0; i2 <= 10; i2++) {
                if (nVar.a(i2)) {
                    this.f66a.g(((nVar.c(i2) & 255) << 24) | (i2 & 16777215));
                    this.f66a.g(nVar.b(i2));
                }
            }
            this.f66a.flush();
        }

        public int c() {
            return 16383;
        }

        public synchronized void close() throws IOException {
            this.e = true;
            l.a((Closeable) this.f66a, (Closeable) this.c);
        }
    }

    static {
        try {
            f63a = "\u0000\u0000\u0000\u0007options\u0000\u0000\u0000\u0004head\u0000\u0000\u0000\u0004post\u0000\u0000\u0000\u0003put\u0000\u0000\u0000\u0006delete\u0000\u0000\u0000\u0005trace\u0000\u0000\u0000\u0006accept\u0000\u0000\u0000\u000eaccept-charset\u0000\u0000\u0000\u000faccept-encoding\u0000\u0000\u0000\u000faccept-language\u0000\u0000\u0000\raccept-ranges\u0000\u0000\u0000\u0003age\u0000\u0000\u0000\u0005allow\u0000\u0000\u0000\rauthorization\u0000\u0000\u0000\rcache-control\u0000\u0000\u0000\nconnection\u0000\u0000\u0000\fcontent-base\u0000\u0000\u0000\u0010content-encoding\u0000\u0000\u0000\u0010content-language\u0000\u0000\u0000\u000econtent-length\u0000\u0000\u0000\u0010content-location\u0000\u0000\u0000\u000bcontent-md5\u0000\u0000\u0000\rcontent-range\u0000\u0000\u0000\fcontent-type\u0000\u0000\u0000\u0004date\u0000\u0000\u0000\u0004etag\u0000\u0000\u0000\u0006expect\u0000\u0000\u0000\u0007expires\u0000\u0000\u0000\u0004from\u0000\u0000\u0000\u0004host\u0000\u0000\u0000\bif-match\u0000\u0000\u0000\u0011if-modified-since\u0000\u0000\u0000\rif-none-match\u0000\u0000\u0000\bif-range\u0000\u0000\u0000\u0013if-unmodified-since\u0000\u0000\u0000\rlast-modified\u0000\u0000\u0000\blocation\u0000\u0000\u0000\fmax-forwards\u0000\u0000\u0000\u0006pragma\u0000\u0000\u0000\u0012proxy-authenticate\u0000\u0000\u0000\u0013proxy-authorization\u0000\u0000\u0000\u0005range\u0000\u0000\u0000\u0007referer\u0000\u0000\u0000\u000bretry-after\u0000\u0000\u0000\u0006server\u0000\u0000\u0000\u0002te\u0000\u0000\u0000\u0007trailer\u0000\u0000\u0000\u0011transfer-encoding\u0000\u0000\u0000\u0007upgrade\u0000\u0000\u0000\nuser-agent\u0000\u0000\u0000\u0004vary\u0000\u0000\u0000\u0003via\u0000\u0000\u0000\u0007warning\u0000\u0000\u0000\u0010www-authenticate\u0000\u0000\u0000\u0006method\u0000\u0000\u0000\u0003get\u0000\u0000\u0000\u0006status\u0000\u0000\u0000\u0006200 OK\u0000\u0000\u0000\u0007version\u0000\u0000\u0000\bHTTP/1.1\u0000\u0000\u0000\u0003url\u0000\u0000\u0000\u0006public\u0000\u0000\u0000\nset-cookie\u0000\u0000\u0000\nkeep-alive\u0000\u0000\u0000\u0006origin100101201202205206300302303304305306307402405406407408409410411412413414415416417502504505203 Non-Authoritative Information204 No Content301 Moved Permanently400 Bad Request401 Unauthorized403 Forbidden404 Not Found500 Internal Server Error501 Not Implemented503 Service UnavailableJan Feb Mar Apr May Jun Jul Aug Sept Oct Nov Dec 00:00:00 Mon, Tue, Wed, Thu, Fri, Sat, Sun, GMTchunked,text/html,image/png,image/jpg,image/gif,application/xml,application/xhtml+xml,text/plain,text/javascript,publicprivatemax-age=gzip,deflate,sdchcharset=utf-8charset=iso-8859-1,utf-,*,enq=0.".getBytes(l.c.name());
        } catch (UnsupportedEncodingException e) {
            throw new AssertionError();
        }
    }

    public b a(e eVar, boolean z) {
        return new a(eVar, z);
    }

    public c a(d dVar, boolean z) {
        return new b(dVar, z);
    }
}
