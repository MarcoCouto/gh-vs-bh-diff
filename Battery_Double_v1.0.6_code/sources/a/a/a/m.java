package a.a.a;

import b.e;
import java.io.IOException;
import java.util.List;

public interface m {

    /* renamed from: a reason: collision with root package name */
    public static final m f60a = new m() {
        public void a(int i, a aVar) {
        }

        public boolean a(int i, e eVar, int i2, boolean z) throws IOException {
            eVar.g((long) i2);
            return true;
        }

        public boolean a(int i, List<f> list) {
            return true;
        }

        public boolean a(int i, List<f> list, boolean z) {
            return true;
        }
    };

    void a(int i, a aVar);

    boolean a(int i, e eVar, int i2, boolean z) throws IOException;

    boolean a(int i, List<f> list);

    boolean a(int i, List<f> list, boolean z);
}
