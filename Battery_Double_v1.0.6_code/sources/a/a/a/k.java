package a.a.a;

import b.c;
import b.e;
import b.f;
import b.h;
import b.l;
import b.s;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.DataFormatException;
import java.util.zip.Inflater;

class k {

    /* renamed from: a reason: collision with root package name */
    private final b.k f54a;
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public int f55b;
    private final e c = l.a((s) this.f54a);

    public k(e eVar) {
        this.f54a = new b.k((s) new h(eVar) {
            public long read(c cVar, long j) throws IOException {
                if (k.this.f55b == 0) {
                    return -1;
                }
                long read = super.read(cVar, Math.min(j, (long) k.this.f55b));
                if (read == -1) {
                    return -1;
                }
                k.this.f55b = (int) (((long) k.this.f55b) - read);
                return read;
            }
        }, (Inflater) new Inflater() {
            public int inflate(byte[] bArr, int i, int i2) throws DataFormatException {
                int inflate = super.inflate(bArr, i, i2);
                if (inflate != 0 || !needsDictionary()) {
                    return inflate;
                }
                setDictionary(o.f63a);
                return super.inflate(bArr, i, i2);
            }
        });
    }

    private f b() throws IOException {
        return this.c.c((long) this.c.k());
    }

    private void c() throws IOException {
        if (this.f55b > 0) {
            this.f54a.a();
            if (this.f55b != 0) {
                throw new IOException("compressedLimit > 0: " + this.f55b);
            }
        }
    }

    public List<f> a(int i) throws IOException {
        this.f55b += i;
        int k = this.c.k();
        if (k < 0) {
            throw new IOException("numberOfPairs < 0: " + k);
        } else if (k > 1024) {
            throw new IOException("numberOfPairs > 1024: " + k);
        } else {
            ArrayList arrayList = new ArrayList(k);
            for (int i2 = 0; i2 < k; i2++) {
                f d = b().d();
                f b2 = b();
                if (d.e() == 0) {
                    throw new IOException("name.size == 0");
                }
                arrayList.add(new f(d, b2));
            }
            c();
            return arrayList;
        }
    }

    public void a() throws IOException {
        this.c.close();
    }
}
