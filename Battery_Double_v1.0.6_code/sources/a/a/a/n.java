package a.a.a;

import java.util.Arrays;

public final class n {

    /* renamed from: a reason: collision with root package name */
    private int f61a;

    /* renamed from: b reason: collision with root package name */
    private int f62b;
    private int c;
    private final int[] d = new int[10];

    /* access modifiers changed from: 0000 */
    public n a(int i, int i2, int i3) {
        if (i < this.d.length) {
            int i4 = 1 << i;
            this.f61a |= i4;
            if ((i2 & 1) != 0) {
                this.f62b |= i4;
            } else {
                this.f62b &= i4 ^ -1;
            }
            if ((i2 & 2) != 0) {
                this.c = i4 | this.c;
            } else {
                this.c = (i4 ^ -1) & this.c;
            }
            this.d[i] = i3;
        }
        return this;
    }

    /* access modifiers changed from: 0000 */
    public void a() {
        this.c = 0;
        this.f62b = 0;
        this.f61a = 0;
        Arrays.fill(this.d, 0);
    }

    /* access modifiers changed from: 0000 */
    public void a(n nVar) {
        for (int i = 0; i < 10; i++) {
            if (nVar.a(i)) {
                a(i, nVar.c(i), nVar.b(i));
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean a(int i) {
        return ((1 << i) & this.f61a) != 0;
    }

    /* access modifiers changed from: 0000 */
    public int b() {
        return Integer.bitCount(this.f61a);
    }

    /* access modifiers changed from: 0000 */
    public int b(int i) {
        return this.d[i];
    }

    /* access modifiers changed from: 0000 */
    public int c() {
        if ((2 & this.f61a) != 0) {
            return this.d[1];
        }
        return -1;
    }

    /* access modifiers changed from: 0000 */
    public int c(int i) {
        int i2 = 0;
        if (h(i)) {
            i2 = 2;
        }
        return g(i) ? i2 | 1 : i2;
    }

    /* access modifiers changed from: 0000 */
    public int d(int i) {
        return (16 & this.f61a) != 0 ? this.d[4] : i;
    }

    /* access modifiers changed from: 0000 */
    public int e(int i) {
        return (32 & this.f61a) != 0 ? this.d[5] : i;
    }

    /* access modifiers changed from: 0000 */
    public int f(int i) {
        return (128 & this.f61a) != 0 ? this.d[7] : i;
    }

    /* access modifiers changed from: 0000 */
    public boolean g(int i) {
        return ((1 << i) & this.f62b) != 0;
    }

    /* access modifiers changed from: 0000 */
    public boolean h(int i) {
        return ((1 << i) & this.c) != 0;
    }
}
