package a.a.a;

import java.io.IOException;

public final class p extends IOException {

    /* renamed from: a reason: collision with root package name */
    public final a f68a;

    public p(a aVar) {
        super("stream was reset: " + aVar);
        this.f68a = aVar;
    }
}
