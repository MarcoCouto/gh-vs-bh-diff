package a.a.a;

import a.a.h;
import a.a.j;
import a.a.l;
import a.x;
import b.e;
import b.f;
import java.io.Closeable;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.Socket;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public final class d implements Closeable {
    static final /* synthetic */ boolean k = (!d.class.desiredAssertionStatus());
    /* access modifiers changed from: private */
    public static final ExecutorService l = new ThreadPoolExecutor(0, Integer.MAX_VALUE, 60, TimeUnit.SECONDS, new SynchronousQueue(), l.a("OkHttp FramedConnection", true));

    /* renamed from: a reason: collision with root package name */
    final x f8a;

    /* renamed from: b reason: collision with root package name */
    final boolean f9b;
    long c;
    long d;
    n e;
    final n f;
    final q g;
    final Socket h;
    final c i;
    final c j;
    /* access modifiers changed from: private */
    public final b m;
    /* access modifiers changed from: private */
    public final Map<Integer, e> n;
    /* access modifiers changed from: private */
    public final String o;
    /* access modifiers changed from: private */
    public int p;
    /* access modifiers changed from: private */
    public int q;
    /* access modifiers changed from: private */
    public boolean r;
    private long s;
    private final ExecutorService t;
    private Map<Integer, l> u;
    /* access modifiers changed from: private */
    public final m v;
    private int w;
    /* access modifiers changed from: private */
    public boolean x;
    /* access modifiers changed from: private */
    public final Set<Integer> y;

    public static class a {
        /* access modifiers changed from: private */

        /* renamed from: a reason: collision with root package name */
        public Socket f17a;
        /* access modifiers changed from: private */

        /* renamed from: b reason: collision with root package name */
        public String f18b;
        /* access modifiers changed from: private */
        public e c;
        /* access modifiers changed from: private */
        public b.d d;
        /* access modifiers changed from: private */
        public b e = b.f19a;
        /* access modifiers changed from: private */
        public x f = x.SPDY_3;
        /* access modifiers changed from: private */
        public m g = m.f60a;
        /* access modifiers changed from: private */
        public boolean h;

        public a(boolean z) throws IOException {
            this.h = z;
        }

        public a a(b bVar) {
            this.e = bVar;
            return this;
        }

        public a a(x xVar) {
            this.f = xVar;
            return this;
        }

        public a a(Socket socket, String str, e eVar, b.d dVar) {
            this.f17a = socket;
            this.f18b = str;
            this.c = eVar;
            this.d = dVar;
            return this;
        }

        public d a() throws IOException {
            return new d(this);
        }
    }

    public static abstract class b {

        /* renamed from: a reason: collision with root package name */
        public static final b f19a = new b() {
            public void a(e eVar) throws IOException {
                eVar.a(a.REFUSED_STREAM);
            }
        };

        public void a(d dVar) {
        }

        public abstract void a(e eVar) throws IOException;
    }

    class c extends h implements a.a.a.b.a {

        /* renamed from: a reason: collision with root package name */
        final b f20a;

        private c(b bVar) {
            super("OkHttp %s", d.this.o);
            this.f20a = bVar;
        }

        private void a(final n nVar) {
            d.l.execute(new h("OkHttp %s ACK Settings", new Object[]{d.this.o}) {
                public void b() {
                    try {
                        d.this.i.a(nVar);
                    } catch (IOException e) {
                    }
                }
            });
        }

        public void a() {
        }

        public void a(int i, int i2, int i3, boolean z) {
        }

        public void a(int i, int i2, List<f> list) {
            d.this.a(i2, list);
        }

        public void a(int i, long j) {
            if (i == 0) {
                synchronized (d.this) {
                    d.this.d += j;
                    d.this.notifyAll();
                }
                return;
            }
            e a2 = d.this.a(i);
            if (a2 != null) {
                synchronized (a2) {
                    a2.a(j);
                }
            }
        }

        public void a(int i, a aVar) {
            if (d.this.d(i)) {
                d.this.c(i, aVar);
                return;
            }
            e b2 = d.this.b(i);
            if (b2 != null) {
                b2.c(aVar);
            }
        }

        public void a(int i, a aVar, f fVar) {
            e[] eVarArr;
            if (fVar.e() > 0) {
            }
            synchronized (d.this) {
                eVarArr = (e[]) d.this.n.values().toArray(new e[d.this.n.size()]);
                d.this.r = true;
            }
            for (e eVar : eVarArr) {
                if (eVar.a() > i && eVar.c()) {
                    eVar.c(a.REFUSED_STREAM);
                    d.this.b(eVar.a());
                }
            }
        }

        public void a(boolean z, int i, int i2) {
            if (z) {
                l c2 = d.this.c(i);
                if (c2 != null) {
                    c2.b();
                    return;
                }
                return;
            }
            d.this.a(true, i, i2, (l) null);
        }

        public void a(boolean z, int i, e eVar, int i2) throws IOException {
            if (d.this.d(i)) {
                d.this.a(i, eVar, i2, z);
                return;
            }
            e a2 = d.this.a(i);
            if (a2 == null) {
                d.this.a(i, a.INVALID_STREAM);
                eVar.g((long) i2);
                return;
            }
            a2.a(eVar, i2);
            if (z) {
                a2.i();
            }
        }

        public void a(boolean z, n nVar) {
            e[] eVarArr;
            long j;
            synchronized (d.this) {
                int f = d.this.f.f(65536);
                if (z) {
                    d.this.f.a();
                }
                d.this.f.a(nVar);
                if (d.this.a() == x.HTTP_2) {
                    a(nVar);
                }
                int f2 = d.this.f.f(65536);
                if (f2 == -1 || f2 == f) {
                    eVarArr = null;
                    j = 0;
                } else {
                    long j2 = (long) (f2 - f);
                    if (!d.this.x) {
                        d.this.a(j2);
                        d.this.x = true;
                    }
                    if (!d.this.n.isEmpty()) {
                        j = j2;
                        eVarArr = (e[]) d.this.n.values().toArray(new e[d.this.n.size()]);
                    } else {
                        j = j2;
                        eVarArr = null;
                    }
                }
                d.l.execute(new h("OkHttp %s settings", d.this.o) {
                    public void b() {
                        d.this.m.a(d.this);
                    }
                });
            }
            if (eVarArr != null && j != 0) {
                for (e eVar : eVarArr) {
                    synchronized (eVar) {
                        eVar.a(j);
                    }
                }
            }
        }

        /* JADX INFO: used method not loaded: a.a.a.e.b(a.a.a.a):null, types can be incorrect */
        /* JADX INFO: used method not loaded: a.a.a.e.a(java.util.List, a.a.a.g):null, types can be incorrect */
        /* JADX WARNING: Code restructure failed: missing block: B:34:0x0092, code lost:
            if (r14.b() == false) goto L_0x00a0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:35:0x0094, code lost:
            r0.b(a.a.a.a.f7b);
            r8.c.b(r11);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:36:0x00a0, code lost:
            r0.a((java.util.List) r13, r14);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:37:0x00a3, code lost:
            if (r10 == false) goto L_?;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:38:0x00a5, code lost:
            r0.i();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:45:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:46:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:47:?, code lost:
            return;
         */
        public void a(boolean z, boolean z2, int i, int i2, List<f> list, g gVar) {
            if (d.this.d(i)) {
                d.this.a(i, list, z2);
                return;
            }
            synchronized (d.this) {
                if (!d.this.r) {
                    e a2 = d.this.a(i);
                    if (a2 == null) {
                        if (gVar.a()) {
                            d.this.a(i, a.INVALID_STREAM);
                        } else if (i > d.this.p) {
                            if (i % 2 != d.this.q % 2) {
                                final e eVar = new e(i, d.this, z, z2, list);
                                d.this.p = i;
                                d.this.n.put(Integer.valueOf(i), eVar);
                                d.l.execute(new h("OkHttp %s stream %d", new Object[]{d.this.o, Integer.valueOf(i)}) {
                                    public void b() {
                                        try {
                                            d.this.m.a(eVar);
                                        } catch (IOException e) {
                                            j.c().a(4, "FramedConnection.Listener failure for " + d.this.o, (Throwable) e);
                                            try {
                                                eVar.a(a.PROTOCOL_ERROR);
                                            } catch (IOException e2) {
                                            }
                                        }
                                    }
                                });
                            }
                        }
                    }
                }
            }
        }

        /* access modifiers changed from: protected */
        public void b() {
            a aVar;
            Throwable th;
            a aVar2 = a.INTERNAL_ERROR;
            a aVar3 = a.INTERNAL_ERROR;
            try {
                if (!d.this.f9b) {
                    this.f20a.a();
                }
                do {
                } while (this.f20a.a(this));
                try {
                    d.this.a(a.NO_ERROR, a.CANCEL);
                } catch (IOException e) {
                }
                l.a((Closeable) this.f20a);
            } catch (IOException e2) {
                aVar = a.PROTOCOL_ERROR;
                try {
                    d.this.a(aVar, a.PROTOCOL_ERROR);
                } catch (IOException e3) {
                }
                l.a((Closeable) this.f20a);
            } catch (Throwable th2) {
                th = th2;
                d.this.a(aVar, aVar3);
                l.a((Closeable) this.f20a);
                throw th;
            }
        }
    }

    private d(a aVar) throws IOException {
        int i2 = 2;
        this.n = new HashMap();
        this.s = System.nanoTime();
        this.c = 0;
        this.e = new n();
        this.f = new n();
        this.x = false;
        this.y = new LinkedHashSet();
        this.f8a = aVar.f;
        this.v = aVar.g;
        this.f9b = aVar.h;
        this.m = aVar.e;
        this.q = aVar.h ? 1 : 2;
        if (aVar.h && this.f8a == x.HTTP_2) {
            this.q += 2;
        }
        if (aVar.h) {
            i2 = 1;
        }
        this.w = i2;
        if (aVar.h) {
            this.e.a(7, 0, 16777216);
        }
        this.o = aVar.f18b;
        if (this.f8a == x.HTTP_2) {
            this.g = new i();
            this.t = new ThreadPoolExecutor(0, 1, 60, TimeUnit.SECONDS, new LinkedBlockingQueue(), l.a(l.a("OkHttp %s Push Observer", this.o), true));
            this.f.a(7, 0, 65535);
            this.f.a(5, 0, 16384);
        } else if (this.f8a == x.SPDY_3) {
            this.g = new o();
            this.t = null;
        } else {
            throw new AssertionError(this.f8a);
        }
        this.d = (long) this.f.f(65536);
        this.h = aVar.f17a;
        this.i = this.g.a(aVar.d, this.f9b);
        this.j = new c(this.g.a(aVar.c, this.f9b));
    }

    private e a(int i2, List<f> list, boolean z, boolean z2) throws IOException {
        int i3;
        e eVar;
        boolean z3;
        boolean z4 = !z;
        boolean z5 = !z2;
        synchronized (this.i) {
            synchronized (this) {
                if (this.r) {
                    throw new IOException("shutdown");
                }
                i3 = this.q;
                this.q += 2;
                eVar = new e(i3, this, z4, z5, list);
                z3 = !z || this.d == 0 || eVar.f25b == 0;
                if (eVar.b()) {
                    this.n.put(Integer.valueOf(i3), eVar);
                    b(false);
                }
            }
            if (i2 == 0) {
                this.i.a(z4, z5, i3, i2, list);
            } else if (this.f9b) {
                throw new IllegalArgumentException("client streams shouldn't have associated stream IDs");
            } else {
                this.i.a(i2, i3, list);
            }
        }
        if (z3) {
            this.i.b();
        }
        return eVar;
    }

    /* access modifiers changed from: private */
    public void a(int i2, e eVar, int i3, boolean z) throws IOException {
        final b.c cVar = new b.c();
        eVar.a((long) i3);
        eVar.read(cVar, (long) i3);
        if (cVar.a() != ((long) i3)) {
            throw new IOException(cVar.a() + " != " + i3);
        }
        final int i4 = i2;
        final int i5 = i3;
        final boolean z2 = z;
        this.t.execute(new h("OkHttp %s Push Data[%s]", new Object[]{this.o, Integer.valueOf(i2)}) {
            public void b() {
                try {
                    boolean a2 = d.this.v.a(i4, cVar, i5, z2);
                    if (a2) {
                        d.this.i.a(i4, a.CANCEL);
                    }
                    if (a2 || z2) {
                        synchronized (d.this) {
                            d.this.y.remove(Integer.valueOf(i4));
                        }
                    }
                } catch (IOException e2) {
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(int i2, List<f> list) {
        synchronized (this) {
            if (this.y.contains(Integer.valueOf(i2))) {
                a(i2, a.PROTOCOL_ERROR);
                return;
            }
            this.y.add(Integer.valueOf(i2));
            final int i3 = i2;
            final List<f> list2 = list;
            this.t.execute(new h("OkHttp %s Push Request[%s]", new Object[]{this.o, Integer.valueOf(i2)}) {
                public void b() {
                    if (d.this.v.a(i3, list2)) {
                        try {
                            d.this.i.a(i3, a.CANCEL);
                            synchronized (d.this) {
                                d.this.y.remove(Integer.valueOf(i3));
                            }
                        } catch (IOException e) {
                        }
                    }
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public void a(int i2, List<f> list, boolean z) {
        final int i3 = i2;
        final List<f> list2 = list;
        final boolean z2 = z;
        this.t.execute(new h("OkHttp %s Push Headers[%s]", new Object[]{this.o, Integer.valueOf(i2)}) {
            public void b() {
                boolean a2 = d.this.v.a(i3, list2, z2);
                if (a2) {
                    try {
                        d.this.i.a(i3, a.CANCEL);
                    } catch (IOException e2) {
                        return;
                    }
                }
                if (a2 || z2) {
                    synchronized (d.this) {
                        d.this.y.remove(Integer.valueOf(i3));
                    }
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(a aVar, a aVar2) throws IOException {
        IOException iOException;
        e[] eVarArr;
        l[] lVarArr;
        if (k || !Thread.holdsLock(this)) {
            try {
                a(aVar);
                iOException = null;
            } catch (IOException e2) {
                iOException = e2;
            }
            synchronized (this) {
                if (!this.n.isEmpty()) {
                    e[] eVarArr2 = (e[]) this.n.values().toArray(new e[this.n.size()]);
                    this.n.clear();
                    b(false);
                    eVarArr = eVarArr2;
                } else {
                    eVarArr = null;
                }
                if (this.u != null) {
                    l[] lVarArr2 = (l[]) this.u.values().toArray(new l[this.u.size()]);
                    this.u = null;
                    lVarArr = lVarArr2;
                } else {
                    lVarArr = null;
                }
            }
            if (eVarArr != null) {
                IOException iOException2 = iOException;
                for (e a2 : eVarArr) {
                    try {
                        a2.a(aVar2);
                    } catch (IOException e3) {
                        if (iOException2 != null) {
                            iOException2 = e3;
                        }
                    }
                }
                iOException = iOException2;
            }
            if (lVarArr != null) {
                for (l c2 : lVarArr) {
                    c2.c();
                }
            }
            try {
                this.i.close();
                e = iOException;
            } catch (IOException e4) {
                e = e4;
                if (iOException != null) {
                    e = iOException;
                }
            }
            try {
                this.h.close();
            } catch (IOException e5) {
                e = e5;
            }
            if (e != null) {
                throw e;
            }
            return;
        }
        throw new AssertionError();
    }

    /* access modifiers changed from: private */
    public void a(boolean z, int i2, int i3, l lVar) {
        final boolean z2 = z;
        final int i4 = i2;
        final int i5 = i3;
        final l lVar2 = lVar;
        l.execute(new h("OkHttp %s ping %08x%08x", new Object[]{this.o, Integer.valueOf(i2), Integer.valueOf(i3)}) {
            public void b() {
                try {
                    d.this.b(z2, i4, i5, lVar2);
                } catch (IOException e2) {
                }
            }
        });
    }

    private synchronized void b(boolean z) {
        this.s = z ? System.nanoTime() : Long.MAX_VALUE;
    }

    /* access modifiers changed from: private */
    public void b(boolean z, int i2, int i3, l lVar) throws IOException {
        synchronized (this.i) {
            if (lVar != null) {
                lVar.a();
            }
            this.i.a(z, i2, i3);
        }
    }

    /* access modifiers changed from: private */
    public synchronized l c(int i2) {
        return this.u != null ? (l) this.u.remove(Integer.valueOf(i2)) : null;
    }

    /* access modifiers changed from: private */
    public void c(int i2, a aVar) {
        final int i3 = i2;
        final a aVar2 = aVar;
        this.t.execute(new h("OkHttp %s Push Reset[%s]", new Object[]{this.o, Integer.valueOf(i2)}) {
            public void b() {
                d.this.v.a(i3, aVar2);
                synchronized (d.this) {
                    d.this.y.remove(Integer.valueOf(i3));
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public boolean d(int i2) {
        return this.f8a == x.HTTP_2 && i2 != 0 && (i2 & 1) == 0;
    }

    /* access modifiers changed from: 0000 */
    public synchronized e a(int i2) {
        return (e) this.n.get(Integer.valueOf(i2));
    }

    public e a(List<f> list, boolean z, boolean z2) throws IOException {
        return a(0, list, z, z2);
    }

    public x a() {
        return this.f8a;
    }

    /* access modifiers changed from: 0000 */
    public void a(int i2, long j2) {
        final int i3 = i2;
        final long j3 = j2;
        l.execute(new h("OkHttp Window Update %s stream %d", new Object[]{this.o, Integer.valueOf(i2)}) {
            public void b() {
                try {
                    d.this.i.a(i3, j3);
                } catch (IOException e) {
                }
            }
        });
    }

    /* access modifiers changed from: 0000 */
    public void a(int i2, a aVar) {
        final int i3 = i2;
        final a aVar2 = aVar;
        l.submit(new h("OkHttp %s stream %d", new Object[]{this.o, Integer.valueOf(i2)}) {
            public void b() {
                try {
                    d.this.b(i3, aVar2);
                } catch (IOException e) {
                }
            }
        });
    }

    public void a(int i2, boolean z, b.c cVar, long j2) throws IOException {
        int min;
        if (j2 == 0) {
            this.i.a(z, i2, cVar, 0);
            return;
        }
        while (j2 > 0) {
            synchronized (this) {
                while (this.d <= 0) {
                    try {
                        if (!this.n.containsKey(Integer.valueOf(i2))) {
                            throw new IOException("stream closed");
                        }
                        wait();
                    } catch (InterruptedException e2) {
                        throw new InterruptedIOException();
                    }
                }
                min = Math.min((int) Math.min(j2, this.d), this.i.c());
                this.d -= (long) min;
            }
            j2 -= (long) min;
            this.i.a(z && j2 == 0, i2, cVar, min);
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(long j2) {
        this.d += j2;
        if (j2 > 0) {
            notifyAll();
        }
    }

    public void a(a aVar) throws IOException {
        synchronized (this.i) {
            synchronized (this) {
                if (!this.r) {
                    this.r = true;
                    int i2 = this.p;
                    this.i.a(i2, aVar, l.f138a);
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(boolean z) throws IOException {
        if (z) {
            this.i.a();
            this.i.b(this.e);
            int f2 = this.e.f(65536);
            if (f2 != 65536) {
                this.i.a(0, (long) (f2 - 65536));
            }
        }
        new Thread(this.j).start();
    }

    public synchronized int b() {
        return this.f.d(Integer.MAX_VALUE);
    }

    /* access modifiers changed from: 0000 */
    public synchronized e b(int i2) {
        e eVar;
        eVar = (e) this.n.remove(Integer.valueOf(i2));
        if (eVar != null && this.n.isEmpty()) {
            b(true);
        }
        notifyAll();
        return eVar;
    }

    /* access modifiers changed from: 0000 */
    public void b(int i2, a aVar) throws IOException {
        this.i.a(i2, aVar);
    }

    public void c() throws IOException {
        this.i.b();
    }

    public void close() throws IOException {
        a(a.NO_ERROR, a.CANCEL);
    }

    public void d() throws IOException {
        a(true);
    }
}
