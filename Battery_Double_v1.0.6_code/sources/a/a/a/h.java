package a.a.a;

import b.c;
import b.e;
import b.f;
import b.l;
import b.s;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

final class h {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public static final f[] f35a = {new f(f.e, ""), new f(f.f32b, "GET"), new f(f.f32b, "POST"), new f(f.c, "/"), new f(f.c, "/index.html"), new f(f.d, "http"), new f(f.d, "https"), new f(f.f31a, "200"), new f(f.f31a, "204"), new f(f.f31a, "206"), new f(f.f31a, "304"), new f(f.f31a, "400"), new f(f.f31a, "404"), new f(f.f31a, "500"), new f("accept-charset", ""), new f("accept-encoding", "gzip, deflate"), new f("accept-language", ""), new f("accept-ranges", ""), new f("accept", ""), new f("access-control-allow-origin", ""), new f("age", ""), new f("allow", ""), new f("authorization", ""), new f("cache-control", ""), new f("content-disposition", ""), new f("content-encoding", ""), new f("content-language", ""), new f("content-length", ""), new f("content-location", ""), new f("content-range", ""), new f("content-type", ""), new f("cookie", ""), new f("date", ""), new f("etag", ""), new f("expect", ""), new f("expires", ""), new f("from", ""), new f("host", ""), new f("if-match", ""), new f("if-modified-since", ""), new f("if-none-match", ""), new f("if-range", ""), new f("if-unmodified-since", ""), new f("last-modified", ""), new f("link", ""), new f("location", ""), new f("max-forwards", ""), new f("proxy-authenticate", ""), new f("proxy-authorization", ""), new f("range", ""), new f("referer", ""), new f("refresh", ""), new f("retry-after", ""), new f("server", ""), new f("set-cookie", ""), new f("strict-transport-security", ""), new f("transfer-encoding", ""), new f("user-agent", ""), new f("vary", ""), new f("via", ""), new f("www-authenticate", "")};
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public static final Map<f, Integer> f36b = c();

    static final class a {

        /* renamed from: a reason: collision with root package name */
        f[] f37a = new f[8];

        /* renamed from: b reason: collision with root package name */
        int f38b = (this.f37a.length - 1);
        int c = 0;
        int d = 0;
        private final List<f> e = new ArrayList();
        private final e f;
        private int g;
        private int h;

        a(int i, s sVar) {
            this.g = i;
            this.h = i;
            this.f = l.a(sVar);
        }

        private void a(int i, f fVar) {
            this.e.add(fVar);
            int i2 = fVar.j;
            if (i != -1) {
                i2 -= this.f37a[d(i)].j;
            }
            if (i2 > this.h) {
                e();
                return;
            }
            int b2 = b((this.d + i2) - this.h);
            if (i == -1) {
                if (this.c + 1 > this.f37a.length) {
                    f[] fVarArr = new f[(this.f37a.length * 2)];
                    System.arraycopy(this.f37a, 0, fVarArr, this.f37a.length, this.f37a.length);
                    this.f38b = this.f37a.length - 1;
                    this.f37a = fVarArr;
                }
                int i3 = this.f38b;
                this.f38b = i3 - 1;
                this.f37a[i3] = fVar;
                this.c++;
            } else {
                this.f37a[b2 + d(i) + i] = fVar;
            }
            this.d = i2 + this.d;
        }

        private int b(int i) {
            int i2 = 0;
            if (i > 0) {
                int length = this.f37a.length;
                while (true) {
                    length--;
                    if (length < this.f38b || i <= 0) {
                        System.arraycopy(this.f37a, this.f38b + 1, this.f37a, this.f38b + 1 + i2, this.c);
                        this.f38b += i2;
                    } else {
                        i -= this.f37a[length].j;
                        this.d -= this.f37a[length].j;
                        this.c--;
                        i2++;
                    }
                }
                System.arraycopy(this.f37a, this.f38b + 1, this.f37a, this.f38b + 1 + i2, this.c);
                this.f38b += i2;
            }
            return i2;
        }

        private void c(int i) throws IOException {
            if (h(i)) {
                this.e.add(h.f35a[i]);
                return;
            }
            int d2 = d(i - h.f35a.length);
            if (d2 < 0 || d2 > this.f37a.length - 1) {
                throw new IOException("Header index too large " + (i + 1));
            }
            this.e.add(this.f37a[d2]);
        }

        private int d(int i) {
            return this.f38b + 1 + i;
        }

        private void d() {
            if (this.h >= this.d) {
                return;
            }
            if (this.h == 0) {
                e();
            } else {
                b(this.d - this.h);
            }
        }

        private void e() {
            this.e.clear();
            Arrays.fill(this.f37a, null);
            this.f38b = this.f37a.length - 1;
            this.c = 0;
            this.d = 0;
        }

        private void e(int i) throws IOException {
            this.e.add(new f(g(i), c()));
        }

        private void f() throws IOException {
            this.e.add(new f(h.b(c()), c()));
        }

        private void f(int i) throws IOException {
            a(-1, new f(g(i), c()));
        }

        private f g(int i) {
            return h(i) ? h.f35a[i].h : this.f37a[d(i - h.f35a.length)].h;
        }

        private void g() throws IOException {
            a(-1, new f(h.b(c()), c()));
        }

        private int h() throws IOException {
            return this.f.i() & 255;
        }

        private boolean h(int i) {
            return i >= 0 && i <= h.f35a.length + -1;
        }

        /* access modifiers changed from: 0000 */
        public int a(int i, int i2) throws IOException {
            int i3 = i & i2;
            if (i3 < i2) {
                return i3;
            }
            int i4 = 0;
            while (true) {
                int h2 = h();
                if ((h2 & 128) == 0) {
                    return (h2 << i4) + i2;
                }
                i2 += (h2 & 127) << i4;
                i4 += 7;
            }
        }

        /* access modifiers changed from: 0000 */
        public void a() throws IOException {
            while (!this.f.f()) {
                byte i = this.f.i() & 255;
                if (i == 128) {
                    throw new IOException("index == 0");
                } else if ((i & 128) == 128) {
                    c(a((int) i, 127) - 1);
                } else if (i == 64) {
                    g();
                } else if ((i & 64) == 64) {
                    f(a((int) i, 63) - 1);
                } else if ((i & 32) == 32) {
                    this.h = a((int) i, 31);
                    if (this.h < 0 || this.h > this.g) {
                        throw new IOException("Invalid dynamic table size update " + this.h);
                    }
                    d();
                } else if (i == 16 || i == 0) {
                    f();
                } else {
                    e(a((int) i, 15) - 1);
                }
            }
        }

        /* access modifiers changed from: 0000 */
        public void a(int i) {
            this.g = i;
            this.h = i;
            d();
        }

        public List<f> b() {
            ArrayList arrayList = new ArrayList(this.e);
            this.e.clear();
            return arrayList;
        }

        /* access modifiers changed from: 0000 */
        public f c() throws IOException {
            int h2 = h();
            boolean z = (h2 & 128) == 128;
            int a2 = a(h2, 127);
            return z ? f.a(j.a().a(this.f.f((long) a2))) : this.f.c((long) a2);
        }
    }

    static final class b {

        /* renamed from: a reason: collision with root package name */
        private final c f39a;

        b(c cVar) {
            this.f39a = cVar;
        }

        /* access modifiers changed from: 0000 */
        public void a(int i, int i2, int i3) throws IOException {
            if (i < i2) {
                this.f39a.i(i3 | i);
                return;
            }
            this.f39a.i(i3 | i2);
            int i4 = i - i2;
            while (i4 >= 128) {
                this.f39a.i((i4 & 127) | 128);
                i4 >>>= 7;
            }
            this.f39a.i(i4);
        }

        /* access modifiers changed from: 0000 */
        public void a(f fVar) throws IOException {
            a(fVar.e(), 127, 0);
            this.f39a.b(fVar);
        }

        /* access modifiers changed from: 0000 */
        public void a(List<f> list) throws IOException {
            int size = list.size();
            for (int i = 0; i < size; i++) {
                f d = ((f) list.get(i)).h.d();
                Integer num = (Integer) h.f36b.get(d);
                if (num != null) {
                    a(num.intValue() + 1, 15, 0);
                    a(((f) list.get(i)).i);
                } else {
                    this.f39a.i(0);
                    a(d);
                    a(((f) list.get(i)).i);
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public static f b(f fVar) throws IOException {
        int i = 0;
        int e = fVar.e();
        while (i < e) {
            byte a2 = fVar.a(i);
            if (a2 < 65 || a2 > 90) {
                i++;
            } else {
                throw new IOException("PROTOCOL_ERROR response malformed: mixed case name: " + fVar.a());
            }
        }
        return fVar;
    }

    private static Map<f, Integer> c() {
        LinkedHashMap linkedHashMap = new LinkedHashMap(f35a.length);
        for (int i = 0; i < f35a.length; i++) {
            if (!linkedHashMap.containsKey(f35a[i].h)) {
                linkedHashMap.put(f35a[i].h, Integer.valueOf(i));
            }
        }
        return Collections.unmodifiableMap(linkedHashMap);
    }
}
