package a.a.a;

import java.util.concurrent.CountDownLatch;

public final class l {

    /* renamed from: a reason: collision with root package name */
    private final CountDownLatch f58a = new CountDownLatch(1);

    /* renamed from: b reason: collision with root package name */
    private long f59b = -1;
    private long c = -1;

    l() {
    }

    /* access modifiers changed from: 0000 */
    public void a() {
        if (this.f59b != -1) {
            throw new IllegalStateException();
        }
        this.f59b = System.nanoTime();
    }

    /* access modifiers changed from: 0000 */
    public void b() {
        if (this.c != -1 || this.f59b == -1) {
            throw new IllegalStateException();
        }
        this.c = System.nanoTime();
        this.f58a.countDown();
    }

    /* access modifiers changed from: 0000 */
    public void c() {
        if (this.c != -1 || this.f59b == -1) {
            throw new IllegalStateException();
        }
        this.c = this.f59b - 1;
        this.f58a.countDown();
    }
}
