package a.a.a;

import a.a.l;
import b.e;
import b.f;
import b.s;
import b.t;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class i implements q {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public static final Logger f40a = Logger.getLogger(b.class.getName());
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public static final f f41b = f.a("PRI * HTTP/2.0\r\n\r\nSM\r\n\r\n");

    static final class a implements s {

        /* renamed from: a reason: collision with root package name */
        int f42a;

        /* renamed from: b reason: collision with root package name */
        byte f43b;
        int c;
        int d;
        short e;
        private final e f;

        public a(e eVar) {
            this.f = eVar;
        }

        private void a() throws IOException {
            int i = this.c;
            int a2 = i.b(this.f);
            this.d = a2;
            this.f42a = a2;
            byte i2 = (byte) (this.f.i() & 255);
            this.f43b = (byte) (this.f.i() & 255);
            if (i.f40a.isLoggable(Level.FINE)) {
                i.f40a.fine(b.a(true, this.c, this.f42a, i2, this.f43b));
            }
            this.c = this.f.k() & Integer.MAX_VALUE;
            if (i2 != 9) {
                throw i.d("%s != TYPE_CONTINUATION", Byte.valueOf(i2));
            } else if (this.c != i) {
                throw i.d("TYPE_CONTINUATION streamId changed", new Object[0]);
            }
        }

        public void close() throws IOException {
        }

        public long read(b.c cVar, long j) throws IOException {
            while (this.d == 0) {
                this.f.g((long) this.e);
                this.e = 0;
                if ((this.f43b & 4) != 0) {
                    return -1;
                }
                a();
            }
            long read = this.f.read(cVar, Math.min(j, (long) this.d));
            if (read == -1) {
                return -1;
            }
            this.d = (int) (((long) this.d) - read);
            return read;
        }

        public t timeout() {
            return this.f.timeout();
        }
    }

    static final class b {

        /* renamed from: a reason: collision with root package name */
        private static final String[] f44a = {"DATA", "HEADERS", "PRIORITY", "RST_STREAM", "SETTINGS", "PUSH_PROMISE", "PING", "GOAWAY", "WINDOW_UPDATE", "CONTINUATION"};

        /* renamed from: b reason: collision with root package name */
        private static final String[] f45b = new String[64];
        private static final String[] c = new String[256];

        static {
            int[] iArr;
            for (int i = 0; i < c.length; i++) {
                c[i] = l.a("%8s", Integer.toBinaryString(i)).replace(' ', '0');
            }
            f45b[0] = "";
            f45b[1] = "END_STREAM";
            int[] iArr2 = {1};
            f45b[8] = "PADDED";
            for (int i2 : iArr2) {
                f45b[i2 | 8] = f45b[i2] + "|PADDED";
            }
            f45b[4] = "END_HEADERS";
            f45b[32] = "PRIORITY";
            f45b[36] = "END_HEADERS|PRIORITY";
            for (int i3 : new int[]{4, 32, 36}) {
                for (int i4 : iArr2) {
                    f45b[i4 | i3] = f45b[i4] + '|' + f45b[i3];
                    f45b[i4 | i3 | 8] = f45b[i4] + '|' + f45b[i3] + "|PADDED";
                }
            }
            for (int i5 = 0; i5 < f45b.length; i5++) {
                if (f45b[i5] == null) {
                    f45b[i5] = c[i5];
                }
            }
        }

        b() {
        }

        static String a(byte b2, byte b3) {
            if (b3 == 0) {
                return "";
            }
            switch (b2) {
                case 2:
                case 3:
                case 7:
                case 8:
                    return c[b3];
                case 4:
                case 6:
                    return b3 == 1 ? "ACK" : c[b3];
                default:
                    String str = b3 < f45b.length ? f45b[b3] : c[b3];
                    return (b2 != 5 || (b3 & 4) == 0) ? (b2 != 0 || (b3 & 32) == 0) ? str : str.replace("PRIORITY", "COMPRESSED") : str.replace("HEADERS", "PUSH_PROMISE");
            }
        }

        static String a(boolean z, int i, int i2, byte b2, byte b3) {
            String a2 = b2 < f44a.length ? f44a[b2] : l.a("0x%02x", Byte.valueOf(b2));
            String a3 = a(b2, b3);
            String str = "%s 0x%08x %5d %-13s %s";
            Object[] objArr = new Object[5];
            objArr[0] = z ? "<<" : ">>";
            objArr[1] = Integer.valueOf(i);
            objArr[2] = Integer.valueOf(i2);
            objArr[3] = a2;
            objArr[4] = a3;
            return l.a(str, objArr);
        }
    }

    static final class c implements b {

        /* renamed from: a reason: collision with root package name */
        final a f46a;

        /* renamed from: b reason: collision with root package name */
        private final e f47b;
        private final a c = new a(this.f47b);
        private final boolean d;

        c(e eVar, int i, boolean z) {
            this.f47b = eVar;
            this.d = z;
            this.f46a = new a(i, this.c);
        }

        private List<f> a(int i, short s, byte b2, int i2) throws IOException {
            a aVar = this.c;
            this.c.d = i;
            aVar.f42a = i;
            this.c.e = s;
            this.c.f43b = b2;
            this.c.c = i2;
            this.f46a.a();
            return this.f46a.b();
        }

        private void a(a.a.a.b.a aVar, int i) throws IOException {
            int k = this.f47b.k();
            aVar.a(i, k & Integer.MAX_VALUE, (this.f47b.i() & 255) + 1, (Integer.MIN_VALUE & k) != 0);
        }

        private void a(a.a.a.b.a aVar, int i, byte b2, int i2) throws IOException {
            if (i2 == 0) {
                throw i.d("PROTOCOL_ERROR: TYPE_HEADERS streamId == 0", new Object[0]);
            }
            boolean z = (b2 & 1) != 0;
            short s = (b2 & 8) != 0 ? (short) (this.f47b.i() & 255) : 0;
            if ((b2 & 32) != 0) {
                a(aVar, i2);
                i -= 5;
            }
            aVar.a(false, z, i2, -1, a(i.b(i, b2, s), s, b2, i2), g.HTTP_20_HEADERS);
        }

        private void b(a.a.a.b.a aVar, int i, byte b2, int i2) throws IOException {
            boolean z = true;
            short s = 0;
            boolean z2 = (b2 & 1) != 0;
            if ((b2 & 32) == 0) {
                z = false;
            }
            if (z) {
                throw i.d("PROTOCOL_ERROR: FLAG_COMPRESSED without SETTINGS_COMPRESS_DATA", new Object[0]);
            }
            if ((b2 & 8) != 0) {
                s = (short) (this.f47b.i() & 255);
            }
            aVar.a(z2, i2, this.f47b, i.b(i, b2, s));
            this.f47b.g((long) s);
        }

        private void c(a.a.a.b.a aVar, int i, byte b2, int i2) throws IOException {
            if (i != 5) {
                throw i.d("TYPE_PRIORITY length: %d != 5", Integer.valueOf(i));
            } else if (i2 == 0) {
                throw i.d("TYPE_PRIORITY streamId == 0", new Object[0]);
            } else {
                a(aVar, i2);
            }
        }

        private void d(a.a.a.b.a aVar, int i, byte b2, int i2) throws IOException {
            if (i != 4) {
                throw i.d("TYPE_RST_STREAM length: %d != 4", Integer.valueOf(i));
            } else if (i2 == 0) {
                throw i.d("TYPE_RST_STREAM streamId == 0", new Object[0]);
            } else {
                int k = this.f47b.k();
                a b3 = a.b(k);
                if (b3 == null) {
                    throw i.d("TYPE_RST_STREAM unexpected error code: %d", Integer.valueOf(k));
                } else {
                    aVar.a(i2, b3);
                }
            }
        }

        private void e(a.a.a.b.a aVar, int i, byte b2, int i2) throws IOException {
            if (i2 != 0) {
                throw i.d("TYPE_SETTINGS streamId != 0", new Object[0]);
            } else if ((b2 & 1) != 0) {
                if (i != 0) {
                    throw i.d("FRAME_SIZE_ERROR ack frame should be empty!", new Object[0]);
                }
                aVar.a();
            } else if (i % 6 != 0) {
                throw i.d("TYPE_SETTINGS length %% 6 != 0: %s", Integer.valueOf(i));
            } else {
                n nVar = new n();
                for (int i3 = 0; i3 < i; i3 += 6) {
                    short j = this.f47b.j();
                    int k = this.f47b.k();
                    switch (j) {
                        case 2:
                            if (!(k == 0 || k == 1)) {
                                throw i.d("PROTOCOL_ERROR SETTINGS_ENABLE_PUSH != 0 or 1", new Object[0]);
                            }
                        case 3:
                            j = 4;
                            break;
                        case 4:
                            j = 7;
                            if (k >= 0) {
                                break;
                            } else {
                                throw i.d("PROTOCOL_ERROR SETTINGS_INITIAL_WINDOW_SIZE > 2^31 - 1", new Object[0]);
                            }
                        case 5:
                            if (k >= 16384 && k <= 16777215) {
                                break;
                            } else {
                                throw i.d("PROTOCOL_ERROR SETTINGS_MAX_FRAME_SIZE: %s", Integer.valueOf(k));
                            }
                    }
                    nVar.a(j, 0, k);
                }
                aVar.a(false, nVar);
                if (nVar.c() >= 0) {
                    this.f46a.a(nVar.c());
                }
            }
        }

        private void f(a.a.a.b.a aVar, int i, byte b2, int i2) throws IOException {
            short s = 0;
            if (i2 == 0) {
                throw i.d("PROTOCOL_ERROR: TYPE_PUSH_PROMISE streamId == 0", new Object[0]);
            }
            if ((b2 & 8) != 0) {
                s = (short) (this.f47b.i() & 255);
            }
            aVar.a(i2, this.f47b.k() & Integer.MAX_VALUE, a(i.b(i - 4, b2, s), s, b2, i2));
        }

        private void g(a.a.a.b.a aVar, int i, byte b2, int i2) throws IOException {
            boolean z = true;
            if (i != 8) {
                throw i.d("TYPE_PING length != 8: %s", Integer.valueOf(i));
            } else if (i2 != 0) {
                throw i.d("TYPE_PING streamId != 0", new Object[0]);
            } else {
                int k = this.f47b.k();
                int k2 = this.f47b.k();
                if ((b2 & 1) == 0) {
                    z = false;
                }
                aVar.a(z, k, k2);
            }
        }

        private void h(a.a.a.b.a aVar, int i, byte b2, int i2) throws IOException {
            if (i < 8) {
                throw i.d("TYPE_GOAWAY length < 8: %s", Integer.valueOf(i));
            } else if (i2 != 0) {
                throw i.d("TYPE_GOAWAY streamId != 0", new Object[0]);
            } else {
                int k = this.f47b.k();
                int k2 = this.f47b.k();
                int i3 = i - 8;
                a b3 = a.b(k2);
                if (b3 == null) {
                    throw i.d("TYPE_GOAWAY unexpected error code: %d", Integer.valueOf(k2));
                }
                f fVar = f.f1371b;
                if (i3 > 0) {
                    fVar = this.f47b.c((long) i3);
                }
                aVar.a(k, b3, fVar);
            }
        }

        private void i(a.a.a.b.a aVar, int i, byte b2, int i2) throws IOException {
            if (i != 4) {
                throw i.d("TYPE_WINDOW_UPDATE length !=4: %s", Integer.valueOf(i));
            }
            long k = ((long) this.f47b.k()) & 2147483647L;
            if (k == 0) {
                throw i.d("windowSizeIncrement was 0", Long.valueOf(k));
            } else {
                aVar.a(i2, k);
            }
        }

        public void a() throws IOException {
            if (!this.d) {
                f c2 = this.f47b.c((long) i.f41b.e());
                if (i.f40a.isLoggable(Level.FINE)) {
                    i.f40a.fine(l.a("<< CONNECTION %s", c2.c()));
                }
                if (!i.f41b.equals(c2)) {
                    throw i.d("Expected a connection header but was %s", c2.a());
                }
            }
        }

        public boolean a(a.a.a.b.a aVar) throws IOException {
            try {
                this.f47b.a(9);
                int a2 = i.b(this.f47b);
                if (a2 < 0 || a2 > 16384) {
                    throw i.d("FRAME_SIZE_ERROR: %s", Integer.valueOf(a2));
                }
                byte i = (byte) (this.f47b.i() & 255);
                byte i2 = (byte) (this.f47b.i() & 255);
                int k = this.f47b.k() & Integer.MAX_VALUE;
                if (i.f40a.isLoggable(Level.FINE)) {
                    i.f40a.fine(b.a(true, k, a2, i, i2));
                }
                switch (i) {
                    case 0:
                        b(aVar, a2, i2, k);
                        return true;
                    case 1:
                        a(aVar, a2, i2, k);
                        return true;
                    case 2:
                        c(aVar, a2, i2, k);
                        return true;
                    case 3:
                        d(aVar, a2, i2, k);
                        return true;
                    case 4:
                        e(aVar, a2, i2, k);
                        return true;
                    case 5:
                        f(aVar, a2, i2, k);
                        return true;
                    case 6:
                        g(aVar, a2, i2, k);
                        return true;
                    case 7:
                        h(aVar, a2, i2, k);
                        return true;
                    case 8:
                        i(aVar, a2, i2, k);
                        return true;
                    default:
                        this.f47b.g((long) a2);
                        return true;
                }
            } catch (IOException e) {
                return false;
            }
        }

        public void close() throws IOException {
            this.f47b.close();
        }
    }

    static final class d implements c {

        /* renamed from: a reason: collision with root package name */
        private final b.d f48a;

        /* renamed from: b reason: collision with root package name */
        private final boolean f49b;
        private final b.c c = new b.c();
        private final b d = new b(this.c);
        private int e = 16384;
        private boolean f;

        d(b.d dVar, boolean z) {
            this.f48a = dVar;
            this.f49b = z;
        }

        private void b(int i, long j) throws IOException {
            while (j > 0) {
                int min = (int) Math.min((long) this.e, j);
                j -= (long) min;
                a(i, min, 9, j == 0 ? (byte) 4 : 0);
                this.f48a.a(this.c, (long) min);
            }
        }

        public synchronized void a() throws IOException {
            if (this.f) {
                throw new IOException("closed");
            } else if (this.f49b) {
                if (i.f40a.isLoggable(Level.FINE)) {
                    i.f40a.fine(l.a(">> CONNECTION %s", i.f41b.c()));
                }
                this.f48a.c(i.f41b.f());
                this.f48a.flush();
            }
        }

        /* access modifiers changed from: 0000 */
        public void a(int i, byte b2, b.c cVar, int i2) throws IOException {
            a(i, i2, 0, b2);
            if (i2 > 0) {
                this.f48a.a(cVar, (long) i2);
            }
        }

        /* access modifiers changed from: 0000 */
        public void a(int i, int i2, byte b2, byte b3) throws IOException {
            if (i.f40a.isLoggable(Level.FINE)) {
                i.f40a.fine(b.a(false, i, i2, b2, b3));
            }
            if (i2 > this.e) {
                throw i.c("FRAME_SIZE_ERROR length > %d: %d", Integer.valueOf(this.e), Integer.valueOf(i2));
            } else if ((Integer.MIN_VALUE & i) != 0) {
                throw i.c("reserved bit set: %s", Integer.valueOf(i));
            } else {
                i.b(this.f48a, i2);
                this.f48a.i(b2 & 255);
                this.f48a.i(b3 & 255);
                this.f48a.g(Integer.MAX_VALUE & i);
            }
        }

        public synchronized void a(int i, int i2, List<f> list) throws IOException {
            if (this.f) {
                throw new IOException("closed");
            }
            this.d.a(list);
            long a2 = this.c.a();
            int min = (int) Math.min((long) (this.e - 4), a2);
            a(i, min + 4, 5, a2 == ((long) min) ? (byte) 4 : 0);
            this.f48a.g(Integer.MAX_VALUE & i2);
            this.f48a.a(this.c, (long) min);
            if (a2 > ((long) min)) {
                b(i, a2 - ((long) min));
            }
        }

        public synchronized void a(int i, long j) throws IOException {
            if (this.f) {
                throw new IOException("closed");
            } else if (j == 0 || j > 2147483647L) {
                throw i.c("windowSizeIncrement == 0 || windowSizeIncrement > 0x7fffffffL: %s", Long.valueOf(j));
            } else {
                a(i, 4, 8, 0);
                this.f48a.g((int) j);
                this.f48a.flush();
            }
        }

        public synchronized void a(int i, a aVar) throws IOException {
            if (this.f) {
                throw new IOException("closed");
            } else if (aVar.s == -1) {
                throw new IllegalArgumentException();
            } else {
                a(i, 4, 3, 0);
                this.f48a.g(aVar.s);
                this.f48a.flush();
            }
        }

        public synchronized void a(int i, a aVar, byte[] bArr) throws IOException {
            if (this.f) {
                throw new IOException("closed");
            } else if (aVar.s == -1) {
                throw i.c("errorCode.httpCode == -1", new Object[0]);
            } else {
                a(0, bArr.length + 8, 7, 0);
                this.f48a.g(i);
                this.f48a.g(aVar.s);
                if (bArr.length > 0) {
                    this.f48a.c(bArr);
                }
                this.f48a.flush();
            }
        }

        public synchronized void a(n nVar) throws IOException {
            if (this.f) {
                throw new IOException("closed");
            }
            this.e = nVar.e(this.e);
            a(0, 0, 4, 1);
            this.f48a.flush();
        }

        public synchronized void a(boolean z, int i, int i2) throws IOException {
            byte b2 = 0;
            synchronized (this) {
                if (this.f) {
                    throw new IOException("closed");
                }
                if (z) {
                    b2 = 1;
                }
                a(0, 8, 6, b2);
                this.f48a.g(i);
                this.f48a.g(i2);
                this.f48a.flush();
            }
        }

        public synchronized void a(boolean z, int i, b.c cVar, int i2) throws IOException {
            if (this.f) {
                throw new IOException("closed");
            }
            byte b2 = 0;
            if (z) {
                b2 = (byte) 1;
            }
            a(i, b2, cVar, i2);
        }

        /* access modifiers changed from: 0000 */
        public void a(boolean z, int i, List<f> list) throws IOException {
            if (this.f) {
                throw new IOException("closed");
            }
            this.d.a(list);
            long a2 = this.c.a();
            int min = (int) Math.min((long) this.e, a2);
            byte b2 = a2 == ((long) min) ? (byte) 4 : 0;
            if (z) {
                b2 = (byte) (b2 | 1);
            }
            a(i, min, 1, b2);
            this.f48a.a(this.c, (long) min);
            if (a2 > ((long) min)) {
                b(i, a2 - ((long) min));
            }
        }

        public synchronized void a(boolean z, boolean z2, int i, int i2, List<f> list) throws IOException {
            if (z2) {
                throw new UnsupportedOperationException();
            } else if (this.f) {
                throw new IOException("closed");
            } else {
                a(z, i, list);
            }
        }

        public synchronized void b() throws IOException {
            if (this.f) {
                throw new IOException("closed");
            }
            this.f48a.flush();
        }

        public synchronized void b(n nVar) throws IOException {
            int i = 0;
            synchronized (this) {
                if (this.f) {
                    throw new IOException("closed");
                }
                a(0, nVar.b() * 6, 4, 0);
                while (i < 10) {
                    if (nVar.a(i)) {
                        int i2 = i == 4 ? 3 : i == 7 ? 4 : i;
                        this.f48a.h(i2);
                        this.f48a.g(nVar.b(i));
                    }
                    i++;
                }
                this.f48a.flush();
            }
        }

        public int c() {
            return this.e;
        }

        public synchronized void close() throws IOException {
            this.f = true;
            this.f48a.close();
        }
    }

    /* access modifiers changed from: private */
    public static int b(int i, byte b2, short s) throws IOException {
        if ((b2 & 8) != 0) {
            i--;
        }
        if (s <= i) {
            return (short) (i - s);
        }
        throw d("PROTOCOL_ERROR padding %s > remaining length %s", Short.valueOf(s), Integer.valueOf(i));
    }

    /* access modifiers changed from: private */
    public static int b(e eVar) throws IOException {
        return ((eVar.i() & 255) << 16) | ((eVar.i() & 255) << 8) | (eVar.i() & 255);
    }

    /* access modifiers changed from: private */
    public static void b(b.d dVar, int i) throws IOException {
        dVar.i((i >>> 16) & 255);
        dVar.i((i >>> 8) & 255);
        dVar.i(i & 255);
    }

    /* access modifiers changed from: private */
    public static IllegalArgumentException c(String str, Object... objArr) {
        throw new IllegalArgumentException(l.a(str, objArr));
    }

    /* access modifiers changed from: private */
    public static IOException d(String str, Object... objArr) throws IOException {
        throw new IOException(l.a(str, objArr));
    }

    public b a(e eVar, boolean z) {
        return new c(eVar, 4096, z);
    }

    public c a(b.d dVar, boolean z) {
        return new d(dVar, z);
    }
}
