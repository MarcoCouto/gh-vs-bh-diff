package a.a.a;

import b.r;
import b.s;
import b.t;
import java.io.EOFException;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

public final class e {
    static final /* synthetic */ boolean d = (!e.class.desiredAssertionStatus());

    /* renamed from: a reason: collision with root package name */
    long f24a = 0;

    /* renamed from: b reason: collision with root package name */
    long f25b;
    final a c;
    /* access modifiers changed from: private */
    public final int e;
    /* access modifiers changed from: private */
    public final d f;
    private final List<f> g;
    private List<f> h;
    private final b i;
    /* access modifiers changed from: private */
    public final c j = new c();
    /* access modifiers changed from: private */
    public final c k = new c();
    /* access modifiers changed from: private */
    public a l = null;

    final class a implements r {

        /* renamed from: a reason: collision with root package name */
        static final /* synthetic */ boolean f26a = (!e.class.desiredAssertionStatus());
        private final b.c c = new b.c();
        /* access modifiers changed from: private */
        public boolean d;
        /* access modifiers changed from: private */
        public boolean e;

        a() {
        }

        private void a(boolean z) throws IOException {
            long min;
            synchronized (e.this) {
                e.this.k.c();
                while (e.this.f25b <= 0 && !this.e && !this.d && e.this.l == null) {
                    try {
                        e.this.l();
                    } catch (Throwable th) {
                        e.this.k.b();
                        throw th;
                    }
                }
                e.this.k.b();
                e.this.k();
                min = Math.min(e.this.f25b, this.c.a());
                e.this.f25b -= min;
            }
            e.this.k.c();
            try {
                e.this.f.a(e.this.e, z && min == this.c.a(), this.c, min);
            } finally {
                e.this.k.b();
            }
        }

        public void a(b.c cVar, long j) throws IOException {
            if (f26a || !Thread.holdsLock(e.this)) {
                this.c.a(cVar, j);
                while (this.c.a() >= 16384) {
                    a(false);
                }
                return;
            }
            throw new AssertionError();
        }

        /* JADX WARNING: Code restructure failed: missing block: B:14:0x0025, code lost:
            if (r6.f27b.c.e != false) goto L_0x0052;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:16:0x002f, code lost:
            if (r6.c.a() <= 0) goto L_0x0042;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:18:0x0039, code lost:
            if (r6.c.a() <= 0) goto L_0x0052;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:19:0x003b, code lost:
            a(true);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:24:0x0042, code lost:
            a.a.a.e.a(r6.f27b).a(a.a.a.e.b(r6.f27b), true, (b.c) null, 0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:25:0x0052, code lost:
            r1 = r6.f27b;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:26:0x0054, code lost:
            monitor-enter(r1);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:29:?, code lost:
            r6.d = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:30:0x0058, code lost:
            monitor-exit(r1);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:31:0x0059, code lost:
            a.a.a.e.a(r6.f27b).c();
            a.a.a.e.f(r6.f27b);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:44:?, code lost:
            return;
         */
        public void close() throws IOException {
            if (f26a || !Thread.holdsLock(e.this)) {
                synchronized (e.this) {
                    if (this.d) {
                    }
                }
            } else {
                throw new AssertionError();
            }
        }

        public void flush() throws IOException {
            if (f26a || !Thread.holdsLock(e.this)) {
                synchronized (e.this) {
                    e.this.k();
                }
                while (this.c.a() > 0) {
                    a(false);
                    e.this.f.c();
                }
                return;
            }
            throw new AssertionError();
        }

        public t timeout() {
            return e.this.k;
        }
    }

    private final class b implements s {

        /* renamed from: a reason: collision with root package name */
        static final /* synthetic */ boolean f28a = (!e.class.desiredAssertionStatus());
        private final b.c c;
        private final b.c d;
        private final long e;
        /* access modifiers changed from: private */
        public boolean f;
        /* access modifiers changed from: private */
        public boolean g;

        private b(long j) {
            this.c = new b.c();
            this.d = new b.c();
            this.e = j;
        }

        private void a() throws IOException {
            e.this.j.c();
            while (this.d.a() == 0 && !this.g && !this.f && e.this.l == null) {
                try {
                    e.this.l();
                } finally {
                    e.this.j.b();
                }
            }
        }

        private void b() throws IOException {
            if (this.f) {
                throw new IOException("stream closed");
            } else if (e.this.l != null) {
                throw new p(e.this.l);
            }
        }

        /* access modifiers changed from: 0000 */
        public void a(b.e eVar, long j) throws IOException {
            boolean z;
            boolean z2;
            if (f28a || !Thread.holdsLock(e.this)) {
                while (j > 0) {
                    synchronized (e.this) {
                        z = this.g;
                        z2 = this.d.a() + j > this.e;
                    }
                    if (z2) {
                        eVar.g(j);
                        e.this.b(a.FLOW_CONTROL_ERROR);
                        return;
                    } else if (z) {
                        eVar.g(j);
                        return;
                    } else {
                        long read = eVar.read(this.c, j);
                        if (read == -1) {
                            throw new EOFException();
                        }
                        j -= read;
                        synchronized (e.this) {
                            boolean z3 = this.d.a() == 0;
                            this.d.a((s) this.c);
                            if (z3) {
                                e.this.notifyAll();
                            }
                        }
                    }
                }
                return;
            }
            throw new AssertionError();
        }

        public void close() throws IOException {
            synchronized (e.this) {
                this.f = true;
                this.d.s();
                e.this.notifyAll();
            }
            e.this.j();
        }

        public long read(b.c cVar, long j) throws IOException {
            long read;
            if (j < 0) {
                throw new IllegalArgumentException("byteCount < 0: " + j);
            }
            synchronized (e.this) {
                a();
                b();
                if (this.d.a() == 0) {
                    read = -1;
                } else {
                    read = this.d.read(cVar, Math.min(j, this.d.a()));
                    e.this.f24a += read;
                    if (e.this.f24a >= ((long) (e.this.f.e.f(65536) / 2))) {
                        e.this.f.a(e.this.e, e.this.f24a);
                        e.this.f24a = 0;
                    }
                    synchronized (e.this.f) {
                        d a2 = e.this.f;
                        a2.c += read;
                        if (e.this.f.c >= ((long) (e.this.f.e.f(65536) / 2))) {
                            e.this.f.a(0, e.this.f.c);
                            e.this.f.c = 0;
                        }
                    }
                }
            }
            return read;
        }

        public t timeout() {
            return e.this.j;
        }
    }

    class c extends b.a {
        c() {
        }

        /* access modifiers changed from: protected */
        public IOException a(IOException iOException) {
            SocketTimeoutException socketTimeoutException = new SocketTimeoutException("timeout");
            if (iOException != null) {
                socketTimeoutException.initCause(iOException);
            }
            return socketTimeoutException;
        }

        /* access modifiers changed from: protected */
        public void a() {
            e.this.b(a.CANCEL);
        }

        public void b() throws IOException {
            if (B_()) {
                throw a(null);
            }
        }
    }

    e(int i2, d dVar, boolean z, boolean z2, List<f> list) {
        if (dVar == null) {
            throw new NullPointerException("connection == null");
        } else if (list == null) {
            throw new NullPointerException("requestHeaders == null");
        } else {
            this.e = i2;
            this.f = dVar;
            this.f25b = (long) dVar.f.f(65536);
            this.i = new b((long) dVar.e.f(65536));
            this.c = new a();
            this.i.g = z2;
            this.c.e = z;
            this.g = list;
        }
    }

    private boolean d(a aVar) {
        if (d || !Thread.holdsLock(this)) {
            synchronized (this) {
                if (this.l != null) {
                    return false;
                }
                if (this.i.g && this.c.e) {
                    return false;
                }
                this.l = aVar;
                notifyAll();
                this.f.b(this.e);
                return true;
            }
        }
        throw new AssertionError();
    }

    /* access modifiers changed from: private */
    public void j() throws IOException {
        boolean z;
        boolean b2;
        if (d || !Thread.holdsLock(this)) {
            synchronized (this) {
                z = !this.i.g && this.i.f && (this.c.e || this.c.d);
                b2 = b();
            }
            if (z) {
                a(a.CANCEL);
            } else if (!b2) {
                this.f.b(this.e);
            }
        } else {
            throw new AssertionError();
        }
    }

    /* access modifiers changed from: private */
    public void k() throws IOException {
        if (this.c.d) {
            throw new IOException("stream closed");
        } else if (this.c.e) {
            throw new IOException("stream finished");
        } else if (this.l != null) {
            throw new p(this.l);
        }
    }

    /* access modifiers changed from: private */
    public void l() throws InterruptedIOException {
        try {
            wait();
        } catch (InterruptedException e2) {
            throw new InterruptedIOException();
        }
    }

    public int a() {
        return this.e;
    }

    /* access modifiers changed from: 0000 */
    public void a(long j2) {
        this.f25b += j2;
        if (j2 > 0) {
            notifyAll();
        }
    }

    public void a(a aVar) throws IOException {
        if (d(aVar)) {
            this.f.b(this.e, aVar);
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(b.e eVar, int i2) throws IOException {
        if (d || !Thread.holdsLock(this)) {
            this.i.a(eVar, (long) i2);
            return;
        }
        throw new AssertionError();
    }

    /* access modifiers changed from: 0000 */
    public void a(List<f> list, g gVar) {
        if (d || !Thread.holdsLock(this)) {
            a aVar = null;
            boolean z = true;
            synchronized (this) {
                if (this.h == null) {
                    if (gVar.c()) {
                        aVar = a.PROTOCOL_ERROR;
                    } else {
                        this.h = list;
                        z = b();
                        notifyAll();
                    }
                } else if (gVar.d()) {
                    aVar = a.STREAM_IN_USE;
                } else {
                    ArrayList arrayList = new ArrayList();
                    arrayList.addAll(this.h);
                    arrayList.addAll(list);
                    this.h = arrayList;
                }
            }
            if (aVar != null) {
                b(aVar);
            } else if (!z) {
                this.f.b(this.e);
            }
        } else {
            throw new AssertionError();
        }
    }

    public void b(a aVar) {
        if (d(aVar)) {
            this.f.a(this.e, aVar);
        }
    }

    public synchronized boolean b() {
        boolean z = false;
        synchronized (this) {
            if (this.l == null) {
                if ((!this.i.g && !this.i.f) || ((!this.c.e && !this.c.d) || this.h == null)) {
                    z = true;
                }
            }
        }
        return z;
    }

    /* access modifiers changed from: 0000 */
    public synchronized void c(a aVar) {
        if (this.l == null) {
            this.l = aVar;
            notifyAll();
        }
    }

    public boolean c() {
        return this.f.f9b == ((this.e & 1) == 1);
    }

    public synchronized List<f> d() throws IOException {
        this.j.c();
        while (this.h == null && this.l == null) {
            try {
                l();
            } catch (Throwable th) {
                this.j.b();
                throw th;
            }
        }
        this.j.b();
        if (this.h != null) {
        } else {
            throw new p(this.l);
        }
        return this.h;
    }

    public t e() {
        return this.j;
    }

    public t f() {
        return this.k;
    }

    public s g() {
        return this.i;
    }

    public r h() {
        synchronized (this) {
            if (this.h == null && !c()) {
                throw new IllegalStateException("reply before requesting the sink");
            }
        }
        return this.c;
    }

    /* access modifiers changed from: 0000 */
    public void i() {
        boolean b2;
        if (d || !Thread.holdsLock(this)) {
            synchronized (this) {
                this.i.g = true;
                b2 = b();
                notifyAll();
            }
            if (!b2) {
                this.f.b(this.e);
                return;
            }
            return;
        }
        throw new AssertionError();
    }
}
