package a.a.a;

import a.a.l;

public final class f {

    /* renamed from: a reason: collision with root package name */
    public static final b.f f31a = b.f.a(":status");

    /* renamed from: b reason: collision with root package name */
    public static final b.f f32b = b.f.a(":method");
    public static final b.f c = b.f.a(":path");
    public static final b.f d = b.f.a(":scheme");
    public static final b.f e = b.f.a(":authority");
    public static final b.f f = b.f.a(":host");
    public static final b.f g = b.f.a(":version");
    public final b.f h;
    public final b.f i;
    final int j;

    public f(b.f fVar, b.f fVar2) {
        this.h = fVar;
        this.i = fVar2;
        this.j = fVar.e() + 32 + fVar2.e();
    }

    public f(b.f fVar, String str) {
        this(fVar, b.f.a(str));
    }

    public f(String str, String str2) {
        this(b.f.a(str), b.f.a(str2));
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof f)) {
            return false;
        }
        f fVar = (f) obj;
        return this.h.equals(fVar.h) && this.i.equals(fVar.i);
    }

    public int hashCode() {
        return ((this.h.hashCode() + 527) * 31) + this.i.hashCode();
    }

    public String toString() {
        return l.a("%s: %s", this.h.a(), this.i.a());
    }
}
