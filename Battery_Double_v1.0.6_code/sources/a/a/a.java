package a.a;

import a.x;
import android.util.Log;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.List;
import javax.net.ssl.SSLSocket;

class a extends j {

    /* renamed from: a reason: collision with root package name */
    private final Class<?> f4a;

    /* renamed from: b reason: collision with root package name */
    private final i<Socket> f5b;
    private final i<Socket> c;
    private final i<Socket> d;
    private final i<Socket> e;

    public a(Class<?> cls, i<Socket> iVar, i<Socket> iVar2, i<Socket> iVar3, i<Socket> iVar4) {
        this.f4a = cls;
        this.f5b = iVar;
        this.c = iVar2;
        this.d = iVar3;
        this.e = iVar4;
    }

    public static j b() {
        Class cls;
        i iVar;
        i iVar2;
        i iVar3;
        try {
            cls = Class.forName("com.android.org.conscrypt.SSLParametersImpl");
        } catch (ClassNotFoundException e2) {
            cls = Class.forName("org.apache.harmony.xnet.provider.jsse.SSLParametersImpl");
        }
        try {
            i iVar4 = new i(null, "setUseSessionTickets", Boolean.TYPE);
            i iVar5 = new i(null, "setHostname", String.class);
            try {
                Class.forName("android.net.Network");
                iVar = new i(byte[].class, "getAlpnSelectedProtocol", new Class[0]);
                try {
                    iVar2 = new i(null, "setAlpnProtocols", byte[].class);
                    iVar3 = iVar;
                } catch (ClassNotFoundException e3) {
                    iVar2 = null;
                    iVar3 = iVar;
                    return new a(cls, iVar4, iVar5, iVar3, iVar2);
                }
            } catch (ClassNotFoundException e4) {
                iVar = null;
                iVar2 = null;
                iVar3 = iVar;
                return new a(cls, iVar4, iVar5, iVar3, iVar2);
            }
            return new a(cls, iVar4, iVar5, iVar3, iVar2);
        } catch (ClassNotFoundException e5) {
            return null;
        }
    }

    public String a(SSLSocket sSLSocket) {
        if (this.d == null || !this.d.a(sSLSocket)) {
            return null;
        }
        byte[] bArr = (byte[]) this.d.d(sSLSocket, new Object[0]);
        return bArr != null ? new String(bArr, l.c) : null;
    }

    public void a(int i, String str, Throwable th) {
        int min;
        int i2 = i == 5 ? 5 : 3;
        if (th != null) {
            str = str + 10 + Log.getStackTraceString(th);
        }
        int i3 = 0;
        int length = str.length();
        while (i3 < length) {
            int indexOf = str.indexOf(10, i3);
            if (indexOf == -1) {
                indexOf = length;
            }
            while (true) {
                min = Math.min(indexOf, i3 + 4000);
                Log.println(i2, "OkHttp", str.substring(i3, min));
                if (min >= indexOf) {
                    break;
                }
                i3 = min;
            }
            i3 = min + 1;
        }
    }

    public void a(Socket socket, InetSocketAddress inetSocketAddress, int i) throws IOException {
        try {
            socket.connect(inetSocketAddress, i);
        } catch (AssertionError e2) {
            if (l.a(e2)) {
                throw new IOException(e2);
            }
            throw e2;
        } catch (SecurityException e3) {
            IOException iOException = new IOException("Exception in connect");
            iOException.initCause(e3);
            throw iOException;
        }
    }

    public void a(SSLSocket sSLSocket, String str, List<x> list) {
        if (str != null) {
            this.f5b.b(sSLSocket, Boolean.valueOf(true));
            this.c.b(sSLSocket, str);
        }
        if (this.e != null && this.e.a(sSLSocket)) {
            this.e.d(sSLSocket, b(list));
        }
    }

    public boolean a() {
        try {
            Class cls = Class.forName("android.security.NetworkSecurityPolicy");
            return ((Boolean) cls.getMethod("isCleartextTrafficPermitted", new Class[0]).invoke(cls.getMethod("getInstance", new Class[0]).invoke(null, new Object[0]), new Object[0])).booleanValue();
        } catch (ClassNotFoundException e2) {
            return super.a();
        } catch (IllegalAccessException | IllegalArgumentException | NoSuchMethodException | InvocationTargetException e3) {
            throw new AssertionError();
        }
    }
}
