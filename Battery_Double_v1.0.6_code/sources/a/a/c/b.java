package a.a.c;

import a.a.a.d;
import a.a.a.d.a;
import a.a.b.j;
import a.a.b.o;
import a.a.b.r;
import a.a.d.c;
import a.a.l;
import a.a.m;
import a.ab;
import a.ad;
import a.g;
import a.i;
import a.k;
import a.q;
import a.s;
import a.x;
import a.z;
import b.e;
import com.hmatalonga.greenhub.Config;
import java.io.IOException;
import java.lang.ref.Reference;
import java.net.ConnectException;
import java.net.ProtocolException;
import java.net.Proxy;
import java.net.Proxy.Type;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.UnknownServiceException;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSocket;

public final class b extends a.a.a.d.b implements i {

    /* renamed from: b reason: collision with root package name */
    public Socket f115b;
    public volatile d c;
    public int d;
    public e e;
    public b.d f;
    public int g;
    public final List<Reference<r>> h = new ArrayList();
    public boolean i;
    public long j = Long.MAX_VALUE;
    private final ad k;
    private Socket l;
    private q m;
    private x n;

    public b(ad adVar) {
        this.k = adVar;
    }

    private z a(int i2, int i3, z zVar, s sVar) throws IOException {
        ab a2;
        String str = "CONNECT " + l.a(sVar, true) + " HTTP/1.1";
        do {
            a.a.b.d dVar = new a.a.b.d(null, this.e, this.f);
            this.e.timeout().a((long) i2, TimeUnit.MILLISECONDS);
            this.f.timeout().a((long) i3, TimeUnit.MILLISECONDS);
            dVar.a(zVar.c(), str);
            dVar.c();
            a2 = dVar.d().a(zVar).a();
            long a3 = j.a(a2);
            if (a3 == -1) {
                a3 = 0;
            }
            b.s b2 = dVar.b(a3);
            l.b(b2, Integer.MAX_VALUE, TimeUnit.MILLISECONDS);
            b2.close();
            switch (a2.b()) {
                case 200:
                    if (this.e.b().f() && this.f.b().f()) {
                        return null;
                    }
                    throw new IOException("TLS tunnel buffered too many bytes!");
                case 407:
                    zVar = this.k.a().d().a(this.k, a2);
                    if (zVar != null) {
                        break;
                    } else {
                        throw new IOException("Failed to authenticate with proxy");
                    }
                default:
                    throw new IOException("Unexpected response code for CONNECT: " + a2.b());
            }
        } while (!"close".equalsIgnoreCase(a2.a("Connection")));
        return zVar;
    }

    private void a(int i2, int i3, int i4, a.a.b bVar) throws IOException {
        z f2 = f();
        s a2 = f2.a();
        int i5 = 0;
        while (true) {
            i5++;
            if (i5 > 21) {
                throw new ProtocolException("Too many tunnel connections attempted: " + 21);
            }
            c(i2, i3, i4, bVar);
            f2 = a(i3, i4, f2, a2);
            if (f2 == null) {
                a(i3, i4, bVar);
                return;
            }
            l.a(this.l);
            this.l = null;
            this.f = null;
            this.e = null;
        }
    }

    private void a(int i2, int i3, a.a.b bVar) throws IOException {
        if (this.k.a().i() != null) {
            b(i2, i3, bVar);
        } else {
            this.n = x.HTTP_1_1;
            this.f115b = this.l;
        }
        if (this.n == x.SPDY_3 || this.n == x.HTTP_2) {
            this.f115b.setSoTimeout(0);
            d a2 = new a(true).a(this.f115b, this.k.a().a().f(), this.e, this.f).a(this.n).a((a.a.a.d.b) this).a();
            a2.d();
            this.g = a2.b();
            this.c = a2;
            return;
        }
        this.g = 1;
    }

    private void b(int i2, int i3, int i4, a.a.b bVar) throws IOException {
        c(i2, i3, i4, bVar);
        a(i3, i4, bVar);
    }

    /* JADX WARNING: type inference failed for: r1v0 */
    /* JADX WARNING: type inference failed for: r1v1, types: [java.net.Socket, javax.net.ssl.SSLSocket] */
    /* JADX WARNING: type inference failed for: r1v2 */
    /* JADX WARNING: type inference failed for: r0v7, types: [java.net.Socket, javax.net.ssl.SSLSocket] */
    /* JADX WARNING: type inference failed for: r1v4 */
    /* JADX WARNING: type inference failed for: r1v6 */
    /* JADX WARNING: type inference failed for: r1v7, types: [java.lang.String] */
    /* JADX WARNING: type inference failed for: r1v13, types: [java.lang.String] */
    /* JADX WARNING: type inference failed for: r1v20 */
    /* JADX WARNING: type inference failed for: r1v21 */
    /* JADX WARNING: type inference failed for: r1v22 */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r1v2
  assigns: []
  uses: []
  mth insns count: 104
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
    	at jadx.core.ProcessClass.process(ProcessClass.java:35)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Unknown variable types count: 5 */
    private void b(int i2, int i3, a.a.b bVar) throws IOException {
        ? r1;
        ? r12;
        ? r13 = 0;
        a.a a2 = this.k.a();
        try {
            ? r0 = (SSLSocket) a2.i().createSocket(this.l, a2.a().f(), a2.a().g(), true);
            try {
                k a3 = bVar.a((SSLSocket) r0);
                if (a3.d()) {
                    a.a.j.c().a((SSLSocket) r0, a2.a().f(), a2.e());
                }
                r0.startHandshake();
                q a4 = q.a(r0.getSession());
                if (!a2.j().verify(a2.a().f(), r0.getSession())) {
                    X509Certificate x509Certificate = (X509Certificate) a4.b().get(0);
                    throw new SSLPeerUnverifiedException("Hostname " + a2.a().f() + " not verified:\n    certificate: " + g.a((Certificate) x509Certificate) + "\n    DN: " + x509Certificate.getSubjectDN().getName() + "\n    subjectAltNames: " + c.a(x509Certificate));
                }
                a2.k().a(a2.a().f(), a4.b());
                if (a3.d()) {
                    r13 = a.a.j.c().a((SSLSocket) r0);
                }
                this.f115b = r0;
                this.e = b.l.a(b.l.b(this.f115b));
                this.f = b.l.a(b.l.a(this.f115b));
                this.m = a4;
                this.n = r13 != 0 ? x.a(r13) : x.HTTP_1_1;
                if (r0 != 0) {
                    a.a.j.c().b((SSLSocket) r0);
                }
            } catch (AssertionError e2) {
                AssertionError assertionError = e2;
                r12 = r0;
                e = assertionError;
            } catch (Throwable th) {
                Throwable th2 = th;
                r1 = r0;
                th = th2;
                if (r1 != 0) {
                    a.a.j.c().b((SSLSocket) r1);
                }
                l.a((Socket) r1);
                throw th;
            }
        } catch (AssertionError e3) {
            e = e3;
            r12 = r13;
            try {
                if (l.a(e)) {
                    throw new IOException(e);
                }
                throw e;
            } catch (Throwable th3) {
                th = th3;
                r1 = r12;
            }
        }
    }

    private void c(int i2, int i3, int i4, a.a.b bVar) throws IOException {
        Proxy b2 = this.k.b();
        this.l = (b2.type() == Type.DIRECT || b2.type() == Type.HTTP) ? this.k.a().c().createSocket() : new Socket(b2);
        this.l.setSoTimeout(i3);
        try {
            a.a.j.c().a(this.l, this.k.c(), i2);
            this.e = b.l.a(b.l.b(this.l));
            this.f = b.l.a(b.l.a(this.l));
        } catch (ConnectException e2) {
            throw new ConnectException("Failed to connect to " + this.k.c());
        }
    }

    private z f() throws IOException {
        return new z.a().a(this.k.a().a()).a("Host", l.a(this.k.a().a(), true)).a("Proxy-Connection", "Keep-Alive").a("User-Agent", m.a()).a();
    }

    public ad a() {
        return this.k;
    }

    public void a(int i2, int i3, int i4, List<k> list, boolean z) throws o {
        if (this.n != null) {
            throw new IllegalStateException("already connected");
        }
        a.a.b bVar = new a.a.b(list);
        if (this.k.a().i() != null || list.contains(k.c)) {
            o oVar = null;
            while (this.n == null) {
                try {
                    if (this.k.d()) {
                        a(i2, i3, i4, bVar);
                    } else {
                        b(i2, i3, i4, bVar);
                    }
                } catch (IOException e2) {
                    l.a(this.f115b);
                    l.a(this.l);
                    this.f115b = null;
                    this.l = null;
                    this.e = null;
                    this.f = null;
                    this.m = null;
                    this.n = null;
                    if (oVar == null) {
                        oVar = new o(e2);
                    } else {
                        oVar.a(e2);
                    }
                    if (!z || !bVar.a(e2)) {
                        throw oVar;
                    }
                }
            }
            return;
        }
        throw new o(new UnknownServiceException("CLEARTEXT communication not supported: " + list));
    }

    public void a(d dVar) {
        this.g = dVar.b();
    }

    public void a(a.a.a.e eVar) throws IOException {
        eVar.a(a.a.a.a.REFUSED_STREAM);
    }

    public boolean a(boolean z) {
        int soTimeout;
        if (this.f115b.isClosed() || this.f115b.isInputShutdown() || this.f115b.isOutputShutdown()) {
            return false;
        }
        if (this.c != null || !z) {
            return true;
        }
        try {
            soTimeout = this.f115b.getSoTimeout();
            this.f115b.setSoTimeout(1);
            if (this.e.f()) {
                this.f115b.setSoTimeout(soTimeout);
                return false;
            }
            this.f115b.setSoTimeout(soTimeout);
            return true;
        } catch (SocketTimeoutException e2) {
            return true;
        } catch (IOException e3) {
            return false;
        } catch (Throwable th) {
            this.f115b.setSoTimeout(soTimeout);
            throw th;
        }
    }

    public void b() {
        l.a(this.l);
    }

    public Socket c() {
        return this.f115b;
    }

    public q d() {
        return this.m;
    }

    public boolean e() {
        return this.c != null;
    }

    public String toString() {
        return "Connection{" + this.k.a().a().f() + ":" + this.k.a().a().g() + ", proxy=" + this.k.b() + " hostAddress=" + this.k.c() + " cipherSuite=" + (this.m != null ? this.m.a() : Config.SERVER_URL_DEFAULT) + " protocol=" + this.n + '}';
    }
}
