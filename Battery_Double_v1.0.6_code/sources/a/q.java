package a;

import a.a.l;
import java.security.cert.Certificate;
import java.util.Collections;
import java.util.List;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSession;

public final class q {

    /* renamed from: a reason: collision with root package name */
    private final ae f189a;

    /* renamed from: b reason: collision with root package name */
    private final h f190b;
    private final List<Certificate> c;
    private final List<Certificate> d;

    private q(ae aeVar, h hVar, List<Certificate> list, List<Certificate> list2) {
        this.f189a = aeVar;
        this.f190b = hVar;
        this.c = list;
        this.d = list2;
    }

    public static q a(SSLSession sSLSession) {
        Certificate[] certificateArr;
        String cipherSuite = sSLSession.getCipherSuite();
        if (cipherSuite == null) {
            throw new IllegalStateException("cipherSuite == null");
        }
        h a2 = h.a(cipherSuite);
        String protocol = sSLSession.getProtocol();
        if (protocol == null) {
            throw new IllegalStateException("tlsVersion == null");
        }
        ae a3 = ae.a(protocol);
        try {
            certificateArr = sSLSession.getPeerCertificates();
        } catch (SSLPeerUnverifiedException e) {
            certificateArr = null;
        }
        List emptyList = certificateArr != null ? l.a((T[]) certificateArr) : Collections.emptyList();
        Certificate[] localCertificates = sSLSession.getLocalCertificates();
        return new q(a3, a2, emptyList, localCertificates != null ? l.a((T[]) localCertificates) : Collections.emptyList());
    }

    public h a() {
        return this.f190b;
    }

    public List<Certificate> b() {
        return this.c;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof q)) {
            return false;
        }
        q qVar = (q) obj;
        return l.a((Object) this.f190b, (Object) qVar.f190b) && this.f190b.equals(qVar.f190b) && this.c.equals(qVar.c) && this.d.equals(qVar.d);
    }

    public int hashCode() {
        return (((((((this.f189a != null ? this.f189a.hashCode() : 0) + 527) * 31) + this.f190b.hashCode()) * 31) + this.c.hashCode()) * 31) + this.d.hashCode();
    }
}
