package a;

import a.a.b.f;
import a.a.l;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public final class r {

    /* renamed from: a reason: collision with root package name */
    private final String[] f191a;

    public static final class a {
        /* access modifiers changed from: private */

        /* renamed from: a reason: collision with root package name */
        public final List<String> f192a = new ArrayList(20);

        private void d(String str, String str2) {
            if (str == null) {
                throw new NullPointerException("name == null");
            } else if (str.isEmpty()) {
                throw new IllegalArgumentException("name is empty");
            } else {
                int length = str.length();
                for (int i = 0; i < length; i++) {
                    char charAt = str.charAt(i);
                    if (charAt <= 31 || charAt >= 127) {
                        throw new IllegalArgumentException(l.a("Unexpected char %#04x at %d in header name: %s", Integer.valueOf(charAt), Integer.valueOf(i), str));
                    }
                }
                if (str2 == null) {
                    throw new NullPointerException("value == null");
                }
                int length2 = str2.length();
                for (int i2 = 0; i2 < length2; i2++) {
                    char charAt2 = str2.charAt(i2);
                    if (charAt2 <= 31 || charAt2 >= 127) {
                        throw new IllegalArgumentException(l.a("Unexpected char %#04x at %d in %s value: %s", Integer.valueOf(charAt2), Integer.valueOf(i2), str, str2));
                    }
                }
            }
        }

        /* access modifiers changed from: 0000 */
        public a a(String str) {
            int indexOf = str.indexOf(":", 1);
            return indexOf != -1 ? b(str.substring(0, indexOf), str.substring(indexOf + 1)) : str.startsWith(":") ? b("", str.substring(1)) : b("", str);
        }

        public a a(String str, String str2) {
            d(str, str2);
            return b(str, str2);
        }

        public r a() {
            return new r(this);
        }

        public a b(String str) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= this.f192a.size()) {
                    return this;
                }
                if (str.equalsIgnoreCase((String) this.f192a.get(i2))) {
                    this.f192a.remove(i2);
                    this.f192a.remove(i2);
                    i2 -= 2;
                }
                i = i2 + 2;
            }
        }

        /* access modifiers changed from: 0000 */
        public a b(String str, String str2) {
            this.f192a.add(str);
            this.f192a.add(str2.trim());
            return this;
        }

        public a c(String str, String str2) {
            d(str, str2);
            b(str);
            b(str, str2);
            return this;
        }
    }

    private r(a aVar) {
        this.f191a = (String[]) aVar.f192a.toArray(new String[aVar.f192a.size()]);
    }

    private r(String[] strArr) {
        this.f191a = strArr;
    }

    public static r a(String... strArr) {
        if (strArr == null) {
            throw new NullPointerException("namesAndValues == null");
        } else if (strArr.length % 2 != 0) {
            throw new IllegalArgumentException("Expected alternating header names and values");
        } else {
            String[] strArr2 = (String[]) strArr.clone();
            for (int i = 0; i < strArr2.length; i++) {
                if (strArr2[i] == null) {
                    throw new IllegalArgumentException("Headers cannot be null");
                }
                strArr2[i] = strArr2[i].trim();
            }
            int i2 = 0;
            while (i2 < strArr2.length) {
                String str = strArr2[i2];
                String str2 = strArr2[i2 + 1];
                if (str.length() != 0 && str.indexOf(0) == -1 && str2.indexOf(0) == -1) {
                    i2 += 2;
                } else {
                    throw new IllegalArgumentException("Unexpected header: " + str + ": " + str2);
                }
            }
            return new r(strArr2);
        }
    }

    private static String a(String[] strArr, String str) {
        for (int length = strArr.length - 2; length >= 0; length -= 2) {
            if (str.equalsIgnoreCase(strArr[length])) {
                return strArr[length + 1];
            }
        }
        return null;
    }

    public int a() {
        return this.f191a.length / 2;
    }

    public String a(int i) {
        return this.f191a[i * 2];
    }

    public String a(String str) {
        return a(this.f191a, str);
    }

    public a b() {
        a aVar = new a();
        Collections.addAll(aVar.f192a, this.f191a);
        return aVar;
    }

    public String b(int i) {
        return this.f191a[(i * 2) + 1];
    }

    public Date b(String str) {
        String a2 = a(str);
        if (a2 != null) {
            return f.a(a2);
        }
        return null;
    }

    public List<String> c(String str) {
        int a2 = a();
        List list = null;
        for (int i = 0; i < a2; i++) {
            if (str.equalsIgnoreCase(a(i))) {
                if (list == null) {
                    list = new ArrayList(2);
                }
                list.add(b(i));
            }
        }
        return list != null ? Collections.unmodifiableList(list) : Collections.emptyList();
    }

    public boolean equals(Object obj) {
        return (obj instanceof r) && Arrays.equals(((r) obj).f191a, this.f191a);
    }

    public int hashCode() {
        return Arrays.hashCode(this.f191a);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        int a2 = a();
        for (int i = 0; i < a2; i++) {
            sb.append(a(i)).append(": ").append(b(i)).append("\n");
        }
        return sb.toString();
    }
}
