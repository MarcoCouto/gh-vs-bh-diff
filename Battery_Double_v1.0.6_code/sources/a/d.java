package a;

import a.a.b.c;
import java.util.concurrent.TimeUnit;

public final class d {

    /* renamed from: a reason: collision with root package name */
    public static final d f161a = new a().a().c();

    /* renamed from: b reason: collision with root package name */
    public static final d f162b = new a().b().a(Integer.MAX_VALUE, TimeUnit.SECONDS).c();
    String c;
    private final boolean d;
    private final boolean e;
    private final int f;
    private final int g;
    private final boolean h;
    private final boolean i;
    private final boolean j;
    private final int k;
    private final int l;
    private final boolean m;
    private final boolean n;

    public static final class a {

        /* renamed from: a reason: collision with root package name */
        boolean f163a;

        /* renamed from: b reason: collision with root package name */
        boolean f164b;
        int c = -1;
        int d = -1;
        int e = -1;
        boolean f;
        boolean g;

        public a a() {
            this.f163a = true;
            return this;
        }

        public a a(int i, TimeUnit timeUnit) {
            if (i < 0) {
                throw new IllegalArgumentException("maxStale < 0: " + i);
            }
            long seconds = timeUnit.toSeconds((long) i);
            this.d = seconds > 2147483647L ? Integer.MAX_VALUE : (int) seconds;
            return this;
        }

        public a b() {
            this.f = true;
            return this;
        }

        public d c() {
            return new d(this);
        }
    }

    private d(a aVar) {
        this.d = aVar.f163a;
        this.e = aVar.f164b;
        this.f = aVar.c;
        this.g = -1;
        this.h = false;
        this.i = false;
        this.j = false;
        this.k = aVar.d;
        this.l = aVar.e;
        this.m = aVar.f;
        this.n = aVar.g;
    }

    private d(boolean z, boolean z2, int i2, int i3, boolean z3, boolean z4, boolean z5, int i4, int i5, boolean z6, boolean z7, String str) {
        this.d = z;
        this.e = z2;
        this.f = i2;
        this.g = i3;
        this.h = z3;
        this.i = z4;
        this.j = z5;
        this.k = i4;
        this.l = i5;
        this.m = z6;
        this.n = z7;
        this.c = str;
    }

    public static d a(r rVar) {
        boolean z;
        String str;
        boolean z2 = false;
        boolean z3 = false;
        int i2 = -1;
        int i3 = -1;
        boolean z4 = false;
        boolean z5 = false;
        boolean z6 = false;
        int i4 = -1;
        int i5 = -1;
        boolean z7 = false;
        boolean z8 = false;
        boolean z9 = true;
        int a2 = rVar.a();
        int i6 = 0;
        String str2 = null;
        while (true) {
            z = z2;
            if (i6 >= a2) {
                break;
            }
            String a3 = rVar.a(i6);
            String b2 = rVar.b(i6);
            if (a3.equalsIgnoreCase("Cache-Control")) {
                if (str2 != null) {
                    z9 = false;
                } else {
                    str2 = b2;
                }
            } else if (a3.equalsIgnoreCase("Pragma")) {
                z9 = false;
            } else {
                z2 = z;
                i6++;
            }
            z2 = z;
            int i7 = 0;
            while (i7 < b2.length()) {
                int a4 = c.a(b2, i7, "=,;");
                String trim = b2.substring(i7, a4).trim();
                if (a4 == b2.length() || b2.charAt(a4) == ',' || b2.charAt(a4) == ';') {
                    i7 = a4 + 1;
                    str = null;
                } else {
                    int a5 = c.a(b2, a4 + 1);
                    if (a5 >= b2.length() || b2.charAt(a5) != '\"') {
                        int a6 = c.a(b2, a5, ",;");
                        String trim2 = b2.substring(a5, a6).trim();
                        i7 = a6;
                        str = trim2;
                    } else {
                        int i8 = a5 + 1;
                        int a7 = c.a(b2, i8, "\"");
                        String substring = b2.substring(i8, a7);
                        i7 = a7 + 1;
                        str = substring;
                    }
                }
                if ("no-cache".equalsIgnoreCase(trim)) {
                    z2 = true;
                } else if ("no-store".equalsIgnoreCase(trim)) {
                    z3 = true;
                } else if ("max-age".equalsIgnoreCase(trim)) {
                    i2 = c.b(str, -1);
                } else if ("s-maxage".equalsIgnoreCase(trim)) {
                    i3 = c.b(str, -1);
                } else if ("private".equalsIgnoreCase(trim)) {
                    z4 = true;
                } else if ("public".equalsIgnoreCase(trim)) {
                    z5 = true;
                } else if ("must-revalidate".equalsIgnoreCase(trim)) {
                    z6 = true;
                } else if ("max-stale".equalsIgnoreCase(trim)) {
                    i4 = c.b(str, Integer.MAX_VALUE);
                } else if ("min-fresh".equalsIgnoreCase(trim)) {
                    i5 = c.b(str, -1);
                } else if ("only-if-cached".equalsIgnoreCase(trim)) {
                    z7 = true;
                } else if ("no-transform".equalsIgnoreCase(trim)) {
                    z8 = true;
                }
            }
            i6++;
        }
        return new d(z, z3, i2, i3, z4, z5, z6, i4, i5, z7, z8, !z9 ? null : str2);
    }

    private String j() {
        StringBuilder sb = new StringBuilder();
        if (this.d) {
            sb.append("no-cache, ");
        }
        if (this.e) {
            sb.append("no-store, ");
        }
        if (this.f != -1) {
            sb.append("max-age=").append(this.f).append(", ");
        }
        if (this.g != -1) {
            sb.append("s-maxage=").append(this.g).append(", ");
        }
        if (this.h) {
            sb.append("private, ");
        }
        if (this.i) {
            sb.append("public, ");
        }
        if (this.j) {
            sb.append("must-revalidate, ");
        }
        if (this.k != -1) {
            sb.append("max-stale=").append(this.k).append(", ");
        }
        if (this.l != -1) {
            sb.append("min-fresh=").append(this.l).append(", ");
        }
        if (this.m) {
            sb.append("only-if-cached, ");
        }
        if (this.n) {
            sb.append("no-transform, ");
        }
        if (sb.length() == 0) {
            return "";
        }
        sb.delete(sb.length() - 2, sb.length());
        return sb.toString();
    }

    public boolean a() {
        return this.d;
    }

    public boolean b() {
        return this.e;
    }

    public int c() {
        return this.f;
    }

    public boolean d() {
        return this.h;
    }

    public boolean e() {
        return this.i;
    }

    public boolean f() {
        return this.j;
    }

    public int g() {
        return this.k;
    }

    public int h() {
        return this.l;
    }

    public boolean i() {
        return this.m;
    }

    public String toString() {
        String str = this.c;
        if (str != null) {
            return str;
        }
        String j2 = j();
        this.c = j2;
        return j2;
    }
}
