package a;

import b.d;
import b.f;
import b.l;
import b.s;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;

public abstract class aa {
    public static aa create(final u uVar, final f fVar) {
        return new aa() {
            public long contentLength() throws IOException {
                return (long) fVar.e();
            }

            public u contentType() {
                return uVar;
            }

            public void writeTo(d dVar) throws IOException {
                dVar.b(fVar);
            }
        };
    }

    public static aa create(final u uVar, final File file) {
        if (file != null) {
            return new aa() {
                public long contentLength() {
                    return file.length();
                }

                public u contentType() {
                    return uVar;
                }

                public void writeTo(d dVar) throws IOException {
                    s sVar = null;
                    try {
                        sVar = l.a(file);
                        dVar.a(sVar);
                    } finally {
                        a.a.l.a((Closeable) sVar);
                    }
                }
            };
        }
        throw new NullPointerException("content == null");
    }

    public static aa create(u uVar, String str) {
        Charset charset = a.a.l.c;
        if (uVar != null) {
            charset = uVar.b();
            if (charset == null) {
                charset = a.a.l.c;
                uVar = u.a(uVar + "; charset=utf-8");
            }
        }
        return create(uVar, str.getBytes(charset));
    }

    public static aa create(u uVar, byte[] bArr) {
        return create(uVar, bArr, 0, bArr.length);
    }

    public static aa create(final u uVar, final byte[] bArr, final int i, final int i2) {
        if (bArr == null) {
            throw new NullPointerException("content == null");
        }
        a.a.l.a((long) bArr.length, (long) i, (long) i2);
        return new aa() {
            public long contentLength() {
                return (long) i2;
            }

            public u contentType() {
                return uVar;
            }

            public void writeTo(d dVar) throws IOException {
                dVar.c(bArr, i, i2);
            }
        };
    }

    public long contentLength() throws IOException {
        return -1;
    }

    public abstract u contentType();

    public abstract void writeTo(d dVar) throws IOException;
}
