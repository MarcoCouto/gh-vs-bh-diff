package a;

import a.a.l;
import b.c;
import b.d;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public final class p extends aa {

    /* renamed from: a reason: collision with root package name */
    private static final u f185a = u.a("application/x-www-form-urlencoded");

    /* renamed from: b reason: collision with root package name */
    private final List<String> f186b;
    private final List<String> c;

    public static final class a {

        /* renamed from: a reason: collision with root package name */
        private final List<String> f187a = new ArrayList();

        /* renamed from: b reason: collision with root package name */
        private final List<String> f188b = new ArrayList();

        public a a(String str, String str2) {
            this.f187a.add(s.a(str, " \"':;<=>@[]^`{}|/\\?#&!$(),~", false, false, true, true));
            this.f188b.add(s.a(str2, " \"':;<=>@[]^`{}|/\\?#&!$(),~", false, false, true, true));
            return this;
        }

        public p a() {
            return new p(this.f187a, this.f188b);
        }

        public a b(String str, String str2) {
            this.f187a.add(s.a(str, " \"':;<=>@[]^`{}|/\\?#&!$(),~", true, false, true, true));
            this.f188b.add(s.a(str2, " \"':;<=>@[]^`{}|/\\?#&!$(),~", true, false, true, true));
            return this;
        }
    }

    private p(List<String> list, List<String> list2) {
        this.f186b = l.a(list);
        this.c = l.a(list2);
    }

    private long a(d dVar, boolean z) {
        long j = 0;
        c b2 = z ? new c() : dVar.b();
        int size = this.f186b.size();
        for (int i = 0; i < size; i++) {
            if (i > 0) {
                b2.i(38);
            }
            b2.b((String) this.f186b.get(i));
            b2.i(61);
            b2.b((String) this.c.get(i));
        }
        if (z) {
            j = b2.a();
            b2.s();
        }
        return j;
    }

    public long contentLength() {
        return a(null, true);
    }

    public u contentType() {
        return f185a;
    }

    public void writeTo(d dVar) throws IOException {
        a(dVar, false);
    }
}
