package a;

import a.a.b.h;

public final class z {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public final s f219a;
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public final String f220b;
    /* access modifiers changed from: private */
    public final r c;
    /* access modifiers changed from: private */
    public final aa d;
    /* access modifiers changed from: private */
    public final Object e;
    private volatile d f;

    public static class a {
        /* access modifiers changed from: private */

        /* renamed from: a reason: collision with root package name */
        public s f221a;
        /* access modifiers changed from: private */

        /* renamed from: b reason: collision with root package name */
        public String f222b;
        /* access modifiers changed from: private */
        public a.r.a c;
        /* access modifiers changed from: private */
        public aa d;
        /* access modifiers changed from: private */
        public Object e;

        public a() {
            this.f222b = "GET";
            this.c = new a.r.a();
        }

        private a(z zVar) {
            this.f221a = zVar.f219a;
            this.f222b = zVar.f220b;
            this.d = zVar.d;
            this.e = zVar.e;
            this.c = zVar.c.b();
        }

        public a a(r rVar) {
            this.c = rVar.b();
            return this;
        }

        public a a(s sVar) {
            if (sVar == null) {
                throw new NullPointerException("url == null");
            }
            this.f221a = sVar;
            return this;
        }

        public a a(String str) {
            if (str == null) {
                throw new NullPointerException("url == null");
            }
            if (str.regionMatches(true, 0, "ws:", 0, 3)) {
                str = "http:" + str.substring(3);
            } else {
                if (str.regionMatches(true, 0, "wss:", 0, 4)) {
                    str = "https:" + str.substring(4);
                }
            }
            s e2 = s.e(str);
            if (e2 != null) {
                return a(e2);
            }
            throw new IllegalArgumentException("unexpected url: " + str);
        }

        public a a(String str, aa aaVar) {
            if (str == null) {
                throw new NullPointerException("method == null");
            } else if (str.length() == 0) {
                throw new IllegalArgumentException("method.length() == 0");
            } else if (aaVar != null && !h.c(str)) {
                throw new IllegalArgumentException("method " + str + " must not have a request body.");
            } else if (aaVar != null || !h.b(str)) {
                this.f222b = str;
                this.d = aaVar;
                return this;
            } else {
                throw new IllegalArgumentException("method " + str + " must have a request body.");
            }
        }

        public a a(String str, String str2) {
            this.c.c(str, str2);
            return this;
        }

        public z a() {
            if (this.f221a != null) {
                return new z(this);
            }
            throw new IllegalStateException("url == null");
        }

        public a b(String str) {
            this.c.b(str);
            return this;
        }

        public a b(String str, String str2) {
            this.c.a(str, str2);
            return this;
        }
    }

    private z(a aVar) {
        this.f219a = aVar.f221a;
        this.f220b = aVar.f222b;
        this.c = aVar.c.a();
        this.d = aVar.d;
        this.e = aVar.e != null ? aVar.e : this;
    }

    public s a() {
        return this.f219a;
    }

    public String a(String str) {
        return this.c.a(str);
    }

    public String b() {
        return this.f220b;
    }

    public r c() {
        return this.c;
    }

    public aa d() {
        return this.d;
    }

    public a e() {
        return new a();
    }

    public d f() {
        d dVar = this.f;
        if (dVar != null) {
            return dVar;
        }
        d a2 = d.a(this.c);
        this.f = a2;
        return a2;
    }

    public boolean g() {
        return this.f219a.c();
    }

    public String toString() {
        return "Request{method=" + this.f220b + ", url=" + this.f219a + ", tag=" + (this.e != this ? this.e : null) + '}';
    }
}
