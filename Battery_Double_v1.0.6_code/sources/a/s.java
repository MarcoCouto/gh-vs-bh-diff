package a;

import a.a.l;
import b.c;
import java.net.InetAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public final class s {

    /* renamed from: a reason: collision with root package name */
    private static final char[] f193a = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public final String f194b;
    private final String c;
    private final String d;
    /* access modifiers changed from: private */
    public final String e;
    /* access modifiers changed from: private */
    public final int f;
    private final List<String> g;
    private final List<String> h;
    private final String i;
    private final String j;

    /* renamed from: a.s$1 reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {

        /* renamed from: a reason: collision with root package name */
        static final /* synthetic */ int[] f195a = new int[C0002a.values().length];

        static {
            try {
                f195a[C0002a.SUCCESS.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                f195a[C0002a.INVALID_HOST.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                f195a[C0002a.UNSUPPORTED_SCHEME.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                f195a[C0002a.MISSING_SCHEME.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                f195a[C0002a.INVALID_PORT.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
        }
    }

    public static final class a {

        /* renamed from: a reason: collision with root package name */
        String f196a;

        /* renamed from: b reason: collision with root package name */
        String f197b = "";
        String c = "";
        String d;
        int e = -1;
        final List<String> f = new ArrayList();
        List<String> g;
        String h;

        /* renamed from: a.s$a$a reason: collision with other inner class name */
        enum C0002a {
            SUCCESS,
            MISSING_SCHEME,
            UNSUPPORTED_SCHEME,
            INVALID_PORT,
            INVALID_HOST
        }

        public a() {
            this.f.add("");
        }

        private static String a(byte[] bArr) {
            int i = 0;
            int i2 = 0;
            int i3 = -1;
            int i4 = 0;
            while (i4 < bArr.length) {
                int i5 = i4;
                while (i5 < 16 && bArr[i5] == 0 && bArr[i5 + 1] == 0) {
                    i5 += 2;
                }
                int i6 = i5 - i4;
                if (i6 > i2) {
                    i2 = i6;
                    i3 = i4;
                }
                i4 = i5 + 2;
            }
            c cVar = new c();
            while (i < bArr.length) {
                if (i == i3) {
                    cVar.i(58);
                    i += i2;
                    if (i == 16) {
                        cVar.i(58);
                    }
                } else {
                    if (i > 0) {
                        cVar.i(58);
                    }
                    cVar.j((long) (((bArr[i] & 255) << 8) | (bArr[i + 1] & 255)));
                    i += 2;
                }
            }
            return cVar.p();
        }

        private void a(String str, int i, int i2) {
            if (i != i2) {
                char charAt = str.charAt(i);
                if (charAt == '/' || charAt == '\\') {
                    this.f.clear();
                    this.f.add("");
                    i++;
                } else {
                    this.f.set(this.f.size() - 1, "");
                }
                int i3 = i;
                while (i3 < i2) {
                    int a2 = l.a(str, i3, i2, "/\\");
                    boolean z = a2 < i2;
                    a(str, i3, a2, z, true);
                    if (z) {
                        a2++;
                    }
                    i3 = a2;
                }
            }
        }

        private void a(String str, int i, int i2, boolean z, boolean z2) {
            String a2 = s.a(str, i, i2, " \"<>^`{}|/\\?#", z2, false, false, true);
            if (!d(a2)) {
                if (e(a2)) {
                    d();
                    return;
                }
                if (((String) this.f.get(this.f.size() - 1)).isEmpty()) {
                    this.f.set(this.f.size() - 1, a2);
                } else {
                    this.f.add(a2);
                }
                if (z) {
                    this.f.add("");
                }
            }
        }

        private static boolean a(String str, int i, int i2, byte[] bArr, int i3) {
            int i4 = i;
            int i5 = i3;
            while (i4 < i2) {
                if (i5 == bArr.length) {
                    return false;
                }
                if (i5 != i3) {
                    if (str.charAt(i4) != '.') {
                        return false;
                    }
                    i4++;
                }
                int i6 = 0;
                int i7 = i4;
                while (i7 < i2) {
                    char charAt = str.charAt(i7);
                    if (charAt < '0' || charAt > '9') {
                        break;
                    } else if (i6 == 0 && i4 != i7) {
                        return false;
                    } else {
                        i6 = ((i6 * 10) + charAt) - 48;
                        if (i6 > 255) {
                            return false;
                        }
                        i7++;
                    }
                }
                if (i7 - i4 == 0) {
                    return false;
                }
                int i8 = i5 + 1;
                bArr[i5] = (byte) i6;
                i5 = i8;
                i4 = i7;
            }
            return i5 == i3 + 4;
        }

        private static int b(String str, int i, int i2) {
            if (i2 - i < 2) {
                return -1;
            }
            char charAt = str.charAt(i);
            if ((charAt < 'a' || charAt > 'z') && (charAt < 'A' || charAt > 'Z')) {
                return -1;
            }
            int i3 = i + 1;
            while (i3 < i2) {
                char charAt2 = str.charAt(i3);
                if ((charAt2 >= 'a' && charAt2 <= 'z') || ((charAt2 >= 'A' && charAt2 <= 'Z') || ((charAt2 >= '0' && charAt2 <= '9') || charAt2 == '+' || charAt2 == '-' || charAt2 == '.'))) {
                    i3++;
                } else if (charAt2 == ':') {
                    return i3;
                } else {
                    return -1;
                }
            }
            return -1;
        }

        private static int c(String str, int i, int i2) {
            int i3 = 0;
            while (i < i2) {
                char charAt = str.charAt(i);
                if (charAt != '\\' && charAt != '/') {
                    break;
                }
                i3++;
                i++;
            }
            return i3;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:6:0x000f, code lost:
            if (r0 >= r5) goto L_0x000a;
         */
        private static int d(String str, int i, int i2) {
            int i3 = i;
            while (i3 < i2) {
                switch (str.charAt(i3)) {
                    case ':':
                        return i3;
                    case '[':
                        do {
                            i3++;
                            break;
                        } while (str.charAt(i3) != ']');
                        break;
                }
                i3++;
            }
            return i2;
        }

        private void d() {
            if (!((String) this.f.remove(this.f.size() - 1)).isEmpty() || this.f.isEmpty()) {
                this.f.add("");
            } else {
                this.f.set(this.f.size() - 1, "");
            }
        }

        private boolean d(String str) {
            return str.equals(".") || str.equalsIgnoreCase("%2e");
        }

        private static String e(String str, int i, int i2) {
            String a2 = s.a(str, i, i2, false);
            if (!a2.contains(":")) {
                return l.a(a2);
            }
            InetAddress f2 = (!a2.startsWith("[") || !a2.endsWith("]")) ? f(a2, 0, a2.length()) : f(a2, 1, a2.length() - 1);
            if (f2 == null) {
                return null;
            }
            byte[] address = f2.getAddress();
            if (address.length == 16) {
                return a(address);
            }
            throw new AssertionError();
        }

        private boolean e(String str) {
            return str.equals("..") || str.equalsIgnoreCase("%2e.") || str.equalsIgnoreCase(".%2e") || str.equalsIgnoreCase("%2e%2e");
        }

        /* JADX WARNING: Code restructure failed: missing block: B:59:?, code lost:
            return null;
         */
        private static InetAddress f(String str, int i, int i2) {
            byte[] bArr = new byte[16];
            int i3 = i;
            int i4 = -1;
            int i5 = -1;
            int i6 = 0;
            while (true) {
                if (i3 >= i2) {
                    break;
                } else if (i6 == bArr.length) {
                    return null;
                } else {
                    if (i3 + 2 > i2 || !str.regionMatches(i3, "::", 0, 2)) {
                        if (i6 != 0) {
                            if (str.regionMatches(i3, ":", 0, 1)) {
                                i3++;
                            } else if (!str.regionMatches(i3, ".", 0, 1)) {
                                return null;
                            } else {
                                if (!a(str, i4, i2, bArr, i6 - 2)) {
                                    return null;
                                }
                                i6 += 2;
                            }
                        }
                    } else if (i5 != -1) {
                        return null;
                    } else {
                        i3 += 2;
                        i5 = i6 + 2;
                        if (i3 == i2) {
                            i6 = i5;
                            break;
                        }
                        i6 = i5;
                    }
                    int i7 = 0;
                    int i8 = i3;
                    while (i8 < i2) {
                        int a2 = s.a(str.charAt(i8));
                        if (a2 == -1) {
                            break;
                        }
                        i7 = (i7 << 4) + a2;
                        i8++;
                    }
                    int i9 = i8 - i3;
                    if (i9 != 0 && i9 <= 4) {
                        int i10 = i6 + 1;
                        bArr[i6] = (byte) ((i7 >>> 8) & 255);
                        i6 = i10 + 1;
                        bArr[i10] = (byte) (i7 & 255);
                        i4 = i3;
                        i3 = i8;
                    }
                }
            }
            if (i6 != bArr.length) {
                if (i5 == -1) {
                    return null;
                }
                System.arraycopy(bArr, i5, bArr, bArr.length - (i6 - i5), i6 - i5);
                Arrays.fill(bArr, i5, (bArr.length - i6) + i5, 0);
            }
            try {
                return InetAddress.getByAddress(bArr);
            } catch (UnknownHostException e2) {
                throw new AssertionError();
            }
        }

        private static int g(String str, int i, int i2) {
            try {
                int parseInt = Integer.parseInt(s.a(str, i, i2, "", false, false, false, true));
                if (parseInt <= 0 || parseInt > 65535) {
                    return -1;
                }
                return parseInt;
            } catch (NumberFormatException e2) {
                return -1;
            }
        }

        /* access modifiers changed from: 0000 */
        public int a() {
            return this.e != -1 ? this.e : s.a(this.f196a);
        }

        /* access modifiers changed from: 0000 */
        public C0002a a(s sVar, String str) {
            int i;
            int a2 = l.a(str, 0, str.length());
            int b2 = l.b(str, a2, str.length());
            if (b(str, a2, b2) != -1) {
                if (str.regionMatches(true, a2, "https:", 0, 6)) {
                    this.f196a = "https";
                    a2 += "https:".length();
                } else {
                    if (!str.regionMatches(true, a2, "http:", 0, 5)) {
                        return C0002a.UNSUPPORTED_SCHEME;
                    }
                    this.f196a = "http";
                    a2 += "http:".length();
                }
            } else if (sVar == null) {
                return C0002a.MISSING_SCHEME;
            } else {
                this.f196a = sVar.f194b;
            }
            boolean z = false;
            boolean z2 = false;
            int c2 = c(str, a2, b2);
            if (c2 >= 2 || sVar == null || !sVar.f194b.equals(this.f196a)) {
                int i2 = a2 + c2;
                while (true) {
                    boolean z3 = z2;
                    boolean z4 = z;
                    int i3 = i2;
                    int a3 = l.a(str, i3, b2, "@/\\?#");
                    switch (a3 != b2 ? str.charAt(a3) : 65535) {
                        case 65535:
                        case '#':
                        case '/':
                        case '?':
                        case '\\':
                            int d2 = d(str, i3, a3);
                            if (d2 + 1 < a3) {
                                this.d = e(str, i3, d2);
                                this.e = g(str, d2 + 1, a3);
                                if (this.e == -1) {
                                    return C0002a.INVALID_PORT;
                                }
                            } else {
                                this.d = e(str, i3, d2);
                                this.e = s.a(this.f196a);
                            }
                            if (this.d != null) {
                                a2 = a3;
                                break;
                            } else {
                                return C0002a.INVALID_HOST;
                            }
                        case '@':
                            if (!z3) {
                                int a4 = l.a(str, i3, a3, ':');
                                String a5 = s.a(str, i3, a4, " \"':;<=>@[]^`{}|/\\?#", true, false, false, true);
                                if (z4) {
                                    a5 = this.f197b + "%40" + a5;
                                }
                                this.f197b = a5;
                                if (a4 != a3) {
                                    z3 = true;
                                    this.c = s.a(str, a4 + 1, a3, " \"':;<=>@[]^`{}|/\\?#", true, false, false, true);
                                }
                                z4 = true;
                            } else {
                                this.c += "%40" + s.a(str, i3, a3, " \"':;<=>@[]^`{}|/\\?#", true, false, false, true);
                            }
                            i2 = a3 + 1;
                            z2 = z3;
                            continue;
                        default:
                            z2 = z3;
                            i2 = i3;
                            continue;
                    }
                    z = z4;
                }
            } else {
                this.f197b = sVar.d();
                this.c = sVar.e();
                this.d = sVar.e;
                this.e = sVar.f;
                this.f.clear();
                this.f.addAll(sVar.i());
                if (a2 == b2 || str.charAt(a2) == '#') {
                    c(sVar.k());
                }
            }
            int a6 = l.a(str, a2, b2, "?#");
            a(str, a2, a6);
            if (a6 >= b2 || str.charAt(a6) != '?') {
                i = a6;
            } else {
                i = l.a(str, a6, b2, '#');
                this.g = s.b(s.a(str, a6 + 1, i, " \"'<>#", true, false, true, true));
            }
            if (i < b2 && str.charAt(i) == '#') {
                this.h = s.a(str, i + 1, b2, "", true, false, false, false);
            }
            return C0002a.SUCCESS;
        }

        public a a(int i) {
            if (i <= 0 || i > 65535) {
                throw new IllegalArgumentException("unexpected port: " + i);
            }
            this.e = i;
            return this;
        }

        public a a(String str) {
            if (str == null) {
                throw new NullPointerException("scheme == null");
            }
            if (str.equalsIgnoreCase("http")) {
                this.f196a = "http";
            } else if (str.equalsIgnoreCase("https")) {
                this.f196a = "https";
            } else {
                throw new IllegalArgumentException("unexpected scheme: " + str);
            }
            return this;
        }

        public a a(String str, String str2) {
            String str3;
            if (str == null) {
                throw new NullPointerException("name == null");
            }
            if (this.g == null) {
                this.g = new ArrayList();
            }
            this.g.add(s.a(str, " \"'<>#&=", false, false, true, true));
            List<String> list = this.g;
            if (str2 != null) {
                str3 = s.a(str2, " \"'<>#&=", false, false, true, true);
            } else {
                str3 = null;
            }
            list.add(str3);
            return this;
        }

        /* access modifiers changed from: 0000 */
        public a b() {
            int size = this.f.size();
            for (int i = 0; i < size; i++) {
                this.f.set(i, s.a((String) this.f.get(i), "[]", true, true, false, true));
            }
            if (this.g != null) {
                int size2 = this.g.size();
                for (int i2 = 0; i2 < size2; i2++) {
                    String str = (String) this.g.get(i2);
                    if (str != null) {
                        this.g.set(i2, s.a(str, "\\^`{|}", true, true, true, true));
                    }
                }
            }
            if (this.h != null) {
                this.h = s.a(this.h, " \"#<>\\^`{|}", true, true, false, false);
            }
            return this;
        }

        public a b(String str) {
            if (str == null) {
                throw new NullPointerException("host == null");
            }
            String e2 = e(str, 0, str.length());
            if (e2 == null) {
                throw new IllegalArgumentException("unexpected host: " + str);
            }
            this.d = e2;
            return this;
        }

        public a b(String str, String str2) {
            String str3;
            if (str == null) {
                throw new NullPointerException("encodedName == null");
            }
            if (this.g == null) {
                this.g = new ArrayList();
            }
            this.g.add(s.a(str, " \"'<>#&=", true, false, true, true));
            List<String> list = this.g;
            if (str2 != null) {
                str3 = s.a(str2, " \"'<>#&=", true, false, true, true);
            } else {
                str3 = null;
            }
            list.add(str3);
            return this;
        }

        public a c(String str) {
            List<String> list;
            if (str != null) {
                list = s.b(s.a(str, " \"'<>#", true, false, true, true));
            } else {
                list = null;
            }
            this.g = list;
            return this;
        }

        public s c() {
            if (this.f196a == null) {
                throw new IllegalStateException("scheme == null");
            } else if (this.d != null) {
                return new s(this, null);
            } else {
                throw new IllegalStateException("host == null");
            }
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append(this.f196a);
            sb.append("://");
            if (!this.f197b.isEmpty() || !this.c.isEmpty()) {
                sb.append(this.f197b);
                if (!this.c.isEmpty()) {
                    sb.append(':');
                    sb.append(this.c);
                }
                sb.append('@');
            }
            if (this.d.indexOf(58) != -1) {
                sb.append('[');
                sb.append(this.d);
                sb.append(']');
            } else {
                sb.append(this.d);
            }
            int a2 = a();
            if (a2 != s.a(this.f196a)) {
                sb.append(':');
                sb.append(a2);
            }
            s.a(sb, this.f);
            if (this.g != null) {
                sb.append('?');
                s.b(sb, this.g);
            }
            if (this.h != null) {
                sb.append('#');
                sb.append(this.h);
            }
            return sb.toString();
        }
    }

    private s(a aVar) {
        String str = null;
        this.f194b = aVar.f196a;
        this.c = a(aVar.f197b, false);
        this.d = a(aVar.c, false);
        this.e = aVar.d;
        this.f = aVar.a();
        this.g = a(aVar.f, false);
        this.h = aVar.g != null ? a(aVar.g, true) : null;
        if (aVar.h != null) {
            str = a(aVar.h, false);
        }
        this.i = str;
        this.j = aVar.toString();
    }

    /* synthetic */ s(a aVar, AnonymousClass1 r2) {
        this(aVar);
    }

    static int a(char c2) {
        if (c2 >= '0' && c2 <= '9') {
            return c2 - '0';
        }
        if (c2 >= 'a' && c2 <= 'f') {
            return (c2 - 'a') + 10;
        }
        if (c2 < 'A' || c2 > 'F') {
            return -1;
        }
        return (c2 - 'A') + 10;
    }

    public static int a(String str) {
        if (str.equals("http")) {
            return 80;
        }
        return str.equals("https") ? 443 : -1;
    }

    static String a(String str, int i2, int i3, String str2, boolean z, boolean z2, boolean z3, boolean z4) {
        int i4 = i2;
        while (i4 < i3) {
            int codePointAt = str.codePointAt(i4);
            if (codePointAt < 32 || codePointAt == 127 || ((codePointAt >= 128 && z4) || str2.indexOf(codePointAt) != -1 || ((codePointAt == 37 && (!z || (z2 && !a(str, i4, i3)))) || (codePointAt == 43 && z3)))) {
                c cVar = new c();
                cVar.a(str, i2, i4);
                a(cVar, str, i4, i3, str2, z, z2, z3, z4);
                return cVar.p();
            }
            i4 += Character.charCount(codePointAt);
        }
        return str.substring(i2, i3);
    }

    static String a(String str, int i2, int i3, boolean z) {
        for (int i4 = i2; i4 < i3; i4++) {
            char charAt = str.charAt(i4);
            if (charAt == '%' || (charAt == '+' && z)) {
                c cVar = new c();
                cVar.a(str, i2, i4);
                a(cVar, str, i4, i3, z);
                return cVar.p();
            }
        }
        return str.substring(i2, i3);
    }

    static String a(String str, String str2, boolean z, boolean z2, boolean z3, boolean z4) {
        return a(str, 0, str.length(), str2, z, z2, z3, z4);
    }

    static String a(String str, boolean z) {
        return a(str, 0, str.length(), z);
    }

    private List<String> a(List<String> list, boolean z) {
        ArrayList arrayList = new ArrayList(list.size());
        for (String str : list) {
            arrayList.add(str != null ? a(str, z) : null);
        }
        return Collections.unmodifiableList(arrayList);
    }

    static void a(c cVar, String str, int i2, int i3, String str2, boolean z, boolean z2, boolean z3, boolean z4) {
        c cVar2 = null;
        while (i2 < i3) {
            int codePointAt = str.codePointAt(i2);
            if (!z || !(codePointAt == 9 || codePointAt == 10 || codePointAt == 12 || codePointAt == 13)) {
                if (codePointAt == 43 && z3) {
                    cVar.b(z ? "+" : "%2B");
                } else if (codePointAt < 32 || codePointAt == 127 || ((codePointAt >= 128 && z4) || str2.indexOf(codePointAt) != -1 || (codePointAt == 37 && (!z || (z2 && !a(str, i2, i3)))))) {
                    if (cVar2 == null) {
                        cVar2 = new c();
                    }
                    cVar2.a(codePointAt);
                    while (!cVar2.f()) {
                        byte i4 = cVar2.i() & 255;
                        cVar.i(37);
                        cVar.i((int) f193a[(i4 >> 4) & 15]);
                        cVar.i((int) f193a[i4 & 15]);
                    }
                } else {
                    cVar.a(codePointAt);
                }
            }
            i2 += Character.charCount(codePointAt);
        }
    }

    static void a(c cVar, String str, int i2, int i3, boolean z) {
        int i4 = i2;
        while (i4 < i3) {
            int codePointAt = str.codePointAt(i4);
            if (codePointAt != 37 || i4 + 2 >= i3) {
                if (codePointAt == 43 && z) {
                    cVar.i(32);
                }
                cVar.a(codePointAt);
            } else {
                int a2 = a(str.charAt(i4 + 1));
                int a3 = a(str.charAt(i4 + 2));
                if (!(a2 == -1 || a3 == -1)) {
                    cVar.i((a2 << 4) + a3);
                    i4 += 2;
                }
                cVar.a(codePointAt);
            }
            i4 += Character.charCount(codePointAt);
        }
    }

    static void a(StringBuilder sb, List<String> list) {
        int size = list.size();
        for (int i2 = 0; i2 < size; i2++) {
            sb.append('/');
            sb.append((String) list.get(i2));
        }
    }

    static boolean a(String str, int i2, int i3) {
        return i2 + 2 < i3 && str.charAt(i2) == '%' && a(str.charAt(i2 + 1)) != -1 && a(str.charAt(i2 + 2)) != -1;
    }

    static List<String> b(String str) {
        ArrayList arrayList = new ArrayList();
        int i2 = 0;
        while (i2 <= str.length()) {
            int indexOf = str.indexOf(38, i2);
            if (indexOf == -1) {
                indexOf = str.length();
            }
            int indexOf2 = str.indexOf(61, i2);
            if (indexOf2 == -1 || indexOf2 > indexOf) {
                arrayList.add(str.substring(i2, indexOf));
                arrayList.add(null);
            } else {
                arrayList.add(str.substring(i2, indexOf2));
                arrayList.add(str.substring(indexOf2 + 1, indexOf));
            }
            i2 = indexOf + 1;
        }
        return arrayList;
    }

    static void b(StringBuilder sb, List<String> list) {
        int size = list.size();
        for (int i2 = 0; i2 < size; i2 += 2) {
            String str = (String) list.get(i2);
            String str2 = (String) list.get(i2 + 1);
            if (i2 > 0) {
                sb.append('&');
            }
            sb.append(str);
            if (str2 != null) {
                sb.append('=');
                sb.append(str2);
            }
        }
    }

    public static s e(String str) {
        a aVar = new a();
        if (aVar.a((s) null, str) == C0002a.SUCCESS) {
            return aVar.c();
        }
        return null;
    }

    public URI a() {
        String aVar = n().b().toString();
        try {
            return new URI(aVar);
        } catch (URISyntaxException e2) {
            try {
                return URI.create(aVar.replaceAll("[\\u0000-\\u001F\\u007F-\\u009F\\p{javaWhitespace}]", ""));
            } catch (Exception e3) {
                throw new RuntimeException(e2);
            }
        }
    }

    public String b() {
        return this.f194b;
    }

    public s c(String str) {
        a d2 = d(str);
        if (d2 != null) {
            return d2.c();
        }
        return null;
    }

    public boolean c() {
        return this.f194b.equals("https");
    }

    public a d(String str) {
        a aVar = new a();
        if (aVar.a(this, str) == C0002a.SUCCESS) {
            return aVar;
        }
        return null;
    }

    public String d() {
        if (this.c.isEmpty()) {
            return "";
        }
        int length = this.f194b.length() + 3;
        return this.j.substring(length, l.a(this.j, length, this.j.length(), ":@"));
    }

    public String e() {
        if (this.d.isEmpty()) {
            return "";
        }
        return this.j.substring(this.j.indexOf(58, this.f194b.length() + 3) + 1, this.j.indexOf(64));
    }

    public boolean equals(Object obj) {
        return (obj instanceof s) && ((s) obj).j.equals(this.j);
    }

    public String f() {
        return this.e;
    }

    public int g() {
        return this.f;
    }

    public String h() {
        int indexOf = this.j.indexOf(47, this.f194b.length() + 3);
        return this.j.substring(indexOf, l.a(this.j, indexOf, this.j.length(), "?#"));
    }

    public int hashCode() {
        return this.j.hashCode();
    }

    public List<String> i() {
        int indexOf = this.j.indexOf(47, this.f194b.length() + 3);
        int a2 = l.a(this.j, indexOf, this.j.length(), "?#");
        ArrayList arrayList = new ArrayList();
        while (indexOf < a2) {
            int i2 = indexOf + 1;
            indexOf = l.a(this.j, i2, a2, '/');
            arrayList.add(this.j.substring(i2, indexOf));
        }
        return arrayList;
    }

    public List<String> j() {
        return this.g;
    }

    public String k() {
        if (this.h == null) {
            return null;
        }
        int indexOf = this.j.indexOf(63) + 1;
        return this.j.substring(indexOf, l.a(this.j, indexOf + 1, this.j.length(), '#'));
    }

    public String l() {
        if (this.h == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        b(sb, this.h);
        return sb.toString();
    }

    public String m() {
        if (this.i == null) {
            return null;
        }
        return this.j.substring(this.j.indexOf(35) + 1);
    }

    public a n() {
        a aVar = new a();
        aVar.f196a = this.f194b;
        aVar.f197b = d();
        aVar.c = e();
        aVar.d = this.e;
        aVar.e = this.f != a(this.f194b) ? this.f : -1;
        aVar.f.clear();
        aVar.f.addAll(i());
        aVar.c(k());
        aVar.h = m();
        return aVar;
    }

    public String toString() {
        return this.j;
    }
}
