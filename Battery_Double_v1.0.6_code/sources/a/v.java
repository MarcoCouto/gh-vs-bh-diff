package a;

import a.a.l;
import b.c;
import b.d;
import b.f;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public final class v extends aa {

    /* renamed from: a reason: collision with root package name */
    public static final u f202a = u.a("multipart/mixed");

    /* renamed from: b reason: collision with root package name */
    public static final u f203b = u.a("multipart/alternative");
    public static final u c = u.a("multipart/digest");
    public static final u d = u.a("multipart/parallel");
    public static final u e = u.a("multipart/form-data");
    private static final byte[] f = {58, 32};
    private static final byte[] g = {13, 10};
    private static final byte[] h = {45, 45};
    private final f i;
    private final u j;
    private final u k;
    private final List<b> l;
    private long m = -1;

    public static final class a {

        /* renamed from: a reason: collision with root package name */
        private final f f204a;

        /* renamed from: b reason: collision with root package name */
        private u f205b;
        private final List<b> c;

        public a() {
            this(UUID.randomUUID().toString());
        }

        public a(String str) {
            this.f205b = v.f202a;
            this.c = new ArrayList();
            this.f204a = f.a(str);
        }

        public a a(r rVar, aa aaVar) {
            return a(b.a(rVar, aaVar));
        }

        public a a(u uVar) {
            if (uVar == null) {
                throw new NullPointerException("type == null");
            } else if (!uVar.a().equals("multipart")) {
                throw new IllegalArgumentException("multipart != " + uVar);
            } else {
                this.f205b = uVar;
                return this;
            }
        }

        public a a(b bVar) {
            if (bVar == null) {
                throw new NullPointerException("part == null");
            }
            this.c.add(bVar);
            return this;
        }

        public v a() {
            if (!this.c.isEmpty()) {
                return new v(this.f204a, this.f205b, this.c);
            }
            throw new IllegalStateException("Multipart body must have at least one part.");
        }
    }

    public static final class b {
        /* access modifiers changed from: private */

        /* renamed from: a reason: collision with root package name */
        public final r f206a;
        /* access modifiers changed from: private */

        /* renamed from: b reason: collision with root package name */
        public final aa f207b;

        private b(r rVar, aa aaVar) {
            this.f206a = rVar;
            this.f207b = aaVar;
        }

        public static b a(r rVar, aa aaVar) {
            if (aaVar == null) {
                throw new NullPointerException("body == null");
            } else if (rVar != null && rVar.a("Content-Type") != null) {
                throw new IllegalArgumentException("Unexpected header: Content-Type");
            } else if (rVar == null || rVar.a("Content-Length") == null) {
                return new b(rVar, aaVar);
            } else {
                throw new IllegalArgumentException("Unexpected header: Content-Length");
            }
        }
    }

    v(f fVar, u uVar, List<b> list) {
        this.i = fVar;
        this.j = uVar;
        this.k = u.a(uVar + "; boundary=" + fVar.a());
        this.l = l.a(list);
    }

    private long a(d dVar, boolean z) throws IOException {
        c cVar;
        long j2 = 0;
        if (z) {
            c cVar2 = new c();
            cVar = cVar2;
            r13 = cVar2;
        } else {
            cVar = 0;
            r13 = dVar;
        }
        int size = this.l.size();
        for (int i2 = 0; i2 < size; i2++) {
            b bVar = (b) this.l.get(i2);
            r a2 = bVar.f206a;
            aa b2 = bVar.f207b;
            r13.c(h);
            r13.b(this.i);
            r13.c(g);
            if (a2 != null) {
                int a3 = a2.a();
                for (int i3 = 0; i3 < a3; i3++) {
                    r13.b(a2.a(i3)).c(f).b(a2.b(i3)).c(g);
                }
            }
            u contentType = b2.contentType();
            if (contentType != null) {
                r13.b("Content-Type: ").b(contentType.toString()).c(g);
            }
            long contentLength = b2.contentLength();
            if (contentLength != -1) {
                r13.b("Content-Length: ").k(contentLength).c(g);
            } else if (z) {
                cVar.s();
                return -1;
            }
            r13.c(g);
            if (z) {
                j2 += contentLength;
            } else {
                b2.writeTo(r13);
            }
            r13.c(g);
        }
        r13.c(h);
        r13.b(this.i);
        r13.c(h);
        r13.c(g);
        if (!z) {
            return j2;
        }
        long a4 = j2 + cVar.a();
        cVar.s();
        return a4;
    }

    public long contentLength() throws IOException {
        long j2 = this.m;
        if (j2 != -1) {
            return j2;
        }
        long a2 = a(null, true);
        this.m = a2;
        return a2;
    }

    public u contentType() {
        return this.k;
    }

    public void writeTo(d dVar) throws IOException {
        a(dVar, false);
    }
}
