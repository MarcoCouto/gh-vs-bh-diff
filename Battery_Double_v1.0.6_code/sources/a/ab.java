package a;

import java.io.Closeable;

public final class ab implements Closeable {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public final z f148a;
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public final x f149b;
    /* access modifiers changed from: private */
    public final int c;
    /* access modifiers changed from: private */
    public final String d;
    /* access modifiers changed from: private */
    public final q e;
    /* access modifiers changed from: private */
    public final r f;
    /* access modifiers changed from: private */
    public final ac g;
    /* access modifiers changed from: private */
    public final ab h;
    /* access modifiers changed from: private */
    public final ab i;
    /* access modifiers changed from: private */
    public final ab j;
    /* access modifiers changed from: private */
    public final long k;
    /* access modifiers changed from: private */
    public final long l;
    private volatile d m;

    public static class a {
        /* access modifiers changed from: private */

        /* renamed from: a reason: collision with root package name */
        public z f150a;
        /* access modifiers changed from: private */

        /* renamed from: b reason: collision with root package name */
        public x f151b;
        /* access modifiers changed from: private */
        public int c;
        /* access modifiers changed from: private */
        public String d;
        /* access modifiers changed from: private */
        public q e;
        /* access modifiers changed from: private */
        public a.r.a f;
        /* access modifiers changed from: private */
        public ac g;
        /* access modifiers changed from: private */
        public ab h;
        /* access modifiers changed from: private */
        public ab i;
        /* access modifiers changed from: private */
        public ab j;
        /* access modifiers changed from: private */
        public long k;
        /* access modifiers changed from: private */
        public long l;

        public a() {
            this.c = -1;
            this.f = new a.r.a();
        }

        private a(ab abVar) {
            this.c = -1;
            this.f150a = abVar.f148a;
            this.f151b = abVar.f149b;
            this.c = abVar.c;
            this.d = abVar.d;
            this.e = abVar.e;
            this.f = abVar.f.b();
            this.g = abVar.g;
            this.h = abVar.h;
            this.i = abVar.i;
            this.j = abVar.j;
            this.k = abVar.k;
            this.l = abVar.l;
        }

        private void a(String str, ab abVar) {
            if (abVar.g != null) {
                throw new IllegalArgumentException(str + ".body != null");
            } else if (abVar.h != null) {
                throw new IllegalArgumentException(str + ".networkResponse != null");
            } else if (abVar.i != null) {
                throw new IllegalArgumentException(str + ".cacheResponse != null");
            } else if (abVar.j != null) {
                throw new IllegalArgumentException(str + ".priorResponse != null");
            }
        }

        private void d(ab abVar) {
            if (abVar.g != null) {
                throw new IllegalArgumentException("priorResponse.body != null");
            }
        }

        public a a(int i2) {
            this.c = i2;
            return this;
        }

        public a a(long j2) {
            this.k = j2;
            return this;
        }

        public a a(ab abVar) {
            if (abVar != null) {
                a("networkResponse", abVar);
            }
            this.h = abVar;
            return this;
        }

        public a a(ac acVar) {
            this.g = acVar;
            return this;
        }

        public a a(q qVar) {
            this.e = qVar;
            return this;
        }

        public a a(r rVar) {
            this.f = rVar.b();
            return this;
        }

        public a a(x xVar) {
            this.f151b = xVar;
            return this;
        }

        public a a(z zVar) {
            this.f150a = zVar;
            return this;
        }

        public a a(String str) {
            this.d = str;
            return this;
        }

        public a a(String str, String str2) {
            this.f.a(str, str2);
            return this;
        }

        public ab a() {
            if (this.f150a == null) {
                throw new IllegalStateException("request == null");
            } else if (this.f151b == null) {
                throw new IllegalStateException("protocol == null");
            } else if (this.c >= 0) {
                return new ab(this);
            } else {
                throw new IllegalStateException("code < 0: " + this.c);
            }
        }

        public a b(long j2) {
            this.l = j2;
            return this;
        }

        public a b(ab abVar) {
            if (abVar != null) {
                a("cacheResponse", abVar);
            }
            this.i = abVar;
            return this;
        }

        public a c(ab abVar) {
            if (abVar != null) {
                d(abVar);
            }
            this.j = abVar;
            return this;
        }
    }

    private ab(a aVar) {
        this.f148a = aVar.f150a;
        this.f149b = aVar.f151b;
        this.c = aVar.c;
        this.d = aVar.d;
        this.e = aVar.e;
        this.f = aVar.f.a();
        this.g = aVar.g;
        this.h = aVar.h;
        this.i = aVar.i;
        this.j = aVar.j;
        this.k = aVar.k;
        this.l = aVar.l;
    }

    public z a() {
        return this.f148a;
    }

    public String a(String str) {
        return a(str, null);
    }

    public String a(String str, String str2) {
        String a2 = this.f.a(str);
        return a2 != null ? a2 : str2;
    }

    public int b() {
        return this.c;
    }

    public boolean c() {
        return this.c >= 200 && this.c < 300;
    }

    public void close() {
        this.g.close();
    }

    public String d() {
        return this.d;
    }

    public q e() {
        return this.e;
    }

    public r f() {
        return this.f;
    }

    public ac g() {
        return this.g;
    }

    public a h() {
        return new a();
    }

    public d i() {
        d dVar = this.m;
        if (dVar != null) {
            return dVar;
        }
        d a2 = d.a(this.f);
        this.m = a2;
        return a2;
    }

    public long j() {
        return this.k;
    }

    public long k() {
        return this.l;
    }

    public String toString() {
        return "Response{protocol=" + this.f149b + ", code=" + this.c + ", message=" + this.d + ", url=" + this.f148a.a() + '}';
    }
}
