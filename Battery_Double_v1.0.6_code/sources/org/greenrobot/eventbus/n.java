package org.greenrobot.eventbus;

final class n {

    /* renamed from: a reason: collision with root package name */
    final Object f4762a;

    /* renamed from: b reason: collision with root package name */
    final l f4763b;
    volatile boolean c = true;

    n(Object obj, l lVar) {
        this.f4762a = obj;
        this.f4763b = lVar;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof n)) {
            return false;
        }
        n nVar = (n) obj;
        return this.f4762a == nVar.f4762a && this.f4763b.equals(nVar.f4763b);
    }

    public int hashCode() {
        return this.f4762a.hashCode() + this.f4763b.f.hashCode();
    }
}
