package org.greenrobot.eventbus;

class a implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final i f4734a = new i();

    /* renamed from: b reason: collision with root package name */
    private final c f4735b;

    a(c cVar) {
        this.f4735b = cVar;
    }

    public void a(n nVar, Object obj) {
        this.f4734a.a(h.a(nVar, obj));
        this.f4735b.b().execute(this);
    }

    public void run() {
        h a2 = this.f4734a.a();
        if (a2 == null) {
            throw new IllegalStateException("No pending post available");
        }
        this.f4735b.a(a2);
    }
}
