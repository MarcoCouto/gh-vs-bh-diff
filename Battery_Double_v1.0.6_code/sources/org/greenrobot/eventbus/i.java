package org.greenrobot.eventbus;

final class i {

    /* renamed from: a reason: collision with root package name */
    private h f4752a;

    /* renamed from: b reason: collision with root package name */
    private h f4753b;

    i() {
    }

    /* access modifiers changed from: 0000 */
    public synchronized h a() {
        h hVar;
        hVar = this.f4752a;
        if (this.f4752a != null) {
            this.f4752a = this.f4752a.c;
            if (this.f4752a == null) {
                this.f4753b = null;
            }
        }
        return hVar;
    }

    /* access modifiers changed from: 0000 */
    public synchronized h a(int i) throws InterruptedException {
        if (this.f4752a == null) {
            wait((long) i);
        }
        return a();
    }

    /* access modifiers changed from: 0000 */
    public synchronized void a(h hVar) {
        if (hVar == null) {
            throw new NullPointerException("null cannot be enqueued");
        }
        if (this.f4753b != null) {
            this.f4753b.c = hVar;
            this.f4753b = hVar;
        } else if (this.f4752a == null) {
            this.f4753b = hVar;
            this.f4752a = hVar;
        } else {
            throw new IllegalStateException("Head present, but no tail");
        }
        notifyAll();
    }
}
