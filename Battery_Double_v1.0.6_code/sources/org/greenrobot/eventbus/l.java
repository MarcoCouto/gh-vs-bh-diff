package org.greenrobot.eventbus;

import java.lang.reflect.Method;

public class l {

    /* renamed from: a reason: collision with root package name */
    final Method f4756a;

    /* renamed from: b reason: collision with root package name */
    final ThreadMode f4757b;
    final Class<?> c;
    final int d;
    final boolean e;
    String f;

    public l(Method method, Class<?> cls, ThreadMode threadMode, int i, boolean z) {
        this.f4756a = method;
        this.f4757b = threadMode;
        this.c = cls;
        this.d = i;
        this.e = z;
    }

    private synchronized void a() {
        if (this.f == null) {
            StringBuilder sb = new StringBuilder(64);
            sb.append(this.f4756a.getDeclaringClass().getName());
            sb.append('#').append(this.f4756a.getName());
            sb.append('(').append(this.c.getName());
            this.f = sb.toString();
        }
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof l)) {
            return false;
        }
        a();
        l lVar = (l) obj;
        lVar.a();
        return this.f.equals(lVar.f);
    }

    public int hashCode() {
        return this.f4756a.hashCode();
    }
}
