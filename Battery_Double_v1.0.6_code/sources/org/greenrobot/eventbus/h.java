package org.greenrobot.eventbus;

import java.util.ArrayList;
import java.util.List;

final class h {
    private static final List<h> d = new ArrayList();

    /* renamed from: a reason: collision with root package name */
    Object f4750a;

    /* renamed from: b reason: collision with root package name */
    n f4751b;
    h c;

    private h(Object obj, n nVar) {
        this.f4750a = obj;
        this.f4751b = nVar;
    }

    static h a(n nVar, Object obj) {
        synchronized (d) {
            int size = d.size();
            if (size <= 0) {
                return new h(obj, nVar);
            }
            h hVar = (h) d.remove(size - 1);
            hVar.f4750a = obj;
            hVar.f4751b = nVar;
            hVar.c = null;
            return hVar;
        }
    }

    static void a(h hVar) {
        hVar.f4750a = null;
        hVar.f4751b = null;
        hVar.c = null;
        synchronized (d) {
            if (d.size() < 10000) {
                d.add(hVar);
            }
        }
    }
}
