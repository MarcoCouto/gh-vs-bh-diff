package org.greenrobot.eventbus;

public final class k {

    /* renamed from: a reason: collision with root package name */
    public final c f4754a;

    /* renamed from: b reason: collision with root package name */
    public final Throwable f4755b;
    public final Object c;
    public final Object d;

    public k(c cVar, Throwable th, Object obj, Object obj2) {
        this.f4754a = cVar;
        this.f4755b = th;
        this.c = obj;
        this.d = obj2;
    }
}
