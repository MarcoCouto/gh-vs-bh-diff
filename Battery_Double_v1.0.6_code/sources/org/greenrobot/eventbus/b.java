package org.greenrobot.eventbus;

import android.util.Log;

final class b implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final i f4736a = new i();

    /* renamed from: b reason: collision with root package name */
    private final c f4737b;
    private volatile boolean c;

    b(c cVar) {
        this.f4737b = cVar;
    }

    public void a(n nVar, Object obj) {
        h a2 = h.a(nVar, obj);
        synchronized (this) {
            this.f4736a.a(a2);
            if (!this.c) {
                this.c = true;
                this.f4737b.b().execute(this);
            }
        }
    }

    public void run() {
        while (true) {
            try {
                h a2 = this.f4736a.a(1000);
                if (a2 == null) {
                    synchronized (this) {
                        a2 = this.f4736a.a();
                        if (a2 == null) {
                            this.c = false;
                            this.c = false;
                            return;
                        }
                    }
                }
                this.f4737b.a(a2);
            } catch (InterruptedException e) {
                Log.w("Event", Thread.currentThread().getName() + " was interruppted", e);
                this.c = false;
                return;
            } catch (Throwable th) {
                this.c = false;
                throw th;
            }
        }
    }
}
