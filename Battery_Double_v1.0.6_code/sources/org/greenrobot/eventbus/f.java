package org.greenrobot.eventbus;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;

final class f extends Handler {

    /* renamed from: a reason: collision with root package name */
    private final i f4746a = new i();

    /* renamed from: b reason: collision with root package name */
    private final int f4747b;
    private final c c;
    private boolean d;

    f(c cVar, Looper looper, int i) {
        super(looper);
        this.c = cVar;
        this.f4747b = i;
    }

    /* access modifiers changed from: 0000 */
    public void a(n nVar, Object obj) {
        h a2 = h.a(nVar, obj);
        synchronized (this) {
            this.f4746a.a(a2);
            if (!this.d) {
                this.d = true;
                if (!sendMessage(obtainMessage())) {
                    throw new e("Could not send handler message");
                }
            }
        }
    }

    public void handleMessage(Message message) {
        try {
            long uptimeMillis = SystemClock.uptimeMillis();
            do {
                h a2 = this.f4746a.a();
                if (a2 == null) {
                    synchronized (this) {
                        a2 = this.f4746a.a();
                        if (a2 == null) {
                            this.d = false;
                            this.d = false;
                            return;
                        }
                    }
                }
                this.c.a(a2);
            } while (SystemClock.uptimeMillis() - uptimeMillis < ((long) this.f4747b));
            if (!sendMessage(obtainMessage())) {
                throw new e("Could not send handler message");
            }
            this.d = true;
        } catch (Throwable th) {
            this.d = false;
            throw th;
        }
    }
}
