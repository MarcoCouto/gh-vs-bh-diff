package android.support.design.internal;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Drawable.ConstantState;
import android.support.design.a.d;
import android.support.design.a.e;
import android.support.design.a.f;
import android.support.design.a.h;
import android.support.v4.i.r;
import android.support.v4.i.t;
import android.support.v7.view.menu.j;
import android.support.v7.widget.bq;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.TextView;
import com.hmatalonga.greenhub.Config;

public class a extends FrameLayout implements android.support.v7.view.menu.p.a {

    /* renamed from: a reason: collision with root package name */
    private static final int[] f297a = {16842912};

    /* renamed from: b reason: collision with root package name */
    private final int f298b;
    private final int c;
    private final float d;
    private final float e;
    private boolean f;
    private ImageView g;
    private final TextView h;
    private final TextView i;
    private int j;
    private j k;
    private ColorStateList l;

    public a(Context context) {
        this(context, null);
    }

    public a(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public a(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.j = -1;
        Resources resources = getResources();
        int dimensionPixelSize = resources.getDimensionPixelSize(d.design_bottom_navigation_text_size);
        int dimensionPixelSize2 = resources.getDimensionPixelSize(d.design_bottom_navigation_active_text_size);
        this.f298b = resources.getDimensionPixelSize(d.design_bottom_navigation_margin);
        this.c = dimensionPixelSize - dimensionPixelSize2;
        this.d = (((float) dimensionPixelSize2) * 1.0f) / ((float) dimensionPixelSize);
        this.e = (((float) dimensionPixelSize) * 1.0f) / ((float) dimensionPixelSize2);
        LayoutInflater.from(context).inflate(h.design_bottom_navigation_item, this, true);
        setBackgroundResource(e.design_bottom_navigation_item_background);
        this.g = (ImageView) findViewById(f.icon);
        this.h = (TextView) findViewById(f.smallLabel);
        this.i = (TextView) findViewById(f.largeLabel);
    }

    public void a(j jVar, int i2) {
        this.k = jVar;
        setCheckable(jVar.isCheckable());
        setChecked(jVar.isChecked());
        setEnabled(jVar.isEnabled());
        setIcon(jVar.getIcon());
        setTitle(jVar.getTitle());
        setId(jVar.getItemId());
        setContentDescription(jVar.getContentDescription());
        bq.a(this, jVar.getTooltipText());
    }

    public boolean a() {
        return false;
    }

    public j getItemData() {
        return this.k;
    }

    public int getItemPosition() {
        return this.j;
    }

    public int[] onCreateDrawableState(int i2) {
        int[] onCreateDrawableState = super.onCreateDrawableState(i2 + 1);
        if (this.k != null && this.k.isCheckable() && this.k.isChecked()) {
            mergeDrawableStates(onCreateDrawableState, f297a);
        }
        return onCreateDrawableState;
    }

    public void setCheckable(boolean z) {
        refreshDrawableState();
    }

    public void setChecked(boolean z) {
        this.i.setPivotX((float) (this.i.getWidth() / 2));
        this.i.setPivotY((float) this.i.getBaseline());
        this.h.setPivotX((float) (this.h.getWidth() / 2));
        this.h.setPivotY((float) this.h.getBaseline());
        if (this.f) {
            if (z) {
                LayoutParams layoutParams = (LayoutParams) this.g.getLayoutParams();
                layoutParams.gravity = 49;
                layoutParams.topMargin = this.f298b;
                this.g.setLayoutParams(layoutParams);
                this.i.setVisibility(0);
                this.i.setScaleX(1.0f);
                this.i.setScaleY(1.0f);
            } else {
                LayoutParams layoutParams2 = (LayoutParams) this.g.getLayoutParams();
                layoutParams2.gravity = 17;
                layoutParams2.topMargin = this.f298b;
                this.g.setLayoutParams(layoutParams2);
                this.i.setVisibility(4);
                this.i.setScaleX(0.5f);
                this.i.setScaleY(0.5f);
            }
            this.h.setVisibility(4);
        } else if (z) {
            LayoutParams layoutParams3 = (LayoutParams) this.g.getLayoutParams();
            layoutParams3.gravity = 49;
            layoutParams3.topMargin = this.f298b + this.c;
            this.g.setLayoutParams(layoutParams3);
            this.i.setVisibility(0);
            this.h.setVisibility(4);
            this.i.setScaleX(1.0f);
            this.i.setScaleY(1.0f);
            this.h.setScaleX(this.d);
            this.h.setScaleY(this.d);
        } else {
            LayoutParams layoutParams4 = (LayoutParams) this.g.getLayoutParams();
            layoutParams4.gravity = 49;
            layoutParams4.topMargin = this.f298b;
            this.g.setLayoutParams(layoutParams4);
            this.i.setVisibility(4);
            this.h.setVisibility(0);
            this.i.setScaleX(this.e);
            this.i.setScaleY(this.e);
            this.h.setScaleX(1.0f);
            this.h.setScaleY(1.0f);
        }
        refreshDrawableState();
    }

    public void setEnabled(boolean z) {
        super.setEnabled(z);
        this.h.setEnabled(z);
        this.i.setEnabled(z);
        this.g.setEnabled(z);
        if (z) {
            t.a((View) this, r.a(getContext(), Config.NOTIFICATION_BATTERY_FULL));
        } else {
            t.a((View) this, (r) null);
        }
    }

    public void setIcon(Drawable drawable) {
        if (drawable != null) {
            ConstantState constantState = drawable.getConstantState();
            if (constantState != null) {
                drawable = constantState.newDrawable();
            }
            drawable = android.support.v4.c.a.a.f(drawable).mutate();
            android.support.v4.c.a.a.a(drawable, this.l);
        }
        this.g.setImageDrawable(drawable);
    }

    public void setIconTintList(ColorStateList colorStateList) {
        this.l = colorStateList;
        if (this.k != null) {
            setIcon(this.k.getIcon());
        }
    }

    public void setItemBackground(int i2) {
        t.a((View) this, i2 == 0 ? null : android.support.v4.b.a.a(getContext(), i2));
    }

    public void setItemPosition(int i2) {
        this.j = i2;
    }

    public void setShiftingMode(boolean z) {
        this.f = z;
    }

    public void setTextColor(ColorStateList colorStateList) {
        this.h.setTextColor(colorStateList);
        this.i.setTextColor(colorStateList);
    }

    public void setTitle(CharSequence charSequence) {
        this.h.setText(charSequence);
        this.i.setText(charSequence);
    }
}
