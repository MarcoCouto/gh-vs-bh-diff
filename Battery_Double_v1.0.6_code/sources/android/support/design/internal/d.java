package android.support.design.internal;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.v7.view.menu.h;
import android.support.v7.view.menu.j;
import android.support.v7.view.menu.o;
import android.support.v7.view.menu.u;

public class d implements o {

    /* renamed from: a reason: collision with root package name */
    private h f302a;

    /* renamed from: b reason: collision with root package name */
    private c f303b;
    private boolean c = false;
    private int d;

    static class a implements Parcelable {
        public static final Creator<a> CREATOR = new Creator<a>() {
            /* renamed from: a */
            public a createFromParcel(Parcel parcel) {
                return new a(parcel);
            }

            /* renamed from: a */
            public a[] newArray(int i) {
                return new a[i];
            }
        };

        /* renamed from: a reason: collision with root package name */
        int f304a;

        a() {
        }

        a(Parcel parcel) {
            this.f304a = parcel.readInt();
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(this.f304a);
        }
    }

    public void a(int i) {
        this.d = i;
    }

    public void a(Context context, h hVar) {
        this.f303b.a(this.f302a);
        this.f302a = hVar;
    }

    public void a(Parcelable parcelable) {
        if (parcelable instanceof a) {
            this.f303b.a(((a) parcelable).f304a);
        }
    }

    public void a(c cVar) {
        this.f303b = cVar;
    }

    public void a(h hVar, boolean z) {
    }

    public void a(android.support.v7.view.menu.o.a aVar) {
    }

    public void a(boolean z) {
        if (!this.c) {
            if (z) {
                this.f303b.a();
            } else {
                this.f303b.b();
            }
        }
    }

    public boolean a() {
        return false;
    }

    public boolean a(h hVar, j jVar) {
        return false;
    }

    public boolean a(u uVar) {
        return false;
    }

    public int b() {
        return this.d;
    }

    public void b(boolean z) {
        this.c = z;
    }

    public boolean b(h hVar, j jVar) {
        return false;
    }

    public Parcelable c() {
        a aVar = new a();
        aVar.f304a = this.f303b.getSelectedItemId();
        return aVar;
    }
}
