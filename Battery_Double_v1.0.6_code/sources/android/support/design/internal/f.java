package android.support.design.internal;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.support.transition.aa;
import android.support.transition.u;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.Map;

public class f extends u {
    private void d(aa aaVar) {
        if (aaVar.f450b instanceof TextView) {
            aaVar.f449a.put("android:textscale:scale", Float.valueOf(((TextView) aaVar.f450b).getScaleX()));
        }
    }

    public Animator a(ViewGroup viewGroup, aa aaVar, aa aaVar2) {
        float f = 1.0f;
        if (aaVar == null || aaVar2 == null || !(aaVar.f450b instanceof TextView) || !(aaVar2.f450b instanceof TextView)) {
            return null;
        }
        final TextView textView = (TextView) aaVar2.f450b;
        Map<String, Object> map = aaVar.f449a;
        Map<String, Object> map2 = aaVar2.f449a;
        float f2 = map.get("android:textscale:scale") != null ? ((Float) map.get("android:textscale:scale")).floatValue() : 1.0f;
        if (map2.get("android:textscale:scale") != null) {
            f = ((Float) map2.get("android:textscale:scale")).floatValue();
        }
        if (f2 == f) {
            return null;
        }
        ValueAnimator ofFloat = ValueAnimator.ofFloat(new float[]{f2, f});
        ofFloat.addUpdateListener(new AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float floatValue = ((Float) valueAnimator.getAnimatedValue()).floatValue();
                textView.setScaleX(floatValue);
                textView.setScaleY(floatValue);
            }
        });
        return ofFloat;
    }

    public void a(aa aaVar) {
        d(aaVar);
    }

    public void b(aa aaVar) {
        d(aaVar);
    }
}
