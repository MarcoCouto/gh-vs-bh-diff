package android.support.design.internal;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.design.a.d;
import android.support.design.a.f;
import android.support.design.a.j;
import android.support.design.widget.b.c;
import android.support.v4.i.t;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.MeasureSpec;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class SnackbarContentLayout extends LinearLayout implements c {

    /* renamed from: a reason: collision with root package name */
    private TextView f295a;

    /* renamed from: b reason: collision with root package name */
    private Button f296b;
    private int c;
    private int d;

    public SnackbarContentLayout(Context context) {
        this(context, null);
    }

    public SnackbarContentLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, j.SnackbarLayout);
        this.c = obtainStyledAttributes.getDimensionPixelSize(j.SnackbarLayout_android_maxWidth, -1);
        this.d = obtainStyledAttributes.getDimensionPixelSize(j.SnackbarLayout_maxActionInlineWidth, -1);
        obtainStyledAttributes.recycle();
    }

    private static void a(View view, int i, int i2) {
        if (t.q(view)) {
            t.a(view, t.f(view), i, t.g(view), i2);
        } else {
            view.setPadding(view.getPaddingLeft(), i, view.getPaddingRight(), i2);
        }
    }

    private boolean a(int i, int i2, int i3) {
        boolean z = false;
        if (i != getOrientation()) {
            setOrientation(i);
            z = true;
        }
        if (this.f295a.getPaddingTop() == i2 && this.f295a.getPaddingBottom() == i3) {
            return z;
        }
        a((View) this.f295a, i2, i3);
        return true;
    }

    public void a(int i, int i2) {
        this.f295a.setAlpha(0.0f);
        this.f295a.animate().alpha(1.0f).setDuration((long) i2).setStartDelay((long) i).start();
        if (this.f296b.getVisibility() == 0) {
            this.f296b.setAlpha(0.0f);
            this.f296b.animate().alpha(1.0f).setDuration((long) i2).setStartDelay((long) i).start();
        }
    }

    public void b(int i, int i2) {
        this.f295a.setAlpha(1.0f);
        this.f295a.animate().alpha(0.0f).setDuration((long) i2).setStartDelay((long) i).start();
        if (this.f296b.getVisibility() == 0) {
            this.f296b.setAlpha(1.0f);
            this.f296b.animate().alpha(0.0f).setDuration((long) i2).setStartDelay((long) i).start();
        }
    }

    public Button getActionView() {
        return this.f296b;
    }

    public TextView getMessageView() {
        return this.f295a;
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        this.f295a = (TextView) findViewById(f.snackbar_text);
        this.f296b = (Button) findViewById(f.snackbar_action);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        boolean z;
        super.onMeasure(i, i2);
        if (this.c > 0 && getMeasuredWidth() > this.c) {
            i = MeasureSpec.makeMeasureSpec(this.c, 1073741824);
            super.onMeasure(i, i2);
        }
        int dimensionPixelSize = getResources().getDimensionPixelSize(d.design_snackbar_padding_vertical_2lines);
        int dimensionPixelSize2 = getResources().getDimensionPixelSize(d.design_snackbar_padding_vertical);
        boolean z2 = this.f295a.getLayout().getLineCount() > 1;
        if (!z2 || this.d <= 0 || this.f296b.getMeasuredWidth() <= this.d) {
            if (!z2) {
                dimensionPixelSize = dimensionPixelSize2;
            }
            if (a(0, dimensionPixelSize, dimensionPixelSize)) {
                z = true;
            }
            z = false;
        } else {
            if (a(1, dimensionPixelSize, dimensionPixelSize - dimensionPixelSize2)) {
                z = true;
            }
            z = false;
        }
        if (z) {
            super.onMeasure(i, i2);
        }
    }
}
