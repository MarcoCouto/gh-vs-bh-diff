package android.support.design.internal;

import android.animation.TimeInterpolator;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.support.design.a.d;
import android.support.transition.e;
import android.support.transition.u;
import android.support.transition.w;
import android.support.transition.y;
import android.support.v4.h.k.a;
import android.support.v4.i.b.b;
import android.support.v4.i.t;
import android.support.v7.view.menu.h;
import android.support.v7.view.menu.j;
import android.support.v7.view.menu.o;
import android.support.v7.view.menu.p;
import android.util.AttributeSet;
import android.view.MenuItem;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

public class c extends ViewGroup implements p {

    /* renamed from: a reason: collision with root package name */
    private final y f299a;

    /* renamed from: b reason: collision with root package name */
    private final int f300b;
    private final int c;
    private final int d;
    private final int e;
    private final OnClickListener f;
    private final a<a> g;
    private boolean h;
    private a[] i;
    private int j;
    private int k;
    private ColorStateList l;
    private ColorStateList m;
    private int n;
    private int[] o;
    /* access modifiers changed from: private */
    public d p;
    /* access modifiers changed from: private */
    public h q;

    public c(Context context) {
        this(context, null);
    }

    public c(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.g = new android.support.v4.h.k.c(5);
        this.h = true;
        this.j = 0;
        this.k = 0;
        Resources resources = getResources();
        this.f300b = resources.getDimensionPixelSize(d.design_bottom_navigation_item_max_width);
        this.c = resources.getDimensionPixelSize(d.design_bottom_navigation_item_min_width);
        this.d = resources.getDimensionPixelSize(d.design_bottom_navigation_active_item_max_width);
        this.e = resources.getDimensionPixelSize(d.design_bottom_navigation_height);
        this.f299a = new e();
        this.f299a.a(0);
        this.f299a.a(115);
        this.f299a.a((TimeInterpolator) new b());
        this.f299a.b((u) new f());
        this.f = new OnClickListener() {
            public void onClick(View view) {
                j itemData = ((a) view).getItemData();
                if (!c.this.q.a((MenuItem) itemData, (o) c.this.p, 0)) {
                    itemData.setChecked(true);
                }
            }
        };
        this.o = new int[5];
    }

    private a getNewItem() {
        a aVar = (a) this.g.a();
        return aVar == null ? new a(getContext()) : aVar;
    }

    public void a() {
        removeAllViews();
        if (this.i != null) {
            for (a a2 : this.i) {
                this.g.a(a2);
            }
        }
        if (this.q.size() == 0) {
            this.j = 0;
            this.k = 0;
            this.i = null;
            return;
        }
        this.i = new a[this.q.size()];
        this.h = this.q.size() > 3;
        for (int i2 = 0; i2 < this.q.size(); i2++) {
            this.p.b(true);
            this.q.getItem(i2).setCheckable(true);
            this.p.b(false);
            a newItem = getNewItem();
            this.i[i2] = newItem;
            newItem.setIconTintList(this.l);
            newItem.setTextColor(this.m);
            newItem.setItemBackground(this.n);
            newItem.setShiftingMode(this.h);
            newItem.a((j) this.q.getItem(i2), 0);
            newItem.setItemPosition(i2);
            newItem.setOnClickListener(this.f);
            addView(newItem);
        }
        this.k = Math.min(this.q.size() - 1, this.k);
        this.q.getItem(this.k).setChecked(true);
    }

    /* access modifiers changed from: 0000 */
    public void a(int i2) {
        int size = this.q.size();
        for (int i3 = 0; i3 < size; i3++) {
            MenuItem item = this.q.getItem(i3);
            if (i2 == item.getItemId()) {
                this.j = i2;
                this.k = i3;
                item.setChecked(true);
                return;
            }
        }
    }

    public void a(h hVar) {
        this.q = hVar;
    }

    public void b() {
        int size = this.q.size();
        if (size != this.i.length) {
            a();
            return;
        }
        int i2 = this.j;
        for (int i3 = 0; i3 < size; i3++) {
            MenuItem item = this.q.getItem(i3);
            if (item.isChecked()) {
                this.j = item.getItemId();
                this.k = i3;
            }
        }
        if (i2 != this.j) {
            w.a(this, this.f299a);
        }
        for (int i4 = 0; i4 < size; i4++) {
            this.p.b(true);
            this.i[i4].a((j) this.q.getItem(i4), 0);
            this.p.b(false);
        }
    }

    public ColorStateList getIconTintList() {
        return this.l;
    }

    public int getItemBackgroundRes() {
        return this.n;
    }

    public ColorStateList getItemTextColor() {
        return this.m;
    }

    public int getSelectedItemId() {
        return this.j;
    }

    public int getWindowAnimations() {
        return 0;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        int childCount = getChildCount();
        int i6 = i4 - i2;
        int i7 = i5 - i3;
        int i8 = 0;
        for (int i9 = 0; i9 < childCount; i9++) {
            View childAt = getChildAt(i9);
            if (childAt.getVisibility() != 8) {
                if (t.e(this) == 1) {
                    childAt.layout((i6 - i8) - childAt.getMeasuredWidth(), 0, i6 - i8, i7);
                } else {
                    childAt.layout(i8, 0, childAt.getMeasuredWidth() + i8, i7);
                }
                i8 += childAt.getMeasuredWidth();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        int i4;
        int size = MeasureSpec.getSize(i2);
        int childCount = getChildCount();
        int makeMeasureSpec = MeasureSpec.makeMeasureSpec(this.e, 1073741824);
        if (this.h) {
            int i5 = childCount - 1;
            int min = Math.min(size - (this.c * i5), this.d);
            int min2 = Math.min((size - min) / i5, this.f300b);
            int i6 = (size - min) - (i5 * min2);
            int i7 = 0;
            while (i7 < childCount) {
                this.o[i7] = i7 == this.k ? min : min2;
                if (i6 > 0) {
                    int[] iArr = this.o;
                    iArr[i7] = iArr[i7] + 1;
                    i4 = i6 - 1;
                } else {
                    i4 = i6;
                }
                i7++;
                i6 = i4;
            }
        } else {
            int min3 = Math.min(size / (childCount == 0 ? 1 : childCount), this.d);
            int i8 = size - (min3 * childCount);
            for (int i9 = 0; i9 < childCount; i9++) {
                this.o[i9] = min3;
                if (i8 > 0) {
                    int[] iArr2 = this.o;
                    iArr2[i9] = iArr2[i9] + 1;
                    i8--;
                }
            }
        }
        int i10 = 0;
        for (int i11 = 0; i11 < childCount; i11++) {
            View childAt = getChildAt(i11);
            if (childAt.getVisibility() != 8) {
                childAt.measure(MeasureSpec.makeMeasureSpec(this.o[i11], 1073741824), makeMeasureSpec);
                childAt.getLayoutParams().width = childAt.getMeasuredWidth();
                i10 += childAt.getMeasuredWidth();
            }
        }
        setMeasuredDimension(View.resolveSizeAndState(i10, MeasureSpec.makeMeasureSpec(i10, 1073741824), 0), View.resolveSizeAndState(this.e, makeMeasureSpec, 0));
    }

    public void setIconTintList(ColorStateList colorStateList) {
        this.l = colorStateList;
        if (this.i != null) {
            for (a iconTintList : this.i) {
                iconTintList.setIconTintList(colorStateList);
            }
        }
    }

    public void setItemBackgroundRes(int i2) {
        this.n = i2;
        if (this.i != null) {
            for (a itemBackground : this.i) {
                itemBackground.setItemBackground(i2);
            }
        }
    }

    public void setItemTextColor(ColorStateList colorStateList) {
        this.m = colorStateList;
        if (this.i != null) {
            for (a textColor : this.i) {
                textColor.setTextColor(colorStateList);
            }
        }
    }

    public void setPresenter(d dVar) {
        this.p = dVar;
    }
}
