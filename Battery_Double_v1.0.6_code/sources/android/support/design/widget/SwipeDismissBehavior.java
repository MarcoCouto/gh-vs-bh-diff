package android.support.design.widget;

import android.support.v4.i.t;
import android.support.v4.widget.q;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

public class SwipeDismissBehavior<V extends View> extends android.support.design.widget.CoordinatorLayout.a<V> {

    /* renamed from: a reason: collision with root package name */
    private boolean f347a;

    /* renamed from: b reason: collision with root package name */
    q f348b;
    a c;
    int d = 2;
    float e = 0.5f;
    float f = 0.0f;
    float g = 0.5f;
    private float h = 0.0f;
    private boolean i;
    private final android.support.v4.widget.q.a j = new android.support.v4.widget.q.a() {

        /* renamed from: b reason: collision with root package name */
        private int f350b;
        private int c = -1;

        private boolean a(View view, float f) {
            if (f != 0.0f) {
                boolean z = t.e(view) == 1;
                if (SwipeDismissBehavior.this.d == 2) {
                    return true;
                }
                if (SwipeDismissBehavior.this.d == 0) {
                    return z ? f < 0.0f : f > 0.0f;
                }
                if (SwipeDismissBehavior.this.d == 1) {
                    return z ? f > 0.0f : f < 0.0f;
                }
                return false;
            }
            return Math.abs(view.getLeft() - this.f350b) >= Math.round(((float) view.getWidth()) * SwipeDismissBehavior.this.e);
        }

        public int a(View view, int i, int i2) {
            return view.getTop();
        }

        public void a(int i) {
            if (SwipeDismissBehavior.this.c != null) {
                SwipeDismissBehavior.this.c.a(i);
            }
        }

        public void a(View view, float f, float f2) {
            int i;
            this.c = -1;
            int width = view.getWidth();
            boolean z = false;
            if (a(view, f)) {
                i = view.getLeft() < this.f350b ? this.f350b - width : this.f350b + width;
                z = true;
            } else {
                i = this.f350b;
            }
            if (SwipeDismissBehavior.this.f348b.a(i, view.getTop())) {
                t.a(view, (Runnable) new b(view, z));
            } else if (z && SwipeDismissBehavior.this.c != null) {
                SwipeDismissBehavior.this.c.a(view);
            }
        }

        public void a(View view, int i, int i2, int i3, int i4) {
            float width = ((float) this.f350b) + (((float) view.getWidth()) * SwipeDismissBehavior.this.f);
            float width2 = ((float) this.f350b) + (((float) view.getWidth()) * SwipeDismissBehavior.this.g);
            if (((float) i) <= width) {
                view.setAlpha(1.0f);
            } else if (((float) i) >= width2) {
                view.setAlpha(0.0f);
            } else {
                view.setAlpha(SwipeDismissBehavior.a(0.0f, 1.0f - SwipeDismissBehavior.b(width, width2, (float) i), 1.0f));
            }
        }

        public boolean a(View view, int i) {
            return this.c == -1 && SwipeDismissBehavior.this.a(view);
        }

        public int b(View view) {
            return view.getWidth();
        }

        public int b(View view, int i, int i2) {
            int width;
            int width2;
            boolean z = t.e(view) == 1;
            if (SwipeDismissBehavior.this.d == 0) {
                if (z) {
                    width = this.f350b - view.getWidth();
                    width2 = this.f350b;
                } else {
                    width = this.f350b;
                    width2 = this.f350b + view.getWidth();
                }
            } else if (SwipeDismissBehavior.this.d != 1) {
                width = this.f350b - view.getWidth();
                width2 = this.f350b + view.getWidth();
            } else if (z) {
                width = this.f350b;
                width2 = this.f350b + view.getWidth();
            } else {
                width = this.f350b - view.getWidth();
                width2 = this.f350b;
            }
            return SwipeDismissBehavior.a(width, i, width2);
        }

        public void b(View view, int i) {
            this.c = i;
            this.f350b = view.getLeft();
            ViewParent parent = view.getParent();
            if (parent != null) {
                parent.requestDisallowInterceptTouchEvent(true);
            }
        }
    };

    public interface a {
        void a(int i);

        void a(View view);
    }

    private class b implements Runnable {

        /* renamed from: b reason: collision with root package name */
        private final View f352b;
        private final boolean c;

        b(View view, boolean z) {
            this.f352b = view;
            this.c = z;
        }

        public void run() {
            if (SwipeDismissBehavior.this.f348b != null && SwipeDismissBehavior.this.f348b.a(true)) {
                t.a(this.f352b, (Runnable) this);
            } else if (this.c && SwipeDismissBehavior.this.c != null) {
                SwipeDismissBehavior.this.c.a(this.f352b);
            }
        }
    }

    static float a(float f2, float f3, float f4) {
        return Math.min(Math.max(f2, f3), f4);
    }

    static int a(int i2, int i3, int i4) {
        return Math.min(Math.max(i2, i3), i4);
    }

    private void a(ViewGroup viewGroup) {
        if (this.f348b == null) {
            this.f348b = this.i ? q.a(viewGroup, this.h, this.j) : q.a(viewGroup, this.j);
        }
    }

    static float b(float f2, float f3, float f4) {
        return (f4 - f2) / (f3 - f2);
    }

    public void a(float f2) {
        this.f = a(0.0f, f2, 1.0f);
    }

    public void a(int i2) {
        this.d = i2;
    }

    public void a(a aVar) {
        this.c = aVar;
    }

    public boolean a(CoordinatorLayout coordinatorLayout, V v, MotionEvent motionEvent) {
        boolean z = this.f347a;
        switch (motionEvent.getActionMasked()) {
            case 0:
                this.f347a = coordinatorLayout.a((View) v, (int) motionEvent.getX(), (int) motionEvent.getY());
                z = this.f347a;
                break;
            case 1:
            case 3:
                this.f347a = false;
                break;
        }
        if (!z) {
            return false;
        }
        a((ViewGroup) coordinatorLayout);
        return this.f348b.a(motionEvent);
    }

    public boolean a(View view) {
        return true;
    }

    public void b(float f2) {
        this.g = a(0.0f, f2, 1.0f);
    }

    public boolean b(CoordinatorLayout coordinatorLayout, V v, MotionEvent motionEvent) {
        if (this.f348b == null) {
            return false;
        }
        this.f348b.b(motionEvent);
        return true;
    }
}
