package android.support.design.widget;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Looper;
import android.os.Message;
import android.support.design.a.C0009a;
import android.support.design.a.h;
import android.support.design.a.j;
import android.support.design.widget.b;
import android.support.v4.i.ac;
import android.support.v4.i.p;
import android.support.v4.i.t;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityManager;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import java.util.List;

public abstract class b<B extends b<B>> {

    /* renamed from: a reason: collision with root package name */
    static final Handler f355a = new Handler(Looper.getMainLooper(), new Callback() {
        public boolean handleMessage(Message message) {
            switch (message.what) {
                case 0:
                    ((b) message.obj).c();
                    return true;
                case 1:
                    ((b) message.obj).c(message.arg1);
                    return true;
                default:
                    return false;
            }
        }
    });
    /* access modifiers changed from: private */
    public static final boolean d = (VERSION.SDK_INT >= 16 && VERSION.SDK_INT <= 19);

    /* renamed from: b reason: collision with root package name */
    final f f356b;
    final a c = new a() {
        public void a() {
            b.f355a.sendMessage(b.f355a.obtainMessage(0, b.this));
        }

        public void a(int i) {
            b.f355a.sendMessage(b.f355a.obtainMessage(1, i, 0, b.this));
        }
    };
    private final ViewGroup e;
    private final Context f;
    /* access modifiers changed from: private */
    public final c g;
    private int h;
    private List<a<B>> i;
    private final AccessibilityManager j;

    public static abstract class a<B> {
        public void a(B b2) {
        }

        public void a(B b2, int i) {
        }
    }

    /* renamed from: android.support.design.widget.b$b reason: collision with other inner class name */
    final class C0010b extends SwipeDismissBehavior<f> {
        C0010b() {
        }

        public boolean a(CoordinatorLayout coordinatorLayout, f fVar, MotionEvent motionEvent) {
            switch (motionEvent.getActionMasked()) {
                case 0:
                    if (coordinatorLayout.a((View) fVar, (int) motionEvent.getX(), (int) motionEvent.getY())) {
                        l.a().c(b.this.c);
                        break;
                    }
                    break;
                case 1:
                case 3:
                    l.a().d(b.this.c);
                    break;
            }
            return super.a(coordinatorLayout, fVar, motionEvent);
        }

        public boolean a(View view) {
            return view instanceof f;
        }
    }

    public interface c {
        void a(int i, int i2);

        void b(int i, int i2);
    }

    interface d {
        void a(View view);

        void b(View view);
    }

    interface e {
        void a(View view, int i, int i2, int i3, int i4);
    }

    static class f extends FrameLayout {

        /* renamed from: a reason: collision with root package name */
        private e f374a;

        /* renamed from: b reason: collision with root package name */
        private d f375b;

        f(Context context) {
            this(context, null);
        }

        f(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, j.SnackbarLayout);
            if (obtainStyledAttributes.hasValue(j.SnackbarLayout_elevation)) {
                t.a((View) this, (float) obtainStyledAttributes.getDimensionPixelSize(j.SnackbarLayout_elevation, 0));
            }
            obtainStyledAttributes.recycle();
            setClickable(true);
        }

        /* access modifiers changed from: protected */
        public void onAttachedToWindow() {
            super.onAttachedToWindow();
            if (this.f375b != null) {
                this.f375b.a(this);
            }
            t.n(this);
        }

        /* access modifiers changed from: protected */
        public void onDetachedFromWindow() {
            super.onDetachedFromWindow();
            if (this.f375b != null) {
                this.f375b.b(this);
            }
        }

        /* access modifiers changed from: protected */
        public void onLayout(boolean z, int i, int i2, int i3, int i4) {
            super.onLayout(z, i, i2, i3, i4);
            if (this.f374a != null) {
                this.f374a.a(this, i, i2, i3, i4);
            }
        }

        /* access modifiers changed from: 0000 */
        public void setOnAttachStateChangeListener(d dVar) {
            this.f375b = dVar;
        }

        /* access modifiers changed from: 0000 */
        public void setOnLayoutChangeListener(e eVar) {
            this.f374a = eVar;
        }
    }

    protected b(ViewGroup viewGroup, View view, c cVar) {
        if (viewGroup == null) {
            throw new IllegalArgumentException("Transient bottom bar must have non-null parent");
        } else if (view == null) {
            throw new IllegalArgumentException("Transient bottom bar must have non-null content");
        } else if (cVar == null) {
            throw new IllegalArgumentException("Transient bottom bar must have non-null callback");
        } else {
            this.e = viewGroup;
            this.g = cVar;
            this.f = viewGroup.getContext();
            p.a(this.f);
            this.f356b = (f) LayoutInflater.from(this.f).inflate(h.design_layout_snackbar, this.e, false);
            this.f356b.addView(view);
            t.b((View) this.f356b, 1);
            t.a((View) this.f356b, 1);
            t.b((View) this.f356b, true);
            t.a((View) this.f356b, (p) new p() {
                public ac a(View view, ac acVar) {
                    view.setPadding(view.getPaddingLeft(), view.getPaddingTop(), view.getPaddingRight(), acVar.d());
                    return acVar;
                }
            });
            this.j = (AccessibilityManager) this.f.getSystemService("accessibility");
        }
    }

    private void e(final int i2) {
        if (VERSION.SDK_INT >= 12) {
            ValueAnimator valueAnimator = new ValueAnimator();
            valueAnimator.setIntValues(new int[]{0, this.f356b.getHeight()});
            valueAnimator.setInterpolator(a.f354b);
            valueAnimator.setDuration(250);
            valueAnimator.addListener(new AnimatorListenerAdapter() {
                public void onAnimationEnd(Animator animator) {
                    b.this.d(i2);
                }

                public void onAnimationStart(Animator animator) {
                    b.this.g.b(0, 180);
                }
            });
            valueAnimator.addUpdateListener(new AnimatorUpdateListener() {

                /* renamed from: b reason: collision with root package name */
                private int f364b = 0;

                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    int intValue = ((Integer) valueAnimator.getAnimatedValue()).intValue();
                    if (b.d) {
                        t.c(b.this.f356b, intValue - this.f364b);
                    } else {
                        b.this.f356b.setTranslationY((float) intValue);
                    }
                    this.f364b = intValue;
                }
            });
            valueAnimator.start();
            return;
        }
        Animation loadAnimation = AnimationUtils.loadAnimation(this.f356b.getContext(), C0009a.design_snackbar_out);
        loadAnimation.setInterpolator(a.f354b);
        loadAnimation.setDuration(250);
        loadAnimation.setAnimationListener(new AnimationListener() {
            public void onAnimationEnd(Animation animation) {
                b.this.d(i2);
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationStart(Animation animation) {
            }
        });
        this.f356b.startAnimation(loadAnimation);
    }

    public B a(int i2) {
        this.h = i2;
        return this;
    }

    public void a() {
        l.a().a(this.h, this.c);
    }

    /* access modifiers changed from: 0000 */
    public void b(int i2) {
        l.a().a(this.c, i2);
    }

    public boolean b() {
        return l.a().e(this.c);
    }

    /* access modifiers changed from: 0000 */
    public final void c() {
        if (this.f356b.getParent() == null) {
            LayoutParams layoutParams = this.f356b.getLayoutParams();
            if (layoutParams instanceof android.support.design.widget.CoordinatorLayout.d) {
                android.support.design.widget.CoordinatorLayout.d dVar = (android.support.design.widget.CoordinatorLayout.d) layoutParams;
                C0010b bVar = new C0010b();
                bVar.a(0.1f);
                bVar.b(0.6f);
                bVar.a(0);
                bVar.a((android.support.design.widget.SwipeDismissBehavior.a) new android.support.design.widget.SwipeDismissBehavior.a() {
                    public void a(int i) {
                        switch (i) {
                            case 0:
                                l.a().d(b.this.c);
                                return;
                            case 1:
                            case 2:
                                l.a().c(b.this.c);
                                return;
                            default:
                                return;
                        }
                    }

                    public void a(View view) {
                        view.setVisibility(8);
                        b.this.b(0);
                    }
                });
                dVar.a((android.support.design.widget.CoordinatorLayout.a) bVar);
                dVar.g = 80;
            }
            this.e.addView(this.f356b);
        }
        this.f356b.setOnAttachStateChangeListener(new d() {
            public void a(View view) {
            }

            public void b(View view) {
                if (b.this.b()) {
                    b.f355a.post(new Runnable() {
                        public void run() {
                            b.this.d(3);
                        }
                    });
                }
            }
        });
        if (!t.v(this.f356b)) {
            this.f356b.setOnLayoutChangeListener(new e() {
                public void a(View view, int i, int i2, int i3, int i4) {
                    b.this.f356b.setOnLayoutChangeListener(null);
                    if (b.this.f()) {
                        b.this.d();
                    } else {
                        b.this.e();
                    }
                }
            });
        } else if (f()) {
            d();
        } else {
            e();
        }
    }

    /* access modifiers changed from: 0000 */
    public final void c(int i2) {
        if (!f() || this.f356b.getVisibility() != 0) {
            d(i2);
        } else {
            e(i2);
        }
    }

    /* access modifiers changed from: 0000 */
    public void d() {
        if (VERSION.SDK_INT >= 12) {
            final int height = this.f356b.getHeight();
            if (d) {
                t.c(this.f356b, height);
            } else {
                this.f356b.setTranslationY((float) height);
            }
            ValueAnimator valueAnimator = new ValueAnimator();
            valueAnimator.setIntValues(new int[]{height, 0});
            valueAnimator.setInterpolator(a.f354b);
            valueAnimator.setDuration(250);
            valueAnimator.addListener(new AnimatorListenerAdapter() {
                public void onAnimationEnd(Animator animator) {
                    b.this.e();
                }

                public void onAnimationStart(Animator animator) {
                    b.this.g.a(70, 180);
                }
            });
            valueAnimator.addUpdateListener(new AnimatorUpdateListener() {
                private int c = height;

                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    int intValue = ((Integer) valueAnimator.getAnimatedValue()).intValue();
                    if (b.d) {
                        t.c(b.this.f356b, intValue - this.c);
                    } else {
                        b.this.f356b.setTranslationY((float) intValue);
                    }
                    this.c = intValue;
                }
            });
            valueAnimator.start();
            return;
        }
        Animation loadAnimation = AnimationUtils.loadAnimation(this.f356b.getContext(), C0009a.design_snackbar_in);
        loadAnimation.setInterpolator(a.f354b);
        loadAnimation.setDuration(250);
        loadAnimation.setAnimationListener(new AnimationListener() {
            public void onAnimationEnd(Animation animation) {
                b.this.e();
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationStart(Animation animation) {
            }
        });
        this.f356b.startAnimation(loadAnimation);
    }

    /* access modifiers changed from: 0000 */
    public void d(int i2) {
        l.a().a(this.c);
        if (this.i != null) {
            for (int size = this.i.size() - 1; size >= 0; size--) {
                ((a) this.i.get(size)).a(this, i2);
            }
        }
        if (VERSION.SDK_INT < 11) {
            this.f356b.setVisibility(8);
        }
        ViewParent parent = this.f356b.getParent();
        if (parent instanceof ViewGroup) {
            ((ViewGroup) parent).removeView(this.f356b);
        }
    }

    /* access modifiers changed from: 0000 */
    public void e() {
        l.a().b(this.c);
        if (this.i != null) {
            for (int size = this.i.size() - 1; size >= 0; size--) {
                ((a) this.i.get(size)).a(this);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean f() {
        return !this.j.isEnabled();
    }
}
