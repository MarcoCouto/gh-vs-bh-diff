package android.support.design.widget;

import android.support.v4.i.b.b;
import android.support.v4.i.b.c;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;

class a {

    /* renamed from: a reason: collision with root package name */
    static final Interpolator f353a = new LinearInterpolator();

    /* renamed from: b reason: collision with root package name */
    static final Interpolator f354b = new b();
    static final Interpolator c = new android.support.v4.i.b.a();
    static final Interpolator d = new c();
    static final Interpolator e = new DecelerateInterpolator();

    static int a(int i, int i2, float f) {
        return Math.round(((float) (i2 - i)) * f) + i;
    }
}
