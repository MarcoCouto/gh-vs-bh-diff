package android.support.design.widget;

import android.support.v4.h.k.a;
import android.support.v4.h.k.b;
import android.support.v4.h.m;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

final class e<T> {

    /* renamed from: a reason: collision with root package name */
    private final a<ArrayList<T>> f378a = new b(10);

    /* renamed from: b reason: collision with root package name */
    private final m<T, ArrayList<T>> f379b = new m<>();
    private final ArrayList<T> c = new ArrayList<>();
    private final HashSet<T> d = new HashSet<>();

    e() {
    }

    private void a(T t, ArrayList<T> arrayList, HashSet<T> hashSet) {
        if (!arrayList.contains(t)) {
            if (hashSet.contains(t)) {
                throw new RuntimeException("This graph contains cyclic dependencies");
            }
            hashSet.add(t);
            ArrayList arrayList2 = (ArrayList) this.f379b.get(t);
            if (arrayList2 != null) {
                int size = arrayList2.size();
                for (int i = 0; i < size; i++) {
                    a(arrayList2.get(i), arrayList, hashSet);
                }
            }
            hashSet.remove(t);
            arrayList.add(t);
        }
    }

    private void a(ArrayList<T> arrayList) {
        arrayList.clear();
        this.f378a.a(arrayList);
    }

    private ArrayList<T> c() {
        ArrayList<T> arrayList = (ArrayList) this.f378a.a();
        return arrayList == null ? new ArrayList<>() : arrayList;
    }

    /* access modifiers changed from: 0000 */
    public void a() {
        int size = this.f379b.size();
        for (int i = 0; i < size; i++) {
            ArrayList arrayList = (ArrayList) this.f379b.c(i);
            if (arrayList != null) {
                a(arrayList);
            }
        }
        this.f379b.clear();
    }

    /* access modifiers changed from: 0000 */
    public void a(T t) {
        if (!this.f379b.containsKey(t)) {
            this.f379b.put(t, null);
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(T t, T t2) {
        if (!this.f379b.containsKey(t) || !this.f379b.containsKey(t2)) {
            throw new IllegalArgumentException("All nodes must be present in the graph before being added as an edge");
        }
        ArrayList arrayList = (ArrayList) this.f379b.get(t);
        if (arrayList == null) {
            arrayList = c();
            this.f379b.put(t, arrayList);
        }
        arrayList.add(t2);
    }

    /* access modifiers changed from: 0000 */
    public ArrayList<T> b() {
        this.c.clear();
        this.d.clear();
        int size = this.f379b.size();
        for (int i = 0; i < size; i++) {
            a(this.f379b.b(i), this.c, this.d);
        }
        return this.c;
    }

    /* access modifiers changed from: 0000 */
    public boolean b(T t) {
        return this.f379b.containsKey(t);
    }

    /* access modifiers changed from: 0000 */
    public List c(T t) {
        return (List) this.f379b.get(t);
    }

    /* access modifiers changed from: 0000 */
    public List<T> d(T t) {
        ArrayList arrayList = null;
        int size = this.f379b.size();
        for (int i = 0; i < size; i++) {
            ArrayList arrayList2 = (ArrayList) this.f379b.c(i);
            if (arrayList2 != null && arrayList2.contains(t)) {
                ArrayList arrayList3 = arrayList == null ? new ArrayList() : arrayList;
                arrayList3.add(this.f379b.b(i));
                arrayList = arrayList3;
            }
        }
        return arrayList;
    }

    /* access modifiers changed from: 0000 */
    public boolean e(T t) {
        int size = this.f379b.size();
        for (int i = 0; i < size; i++) {
            ArrayList arrayList = (ArrayList) this.f379b.c(i);
            if (arrayList != null && arrayList.contains(t)) {
                return true;
            }
        }
        return false;
    }
}
