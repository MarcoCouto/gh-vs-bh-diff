package android.support.design.widget;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.support.design.a.i;
import android.support.design.a.j;
import android.support.v4.i.q;
import android.support.v4.i.r;
import android.support.v4.i.t;
import android.support.v4.i.w;
import android.support.v4.widget.n;
import android.support.v7.widget.bq;
import android.text.Layout;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import com.hmatalonga.greenhub.Config;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;

@android.support.v4.i.w.a
public class o extends HorizontalScrollView {
    private static final android.support.v4.h.k.a<e> n = new android.support.v4.h.k.c(16);
    private DataSetObserver A;
    private f B;
    private a C;
    private boolean D;
    private final android.support.v4.h.k.a<g> E;

    /* renamed from: a reason: collision with root package name */
    int f412a;

    /* renamed from: b reason: collision with root package name */
    int f413b;
    int c;
    int d;
    int e;
    ColorStateList f;
    float g;
    float h;
    final int i;
    int j;
    int k;
    int l;
    w m;
    private final ArrayList<e> o;
    private e p;
    private final d q;
    private final int r;
    private final int s;
    private final int t;
    private int u;
    private b v;
    private final ArrayList<b> w;
    private b x;
    private ValueAnimator y;
    private q z;

    private class a implements android.support.v4.i.w.e {

        /* renamed from: b reason: collision with root package name */
        private boolean f416b;

        a() {
        }

        public void a(w wVar, q qVar, q qVar2) {
            if (o.this.m == wVar) {
                o.this.a(qVar2, this.f416b);
            }
        }

        /* access modifiers changed from: 0000 */
        public void a(boolean z) {
            this.f416b = z;
        }
    }

    public interface b {
        void a(e eVar);

        void b(e eVar);

        void c(e eVar);
    }

    private class c extends DataSetObserver {
        c() {
        }

        public void onChanged() {
            o.this.c();
        }

        public void onInvalidated() {
            o.this.c();
        }
    }

    private class d extends LinearLayout {

        /* renamed from: a reason: collision with root package name */
        int f418a = -1;

        /* renamed from: b reason: collision with root package name */
        float f419b;
        private int d;
        private final Paint e;
        private int f = -1;
        private int g = -1;
        private int h = -1;
        private ValueAnimator i;

        d(Context context) {
            super(context);
            setWillNotDraw(false);
            this.e = new Paint();
        }

        private void c() {
            int i2;
            int i3;
            View childAt = getChildAt(this.f418a);
            if (childAt == null || childAt.getWidth() <= 0) {
                i2 = -1;
                i3 = -1;
            } else {
                i3 = childAt.getLeft();
                i2 = childAt.getRight();
                if (this.f419b > 0.0f && this.f418a < getChildCount() - 1) {
                    View childAt2 = getChildAt(this.f418a + 1);
                    i3 = (int) ((((float) i3) * (1.0f - this.f419b)) + (this.f419b * ((float) childAt2.getLeft())));
                    i2 = (int) ((((float) i2) * (1.0f - this.f419b)) + (((float) childAt2.getRight()) * this.f419b));
                }
            }
            a(i3, i2);
        }

        /* access modifiers changed from: 0000 */
        public void a(int i2) {
            if (this.e.getColor() != i2) {
                this.e.setColor(i2);
                t.c(this);
            }
        }

        /* access modifiers changed from: 0000 */
        public void a(int i2, float f2) {
            if (this.i != null && this.i.isRunning()) {
                this.i.cancel();
            }
            this.f418a = i2;
            this.f419b = f2;
            c();
        }

        /* access modifiers changed from: 0000 */
        public void a(int i2, int i3) {
            if (i2 != this.g || i3 != this.h) {
                this.g = i2;
                this.h = i3;
                t.c(this);
            }
        }

        /* access modifiers changed from: 0000 */
        public boolean a() {
            int childCount = getChildCount();
            for (int i2 = 0; i2 < childCount; i2++) {
                if (getChildAt(i2).getWidth() <= 0) {
                    return true;
                }
            }
            return false;
        }

        /* access modifiers changed from: 0000 */
        public float b() {
            return ((float) this.f418a) + this.f419b;
        }

        /* access modifiers changed from: 0000 */
        public void b(int i2) {
            if (this.d != i2) {
                this.d = i2;
                t.c(this);
            }
        }

        /* access modifiers changed from: 0000 */
        public void b(final int i2, int i3) {
            final int i4;
            final int i5;
            if (this.i != null && this.i.isRunning()) {
                this.i.cancel();
            }
            boolean z = t.e(this) == 1;
            View childAt = getChildAt(i2);
            if (childAt == null) {
                c();
                return;
            }
            final int left = childAt.getLeft();
            final int right = childAt.getRight();
            if (Math.abs(i2 - this.f418a) <= 1) {
                i5 = this.g;
                i4 = this.h;
            } else {
                int b2 = o.this.b(24);
                if (i2 < this.f418a) {
                    if (z) {
                        i4 = left - b2;
                        i5 = i4;
                    } else {
                        i4 = right + b2;
                        i5 = i4;
                    }
                } else if (z) {
                    i4 = right + b2;
                    i5 = i4;
                } else {
                    i4 = left - b2;
                    i5 = i4;
                }
            }
            if (i5 != left || i4 != right) {
                ValueAnimator valueAnimator = new ValueAnimator();
                this.i = valueAnimator;
                valueAnimator.setInterpolator(a.f354b);
                valueAnimator.setDuration((long) i3);
                valueAnimator.setFloatValues(new float[]{0.0f, 1.0f});
                valueAnimator.addUpdateListener(new AnimatorUpdateListener() {
                    public void onAnimationUpdate(ValueAnimator valueAnimator) {
                        float animatedFraction = valueAnimator.getAnimatedFraction();
                        d.this.a(a.a(i5, left, animatedFraction), a.a(i4, right, animatedFraction));
                    }
                });
                valueAnimator.addListener(new AnimatorListenerAdapter() {
                    public void onAnimationEnd(Animator animator) {
                        d.this.f418a = i2;
                        d.this.f419b = 0.0f;
                    }
                });
                valueAnimator.start();
            }
        }

        public void draw(Canvas canvas) {
            super.draw(canvas);
            if (this.g >= 0 && this.h > this.g) {
                canvas.drawRect((float) this.g, (float) (getHeight() - this.d), (float) this.h, (float) getHeight(), this.e);
            }
        }

        /* access modifiers changed from: protected */
        public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
            super.onLayout(z, i2, i3, i4, i5);
            if (this.i == null || !this.i.isRunning()) {
                c();
                return;
            }
            this.i.cancel();
            b(this.f418a, Math.round(((float) this.i.getDuration()) * (1.0f - this.i.getAnimatedFraction())));
        }

        /* access modifiers changed from: protected */
        public void onMeasure(int i2, int i3) {
            boolean z;
            boolean z2 = false;
            super.onMeasure(i2, i3);
            if (MeasureSpec.getMode(i2) == 1073741824 && o.this.l == 1 && o.this.k == 1) {
                int childCount = getChildCount();
                int i4 = 0;
                int i5 = 0;
                while (i4 < childCount) {
                    View childAt = getChildAt(i4);
                    i4++;
                    i5 = childAt.getVisibility() == 0 ? Math.max(i5, childAt.getMeasuredWidth()) : i5;
                }
                if (i5 > 0) {
                    if (i5 * childCount <= getMeasuredWidth() - (o.this.b(16) * 2)) {
                        int i6 = 0;
                        while (i6 < childCount) {
                            LayoutParams layoutParams = (LayoutParams) getChildAt(i6).getLayoutParams();
                            if (layoutParams.width == i5 && layoutParams.weight == 0.0f) {
                                z = z2;
                            } else {
                                layoutParams.width = i5;
                                layoutParams.weight = 0.0f;
                                z = true;
                            }
                            i6++;
                            z2 = z;
                        }
                    } else {
                        o.this.k = 0;
                        o.this.a(false);
                        z2 = true;
                    }
                    if (z2) {
                        super.onMeasure(i2, i3);
                    }
                }
            }
        }

        public void onRtlPropertiesChanged(int i2) {
            super.onRtlPropertiesChanged(i2);
            if (VERSION.SDK_INT < 23 && this.f != i2) {
                requestLayout();
                this.f = i2;
            }
        }
    }

    public static final class e {

        /* renamed from: a reason: collision with root package name */
        o f424a;

        /* renamed from: b reason: collision with root package name */
        g f425b;
        private Object c;
        private Drawable d;
        private CharSequence e;
        private CharSequence f;
        private int g = -1;
        private View h;

        e() {
        }

        public e a(int i) {
            return a(LayoutInflater.from(this.f425b.getContext()).inflate(i, this.f425b, false));
        }

        public e a(Drawable drawable) {
            this.d = drawable;
            h();
            return this;
        }

        public e a(View view) {
            this.h = view;
            h();
            return this;
        }

        public e a(CharSequence charSequence) {
            this.e = charSequence;
            h();
            return this;
        }

        public View a() {
            return this.h;
        }

        public Drawable b() {
            return this.d;
        }

        public e b(CharSequence charSequence) {
            this.f = charSequence;
            h();
            return this;
        }

        /* access modifiers changed from: 0000 */
        public void b(int i) {
            this.g = i;
        }

        public int c() {
            return this.g;
        }

        public e c(int i) {
            if (this.f424a != null) {
                return a(android.support.v7.c.a.b.b(this.f424a.getContext(), i));
            }
            throw new IllegalArgumentException("Tab not attached to a TabLayout");
        }

        public e d(int i) {
            if (this.f424a != null) {
                return b(this.f424a.getResources().getText(i));
            }
            throw new IllegalArgumentException("Tab not attached to a TabLayout");
        }

        public CharSequence d() {
            return this.e;
        }

        public void e() {
            if (this.f424a == null) {
                throw new IllegalArgumentException("Tab not attached to a TabLayout");
            }
            this.f424a.b(this);
        }

        public boolean f() {
            if (this.f424a != null) {
                return this.f424a.getSelectedTabPosition() == this.g;
            }
            throw new IllegalArgumentException("Tab not attached to a TabLayout");
        }

        public CharSequence g() {
            return this.f;
        }

        /* access modifiers changed from: 0000 */
        public void h() {
            if (this.f425b != null) {
                this.f425b.b();
            }
        }

        /* access modifiers changed from: 0000 */
        public void i() {
            this.f424a = null;
            this.f425b = null;
            this.c = null;
            this.d = null;
            this.e = null;
            this.f = null;
            this.g = -1;
            this.h = null;
        }
    }

    public static class f implements android.support.v4.i.w.f {

        /* renamed from: a reason: collision with root package name */
        private final WeakReference<o> f426a;

        /* renamed from: b reason: collision with root package name */
        private int f427b;
        private int c;

        public f(o oVar) {
            this.f426a = new WeakReference<>(oVar);
        }

        /* access modifiers changed from: 0000 */
        public void a() {
            this.c = 0;
            this.f427b = 0;
        }

        public void a(int i) {
            this.f427b = this.c;
            this.c = i;
        }

        public void a(int i, float f, int i2) {
            boolean z = false;
            o oVar = (o) this.f426a.get();
            if (oVar != null) {
                boolean z2 = this.c != 2 || this.f427b == 1;
                if (!(this.c == 2 && this.f427b == 0)) {
                    z = true;
                }
                oVar.a(i, f, z2, z);
            }
        }

        public void b(int i) {
            o oVar = (o) this.f426a.get();
            if (oVar != null && oVar.getSelectedTabPosition() != i && i < oVar.getTabCount()) {
                oVar.b(oVar.a(i), this.c == 0 || (this.c == 2 && this.f427b == 0));
            }
        }
    }

    class g extends LinearLayout {

        /* renamed from: b reason: collision with root package name */
        private e f429b;
        private TextView c;
        private ImageView d;
        private View e;
        private TextView f;
        private ImageView g;
        private int h = 2;

        public g(Context context) {
            super(context);
            if (o.this.i != 0) {
                t.a((View) this, android.support.v7.c.a.b.b(context, o.this.i));
            }
            t.a(this, o.this.f412a, o.this.f413b, o.this.c, o.this.d);
            setGravity(17);
            setOrientation(1);
            setClickable(true);
            t.a((View) this, r.a(getContext(), Config.NOTIFICATION_BATTERY_FULL));
        }

        private float a(Layout layout, int i, float f2) {
            return layout.getLineWidth(i) * (f2 / layout.getPaint().getTextSize());
        }

        private void a(TextView textView, ImageView imageView) {
            CharSequence charSequence = null;
            Drawable drawable = this.f429b != null ? this.f429b.b() : null;
            CharSequence charSequence2 = this.f429b != null ? this.f429b.d() : null;
            CharSequence charSequence3 = this.f429b != null ? this.f429b.g() : null;
            if (imageView != null) {
                if (drawable != null) {
                    imageView.setImageDrawable(drawable);
                    imageView.setVisibility(0);
                    setVisibility(0);
                } else {
                    imageView.setVisibility(8);
                    imageView.setImageDrawable(null);
                }
                imageView.setContentDescription(charSequence3);
            }
            boolean z = !TextUtils.isEmpty(charSequence2);
            if (textView != null) {
                if (z) {
                    textView.setText(charSequence2);
                    textView.setVisibility(0);
                    setVisibility(0);
                } else {
                    textView.setVisibility(8);
                    textView.setText(null);
                }
                textView.setContentDescription(charSequence3);
            }
            if (imageView != null) {
                MarginLayoutParams marginLayoutParams = (MarginLayoutParams) imageView.getLayoutParams();
                int i = (!z || imageView.getVisibility() != 0) ? 0 : o.this.b(8);
                if (i != marginLayoutParams.bottomMargin) {
                    marginLayoutParams.bottomMargin = i;
                    imageView.requestLayout();
                }
            }
            if (!z) {
                charSequence = charSequence3;
            }
            bq.a(this, charSequence);
        }

        /* access modifiers changed from: 0000 */
        public void a() {
            a(null);
            setSelected(false);
        }

        /* access modifiers changed from: 0000 */
        public void a(e eVar) {
            if (eVar != this.f429b) {
                this.f429b = eVar;
                b();
            }
        }

        /* access modifiers changed from: 0000 */
        public final void b() {
            e eVar = this.f429b;
            View view = eVar != null ? eVar.a() : null;
            if (view != null) {
                ViewParent parent = view.getParent();
                if (parent != this) {
                    if (parent != null) {
                        ((ViewGroup) parent).removeView(view);
                    }
                    addView(view);
                }
                this.e = view;
                if (this.c != null) {
                    this.c.setVisibility(8);
                }
                if (this.d != null) {
                    this.d.setVisibility(8);
                    this.d.setImageDrawable(null);
                }
                this.f = (TextView) view.findViewById(16908308);
                if (this.f != null) {
                    this.h = n.a(this.f);
                }
                this.g = (ImageView) view.findViewById(16908294);
            } else {
                if (this.e != null) {
                    removeView(this.e);
                    this.e = null;
                }
                this.f = null;
                this.g = null;
            }
            if (this.e == null) {
                if (this.d == null) {
                    ImageView imageView = (ImageView) LayoutInflater.from(getContext()).inflate(android.support.design.a.h.design_layout_tab_icon, this, false);
                    addView(imageView, 0);
                    this.d = imageView;
                }
                if (this.c == null) {
                    TextView textView = (TextView) LayoutInflater.from(getContext()).inflate(android.support.design.a.h.design_layout_tab_text, this, false);
                    addView(textView);
                    this.c = textView;
                    this.h = n.a(this.c);
                }
                n.a(this.c, o.this.e);
                if (o.this.f != null) {
                    this.c.setTextColor(o.this.f);
                }
                a(this.c, this.d);
            } else if (!(this.f == null && this.g == null)) {
                a(this.f, this.g);
            }
            setSelected(eVar != null && eVar.f());
        }

        public void onInitializeAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
            super.onInitializeAccessibilityEvent(accessibilityEvent);
            accessibilityEvent.setClassName(android.support.v7.app.a.c.class.getName());
        }

        public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo accessibilityNodeInfo) {
            super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo);
            accessibilityNodeInfo.setClassName(android.support.v7.app.a.c.class.getName());
        }

        public void onMeasure(int i, int i2) {
            boolean z = true;
            int size = MeasureSpec.getSize(i);
            int mode = MeasureSpec.getMode(i);
            int tabMaxWidth = o.this.getTabMaxWidth();
            if (tabMaxWidth > 0 && (mode == 0 || size > tabMaxWidth)) {
                i = MeasureSpec.makeMeasureSpec(o.this.j, Integer.MIN_VALUE);
            }
            super.onMeasure(i, i2);
            if (this.c != null) {
                getResources();
                float f2 = o.this.g;
                int i3 = this.h;
                if (this.d != null && this.d.getVisibility() == 0) {
                    i3 = 1;
                } else if (this.c != null && this.c.getLineCount() > 1) {
                    f2 = o.this.h;
                }
                float textSize = this.c.getTextSize();
                int lineCount = this.c.getLineCount();
                int a2 = n.a(this.c);
                if (f2 != textSize || (a2 >= 0 && i3 != a2)) {
                    if (o.this.l == 1 && f2 > textSize && lineCount == 1) {
                        Layout layout = this.c.getLayout();
                        if (layout == null || a(layout, 0, f2) > ((float) ((getMeasuredWidth() - getPaddingLeft()) - getPaddingRight()))) {
                            z = false;
                        }
                    }
                    if (z) {
                        this.c.setTextSize(0, f2);
                        this.c.setMaxLines(i3);
                        super.onMeasure(i, i2);
                    }
                }
            }
        }

        public boolean performClick() {
            boolean performClick = super.performClick();
            if (this.f429b == null) {
                return performClick;
            }
            if (!performClick) {
                playSoundEffect(0);
            }
            this.f429b.e();
            return true;
        }

        public void setSelected(boolean z) {
            boolean z2 = isSelected() != z;
            super.setSelected(z);
            if (z2 && z && VERSION.SDK_INT < 16) {
                sendAccessibilityEvent(4);
            }
            if (this.c != null) {
                this.c.setSelected(z);
            }
            if (this.d != null) {
                this.d.setSelected(z);
            }
            if (this.e != null) {
                this.e.setSelected(z);
            }
        }
    }

    public static class h implements b {

        /* renamed from: a reason: collision with root package name */
        private final w f430a;

        public h(w wVar) {
            this.f430a = wVar;
        }

        public void a(e eVar) {
            this.f430a.setCurrentItem(eVar.c());
        }

        public void b(e eVar) {
        }

        public void c(e eVar) {
        }
    }

    public o(Context context) {
        this(context, null);
    }

    public o(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    /* JADX INFO: finally extract failed */
    public o(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.o = new ArrayList<>();
        this.j = Integer.MAX_VALUE;
        this.w = new ArrayList<>();
        this.E = new android.support.v4.h.k.b(12);
        p.a(context);
        setHorizontalScrollBarEnabled(false);
        this.q = new d(context);
        super.addView(this.q, 0, new FrameLayout.LayoutParams(-2, -1));
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, j.TabLayout, i2, i.Widget_Design_TabLayout);
        this.q.b(obtainStyledAttributes.getDimensionPixelSize(j.TabLayout_tabIndicatorHeight, 0));
        this.q.a(obtainStyledAttributes.getColor(j.TabLayout_tabIndicatorColor, 0));
        int dimensionPixelSize = obtainStyledAttributes.getDimensionPixelSize(j.TabLayout_tabPadding, 0);
        this.d = dimensionPixelSize;
        this.c = dimensionPixelSize;
        this.f413b = dimensionPixelSize;
        this.f412a = dimensionPixelSize;
        this.f412a = obtainStyledAttributes.getDimensionPixelSize(j.TabLayout_tabPaddingStart, this.f412a);
        this.f413b = obtainStyledAttributes.getDimensionPixelSize(j.TabLayout_tabPaddingTop, this.f413b);
        this.c = obtainStyledAttributes.getDimensionPixelSize(j.TabLayout_tabPaddingEnd, this.c);
        this.d = obtainStyledAttributes.getDimensionPixelSize(j.TabLayout_tabPaddingBottom, this.d);
        this.e = obtainStyledAttributes.getResourceId(j.TabLayout_tabTextAppearance, i.TextAppearance_Design_Tab);
        TypedArray obtainStyledAttributes2 = context.obtainStyledAttributes(this.e, android.support.v7.a.a.j.TextAppearance);
        try {
            this.g = (float) obtainStyledAttributes2.getDimensionPixelSize(android.support.v7.a.a.j.TextAppearance_android_textSize, 0);
            this.f = obtainStyledAttributes2.getColorStateList(android.support.v7.a.a.j.TextAppearance_android_textColor);
            obtainStyledAttributes2.recycle();
            if (obtainStyledAttributes.hasValue(j.TabLayout_tabTextColor)) {
                this.f = obtainStyledAttributes.getColorStateList(j.TabLayout_tabTextColor);
            }
            if (obtainStyledAttributes.hasValue(j.TabLayout_tabSelectedTextColor)) {
                this.f = a(this.f.getDefaultColor(), obtainStyledAttributes.getColor(j.TabLayout_tabSelectedTextColor, 0));
            }
            this.r = obtainStyledAttributes.getDimensionPixelSize(j.TabLayout_tabMinWidth, -1);
            this.s = obtainStyledAttributes.getDimensionPixelSize(j.TabLayout_tabMaxWidth, -1);
            this.i = obtainStyledAttributes.getResourceId(j.TabLayout_tabBackground, 0);
            this.u = obtainStyledAttributes.getDimensionPixelSize(j.TabLayout_tabContentStart, 0);
            this.l = obtainStyledAttributes.getInt(j.TabLayout_tabMode, 1);
            this.k = obtainStyledAttributes.getInt(j.TabLayout_tabGravity, 0);
            obtainStyledAttributes.recycle();
            Resources resources = getResources();
            this.h = (float) resources.getDimensionPixelSize(android.support.design.a.d.design_tab_text_size_2line);
            this.t = resources.getDimensionPixelSize(android.support.design.a.d.design_tab_scrollable_min_width);
            g();
        } catch (Throwable th) {
            obtainStyledAttributes2.recycle();
            throw th;
        }
    }

    private int a(int i2, float f2) {
        int i3 = 0;
        if (this.l != 0) {
            return 0;
        }
        View childAt = this.q.getChildAt(i2);
        View view = i2 + 1 < this.q.getChildCount() ? this.q.getChildAt(i2 + 1) : null;
        int i4 = childAt != null ? childAt.getWidth() : 0;
        if (view != null) {
            i3 = view.getWidth();
        }
        int left = (childAt.getLeft() + (i4 / 2)) - (getWidth() / 2);
        int i5 = (int) (((float) (i3 + i4)) * 0.5f * f2);
        return t.e(this) == 0 ? i5 + left : left - i5;
    }

    private static ColorStateList a(int i2, int i3) {
        return new ColorStateList(new int[][]{SELECTED_STATE_SET, EMPTY_STATE_SET}, new int[]{i3, i2});
    }

    private void a(n nVar) {
        e a2 = a();
        if (nVar.f410a != null) {
            a2.a(nVar.f410a);
        }
        if (nVar.f411b != null) {
            a2.a(nVar.f411b);
        }
        if (nVar.c != 0) {
            a2.a(nVar.c);
        }
        if (!TextUtils.isEmpty(nVar.getContentDescription())) {
            a2.b(nVar.getContentDescription());
        }
        a(a2);
    }

    private void a(e eVar, int i2) {
        eVar.b(i2);
        this.o.add(i2, eVar);
        int size = this.o.size();
        for (int i3 = i2 + 1; i3 < size; i3++) {
            ((e) this.o.get(i3)).b(i3);
        }
    }

    private void a(w wVar, boolean z2, boolean z3) {
        if (this.m != null) {
            if (this.B != null) {
                this.m.b((android.support.v4.i.w.f) this.B);
            }
            if (this.C != null) {
                this.m.b((android.support.v4.i.w.e) this.C);
            }
        }
        if (this.x != null) {
            b(this.x);
            this.x = null;
        }
        if (wVar != null) {
            this.m = wVar;
            if (this.B == null) {
                this.B = new f(this);
            }
            this.B.a();
            wVar.a((android.support.v4.i.w.f) this.B);
            this.x = new h(wVar);
            a(this.x);
            q adapter = wVar.getAdapter();
            if (adapter != null) {
                a(adapter, z2);
            }
            if (this.C == null) {
                this.C = new a();
            }
            this.C.a(z2);
            wVar.a((android.support.v4.i.w.e) this.C);
            a(wVar.getCurrentItem(), 0.0f, true);
        } else {
            this.m = null;
            a((q) null, false);
        }
        this.D = z3;
    }

    private void a(View view) {
        if (view instanceof n) {
            a((n) view);
            return;
        }
        throw new IllegalArgumentException("Only TabItem instances can be added to TabLayout");
    }

    private void a(LayoutParams layoutParams) {
        if (this.l == 1 && this.k == 0) {
            layoutParams.width = 0;
            layoutParams.weight = 1.0f;
            return;
        }
        layoutParams.width = -2;
        layoutParams.weight = 0.0f;
    }

    private g c(e eVar) {
        g gVar = this.E != null ? (g) this.E.a() : null;
        if (gVar == null) {
            gVar = new g(getContext());
        }
        gVar.a(eVar);
        gVar.setFocusable(true);
        gVar.setMinimumWidth(getTabMinWidth());
        return gVar;
    }

    private void c(int i2) {
        g gVar = (g) this.q.getChildAt(i2);
        this.q.removeViewAt(i2);
        if (gVar != null) {
            gVar.a();
            this.E.a(gVar);
        }
        requestLayout();
    }

    private void d() {
        int size = this.o.size();
        for (int i2 = 0; i2 < size; i2++) {
            ((e) this.o.get(i2)).h();
        }
    }

    private void d(int i2) {
        if (i2 != -1) {
            if (getWindowToken() == null || !t.v(this) || this.q.a()) {
                a(i2, 0.0f, true);
                return;
            }
            int scrollX = getScrollX();
            int a2 = a(i2, 0.0f);
            if (scrollX != a2) {
                f();
                this.y.setIntValues(new int[]{scrollX, a2});
                this.y.start();
            }
            this.q.b(i2, 300);
        }
    }

    private void d(e eVar) {
        this.q.addView(eVar.f425b, eVar.c(), e());
    }

    private LayoutParams e() {
        LayoutParams layoutParams = new LayoutParams(-2, -1);
        a(layoutParams);
        return layoutParams;
    }

    private void e(e eVar) {
        for (int size = this.w.size() - 1; size >= 0; size--) {
            ((b) this.w.get(size)).a(eVar);
        }
    }

    private void f() {
        if (this.y == null) {
            this.y = new ValueAnimator();
            this.y.setInterpolator(a.f354b);
            this.y.setDuration(300);
            this.y.addUpdateListener(new AnimatorUpdateListener() {
                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    o.this.scrollTo(((Integer) valueAnimator.getAnimatedValue()).intValue(), 0);
                }
            });
        }
    }

    private void f(e eVar) {
        for (int size = this.w.size() - 1; size >= 0; size--) {
            ((b) this.w.get(size)).b(eVar);
        }
    }

    private void g() {
        t.a(this.q, this.l == 0 ? Math.max(0, this.u - this.f412a) : 0, 0, 0, 0);
        switch (this.l) {
            case 0:
                this.q.setGravity(8388611);
                break;
            case 1:
                this.q.setGravity(1);
                break;
        }
        a(true);
    }

    private void g(e eVar) {
        for (int size = this.w.size() - 1; size >= 0; size--) {
            ((b) this.w.get(size)).c(eVar);
        }
    }

    private int getDefaultHeight() {
        boolean z2;
        int size = this.o.size();
        int i2 = 0;
        while (true) {
            if (i2 >= size) {
                z2 = false;
                break;
            }
            e eVar = (e) this.o.get(i2);
            if (eVar != null && eVar.b() != null && !TextUtils.isEmpty(eVar.d())) {
                z2 = true;
                break;
            }
            i2++;
        }
        return z2 ? 72 : 48;
    }

    private float getScrollPosition() {
        return this.q.b();
    }

    private int getTabMinWidth() {
        if (this.r != -1) {
            return this.r;
        }
        if (this.l == 0) {
            return this.t;
        }
        return 0;
    }

    private int getTabScrollRange() {
        return Math.max(0, ((this.q.getWidth() - getWidth()) - getPaddingLeft()) - getPaddingRight());
    }

    private void setSelectedTabView(int i2) {
        int childCount = this.q.getChildCount();
        if (i2 < childCount) {
            int i3 = 0;
            while (i3 < childCount) {
                this.q.getChildAt(i3).setSelected(i3 == i2);
                i3++;
            }
        }
    }

    public e a() {
        e eVar = (e) n.a();
        if (eVar == null) {
            eVar = new e();
        }
        eVar.f424a = this;
        eVar.f425b = c(eVar);
        return eVar;
    }

    public e a(int i2) {
        if (i2 < 0 || i2 >= getTabCount()) {
            return null;
        }
        return (e) this.o.get(i2);
    }

    public void a(int i2, float f2, boolean z2) {
        a(i2, f2, z2, true);
    }

    /* access modifiers changed from: 0000 */
    public void a(int i2, float f2, boolean z2, boolean z3) {
        int round = Math.round(((float) i2) + f2);
        if (round >= 0 && round < this.q.getChildCount()) {
            if (z3) {
                this.q.a(i2, f2);
            }
            if (this.y != null && this.y.isRunning()) {
                this.y.cancel();
            }
            scrollTo(a(i2, f2), 0);
            if (z2) {
                setSelectedTabView(round);
            }
        }
    }

    public void a(b bVar) {
        if (!this.w.contains(bVar)) {
            this.w.add(bVar);
        }
    }

    public void a(e eVar) {
        a(eVar, this.o.isEmpty());
    }

    public void a(e eVar, int i2, boolean z2) {
        if (eVar.f424a != this) {
            throw new IllegalArgumentException("Tab belongs to a different TabLayout.");
        }
        a(eVar, i2);
        d(eVar);
        if (z2) {
            eVar.e();
        }
    }

    public void a(e eVar, boolean z2) {
        a(eVar, this.o.size(), z2);
    }

    /* access modifiers changed from: 0000 */
    public void a(q qVar, boolean z2) {
        if (!(this.z == null || this.A == null)) {
            this.z.b(this.A);
        }
        this.z = qVar;
        if (z2 && qVar != null) {
            if (this.A == null) {
                this.A = new c();
            }
            qVar.a(this.A);
        }
        c();
    }

    public void a(w wVar, boolean z2) {
        a(wVar, z2, false);
    }

    /* access modifiers changed from: 0000 */
    public void a(boolean z2) {
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < this.q.getChildCount()) {
                View childAt = this.q.getChildAt(i3);
                childAt.setMinimumWidth(getTabMinWidth());
                a((LayoutParams) childAt.getLayoutParams());
                if (z2) {
                    childAt.requestLayout();
                }
                i2 = i3 + 1;
            } else {
                return;
            }
        }
    }

    public void addView(View view) {
        a(view);
    }

    public void addView(View view, int i2) {
        a(view);
    }

    public void addView(View view, int i2, ViewGroup.LayoutParams layoutParams) {
        a(view);
    }

    public void addView(View view, ViewGroup.LayoutParams layoutParams) {
        a(view);
    }

    /* access modifiers changed from: 0000 */
    public int b(int i2) {
        return Math.round(getResources().getDisplayMetrics().density * ((float) i2));
    }

    public void b() {
        for (int childCount = this.q.getChildCount() - 1; childCount >= 0; childCount--) {
            c(childCount);
        }
        Iterator it = this.o.iterator();
        while (it.hasNext()) {
            e eVar = (e) it.next();
            it.remove();
            eVar.i();
            n.a(eVar);
        }
        this.p = null;
    }

    public void b(b bVar) {
        this.w.remove(bVar);
    }

    /* access modifiers changed from: 0000 */
    public void b(e eVar) {
        b(eVar, true);
    }

    /* access modifiers changed from: 0000 */
    public void b(e eVar, boolean z2) {
        e eVar2 = this.p;
        if (eVar2 != eVar) {
            int i2 = eVar != null ? eVar.c() : -1;
            if (z2) {
                if ((eVar2 == null || eVar2.c() == -1) && i2 != -1) {
                    a(i2, 0.0f, true);
                } else {
                    d(i2);
                }
                if (i2 != -1) {
                    setSelectedTabView(i2);
                }
            }
            if (eVar2 != null) {
                f(eVar2);
            }
            this.p = eVar;
            if (eVar != null) {
                e(eVar);
            }
        } else if (eVar2 != null) {
            g(eVar);
            d(eVar.c());
        }
    }

    /* access modifiers changed from: 0000 */
    public void c() {
        b();
        if (this.z != null) {
            int b2 = this.z.b();
            for (int i2 = 0; i2 < b2; i2++) {
                a(a().a(this.z.b(i2)), false);
            }
            if (this.m != null && b2 > 0) {
                int currentItem = this.m.getCurrentItem();
                if (currentItem != getSelectedTabPosition() && currentItem < getTabCount()) {
                    b(a(currentItem));
                }
            }
        }
    }

    public FrameLayout.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return generateDefaultLayoutParams();
    }

    public int getSelectedTabPosition() {
        if (this.p != null) {
            return this.p.c();
        }
        return -1;
    }

    public int getTabCount() {
        return this.o.size();
    }

    public int getTabGravity() {
        return this.k;
    }

    /* access modifiers changed from: 0000 */
    public int getTabMaxWidth() {
        return this.j;
    }

    public int getTabMode() {
        return this.l;
    }

    public ColorStateList getTabTextColors() {
        return this.f;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.m == null) {
            ViewParent parent = getParent();
            if (parent instanceof w) {
                a((w) parent, true, true);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.D) {
            setupWithViewPager(null);
            this.D = false;
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        boolean z2;
        boolean z3 = true;
        int b2 = b(getDefaultHeight()) + getPaddingTop() + getPaddingBottom();
        switch (MeasureSpec.getMode(i3)) {
            case Integer.MIN_VALUE:
                i3 = MeasureSpec.makeMeasureSpec(Math.min(b2, MeasureSpec.getSize(i3)), 1073741824);
                break;
            case 0:
                i3 = MeasureSpec.makeMeasureSpec(b2, 1073741824);
                break;
        }
        int size = MeasureSpec.getSize(i2);
        if (MeasureSpec.getMode(i2) != 0) {
            this.j = this.s > 0 ? this.s : size - b(56);
        }
        super.onMeasure(i2, i3);
        if (getChildCount() == 1) {
            View childAt = getChildAt(0);
            switch (this.l) {
                case 0:
                    if (childAt.getMeasuredWidth() >= getMeasuredWidth()) {
                        z2 = false;
                        break;
                    } else {
                        z2 = true;
                        break;
                    }
                case 1:
                    if (childAt.getMeasuredWidth() == getMeasuredWidth()) {
                        z3 = false;
                    }
                    z2 = z3;
                    break;
                default:
                    z2 = false;
                    break;
            }
            if (z2) {
                childAt.measure(MeasureSpec.makeMeasureSpec(getMeasuredWidth(), 1073741824), getChildMeasureSpec(i3, getPaddingTop() + getPaddingBottom(), childAt.getLayoutParams().height));
            }
        }
    }

    @Deprecated
    public void setOnTabSelectedListener(b bVar) {
        if (this.v != null) {
            b(this.v);
        }
        this.v = bVar;
        if (bVar != null) {
            a(bVar);
        }
    }

    /* access modifiers changed from: 0000 */
    public void setScrollAnimatorListener(AnimatorListener animatorListener) {
        f();
        this.y.addListener(animatorListener);
    }

    public void setSelectedTabIndicatorColor(int i2) {
        this.q.a(i2);
    }

    public void setSelectedTabIndicatorHeight(int i2) {
        this.q.b(i2);
    }

    public void setTabGravity(int i2) {
        if (this.k != i2) {
            this.k = i2;
            g();
        }
    }

    public void setTabMode(int i2) {
        if (i2 != this.l) {
            this.l = i2;
            g();
        }
    }

    public void setTabTextColors(ColorStateList colorStateList) {
        if (this.f != colorStateList) {
            this.f = colorStateList;
            d();
        }
    }

    @Deprecated
    public void setTabsFromPagerAdapter(q qVar) {
        a(qVar, false);
    }

    public void setupWithViewPager(w wVar) {
        a(wVar, true);
    }

    public boolean shouldDelayChildPressedState() {
        return getTabScrollRange() > 0;
    }
}
