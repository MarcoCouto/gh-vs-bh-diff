package android.support.design.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.a.a.C0026a;

class p {

    /* renamed from: a reason: collision with root package name */
    private static final int[] f431a = {C0026a.colorPrimary};

    static void a(Context context) {
        boolean z = false;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(f431a);
        if (!obtainStyledAttributes.hasValue(0)) {
            z = true;
        }
        obtainStyledAttributes.recycle();
        if (z) {
            throw new IllegalArgumentException("You need to use a Theme.AppCompat theme (or descendant) with the design library.");
        }
    }
}
