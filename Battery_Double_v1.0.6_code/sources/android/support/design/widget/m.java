package android.support.design.widget;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.util.StateSet;
import java.util.ArrayList;

final class m {

    /* renamed from: a reason: collision with root package name */
    ValueAnimator f405a = null;

    /* renamed from: b reason: collision with root package name */
    private final ArrayList<a> f406b = new ArrayList<>();
    private a c = null;
    private final AnimatorListener d = new AnimatorListenerAdapter() {
        public void onAnimationEnd(Animator animator) {
            if (m.this.f405a == animator) {
                m.this.f405a = null;
            }
        }
    };

    static class a {

        /* renamed from: a reason: collision with root package name */
        final int[] f408a;

        /* renamed from: b reason: collision with root package name */
        final ValueAnimator f409b;

        a(int[] iArr, ValueAnimator valueAnimator) {
            this.f408a = iArr;
            this.f409b = valueAnimator;
        }
    }

    m() {
    }

    private void a(a aVar) {
        this.f405a = aVar.f409b;
        this.f405a.start();
    }

    private void b() {
        if (this.f405a != null) {
            this.f405a.cancel();
            this.f405a = null;
        }
    }

    public void a() {
        if (this.f405a != null) {
            this.f405a.end();
            this.f405a = null;
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(int[] iArr) {
        a aVar;
        int size = this.f406b.size();
        int i = 0;
        while (true) {
            if (i >= size) {
                aVar = null;
                break;
            }
            aVar = (a) this.f406b.get(i);
            if (StateSet.stateSetMatches(aVar.f408a, iArr)) {
                break;
            }
            i++;
        }
        if (aVar != this.c) {
            if (this.c != null) {
                b();
            }
            this.c = aVar;
            if (aVar != null) {
                a(aVar);
            }
        }
    }

    public void a(int[] iArr, ValueAnimator valueAnimator) {
        a aVar = new a(iArr, valueAnimator);
        valueAnimator.addListener(this.d);
        this.f406b.add(aVar);
    }
}
