package android.support.design.widget;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff.Mode;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Build.VERSION;
import android.support.v4.i.t;
import android.view.ViewTreeObserver.OnPreDrawListener;
import android.view.animation.Interpolator;

class f {

    /* renamed from: a reason: collision with root package name */
    static final Interpolator f380a = a.c;
    static final int[] j = {16842919, 16842910};
    static final int[] k = {16842908, 16842910};
    static final int[] l = {16842910};
    static final int[] m = new int[0];

    /* renamed from: b reason: collision with root package name */
    int f381b = 0;
    j c;
    Drawable d;
    Drawable e;
    c f;
    Drawable g;
    float h;
    float i;
    final v n;
    final k o;
    private final m p;
    private float q;
    private final Rect r = new Rect();
    private OnPreDrawListener s;

    private class a extends e {
        a() {
            super();
        }

        /* access modifiers changed from: protected */
        public float a() {
            return 0.0f;
        }
    }

    private class b extends e {
        b() {
            super();
        }

        /* access modifiers changed from: protected */
        public float a() {
            return f.this.h + f.this.i;
        }
    }

    interface c {
        void a();

        void b();
    }

    private class d extends e {
        d() {
            super();
        }

        /* access modifiers changed from: protected */
        public float a() {
            return f.this.h;
        }
    }

    private abstract class e extends AnimatorListenerAdapter implements AnimatorUpdateListener {

        /* renamed from: a reason: collision with root package name */
        private boolean f390a;
        private float c;
        private float d;

        private e() {
        }

        /* access modifiers changed from: protected */
        public abstract float a();

        public void onAnimationEnd(Animator animator) {
            f.this.c.b(this.d);
            this.f390a = false;
        }

        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            if (!this.f390a) {
                this.c = f.this.c.a();
                this.d = a();
                this.f390a = true;
            }
            f.this.c.b(this.c + ((this.d - this.c) * valueAnimator.getAnimatedFraction()));
        }
    }

    f(v vVar, k kVar) {
        this.n = vVar;
        this.o = kVar;
        this.p = new m();
        this.p.a(j, a((e) new b()));
        this.p.a(k, a((e) new b()));
        this.p.a(l, a((e) new d()));
        this.p.a(m, a((e) new a()));
        this.q = this.n.getRotation();
    }

    private ValueAnimator a(e eVar) {
        ValueAnimator valueAnimator = new ValueAnimator();
        valueAnimator.setInterpolator(f380a);
        valueAnimator.setDuration(100);
        valueAnimator.addListener(eVar);
        valueAnimator.addUpdateListener(eVar);
        valueAnimator.setFloatValues(new float[]{0.0f, 1.0f});
        return valueAnimator;
    }

    private static ColorStateList b(int i2) {
        return new ColorStateList(new int[][]{k, j, new int[0]}, new int[]{i2, i2, 0});
    }

    private void o() {
        if (this.s == null) {
            this.s = new OnPreDrawListener() {
                public boolean onPreDraw() {
                    f.this.j();
                    return true;
                }
            };
        }
    }

    private boolean p() {
        return t.v(this.n) && !this.n.isInEditMode();
    }

    private void q() {
        if (VERSION.SDK_INT == 19) {
            if (this.q % 90.0f != 0.0f) {
                if (this.n.getLayerType() != 1) {
                    this.n.setLayerType(1, null);
                }
            } else if (this.n.getLayerType() != 0) {
                this.n.setLayerType(0, null);
            }
        }
        if (this.c != null) {
            this.c.a(-this.q);
        }
        if (this.f != null) {
            this.f.b(-this.q);
        }
    }

    /* access modifiers changed from: 0000 */
    public float a() {
        return this.h;
    }

    /* access modifiers changed from: 0000 */
    public c a(int i2, ColorStateList colorStateList) {
        Context context = this.n.getContext();
        c i3 = i();
        i3.a(android.support.v4.b.a.c(context, android.support.design.a.c.design_fab_stroke_top_outer_color), android.support.v4.b.a.c(context, android.support.design.a.c.design_fab_stroke_top_inner_color), android.support.v4.b.a.c(context, android.support.design.a.c.design_fab_stroke_end_inner_color), android.support.v4.b.a.c(context, android.support.design.a.c.design_fab_stroke_end_outer_color));
        i3.a((float) i2);
        i3.a(colorStateList);
        return i3;
    }

    /* access modifiers changed from: 0000 */
    public final void a(float f2) {
        if (this.h != f2) {
            this.h = f2;
            a(f2, this.i);
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(float f2, float f3) {
        if (this.c != null) {
            this.c.a(f2, this.i + f2);
            e();
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(int i2) {
        if (this.e != null) {
            android.support.v4.c.a.a.a(this.e, b(i2));
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(ColorStateList colorStateList) {
        if (this.d != null) {
            android.support.v4.c.a.a.a(this.d, colorStateList);
        }
        if (this.f != null) {
            this.f.a(colorStateList);
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(ColorStateList colorStateList, Mode mode, int i2, int i3) {
        Drawable[] drawableArr;
        this.d = android.support.v4.c.a.a.f(k());
        android.support.v4.c.a.a.a(this.d, colorStateList);
        if (mode != null) {
            android.support.v4.c.a.a.a(this.d, mode);
        }
        this.e = android.support.v4.c.a.a.f(k());
        android.support.v4.c.a.a.a(this.e, b(i2));
        if (i3 > 0) {
            this.f = a(i3, colorStateList);
            drawableArr = new Drawable[]{this.f, this.d, this.e};
        } else {
            this.f = null;
            drawableArr = new Drawable[]{this.d, this.e};
        }
        this.g = new LayerDrawable(drawableArr);
        this.c = new j(this.n.getContext(), this.g, this.o.a(), this.h, this.h + this.i);
        this.c.a(false);
        this.o.a(this.c);
    }

    /* access modifiers changed from: 0000 */
    public void a(Mode mode) {
        if (this.d != null) {
            android.support.v4.c.a.a.a(this.d, mode);
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(Rect rect) {
        this.c.getPadding(rect);
    }

    /* access modifiers changed from: 0000 */
    public void a(final c cVar, final boolean z) {
        if (!n()) {
            this.n.animate().cancel();
            if (p()) {
                this.f381b = 1;
                this.n.animate().scaleX(0.0f).scaleY(0.0f).alpha(0.0f).setDuration(200).setInterpolator(a.c).setListener(new AnimatorListenerAdapter() {
                    private boolean d;

                    public void onAnimationCancel(Animator animator) {
                        this.d = true;
                    }

                    public void onAnimationEnd(Animator animator) {
                        f.this.f381b = 0;
                        if (!this.d) {
                            f.this.n.a(z ? 8 : 4, z);
                            if (cVar != null) {
                                cVar.b();
                            }
                        }
                    }

                    public void onAnimationStart(Animator animator) {
                        f.this.n.a(0, z);
                        this.d = false;
                    }
                });
                return;
            }
            this.n.a(z ? 8 : 4, z);
            if (cVar != null) {
                cVar.b();
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(int[] iArr) {
        this.p.a(iArr);
    }

    /* access modifiers changed from: 0000 */
    public void b() {
        this.p.a();
    }

    /* access modifiers changed from: 0000 */
    public final void b(float f2) {
        if (this.i != f2) {
            this.i = f2;
            a(this.h, f2);
        }
    }

    /* access modifiers changed from: 0000 */
    public void b(Rect rect) {
    }

    /* access modifiers changed from: 0000 */
    public void b(final c cVar, final boolean z) {
        if (!m()) {
            this.n.animate().cancel();
            if (p()) {
                this.f381b = 2;
                if (this.n.getVisibility() != 0) {
                    this.n.setAlpha(0.0f);
                    this.n.setScaleY(0.0f);
                    this.n.setScaleX(0.0f);
                }
                this.n.animate().scaleX(1.0f).scaleY(1.0f).alpha(1.0f).setDuration(200).setInterpolator(a.d).setListener(new AnimatorListenerAdapter() {
                    public void onAnimationEnd(Animator animator) {
                        f.this.f381b = 0;
                        if (cVar != null) {
                            cVar.a();
                        }
                    }

                    public void onAnimationStart(Animator animator) {
                        f.this.n.a(0, z);
                    }
                });
                return;
            }
            this.n.a(0, z);
            this.n.setAlpha(1.0f);
            this.n.setScaleY(1.0f);
            this.n.setScaleX(1.0f);
            if (cVar != null) {
                cVar.a();
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public final Drawable c() {
        return this.g;
    }

    /* access modifiers changed from: 0000 */
    public void d() {
    }

    /* access modifiers changed from: 0000 */
    public final void e() {
        Rect rect = this.r;
        a(rect);
        b(rect);
        this.o.a(rect.left, rect.top, rect.right, rect.bottom);
    }

    /* access modifiers changed from: 0000 */
    public void f() {
        if (h()) {
            o();
            this.n.getViewTreeObserver().addOnPreDrawListener(this.s);
        }
    }

    /* access modifiers changed from: 0000 */
    public void g() {
        if (this.s != null) {
            this.n.getViewTreeObserver().removeOnPreDrawListener(this.s);
            this.s = null;
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean h() {
        return true;
    }

    /* access modifiers changed from: 0000 */
    public c i() {
        return new c();
    }

    /* access modifiers changed from: 0000 */
    public void j() {
        float rotation = this.n.getRotation();
        if (this.q != rotation) {
            this.q = rotation;
            q();
        }
    }

    /* access modifiers changed from: 0000 */
    public GradientDrawable k() {
        GradientDrawable l2 = l();
        l2.setShape(1);
        l2.setColor(-1);
        return l2;
    }

    /* access modifiers changed from: 0000 */
    public GradientDrawable l() {
        return new GradientDrawable();
    }

    /* access modifiers changed from: 0000 */
    public boolean m() {
        return this.n.getVisibility() != 0 ? this.f381b == 2 : this.f381b != 1;
    }

    /* access modifiers changed from: 0000 */
    public boolean n() {
        return this.n.getVisibility() == 0 ? this.f381b == 1 : this.f381b != 2;
    }
}
