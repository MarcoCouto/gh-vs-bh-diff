package android.support.design.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.ClassLoaderCreator;
import android.os.Parcelable.Creator;
import android.support.design.a.d;
import android.support.design.a.j;
import android.support.v4.i.t;
import android.support.v4.widget.q;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import java.lang.ref.WeakReference;

public class BottomSheetBehavior<V extends View> extends android.support.design.widget.CoordinatorLayout.a<V> {

    /* renamed from: a reason: collision with root package name */
    int f323a;

    /* renamed from: b reason: collision with root package name */
    int f324b;
    boolean c;
    int d = 4;
    q e;
    int f;
    WeakReference<V> g;
    WeakReference<View> h;
    int i;
    boolean j;
    private float k;
    private int l;
    private boolean m;
    private int n;
    private boolean o;
    private boolean p;
    private int q;
    private boolean r;
    private a s;
    private VelocityTracker t;
    private int u;
    private final android.support.v4.widget.q.a v = new android.support.v4.widget.q.a() {
        public int a(View view) {
            return BottomSheetBehavior.this.c ? BottomSheetBehavior.this.f - BottomSheetBehavior.this.f323a : BottomSheetBehavior.this.f324b - BottomSheetBehavior.this.f323a;
        }

        public int a(View view, int i, int i2) {
            return android.support.v4.e.a.a(i, BottomSheetBehavior.this.f323a, BottomSheetBehavior.this.c ? BottomSheetBehavior.this.f : BottomSheetBehavior.this.f324b);
        }

        public void a(int i) {
            if (i == 1) {
                BottomSheetBehavior.this.b(1);
            }
        }

        public void a(View view, float f, float f2) {
            int i;
            int i2 = 3;
            if (f2 < 0.0f) {
                i = BottomSheetBehavior.this.f323a;
            } else if (BottomSheetBehavior.this.c && BottomSheetBehavior.this.a(view, f2)) {
                i = BottomSheetBehavior.this.f;
                i2 = 5;
            } else if (f2 == 0.0f) {
                int top = view.getTop();
                if (Math.abs(top - BottomSheetBehavior.this.f323a) < Math.abs(top - BottomSheetBehavior.this.f324b)) {
                    i = BottomSheetBehavior.this.f323a;
                } else {
                    i = BottomSheetBehavior.this.f324b;
                    i2 = 4;
                }
            } else {
                i = BottomSheetBehavior.this.f324b;
                i2 = 4;
            }
            if (BottomSheetBehavior.this.e.a(view.getLeft(), i)) {
                BottomSheetBehavior.this.b(2);
                t.a(view, (Runnable) new c(view, i2));
                return;
            }
            BottomSheetBehavior.this.b(i2);
        }

        public void a(View view, int i, int i2, int i3, int i4) {
            BottomSheetBehavior.this.c(i2);
        }

        public boolean a(View view, int i) {
            if (BottomSheetBehavior.this.d == 1 || BottomSheetBehavior.this.j) {
                return false;
            }
            if (BottomSheetBehavior.this.d == 3 && BottomSheetBehavior.this.i == i) {
                View view2 = (View) BottomSheetBehavior.this.h.get();
                if (view2 != null && view2.canScrollVertically(-1)) {
                    return false;
                }
            }
            return BottomSheetBehavior.this.g != null && BottomSheetBehavior.this.g.get() == view;
        }

        public int b(View view, int i, int i2) {
            return view.getLeft();
        }
    };

    public static abstract class a {
        public abstract void a(View view, float f);

        public abstract void a(View view, int i);
    }

    protected static class b extends android.support.v4.i.a {
        public static final Creator<b> CREATOR = new ClassLoaderCreator<b>() {
            /* renamed from: a */
            public b createFromParcel(Parcel parcel) {
                return new b(parcel, (ClassLoader) null);
            }

            /* renamed from: a */
            public b createFromParcel(Parcel parcel, ClassLoader classLoader) {
                return new b(parcel, classLoader);
            }

            /* renamed from: a */
            public b[] newArray(int i) {
                return new b[i];
            }
        };

        /* renamed from: a reason: collision with root package name */
        final int f326a;

        public b(Parcel parcel, ClassLoader classLoader) {
            super(parcel, classLoader);
            this.f326a = parcel.readInt();
        }

        public b(Parcelable parcelable, int i) {
            super(parcelable);
            this.f326a = i;
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.f326a);
        }
    }

    private class c implements Runnable {

        /* renamed from: b reason: collision with root package name */
        private final View f328b;
        private final int c;

        c(View view, int i) {
            this.f328b = view;
            this.c = i;
        }

        public void run() {
            if (BottomSheetBehavior.this.e == null || !BottomSheetBehavior.this.e.a(true)) {
                BottomSheetBehavior.this.b(this.c);
            } else {
                t.a(this.f328b, (Runnable) this);
            }
        }
    }

    public BottomSheetBehavior() {
    }

    public BottomSheetBehavior(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, j.BottomSheetBehavior_Layout);
        TypedValue peekValue = obtainStyledAttributes.peekValue(j.BottomSheetBehavior_Layout_behavior_peekHeight);
        if (peekValue == null || peekValue.data != -1) {
            a(obtainStyledAttributes.getDimensionPixelSize(j.BottomSheetBehavior_Layout_behavior_peekHeight, -1));
        } else {
            a(peekValue.data);
        }
        a(obtainStyledAttributes.getBoolean(j.BottomSheetBehavior_Layout_behavior_hideable, false));
        b(obtainStyledAttributes.getBoolean(j.BottomSheetBehavior_Layout_behavior_skipCollapsed, false));
        obtainStyledAttributes.recycle();
        this.k = (float) ViewConfiguration.get(context).getScaledMaximumFlingVelocity();
    }

    private void a() {
        this.i = -1;
        if (this.t != null) {
            this.t.recycle();
            this.t = null;
        }
    }

    private float b() {
        this.t.computeCurrentVelocity(1000, this.k);
        return this.t.getYVelocity(this.i);
    }

    /* access modifiers changed from: 0000 */
    public View a(View view) {
        if (t.t(view)) {
            return view;
        }
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            int childCount = viewGroup.getChildCount();
            for (int i2 = 0; i2 < childCount; i2++) {
                View a2 = a(viewGroup.getChildAt(i2));
                if (a2 != null) {
                    return a2;
                }
            }
        }
        return null;
    }

    public final void a(int i2) {
        boolean z = true;
        if (i2 == -1) {
            if (!this.m) {
                this.m = true;
            }
            z = false;
        } else {
            if (this.m || this.l != i2) {
                this.m = false;
                this.l = Math.max(0, i2);
                this.f324b = this.f - i2;
            }
            z = false;
        }
        if (z && this.d == 4 && this.g != null) {
            View view = (View) this.g.get();
            if (view != null) {
                view.requestLayout();
            }
        }
    }

    public void a(CoordinatorLayout coordinatorLayout, V v2, Parcelable parcelable) {
        b bVar = (b) parcelable;
        super.a(coordinatorLayout, v2, bVar.a());
        if (bVar.f326a == 1 || bVar.f326a == 2) {
            this.d = 4;
        } else {
            this.d = bVar.f326a;
        }
    }

    public void a(CoordinatorLayout coordinatorLayout, V v2, View view, int i2, int i3, int[] iArr) {
        if (view == ((View) this.h.get())) {
            int top = v2.getTop();
            int i4 = top - i3;
            if (i3 > 0) {
                if (i4 < this.f323a) {
                    iArr[1] = top - this.f323a;
                    t.c(v2, -iArr[1]);
                    b(3);
                } else {
                    iArr[1] = i3;
                    t.c(v2, -i3);
                    b(1);
                }
            } else if (i3 < 0 && !view.canScrollVertically(-1)) {
                if (i4 <= this.f324b || this.c) {
                    iArr[1] = i3;
                    t.c(v2, -i3);
                    b(1);
                } else {
                    iArr[1] = top - this.f324b;
                    t.c(v2, -iArr[1]);
                    b(4);
                }
            }
            c(v2.getTop());
            this.q = i3;
            this.r = true;
        }
    }

    public void a(boolean z) {
        this.c = z;
    }

    public boolean a(CoordinatorLayout coordinatorLayout, V v2, int i2) {
        int i3;
        if (t.o(coordinatorLayout) && !t.o(v2)) {
            t.b((View) v2, true);
        }
        int top = v2.getTop();
        coordinatorLayout.a((View) v2, i2);
        this.f = coordinatorLayout.getHeight();
        if (this.m) {
            if (this.n == 0) {
                this.n = coordinatorLayout.getResources().getDimensionPixelSize(d.design_bottom_sheet_peek_height_min);
            }
            i3 = Math.max(this.n, this.f - ((coordinatorLayout.getWidth() * 9) / 16));
        } else {
            i3 = this.l;
        }
        this.f323a = Math.max(0, this.f - v2.getHeight());
        this.f324b = Math.max(this.f - i3, this.f323a);
        if (this.d == 3) {
            t.c(v2, this.f323a);
        } else if (this.c && this.d == 5) {
            t.c(v2, this.f);
        } else if (this.d == 4) {
            t.c(v2, this.f324b);
        } else if (this.d == 1 || this.d == 2) {
            t.c(v2, top - v2.getTop());
        }
        if (this.e == null) {
            this.e = q.a((ViewGroup) coordinatorLayout, this.v);
        }
        this.g = new WeakReference<>(v2);
        this.h = new WeakReference<>(a((View) v2));
        return true;
    }

    public boolean a(CoordinatorLayout coordinatorLayout, V v2, MotionEvent motionEvent) {
        boolean z = true;
        if (!v2.isShown()) {
            this.p = true;
            return false;
        }
        int actionMasked = motionEvent.getActionMasked();
        if (actionMasked == 0) {
            a();
        }
        if (this.t == null) {
            this.t = VelocityTracker.obtain();
        }
        this.t.addMovement(motionEvent);
        switch (actionMasked) {
            case 0:
                int x = (int) motionEvent.getX();
                this.u = (int) motionEvent.getY();
                View view = this.h != null ? (View) this.h.get() : null;
                if (view != null && coordinatorLayout.a(view, x, this.u)) {
                    this.i = motionEvent.getPointerId(motionEvent.getActionIndex());
                    this.j = true;
                }
                this.p = this.i == -1 && !coordinatorLayout.a((View) v2, x, this.u);
                break;
            case 1:
            case 3:
                this.j = false;
                this.i = -1;
                if (this.p) {
                    this.p = false;
                    return false;
                }
                break;
        }
        if (!this.p && this.e.a(motionEvent)) {
            return true;
        }
        View view2 = (View) this.h.get();
        if (actionMasked != 2 || view2 == null || this.p || this.d == 1 || coordinatorLayout.a(view2, (int) motionEvent.getX(), (int) motionEvent.getY()) || Math.abs(((float) this.u) - motionEvent.getY()) <= ((float) this.e.a())) {
            z = false;
        }
        return z;
    }

    public boolean a(CoordinatorLayout coordinatorLayout, V v2, View view, float f2, float f3) {
        return view == this.h.get() && (this.d != 3 || super.a(coordinatorLayout, v2, view, f2, f3));
    }

    public boolean a(CoordinatorLayout coordinatorLayout, V v2, View view, View view2, int i2) {
        this.q = 0;
        this.r = false;
        return (i2 & 2) != 0;
    }

    /* access modifiers changed from: 0000 */
    public boolean a(View view, float f2) {
        if (this.o) {
            return true;
        }
        if (view.getTop() < this.f324b) {
            return false;
        }
        return Math.abs((((float) view.getTop()) + (0.1f * f2)) - ((float) this.f324b)) / ((float) this.l) > 0.5f;
    }

    public Parcelable b(CoordinatorLayout coordinatorLayout, V v2) {
        return new b(super.b(coordinatorLayout, v2), this.d);
    }

    /* access modifiers changed from: 0000 */
    public void b(int i2) {
        if (this.d != i2) {
            this.d = i2;
            View view = (View) this.g.get();
            if (view != null && this.s != null) {
                this.s.a(view, i2);
            }
        }
    }

    public void b(boolean z) {
        this.o = z;
    }

    public boolean b(CoordinatorLayout coordinatorLayout, V v2, MotionEvent motionEvent) {
        if (!v2.isShown()) {
            return false;
        }
        int actionMasked = motionEvent.getActionMasked();
        if (this.d == 1 && actionMasked == 0) {
            return true;
        }
        if (this.e != null) {
            this.e.b(motionEvent);
        }
        if (actionMasked == 0) {
            a();
        }
        if (this.t == null) {
            this.t = VelocityTracker.obtain();
        }
        this.t.addMovement(motionEvent);
        if (actionMasked == 2 && !this.p && Math.abs(((float) this.u) - motionEvent.getY()) > ((float) this.e.a())) {
            this.e.a((View) v2, motionEvent.getPointerId(motionEvent.getActionIndex()));
        }
        return !this.p;
    }

    /* access modifiers changed from: 0000 */
    public void c(int i2) {
        View view = (View) this.g.get();
        if (view != null && this.s != null) {
            if (i2 > this.f324b) {
                this.s.a(view, ((float) (this.f324b - i2)) / ((float) (this.f - this.f324b)));
            } else {
                this.s.a(view, ((float) (this.f324b - i2)) / ((float) (this.f324b - this.f323a)));
            }
        }
    }

    public void c(CoordinatorLayout coordinatorLayout, V v2, View view) {
        int i2;
        int i3 = 3;
        if (v2.getTop() == this.f323a) {
            b(3);
        } else if (this.h != null && view == this.h.get() && this.r) {
            if (this.q > 0) {
                i2 = this.f323a;
            } else if (this.c && a(v2, b())) {
                i2 = this.f;
                i3 = 5;
            } else if (this.q == 0) {
                int top = v2.getTop();
                if (Math.abs(top - this.f323a) < Math.abs(top - this.f324b)) {
                    i2 = this.f323a;
                } else {
                    i2 = this.f324b;
                    i3 = 4;
                }
            } else {
                i2 = this.f324b;
                i3 = 4;
            }
            if (this.e.a((View) v2, v2.getLeft(), i2)) {
                b(2);
                t.a((View) v2, (Runnable) new c(v2, i3));
            } else {
                b(i3);
            }
            this.r = false;
        }
    }
}
