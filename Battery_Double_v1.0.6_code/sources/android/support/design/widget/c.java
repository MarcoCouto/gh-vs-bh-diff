package android.support.design.widget;

import android.content.res.ColorStateList;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.Shader.TileMode;
import android.graphics.drawable.Drawable;
import android.support.v4.c.a;

class c extends Drawable {

    /* renamed from: a reason: collision with root package name */
    final Paint f376a = new Paint(1);

    /* renamed from: b reason: collision with root package name */
    final Rect f377b = new Rect();
    final RectF c = new RectF();
    float d;
    private int e;
    private int f;
    private int g;
    private int h;
    private ColorStateList i;
    private int j;
    private boolean k = true;
    private float l;

    public c() {
        this.f376a.setStyle(Style.STROKE);
    }

    private Shader a() {
        Rect rect = this.f377b;
        copyBounds(rect);
        float height = this.d / ((float) rect.height());
        return new LinearGradient(0.0f, (float) rect.top, 0.0f, (float) rect.bottom, new int[]{a.a(this.e, this.j), a.a(this.f, this.j), a.a(a.b(this.f, 0), this.j), a.a(a.b(this.h, 0), this.j), a.a(this.h, this.j), a.a(this.g, this.j)}, new float[]{0.0f, height, 0.5f, 0.5f, 1.0f - height, 1.0f}, TileMode.CLAMP);
    }

    /* access modifiers changed from: 0000 */
    public void a(float f2) {
        if (this.d != f2) {
            this.d = f2;
            this.f376a.setStrokeWidth(1.3333f * f2);
            this.k = true;
            invalidateSelf();
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(int i2, int i3, int i4, int i5) {
        this.e = i2;
        this.f = i3;
        this.g = i4;
        this.h = i5;
    }

    /* access modifiers changed from: 0000 */
    public void a(ColorStateList colorStateList) {
        if (colorStateList != null) {
            this.j = colorStateList.getColorForState(getState(), this.j);
        }
        this.i = colorStateList;
        this.k = true;
        invalidateSelf();
    }

    /* access modifiers changed from: 0000 */
    public final void b(float f2) {
        if (f2 != this.l) {
            this.l = f2;
            invalidateSelf();
        }
    }

    public void draw(Canvas canvas) {
        if (this.k) {
            this.f376a.setShader(a());
            this.k = false;
        }
        float strokeWidth = this.f376a.getStrokeWidth() / 2.0f;
        RectF rectF = this.c;
        copyBounds(this.f377b);
        rectF.set(this.f377b);
        rectF.left += strokeWidth;
        rectF.top += strokeWidth;
        rectF.right -= strokeWidth;
        rectF.bottom -= strokeWidth;
        canvas.save();
        canvas.rotate(this.l, rectF.centerX(), rectF.centerY());
        canvas.drawOval(rectF, this.f376a);
        canvas.restore();
    }

    public int getOpacity() {
        return this.d > 0.0f ? -3 : -2;
    }

    public boolean getPadding(Rect rect) {
        int round = Math.round(this.d);
        rect.set(round, round, round, round);
        return true;
    }

    public boolean isStateful() {
        return (this.i != null && this.i.isStateful()) || super.isStateful();
    }

    /* access modifiers changed from: protected */
    public void onBoundsChange(Rect rect) {
        this.k = true;
    }

    /* access modifiers changed from: protected */
    public boolean onStateChange(int[] iArr) {
        if (this.i != null) {
            int colorForState = this.i.getColorForState(iArr, this.j);
            if (colorForState != this.j) {
                this.k = true;
                this.j = colorForState;
            }
        }
        if (this.k) {
            invalidateSelf();
        }
        return this.k;
    }

    public void setAlpha(int i2) {
        this.f376a.setAlpha(i2);
        invalidateSelf();
    }

    public void setColorFilter(ColorFilter colorFilter) {
        this.f376a.setColorFilter(colorFilter);
        invalidateSelf();
    }
}
