package android.support.design.widget;

import android.support.v4.i.t;
import android.view.View;

class s {

    /* renamed from: a reason: collision with root package name */
    private final View f436a;

    /* renamed from: b reason: collision with root package name */
    private int f437b;
    private int c;
    private int d;
    private int e;

    public s(View view) {
        this.f436a = view;
    }

    private void c() {
        t.c(this.f436a, this.d - (this.f436a.getTop() - this.f437b));
        t.d(this.f436a, this.e - (this.f436a.getLeft() - this.c));
    }

    public void a() {
        this.f437b = this.f436a.getTop();
        this.c = this.f436a.getLeft();
        c();
    }

    public boolean a(int i) {
        if (this.d == i) {
            return false;
        }
        this.d = i;
        c();
        return true;
    }

    public int b() {
        return this.d;
    }

    public boolean b(int i) {
        if (this.e == i) {
            return false;
        }
        this.e = i;
        c();
        return true;
    }
}
