package android.support.design.widget;

import android.content.Context;
import android.graphics.Rect;
import android.support.design.widget.CoordinatorLayout.d;
import android.support.v4.e.a;
import android.support.v4.i.ac;
import android.support.v4.i.e;
import android.support.v4.i.t;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.MeasureSpec;
import java.util.List;

abstract class i extends r<View> {

    /* renamed from: a reason: collision with root package name */
    final Rect f396a = new Rect();

    /* renamed from: b reason: collision with root package name */
    final Rect f397b = new Rect();
    private int c = 0;
    private int d;

    public i() {
    }

    public i(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    private static int c(int i) {
        if (i == 0) {
            return 8388659;
        }
        return i;
    }

    /* access modifiers changed from: 0000 */
    public float a(View view) {
        return 1.0f;
    }

    /* access modifiers changed from: 0000 */
    public final int a() {
        return this.c;
    }

    public boolean a(CoordinatorLayout coordinatorLayout, View view, int i, int i2, int i3, int i4) {
        int i5 = view.getLayoutParams().height;
        if (i5 == -1 || i5 == -2) {
            View b2 = b(coordinatorLayout.c(view));
            if (b2 != null) {
                if (t.o(b2) && !t.o(view)) {
                    t.b(view, true);
                    if (t.o(view)) {
                        view.requestLayout();
                        return true;
                    }
                }
                int size = MeasureSpec.getSize(i3);
                if (size == 0) {
                    size = coordinatorLayout.getHeight();
                }
                coordinatorLayout.a(view, i, i2, MeasureSpec.makeMeasureSpec(b(b2) + (size - b2.getMeasuredHeight()), i5 == -1 ? 1073741824 : Integer.MIN_VALUE), i4);
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: 0000 */
    public int b(View view) {
        return view.getMeasuredHeight();
    }

    /* access modifiers changed from: 0000 */
    public abstract View b(List<View> list);

    public final void b(int i) {
        this.d = i;
    }

    /* access modifiers changed from: protected */
    public void b(CoordinatorLayout coordinatorLayout, View view, int i) {
        View b2 = b(coordinatorLayout.c(view));
        if (b2 != null) {
            d dVar = (d) view.getLayoutParams();
            Rect rect = this.f396a;
            rect.set(coordinatorLayout.getPaddingLeft() + dVar.leftMargin, b2.getBottom() + dVar.topMargin, (coordinatorLayout.getWidth() - coordinatorLayout.getPaddingRight()) - dVar.rightMargin, ((coordinatorLayout.getHeight() + b2.getBottom()) - coordinatorLayout.getPaddingBottom()) - dVar.bottomMargin);
            ac lastWindowInsets = coordinatorLayout.getLastWindowInsets();
            if (lastWindowInsets != null && t.o(coordinatorLayout) && !t.o(view)) {
                rect.left += lastWindowInsets.a();
                rect.right -= lastWindowInsets.c();
            }
            Rect rect2 = this.f397b;
            e.a(c(dVar.c), view.getMeasuredWidth(), view.getMeasuredHeight(), rect, rect2, i);
            int c2 = c(b2);
            view.layout(rect2.left, rect2.top - c2, rect2.right, rect2.bottom - c2);
            this.c = rect2.top - b2.getBottom();
            return;
        }
        super.b(coordinatorLayout, view, i);
        this.c = 0;
    }

    /* access modifiers changed from: 0000 */
    public final int c(View view) {
        if (this.d == 0) {
            return 0;
        }
        return a.a((int) (a(view) * ((float) this.d)), 0, this.d);
    }

    public final int d() {
        return this.d;
    }
}
