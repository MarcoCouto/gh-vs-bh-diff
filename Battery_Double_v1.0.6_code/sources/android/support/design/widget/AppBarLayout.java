package android.support.design.widget;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.os.Build.VERSION;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.ClassLoaderCreator;
import android.os.Parcelable.Creator;
import android.support.design.a.i;
import android.support.design.a.j;
import android.support.design.widget.CoordinatorLayout.d;
import android.support.v4.i.ac;
import android.support.v4.i.p;
import android.support.v4.i.t;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import java.lang.ref.WeakReference;
import java.util.List;

@android.support.design.widget.CoordinatorLayout.b(a = Behavior.class)
public class AppBarLayout extends LinearLayout {

    /* renamed from: a reason: collision with root package name */
    private int f309a;

    /* renamed from: b reason: collision with root package name */
    private int f310b;
    private int c;
    private boolean d;
    private int e;
    private ac f;
    private List<b> g;
    private boolean h;
    private boolean i;
    private int[] j;

    public static class Behavior extends h<AppBarLayout> {
        /* access modifiers changed from: private */

        /* renamed from: b reason: collision with root package name */
        public int f312b;
        private ValueAnimator c;
        private int d = -1;
        private boolean e;
        private float f;
        private WeakReference<View> g;
        private a h;

        public static abstract class a {
            public abstract boolean a(AppBarLayout appBarLayout);
        }

        protected static class b extends android.support.v4.i.a {
            public static final Creator<b> CREATOR = new ClassLoaderCreator<b>() {
                /* renamed from: a */
                public b createFromParcel(Parcel parcel) {
                    return new b(parcel, null);
                }

                /* renamed from: a */
                public b createFromParcel(Parcel parcel, ClassLoader classLoader) {
                    return new b(parcel, classLoader);
                }

                /* renamed from: a */
                public b[] newArray(int i) {
                    return new b[i];
                }
            };

            /* renamed from: a reason: collision with root package name */
            int f315a;

            /* renamed from: b reason: collision with root package name */
            float f316b;
            boolean c;

            public b(Parcel parcel, ClassLoader classLoader) {
                super(parcel, classLoader);
                this.f315a = parcel.readInt();
                this.f316b = parcel.readFloat();
                this.c = parcel.readByte() != 0;
            }

            public b(Parcelable parcelable) {
                super(parcelable);
            }

            public void writeToParcel(Parcel parcel, int i) {
                super.writeToParcel(parcel, i);
                parcel.writeInt(this.f315a);
                parcel.writeFloat(this.f316b);
                parcel.writeByte((byte) (this.c ? 1 : 0));
            }
        }

        public Behavior() {
        }

        public Behavior(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }

        private int a(AppBarLayout appBarLayout, int i) {
            int childCount = appBarLayout.getChildCount();
            for (int i2 = 0; i2 < childCount; i2++) {
                View childAt = appBarLayout.getChildAt(i2);
                if (childAt.getTop() <= (-i) && childAt.getBottom() >= (-i)) {
                    return i2;
                }
            }
            return -1;
        }

        private void a(CoordinatorLayout coordinatorLayout, AppBarLayout appBarLayout, int i, float f2) {
            int abs = Math.abs(a() - i);
            float abs2 = Math.abs(f2);
            a(coordinatorLayout, appBarLayout, i, abs2 > 0.0f ? Math.round((((float) abs) / abs2) * 1000.0f) * 3 : (int) (((((float) abs) / ((float) appBarLayout.getHeight())) + 1.0f) * 150.0f));
        }

        private void a(final CoordinatorLayout coordinatorLayout, final AppBarLayout appBarLayout, int i, int i2) {
            int a2 = a();
            if (a2 != i) {
                if (this.c == null) {
                    this.c = new ValueAnimator();
                    this.c.setInterpolator(a.e);
                    this.c.addUpdateListener(new AnimatorUpdateListener() {
                        public void onAnimationUpdate(ValueAnimator valueAnimator) {
                            Behavior.this.a_(coordinatorLayout, appBarLayout, ((Integer) valueAnimator.getAnimatedValue()).intValue());
                        }
                    });
                } else {
                    this.c.cancel();
                }
                this.c.setDuration((long) Math.min(i2, 600));
                this.c.setIntValues(new int[]{a2, i});
                this.c.start();
            } else if (this.c != null && this.c.isRunning()) {
                this.c.cancel();
            }
        }

        private void a(CoordinatorLayout coordinatorLayout, AppBarLayout appBarLayout, int i, int i2, boolean z) {
            boolean z2 = true;
            boolean z3 = false;
            View c2 = c(appBarLayout, i);
            if (c2 != null) {
                int a2 = ((a) c2.getLayoutParams()).a();
                if ((a2 & 1) != 0) {
                    int i3 = t.i(c2);
                    if (i2 > 0 && (a2 & 12) != 0) {
                        z3 = (-i) >= (c2.getBottom() - i3) - appBarLayout.getTopInset();
                    } else if ((a2 & 2) != 0) {
                        if ((-i) < (c2.getBottom() - i3) - appBarLayout.getTopInset()) {
                            z2 = false;
                        }
                        z3 = z2;
                    }
                }
                boolean a3 = appBarLayout.a(z3);
                if (VERSION.SDK_INT < 11) {
                    return;
                }
                if (z || (a3 && d(coordinatorLayout, appBarLayout))) {
                    appBarLayout.jumpDrawablesToCurrentState();
                }
            }
        }

        private static boolean a(int i, int i2) {
            return (i & i2) == i2;
        }

        private int b(AppBarLayout appBarLayout, int i) {
            int i2;
            int abs = Math.abs(i);
            int childCount = appBarLayout.getChildCount();
            int i3 = 0;
            while (i3 < childCount) {
                View childAt = appBarLayout.getChildAt(i3);
                a aVar = (a) childAt.getLayoutParams();
                Interpolator b2 = aVar.b();
                if (abs < childAt.getTop() || abs > childAt.getBottom()) {
                    i3++;
                } else if (b2 == null) {
                    return i;
                } else {
                    int a2 = aVar.a();
                    if ((a2 & 1) != 0) {
                        i2 = aVar.bottomMargin + childAt.getHeight() + aVar.topMargin + 0;
                        if ((a2 & 2) != 0) {
                            i2 -= t.i(childAt);
                        }
                    } else {
                        i2 = 0;
                    }
                    if (t.o(childAt)) {
                        i2 -= appBarLayout.getTopInset();
                    }
                    if (i2 <= 0) {
                        return i;
                    }
                    return Integer.signum(i) * (Math.round(b2.getInterpolation(((float) (abs - childAt.getTop())) / ((float) i2)) * ((float) i2)) + childAt.getTop());
                }
            }
            return i;
        }

        private static View c(AppBarLayout appBarLayout, int i) {
            int abs = Math.abs(i);
            int childCount = appBarLayout.getChildCount();
            for (int i2 = 0; i2 < childCount; i2++) {
                View childAt = appBarLayout.getChildAt(i2);
                if (abs >= childAt.getTop() && abs <= childAt.getBottom()) {
                    return childAt;
                }
            }
            return null;
        }

        private void c(CoordinatorLayout coordinatorLayout, AppBarLayout appBarLayout) {
            int i;
            int a2 = a();
            int a3 = a(appBarLayout, a2);
            if (a3 >= 0) {
                View childAt = appBarLayout.getChildAt(a3);
                int a4 = ((a) childAt.getLayoutParams()).a();
                if ((a4 & 17) == 17) {
                    int i2 = -childAt.getTop();
                    int i3 = -childAt.getBottom();
                    if (a3 == appBarLayout.getChildCount() - 1) {
                        i3 += appBarLayout.getTopInset();
                    }
                    if (a(a4, 2)) {
                        i3 += t.i(childAt);
                        i = i2;
                    } else if (a(a4, 5)) {
                        i = t.i(childAt) + i3;
                        if (a2 >= i) {
                            i3 = i;
                            i = i2;
                        }
                    } else {
                        i = i2;
                    }
                    if (a2 >= (i3 + i) / 2) {
                        i3 = i;
                    }
                    a(coordinatorLayout, appBarLayout, android.support.v4.e.a.a(i3, -appBarLayout.getTotalScrollRange(), 0), 0.0f);
                }
            }
        }

        private boolean d(CoordinatorLayout coordinatorLayout, AppBarLayout appBarLayout) {
            List d2 = coordinatorLayout.d((View) appBarLayout);
            int size = d2.size();
            for (int i = 0; i < size; i++) {
                android.support.design.widget.CoordinatorLayout.a b2 = ((d) ((View) d2.get(i)).getLayoutParams()).b();
                if (b2 instanceof ScrollingViewBehavior) {
                    return ((ScrollingViewBehavior) b2).d() != 0;
                }
            }
            return false;
        }

        /* access modifiers changed from: 0000 */
        public int a() {
            return b() + this.f312b;
        }

        /* access modifiers changed from: 0000 */
        public int a(CoordinatorLayout coordinatorLayout, AppBarLayout appBarLayout, int i, int i2, int i3) {
            int a2 = a();
            if (i2 == 0 || a2 < i2 || a2 > i3) {
                this.f312b = 0;
                return 0;
            }
            int a3 = android.support.v4.e.a.a(i, i2, i3);
            if (a2 == a3) {
                return 0;
            }
            int i4 = appBarLayout.b() ? b(appBarLayout, a3) : a3;
            boolean a4 = a(i4);
            int i5 = a2 - a3;
            this.f312b = a3 - i4;
            if (!a4 && appBarLayout.b()) {
                coordinatorLayout.b((View) appBarLayout);
            }
            appBarLayout.a(b());
            a(coordinatorLayout, appBarLayout, a3, a3 < a2 ? -1 : 1, false);
            return i5;
        }

        /* access modifiers changed from: 0000 */
        public void a(CoordinatorLayout coordinatorLayout, AppBarLayout appBarLayout) {
            c(coordinatorLayout, appBarLayout);
        }

        public void a(CoordinatorLayout coordinatorLayout, AppBarLayout appBarLayout, Parcelable parcelable) {
            if (parcelable instanceof b) {
                b bVar = (b) parcelable;
                super.a(coordinatorLayout, appBarLayout, bVar.a());
                this.d = bVar.f315a;
                this.f = bVar.f316b;
                this.e = bVar.c;
                return;
            }
            super.a(coordinatorLayout, appBarLayout, parcelable);
            this.d = -1;
        }

        public void a(CoordinatorLayout coordinatorLayout, AppBarLayout appBarLayout, View view, int i) {
            if (i == 0) {
                c(coordinatorLayout, appBarLayout);
            }
            this.g = new WeakReference<>(view);
        }

        public void a(CoordinatorLayout coordinatorLayout, AppBarLayout appBarLayout, View view, int i, int i2, int i3, int i4, int i5) {
            if (i4 < 0) {
                b(coordinatorLayout, appBarLayout, i4, -appBarLayout.getDownNestedScrollRange(), 0);
            }
        }

        public void a(CoordinatorLayout coordinatorLayout, AppBarLayout appBarLayout, View view, int i, int i2, int[] iArr, int i3) {
            int i4;
            int i5;
            if (i2 != 0) {
                if (i2 < 0) {
                    i4 = -appBarLayout.getTotalScrollRange();
                    i5 = i4 + appBarLayout.getDownNestedPreScrollRange();
                } else {
                    i4 = -appBarLayout.getUpNestedPreScrollRange();
                    i5 = 0;
                }
                if (i4 != i5) {
                    iArr[1] = b(coordinatorLayout, appBarLayout, i2, i4, i5);
                }
            }
        }

        public /* bridge */ /* synthetic */ boolean a(int i) {
            return super.a(i);
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public boolean c(AppBarLayout appBarLayout) {
            if (this.h != null) {
                return this.h.a(appBarLayout);
            }
            if (this.g == null) {
                return true;
            }
            View view = (View) this.g.get();
            return view != null && view.isShown() && !view.canScrollVertically(-1);
        }

        public boolean a(CoordinatorLayout coordinatorLayout, AppBarLayout appBarLayout, int i) {
            boolean a2 = super.a(coordinatorLayout, appBarLayout, i);
            int pendingAction = appBarLayout.getPendingAction();
            if (this.d >= 0 && (pendingAction & 8) == 0) {
                View childAt = appBarLayout.getChildAt(this.d);
                int i2 = -childAt.getBottom();
                a_(coordinatorLayout, appBarLayout, this.e ? t.i(childAt) + appBarLayout.getTopInset() + i2 : Math.round(((float) childAt.getHeight()) * this.f) + i2);
            } else if (pendingAction != 0) {
                boolean z = (pendingAction & 4) != 0;
                if ((pendingAction & 2) != 0) {
                    int i3 = -appBarLayout.getUpNestedPreScrollRange();
                    if (z) {
                        a(coordinatorLayout, appBarLayout, i3, 0.0f);
                    } else {
                        a_(coordinatorLayout, appBarLayout, i3);
                    }
                } else if ((pendingAction & 1) != 0) {
                    if (z) {
                        a(coordinatorLayout, appBarLayout, 0, 0.0f);
                    } else {
                        a_(coordinatorLayout, appBarLayout, 0);
                    }
                }
            }
            appBarLayout.d();
            this.d = -1;
            a(android.support.v4.e.a.a(b(), -appBarLayout.getTotalScrollRange(), 0));
            a(coordinatorLayout, appBarLayout, b(), 0, true);
            appBarLayout.a(b());
            return a2;
        }

        public boolean a(CoordinatorLayout coordinatorLayout, AppBarLayout appBarLayout, int i, int i2, int i3, int i4) {
            if (((d) appBarLayout.getLayoutParams()).height != -2) {
                return super.a(coordinatorLayout, appBarLayout, i, i2, i3, i4);
            }
            coordinatorLayout.a((View) appBarLayout, i, i2, MeasureSpec.makeMeasureSpec(0, 0), i4);
            return true;
        }

        public boolean a(CoordinatorLayout coordinatorLayout, AppBarLayout appBarLayout, View view, View view2, int i, int i2) {
            boolean z = (i & 2) != 0 && appBarLayout.c() && coordinatorLayout.getHeight() - view.getHeight() <= appBarLayout.getHeight();
            if (z && this.c != null) {
                this.c.cancel();
            }
            this.g = null;
            return z;
        }

        public /* bridge */ /* synthetic */ int b() {
            return super.b();
        }

        /* access modifiers changed from: 0000 */
        public int b(AppBarLayout appBarLayout) {
            return -appBarLayout.getDownNestedScrollRange();
        }

        public Parcelable b(CoordinatorLayout coordinatorLayout, AppBarLayout appBarLayout) {
            boolean z = false;
            Parcelable b2 = super.b(coordinatorLayout, appBarLayout);
            int b3 = b();
            int childCount = appBarLayout.getChildCount();
            int i = 0;
            while (i < childCount) {
                View childAt = appBarLayout.getChildAt(i);
                int bottom = childAt.getBottom() + b3;
                if (childAt.getTop() + b3 > 0 || bottom < 0) {
                    i++;
                } else {
                    b bVar = new b(b2);
                    bVar.f315a = i;
                    if (bottom == t.i(childAt) + appBarLayout.getTopInset()) {
                        z = true;
                    }
                    bVar.c = z;
                    bVar.f316b = ((float) bottom) / ((float) childAt.getHeight());
                    return bVar;
                }
            }
            return b2;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: c */
        public int a(AppBarLayout appBarLayout) {
            return appBarLayout.getTotalScrollRange();
        }
    }

    public static class ScrollingViewBehavior extends i {
        public ScrollingViewBehavior() {
        }

        public ScrollingViewBehavior(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, j.ScrollingViewBehavior_Layout);
            b(obtainStyledAttributes.getDimensionPixelSize(j.ScrollingViewBehavior_Layout_behavior_overlapTop, 0));
            obtainStyledAttributes.recycle();
        }

        private static int a(AppBarLayout appBarLayout) {
            android.support.design.widget.CoordinatorLayout.a b2 = ((d) appBarLayout.getLayoutParams()).b();
            if (b2 instanceof Behavior) {
                return ((Behavior) b2).a();
            }
            return 0;
        }

        private void e(CoordinatorLayout coordinatorLayout, View view, View view2) {
            android.support.design.widget.CoordinatorLayout.a b2 = ((d) view2.getLayoutParams()).b();
            if (b2 instanceof Behavior) {
                int bottom = view2.getBottom() - view.getTop();
                t.c(view, ((((Behavior) b2).f312b + bottom) + a()) - c(view2));
            }
        }

        /* access modifiers changed from: 0000 */
        public float a(View view) {
            if (!(view instanceof AppBarLayout)) {
                return 0.0f;
            }
            AppBarLayout appBarLayout = (AppBarLayout) view;
            int totalScrollRange = appBarLayout.getTotalScrollRange();
            int downNestedPreScrollRange = appBarLayout.getDownNestedPreScrollRange();
            int a2 = a(appBarLayout);
            if (downNestedPreScrollRange != 0 && totalScrollRange + a2 <= downNestedPreScrollRange) {
                return 0.0f;
            }
            int i = totalScrollRange - downNestedPreScrollRange;
            if (i != 0) {
                return 1.0f + (((float) a2) / ((float) i));
            }
            return 0.0f;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public AppBarLayout b(List<View> list) {
            int size = list.size();
            for (int i = 0; i < size; i++) {
                View view = (View) list.get(i);
                if (view instanceof AppBarLayout) {
                    return (AppBarLayout) view;
                }
            }
            return null;
        }

        public /* bridge */ /* synthetic */ boolean a(int i) {
            return super.a(i);
        }

        public /* bridge */ /* synthetic */ boolean a(CoordinatorLayout coordinatorLayout, View view, int i) {
            return super.a(coordinatorLayout, view, i);
        }

        public /* bridge */ /* synthetic */ boolean a(CoordinatorLayout coordinatorLayout, View view, int i, int i2, int i3, int i4) {
            return super.a(coordinatorLayout, view, i, i2, i3, i4);
        }

        public boolean a(CoordinatorLayout coordinatorLayout, View view, Rect rect, boolean z) {
            AppBarLayout a2 = b(coordinatorLayout.c(view));
            if (a2 != null) {
                rect.offset(view.getLeft(), view.getTop());
                Rect rect2 = this.f396a;
                rect2.set(0, 0, coordinatorLayout.getWidth(), coordinatorLayout.getHeight());
                if (!rect2.contains(rect)) {
                    a2.a(false, !z);
                    return true;
                }
            }
            return false;
        }

        public boolean a(CoordinatorLayout coordinatorLayout, View view, View view2) {
            return view2 instanceof AppBarLayout;
        }

        public /* bridge */ /* synthetic */ int b() {
            return super.b();
        }

        /* access modifiers changed from: 0000 */
        public int b(View view) {
            return view instanceof AppBarLayout ? ((AppBarLayout) view).getTotalScrollRange() : super.b(view);
        }

        public boolean b(CoordinatorLayout coordinatorLayout, View view, View view2) {
            e(coordinatorLayout, view, view2);
            return false;
        }
    }

    public static class a extends LayoutParams {

        /* renamed from: a reason: collision with root package name */
        int f317a = 1;

        /* renamed from: b reason: collision with root package name */
        Interpolator f318b;

        public a(int i, int i2) {
            super(i, i2);
        }

        public a(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, j.AppBarLayout_Layout);
            this.f317a = obtainStyledAttributes.getInt(j.AppBarLayout_Layout_layout_scrollFlags, 0);
            if (obtainStyledAttributes.hasValue(j.AppBarLayout_Layout_layout_scrollInterpolator)) {
                this.f318b = AnimationUtils.loadInterpolator(context, obtainStyledAttributes.getResourceId(j.AppBarLayout_Layout_layout_scrollInterpolator, 0));
            }
            obtainStyledAttributes.recycle();
        }

        public a(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
        }

        public a(MarginLayoutParams marginLayoutParams) {
            super(marginLayoutParams);
        }

        public a(LayoutParams layoutParams) {
            super(layoutParams);
        }

        public int a() {
            return this.f317a;
        }

        public Interpolator b() {
            return this.f318b;
        }

        /* access modifiers changed from: 0000 */
        public boolean c() {
            return (this.f317a & 1) == 1 && (this.f317a & 10) != 0;
        }
    }

    public interface b {
        void a(AppBarLayout appBarLayout, int i);
    }

    public AppBarLayout(Context context) {
        this(context, null);
    }

    public AppBarLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f309a = -1;
        this.f310b = -1;
        this.c = -1;
        this.e = 0;
        setOrientation(1);
        p.a(context);
        if (VERSION.SDK_INT >= 21) {
            u.a(this);
            u.a(this, attributeSet, 0, i.Widget_Design_AppBarLayout);
        }
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, j.AppBarLayout, 0, i.Widget_Design_AppBarLayout);
        t.a((View) this, obtainStyledAttributes.getDrawable(j.AppBarLayout_android_background));
        if (obtainStyledAttributes.hasValue(j.AppBarLayout_expanded)) {
            a(obtainStyledAttributes.getBoolean(j.AppBarLayout_expanded, false), false, false);
        }
        if (VERSION.SDK_INT >= 21 && obtainStyledAttributes.hasValue(j.AppBarLayout_elevation)) {
            u.a(this, (float) obtainStyledAttributes.getDimensionPixelSize(j.AppBarLayout_elevation, 0));
        }
        if (VERSION.SDK_INT >= 26) {
            if (obtainStyledAttributes.hasValue(j.AppBarLayout_android_keyboardNavigationCluster)) {
                setKeyboardNavigationCluster(obtainStyledAttributes.getBoolean(j.AppBarLayout_android_keyboardNavigationCluster, false));
            }
            if (obtainStyledAttributes.hasValue(j.AppBarLayout_android_touchscreenBlocksFocus)) {
                setTouchscreenBlocksFocus(obtainStyledAttributes.getBoolean(j.AppBarLayout_android_touchscreenBlocksFocus, false));
            }
        }
        obtainStyledAttributes.recycle();
        t.a((View) this, (p) new p() {
            public ac a(View view, ac acVar) {
                return AppBarLayout.this.a(acVar);
            }
        });
    }

    private void a(boolean z, boolean z2, boolean z3) {
        int i2 = 0;
        int i3 = (z2 ? 4 : 0) | (z ? 1 : 2);
        if (z3) {
            i2 = 8;
        }
        this.e = i2 | i3;
        requestLayout();
    }

    private boolean b(boolean z) {
        if (this.h == z) {
            return false;
        }
        this.h = z;
        refreshDrawableState();
        return true;
    }

    private void e() {
        boolean z;
        int childCount = getChildCount();
        int i2 = 0;
        while (true) {
            if (i2 >= childCount) {
                z = false;
                break;
            } else if (((a) getChildAt(i2).getLayoutParams()).c()) {
                z = true;
                break;
            } else {
                i2++;
            }
        }
        b(z);
    }

    private void f() {
        this.f309a = -1;
        this.f310b = -1;
        this.c = -1;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public a generateDefaultLayoutParams() {
        return new a(-1, -2);
    }

    /* renamed from: a */
    public a generateLayoutParams(AttributeSet attributeSet) {
        return new a(getContext(), attributeSet);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public a generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return (VERSION.SDK_INT < 19 || !(layoutParams instanceof LayoutParams)) ? layoutParams instanceof MarginLayoutParams ? new a((MarginLayoutParams) layoutParams) : new a(layoutParams) : new a((LayoutParams) layoutParams);
    }

    /* access modifiers changed from: 0000 */
    public ac a(ac acVar) {
        ac acVar2 = null;
        if (t.o(this)) {
            acVar2 = acVar;
        }
        if (!android.support.v4.h.i.a(this.f, acVar2)) {
            this.f = acVar2;
            f();
        }
        return acVar;
    }

    /* access modifiers changed from: 0000 */
    public void a(int i2) {
        if (this.g != null) {
            int size = this.g.size();
            for (int i3 = 0; i3 < size; i3++) {
                b bVar = (b) this.g.get(i3);
                if (bVar != null) {
                    bVar.a(this, i2);
                }
            }
        }
    }

    public void a(boolean z, boolean z2) {
        a(z, z2, true);
    }

    /* access modifiers changed from: 0000 */
    public boolean a(boolean z) {
        if (this.i == z) {
            return false;
        }
        this.i = z;
        refreshDrawableState();
        return true;
    }

    /* access modifiers changed from: 0000 */
    public boolean b() {
        return this.d;
    }

    /* access modifiers changed from: 0000 */
    public boolean c() {
        return getTotalScrollRange() != 0;
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return layoutParams instanceof a;
    }

    /* access modifiers changed from: 0000 */
    public void d() {
        this.e = 0;
    }

    /* access modifiers changed from: 0000 */
    public int getDownNestedPreScrollRange() {
        int i2;
        if (this.f310b != -1) {
            return this.f310b;
        }
        int childCount = getChildCount() - 1;
        int i3 = 0;
        while (childCount >= 0) {
            View childAt = getChildAt(childCount);
            a aVar = (a) childAt.getLayoutParams();
            int measuredHeight = childAt.getMeasuredHeight();
            int i4 = aVar.f317a;
            if ((i4 & 5) == 5) {
                int i5 = aVar.bottomMargin + aVar.topMargin + i3;
                i2 = (i4 & 8) != 0 ? i5 + t.i(childAt) : (i4 & 2) != 0 ? i5 + (measuredHeight - t.i(childAt)) : i5 + (measuredHeight - getTopInset());
            } else if (i3 > 0) {
                break;
            } else {
                i2 = i3;
            }
            childCount--;
            i3 = i2;
        }
        int max = Math.max(0, i3);
        this.f310b = max;
        return max;
    }

    /* access modifiers changed from: 0000 */
    public int getDownNestedScrollRange() {
        int i2;
        if (this.c != -1) {
            return this.c;
        }
        int childCount = getChildCount();
        int i3 = 0;
        int i4 = 0;
        while (true) {
            if (i3 >= childCount) {
                break;
            }
            View childAt = getChildAt(i3);
            a aVar = (a) childAt.getLayoutParams();
            int measuredHeight = childAt.getMeasuredHeight() + aVar.topMargin + aVar.bottomMargin;
            int i5 = aVar.f317a;
            if ((i5 & 1) == 0) {
                break;
            }
            i4 += measuredHeight;
            if ((i5 & 2) != 0) {
                i2 = i4 - (t.i(childAt) + getTopInset());
                break;
            }
            i3++;
        }
        i2 = i4;
        int max = Math.max(0, i2);
        this.c = max;
        return max;
    }

    /* access modifiers changed from: 0000 */
    public final int getMinimumHeightForVisibleOverlappingContent() {
        int topInset = getTopInset();
        int i2 = t.i(this);
        if (i2 != 0) {
            return (i2 * 2) + topInset;
        }
        int childCount = getChildCount();
        int i3 = childCount >= 1 ? t.i(getChildAt(childCount - 1)) : 0;
        return i3 != 0 ? (i3 * 2) + topInset : getHeight() / 3;
    }

    /* access modifiers changed from: 0000 */
    public int getPendingAction() {
        return this.e;
    }

    @Deprecated
    public float getTargetElevation() {
        return 0.0f;
    }

    /* access modifiers changed from: 0000 */
    public final int getTopInset() {
        if (this.f != null) {
            return this.f.b();
        }
        return 0;
    }

    public final int getTotalScrollRange() {
        int i2;
        if (this.f309a != -1) {
            return this.f309a;
        }
        int childCount = getChildCount();
        int i3 = 0;
        int i4 = 0;
        while (true) {
            if (i3 >= childCount) {
                break;
            }
            View childAt = getChildAt(i3);
            a aVar = (a) childAt.getLayoutParams();
            int measuredHeight = childAt.getMeasuredHeight();
            int i5 = aVar.f317a;
            if ((i5 & 1) == 0) {
                break;
            }
            i4 += aVar.bottomMargin + measuredHeight + aVar.topMargin;
            if ((i5 & 2) != 0) {
                i2 = i4 - t.i(childAt);
                break;
            }
            i3++;
        }
        i2 = i4;
        int max = Math.max(0, i2 - getTopInset());
        this.f309a = max;
        return max;
    }

    /* access modifiers changed from: 0000 */
    public int getUpNestedPreScrollRange() {
        return getTotalScrollRange();
    }

    /* access modifiers changed from: protected */
    public int[] onCreateDrawableState(int i2) {
        if (this.j == null) {
            this.j = new int[2];
        }
        int[] iArr = this.j;
        int[] onCreateDrawableState = super.onCreateDrawableState(iArr.length + i2);
        iArr[0] = this.h ? android.support.design.a.b.state_collapsible : -android.support.design.a.b.state_collapsible;
        iArr[1] = (!this.h || !this.i) ? -android.support.design.a.b.state_collapsed : android.support.design.a.b.state_collapsed;
        return mergeDrawableStates(onCreateDrawableState, iArr);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        super.onLayout(z, i2, i3, i4, i5);
        f();
        this.d = false;
        int childCount = getChildCount();
        int i6 = 0;
        while (true) {
            if (i6 >= childCount) {
                break;
            } else if (((a) getChildAt(i6).getLayoutParams()).b() != null) {
                this.d = true;
                break;
            } else {
                i6++;
            }
        }
        e();
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
        f();
    }

    public void setExpanded(boolean z) {
        a(z, t.v(this));
    }

    public void setOrientation(int i2) {
        if (i2 != 1) {
            throw new IllegalArgumentException("AppBarLayout is always vertical and does not support horizontal orientation");
        }
        super.setOrientation(i2);
    }

    @Deprecated
    public void setTargetElevation(float f2) {
        if (VERSION.SDK_INT >= 21) {
            u.a(this, f2);
        }
    }
}
