package android.support.design.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.ClassLoaderCreator;
import android.os.Parcelable.Creator;
import android.support.design.a.i;
import android.support.design.a.j;
import android.support.design.internal.d;
import android.support.v4.i.t;
import android.support.v7.a.a.C0026a;
import android.support.v7.view.g;
import android.support.v7.view.menu.h;
import android.support.v7.view.menu.o;
import android.support.v7.widget.bo;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;

public class BottomNavigationView extends FrameLayout {

    /* renamed from: a reason: collision with root package name */
    private static final int[] f319a = {16842912};

    /* renamed from: b reason: collision with root package name */
    private static final int[] f320b = {-16842910};
    private final h c;
    private final android.support.design.internal.c d;
    private final d e;
    private MenuInflater f;
    /* access modifiers changed from: private */
    public b g;
    /* access modifiers changed from: private */
    public a h;

    public interface a {
        void a(MenuItem menuItem);
    }

    public interface b {
        boolean a(MenuItem menuItem);
    }

    static class c extends android.support.v4.i.a {
        public static final Creator<c> CREATOR = new ClassLoaderCreator<c>() {
            /* renamed from: a */
            public c createFromParcel(Parcel parcel) {
                return new c(parcel, null);
            }

            /* renamed from: a */
            public c createFromParcel(Parcel parcel, ClassLoader classLoader) {
                return new c(parcel, classLoader);
            }

            /* renamed from: a */
            public c[] newArray(int i) {
                return new c[i];
            }
        };

        /* renamed from: a reason: collision with root package name */
        Bundle f322a;

        public c(Parcel parcel, ClassLoader classLoader) {
            super(parcel, classLoader);
            a(parcel, classLoader);
        }

        public c(Parcelable parcelable) {
            super(parcelable);
        }

        private void a(Parcel parcel, ClassLoader classLoader) {
            this.f322a = parcel.readBundle(classLoader);
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeBundle(this.f322a);
        }
    }

    public BottomNavigationView(Context context) {
        this(context, null);
    }

    public BottomNavigationView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public BottomNavigationView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.e = new d();
        p.a(context);
        this.c = new android.support.design.internal.b(context);
        this.d = new android.support.design.internal.c(context);
        LayoutParams layoutParams = new LayoutParams(-2, -2);
        layoutParams.gravity = 17;
        this.d.setLayoutParams(layoutParams);
        this.e.a(this.d);
        this.e.a(1);
        this.d.setPresenter(this.e);
        this.c.a((o) this.e);
        this.e.a(getContext(), this.c);
        bo a2 = bo.a(context, attributeSet, j.BottomNavigationView, i, i.Widget_Design_BottomNavigationView);
        if (a2.g(j.BottomNavigationView_itemIconTint)) {
            this.d.setIconTintList(a2.e(j.BottomNavigationView_itemIconTint));
        } else {
            this.d.setIconTintList(b(16842808));
        }
        if (a2.g(j.BottomNavigationView_itemTextColor)) {
            this.d.setItemTextColor(a2.e(j.BottomNavigationView_itemTextColor));
        } else {
            this.d.setItemTextColor(b(16842808));
        }
        if (a2.g(j.BottomNavigationView_elevation)) {
            t.a((View) this, (float) a2.e(j.BottomNavigationView_elevation, 0));
        }
        this.d.setItemBackgroundRes(a2.g(j.BottomNavigationView_itemBackground, 0));
        if (a2.g(j.BottomNavigationView_menu)) {
            a(a2.g(j.BottomNavigationView_menu, 0));
        }
        a2.a();
        addView(this.d, layoutParams);
        if (VERSION.SDK_INT < 21) {
            a(context);
        }
        this.c.a((android.support.v7.view.menu.h.a) new android.support.v7.view.menu.h.a() {
            public void a(h hVar) {
            }

            public boolean a(h hVar, MenuItem menuItem) {
                if (BottomNavigationView.this.h == null || menuItem.getItemId() != BottomNavigationView.this.getSelectedItemId()) {
                    return BottomNavigationView.this.g != null && !BottomNavigationView.this.g.a(menuItem);
                }
                BottomNavigationView.this.h.a(menuItem);
                return true;
            }
        });
    }

    private void a(Context context) {
        View view = new View(context);
        view.setBackgroundColor(android.support.v4.b.a.c(context, android.support.design.a.c.design_bottom_navigation_shadow_color));
        view.setLayoutParams(new LayoutParams(-1, getResources().getDimensionPixelSize(android.support.design.a.d.design_bottom_navigation_shadow_height)));
        addView(view);
    }

    private ColorStateList b(int i) {
        TypedValue typedValue = new TypedValue();
        if (!getContext().getTheme().resolveAttribute(i, typedValue, true)) {
            return null;
        }
        ColorStateList a2 = android.support.v7.c.a.b.a(getContext(), typedValue.resourceId);
        if (!getContext().getTheme().resolveAttribute(C0026a.colorPrimary, typedValue, true)) {
            return null;
        }
        int i2 = typedValue.data;
        int defaultColor = a2.getDefaultColor();
        return new ColorStateList(new int[][]{f320b, f319a, EMPTY_STATE_SET}, new int[]{a2.getColorForState(f320b, defaultColor), i2, defaultColor});
    }

    private MenuInflater getMenuInflater() {
        if (this.f == null) {
            this.f = new g(getContext());
        }
        return this.f;
    }

    public void a(int i) {
        this.e.b(true);
        getMenuInflater().inflate(i, this.c);
        this.e.b(false);
        this.e.a(true);
    }

    public int getItemBackgroundResource() {
        return this.d.getItemBackgroundRes();
    }

    public ColorStateList getItemIconTintList() {
        return this.d.getIconTintList();
    }

    public ColorStateList getItemTextColor() {
        return this.d.getItemTextColor();
    }

    public int getMaxItemCount() {
        return 5;
    }

    public Menu getMenu() {
        return this.c;
    }

    public int getSelectedItemId() {
        return this.d.getSelectedItemId();
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (!(parcelable instanceof c)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        c cVar = (c) parcelable;
        super.onRestoreInstanceState(cVar.a());
        this.c.b(cVar.f322a);
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        c cVar = new c(super.onSaveInstanceState());
        cVar.f322a = new Bundle();
        this.c.a(cVar.f322a);
        return cVar;
    }

    public void setItemBackgroundResource(int i) {
        this.d.setItemBackgroundRes(i);
    }

    public void setItemIconTintList(ColorStateList colorStateList) {
        this.d.setIconTintList(colorStateList);
    }

    public void setItemTextColor(ColorStateList colorStateList) {
        this.d.setItemTextColor(colorStateList);
    }

    public void setOnNavigationItemReselectedListener(a aVar) {
        this.h = aVar;
    }

    public void setOnNavigationItemSelectedListener(b bVar) {
        this.g = bVar;
    }

    public void setSelectedItemId(int i) {
        MenuItem findItem = this.c.findItem(i);
        if (findItem != null && !this.c.a(findItem, (o) this.e, 0)) {
            findItem.setChecked(true);
        }
    }
}
