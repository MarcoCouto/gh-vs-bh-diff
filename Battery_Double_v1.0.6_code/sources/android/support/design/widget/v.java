package android.support.design.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageButton;

class v extends ImageButton {

    /* renamed from: a reason: collision with root package name */
    private int f439a;

    public v(Context context) {
        this(context, null);
    }

    public v(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public v(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f439a = getVisibility();
    }

    /* access modifiers changed from: 0000 */
    public final void a(int i, boolean z) {
        super.setVisibility(i);
        if (z) {
            this.f439a = i;
        }
    }

    /* access modifiers changed from: 0000 */
    public final int getUserSetVisibility() {
        return this.f439a;
    }

    public void setVisibility(int i) {
        a(i, true);
    }
}
