package android.support.design.widget;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Region.Op;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.ClassLoaderCreator;
import android.os.Parcelable.Creator;
import android.os.SystemClock;
import android.support.design.a.i;
import android.support.design.a.j;
import android.support.v4.i.ac;
import android.support.v4.i.n;
import android.support.v4.i.o;
import android.support.v4.i.p;
import android.support.v4.i.t;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.BaseSavedState;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.ViewGroup.OnHierarchyChangeListener;
import android.view.ViewParent;
import android.view.ViewTreeObserver.OnPreDrawListener;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CoordinatorLayout extends ViewGroup implements n {

    /* renamed from: a reason: collision with root package name */
    static final String f332a;

    /* renamed from: b reason: collision with root package name */
    static final Class<?>[] f333b = {Context.class, AttributeSet.class};
    static final ThreadLocal<Map<String, Constructor<a>>> c = new ThreadLocal<>();
    static final Comparator<View> d;
    private static final android.support.v4.h.k.a<Rect> f = new android.support.v4.h.k.c(12);
    OnHierarchyChangeListener e;
    private final List<View> g;
    private final e<View> h;
    private final List<View> i;
    private final List<View> j;
    private final int[] k;
    private Paint l;
    private boolean m;
    private boolean n;
    private int[] o;
    private View p;
    private View q;
    private e r;
    private boolean s;
    private ac t;
    private boolean u;
    private Drawable v;
    private p w;
    private final o x;

    public static abstract class a<V extends View> {
        public a() {
        }

        public a(Context context, AttributeSet attributeSet) {
        }

        public ac a(CoordinatorLayout coordinatorLayout, V v, ac acVar) {
            return acVar;
        }

        public void a(d dVar) {
        }

        public void a(CoordinatorLayout coordinatorLayout, V v, Parcelable parcelable) {
        }

        public void a(CoordinatorLayout coordinatorLayout, V v, View view, int i) {
            if (i == 0) {
                c(coordinatorLayout, v, view);
            }
        }

        @Deprecated
        public void a(CoordinatorLayout coordinatorLayout, V v, View view, int i, int i2, int i3, int i4) {
        }

        public void a(CoordinatorLayout coordinatorLayout, V v, View view, int i, int i2, int i3, int i4, int i5) {
            if (i5 == 0) {
                a(coordinatorLayout, v, view, i, i2, i3, i4);
            }
        }

        @Deprecated
        public void a(CoordinatorLayout coordinatorLayout, V v, View view, int i, int i2, int[] iArr) {
        }

        public void a(CoordinatorLayout coordinatorLayout, V v, View view, int i, int i2, int[] iArr, int i3) {
            if (i3 == 0) {
                a(coordinatorLayout, v, view, i, i2, iArr);
            }
        }

        public boolean a(CoordinatorLayout coordinatorLayout, V v, int i) {
            return false;
        }

        public boolean a(CoordinatorLayout coordinatorLayout, V v, int i, int i2, int i3, int i4) {
            return false;
        }

        public boolean a(CoordinatorLayout coordinatorLayout, V v, Rect rect) {
            return false;
        }

        public boolean a(CoordinatorLayout coordinatorLayout, V v, Rect rect, boolean z) {
            return false;
        }

        public boolean a(CoordinatorLayout coordinatorLayout, V v, MotionEvent motionEvent) {
            return false;
        }

        public boolean a(CoordinatorLayout coordinatorLayout, V v, View view) {
            return false;
        }

        public boolean a(CoordinatorLayout coordinatorLayout, V v, View view, float f, float f2) {
            return false;
        }

        public boolean a(CoordinatorLayout coordinatorLayout, V v, View view, float f, float f2, boolean z) {
            return false;
        }

        @Deprecated
        public boolean a(CoordinatorLayout coordinatorLayout, V v, View view, View view2, int i) {
            return false;
        }

        public boolean a(CoordinatorLayout coordinatorLayout, V v, View view, View view2, int i, int i2) {
            if (i2 == 0) {
                return a(coordinatorLayout, v, view, view2, i);
            }
            return false;
        }

        public Parcelable b(CoordinatorLayout coordinatorLayout, V v) {
            return BaseSavedState.EMPTY_STATE;
        }

        @Deprecated
        public void b(CoordinatorLayout coordinatorLayout, V v, View view, View view2, int i) {
        }

        public void b(CoordinatorLayout coordinatorLayout, V v, View view, View view2, int i, int i2) {
            if (i2 == 0) {
                b(coordinatorLayout, v, view, view2, i);
            }
        }

        public boolean b(CoordinatorLayout coordinatorLayout, V v, MotionEvent motionEvent) {
            return false;
        }

        public boolean b(CoordinatorLayout coordinatorLayout, V v, View view) {
            return false;
        }

        public int c(CoordinatorLayout coordinatorLayout, V v) {
            return -16777216;
        }

        public void c() {
        }

        @Deprecated
        public void c(CoordinatorLayout coordinatorLayout, V v, View view) {
        }

        public float d(CoordinatorLayout coordinatorLayout, V v) {
            return 0.0f;
        }

        public void d(CoordinatorLayout coordinatorLayout, V v, View view) {
        }

        public boolean e(CoordinatorLayout coordinatorLayout, V v) {
            return d(coordinatorLayout, v) > 0.0f;
        }
    }

    @Retention(RetentionPolicy.RUNTIME)
    public @interface b {
        Class<? extends a> a();
    }

    private class c implements OnHierarchyChangeListener {
        c() {
        }

        public void onChildViewAdded(View view, View view2) {
            if (CoordinatorLayout.this.e != null) {
                CoordinatorLayout.this.e.onChildViewAdded(view, view2);
            }
        }

        public void onChildViewRemoved(View view, View view2) {
            CoordinatorLayout.this.a(2);
            if (CoordinatorLayout.this.e != null) {
                CoordinatorLayout.this.e.onChildViewRemoved(view, view2);
            }
        }
    }

    public static class d extends MarginLayoutParams {

        /* renamed from: a reason: collision with root package name */
        a f336a;

        /* renamed from: b reason: collision with root package name */
        boolean f337b = false;
        public int c = 0;
        public int d = 0;
        public int e = -1;
        int f = -1;
        public int g = 0;
        public int h = 0;
        int i;
        int j;
        View k;
        View l;
        final Rect m = new Rect();
        Object n;
        private boolean o;
        private boolean p;
        private boolean q;
        private boolean r;

        public d(int i2, int i3) {
            super(i2, i3);
        }

        d(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, j.CoordinatorLayout_Layout);
            this.c = obtainStyledAttributes.getInteger(j.CoordinatorLayout_Layout_android_layout_gravity, 0);
            this.f = obtainStyledAttributes.getResourceId(j.CoordinatorLayout_Layout_layout_anchor, -1);
            this.d = obtainStyledAttributes.getInteger(j.CoordinatorLayout_Layout_layout_anchorGravity, 0);
            this.e = obtainStyledAttributes.getInteger(j.CoordinatorLayout_Layout_layout_keyline, -1);
            this.g = obtainStyledAttributes.getInt(j.CoordinatorLayout_Layout_layout_insetEdge, 0);
            this.h = obtainStyledAttributes.getInt(j.CoordinatorLayout_Layout_layout_dodgeInsetEdges, 0);
            this.f337b = obtainStyledAttributes.hasValue(j.CoordinatorLayout_Layout_layout_behavior);
            if (this.f337b) {
                this.f336a = CoordinatorLayout.a(context, attributeSet, obtainStyledAttributes.getString(j.CoordinatorLayout_Layout_layout_behavior));
            }
            obtainStyledAttributes.recycle();
            if (this.f336a != null) {
                this.f336a.a(this);
            }
        }

        public d(d dVar) {
            super(dVar);
        }

        public d(LayoutParams layoutParams) {
            super(layoutParams);
        }

        public d(MarginLayoutParams marginLayoutParams) {
            super(marginLayoutParams);
        }

        private void a(View view, CoordinatorLayout coordinatorLayout) {
            this.k = coordinatorLayout.findViewById(this.f);
            if (this.k != null) {
                if (this.k != coordinatorLayout) {
                    View view2 = this.k;
                    ViewParent parent = this.k.getParent();
                    while (parent != coordinatorLayout && parent != null) {
                        if (parent != view) {
                            if (parent instanceof View) {
                                view2 = (View) parent;
                            }
                            parent = parent.getParent();
                        } else if (coordinatorLayout.isInEditMode()) {
                            this.l = null;
                            this.k = null;
                            return;
                        } else {
                            throw new IllegalStateException("Anchor must not be a descendant of the anchored view");
                        }
                    }
                    this.l = view2;
                } else if (coordinatorLayout.isInEditMode()) {
                    this.l = null;
                    this.k = null;
                } else {
                    throw new IllegalStateException("View can not be anchored to the the parent CoordinatorLayout");
                }
            } else if (coordinatorLayout.isInEditMode()) {
                this.l = null;
                this.k = null;
            } else {
                throw new IllegalStateException("Could not find CoordinatorLayout descendant view with id " + coordinatorLayout.getResources().getResourceName(this.f) + " to anchor view " + view);
            }
        }

        private boolean a(View view, int i2) {
            int a2 = android.support.v4.i.e.a(((d) view.getLayoutParams()).g, i2);
            return a2 != 0 && (android.support.v4.i.e.a(this.h, i2) & a2) == a2;
        }

        private boolean b(View view, CoordinatorLayout coordinatorLayout) {
            if (this.k.getId() != this.f) {
                return false;
            }
            View view2 = this.k;
            for (ViewParent parent = this.k.getParent(); parent != coordinatorLayout; parent = parent.getParent()) {
                if (parent == null || parent == view) {
                    this.l = null;
                    this.k = null;
                    return false;
                }
                if (parent instanceof View) {
                    view2 = (View) parent;
                }
            }
            this.l = view2;
            return true;
        }

        public int a() {
            return this.f;
        }

        /* access modifiers changed from: 0000 */
        public void a(int i2) {
            a(i2, false);
        }

        /* access modifiers changed from: 0000 */
        public void a(int i2, boolean z) {
            switch (i2) {
                case 0:
                    this.p = z;
                    return;
                case 1:
                    this.q = z;
                    return;
                default:
                    return;
            }
        }

        /* access modifiers changed from: 0000 */
        public void a(Rect rect) {
            this.m.set(rect);
        }

        public void a(a aVar) {
            if (this.f336a != aVar) {
                if (this.f336a != null) {
                    this.f336a.c();
                }
                this.f336a = aVar;
                this.n = null;
                this.f337b = true;
                if (aVar != null) {
                    aVar.a(this);
                }
            }
        }

        /* access modifiers changed from: 0000 */
        public void a(boolean z) {
            this.r = z;
        }

        /* access modifiers changed from: 0000 */
        public boolean a(CoordinatorLayout coordinatorLayout, View view) {
            if (this.o) {
                return true;
            }
            boolean e2 = (this.f336a != null ? this.f336a.e(coordinatorLayout, view) : false) | this.o;
            this.o = e2;
            return e2;
        }

        /* access modifiers changed from: 0000 */
        public boolean a(CoordinatorLayout coordinatorLayout, View view, View view2) {
            return view2 == this.l || a(view2, t.e(coordinatorLayout)) || (this.f336a != null && this.f336a.a(coordinatorLayout, view, view2));
        }

        public a b() {
            return this.f336a;
        }

        /* access modifiers changed from: 0000 */
        public View b(CoordinatorLayout coordinatorLayout, View view) {
            if (this.f == -1) {
                this.l = null;
                this.k = null;
                return null;
            }
            if (this.k == null || !b(view, coordinatorLayout)) {
                a(view, coordinatorLayout);
            }
            return this.k;
        }

        /* access modifiers changed from: 0000 */
        public boolean b(int i2) {
            switch (i2) {
                case 0:
                    return this.p;
                case 1:
                    return this.q;
                default:
                    return false;
            }
        }

        /* access modifiers changed from: 0000 */
        public Rect c() {
            return this.m;
        }

        /* access modifiers changed from: 0000 */
        public boolean d() {
            return this.k == null && this.f != -1;
        }

        /* access modifiers changed from: 0000 */
        public boolean e() {
            if (this.f336a == null) {
                this.o = false;
            }
            return this.o;
        }

        /* access modifiers changed from: 0000 */
        public void f() {
            this.o = false;
        }

        /* access modifiers changed from: 0000 */
        public boolean g() {
            return this.r;
        }

        /* access modifiers changed from: 0000 */
        public void h() {
            this.r = false;
        }
    }

    class e implements OnPreDrawListener {
        e() {
        }

        public boolean onPreDraw() {
            CoordinatorLayout.this.a(0);
            return true;
        }
    }

    protected static class f extends android.support.v4.i.a {
        public static final Creator<f> CREATOR = new ClassLoaderCreator<f>() {
            /* renamed from: a */
            public f createFromParcel(Parcel parcel) {
                return new f(parcel, null);
            }

            /* renamed from: a */
            public f createFromParcel(Parcel parcel, ClassLoader classLoader) {
                return new f(parcel, classLoader);
            }

            /* renamed from: a */
            public f[] newArray(int i) {
                return new f[i];
            }
        };

        /* renamed from: a reason: collision with root package name */
        SparseArray<Parcelable> f339a;

        public f(Parcel parcel, ClassLoader classLoader) {
            super(parcel, classLoader);
            int readInt = parcel.readInt();
            int[] iArr = new int[readInt];
            parcel.readIntArray(iArr);
            Parcelable[] readParcelableArray = parcel.readParcelableArray(classLoader);
            this.f339a = new SparseArray<>(readInt);
            for (int i = 0; i < readInt; i++) {
                this.f339a.append(iArr[i], readParcelableArray[i]);
            }
        }

        public f(Parcelable parcelable) {
            super(parcelable);
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            int i2 = this.f339a != null ? this.f339a.size() : 0;
            parcel.writeInt(i2);
            int[] iArr = new int[i2];
            Parcelable[] parcelableArr = new Parcelable[i2];
            for (int i3 = 0; i3 < i2; i3++) {
                iArr[i3] = this.f339a.keyAt(i3);
                parcelableArr[i3] = (Parcelable) this.f339a.valueAt(i3);
            }
            parcel.writeIntArray(iArr);
            parcel.writeParcelableArray(parcelableArr, i);
        }
    }

    static class g implements Comparator<View> {
        g() {
        }

        /* renamed from: a */
        public int compare(View view, View view2) {
            float w = t.w(view);
            float w2 = t.w(view2);
            if (w > w2) {
                return -1;
            }
            return w < w2 ? 1 : 0;
        }
    }

    static {
        Package packageR = CoordinatorLayout.class.getPackage();
        f332a = packageR != null ? packageR.getName() : null;
        if (VERSION.SDK_INT >= 21) {
            d = new g();
        } else {
            d = null;
        }
    }

    public CoordinatorLayout(Context context) {
        this(context, null);
    }

    public CoordinatorLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public CoordinatorLayout(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.g = new ArrayList();
        this.h = new e<>();
        this.i = new ArrayList();
        this.j = new ArrayList();
        this.k = new int[2];
        this.x = new o(this);
        p.a(context);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, j.CoordinatorLayout, i2, i.Widget_Design_CoordinatorLayout);
        int resourceId = obtainStyledAttributes.getResourceId(j.CoordinatorLayout_keylines, 0);
        if (resourceId != 0) {
            Resources resources = context.getResources();
            this.o = resources.getIntArray(resourceId);
            float f2 = resources.getDisplayMetrics().density;
            int length = this.o.length;
            for (int i3 = 0; i3 < length; i3++) {
                this.o[i3] = (int) (((float) this.o[i3]) * f2);
            }
        }
        this.v = obtainStyledAttributes.getDrawable(j.CoordinatorLayout_statusBarBackground);
        obtainStyledAttributes.recycle();
        g();
        super.setOnHierarchyChangeListener(new c());
    }

    static a a(Context context, AttributeSet attributeSet, String str) {
        Map map;
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        if (str.startsWith(".")) {
            str = context.getPackageName() + str;
        } else if (str.indexOf(46) < 0 && !TextUtils.isEmpty(f332a)) {
            str = f332a + '.' + str;
        }
        try {
            Map map2 = (Map) c.get();
            if (map2 == null) {
                HashMap hashMap = new HashMap();
                c.set(hashMap);
                map = hashMap;
            } else {
                map = map2;
            }
            Constructor constructor = (Constructor) map.get(str);
            if (constructor == null) {
                constructor = Class.forName(str, true, context.getClassLoader()).getConstructor(f333b);
                constructor.setAccessible(true);
                map.put(str, constructor);
            }
            return (a) constructor.newInstance(new Object[]{context, attributeSet});
        } catch (Exception e2) {
            throw new RuntimeException("Could not inflate Behavior subclass " + str, e2);
        }
    }

    private static void a(Rect rect) {
        rect.setEmpty();
        f.a(rect);
    }

    private void a(d dVar, Rect rect, int i2, int i3) {
        int width = getWidth();
        int height = getHeight();
        int max = Math.max(getPaddingLeft() + dVar.leftMargin, Math.min(rect.left, ((width - getPaddingRight()) - i2) - dVar.rightMargin));
        int max2 = Math.max(getPaddingTop() + dVar.topMargin, Math.min(rect.top, ((height - getPaddingBottom()) - i3) - dVar.bottomMargin));
        rect.set(max, max2, max + i2, max2 + i3);
    }

    private void a(View view, int i2, Rect rect, Rect rect2, d dVar, int i3, int i4) {
        int width;
        int height;
        int a2 = android.support.v4.i.e.a(e(dVar.c), i2);
        int a3 = android.support.v4.i.e.a(c(dVar.d), i2);
        int i5 = a2 & 7;
        int i6 = a2 & 112;
        int i7 = a3 & 112;
        switch (a3 & 7) {
            case 1:
                width = (rect.width() / 2) + rect.left;
                break;
            case 5:
                width = rect.right;
                break;
            default:
                width = rect.left;
                break;
        }
        switch (i7) {
            case 16:
                height = rect.top + (rect.height() / 2);
                break;
            case 80:
                height = rect.bottom;
                break;
            default:
                height = rect.top;
                break;
        }
        switch (i5) {
            case 1:
                width -= i3 / 2;
                break;
            case 5:
                break;
            default:
                width -= i3;
                break;
        }
        switch (i6) {
            case 16:
                height -= i4 / 2;
                break;
            case 80:
                break;
            default:
                height -= i4;
                break;
        }
        rect2.set(width, height, width + i3, height + i4);
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x00a9  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00c4  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00cc  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00e5  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00ff  */
    private void a(View view, Rect rect, int i2) {
        boolean z;
        boolean z2;
        boolean z3;
        if (t.v(view) && view.getWidth() > 0 && view.getHeight() > 0) {
            d dVar = (d) view.getLayoutParams();
            a b2 = dVar.b();
            Rect e2 = e();
            Rect e3 = e();
            e3.set(view.getLeft(), view.getTop(), view.getRight(), view.getBottom());
            if (b2 == null || !b2.a(this, view, e2)) {
                e2.set(e3);
            } else if (!e3.contains(e2)) {
                throw new IllegalArgumentException("Rect should be within the child's bounds. Rect:" + e2.toShortString() + " | Bounds:" + e3.toShortString());
            }
            a(e3);
            if (e2.isEmpty()) {
                a(e2);
                return;
            }
            int a2 = android.support.v4.i.e.a(dVar.h, i2);
            if ((a2 & 48) == 48) {
                int i3 = (e2.top - dVar.topMargin) - dVar.j;
                if (i3 < rect.top) {
                    f(view, rect.top - i3);
                    z = true;
                    if ((a2 & 80) == 80) {
                        int height = ((getHeight() - e2.bottom) - dVar.bottomMargin) + dVar.j;
                        if (height < rect.bottom) {
                            f(view, height - rect.bottom);
                            z = true;
                        }
                    }
                    if (!z) {
                        f(view, 0);
                    }
                    if ((a2 & 3) == 3) {
                        int i4 = (e2.left - dVar.leftMargin) - dVar.i;
                        if (i4 < rect.left) {
                            e(view, rect.left - i4);
                            z2 = true;
                            if ((a2 & 5) == 5) {
                                int width = dVar.i + ((getWidth() - e2.right) - dVar.rightMargin);
                                if (width < rect.right) {
                                    e(view, width - rect.right);
                                    z3 = true;
                                    if (!z3) {
                                        e(view, 0);
                                    }
                                    a(e2);
                                }
                            }
                            z3 = z2;
                            if (!z3) {
                            }
                            a(e2);
                        }
                    }
                    z2 = false;
                    if ((a2 & 5) == 5) {
                    }
                    z3 = z2;
                    if (!z3) {
                    }
                    a(e2);
                }
            }
            z = false;
            if ((a2 & 80) == 80) {
            }
            if (!z) {
            }
            if ((a2 & 3) == 3) {
            }
            z2 = false;
            if ((a2 & 5) == 5) {
            }
            z3 = z2;
            if (!z3) {
            }
            a(e2);
        }
    }

    private void a(View view, View view2, int i2) {
        d dVar = (d) view.getLayoutParams();
        Rect e2 = e();
        Rect e3 = e();
        try {
            a(view2, e2);
            a(view, i2, e2, e3);
            view.layout(e3.left, e3.top, e3.right, e3.bottom);
        } finally {
            a(e2);
            a(e3);
        }
    }

    private void a(List<View> list) {
        list.clear();
        boolean isChildrenDrawingOrderEnabled = isChildrenDrawingOrderEnabled();
        int childCount = getChildCount();
        int i2 = childCount - 1;
        while (i2 >= 0) {
            list.add(getChildAt(isChildrenDrawingOrderEnabled ? getChildDrawingOrder(childCount, i2) : i2));
            i2--;
        }
        if (d != null) {
            Collections.sort(list, d);
        }
    }

    private void a(boolean z) {
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = getChildAt(i2);
            a b2 = ((d) childAt.getLayoutParams()).b();
            if (b2 != null) {
                long uptimeMillis = SystemClock.uptimeMillis();
                MotionEvent obtain = MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, 0.0f, 0.0f, 0);
                if (z) {
                    b2.a(this, childAt, obtain);
                } else {
                    b2.b(this, childAt, obtain);
                }
                obtain.recycle();
            }
        }
        for (int i3 = 0; i3 < childCount; i3++) {
            ((d) getChildAt(i3).getLayoutParams()).f();
        }
        this.m = false;
    }

    private boolean a(MotionEvent motionEvent, int i2) {
        boolean z;
        boolean z2;
        MotionEvent motionEvent2;
        boolean z3 = false;
        boolean z4 = false;
        MotionEvent motionEvent3 = null;
        int actionMasked = motionEvent.getActionMasked();
        List<View> list = this.i;
        a(list);
        int size = list.size();
        int i3 = 0;
        while (true) {
            if (i3 < size) {
                View view = (View) list.get(i3);
                d dVar = (d) view.getLayoutParams();
                a b2 = dVar.b();
                if ((!z3 && !z4) || actionMasked == 0) {
                    if (!z3 && b2 != null) {
                        switch (i2) {
                            case 0:
                                z3 = b2.a(this, view, motionEvent);
                                break;
                            case 1:
                                z3 = b2.b(this, view, motionEvent);
                                break;
                        }
                        if (z3) {
                            this.p = view;
                        }
                    }
                    z = z3;
                    boolean e2 = dVar.e();
                    boolean a2 = dVar.a(this, view);
                    boolean z5 = a2 && !e2;
                    if (!a2 || z5) {
                        MotionEvent motionEvent4 = motionEvent3;
                        z2 = z5;
                        motionEvent2 = motionEvent4;
                    }
                } else if (b2 != null) {
                    if (motionEvent3 == null) {
                        long uptimeMillis = SystemClock.uptimeMillis();
                        motionEvent2 = MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, 0.0f, 0.0f, 0);
                    } else {
                        motionEvent2 = motionEvent3;
                    }
                    switch (i2) {
                        case 0:
                            b2.a(this, view, motionEvent2);
                            break;
                        case 1:
                            b2.b(this, view, motionEvent2);
                            break;
                    }
                    z2 = z4;
                    z = z3;
                } else {
                    motionEvent2 = motionEvent3;
                    z = z3;
                    z2 = z4;
                }
                i3++;
                z4 = z2;
                z3 = z;
                motionEvent3 = motionEvent2;
            } else {
                z = z3;
            }
        }
        list.clear();
        return z;
    }

    private int b(int i2) {
        if (this.o == null) {
            Log.e("CoordinatorLayout", "No keylines defined for " + this + " - attempted index lookup " + i2);
            return 0;
        } else if (i2 >= 0 && i2 < this.o.length) {
            return this.o[i2];
        } else {
            Log.e("CoordinatorLayout", "Keyline index " + i2 + " out of range for " + this);
            return 0;
        }
    }

    private ac b(ac acVar) {
        ac acVar2;
        if (acVar.e()) {
            return acVar;
        }
        int childCount = getChildCount();
        int i2 = 0;
        ac acVar3 = acVar;
        while (true) {
            if (i2 >= childCount) {
                acVar2 = acVar3;
                break;
            }
            View childAt = getChildAt(i2);
            if (t.o(childAt)) {
                a b2 = ((d) childAt.getLayoutParams()).b();
                if (b2 != null) {
                    acVar2 = b2.a(this, childAt, acVar3);
                    if (acVar2.e()) {
                        break;
                    }
                    i2++;
                    acVar3 = acVar2;
                }
            }
            acVar2 = acVar3;
            i2++;
            acVar3 = acVar2;
        }
        return acVar2;
    }

    private void b(View view, int i2, int i3) {
        d dVar = (d) view.getLayoutParams();
        int a2 = android.support.v4.i.e.a(d(dVar.c), i3);
        int i4 = a2 & 7;
        int i5 = a2 & 112;
        int width = getWidth();
        int height = getHeight();
        int measuredWidth = view.getMeasuredWidth();
        int measuredHeight = view.getMeasuredHeight();
        if (i3 == 1) {
            i2 = width - i2;
        }
        int b2 = b(i2) - measuredWidth;
        int i6 = 0;
        switch (i4) {
            case 1:
                b2 += measuredWidth / 2;
                break;
            case 5:
                b2 += measuredWidth;
                break;
        }
        switch (i5) {
            case 16:
                i6 = 0 + (measuredHeight / 2);
                break;
            case 80:
                i6 = 0 + measuredHeight;
                break;
        }
        int max = Math.max(getPaddingLeft() + dVar.leftMargin, Math.min(b2, ((width - getPaddingRight()) - measuredWidth) - dVar.rightMargin));
        int max2 = Math.max(getPaddingTop() + dVar.topMargin, Math.min(i6, ((height - getPaddingBottom()) - measuredHeight) - dVar.bottomMargin));
        view.layout(max, max2, max + measuredWidth, max2 + measuredHeight);
    }

    private static int c(int i2) {
        int i3 = (i2 & 7) == 0 ? 8388611 | i2 : i2;
        return (i3 & 112) == 0 ? i3 | 48 : i3;
    }

    private static int d(int i2) {
        if (i2 == 0) {
            return 8388661;
        }
        return i2;
    }

    private void d(View view, int i2) {
        d dVar = (d) view.getLayoutParams();
        Rect e2 = e();
        e2.set(getPaddingLeft() + dVar.leftMargin, getPaddingTop() + dVar.topMargin, (getWidth() - getPaddingRight()) - dVar.rightMargin, (getHeight() - getPaddingBottom()) - dVar.bottomMargin);
        if (this.t != null && t.o(this) && !t.o(view)) {
            e2.left += this.t.a();
            e2.top += this.t.b();
            e2.right -= this.t.c();
            e2.bottom -= this.t.d();
        }
        Rect e3 = e();
        android.support.v4.i.e.a(c(dVar.c), view.getMeasuredWidth(), view.getMeasuredHeight(), e2, e3, i2);
        view.layout(e3.left, e3.top, e3.right, e3.bottom);
        a(e2);
        a(e3);
    }

    private static int e(int i2) {
        if (i2 == 0) {
            return 17;
        }
        return i2;
    }

    private static Rect e() {
        Rect rect = (Rect) f.a();
        return rect == null ? new Rect() : rect;
    }

    private void e(View view, int i2) {
        d dVar = (d) view.getLayoutParams();
        if (dVar.i != i2) {
            t.d(view, i2 - dVar.i);
            dVar.i = i2;
        }
    }

    private boolean e(View view) {
        return this.h.e(view);
    }

    private void f() {
        this.g.clear();
        this.h.a();
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = getChildAt(i2);
            d a2 = a(childAt);
            a2.b(this, childAt);
            this.h.a(childAt);
            for (int i3 = 0; i3 < childCount; i3++) {
                if (i3 != i2) {
                    View childAt2 = getChildAt(i3);
                    if (a2.a(this, childAt, childAt2)) {
                        if (!this.h.b(childAt2)) {
                            this.h.a(childAt2);
                        }
                        this.h.a(childAt2, childAt);
                    }
                }
            }
        }
        this.g.addAll(this.h.b());
        Collections.reverse(this.g);
    }

    private void f(View view, int i2) {
        d dVar = (d) view.getLayoutParams();
        if (dVar.j != i2) {
            t.c(view, i2 - dVar.j);
            dVar.j = i2;
        }
    }

    private void g() {
        if (VERSION.SDK_INT >= 21) {
            if (t.o(this)) {
                if (this.w == null) {
                    this.w = new p() {
                        public ac a(View view, ac acVar) {
                            return CoordinatorLayout.this.a(acVar);
                        }
                    };
                }
                t.a((View) this, this.w);
                setSystemUiVisibility(1280);
                return;
            }
            t.a((View) this, (p) null);
        }
    }

    /* renamed from: a */
    public d generateLayoutParams(AttributeSet attributeSet) {
        return new d(getContext(), attributeSet);
    }

    /* access modifiers changed from: 0000 */
    public d a(View view) {
        d dVar = (d) view.getLayoutParams();
        if (!dVar.f337b) {
            b bVar = null;
            for (Class cls = view.getClass(); cls != null; cls = cls.getSuperclass()) {
                bVar = (b) cls.getAnnotation(b.class);
                if (bVar != null) {
                    break;
                }
            }
            b bVar2 = bVar;
            if (bVar2 != null) {
                try {
                    dVar.a((a) bVar2.a().getDeclaredConstructor(new Class[0]).newInstance(new Object[0]));
                } catch (Exception e2) {
                    Log.e("CoordinatorLayout", "Default behavior class " + bVar2.a().getName() + " could not be instantiated. Did you forget a default constructor?", e2);
                }
            }
            dVar.f337b = true;
        }
        return dVar;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public d generateLayoutParams(LayoutParams layoutParams) {
        return layoutParams instanceof d ? new d((d) layoutParams) : layoutParams instanceof MarginLayoutParams ? new d((MarginLayoutParams) layoutParams) : new d(layoutParams);
    }

    /* access modifiers changed from: 0000 */
    public final ac a(ac acVar) {
        boolean z = true;
        if (android.support.v4.h.i.a(this.t, acVar)) {
            return acVar;
        }
        this.t = acVar;
        this.u = acVar != null && acVar.b() > 0;
        if (this.u || getBackground() != null) {
            z = false;
        }
        setWillNotDraw(z);
        ac b2 = b(acVar);
        requestLayout();
        return b2;
    }

    /* access modifiers changed from: 0000 */
    public void a() {
        boolean z = false;
        int childCount = getChildCount();
        int i2 = 0;
        while (true) {
            if (i2 >= childCount) {
                break;
            } else if (e(getChildAt(i2))) {
                z = true;
                break;
            } else {
                i2++;
            }
        }
        if (z == this.s) {
            return;
        }
        if (z) {
            b();
        } else {
            c();
        }
    }

    /* access modifiers changed from: 0000 */
    public final void a(int i2) {
        boolean z;
        int e2 = t.e(this);
        int size = this.g.size();
        Rect e3 = e();
        Rect e4 = e();
        Rect e5 = e();
        for (int i3 = 0; i3 < size; i3++) {
            View view = (View) this.g.get(i3);
            d dVar = (d) view.getLayoutParams();
            if (i2 != 0 || view.getVisibility() != 8) {
                for (int i4 = 0; i4 < i3; i4++) {
                    if (dVar.l == ((View) this.g.get(i4))) {
                        b(view, e2);
                    }
                }
                a(view, true, e4);
                if (dVar.g != 0 && !e4.isEmpty()) {
                    int a2 = android.support.v4.i.e.a(dVar.g, e2);
                    switch (a2 & 112) {
                        case 48:
                            e3.top = Math.max(e3.top, e4.bottom);
                            break;
                        case 80:
                            e3.bottom = Math.max(e3.bottom, getHeight() - e4.top);
                            break;
                    }
                    switch (a2 & 7) {
                        case 3:
                            e3.left = Math.max(e3.left, e4.right);
                            break;
                        case 5:
                            e3.right = Math.max(e3.right, getWidth() - e4.left);
                            break;
                    }
                }
                if (dVar.h != 0 && view.getVisibility() == 0) {
                    a(view, e3, e2);
                }
                if (i2 != 2) {
                    c(view, e5);
                    if (!e5.equals(e4)) {
                        b(view, e4);
                    }
                }
                for (int i5 = i3 + 1; i5 < size; i5++) {
                    View view2 = (View) this.g.get(i5);
                    d dVar2 = (d) view2.getLayoutParams();
                    a b2 = dVar2.b();
                    if (b2 != null && b2.a(this, view2, view)) {
                        if (i2 != 0 || !dVar2.g()) {
                            switch (i2) {
                                case 2:
                                    b2.d(this, view2, view);
                                    z = true;
                                    break;
                                default:
                                    z = b2.b(this, view2, view);
                                    break;
                            }
                            if (i2 == 1) {
                                dVar2.a(z);
                            }
                        } else {
                            dVar2.h();
                        }
                    }
                }
            }
        }
        a(e3);
        a(e4);
        a(e5);
    }

    public void a(View view, int i2) {
        d dVar = (d) view.getLayoutParams();
        if (dVar.d()) {
            throw new IllegalStateException("An anchor may not be changed after CoordinatorLayout measurement begins before layout is complete.");
        } else if (dVar.k != null) {
            a(view, dVar.k, i2);
        } else if (dVar.e >= 0) {
            b(view, dVar.e, i2);
        } else {
            d(view, i2);
        }
    }

    public void a(View view, int i2, int i3, int i4, int i5) {
        measureChildWithMargins(view, i2, i3, i4, i5);
    }

    public void a(View view, int i2, int i3, int i4, int i5, int i6) {
        boolean z;
        int childCount = getChildCount();
        boolean z2 = false;
        int i7 = 0;
        while (i7 < childCount) {
            View childAt = getChildAt(i7);
            if (childAt.getVisibility() == 8) {
                z = z2;
            } else {
                d dVar = (d) childAt.getLayoutParams();
                if (!dVar.b(i6)) {
                    z = z2;
                } else {
                    a b2 = dVar.b();
                    if (b2 != null) {
                        b2.a(this, childAt, view, i2, i3, i4, i5, i6);
                        z = true;
                    } else {
                        z = z2;
                    }
                }
            }
            i7++;
            z2 = z;
        }
        if (z2) {
            a(1);
        }
    }

    public void a(View view, int i2, int i3, int[] iArr, int i4) {
        boolean z;
        int i5;
        int i6;
        int i7 = 0;
        int i8 = 0;
        boolean z2 = false;
        int childCount = getChildCount();
        int i9 = 0;
        while (i9 < childCount) {
            View childAt = getChildAt(i9);
            if (childAt.getVisibility() == 8) {
                z = z2;
                i5 = i7;
                i6 = i8;
            } else {
                d dVar = (d) childAt.getLayoutParams();
                if (!dVar.b(i4)) {
                    z = z2;
                    i5 = i7;
                    i6 = i8;
                } else {
                    a b2 = dVar.b();
                    if (b2 != null) {
                        int[] iArr2 = this.k;
                        this.k[1] = 0;
                        iArr2[0] = 0;
                        b2.a(this, childAt, view, i2, i3, this.k, i4);
                        i5 = i2 > 0 ? Math.max(i7, this.k[0]) : Math.min(i7, this.k[0]);
                        i6 = i3 > 0 ? Math.max(i8, this.k[1]) : Math.min(i8, this.k[1]);
                        z = true;
                    } else {
                        z = z2;
                        i5 = i7;
                        i6 = i8;
                    }
                }
            }
            i9++;
            i8 = i6;
            i7 = i5;
            z2 = z;
        }
        iArr[0] = i7;
        iArr[1] = i8;
        if (z2) {
            a(1);
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(View view, int i2, Rect rect, Rect rect2) {
        d dVar = (d) view.getLayoutParams();
        int measuredWidth = view.getMeasuredWidth();
        int measuredHeight = view.getMeasuredHeight();
        a(view, i2, rect, rect2, dVar, measuredWidth, measuredHeight);
        a(dVar, rect2, measuredWidth, measuredHeight);
    }

    /* access modifiers changed from: 0000 */
    public void a(View view, Rect rect) {
        q.b(this, view, rect);
    }

    /* access modifiers changed from: 0000 */
    public void a(View view, boolean z, Rect rect) {
        if (view.isLayoutRequested() || view.getVisibility() == 8) {
            rect.setEmpty();
        } else if (z) {
            a(view, rect);
        } else {
            rect.set(view.getLeft(), view.getTop(), view.getRight(), view.getBottom());
        }
    }

    public boolean a(View view, int i2, int i3) {
        Rect e2 = e();
        a(view, e2);
        try {
            return e2.contains(i2, i3);
        } finally {
            a(e2);
        }
    }

    public boolean a(View view, View view2, int i2, int i3) {
        boolean z;
        boolean z2 = false;
        int childCount = getChildCount();
        int i4 = 0;
        while (i4 < childCount) {
            View childAt = getChildAt(i4);
            if (childAt.getVisibility() == 8) {
                z = z2;
            } else {
                d dVar = (d) childAt.getLayoutParams();
                a b2 = dVar.b();
                if (b2 != null) {
                    boolean a2 = b2.a(this, childAt, view, view2, i2, i3);
                    z = z2 | a2;
                    dVar.a(i3, a2);
                } else {
                    dVar.a(i3, false);
                    z = z2;
                }
            }
            i4++;
            z2 = z;
        }
        return z2;
    }

    /* access modifiers changed from: 0000 */
    public void b() {
        if (this.n) {
            if (this.r == null) {
                this.r = new e();
            }
            getViewTreeObserver().addOnPreDrawListener(this.r);
        }
        this.s = true;
    }

    public void b(View view) {
        List c2 = this.h.c(view);
        if (c2 != null && !c2.isEmpty()) {
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 < c2.size()) {
                    View view2 = (View) c2.get(i3);
                    a b2 = ((d) view2.getLayoutParams()).b();
                    if (b2 != null) {
                        b2.b(this, view2, view);
                    }
                    i2 = i3 + 1;
                } else {
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void b(View view, int i2) {
        d dVar = (d) view.getLayoutParams();
        if (dVar.k != null) {
            Rect e2 = e();
            Rect e3 = e();
            Rect e4 = e();
            a(dVar.k, e2);
            a(view, false, e3);
            int measuredWidth = view.getMeasuredWidth();
            int measuredHeight = view.getMeasuredHeight();
            a(view, i2, e2, e4, dVar, measuredWidth, measuredHeight);
            boolean z = (e4.left == e3.left && e4.top == e3.top) ? false : true;
            a(dVar, e4, measuredWidth, measuredHeight);
            int i3 = e4.left - e3.left;
            int i4 = e4.top - e3.top;
            if (i3 != 0) {
                t.d(view, i3);
            }
            if (i4 != 0) {
                t.c(view, i4);
            }
            if (z) {
                a b2 = dVar.b();
                if (b2 != null) {
                    b2.b(this, view, dVar.k);
                }
            }
            a(e2);
            a(e3);
            a(e4);
        }
    }

    /* access modifiers changed from: 0000 */
    public void b(View view, Rect rect) {
        ((d) view.getLayoutParams()).a(rect);
    }

    public void b(View view, View view2, int i2, int i3) {
        this.x.a(view, view2, i2, i3);
        this.q = view2;
        int childCount = getChildCount();
        for (int i4 = 0; i4 < childCount; i4++) {
            View childAt = getChildAt(i4);
            d dVar = (d) childAt.getLayoutParams();
            if (dVar.b(i3)) {
                a b2 = dVar.b();
                if (b2 != null) {
                    b2.b(this, childAt, view, view2, i2, i3);
                }
            }
        }
    }

    public List<View> c(View view) {
        List d2 = this.h.d(view);
        this.j.clear();
        if (d2 != null) {
            this.j.addAll(d2);
        }
        return this.j;
    }

    /* access modifiers changed from: 0000 */
    public void c() {
        if (this.n && this.r != null) {
            getViewTreeObserver().removeOnPreDrawListener(this.r);
        }
        this.s = false;
    }

    public void c(View view, int i2) {
        this.x.a(view, i2);
        int childCount = getChildCount();
        for (int i3 = 0; i3 < childCount; i3++) {
            View childAt = getChildAt(i3);
            d dVar = (d) childAt.getLayoutParams();
            if (dVar.b(i2)) {
                a b2 = dVar.b();
                if (b2 != null) {
                    b2.a(this, childAt, view, i2);
                }
                dVar.a(i2);
                dVar.h();
            }
        }
        this.q = null;
    }

    /* access modifiers changed from: 0000 */
    public void c(View view, Rect rect) {
        rect.set(((d) view.getLayoutParams()).c());
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(LayoutParams layoutParams) {
        return (layoutParams instanceof d) && super.checkLayoutParams(layoutParams);
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public d generateDefaultLayoutParams() {
        return new d(-2, -2);
    }

    public List<View> d(View view) {
        List c2 = this.h.c(view);
        this.j.clear();
        if (c2 != null) {
            this.j.addAll(c2);
        }
        return this.j;
    }

    /* access modifiers changed from: protected */
    public boolean drawChild(Canvas canvas, View view, long j2) {
        d dVar = (d) view.getLayoutParams();
        if (dVar.f336a != null) {
            float d2 = dVar.f336a.d(this, view);
            if (d2 > 0.0f) {
                if (this.l == null) {
                    this.l = new Paint();
                }
                this.l.setColor(dVar.f336a.c(this, view));
                this.l.setAlpha(android.support.v4.e.a.a(Math.round(d2 * 255.0f), 0, 255));
                int save = canvas.save();
                if (view.isOpaque()) {
                    canvas.clipRect((float) view.getLeft(), (float) view.getTop(), (float) view.getRight(), (float) view.getBottom(), Op.DIFFERENCE);
                }
                canvas.drawRect((float) getPaddingLeft(), (float) getPaddingTop(), (float) (getWidth() - getPaddingRight()), (float) (getHeight() - getPaddingBottom()), this.l);
                canvas.restoreToCount(save);
            }
        }
        return super.drawChild(canvas, view, j2);
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        int[] drawableState = getDrawableState();
        boolean z = false;
        Drawable drawable = this.v;
        if (drawable != null && drawable.isStateful()) {
            z = false | drawable.setState(drawableState);
        }
        if (z) {
            invalidate();
        }
    }

    /* access modifiers changed from: 0000 */
    public final List<View> getDependencySortedChildren() {
        f();
        return Collections.unmodifiableList(this.g);
    }

    /* access modifiers changed from: 0000 */
    public final ac getLastWindowInsets() {
        return this.t;
    }

    public int getNestedScrollAxes() {
        return this.x.a();
    }

    public Drawable getStatusBarBackground() {
        return this.v;
    }

    /* access modifiers changed from: protected */
    public int getSuggestedMinimumHeight() {
        return Math.max(super.getSuggestedMinimumHeight(), getPaddingTop() + getPaddingBottom());
    }

    /* access modifiers changed from: protected */
    public int getSuggestedMinimumWidth() {
        return Math.max(super.getSuggestedMinimumWidth(), getPaddingLeft() + getPaddingRight());
    }

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        a(false);
        if (this.s) {
            if (this.r == null) {
                this.r = new e();
            }
            getViewTreeObserver().addOnPreDrawListener(this.r);
        }
        if (this.t == null && t.o(this)) {
            t.n(this);
        }
        this.n = true;
    }

    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        a(false);
        if (this.s && this.r != null) {
            getViewTreeObserver().removeOnPreDrawListener(this.r);
        }
        if (this.q != null) {
            onStopNestedScroll(this.q);
        }
        this.n = false;
    }

    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.u && this.v != null) {
            int i2 = this.t != null ? this.t.b() : 0;
            if (i2 > 0) {
                this.v.setBounds(0, 0, getWidth(), i2);
                this.v.draw(canvas);
            }
        }
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        MotionEvent motionEvent2 = null;
        int actionMasked = motionEvent.getActionMasked();
        if (actionMasked == 0) {
            a(true);
        }
        boolean a2 = a(motionEvent, 0);
        if (motionEvent2 != null) {
            motionEvent2.recycle();
        }
        if (actionMasked == 1 || actionMasked == 3) {
            a(true);
        }
        return a2;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        int e2 = t.e(this);
        int size = this.g.size();
        for (int i6 = 0; i6 < size; i6++) {
            View view = (View) this.g.get(i6);
            if (view.getVisibility() != 8) {
                a b2 = ((d) view.getLayoutParams()).b();
                if (b2 == null || !b2.a(this, view, e2)) {
                    a(view, e2);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        int i4;
        int i5;
        int max;
        int combineMeasuredStates;
        int i6;
        f();
        a();
        int paddingLeft = getPaddingLeft();
        int paddingTop = getPaddingTop();
        int paddingRight = getPaddingRight();
        int paddingBottom = getPaddingBottom();
        int e2 = t.e(this);
        boolean z = e2 == 1;
        int mode = MeasureSpec.getMode(i2);
        int size = MeasureSpec.getSize(i2);
        int mode2 = MeasureSpec.getMode(i3);
        int size2 = MeasureSpec.getSize(i3);
        int i7 = paddingLeft + paddingRight;
        int i8 = paddingTop + paddingBottom;
        int suggestedMinimumWidth = getSuggestedMinimumWidth();
        int suggestedMinimumHeight = getSuggestedMinimumHeight();
        int i9 = 0;
        boolean z2 = this.t != null && t.o(this);
        int size3 = this.g.size();
        int i10 = 0;
        while (i10 < size3) {
            View view = (View) this.g.get(i10);
            if (view.getVisibility() == 8) {
                combineMeasuredStates = i9;
                max = suggestedMinimumHeight;
                i6 = suggestedMinimumWidth;
            } else {
                d dVar = (d) view.getLayoutParams();
                int i11 = 0;
                if (dVar.e >= 0 && mode != 0) {
                    int b2 = b(dVar.e);
                    int a2 = android.support.v4.i.e.a(d(dVar.c), e2) & 7;
                    if ((a2 == 3 && !z) || (a2 == 5 && z)) {
                        i11 = Math.max(0, (size - paddingRight) - b2);
                    } else if ((a2 == 5 && !z) || (a2 == 3 && z)) {
                        i11 = Math.max(0, b2 - paddingLeft);
                    }
                }
                if (!z2 || t.o(view)) {
                    i4 = i3;
                    i5 = i2;
                } else {
                    int b3 = this.t.b() + this.t.d();
                    i5 = MeasureSpec.makeMeasureSpec(size - (this.t.a() + this.t.c()), mode);
                    i4 = MeasureSpec.makeMeasureSpec(size2 - b3, mode2);
                }
                a b4 = dVar.b();
                if (b4 == null || !b4.a(this, view, i5, i11, i4, 0)) {
                    a(view, i5, i11, i4, 0);
                }
                int max2 = Math.max(suggestedMinimumWidth, view.getMeasuredWidth() + i7 + dVar.leftMargin + dVar.rightMargin);
                max = Math.max(suggestedMinimumHeight, view.getMeasuredHeight() + i8 + dVar.topMargin + dVar.bottomMargin);
                combineMeasuredStates = View.combineMeasuredStates(i9, view.getMeasuredState());
                i6 = max2;
            }
            i10++;
            i9 = combineMeasuredStates;
            suggestedMinimumHeight = max;
            suggestedMinimumWidth = i6;
        }
        setMeasuredDimension(View.resolveSizeAndState(suggestedMinimumWidth, i2, -16777216 & i9), View.resolveSizeAndState(suggestedMinimumHeight, i3, i9 << 16));
    }

    public boolean onNestedFling(View view, float f2, float f3, boolean z) {
        boolean z2;
        int childCount = getChildCount();
        int i2 = 0;
        boolean z3 = false;
        while (i2 < childCount) {
            View childAt = getChildAt(i2);
            if (childAt.getVisibility() == 8) {
                z2 = z3;
            } else {
                d dVar = (d) childAt.getLayoutParams();
                if (!dVar.b(0)) {
                    z2 = z3;
                } else {
                    a b2 = dVar.b();
                    z2 = b2 != null ? b2.a(this, childAt, view, f2, f3, z) | z3 : z3;
                }
            }
            i2++;
            z3 = z2;
        }
        if (z3) {
            a(1);
        }
        return z3;
    }

    public boolean onNestedPreFling(View view, float f2, float f3) {
        boolean z;
        int childCount = getChildCount();
        int i2 = 0;
        boolean z2 = false;
        while (i2 < childCount) {
            View childAt = getChildAt(i2);
            if (childAt.getVisibility() == 8) {
                z = z2;
            } else {
                d dVar = (d) childAt.getLayoutParams();
                if (!dVar.b(0)) {
                    z = z2;
                } else {
                    a b2 = dVar.b();
                    z = b2 != null ? b2.a(this, childAt, view, f2, f3) | z2 : z2;
                }
            }
            i2++;
            z2 = z;
        }
        return z2;
    }

    public void onNestedPreScroll(View view, int i2, int i3, int[] iArr) {
        a(view, i2, i3, iArr, 0);
    }

    public void onNestedScroll(View view, int i2, int i3, int i4, int i5) {
        a(view, i2, i3, i4, i5, 0);
    }

    public void onNestedScrollAccepted(View view, View view2, int i2) {
        b(view, view2, i2, 0);
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (!(parcelable instanceof f)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        f fVar = (f) parcelable;
        super.onRestoreInstanceState(fVar.a());
        SparseArray<Parcelable> sparseArray = fVar.f339a;
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = getChildAt(i2);
            int id = childAt.getId();
            a b2 = a(childAt).b();
            if (!(id == -1 || b2 == null)) {
                Parcelable parcelable2 = (Parcelable) sparseArray.get(id);
                if (parcelable2 != null) {
                    b2.a(this, childAt, parcelable2);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        f fVar = new f(super.onSaveInstanceState());
        SparseArray<Parcelable> sparseArray = new SparseArray<>();
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = getChildAt(i2);
            int id = childAt.getId();
            a b2 = ((d) childAt.getLayoutParams()).b();
            if (!(id == -1 || b2 == null)) {
                Parcelable b3 = b2.b(this, childAt);
                if (b3 != null) {
                    sparseArray.append(id, b3);
                }
            }
        }
        fVar.f339a = sparseArray;
        return fVar;
    }

    public boolean onStartNestedScroll(View view, View view2, int i2) {
        return a(view, view2, i2, 0);
    }

    public void onStopNestedScroll(View view) {
        c(view, 0);
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x002d  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0038  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0043  */
    public boolean onTouchEvent(MotionEvent motionEvent) {
        boolean z;
        boolean z2;
        MotionEvent motionEvent2;
        MotionEvent motionEvent3 = null;
        int actionMasked = motionEvent.getActionMasked();
        if (this.p == null) {
            boolean a2 = a(motionEvent, 1);
            if (a2) {
                z = a2;
            } else {
                z = a2;
                z2 = false;
                if (this.p != null) {
                    z2 |= super.onTouchEvent(motionEvent);
                } else if (z) {
                    if (0 == 0) {
                        long uptimeMillis = SystemClock.uptimeMillis();
                        motionEvent2 = MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, 0.0f, 0.0f, 0);
                    } else {
                        motionEvent2 = null;
                    }
                    super.onTouchEvent(motionEvent2);
                    motionEvent3 = motionEvent2;
                }
                if (z2 || actionMasked == 0) {
                }
                if (motionEvent3 != null) {
                    motionEvent3.recycle();
                }
                if (actionMasked == 1 || actionMasked == 3) {
                    a(false);
                }
                return z2;
            }
        } else {
            z = false;
        }
        a b2 = ((d) this.p.getLayoutParams()).b();
        z2 = b2 != null ? b2.b(this, this.p, motionEvent) : false;
        if (this.p != null) {
        }
        if (motionEvent3 != null) {
        }
        a(false);
        return z2;
    }

    public boolean requestChildRectangleOnScreen(View view, Rect rect, boolean z) {
        a b2 = ((d) view.getLayoutParams()).b();
        if (b2 == null || !b2.a(this, view, rect, z)) {
            return super.requestChildRectangleOnScreen(view, rect, z);
        }
        return true;
    }

    public void requestDisallowInterceptTouchEvent(boolean z) {
        super.requestDisallowInterceptTouchEvent(z);
        if (z && !this.m) {
            a(false);
            this.m = true;
        }
    }

    public void setFitsSystemWindows(boolean z) {
        super.setFitsSystemWindows(z);
        g();
    }

    public void setOnHierarchyChangeListener(OnHierarchyChangeListener onHierarchyChangeListener) {
        this.e = onHierarchyChangeListener;
    }

    public void setStatusBarBackground(Drawable drawable) {
        Drawable drawable2 = null;
        if (this.v != drawable) {
            if (this.v != null) {
                this.v.setCallback(null);
            }
            if (drawable != null) {
                drawable2 = drawable.mutate();
            }
            this.v = drawable2;
            if (this.v != null) {
                if (this.v.isStateful()) {
                    this.v.setState(getDrawableState());
                }
                android.support.v4.c.a.a.b(this.v, t.e(this));
                this.v.setVisible(getVisibility() == 0, false);
                this.v.setCallback(this);
            }
            t.c(this);
        }
    }

    public void setStatusBarBackgroundColor(int i2) {
        setStatusBarBackground(new ColorDrawable(i2));
    }

    public void setStatusBarBackgroundResource(int i2) {
        setStatusBarBackground(i2 != 0 ? android.support.v4.b.a.a(getContext(), i2) : null);
    }

    public void setVisibility(int i2) {
        super.setVisibility(i2);
        boolean z = i2 == 0;
        if (this.v != null && this.v.isVisible() != z) {
            this.v.setVisible(z, false);
        }
    }

    /* access modifiers changed from: protected */
    public boolean verifyDrawable(Drawable drawable) {
        return super.verifyDrawable(drawable) || drawable == this.v;
    }
}
