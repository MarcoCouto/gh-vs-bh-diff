package android.support.design.widget;

import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Looper;
import android.os.Message;
import com.hmatalonga.greenhub.Config;
import java.lang.ref.WeakReference;

class l {

    /* renamed from: a reason: collision with root package name */
    private static l f400a;

    /* renamed from: b reason: collision with root package name */
    private final Object f401b = new Object();
    private final Handler c = new Handler(Looper.getMainLooper(), new Callback() {
        public boolean handleMessage(Message message) {
            switch (message.what) {
                case 0:
                    l.this.a((b) message.obj);
                    return true;
                default:
                    return false;
            }
        }
    });
    private b d;
    private b e;

    interface a {
        void a();

        void a(int i);
    }

    private static class b {

        /* renamed from: a reason: collision with root package name */
        final WeakReference<a> f403a;

        /* renamed from: b reason: collision with root package name */
        int f404b;
        boolean c;

        b(int i, a aVar) {
            this.f403a = new WeakReference<>(aVar);
            this.f404b = i;
        }

        /* access modifiers changed from: 0000 */
        public boolean a(a aVar) {
            return aVar != null && this.f403a.get() == aVar;
        }
    }

    private l() {
    }

    static l a() {
        if (f400a == null) {
            f400a = new l();
        }
        return f400a;
    }

    private boolean a(b bVar, int i) {
        a aVar = (a) bVar.f403a.get();
        if (aVar == null) {
            return false;
        }
        this.c.removeCallbacksAndMessages(bVar);
        aVar.a(i);
        return true;
    }

    private void b() {
        if (this.e != null) {
            this.d = this.e;
            this.e = null;
            a aVar = (a) this.d.f403a.get();
            if (aVar != null) {
                aVar.a();
            } else {
                this.d = null;
            }
        }
    }

    private void b(b bVar) {
        if (bVar.f404b != -2) {
            int i = 2750;
            if (bVar.f404b > 0) {
                i = bVar.f404b;
            } else if (bVar.f404b == -1) {
                i = Config.PENDING_REMOVAL_TIMEOUT;
            }
            this.c.removeCallbacksAndMessages(bVar);
            this.c.sendMessageDelayed(Message.obtain(this.c, 0, bVar), (long) i);
        }
    }

    private boolean f(a aVar) {
        return this.d != null && this.d.a(aVar);
    }

    private boolean g(a aVar) {
        return this.e != null && this.e.a(aVar);
    }

    public void a(int i, a aVar) {
        synchronized (this.f401b) {
            if (f(aVar)) {
                this.d.f404b = i;
                this.c.removeCallbacksAndMessages(this.d);
                b(this.d);
                return;
            }
            if (g(aVar)) {
                this.e.f404b = i;
            } else {
                this.e = new b(i, aVar);
            }
            if (this.d == null || !a(this.d, 4)) {
                this.d = null;
                b();
            }
        }
    }

    public void a(a aVar) {
        synchronized (this.f401b) {
            if (f(aVar)) {
                this.d = null;
                if (this.e != null) {
                    b();
                }
            }
        }
    }

    public void a(a aVar, int i) {
        synchronized (this.f401b) {
            if (f(aVar)) {
                a(this.d, i);
            } else if (g(aVar)) {
                a(this.e, i);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(b bVar) {
        synchronized (this.f401b) {
            if (this.d == bVar || this.e == bVar) {
                a(bVar, 2);
            }
        }
    }

    public void b(a aVar) {
        synchronized (this.f401b) {
            if (f(aVar)) {
                b(this.d);
            }
        }
    }

    public void c(a aVar) {
        synchronized (this.f401b) {
            if (f(aVar) && !this.d.c) {
                this.d.c = true;
                this.c.removeCallbacksAndMessages(this.d);
            }
        }
    }

    public void d(a aVar) {
        synchronized (this.f401b) {
            if (f(aVar) && this.d.c) {
                this.d.c = false;
                b(this.d);
            }
        }
    }

    public boolean e(a aVar) {
        boolean z;
        synchronized (this.f401b) {
            z = f(aVar) || g(aVar);
        }
        return z;
    }
}
