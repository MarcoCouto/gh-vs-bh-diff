package android.support.design.widget;

import android.content.Context;
import android.support.v4.i.b;
import android.support.v4.i.t;
import android.support.v7.a.a.C0026a;
import android.support.v7.widget.o;
import android.util.AttributeSet;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;
import android.widget.Checkable;

public class CheckableImageButton extends o implements Checkable {

    /* renamed from: a reason: collision with root package name */
    private static final int[] f329a = {16842912};

    /* renamed from: b reason: collision with root package name */
    private boolean f330b;

    public CheckableImageButton(Context context) {
        this(context, null);
    }

    public CheckableImageButton(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, C0026a.imageButtonStyle);
    }

    public CheckableImageButton(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        t.a((View) this, (b) new b() {
            public void a(View view, android.support.v4.i.a.b bVar) {
                super.a(view, bVar);
                bVar.a(true);
                bVar.b(CheckableImageButton.this.isChecked());
            }

            public void a(View view, AccessibilityEvent accessibilityEvent) {
                super.a(view, accessibilityEvent);
                accessibilityEvent.setChecked(CheckableImageButton.this.isChecked());
            }
        });
    }

    public boolean isChecked() {
        return this.f330b;
    }

    public int[] onCreateDrawableState(int i) {
        return this.f330b ? mergeDrawableStates(super.onCreateDrawableState(f329a.length + i), f329a) : super.onCreateDrawableState(i);
    }

    public void setChecked(boolean z) {
        if (this.f330b != z) {
            this.f330b = z;
            refreshDrawableState();
            sendAccessibilityEvent(2048);
        }
    }

    public void toggle() {
        setChecked(!this.f330b);
    }
}
