package android.support.design.widget;

import android.content.Context;
import android.support.design.widget.CoordinatorLayout.a;
import android.util.AttributeSet;
import android.view.View;

class r<V extends View> extends a<V> {

    /* renamed from: a reason: collision with root package name */
    private s f434a;

    /* renamed from: b reason: collision with root package name */
    private int f435b = 0;
    private int c = 0;

    public r() {
    }

    public r(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public boolean a(int i) {
        if (this.f434a != null) {
            return this.f434a.a(i);
        }
        this.f435b = i;
        return false;
    }

    public boolean a(CoordinatorLayout coordinatorLayout, V v, int i) {
        b(coordinatorLayout, v, i);
        if (this.f434a == null) {
            this.f434a = new s(v);
        }
        this.f434a.a();
        if (this.f435b != 0) {
            this.f434a.a(this.f435b);
            this.f435b = 0;
        }
        if (this.c != 0) {
            this.f434a.b(this.c);
            this.c = 0;
        }
        return true;
    }

    public int b() {
        if (this.f434a != null) {
            return this.f434a.b();
        }
        return 0;
    }

    /* access modifiers changed from: protected */
    public void b(CoordinatorLayout coordinatorLayout, V v, int i) {
        coordinatorLayout.a((View) v, i);
    }
}
