package android.support.c.a;

import android.content.res.Resources.Theme;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff.Mode;
import android.graphics.Rect;
import android.graphics.Region;
import android.graphics.drawable.Drawable;
import android.support.v4.c.a.a;
import android.support.v4.c.a.f;

abstract class h extends Drawable implements f {

    /* renamed from: b reason: collision with root package name */
    Drawable f279b;

    h() {
    }

    public void applyTheme(Theme theme) {
        if (this.f279b != null) {
            a.a(this.f279b, theme);
        }
    }

    public void clearColorFilter() {
        if (this.f279b != null) {
            this.f279b.clearColorFilter();
        } else {
            super.clearColorFilter();
        }
    }

    public ColorFilter getColorFilter() {
        if (this.f279b != null) {
            return a.e(this.f279b);
        }
        return null;
    }

    public Drawable getCurrent() {
        return this.f279b != null ? this.f279b.getCurrent() : super.getCurrent();
    }

    public int getMinimumHeight() {
        return this.f279b != null ? this.f279b.getMinimumHeight() : super.getMinimumHeight();
    }

    public int getMinimumWidth() {
        return this.f279b != null ? this.f279b.getMinimumWidth() : super.getMinimumWidth();
    }

    public boolean getPadding(Rect rect) {
        return this.f279b != null ? this.f279b.getPadding(rect) : super.getPadding(rect);
    }

    public int[] getState() {
        return this.f279b != null ? this.f279b.getState() : super.getState();
    }

    public Region getTransparentRegion() {
        return this.f279b != null ? this.f279b.getTransparentRegion() : super.getTransparentRegion();
    }

    public void jumpToCurrentState() {
        if (this.f279b != null) {
            a.a(this.f279b);
        }
    }

    /* access modifiers changed from: protected */
    public void onBoundsChange(Rect rect) {
        if (this.f279b != null) {
            this.f279b.setBounds(rect);
        } else {
            super.onBoundsChange(rect);
        }
    }

    /* access modifiers changed from: protected */
    public boolean onLevelChange(int i) {
        return this.f279b != null ? this.f279b.setLevel(i) : super.onLevelChange(i);
    }

    public void setChangingConfigurations(int i) {
        if (this.f279b != null) {
            this.f279b.setChangingConfigurations(i);
        } else {
            super.setChangingConfigurations(i);
        }
    }

    public void setColorFilter(int i, Mode mode) {
        if (this.f279b != null) {
            this.f279b.setColorFilter(i, mode);
        } else {
            super.setColorFilter(i, mode);
        }
    }

    public void setFilterBitmap(boolean z) {
        if (this.f279b != null) {
            this.f279b.setFilterBitmap(z);
        }
    }

    public void setHotspot(float f, float f2) {
        if (this.f279b != null) {
            a.a(this.f279b, f, f2);
        }
    }

    public void setHotspotBounds(int i, int i2, int i3, int i4) {
        if (this.f279b != null) {
            a.a(this.f279b, i, i2, i3, i4);
        }
    }

    public boolean setState(int[] iArr) {
        return this.f279b != null ? this.f279b.setState(iArr) : super.setState(iArr);
    }
}
