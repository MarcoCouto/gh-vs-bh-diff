package android.support.c.a;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorSet;
import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff.Mode;
import android.graphics.Rect;
import android.graphics.Region;
import android.graphics.drawable.AnimatedVectorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Drawable.Callback;
import android.graphics.drawable.Drawable.ConstantState;
import android.os.Build.VERSION;
import android.util.AttributeSet;
import java.io.IOException;
import java.util.ArrayList;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class c extends h implements b {

    /* renamed from: a reason: collision with root package name */
    final Callback f270a;
    private a c;
    private Context d;
    private ArgbEvaluator e;
    private AnimatorListener f;
    private ArrayList<Object> g;

    private static class a extends ConstantState {

        /* renamed from: a reason: collision with root package name */
        int f272a;

        /* renamed from: b reason: collision with root package name */
        i f273b;
        AnimatorSet c;
        android.support.v4.h.a<Animator, String> d;
        /* access modifiers changed from: private */
        public ArrayList<Animator> e;

        public a(Context context, a aVar, Callback callback, Resources resources) {
            if (aVar != null) {
                this.f272a = aVar.f272a;
                if (aVar.f273b != null) {
                    ConstantState constantState = aVar.f273b.getConstantState();
                    if (resources != null) {
                        this.f273b = (i) constantState.newDrawable(resources);
                    } else {
                        this.f273b = (i) constantState.newDrawable();
                    }
                    this.f273b = (i) this.f273b.mutate();
                    this.f273b.setCallback(callback);
                    this.f273b.setBounds(aVar.f273b.getBounds());
                    this.f273b.a(false);
                }
                if (aVar.e != null) {
                    int size = aVar.e.size();
                    this.e = new ArrayList<>(size);
                    this.d = new android.support.v4.h.a<>(size);
                    for (int i = 0; i < size; i++) {
                        Animator animator = (Animator) aVar.e.get(i);
                        Animator clone = animator.clone();
                        String str = (String) aVar.d.get(animator);
                        clone.setTarget(this.f273b.a(str));
                        this.e.add(clone);
                        this.d.put(clone, str);
                    }
                    a();
                }
            }
        }

        public void a() {
            if (this.c == null) {
                this.c = new AnimatorSet();
            }
            this.c.playTogether(this.e);
        }

        public int getChangingConfigurations() {
            return this.f272a;
        }

        public Drawable newDrawable() {
            throw new IllegalStateException("No constant state support for SDK < 24.");
        }

        public Drawable newDrawable(Resources resources) {
            throw new IllegalStateException("No constant state support for SDK < 24.");
        }
    }

    private static class b extends ConstantState {

        /* renamed from: a reason: collision with root package name */
        private final ConstantState f274a;

        public b(ConstantState constantState) {
            this.f274a = constantState;
        }

        public boolean canApplyTheme() {
            return this.f274a.canApplyTheme();
        }

        public int getChangingConfigurations() {
            return this.f274a.getChangingConfigurations();
        }

        public Drawable newDrawable() {
            c cVar = new c();
            cVar.f279b = this.f274a.newDrawable();
            cVar.f279b.setCallback(cVar.f270a);
            return cVar;
        }

        public Drawable newDrawable(Resources resources) {
            c cVar = new c();
            cVar.f279b = this.f274a.newDrawable(resources);
            cVar.f279b.setCallback(cVar.f270a);
            return cVar;
        }

        public Drawable newDrawable(Resources resources, Theme theme) {
            c cVar = new c();
            cVar.f279b = this.f274a.newDrawable(resources, theme);
            cVar.f279b.setCallback(cVar.f270a);
            return cVar;
        }
    }

    c() {
        this(null, null, null);
    }

    private c(Context context) {
        this(context, null, null);
    }

    private c(Context context, a aVar, Resources resources) {
        this.e = null;
        this.f = null;
        this.g = null;
        this.f270a = new Callback() {
            public void invalidateDrawable(Drawable drawable) {
                c.this.invalidateSelf();
            }

            public void scheduleDrawable(Drawable drawable, Runnable runnable, long j) {
                c.this.scheduleSelf(runnable, j);
            }

            public void unscheduleDrawable(Drawable drawable, Runnable runnable) {
                c.this.unscheduleSelf(runnable);
            }
        };
        this.d = context;
        if (aVar != null) {
            this.c = aVar;
        } else {
            this.c = new a(context, aVar, this.f270a, resources);
        }
    }

    public static c a(Context context, Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Theme theme) throws XmlPullParserException, IOException {
        c cVar = new c(context);
        cVar.inflate(resources, xmlPullParser, attributeSet, theme);
        return cVar;
    }

    private void a(Animator animator) {
        if (animator instanceof AnimatorSet) {
            ArrayList childAnimations = ((AnimatorSet) animator).getChildAnimations();
            if (childAnimations != null) {
                int i = 0;
                while (true) {
                    int i2 = i;
                    if (i2 >= childAnimations.size()) {
                        break;
                    }
                    a((Animator) childAnimations.get(i2));
                    i = i2 + 1;
                }
            }
        }
        if (animator instanceof ObjectAnimator) {
            ObjectAnimator objectAnimator = (ObjectAnimator) animator;
            String propertyName = objectAnimator.getPropertyName();
            if ("fillColor".equals(propertyName) || "strokeColor".equals(propertyName)) {
                if (this.e == null) {
                    this.e = new ArgbEvaluator();
                }
                objectAnimator.setEvaluator(this.e);
            }
        }
    }

    private void a(String str, Animator animator) {
        animator.setTarget(this.c.f273b.a(str));
        if (VERSION.SDK_INT < 21) {
            a(animator);
        }
        if (this.c.e == null) {
            this.c.e = new ArrayList();
            this.c.d = new android.support.v4.h.a<>();
        }
        this.c.e.add(animator);
        this.c.d.put(animator, str);
    }

    public void applyTheme(Theme theme) {
        if (this.f279b != null) {
            android.support.v4.c.a.a.a(this.f279b, theme);
        }
    }

    public boolean canApplyTheme() {
        if (this.f279b != null) {
            return android.support.v4.c.a.a.d(this.f279b);
        }
        return false;
    }

    public /* bridge */ /* synthetic */ void clearColorFilter() {
        super.clearColorFilter();
    }

    public void draw(Canvas canvas) {
        if (this.f279b != null) {
            this.f279b.draw(canvas);
            return;
        }
        this.c.f273b.draw(canvas);
        if (this.c.c.isStarted()) {
            invalidateSelf();
        }
    }

    public int getAlpha() {
        return this.f279b != null ? android.support.v4.c.a.a.c(this.f279b) : this.c.f273b.getAlpha();
    }

    public int getChangingConfigurations() {
        return this.f279b != null ? this.f279b.getChangingConfigurations() : super.getChangingConfigurations() | this.c.f272a;
    }

    public /* bridge */ /* synthetic */ ColorFilter getColorFilter() {
        return super.getColorFilter();
    }

    public ConstantState getConstantState() {
        if (this.f279b == null || VERSION.SDK_INT < 24) {
            return null;
        }
        return new b(this.f279b.getConstantState());
    }

    public /* bridge */ /* synthetic */ Drawable getCurrent() {
        return super.getCurrent();
    }

    public int getIntrinsicHeight() {
        return this.f279b != null ? this.f279b.getIntrinsicHeight() : this.c.f273b.getIntrinsicHeight();
    }

    public int getIntrinsicWidth() {
        return this.f279b != null ? this.f279b.getIntrinsicWidth() : this.c.f273b.getIntrinsicWidth();
    }

    public /* bridge */ /* synthetic */ int getMinimumHeight() {
        return super.getMinimumHeight();
    }

    public /* bridge */ /* synthetic */ int getMinimumWidth() {
        return super.getMinimumWidth();
    }

    public int getOpacity() {
        return this.f279b != null ? this.f279b.getOpacity() : this.c.f273b.getOpacity();
    }

    public /* bridge */ /* synthetic */ boolean getPadding(Rect rect) {
        return super.getPadding(rect);
    }

    public /* bridge */ /* synthetic */ int[] getState() {
        return super.getState();
    }

    public /* bridge */ /* synthetic */ Region getTransparentRegion() {
        return super.getTransparentRegion();
    }

    public void inflate(Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet) throws XmlPullParserException, IOException {
        inflate(resources, xmlPullParser, attributeSet, null);
    }

    public void inflate(Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Theme theme) throws XmlPullParserException, IOException {
        if (this.f279b != null) {
            android.support.v4.c.a.a.a(this.f279b, resources, xmlPullParser, attributeSet, theme);
            return;
        }
        int eventType = xmlPullParser.getEventType();
        int depth = xmlPullParser.getDepth() + 1;
        while (eventType != 1 && (xmlPullParser.getDepth() >= depth || eventType != 3)) {
            if (eventType == 2) {
                String name = xmlPullParser.getName();
                if ("animated-vector".equals(name)) {
                    TypedArray a2 = android.support.v4.b.a.c.a(resources, theme, attributeSet, a.e);
                    int resourceId = a2.getResourceId(0, 0);
                    if (resourceId != 0) {
                        i a3 = i.a(resources, resourceId, theme);
                        a3.a(false);
                        a3.setCallback(this.f270a);
                        if (this.c.f273b != null) {
                            this.c.f273b.setCallback(null);
                        }
                        this.c.f273b = a3;
                    }
                    a2.recycle();
                } else if ("target".equals(name)) {
                    TypedArray obtainAttributes = resources.obtainAttributes(attributeSet, a.f);
                    String string = obtainAttributes.getString(0);
                    int resourceId2 = obtainAttributes.getResourceId(1, 0);
                    if (resourceId2 != 0) {
                        if (this.d != null) {
                            a(string, e.a(this.d, resourceId2));
                        } else {
                            obtainAttributes.recycle();
                            throw new IllegalStateException("Context can't be null when inflating animators");
                        }
                    }
                    obtainAttributes.recycle();
                } else {
                    continue;
                }
            }
            eventType = xmlPullParser.next();
        }
        this.c.a();
    }

    public boolean isAutoMirrored() {
        return this.f279b != null ? android.support.v4.c.a.a.b(this.f279b) : this.c.f273b.isAutoMirrored();
    }

    public boolean isRunning() {
        return this.f279b != null ? ((AnimatedVectorDrawable) this.f279b).isRunning() : this.c.c.isRunning();
    }

    public boolean isStateful() {
        return this.f279b != null ? this.f279b.isStateful() : this.c.f273b.isStateful();
    }

    public /* bridge */ /* synthetic */ void jumpToCurrentState() {
        super.jumpToCurrentState();
    }

    public Drawable mutate() {
        if (this.f279b != null) {
            this.f279b.mutate();
        }
        return this;
    }

    /* access modifiers changed from: protected */
    public void onBoundsChange(Rect rect) {
        if (this.f279b != null) {
            this.f279b.setBounds(rect);
        } else {
            this.c.f273b.setBounds(rect);
        }
    }

    /* access modifiers changed from: protected */
    public boolean onLevelChange(int i) {
        return this.f279b != null ? this.f279b.setLevel(i) : this.c.f273b.setLevel(i);
    }

    /* access modifiers changed from: protected */
    public boolean onStateChange(int[] iArr) {
        return this.f279b != null ? this.f279b.setState(iArr) : this.c.f273b.setState(iArr);
    }

    public void setAlpha(int i) {
        if (this.f279b != null) {
            this.f279b.setAlpha(i);
        } else {
            this.c.f273b.setAlpha(i);
        }
    }

    public void setAutoMirrored(boolean z) {
        if (this.f279b != null) {
            android.support.v4.c.a.a.a(this.f279b, z);
        } else {
            this.c.f273b.setAutoMirrored(z);
        }
    }

    public /* bridge */ /* synthetic */ void setChangingConfigurations(int i) {
        super.setChangingConfigurations(i);
    }

    public /* bridge */ /* synthetic */ void setColorFilter(int i, Mode mode) {
        super.setColorFilter(i, mode);
    }

    public void setColorFilter(ColorFilter colorFilter) {
        if (this.f279b != null) {
            this.f279b.setColorFilter(colorFilter);
        } else {
            this.c.f273b.setColorFilter(colorFilter);
        }
    }

    public /* bridge */ /* synthetic */ void setFilterBitmap(boolean z) {
        super.setFilterBitmap(z);
    }

    public /* bridge */ /* synthetic */ void setHotspot(float f2, float f3) {
        super.setHotspot(f2, f3);
    }

    public /* bridge */ /* synthetic */ void setHotspotBounds(int i, int i2, int i3, int i4) {
        super.setHotspotBounds(i, i2, i3, i4);
    }

    public /* bridge */ /* synthetic */ boolean setState(int[] iArr) {
        return super.setState(iArr);
    }

    public void setTint(int i) {
        if (this.f279b != null) {
            android.support.v4.c.a.a.a(this.f279b, i);
        } else {
            this.c.f273b.setTint(i);
        }
    }

    public void setTintList(ColorStateList colorStateList) {
        if (this.f279b != null) {
            android.support.v4.c.a.a.a(this.f279b, colorStateList);
        } else {
            this.c.f273b.setTintList(colorStateList);
        }
    }

    public void setTintMode(Mode mode) {
        if (this.f279b != null) {
            android.support.v4.c.a.a.a(this.f279b, mode);
        } else {
            this.c.f273b.setTintMode(mode);
        }
    }

    public boolean setVisible(boolean z, boolean z2) {
        if (this.f279b != null) {
            return this.f279b.setVisible(z, z2);
        }
        this.c.f273b.setVisible(z, z2);
        return super.setVisible(z, z2);
    }

    public void start() {
        if (this.f279b != null) {
            ((AnimatedVectorDrawable) this.f279b).start();
        } else if (!this.c.c.isStarted()) {
            this.c.c.start();
            invalidateSelf();
        }
    }

    public void stop() {
        if (this.f279b != null) {
            ((AnimatedVectorDrawable) this.f279b).stop();
        } else {
            this.c.c.end();
        }
    }
}
