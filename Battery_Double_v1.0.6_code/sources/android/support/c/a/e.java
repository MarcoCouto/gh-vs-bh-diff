package android.support.c.a;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.animation.Keyframe;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.TypeEvaluator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.Resources.NotFoundException;
import android.content.res.Resources.Theme;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.os.Build.VERSION;
import android.support.v4.b.a.c;
import android.support.v4.c.b;
import android.support.v4.c.b.C0014b;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.util.Xml;
import android.view.InflateException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class e {

    private static class a implements TypeEvaluator<C0014b[]> {

        /* renamed from: a reason: collision with root package name */
        private C0014b[] f275a;

        private a() {
        }

        /* renamed from: a */
        public C0014b[] evaluate(float f, C0014b[] bVarArr, C0014b[] bVarArr2) {
            if (!b.a(bVarArr, bVarArr2)) {
                throw new IllegalArgumentException("Can't interpolate between two incompatible pathData");
            }
            if (this.f275a == null || !b.a(this.f275a, bVarArr)) {
                this.f275a = b.a(bVarArr);
            }
            for (int i = 0; i < bVarArr.length; i++) {
                this.f275a[i].a(bVarArr[i], bVarArr2[i], f);
            }
            return this.f275a;
        }
    }

    private static int a(Resources resources, Theme theme, AttributeSet attributeSet, XmlPullParser xmlPullParser) {
        int i = 0;
        TypedArray a2 = c.a(resources, theme, attributeSet, a.j);
        TypedValue b2 = c.b(a2, xmlPullParser, "value", 0);
        if ((b2 != null) && a(b2.type)) {
            i = 3;
        }
        a2.recycle();
        return i;
    }

    private static int a(TypedArray typedArray, int i, int i2) {
        TypedValue peekValue = typedArray.peekValue(i);
        boolean z = peekValue != null;
        int i3 = z ? peekValue.type : 0;
        TypedValue peekValue2 = typedArray.peekValue(i2);
        boolean z2 = peekValue2 != null;
        return ((!z || !a(i3)) && (!z2 || !a(z2 ? peekValue2.type : 0))) ? 0 : 3;
    }

    public static Animator a(Context context, int i) throws NotFoundException {
        return VERSION.SDK_INT >= 24 ? AnimatorInflater.loadAnimator(context, i) : a(context, context.getResources(), context.getTheme(), i);
    }

    public static Animator a(Context context, Resources resources, Theme theme, int i) throws NotFoundException {
        return a(context, resources, theme, i, 1.0f);
    }

    public static Animator a(Context context, Resources resources, Theme theme, int i, float f) throws NotFoundException {
        XmlResourceParser xmlResourceParser = null;
        try {
            xmlResourceParser = resources.getAnimation(i);
            Animator a2 = a(context, resources, theme, (XmlPullParser) xmlResourceParser, f);
            if (xmlResourceParser != null) {
                xmlResourceParser.close();
            }
            return a2;
        } catch (XmlPullParserException e) {
            NotFoundException notFoundException = new NotFoundException("Can't load animation resource ID #0x" + Integer.toHexString(i));
            notFoundException.initCause(e);
            throw notFoundException;
        } catch (IOException e2) {
            NotFoundException notFoundException2 = new NotFoundException("Can't load animation resource ID #0x" + Integer.toHexString(i));
            notFoundException2.initCause(e2);
            throw notFoundException2;
        } catch (Throwable th) {
            if (xmlResourceParser != null) {
                xmlResourceParser.close();
            }
            throw th;
        }
    }

    private static Animator a(Context context, Resources resources, Theme theme, XmlPullParser xmlPullParser, float f) throws XmlPullParserException, IOException {
        return a(context, resources, theme, xmlPullParser, Xml.asAttributeSet(xmlPullParser), null, 0, f);
    }

    private static Animator a(Context context, Resources resources, Theme theme, XmlPullParser xmlPullParser, AttributeSet attributeSet, AnimatorSet animatorSet, int i, float f) throws XmlPullParserException, IOException {
        boolean z;
        Animator animator = null;
        ArrayList arrayList = null;
        int depth = xmlPullParser.getDepth();
        while (true) {
            int next = xmlPullParser.next();
            if ((next != 3 || xmlPullParser.getDepth() > depth) && next != 1) {
                if (next == 2) {
                    String name = xmlPullParser.getName();
                    if (name.equals("objectAnimator")) {
                        animator = a(context, resources, theme, attributeSet, f, xmlPullParser);
                        z = false;
                    } else if (name.equals("animator")) {
                        animator = a(context, resources, theme, attributeSet, null, f, xmlPullParser);
                        z = false;
                    } else if (name.equals("set")) {
                        Animator animatorSet2 = new AnimatorSet();
                        TypedArray a2 = c.a(resources, theme, attributeSet, a.h);
                        a(context, resources, theme, xmlPullParser, attributeSet, (AnimatorSet) animatorSet2, c.a(a2, xmlPullParser, "ordering", 0, 0), f);
                        a2.recycle();
                        z = false;
                        animator = animatorSet2;
                    } else if (name.equals("propertyValuesHolder")) {
                        PropertyValuesHolder[] a3 = a(context, resources, theme, xmlPullParser, Xml.asAttributeSet(xmlPullParser));
                        if (!(a3 == null || animator == null || !(animator instanceof ValueAnimator))) {
                            ((ValueAnimator) animator).setValues(a3);
                        }
                        z = true;
                    } else {
                        throw new RuntimeException("Unknown animator name: " + xmlPullParser.getName());
                    }
                    if (animatorSet != null && !z) {
                        ArrayList arrayList2 = arrayList == null ? new ArrayList() : arrayList;
                        arrayList2.add(animator);
                        arrayList = arrayList2;
                    }
                }
            }
        }
        if (!(animatorSet == null || arrayList == null)) {
            Animator[] animatorArr = new Animator[arrayList.size()];
            Iterator it = arrayList.iterator();
            int i2 = 0;
            while (it.hasNext()) {
                int i3 = i2 + 1;
                animatorArr[i2] = (Animator) it.next();
                i2 = i3;
            }
            if (i == 0) {
                animatorSet.playTogether(animatorArr);
            } else {
                animatorSet.playSequentially(animatorArr);
            }
        }
        return animator;
    }

    private static Keyframe a(Keyframe keyframe, float f) {
        return keyframe.getType() == Float.TYPE ? Keyframe.ofFloat(f) : keyframe.getType() == Integer.TYPE ? Keyframe.ofInt(f) : Keyframe.ofObject(f);
    }

    private static Keyframe a(Context context, Resources resources, Theme theme, AttributeSet attributeSet, int i, XmlPullParser xmlPullParser) throws XmlPullParserException, IOException {
        TypedArray a2 = c.a(resources, theme, attributeSet, a.j);
        Keyframe keyframe = null;
        float a3 = c.a(a2, xmlPullParser, "fraction", 3, -1.0f);
        TypedValue b2 = c.b(a2, xmlPullParser, "value", 0);
        boolean z = b2 != null;
        if (i == 4) {
            i = (!z || !a(b2.type)) ? 0 : 3;
        }
        if (z) {
            switch (i) {
                case 0:
                    keyframe = Keyframe.ofFloat(a3, c.a(a2, xmlPullParser, "value", 0, 0.0f));
                    break;
                case 1:
                case 3:
                    keyframe = Keyframe.ofInt(a3, c.a(a2, xmlPullParser, "value", 0, 0));
                    break;
            }
        } else {
            keyframe = i == 0 ? Keyframe.ofFloat(a3) : Keyframe.ofInt(a3);
        }
        int c = c.c(a2, xmlPullParser, "interpolator", 1, 0);
        if (c > 0) {
            keyframe.setInterpolator(d.a(context, c));
        }
        a2.recycle();
        return keyframe;
    }

    private static ObjectAnimator a(Context context, Resources resources, Theme theme, AttributeSet attributeSet, float f, XmlPullParser xmlPullParser) throws NotFoundException {
        ObjectAnimator objectAnimator = new ObjectAnimator();
        a(context, resources, theme, attributeSet, objectAnimator, f, xmlPullParser);
        return objectAnimator;
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0043  */
    private static PropertyValuesHolder a(Context context, Resources resources, Theme theme, XmlPullParser xmlPullParser, String str, int i) throws XmlPullParserException, IOException {
        int i2;
        ArrayList arrayList;
        ArrayList arrayList2 = null;
        int i3 = i;
        while (true) {
            int next = xmlPullParser.next();
            if (next != 3 && next != 1) {
                if (xmlPullParser.getName().equals("keyframe")) {
                    if (i3 == 4) {
                        i3 = a(resources, theme, Xml.asAttributeSet(xmlPullParser), xmlPullParser);
                    }
                    Keyframe a2 = a(context, resources, theme, Xml.asAttributeSet(xmlPullParser), i3, xmlPullParser);
                    if (a2 != null) {
                        arrayList = arrayList2 == null ? new ArrayList() : arrayList2;
                        arrayList.add(a2);
                    } else {
                        arrayList = arrayList2;
                    }
                    xmlPullParser.next();
                } else {
                    arrayList = arrayList2;
                }
                arrayList2 = arrayList;
            } else if (arrayList2 != null) {
                int size = arrayList2.size();
                if (size > 0) {
                    Keyframe keyframe = (Keyframe) arrayList2.get(0);
                    Keyframe keyframe2 = (Keyframe) arrayList2.get(size - 1);
                    float fraction = keyframe2.getFraction();
                    if (fraction >= 1.0f) {
                        i2 = size;
                    } else if (fraction < 0.0f) {
                        keyframe2.setFraction(1.0f);
                        i2 = size;
                    } else {
                        arrayList2.add(arrayList2.size(), a(keyframe2, 1.0f));
                        i2 = size + 1;
                    }
                    float fraction2 = keyframe.getFraction();
                    if (fraction2 != 0.0f) {
                        if (fraction2 < 0.0f) {
                            keyframe.setFraction(0.0f);
                        } else {
                            arrayList2.add(0, a(keyframe, 0.0f));
                            i2++;
                        }
                    }
                    Keyframe[] keyframeArr = new Keyframe[i2];
                    arrayList2.toArray(keyframeArr);
                    for (int i4 = 0; i4 < i2; i4++) {
                        Keyframe keyframe3 = keyframeArr[i4];
                        if (keyframe3.getFraction() < 0.0f) {
                            if (i4 == 0) {
                                keyframe3.setFraction(0.0f);
                            } else if (i4 == i2 - 1) {
                                keyframe3.setFraction(1.0f);
                            } else {
                                int i5 = i4 + 1;
                                int i6 = i4;
                                while (i5 < i2 - 1 && keyframeArr[i5].getFraction() < 0.0f) {
                                    i6 = i5;
                                    i5++;
                                }
                                a(keyframeArr, keyframeArr[i6 + 1].getFraction() - keyframeArr[i4 - 1].getFraction(), i4, i6);
                            }
                        }
                    }
                    PropertyValuesHolder ofKeyframe = PropertyValuesHolder.ofKeyframe(str, keyframeArr);
                    if (i3 != 3) {
                        return ofKeyframe;
                    }
                    ofKeyframe.setEvaluator(f.a());
                    return ofKeyframe;
                }
            }
        }
        if (arrayList2 != null) {
        }
        return null;
    }

    private static PropertyValuesHolder a(TypedArray typedArray, int i, int i2, int i3, String str) {
        PropertyValuesHolder propertyValuesHolder;
        TypedValue peekValue = typedArray.peekValue(i2);
        boolean z = peekValue != null;
        int i4 = z ? peekValue.type : 0;
        TypedValue peekValue2 = typedArray.peekValue(i3);
        boolean z2 = peekValue2 != null;
        int i5 = z2 ? peekValue2.type : 0;
        if (i == 4) {
            i = ((!z || !a(i4)) && (!z2 || !a(i5))) ? 0 : 3;
        }
        boolean z3 = i == 0;
        if (i == 2) {
            String string = typedArray.getString(i2);
            String string2 = typedArray.getString(i3);
            C0014b[] b2 = b.b(string);
            C0014b[] b3 = b.b(string2);
            if (!(b2 == null && b3 == null)) {
                if (b2 != null) {
                    a aVar = new a();
                    if (b3 == null) {
                        return PropertyValuesHolder.ofObject(str, aVar, new Object[]{b2});
                    } else if (!b.a(b2, b3)) {
                        throw new InflateException(" Can't morph from " + string + " to " + string2);
                    } else {
                        return PropertyValuesHolder.ofObject(str, aVar, new Object[]{b2, b3});
                    }
                } else if (b3 != null) {
                    return PropertyValuesHolder.ofObject(str, new a(), new Object[]{b3});
                }
            }
            return null;
        }
        f fVar = null;
        if (i == 3) {
            fVar = f.a();
        }
        if (z3) {
            if (z) {
                float f = i4 == 5 ? typedArray.getDimension(i2, 0.0f) : typedArray.getFloat(i2, 0.0f);
                if (z2) {
                    propertyValuesHolder = PropertyValuesHolder.ofFloat(str, new float[]{f, i5 == 5 ? typedArray.getDimension(i3, 0.0f) : typedArray.getFloat(i3, 0.0f)});
                } else {
                    propertyValuesHolder = PropertyValuesHolder.ofFloat(str, new float[]{f});
                }
            } else {
                propertyValuesHolder = PropertyValuesHolder.ofFloat(str, new float[]{i5 == 5 ? typedArray.getDimension(i3, 0.0f) : typedArray.getFloat(i3, 0.0f)});
            }
        } else if (z) {
            int i6 = i4 == 5 ? (int) typedArray.getDimension(i2, 0.0f) : a(i4) ? typedArray.getColor(i2, 0) : typedArray.getInt(i2, 0);
            if (z2) {
                int i7 = i5 == 5 ? (int) typedArray.getDimension(i3, 0.0f) : a(i5) ? typedArray.getColor(i3, 0) : typedArray.getInt(i3, 0);
                propertyValuesHolder = PropertyValuesHolder.ofInt(str, new int[]{i6, i7});
            } else {
                propertyValuesHolder = PropertyValuesHolder.ofInt(str, new int[]{i6});
            }
        } else if (z2) {
            int i8 = i5 == 5 ? (int) typedArray.getDimension(i3, 0.0f) : a(i5) ? typedArray.getColor(i3, 0) : typedArray.getInt(i3, 0);
            propertyValuesHolder = PropertyValuesHolder.ofInt(str, new int[]{i8});
        } else {
            propertyValuesHolder = null;
        }
        if (propertyValuesHolder == null || fVar == null) {
            return propertyValuesHolder;
        }
        propertyValuesHolder.setEvaluator(fVar);
        return propertyValuesHolder;
    }

    private static ValueAnimator a(Context context, Resources resources, Theme theme, AttributeSet attributeSet, ValueAnimator valueAnimator, float f, XmlPullParser xmlPullParser) throws NotFoundException {
        TypedArray a2 = c.a(resources, theme, attributeSet, a.g);
        TypedArray a3 = c.a(resources, theme, attributeSet, a.k);
        if (valueAnimator == null) {
            valueAnimator = new ValueAnimator();
        }
        a(valueAnimator, a2, a3, f, xmlPullParser);
        int c = c.c(a2, xmlPullParser, "interpolator", 0, 0);
        if (c > 0) {
            valueAnimator.setInterpolator(d.a(context, c));
        }
        a2.recycle();
        if (a3 != null) {
            a3.recycle();
        }
        return valueAnimator;
    }

    private static void a(ValueAnimator valueAnimator, TypedArray typedArray, int i, float f, XmlPullParser xmlPullParser) {
        ObjectAnimator objectAnimator = (ObjectAnimator) valueAnimator;
        String a2 = c.a(typedArray, xmlPullParser, "pathData", 1);
        if (a2 != null) {
            String a3 = c.a(typedArray, xmlPullParser, "propertyXName", 2);
            String a4 = c.a(typedArray, xmlPullParser, "propertyYName", 3);
            if (i == 2 || i == 4) {
            }
            if (a3 == null && a4 == null) {
                throw new InflateException(typedArray.getPositionDescription() + " propertyXName or propertyYName is needed for PathData");
            }
            a(b.a(a2), objectAnimator, 0.5f * f, a3, a4);
            return;
        }
        objectAnimator.setPropertyName(c.a(typedArray, xmlPullParser, "propertyName", 0));
    }

    private static void a(ValueAnimator valueAnimator, TypedArray typedArray, TypedArray typedArray2, float f, XmlPullParser xmlPullParser) {
        long a2 = (long) c.a(typedArray, xmlPullParser, "duration", 1, 300);
        long a3 = (long) c.a(typedArray, xmlPullParser, "startOffset", 2, 0);
        int a4 = c.a(typedArray, xmlPullParser, "valueType", 7, 4);
        if (c.a(xmlPullParser, "valueFrom") && c.a(xmlPullParser, "valueTo")) {
            if (a4 == 4) {
                a4 = a(typedArray, 5, 6);
            }
            PropertyValuesHolder a5 = a(typedArray, a4, 5, 6, "");
            if (a5 != null) {
                valueAnimator.setValues(new PropertyValuesHolder[]{a5});
            }
        }
        valueAnimator.setDuration(a2);
        valueAnimator.setStartDelay(a3);
        valueAnimator.setRepeatCount(c.a(typedArray, xmlPullParser, "repeatCount", 3, 0));
        valueAnimator.setRepeatMode(c.a(typedArray, xmlPullParser, "repeatMode", 4, 1));
        if (typedArray2 != null) {
            a(valueAnimator, typedArray2, a4, f, xmlPullParser);
        }
    }

    private static void a(Path path, ObjectAnimator objectAnimator, float f, String str, String str2) {
        int i;
        PathMeasure pathMeasure = new PathMeasure(path, false);
        float f2 = 0.0f;
        ArrayList arrayList = new ArrayList();
        arrayList.add(Float.valueOf(0.0f));
        do {
            f2 += pathMeasure.getLength();
            arrayList.add(Float.valueOf(f2));
        } while (pathMeasure.nextContour());
        PathMeasure pathMeasure2 = new PathMeasure(path, false);
        int min = Math.min(100, ((int) (f2 / f)) + 1);
        float[] fArr = new float[min];
        float[] fArr2 = new float[min];
        float[] fArr3 = new float[2];
        int i2 = 0;
        float f3 = f2 / ((float) (min - 1));
        int i3 = 0;
        float f4 = 0.0f;
        while (i3 < min) {
            pathMeasure2.getPosTan(f4, fArr3, null);
            pathMeasure2.getPosTan(f4, fArr3, null);
            fArr[i3] = fArr3[0];
            fArr2[i3] = fArr3[1];
            float f5 = f4 + f3;
            if (i2 + 1 >= arrayList.size() || f5 <= ((Float) arrayList.get(i2 + 1)).floatValue()) {
                f4 = f5;
                i = i2;
            } else {
                f4 = f5 - ((Float) arrayList.get(i2 + 1)).floatValue();
                i = i2 + 1;
                pathMeasure2.nextContour();
            }
            i3++;
            i2 = i;
        }
        PropertyValuesHolder propertyValuesHolder = null;
        PropertyValuesHolder propertyValuesHolder2 = null;
        if (str != null) {
            propertyValuesHolder = PropertyValuesHolder.ofFloat(str, fArr);
        }
        if (str2 != null) {
            propertyValuesHolder2 = PropertyValuesHolder.ofFloat(str2, fArr2);
        }
        if (propertyValuesHolder == null) {
            objectAnimator.setValues(new PropertyValuesHolder[]{propertyValuesHolder2});
        } else if (propertyValuesHolder2 == null) {
            objectAnimator.setValues(new PropertyValuesHolder[]{propertyValuesHolder});
        } else {
            objectAnimator.setValues(new PropertyValuesHolder[]{propertyValuesHolder, propertyValuesHolder2});
        }
    }

    private static void a(Keyframe[] keyframeArr, float f, int i, int i2) {
        float f2 = f / ((float) ((i2 - i) + 2));
        while (i <= i2) {
            keyframeArr[i].setFraction(keyframeArr[i - 1].getFraction() + f2);
            i++;
        }
    }

    private static boolean a(int i) {
        return i >= 28 && i <= 31;
    }

    private static PropertyValuesHolder[] a(Context context, Resources resources, Theme theme, XmlPullParser xmlPullParser, AttributeSet attributeSet) throws XmlPullParserException, IOException {
        ArrayList arrayList;
        ArrayList arrayList2 = null;
        while (true) {
            int eventType = xmlPullParser.getEventType();
            if (eventType != 3 && eventType != 1) {
                if (eventType != 2) {
                    xmlPullParser.next();
                } else {
                    if (xmlPullParser.getName().equals("propertyValuesHolder")) {
                        TypedArray a2 = c.a(resources, theme, attributeSet, a.i);
                        String a3 = c.a(a2, xmlPullParser, "propertyName", 3);
                        int a4 = c.a(a2, xmlPullParser, "valueType", 2, 4);
                        PropertyValuesHolder a5 = a(context, resources, theme, xmlPullParser, a3, a4);
                        PropertyValuesHolder propertyValuesHolder = a5 == null ? a(a2, a4, 0, 1, a3) : a5;
                        if (propertyValuesHolder != null) {
                            arrayList = arrayList2 == null ? new ArrayList() : arrayList2;
                            arrayList.add(propertyValuesHolder);
                        } else {
                            arrayList = arrayList2;
                        }
                        a2.recycle();
                    } else {
                        arrayList = arrayList2;
                    }
                    xmlPullParser.next();
                    arrayList2 = arrayList;
                }
            }
        }
        if (arrayList2 == null) {
            return null;
        }
        int size = arrayList2.size();
        PropertyValuesHolder[] propertyValuesHolderArr = new PropertyValuesHolder[size];
        for (int i = 0; i < size; i++) {
            propertyValuesHolderArr[i] = (PropertyValuesHolder) arrayList2.get(i);
        }
        return propertyValuesHolderArr;
    }
}
