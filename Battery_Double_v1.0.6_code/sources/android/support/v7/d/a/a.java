package android.support.v7.d.a;

import android.content.res.ColorStateList;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff.Mode;
import android.graphics.Rect;
import android.graphics.Region;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Drawable.Callback;

public class a extends Drawable implements Callback {

    /* renamed from: a reason: collision with root package name */
    private Drawable f951a;

    public a(Drawable drawable) {
        a(drawable);
    }

    public void a(Drawable drawable) {
        if (this.f951a != null) {
            this.f951a.setCallback(null);
        }
        this.f951a = drawable;
        if (drawable != null) {
            drawable.setCallback(this);
        }
    }

    public Drawable b() {
        return this.f951a;
    }

    public void draw(Canvas canvas) {
        this.f951a.draw(canvas);
    }

    public int getChangingConfigurations() {
        return this.f951a.getChangingConfigurations();
    }

    public Drawable getCurrent() {
        return this.f951a.getCurrent();
    }

    public int getIntrinsicHeight() {
        return this.f951a.getIntrinsicHeight();
    }

    public int getIntrinsicWidth() {
        return this.f951a.getIntrinsicWidth();
    }

    public int getMinimumHeight() {
        return this.f951a.getMinimumHeight();
    }

    public int getMinimumWidth() {
        return this.f951a.getMinimumWidth();
    }

    public int getOpacity() {
        return this.f951a.getOpacity();
    }

    public boolean getPadding(Rect rect) {
        return this.f951a.getPadding(rect);
    }

    public int[] getState() {
        return this.f951a.getState();
    }

    public Region getTransparentRegion() {
        return this.f951a.getTransparentRegion();
    }

    public void invalidateDrawable(Drawable drawable) {
        invalidateSelf();
    }

    public boolean isAutoMirrored() {
        return android.support.v4.c.a.a.b(this.f951a);
    }

    public boolean isStateful() {
        return this.f951a.isStateful();
    }

    public void jumpToCurrentState() {
        android.support.v4.c.a.a.a(this.f951a);
    }

    /* access modifiers changed from: protected */
    public void onBoundsChange(Rect rect) {
        this.f951a.setBounds(rect);
    }

    /* access modifiers changed from: protected */
    public boolean onLevelChange(int i) {
        return this.f951a.setLevel(i);
    }

    public void scheduleDrawable(Drawable drawable, Runnable runnable, long j) {
        scheduleSelf(runnable, j);
    }

    public void setAlpha(int i) {
        this.f951a.setAlpha(i);
    }

    public void setAutoMirrored(boolean z) {
        android.support.v4.c.a.a.a(this.f951a, z);
    }

    public void setChangingConfigurations(int i) {
        this.f951a.setChangingConfigurations(i);
    }

    public void setColorFilter(ColorFilter colorFilter) {
        this.f951a.setColorFilter(colorFilter);
    }

    public void setDither(boolean z) {
        this.f951a.setDither(z);
    }

    public void setFilterBitmap(boolean z) {
        this.f951a.setFilterBitmap(z);
    }

    public void setHotspot(float f, float f2) {
        android.support.v4.c.a.a.a(this.f951a, f, f2);
    }

    public void setHotspotBounds(int i, int i2, int i3, int i4) {
        android.support.v4.c.a.a.a(this.f951a, i, i2, i3, i4);
    }

    public boolean setState(int[] iArr) {
        return this.f951a.setState(iArr);
    }

    public void setTint(int i) {
        android.support.v4.c.a.a.a(this.f951a, i);
    }

    public void setTintList(ColorStateList colorStateList) {
        android.support.v4.c.a.a.a(this.f951a, colorStateList);
    }

    public void setTintMode(Mode mode) {
        android.support.v4.c.a.a.a(this.f951a, mode);
    }

    public boolean setVisible(boolean z, boolean z2) {
        return super.setVisible(z, z2) || this.f951a.setVisible(z, z2);
    }

    public void unscheduleDrawable(Drawable drawable, Runnable runnable) {
        unscheduleSelf(runnable);
    }
}
