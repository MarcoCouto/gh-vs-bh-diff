package android.support.v7.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.i.aa;
import android.support.v4.i.t;
import android.support.v4.i.y;
import android.support.v4.i.z;
import android.support.v7.a.a.C0026a;
import android.support.v7.a.a.e;
import android.support.v7.a.a.f;
import android.support.v7.a.a.h;
import android.support.v7.a.a.j;
import android.support.v7.c.a.b;
import android.support.v7.view.menu.a;
import android.support.v7.view.menu.o;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window.Callback;

public class bp implements ak {

    /* renamed from: a reason: collision with root package name */
    Toolbar f1265a;

    /* renamed from: b reason: collision with root package name */
    CharSequence f1266b;
    Callback c;
    boolean d;
    private int e;
    private View f;
    private View g;
    private Drawable h;
    private Drawable i;
    private Drawable j;
    private boolean k;
    private CharSequence l;
    private CharSequence m;
    private d n;
    private int o;
    private int p;
    private Drawable q;

    public bp(Toolbar toolbar, boolean z) {
        this(toolbar, z, h.abc_action_bar_up_description, e.abc_ic_ab_back_material);
    }

    public bp(Toolbar toolbar, boolean z, int i2, int i3) {
        this.o = 0;
        this.p = 0;
        this.f1265a = toolbar;
        this.f1266b = toolbar.getTitle();
        this.l = toolbar.getSubtitle();
        this.k = this.f1266b != null;
        this.j = toolbar.getNavigationIcon();
        bo a2 = bo.a(toolbar.getContext(), null, j.ActionBar, C0026a.actionBarStyle, 0);
        this.q = a2.a(j.ActionBar_homeAsUpIndicator);
        if (z) {
            CharSequence c2 = a2.c(j.ActionBar_title);
            if (!TextUtils.isEmpty(c2)) {
                b(c2);
            }
            CharSequence c3 = a2.c(j.ActionBar_subtitle);
            if (!TextUtils.isEmpty(c3)) {
                c(c3);
            }
            Drawable a3 = a2.a(j.ActionBar_logo);
            if (a3 != null) {
                b(a3);
            }
            Drawable a4 = a2.a(j.ActionBar_icon);
            if (a4 != null) {
                a(a4);
            }
            if (this.j == null && this.q != null) {
                c(this.q);
            }
            c(a2.a(j.ActionBar_displayOptions, 0));
            int g2 = a2.g(j.ActionBar_customNavigationLayout, 0);
            if (g2 != 0) {
                a(LayoutInflater.from(this.f1265a.getContext()).inflate(g2, this.f1265a, false));
                c(this.e | 16);
            }
            int f2 = a2.f(j.ActionBar_height, 0);
            if (f2 > 0) {
                LayoutParams layoutParams = this.f1265a.getLayoutParams();
                layoutParams.height = f2;
                this.f1265a.setLayoutParams(layoutParams);
            }
            int d2 = a2.d(j.ActionBar_contentInsetStart, -1);
            int d3 = a2.d(j.ActionBar_contentInsetEnd, -1);
            if (d2 >= 0 || d3 >= 0) {
                this.f1265a.a(Math.max(d2, 0), Math.max(d3, 0));
            }
            int g3 = a2.g(j.ActionBar_titleTextStyle, 0);
            if (g3 != 0) {
                this.f1265a.a(this.f1265a.getContext(), g3);
            }
            int g4 = a2.g(j.ActionBar_subtitleTextStyle, 0);
            if (g4 != 0) {
                this.f1265a.b(this.f1265a.getContext(), g4);
            }
            int g5 = a2.g(j.ActionBar_popupTheme, 0);
            if (g5 != 0) {
                this.f1265a.setPopupTheme(g5);
            }
        } else {
            this.e = r();
        }
        a2.a();
        e(i2);
        this.m = this.f1265a.getNavigationContentDescription();
        this.f1265a.setNavigationOnClickListener(new OnClickListener() {

            /* renamed from: a reason: collision with root package name */
            final a f1267a = new a(bp.this.f1265a.getContext(), 0, 16908332, 0, 0, bp.this.f1266b);

            public void onClick(View view) {
                if (bp.this.c != null && bp.this.d) {
                    bp.this.c.onMenuItemSelected(0, this.f1267a);
                }
            }
        });
    }

    private void e(CharSequence charSequence) {
        this.f1266b = charSequence;
        if ((this.e & 8) != 0) {
            this.f1265a.setTitle(charSequence);
        }
    }

    private int r() {
        if (this.f1265a.getNavigationIcon() == null) {
            return 11;
        }
        this.q = this.f1265a.getNavigationIcon();
        return 15;
    }

    private void s() {
        Drawable drawable = null;
        if ((this.e & 2) != 0) {
            drawable = (this.e & 1) != 0 ? this.i != null ? this.i : this.h : this.h;
        }
        this.f1265a.setLogo(drawable);
    }

    private void t() {
        if ((this.e & 4) != 0) {
            this.f1265a.setNavigationIcon(this.j != null ? this.j : this.q);
        } else {
            this.f1265a.setNavigationIcon((Drawable) null);
        }
    }

    private void u() {
        if ((this.e & 4) == 0) {
            return;
        }
        if (TextUtils.isEmpty(this.m)) {
            this.f1265a.setNavigationContentDescription(this.p);
        } else {
            this.f1265a.setNavigationContentDescription(this.m);
        }
    }

    public y a(final int i2, long j2) {
        return t.j(this.f1265a).a(i2 == 0 ? 1.0f : 0.0f).a(j2).a((z) new aa() {
            private boolean c = false;

            public void a(View view) {
                bp.this.f1265a.setVisibility(0);
            }

            public void b(View view) {
                if (!this.c) {
                    bp.this.f1265a.setVisibility(i2);
                }
            }

            public void c(View view) {
                this.c = true;
            }
        });
    }

    public ViewGroup a() {
        return this.f1265a;
    }

    public void a(int i2) {
        a(i2 != 0 ? b.b(b(), i2) : null);
    }

    public void a(Drawable drawable) {
        this.h = drawable;
        s();
    }

    public void a(o.a aVar, android.support.v7.view.menu.h.a aVar2) {
        this.f1265a.a(aVar, aVar2);
    }

    public void a(bg bgVar) {
        if (this.f != null && this.f.getParent() == this.f1265a) {
            this.f1265a.removeView(this.f);
        }
        this.f = bgVar;
        if (bgVar != null && this.o == 2) {
            this.f1265a.addView(this.f, 0);
            Toolbar.b bVar = (Toolbar.b) this.f.getLayoutParams();
            bVar.width = -2;
            bVar.height = -2;
            bVar.f889a = 8388691;
            bgVar.setAllowCollapse(true);
        }
    }

    public void a(Menu menu, o.a aVar) {
        if (this.n == null) {
            this.n = new d(this.f1265a.getContext());
            this.n.a(f.action_menu_presenter);
        }
        this.n.a(aVar);
        this.f1265a.a((android.support.v7.view.menu.h) menu, this.n);
    }

    public void a(View view) {
        if (!(this.g == null || (this.e & 16) == 0)) {
            this.f1265a.removeView(this.g);
        }
        this.g = view;
        if (view != null && (this.e & 16) != 0) {
            this.f1265a.addView(this.g);
        }
    }

    public void a(Callback callback) {
        this.c = callback;
    }

    public void a(CharSequence charSequence) {
        if (!this.k) {
            e(charSequence);
        }
    }

    public void a(boolean z) {
        this.f1265a.setCollapsible(z);
    }

    public Context b() {
        return this.f1265a.getContext();
    }

    public void b(int i2) {
        b(i2 != 0 ? b.b(b(), i2) : null);
    }

    public void b(Drawable drawable) {
        this.i = drawable;
        s();
    }

    public void b(CharSequence charSequence) {
        this.k = true;
        e(charSequence);
    }

    public void b(boolean z) {
    }

    public void c(int i2) {
        int i3 = this.e ^ i2;
        this.e = i2;
        if (i3 != 0) {
            if ((i3 & 4) != 0) {
                if ((i2 & 4) != 0) {
                    u();
                }
                t();
            }
            if ((i3 & 3) != 0) {
                s();
            }
            if ((i3 & 8) != 0) {
                if ((i2 & 8) != 0) {
                    this.f1265a.setTitle(this.f1266b);
                    this.f1265a.setSubtitle(this.l);
                } else {
                    this.f1265a.setTitle((CharSequence) null);
                    this.f1265a.setSubtitle((CharSequence) null);
                }
            }
            if ((i3 & 16) != 0 && this.g != null) {
                if ((i2 & 16) != 0) {
                    this.f1265a.addView(this.g);
                } else {
                    this.f1265a.removeView(this.g);
                }
            }
        }
    }

    public void c(Drawable drawable) {
        this.j = drawable;
        t();
    }

    public void c(CharSequence charSequence) {
        this.l = charSequence;
        if ((this.e & 8) != 0) {
            this.f1265a.setSubtitle(charSequence);
        }
    }

    public boolean c() {
        return this.f1265a.g();
    }

    public void d() {
        this.f1265a.h();
    }

    public void d(int i2) {
        this.f1265a.setVisibility(i2);
    }

    public void d(CharSequence charSequence) {
        this.m = charSequence;
        u();
    }

    public CharSequence e() {
        return this.f1265a.getTitle();
    }

    public void e(int i2) {
        if (i2 != this.p) {
            this.p = i2;
            if (TextUtils.isEmpty(this.f1265a.getNavigationContentDescription())) {
                f(this.p);
            }
        }
    }

    public void f() {
        Log.i("ToolbarWidgetWrapper", "Progress display unsupported");
    }

    public void f(int i2) {
        d((CharSequence) i2 == 0 ? null : b().getString(i2));
    }

    public void g() {
        Log.i("ToolbarWidgetWrapper", "Progress display unsupported");
    }

    public boolean h() {
        return this.f1265a.a();
    }

    public boolean i() {
        return this.f1265a.b();
    }

    public boolean j() {
        return this.f1265a.c();
    }

    public boolean k() {
        return this.f1265a.d();
    }

    public boolean l() {
        return this.f1265a.e();
    }

    public void m() {
        this.d = true;
    }

    public void n() {
        this.f1265a.f();
    }

    public int o() {
        return this.e;
    }

    public int p() {
        return this.o;
    }

    public Menu q() {
        return this.f1265a.getMenu();
    }
}
