package android.support.v7.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.support.v4.i.s;
import android.support.v4.widget.b;
import android.support.v7.a.a.C0026a;
import android.util.AttributeSet;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.Button;
import android.widget.TextView;

public class i extends Button implements s, b {

    /* renamed from: b reason: collision with root package name */
    private final h f1314b;
    private final z c;

    public i(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, C0026a.buttonStyle);
    }

    public i(Context context, AttributeSet attributeSet, int i) {
        super(bl.a(context), attributeSet, i);
        this.f1314b = new h(this);
        this.f1314b.a(attributeSet, i);
        this.c = z.a((TextView) this);
        this.c.a(attributeSet, i);
        this.c.a();
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        if (this.f1314b != null) {
            this.f1314b.c();
        }
        if (this.c != null) {
            this.c.a();
        }
    }

    public int getAutoSizeMaxTextSize() {
        if (f832a) {
            return super.getAutoSizeMaxTextSize();
        }
        if (this.c != null) {
            return this.c.g();
        }
        return -1;
    }

    public int getAutoSizeMinTextSize() {
        if (f832a) {
            return super.getAutoSizeMinTextSize();
        }
        if (this.c != null) {
            return this.c.f();
        }
        return -1;
    }

    public int getAutoSizeStepGranularity() {
        if (f832a) {
            return super.getAutoSizeStepGranularity();
        }
        if (this.c != null) {
            return this.c.e();
        }
        return -1;
    }

    public int[] getAutoSizeTextAvailableSizes() {
        return f832a ? super.getAutoSizeTextAvailableSizes() : this.c != null ? this.c.h() : new int[0];
    }

    public int getAutoSizeTextType() {
        if (f832a) {
            return super.getAutoSizeTextType() == 1 ? 1 : 0;
        }
        if (this.c != null) {
            return this.c.d();
        }
        return 0;
    }

    public ColorStateList getSupportBackgroundTintList() {
        if (this.f1314b != null) {
            return this.f1314b.a();
        }
        return null;
    }

    public Mode getSupportBackgroundTintMode() {
        if (this.f1314b != null) {
            return this.f1314b.b();
        }
        return null;
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        super.onInitializeAccessibilityEvent(accessibilityEvent);
        accessibilityEvent.setClassName(Button.class.getName());
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo accessibilityNodeInfo) {
        super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo);
        accessibilityNodeInfo.setClassName(Button.class.getName());
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        if (this.c != null) {
            this.c.a(z, i, i2, i3, i4);
        }
    }

    /* access modifiers changed from: protected */
    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        super.onTextChanged(charSequence, i, i2, i3);
        if (this.c != null && !f832a && this.c.c()) {
            this.c.b();
        }
    }

    public void setAutoSizeTextTypeUniformWithConfiguration(int i, int i2, int i3, int i4) throws IllegalArgumentException {
        if (f832a) {
            super.setAutoSizeTextTypeUniformWithConfiguration(i, i2, i3, i4);
        } else if (this.c != null) {
            this.c.a(i, i2, i3, i4);
        }
    }

    public void setAutoSizeTextTypeUniformWithPresetSizes(int[] iArr, int i) throws IllegalArgumentException {
        if (f832a) {
            super.setAutoSizeTextTypeUniformWithPresetSizes(iArr, i);
        } else if (this.c != null) {
            this.c.a(iArr, i);
        }
    }

    public void setAutoSizeTextTypeWithDefaults(int i) {
        if (f832a) {
            super.setAutoSizeTextTypeWithDefaults(i);
        } else if (this.c != null) {
            this.c.a(i);
        }
    }

    public void setBackgroundDrawable(Drawable drawable) {
        super.setBackgroundDrawable(drawable);
        if (this.f1314b != null) {
            this.f1314b.a(drawable);
        }
    }

    public void setBackgroundResource(int i) {
        super.setBackgroundResource(i);
        if (this.f1314b != null) {
            this.f1314b.a(i);
        }
    }

    public void setSupportAllCaps(boolean z) {
        if (this.c != null) {
            this.c.a(z);
        }
    }

    public void setSupportBackgroundTintList(ColorStateList colorStateList) {
        if (this.f1314b != null) {
            this.f1314b.a(colorStateList);
        }
    }

    public void setSupportBackgroundTintMode(Mode mode) {
        if (this.f1314b != null) {
            this.f1314b.a(mode);
        }
    }

    public void setTextAppearance(Context context, int i) {
        super.setTextAppearance(context, i);
        if (this.c != null) {
            this.c.a(context, i);
        }
    }

    public void setTextSize(int i, float f) {
        if (f832a) {
            super.setTextSize(i, f);
        } else if (this.c != null) {
            this.c.a(i, f);
        }
    }
}
