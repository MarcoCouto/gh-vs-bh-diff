package android.support.v7.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.ClassLoaderCreator;
import android.os.Parcelable.Creator;
import android.support.v4.i.g;
import android.support.v4.i.t;
import android.support.v7.a.a.C0026a;
import android.support.v7.app.a.C0028a;
import android.support.v7.view.menu.h;
import android.support.v7.view.menu.j;
import android.support.v7.view.menu.o;
import android.support.v7.view.menu.u;
import android.support.v7.widget.ActionMenuView.e;
import android.text.TextUtils;
import android.text.TextUtils.TruncateAt;
import android.util.AttributeSet;
import android.view.ContextThemeWrapper;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;

public class Toolbar extends ViewGroup {
    private int A;
    private int B;
    private boolean C;
    private boolean D;
    private final ArrayList<View> E;
    private final ArrayList<View> F;
    private final int[] G;
    private final e H;
    private bp I;
    private d J;
    private a K;
    private android.support.v7.view.menu.o.a L;
    private android.support.v7.view.menu.h.a M;
    private boolean N;
    private final Runnable O;

    /* renamed from: a reason: collision with root package name */
    ImageButton f1129a;

    /* renamed from: b reason: collision with root package name */
    View f1130b;
    int c;
    c d;
    private ActionMenuView e;
    private TextView f;
    private TextView g;
    private ImageButton h;
    private ImageView i;
    private Drawable j;
    private CharSequence k;
    private Context l;
    private int m;
    private int n;
    private int o;
    private int p;
    private int q;
    private int r;
    private int s;
    private int t;
    private be u;
    private int v;
    private int w;
    private int x;
    private CharSequence y;
    private CharSequence z;

    private class a implements o {

        /* renamed from: a reason: collision with root package name */
        h f1134a;

        /* renamed from: b reason: collision with root package name */
        j f1135b;

        a() {
        }

        public void a(Context context, h hVar) {
            if (!(this.f1134a == null || this.f1135b == null)) {
                this.f1134a.d(this.f1135b);
            }
            this.f1134a = hVar;
        }

        public void a(Parcelable parcelable) {
        }

        public void a(h hVar, boolean z) {
        }

        public void a(android.support.v7.view.menu.o.a aVar) {
        }

        public void a(boolean z) {
            boolean z2 = false;
            if (this.f1135b != null) {
                if (this.f1134a != null) {
                    int size = this.f1134a.size();
                    int i = 0;
                    while (true) {
                        if (i >= size) {
                            break;
                        } else if (this.f1134a.getItem(i) == this.f1135b) {
                            z2 = true;
                            break;
                        } else {
                            i++;
                        }
                    }
                }
                if (!z2) {
                    b(this.f1134a, this.f1135b);
                }
            }
        }

        public boolean a() {
            return false;
        }

        public boolean a(h hVar, j jVar) {
            Toolbar.this.i();
            if (Toolbar.this.f1129a.getParent() != Toolbar.this) {
                Toolbar.this.addView(Toolbar.this.f1129a);
            }
            Toolbar.this.f1130b = jVar.getActionView();
            this.f1135b = jVar;
            if (Toolbar.this.f1130b.getParent() != Toolbar.this) {
                b j = Toolbar.this.generateDefaultLayoutParams();
                j.f889a = 8388611 | (Toolbar.this.c & 112);
                j.f1136b = 2;
                Toolbar.this.f1130b.setLayoutParams(j);
                Toolbar.this.addView(Toolbar.this.f1130b);
            }
            Toolbar.this.k();
            Toolbar.this.requestLayout();
            jVar.e(true);
            if (Toolbar.this.f1130b instanceof android.support.v7.view.c) {
                ((android.support.v7.view.c) Toolbar.this.f1130b).a();
            }
            return true;
        }

        public boolean a(u uVar) {
            return false;
        }

        public int b() {
            return 0;
        }

        public boolean b(h hVar, j jVar) {
            if (Toolbar.this.f1130b instanceof android.support.v7.view.c) {
                ((android.support.v7.view.c) Toolbar.this.f1130b).b();
            }
            Toolbar.this.removeView(Toolbar.this.f1130b);
            Toolbar.this.removeView(Toolbar.this.f1129a);
            Toolbar.this.f1130b = null;
            Toolbar.this.l();
            this.f1135b = null;
            Toolbar.this.requestLayout();
            jVar.e(false);
            return true;
        }

        public Parcelable c() {
            return null;
        }
    }

    public static class b extends C0028a {

        /* renamed from: b reason: collision with root package name */
        int f1136b;

        public b(int i, int i2) {
            super(i, i2);
            this.f1136b = 0;
            this.f889a = 8388627;
        }

        public b(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            this.f1136b = 0;
        }

        public b(C0028a aVar) {
            super(aVar);
            this.f1136b = 0;
        }

        public b(b bVar) {
            super((C0028a) bVar);
            this.f1136b = 0;
            this.f1136b = bVar.f1136b;
        }

        public b(LayoutParams layoutParams) {
            super(layoutParams);
            this.f1136b = 0;
        }

        public b(MarginLayoutParams marginLayoutParams) {
            super((LayoutParams) marginLayoutParams);
            this.f1136b = 0;
            a(marginLayoutParams);
        }

        /* access modifiers changed from: 0000 */
        public void a(MarginLayoutParams marginLayoutParams) {
            this.leftMargin = marginLayoutParams.leftMargin;
            this.topMargin = marginLayoutParams.topMargin;
            this.rightMargin = marginLayoutParams.rightMargin;
            this.bottomMargin = marginLayoutParams.bottomMargin;
        }
    }

    public interface c {
        boolean a(MenuItem menuItem);
    }

    public static class d extends android.support.v4.i.a {
        public static final Creator<d> CREATOR = new ClassLoaderCreator<d>() {
            /* renamed from: a */
            public d createFromParcel(Parcel parcel) {
                return new d(parcel, null);
            }

            /* renamed from: a */
            public d createFromParcel(Parcel parcel, ClassLoader classLoader) {
                return new d(parcel, classLoader);
            }

            /* renamed from: a */
            public d[] newArray(int i) {
                return new d[i];
            }
        };

        /* renamed from: a reason: collision with root package name */
        int f1137a;

        /* renamed from: b reason: collision with root package name */
        boolean f1138b;

        public d(Parcel parcel, ClassLoader classLoader) {
            super(parcel, classLoader);
            this.f1137a = parcel.readInt();
            this.f1138b = parcel.readInt() != 0;
        }

        public d(Parcelable parcelable) {
            super(parcelable);
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.f1137a);
            parcel.writeInt(this.f1138b ? 1 : 0);
        }
    }

    public Toolbar(Context context) {
        this(context, null);
    }

    public Toolbar(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, C0026a.toolbarStyle);
    }

    public Toolbar(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.x = 8388627;
        this.E = new ArrayList<>();
        this.F = new ArrayList<>();
        this.G = new int[2];
        this.H = new e() {
            public boolean a(MenuItem menuItem) {
                if (Toolbar.this.d != null) {
                    return Toolbar.this.d.a(menuItem);
                }
                return false;
            }
        };
        this.O = new Runnable() {
            public void run() {
                Toolbar.this.d();
            }
        };
        bo a2 = bo.a(getContext(), attributeSet, android.support.v7.a.a.j.Toolbar, i2, 0);
        this.n = a2.g(android.support.v7.a.a.j.Toolbar_titleTextAppearance, 0);
        this.o = a2.g(android.support.v7.a.a.j.Toolbar_subtitleTextAppearance, 0);
        this.x = a2.c(android.support.v7.a.a.j.Toolbar_android_gravity, this.x);
        this.c = a2.c(android.support.v7.a.a.j.Toolbar_buttonGravity, 48);
        int d2 = a2.d(android.support.v7.a.a.j.Toolbar_titleMargin, 0);
        if (a2.g(android.support.v7.a.a.j.Toolbar_titleMargins)) {
            d2 = a2.d(android.support.v7.a.a.j.Toolbar_titleMargins, d2);
        }
        this.t = d2;
        this.s = d2;
        this.r = d2;
        this.q = d2;
        int d3 = a2.d(android.support.v7.a.a.j.Toolbar_titleMarginStart, -1);
        if (d3 >= 0) {
            this.q = d3;
        }
        int d4 = a2.d(android.support.v7.a.a.j.Toolbar_titleMarginEnd, -1);
        if (d4 >= 0) {
            this.r = d4;
        }
        int d5 = a2.d(android.support.v7.a.a.j.Toolbar_titleMarginTop, -1);
        if (d5 >= 0) {
            this.s = d5;
        }
        int d6 = a2.d(android.support.v7.a.a.j.Toolbar_titleMarginBottom, -1);
        if (d6 >= 0) {
            this.t = d6;
        }
        this.p = a2.e(android.support.v7.a.a.j.Toolbar_maxButtonHeight, -1);
        int d7 = a2.d(android.support.v7.a.a.j.Toolbar_contentInsetStart, Integer.MIN_VALUE);
        int d8 = a2.d(android.support.v7.a.a.j.Toolbar_contentInsetEnd, Integer.MIN_VALUE);
        int e2 = a2.e(android.support.v7.a.a.j.Toolbar_contentInsetLeft, 0);
        int e3 = a2.e(android.support.v7.a.a.j.Toolbar_contentInsetRight, 0);
        s();
        this.u.b(e2, e3);
        if (!(d7 == Integer.MIN_VALUE && d8 == Integer.MIN_VALUE)) {
            this.u.a(d7, d8);
        }
        this.v = a2.d(android.support.v7.a.a.j.Toolbar_contentInsetStartWithNavigation, Integer.MIN_VALUE);
        this.w = a2.d(android.support.v7.a.a.j.Toolbar_contentInsetEndWithActions, Integer.MIN_VALUE);
        this.j = a2.a(android.support.v7.a.a.j.Toolbar_collapseIcon);
        this.k = a2.c(android.support.v7.a.a.j.Toolbar_collapseContentDescription);
        CharSequence c2 = a2.c(android.support.v7.a.a.j.Toolbar_title);
        if (!TextUtils.isEmpty(c2)) {
            setTitle(c2);
        }
        CharSequence c3 = a2.c(android.support.v7.a.a.j.Toolbar_subtitle);
        if (!TextUtils.isEmpty(c3)) {
            setSubtitle(c3);
        }
        this.l = getContext();
        setPopupTheme(a2.g(android.support.v7.a.a.j.Toolbar_popupTheme, 0));
        Drawable a3 = a2.a(android.support.v7.a.a.j.Toolbar_navigationIcon);
        if (a3 != null) {
            setNavigationIcon(a3);
        }
        CharSequence c4 = a2.c(android.support.v7.a.a.j.Toolbar_navigationContentDescription);
        if (!TextUtils.isEmpty(c4)) {
            setNavigationContentDescription(c4);
        }
        Drawable a4 = a2.a(android.support.v7.a.a.j.Toolbar_logo);
        if (a4 != null) {
            setLogo(a4);
        }
        CharSequence c5 = a2.c(android.support.v7.a.a.j.Toolbar_logoDescription);
        if (!TextUtils.isEmpty(c5)) {
            setLogoDescription(c5);
        }
        if (a2.g(android.support.v7.a.a.j.Toolbar_titleTextColor)) {
            setTitleTextColor(a2.b(android.support.v7.a.a.j.Toolbar_titleTextColor, -1));
        }
        if (a2.g(android.support.v7.a.a.j.Toolbar_subtitleTextColor)) {
            setSubtitleTextColor(a2.b(android.support.v7.a.a.j.Toolbar_subtitleTextColor, -1));
        }
        a2.a();
    }

    private int a(View view, int i2) {
        int i3;
        b bVar = (b) view.getLayoutParams();
        int measuredHeight = view.getMeasuredHeight();
        int i4 = i2 > 0 ? (measuredHeight - i2) / 2 : 0;
        switch (b(bVar.f889a)) {
            case 48:
                return getPaddingTop() - i4;
            case 80:
                return (((getHeight() - getPaddingBottom()) - measuredHeight) - bVar.bottomMargin) - i4;
            default:
                int paddingTop = getPaddingTop();
                int paddingBottom = getPaddingBottom();
                int height = getHeight();
                int i5 = (((height - paddingTop) - paddingBottom) - measuredHeight) / 2;
                if (i5 < bVar.topMargin) {
                    i3 = bVar.topMargin;
                } else {
                    int i6 = (((height - paddingBottom) - measuredHeight) - i5) - paddingTop;
                    i3 = i6 < bVar.bottomMargin ? Math.max(0, i5 - (bVar.bottomMargin - i6)) : i5;
                }
                return i3 + paddingTop;
        }
    }

    private int a(View view, int i2, int i3, int i4, int i5, int[] iArr) {
        MarginLayoutParams marginLayoutParams = (MarginLayoutParams) view.getLayoutParams();
        int i6 = marginLayoutParams.leftMargin - iArr[0];
        int i7 = marginLayoutParams.rightMargin - iArr[1];
        int max = Math.max(0, i6) + Math.max(0, i7);
        iArr[0] = Math.max(0, -i6);
        iArr[1] = Math.max(0, -i7);
        view.measure(getChildMeasureSpec(i2, getPaddingLeft() + getPaddingRight() + max + i3, marginLayoutParams.width), getChildMeasureSpec(i4, getPaddingTop() + getPaddingBottom() + marginLayoutParams.topMargin + marginLayoutParams.bottomMargin + i5, marginLayoutParams.height));
        return view.getMeasuredWidth() + max;
    }

    private int a(View view, int i2, int[] iArr, int i3) {
        b bVar = (b) view.getLayoutParams();
        int i4 = bVar.leftMargin - iArr[0];
        int max = Math.max(0, i4) + i2;
        iArr[0] = Math.max(0, -i4);
        int a2 = a(view, i3);
        int measuredWidth = view.getMeasuredWidth();
        view.layout(max, a2, max + measuredWidth, view.getMeasuredHeight() + a2);
        return bVar.rightMargin + measuredWidth + max;
    }

    private int a(List<View> list, int[] iArr) {
        int i2 = iArr[0];
        int i3 = iArr[1];
        int size = list.size();
        int i4 = 0;
        int i5 = 0;
        int i6 = i3;
        int i7 = i2;
        while (i4 < size) {
            View view = (View) list.get(i4);
            b bVar = (b) view.getLayoutParams();
            int i8 = bVar.leftMargin - i7;
            int i9 = bVar.rightMargin - i6;
            int max = Math.max(0, i8);
            int max2 = Math.max(0, i9);
            i7 = Math.max(0, -i8);
            i6 = Math.max(0, -i9);
            i4++;
            i5 += view.getMeasuredWidth() + max + max2;
        }
        return i5;
    }

    private void a(View view, int i2, int i3, int i4, int i5, int i6) {
        MarginLayoutParams marginLayoutParams = (MarginLayoutParams) view.getLayoutParams();
        int childMeasureSpec = getChildMeasureSpec(i2, getPaddingLeft() + getPaddingRight() + marginLayoutParams.leftMargin + marginLayoutParams.rightMargin + i3, marginLayoutParams.width);
        int childMeasureSpec2 = getChildMeasureSpec(i4, getPaddingTop() + getPaddingBottom() + marginLayoutParams.topMargin + marginLayoutParams.bottomMargin + i5, marginLayoutParams.height);
        int mode = MeasureSpec.getMode(childMeasureSpec2);
        if (mode != 1073741824 && i6 >= 0) {
            if (mode != 0) {
                i6 = Math.min(MeasureSpec.getSize(childMeasureSpec2), i6);
            }
            childMeasureSpec2 = MeasureSpec.makeMeasureSpec(i6, 1073741824);
        }
        view.measure(childMeasureSpec, childMeasureSpec2);
    }

    private void a(View view, boolean z2) {
        LayoutParams layoutParams = view.getLayoutParams();
        b bVar = layoutParams == null ? generateDefaultLayoutParams() : !checkLayoutParams(layoutParams) ? generateLayoutParams(layoutParams) : (b) layoutParams;
        bVar.f1136b = 1;
        if (!z2 || this.f1130b == null) {
            addView(view, bVar);
            return;
        }
        view.setLayoutParams(bVar);
        this.F.add(view);
    }

    private void a(List<View> list, int i2) {
        boolean z2 = true;
        if (t.e(this) != 1) {
            z2 = false;
        }
        int childCount = getChildCount();
        int a2 = android.support.v4.i.e.a(i2, t.e(this));
        list.clear();
        if (z2) {
            for (int i3 = childCount - 1; i3 >= 0; i3--) {
                View childAt = getChildAt(i3);
                b bVar = (b) childAt.getLayoutParams();
                if (bVar.f1136b == 0 && a(childAt) && c(bVar.f889a) == a2) {
                    list.add(childAt);
                }
            }
            return;
        }
        for (int i4 = 0; i4 < childCount; i4++) {
            View childAt2 = getChildAt(i4);
            b bVar2 = (b) childAt2.getLayoutParams();
            if (bVar2.f1136b == 0 && a(childAt2) && c(bVar2.f889a) == a2) {
                list.add(childAt2);
            }
        }
    }

    private boolean a(View view) {
        return (view == null || view.getParent() != this || view.getVisibility() == 8) ? false : true;
    }

    private int b(int i2) {
        int i3 = i2 & 112;
        switch (i3) {
            case 16:
            case 48:
            case 80:
                return i3;
            default:
                return this.x & 112;
        }
    }

    private int b(View view) {
        MarginLayoutParams marginLayoutParams = (MarginLayoutParams) view.getLayoutParams();
        return g.b(marginLayoutParams) + g.a(marginLayoutParams);
    }

    private int b(View view, int i2, int[] iArr, int i3) {
        b bVar = (b) view.getLayoutParams();
        int i4 = bVar.rightMargin - iArr[1];
        int max = i2 - Math.max(0, i4);
        iArr[1] = Math.max(0, -i4);
        int a2 = a(view, i3);
        int measuredWidth = view.getMeasuredWidth();
        view.layout(max - measuredWidth, a2, max, view.getMeasuredHeight() + a2);
        return max - (bVar.leftMargin + measuredWidth);
    }

    private int c(int i2) {
        int e2 = t.e(this);
        int a2 = android.support.v4.i.e.a(i2, e2) & 7;
        switch (a2) {
            case 1:
            case 3:
            case 5:
                return a2;
            default:
                return e2 == 1 ? 5 : 3;
        }
    }

    private int c(View view) {
        MarginLayoutParams marginLayoutParams = (MarginLayoutParams) view.getLayoutParams();
        return marginLayoutParams.bottomMargin + marginLayoutParams.topMargin;
    }

    private boolean d(View view) {
        return view.getParent() == this || this.F.contains(view);
    }

    private MenuInflater getMenuInflater() {
        return new android.support.v7.view.g(getContext());
    }

    private void m() {
        if (this.i == null) {
            this.i = new q(getContext());
        }
    }

    private void n() {
        o();
        if (this.e.d() == null) {
            h hVar = (h) this.e.getMenu();
            if (this.K == null) {
                this.K = new a();
            }
            this.e.setExpandedActionViewsExclusive(true);
            hVar.a((o) this.K, this.l);
        }
    }

    private void o() {
        if (this.e == null) {
            this.e = new ActionMenuView(getContext());
            this.e.setPopupTheme(this.m);
            this.e.setOnMenuItemClickListener(this.H);
            this.e.a(this.L, this.M);
            b j2 = generateDefaultLayoutParams();
            j2.f889a = 8388613 | (this.c & 112);
            this.e.setLayoutParams(j2);
            a((View) this.e, false);
        }
    }

    private void p() {
        if (this.h == null) {
            this.h = new o(getContext(), null, C0026a.toolbarNavigationButtonStyle);
            b j2 = generateDefaultLayoutParams();
            j2.f889a = 8388611 | (this.c & 112);
            this.h.setLayoutParams(j2);
        }
    }

    private void q() {
        removeCallbacks(this.O);
        post(this.O);
    }

    private boolean r() {
        if (!this.N) {
            return false;
        }
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = getChildAt(i2);
            if (a(childAt) && childAt.getMeasuredWidth() > 0 && childAt.getMeasuredHeight() > 0) {
                return false;
            }
        }
        return true;
    }

    private void s() {
        if (this.u == null) {
            this.u = new be();
        }
    }

    /* renamed from: a */
    public b generateLayoutParams(AttributeSet attributeSet) {
        return new b(getContext(), attributeSet);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public b generateLayoutParams(LayoutParams layoutParams) {
        return layoutParams instanceof b ? new b((b) layoutParams) : layoutParams instanceof C0028a ? new b((C0028a) layoutParams) : layoutParams instanceof MarginLayoutParams ? new b((MarginLayoutParams) layoutParams) : new b(layoutParams);
    }

    public void a(int i2) {
        getMenuInflater().inflate(i2, getMenu());
    }

    public void a(int i2, int i3) {
        s();
        this.u.a(i2, i3);
    }

    public void a(Context context, int i2) {
        this.n = i2;
        if (this.f != null) {
            this.f.setTextAppearance(context, i2);
        }
    }

    public void a(h hVar, d dVar) {
        if (hVar != null || this.e != null) {
            o();
            h d2 = this.e.d();
            if (d2 != hVar) {
                if (d2 != null) {
                    d2.b((o) this.J);
                    d2.b((o) this.K);
                }
                if (this.K == null) {
                    this.K = new a();
                }
                dVar.d(true);
                if (hVar != null) {
                    hVar.a((o) dVar, this.l);
                    hVar.a((o) this.K, this.l);
                } else {
                    dVar.a(this.l, (h) null);
                    this.K.a(this.l, (h) null);
                    dVar.a(true);
                    this.K.a(true);
                }
                this.e.setPopupTheme(this.m);
                this.e.setPresenter(dVar);
                this.J = dVar;
            }
        }
    }

    public void a(android.support.v7.view.menu.o.a aVar, android.support.v7.view.menu.h.a aVar2) {
        this.L = aVar;
        this.M = aVar2;
        if (this.e != null) {
            this.e.a(aVar, aVar2);
        }
    }

    public boolean a() {
        return getVisibility() == 0 && this.e != null && this.e.a();
    }

    public void b(Context context, int i2) {
        this.o = i2;
        if (this.g != null) {
            this.g.setTextAppearance(context, i2);
        }
    }

    public boolean b() {
        return this.e != null && this.e.g();
    }

    public boolean c() {
        return this.e != null && this.e.h();
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(LayoutParams layoutParams) {
        return super.checkLayoutParams(layoutParams) && (layoutParams instanceof b);
    }

    public boolean d() {
        return this.e != null && this.e.e();
    }

    public boolean e() {
        return this.e != null && this.e.f();
    }

    public void f() {
        if (this.e != null) {
            this.e.i();
        }
    }

    public boolean g() {
        return (this.K == null || this.K.f1135b == null) ? false : true;
    }

    public int getContentInsetEnd() {
        if (this.u != null) {
            return this.u.d();
        }
        return 0;
    }

    public int getContentInsetEndWithActions() {
        return this.w != Integer.MIN_VALUE ? this.w : getContentInsetEnd();
    }

    public int getContentInsetLeft() {
        if (this.u != null) {
            return this.u.a();
        }
        return 0;
    }

    public int getContentInsetRight() {
        if (this.u != null) {
            return this.u.b();
        }
        return 0;
    }

    public int getContentInsetStart() {
        if (this.u != null) {
            return this.u.c();
        }
        return 0;
    }

    public int getContentInsetStartWithNavigation() {
        return this.v != Integer.MIN_VALUE ? this.v : getContentInsetStart();
    }

    public int getCurrentContentInsetEnd() {
        boolean z2;
        if (this.e != null) {
            h d2 = this.e.d();
            z2 = d2 != null && d2.hasVisibleItems();
        } else {
            z2 = false;
        }
        return z2 ? Math.max(getContentInsetEnd(), Math.max(this.w, 0)) : getContentInsetEnd();
    }

    public int getCurrentContentInsetLeft() {
        return t.e(this) == 1 ? getCurrentContentInsetEnd() : getCurrentContentInsetStart();
    }

    public int getCurrentContentInsetRight() {
        return t.e(this) == 1 ? getCurrentContentInsetStart() : getCurrentContentInsetEnd();
    }

    public int getCurrentContentInsetStart() {
        return getNavigationIcon() != null ? Math.max(getContentInsetStart(), Math.max(this.v, 0)) : getContentInsetStart();
    }

    public Drawable getLogo() {
        if (this.i != null) {
            return this.i.getDrawable();
        }
        return null;
    }

    public CharSequence getLogoDescription() {
        if (this.i != null) {
            return this.i.getContentDescription();
        }
        return null;
    }

    public Menu getMenu() {
        n();
        return this.e.getMenu();
    }

    public CharSequence getNavigationContentDescription() {
        if (this.h != null) {
            return this.h.getContentDescription();
        }
        return null;
    }

    public Drawable getNavigationIcon() {
        if (this.h != null) {
            return this.h.getDrawable();
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    public d getOuterActionMenuPresenter() {
        return this.J;
    }

    public Drawable getOverflowIcon() {
        n();
        return this.e.getOverflowIcon();
    }

    /* access modifiers changed from: 0000 */
    public Context getPopupContext() {
        return this.l;
    }

    public int getPopupTheme() {
        return this.m;
    }

    public CharSequence getSubtitle() {
        return this.z;
    }

    public CharSequence getTitle() {
        return this.y;
    }

    public int getTitleMarginBottom() {
        return this.t;
    }

    public int getTitleMarginEnd() {
        return this.r;
    }

    public int getTitleMarginStart() {
        return this.q;
    }

    public int getTitleMarginTop() {
        return this.s;
    }

    public ak getWrapper() {
        if (this.I == null) {
            this.I = new bp(this, true);
        }
        return this.I;
    }

    public void h() {
        j jVar = this.K == null ? null : this.K.f1135b;
        if (jVar != null) {
            jVar.collapseActionView();
        }
    }

    /* access modifiers changed from: 0000 */
    public void i() {
        if (this.f1129a == null) {
            this.f1129a = new o(getContext(), null, C0026a.toolbarNavigationButtonStyle);
            this.f1129a.setImageDrawable(this.j);
            this.f1129a.setContentDescription(this.k);
            b j2 = generateDefaultLayoutParams();
            j2.f889a = 8388611 | (this.c & 112);
            j2.f1136b = 2;
            this.f1129a.setLayoutParams(j2);
            this.f1129a.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    Toolbar.this.h();
                }
            });
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: j */
    public b generateDefaultLayoutParams() {
        return new b(-2, -2);
    }

    /* access modifiers changed from: 0000 */
    public void k() {
        for (int childCount = getChildCount() - 1; childCount >= 0; childCount--) {
            View childAt = getChildAt(childCount);
            if (!(((b) childAt.getLayoutParams()).f1136b == 2 || childAt == this.e)) {
                removeViewAt(childCount);
                this.F.add(childAt);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void l() {
        for (int size = this.F.size() - 1; size >= 0; size--) {
            addView((View) this.F.get(size));
        }
        this.F.clear();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        removeCallbacks(this.O);
    }

    public boolean onHoverEvent(MotionEvent motionEvent) {
        int actionMasked = motionEvent.getActionMasked();
        if (actionMasked == 9) {
            this.D = false;
        }
        if (!this.D) {
            boolean onHoverEvent = super.onHoverEvent(motionEvent);
            if (actionMasked == 9 && !onHoverEvent) {
                this.D = true;
            }
        }
        if (actionMasked == 10 || actionMasked == 3) {
            this.D = false;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z2, int i2, int i3, int i4, int i5) {
        int i6;
        int i7;
        int i8;
        int i9;
        int i10;
        int i11;
        int i12;
        int i13;
        int i14;
        int i15;
        int i16;
        int i17;
        boolean z3 = t.e(this) == 1;
        int width = getWidth();
        int height = getHeight();
        int paddingLeft = getPaddingLeft();
        int paddingRight = getPaddingRight();
        int paddingTop = getPaddingTop();
        int paddingBottom = getPaddingBottom();
        int i18 = width - paddingRight;
        int[] iArr = this.G;
        iArr[1] = 0;
        iArr[0] = 0;
        int i19 = t.i(this);
        int i20 = i19 >= 0 ? Math.min(i19, i5 - i3) : 0;
        if (!a((View) this.h)) {
            i6 = i18;
            i7 = paddingLeft;
        } else if (z3) {
            i6 = b(this.h, i18, iArr, i20);
            i7 = paddingLeft;
        } else {
            int i21 = i18;
            i7 = a(this.h, paddingLeft, iArr, i20);
            i6 = i21;
        }
        if (a((View) this.f1129a)) {
            if (z3) {
                i6 = b(this.f1129a, i6, iArr, i20);
            } else {
                i7 = a(this.f1129a, i7, iArr, i20);
            }
        }
        if (a((View) this.e)) {
            if (z3) {
                i7 = a(this.e, i7, iArr, i20);
            } else {
                i6 = b(this.e, i6, iArr, i20);
            }
        }
        int currentContentInsetLeft = getCurrentContentInsetLeft();
        int currentContentInsetRight = getCurrentContentInsetRight();
        iArr[0] = Math.max(0, currentContentInsetLeft - i7);
        iArr[1] = Math.max(0, currentContentInsetRight - ((width - paddingRight) - i6));
        int max = Math.max(i7, currentContentInsetLeft);
        int min = Math.min(i6, (width - paddingRight) - currentContentInsetRight);
        if (a(this.f1130b)) {
            if (z3) {
                min = b(this.f1130b, min, iArr, i20);
            } else {
                max = a(this.f1130b, max, iArr, i20);
            }
        }
        if (!a((View) this.i)) {
            i8 = min;
            i9 = max;
        } else if (z3) {
            i8 = b(this.i, min, iArr, i20);
            i9 = max;
        } else {
            i8 = min;
            i9 = a(this.i, max, iArr, i20);
        }
        boolean a2 = a((View) this.f);
        boolean a3 = a((View) this.g);
        int i22 = 0;
        if (a2) {
            b bVar = (b) this.f.getLayoutParams();
            i22 = 0 + bVar.bottomMargin + bVar.topMargin + this.f.getMeasuredHeight();
        }
        if (a3) {
            b bVar2 = (b) this.g.getLayoutParams();
            i10 = bVar2.bottomMargin + bVar2.topMargin + this.g.getMeasuredHeight() + i22;
        } else {
            i10 = i22;
        }
        if (a2 || a3) {
            b bVar3 = (b) (a2 ? this.f : this.g).getLayoutParams();
            b bVar4 = (b) (a3 ? this.g : this.f).getLayoutParams();
            boolean z4 = (a2 && this.f.getMeasuredWidth() > 0) || (a3 && this.g.getMeasuredWidth() > 0);
            switch (this.x & 112) {
                case 48:
                    i11 = bVar3.topMargin + getPaddingTop() + this.s;
                    break;
                case 80:
                    i11 = (((height - paddingBottom) - bVar4.bottomMargin) - this.t) - i10;
                    break;
                default:
                    int i23 = (((height - paddingTop) - paddingBottom) - i10) / 2;
                    if (i23 < bVar3.topMargin + this.s) {
                        i17 = bVar3.topMargin + this.s;
                    } else {
                        int i24 = (((height - paddingBottom) - i10) - i23) - paddingTop;
                        i17 = i24 < bVar3.bottomMargin + this.t ? Math.max(0, i23 - ((bVar4.bottomMargin + this.t) - i24)) : i23;
                    }
                    i11 = paddingTop + i17;
                    break;
            }
            if (z3) {
                int i25 = (z4 ? this.q : 0) - iArr[1];
                int max2 = i8 - Math.max(0, i25);
                iArr[1] = Math.max(0, -i25);
                if (a2) {
                    b bVar5 = (b) this.f.getLayoutParams();
                    int measuredWidth = max2 - this.f.getMeasuredWidth();
                    int measuredHeight = this.f.getMeasuredHeight() + i11;
                    this.f.layout(measuredWidth, i11, max2, measuredHeight);
                    i11 = measuredHeight + bVar5.bottomMargin;
                    i15 = measuredWidth - this.r;
                } else {
                    i15 = max2;
                }
                if (a3) {
                    b bVar6 = (b) this.g.getLayoutParams();
                    int i26 = bVar6.topMargin + i11;
                    int measuredHeight2 = this.g.getMeasuredHeight() + i26;
                    this.g.layout(max2 - this.g.getMeasuredWidth(), i26, max2, measuredHeight2);
                    int i27 = bVar6.bottomMargin + measuredHeight2;
                    i16 = max2 - this.r;
                } else {
                    i16 = max2;
                }
                i8 = z4 ? Math.min(i15, i16) : max2;
            } else {
                int i28 = (z4 ? this.q : 0) - iArr[0];
                i9 += Math.max(0, i28);
                iArr[0] = Math.max(0, -i28);
                if (a2) {
                    b bVar7 = (b) this.f.getLayoutParams();
                    int measuredWidth2 = this.f.getMeasuredWidth() + i9;
                    int measuredHeight3 = this.f.getMeasuredHeight() + i11;
                    this.f.layout(i9, i11, measuredWidth2, measuredHeight3);
                    int i29 = bVar7.bottomMargin + measuredHeight3;
                    i12 = measuredWidth2 + this.r;
                    i13 = i29;
                } else {
                    i12 = i9;
                    i13 = i11;
                }
                if (a3) {
                    b bVar8 = (b) this.g.getLayoutParams();
                    int i30 = i13 + bVar8.topMargin;
                    int measuredWidth3 = this.g.getMeasuredWidth() + i9;
                    int measuredHeight4 = this.g.getMeasuredHeight() + i30;
                    this.g.layout(i9, i30, measuredWidth3, measuredHeight4);
                    int i31 = bVar8.bottomMargin + measuredHeight4;
                    i14 = this.r + measuredWidth3;
                } else {
                    i14 = i9;
                }
                if (z4) {
                    i9 = Math.max(i12, i14);
                }
            }
        }
        a((List<View>) this.E, 3);
        int size = this.E.size();
        int i32 = i9;
        for (int i33 = 0; i33 < size; i33++) {
            i32 = a((View) this.E.get(i33), i32, iArr, i20);
        }
        a((List<View>) this.E, 5);
        int size2 = this.E.size();
        for (int i34 = 0; i34 < size2; i34++) {
            i8 = b((View) this.E.get(i34), i8, iArr, i20);
        }
        a((List<View>) this.E, 1);
        int a4 = a((List<View>) this.E, iArr);
        int i35 = ((((width - paddingLeft) - paddingRight) / 2) + paddingLeft) - (a4 / 2);
        int i36 = a4 + i35;
        if (i35 < i32) {
            i35 = i32;
        } else if (i36 > i8) {
            i35 -= i36 - i8;
        }
        int size3 = this.E.size();
        int i37 = i35;
        for (int i38 = 0; i38 < size3; i38++) {
            i37 = a((View) this.E.get(i38), i37, iArr, i20);
        }
        this.E.clear();
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        char c2;
        char c3;
        int i4;
        int i5;
        int i6 = 0;
        int i7 = 0;
        int[] iArr = this.G;
        if (bw.a(this)) {
            c2 = 0;
            c3 = 1;
        } else {
            c2 = 1;
            c3 = 0;
        }
        int i8 = 0;
        if (a((View) this.h)) {
            a((View) this.h, i2, 0, i3, 0, this.p);
            i8 = this.h.getMeasuredWidth() + b((View) this.h);
            int max = Math.max(0, this.h.getMeasuredHeight() + c((View) this.h));
            i7 = View.combineMeasuredStates(0, this.h.getMeasuredState());
            i6 = max;
        }
        if (a((View) this.f1129a)) {
            a((View) this.f1129a, i2, 0, i3, 0, this.p);
            i8 = this.f1129a.getMeasuredWidth() + b((View) this.f1129a);
            i6 = Math.max(i6, this.f1129a.getMeasuredHeight() + c((View) this.f1129a));
            i7 = View.combineMeasuredStates(i7, this.f1129a.getMeasuredState());
        }
        int currentContentInsetStart = getCurrentContentInsetStart();
        int max2 = 0 + Math.max(currentContentInsetStart, i8);
        iArr[c3] = Math.max(0, currentContentInsetStart - i8);
        int i9 = 0;
        if (a((View) this.e)) {
            a((View) this.e, i2, max2, i3, 0, this.p);
            i9 = this.e.getMeasuredWidth() + b((View) this.e);
            i6 = Math.max(i6, this.e.getMeasuredHeight() + c((View) this.e));
            i7 = View.combineMeasuredStates(i7, this.e.getMeasuredState());
        }
        int currentContentInsetEnd = getCurrentContentInsetEnd();
        int max3 = max2 + Math.max(currentContentInsetEnd, i9);
        iArr[c2] = Math.max(0, currentContentInsetEnd - i9);
        if (a(this.f1130b)) {
            max3 += a(this.f1130b, i2, max3, i3, 0, iArr);
            i6 = Math.max(i6, this.f1130b.getMeasuredHeight() + c(this.f1130b));
            i7 = View.combineMeasuredStates(i7, this.f1130b.getMeasuredState());
        }
        if (a((View) this.i)) {
            max3 += a((View) this.i, i2, max3, i3, 0, iArr);
            i6 = Math.max(i6, this.i.getMeasuredHeight() + c((View) this.i));
            i7 = View.combineMeasuredStates(i7, this.i.getMeasuredState());
        }
        int childCount = getChildCount();
        int i10 = 0;
        int i11 = i6;
        int i12 = i7;
        while (i10 < childCount) {
            View childAt = getChildAt(i10);
            if (((b) childAt.getLayoutParams()).f1136b != 0) {
                i4 = i12;
                i5 = i11;
            } else if (!a(childAt)) {
                i4 = i12;
                i5 = i11;
            } else {
                max3 += a(childAt, i2, max3, i3, 0, iArr);
                int max4 = Math.max(i11, childAt.getMeasuredHeight() + c(childAt));
                i4 = View.combineMeasuredStates(i12, childAt.getMeasuredState());
                i5 = max4;
            }
            i10++;
            i12 = i4;
            i11 = i5;
        }
        int i13 = 0;
        int i14 = 0;
        int i15 = this.s + this.t;
        int i16 = this.q + this.r;
        if (a((View) this.f)) {
            a((View) this.f, i2, max3 + i16, i3, i15, iArr);
            i13 = b((View) this.f) + this.f.getMeasuredWidth();
            i14 = this.f.getMeasuredHeight() + c((View) this.f);
            i12 = View.combineMeasuredStates(i12, this.f.getMeasuredState());
        }
        if (a((View) this.g)) {
            i13 = Math.max(i13, a((View) this.g, i2, max3 + i16, i3, i15 + i14, iArr));
            i14 += this.g.getMeasuredHeight() + c((View) this.g);
            i12 = View.combineMeasuredStates(i12, this.g.getMeasuredState());
        }
        int i17 = i13 + max3;
        int max5 = Math.max(i11, i14) + getPaddingTop() + getPaddingBottom();
        int resolveSizeAndState = View.resolveSizeAndState(Math.max(i17 + getPaddingLeft() + getPaddingRight(), getSuggestedMinimumWidth()), i2, -16777216 & i12);
        int resolveSizeAndState2 = View.resolveSizeAndState(Math.max(max5, getSuggestedMinimumHeight()), i3, i12 << 16);
        if (r()) {
            resolveSizeAndState2 = 0;
        }
        setMeasuredDimension(resolveSizeAndState, resolveSizeAndState2);
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (!(parcelable instanceof d)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        d dVar = (d) parcelable;
        super.onRestoreInstanceState(dVar.a());
        h hVar = this.e != null ? this.e.d() : null;
        if (!(dVar.f1137a == 0 || this.K == null || hVar == null)) {
            MenuItem findItem = hVar.findItem(dVar.f1137a);
            if (findItem != null) {
                findItem.expandActionView();
            }
        }
        if (dVar.f1138b) {
            q();
        }
    }

    public void onRtlPropertiesChanged(int i2) {
        boolean z2 = true;
        if (VERSION.SDK_INT >= 17) {
            super.onRtlPropertiesChanged(i2);
        }
        s();
        be beVar = this.u;
        if (i2 != 1) {
            z2 = false;
        }
        beVar.a(z2);
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        d dVar = new d(super.onSaveInstanceState());
        if (!(this.K == null || this.K.f1135b == null)) {
            dVar.f1137a = this.K.f1135b.getItemId();
        }
        dVar.f1138b = b();
        return dVar;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        int actionMasked = motionEvent.getActionMasked();
        if (actionMasked == 0) {
            this.C = false;
        }
        if (!this.C) {
            boolean onTouchEvent = super.onTouchEvent(motionEvent);
            if (actionMasked == 0 && !onTouchEvent) {
                this.C = true;
            }
        }
        if (actionMasked == 1 || actionMasked == 3) {
            this.C = false;
        }
        return true;
    }

    public void setCollapsible(boolean z2) {
        this.N = z2;
        requestLayout();
    }

    public void setContentInsetEndWithActions(int i2) {
        if (i2 < 0) {
            i2 = Integer.MIN_VALUE;
        }
        if (i2 != this.w) {
            this.w = i2;
            if (getNavigationIcon() != null) {
                requestLayout();
            }
        }
    }

    public void setContentInsetStartWithNavigation(int i2) {
        if (i2 < 0) {
            i2 = Integer.MIN_VALUE;
        }
        if (i2 != this.v) {
            this.v = i2;
            if (getNavigationIcon() != null) {
                requestLayout();
            }
        }
    }

    public void setLogo(int i2) {
        setLogo(android.support.v7.c.a.b.b(getContext(), i2));
    }

    public void setLogo(Drawable drawable) {
        if (drawable != null) {
            m();
            if (!d(this.i)) {
                a((View) this.i, true);
            }
        } else if (this.i != null && d(this.i)) {
            removeView(this.i);
            this.F.remove(this.i);
        }
        if (this.i != null) {
            this.i.setImageDrawable(drawable);
        }
    }

    public void setLogoDescription(int i2) {
        setLogoDescription(getContext().getText(i2));
    }

    public void setLogoDescription(CharSequence charSequence) {
        if (!TextUtils.isEmpty(charSequence)) {
            m();
        }
        if (this.i != null) {
            this.i.setContentDescription(charSequence);
        }
    }

    public void setNavigationContentDescription(int i2) {
        setNavigationContentDescription(i2 != 0 ? getContext().getText(i2) : null);
    }

    public void setNavigationContentDescription(CharSequence charSequence) {
        if (!TextUtils.isEmpty(charSequence)) {
            p();
        }
        if (this.h != null) {
            this.h.setContentDescription(charSequence);
        }
    }

    public void setNavigationIcon(int i2) {
        setNavigationIcon(android.support.v7.c.a.b.b(getContext(), i2));
    }

    public void setNavigationIcon(Drawable drawable) {
        if (drawable != null) {
            p();
            if (!d(this.h)) {
                a((View) this.h, true);
            }
        } else if (this.h != null && d(this.h)) {
            removeView(this.h);
            this.F.remove(this.h);
        }
        if (this.h != null) {
            this.h.setImageDrawable(drawable);
        }
    }

    public void setNavigationOnClickListener(OnClickListener onClickListener) {
        p();
        this.h.setOnClickListener(onClickListener);
    }

    public void setOnMenuItemClickListener(c cVar) {
        this.d = cVar;
    }

    public void setOverflowIcon(Drawable drawable) {
        n();
        this.e.setOverflowIcon(drawable);
    }

    public void setPopupTheme(int i2) {
        if (this.m != i2) {
            this.m = i2;
            if (i2 == 0) {
                this.l = getContext();
            } else {
                this.l = new ContextThemeWrapper(getContext(), i2);
            }
        }
    }

    public void setSubtitle(int i2) {
        setSubtitle(getContext().getText(i2));
    }

    public void setSubtitle(CharSequence charSequence) {
        if (!TextUtils.isEmpty(charSequence)) {
            if (this.g == null) {
                Context context = getContext();
                this.g = new ab(context);
                this.g.setSingleLine();
                this.g.setEllipsize(TruncateAt.END);
                if (this.o != 0) {
                    this.g.setTextAppearance(context, this.o);
                }
                if (this.B != 0) {
                    this.g.setTextColor(this.B);
                }
            }
            if (!d(this.g)) {
                a((View) this.g, true);
            }
        } else if (this.g != null && d(this.g)) {
            removeView(this.g);
            this.F.remove(this.g);
        }
        if (this.g != null) {
            this.g.setText(charSequence);
        }
        this.z = charSequence;
    }

    public void setSubtitleTextColor(int i2) {
        this.B = i2;
        if (this.g != null) {
            this.g.setTextColor(i2);
        }
    }

    public void setTitle(int i2) {
        setTitle(getContext().getText(i2));
    }

    public void setTitle(CharSequence charSequence) {
        if (!TextUtils.isEmpty(charSequence)) {
            if (this.f == null) {
                Context context = getContext();
                this.f = new ab(context);
                this.f.setSingleLine();
                this.f.setEllipsize(TruncateAt.END);
                if (this.n != 0) {
                    this.f.setTextAppearance(context, this.n);
                }
                if (this.A != 0) {
                    this.f.setTextColor(this.A);
                }
            }
            if (!d(this.f)) {
                a((View) this.f, true);
            }
        } else if (this.f != null && d(this.f)) {
            removeView(this.f);
            this.F.remove(this.f);
        }
        if (this.f != null) {
            this.f.setText(charSequence);
        }
        this.y = charSequence;
    }

    public void setTitleMarginBottom(int i2) {
        this.t = i2;
        requestLayout();
    }

    public void setTitleMarginEnd(int i2) {
        this.r = i2;
        requestLayout();
    }

    public void setTitleMarginStart(int i2) {
        this.q = i2;
        requestLayout();
    }

    public void setTitleMarginTop(int i2) {
        this.s = i2;
        requestLayout();
    }

    public void setTitleTextColor(int i2) {
        this.A = i2;
        if (this.f != null) {
            this.f.setTextColor(i2);
        }
    }
}
