package android.support.v7.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.support.v4.i.e;
import android.support.v4.i.t;
import android.support.v7.a.a.j;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;

public class at extends ViewGroup {

    /* renamed from: a reason: collision with root package name */
    private boolean f1217a;

    /* renamed from: b reason: collision with root package name */
    private int f1218b;
    private int c;
    private int d;
    private int e;
    private int f;
    private float g;
    private boolean h;
    private int[] i;
    private int[] j;
    private Drawable k;
    private int l;
    private int m;
    private int n;
    private int o;

    public static class a extends MarginLayoutParams {
        public float g;
        public int h;

        public a(int i, int i2) {
            super(i, i2);
            this.h = -1;
            this.g = 0.0f;
        }

        public a(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            this.h = -1;
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, j.LinearLayoutCompat_Layout);
            this.g = obtainStyledAttributes.getFloat(j.LinearLayoutCompat_Layout_android_layout_weight, 0.0f);
            this.h = obtainStyledAttributes.getInt(j.LinearLayoutCompat_Layout_android_layout_gravity, -1);
            obtainStyledAttributes.recycle();
        }

        public a(LayoutParams layoutParams) {
            super(layoutParams);
            this.h = -1;
        }
    }

    public at(Context context) {
        this(context, null);
    }

    public at(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public at(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.f1217a = true;
        this.f1218b = -1;
        this.c = 0;
        this.e = 8388659;
        bo a2 = bo.a(context, attributeSet, j.LinearLayoutCompat, i2, 0);
        int a3 = a2.a(j.LinearLayoutCompat_android_orientation, -1);
        if (a3 >= 0) {
            setOrientation(a3);
        }
        int a4 = a2.a(j.LinearLayoutCompat_android_gravity, -1);
        if (a4 >= 0) {
            setGravity(a4);
        }
        boolean a5 = a2.a(j.LinearLayoutCompat_android_baselineAligned, true);
        if (!a5) {
            setBaselineAligned(a5);
        }
        this.g = a2.a(j.LinearLayoutCompat_android_weightSum, -1.0f);
        this.f1218b = a2.a(j.LinearLayoutCompat_android_baselineAlignedChildIndex, -1);
        this.h = a2.a(j.LinearLayoutCompat_measureWithLargestChild, false);
        setDividerDrawable(a2.a(j.LinearLayoutCompat_divider));
        this.n = a2.a(j.LinearLayoutCompat_showDividers, 0);
        this.o = a2.e(j.LinearLayoutCompat_dividerPadding, 0);
        a2.a();
    }

    private void a(View view, int i2, int i3, int i4, int i5) {
        view.layout(i2, i3, i2 + i4, i3 + i5);
    }

    private void c(int i2, int i3) {
        int makeMeasureSpec = MeasureSpec.makeMeasureSpec(getMeasuredWidth(), 1073741824);
        for (int i4 = 0; i4 < i2; i4++) {
            View b2 = b(i4);
            if (b2.getVisibility() != 8) {
                a aVar = (a) b2.getLayoutParams();
                if (aVar.width == -1) {
                    int i5 = aVar.height;
                    aVar.height = b2.getMeasuredHeight();
                    measureChildWithMargins(b2, makeMeasureSpec, 0, i3, 0);
                    aVar.height = i5;
                }
            }
        }
    }

    private void d(int i2, int i3) {
        int makeMeasureSpec = MeasureSpec.makeMeasureSpec(getMeasuredHeight(), 1073741824);
        for (int i4 = 0; i4 < i2; i4++) {
            View b2 = b(i4);
            if (b2.getVisibility() != 8) {
                a aVar = (a) b2.getLayoutParams();
                if (aVar.height == -1) {
                    int i5 = aVar.width;
                    aVar.width = b2.getMeasuredWidth();
                    measureChildWithMargins(b2, i3, 0, makeMeasureSpec, 0);
                    aVar.width = i5;
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public int a(View view) {
        return 0;
    }

    /* access modifiers changed from: 0000 */
    public int a(View view, int i2) {
        return 0;
    }

    /* access modifiers changed from: 0000 */
    public void a(int i2, int i3) {
        int i4;
        int i5;
        float f2;
        int i6;
        int i7;
        boolean z;
        int i8;
        int i9;
        int i10;
        int i11;
        boolean z2;
        boolean z3;
        int max;
        int i12;
        boolean z4;
        int i13;
        int i14;
        int i15;
        this.f = 0;
        int i16 = 0;
        int i17 = 0;
        int i18 = 0;
        int i19 = 0;
        boolean z5 = true;
        float f3 = 0.0f;
        int virtualChildCount = getVirtualChildCount();
        int mode = MeasureSpec.getMode(i2);
        int mode2 = MeasureSpec.getMode(i3);
        boolean z6 = false;
        boolean z7 = false;
        int i20 = this.f1218b;
        boolean z8 = this.h;
        int i21 = 0;
        int i22 = 0;
        while (i22 < virtualChildCount) {
            View b2 = b(i22);
            if (b2 == null) {
                this.f += d(i22);
                i14 = i21;
                z4 = z7;
                z3 = z5;
                i15 = i17;
                i13 = i16;
            } else if (b2.getVisibility() == 8) {
                i22 += a(b2, i22);
                i14 = i21;
                z4 = z7;
                z3 = z5;
                i15 = i17;
                i13 = i16;
            } else {
                if (c(i22)) {
                    this.f += this.m;
                }
                a aVar = (a) b2.getLayoutParams();
                float f4 = f3 + aVar.g;
                if (mode2 == 1073741824 && aVar.height == 0 && aVar.g > 0.0f) {
                    int i23 = this.f;
                    this.f = Math.max(i23, aVar.topMargin + i23 + aVar.bottomMargin);
                    z7 = true;
                } else {
                    int i24 = Integer.MIN_VALUE;
                    if (aVar.height == 0 && aVar.g > 0.0f) {
                        i24 = 0;
                        aVar.height = -2;
                    }
                    int i25 = i24;
                    a(b2, i22, i2, 0, i3, f4 == 0.0f ? this.f : 0);
                    if (i25 != Integer.MIN_VALUE) {
                        aVar.height = i25;
                    }
                    int measuredHeight = b2.getMeasuredHeight();
                    int i26 = this.f;
                    this.f = Math.max(i26, i26 + measuredHeight + aVar.topMargin + aVar.bottomMargin + b(b2));
                    if (z8) {
                        i21 = Math.max(measuredHeight, i21);
                    }
                }
                if (i20 >= 0 && i20 == i22 + 1) {
                    this.c = this.f;
                }
                if (i22 >= i20 || aVar.g <= 0.0f) {
                    boolean z9 = false;
                    if (mode == 1073741824 || aVar.width != -1) {
                        z2 = z6;
                    } else {
                        z2 = true;
                        z9 = true;
                    }
                    int i27 = aVar.rightMargin + aVar.leftMargin;
                    int measuredWidth = b2.getMeasuredWidth() + i27;
                    int max2 = Math.max(i16, measuredWidth);
                    int combineMeasuredStates = View.combineMeasuredStates(i17, b2.getMeasuredState());
                    z3 = z5 && aVar.width == -1;
                    if (aVar.g > 0.0f) {
                        i12 = Math.max(i19, z9 ? i27 : measuredWidth);
                        max = i18;
                    } else {
                        if (!z9) {
                            i27 = measuredWidth;
                        }
                        max = Math.max(i18, i27);
                        i12 = i19;
                    }
                    i22 += a(b2, i22);
                    z4 = z7;
                    i19 = i12;
                    i18 = max;
                    i13 = max2;
                    i14 = i21;
                    i15 = combineMeasuredStates;
                    z6 = z2;
                    f3 = f4;
                } else {
                    throw new RuntimeException("A child of LinearLayout with index less than mBaselineAlignedChildIndex has weight > 0, which won't work.  Either remove the weight, or don't set mBaselineAlignedChildIndex.");
                }
            }
            i22++;
            i21 = i14;
            z7 = z4;
            z5 = z3;
            i17 = i15;
            i16 = i13;
        }
        if (this.f > 0 && c(virtualChildCount)) {
            this.f += this.m;
        }
        if (z8 && (mode2 == Integer.MIN_VALUE || mode2 == 0)) {
            this.f = 0;
            int i28 = 0;
            while (i28 < virtualChildCount) {
                View b3 = b(i28);
                if (b3 == null) {
                    this.f += d(i28);
                    i11 = i28;
                } else if (b3.getVisibility() == 8) {
                    i11 = a(b3, i28) + i28;
                } else {
                    a aVar2 = (a) b3.getLayoutParams();
                    int i29 = this.f;
                    this.f = Math.max(i29, aVar2.bottomMargin + i29 + i21 + aVar2.topMargin + b(b3));
                    i11 = i28;
                }
                i28 = i11 + 1;
            }
        }
        this.f += getPaddingTop() + getPaddingBottom();
        int resolveSizeAndState = View.resolveSizeAndState(Math.max(this.f, getSuggestedMinimumHeight()), i3, 0);
        int i30 = (16777215 & resolveSizeAndState) - this.f;
        if (z7 || (i30 != 0 && f3 > 0.0f)) {
            if (this.g > 0.0f) {
                f3 = this.g;
            }
            this.f = 0;
            int i31 = 0;
            float f5 = f3;
            boolean z10 = z5;
            int i32 = i18;
            int i33 = i17;
            int i34 = i16;
            int i35 = i30;
            while (i31 < virtualChildCount) {
                View b4 = b(i31);
                if (b4.getVisibility() == 8) {
                    i8 = i32;
                    i10 = i33;
                    i9 = i34;
                    z = z10;
                } else {
                    a aVar3 = (a) b4.getLayoutParams();
                    float f6 = aVar3.g;
                    if (f6 > 0.0f) {
                        int i36 = (int) ((((float) i35) * f6) / f5);
                        float f7 = f5 - f6;
                        int i37 = i35 - i36;
                        int childMeasureSpec = getChildMeasureSpec(i2, getPaddingLeft() + getPaddingRight() + aVar3.leftMargin + aVar3.rightMargin, aVar3.width);
                        if (aVar3.height == 0 && mode2 == 1073741824) {
                            if (i36 <= 0) {
                                i36 = 0;
                            }
                            b4.measure(childMeasureSpec, MeasureSpec.makeMeasureSpec(i36, 1073741824));
                        } else {
                            int measuredHeight2 = i36 + b4.getMeasuredHeight();
                            if (measuredHeight2 < 0) {
                                measuredHeight2 = 0;
                            }
                            b4.measure(childMeasureSpec, MeasureSpec.makeMeasureSpec(measuredHeight2, 1073741824));
                        }
                        float f8 = f7;
                        i6 = i37;
                        i7 = View.combineMeasuredStates(i33, b4.getMeasuredState() & -256);
                        f2 = f8;
                    } else {
                        f2 = f5;
                        i6 = i35;
                        i7 = i33;
                    }
                    int i38 = aVar3.leftMargin + aVar3.rightMargin;
                    int measuredWidth2 = b4.getMeasuredWidth() + i38;
                    int max3 = Math.max(i34, measuredWidth2);
                    if (!(mode != 1073741824 && aVar3.width == -1)) {
                        i38 = measuredWidth2;
                    }
                    int max4 = Math.max(i32, i38);
                    z = z10 && aVar3.width == -1;
                    int i39 = this.f;
                    this.f = Math.max(i39, aVar3.bottomMargin + b4.getMeasuredHeight() + i39 + aVar3.topMargin + b(b4));
                    i8 = max4;
                    i9 = max3;
                    float f9 = f2;
                    i10 = i7;
                    i35 = i6;
                    f5 = f9;
                }
                i31++;
                i32 = i8;
                i34 = i9;
                z10 = z;
                i33 = i10;
            }
            this.f += getPaddingTop() + getPaddingBottom();
            z5 = z10;
            i5 = i32;
            i17 = i33;
            i4 = i34;
        } else {
            int max5 = Math.max(i18, i19);
            if (z8 && mode2 != 1073741824) {
                int i40 = 0;
                while (true) {
                    int i41 = i40;
                    if (i41 >= virtualChildCount) {
                        break;
                    }
                    View b5 = b(i41);
                    if (!(b5 == null || b5.getVisibility() == 8 || ((a) b5.getLayoutParams()).g <= 0.0f)) {
                        b5.measure(MeasureSpec.makeMeasureSpec(b5.getMeasuredWidth(), 1073741824), MeasureSpec.makeMeasureSpec(i21, 1073741824));
                    }
                    i40 = i41 + 1;
                }
            }
            i5 = max5;
            i4 = i16;
        }
        if (z5 || mode == 1073741824) {
            i5 = i4;
        }
        setMeasuredDimension(View.resolveSizeAndState(Math.max(i5 + getPaddingLeft() + getPaddingRight(), getSuggestedMinimumWidth()), i2, i17), resolveSizeAndState);
        if (z6) {
            c(virtualChildCount, i3);
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(int i2, int i3, int i4, int i5) {
        int paddingTop;
        int i6;
        int i7;
        int paddingLeft = getPaddingLeft();
        int i8 = i4 - i2;
        int paddingRight = i8 - getPaddingRight();
        int paddingRight2 = (i8 - paddingLeft) - getPaddingRight();
        int virtualChildCount = getVirtualChildCount();
        int i9 = this.e & 8388615;
        switch (this.e & 112) {
            case 16:
                paddingTop = getPaddingTop() + (((i5 - i3) - this.f) / 2);
                break;
            case 80:
                paddingTop = ((getPaddingTop() + i5) - i3) - this.f;
                break;
            default:
                paddingTop = getPaddingTop();
                break;
        }
        int i10 = 0;
        int i11 = paddingTop;
        while (i10 < virtualChildCount) {
            View b2 = b(i10);
            if (b2 == null) {
                i11 += d(i10);
                i6 = i10;
            } else if (b2.getVisibility() != 8) {
                int measuredWidth = b2.getMeasuredWidth();
                int measuredHeight = b2.getMeasuredHeight();
                a aVar = (a) b2.getLayoutParams();
                int i12 = aVar.h;
                if (i12 < 0) {
                    i12 = i9;
                }
                switch (e.a(i12, t.e(this)) & 7) {
                    case 1:
                        i7 = ((((paddingRight2 - measuredWidth) / 2) + paddingLeft) + aVar.leftMargin) - aVar.rightMargin;
                        break;
                    case 5:
                        i7 = (paddingRight - measuredWidth) - aVar.rightMargin;
                        break;
                    default:
                        i7 = paddingLeft + aVar.leftMargin;
                        break;
                }
                int i13 = (c(i10) ? this.m + i11 : i11) + aVar.topMargin;
                a(b2, i7, i13 + a(b2), measuredWidth, measuredHeight);
                i11 = i13 + aVar.bottomMargin + measuredHeight + b(b2);
                i6 = a(b2, i10) + i10;
            } else {
                i6 = i10;
            }
            i10 = i6 + 1;
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(Canvas canvas) {
        int bottom;
        int virtualChildCount = getVirtualChildCount();
        for (int i2 = 0; i2 < virtualChildCount; i2++) {
            View b2 = b(i2);
            if (!(b2 == null || b2.getVisibility() == 8 || !c(i2))) {
                a(canvas, (b2.getTop() - ((a) b2.getLayoutParams()).topMargin) - this.m);
            }
        }
        if (c(virtualChildCount)) {
            View b3 = b(virtualChildCount - 1);
            if (b3 == null) {
                bottom = (getHeight() - getPaddingBottom()) - this.m;
            } else {
                a aVar = (a) b3.getLayoutParams();
                bottom = aVar.bottomMargin + b3.getBottom();
            }
            a(canvas, bottom);
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(Canvas canvas, int i2) {
        this.k.setBounds(getPaddingLeft() + this.o, i2, (getWidth() - getPaddingRight()) - this.o, this.m + i2);
        this.k.draw(canvas);
    }

    /* access modifiers changed from: 0000 */
    public void a(View view, int i2, int i3, int i4, int i5, int i6) {
        measureChildWithMargins(view, i3, i4, i5, i6);
    }

    /* access modifiers changed from: 0000 */
    public int b(View view) {
        return 0;
    }

    /* renamed from: b */
    public a generateLayoutParams(AttributeSet attributeSet) {
        return new a(getContext(), attributeSet);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public a generateLayoutParams(LayoutParams layoutParams) {
        return new a(layoutParams);
    }

    /* access modifiers changed from: 0000 */
    public View b(int i2) {
        return getChildAt(i2);
    }

    /* access modifiers changed from: 0000 */
    public void b(int i2, int i3) {
        int i4;
        int i5;
        float f2;
        int i6;
        int i7;
        int i8;
        boolean z;
        int i9;
        int i10;
        float f3;
        int i11;
        boolean z2;
        boolean z3;
        int max;
        int i12;
        boolean z4;
        int i13;
        int i14;
        int i15;
        this.f = 0;
        int i16 = 0;
        int i17 = 0;
        int i18 = 0;
        int i19 = 0;
        boolean z5 = true;
        float f4 = 0.0f;
        int virtualChildCount = getVirtualChildCount();
        int mode = MeasureSpec.getMode(i2);
        int mode2 = MeasureSpec.getMode(i3);
        boolean z6 = false;
        boolean z7 = false;
        if (this.i == null || this.j == null) {
            this.i = new int[4];
            this.j = new int[4];
        }
        int[] iArr = this.i;
        int[] iArr2 = this.j;
        iArr[3] = -1;
        iArr[2] = -1;
        iArr[1] = -1;
        iArr[0] = -1;
        iArr2[3] = -1;
        iArr2[2] = -1;
        iArr2[1] = -1;
        iArr2[0] = -1;
        boolean z8 = this.f1217a;
        boolean z9 = this.h;
        boolean z10 = mode == 1073741824;
        int i20 = 0;
        int i21 = 0;
        while (i21 < virtualChildCount) {
            View b2 = b(i21);
            if (b2 == null) {
                this.f += d(i21);
                i14 = i20;
                z4 = z7;
                z3 = z5;
                i15 = i17;
                i13 = i16;
            } else if (b2.getVisibility() == 8) {
                i21 += a(b2, i21);
                i14 = i20;
                z4 = z7;
                z3 = z5;
                i15 = i17;
                i13 = i16;
            } else {
                if (c(i21)) {
                    this.f += this.l;
                }
                a aVar = (a) b2.getLayoutParams();
                float f5 = f4 + aVar.g;
                if (mode == 1073741824 && aVar.width == 0 && aVar.g > 0.0f) {
                    if (z10) {
                        this.f += aVar.leftMargin + aVar.rightMargin;
                    } else {
                        int i22 = this.f;
                        this.f = Math.max(i22, aVar.leftMargin + i22 + aVar.rightMargin);
                    }
                    if (z8) {
                        int makeMeasureSpec = MeasureSpec.makeMeasureSpec(0, 0);
                        b2.measure(makeMeasureSpec, makeMeasureSpec);
                    } else {
                        z7 = true;
                    }
                } else {
                    int i23 = Integer.MIN_VALUE;
                    if (aVar.width == 0 && aVar.g > 0.0f) {
                        i23 = 0;
                        aVar.width = -2;
                    }
                    int i24 = i23;
                    a(b2, i21, i2, f5 == 0.0f ? this.f : 0, i3, 0);
                    if (i24 != Integer.MIN_VALUE) {
                        aVar.width = i24;
                    }
                    int measuredWidth = b2.getMeasuredWidth();
                    if (z10) {
                        this.f += aVar.leftMargin + measuredWidth + aVar.rightMargin + b(b2);
                    } else {
                        int i25 = this.f;
                        this.f = Math.max(i25, i25 + measuredWidth + aVar.leftMargin + aVar.rightMargin + b(b2));
                    }
                    if (z9) {
                        i20 = Math.max(measuredWidth, i20);
                    }
                }
                boolean z11 = false;
                if (mode2 == 1073741824 || aVar.height != -1) {
                    z2 = z6;
                } else {
                    z2 = true;
                    z11 = true;
                }
                int i26 = aVar.bottomMargin + aVar.topMargin;
                int measuredHeight = b2.getMeasuredHeight() + i26;
                int combineMeasuredStates = View.combineMeasuredStates(i17, b2.getMeasuredState());
                if (z8) {
                    int baseline = b2.getBaseline();
                    if (baseline != -1) {
                        int i27 = ((((aVar.h < 0 ? this.e : aVar.h) & 112) >> 4) & -2) >> 1;
                        iArr[i27] = Math.max(iArr[i27], baseline);
                        iArr2[i27] = Math.max(iArr2[i27], measuredHeight - baseline);
                    }
                }
                int max2 = Math.max(i16, measuredHeight);
                z3 = z5 && aVar.height == -1;
                if (aVar.g > 0.0f) {
                    i12 = Math.max(i19, z11 ? i26 : measuredHeight);
                    max = i18;
                } else {
                    if (!z11) {
                        i26 = measuredHeight;
                    }
                    max = Math.max(i18, i26);
                    i12 = i19;
                }
                i21 += a(b2, i21);
                z4 = z7;
                i19 = i12;
                i18 = max;
                i13 = max2;
                i14 = i20;
                i15 = combineMeasuredStates;
                z6 = z2;
                f4 = f5;
            }
            i21++;
            i20 = i14;
            z7 = z4;
            z5 = z3;
            i17 = i15;
            i16 = i13;
        }
        if (this.f > 0 && c(virtualChildCount)) {
            this.f += this.l;
        }
        int max3 = (iArr[1] == -1 && iArr[0] == -1 && iArr[2] == -1 && iArr[3] == -1) ? i16 : Math.max(i16, Math.max(iArr[3], Math.max(iArr[0], Math.max(iArr[1], iArr[2]))) + Math.max(iArr2[3], Math.max(iArr2[0], Math.max(iArr2[1], iArr2[2]))));
        if (z9 && (mode == Integer.MIN_VALUE || mode == 0)) {
            this.f = 0;
            int i28 = 0;
            while (i28 < virtualChildCount) {
                View b3 = b(i28);
                if (b3 == null) {
                    this.f += d(i28);
                    i11 = i28;
                } else if (b3.getVisibility() == 8) {
                    i11 = a(b3, i28) + i28;
                } else {
                    a aVar2 = (a) b3.getLayoutParams();
                    if (z10) {
                        this.f = aVar2.rightMargin + aVar2.leftMargin + i20 + b(b3) + this.f;
                        i11 = i28;
                    } else {
                        int i29 = this.f;
                        this.f = Math.max(i29, aVar2.rightMargin + i29 + i20 + aVar2.leftMargin + b(b3));
                        i11 = i28;
                    }
                }
                i28 = i11 + 1;
            }
        }
        this.f += getPaddingLeft() + getPaddingRight();
        int resolveSizeAndState = View.resolveSizeAndState(Math.max(this.f, getSuggestedMinimumWidth()), i2, 0);
        int i30 = (16777215 & resolveSizeAndState) - this.f;
        if (z7 || (i30 != 0 && f4 > 0.0f)) {
            if (this.g > 0.0f) {
                f4 = this.g;
            }
            iArr[3] = -1;
            iArr[2] = -1;
            iArr[1] = -1;
            iArr[0] = -1;
            iArr2[3] = -1;
            iArr2[2] = -1;
            iArr2[1] = -1;
            iArr2[0] = -1;
            this.f = 0;
            int i31 = 0;
            float f6 = f4;
            boolean z12 = z5;
            int i32 = i18;
            int i33 = i17;
            int i34 = i30;
            int i35 = -1;
            while (i31 < virtualChildCount) {
                View b4 = b(i31);
                if (b4 == null) {
                    f2 = f6;
                    i6 = i34;
                    i7 = i35;
                    i8 = i32;
                    z = z12;
                } else if (b4.getVisibility() == 8) {
                    f2 = f6;
                    i6 = i34;
                    i7 = i35;
                    i8 = i32;
                    z = z12;
                } else {
                    a aVar3 = (a) b4.getLayoutParams();
                    float f7 = aVar3.g;
                    if (f7 > 0.0f) {
                        int i36 = (int) ((((float) i34) * f7) / f6);
                        float f8 = f6 - f7;
                        i9 = i34 - i36;
                        int childMeasureSpec = getChildMeasureSpec(i3, getPaddingTop() + getPaddingBottom() + aVar3.topMargin + aVar3.bottomMargin, aVar3.height);
                        if (aVar3.width == 0 && mode == 1073741824) {
                            if (i36 <= 0) {
                                i36 = 0;
                            }
                            b4.measure(MeasureSpec.makeMeasureSpec(i36, 1073741824), childMeasureSpec);
                        } else {
                            int measuredWidth2 = i36 + b4.getMeasuredWidth();
                            if (measuredWidth2 < 0) {
                                measuredWidth2 = 0;
                            }
                            b4.measure(MeasureSpec.makeMeasureSpec(measuredWidth2, 1073741824), childMeasureSpec);
                        }
                        i10 = View.combineMeasuredStates(i33, b4.getMeasuredState() & -16777216);
                        f3 = f8;
                    } else {
                        i9 = i34;
                        i10 = i33;
                        f3 = f6;
                    }
                    if (z10) {
                        this.f += b4.getMeasuredWidth() + aVar3.leftMargin + aVar3.rightMargin + b(b4);
                    } else {
                        int i37 = this.f;
                        this.f = Math.max(i37, b4.getMeasuredWidth() + i37 + aVar3.leftMargin + aVar3.rightMargin + b(b4));
                    }
                    boolean z13 = mode2 != 1073741824 && aVar3.height == -1;
                    int i38 = aVar3.topMargin + aVar3.bottomMargin;
                    int measuredHeight2 = b4.getMeasuredHeight() + i38;
                    int max4 = Math.max(i35, measuredHeight2);
                    int max5 = Math.max(i32, z13 ? i38 : measuredHeight2);
                    boolean z14 = z12 && aVar3.height == -1;
                    if (z8) {
                        int baseline2 = b4.getBaseline();
                        if (baseline2 != -1) {
                            int i39 = ((((aVar3.h < 0 ? this.e : aVar3.h) & 112) >> 4) & -2) >> 1;
                            iArr[i39] = Math.max(iArr[i39], baseline2);
                            iArr2[i39] = Math.max(iArr2[i39], measuredHeight2 - baseline2);
                        }
                    }
                    f2 = f3;
                    i8 = max5;
                    z = z14;
                    i33 = i10;
                    i6 = i9;
                    i7 = max4;
                }
                i31++;
                i32 = i8;
                i35 = i7;
                z12 = z;
                i34 = i6;
                f6 = f2;
            }
            this.f += getPaddingLeft() + getPaddingRight();
            if (!(iArr[1] == -1 && iArr[0] == -1 && iArr[2] == -1 && iArr[3] == -1)) {
                i35 = Math.max(i35, Math.max(iArr[3], Math.max(iArr[0], Math.max(iArr[1], iArr[2]))) + Math.max(iArr2[3], Math.max(iArr2[0], Math.max(iArr2[1], iArr2[2]))));
            }
            z5 = z12;
            i5 = i32;
            i17 = i33;
            i4 = i35;
        } else {
            int max6 = Math.max(i18, i19);
            if (z9 && mode != 1073741824) {
                int i40 = 0;
                while (true) {
                    int i41 = i40;
                    if (i41 >= virtualChildCount) {
                        break;
                    }
                    View b5 = b(i41);
                    if (!(b5 == null || b5.getVisibility() == 8 || ((a) b5.getLayoutParams()).g <= 0.0f)) {
                        b5.measure(MeasureSpec.makeMeasureSpec(i20, 1073741824), MeasureSpec.makeMeasureSpec(b5.getMeasuredHeight(), 1073741824));
                    }
                    i40 = i41 + 1;
                }
            }
            i5 = max6;
            i4 = max3;
        }
        if (z5 || mode2 == 1073741824) {
            i5 = i4;
        }
        setMeasuredDimension((-16777216 & i17) | resolveSizeAndState, View.resolveSizeAndState(Math.max(i5 + getPaddingTop() + getPaddingBottom(), getSuggestedMinimumHeight()), i3, i17 << 16));
        if (z6) {
            d(virtualChildCount, i2);
        }
    }

    /* access modifiers changed from: 0000 */
    public void b(int i2, int i3, int i4, int i5) {
        int paddingLeft;
        int i6;
        int i7;
        int i8;
        int i9;
        boolean a2 = bw.a(this);
        int paddingTop = getPaddingTop();
        int i10 = i5 - i3;
        int paddingBottom = i10 - getPaddingBottom();
        int paddingBottom2 = (i10 - paddingTop) - getPaddingBottom();
        int virtualChildCount = getVirtualChildCount();
        int i11 = this.e & 8388615;
        int i12 = this.e & 112;
        boolean z = this.f1217a;
        int[] iArr = this.i;
        int[] iArr2 = this.j;
        switch (e.a(i11, t.e(this))) {
            case 1:
                paddingLeft = getPaddingLeft() + (((i4 - i2) - this.f) / 2);
                break;
            case 5:
                paddingLeft = ((getPaddingLeft() + i4) - i2) - this.f;
                break;
            default:
                paddingLeft = getPaddingLeft();
                break;
        }
        if (a2) {
            i6 = -1;
            i7 = virtualChildCount - 1;
        } else {
            i6 = 1;
            i7 = 0;
        }
        int i13 = 0;
        while (i13 < virtualChildCount) {
            int i14 = i7 + (i6 * i13);
            View b2 = b(i14);
            if (b2 == null) {
                paddingLeft += d(i14);
                i8 = i13;
            } else if (b2.getVisibility() != 8) {
                int measuredWidth = b2.getMeasuredWidth();
                int measuredHeight = b2.getMeasuredHeight();
                a aVar = (a) b2.getLayoutParams();
                int i15 = (!z || aVar.height == -1) ? -1 : b2.getBaseline();
                int i16 = aVar.h;
                if (i16 < 0) {
                    i16 = i12;
                }
                switch (i16 & 112) {
                    case 16:
                        i9 = ((((paddingBottom2 - measuredHeight) / 2) + paddingTop) + aVar.topMargin) - aVar.bottomMargin;
                        break;
                    case 48:
                        i9 = paddingTop + aVar.topMargin;
                        if (i15 != -1) {
                            i9 += iArr[1] - i15;
                            break;
                        }
                        break;
                    case 80:
                        i9 = (paddingBottom - measuredHeight) - aVar.bottomMargin;
                        if (i15 != -1) {
                            i9 -= iArr2[2] - (b2.getMeasuredHeight() - i15);
                            break;
                        }
                        break;
                    default:
                        i9 = paddingTop;
                        break;
                }
                int i17 = (c(i14) ? this.l + paddingLeft : paddingLeft) + aVar.leftMargin;
                a(b2, i17 + a(b2), i9, measuredWidth, measuredHeight);
                paddingLeft = i17 + aVar.rightMargin + measuredWidth + b(b2);
                i8 = a(b2, i14) + i13;
            } else {
                i8 = i13;
            }
            i13 = i8 + 1;
        }
    }

    /* access modifiers changed from: 0000 */
    public void b(Canvas canvas) {
        int right;
        int virtualChildCount = getVirtualChildCount();
        boolean a2 = bw.a(this);
        for (int i2 = 0; i2 < virtualChildCount; i2++) {
            View b2 = b(i2);
            if (!(b2 == null || b2.getVisibility() == 8 || !c(i2))) {
                a aVar = (a) b2.getLayoutParams();
                b(canvas, a2 ? aVar.rightMargin + b2.getRight() : (b2.getLeft() - aVar.leftMargin) - this.l);
            }
        }
        if (c(virtualChildCount)) {
            View b3 = b(virtualChildCount - 1);
            if (b3 == null) {
                right = a2 ? getPaddingLeft() : (getWidth() - getPaddingRight()) - this.l;
            } else {
                a aVar2 = (a) b3.getLayoutParams();
                right = a2 ? (b3.getLeft() - aVar2.leftMargin) - this.l : aVar2.rightMargin + b3.getRight();
            }
            b(canvas, right);
        }
    }

    /* access modifiers changed from: 0000 */
    public void b(Canvas canvas, int i2) {
        this.k.setBounds(i2, getPaddingTop() + this.o, this.l + i2, (getHeight() - getPaddingBottom()) - this.o);
        this.k.draw(canvas);
    }

    /* access modifiers changed from: protected */
    public boolean c(int i2) {
        if (i2 == 0) {
            return (this.n & 1) != 0;
        }
        if (i2 == getChildCount()) {
            return (this.n & 4) != 0;
        }
        if ((this.n & 2) == 0) {
            return false;
        }
        for (int i3 = i2 - 1; i3 >= 0; i3--) {
            if (getChildAt(i3).getVisibility() != 8) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(LayoutParams layoutParams) {
        return layoutParams instanceof a;
    }

    /* access modifiers changed from: 0000 */
    public int d(int i2) {
        return 0;
    }

    public int getBaseline() {
        int i2;
        if (this.f1218b < 0) {
            return super.getBaseline();
        }
        if (getChildCount() <= this.f1218b) {
            throw new RuntimeException("mBaselineAlignedChildIndex of LinearLayout set to an index that is out of bounds.");
        }
        View childAt = getChildAt(this.f1218b);
        int baseline = childAt.getBaseline();
        if (baseline != -1) {
            int i3 = this.c;
            if (this.d == 1) {
                int i4 = this.e & 112;
                if (i4 != 48) {
                    switch (i4) {
                        case 16:
                            i2 = i3 + (((((getBottom() - getTop()) - getPaddingTop()) - getPaddingBottom()) - this.f) / 2);
                            break;
                        case 80:
                            i2 = ((getBottom() - getTop()) - getPaddingBottom()) - this.f;
                            break;
                    }
                }
            }
            i2 = i3;
            return ((a) childAt.getLayoutParams()).topMargin + i2 + baseline;
        } else if (this.f1218b == 0) {
            return -1;
        } else {
            throw new RuntimeException("mBaselineAlignedChildIndex of LinearLayout points to a View that doesn't know how to get its baseline.");
        }
    }

    public int getBaselineAlignedChildIndex() {
        return this.f1218b;
    }

    public Drawable getDividerDrawable() {
        return this.k;
    }

    public int getDividerPadding() {
        return this.o;
    }

    public int getDividerWidth() {
        return this.l;
    }

    public int getGravity() {
        return this.e;
    }

    public int getOrientation() {
        return this.d;
    }

    public int getShowDividers() {
        return this.n;
    }

    /* access modifiers changed from: 0000 */
    public int getVirtualChildCount() {
        return getChildCount();
    }

    public float getWeightSum() {
        return this.g;
    }

    /* access modifiers changed from: protected */
    /* renamed from: j */
    public a generateDefaultLayoutParams() {
        if (this.d == 0) {
            return new a(-2, -2);
        }
        if (this.d == 1) {
            return new a(-1, -2);
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        if (this.k != null) {
            if (this.d == 1) {
                a(canvas);
            } else {
                b(canvas);
            }
        }
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        super.onInitializeAccessibilityEvent(accessibilityEvent);
        accessibilityEvent.setClassName(at.class.getName());
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo accessibilityNodeInfo) {
        super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo);
        accessibilityNodeInfo.setClassName(at.class.getName());
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        if (this.d == 1) {
            a(i2, i3, i4, i5);
        } else {
            b(i2, i3, i4, i5);
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        if (this.d == 1) {
            a(i2, i3);
        } else {
            b(i2, i3);
        }
    }

    public void setBaselineAligned(boolean z) {
        this.f1217a = z;
    }

    public void setBaselineAlignedChildIndex(int i2) {
        if (i2 < 0 || i2 >= getChildCount()) {
            throw new IllegalArgumentException("base aligned child index out of range (0, " + getChildCount() + ")");
        }
        this.f1218b = i2;
    }

    public void setDividerDrawable(Drawable drawable) {
        boolean z = false;
        if (drawable != this.k) {
            this.k = drawable;
            if (drawable != null) {
                this.l = drawable.getIntrinsicWidth();
                this.m = drawable.getIntrinsicHeight();
            } else {
                this.l = 0;
                this.m = 0;
            }
            if (drawable == null) {
                z = true;
            }
            setWillNotDraw(z);
            requestLayout();
        }
    }

    public void setDividerPadding(int i2) {
        this.o = i2;
    }

    public void setGravity(int i2) {
        if (this.e != i2) {
            int i3 = (8388615 & i2) == 0 ? 8388611 | i2 : i2;
            if ((i3 & 112) == 0) {
                i3 |= 48;
            }
            this.e = i3;
            requestLayout();
        }
    }

    public void setHorizontalGravity(int i2) {
        int i3 = i2 & 8388615;
        if ((this.e & 8388615) != i3) {
            this.e = i3 | (this.e & -8388616);
            requestLayout();
        }
    }

    public void setMeasureWithLargestChildEnabled(boolean z) {
        this.h = z;
    }

    public void setOrientation(int i2) {
        if (this.d != i2) {
            this.d = i2;
            requestLayout();
        }
    }

    public void setShowDividers(int i2) {
        if (i2 != this.n) {
            requestLayout();
        }
        this.n = i2;
    }

    public void setVerticalGravity(int i2) {
        int i3 = i2 & 112;
        if ((this.e & 112) != i3) {
            this.e = i3 | (this.e & -113);
            requestLayout();
        }
    }

    public void setWeightSum(float f2) {
        this.g = Math.max(0.0f, f2);
    }

    public boolean shouldDelayChildPressedState() {
        return false;
    }
}
