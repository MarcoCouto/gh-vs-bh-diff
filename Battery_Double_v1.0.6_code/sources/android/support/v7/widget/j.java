package android.support.v7.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.support.v4.widget.o;
import android.support.v7.a.a.C0026a;
import android.support.v7.c.a.b;
import android.util.AttributeSet;
import android.widget.CheckBox;

public class j extends CheckBox implements o {

    /* renamed from: a reason: collision with root package name */
    private final l f1315a;

    public j(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, C0026a.checkboxStyle);
    }

    public j(Context context, AttributeSet attributeSet, int i) {
        super(bl.a(context), attributeSet, i);
        this.f1315a = new l(this);
        this.f1315a.a(attributeSet, i);
    }

    public int getCompoundPaddingLeft() {
        int compoundPaddingLeft = super.getCompoundPaddingLeft();
        return this.f1315a != null ? this.f1315a.a(compoundPaddingLeft) : compoundPaddingLeft;
    }

    public ColorStateList getSupportButtonTintList() {
        if (this.f1315a != null) {
            return this.f1315a.a();
        }
        return null;
    }

    public Mode getSupportButtonTintMode() {
        if (this.f1315a != null) {
            return this.f1315a.b();
        }
        return null;
    }

    public void setButtonDrawable(int i) {
        setButtonDrawable(b.b(getContext(), i));
    }

    public void setButtonDrawable(Drawable drawable) {
        super.setButtonDrawable(drawable);
        if (this.f1315a != null) {
            this.f1315a.c();
        }
    }

    public void setSupportButtonTintList(ColorStateList colorStateList) {
        if (this.f1315a != null) {
            this.f1315a.a(colorStateList);
        }
    }

    public void setSupportButtonTintMode(Mode mode) {
        if (this.f1315a != null) {
            this.f1315a.a(mode);
        }
    }
}
