package android.support.v7.widget;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.DataSetObserver;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.i.c;
import android.support.v7.a.a.f;
import android.support.v7.a.a.g;
import android.support.v7.a.a.h;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.TextView;

public class ActivityChooserView extends ViewGroup {

    /* renamed from: a reason: collision with root package name */
    final a f1034a;

    /* renamed from: b reason: collision with root package name */
    final FrameLayout f1035b;
    final FrameLayout c;
    c d;
    final DataSetObserver e;
    OnDismissListener f;
    boolean g;
    int h;
    private final b i;
    private final at j;
    private final ImageView k;
    private final int l;
    private final OnGlobalLayoutListener m;
    private au n;
    private boolean o;
    private int p;

    public static class InnerLayout extends at {

        /* renamed from: a reason: collision with root package name */
        private static final int[] f1036a = {16842964};

        public InnerLayout(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            bo a2 = bo.a(context, attributeSet, f1036a);
            setBackgroundDrawable(a2.a(0));
            a2.a();
        }
    }

    private class a extends BaseAdapter {

        /* renamed from: a reason: collision with root package name */
        final /* synthetic */ ActivityChooserView f1037a;

        /* renamed from: b reason: collision with root package name */
        private e f1038b;
        private int c;
        private boolean d;
        private boolean e;
        private boolean f;

        public int a() {
            int i = this.c;
            this.c = Integer.MAX_VALUE;
            int makeMeasureSpec = MeasureSpec.makeMeasureSpec(0, 0);
            int makeMeasureSpec2 = MeasureSpec.makeMeasureSpec(0, 0);
            int count = getCount();
            View view = null;
            int i2 = 0;
            for (int i3 = 0; i3 < count; i3++) {
                view = getView(i3, view, null);
                view.measure(makeMeasureSpec, makeMeasureSpec2);
                i2 = Math.max(i2, view.getMeasuredWidth());
            }
            this.c = i;
            return i2;
        }

        public void a(int i) {
            if (this.c != i) {
                this.c = i;
                notifyDataSetChanged();
            }
        }

        public void a(e eVar) {
            e d2 = this.f1037a.f1034a.d();
            if (d2 != null && this.f1037a.isShown()) {
                d2.unregisterObserver(this.f1037a.e);
            }
            this.f1038b = eVar;
            if (eVar != null && this.f1037a.isShown()) {
                eVar.registerObserver(this.f1037a.e);
            }
            notifyDataSetChanged();
        }

        public void a(boolean z) {
            if (this.f != z) {
                this.f = z;
                notifyDataSetChanged();
            }
        }

        public void a(boolean z, boolean z2) {
            if (this.d != z || this.e != z2) {
                this.d = z;
                this.e = z2;
                notifyDataSetChanged();
            }
        }

        public ResolveInfo b() {
            return this.f1038b.b();
        }

        public int c() {
            return this.f1038b.a();
        }

        public e d() {
            return this.f1038b;
        }

        public boolean e() {
            return this.d;
        }

        public int getCount() {
            int a2 = this.f1038b.a();
            if (!this.d && this.f1038b.b() != null) {
                a2--;
            }
            int min = Math.min(a2, this.c);
            return this.f ? min + 1 : min;
        }

        public Object getItem(int i) {
            switch (getItemViewType(i)) {
                case 0:
                    if (!this.d && this.f1038b.b() != null) {
                        i++;
                    }
                    return this.f1038b.a(i);
                case 1:
                    return null;
                default:
                    throw new IllegalArgumentException();
            }
        }

        public long getItemId(int i) {
            return (long) i;
        }

        public int getItemViewType(int i) {
            return (!this.f || i != getCount() + -1) ? 0 : 1;
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            switch (getItemViewType(i)) {
                case 0:
                    if (view == null || view.getId() != f.list_item) {
                        view = LayoutInflater.from(this.f1037a.getContext()).inflate(g.abc_activity_chooser_view_list_item, viewGroup, false);
                    }
                    PackageManager packageManager = this.f1037a.getContext().getPackageManager();
                    ResolveInfo resolveInfo = (ResolveInfo) getItem(i);
                    ((ImageView) view.findViewById(f.icon)).setImageDrawable(resolveInfo.loadIcon(packageManager));
                    ((TextView) view.findViewById(f.title)).setText(resolveInfo.loadLabel(packageManager));
                    if (!this.d || i != 0 || !this.e) {
                        view.setActivated(false);
                        return view;
                    }
                    view.setActivated(true);
                    return view;
                case 1:
                    if (view != null && view.getId() == 1) {
                        return view;
                    }
                    View inflate = LayoutInflater.from(this.f1037a.getContext()).inflate(g.abc_activity_chooser_view_list_item, viewGroup, false);
                    inflate.setId(1);
                    ((TextView) inflate.findViewById(f.title)).setText(this.f1037a.getContext().getString(h.abc_activity_chooser_view_see_all));
                    return inflate;
                default:
                    throw new IllegalArgumentException();
            }
        }

        public int getViewTypeCount() {
            return 3;
        }
    }

    private class b implements OnClickListener, OnLongClickListener, OnItemClickListener, OnDismissListener {

        /* renamed from: a reason: collision with root package name */
        final /* synthetic */ ActivityChooserView f1039a;

        private void a() {
            if (this.f1039a.f != null) {
                this.f1039a.f.onDismiss();
            }
        }

        public void onClick(View view) {
            if (view == this.f1039a.c) {
                this.f1039a.b();
                Intent b2 = this.f1039a.f1034a.d().b(this.f1039a.f1034a.d().a(this.f1039a.f1034a.b()));
                if (b2 != null) {
                    b2.addFlags(524288);
                    this.f1039a.getContext().startActivity(b2);
                }
            } else if (view == this.f1039a.f1035b) {
                this.f1039a.g = false;
                this.f1039a.a(this.f1039a.h);
            } else {
                throw new IllegalArgumentException();
            }
        }

        public void onDismiss() {
            a();
            if (this.f1039a.d != null) {
                this.f1039a.d.a(false);
            }
        }

        public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            switch (((a) adapterView.getAdapter()).getItemViewType(i)) {
                case 0:
                    this.f1039a.b();
                    if (!this.f1039a.g) {
                        if (!this.f1039a.f1034a.e()) {
                            i++;
                        }
                        Intent b2 = this.f1039a.f1034a.d().b(i);
                        if (b2 != null) {
                            b2.addFlags(524288);
                            this.f1039a.getContext().startActivity(b2);
                            return;
                        }
                        return;
                    } else if (i > 0) {
                        this.f1039a.f1034a.d().c(i);
                        return;
                    } else {
                        return;
                    }
                case 1:
                    this.f1039a.a(Integer.MAX_VALUE);
                    return;
                default:
                    throw new IllegalArgumentException();
            }
        }

        public boolean onLongClick(View view) {
            if (view == this.f1039a.c) {
                if (this.f1039a.f1034a.getCount() > 0) {
                    this.f1039a.g = true;
                    this.f1039a.a(this.f1039a.h);
                }
                return true;
            }
            throw new IllegalArgumentException();
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(int i2) {
        if (this.f1034a.d() == null) {
            throw new IllegalStateException("No data model. Did you call #setDataModel?");
        }
        getViewTreeObserver().addOnGlobalLayoutListener(this.m);
        boolean z = this.c.getVisibility() == 0;
        int c2 = this.f1034a.c();
        int i3 = z ? 1 : 0;
        if (i2 == Integer.MAX_VALUE || c2 <= i3 + i2) {
            this.f1034a.a(false);
            this.f1034a.a(i2);
        } else {
            this.f1034a.a(true);
            this.f1034a.a(i2 - 1);
        }
        au listPopupWindow = getListPopupWindow();
        if (!listPopupWindow.f()) {
            if (this.g || !z) {
                this.f1034a.a(true, z);
            } else {
                this.f1034a.a(false, false);
            }
            listPopupWindow.g(Math.min(this.f1034a.a(), this.l));
            listPopupWindow.d();
            if (this.d != null) {
                this.d.a(true);
            }
            listPopupWindow.g().setContentDescription(getContext().getString(h.abc_activitychooserview_choose_application));
            listPopupWindow.g().setSelector(new ColorDrawable(0));
        }
    }

    public boolean a() {
        if (c() || !this.o) {
            return false;
        }
        this.g = false;
        a(this.h);
        return true;
    }

    public boolean b() {
        if (c()) {
            getListPopupWindow().e();
            ViewTreeObserver viewTreeObserver = getViewTreeObserver();
            if (viewTreeObserver.isAlive()) {
                viewTreeObserver.removeGlobalOnLayoutListener(this.m);
            }
        }
        return true;
    }

    public boolean c() {
        return getListPopupWindow().f();
    }

    public e getDataModel() {
        return this.f1034a.d();
    }

    /* access modifiers changed from: 0000 */
    public au getListPopupWindow() {
        if (this.n == null) {
            this.n = new au(getContext());
            this.n.a((ListAdapter) this.f1034a);
            this.n.b((View) this);
            this.n.a(true);
            this.n.a((OnItemClickListener) this.i);
            this.n.a((OnDismissListener) this.i);
        }
        return this.n;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        e d2 = this.f1034a.d();
        if (d2 != null) {
            d2.registerObserver(this.e);
        }
        this.o = true;
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        e d2 = this.f1034a.d();
        if (d2 != null) {
            d2.unregisterObserver(this.e);
        }
        ViewTreeObserver viewTreeObserver = getViewTreeObserver();
        if (viewTreeObserver.isAlive()) {
            viewTreeObserver.removeGlobalOnLayoutListener(this.m);
        }
        if (c()) {
            b();
        }
        this.o = false;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        this.j.layout(0, 0, i4 - i2, i5 - i3);
        if (!c()) {
            b();
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        at atVar = this.j;
        if (this.c.getVisibility() != 0) {
            i3 = MeasureSpec.makeMeasureSpec(MeasureSpec.getSize(i3), 1073741824);
        }
        measureChild(atVar, i2, i3);
        setMeasuredDimension(atVar.getMeasuredWidth(), atVar.getMeasuredHeight());
    }

    public void setActivityChooserModel(e eVar) {
        this.f1034a.a(eVar);
        if (c()) {
            b();
            a();
        }
    }

    public void setDefaultActionButtonContentDescription(int i2) {
        this.p = i2;
    }

    public void setExpandActivityOverflowButtonContentDescription(int i2) {
        this.k.setContentDescription(getContext().getString(i2));
    }

    public void setExpandActivityOverflowButtonDrawable(Drawable drawable) {
        this.k.setImageDrawable(drawable);
    }

    public void setInitialActivityCount(int i2) {
        this.h = i2;
    }

    public void setOnDismissListener(OnDismissListener onDismissListener) {
        this.f = onDismissListener;
    }

    public void setProvider(c cVar) {
        this.d = cVar;
    }
}
