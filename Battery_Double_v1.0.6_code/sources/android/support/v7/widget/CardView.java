package android.support.v7.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.support.v7.b.a.C0029a;
import android.support.v7.b.a.c;
import android.support.v7.b.a.d;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.MeasureSpec;
import android.widget.FrameLayout;

public class CardView extends FrameLayout {
    private static final int[] e = {16842801};
    private static final ah f;

    /* renamed from: a reason: collision with root package name */
    int f1042a;

    /* renamed from: b reason: collision with root package name */
    int f1043b;
    final Rect c = new Rect();
    final Rect d = new Rect();
    private boolean g;
    private boolean h;
    private final ag i = new ag() {

        /* renamed from: b reason: collision with root package name */
        private Drawable f1045b;

        public void a(int i, int i2) {
            if (i > CardView.this.f1042a) {
                CardView.super.setMinimumWidth(i);
            }
            if (i2 > CardView.this.f1043b) {
                CardView.super.setMinimumHeight(i2);
            }
        }

        public void a(int i, int i2, int i3, int i4) {
            CardView.this.d.set(i, i2, i3, i4);
            CardView.super.setPadding(CardView.this.c.left + i, CardView.this.c.top + i2, CardView.this.c.right + i3, CardView.this.c.bottom + i4);
        }

        public void a(Drawable drawable) {
            this.f1045b = drawable;
            CardView.this.setBackgroundDrawable(drawable);
        }

        public boolean a() {
            return CardView.this.getUseCompatPadding();
        }

        public boolean b() {
            return CardView.this.getPreventCornerOverlap();
        }

        public Drawable c() {
            return this.f1045b;
        }

        public View d() {
            return CardView.this;
        }
    };

    static {
        if (VERSION.SDK_INT >= 21) {
            f = new ae();
        } else if (VERSION.SDK_INT >= 17) {
            f = new ad();
        } else {
            f = new af();
        }
        f.a();
    }

    public CardView(Context context) {
        super(context);
        a(context, null, 0);
    }

    public CardView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context, attributeSet, 0);
    }

    public CardView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        a(context, attributeSet, i2);
    }

    private void a(Context context, AttributeSet attributeSet, int i2) {
        ColorStateList valueOf;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, d.CardView, i2, c.CardView);
        if (obtainStyledAttributes.hasValue(d.CardView_cardBackgroundColor)) {
            valueOf = obtainStyledAttributes.getColorStateList(d.CardView_cardBackgroundColor);
        } else {
            TypedArray obtainStyledAttributes2 = getContext().obtainStyledAttributes(e);
            int color = obtainStyledAttributes2.getColor(0, 0);
            obtainStyledAttributes2.recycle();
            float[] fArr = new float[3];
            Color.colorToHSV(color, fArr);
            valueOf = ColorStateList.valueOf(fArr[2] > 0.5f ? getResources().getColor(C0029a.cardview_light_background) : getResources().getColor(C0029a.cardview_dark_background));
        }
        float dimension = obtainStyledAttributes.getDimension(d.CardView_cardCornerRadius, 0.0f);
        float dimension2 = obtainStyledAttributes.getDimension(d.CardView_cardElevation, 0.0f);
        float dimension3 = obtainStyledAttributes.getDimension(d.CardView_cardMaxElevation, 0.0f);
        this.g = obtainStyledAttributes.getBoolean(d.CardView_cardUseCompatPadding, false);
        this.h = obtainStyledAttributes.getBoolean(d.CardView_cardPreventCornerOverlap, true);
        int dimensionPixelSize = obtainStyledAttributes.getDimensionPixelSize(d.CardView_contentPadding, 0);
        this.c.left = obtainStyledAttributes.getDimensionPixelSize(d.CardView_contentPaddingLeft, dimensionPixelSize);
        this.c.top = obtainStyledAttributes.getDimensionPixelSize(d.CardView_contentPaddingTop, dimensionPixelSize);
        this.c.right = obtainStyledAttributes.getDimensionPixelSize(d.CardView_contentPaddingRight, dimensionPixelSize);
        this.c.bottom = obtainStyledAttributes.getDimensionPixelSize(d.CardView_contentPaddingBottom, dimensionPixelSize);
        if (dimension2 > dimension3) {
            dimension3 = dimension2;
        }
        this.f1042a = obtainStyledAttributes.getDimensionPixelSize(d.CardView_android_minWidth, 0);
        this.f1043b = obtainStyledAttributes.getDimensionPixelSize(d.CardView_android_minHeight, 0);
        obtainStyledAttributes.recycle();
        f.a(this.i, context, valueOf, dimension, dimension2, dimension3);
    }

    public ColorStateList getCardBackgroundColor() {
        return f.i(this.i);
    }

    public float getCardElevation() {
        return f.e(this.i);
    }

    public int getContentPaddingBottom() {
        return this.c.bottom;
    }

    public int getContentPaddingLeft() {
        return this.c.left;
    }

    public int getContentPaddingRight() {
        return this.c.right;
    }

    public int getContentPaddingTop() {
        return this.c.top;
    }

    public float getMaxCardElevation() {
        return f.a(this.i);
    }

    public boolean getPreventCornerOverlap() {
        return this.h;
    }

    public float getRadius() {
        return f.d(this.i);
    }

    public boolean getUseCompatPadding() {
        return this.g;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        if (!(f instanceof ae)) {
            int mode = MeasureSpec.getMode(i2);
            switch (mode) {
                case Integer.MIN_VALUE:
                case 1073741824:
                    i2 = MeasureSpec.makeMeasureSpec(Math.max((int) Math.ceil((double) f.b(this.i)), MeasureSpec.getSize(i2)), mode);
                    break;
            }
            int mode2 = MeasureSpec.getMode(i3);
            switch (mode2) {
                case Integer.MIN_VALUE:
                case 1073741824:
                    i3 = MeasureSpec.makeMeasureSpec(Math.max((int) Math.ceil((double) f.c(this.i)), MeasureSpec.getSize(i3)), mode2);
                    break;
            }
            super.onMeasure(i2, i3);
            return;
        }
        super.onMeasure(i2, i3);
    }

    public void setCardBackgroundColor(int i2) {
        f.a(this.i, ColorStateList.valueOf(i2));
    }

    public void setCardBackgroundColor(ColorStateList colorStateList) {
        f.a(this.i, colorStateList);
    }

    public void setCardElevation(float f2) {
        f.c(this.i, f2);
    }

    public void setMaxCardElevation(float f2) {
        f.b(this.i, f2);
    }

    public void setMinimumHeight(int i2) {
        this.f1043b = i2;
        super.setMinimumHeight(i2);
    }

    public void setMinimumWidth(int i2) {
        this.f1042a = i2;
        super.setMinimumWidth(i2);
    }

    public void setPadding(int i2, int i3, int i4, int i5) {
    }

    public void setPaddingRelative(int i2, int i3, int i4, int i5) {
    }

    public void setPreventCornerOverlap(boolean z) {
        if (z != this.h) {
            this.h = z;
            f.h(this.i);
        }
    }

    public void setRadius(float f2) {
        f.a(this.i, f2);
    }

    public void setUseCompatPadding(boolean z) {
        if (this.g != z) {
            this.g = z;
            f.g(this.i);
        }
    }
}
