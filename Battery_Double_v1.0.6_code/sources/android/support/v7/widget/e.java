package android.support.v7.widget;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.database.DataSetObservable;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;
import android.util.Xml;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

class e extends DataSetObservable {

    /* renamed from: a reason: collision with root package name */
    static final String f1299a = e.class.getSimpleName();
    private static final Object e = new Object();
    private static final Map<String, e> f = new HashMap();

    /* renamed from: b reason: collision with root package name */
    final Context f1300b;
    final String c;
    boolean d;
    private final Object g;
    private final List<a> h;
    private final List<c> i;
    private Intent j;
    private b k;
    private int l;
    private boolean m;
    private boolean n;
    private boolean o;
    private d p;

    public static final class a implements Comparable<a> {

        /* renamed from: a reason: collision with root package name */
        public final ResolveInfo f1301a;

        /* renamed from: b reason: collision with root package name */
        public float f1302b;

        public a(ResolveInfo resolveInfo) {
            this.f1301a = resolveInfo;
        }

        /* renamed from: a */
        public int compareTo(a aVar) {
            return Float.floatToIntBits(aVar.f1302b) - Float.floatToIntBits(this.f1302b);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            return Float.floatToIntBits(this.f1302b) == Float.floatToIntBits(((a) obj).f1302b);
        }

        public int hashCode() {
            return Float.floatToIntBits(this.f1302b) + 31;
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append("[");
            sb.append("resolveInfo:").append(this.f1301a.toString());
            sb.append("; weight:").append(new BigDecimal((double) this.f1302b));
            sb.append("]");
            return sb.toString();
        }
    }

    public interface b {
        void a(Intent intent, List<a> list, List<c> list2);
    }

    public static final class c {

        /* renamed from: a reason: collision with root package name */
        public final ComponentName f1303a;

        /* renamed from: b reason: collision with root package name */
        public final long f1304b;
        public final float c;

        public c(ComponentName componentName, long j, float f) {
            this.f1303a = componentName;
            this.f1304b = j;
            this.c = f;
        }

        public c(String str, long j, float f) {
            this(ComponentName.unflattenFromString(str), j, f);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            c cVar = (c) obj;
            if (this.f1303a == null) {
                if (cVar.f1303a != null) {
                    return false;
                }
            } else if (!this.f1303a.equals(cVar.f1303a)) {
                return false;
            }
            if (this.f1304b != cVar.f1304b) {
                return false;
            }
            return Float.floatToIntBits(this.c) == Float.floatToIntBits(cVar.c);
        }

        public int hashCode() {
            return (((((this.f1303a == null ? 0 : this.f1303a.hashCode()) + 31) * 31) + ((int) (this.f1304b ^ (this.f1304b >>> 32)))) * 31) + Float.floatToIntBits(this.c);
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append("[");
            sb.append("; activity:").append(this.f1303a);
            sb.append("; time:").append(this.f1304b);
            sb.append("; weight:").append(new BigDecimal((double) this.c));
            sb.append("]");
            return sb.toString();
        }
    }

    public interface d {
        boolean a(e eVar, Intent intent);
    }

    /* renamed from: android.support.v7.widget.e$e reason: collision with other inner class name */
    private final class C0034e extends AsyncTask<Object, Void, Void> {
        C0034e() {
        }

        /* renamed from: a */
        public Void doInBackground(Object... objArr) {
            List list = objArr[0];
            String str = objArr[1];
            try {
                FileOutputStream openFileOutput = e.this.f1300b.openFileOutput(str, 0);
                XmlSerializer newSerializer = Xml.newSerializer();
                try {
                    newSerializer.setOutput(openFileOutput, null);
                    newSerializer.startDocument("UTF-8", Boolean.valueOf(true));
                    newSerializer.startTag(null, "historical-records");
                    int size = list.size();
                    for (int i = 0; i < size; i++) {
                        c cVar = (c) list.remove(0);
                        newSerializer.startTag(null, "historical-record");
                        newSerializer.attribute(null, "activity", cVar.f1303a.flattenToString());
                        newSerializer.attribute(null, "time", String.valueOf(cVar.f1304b));
                        newSerializer.attribute(null, "weight", String.valueOf(cVar.c));
                        newSerializer.endTag(null, "historical-record");
                    }
                    newSerializer.endTag(null, "historical-records");
                    newSerializer.endDocument();
                    e.this.d = true;
                    if (openFileOutput != null) {
                        try {
                            openFileOutput.close();
                        } catch (IOException e) {
                        }
                    }
                } catch (IllegalArgumentException e2) {
                    Log.e(e.f1299a, "Error writing historical record file: " + e.this.c, e2);
                    e.this.d = true;
                    if (openFileOutput != null) {
                        try {
                            openFileOutput.close();
                        } catch (IOException e3) {
                        }
                    }
                } catch (IllegalStateException e4) {
                    Log.e(e.f1299a, "Error writing historical record file: " + e.this.c, e4);
                    e.this.d = true;
                    if (openFileOutput != null) {
                        try {
                            openFileOutput.close();
                        } catch (IOException e5) {
                        }
                    }
                } catch (IOException e6) {
                    Log.e(e.f1299a, "Error writing historical record file: " + e.this.c, e6);
                    e.this.d = true;
                    if (openFileOutput != null) {
                        try {
                            openFileOutput.close();
                        } catch (IOException e7) {
                        }
                    }
                } catch (Throwable th) {
                    e.this.d = true;
                    if (openFileOutput != null) {
                        try {
                            openFileOutput.close();
                        } catch (IOException e8) {
                        }
                    }
                    throw th;
                }
            } catch (FileNotFoundException e9) {
                Log.e(e.f1299a, "Error writing historical record file: " + str, e9);
            }
            return null;
        }
    }

    private boolean a(c cVar) {
        boolean add = this.i.add(cVar);
        if (add) {
            this.n = true;
            h();
            c();
            e();
            notifyChanged();
        }
        return add;
    }

    private void c() {
        if (!this.m) {
            throw new IllegalStateException("No preceding call to #readHistoricalData");
        } else if (this.n) {
            this.n = false;
            if (!TextUtils.isEmpty(this.c)) {
                new C0034e().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Object[]{new ArrayList(this.i), this.c});
            }
        }
    }

    private void d() {
        boolean f2 = f() | g();
        h();
        if (f2) {
            e();
            notifyChanged();
        }
    }

    private boolean e() {
        if (this.k == null || this.j == null || this.h.isEmpty() || this.i.isEmpty()) {
            return false;
        }
        this.k.a(this.j, this.h, Collections.unmodifiableList(this.i));
        return true;
    }

    private boolean f() {
        if (!this.o || this.j == null) {
            return false;
        }
        this.o = false;
        this.h.clear();
        List queryIntentActivities = this.f1300b.getPackageManager().queryIntentActivities(this.j, 0);
        int size = queryIntentActivities.size();
        for (int i2 = 0; i2 < size; i2++) {
            this.h.add(new a((ResolveInfo) queryIntentActivities.get(i2)));
        }
        return true;
    }

    private boolean g() {
        if (!this.d || !this.n || TextUtils.isEmpty(this.c)) {
            return false;
        }
        this.d = false;
        this.m = true;
        i();
        return true;
    }

    private void h() {
        int size = this.i.size() - this.l;
        if (size > 0) {
            this.n = true;
            for (int i2 = 0; i2 < size; i2++) {
                c cVar = (c) this.i.remove(0);
            }
        }
    }

    private void i() {
        try {
            FileInputStream openFileInput = this.f1300b.openFileInput(this.c);
            try {
                XmlPullParser newPullParser = Xml.newPullParser();
                newPullParser.setInput(openFileInput, "UTF-8");
                int i2 = 0;
                while (i2 != 1 && i2 != 2) {
                    i2 = newPullParser.next();
                }
                if (!"historical-records".equals(newPullParser.getName())) {
                    throw new XmlPullParserException("Share records file does not start with historical-records tag.");
                }
                List<c> list = this.i;
                list.clear();
                while (true) {
                    int next = newPullParser.next();
                    if (next == 1) {
                        if (openFileInput != null) {
                            try {
                                openFileInput.close();
                                return;
                            } catch (IOException e2) {
                                return;
                            }
                        } else {
                            return;
                        }
                    } else if (!(next == 3 || next == 4)) {
                        if (!"historical-record".equals(newPullParser.getName())) {
                            throw new XmlPullParserException("Share records file not well-formed.");
                        }
                        list.add(new c(newPullParser.getAttributeValue(null, "activity"), Long.parseLong(newPullParser.getAttributeValue(null, "time")), Float.parseFloat(newPullParser.getAttributeValue(null, "weight"))));
                    }
                }
            } catch (XmlPullParserException e3) {
                Log.e(f1299a, "Error reading historical recrod file: " + this.c, e3);
                if (openFileInput != null) {
                    try {
                        openFileInput.close();
                    } catch (IOException e4) {
                    }
                }
            } catch (IOException e5) {
                Log.e(f1299a, "Error reading historical recrod file: " + this.c, e5);
                if (openFileInput != null) {
                    try {
                        openFileInput.close();
                    } catch (IOException e6) {
                    }
                }
            } finally {
                if (openFileInput != null) {
                    try {
                        openFileInput.close();
                    } catch (IOException e7) {
                    }
                }
            }
        } catch (FileNotFoundException e8) {
        }
    }

    public int a() {
        int size;
        synchronized (this.g) {
            d();
            size = this.h.size();
        }
        return size;
    }

    public int a(ResolveInfo resolveInfo) {
        synchronized (this.g) {
            d();
            List<a> list = this.h;
            int size = list.size();
            for (int i2 = 0; i2 < size; i2++) {
                if (((a) list.get(i2)).f1301a == resolveInfo) {
                    return i2;
                }
            }
            return -1;
        }
    }

    public ResolveInfo a(int i2) {
        ResolveInfo resolveInfo;
        synchronized (this.g) {
            d();
            resolveInfo = ((a) this.h.get(i2)).f1301a;
        }
        return resolveInfo;
    }

    public Intent b(int i2) {
        synchronized (this.g) {
            if (this.j == null) {
                return null;
            }
            d();
            a aVar = (a) this.h.get(i2);
            ComponentName componentName = new ComponentName(aVar.f1301a.activityInfo.packageName, aVar.f1301a.activityInfo.name);
            Intent intent = new Intent(this.j);
            intent.setComponent(componentName);
            if (this.p != null) {
                if (this.p.a(this, new Intent(intent))) {
                    return null;
                }
            }
            a(new c(componentName, System.currentTimeMillis(), 1.0f));
            return intent;
        }
    }

    public ResolveInfo b() {
        synchronized (this.g) {
            d();
            if (this.h.isEmpty()) {
                return null;
            }
            ResolveInfo resolveInfo = ((a) this.h.get(0)).f1301a;
            return resolveInfo;
        }
    }

    public void c(int i2) {
        synchronized (this.g) {
            d();
            a aVar = (a) this.h.get(i2);
            a aVar2 = (a) this.h.get(0);
            a(new c(new ComponentName(aVar.f1301a.activityInfo.packageName, aVar.f1301a.activityInfo.name), System.currentTimeMillis(), aVar2 != null ? (aVar2.f1302b - aVar.f1302b) + 5.0f : 1.0f));
        }
    }
}
