package android.support.v7.widget;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.drawable.Drawable;

class b extends Drawable {

    /* renamed from: a reason: collision with root package name */
    final ActionBarContainer f1236a;

    public b(ActionBarContainer actionBarContainer) {
        this.f1236a = actionBarContainer;
    }

    public void draw(Canvas canvas) {
        if (!this.f1236a.d) {
            if (this.f1236a.f1020a != null) {
                this.f1236a.f1020a.draw(canvas);
            }
            if (this.f1236a.f1021b != null && this.f1236a.e) {
                this.f1236a.f1021b.draw(canvas);
            }
        } else if (this.f1236a.c != null) {
            this.f1236a.c.draw(canvas);
        }
    }

    public int getOpacity() {
        return 0;
    }

    public void setAlpha(int i) {
    }

    public void setColorFilter(ColorFilter colorFilter) {
    }
}
