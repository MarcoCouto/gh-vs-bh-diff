package android.support.v7.widget;

import android.graphics.PorterDuff.Mode;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Drawable.ConstantState;
import android.graphics.drawable.DrawableContainer;
import android.graphics.drawable.DrawableContainer.DrawableContainerState;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.InsetDrawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.ScaleDrawable;
import android.os.Build.VERSION;
import android.support.v4.c.a.b;
import android.support.v7.d.a.a;

public class am {

    /* renamed from: a reason: collision with root package name */
    public static final Rect f1196a = new Rect();

    /* renamed from: b reason: collision with root package name */
    private static Class<?> f1197b;

    static {
        if (VERSION.SDK_INT >= 18) {
            try {
                f1197b = Class.forName("android.graphics.Insets");
            } catch (ClassNotFoundException e) {
            }
        }
    }

    public static Mode a(int i, Mode mode) {
        switch (i) {
            case 3:
                return Mode.SRC_OVER;
            case 5:
                return Mode.SRC_IN;
            case 9:
                return Mode.SRC_ATOP;
            case 14:
                return Mode.MULTIPLY;
            case 15:
                return Mode.SCREEN;
            case 16:
                return Mode.ADD;
            default:
                return mode;
        }
    }

    static void a(Drawable drawable) {
        if (VERSION.SDK_INT == 21 && "android.graphics.drawable.VectorDrawable".equals(drawable.getClass().getName())) {
            c(drawable);
        }
    }

    public static boolean b(Drawable drawable) {
        if (VERSION.SDK_INT < 15 && (drawable instanceof InsetDrawable)) {
            return false;
        }
        if (VERSION.SDK_INT < 15 && (drawable instanceof GradientDrawable)) {
            return false;
        }
        if (VERSION.SDK_INT < 17 && (drawable instanceof LayerDrawable)) {
            return false;
        }
        if (drawable instanceof DrawableContainer) {
            ConstantState constantState = drawable.getConstantState();
            if (constantState instanceof DrawableContainerState) {
                for (Drawable b2 : ((DrawableContainerState) constantState).getChildren()) {
                    if (!b(b2)) {
                        return false;
                    }
                }
            }
        } else if (drawable instanceof b) {
            return b(((b) drawable).a());
        } else {
            if (drawable instanceof a) {
                return b(((a) drawable).b());
            }
            if (drawable instanceof ScaleDrawable) {
                return b(((ScaleDrawable) drawable).getDrawable());
            }
        }
        return true;
    }

    private static void c(Drawable drawable) {
        int[] state = drawable.getState();
        if (state == null || state.length == 0) {
            drawable.setState(bj.e);
        } else {
            drawable.setState(bj.h);
        }
        drawable.setState(state);
    }
}
