package android.support.v7.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources.NotFoundException;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.support.v4.b.a.b.a;
import android.support.v4.widget.b;
import android.support.v7.a.a.j;
import android.text.method.PasswordTransformationMethod;
import android.util.AttributeSet;
import android.widget.TextView;
import java.lang.ref.WeakReference;

class z {

    /* renamed from: a reason: collision with root package name */
    final TextView f1355a;

    /* renamed from: b reason: collision with root package name */
    private bm f1356b;
    private bm c;
    private bm d;
    private bm e;
    private final ac f;
    private int g = 0;
    private Typeface h;
    private boolean i;

    z(TextView textView) {
        this.f1355a = textView;
        this.f = new ac(this.f1355a);
    }

    protected static bm a(Context context, m mVar, int i2) {
        ColorStateList b2 = mVar.b(context, i2);
        if (b2 == null) {
            return null;
        }
        bm bmVar = new bm();
        bmVar.d = true;
        bmVar.f1260a = b2;
        return bmVar;
    }

    static z a(TextView textView) {
        return VERSION.SDK_INT >= 17 ? new aa(textView) : new z(textView);
    }

    private void a(Context context, bo boVar) {
        boolean z = true;
        this.g = boVar.a(j.TextAppearance_android_textStyle, this.g);
        if (boVar.g(j.TextAppearance_android_fontFamily) || boVar.g(j.TextAppearance_fontFamily)) {
            this.h = null;
            int i2 = boVar.g(j.TextAppearance_android_fontFamily) ? j.TextAppearance_android_fontFamily : j.TextAppearance_fontFamily;
            if (!context.isRestricted()) {
                final WeakReference weakReference = new WeakReference(this.f1355a);
                try {
                    this.h = boVar.a(i2, this.g, (a) new a() {
                        public void a(int i) {
                        }

                        public void a(Typeface typeface) {
                            z.this.a(weakReference, typeface);
                        }
                    });
                    if (this.h != null) {
                        z = false;
                    }
                    this.i = z;
                } catch (NotFoundException | UnsupportedOperationException e2) {
                }
            }
            if (this.h == null) {
                String d2 = boVar.d(i2);
                if (d2 != null) {
                    this.h = Typeface.create(d2, this.g);
                }
            }
        } else if (boVar.g(j.TextAppearance_android_typeface)) {
            this.i = false;
            switch (boVar.a(j.TextAppearance_android_typeface, 1)) {
                case 1:
                    this.h = Typeface.SANS_SERIF;
                    return;
                case 2:
                    this.h = Typeface.SERIF;
                    return;
                case 3:
                    this.h = Typeface.MONOSPACE;
                    return;
                default:
                    return;
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(WeakReference<TextView> weakReference, Typeface typeface) {
        if (this.i) {
            this.h = typeface;
            TextView textView = (TextView) weakReference.get();
            if (textView != null) {
                textView.setTypeface(typeface, this.g);
            }
        }
    }

    private void b(int i2, float f2) {
        this.f.a(i2, f2);
    }

    /* access modifiers changed from: 0000 */
    public void a() {
        if (this.f1356b != null || this.c != null || this.d != null || this.e != null) {
            Drawable[] compoundDrawables = this.f1355a.getCompoundDrawables();
            a(compoundDrawables[0], this.f1356b);
            a(compoundDrawables[1], this.c);
            a(compoundDrawables[2], this.d);
            a(compoundDrawables[3], this.e);
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(int i2) {
        this.f.a(i2);
    }

    /* access modifiers changed from: 0000 */
    public void a(int i2, float f2) {
        if (!b.f832a && !c()) {
            b(i2, f2);
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(int i2, int i3, int i4, int i5) throws IllegalArgumentException {
        this.f.a(i2, i3, i4, i5);
    }

    /* access modifiers changed from: 0000 */
    public void a(Context context, int i2) {
        bo a2 = bo.a(context, i2, j.TextAppearance);
        if (a2.g(j.TextAppearance_textAllCaps)) {
            a(a2.a(j.TextAppearance_textAllCaps, false));
        }
        if (VERSION.SDK_INT < 23 && a2.g(j.TextAppearance_android_textColor)) {
            ColorStateList e2 = a2.e(j.TextAppearance_android_textColor);
            if (e2 != null) {
                this.f1355a.setTextColor(e2);
            }
        }
        a(context, a2);
        a2.a();
        if (this.h != null) {
            this.f1355a.setTypeface(this.h, this.g);
        }
    }

    /* access modifiers changed from: 0000 */
    public final void a(Drawable drawable, bm bmVar) {
        if (drawable != null && bmVar != null) {
            m.a(drawable, bmVar, this.f1355a.getDrawableState());
        }
    }

    /* access modifiers changed from: 0000 */
    @SuppressLint({"NewApi"})
    public void a(AttributeSet attributeSet, int i2) {
        ColorStateList colorStateList;
        ColorStateList colorStateList2;
        boolean z;
        boolean z2;
        ColorStateList colorStateList3 = null;
        Context context = this.f1355a.getContext();
        m a2 = m.a();
        bo a3 = bo.a(context, attributeSet, j.AppCompatTextHelper, i2, 0);
        int g2 = a3.g(j.AppCompatTextHelper_android_textAppearance, -1);
        if (a3.g(j.AppCompatTextHelper_android_drawableLeft)) {
            this.f1356b = a(context, a2, a3.g(j.AppCompatTextHelper_android_drawableLeft, 0));
        }
        if (a3.g(j.AppCompatTextHelper_android_drawableTop)) {
            this.c = a(context, a2, a3.g(j.AppCompatTextHelper_android_drawableTop, 0));
        }
        if (a3.g(j.AppCompatTextHelper_android_drawableRight)) {
            this.d = a(context, a2, a3.g(j.AppCompatTextHelper_android_drawableRight, 0));
        }
        if (a3.g(j.AppCompatTextHelper_android_drawableBottom)) {
            this.e = a(context, a2, a3.g(j.AppCompatTextHelper_android_drawableBottom, 0));
        }
        a3.a();
        boolean z3 = this.f1355a.getTransformationMethod() instanceof PasswordTransformationMethod;
        if (g2 != -1) {
            bo a4 = bo.a(context, g2, j.TextAppearance);
            if (z3 || !a4.g(j.TextAppearance_textAllCaps)) {
                z = false;
                z2 = false;
            } else {
                z2 = a4.a(j.TextAppearance_textAllCaps, false);
                z = true;
            }
            a(context, a4);
            if (VERSION.SDK_INT < 23) {
                colorStateList2 = a4.g(j.TextAppearance_android_textColor) ? a4.e(j.TextAppearance_android_textColor) : null;
                colorStateList = a4.g(j.TextAppearance_android_textColorHint) ? a4.e(j.TextAppearance_android_textColorHint) : null;
                if (a4.g(j.TextAppearance_android_textColorLink)) {
                    colorStateList3 = a4.e(j.TextAppearance_android_textColorLink);
                }
            } else {
                colorStateList = null;
                colorStateList2 = null;
            }
            a4.a();
        } else {
            colorStateList = null;
            colorStateList2 = null;
            z = false;
            z2 = false;
        }
        bo a5 = bo.a(context, attributeSet, j.TextAppearance, i2, 0);
        if (!z3 && a5.g(j.TextAppearance_textAllCaps)) {
            z2 = a5.a(j.TextAppearance_textAllCaps, false);
            z = true;
        }
        if (VERSION.SDK_INT < 23) {
            if (a5.g(j.TextAppearance_android_textColor)) {
                colorStateList2 = a5.e(j.TextAppearance_android_textColor);
            }
            if (a5.g(j.TextAppearance_android_textColorHint)) {
                colorStateList = a5.e(j.TextAppearance_android_textColorHint);
            }
            if (a5.g(j.TextAppearance_android_textColorLink)) {
                colorStateList3 = a5.e(j.TextAppearance_android_textColorLink);
            }
        }
        a(context, a5);
        a5.a();
        if (colorStateList2 != null) {
            this.f1355a.setTextColor(colorStateList2);
        }
        if (colorStateList != null) {
            this.f1355a.setHintTextColor(colorStateList);
        }
        if (colorStateList3 != null) {
            this.f1355a.setLinkTextColor(colorStateList3);
        }
        if (!z3 && z) {
            a(z2);
        }
        if (this.h != null) {
            this.f1355a.setTypeface(this.h, this.g);
        }
        this.f.a(attributeSet, i2);
        if (b.f832a && this.f.a() != 0) {
            int[] e2 = this.f.e();
            if (e2.length <= 0) {
                return;
            }
            if (((float) this.f1355a.getAutoSizeStepGranularity()) != -1.0f) {
                this.f1355a.setAutoSizeTextTypeUniformWithConfiguration(this.f.c(), this.f.d(), this.f.b(), 0);
            } else {
                this.f1355a.setAutoSizeTextTypeUniformWithPresetSizes(e2, 0);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(boolean z) {
        this.f1355a.setAllCaps(z);
    }

    /* access modifiers changed from: 0000 */
    public void a(boolean z, int i2, int i3, int i4, int i5) {
        if (!b.f832a) {
            b();
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(int[] iArr, int i2) throws IllegalArgumentException {
        this.f.a(iArr, i2);
    }

    /* access modifiers changed from: 0000 */
    public void b() {
        this.f.f();
    }

    /* access modifiers changed from: 0000 */
    public boolean c() {
        return this.f.g();
    }

    /* access modifiers changed from: 0000 */
    public int d() {
        return this.f.a();
    }

    /* access modifiers changed from: 0000 */
    public int e() {
        return this.f.b();
    }

    /* access modifiers changed from: 0000 */
    public int f() {
        return this.f.c();
    }

    /* access modifiers changed from: 0000 */
    public int g() {
        return this.f.d();
    }

    /* access modifiers changed from: 0000 */
    public int[] h() {
        return this.f.e();
    }
}
