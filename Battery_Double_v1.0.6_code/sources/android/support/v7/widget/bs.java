package android.support.v7.widget;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.Resources;
import android.graphics.Rect;
import android.support.v7.a.a.d;
import android.support.v7.a.a.f;
import android.support.v7.a.a.g;
import android.support.v7.a.a.i;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.TextView;
import com.hmatalonga.greenhub.Config;

class bs {

    /* renamed from: a reason: collision with root package name */
    private final Context f1276a;

    /* renamed from: b reason: collision with root package name */
    private final View f1277b;
    private final TextView c;
    private final LayoutParams d = new LayoutParams();
    private final Rect e = new Rect();
    private final int[] f = new int[2];
    private final int[] g = new int[2];

    bs(Context context) {
        this.f1276a = context;
        this.f1277b = LayoutInflater.from(this.f1276a).inflate(g.tooltip, null);
        this.c = (TextView) this.f1277b.findViewById(f.message);
        this.d.setTitle(getClass().getSimpleName());
        this.d.packageName = this.f1276a.getPackageName();
        this.d.type = Config.NOTIFICATION_BATTERY_FULL;
        this.d.width = -2;
        this.d.height = -2;
        this.d.format = -3;
        this.d.windowAnimations = i.Animation_AppCompat_Tooltip;
        this.d.flags = 24;
    }

    private static View a(View view) {
        for (Context context = view.getContext(); context instanceof ContextWrapper; context = ((ContextWrapper) context).getBaseContext()) {
            if (context instanceof Activity) {
                return ((Activity) context).getWindow().getDecorView();
            }
        }
        return view.getRootView();
    }

    private void a(View view, int i, int i2, boolean z, LayoutParams layoutParams) {
        int height;
        int i3;
        int dimensionPixelOffset = this.f1276a.getResources().getDimensionPixelOffset(d.tooltip_precise_anchor_threshold);
        if (view.getWidth() < dimensionPixelOffset) {
            i = view.getWidth() / 2;
        }
        if (view.getHeight() >= dimensionPixelOffset) {
            int dimensionPixelOffset2 = this.f1276a.getResources().getDimensionPixelOffset(d.tooltip_precise_anchor_extra_offset);
            height = i2 + dimensionPixelOffset2;
            i3 = i2 - dimensionPixelOffset2;
        } else {
            height = view.getHeight();
            i3 = 0;
        }
        layoutParams.gravity = 49;
        int dimensionPixelOffset3 = this.f1276a.getResources().getDimensionPixelOffset(z ? d.tooltip_y_offset_touch : d.tooltip_y_offset_non_touch);
        View a2 = a(view);
        if (a2 == null) {
            Log.e("TooltipPopup", "Cannot find app view");
            return;
        }
        a2.getWindowVisibleDisplayFrame(this.e);
        if (this.e.left < 0 && this.e.top < 0) {
            Resources resources = this.f1276a.getResources();
            int identifier = resources.getIdentifier("status_bar_height", "dimen", "android");
            int i4 = identifier != 0 ? resources.getDimensionPixelSize(identifier) : 0;
            DisplayMetrics displayMetrics = resources.getDisplayMetrics();
            this.e.set(0, i4, displayMetrics.widthPixels, displayMetrics.heightPixels);
        }
        a2.getLocationOnScreen(this.g);
        view.getLocationOnScreen(this.f);
        int[] iArr = this.f;
        iArr[0] = iArr[0] - this.g[0];
        int[] iArr2 = this.f;
        iArr2[1] = iArr2[1] - this.g[1];
        layoutParams.x = (this.f[0] + i) - (this.e.width() / 2);
        int makeMeasureSpec = MeasureSpec.makeMeasureSpec(0, 0);
        this.f1277b.measure(makeMeasureSpec, makeMeasureSpec);
        int measuredHeight = this.f1277b.getMeasuredHeight();
        int i5 = ((i3 + this.f[1]) - dimensionPixelOffset3) - measuredHeight;
        int i6 = height + this.f[1] + dimensionPixelOffset3;
        if (z) {
            if (i5 >= 0) {
                layoutParams.y = i5;
            } else {
                layoutParams.y = i6;
            }
        } else if (measuredHeight + i6 <= this.e.height()) {
            layoutParams.y = i6;
        } else {
            layoutParams.y = i5;
        }
    }

    /* access modifiers changed from: 0000 */
    public void a() {
        if (b()) {
            ((WindowManager) this.f1276a.getSystemService("window")).removeView(this.f1277b);
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(View view, int i, int i2, boolean z, CharSequence charSequence) {
        if (b()) {
            a();
        }
        this.c.setText(charSequence);
        a(view, i, i2, z, this.d);
        ((WindowManager) this.f1276a.getSystemService("window")).addView(this.f1277b, this.d);
    }

    /* access modifiers changed from: 0000 */
    public boolean b() {
        return this.f1277b.getParent() != null;
    }
}
