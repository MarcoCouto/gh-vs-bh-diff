package android.support.v7.widget;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.Resources.NotFoundException;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.support.v7.app.e;
import java.lang.ref.WeakReference;

public class bt extends Resources {

    /* renamed from: a reason: collision with root package name */
    private final WeakReference<Context> f1278a;

    public bt(Context context, Resources resources) {
        super(resources.getAssets(), resources.getDisplayMetrics(), resources.getConfiguration());
        this.f1278a = new WeakReference<>(context);
    }

    public static boolean a() {
        return e.k() && VERSION.SDK_INT <= 20;
    }

    /* access modifiers changed from: 0000 */
    public final Drawable a(int i) {
        return super.getDrawable(i);
    }

    public Drawable getDrawable(int i) throws NotFoundException {
        Context context = (Context) this.f1278a.get();
        return context != null ? m.a().a(context, this, i) : super.getDrawable(i);
    }
}
