package android.support.v7.widget;

import android.content.res.ColorStateList;
import android.graphics.Canvas;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.support.v4.c.a.a;
import android.support.v4.i.t;
import android.support.v7.a.a.j;
import android.util.AttributeSet;
import android.widget.SeekBar;

class x extends t {

    /* renamed from: a reason: collision with root package name */
    private final SeekBar f1340a;

    /* renamed from: b reason: collision with root package name */
    private Drawable f1341b;
    private ColorStateList c = null;
    private Mode d = null;
    private boolean e = false;
    private boolean f = false;

    x(SeekBar seekBar) {
        super(seekBar);
        this.f1340a = seekBar;
    }

    private void d() {
        if (this.f1341b == null) {
            return;
        }
        if (this.e || this.f) {
            this.f1341b = a.f(this.f1341b.mutate());
            if (this.e) {
                a.a(this.f1341b, this.c);
            }
            if (this.f) {
                a.a(this.f1341b, this.d);
            }
            if (this.f1341b.isStateful()) {
                this.f1341b.setState(this.f1340a.getDrawableState());
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(Canvas canvas) {
        int i = 1;
        if (this.f1341b != null) {
            int max = this.f1340a.getMax();
            if (max > 1) {
                int intrinsicWidth = this.f1341b.getIntrinsicWidth();
                int intrinsicHeight = this.f1341b.getIntrinsicHeight();
                int i2 = intrinsicWidth >= 0 ? intrinsicWidth / 2 : 1;
                if (intrinsicHeight >= 0) {
                    i = intrinsicHeight / 2;
                }
                this.f1341b.setBounds(-i2, -i, i2, i);
                float width = ((float) ((this.f1340a.getWidth() - this.f1340a.getPaddingLeft()) - this.f1340a.getPaddingRight())) / ((float) max);
                int save = canvas.save();
                canvas.translate((float) this.f1340a.getPaddingLeft(), (float) (this.f1340a.getHeight() / 2));
                for (int i3 = 0; i3 <= max; i3++) {
                    this.f1341b.draw(canvas);
                    canvas.translate(width, 0.0f);
                }
                canvas.restoreToCount(save);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(Drawable drawable) {
        if (this.f1341b != null) {
            this.f1341b.setCallback(null);
        }
        this.f1341b = drawable;
        if (drawable != null) {
            drawable.setCallback(this.f1340a);
            a.b(drawable, t.e(this.f1340a));
            if (drawable.isStateful()) {
                drawable.setState(this.f1340a.getDrawableState());
            }
            d();
        }
        this.f1340a.invalidate();
    }

    /* access modifiers changed from: 0000 */
    public void a(AttributeSet attributeSet, int i) {
        super.a(attributeSet, i);
        bo a2 = bo.a(this.f1340a.getContext(), attributeSet, j.AppCompatSeekBar, i, 0);
        Drawable b2 = a2.b(j.AppCompatSeekBar_android_thumb);
        if (b2 != null) {
            this.f1340a.setThumb(b2);
        }
        a(a2.a(j.AppCompatSeekBar_tickMark));
        if (a2.g(j.AppCompatSeekBar_tickMarkTintMode)) {
            this.d = am.a(a2.a(j.AppCompatSeekBar_tickMarkTintMode, -1), this.d);
            this.f = true;
        }
        if (a2.g(j.AppCompatSeekBar_tickMarkTint)) {
            this.c = a2.e(j.AppCompatSeekBar_tickMarkTint);
            this.e = true;
        }
        a2.a();
        d();
    }

    /* access modifiers changed from: 0000 */
    public void b() {
        if (this.f1341b != null) {
            this.f1341b.jumpToCurrentState();
        }
    }

    /* access modifiers changed from: 0000 */
    public void c() {
        Drawable drawable = this.f1341b;
        if (drawable != null && drawable.isStateful() && drawable.setState(this.f1340a.getDrawableState())) {
            this.f1340a.invalidateDrawable(drawable);
        }
    }
}
