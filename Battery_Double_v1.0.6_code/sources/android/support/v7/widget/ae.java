package android.support.v7.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.view.View;

class ae implements ah {
    ae() {
    }

    private bc j(ag agVar) {
        return (bc) agVar.c();
    }

    public float a(ag agVar) {
        return j(agVar).a();
    }

    public void a() {
    }

    public void a(ag agVar, float f) {
        j(agVar).a(f);
    }

    public void a(ag agVar, Context context, ColorStateList colorStateList, float f, float f2, float f3) {
        agVar.a(new bc(colorStateList, f));
        View d = agVar.d();
        d.setClipToOutline(true);
        d.setElevation(f2);
        b(agVar, f3);
    }

    public void a(ag agVar, ColorStateList colorStateList) {
        j(agVar).a(colorStateList);
    }

    public float b(ag agVar) {
        return d(agVar) * 2.0f;
    }

    public void b(ag agVar, float f) {
        j(agVar).a(f, agVar.a(), agVar.b());
        f(agVar);
    }

    public float c(ag agVar) {
        return d(agVar) * 2.0f;
    }

    public void c(ag agVar, float f) {
        agVar.d().setElevation(f);
    }

    public float d(ag agVar) {
        return j(agVar).b();
    }

    public float e(ag agVar) {
        return agVar.d().getElevation();
    }

    public void f(ag agVar) {
        if (!agVar.a()) {
            agVar.a(0, 0, 0, 0);
            return;
        }
        float a2 = a(agVar);
        float d = d(agVar);
        int ceil = (int) Math.ceil((double) bd.b(a2, d, agVar.b()));
        int ceil2 = (int) Math.ceil((double) bd.a(a2, d, agVar.b()));
        agVar.a(ceil, ceil2, ceil, ceil2);
    }

    public void g(ag agVar) {
        b(agVar, a(agVar));
    }

    public void h(ag agVar) {
        b(agVar, a(agVar));
    }

    public ColorStateList i(ag agVar) {
        return j(agVar).c();
    }
}
