package android.support.v7.widget;

import android.content.Context;
import android.graphics.Rect;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.v4.i.a.b.C0017b;
import android.support.v7.widget.RecyclerView.h;
import android.support.v7.widget.RecyclerView.i;
import android.support.v7.widget.RecyclerView.o;
import android.support.v7.widget.RecyclerView.t;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.accessibility.AccessibilityEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.List;

public class StaggeredGridLayoutManager extends h {
    private d A;
    private int B;
    private final Rect C = new Rect();
    private final a D = new a();
    private boolean E = false;
    private boolean F = true;
    private int[] G;
    private final Runnable H = new Runnable() {
        public void run() {
            StaggeredGridLayoutManager.this.f();
        }
    };

    /* renamed from: a reason: collision with root package name */
    e[] f1114a;

    /* renamed from: b reason: collision with root package name */
    az f1115b;
    az c;
    boolean d = false;
    boolean e = false;
    int f = -1;
    int g = Integer.MIN_VALUE;
    c h = new c();
    private int i = -1;
    private int j;
    private int k;
    private final as l;
    private BitSet m;
    private int n = 2;
    private boolean o;
    private boolean z;

    class a {

        /* renamed from: a reason: collision with root package name */
        int f1117a;

        /* renamed from: b reason: collision with root package name */
        int f1118b;
        boolean c;
        boolean d;
        boolean e;
        int[] f;

        a() {
            a();
        }

        /* access modifiers changed from: 0000 */
        public void a() {
            this.f1117a = -1;
            this.f1118b = Integer.MIN_VALUE;
            this.c = false;
            this.d = false;
            this.e = false;
            if (this.f != null) {
                Arrays.fill(this.f, -1);
            }
        }

        /* access modifiers changed from: 0000 */
        public void a(int i) {
            if (this.c) {
                this.f1118b = StaggeredGridLayoutManager.this.f1115b.d() - i;
            } else {
                this.f1118b = StaggeredGridLayoutManager.this.f1115b.c() + i;
            }
        }

        /* access modifiers changed from: 0000 */
        public void a(e[] eVarArr) {
            int length = eVarArr.length;
            if (this.f == null || this.f.length < length) {
                this.f = new int[StaggeredGridLayoutManager.this.f1114a.length];
            }
            for (int i = 0; i < length; i++) {
                this.f[i] = eVarArr[i].a(Integer.MIN_VALUE);
            }
        }

        /* access modifiers changed from: 0000 */
        public void b() {
            this.f1118b = this.c ? StaggeredGridLayoutManager.this.f1115b.d() : StaggeredGridLayoutManager.this.f1115b.c();
        }
    }

    public static class b extends i {

        /* renamed from: a reason: collision with root package name */
        e f1119a;

        /* renamed from: b reason: collision with root package name */
        boolean f1120b;

        public b(int i, int i2) {
            super(i, i2);
        }

        public b(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }

        public b(LayoutParams layoutParams) {
            super(layoutParams);
        }

        public b(MarginLayoutParams marginLayoutParams) {
            super(marginLayoutParams);
        }

        public boolean a() {
            return this.f1120b;
        }

        public final int b() {
            if (this.f1119a == null) {
                return -1;
            }
            return this.f1119a.e;
        }
    }

    static class c {

        /* renamed from: a reason: collision with root package name */
        int[] f1121a;

        /* renamed from: b reason: collision with root package name */
        List<a> f1122b;

        static class a implements Parcelable {
            public static final Creator<a> CREATOR = new Creator<a>() {
                /* renamed from: a */
                public a createFromParcel(Parcel parcel) {
                    return new a(parcel);
                }

                /* renamed from: a */
                public a[] newArray(int i) {
                    return new a[i];
                }
            };

            /* renamed from: a reason: collision with root package name */
            int f1123a;

            /* renamed from: b reason: collision with root package name */
            int f1124b;
            int[] c;
            boolean d;

            a() {
            }

            a(Parcel parcel) {
                boolean z = true;
                this.f1123a = parcel.readInt();
                this.f1124b = parcel.readInt();
                if (parcel.readInt() != 1) {
                    z = false;
                }
                this.d = z;
                int readInt = parcel.readInt();
                if (readInt > 0) {
                    this.c = new int[readInt];
                    parcel.readIntArray(this.c);
                }
            }

            /* access modifiers changed from: 0000 */
            public int a(int i) {
                if (this.c == null) {
                    return 0;
                }
                return this.c[i];
            }

            public int describeContents() {
                return 0;
            }

            public String toString() {
                return "FullSpanItem{mPosition=" + this.f1123a + ", mGapDir=" + this.f1124b + ", mHasUnwantedGapAfter=" + this.d + ", mGapPerSpan=" + Arrays.toString(this.c) + '}';
            }

            public void writeToParcel(Parcel parcel, int i) {
                parcel.writeInt(this.f1123a);
                parcel.writeInt(this.f1124b);
                parcel.writeInt(this.d ? 1 : 0);
                if (this.c == null || this.c.length <= 0) {
                    parcel.writeInt(0);
                    return;
                }
                parcel.writeInt(this.c.length);
                parcel.writeIntArray(this.c);
            }
        }

        c() {
        }

        private void c(int i, int i2) {
            if (this.f1122b != null) {
                int i3 = i + i2;
                for (int size = this.f1122b.size() - 1; size >= 0; size--) {
                    a aVar = (a) this.f1122b.get(size);
                    if (aVar.f1123a >= i) {
                        if (aVar.f1123a < i3) {
                            this.f1122b.remove(size);
                        } else {
                            aVar.f1123a -= i2;
                        }
                    }
                }
            }
        }

        private void d(int i, int i2) {
            if (this.f1122b != null) {
                for (int size = this.f1122b.size() - 1; size >= 0; size--) {
                    a aVar = (a) this.f1122b.get(size);
                    if (aVar.f1123a >= i) {
                        aVar.f1123a += i2;
                    }
                }
            }
        }

        private int g(int i) {
            if (this.f1122b == null) {
                return -1;
            }
            a f = f(i);
            if (f != null) {
                this.f1122b.remove(f);
            }
            int size = this.f1122b.size();
            int i2 = 0;
            while (true) {
                if (i2 >= size) {
                    i2 = -1;
                    break;
                } else if (((a) this.f1122b.get(i2)).f1123a >= i) {
                    break;
                } else {
                    i2++;
                }
            }
            if (i2 == -1) {
                return -1;
            }
            a aVar = (a) this.f1122b.get(i2);
            this.f1122b.remove(i2);
            return aVar.f1123a;
        }

        /* access modifiers changed from: 0000 */
        public int a(int i) {
            if (this.f1122b != null) {
                for (int size = this.f1122b.size() - 1; size >= 0; size--) {
                    if (((a) this.f1122b.get(size)).f1123a >= i) {
                        this.f1122b.remove(size);
                    }
                }
            }
            return b(i);
        }

        public a a(int i, int i2, int i3, boolean z) {
            if (this.f1122b == null) {
                return null;
            }
            int size = this.f1122b.size();
            for (int i4 = 0; i4 < size; i4++) {
                a aVar = (a) this.f1122b.get(i4);
                if (aVar.f1123a >= i2) {
                    return null;
                }
                if (aVar.f1123a >= i) {
                    if (i3 == 0 || aVar.f1124b == i3) {
                        return aVar;
                    }
                    if (z && aVar.d) {
                        return aVar;
                    }
                }
            }
            return null;
        }

        /* access modifiers changed from: 0000 */
        public void a() {
            if (this.f1121a != null) {
                Arrays.fill(this.f1121a, -1);
            }
            this.f1122b = null;
        }

        /* access modifiers changed from: 0000 */
        public void a(int i, int i2) {
            if (this.f1121a != null && i < this.f1121a.length) {
                e(i + i2);
                System.arraycopy(this.f1121a, i + i2, this.f1121a, i, (this.f1121a.length - i) - i2);
                Arrays.fill(this.f1121a, this.f1121a.length - i2, this.f1121a.length, -1);
                c(i, i2);
            }
        }

        /* access modifiers changed from: 0000 */
        public void a(int i, e eVar) {
            e(i);
            this.f1121a[i] = eVar.e;
        }

        public void a(a aVar) {
            if (this.f1122b == null) {
                this.f1122b = new ArrayList();
            }
            int size = this.f1122b.size();
            for (int i = 0; i < size; i++) {
                a aVar2 = (a) this.f1122b.get(i);
                if (aVar2.f1123a == aVar.f1123a) {
                    this.f1122b.remove(i);
                }
                if (aVar2.f1123a >= aVar.f1123a) {
                    this.f1122b.add(i, aVar);
                    return;
                }
            }
            this.f1122b.add(aVar);
        }

        /* access modifiers changed from: 0000 */
        public int b(int i) {
            if (this.f1121a == null || i >= this.f1121a.length) {
                return -1;
            }
            int g = g(i);
            if (g == -1) {
                Arrays.fill(this.f1121a, i, this.f1121a.length, -1);
                return this.f1121a.length;
            }
            Arrays.fill(this.f1121a, i, g + 1, -1);
            return g + 1;
        }

        /* access modifiers changed from: 0000 */
        public void b(int i, int i2) {
            if (this.f1121a != null && i < this.f1121a.length) {
                e(i + i2);
                System.arraycopy(this.f1121a, i, this.f1121a, i + i2, (this.f1121a.length - i) - i2);
                Arrays.fill(this.f1121a, i, i + i2, -1);
                d(i, i2);
            }
        }

        /* access modifiers changed from: 0000 */
        public int c(int i) {
            if (this.f1121a == null || i >= this.f1121a.length) {
                return -1;
            }
            return this.f1121a[i];
        }

        /* access modifiers changed from: 0000 */
        public int d(int i) {
            int length = this.f1121a.length;
            while (length <= i) {
                length *= 2;
            }
            return length;
        }

        /* access modifiers changed from: 0000 */
        public void e(int i) {
            if (this.f1121a == null) {
                this.f1121a = new int[(Math.max(i, 10) + 1)];
                Arrays.fill(this.f1121a, -1);
            } else if (i >= this.f1121a.length) {
                int[] iArr = this.f1121a;
                this.f1121a = new int[d(i)];
                System.arraycopy(iArr, 0, this.f1121a, 0, iArr.length);
                Arrays.fill(this.f1121a, iArr.length, this.f1121a.length, -1);
            }
        }

        public a f(int i) {
            if (this.f1122b == null) {
                return null;
            }
            for (int size = this.f1122b.size() - 1; size >= 0; size--) {
                a aVar = (a) this.f1122b.get(size);
                if (aVar.f1123a == i) {
                    return aVar;
                }
            }
            return null;
        }
    }

    public static class d implements Parcelable {
        public static final Creator<d> CREATOR = new Creator<d>() {
            /* renamed from: a */
            public d createFromParcel(Parcel parcel) {
                return new d(parcel);
            }

            /* renamed from: a */
            public d[] newArray(int i) {
                return new d[i];
            }
        };

        /* renamed from: a reason: collision with root package name */
        int f1125a;

        /* renamed from: b reason: collision with root package name */
        int f1126b;
        int c;
        int[] d;
        int e;
        int[] f;
        List<a> g;
        boolean h;
        boolean i;
        boolean j;

        public d() {
        }

        d(Parcel parcel) {
            boolean z = true;
            this.f1125a = parcel.readInt();
            this.f1126b = parcel.readInt();
            this.c = parcel.readInt();
            if (this.c > 0) {
                this.d = new int[this.c];
                parcel.readIntArray(this.d);
            }
            this.e = parcel.readInt();
            if (this.e > 0) {
                this.f = new int[this.e];
                parcel.readIntArray(this.f);
            }
            this.h = parcel.readInt() == 1;
            this.i = parcel.readInt() == 1;
            if (parcel.readInt() != 1) {
                z = false;
            }
            this.j = z;
            this.g = parcel.readArrayList(a.class.getClassLoader());
        }

        public d(d dVar) {
            this.c = dVar.c;
            this.f1125a = dVar.f1125a;
            this.f1126b = dVar.f1126b;
            this.d = dVar.d;
            this.e = dVar.e;
            this.f = dVar.f;
            this.h = dVar.h;
            this.i = dVar.i;
            this.j = dVar.j;
            this.g = dVar.g;
        }

        /* access modifiers changed from: 0000 */
        public void a() {
            this.d = null;
            this.c = 0;
            this.e = 0;
            this.f = null;
            this.g = null;
        }

        /* access modifiers changed from: 0000 */
        public void b() {
            this.d = null;
            this.c = 0;
            this.f1125a = -1;
            this.f1126b = -1;
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel parcel, int i2) {
            int i3 = 1;
            parcel.writeInt(this.f1125a);
            parcel.writeInt(this.f1126b);
            parcel.writeInt(this.c);
            if (this.c > 0) {
                parcel.writeIntArray(this.d);
            }
            parcel.writeInt(this.e);
            if (this.e > 0) {
                parcel.writeIntArray(this.f);
            }
            parcel.writeInt(this.h ? 1 : 0);
            parcel.writeInt(this.i ? 1 : 0);
            if (!this.j) {
                i3 = 0;
            }
            parcel.writeInt(i3);
            parcel.writeList(this.g);
        }
    }

    class e {

        /* renamed from: a reason: collision with root package name */
        ArrayList<View> f1127a = new ArrayList<>();

        /* renamed from: b reason: collision with root package name */
        int f1128b = Integer.MIN_VALUE;
        int c = Integer.MIN_VALUE;
        int d = 0;
        final int e;

        e(int i) {
            this.e = i;
        }

        /* access modifiers changed from: 0000 */
        public int a(int i) {
            if (this.f1128b != Integer.MIN_VALUE) {
                return this.f1128b;
            }
            if (this.f1127a.size() == 0) {
                return i;
            }
            a();
            return this.f1128b;
        }

        /* access modifiers changed from: 0000 */
        public int a(int i, int i2, boolean z) {
            return a(i, i2, false, false, z);
        }

        /* access modifiers changed from: 0000 */
        public int a(int i, int i2, boolean z, boolean z2, boolean z3) {
            int c2 = StaggeredGridLayoutManager.this.f1115b.c();
            int d2 = StaggeredGridLayoutManager.this.f1115b.d();
            int i3 = i2 > i ? 1 : -1;
            while (i != i2) {
                View view = (View) this.f1127a.get(i);
                int a2 = StaggeredGridLayoutManager.this.f1115b.a(view);
                int b2 = StaggeredGridLayoutManager.this.f1115b.b(view);
                boolean z4 = z3 ? a2 <= d2 : a2 < d2;
                boolean z5 = z3 ? b2 >= c2 : b2 > c2;
                if (z4 && z5) {
                    if (!z || !z2) {
                        if (z2) {
                            return StaggeredGridLayoutManager.this.d(view);
                        }
                        if (a2 < c2 || b2 > d2) {
                            return StaggeredGridLayoutManager.this.d(view);
                        }
                    } else if (a2 >= c2 && b2 <= d2) {
                        return StaggeredGridLayoutManager.this.d(view);
                    }
                }
                i += i3;
            }
            return -1;
        }

        public View a(int i, int i2) {
            View view = null;
            if (i2 == -1) {
                int size = this.f1127a.size();
                int i3 = 0;
                while (i3 < size) {
                    View view2 = (View) this.f1127a.get(i3);
                    if ((StaggeredGridLayoutManager.this.d && StaggeredGridLayoutManager.this.d(view2) <= i) || ((!StaggeredGridLayoutManager.this.d && StaggeredGridLayoutManager.this.d(view2) >= i) || !view2.hasFocusable())) {
                        break;
                    }
                    i3++;
                    view = view2;
                }
                return view;
            }
            int size2 = this.f1127a.size() - 1;
            while (size2 >= 0) {
                View view3 = (View) this.f1127a.get(size2);
                if (StaggeredGridLayoutManager.this.d && StaggeredGridLayoutManager.this.d(view3) >= i) {
                    break;
                } else if (StaggeredGridLayoutManager.this.d || StaggeredGridLayoutManager.this.d(view3) > i) {
                    if (!view3.hasFocusable()) {
                        break;
                    }
                    size2--;
                    view = view3;
                } else {
                    return view;
                }
            }
            return view;
        }

        /* access modifiers changed from: 0000 */
        public void a() {
            View view = (View) this.f1127a.get(0);
            b c2 = c(view);
            this.f1128b = StaggeredGridLayoutManager.this.f1115b.a(view);
            if (c2.f1120b) {
                a f2 = StaggeredGridLayoutManager.this.h.f(c2.f());
                if (f2 != null && f2.f1124b == -1) {
                    this.f1128b -= f2.a(this.e);
                }
            }
        }

        /* access modifiers changed from: 0000 */
        public void a(View view) {
            b c2 = c(view);
            c2.f1119a = this;
            this.f1127a.add(0, view);
            this.f1128b = Integer.MIN_VALUE;
            if (this.f1127a.size() == 1) {
                this.c = Integer.MIN_VALUE;
            }
            if (c2.d() || c2.e()) {
                this.d += StaggeredGridLayoutManager.this.f1115b.e(view);
            }
        }

        /* access modifiers changed from: 0000 */
        public void a(boolean z, int i) {
            int a2 = z ? b(Integer.MIN_VALUE) : a(Integer.MIN_VALUE);
            e();
            if (a2 != Integer.MIN_VALUE) {
                if (z && a2 < StaggeredGridLayoutManager.this.f1115b.d()) {
                    return;
                }
                if (z || a2 <= StaggeredGridLayoutManager.this.f1115b.c()) {
                    if (i != Integer.MIN_VALUE) {
                        a2 += i;
                    }
                    this.c = a2;
                    this.f1128b = a2;
                }
            }
        }

        /* access modifiers changed from: 0000 */
        public int b() {
            if (this.f1128b != Integer.MIN_VALUE) {
                return this.f1128b;
            }
            a();
            return this.f1128b;
        }

        /* access modifiers changed from: 0000 */
        public int b(int i) {
            if (this.c != Integer.MIN_VALUE) {
                return this.c;
            }
            if (this.f1127a.size() == 0) {
                return i;
            }
            c();
            return this.c;
        }

        /* access modifiers changed from: 0000 */
        public void b(View view) {
            b c2 = c(view);
            c2.f1119a = this;
            this.f1127a.add(view);
            this.c = Integer.MIN_VALUE;
            if (this.f1127a.size() == 1) {
                this.f1128b = Integer.MIN_VALUE;
            }
            if (c2.d() || c2.e()) {
                this.d += StaggeredGridLayoutManager.this.f1115b.e(view);
            }
        }

        /* access modifiers changed from: 0000 */
        public b c(View view) {
            return (b) view.getLayoutParams();
        }

        /* access modifiers changed from: 0000 */
        public void c() {
            View view = (View) this.f1127a.get(this.f1127a.size() - 1);
            b c2 = c(view);
            this.c = StaggeredGridLayoutManager.this.f1115b.b(view);
            if (c2.f1120b) {
                a f2 = StaggeredGridLayoutManager.this.h.f(c2.f());
                if (f2 != null && f2.f1124b == 1) {
                    this.c = f2.a(this.e) + this.c;
                }
            }
        }

        /* access modifiers changed from: 0000 */
        public void c(int i) {
            this.f1128b = i;
            this.c = i;
        }

        /* access modifiers changed from: 0000 */
        public int d() {
            if (this.c != Integer.MIN_VALUE) {
                return this.c;
            }
            c();
            return this.c;
        }

        /* access modifiers changed from: 0000 */
        public void d(int i) {
            if (this.f1128b != Integer.MIN_VALUE) {
                this.f1128b += i;
            }
            if (this.c != Integer.MIN_VALUE) {
                this.c += i;
            }
        }

        /* access modifiers changed from: 0000 */
        public void e() {
            this.f1127a.clear();
            f();
            this.d = 0;
        }

        /* access modifiers changed from: 0000 */
        public void f() {
            this.f1128b = Integer.MIN_VALUE;
            this.c = Integer.MIN_VALUE;
        }

        /* access modifiers changed from: 0000 */
        public void g() {
            int size = this.f1127a.size();
            View view = (View) this.f1127a.remove(size - 1);
            b c2 = c(view);
            c2.f1119a = null;
            if (c2.d() || c2.e()) {
                this.d -= StaggeredGridLayoutManager.this.f1115b.e(view);
            }
            if (size == 1) {
                this.f1128b = Integer.MIN_VALUE;
            }
            this.c = Integer.MIN_VALUE;
        }

        /* access modifiers changed from: 0000 */
        public void h() {
            View view = (View) this.f1127a.remove(0);
            b c2 = c(view);
            c2.f1119a = null;
            if (this.f1127a.size() == 0) {
                this.c = Integer.MIN_VALUE;
            }
            if (c2.d() || c2.e()) {
                this.d -= StaggeredGridLayoutManager.this.f1115b.e(view);
            }
            this.f1128b = Integer.MIN_VALUE;
        }

        public int i() {
            return this.d;
        }

        public int j() {
            return StaggeredGridLayoutManager.this.d ? a(this.f1127a.size() - 1, -1, true) : a(0, this.f1127a.size(), true);
        }

        public int k() {
            return StaggeredGridLayoutManager.this.d ? a(0, this.f1127a.size(), true) : a(this.f1127a.size() - 1, -1, true);
        }
    }

    public StaggeredGridLayoutManager(int i2, int i3) {
        boolean z2 = true;
        this.j = i3;
        a(i2);
        if (this.n == 0) {
            z2 = false;
        }
        c(z2);
        this.l = new as();
        L();
    }

    public StaggeredGridLayoutManager(Context context, AttributeSet attributeSet, int i2, int i3) {
        boolean z2 = true;
        android.support.v7.widget.RecyclerView.h.b a2 = a(context, attributeSet, i2, i3);
        b(a2.f1084a);
        a(a2.f1085b);
        a(a2.c);
        if (this.n == 0) {
            z2 = false;
        }
        c(z2);
        this.l = new as();
        L();
    }

    private void L() {
        this.f1115b = az.a(this, this.j);
        this.c = az.a(this, 1 - this.j);
    }

    private void M() {
        boolean z2 = true;
        if (this.j == 1 || !i()) {
            this.e = this.d;
            return;
        }
        if (this.d) {
            z2 = false;
        }
        this.e = z2;
    }

    private void N() {
        float max;
        if (this.c.h() != 1073741824) {
            float f2 = 0.0f;
            int u = u();
            int i2 = 0;
            while (i2 < u) {
                View h2 = h(i2);
                float e2 = (float) this.c.e(h2);
                if (e2 < f2) {
                    max = f2;
                } else {
                    max = Math.max(f2, ((b) h2.getLayoutParams()).a() ? (1.0f * e2) / ((float) this.i) : e2);
                }
                i2++;
                f2 = max;
            }
            int i3 = this.k;
            int round = Math.round(((float) this.i) * f2);
            if (this.c.h() == Integer.MIN_VALUE) {
                round = Math.min(round, this.c.f());
            }
            e(round);
            if (this.k != i3) {
                for (int i4 = 0; i4 < u; i4++) {
                    View h3 = h(i4);
                    b bVar = (b) h3.getLayoutParams();
                    if (!bVar.f1120b) {
                        if (!i() || this.j != 1) {
                            int i5 = bVar.f1119a.e * this.k;
                            int i6 = bVar.f1119a.e * i3;
                            if (this.j == 1) {
                                h3.offsetLeftAndRight(i5 - i6);
                            } else {
                                h3.offsetTopAndBottom(i5 - i6);
                            }
                        } else {
                            h3.offsetLeftAndRight(((-((this.i - 1) - bVar.f1119a.e)) * this.k) - ((-((this.i - 1) - bVar.f1119a.e)) * i3));
                        }
                    }
                }
            }
        }
    }

    private int a(o oVar, as asVar, t tVar) {
        e eVar;
        int e2;
        int i2;
        int e3;
        int i3;
        this.m.set(0, this.i, true);
        int i4 = this.l.i ? asVar.e == 1 ? Integer.MAX_VALUE : Integer.MIN_VALUE : asVar.e == 1 ? asVar.g + asVar.f1216b : asVar.f - asVar.f1216b;
        a(asVar.e, i4);
        int c2 = this.e ? this.f1115b.d() : this.f1115b.c();
        boolean z2 = false;
        while (asVar.a(tVar) && (this.l.i || !this.m.isEmpty())) {
            View a2 = asVar.a(oVar);
            b bVar = (b) a2.getLayoutParams();
            int f2 = bVar.f();
            int c3 = this.h.c(f2);
            boolean z3 = c3 == -1;
            if (z3) {
                e a3 = bVar.f1120b ? this.f1114a[0] : a(asVar);
                this.h.a(f2, a3);
                eVar = a3;
            } else {
                eVar = this.f1114a[c3];
            }
            bVar.f1119a = eVar;
            if (asVar.e == 1) {
                b(a2);
            } else {
                b(a2, 0);
            }
            a(a2, bVar, false);
            if (asVar.e == 1) {
                int b2 = bVar.f1120b ? q(c2) : eVar.b(c2);
                i2 = b2 + this.f1115b.e(a2);
                if (!z3 || !bVar.f1120b) {
                    e2 = b2;
                } else {
                    a m2 = m(b2);
                    m2.f1124b = -1;
                    m2.f1123a = f2;
                    this.h.a(m2);
                    e2 = b2;
                }
            } else {
                int a4 = bVar.f1120b ? p(c2) : eVar.a(c2);
                e2 = a4 - this.f1115b.e(a2);
                if (z3 && bVar.f1120b) {
                    a n2 = n(a4);
                    n2.f1124b = 1;
                    n2.f1123a = f2;
                    this.h.a(n2);
                }
                i2 = a4;
            }
            if (bVar.f1120b && asVar.d == -1) {
                if (z3) {
                    this.E = true;
                } else {
                    boolean z4 = asVar.e == 1 ? !l() : !m();
                    if (z4) {
                        a f3 = this.h.f(f2);
                        if (f3 != null) {
                            f3.d = true;
                        }
                        this.E = true;
                    }
                }
            }
            a(a2, bVar, asVar);
            if (!i() || this.j != 1) {
                int c4 = bVar.f1120b ? this.c.c() : (eVar.e * this.k) + this.c.c();
                e3 = c4 + this.c.e(a2);
                i3 = c4;
            } else {
                int d2 = bVar.f1120b ? this.c.d() : this.c.d() - (((this.i - 1) - eVar.e) * this.k);
                i3 = d2 - this.c.e(a2);
                e3 = d2;
            }
            if (this.j == 1) {
                a(a2, i3, e2, e3, i2);
            } else {
                a(a2, e2, i3, i2, e3);
            }
            if (bVar.f1120b) {
                a(this.l.e, i4);
            } else {
                a(eVar, this.l.e, i4);
            }
            a(oVar, this.l);
            if (this.l.h && a2.hasFocusable()) {
                if (bVar.f1120b) {
                    this.m.clear();
                } else {
                    this.m.set(eVar.e, false);
                }
            }
            z2 = true;
        }
        if (!z2) {
            a(oVar, this.l);
        }
        int q = this.l.e == -1 ? this.f1115b.c() - p(this.f1115b.c()) : q(this.f1115b.d()) - this.f1115b.d();
        if (q > 0) {
            return Math.min(asVar.f1216b, q);
        }
        return 0;
    }

    private e a(as asVar) {
        int i2;
        int i3;
        e eVar;
        e eVar2;
        e eVar3 = null;
        int i4 = -1;
        if (s(asVar.e)) {
            i2 = this.i - 1;
            i3 = -1;
        } else {
            i2 = 0;
            i3 = this.i;
            i4 = 1;
        }
        if (asVar.e == 1) {
            int c2 = this.f1115b.c();
            int i5 = i2;
            int i6 = Integer.MAX_VALUE;
            while (i5 != i3) {
                e eVar4 = this.f1114a[i5];
                int b2 = eVar4.b(c2);
                if (b2 < i6) {
                    eVar2 = eVar4;
                } else {
                    b2 = i6;
                    eVar2 = eVar3;
                }
                i5 += i4;
                eVar3 = eVar2;
                i6 = b2;
            }
        } else {
            int d2 = this.f1115b.d();
            int i7 = i2;
            int i8 = Integer.MIN_VALUE;
            while (i7 != i3) {
                e eVar5 = this.f1114a[i7];
                int a2 = eVar5.a(d2);
                if (a2 > i8) {
                    eVar = eVar5;
                } else {
                    a2 = i8;
                    eVar = eVar3;
                }
                i7 += i4;
                eVar3 = eVar;
                i8 = a2;
            }
        }
        return eVar3;
    }

    private void a(int i2, int i3) {
        for (int i4 = 0; i4 < this.i; i4++) {
            if (!this.f1114a[i4].f1127a.isEmpty()) {
                a(this.f1114a[i4], i2, i3);
            }
        }
    }

    private void a(o oVar, int i2) {
        while (u() > 0) {
            View h2 = h(0);
            if (this.f1115b.b(h2) <= i2 && this.f1115b.c(h2) <= i2) {
                b bVar = (b) h2.getLayoutParams();
                if (bVar.f1120b) {
                    int i3 = 0;
                    while (i3 < this.i) {
                        if (this.f1114a[i3].f1127a.size() != 1) {
                            i3++;
                        } else {
                            return;
                        }
                    }
                    for (int i4 = 0; i4 < this.i; i4++) {
                        this.f1114a[i4].h();
                    }
                } else if (bVar.f1119a.f1127a.size() != 1) {
                    bVar.f1119a.h();
                } else {
                    return;
                }
                a(h2, oVar);
            } else {
                return;
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:79:0x012d  */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x013e  */
    /* JADX WARNING: Removed duplicated region for block: B:97:? A[RETURN, SYNTHETIC] */
    private void a(o oVar, t tVar, boolean z2) {
        boolean z3;
        a aVar = this.D;
        if (!(this.A == null && this.f == -1) && tVar.e() == 0) {
            c(oVar);
            aVar.a();
            return;
        }
        boolean z4 = (aVar.e && this.f == -1 && this.A == null) ? false : true;
        if (z4) {
            aVar.a();
            if (this.A != null) {
                a(aVar);
            } else {
                M();
                aVar.c = this.e;
            }
            a(tVar, aVar);
            aVar.e = true;
        }
        if (this.A == null && this.f == -1 && !(aVar.c == this.o && i() == this.z)) {
            this.h.a();
            aVar.d = true;
        }
        if (u() > 0 && (this.A == null || this.A.c < 1)) {
            if (aVar.d) {
                for (int i2 = 0; i2 < this.i; i2++) {
                    this.f1114a[i2].e();
                    if (aVar.f1118b != Integer.MIN_VALUE) {
                        this.f1114a[i2].c(aVar.f1118b);
                    }
                }
            } else if (z4 || this.D.f == null) {
                for (int i3 = 0; i3 < this.i; i3++) {
                    this.f1114a[i3].a(this.e, aVar.f1118b);
                }
                this.D.a(this.f1114a);
            } else {
                for (int i4 = 0; i4 < this.i; i4++) {
                    e eVar = this.f1114a[i4];
                    eVar.e();
                    eVar.c(this.D.f[i4]);
                }
            }
        }
        a(oVar);
        this.l.f1215a = false;
        this.E = false;
        e(this.c.f());
        b(aVar.f1117a, tVar);
        if (aVar.c) {
            l(-1);
            a(oVar, this.l, tVar);
            l(1);
            this.l.c = aVar.f1117a + this.l.d;
            a(oVar, this.l, tVar);
        } else {
            l(1);
            a(oVar, this.l, tVar);
            l(-1);
            this.l.c = aVar.f1117a + this.l.d;
            a(oVar, this.l, tVar);
        }
        N();
        if (u() > 0) {
            if (this.e) {
                b(oVar, tVar, true);
                c(oVar, tVar, false);
            } else {
                c(oVar, tVar, true);
                b(oVar, tVar, false);
            }
        }
        if (z2 && !tVar.a()) {
            if (this.n != 0 && u() > 0 && (this.E || g() != null)) {
                a(this.H);
                if (f()) {
                    z3 = true;
                    if (tVar.a()) {
                        this.D.a();
                    }
                    this.o = aVar.c;
                    this.z = i();
                    if (!z3) {
                        this.D.a();
                        a(oVar, tVar, false);
                        return;
                    }
                    return;
                }
            }
        }
        z3 = false;
        if (tVar.a()) {
        }
        this.o = aVar.c;
        this.z = i();
        if (!z3) {
        }
    }

    private void a(o oVar, as asVar) {
        if (asVar.f1215a && !asVar.i) {
            if (asVar.f1216b == 0) {
                if (asVar.e == -1) {
                    b(oVar, asVar.g);
                } else {
                    a(oVar, asVar.f);
                }
            } else if (asVar.e == -1) {
                int o2 = asVar.f - o(asVar.f);
                b(oVar, o2 < 0 ? asVar.g : asVar.g - Math.min(o2, asVar.f1216b));
            } else {
                int r = r(asVar.g) - asVar.g;
                a(oVar, r < 0 ? asVar.f : Math.min(r, asVar.f1216b) + asVar.f);
            }
        }
    }

    private void a(a aVar) {
        if (this.A.c > 0) {
            if (this.A.c == this.i) {
                for (int i2 = 0; i2 < this.i; i2++) {
                    this.f1114a[i2].e();
                    int i3 = this.A.d[i2];
                    if (i3 != Integer.MIN_VALUE) {
                        i3 = this.A.i ? i3 + this.f1115b.d() : i3 + this.f1115b.c();
                    }
                    this.f1114a[i2].c(i3);
                }
            } else {
                this.A.a();
                this.A.f1125a = this.A.f1126b;
            }
        }
        this.z = this.A.j;
        a(this.A.h);
        M();
        if (this.A.f1125a != -1) {
            this.f = this.A.f1125a;
            aVar.c = this.A.i;
        } else {
            aVar.c = this.e;
        }
        if (this.A.e > 1) {
            this.h.f1121a = this.A.f;
            this.h.f1122b = this.A.g;
        }
    }

    private void a(e eVar, int i2, int i3) {
        int i4 = eVar.i();
        if (i2 == -1) {
            if (i4 + eVar.b() <= i3) {
                this.m.set(eVar.e, false);
            }
        } else if (eVar.d() - i4 >= i3) {
            this.m.set(eVar.e, false);
        }
    }

    private void a(View view, int i2, int i3, boolean z2) {
        b(view, this.C);
        b bVar = (b) view.getLayoutParams();
        int b2 = b(i2, bVar.leftMargin + this.C.left, bVar.rightMargin + this.C.right);
        int b3 = b(i3, bVar.topMargin + this.C.top, bVar.bottomMargin + this.C.bottom);
        if (z2 ? a(view, b2, b3, (i) bVar) : b(view, b2, b3, (i) bVar)) {
            view.measure(b2, b3);
        }
    }

    private void a(View view, b bVar, as asVar) {
        if (asVar.e == 1) {
            if (bVar.f1120b) {
                p(view);
            } else {
                bVar.f1119a.b(view);
            }
        } else if (bVar.f1120b) {
            q(view);
        } else {
            bVar.f1119a.a(view);
        }
    }

    private void a(View view, b bVar, boolean z2) {
        if (bVar.f1120b) {
            if (this.j == 1) {
                a(view, this.B, a(y(), w(), 0, bVar.height, true), z2);
            } else {
                a(view, a(x(), v(), 0, bVar.width, true), this.B, z2);
            }
        } else if (this.j == 1) {
            a(view, a(this.k, v(), 0, bVar.width, false), a(y(), w(), 0, bVar.height, true), z2);
        } else {
            a(view, a(x(), v(), 0, bVar.width, true), a(this.k, w(), 0, bVar.height, false), z2);
        }
    }

    private boolean a(e eVar) {
        boolean z2 = true;
        if (this.e) {
            if (eVar.d() < this.f1115b.d()) {
                return !eVar.c((View) eVar.f1127a.get(eVar.f1127a.size() + -1)).f1120b;
            }
        } else if (eVar.b() > this.f1115b.c()) {
            if (eVar.c((View) eVar.f1127a.get(0)).f1120b) {
                z2 = false;
            }
            return z2;
        }
        return false;
    }

    private int b(int i2, int i3, int i4) {
        if (i3 == 0 && i4 == 0) {
            return i2;
        }
        int mode = MeasureSpec.getMode(i2);
        return (mode == Integer.MIN_VALUE || mode == 1073741824) ? MeasureSpec.makeMeasureSpec(Math.max(0, (MeasureSpec.getSize(i2) - i3) - i4), mode) : i2;
    }

    private int b(t tVar) {
        boolean z2 = true;
        if (u() == 0) {
            return 0;
        }
        az azVar = this.f1115b;
        View b2 = b(!this.F);
        if (this.F) {
            z2 = false;
        }
        return bf.a(tVar, azVar, b2, d(z2), this, this.F, this.e);
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x002b  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x006b  */
    private void b(int i2, t tVar) {
        int i3;
        int i4;
        boolean z2 = false;
        this.l.f1216b = 0;
        this.l.c = i2;
        if (r()) {
            int c2 = tVar.c();
            if (c2 != -1) {
                if (this.e == (c2 < i2)) {
                    i3 = this.f1115b.f();
                    i4 = 0;
                } else {
                    i4 = this.f1115b.f();
                    i3 = 0;
                }
                if (!q()) {
                    this.l.f = this.f1115b.c() - i4;
                    this.l.g = i3 + this.f1115b.d();
                } else {
                    this.l.g = i3 + this.f1115b.e();
                    this.l.f = -i4;
                }
                this.l.h = false;
                this.l.f1215a = true;
                as asVar = this.l;
                if (this.f1115b.h() == 0 && this.f1115b.e() == 0) {
                    z2 = true;
                }
                asVar.i = z2;
            }
        }
        i3 = 0;
        i4 = 0;
        if (!q()) {
        }
        this.l.h = false;
        this.l.f1215a = true;
        as asVar2 = this.l;
        z2 = true;
        asVar2.i = z2;
    }

    private void b(o oVar, int i2) {
        int u = u() - 1;
        while (u >= 0) {
            View h2 = h(u);
            if (this.f1115b.a(h2) >= i2 && this.f1115b.d(h2) >= i2) {
                b bVar = (b) h2.getLayoutParams();
                if (bVar.f1120b) {
                    int i3 = 0;
                    while (i3 < this.i) {
                        if (this.f1114a[i3].f1127a.size() != 1) {
                            i3++;
                        } else {
                            return;
                        }
                    }
                    for (int i4 = 0; i4 < this.i; i4++) {
                        this.f1114a[i4].g();
                    }
                } else if (bVar.f1119a.f1127a.size() != 1) {
                    bVar.f1119a.g();
                } else {
                    return;
                }
                a(h2, oVar);
                u--;
            } else {
                return;
            }
        }
    }

    private void b(o oVar, t tVar, boolean z2) {
        int q = q(Integer.MIN_VALUE);
        if (q != Integer.MIN_VALUE) {
            int d2 = this.f1115b.d() - q;
            if (d2 > 0) {
                int i2 = d2 - (-c(-d2, oVar, tVar));
                if (z2 && i2 > 0) {
                    this.f1115b.a(i2);
                }
            }
        }
    }

    private void c(int i2, int i3, int i4) {
        int i5;
        int i6;
        int K = this.e ? J() : K();
        if (i4 != 8) {
            i5 = i2 + i3;
            i6 = i2;
        } else if (i2 < i3) {
            i5 = i3 + 1;
            i6 = i2;
        } else {
            i5 = i2 + 1;
            i6 = i3;
        }
        this.h.b(i6);
        switch (i4) {
            case 1:
                this.h.b(i2, i3);
                break;
            case 2:
                this.h.a(i2, i3);
                break;
            case 8:
                this.h.a(i2, 1);
                this.h.b(i3, 1);
                break;
        }
        if (i5 > K) {
            if (i6 <= (this.e ? K() : J())) {
                n();
            }
        }
    }

    private void c(o oVar, t tVar, boolean z2) {
        int p = p(Integer.MAX_VALUE);
        if (p != Integer.MAX_VALUE) {
            int c2 = p - this.f1115b.c();
            if (c2 > 0) {
                int c3 = c2 - c(c2, oVar, tVar);
                if (z2 && c3 > 0) {
                    this.f1115b.a(-c3);
                }
            }
        }
    }

    private boolean c(t tVar, a aVar) {
        aVar.f1117a = this.o ? v(tVar.e()) : u(tVar.e());
        aVar.f1118b = Integer.MIN_VALUE;
        return true;
    }

    private int i(t tVar) {
        boolean z2 = true;
        if (u() == 0) {
            return 0;
        }
        az azVar = this.f1115b;
        View b2 = b(!this.F);
        if (this.F) {
            z2 = false;
        }
        return bf.a(tVar, azVar, b2, d(z2), this, this.F);
    }

    private int j(t tVar) {
        boolean z2 = true;
        if (u() == 0) {
            return 0;
        }
        az azVar = this.f1115b;
        View b2 = b(!this.F);
        if (this.F) {
            z2 = false;
        }
        return bf.b(tVar, azVar, b2, d(z2), this, this.F);
    }

    private void l(int i2) {
        int i3 = 1;
        this.l.e = i2;
        as asVar = this.l;
        if (this.e != (i2 == -1)) {
            i3 = -1;
        }
        asVar.d = i3;
    }

    private a m(int i2) {
        a aVar = new a();
        aVar.c = new int[this.i];
        for (int i3 = 0; i3 < this.i; i3++) {
            aVar.c[i3] = i2 - this.f1114a[i3].b(i2);
        }
        return aVar;
    }

    private a n(int i2) {
        a aVar = new a();
        aVar.c = new int[this.i];
        for (int i3 = 0; i3 < this.i; i3++) {
            aVar.c[i3] = this.f1114a[i3].a(i2) - i2;
        }
        return aVar;
    }

    private int o(int i2) {
        int a2 = this.f1114a[0].a(i2);
        for (int i3 = 1; i3 < this.i; i3++) {
            int a3 = this.f1114a[i3].a(i2);
            if (a3 > a2) {
                a2 = a3;
            }
        }
        return a2;
    }

    private int p(int i2) {
        int a2 = this.f1114a[0].a(i2);
        for (int i3 = 1; i3 < this.i; i3++) {
            int a3 = this.f1114a[i3].a(i2);
            if (a3 < a2) {
                a2 = a3;
            }
        }
        return a2;
    }

    private void p(View view) {
        for (int i2 = this.i - 1; i2 >= 0; i2--) {
            this.f1114a[i2].b(view);
        }
    }

    private int q(int i2) {
        int b2 = this.f1114a[0].b(i2);
        for (int i3 = 1; i3 < this.i; i3++) {
            int b3 = this.f1114a[i3].b(i2);
            if (b3 > b2) {
                b2 = b3;
            }
        }
        return b2;
    }

    private void q(View view) {
        for (int i2 = this.i - 1; i2 >= 0; i2--) {
            this.f1114a[i2].a(view);
        }
    }

    private int r(int i2) {
        int b2 = this.f1114a[0].b(i2);
        for (int i3 = 1; i3 < this.i; i3++) {
            int b3 = this.f1114a[i3].b(i2);
            if (b3 < b2) {
                b2 = b3;
            }
        }
        return b2;
    }

    private boolean s(int i2) {
        if (this.j == 0) {
            return (i2 == -1) != this.e;
        }
        return ((i2 == -1) == this.e) == i();
    }

    private int t(int i2) {
        int i3 = -1;
        if (u() == 0) {
            return this.e ? 1 : -1;
        }
        if ((i2 < K()) == this.e) {
            i3 = 1;
        }
        return i3;
    }

    private int u(int i2) {
        int u = u();
        for (int i3 = 0; i3 < u; i3++) {
            int d2 = d(h(i3));
            if (d2 >= 0 && d2 < i2) {
                return d2;
            }
        }
        return 0;
    }

    private int v(int i2) {
        for (int u = u() - 1; u >= 0; u--) {
            int d2 = d(h(u));
            if (d2 >= 0 && d2 < i2) {
                return d2;
            }
        }
        return 0;
    }

    private int w(int i2) {
        int i3 = Integer.MIN_VALUE;
        int i4 = 1;
        switch (i2) {
            case 1:
                return (this.j == 1 || !i()) ? -1 : 1;
            case 2:
                if (this.j == 1) {
                    return 1;
                }
                return !i() ? 1 : -1;
            case 17:
                return this.j != 0 ? Integer.MIN_VALUE : -1;
            case 33:
                return this.j != 1 ? Integer.MIN_VALUE : -1;
            case 66:
                if (this.j != 0) {
                    i4 = Integer.MIN_VALUE;
                }
                return i4;
            case 130:
                if (this.j == 1) {
                    i3 = 1;
                }
                return i3;
            default:
                return Integer.MIN_VALUE;
        }
    }

    /* access modifiers changed from: 0000 */
    public int J() {
        int u = u();
        if (u == 0) {
            return 0;
        }
        return d(h(u - 1));
    }

    /* access modifiers changed from: 0000 */
    public int K() {
        if (u() == 0) {
            return 0;
        }
        return d(h(0));
    }

    public int a(int i2, o oVar, t tVar) {
        return c(i2, oVar, tVar);
    }

    public int a(o oVar, t tVar) {
        return this.j == 0 ? this.i : super.a(oVar, tVar);
    }

    public i a() {
        return this.j == 0 ? new b(-2, -1) : new b(-1, -2);
    }

    public i a(Context context, AttributeSet attributeSet) {
        return new b(context, attributeSet);
    }

    public i a(LayoutParams layoutParams) {
        return layoutParams instanceof MarginLayoutParams ? new b((MarginLayoutParams) layoutParams) : new b(layoutParams);
    }

    public View a(View view, int i2, o oVar, t tVar) {
        if (u() == 0) {
            return null;
        }
        View e2 = e(view);
        if (e2 == null) {
            return null;
        }
        M();
        int w = w(i2);
        if (w == Integer.MIN_VALUE) {
            return null;
        }
        b bVar = (b) e2.getLayoutParams();
        boolean z2 = bVar.f1120b;
        e eVar = bVar.f1119a;
        int K = w == 1 ? J() : K();
        b(K, tVar);
        l(w);
        this.l.c = this.l.d + K;
        this.l.f1216b = (int) (0.33333334f * ((float) this.f1115b.f()));
        this.l.h = true;
        this.l.f1215a = false;
        a(oVar, this.l, tVar);
        this.o = this.e;
        if (!z2) {
            View a2 = eVar.a(K, w);
            if (!(a2 == null || a2 == e2)) {
                return a2;
            }
        }
        if (s(w)) {
            for (int i3 = this.i - 1; i3 >= 0; i3--) {
                View a3 = this.f1114a[i3].a(K, w);
                if (a3 != null && a3 != e2) {
                    return a3;
                }
            }
        } else {
            for (int i4 = 0; i4 < this.i; i4++) {
                View a4 = this.f1114a[i4].a(K, w);
                if (a4 != null && a4 != e2) {
                    return a4;
                }
            }
        }
        boolean z3 = (!this.d) == (w == -1);
        if (!z2) {
            View c2 = c(z3 ? eVar.j() : eVar.k());
            if (!(c2 == null || c2 == e2)) {
                return c2;
            }
        }
        if (s(w)) {
            for (int i5 = this.i - 1; i5 >= 0; i5--) {
                if (i5 != eVar.e) {
                    View c3 = c(z3 ? this.f1114a[i5].j() : this.f1114a[i5].k());
                    if (!(c3 == null || c3 == e2)) {
                        return c3;
                    }
                }
            }
        } else {
            for (int i6 = 0; i6 < this.i; i6++) {
                View c4 = c(z3 ? this.f1114a[i6].j() : this.f1114a[i6].k());
                if (c4 != null && c4 != e2) {
                    return c4;
                }
            }
        }
        return null;
    }

    public void a(int i2) {
        a((String) null);
        if (i2 != this.i) {
            h();
            this.i = i2;
            this.m = new BitSet(this.i);
            this.f1114a = new e[this.i];
            for (int i3 = 0; i3 < this.i; i3++) {
                this.f1114a[i3] = new e(i3);
            }
            n();
        }
    }

    public void a(int i2, int i3, t tVar, android.support.v7.widget.RecyclerView.h.a aVar) {
        if (this.j != 0) {
            i2 = i3;
        }
        if (u() != 0 && i2 != 0) {
            a(i2, tVar);
            if (this.G == null || this.G.length < this.i) {
                this.G = new int[this.i];
            }
            int i4 = 0;
            for (int i5 = 0; i5 < this.i; i5++) {
                int b2 = this.l.d == -1 ? this.l.f - this.f1114a[i5].a(this.l.f) : this.f1114a[i5].b(this.l.g) - this.l.g;
                if (b2 >= 0) {
                    this.G[i4] = b2;
                    i4++;
                }
            }
            Arrays.sort(this.G, 0, i4);
            for (int i6 = 0; i6 < i4 && this.l.a(tVar); i6++) {
                aVar.b(this.l.c, this.G[i6]);
                this.l.c += this.l.d;
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(int i2, t tVar) {
        int i3;
        int K;
        if (i2 > 0) {
            K = J();
            i3 = 1;
        } else {
            i3 = -1;
            K = K();
        }
        this.l.f1215a = true;
        b(K, tVar);
        l(i3);
        this.l.c = this.l.d + K;
        this.l.f1216b = Math.abs(i2);
    }

    public void a(Rect rect, int i2, int i3) {
        int a2;
        int a3;
        int B2 = B() + z();
        int A2 = A() + C();
        if (this.j == 1) {
            a3 = a(i3, A2 + rect.height(), F());
            a2 = a(i2, B2 + (this.k * this.i), E());
        } else {
            a2 = a(i2, B2 + rect.width(), E());
            a3 = a(i3, A2 + (this.k * this.i), F());
        }
        g(a2, a3);
    }

    public void a(Parcelable parcelable) {
        if (parcelable instanceof d) {
            this.A = (d) parcelable;
            n();
        }
    }

    public void a(o oVar, t tVar, View view, android.support.v4.i.a.b bVar) {
        LayoutParams layoutParams = view.getLayoutParams();
        if (!(layoutParams instanceof b)) {
            super.a(view, bVar);
            return;
        }
        b bVar2 = (b) layoutParams;
        if (this.j == 0) {
            bVar.b((Object) C0017b.a(bVar2.b(), bVar2.f1120b ? this.i : 1, -1, -1, bVar2.f1120b, false));
        } else {
            bVar.b((Object) C0017b.a(-1, -1, bVar2.b(), bVar2.f1120b ? this.i : 1, bVar2.f1120b, false));
        }
    }

    public void a(t tVar) {
        super.a(tVar);
        this.f = -1;
        this.g = Integer.MIN_VALUE;
        this.A = null;
        this.D.a();
    }

    /* access modifiers changed from: 0000 */
    public void a(t tVar, a aVar) {
        if (!b(tVar, aVar) && !c(tVar, aVar)) {
            aVar.b();
            aVar.f1117a = 0;
        }
    }

    public void a(RecyclerView recyclerView) {
        this.h.a();
        n();
    }

    public void a(RecyclerView recyclerView, int i2, int i3) {
        c(i2, i3, 1);
    }

    public void a(RecyclerView recyclerView, int i2, int i3, int i4) {
        c(i2, i3, 8);
    }

    public void a(RecyclerView recyclerView, int i2, int i3, Object obj) {
        c(i2, i3, 4);
    }

    public void a(RecyclerView recyclerView, o oVar) {
        a(this.H);
        for (int i2 = 0; i2 < this.i; i2++) {
            this.f1114a[i2].e();
        }
        recyclerView.requestLayout();
    }

    public void a(AccessibilityEvent accessibilityEvent) {
        super.a(accessibilityEvent);
        if (u() > 0) {
            View b2 = b(false);
            View d2 = d(false);
            if (b2 != null && d2 != null) {
                int d3 = d(b2);
                int d4 = d(d2);
                if (d3 < d4) {
                    accessibilityEvent.setFromIndex(d3);
                    accessibilityEvent.setToIndex(d4);
                    return;
                }
                accessibilityEvent.setFromIndex(d4);
                accessibilityEvent.setToIndex(d3);
            }
        }
    }

    public void a(String str) {
        if (this.A == null) {
            super.a(str);
        }
    }

    public void a(boolean z2) {
        a((String) null);
        if (!(this.A == null || this.A.h == z2)) {
            this.A.h = z2;
        }
        this.d = z2;
        n();
    }

    public boolean a(i iVar) {
        return iVar instanceof b;
    }

    public int b(int i2, o oVar, t tVar) {
        return c(i2, oVar, tVar);
    }

    public int b(o oVar, t tVar) {
        return this.j == 1 ? this.i : super.b(oVar, tVar);
    }

    /* access modifiers changed from: 0000 */
    public View b(boolean z2) {
        int c2 = this.f1115b.c();
        int d2 = this.f1115b.d();
        int u = u();
        View view = null;
        for (int i2 = 0; i2 < u; i2++) {
            View h2 = h(i2);
            int a2 = this.f1115b.a(h2);
            if (this.f1115b.b(h2) > c2 && a2 < d2) {
                if (a2 >= c2 || !z2) {
                    return h2;
                }
                if (view == null) {
                    view = h2;
                }
            }
        }
        return view;
    }

    public void b(int i2) {
        if (i2 == 0 || i2 == 1) {
            a((String) null);
            if (i2 != this.j) {
                this.j = i2;
                az azVar = this.f1115b;
                this.f1115b = this.c;
                this.c = azVar;
                n();
                return;
            }
            return;
        }
        throw new IllegalArgumentException("invalid orientation.");
    }

    public void b(RecyclerView recyclerView, int i2, int i3) {
        c(i2, i3, 2);
    }

    public boolean b() {
        return this.A == null;
    }

    /* access modifiers changed from: 0000 */
    public boolean b(t tVar, a aVar) {
        boolean z2 = false;
        if (tVar.a() || this.f == -1) {
            return false;
        }
        if (this.f < 0 || this.f >= tVar.e()) {
            this.f = -1;
            this.g = Integer.MIN_VALUE;
            return false;
        } else if (this.A == null || this.A.f1125a == -1 || this.A.c < 1) {
            View c2 = c(this.f);
            if (c2 != null) {
                aVar.f1117a = this.e ? J() : K();
                if (this.g != Integer.MIN_VALUE) {
                    if (aVar.c) {
                        aVar.f1118b = (this.f1115b.d() - this.g) - this.f1115b.b(c2);
                        return true;
                    }
                    aVar.f1118b = (this.f1115b.c() + this.g) - this.f1115b.a(c2);
                    return true;
                } else if (this.f1115b.e(c2) > this.f1115b.f()) {
                    aVar.f1118b = aVar.c ? this.f1115b.d() : this.f1115b.c();
                    return true;
                } else {
                    int a2 = this.f1115b.a(c2) - this.f1115b.c();
                    if (a2 < 0) {
                        aVar.f1118b = -a2;
                        return true;
                    }
                    int d2 = this.f1115b.d() - this.f1115b.b(c2);
                    if (d2 < 0) {
                        aVar.f1118b = d2;
                        return true;
                    }
                    aVar.f1118b = Integer.MIN_VALUE;
                    return true;
                }
            } else {
                aVar.f1117a = this.f;
                if (this.g == Integer.MIN_VALUE) {
                    if (t(aVar.f1117a) == 1) {
                        z2 = true;
                    }
                    aVar.c = z2;
                    aVar.b();
                } else {
                    aVar.a(this.g);
                }
                aVar.d = true;
                return true;
            }
        } else {
            aVar.f1118b = Integer.MIN_VALUE;
            aVar.f1117a = this.f;
            return true;
        }
    }

    /* access modifiers changed from: 0000 */
    public int c(int i2, o oVar, t tVar) {
        if (u() == 0 || i2 == 0) {
            return 0;
        }
        a(i2, tVar);
        int a2 = a(oVar, this.l, tVar);
        if (this.l.f1216b >= a2) {
            i2 = i2 < 0 ? -a2 : a2;
        }
        this.f1115b.a(-i2);
        this.o = this.e;
        this.l.f1216b = 0;
        a(oVar, this.l);
        return i2;
    }

    public int c(t tVar) {
        return b(tVar);
    }

    public Parcelable c() {
        int a2;
        if (this.A != null) {
            return new d(this.A);
        }
        d dVar = new d();
        dVar.h = this.d;
        dVar.i = this.o;
        dVar.j = this.z;
        if (this.h == null || this.h.f1121a == null) {
            dVar.e = 0;
        } else {
            dVar.f = this.h.f1121a;
            dVar.e = dVar.f.length;
            dVar.g = this.h.f1122b;
        }
        if (u() > 0) {
            dVar.f1125a = this.o ? J() : K();
            dVar.f1126b = j();
            dVar.c = this.i;
            dVar.d = new int[this.i];
            for (int i2 = 0; i2 < this.i; i2++) {
                if (this.o) {
                    a2 = this.f1114a[i2].b(Integer.MIN_VALUE);
                    if (a2 != Integer.MIN_VALUE) {
                        a2 -= this.f1115b.d();
                    }
                } else {
                    a2 = this.f1114a[i2].a(Integer.MIN_VALUE);
                    if (a2 != Integer.MIN_VALUE) {
                        a2 -= this.f1115b.c();
                    }
                }
                dVar.d[i2] = a2;
            }
        } else {
            dVar.f1125a = -1;
            dVar.f1126b = -1;
            dVar.c = 0;
        }
        return dVar;
    }

    public void c(o oVar, t tVar) {
        a(oVar, tVar, true);
    }

    public int d(t tVar) {
        return b(tVar);
    }

    /* access modifiers changed from: 0000 */
    public View d(boolean z2) {
        int c2 = this.f1115b.c();
        int d2 = this.f1115b.d();
        View view = null;
        for (int u = u() - 1; u >= 0; u--) {
            View h2 = h(u);
            int a2 = this.f1115b.a(h2);
            int b2 = this.f1115b.b(h2);
            if (b2 > c2 && a2 < d2) {
                if (b2 <= d2 || !z2) {
                    return h2;
                }
                if (view == null) {
                    view = h2;
                }
            }
        }
        return view;
    }

    public void d(int i2) {
        if (!(this.A == null || this.A.f1125a == i2)) {
            this.A.b();
        }
        this.f = i2;
        this.g = Integer.MIN_VALUE;
        n();
    }

    public boolean d() {
        return this.j == 0;
    }

    public int e(t tVar) {
        return i(tVar);
    }

    /* access modifiers changed from: 0000 */
    public void e(int i2) {
        this.k = i2 / this.i;
        this.B = MeasureSpec.makeMeasureSpec(i2, this.c.h());
    }

    public boolean e() {
        return this.j == 1;
    }

    public int f(t tVar) {
        return i(tVar);
    }

    /* access modifiers changed from: 0000 */
    public boolean f() {
        int K;
        int J;
        if (u() == 0 || this.n == 0 || !p()) {
            return false;
        }
        if (this.e) {
            K = J();
            J = K();
        } else {
            K = K();
            J = J();
        }
        if (K == 0 && g() != null) {
            this.h.a();
            H();
            n();
            return true;
        } else if (!this.E) {
            return false;
        } else {
            int i2 = this.e ? -1 : 1;
            a a2 = this.h.a(K, J + 1, i2, true);
            if (a2 == null) {
                this.E = false;
                this.h.a(J + 1);
                return false;
            }
            a a3 = this.h.a(K, a2.f1123a, i2 * -1, true);
            if (a3 == null) {
                this.h.a(a2.f1123a);
            } else {
                this.h.a(a3.f1123a + 1);
            }
            H();
            n();
            return true;
        }
    }

    public int g(t tVar) {
        return j(tVar);
    }

    /* access modifiers changed from: 0000 */
    public View g() {
        int i2;
        boolean z2;
        int u = u() - 1;
        BitSet bitSet = new BitSet(this.i);
        bitSet.set(0, this.i, true);
        char c2 = (this.j != 1 || !i()) ? (char) 65535 : 1;
        if (this.e) {
            i2 = -1;
        } else {
            i2 = u + 1;
            u = 0;
        }
        int i3 = u < i2 ? 1 : -1;
        for (int i4 = u; i4 != i2; i4 += i3) {
            View h2 = h(i4);
            b bVar = (b) h2.getLayoutParams();
            if (bitSet.get(bVar.f1119a.e)) {
                if (a(bVar.f1119a)) {
                    return h2;
                }
                bitSet.clear(bVar.f1119a.e);
            }
            if (!bVar.f1120b && i4 + i3 != i2) {
                View h3 = h(i4 + i3);
                if (this.e) {
                    int b2 = this.f1115b.b(h2);
                    int b3 = this.f1115b.b(h3);
                    if (b2 < b3) {
                        return h2;
                    }
                    if (b2 == b3) {
                        z2 = true;
                    }
                    z2 = false;
                } else {
                    int a2 = this.f1115b.a(h2);
                    int a3 = this.f1115b.a(h3);
                    if (a2 > a3) {
                        return h2;
                    }
                    if (a2 == a3) {
                        z2 = true;
                    }
                    z2 = false;
                }
                if (z2) {
                    if ((bVar.f1119a.e - ((b) h3.getLayoutParams()).f1119a.e < 0) != (c2 < 0)) {
                        return h2;
                    }
                } else {
                    continue;
                }
            }
        }
        return null;
    }

    public int h(t tVar) {
        return j(tVar);
    }

    public void h() {
        this.h.a();
        n();
    }

    public void i(int i2) {
        super.i(i2);
        for (int i3 = 0; i3 < this.i; i3++) {
            this.f1114a[i3].d(i2);
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean i() {
        return s() == 1;
    }

    /* access modifiers changed from: 0000 */
    public int j() {
        View b2 = this.e ? d(true) : b(true);
        if (b2 == null) {
            return -1;
        }
        return d(b2);
    }

    public void j(int i2) {
        super.j(i2);
        for (int i3 = 0; i3 < this.i; i3++) {
            this.f1114a[i3].d(i2);
        }
    }

    public void k(int i2) {
        if (i2 == 0) {
            f();
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean l() {
        int b2 = this.f1114a[0].b(Integer.MIN_VALUE);
        for (int i2 = 1; i2 < this.i; i2++) {
            if (this.f1114a[i2].b(Integer.MIN_VALUE) != b2) {
                return false;
            }
        }
        return true;
    }

    /* access modifiers changed from: 0000 */
    public boolean m() {
        int a2 = this.f1114a[0].a(Integer.MIN_VALUE);
        for (int i2 = 1; i2 < this.i; i2++) {
            if (this.f1114a[i2].a(Integer.MIN_VALUE) != a2) {
                return false;
            }
        }
        return true;
    }
}
