package android.support.v7.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;

class af implements ah {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public final RectF f1168a = new RectF();

    af() {
    }

    private bd a(Context context, ColorStateList colorStateList, float f, float f2, float f3) {
        return new bd(context.getResources(), colorStateList, f, f2, f3);
    }

    private bd j(ag agVar) {
        return (bd) agVar.c();
    }

    public float a(ag agVar) {
        return j(agVar).c();
    }

    public void a() {
        bd.f1242a = new a() {
            public void a(Canvas canvas, RectF rectF, float f, Paint paint) {
                float f2 = 2.0f * f;
                float width = (rectF.width() - f2) - 1.0f;
                float height = (rectF.height() - f2) - 1.0f;
                if (f >= 1.0f) {
                    float f3 = f + 0.5f;
                    af.this.f1168a.set(-f3, -f3, f3, f3);
                    int save = canvas.save();
                    canvas.translate(rectF.left + f3, rectF.top + f3);
                    canvas.drawArc(af.this.f1168a, 180.0f, 90.0f, true, paint);
                    canvas.translate(width, 0.0f);
                    canvas.rotate(90.0f);
                    canvas.drawArc(af.this.f1168a, 180.0f, 90.0f, true, paint);
                    canvas.translate(height, 0.0f);
                    canvas.rotate(90.0f);
                    canvas.drawArc(af.this.f1168a, 180.0f, 90.0f, true, paint);
                    canvas.translate(width, 0.0f);
                    canvas.rotate(90.0f);
                    canvas.drawArc(af.this.f1168a, 180.0f, 90.0f, true, paint);
                    canvas.restoreToCount(save);
                    canvas.drawRect((rectF.left + f3) - 1.0f, rectF.top, 1.0f + (rectF.right - f3), rectF.top + f3, paint);
                    canvas.drawRect((rectF.left + f3) - 1.0f, rectF.bottom - f3, 1.0f + (rectF.right - f3), rectF.bottom, paint);
                }
                canvas.drawRect(rectF.left, rectF.top + f, rectF.right, rectF.bottom - f, paint);
            }
        };
    }

    public void a(ag agVar, float f) {
        j(agVar).a(f);
        f(agVar);
    }

    public void a(ag agVar, Context context, ColorStateList colorStateList, float f, float f2, float f3) {
        bd a2 = a(context, colorStateList, f, f2, f3);
        a2.a(agVar.b());
        agVar.a(a2);
        f(agVar);
    }

    public void a(ag agVar, ColorStateList colorStateList) {
        j(agVar).a(colorStateList);
    }

    public float b(ag agVar) {
        return j(agVar).d();
    }

    public void b(ag agVar, float f) {
        j(agVar).c(f);
        f(agVar);
    }

    public float c(ag agVar) {
        return j(agVar).e();
    }

    public void c(ag agVar, float f) {
        j(agVar).b(f);
    }

    public float d(ag agVar) {
        return j(agVar).a();
    }

    public float e(ag agVar) {
        return j(agVar).b();
    }

    public void f(ag agVar) {
        Rect rect = new Rect();
        j(agVar).a(rect);
        agVar.a((int) Math.ceil((double) b(agVar)), (int) Math.ceil((double) c(agVar)));
        agVar.a(rect.left, rect.top, rect.right, rect.bottom);
    }

    public void g(ag agVar) {
    }

    public void h(ag agVar) {
        j(agVar).a(agVar.b());
        f(agVar);
    }

    public ColorStateList i(ag agVar) {
        return j(agVar).f();
    }
}
