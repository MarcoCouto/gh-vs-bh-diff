package android.support.v7.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v4.i.s;
import android.support.v4.widget.p;
import android.util.AttributeSet;
import android.widget.ImageView;

public class q extends ImageView implements s, p {

    /* renamed from: a reason: collision with root package name */
    private final h f1328a;

    /* renamed from: b reason: collision with root package name */
    private final p f1329b;

    public q(Context context) {
        this(context, null);
    }

    public q(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public q(Context context, AttributeSet attributeSet, int i) {
        super(bl.a(context), attributeSet, i);
        this.f1328a = new h(this);
        this.f1328a.a(attributeSet, i);
        this.f1329b = new p(this);
        this.f1329b.a(attributeSet, i);
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        if (this.f1328a != null) {
            this.f1328a.c();
        }
        if (this.f1329b != null) {
            this.f1329b.d();
        }
    }

    public ColorStateList getSupportBackgroundTintList() {
        if (this.f1328a != null) {
            return this.f1328a.a();
        }
        return null;
    }

    public Mode getSupportBackgroundTintMode() {
        if (this.f1328a != null) {
            return this.f1328a.b();
        }
        return null;
    }

    public ColorStateList getSupportImageTintList() {
        if (this.f1329b != null) {
            return this.f1329b.b();
        }
        return null;
    }

    public Mode getSupportImageTintMode() {
        if (this.f1329b != null) {
            return this.f1329b.c();
        }
        return null;
    }

    public boolean hasOverlappingRendering() {
        return this.f1329b.a() && super.hasOverlappingRendering();
    }

    public void setBackgroundDrawable(Drawable drawable) {
        super.setBackgroundDrawable(drawable);
        if (this.f1328a != null) {
            this.f1328a.a(drawable);
        }
    }

    public void setBackgroundResource(int i) {
        super.setBackgroundResource(i);
        if (this.f1328a != null) {
            this.f1328a.a(i);
        }
    }

    public void setImageBitmap(Bitmap bitmap) {
        super.setImageBitmap(bitmap);
        if (this.f1329b != null) {
            this.f1329b.d();
        }
    }

    public void setImageDrawable(Drawable drawable) {
        super.setImageDrawable(drawable);
        if (this.f1329b != null) {
            this.f1329b.d();
        }
    }

    public void setImageResource(int i) {
        if (this.f1329b != null) {
            this.f1329b.a(i);
        }
    }

    public void setImageURI(Uri uri) {
        super.setImageURI(uri);
        if (this.f1329b != null) {
            this.f1329b.d();
        }
    }

    public void setSupportBackgroundTintList(ColorStateList colorStateList) {
        if (this.f1328a != null) {
            this.f1328a.a(colorStateList);
        }
    }

    public void setSupportBackgroundTintMode(Mode mode) {
        if (this.f1328a != null) {
            this.f1328a.a(mode);
        }
    }

    public void setSupportImageTintList(ColorStateList colorStateList) {
        if (this.f1329b != null) {
            this.f1329b.a(colorStateList);
        }
    }

    public void setSupportImageTintMode(Mode mode) {
        if (this.f1329b != null) {
            this.f1329b.a(mode);
        }
    }
}
