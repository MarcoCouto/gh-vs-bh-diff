package android.support.v7.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v4.i.s;
import android.support.v4.widget.p;
import android.support.v7.a.a.C0026a;
import android.util.AttributeSet;
import android.widget.ImageButton;

public class o extends ImageButton implements s, p {

    /* renamed from: a reason: collision with root package name */
    private final h f1324a;

    /* renamed from: b reason: collision with root package name */
    private final p f1325b;

    public o(Context context) {
        this(context, null);
    }

    public o(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, C0026a.imageButtonStyle);
    }

    public o(Context context, AttributeSet attributeSet, int i) {
        super(bl.a(context), attributeSet, i);
        this.f1324a = new h(this);
        this.f1324a.a(attributeSet, i);
        this.f1325b = new p(this);
        this.f1325b.a(attributeSet, i);
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        if (this.f1324a != null) {
            this.f1324a.c();
        }
        if (this.f1325b != null) {
            this.f1325b.d();
        }
    }

    public ColorStateList getSupportBackgroundTintList() {
        if (this.f1324a != null) {
            return this.f1324a.a();
        }
        return null;
    }

    public Mode getSupportBackgroundTintMode() {
        if (this.f1324a != null) {
            return this.f1324a.b();
        }
        return null;
    }

    public ColorStateList getSupportImageTintList() {
        if (this.f1325b != null) {
            return this.f1325b.b();
        }
        return null;
    }

    public Mode getSupportImageTintMode() {
        if (this.f1325b != null) {
            return this.f1325b.c();
        }
        return null;
    }

    public boolean hasOverlappingRendering() {
        return this.f1325b.a() && super.hasOverlappingRendering();
    }

    public void setBackgroundDrawable(Drawable drawable) {
        super.setBackgroundDrawable(drawable);
        if (this.f1324a != null) {
            this.f1324a.a(drawable);
        }
    }

    public void setBackgroundResource(int i) {
        super.setBackgroundResource(i);
        if (this.f1324a != null) {
            this.f1324a.a(i);
        }
    }

    public void setImageBitmap(Bitmap bitmap) {
        super.setImageBitmap(bitmap);
        if (this.f1325b != null) {
            this.f1325b.d();
        }
    }

    public void setImageDrawable(Drawable drawable) {
        super.setImageDrawable(drawable);
        if (this.f1325b != null) {
            this.f1325b.d();
        }
    }

    public void setImageResource(int i) {
        this.f1325b.a(i);
    }

    public void setImageURI(Uri uri) {
        super.setImageURI(uri);
        if (this.f1325b != null) {
            this.f1325b.d();
        }
    }

    public void setSupportBackgroundTintList(ColorStateList colorStateList) {
        if (this.f1324a != null) {
            this.f1324a.a(colorStateList);
        }
    }

    public void setSupportBackgroundTintMode(Mode mode) {
        if (this.f1324a != null) {
            this.f1324a.a(mode);
        }
    }

    public void setSupportImageTintList(ColorStateList colorStateList) {
        if (this.f1325b != null) {
            this.f1325b.a(colorStateList);
        }
    }

    public void setSupportImageTintMode(Mode mode) {
        if (this.f1325b != null) {
            this.f1325b.a(mode);
        }
    }
}
