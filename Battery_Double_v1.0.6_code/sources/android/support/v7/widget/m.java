package android.support.v7.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.content.res.XmlResourceParser;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Drawable.ConstantState;
import android.graphics.drawable.LayerDrawable;
import android.os.Build.VERSION;
import android.support.c.a.i;
import android.support.v4.h.f;
import android.support.v4.h.g;
import android.support.v4.h.n;
import android.support.v7.a.a.C0026a;
import android.support.v7.a.a.e;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.util.Xml;
import java.lang.ref.WeakReference;
import java.util.WeakHashMap;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public final class m {

    /* renamed from: a reason: collision with root package name */
    private static final Mode f1320a = Mode.SRC_IN;

    /* renamed from: b reason: collision with root package name */
    private static m f1321b;
    private static final b c = new b(6);
    private static final int[] d = {e.abc_textfield_search_default_mtrl_alpha, e.abc_textfield_default_mtrl_alpha, e.abc_ab_share_pack_mtrl_alpha};
    private static final int[] e = {e.abc_ic_commit_search_api_mtrl_alpha, e.abc_seekbar_tick_mark_material, e.abc_ic_menu_share_mtrl_alpha, e.abc_ic_menu_copy_mtrl_am_alpha, e.abc_ic_menu_cut_mtrl_alpha, e.abc_ic_menu_selectall_mtrl_alpha, e.abc_ic_menu_paste_mtrl_am_alpha};
    private static final int[] f = {e.abc_textfield_activated_mtrl_alpha, e.abc_textfield_search_activated_mtrl_alpha, e.abc_cab_background_top_mtrl_alpha, e.abc_text_cursor_material, e.abc_text_select_handle_left_mtrl_dark, e.abc_text_select_handle_middle_mtrl_dark, e.abc_text_select_handle_right_mtrl_dark, e.abc_text_select_handle_left_mtrl_light, e.abc_text_select_handle_middle_mtrl_light, e.abc_text_select_handle_right_mtrl_light};
    private static final int[] g = {e.abc_popup_background_mtrl_mult, e.abc_cab_background_internal_bg, e.abc_menu_hardkey_panel_mtrl_mult};
    private static final int[] h = {e.abc_tab_indicator_material, e.abc_textfield_search_material};
    private static final int[] i = {e.abc_btn_check_material, e.abc_btn_radio_material};
    private WeakHashMap<Context, n<ColorStateList>> j;
    private android.support.v4.h.a<String, c> k;
    private n<String> l;
    private final Object m = new Object();
    private final WeakHashMap<Context, f<WeakReference<ConstantState>>> n = new WeakHashMap<>(0);
    private TypedValue o;
    private boolean p;

    private static class a implements c {
        a() {
        }

        public Drawable a(Context context, XmlPullParser xmlPullParser, AttributeSet attributeSet, Theme theme) {
            try {
                return android.support.c.a.c.a(context, context.getResources(), xmlPullParser, attributeSet, theme);
            } catch (Exception e) {
                Log.e("AvdcInflateDelegate", "Exception while inflating <animated-vector>", e);
                return null;
            }
        }
    }

    private static class b extends g<Integer, PorterDuffColorFilter> {
        public b(int i) {
            super(i);
        }

        private static int b(int i, Mode mode) {
            return ((i + 31) * 31) + mode.hashCode();
        }

        /* access modifiers changed from: 0000 */
        public PorterDuffColorFilter a(int i, Mode mode) {
            return (PorterDuffColorFilter) a(Integer.valueOf(b(i, mode)));
        }

        /* access modifiers changed from: 0000 */
        public PorterDuffColorFilter a(int i, Mode mode, PorterDuffColorFilter porterDuffColorFilter) {
            return (PorterDuffColorFilter) a(Integer.valueOf(b(i, mode)), porterDuffColorFilter);
        }
    }

    private interface c {
        Drawable a(Context context, XmlPullParser xmlPullParser, AttributeSet attributeSet, Theme theme);
    }

    private static class d implements c {
        d() {
        }

        public Drawable a(Context context, XmlPullParser xmlPullParser, AttributeSet attributeSet, Theme theme) {
            try {
                return i.a(context.getResources(), xmlPullParser, attributeSet, theme);
            } catch (Exception e) {
                Log.e("VdcInflateDelegate", "Exception while inflating <vector>", e);
                return null;
            }
        }
    }

    private static long a(TypedValue typedValue) {
        return (((long) typedValue.assetCookie) << 32) | ((long) typedValue.data);
    }

    static Mode a(int i2) {
        if (i2 == e.abc_switch_thumb_material) {
            return Mode.MULTIPLY;
        }
        return null;
    }

    public static PorterDuffColorFilter a(int i2, Mode mode) {
        PorterDuffColorFilter a2 = c.a(i2, mode);
        if (a2 != null) {
            return a2;
        }
        PorterDuffColorFilter porterDuffColorFilter = new PorterDuffColorFilter(i2, mode);
        c.a(i2, mode, porterDuffColorFilter);
        return porterDuffColorFilter;
    }

    private static PorterDuffColorFilter a(ColorStateList colorStateList, Mode mode, int[] iArr) {
        if (colorStateList == null || mode == null) {
            return null;
        }
        return a(colorStateList.getColorForState(iArr, 0), mode);
    }

    private Drawable a(Context context, int i2, boolean z, Drawable drawable) {
        ColorStateList b2 = b(context, i2);
        if (b2 != null) {
            if (am.b(drawable)) {
                drawable = drawable.mutate();
            }
            Drawable f2 = android.support.v4.c.a.a.f(drawable);
            android.support.v4.c.a.a.a(f2, b2);
            Mode a2 = a(i2);
            if (a2 == null) {
                return f2;
            }
            android.support.v4.c.a.a.a(f2, a2);
            return f2;
        } else if (i2 == e.abc_seekbar_track_material) {
            LayerDrawable layerDrawable = (LayerDrawable) drawable;
            a(layerDrawable.findDrawableByLayerId(16908288), bj.a(context, C0026a.colorControlNormal), f1320a);
            a(layerDrawable.findDrawableByLayerId(16908303), bj.a(context, C0026a.colorControlNormal), f1320a);
            a(layerDrawable.findDrawableByLayerId(16908301), bj.a(context, C0026a.colorControlActivated), f1320a);
            return drawable;
        } else if (i2 == e.abc_ratingbar_material || i2 == e.abc_ratingbar_indicator_material || i2 == e.abc_ratingbar_small_material) {
            LayerDrawable layerDrawable2 = (LayerDrawable) drawable;
            a(layerDrawable2.findDrawableByLayerId(16908288), bj.c(context, C0026a.colorControlNormal), f1320a);
            a(layerDrawable2.findDrawableByLayerId(16908303), bj.a(context, C0026a.colorControlActivated), f1320a);
            a(layerDrawable2.findDrawableByLayerId(16908301), bj.a(context, C0026a.colorControlActivated), f1320a);
            return drawable;
        } else if (a(context, i2, drawable) || !z) {
            return drawable;
        } else {
            return null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        return null;
     */
    private Drawable a(Context context, long j2) {
        synchronized (this.m) {
            f fVar = (f) this.n.get(context);
            if (fVar == null) {
                return null;
            }
            WeakReference weakReference = (WeakReference) fVar.a(j2);
            if (weakReference != null) {
                ConstantState constantState = (ConstantState) weakReference.get();
                if (constantState != null) {
                    Drawable newDrawable = constantState.newDrawable(context.getResources());
                    return newDrawable;
                }
                fVar.b(j2);
            }
        }
    }

    public static m a() {
        if (f1321b == null) {
            f1321b = new m();
            a(f1321b);
        }
        return f1321b;
    }

    private void a(Context context, int i2, ColorStateList colorStateList) {
        if (this.j == null) {
            this.j = new WeakHashMap<>();
        }
        n nVar = (n) this.j.get(context);
        if (nVar == null) {
            nVar = new n();
            this.j.put(context, nVar);
        }
        nVar.c(i2, colorStateList);
    }

    private static void a(Drawable drawable, int i2, Mode mode) {
        if (am.b(drawable)) {
            drawable = drawable.mutate();
        }
        if (mode == null) {
            mode = f1320a;
        }
        drawable.setColorFilter(a(i2, mode));
    }

    static void a(Drawable drawable, bm bmVar, int[] iArr) {
        if (!am.b(drawable) || drawable.mutate() == drawable) {
            if (bmVar.d || bmVar.c) {
                drawable.setColorFilter(a(bmVar.d ? bmVar.f1260a : null, bmVar.c ? bmVar.f1261b : f1320a, iArr));
            } else {
                drawable.clearColorFilter();
            }
            if (VERSION.SDK_INT <= 23) {
                drawable.invalidateSelf();
                return;
            }
            return;
        }
        Log.d("AppCompatDrawableManag", "Mutated drawable is not the same instance as the input.");
    }

    private static void a(m mVar) {
        if (VERSION.SDK_INT < 24) {
            mVar.a("vector", (c) new d());
            mVar.a("animated-vector", (c) new a());
        }
    }

    private void a(String str, c cVar) {
        if (this.k == null) {
            this.k = new android.support.v4.h.a<>();
        }
        this.k.put(str, cVar);
    }

    static boolean a(Context context, int i2, Drawable drawable) {
        int i3;
        int i4;
        Mode mode;
        boolean z;
        Mode mode2 = f1320a;
        if (a(d, i2)) {
            i4 = C0026a.colorControlNormal;
            mode = mode2;
            z = true;
            i3 = -1;
        } else if (a(f, i2)) {
            i4 = C0026a.colorControlActivated;
            mode = mode2;
            z = true;
            i3 = -1;
        } else if (a(g, i2)) {
            z = true;
            mode = Mode.MULTIPLY;
            i4 = 16842801;
            i3 = -1;
        } else if (i2 == e.abc_list_divider_mtrl_alpha) {
            i4 = 16842800;
            i3 = Math.round(40.8f);
            mode = mode2;
            z = true;
        } else if (i2 == e.abc_dialog_material_background) {
            i4 = 16842801;
            mode = mode2;
            z = true;
            i3 = -1;
        } else {
            i3 = -1;
            i4 = 0;
            mode = mode2;
            z = false;
        }
        if (!z) {
            return false;
        }
        if (am.b(drawable)) {
            drawable = drawable.mutate();
        }
        drawable.setColorFilter(a(bj.a(context, i4), mode));
        if (i3 == -1) {
            return true;
        }
        drawable.setAlpha(i3);
        return true;
    }

    private boolean a(Context context, long j2, Drawable drawable) {
        ConstantState constantState = drawable.getConstantState();
        if (constantState == null) {
            return false;
        }
        synchronized (this.m) {
            f fVar = (f) this.n.get(context);
            if (fVar == null) {
                fVar = new f();
                this.n.put(context, fVar);
            }
            fVar.b(j2, new WeakReference(constantState));
        }
        return true;
    }

    private static boolean a(Drawable drawable) {
        return (drawable instanceof i) || "android.graphics.drawable.VectorDrawable".equals(drawable.getClass().getName());
    }

    private static boolean a(int[] iArr, int i2) {
        for (int i3 : iArr) {
            if (i3 == i2) {
                return true;
            }
        }
        return false;
    }

    private ColorStateList b(Context context) {
        return f(context, bj.a(context, C0026a.colorButtonNormal));
    }

    private ColorStateList c(Context context) {
        return f(context, 0);
    }

    private Drawable c(Context context, int i2) {
        if (this.o == null) {
            this.o = new TypedValue();
        }
        TypedValue typedValue = this.o;
        context.getResources().getValue(i2, typedValue, true);
        long a2 = a(typedValue);
        Drawable a3 = a(context, a2);
        if (a3 == null) {
            if (i2 == e.abc_cab_background_top_material) {
                a3 = new LayerDrawable(new Drawable[]{a(context, e.abc_cab_background_internal_bg), a(context, e.abc_cab_background_top_mtrl_alpha)});
            }
            if (a3 != null) {
                a3.setChangingConfigurations(typedValue.changingConfigurations);
                a(context, a2, a3);
            }
        }
        return a3;
    }

    private ColorStateList d(Context context) {
        return f(context, bj.a(context, C0026a.colorAccent));
    }

    /* JADX WARNING: Removed duplicated region for block: B:35:0x008d  */
    /* JADX WARNING: Removed duplicated region for block: B:49:? A[RETURN, SYNTHETIC] */
    private Drawable d(Context context, int i2) {
        Drawable drawable;
        int next;
        if (this.k == null || this.k.isEmpty()) {
            return null;
        }
        if (this.l != null) {
            String str = (String) this.l.a(i2);
            if ("appcompat_skip_skip".equals(str) || (str != null && this.k.get(str) == null)) {
                return null;
            }
        } else {
            this.l = new n<>();
        }
        if (this.o == null) {
            this.o = new TypedValue();
        }
        TypedValue typedValue = this.o;
        Resources resources = context.getResources();
        resources.getValue(i2, typedValue, true);
        long a2 = a(typedValue);
        Drawable a3 = a(context, a2);
        if (a3 != null) {
            return a3;
        }
        if (typedValue.string != null && typedValue.string.toString().endsWith(".xml")) {
            try {
                XmlResourceParser xml = resources.getXml(i2);
                AttributeSet asAttributeSet = Xml.asAttributeSet(xml);
                do {
                    next = xml.next();
                    if (next == 2) {
                        break;
                    }
                } while (next != 1);
                if (next != 2) {
                    throw new XmlPullParserException("No start tag found");
                }
                String name = xml.getName();
                this.l.c(i2, name);
                c cVar = (c) this.k.get(name);
                if (cVar != null) {
                    a3 = cVar.a(context, xml, asAttributeSet, context.getTheme());
                }
                if (a3 != null) {
                    a3.setChangingConfigurations(typedValue.changingConfigurations);
                    if (a(context, a2, a3)) {
                    }
                }
                drawable = a3;
                if (drawable == null) {
                    return drawable;
                }
                this.l.c(i2, "appcompat_skip_skip");
                return drawable;
            } catch (Exception e2) {
                Log.e("AppCompatDrawableManag", "Exception while inflating drawable", e2);
            }
        }
        drawable = a3;
        if (drawable == null) {
        }
    }

    private ColorStateList e(Context context) {
        int[][] iArr = new int[3][];
        int[] iArr2 = new int[3];
        ColorStateList b2 = bj.b(context, C0026a.colorSwitchThumbNormal);
        if (b2 == null || !b2.isStateful()) {
            iArr[0] = bj.f1256a;
            iArr2[0] = bj.c(context, C0026a.colorSwitchThumbNormal);
            iArr[1] = bj.e;
            iArr2[1] = bj.a(context, C0026a.colorControlActivated);
            iArr[2] = bj.h;
            iArr2[2] = bj.a(context, C0026a.colorSwitchThumbNormal);
        } else {
            iArr[0] = bj.f1256a;
            iArr2[0] = b2.getColorForState(iArr[0], 0);
            iArr[1] = bj.e;
            iArr2[1] = bj.a(context, C0026a.colorControlActivated);
            iArr[2] = bj.h;
            iArr2[2] = b2.getDefaultColor();
        }
        return new ColorStateList(iArr, iArr2);
    }

    private ColorStateList e(Context context, int i2) {
        if (this.j == null) {
            return null;
        }
        n nVar = (n) this.j.get(context);
        if (nVar != null) {
            return (ColorStateList) nVar.a(i2);
        }
        return null;
    }

    private ColorStateList f(Context context, int i2) {
        int a2 = bj.a(context, C0026a.colorControlHighlight);
        return new ColorStateList(new int[][]{bj.f1256a, bj.d, bj.f1257b, bj.h}, new int[]{bj.c(context, C0026a.colorButtonNormal), android.support.v4.c.a.a(a2, i2), android.support.v4.c.a.a(a2, i2), i2});
    }

    private void f(Context context) {
        if (!this.p) {
            this.p = true;
            Drawable a2 = a(context, e.abc_vector_test);
            if (a2 == null || !a(a2)) {
                this.p = false;
                throw new IllegalStateException("This app has been built with an incorrect configuration. Please configure your build for VectorDrawableCompat.");
            }
        }
    }

    public Drawable a(Context context, int i2) {
        return a(context, i2, false);
    }

    /* access modifiers changed from: 0000 */
    public Drawable a(Context context, int i2, boolean z) {
        f(context);
        Drawable d2 = d(context, i2);
        if (d2 == null) {
            d2 = c(context, i2);
        }
        if (d2 == null) {
            d2 = android.support.v4.b.a.a(context, i2);
        }
        if (d2 != null) {
            d2 = a(context, i2, z, d2);
        }
        if (d2 != null) {
            am.a(d2);
        }
        return d2;
    }

    /* access modifiers changed from: 0000 */
    public Drawable a(Context context, bt btVar, int i2) {
        Drawable d2 = d(context, i2);
        if (d2 == null) {
            d2 = btVar.a(i2);
        }
        if (d2 != null) {
            return a(context, i2, false, d2);
        }
        return null;
    }

    public void a(Context context) {
        synchronized (this.m) {
            f fVar = (f) this.n.get(context);
            if (fVar != null) {
                fVar.c();
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public ColorStateList b(Context context, int i2) {
        ColorStateList e2 = e(context, i2);
        if (e2 == null) {
            if (i2 == e.abc_edit_text_material) {
                e2 = android.support.v7.c.a.b.a(context, android.support.v7.a.a.c.abc_tint_edittext);
            } else if (i2 == e.abc_switch_track_mtrl_alpha) {
                e2 = android.support.v7.c.a.b.a(context, android.support.v7.a.a.c.abc_tint_switch_track);
            } else if (i2 == e.abc_switch_thumb_material) {
                e2 = e(context);
            } else if (i2 == e.abc_btn_default_mtrl_shape) {
                e2 = b(context);
            } else if (i2 == e.abc_btn_borderless_material) {
                e2 = c(context);
            } else if (i2 == e.abc_btn_colored_material) {
                e2 = d(context);
            } else if (i2 == e.abc_spinner_mtrl_am_alpha || i2 == e.abc_spinner_textfield_background_material) {
                e2 = android.support.v7.c.a.b.a(context, android.support.v7.a.a.c.abc_tint_spinner);
            } else if (a(e, i2)) {
                e2 = bj.b(context, C0026a.colorControlNormal);
            } else if (a(h, i2)) {
                e2 = android.support.v7.c.a.b.a(context, android.support.v7.a.a.c.abc_tint_default);
            } else if (a(i, i2)) {
                e2 = android.support.v7.c.a.b.a(context, android.support.v7.a.a.c.abc_tint_btn_checkable);
            } else if (i2 == e.abc_seekbar_thumb_material) {
                e2 = android.support.v7.c.a.b.a(context, android.support.v7.a.a.c.abc_tint_seek_thumb);
            }
            if (e2 != null) {
                a(context, i2, e2);
            }
        }
        return e2;
    }
}
