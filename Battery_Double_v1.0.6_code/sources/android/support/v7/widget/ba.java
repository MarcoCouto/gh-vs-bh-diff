package android.support.v7.widget;

import android.os.Bundle;
import android.support.v4.i.b;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;

public class ba extends b {

    /* renamed from: a reason: collision with root package name */
    final RecyclerView f1237a;
    final b c = new a(this);

    public static class a extends b {

        /* renamed from: a reason: collision with root package name */
        final ba f1238a;

        public a(ba baVar) {
            this.f1238a = baVar;
        }

        public void a(View view, android.support.v4.i.a.b bVar) {
            super.a(view, bVar);
            if (!this.f1238a.b() && this.f1238a.f1237a.getLayoutManager() != null) {
                this.f1238a.f1237a.getLayoutManager().a(view, bVar);
            }
        }

        public boolean a(View view, int i, Bundle bundle) {
            if (super.a(view, i, bundle)) {
                return true;
            }
            if (this.f1238a.b() || this.f1238a.f1237a.getLayoutManager() == null) {
                return false;
            }
            return this.f1238a.f1237a.getLayoutManager().a(view, i, bundle);
        }
    }

    public ba(RecyclerView recyclerView) {
        this.f1237a = recyclerView;
    }

    public void a(View view, android.support.v4.i.a.b bVar) {
        super.a(view, bVar);
        bVar.a((CharSequence) RecyclerView.class.getName());
        if (!b() && this.f1237a.getLayoutManager() != null) {
            this.f1237a.getLayoutManager().a(bVar);
        }
    }

    public void a(View view, AccessibilityEvent accessibilityEvent) {
        super.a(view, accessibilityEvent);
        accessibilityEvent.setClassName(RecyclerView.class.getName());
        if ((view instanceof RecyclerView) && !b()) {
            RecyclerView recyclerView = (RecyclerView) view;
            if (recyclerView.getLayoutManager() != null) {
                recyclerView.getLayoutManager().a(accessibilityEvent);
            }
        }
    }

    public boolean a(View view, int i, Bundle bundle) {
        if (super.a(view, i, bundle)) {
            return true;
        }
        if (b() || this.f1237a.getLayoutManager() == null) {
            return false;
        }
        return this.f1237a.getLayoutManager().a(i, bundle);
    }

    /* access modifiers changed from: 0000 */
    public boolean b() {
        return this.f1237a.w();
    }

    public b c() {
        return this.c;
    }
}
