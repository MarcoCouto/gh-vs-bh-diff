package android.support.v7.widget;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.support.v7.a.a.C0026a;
import android.text.TextUtils;
import android.text.TextUtils.TruncateAt;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.BaseAdapter;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

public class bg extends HorizontalScrollView implements OnItemSelectedListener {
    private static final Interpolator j = new DecelerateInterpolator();

    /* renamed from: a reason: collision with root package name */
    Runnable f1246a;

    /* renamed from: b reason: collision with root package name */
    at f1247b;
    int c;
    int d;
    private b e;
    private Spinner f;
    private boolean g;
    private int h;
    private int i;

    private class a extends BaseAdapter {
        a() {
        }

        public int getCount() {
            return bg.this.f1247b.getChildCount();
        }

        public Object getItem(int i) {
            return ((c) bg.this.f1247b.getChildAt(i)).b();
        }

        public long getItemId(int i) {
            return (long) i;
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            if (view == null) {
                return bg.this.a((android.support.v7.app.a.c) getItem(i), true);
            }
            ((c) view).a((android.support.v7.app.a.c) getItem(i));
            return view;
        }
    }

    private class b implements OnClickListener {
        b() {
        }

        public void onClick(View view) {
            ((c) view).b().d();
            int childCount = bg.this.f1247b.getChildCount();
            for (int i = 0; i < childCount; i++) {
                View childAt = bg.this.f1247b.getChildAt(i);
                childAt.setSelected(childAt == view);
            }
        }
    }

    private class c extends at {

        /* renamed from: b reason: collision with root package name */
        private final int[] f1253b = {16842964};
        private android.support.v7.app.a.c c;
        private TextView d;
        private ImageView e;
        private View f;

        public c(Context context, android.support.v7.app.a.c cVar, boolean z) {
            super(context, null, C0026a.actionBarTabStyle);
            this.c = cVar;
            bo a2 = bo.a(context, null, this.f1253b, C0026a.actionBarTabStyle, 0);
            if (a2.g(0)) {
                setBackgroundDrawable(a2.a(0));
            }
            a2.a();
            if (z) {
                setGravity(8388627);
            }
            a();
        }

        public void a() {
            android.support.v7.app.a.c cVar = this.c;
            View c2 = cVar.c();
            if (c2 != null) {
                ViewParent parent = c2.getParent();
                if (parent != this) {
                    if (parent != null) {
                        ((ViewGroup) parent).removeView(c2);
                    }
                    addView(c2);
                }
                this.f = c2;
                if (this.d != null) {
                    this.d.setVisibility(8);
                }
                if (this.e != null) {
                    this.e.setVisibility(8);
                    this.e.setImageDrawable(null);
                    return;
                }
                return;
            }
            if (this.f != null) {
                removeView(this.f);
                this.f = null;
            }
            Drawable a2 = cVar.a();
            CharSequence b2 = cVar.b();
            if (a2 != null) {
                if (this.e == null) {
                    q qVar = new q(getContext());
                    android.support.v7.widget.at.a aVar = new android.support.v7.widget.at.a(-2, -2);
                    aVar.h = 16;
                    qVar.setLayoutParams(aVar);
                    addView(qVar, 0);
                    this.e = qVar;
                }
                this.e.setImageDrawable(a2);
                this.e.setVisibility(0);
            } else if (this.e != null) {
                this.e.setVisibility(8);
                this.e.setImageDrawable(null);
            }
            boolean z = !TextUtils.isEmpty(b2);
            if (z) {
                if (this.d == null) {
                    ab abVar = new ab(getContext(), null, C0026a.actionBarTabTextStyle);
                    abVar.setEllipsize(TruncateAt.END);
                    android.support.v7.widget.at.a aVar2 = new android.support.v7.widget.at.a(-2, -2);
                    aVar2.h = 16;
                    abVar.setLayoutParams(aVar2);
                    addView(abVar);
                    this.d = abVar;
                }
                this.d.setText(b2);
                this.d.setVisibility(0);
            } else if (this.d != null) {
                this.d.setVisibility(8);
                this.d.setText(null);
            }
            if (this.e != null) {
                this.e.setContentDescription(cVar.e());
            }
            bq.a(this, z ? null : cVar.e());
        }

        public void a(android.support.v7.app.a.c cVar) {
            this.c = cVar;
            a();
        }

        public android.support.v7.app.a.c b() {
            return this.c;
        }

        public void onInitializeAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
            super.onInitializeAccessibilityEvent(accessibilityEvent);
            accessibilityEvent.setClassName(android.support.v7.app.a.c.class.getName());
        }

        public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo accessibilityNodeInfo) {
            super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo);
            accessibilityNodeInfo.setClassName(android.support.v7.app.a.c.class.getName());
        }

        public void onMeasure(int i, int i2) {
            super.onMeasure(i, i2);
            if (bg.this.c > 0 && getMeasuredWidth() > bg.this.c) {
                super.onMeasure(MeasureSpec.makeMeasureSpec(bg.this.c, 1073741824), i2);
            }
        }

        public void setSelected(boolean z) {
            boolean z2 = isSelected() != z;
            super.setSelected(z);
            if (z2 && z) {
                sendAccessibilityEvent(4);
            }
        }
    }

    private boolean a() {
        return this.f != null && this.f.getParent() == this;
    }

    private void b() {
        if (!a()) {
            if (this.f == null) {
                this.f = d();
            }
            removeView(this.f1247b);
            addView(this.f, new LayoutParams(-2, -1));
            if (this.f.getAdapter() == null) {
                this.f.setAdapter(new a());
            }
            if (this.f1246a != null) {
                removeCallbacks(this.f1246a);
                this.f1246a = null;
            }
            this.f.setSelection(this.i);
        }
    }

    private boolean c() {
        if (a()) {
            removeView(this.f);
            addView(this.f1247b, new LayoutParams(-2, -1));
            setTabSelected(this.f.getSelectedItemPosition());
        }
        return false;
    }

    private Spinner d() {
        y yVar = new y(getContext(), null, C0026a.actionDropDownStyle);
        yVar.setLayoutParams(new android.support.v7.widget.at.a(-2, -1));
        yVar.setOnItemSelectedListener(this);
        return yVar;
    }

    /* access modifiers changed from: 0000 */
    public c a(android.support.v7.app.a.c cVar, boolean z) {
        c cVar2 = new c(getContext(), cVar, z);
        if (z) {
            cVar2.setBackgroundDrawable(null);
            cVar2.setLayoutParams(new AbsListView.LayoutParams(-1, this.h));
        } else {
            cVar2.setFocusable(true);
            if (this.e == null) {
                this.e = new b();
            }
            cVar2.setOnClickListener(this.e);
        }
        return cVar2;
    }

    public void a(int i2) {
        final View childAt = this.f1247b.getChildAt(i2);
        if (this.f1246a != null) {
            removeCallbacks(this.f1246a);
        }
        this.f1246a = new Runnable() {
            public void run() {
                bg.this.smoothScrollTo(childAt.getLeft() - ((bg.this.getWidth() - childAt.getWidth()) / 2), 0);
                bg.this.f1246a = null;
            }
        };
        post(this.f1246a);
    }

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.f1246a != null) {
            post(this.f1246a);
        }
    }

    /* access modifiers changed from: protected */
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        android.support.v7.view.a a2 = android.support.v7.view.a.a(getContext());
        setContentHeight(a2.e());
        this.d = a2.g();
    }

    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.f1246a != null) {
            removeCallbacks(this.f1246a);
        }
    }

    public void onItemSelected(AdapterView<?> adapterView, View view, int i2, long j2) {
        ((c) view).b().d();
    }

    public void onMeasure(int i2, int i3) {
        boolean z = true;
        int mode = MeasureSpec.getMode(i2);
        boolean z2 = mode == 1073741824;
        setFillViewport(z2);
        int childCount = this.f1247b.getChildCount();
        if (childCount <= 1 || !(mode == 1073741824 || mode == Integer.MIN_VALUE)) {
            this.c = -1;
        } else {
            if (childCount > 2) {
                this.c = (int) (((float) MeasureSpec.getSize(i2)) * 0.4f);
            } else {
                this.c = MeasureSpec.getSize(i2) / 2;
            }
            this.c = Math.min(this.c, this.d);
        }
        int makeMeasureSpec = MeasureSpec.makeMeasureSpec(this.h, 1073741824);
        if (z2 || !this.g) {
            z = false;
        }
        if (z) {
            this.f1247b.measure(0, makeMeasureSpec);
            if (this.f1247b.getMeasuredWidth() > MeasureSpec.getSize(i2)) {
                b();
            } else {
                c();
            }
        } else {
            c();
        }
        int measuredWidth = getMeasuredWidth();
        super.onMeasure(i2, makeMeasureSpec);
        int measuredWidth2 = getMeasuredWidth();
        if (z2 && measuredWidth != measuredWidth2) {
            setTabSelected(this.i);
        }
    }

    public void onNothingSelected(AdapterView<?> adapterView) {
    }

    public void setAllowCollapse(boolean z) {
        this.g = z;
    }

    public void setContentHeight(int i2) {
        this.h = i2;
        requestLayout();
    }

    public void setTabSelected(int i2) {
        this.i = i2;
        int childCount = this.f1247b.getChildCount();
        int i3 = 0;
        while (i3 < childCount) {
            View childAt = this.f1247b.getChildAt(i3);
            boolean z = i3 == i2;
            childAt.setSelected(z);
            if (z) {
                a(i2);
            }
            i3++;
        }
        if (this.f != null && i2 >= 0) {
            this.f.setSelection(i2);
        }
    }
}
