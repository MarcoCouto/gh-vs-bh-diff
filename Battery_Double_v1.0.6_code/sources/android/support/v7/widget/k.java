package android.support.v7.widget;

import android.content.Context;
import android.support.v7.c.a.b;
import android.util.AttributeSet;
import android.widget.CheckedTextView;
import android.widget.TextView;

public class k extends CheckedTextView {

    /* renamed from: a reason: collision with root package name */
    private static final int[] f1316a = {16843016};

    /* renamed from: b reason: collision with root package name */
    private final z f1317b;

    public k(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 16843720);
    }

    public k(Context context, AttributeSet attributeSet, int i) {
        super(bl.a(context), attributeSet, i);
        this.f1317b = z.a((TextView) this);
        this.f1317b.a(attributeSet, i);
        this.f1317b.a();
        bo a2 = bo.a(getContext(), attributeSet, f1316a, i, 0);
        setCheckMarkDrawable(a2.a(0));
        a2.a();
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        if (this.f1317b != null) {
            this.f1317b.a();
        }
    }

    public void setCheckMarkDrawable(int i) {
        setCheckMarkDrawable(b.b(getContext(), i));
    }

    public void setTextAppearance(Context context, int i) {
        super.setTextAppearance(context, i);
        if (this.f1317b != null) {
            this.f1317b.a(context, i);
        }
    }
}
