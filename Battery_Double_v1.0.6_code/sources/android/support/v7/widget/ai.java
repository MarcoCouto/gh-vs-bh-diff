package android.support.v7.widget;

import android.support.v7.widget.RecyclerView.w;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import java.util.ArrayList;
import java.util.List;

class ai {

    /* renamed from: a reason: collision with root package name */
    final b f1170a;

    /* renamed from: b reason: collision with root package name */
    final a f1171b = new a();
    final List<View> c = new ArrayList();

    static class a {

        /* renamed from: a reason: collision with root package name */
        long f1172a = 0;

        /* renamed from: b reason: collision with root package name */
        a f1173b;

        a() {
        }

        private void b() {
            if (this.f1173b == null) {
                this.f1173b = new a();
            }
        }

        /* access modifiers changed from: 0000 */
        public void a() {
            this.f1172a = 0;
            if (this.f1173b != null) {
                this.f1173b.a();
            }
        }

        /* access modifiers changed from: 0000 */
        public void a(int i) {
            if (i >= 64) {
                b();
                this.f1173b.a(i - 64);
                return;
            }
            this.f1172a |= 1 << i;
        }

        /* access modifiers changed from: 0000 */
        public void a(int i, boolean z) {
            if (i >= 64) {
                b();
                this.f1173b.a(i - 64, z);
                return;
            }
            boolean z2 = (this.f1172a & Long.MIN_VALUE) != 0;
            long j = (1 << i) - 1;
            this.f1172a = (((j ^ -1) & this.f1172a) << 1) | (this.f1172a & j);
            if (z) {
                a(i);
            } else {
                b(i);
            }
            if (z2 || this.f1173b != null) {
                b();
                this.f1173b.a(0, z2);
            }
        }

        /* access modifiers changed from: 0000 */
        public void b(int i) {
            if (i < 64) {
                this.f1172a &= (1 << i) ^ -1;
            } else if (this.f1173b != null) {
                this.f1173b.b(i - 64);
            }
        }

        /* access modifiers changed from: 0000 */
        public boolean c(int i) {
            if (i < 64) {
                return (this.f1172a & (1 << i)) != 0;
            }
            b();
            return this.f1173b.c(i - 64);
        }

        /* access modifiers changed from: 0000 */
        public boolean d(int i) {
            if (i >= 64) {
                b();
                return this.f1173b.d(i - 64);
            }
            long j = 1 << i;
            boolean z = (this.f1172a & j) != 0;
            this.f1172a &= j ^ -1;
            long j2 = j - 1;
            this.f1172a = Long.rotateRight((j2 ^ -1) & this.f1172a, 1) | (this.f1172a & j2);
            if (this.f1173b == null) {
                return z;
            }
            if (this.f1173b.c(0)) {
                a(63);
            }
            this.f1173b.d(0);
            return z;
        }

        /* access modifiers changed from: 0000 */
        public int e(int i) {
            return this.f1173b == null ? i >= 64 ? Long.bitCount(this.f1172a) : Long.bitCount(this.f1172a & ((1 << i) - 1)) : i < 64 ? Long.bitCount(this.f1172a & ((1 << i) - 1)) : this.f1173b.e(i - 64) + Long.bitCount(this.f1172a);
        }

        public String toString() {
            return this.f1173b == null ? Long.toBinaryString(this.f1172a) : this.f1173b.toString() + "xx" + Long.toBinaryString(this.f1172a);
        }
    }

    interface b {
        int a();

        int a(View view);

        void a(int i);

        void a(View view, int i);

        void a(View view, int i, LayoutParams layoutParams);

        w b(View view);

        View b(int i);

        void b();

        void c(int i);

        void c(View view);

        void d(View view);
    }

    ai(b bVar) {
        this.f1170a = bVar;
    }

    private int f(int i) {
        if (i < 0) {
            return -1;
        }
        int a2 = this.f1170a.a();
        int i2 = i;
        while (i2 < a2) {
            int e = i - (i2 - this.f1171b.e(i2));
            if (e == 0) {
                while (this.f1171b.c(i2)) {
                    i2++;
                }
                return i2;
            }
            i2 += e;
        }
        return -1;
    }

    private void g(View view) {
        this.c.add(view);
        this.f1170a.c(view);
    }

    private boolean h(View view) {
        if (!this.c.remove(view)) {
            return false;
        }
        this.f1170a.d(view);
        return true;
    }

    /* access modifiers changed from: 0000 */
    public void a() {
        this.f1171b.a();
        for (int size = this.c.size() - 1; size >= 0; size--) {
            this.f1170a.d((View) this.c.get(size));
            this.c.remove(size);
        }
        this.f1170a.b();
    }

    /* access modifiers changed from: 0000 */
    public void a(int i) {
        int f = f(i);
        View b2 = this.f1170a.b(f);
        if (b2 != null) {
            if (this.f1171b.d(f)) {
                h(b2);
            }
            this.f1170a.a(f);
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(View view) {
        int a2 = this.f1170a.a(view);
        if (a2 >= 0) {
            if (this.f1171b.d(a2)) {
                h(view);
            }
            this.f1170a.a(a2);
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(View view, int i, LayoutParams layoutParams, boolean z) {
        int f = i < 0 ? this.f1170a.a() : f(i);
        this.f1171b.a(f, z);
        if (z) {
            g(view);
        }
        this.f1170a.a(view, f, layoutParams);
    }

    /* access modifiers changed from: 0000 */
    public void a(View view, int i, boolean z) {
        int f = i < 0 ? this.f1170a.a() : f(i);
        this.f1171b.a(f, z);
        if (z) {
            g(view);
        }
        this.f1170a.a(view, f);
    }

    /* access modifiers changed from: 0000 */
    public void a(View view, boolean z) {
        a(view, -1, z);
    }

    /* access modifiers changed from: 0000 */
    public int b() {
        return this.f1170a.a() - this.c.size();
    }

    /* access modifiers changed from: 0000 */
    public int b(View view) {
        int a2 = this.f1170a.a(view);
        if (a2 != -1 && !this.f1171b.c(a2)) {
            return a2 - this.f1171b.e(a2);
        }
        return -1;
    }

    /* access modifiers changed from: 0000 */
    public View b(int i) {
        return this.f1170a.b(f(i));
    }

    /* access modifiers changed from: 0000 */
    public int c() {
        return this.f1170a.a();
    }

    /* access modifiers changed from: 0000 */
    public View c(int i) {
        int size = this.c.size();
        for (int i2 = 0; i2 < size; i2++) {
            View view = (View) this.c.get(i2);
            w b2 = this.f1170a.b(view);
            if (b2.d() == i && !b2.n() && !b2.q()) {
                return view;
            }
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    public boolean c(View view) {
        return this.c.contains(view);
    }

    /* access modifiers changed from: 0000 */
    public View d(int i) {
        return this.f1170a.b(i);
    }

    /* access modifiers changed from: 0000 */
    public void d(View view) {
        int a2 = this.f1170a.a(view);
        if (a2 < 0) {
            throw new IllegalArgumentException("view is not a child, cannot hide " + view);
        }
        this.f1171b.a(a2);
        g(view);
    }

    /* access modifiers changed from: 0000 */
    public void e(int i) {
        int f = f(i);
        this.f1171b.d(f);
        this.f1170a.c(f);
    }

    /* access modifiers changed from: 0000 */
    public void e(View view) {
        int a2 = this.f1170a.a(view);
        if (a2 < 0) {
            throw new IllegalArgumentException("view is not a child, cannot hide " + view);
        } else if (!this.f1171b.c(a2)) {
            throw new RuntimeException("trying to unhide a view that was not hidden" + view);
        } else {
            this.f1171b.b(a2);
            h(view);
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean f(View view) {
        int a2 = this.f1170a.a(view);
        if (a2 == -1) {
            if (h(view)) {
            }
            return true;
        } else if (!this.f1171b.c(a2)) {
            return false;
        } else {
            this.f1171b.d(a2);
            if (!h(view)) {
            }
            this.f1170a.a(a2);
            return true;
        }
    }

    public String toString() {
        return this.f1171b.toString() + ", hidden list:" + this.c.size();
    }
}
