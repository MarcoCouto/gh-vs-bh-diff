package android.support.v7.widget;

class be {

    /* renamed from: a reason: collision with root package name */
    private int f1244a = 0;

    /* renamed from: b reason: collision with root package name */
    private int f1245b = 0;
    private int c = Integer.MIN_VALUE;
    private int d = Integer.MIN_VALUE;
    private int e = 0;
    private int f = 0;
    private boolean g = false;
    private boolean h = false;

    be() {
    }

    public int a() {
        return this.f1244a;
    }

    public void a(int i, int i2) {
        this.c = i;
        this.d = i2;
        this.h = true;
        if (this.g) {
            if (i2 != Integer.MIN_VALUE) {
                this.f1244a = i2;
            }
            if (i != Integer.MIN_VALUE) {
                this.f1245b = i;
                return;
            }
            return;
        }
        if (i != Integer.MIN_VALUE) {
            this.f1244a = i;
        }
        if (i2 != Integer.MIN_VALUE) {
            this.f1245b = i2;
        }
    }

    public void a(boolean z) {
        if (z != this.g) {
            this.g = z;
            if (!this.h) {
                this.f1244a = this.e;
                this.f1245b = this.f;
            } else if (z) {
                this.f1244a = this.d != Integer.MIN_VALUE ? this.d : this.e;
                this.f1245b = this.c != Integer.MIN_VALUE ? this.c : this.f;
            } else {
                this.f1244a = this.c != Integer.MIN_VALUE ? this.c : this.e;
                this.f1245b = this.d != Integer.MIN_VALUE ? this.d : this.f;
            }
        }
    }

    public int b() {
        return this.f1245b;
    }

    public void b(int i, int i2) {
        this.h = false;
        if (i != Integer.MIN_VALUE) {
            this.e = i;
            this.f1244a = i;
        }
        if (i2 != Integer.MIN_VALUE) {
            this.f = i2;
            this.f1245b = i2;
        }
    }

    public int c() {
        return this.g ? this.f1245b : this.f1244a;
    }

    public int d() {
        return this.g ? this.f1244a : this.f1245b;
    }
}
