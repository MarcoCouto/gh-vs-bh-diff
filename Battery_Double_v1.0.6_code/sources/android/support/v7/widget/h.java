package android.support.v7.widget;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.support.v4.i.t;
import android.support.v7.a.a.j;
import android.util.AttributeSet;
import android.view.View;

class h {

    /* renamed from: a reason: collision with root package name */
    private final View f1312a;

    /* renamed from: b reason: collision with root package name */
    private final m f1313b;
    private int c = -1;
    private bm d;
    private bm e;
    private bm f;

    h(View view) {
        this.f1312a = view;
        this.f1313b = m.a();
    }

    private boolean b(Drawable drawable) {
        if (this.f == null) {
            this.f = new bm();
        }
        bm bmVar = this.f;
        bmVar.a();
        ColorStateList r = t.r(this.f1312a);
        if (r != null) {
            bmVar.d = true;
            bmVar.f1260a = r;
        }
        Mode s = t.s(this.f1312a);
        if (s != null) {
            bmVar.c = true;
            bmVar.f1261b = s;
        }
        if (!bmVar.d && !bmVar.c) {
            return false;
        }
        m.a(drawable, bmVar, this.f1312a.getDrawableState());
        return true;
    }

    private boolean d() {
        int i = VERSION.SDK_INT;
        return i > 21 ? this.d != null : i == 21;
    }

    /* access modifiers changed from: 0000 */
    public ColorStateList a() {
        if (this.e != null) {
            return this.e.f1260a;
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    public void a(int i) {
        this.c = i;
        b(this.f1313b != null ? this.f1313b.b(this.f1312a.getContext(), i) : null);
        c();
    }

    /* access modifiers changed from: 0000 */
    public void a(ColorStateList colorStateList) {
        if (this.e == null) {
            this.e = new bm();
        }
        this.e.f1260a = colorStateList;
        this.e.d = true;
        c();
    }

    /* access modifiers changed from: 0000 */
    public void a(Mode mode) {
        if (this.e == null) {
            this.e = new bm();
        }
        this.e.f1261b = mode;
        this.e.c = true;
        c();
    }

    /* access modifiers changed from: 0000 */
    public void a(Drawable drawable) {
        this.c = -1;
        b((ColorStateList) null);
        c();
    }

    /* access modifiers changed from: 0000 */
    public void a(AttributeSet attributeSet, int i) {
        bo a2 = bo.a(this.f1312a.getContext(), attributeSet, j.ViewBackgroundHelper, i, 0);
        try {
            if (a2.g(j.ViewBackgroundHelper_android_background)) {
                this.c = a2.g(j.ViewBackgroundHelper_android_background, -1);
                ColorStateList b2 = this.f1313b.b(this.f1312a.getContext(), this.c);
                if (b2 != null) {
                    b(b2);
                }
            }
            if (a2.g(j.ViewBackgroundHelper_backgroundTint)) {
                t.a(this.f1312a, a2.e(j.ViewBackgroundHelper_backgroundTint));
            }
            if (a2.g(j.ViewBackgroundHelper_backgroundTintMode)) {
                t.a(this.f1312a, am.a(a2.a(j.ViewBackgroundHelper_backgroundTintMode, -1), null));
            }
        } finally {
            a2.a();
        }
    }

    /* access modifiers changed from: 0000 */
    public Mode b() {
        if (this.e != null) {
            return this.e.f1261b;
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    public void b(ColorStateList colorStateList) {
        if (colorStateList != null) {
            if (this.d == null) {
                this.d = new bm();
            }
            this.d.f1260a = colorStateList;
            this.d.d = true;
        } else {
            this.d = null;
        }
        c();
    }

    /* access modifiers changed from: 0000 */
    public void c() {
        Drawable background = this.f1312a.getBackground();
        if (background == null) {
            return;
        }
        if (d() && b(background)) {
            return;
        }
        if (this.e != null) {
            m.a(background, this.e, this.f1312a.getDrawableState());
        } else if (this.d != null) {
            m.a(background, this.d, this.f1312a.getDrawableState());
        }
    }
}
