package android.support.v7.widget;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.v7.widget.RecyclerView.h;
import android.support.v7.widget.RecyclerView.i;
import android.support.v7.widget.RecyclerView.o;
import android.support.v7.widget.RecyclerView.t;
import android.support.v7.widget.RecyclerView.w;
import android.support.v7.widget.a.a.e;
import android.util.AttributeSet;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;
import java.util.List;

public class LinearLayoutManager extends h implements e {

    /* renamed from: a reason: collision with root package name */
    private c f1056a;

    /* renamed from: b reason: collision with root package name */
    private boolean f1057b;
    private boolean c;
    private boolean d;
    private boolean e;
    private boolean f;
    private final b g;
    private int h;
    int i;
    az j;
    boolean k;
    int l;
    int m;
    d n;
    final a o;

    class a {

        /* renamed from: a reason: collision with root package name */
        int f1058a;

        /* renamed from: b reason: collision with root package name */
        int f1059b;
        boolean c;
        boolean d;

        a() {
            a();
        }

        /* access modifiers changed from: 0000 */
        public void a() {
            this.f1058a = -1;
            this.f1059b = Integer.MIN_VALUE;
            this.c = false;
            this.d = false;
        }

        public void a(View view) {
            int b2 = LinearLayoutManager.this.j.b();
            if (b2 >= 0) {
                b(view);
                return;
            }
            this.f1058a = LinearLayoutManager.this.d(view);
            if (this.c) {
                int d2 = (LinearLayoutManager.this.j.d() - b2) - LinearLayoutManager.this.j.b(view);
                this.f1059b = LinearLayoutManager.this.j.d() - d2;
                if (d2 > 0) {
                    int e2 = this.f1059b - LinearLayoutManager.this.j.e(view);
                    int c2 = LinearLayoutManager.this.j.c();
                    int min = e2 - (c2 + Math.min(LinearLayoutManager.this.j.a(view) - c2, 0));
                    if (min < 0) {
                        this.f1059b = Math.min(d2, -min) + this.f1059b;
                        return;
                    }
                    return;
                }
                return;
            }
            int a2 = LinearLayoutManager.this.j.a(view);
            int c3 = a2 - LinearLayoutManager.this.j.c();
            this.f1059b = a2;
            if (c3 > 0) {
                int d3 = (LinearLayoutManager.this.j.d() - Math.min(0, (LinearLayoutManager.this.j.d() - b2) - LinearLayoutManager.this.j.b(view))) - (a2 + LinearLayoutManager.this.j.e(view));
                if (d3 < 0) {
                    this.f1059b -= Math.min(c3, -d3);
                }
            }
        }

        /* access modifiers changed from: 0000 */
        public boolean a(View view, t tVar) {
            i iVar = (i) view.getLayoutParams();
            return !iVar.d() && iVar.f() >= 0 && iVar.f() < tVar.e();
        }

        /* access modifiers changed from: 0000 */
        public void b() {
            this.f1059b = this.c ? LinearLayoutManager.this.j.d() : LinearLayoutManager.this.j.c();
        }

        public void b(View view) {
            if (this.c) {
                this.f1059b = LinearLayoutManager.this.j.b(view) + LinearLayoutManager.this.j.b();
            } else {
                this.f1059b = LinearLayoutManager.this.j.a(view);
            }
            this.f1058a = LinearLayoutManager.this.d(view);
        }

        public String toString() {
            return "AnchorInfo{mPosition=" + this.f1058a + ", mCoordinate=" + this.f1059b + ", mLayoutFromEnd=" + this.c + ", mValid=" + this.d + '}';
        }
    }

    protected static class b {

        /* renamed from: a reason: collision with root package name */
        public int f1060a;

        /* renamed from: b reason: collision with root package name */
        public boolean f1061b;
        public boolean c;
        public boolean d;

        protected b() {
        }

        /* access modifiers changed from: 0000 */
        public void a() {
            this.f1060a = 0;
            this.f1061b = false;
            this.c = false;
            this.d = false;
        }
    }

    static class c {

        /* renamed from: a reason: collision with root package name */
        boolean f1062a = true;

        /* renamed from: b reason: collision with root package name */
        int f1063b;
        int c;
        int d;
        int e;
        int f;
        int g;
        int h = 0;
        boolean i = false;
        int j;
        List<w> k = null;
        boolean l;

        c() {
        }

        private View b() {
            int size = this.k.size();
            for (int i2 = 0; i2 < size; i2++) {
                View view = ((w) this.k.get(i2)).f1102a;
                i iVar = (i) view.getLayoutParams();
                if (!iVar.d() && this.d == iVar.f()) {
                    a(view);
                    return view;
                }
            }
            return null;
        }

        /* access modifiers changed from: 0000 */
        public View a(o oVar) {
            if (this.k != null) {
                return b();
            }
            View c2 = oVar.c(this.d);
            this.d += this.e;
            return c2;
        }

        public void a() {
            a((View) null);
        }

        public void a(View view) {
            View b2 = b(view);
            if (b2 == null) {
                this.d = -1;
            } else {
                this.d = ((i) b2.getLayoutParams()).f();
            }
        }

        /* access modifiers changed from: 0000 */
        public boolean a(t tVar) {
            return this.d >= 0 && this.d < tVar.e();
        }

        public View b(View view) {
            int i2;
            View view2;
            int size = this.k.size();
            View view3 = null;
            int i3 = Integer.MAX_VALUE;
            int i4 = 0;
            while (i4 < size) {
                View view4 = ((w) this.k.get(i4)).f1102a;
                i iVar = (i) view4.getLayoutParams();
                if (view4 != view) {
                    if (iVar.d()) {
                        i2 = i3;
                        view2 = view3;
                    } else {
                        i2 = (iVar.f() - this.d) * this.e;
                        if (i2 < 0) {
                            i2 = i3;
                            view2 = view3;
                        } else if (i2 < i3) {
                            if (i2 == 0) {
                                return view4;
                            }
                            view2 = view4;
                        }
                    }
                    i4++;
                    view3 = view2;
                    i3 = i2;
                }
                i2 = i3;
                view2 = view3;
                i4++;
                view3 = view2;
                i3 = i2;
            }
            return view3;
        }
    }

    public static class d implements Parcelable {
        public static final Creator<d> CREATOR = new Creator<d>() {
            /* renamed from: a */
            public d createFromParcel(Parcel parcel) {
                return new d(parcel);
            }

            /* renamed from: a */
            public d[] newArray(int i) {
                return new d[i];
            }
        };

        /* renamed from: a reason: collision with root package name */
        int f1064a;

        /* renamed from: b reason: collision with root package name */
        int f1065b;
        boolean c;

        public d() {
        }

        d(Parcel parcel) {
            boolean z = true;
            this.f1064a = parcel.readInt();
            this.f1065b = parcel.readInt();
            if (parcel.readInt() != 1) {
                z = false;
            }
            this.c = z;
        }

        public d(d dVar) {
            this.f1064a = dVar.f1064a;
            this.f1065b = dVar.f1065b;
            this.c = dVar.c;
        }

        /* access modifiers changed from: 0000 */
        public boolean a() {
            return this.f1064a >= 0;
        }

        /* access modifiers changed from: 0000 */
        public void b() {
            this.f1064a = -1;
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(this.f1064a);
            parcel.writeInt(this.f1065b);
            parcel.writeInt(this.c ? 1 : 0);
        }
    }

    public LinearLayoutManager(Context context) {
        this(context, 1, false);
    }

    public LinearLayoutManager(Context context, int i2, boolean z) {
        this.c = false;
        this.k = false;
        this.d = false;
        this.e = true;
        this.l = -1;
        this.m = Integer.MIN_VALUE;
        this.n = null;
        this.o = new a();
        this.g = new b();
        this.h = 2;
        b(i2);
        b(z);
        c(true);
    }

    public LinearLayoutManager(Context context, AttributeSet attributeSet, int i2, int i3) {
        this.c = false;
        this.k = false;
        this.d = false;
        this.e = true;
        this.l = -1;
        this.m = Integer.MIN_VALUE;
        this.n = null;
        this.o = new a();
        this.g = new b();
        this.h = 2;
        android.support.v7.widget.RecyclerView.h.b a2 = a(context, attributeSet, i2, i3);
        b(a2.f1084a);
        b(a2.c);
        a(a2.d);
        c(true);
    }

    private void J() {
        boolean z = true;
        if (this.i == 1 || !g()) {
            this.k = this.c;
            return;
        }
        if (this.c) {
            z = false;
        }
        this.k = z;
    }

    private View K() {
        return h(this.k ? u() - 1 : 0);
    }

    private View L() {
        return h(this.k ? 0 : u() - 1);
    }

    private int a(int i2, o oVar, t tVar, boolean z) {
        int d2 = this.j.d() - i2;
        if (d2 <= 0) {
            return 0;
        }
        int i3 = -c(-d2, oVar, tVar);
        int i4 = i2 + i3;
        if (!z) {
            return i3;
        }
        int d3 = this.j.d() - i4;
        if (d3 <= 0) {
            return i3;
        }
        this.j.a(d3);
        return i3 + d3;
    }

    private View a(boolean z, boolean z2) {
        return this.k ? a(u() - 1, -1, z, z2) : a(0, u(), z, z2);
    }

    private void a(int i2, int i3) {
        this.f1056a.c = this.j.d() - i3;
        this.f1056a.e = this.k ? -1 : 1;
        this.f1056a.d = i2;
        this.f1056a.f = 1;
        this.f1056a.f1063b = i3;
        this.f1056a.g = Integer.MIN_VALUE;
    }

    private void a(int i2, int i3, boolean z, t tVar) {
        int c2;
        int i4 = -1;
        int i5 = 1;
        this.f1056a.l = j();
        this.f1056a.h = b(tVar);
        this.f1056a.f = i2;
        if (i2 == 1) {
            this.f1056a.h += this.j.g();
            View L = L();
            c cVar = this.f1056a;
            if (!this.k) {
                i4 = 1;
            }
            cVar.e = i4;
            this.f1056a.d = d(L) + this.f1056a.e;
            this.f1056a.f1063b = this.j.b(L);
            c2 = this.j.b(L) - this.j.d();
        } else {
            View K = K();
            this.f1056a.h += this.j.c();
            c cVar2 = this.f1056a;
            if (!this.k) {
                i5 = -1;
            }
            cVar2.e = i5;
            this.f1056a.d = d(K) + this.f1056a.e;
            this.f1056a.f1063b = this.j.a(K);
            c2 = (-this.j.a(K)) + this.j.c();
        }
        this.f1056a.c = i3;
        if (z) {
            this.f1056a.c -= c2;
        }
        this.f1056a.g = c2;
    }

    private void a(a aVar) {
        a(aVar.f1058a, aVar.f1059b);
    }

    private void a(o oVar, int i2) {
        if (i2 >= 0) {
            int u = u();
            if (this.k) {
                for (int i3 = u - 1; i3 >= 0; i3--) {
                    View h2 = h(i3);
                    if (this.j.b(h2) > i2 || this.j.c(h2) > i2) {
                        a(oVar, u - 1, i3);
                        return;
                    }
                }
                return;
            }
            for (int i4 = 0; i4 < u; i4++) {
                View h3 = h(i4);
                if (this.j.b(h3) > i2 || this.j.c(h3) > i2) {
                    a(oVar, 0, i4);
                    return;
                }
            }
        }
    }

    private void a(o oVar, int i2, int i3) {
        if (i2 != i3) {
            if (i3 > i2) {
                for (int i4 = i3 - 1; i4 >= i2; i4--) {
                    a(i4, oVar);
                }
                return;
            }
            while (i2 > i3) {
                a(i2, oVar);
                i2--;
            }
        }
    }

    private void a(o oVar, c cVar) {
        if (cVar.f1062a && !cVar.l) {
            if (cVar.f == -1) {
                b(oVar, cVar.g);
            } else {
                a(oVar, cVar.g);
            }
        }
    }

    private void a(o oVar, t tVar, a aVar) {
        if (!a(tVar, aVar) && !b(oVar, tVar, aVar)) {
            aVar.b();
            aVar.f1058a = this.d ? tVar.e() - 1 : 0;
        }
    }

    private boolean a(t tVar, a aVar) {
        boolean z = false;
        if (tVar.a() || this.l == -1) {
            return false;
        }
        if (this.l < 0 || this.l >= tVar.e()) {
            this.l = -1;
            this.m = Integer.MIN_VALUE;
            return false;
        }
        aVar.f1058a = this.l;
        if (this.n != null && this.n.a()) {
            aVar.c = this.n.c;
            if (aVar.c) {
                aVar.f1059b = this.j.d() - this.n.f1065b;
                return true;
            }
            aVar.f1059b = this.j.c() + this.n.f1065b;
            return true;
        } else if (this.m == Integer.MIN_VALUE) {
            View c2 = c(this.l);
            if (c2 == null) {
                if (u() > 0) {
                    if ((this.l < d(h(0))) == this.k) {
                        z = true;
                    }
                    aVar.c = z;
                }
                aVar.b();
                return true;
            } else if (this.j.e(c2) > this.j.f()) {
                aVar.b();
                return true;
            } else if (this.j.a(c2) - this.j.c() < 0) {
                aVar.f1059b = this.j.c();
                aVar.c = false;
                return true;
            } else if (this.j.d() - this.j.b(c2) < 0) {
                aVar.f1059b = this.j.d();
                aVar.c = true;
                return true;
            } else {
                aVar.f1059b = aVar.c ? this.j.b(c2) + this.j.b() : this.j.a(c2);
                return true;
            }
        } else {
            aVar.c = this.k;
            if (this.k) {
                aVar.f1059b = this.j.d() - this.m;
                return true;
            }
            aVar.f1059b = this.j.c() + this.m;
            return true;
        }
    }

    private int b(int i2, o oVar, t tVar, boolean z) {
        int c2 = i2 - this.j.c();
        if (c2 <= 0) {
            return 0;
        }
        int i3 = -c(c2, oVar, tVar);
        int i4 = i2 + i3;
        if (!z) {
            return i3;
        }
        int c3 = i4 - this.j.c();
        if (c3 <= 0) {
            return i3;
        }
        this.j.a(-c3);
        return i3 - c3;
    }

    private View b(boolean z, boolean z2) {
        return this.k ? a(0, u(), z, z2) : a(u() - 1, -1, z, z2);
    }

    private void b(a aVar) {
        h(aVar.f1058a, aVar.f1059b);
    }

    private void b(o oVar, int i2) {
        int u = u();
        if (i2 >= 0) {
            int e2 = this.j.e() - i2;
            if (this.k) {
                for (int i3 = 0; i3 < u; i3++) {
                    View h2 = h(i3);
                    if (this.j.a(h2) < e2 || this.j.d(h2) < e2) {
                        a(oVar, 0, i3);
                        return;
                    }
                }
                return;
            }
            for (int i4 = u - 1; i4 >= 0; i4--) {
                View h3 = h(i4);
                if (this.j.a(h3) < e2 || this.j.d(h3) < e2) {
                    a(oVar, u - 1, i4);
                    return;
                }
            }
        }
    }

    private void b(o oVar, t tVar, int i2, int i3) {
        int e2;
        int i4;
        if (tVar.b() && u() != 0 && !tVar.a() && b()) {
            int i5 = 0;
            int i6 = 0;
            List<w> c2 = oVar.c();
            int size = c2.size();
            int d2 = d(h(0));
            int i7 = 0;
            while (i7 < size) {
                w wVar = (w) c2.get(i7);
                if (wVar.q()) {
                    e2 = i6;
                    i4 = i5;
                } else {
                    if (((wVar.d() < d2) != this.k ? (char) 65535 : 1) == 65535) {
                        i4 = this.j.e(wVar.f1102a) + i5;
                        e2 = i6;
                    } else {
                        e2 = this.j.e(wVar.f1102a) + i6;
                        i4 = i5;
                    }
                }
                i7++;
                i5 = i4;
                i6 = e2;
            }
            this.f1056a.k = c2;
            if (i5 > 0) {
                h(d(K()), i2);
                this.f1056a.h = i5;
                this.f1056a.c = 0;
                this.f1056a.a();
                a(oVar, this.f1056a, tVar, false);
            }
            if (i6 > 0) {
                a(d(L()), i3);
                this.f1056a.h = i6;
                this.f1056a.c = 0;
                this.f1056a.a();
                a(oVar, this.f1056a, tVar, false);
            }
            this.f1056a.k = null;
        }
    }

    private boolean b(o oVar, t tVar, a aVar) {
        boolean z = false;
        if (u() == 0) {
            return false;
        }
        View D = D();
        if (D != null && aVar.a(D, tVar)) {
            aVar.a(D);
            return true;
        } else if (this.f1057b != this.d) {
            return false;
        } else {
            View g2 = aVar.c ? f(oVar, tVar) : g(oVar, tVar);
            if (g2 == null) {
                return false;
            }
            aVar.b(g2);
            if (!tVar.a() && b()) {
                if (this.j.a(g2) >= this.j.d() || this.j.b(g2) < this.j.c()) {
                    z = true;
                }
                if (z) {
                    aVar.f1059b = aVar.c ? this.j.d() : this.j.c();
                }
            }
            return true;
        }
    }

    private View f(o oVar, t tVar) {
        return this.k ? h(oVar, tVar) : i(oVar, tVar);
    }

    private View g(o oVar, t tVar) {
        return this.k ? i(oVar, tVar) : h(oVar, tVar);
    }

    private View h(o oVar, t tVar) {
        return a(oVar, tVar, 0, u(), tVar.e());
    }

    private void h(int i2, int i3) {
        this.f1056a.c = i3 - this.j.c();
        this.f1056a.d = i2;
        this.f1056a.e = this.k ? 1 : -1;
        this.f1056a.f = -1;
        this.f1056a.f1063b = i3;
        this.f1056a.g = Integer.MIN_VALUE;
    }

    private int i(t tVar) {
        boolean z = false;
        if (u() == 0) {
            return 0;
        }
        h();
        az azVar = this.j;
        View a2 = a(!this.e, true);
        if (!this.e) {
            z = true;
        }
        return bf.a(tVar, azVar, a2, b(z, true), this, this.e, this.k);
    }

    private View i(o oVar, t tVar) {
        return a(oVar, tVar, u() - 1, -1, tVar.e());
    }

    private int j(t tVar) {
        boolean z = false;
        if (u() == 0) {
            return 0;
        }
        h();
        az azVar = this.j;
        View a2 = a(!this.e, true);
        if (!this.e) {
            z = true;
        }
        return bf.a(tVar, azVar, a2, b(z, true), this, this.e);
    }

    private View j(o oVar, t tVar) {
        return this.k ? l(oVar, tVar) : m(oVar, tVar);
    }

    private int k(t tVar) {
        boolean z = false;
        if (u() == 0) {
            return 0;
        }
        h();
        az azVar = this.j;
        View a2 = a(!this.e, true);
        if (!this.e) {
            z = true;
        }
        return bf.b(tVar, azVar, a2, b(z, true), this, this.e);
    }

    private View k(o oVar, t tVar) {
        return this.k ? m(oVar, tVar) : l(oVar, tVar);
    }

    private View l(o oVar, t tVar) {
        return c(0, u());
    }

    private View m(o oVar, t tVar) {
        return c(u() - 1, -1);
    }

    public int a(int i2, o oVar, t tVar) {
        if (this.i == 1) {
            return 0;
        }
        return c(i2, oVar, tVar);
    }

    /* access modifiers changed from: 0000 */
    public int a(o oVar, c cVar, t tVar, boolean z) {
        int i2 = cVar.c;
        if (cVar.g != Integer.MIN_VALUE) {
            if (cVar.c < 0) {
                cVar.g += cVar.c;
            }
            a(oVar, cVar);
        }
        int i3 = cVar.c + cVar.h;
        b bVar = this.g;
        while (true) {
            if ((!cVar.l && i3 <= 0) || !cVar.a(tVar)) {
                break;
            }
            bVar.a();
            a(oVar, tVar, cVar, bVar);
            if (!bVar.f1061b) {
                cVar.f1063b += bVar.f1060a * cVar.f;
                if (!bVar.c || this.f1056a.k != null || !tVar.a()) {
                    cVar.c -= bVar.f1060a;
                    i3 -= bVar.f1060a;
                }
                if (cVar.g != Integer.MIN_VALUE) {
                    cVar.g += bVar.f1060a;
                    if (cVar.c < 0) {
                        cVar.g += cVar.c;
                    }
                    a(oVar, cVar);
                }
                if (z && bVar.d) {
                    break;
                }
            } else {
                break;
            }
        }
        return i2 - cVar.c;
    }

    public i a() {
        return new i(-2, -2);
    }

    /* access modifiers changed from: 0000 */
    public View a(int i2, int i3, boolean z, boolean z2) {
        int i4 = 320;
        h();
        int i5 = z ? 24579 : 320;
        if (!z2) {
            i4 = 0;
        }
        return this.i == 0 ? this.r.a(i2, i3, i5, i4) : this.s.a(i2, i3, i5, i4);
    }

    /* access modifiers changed from: 0000 */
    public View a(o oVar, t tVar, int i2, int i3, int i4) {
        View view;
        View view2 = null;
        h();
        int c2 = this.j.c();
        int d2 = this.j.d();
        int i5 = i3 > i2 ? 1 : -1;
        View view3 = null;
        while (i2 != i3) {
            View h2 = h(i2);
            int d3 = d(h2);
            if (d3 >= 0 && d3 < i4) {
                if (((i) h2.getLayoutParams()).d()) {
                    if (view3 == null) {
                        view = view2;
                        i2 += i5;
                        view2 = view;
                        view3 = h2;
                    }
                } else if (this.j.a(h2) < d2 && this.j.b(h2) >= c2) {
                    return h2;
                } else {
                    if (view2 == null) {
                        view = h2;
                        h2 = view3;
                        i2 += i5;
                        view2 = view;
                        view3 = h2;
                    }
                }
            }
            view = view2;
            h2 = view3;
            i2 += i5;
            view2 = view;
            view3 = h2;
        }
        if (view2 == null) {
            view2 = view3;
        }
        return view2;
    }

    public View a(View view, int i2, o oVar, t tVar) {
        J();
        if (u() == 0) {
            return null;
        }
        int e2 = e(i2);
        if (e2 == Integer.MIN_VALUE) {
            return null;
        }
        h();
        h();
        a(e2, (int) (0.33333334f * ((float) this.j.f())), false, tVar);
        this.f1056a.g = Integer.MIN_VALUE;
        this.f1056a.f1062a = false;
        a(oVar, this.f1056a, tVar, true);
        View j2 = e2 == -1 ? k(oVar, tVar) : j(oVar, tVar);
        View L = e2 == -1 ? K() : L();
        if (!L.hasFocusable()) {
            return j2;
        }
        if (j2 == null) {
            return null;
        }
        return L;
    }

    public void a(int i2, int i3, t tVar, android.support.v7.widget.RecyclerView.h.a aVar) {
        if (this.i != 0) {
            i2 = i3;
        }
        if (u() != 0 && i2 != 0) {
            h();
            a(i2 > 0 ? 1 : -1, Math.abs(i2), true, tVar);
            a(tVar, this.f1056a, aVar);
        }
    }

    public void a(int i2, android.support.v7.widget.RecyclerView.h.a aVar) {
        int i3;
        boolean z;
        if (this.n == null || !this.n.a()) {
            J();
            boolean z2 = this.k;
            if (this.l == -1) {
                i3 = z2 ? i2 - 1 : 0;
                z = z2;
            } else {
                i3 = this.l;
                z = z2;
            }
        } else {
            z = this.n.c;
            i3 = this.n.f1064a;
        }
        int i4 = z ? -1 : 1;
        for (int i5 = 0; i5 < this.h && i3 >= 0 && i3 < i2; i5++) {
            aVar.b(i3, 0);
            i3 += i4;
        }
    }

    public void a(Parcelable parcelable) {
        if (parcelable instanceof d) {
            this.n = (d) parcelable;
            n();
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(o oVar, t tVar, a aVar, int i2) {
    }

    /* access modifiers changed from: 0000 */
    public void a(o oVar, t tVar, c cVar, b bVar) {
        int A;
        int f2;
        int i2;
        int i3;
        int f3;
        View a2 = cVar.a(oVar);
        if (a2 == null) {
            bVar.f1061b = true;
            return;
        }
        i iVar = (i) a2.getLayoutParams();
        if (cVar.k == null) {
            if (this.k == (cVar.f == -1)) {
                b(a2);
            } else {
                b(a2, 0);
            }
        } else {
            if (this.k == (cVar.f == -1)) {
                a(a2);
            } else {
                a(a2, 0);
            }
        }
        a(a2, 0, 0);
        bVar.f1060a = this.j.e(a2);
        if (this.i == 1) {
            if (g()) {
                f3 = x() - B();
                i2 = f3 - this.j.f(a2);
            } else {
                i2 = z();
                f3 = this.j.f(a2) + i2;
            }
            if (cVar.f == -1) {
                f2 = cVar.f1063b;
                A = cVar.f1063b - bVar.f1060a;
                i3 = f3;
            } else {
                A = cVar.f1063b;
                f2 = bVar.f1060a + cVar.f1063b;
                i3 = f3;
            }
        } else {
            A = A();
            f2 = A + this.j.f(a2);
            if (cVar.f == -1) {
                i2 = cVar.f1063b - bVar.f1060a;
                i3 = cVar.f1063b;
            } else {
                i2 = cVar.f1063b;
                i3 = cVar.f1063b + bVar.f1060a;
            }
        }
        a(a2, i2, A, i3, f2);
        if (iVar.d() || iVar.e()) {
            bVar.c = true;
        }
        bVar.d = a2.hasFocusable();
    }

    public void a(t tVar) {
        super.a(tVar);
        this.n = null;
        this.l = -1;
        this.m = Integer.MIN_VALUE;
        this.o.a();
    }

    /* access modifiers changed from: 0000 */
    public void a(t tVar, c cVar, android.support.v7.widget.RecyclerView.h.a aVar) {
        int i2 = cVar.d;
        if (i2 >= 0 && i2 < tVar.e()) {
            aVar.b(i2, Math.max(0, cVar.g));
        }
    }

    public void a(RecyclerView recyclerView, o oVar) {
        super.a(recyclerView, oVar);
        if (this.f) {
            c(oVar);
            oVar.a();
        }
    }

    public void a(View view, View view2, int i2, int i3) {
        a("Cannot drop a view during a scroll or layout calculation");
        h();
        J();
        int d2 = d(view);
        int d3 = d(view2);
        boolean z = d2 < d3 ? true : true;
        if (this.k) {
            if (z) {
                b(d3, this.j.d() - (this.j.a(view2) + this.j.e(view)));
            } else {
                b(d3, this.j.d() - this.j.b(view2));
            }
        } else if (z) {
            b(d3, this.j.a(view2));
        } else {
            b(d3, this.j.b(view2) - this.j.e(view));
        }
    }

    public void a(AccessibilityEvent accessibilityEvent) {
        super.a(accessibilityEvent);
        if (u() > 0) {
            accessibilityEvent.setFromIndex(l());
            accessibilityEvent.setToIndex(m());
        }
    }

    public void a(String str) {
        if (this.n == null) {
            super.a(str);
        }
    }

    public void a(boolean z) {
        a((String) null);
        if (this.d != z) {
            this.d = z;
            n();
        }
    }

    public int b(int i2, o oVar, t tVar) {
        if (this.i == 0) {
            return 0;
        }
        return c(i2, oVar, tVar);
    }

    /* access modifiers changed from: protected */
    public int b(t tVar) {
        if (tVar.d()) {
            return this.j.f();
        }
        return 0;
    }

    public void b(int i2) {
        if (i2 == 0 || i2 == 1) {
            a((String) null);
            if (i2 != this.i) {
                this.i = i2;
                this.j = null;
                n();
                return;
            }
            return;
        }
        throw new IllegalArgumentException("invalid orientation:" + i2);
    }

    public void b(int i2, int i3) {
        this.l = i2;
        this.m = i3;
        if (this.n != null) {
            this.n.b();
        }
        n();
    }

    public void b(boolean z) {
        a((String) null);
        if (z != this.c) {
            this.c = z;
            n();
        }
    }

    public boolean b() {
        return this.n == null && this.f1057b == this.d;
    }

    /* access modifiers changed from: 0000 */
    public int c(int i2, o oVar, t tVar) {
        if (u() == 0 || i2 == 0) {
            return 0;
        }
        this.f1056a.f1062a = true;
        h();
        int i3 = i2 > 0 ? 1 : -1;
        int abs = Math.abs(i2);
        a(i3, abs, true, tVar);
        int a2 = this.f1056a.g + a(oVar, this.f1056a, tVar, false);
        if (a2 < 0) {
            return 0;
        }
        if (abs > a2) {
            i2 = i3 * a2;
        }
        this.j.a(-i2);
        this.f1056a.j = i2;
        return i2;
    }

    public int c(t tVar) {
        return i(tVar);
    }

    public Parcelable c() {
        if (this.n != null) {
            return new d(this.n);
        }
        d dVar = new d();
        if (u() > 0) {
            h();
            boolean z = this.f1057b ^ this.k;
            dVar.c = z;
            if (z) {
                View L = L();
                dVar.f1065b = this.j.d() - this.j.b(L);
                dVar.f1064a = d(L);
                return dVar;
            }
            View K = K();
            dVar.f1064a = d(K);
            dVar.f1065b = this.j.a(K) - this.j.c();
            return dVar;
        }
        dVar.b();
        return dVar;
    }

    public View c(int i2) {
        int u = u();
        if (u == 0) {
            return null;
        }
        int d2 = i2 - d(h(0));
        if (d2 >= 0 && d2 < u) {
            View h2 = h(d2);
            if (d(h2) == i2) {
                return h2;
            }
        }
        return super.c(i2);
    }

    /* access modifiers changed from: 0000 */
    public View c(int i2, int i3) {
        int i4;
        int i5;
        h();
        char c2 = i3 > i2 ? 1 : i3 < i2 ? (char) 65535 : 0;
        if (c2 == 0) {
            return h(i2);
        }
        if (this.j.a(h(i2)) < this.j.c()) {
            i4 = 16644;
            i5 = 16388;
        } else {
            i4 = 4161;
            i5 = 4097;
        }
        return this.i == 0 ? this.r.a(i2, i3, i4, i5) : this.s.a(i2, i3, i4, i5);
    }

    public void c(o oVar, t tVar) {
        int i2;
        int i3;
        int i4;
        int i5;
        int a2;
        int i6 = -1;
        if (!(this.n == null && this.l == -1) && tVar.e() == 0) {
            c(oVar);
            return;
        }
        if (this.n != null && this.n.a()) {
            this.l = this.n.f1064a;
        }
        h();
        this.f1056a.f1062a = false;
        J();
        View D = D();
        if (!this.o.d || this.l != -1 || this.n != null) {
            this.o.a();
            this.o.c = this.k ^ this.d;
            a(oVar, tVar, this.o);
            this.o.d = true;
        } else if (D != null && (this.j.a(D) >= this.j.d() || this.j.b(D) <= this.j.c())) {
            this.o.a(D);
        }
        int b2 = b(tVar);
        if (this.f1056a.j >= 0) {
            i2 = 0;
        } else {
            i2 = b2;
            b2 = 0;
        }
        int c2 = i2 + this.j.c();
        int g2 = b2 + this.j.g();
        if (!(!tVar.a() || this.l == -1 || this.m == Integer.MIN_VALUE)) {
            View c3 = c(this.l);
            if (c3 != null) {
                if (this.k) {
                    a2 = (this.j.d() - this.j.b(c3)) - this.m;
                } else {
                    a2 = this.m - (this.j.a(c3) - this.j.c());
                }
                if (a2 > 0) {
                    c2 += a2;
                } else {
                    g2 -= a2;
                }
            }
        }
        if (this.o.c) {
            if (this.k) {
                i6 = 1;
            }
        } else if (!this.k) {
            i6 = 1;
        }
        a(oVar, tVar, this.o, i6);
        a(oVar);
        this.f1056a.l = j();
        this.f1056a.i = tVar.a();
        if (this.o.c) {
            b(this.o);
            this.f1056a.h = c2;
            a(oVar, this.f1056a, tVar, false);
            int i7 = this.f1056a.f1063b;
            int i8 = this.f1056a.d;
            if (this.f1056a.c > 0) {
                g2 += this.f1056a.c;
            }
            a(this.o);
            this.f1056a.h = g2;
            this.f1056a.d += this.f1056a.e;
            a(oVar, this.f1056a, tVar, false);
            int i9 = this.f1056a.f1063b;
            if (this.f1056a.c > 0) {
                int i10 = this.f1056a.c;
                h(i8, i7);
                this.f1056a.h = i10;
                a(oVar, this.f1056a, tVar, false);
                i5 = this.f1056a.f1063b;
            } else {
                i5 = i7;
            }
            i4 = i5;
            i3 = i9;
        } else {
            a(this.o);
            this.f1056a.h = g2;
            a(oVar, this.f1056a, tVar, false);
            i3 = this.f1056a.f1063b;
            int i11 = this.f1056a.d;
            if (this.f1056a.c > 0) {
                c2 += this.f1056a.c;
            }
            b(this.o);
            this.f1056a.h = c2;
            this.f1056a.d += this.f1056a.e;
            a(oVar, this.f1056a, tVar, false);
            i4 = this.f1056a.f1063b;
            if (this.f1056a.c > 0) {
                int i12 = this.f1056a.c;
                a(i11, i3);
                this.f1056a.h = i12;
                a(oVar, this.f1056a, tVar, false);
                i3 = this.f1056a.f1063b;
            }
        }
        if (u() > 0) {
            if (this.k ^ this.d) {
                int a3 = a(i3, oVar, tVar, true);
                int i13 = i4 + a3;
                int i14 = i3 + a3;
                int b3 = b(i13, oVar, tVar, false);
                i4 = i13 + b3;
                i3 = i14 + b3;
            } else {
                int b4 = b(i4, oVar, tVar, true);
                int i15 = i4 + b4;
                int i16 = i3 + b4;
                int a4 = a(i16, oVar, tVar, false);
                i4 = i15 + a4;
                i3 = i16 + a4;
            }
        }
        b(oVar, tVar, i4, i3);
        if (!tVar.a()) {
            this.j.a();
        } else {
            this.o.a();
        }
        this.f1057b = this.d;
    }

    public int d(t tVar) {
        return i(tVar);
    }

    public void d(int i2) {
        this.l = i2;
        this.m = Integer.MIN_VALUE;
        if (this.n != null) {
            this.n.b();
        }
        n();
    }

    public boolean d() {
        return this.i == 0;
    }

    /* access modifiers changed from: 0000 */
    public int e(int i2) {
        int i3 = Integer.MIN_VALUE;
        int i4 = 1;
        switch (i2) {
            case 1:
                return (this.i == 1 || !g()) ? -1 : 1;
            case 2:
                if (this.i == 1) {
                    return 1;
                }
                return !g() ? 1 : -1;
            case 17:
                return this.i != 0 ? Integer.MIN_VALUE : -1;
            case 33:
                return this.i != 1 ? Integer.MIN_VALUE : -1;
            case 66:
                if (this.i != 0) {
                    i4 = Integer.MIN_VALUE;
                }
                return i4;
            case 130:
                if (this.i == 1) {
                    i3 = 1;
                }
                return i3;
            default:
                return Integer.MIN_VALUE;
        }
    }

    public int e(t tVar) {
        return j(tVar);
    }

    public boolean e() {
        return this.i == 1;
    }

    public int f() {
        return this.i;
    }

    public int f(t tVar) {
        return j(tVar);
    }

    public int g(t tVar) {
        return k(tVar);
    }

    /* access modifiers changed from: protected */
    public boolean g() {
        return s() == 1;
    }

    public int h(t tVar) {
        return k(tVar);
    }

    /* access modifiers changed from: 0000 */
    public void h() {
        if (this.f1056a == null) {
            this.f1056a = i();
        }
        if (this.j == null) {
            this.j = az.a(this, this.i);
        }
    }

    /* access modifiers changed from: 0000 */
    public c i() {
        return new c();
    }

    /* access modifiers changed from: 0000 */
    public boolean j() {
        return this.j.h() == 0 && this.j.e() == 0;
    }

    /* access modifiers changed from: 0000 */
    public boolean k() {
        return (w() == 1073741824 || v() == 1073741824 || !I()) ? false : true;
    }

    public int l() {
        View a2 = a(0, u(), false, true);
        if (a2 == null) {
            return -1;
        }
        return d(a2);
    }

    public int m() {
        View a2 = a(u() - 1, -1, false, true);
        if (a2 == null) {
            return -1;
        }
        return d(a2);
    }
}
