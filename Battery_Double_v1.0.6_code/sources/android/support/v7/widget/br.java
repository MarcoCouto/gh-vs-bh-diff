package android.support.v7.widget;

import android.support.v4.i.t;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnAttachStateChangeListener;
import android.view.View.OnHoverListener;
import android.view.View.OnLongClickListener;
import android.view.ViewConfiguration;
import android.view.accessibility.AccessibilityManager;

class br implements OnAttachStateChangeListener, OnHoverListener, OnLongClickListener {
    private static br i;

    /* renamed from: a reason: collision with root package name */
    private final View f1272a;

    /* renamed from: b reason: collision with root package name */
    private final CharSequence f1273b;
    private final Runnable c = new Runnable() {
        public void run() {
            br.this.a(false);
        }
    };
    private final Runnable d = new Runnable() {
        public void run() {
            br.this.a();
        }
    };
    private int e;
    private int f;
    private bs g;
    private boolean h;

    private br(View view, CharSequence charSequence) {
        this.f1272a = view;
        this.f1273b = charSequence;
        this.f1272a.setOnLongClickListener(this);
        this.f1272a.setOnHoverListener(this);
    }

    /* access modifiers changed from: private */
    public void a() {
        if (i == this) {
            i = null;
            if (this.g != null) {
                this.g.a();
                this.g = null;
                this.f1272a.removeOnAttachStateChangeListener(this);
            } else {
                Log.e("TooltipCompatHandler", "sActiveHandler.mPopup == null");
            }
        }
        this.f1272a.removeCallbacks(this.c);
        this.f1272a.removeCallbacks(this.d);
    }

    public static void a(View view, CharSequence charSequence) {
        if (TextUtils.isEmpty(charSequence)) {
            if (i != null && i.f1272a == view) {
                i.a();
            }
            view.setOnLongClickListener(null);
            view.setLongClickable(false);
            view.setOnHoverListener(null);
            return;
        }
        new br(view, charSequence);
    }

    /* access modifiers changed from: private */
    public void a(boolean z) {
        if (t.y(this.f1272a)) {
            if (i != null) {
                i.a();
            }
            i = this;
            this.h = z;
            this.g = new bs(this.f1272a.getContext());
            this.g.a(this.f1272a, this.e, this.f, this.h, this.f1273b);
            this.f1272a.addOnAttachStateChangeListener(this);
            long longPressTimeout = this.h ? 2500 : (t.m(this.f1272a) & 1) == 1 ? 3000 - ((long) ViewConfiguration.getLongPressTimeout()) : 15000 - ((long) ViewConfiguration.getLongPressTimeout());
            this.f1272a.removeCallbacks(this.d);
            this.f1272a.postDelayed(this.d, longPressTimeout);
        }
    }

    public boolean onHover(View view, MotionEvent motionEvent) {
        if (this.g == null || !this.h) {
            AccessibilityManager accessibilityManager = (AccessibilityManager) this.f1272a.getContext().getSystemService("accessibility");
            if (!accessibilityManager.isEnabled() || !accessibilityManager.isTouchExplorationEnabled()) {
                switch (motionEvent.getAction()) {
                    case 7:
                        if (this.f1272a.isEnabled() && this.g == null) {
                            this.e = (int) motionEvent.getX();
                            this.f = (int) motionEvent.getY();
                            this.f1272a.removeCallbacks(this.c);
                            this.f1272a.postDelayed(this.c, (long) ViewConfiguration.getLongPressTimeout());
                            break;
                        }
                    case 10:
                        a();
                        break;
                }
            }
        }
        return false;
    }

    public boolean onLongClick(View view) {
        this.e = view.getWidth() / 2;
        this.f = view.getHeight() / 2;
        a(true);
        return true;
    }

    public void onViewAttachedToWindow(View view) {
    }

    public void onViewDetachedFromWindow(View view) {
        a();
    }
}
