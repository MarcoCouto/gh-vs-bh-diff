package android.support.v7.widget;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.Resources.NotFoundException;
import android.graphics.drawable.Drawable;
import java.lang.ref.WeakReference;

class bn extends bb {

    /* renamed from: a reason: collision with root package name */
    private final WeakReference<Context> f1262a;

    public bn(Context context, Resources resources) {
        super(resources);
        this.f1262a = new WeakReference<>(context);
    }

    public Drawable getDrawable(int i) throws NotFoundException {
        Drawable drawable = super.getDrawable(i);
        Context context = (Context) this.f1262a.get();
        if (!(drawable == null || context == null)) {
            m.a();
            m.a(context, i, drawable);
        }
        return drawable;
    }
}
