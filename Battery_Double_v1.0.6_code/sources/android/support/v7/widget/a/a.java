package android.support.v7.widget.a;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.Build.VERSION;
import android.support.v4.i.t;
import android.support.v7.e.a.C0030a;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.g;
import android.support.v7.widget.RecyclerView.h;
import android.support.v7.widget.RecyclerView.j;
import android.support.v7.widget.RecyclerView.l;
import android.support.v7.widget.RecyclerView.w;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewParent;
import android.view.animation.Interpolator;
import java.util.ArrayList;
import java.util.List;

public class a extends g implements j {
    private b A;
    private final l B = new l() {
        public void a(boolean z) {
            if (z) {
                a.this.a((w) null, 0);
            }
        }

        public boolean a(RecyclerView recyclerView, MotionEvent motionEvent) {
            a.this.u.a(motionEvent);
            int actionMasked = motionEvent.getActionMasked();
            if (actionMasked == 0) {
                a.this.k = motionEvent.getPointerId(0);
                a.this.c = motionEvent.getX();
                a.this.d = motionEvent.getY();
                a.this.c();
                if (a.this.f1146b == null) {
                    c b2 = a.this.b(motionEvent);
                    if (b2 != null) {
                        a.this.c -= b2.l;
                        a.this.d -= b2.m;
                        a.this.a(b2.h, true);
                        if (a.this.f1145a.remove(b2.h.f1102a)) {
                            a.this.l.d(a.this.p, b2.h);
                        }
                        a.this.a(b2.h, b2.i);
                        a.this.a(motionEvent, a.this.n, 0);
                    }
                }
            } else if (actionMasked == 3 || actionMasked == 1) {
                a.this.k = -1;
                a.this.a((w) null, 0);
            } else if (a.this.k != -1) {
                int findPointerIndex = motionEvent.findPointerIndex(a.this.k);
                if (findPointerIndex >= 0) {
                    a.this.a(actionMasked, motionEvent, findPointerIndex);
                }
            }
            if (a.this.r != null) {
                a.this.r.addMovement(motionEvent);
            }
            return a.this.f1146b != null;
        }

        public void b(RecyclerView recyclerView, MotionEvent motionEvent) {
            int i = 0;
            a.this.u.a(motionEvent);
            if (a.this.r != null) {
                a.this.r.addMovement(motionEvent);
            }
            if (a.this.k != -1) {
                int actionMasked = motionEvent.getActionMasked();
                int findPointerIndex = motionEvent.findPointerIndex(a.this.k);
                if (findPointerIndex >= 0) {
                    a.this.a(actionMasked, motionEvent, findPointerIndex);
                }
                w wVar = a.this.f1146b;
                if (wVar != null) {
                    switch (actionMasked) {
                        case 1:
                            break;
                        case 2:
                            if (findPointerIndex >= 0) {
                                a.this.a(motionEvent, a.this.n, findPointerIndex);
                                a.this.a(wVar);
                                a.this.p.removeCallbacks(a.this.q);
                                a.this.q.run();
                                a.this.p.invalidate();
                                return;
                            }
                            return;
                        case 3:
                            if (a.this.r != null) {
                                a.this.r.clear();
                                break;
                            }
                            break;
                        case 6:
                            int actionIndex = motionEvent.getActionIndex();
                            if (motionEvent.getPointerId(actionIndex) == a.this.k) {
                                if (actionIndex == 0) {
                                    i = 1;
                                }
                                a.this.k = motionEvent.getPointerId(i);
                                a.this.a(motionEvent, a.this.n, actionIndex);
                                return;
                            }
                            return;
                        default:
                            return;
                    }
                    a.this.a((w) null, 0);
                    a.this.k = -1;
                }
            }
        }
    };
    private Rect C;
    private long D;

    /* renamed from: a reason: collision with root package name */
    final List<View> f1145a = new ArrayList();

    /* renamed from: b reason: collision with root package name */
    w f1146b = null;
    float c;
    float d;
    float e;
    float f;
    float g;
    float h;
    float i;
    float j;
    int k = -1;
    C0032a l;
    int m = 0;
    int n;
    List<c> o = new ArrayList();
    RecyclerView p;
    final Runnable q = new Runnable() {
        public void run() {
            if (a.this.f1146b != null && a.this.b()) {
                if (a.this.f1146b != null) {
                    a.this.a(a.this.f1146b);
                }
                a.this.p.removeCallbacks(a.this.q);
                t.a((View) a.this.p, (Runnable) this);
            }
        }
    };
    VelocityTracker r;
    View s = null;
    int t = -1;
    android.support.v4.i.d u;
    private final float[] v = new float[2];
    private int w;
    private List<w> x;
    private List<Integer> y;
    private android.support.v7.widget.RecyclerView.d z = null;

    /* renamed from: android.support.v7.widget.a.a$a reason: collision with other inner class name */
    public static abstract class C0032a {

        /* renamed from: a reason: collision with root package name */
        private static final b f1154a;

        /* renamed from: b reason: collision with root package name */
        private static final Interpolator f1155b = new Interpolator() {
            public float getInterpolation(float f) {
                return f * f * f * f * f;
            }
        };
        private static final Interpolator c = new Interpolator() {
            public float getInterpolation(float f) {
                float f2 = f - 1.0f;
                return (f2 * f2 * f2 * f2 * f2) + 1.0f;
            }
        };
        private int d = -1;

        static {
            if (VERSION.SDK_INT >= 21) {
                f1154a = new a();
            } else {
                f1154a = new b();
            }
        }

        public static int a(int i, int i2) {
            int i3 = i & 789516;
            if (i3 == 0) {
                return i;
            }
            int i4 = (i3 ^ -1) & i;
            return i2 == 0 ? i4 | (i3 << 2) : i4 | ((i3 << 1) & -789517) | (((i3 << 1) & 789516) << 2);
        }

        private int a(RecyclerView recyclerView) {
            if (this.d == -1) {
                this.d = recyclerView.getResources().getDimensionPixelSize(C0030a.item_touch_helper_max_drag_scroll_per_frame);
            }
            return this.d;
        }

        public static int b(int i, int i2) {
            return c(0, i2 | i) | c(1, i2) | c(2, i);
        }

        public static int c(int i, int i2) {
            return i2 << (i * 8);
        }

        public float a(float f) {
            return f;
        }

        public float a(w wVar) {
            return 0.5f;
        }

        public int a(RecyclerView recyclerView, int i, int i2, int i3, long j) {
            float f = 1.0f;
            int a2 = (int) (((float) (a(recyclerView) * ((int) Math.signum((float) i2)))) * c.getInterpolation(Math.min(1.0f, (((float) Math.abs(i2)) * 1.0f) / ((float) i))));
            if (j <= 2000) {
                f = ((float) j) / 2000.0f;
            }
            int interpolation = (int) (f1155b.getInterpolation(f) * ((float) a2));
            return interpolation == 0 ? i2 > 0 ? 1 : -1 : interpolation;
        }

        public abstract int a(RecyclerView recyclerView, w wVar);

        public long a(RecyclerView recyclerView, int i, float f, float f2) {
            android.support.v7.widget.RecyclerView.e itemAnimator = recyclerView.getItemAnimator();
            return itemAnimator == null ? i == 8 ? 200 : 250 : i == 8 ? itemAnimator.e() : itemAnimator.g();
        }

        /* JADX WARNING: Removed duplicated region for block: B:12:0x0053  */
        /* JADX WARNING: Removed duplicated region for block: B:20:0x0074  */
        /* JADX WARNING: Removed duplicated region for block: B:28:0x0095  */
        public w a(w wVar, List<w> list, int i, int i2) {
            w wVar2;
            int i3;
            int i4;
            int i5;
            int i6;
            w wVar3;
            int width = i + wVar.f1102a.getWidth();
            int height = i2 + wVar.f1102a.getHeight();
            w wVar4 = null;
            int i7 = -1;
            int left = i - wVar.f1102a.getLeft();
            int top = i2 - wVar.f1102a.getTop();
            int size = list.size();
            int i8 = 0;
            while (i8 < size) {
                w wVar5 = (w) list.get(i8);
                if (left > 0) {
                    int right = wVar5.f1102a.getRight() - width;
                    if (right < 0 && wVar5.f1102a.getRight() > wVar.f1102a.getRight()) {
                        int abs = Math.abs(right);
                        if (abs > i7) {
                            i3 = abs;
                            wVar2 = wVar5;
                            if (left < 0) {
                                int left2 = wVar5.f1102a.getLeft() - i;
                                if (left2 > 0 && wVar5.f1102a.getLeft() < wVar.f1102a.getLeft()) {
                                    i4 = Math.abs(left2);
                                    if (i4 > i3) {
                                        wVar2 = wVar5;
                                        if (top < 0) {
                                            int top2 = wVar5.f1102a.getTop() - i2;
                                            if (top2 > 0 && wVar5.f1102a.getTop() < wVar.f1102a.getTop()) {
                                                i5 = Math.abs(top2);
                                                if (i5 > i4) {
                                                    wVar2 = wVar5;
                                                    if (top > 0) {
                                                        int bottom = wVar5.f1102a.getBottom() - height;
                                                        if (bottom < 0 && wVar5.f1102a.getBottom() > wVar.f1102a.getBottom()) {
                                                            int abs2 = Math.abs(bottom);
                                                            if (abs2 > i5) {
                                                                int i9 = abs2;
                                                                wVar3 = wVar5;
                                                                i6 = i9;
                                                                i8++;
                                                                wVar4 = wVar3;
                                                                i7 = i6;
                                                            }
                                                        }
                                                    }
                                                    i6 = i5;
                                                    wVar3 = wVar2;
                                                    i8++;
                                                    wVar4 = wVar3;
                                                    i7 = i6;
                                                }
                                            }
                                        }
                                        i5 = i4;
                                        if (top > 0) {
                                        }
                                        i6 = i5;
                                        wVar3 = wVar2;
                                        i8++;
                                        wVar4 = wVar3;
                                        i7 = i6;
                                    }
                                }
                            }
                            i4 = i3;
                            if (top < 0) {
                            }
                            i5 = i4;
                            if (top > 0) {
                            }
                            i6 = i5;
                            wVar3 = wVar2;
                            i8++;
                            wVar4 = wVar3;
                            i7 = i6;
                        }
                    }
                }
                wVar2 = wVar4;
                i3 = i7;
                if (left < 0) {
                }
                i4 = i3;
                if (top < 0) {
                }
                i5 = i4;
                if (top > 0) {
                }
                i6 = i5;
                wVar3 = wVar2;
                i8++;
                wVar4 = wVar3;
                i7 = i6;
            }
            return wVar4;
        }

        public void a(Canvas canvas, RecyclerView recyclerView, w wVar, float f, float f2, int i, boolean z) {
            f1154a.a(canvas, recyclerView, wVar.f1102a, f, f2, i, z);
        }

        /* access modifiers changed from: 0000 */
        public void a(Canvas canvas, RecyclerView recyclerView, w wVar, List<c> list, int i, float f, float f2) {
            int size = list.size();
            for (int i2 = 0; i2 < size; i2++) {
                c cVar = (c) list.get(i2);
                cVar.c();
                int save = canvas.save();
                a(canvas, recyclerView, cVar.h, cVar.l, cVar.m, cVar.i, false);
                canvas.restoreToCount(save);
            }
            if (wVar != null) {
                int save2 = canvas.save();
                a(canvas, recyclerView, wVar, f, f2, i, true);
                canvas.restoreToCount(save2);
            }
        }

        public abstract void a(w wVar, int i);

        public void a(RecyclerView recyclerView, w wVar, int i, w wVar2, int i2, int i3, int i4) {
            h layoutManager = recyclerView.getLayoutManager();
            if (layoutManager instanceof e) {
                ((e) layoutManager).a(wVar.f1102a, wVar2.f1102a, i3, i4);
                return;
            }
            if (layoutManager.d()) {
                if (layoutManager.h(wVar2.f1102a) <= recyclerView.getPaddingLeft()) {
                    recyclerView.a(i2);
                }
                if (layoutManager.j(wVar2.f1102a) >= recyclerView.getWidth() - recyclerView.getPaddingRight()) {
                    recyclerView.a(i2);
                }
            }
            if (layoutManager.e()) {
                if (layoutManager.i(wVar2.f1102a) <= recyclerView.getPaddingTop()) {
                    recyclerView.a(i2);
                }
                if (layoutManager.k(wVar2.f1102a) >= recyclerView.getHeight() - recyclerView.getPaddingBottom()) {
                    recyclerView.a(i2);
                }
            }
        }

        public boolean a() {
            return true;
        }

        public boolean a(RecyclerView recyclerView, w wVar, w wVar2) {
            return true;
        }

        public float b(float f) {
            return f;
        }

        public float b(w wVar) {
            return 0.5f;
        }

        /* access modifiers changed from: 0000 */
        public final int b(RecyclerView recyclerView, w wVar) {
            return d(a(recyclerView, wVar), t.e(recyclerView));
        }

        public void b(Canvas canvas, RecyclerView recyclerView, w wVar, float f, float f2, int i, boolean z) {
            f1154a.b(canvas, recyclerView, wVar.f1102a, f, f2, i, z);
        }

        /* access modifiers changed from: 0000 */
        public void b(Canvas canvas, RecyclerView recyclerView, w wVar, List<c> list, int i, float f, float f2) {
            boolean z;
            int size = list.size();
            for (int i2 = 0; i2 < size; i2++) {
                c cVar = (c) list.get(i2);
                int save = canvas.save();
                b(canvas, recyclerView, cVar.h, cVar.l, cVar.m, cVar.i, false);
                canvas.restoreToCount(save);
            }
            if (wVar != null) {
                int save2 = canvas.save();
                b(canvas, recyclerView, wVar, f, f2, i, true);
                canvas.restoreToCount(save2);
            }
            boolean z2 = false;
            int i3 = size - 1;
            while (i3 >= 0) {
                c cVar2 = (c) list.get(i3);
                if (!cVar2.o || cVar2.k) {
                    z = !cVar2.o ? true : z2;
                } else {
                    list.remove(i3);
                    z = z2;
                }
                i3--;
                z2 = z;
            }
            if (z2) {
                recyclerView.invalidate();
            }
        }

        public void b(w wVar, int i) {
            if (wVar != null) {
                f1154a.b(wVar.f1102a);
            }
        }

        public boolean b() {
            return true;
        }

        public abstract boolean b(RecyclerView recyclerView, w wVar, w wVar2);

        public int c() {
            return 0;
        }

        /* access modifiers changed from: 0000 */
        public boolean c(RecyclerView recyclerView, w wVar) {
            return (b(recyclerView, wVar) & 16711680) != 0;
        }

        public int d(int i, int i2) {
            int i3 = i & 3158064;
            if (i3 == 0) {
                return i;
            }
            int i4 = (i3 ^ -1) & i;
            return i2 == 0 ? i4 | (i3 >> 2) : i4 | ((i3 >> 1) & -3158065) | (((i3 >> 1) & 3158064) >> 2);
        }

        public void d(RecyclerView recyclerView, w wVar) {
            f1154a.a(wVar.f1102a);
        }
    }

    private class b extends SimpleOnGestureListener {

        /* renamed from: b reason: collision with root package name */
        private boolean f1157b = true;

        b() {
        }

        /* access modifiers changed from: 0000 */
        public void a() {
            this.f1157b = false;
        }

        public boolean onDown(MotionEvent motionEvent) {
            return true;
        }

        public void onLongPress(MotionEvent motionEvent) {
            if (this.f1157b) {
                View a2 = a.this.a(motionEvent);
                if (a2 != null) {
                    w b2 = a.this.p.b(a2);
                    if (b2 != null && a.this.l.c(a.this.p, b2) && motionEvent.getPointerId(0) == a.this.k) {
                        int findPointerIndex = motionEvent.findPointerIndex(a.this.k);
                        float x = motionEvent.getX(findPointerIndex);
                        float y = motionEvent.getY(findPointerIndex);
                        a.this.c = x;
                        a.this.d = y;
                        a aVar = a.this;
                        a.this.h = 0.0f;
                        aVar.g = 0.0f;
                        if (a.this.l.a()) {
                            a.this.a(b2, 2);
                        }
                    }
                }
            }
        }
    }

    private static class c implements AnimatorListener {

        /* renamed from: a reason: collision with root package name */
        private final ValueAnimator f1158a;

        /* renamed from: b reason: collision with root package name */
        private float f1159b;
        final float d;
        final float e;
        final float f;
        final float g;
        final w h;
        final int i;
        final int j;
        public boolean k;
        float l;
        float m;
        boolean n = false;
        boolean o = false;

        c(w wVar, int i2, int i3, float f2, float f3, float f4, float f5) {
            this.i = i3;
            this.j = i2;
            this.h = wVar;
            this.d = f2;
            this.e = f3;
            this.f = f4;
            this.g = f5;
            this.f1158a = ValueAnimator.ofFloat(new float[]{0.0f, 1.0f});
            this.f1158a.addUpdateListener(new AnimatorUpdateListener() {
                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    c.this.a(valueAnimator.getAnimatedFraction());
                }
            });
            this.f1158a.setTarget(wVar.f1102a);
            this.f1158a.addListener(this);
            a(0.0f);
        }

        public void a() {
            this.h.a(false);
            this.f1158a.start();
        }

        public void a(float f2) {
            this.f1159b = f2;
        }

        public void a(long j2) {
            this.f1158a.setDuration(j2);
        }

        public void b() {
            this.f1158a.cancel();
        }

        public void c() {
            if (this.d == this.f) {
                this.l = this.h.f1102a.getTranslationX();
            } else {
                this.l = this.d + (this.f1159b * (this.f - this.d));
            }
            if (this.e == this.g) {
                this.m = this.h.f1102a.getTranslationY();
            } else {
                this.m = this.e + (this.f1159b * (this.g - this.e));
            }
        }

        public void onAnimationCancel(Animator animator) {
            a(1.0f);
        }

        public void onAnimationEnd(Animator animator) {
            if (!this.o) {
                this.h.a(true);
            }
            this.o = true;
        }

        public void onAnimationRepeat(Animator animator) {
        }

        public void onAnimationStart(Animator animator) {
        }
    }

    public static abstract class d extends C0032a {

        /* renamed from: a reason: collision with root package name */
        private int f1161a;

        /* renamed from: b reason: collision with root package name */
        private int f1162b;

        public d(int i, int i2) {
            this.f1161a = i2;
            this.f1162b = i;
        }

        public int a(RecyclerView recyclerView, w wVar) {
            return b(f(recyclerView, wVar), e(recyclerView, wVar));
        }

        public int e(RecyclerView recyclerView, w wVar) {
            return this.f1161a;
        }

        public int f(RecyclerView recyclerView, w wVar) {
            return this.f1162b;
        }
    }

    public interface e {
        void a(View view, View view2, int i, int i2);
    }

    public a(C0032a aVar) {
        this.l = aVar;
    }

    private void a(float[] fArr) {
        if ((this.n & 12) != 0) {
            fArr[0] = (this.i + this.g) - ((float) this.f1146b.f1102a.getLeft());
        } else {
            fArr[0] = this.f1146b.f1102a.getTranslationX();
        }
        if ((this.n & 3) != 0) {
            fArr[1] = (this.j + this.h) - ((float) this.f1146b.f1102a.getTop());
        } else {
            fArr[1] = this.f1146b.f1102a.getTranslationY();
        }
    }

    private static boolean a(View view, float f2, float f3, float f4, float f5) {
        return f2 >= f4 && f2 <= ((float) view.getWidth()) + f4 && f3 >= f5 && f3 <= ((float) view.getHeight()) + f5;
    }

    private int b(w wVar, int i2) {
        int i3 = 8;
        if ((i2 & 12) != 0) {
            int i4 = this.g > 0.0f ? 8 : 4;
            if (this.r != null && this.k > -1) {
                this.r.computeCurrentVelocity(1000, this.l.b(this.f));
                float xVelocity = this.r.getXVelocity(this.k);
                float yVelocity = this.r.getYVelocity(this.k);
                if (xVelocity <= 0.0f) {
                    i3 = 4;
                }
                float abs = Math.abs(xVelocity);
                if ((i3 & i2) != 0 && i4 == i3 && abs >= this.l.a(this.e) && abs > Math.abs(yVelocity)) {
                    return i3;
                }
            }
            float width = ((float) this.p.getWidth()) * this.l.a(wVar);
            if ((i2 & i4) != 0 && Math.abs(this.g) > width) {
                return i4;
            }
        }
        return 0;
    }

    private List<w> b(w wVar) {
        if (this.x == null) {
            this.x = new ArrayList();
            this.y = new ArrayList();
        } else {
            this.x.clear();
            this.y.clear();
        }
        int c2 = this.l.c();
        int round = Math.round(this.i + this.g) - c2;
        int round2 = Math.round(this.j + this.h) - c2;
        int width = wVar.f1102a.getWidth() + round + (c2 * 2);
        int height = wVar.f1102a.getHeight() + round2 + (c2 * 2);
        int i2 = (round + width) / 2;
        int i3 = (round2 + height) / 2;
        h layoutManager = this.p.getLayoutManager();
        int u2 = layoutManager.u();
        for (int i4 = 0; i4 < u2; i4++) {
            View h2 = layoutManager.h(i4);
            if (h2 != wVar.f1102a && h2.getBottom() >= round2 && h2.getTop() <= height && h2.getRight() >= round && h2.getLeft() <= width) {
                w b2 = this.p.b(h2);
                if (this.l.a(this.p, this.f1146b, b2)) {
                    int abs = Math.abs(i2 - ((h2.getLeft() + h2.getRight()) / 2));
                    int abs2 = Math.abs(i3 - ((h2.getBottom() + h2.getTop()) / 2));
                    int i5 = (abs * abs) + (abs2 * abs2);
                    int size = this.x.size();
                    int i6 = 0;
                    int i7 = 0;
                    while (i7 < size && i5 > ((Integer) this.y.get(i7)).intValue()) {
                        i6++;
                        i7++;
                    }
                    this.x.add(i6, b2);
                    this.y.add(i6, Integer.valueOf(i5));
                }
            }
        }
        return this.x;
    }

    private int c(w wVar) {
        if (this.m == 2) {
            return 0;
        }
        int a2 = this.l.a(this.p, wVar);
        int d2 = (this.l.d(a2, t.e(this.p)) & 65280) >> 8;
        if (d2 == 0) {
            return 0;
        }
        int i2 = (a2 & 65280) >> 8;
        if (Math.abs(this.g) > Math.abs(this.h)) {
            int b2 = b(wVar, d2);
            if (b2 > 0) {
                return (i2 & b2) == 0 ? C0032a.a(b2, t.e(this.p)) : b2;
            }
            int c2 = c(wVar, d2);
            if (c2 > 0) {
                return c2;
            }
            return 0;
        }
        int c3 = c(wVar, d2);
        if (c3 > 0) {
            return c3;
        }
        int b3 = b(wVar, d2);
        if (b3 > 0) {
            return (i2 & b3) == 0 ? C0032a.a(b3, t.e(this.p)) : b3;
        }
        return 0;
    }

    private int c(w wVar, int i2) {
        int i3 = 2;
        if ((i2 & 3) != 0) {
            int i4 = this.h > 0.0f ? 2 : 1;
            if (this.r != null && this.k > -1) {
                this.r.computeCurrentVelocity(1000, this.l.b(this.f));
                float xVelocity = this.r.getXVelocity(this.k);
                float yVelocity = this.r.getYVelocity(this.k);
                if (yVelocity <= 0.0f) {
                    i3 = 1;
                }
                float abs = Math.abs(yVelocity);
                if ((i3 & i2) != 0 && i3 == i4 && abs >= this.l.a(this.e) && abs > Math.abs(xVelocity)) {
                    return i3;
                }
            }
            float height = ((float) this.p.getHeight()) * this.l.a(wVar);
            if ((i2 & i4) != 0 && Math.abs(this.h) > height) {
                return i4;
            }
        }
        return 0;
    }

    private w c(MotionEvent motionEvent) {
        h layoutManager = this.p.getLayoutManager();
        if (this.k == -1) {
            return null;
        }
        int findPointerIndex = motionEvent.findPointerIndex(this.k);
        float x2 = motionEvent.getX(findPointerIndex) - this.c;
        float y2 = motionEvent.getY(findPointerIndex) - this.d;
        float abs = Math.abs(x2);
        float abs2 = Math.abs(y2);
        if (abs < ((float) this.w) && abs2 < ((float) this.w)) {
            return null;
        }
        if (abs > abs2 && layoutManager.d()) {
            return null;
        }
        if (abs2 > abs && layoutManager.e()) {
            return null;
        }
        View a2 = a(motionEvent);
        if (a2 != null) {
            return this.p.b(a2);
        }
        return null;
    }

    private void d() {
        this.w = ViewConfiguration.get(this.p.getContext()).getScaledTouchSlop();
        this.p.a((g) this);
        this.p.a(this.B);
        this.p.a((j) this);
        f();
    }

    private void e() {
        this.p.b((g) this);
        this.p.b(this.B);
        this.p.b((j) this);
        for (int size = this.o.size() - 1; size >= 0; size--) {
            this.l.d(this.p, ((c) this.o.get(0)).h);
        }
        this.o.clear();
        this.s = null;
        this.t = -1;
        h();
        g();
    }

    private void f() {
        this.A = new b();
        this.u = new android.support.v4.i.d(this.p.getContext(), this.A);
    }

    private void g() {
        if (this.A != null) {
            this.A.a();
            this.A = null;
        }
        if (this.u != null) {
            this.u = null;
        }
    }

    private void h() {
        if (this.r != null) {
            this.r.recycle();
            this.r = null;
        }
    }

    private void i() {
        if (VERSION.SDK_INT < 21) {
            if (this.z == null) {
                this.z = new android.support.v7.widget.RecyclerView.d() {
                    public int a(int i, int i2) {
                        if (a.this.s == null) {
                            return i2;
                        }
                        int i3 = a.this.t;
                        if (i3 == -1) {
                            i3 = a.this.p.indexOfChild(a.this.s);
                            a.this.t = i3;
                        }
                        return i2 == i + -1 ? i3 : i2 >= i3 ? i2 + 1 : i2;
                    }
                };
            }
            this.p.setChildDrawingOrderCallback(this.z);
        }
    }

    /* access modifiers changed from: 0000 */
    public int a(w wVar, boolean z2) {
        for (int size = this.o.size() - 1; size >= 0; size--) {
            c cVar = (c) this.o.get(size);
            if (cVar.h == wVar) {
                cVar.n |= z2;
                if (!cVar.o) {
                    cVar.b();
                }
                this.o.remove(size);
                return cVar.j;
            }
        }
        return 0;
    }

    /* access modifiers changed from: 0000 */
    public View a(MotionEvent motionEvent) {
        float x2 = motionEvent.getX();
        float y2 = motionEvent.getY();
        if (this.f1146b != null) {
            View view = this.f1146b.f1102a;
            if (a(view, x2, y2, this.i + this.g, this.j + this.h)) {
                return view;
            }
        }
        for (int size = this.o.size() - 1; size >= 0; size--) {
            c cVar = (c) this.o.get(size);
            View view2 = cVar.h.f1102a;
            if (a(view2, x2, y2, cVar.l, cVar.m)) {
                return view2;
            }
        }
        return this.p.a(x2, y2);
    }

    public void a(Canvas canvas, RecyclerView recyclerView, RecyclerView.t tVar) {
        float f2;
        float f3 = 0.0f;
        if (this.f1146b != null) {
            a(this.v);
            f2 = this.v[0];
            f3 = this.v[1];
        } else {
            f2 = 0.0f;
        }
        this.l.b(canvas, recyclerView, this.f1146b, this.o, this.m, f2, f3);
    }

    public void a(Rect rect, View view, RecyclerView recyclerView, RecyclerView.t tVar) {
        rect.setEmpty();
    }

    /* access modifiers changed from: 0000 */
    public void a(w wVar) {
        if (!this.p.isLayoutRequested() && this.m == 2) {
            float b2 = this.l.b(wVar);
            int i2 = (int) (this.i + this.g);
            int i3 = (int) (this.j + this.h);
            if (((float) Math.abs(i3 - wVar.f1102a.getTop())) >= ((float) wVar.f1102a.getHeight()) * b2 || ((float) Math.abs(i2 - wVar.f1102a.getLeft())) >= b2 * ((float) wVar.f1102a.getWidth())) {
                List b3 = b(wVar);
                if (b3.size() != 0) {
                    w a2 = this.l.a(wVar, b3, i2, i3);
                    if (a2 == null) {
                        this.x.clear();
                        this.y.clear();
                        return;
                    }
                    int e2 = a2.e();
                    int e3 = wVar.e();
                    if (this.l.b(this.p, wVar, a2)) {
                        this.l.a(this.p, wVar, e3, a2, e2, i2, i3);
                    }
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(w wVar, int i2) {
        float f2;
        float signum;
        if (wVar != this.f1146b || i2 != this.m) {
            this.D = Long.MIN_VALUE;
            int i3 = this.m;
            a(wVar, true);
            this.m = i2;
            if (i2 == 2) {
                this.s = wVar.f1102a;
                i();
            }
            int i4 = (1 << ((i2 * 8) + 8)) - 1;
            boolean z2 = false;
            if (this.f1146b != null) {
                w wVar2 = this.f1146b;
                if (wVar2.f1102a.getParent() != null) {
                    final int c2 = i3 == 2 ? 0 : c(wVar2);
                    h();
                    switch (c2) {
                        case 1:
                        case 2:
                            f2 = 0.0f;
                            signum = Math.signum(this.h) * ((float) this.p.getHeight());
                            break;
                        case 4:
                        case 8:
                        case 16:
                        case 32:
                            signum = 0.0f;
                            f2 = Math.signum(this.g) * ((float) this.p.getWidth());
                            break;
                        default:
                            f2 = 0.0f;
                            signum = 0.0f;
                            break;
                    }
                    int i5 = i3 == 2 ? 8 : c2 > 0 ? 2 : 4;
                    a(this.v);
                    float f3 = this.v[0];
                    float f4 = this.v[1];
                    final w wVar3 = wVar2;
                    AnonymousClass3 r0 = new c(wVar2, i5, i3, f3, f4, f2, signum) {
                        public void onAnimationEnd(Animator animator) {
                            super.onAnimationEnd(animator);
                            if (!this.n) {
                                if (c2 <= 0) {
                                    a.this.l.d(a.this.p, wVar3);
                                } else {
                                    a.this.f1145a.add(wVar3.f1102a);
                                    this.k = true;
                                    if (c2 > 0) {
                                        a.this.a((c) this, c2);
                                    }
                                }
                                if (a.this.s == wVar3.f1102a) {
                                    a.this.c(wVar3.f1102a);
                                }
                            }
                        }
                    };
                    r0.a(this.l.a(this.p, i5, f2 - f3, signum - f4));
                    this.o.add(r0);
                    r0.a();
                    z2 = true;
                } else {
                    c(wVar2.f1102a);
                    this.l.d(this.p, wVar2);
                }
                this.f1146b = null;
            }
            boolean z3 = z2;
            if (wVar != null) {
                this.n = (this.l.b(this.p, wVar) & i4) >> (this.m * 8);
                this.i = (float) wVar.f1102a.getLeft();
                this.j = (float) wVar.f1102a.getTop();
                this.f1146b = wVar;
                if (i2 == 2) {
                    this.f1146b.f1102a.performHapticFeedback(0);
                }
            }
            ViewParent parent = this.p.getParent();
            if (parent != null) {
                parent.requestDisallowInterceptTouchEvent(this.f1146b != null);
            }
            if (!z3) {
                this.p.getLayoutManager().H();
            }
            this.l.b(this.f1146b, this.m);
            this.p.invalidate();
        }
    }

    public void a(RecyclerView recyclerView) {
        if (this.p != recyclerView) {
            if (this.p != null) {
                e();
            }
            this.p = recyclerView;
            if (this.p != null) {
                Resources resources = recyclerView.getResources();
                this.e = resources.getDimension(C0030a.item_touch_helper_swipe_escape_velocity);
                this.f = resources.getDimension(C0030a.item_touch_helper_swipe_escape_max_velocity);
                d();
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(final c cVar, final int i2) {
        this.p.post(new Runnable() {
            public void run() {
                if (a.this.p != null && a.this.p.isAttachedToWindow() && !cVar.n && cVar.h.e() != -1) {
                    android.support.v7.widget.RecyclerView.e itemAnimator = a.this.p.getItemAnimator();
                    if ((itemAnimator == null || !itemAnimator.a((android.support.v7.widget.RecyclerView.e.a) null)) && !a.this.a()) {
                        a.this.l.a(cVar.h, i2);
                    } else {
                        a.this.p.post(this);
                    }
                }
            }
        });
    }

    /* access modifiers changed from: 0000 */
    public void a(MotionEvent motionEvent, int i2, int i3) {
        float x2 = motionEvent.getX(i3);
        float y2 = motionEvent.getY(i3);
        this.g = x2 - this.c;
        this.h = y2 - this.d;
        if ((i2 & 4) == 0) {
            this.g = Math.max(0.0f, this.g);
        }
        if ((i2 & 8) == 0) {
            this.g = Math.min(0.0f, this.g);
        }
        if ((i2 & 1) == 0) {
            this.h = Math.max(0.0f, this.h);
        }
        if ((i2 & 2) == 0) {
            this.h = Math.min(0.0f, this.h);
        }
    }

    public void a(View view) {
    }

    /* access modifiers changed from: 0000 */
    public boolean a() {
        int size = this.o.size();
        for (int i2 = 0; i2 < size; i2++) {
            if (!((c) this.o.get(i2)).o) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: 0000 */
    public boolean a(int i2, MotionEvent motionEvent, int i3) {
        if (this.f1146b != null || i2 != 2 || this.m == 2 || !this.l.b() || this.p.getScrollState() == 1) {
            return false;
        }
        w c2 = c(motionEvent);
        if (c2 == null) {
            return false;
        }
        int b2 = (this.l.b(this.p, c2) & 65280) >> 8;
        if (b2 == 0) {
            return false;
        }
        float x2 = motionEvent.getX(i3);
        float f2 = x2 - this.c;
        float y2 = motionEvent.getY(i3) - this.d;
        float abs = Math.abs(f2);
        float abs2 = Math.abs(y2);
        if (abs < ((float) this.w) && abs2 < ((float) this.w)) {
            return false;
        }
        if (abs > abs2) {
            if (f2 < 0.0f && (b2 & 4) == 0) {
                return false;
            }
            if (f2 > 0.0f && (b2 & 8) == 0) {
                return false;
            }
        } else if (y2 < 0.0f && (b2 & 1) == 0) {
            return false;
        } else {
            if (y2 > 0.0f && (b2 & 2) == 0) {
                return false;
            }
        }
        this.h = 0.0f;
        this.g = 0.0f;
        this.k = motionEvent.getPointerId(0);
        a(c2, 1);
        return true;
    }

    /* access modifiers changed from: 0000 */
    public c b(MotionEvent motionEvent) {
        if (this.o.isEmpty()) {
            return null;
        }
        View a2 = a(motionEvent);
        for (int size = this.o.size() - 1; size >= 0; size--) {
            c cVar = (c) this.o.get(size);
            if (cVar.h.f1102a == a2) {
                return cVar;
            }
        }
        return null;
    }

    public void b(Canvas canvas, RecyclerView recyclerView, RecyclerView.t tVar) {
        float f2;
        float f3 = 0.0f;
        this.t = -1;
        if (this.f1146b != null) {
            a(this.v);
            f2 = this.v[0];
            f3 = this.v[1];
        } else {
            f2 = 0.0f;
        }
        this.l.a(canvas, recyclerView, this.f1146b, this.o, this.m, f2, f3);
    }

    public void b(View view) {
        c(view);
        w b2 = this.p.b(view);
        if (b2 != null) {
            if (this.f1146b == null || b2 != this.f1146b) {
                a(b2, false);
                if (this.f1145a.remove(b2.f1102a)) {
                    this.l.d(this.p, b2);
                    return;
                }
                return;
            }
            a((w) null, 0);
        }
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00e4, code lost:
        if (r4 <= 0) goto L_0x00e6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x010c, code lost:
        if (r8 <= 0) goto L_0x010e;
     */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0059  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0077  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0090  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x00a9 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00b1  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0115  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0117  */
    public boolean b() {
        int i2;
        int i3;
        int i4;
        int i5;
        if (this.f1146b == null) {
            this.D = Long.MIN_VALUE;
            return false;
        }
        long currentTimeMillis = System.currentTimeMillis();
        long j2 = this.D == Long.MIN_VALUE ? 0 : currentTimeMillis - this.D;
        h layoutManager = this.p.getLayoutManager();
        if (this.C == null) {
            this.C = new Rect();
        }
        layoutManager.b(this.f1146b.f1102a, this.C);
        if (layoutManager.d()) {
            int i6 = (int) (this.i + this.g);
            i2 = (i6 - this.C.left) - this.p.getPaddingLeft();
            if (this.g >= 0.0f || i2 >= 0) {
                if (this.g > 0.0f) {
                    i2 = ((i6 + this.f1146b.f1102a.getWidth()) + this.C.right) - (this.p.getWidth() - this.p.getPaddingRight());
                }
            }
            if (layoutManager.e()) {
                int i7 = (int) (this.j + this.h);
                i3 = (i7 - this.C.top) - this.p.getPaddingTop();
                if (this.h >= 0.0f || i3 >= 0) {
                    if (this.h > 0.0f) {
                        i3 = ((i7 + this.f1146b.f1102a.getHeight()) + this.C.bottom) - (this.p.getHeight() - this.p.getPaddingBottom());
                    }
                }
                i4 = i2 != 0 ? this.l.a(this.p, this.f1146b.f1102a.getWidth(), i2, this.p.getWidth(), j2) : i2;
                i5 = i3 != 0 ? this.l.a(this.p, this.f1146b.f1102a.getHeight(), i3, this.p.getHeight(), j2) : i3;
                if (i4 == 0 || i5 != 0) {
                    if (this.D == Long.MIN_VALUE) {
                        this.D = currentTimeMillis;
                    }
                    this.p.scrollBy(i4, i5);
                    return true;
                }
                this.D = Long.MIN_VALUE;
                return false;
            }
            i3 = 0;
            if (i2 != 0) {
            }
            if (i3 != 0) {
            }
            if (i4 == 0) {
            }
            if (this.D == Long.MIN_VALUE) {
            }
            this.p.scrollBy(i4, i5);
            return true;
        }
        i2 = 0;
        if (layoutManager.e()) {
        }
        i3 = 0;
        if (i2 != 0) {
        }
        if (i3 != 0) {
        }
        if (i4 == 0) {
        }
        if (this.D == Long.MIN_VALUE) {
        }
        this.p.scrollBy(i4, i5);
        return true;
    }

    /* access modifiers changed from: 0000 */
    public void c() {
        if (this.r != null) {
            this.r.recycle();
        }
        this.r = VelocityTracker.obtain();
    }

    /* access modifiers changed from: 0000 */
    public void c(View view) {
        if (view == this.s) {
            this.s = null;
            if (this.z != null) {
                this.p.setChildDrawingOrderCallback(null);
            }
        }
    }
}
