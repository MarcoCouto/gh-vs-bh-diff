package android.support.v7.widget.a;

import android.graphics.Canvas;
import android.support.v4.i.t;
import android.support.v7.widget.RecyclerView;
import android.view.View;

class c {

    static class a extends b {
        a() {
        }

        private float a(RecyclerView recyclerView, View view) {
            int childCount = recyclerView.getChildCount();
            float f = 0.0f;
            for (int i = 0; i < childCount; i++) {
                View childAt = recyclerView.getChildAt(i);
                if (childAt != view) {
                    float k = t.k(childAt);
                    if (k > f) {
                        f = k;
                    }
                }
            }
            return f;
        }

        public void a(Canvas canvas, RecyclerView recyclerView, View view, float f, float f2, int i, boolean z) {
            if (z && view.getTag(android.support.v7.e.a.b.item_touch_helper_previous_elevation) == null) {
                Float valueOf = Float.valueOf(t.k(view));
                t.a(view, 1.0f + a(recyclerView, view));
                view.setTag(android.support.v7.e.a.b.item_touch_helper_previous_elevation, valueOf);
            }
            super.a(canvas, recyclerView, view, f, f2, i, z);
        }

        public void a(View view) {
            Object tag = view.getTag(android.support.v7.e.a.b.item_touch_helper_previous_elevation);
            if (tag != null && (tag instanceof Float)) {
                t.a(view, ((Float) tag).floatValue());
            }
            view.setTag(android.support.v7.e.a.b.item_touch_helper_previous_elevation, null);
            super.a(view);
        }
    }

    static class b implements b {
        b() {
        }

        public void a(Canvas canvas, RecyclerView recyclerView, View view, float f, float f2, int i, boolean z) {
            view.setTranslationX(f);
            view.setTranslationY(f2);
        }

        public void a(View view) {
            view.setTranslationX(0.0f);
            view.setTranslationY(0.0f);
        }

        public void b(Canvas canvas, RecyclerView recyclerView, View view, float f, float f2, int i, boolean z) {
        }

        public void b(View view) {
        }
    }
}
