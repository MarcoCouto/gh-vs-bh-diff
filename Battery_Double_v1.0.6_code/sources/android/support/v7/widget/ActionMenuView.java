package android.support.v7.widget;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.support.v7.view.menu.ActionMenuItemView;
import android.support.v7.view.menu.h;
import android.support.v7.view.menu.j;
import android.support.v7.view.menu.o;
import android.support.v7.view.menu.p;
import android.util.AttributeSet;
import android.view.ContextThemeWrapper;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewDebug.ExportedProperty;
import android.view.ViewGroup.LayoutParams;
import android.view.accessibility.AccessibilityEvent;

public class ActionMenuView extends at implements android.support.v7.view.menu.h.b, p {

    /* renamed from: a reason: collision with root package name */
    android.support.v7.view.menu.h.a f1029a;

    /* renamed from: b reason: collision with root package name */
    e f1030b;
    private h c;
    private Context d;
    private int e;
    private boolean f;
    private d g;
    private android.support.v7.view.menu.o.a h;
    private boolean i;
    private int j;
    private int k;
    private int l;

    public interface a {
        boolean c();

        boolean d();
    }

    private static class b implements android.support.v7.view.menu.o.a {
        b() {
        }

        public void a(h hVar, boolean z) {
        }

        public boolean a(h hVar) {
            return false;
        }
    }

    public static class c extends android.support.v7.widget.at.a {
        @ExportedProperty

        /* renamed from: a reason: collision with root package name */
        public boolean f1031a;
        @ExportedProperty

        /* renamed from: b reason: collision with root package name */
        public int f1032b;
        @ExportedProperty
        public int c;
        @ExportedProperty
        public boolean d;
        @ExportedProperty
        public boolean e;
        boolean f;

        public c(int i, int i2) {
            super(i, i2);
            this.f1031a = false;
        }

        public c(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }

        public c(c cVar) {
            super(cVar);
            this.f1031a = cVar.f1031a;
        }

        public c(LayoutParams layoutParams) {
            super(layoutParams);
        }
    }

    private class d implements android.support.v7.view.menu.h.a {
        d() {
        }

        public void a(h hVar) {
            if (ActionMenuView.this.f1029a != null) {
                ActionMenuView.this.f1029a.a(hVar);
            }
        }

        public boolean a(h hVar, MenuItem menuItem) {
            return ActionMenuView.this.f1030b != null && ActionMenuView.this.f1030b.a(menuItem);
        }
    }

    public interface e {
        boolean a(MenuItem menuItem);
    }

    public ActionMenuView(Context context) {
        this(context, null);
    }

    public ActionMenuView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setBaselineAligned(false);
        float f2 = context.getResources().getDisplayMetrics().density;
        this.k = (int) (56.0f * f2);
        this.l = (int) (f2 * 4.0f);
        this.d = context;
        this.e = 0;
    }

    static int a(View view, int i2, int i3, int i4, int i5) {
        int i6;
        boolean z = false;
        c cVar = (c) view.getLayoutParams();
        int makeMeasureSpec = MeasureSpec.makeMeasureSpec(MeasureSpec.getSize(i4) - i5, MeasureSpec.getMode(i4));
        ActionMenuItemView actionMenuItemView = view instanceof ActionMenuItemView ? (ActionMenuItemView) view : null;
        boolean z2 = actionMenuItemView != null && actionMenuItemView.b();
        if (i3 <= 0 || (z2 && i3 < 2)) {
            i6 = 0;
        } else {
            view.measure(MeasureSpec.makeMeasureSpec(i2 * i3, Integer.MIN_VALUE), makeMeasureSpec);
            int measuredWidth = view.getMeasuredWidth();
            i6 = measuredWidth / i2;
            if (measuredWidth % i2 != 0) {
                i6++;
            }
            if (z2 && i6 < 2) {
                i6 = 2;
            }
        }
        if (!cVar.f1031a && z2) {
            z = true;
        }
        cVar.d = z;
        cVar.f1032b = i6;
        view.measure(MeasureSpec.makeMeasureSpec(i6 * i2, 1073741824), makeMeasureSpec);
        return i6;
    }

    /* JADX WARNING: Removed duplicated region for block: B:108:0x0276  */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x01cf  */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x01de  */
    private void c(int i2, int i3) {
        long j2;
        boolean z;
        float f2;
        int i4;
        boolean z2;
        int i5;
        int i6;
        int i7;
        int i8;
        int i9;
        int i10;
        int i11;
        long j3;
        int mode = MeasureSpec.getMode(i3);
        int size = MeasureSpec.getSize(i2);
        int size2 = MeasureSpec.getSize(i3);
        int paddingLeft = getPaddingLeft() + getPaddingRight();
        int paddingTop = getPaddingTop() + getPaddingBottom();
        int childMeasureSpec = getChildMeasureSpec(i3, paddingTop, -2);
        int i12 = size - paddingLeft;
        int i13 = i12 / this.k;
        int i14 = i12 % this.k;
        if (i13 == 0) {
            setMeasuredDimension(i12, 0);
            return;
        }
        int i15 = this.k + (i14 / i13);
        int i16 = 0;
        int i17 = 0;
        int i18 = 0;
        int i19 = 0;
        boolean z3 = false;
        long j4 = 0;
        int childCount = getChildCount();
        int i20 = 0;
        while (i20 < childCount) {
            View childAt = getChildAt(i20);
            if (childAt.getVisibility() == 8) {
                i8 = i19;
                j3 = j4;
                i10 = i16;
                i11 = i13;
                i9 = i17;
            } else {
                boolean z4 = childAt instanceof ActionMenuItemView;
                int i21 = i19 + 1;
                if (z4) {
                    childAt.setPadding(this.l, 0, this.l, 0);
                }
                c cVar = (c) childAt.getLayoutParams();
                cVar.f = false;
                cVar.c = 0;
                cVar.f1032b = 0;
                cVar.d = false;
                cVar.leftMargin = 0;
                cVar.rightMargin = 0;
                cVar.e = z4 && ((ActionMenuItemView) childAt).b();
                int a2 = a(childAt, i15, cVar.f1031a ? 1 : i13, childMeasureSpec, paddingTop);
                int max = Math.max(i17, a2);
                int i22 = cVar.d ? i18 + 1 : i18;
                boolean z5 = cVar.f1031a ? true : z3;
                int i23 = i13 - a2;
                int max2 = Math.max(i16, childAt.getMeasuredHeight());
                if (a2 == 1) {
                    long j5 = ((long) (1 << i20)) | j4;
                    i10 = max2;
                    i11 = i23;
                    i18 = i22;
                    z3 = z5;
                    j3 = j5;
                    i9 = max;
                    i8 = i21;
                } else {
                    i8 = i21;
                    i9 = max;
                    long j6 = j4;
                    i10 = max2;
                    i11 = i23;
                    z3 = z5;
                    i18 = i22;
                    j3 = j6;
                }
            }
            i20++;
            i17 = i9;
            i16 = i10;
            i13 = i11;
            j4 = j3;
            i19 = i8;
        }
        boolean z6 = z3 && i19 == 2;
        boolean z7 = false;
        long j7 = j4;
        int i24 = i13;
        while (true) {
            if (i18 <= 0 || i24 <= 0) {
                j2 = j7;
            } else {
                int i25 = Integer.MAX_VALUE;
                long j8 = 0;
                int i26 = 0;
                int i27 = 0;
                while (i27 < childCount) {
                    c cVar2 = (c) getChildAt(i27).getLayoutParams();
                    if (!cVar2.d) {
                        i6 = i26;
                        i7 = i25;
                    } else if (cVar2.f1032b < i25) {
                        i7 = cVar2.f1032b;
                        j8 = (long) (1 << i27);
                        i6 = 1;
                    } else if (cVar2.f1032b == i25) {
                        j8 |= (long) (1 << i27);
                        i6 = i26 + 1;
                        i7 = i25;
                    } else {
                        i6 = i26;
                        i7 = i25;
                    }
                    i27++;
                    i25 = i7;
                    i26 = i6;
                }
                long j9 = j7 | j8;
                if (i26 > i24) {
                    j2 = j9;
                    break;
                }
                int i28 = i25 + 1;
                int i29 = 0;
                int i30 = i24;
                long j10 = j9;
                while (i29 < childCount) {
                    View childAt2 = getChildAt(i29);
                    c cVar3 = (c) childAt2.getLayoutParams();
                    if ((((long) (1 << i29)) & j8) != 0) {
                        if (z6 && cVar3.e && i30 == 1) {
                            childAt2.setPadding(this.l + i15, 0, this.l, 0);
                        }
                        cVar3.f1032b++;
                        cVar3.f = true;
                        i5 = i30 - 1;
                    } else if (cVar3.f1032b == i28) {
                        j10 |= (long) (1 << i29);
                        i5 = i30;
                    } else {
                        i5 = i30;
                    }
                    i29++;
                    i30 = i5;
                }
                j7 = j10;
                z7 = true;
                i24 = i30;
            }
        }
        boolean z8 = !z3 && i19 == 1;
        if (i24 <= 0 || j2 == 0 || (i24 >= i19 - 1 && !z8 && i17 <= 1)) {
            z = z7;
        } else {
            float bitCount = (float) Long.bitCount(j2);
            if (!z8) {
                if ((1 & j2) != 0 && !((c) getChildAt(0).getLayoutParams()).e) {
                    bitCount -= 0.5f;
                }
                if ((((long) (1 << (childCount - 1))) & j2) != 0) {
                    if (!((c) getChildAt(childCount - 1).getLayoutParams()).e) {
                        f2 = bitCount - 0.5f;
                        int i31 = f2 <= 0.0f ? (int) (((float) (i24 * i15)) / f2) : 0;
                        i4 = 0;
                        z = z7;
                        while (i4 < childCount) {
                            if ((((long) (1 << i4)) & j2) == 0) {
                                z2 = z;
                            } else {
                                View childAt3 = getChildAt(i4);
                                c cVar4 = (c) childAt3.getLayoutParams();
                                if (childAt3 instanceof ActionMenuItemView) {
                                    cVar4.c = i31;
                                    cVar4.f = true;
                                    if (i4 == 0 && !cVar4.e) {
                                        cVar4.leftMargin = (-i31) / 2;
                                    }
                                    z2 = true;
                                } else if (cVar4.f1031a) {
                                    cVar4.c = i31;
                                    cVar4.f = true;
                                    cVar4.rightMargin = (-i31) / 2;
                                    z2 = true;
                                } else {
                                    if (i4 != 0) {
                                        cVar4.leftMargin = i31 / 2;
                                    }
                                    if (i4 != childCount - 1) {
                                        cVar4.rightMargin = i31 / 2;
                                    }
                                    z2 = z;
                                }
                            }
                            i4++;
                            z = z2;
                        }
                    }
                }
            }
            f2 = bitCount;
            if (f2 <= 0.0f) {
            }
            i4 = 0;
            z = z7;
            while (i4 < childCount) {
            }
        }
        if (z) {
            int i32 = 0;
            while (true) {
                int i33 = i32;
                if (i33 >= childCount) {
                    break;
                }
                View childAt4 = getChildAt(i33);
                c cVar5 = (c) childAt4.getLayoutParams();
                if (cVar5.f) {
                    childAt4.measure(MeasureSpec.makeMeasureSpec(cVar5.c + (cVar5.f1032b * i15), 1073741824), childMeasureSpec);
                }
                i32 = i33 + 1;
            }
        }
        if (mode == 1073741824) {
            i16 = size2;
        }
        setMeasuredDimension(i12, i16);
    }

    /* renamed from: a */
    public c generateLayoutParams(AttributeSet attributeSet) {
        return new c(getContext(), attributeSet);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public c generateLayoutParams(LayoutParams layoutParams) {
        if (layoutParams == null) {
            return j();
        }
        c cVar = layoutParams instanceof c ? new c((c) layoutParams) : new c(layoutParams);
        if (cVar.h > 0) {
            return cVar;
        }
        cVar.h = 16;
        return cVar;
    }

    public void a(h hVar) {
        this.c = hVar;
    }

    public void a(android.support.v7.view.menu.o.a aVar, android.support.v7.view.menu.h.a aVar2) {
        this.h = aVar;
        this.f1029a = aVar2;
    }

    public boolean a() {
        return this.f;
    }

    /* access modifiers changed from: protected */
    public boolean a(int i2) {
        boolean z = false;
        if (i2 == 0) {
            return false;
        }
        View childAt = getChildAt(i2 - 1);
        View childAt2 = getChildAt(i2);
        if (i2 < getChildCount() && (childAt instanceof a)) {
            z = false | ((a) childAt).d();
        }
        return (i2 <= 0 || !(childAt2 instanceof a)) ? z : ((a) childAt2).c() | z;
    }

    public boolean a(j jVar) {
        return this.c.a((MenuItem) jVar, 0);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public c j() {
        c cVar = new c(-2, -2);
        cVar.h = 16;
        return cVar;
    }

    public c c() {
        c b2 = j();
        b2.f1031a = true;
        return b2;
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(LayoutParams layoutParams) {
        return layoutParams != null && (layoutParams instanceof c);
    }

    public h d() {
        return this.c;
    }

    public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        return false;
    }

    public boolean e() {
        return this.g != null && this.g.f();
    }

    public boolean f() {
        return this.g != null && this.g.g();
    }

    public boolean g() {
        return this.g != null && this.g.j();
    }

    public Menu getMenu() {
        if (this.c == null) {
            Context context = getContext();
            this.c = new h(context);
            this.c.a((android.support.v7.view.menu.h.a) new d());
            this.g = new d(context);
            this.g.c(true);
            this.g.a(this.h != null ? this.h : new b());
            this.c.a((o) this.g, this.d);
            this.g.a(this);
        }
        return this.c;
    }

    public Drawable getOverflowIcon() {
        getMenu();
        return this.g.e();
    }

    public int getPopupTheme() {
        return this.e;
    }

    public int getWindowAnimations() {
        return 0;
    }

    public boolean h() {
        return this.g != null && this.g.k();
    }

    public void i() {
        if (this.g != null) {
            this.g.h();
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        if (this.g != null) {
            this.g.a(false);
            if (this.g.j()) {
                this.g.g();
                this.g.f();
            }
        }
    }

    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        i();
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        int i6;
        int i7;
        int i8;
        int i9;
        int i10;
        boolean z2;
        int width;
        int i11;
        if (!this.i) {
            super.onLayout(z, i2, i3, i4, i5);
            return;
        }
        int childCount = getChildCount();
        int i12 = (i5 - i3) / 2;
        int dividerWidth = getDividerWidth();
        int i13 = 0;
        int i14 = 0;
        int paddingRight = ((i4 - i2) - getPaddingRight()) - getPaddingLeft();
        boolean z3 = false;
        boolean a2 = bw.a(this);
        int i15 = 0;
        while (i15 < childCount) {
            View childAt = getChildAt(i15);
            if (childAt.getVisibility() == 8) {
                z2 = z3;
                i9 = i14;
                i8 = paddingRight;
                i10 = i13;
            } else {
                c cVar = (c) childAt.getLayoutParams();
                if (cVar.f1031a) {
                    int measuredWidth = childAt.getMeasuredWidth();
                    if (a(i15)) {
                        measuredWidth += dividerWidth;
                    }
                    int measuredHeight = childAt.getMeasuredHeight();
                    if (a2) {
                        i11 = cVar.leftMargin + getPaddingLeft();
                        width = i11 + measuredWidth;
                    } else {
                        width = (getWidth() - getPaddingRight()) - cVar.rightMargin;
                        i11 = width - measuredWidth;
                    }
                    int i16 = i12 - (measuredHeight / 2);
                    childAt.layout(i11, i16, width, measuredHeight + i16);
                    i8 = paddingRight - measuredWidth;
                    z2 = true;
                    i9 = i14;
                    i10 = i13;
                } else {
                    int measuredWidth2 = childAt.getMeasuredWidth() + cVar.leftMargin + cVar.rightMargin;
                    int i17 = i13 + measuredWidth2;
                    int i18 = paddingRight - measuredWidth2;
                    if (a(i15)) {
                        i17 += dividerWidth;
                    }
                    boolean z4 = z3;
                    i8 = i18;
                    i9 = i14 + 1;
                    i10 = i17;
                    z2 = z4;
                }
            }
            i15++;
            i13 = i10;
            paddingRight = i8;
            i14 = i9;
            z3 = z2;
        }
        if (childCount != 1 || z3) {
            int i19 = i14 - (z3 ? 0 : 1);
            int max = Math.max(0, i19 > 0 ? paddingRight / i19 : 0);
            if (a2) {
                int width2 = getWidth() - getPaddingRight();
                int i20 = 0;
                while (i20 < childCount) {
                    View childAt2 = getChildAt(i20);
                    c cVar2 = (c) childAt2.getLayoutParams();
                    if (childAt2.getVisibility() == 8) {
                        i7 = width2;
                    } else if (cVar2.f1031a) {
                        i7 = width2;
                    } else {
                        int i21 = width2 - cVar2.rightMargin;
                        int measuredWidth3 = childAt2.getMeasuredWidth();
                        int measuredHeight2 = childAt2.getMeasuredHeight();
                        int i22 = i12 - (measuredHeight2 / 2);
                        childAt2.layout(i21 - measuredWidth3, i22, i21, measuredHeight2 + i22);
                        i7 = i21 - ((cVar2.leftMargin + measuredWidth3) + max);
                    }
                    i20++;
                    width2 = i7;
                }
                return;
            }
            int paddingLeft = getPaddingLeft();
            int i23 = 0;
            while (i23 < childCount) {
                View childAt3 = getChildAt(i23);
                c cVar3 = (c) childAt3.getLayoutParams();
                if (childAt3.getVisibility() == 8) {
                    i6 = paddingLeft;
                } else if (cVar3.f1031a) {
                    i6 = paddingLeft;
                } else {
                    int i24 = paddingLeft + cVar3.leftMargin;
                    int measuredWidth4 = childAt3.getMeasuredWidth();
                    int measuredHeight3 = childAt3.getMeasuredHeight();
                    int i25 = i12 - (measuredHeight3 / 2);
                    childAt3.layout(i24, i25, i24 + measuredWidth4, measuredHeight3 + i25);
                    i6 = cVar3.rightMargin + measuredWidth4 + max + i24;
                }
                i23++;
                paddingLeft = i6;
            }
            return;
        }
        View childAt4 = getChildAt(0);
        int measuredWidth5 = childAt4.getMeasuredWidth();
        int measuredHeight4 = childAt4.getMeasuredHeight();
        int i26 = ((i4 - i2) / 2) - (measuredWidth5 / 2);
        int i27 = i12 - (measuredHeight4 / 2);
        childAt4.layout(i26, i27, measuredWidth5 + i26, measuredHeight4 + i27);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        boolean z = this.i;
        this.i = MeasureSpec.getMode(i2) == 1073741824;
        if (z != this.i) {
            this.j = 0;
        }
        int size = MeasureSpec.getSize(i2);
        if (!(!this.i || this.c == null || size == this.j)) {
            this.j = size;
            this.c.b(true);
        }
        int childCount = getChildCount();
        if (!this.i || childCount <= 0) {
            for (int i4 = 0; i4 < childCount; i4++) {
                c cVar = (c) getChildAt(i4).getLayoutParams();
                cVar.rightMargin = 0;
                cVar.leftMargin = 0;
            }
            super.onMeasure(i2, i3);
            return;
        }
        c(i2, i3);
    }

    public void setExpandedActionViewsExclusive(boolean z) {
        this.g.d(z);
    }

    public void setOnMenuItemClickListener(e eVar) {
        this.f1030b = eVar;
    }

    public void setOverflowIcon(Drawable drawable) {
        getMenu();
        this.g.a(drawable);
    }

    public void setOverflowReserved(boolean z) {
        this.f = z;
    }

    public void setPopupTheme(int i2) {
        if (this.e != i2) {
            this.e = i2;
            if (i2 == 0) {
                this.d = getContext();
            } else {
                this.d = new ContextThemeWrapper(getContext(), i2);
            }
        }
    }

    public void setPresenter(d dVar) {
        this.g = dVar;
        this.g.a(this);
    }
}
