package android.support.v7.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.support.v7.a.a.C0026a;
import android.util.AttributeSet;
import android.widget.SeekBar;

public class w extends SeekBar {

    /* renamed from: a reason: collision with root package name */
    private final x f1339a;

    public w(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, C0026a.seekBarStyle);
    }

    public w(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f1339a = new x(this);
        this.f1339a.a(attributeSet, i);
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        this.f1339a.c();
    }

    public void jumpDrawablesToCurrentState() {
        super.jumpDrawablesToCurrentState();
        this.f1339a.b();
    }

    /* access modifiers changed from: protected */
    public synchronized void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        this.f1339a.a(canvas);
    }
}
