package android.support.v7.widget;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.RippleDrawable;
import android.os.Build.VERSION;
import android.support.v4.widget.i;
import android.support.v7.a.a.j;
import android.support.v7.c.a.b;
import android.util.AttributeSet;
import android.widget.ImageView;

public class p {

    /* renamed from: a reason: collision with root package name */
    private final ImageView f1326a;

    /* renamed from: b reason: collision with root package name */
    private bm f1327b;
    private bm c;
    private bm d;

    public p(ImageView imageView) {
        this.f1326a = imageView;
    }

    private boolean a(Drawable drawable) {
        if (this.d == null) {
            this.d = new bm();
        }
        bm bmVar = this.d;
        bmVar.a();
        ColorStateList a2 = i.a(this.f1326a);
        if (a2 != null) {
            bmVar.d = true;
            bmVar.f1260a = a2;
        }
        Mode b2 = i.b(this.f1326a);
        if (b2 != null) {
            bmVar.c = true;
            bmVar.f1261b = b2;
        }
        if (!bmVar.d && !bmVar.c) {
            return false;
        }
        m.a(drawable, bmVar, this.f1326a.getDrawableState());
        return true;
    }

    private boolean e() {
        int i = VERSION.SDK_INT;
        return i > 21 ? this.f1327b != null : i == 21;
    }

    public void a(int i) {
        if (i != 0) {
            Drawable b2 = b.b(this.f1326a.getContext(), i);
            if (b2 != null) {
                am.a(b2);
            }
            this.f1326a.setImageDrawable(b2);
        } else {
            this.f1326a.setImageDrawable(null);
        }
        d();
    }

    /* access modifiers changed from: 0000 */
    public void a(ColorStateList colorStateList) {
        if (this.c == null) {
            this.c = new bm();
        }
        this.c.f1260a = colorStateList;
        this.c.d = true;
        d();
    }

    /* access modifiers changed from: 0000 */
    public void a(Mode mode) {
        if (this.c == null) {
            this.c = new bm();
        }
        this.c.f1261b = mode;
        this.c.c = true;
        d();
    }

    public void a(AttributeSet attributeSet, int i) {
        bo a2 = bo.a(this.f1326a.getContext(), attributeSet, j.AppCompatImageView, i, 0);
        try {
            Drawable drawable = this.f1326a.getDrawable();
            if (drawable == null) {
                int g = a2.g(j.AppCompatImageView_srcCompat, -1);
                if (g != -1) {
                    drawable = b.b(this.f1326a.getContext(), g);
                    if (drawable != null) {
                        this.f1326a.setImageDrawable(drawable);
                    }
                }
            }
            if (drawable != null) {
                am.a(drawable);
            }
            if (a2.g(j.AppCompatImageView_tint)) {
                i.a(this.f1326a, a2.e(j.AppCompatImageView_tint));
            }
            if (a2.g(j.AppCompatImageView_tintMode)) {
                i.a(this.f1326a, am.a(a2.a(j.AppCompatImageView_tintMode, -1), null));
            }
        } finally {
            a2.a();
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean a() {
        return VERSION.SDK_INT < 21 || !(this.f1326a.getBackground() instanceof RippleDrawable);
    }

    /* access modifiers changed from: 0000 */
    public ColorStateList b() {
        if (this.c != null) {
            return this.c.f1260a;
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    public Mode c() {
        if (this.c != null) {
            return this.c.f1261b;
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    public void d() {
        Drawable drawable = this.f1326a.getDrawable();
        if (drawable != null) {
            am.a(drawable);
        }
        if (drawable == null) {
            return;
        }
        if (e() && a(drawable)) {
            return;
        }
        if (this.c != null) {
            m.a(drawable, this.c, this.f1326a.getDrawableState());
        } else if (this.f1327b != null) {
            m.a(drawable, this.f1327b, this.f1326a.getDrawableState());
        }
    }
}
