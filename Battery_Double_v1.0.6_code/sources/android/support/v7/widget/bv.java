package android.support.v7.widget;

import android.support.v4.h.f;
import android.support.v7.widget.RecyclerView.e.c;
import android.support.v7.widget.RecyclerView.w;

class bv {

    /* renamed from: a reason: collision with root package name */
    final android.support.v4.h.a<w, a> f1283a = new android.support.v4.h.a<>();

    /* renamed from: b reason: collision with root package name */
    final f<w> f1284b = new f<>();

    static class a {
        static android.support.v4.h.k.a<a> d = new android.support.v4.h.k.b(20);

        /* renamed from: a reason: collision with root package name */
        int f1285a;

        /* renamed from: b reason: collision with root package name */
        c f1286b;
        c c;

        private a() {
        }

        static a a() {
            a aVar = (a) d.a();
            return aVar == null ? new a() : aVar;
        }

        static void a(a aVar) {
            aVar.f1285a = 0;
            aVar.f1286b = null;
            aVar.c = null;
            d.a(aVar);
        }

        static void b() {
            do {
            } while (d.a() != null);
        }
    }

    interface b {
        void a(w wVar);

        void a(w wVar, c cVar, c cVar2);

        void b(w wVar, c cVar, c cVar2);

        void c(w wVar, c cVar, c cVar2);
    }

    bv() {
    }

    private c a(w wVar, int i) {
        c cVar = null;
        int a2 = this.f1283a.a((Object) wVar);
        if (a2 >= 0) {
            a aVar = (a) this.f1283a.c(a2);
            if (!(aVar == null || (aVar.f1285a & i) == 0)) {
                aVar.f1285a &= i ^ -1;
                if (i == 4) {
                    cVar = aVar.f1286b;
                } else if (i == 8) {
                    cVar = aVar.c;
                } else {
                    throw new IllegalArgumentException("Must provide flag PRE or POST");
                }
                if ((aVar.f1285a & 12) == 0) {
                    this.f1283a.d(a2);
                    a.a(aVar);
                }
            }
        }
        return cVar;
    }

    /* access modifiers changed from: 0000 */
    public w a(long j) {
        return (w) this.f1284b.a(j);
    }

    /* access modifiers changed from: 0000 */
    public void a() {
        this.f1283a.clear();
        this.f1284b.c();
    }

    /* access modifiers changed from: 0000 */
    public void a(long j, w wVar) {
        this.f1284b.b(j, wVar);
    }

    /* access modifiers changed from: 0000 */
    public void a(w wVar, c cVar) {
        a aVar = (a) this.f1283a.get(wVar);
        if (aVar == null) {
            aVar = a.a();
            this.f1283a.put(wVar, aVar);
        }
        aVar.f1286b = cVar;
        aVar.f1285a |= 4;
    }

    /* access modifiers changed from: 0000 */
    public void a(b bVar) {
        for (int size = this.f1283a.size() - 1; size >= 0; size--) {
            w wVar = (w) this.f1283a.b(size);
            a aVar = (a) this.f1283a.d(size);
            if ((aVar.f1285a & 3) == 3) {
                bVar.a(wVar);
            } else if ((aVar.f1285a & 1) != 0) {
                if (aVar.f1286b == null) {
                    bVar.a(wVar);
                } else {
                    bVar.a(wVar, aVar.f1286b, aVar.c);
                }
            } else if ((aVar.f1285a & 14) == 14) {
                bVar.b(wVar, aVar.f1286b, aVar.c);
            } else if ((aVar.f1285a & 12) == 12) {
                bVar.c(wVar, aVar.f1286b, aVar.c);
            } else if ((aVar.f1285a & 4) != 0) {
                bVar.a(wVar, aVar.f1286b, null);
            } else if ((aVar.f1285a & 8) != 0) {
                bVar.b(wVar, aVar.f1286b, aVar.c);
            } else if ((aVar.f1285a & 2) != 0) {
            }
            a.a(aVar);
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean a(w wVar) {
        a aVar = (a) this.f1283a.get(wVar);
        return (aVar == null || (aVar.f1285a & 1) == 0) ? false : true;
    }

    /* access modifiers changed from: 0000 */
    public c b(w wVar) {
        return a(wVar, 4);
    }

    /* access modifiers changed from: 0000 */
    public void b() {
        a.b();
    }

    /* access modifiers changed from: 0000 */
    public void b(w wVar, c cVar) {
        a aVar = (a) this.f1283a.get(wVar);
        if (aVar == null) {
            aVar = a.a();
            this.f1283a.put(wVar, aVar);
        }
        aVar.f1285a |= 2;
        aVar.f1286b = cVar;
    }

    /* access modifiers changed from: 0000 */
    public c c(w wVar) {
        return a(wVar, 8);
    }

    /* access modifiers changed from: 0000 */
    public void c(w wVar, c cVar) {
        a aVar = (a) this.f1283a.get(wVar);
        if (aVar == null) {
            aVar = a.a();
            this.f1283a.put(wVar, aVar);
        }
        aVar.c = cVar;
        aVar.f1285a |= 8;
    }

    /* access modifiers changed from: 0000 */
    public boolean d(w wVar) {
        a aVar = (a) this.f1283a.get(wVar);
        return (aVar == null || (aVar.f1285a & 4) == 0) ? false : true;
    }

    /* access modifiers changed from: 0000 */
    public void e(w wVar) {
        a aVar = (a) this.f1283a.get(wVar);
        if (aVar == null) {
            aVar = a.a();
            this.f1283a.put(wVar, aVar);
        }
        aVar.f1285a |= 1;
    }

    /* access modifiers changed from: 0000 */
    public void f(w wVar) {
        a aVar = (a) this.f1283a.get(wVar);
        if (aVar != null) {
            aVar.f1285a &= -2;
        }
    }

    /* access modifiers changed from: 0000 */
    public void g(w wVar) {
        int b2 = this.f1284b.b() - 1;
        while (true) {
            if (b2 < 0) {
                break;
            } else if (wVar == this.f1284b.c(b2)) {
                this.f1284b.a(b2);
                break;
            } else {
                b2--;
            }
        }
        a aVar = (a) this.f1283a.remove(wVar);
        if (aVar != null) {
            a.a(aVar);
        }
    }

    public void h(w wVar) {
        f(wVar);
    }
}
