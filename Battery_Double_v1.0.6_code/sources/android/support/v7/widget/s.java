package android.support.v7.widget;

import android.content.Context;
import android.os.Build.VERSION;
import android.support.v4.widget.l;
import android.support.v7.a.a.j;
import android.util.AttributeSet;
import android.view.View;
import android.widget.PopupWindow;

class s extends PopupWindow {

    /* renamed from: a reason: collision with root package name */
    private static final boolean f1332a = (VERSION.SDK_INT < 21);

    /* renamed from: b reason: collision with root package name */
    private boolean f1333b;

    public s(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        a(context, attributeSet, i, i2);
    }

    private void a(Context context, AttributeSet attributeSet, int i, int i2) {
        bo a2 = bo.a(context, attributeSet, j.PopupWindow, i, i2);
        if (a2.g(j.PopupWindow_overlapAnchor)) {
            a(a2.a(j.PopupWindow_overlapAnchor, false));
        }
        setBackgroundDrawable(a2.a(j.PopupWindow_android_popupBackground));
        a2.a();
    }

    private void a(boolean z) {
        if (f1332a) {
            this.f1333b = z;
        } else {
            l.a((PopupWindow) this, z);
        }
    }

    public void showAsDropDown(View view, int i, int i2) {
        if (f1332a && this.f1333b) {
            i2 -= view.getHeight();
        }
        super.showAsDropDown(view, i, i2);
    }

    public void showAsDropDown(View view, int i, int i2, int i3) {
        if (f1332a && this.f1333b) {
            i2 -= view.getHeight();
        }
        super.showAsDropDown(view, i, i2, i3);
    }

    public void update(View view, int i, int i2, int i3, int i4) {
        super.update(view, i, (!f1332a || !this.f1333b) ? i2 : i2 - view.getHeight(), i3, i4);
    }
}
