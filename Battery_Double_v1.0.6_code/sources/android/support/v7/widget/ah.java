package android.support.v7.widget;

import android.content.Context;
import android.content.res.ColorStateList;

interface ah {
    float a(ag agVar);

    void a();

    void a(ag agVar, float f);

    void a(ag agVar, Context context, ColorStateList colorStateList, float f, float f2, float f3);

    void a(ag agVar, ColorStateList colorStateList);

    float b(ag agVar);

    void b(ag agVar, float f);

    float c(ag agVar);

    void c(ag agVar, float f);

    float d(ag agVar);

    float e(ag agVar);

    void g(ag agVar);

    void h(ag agVar);

    ColorStateList i(ag agVar);
}
