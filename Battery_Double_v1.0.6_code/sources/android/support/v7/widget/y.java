package android.support.v7.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources.Theme;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.graphics.PorterDuff.Mode;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.support.v4.i.s;
import android.support.v4.i.t;
import android.support.v7.a.a.C0026a;
import android.support.v7.a.a.g;
import android.support.v7.a.a.j;
import android.support.v7.view.d;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.ThemedSpinnerAdapter;

public class y extends Spinner implements s {

    /* renamed from: a reason: collision with root package name */
    private static final int[] f1342a = {16843505};

    /* renamed from: b reason: collision with root package name */
    private final h f1343b;
    private final Context c;
    private aq d;
    private SpinnerAdapter e;
    private final boolean f;
    /* access modifiers changed from: private */
    public b g;
    /* access modifiers changed from: private */
    public int h;
    /* access modifiers changed from: private */
    public final Rect i;

    private static class a implements ListAdapter, SpinnerAdapter {

        /* renamed from: a reason: collision with root package name */
        private SpinnerAdapter f1346a;

        /* renamed from: b reason: collision with root package name */
        private ListAdapter f1347b;

        public a(SpinnerAdapter spinnerAdapter, Theme theme) {
            this.f1346a = spinnerAdapter;
            if (spinnerAdapter instanceof ListAdapter) {
                this.f1347b = (ListAdapter) spinnerAdapter;
            }
            if (theme == null) {
                return;
            }
            if (VERSION.SDK_INT >= 23 && (spinnerAdapter instanceof ThemedSpinnerAdapter)) {
                ThemedSpinnerAdapter themedSpinnerAdapter = (ThemedSpinnerAdapter) spinnerAdapter;
                if (themedSpinnerAdapter.getDropDownViewTheme() != theme) {
                    themedSpinnerAdapter.setDropDownViewTheme(theme);
                }
            } else if (spinnerAdapter instanceof bk) {
                bk bkVar = (bk) spinnerAdapter;
                if (bkVar.a() == null) {
                    bkVar.a(theme);
                }
            }
        }

        public boolean areAllItemsEnabled() {
            ListAdapter listAdapter = this.f1347b;
            if (listAdapter != null) {
                return listAdapter.areAllItemsEnabled();
            }
            return true;
        }

        public int getCount() {
            if (this.f1346a == null) {
                return 0;
            }
            return this.f1346a.getCount();
        }

        public View getDropDownView(int i, View view, ViewGroup viewGroup) {
            if (this.f1346a == null) {
                return null;
            }
            return this.f1346a.getDropDownView(i, view, viewGroup);
        }

        public Object getItem(int i) {
            if (this.f1346a == null) {
                return null;
            }
            return this.f1346a.getItem(i);
        }

        public long getItemId(int i) {
            if (this.f1346a == null) {
                return -1;
            }
            return this.f1346a.getItemId(i);
        }

        public int getItemViewType(int i) {
            return 0;
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            return getDropDownView(i, view, viewGroup);
        }

        public int getViewTypeCount() {
            return 1;
        }

        public boolean hasStableIds() {
            return this.f1346a != null && this.f1346a.hasStableIds();
        }

        public boolean isEmpty() {
            return getCount() == 0;
        }

        public boolean isEnabled(int i) {
            ListAdapter listAdapter = this.f1347b;
            if (listAdapter != null) {
                return listAdapter.isEnabled(i);
            }
            return true;
        }

        public void registerDataSetObserver(DataSetObserver dataSetObserver) {
            if (this.f1346a != null) {
                this.f1346a.registerDataSetObserver(dataSetObserver);
            }
        }

        public void unregisterDataSetObserver(DataSetObserver dataSetObserver) {
            if (this.f1346a != null) {
                this.f1346a.unregisterDataSetObserver(dataSetObserver);
            }
        }
    }

    private class b extends au {

        /* renamed from: a reason: collision with root package name */
        ListAdapter f1348a;
        private CharSequence h;
        private final Rect i = new Rect();

        public b(Context context, AttributeSet attributeSet, int i2) {
            super(context, attributeSet, i2);
            b((View) y.this);
            a(true);
            a(0);
            a((OnItemClickListener) new OnItemClickListener(y.this) {
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                    y.this.setSelection(i);
                    if (y.this.getOnItemClickListener() != null) {
                        y.this.performItemClick(view, i, b.this.f1348a.getItemId(i));
                    }
                    b.this.e();
                }
            });
        }

        public CharSequence a() {
            return this.h;
        }

        public void a(ListAdapter listAdapter) {
            super.a(listAdapter);
            this.f1348a = listAdapter;
        }

        public void a(CharSequence charSequence) {
            this.h = charSequence;
        }

        /* access modifiers changed from: 0000 */
        public boolean a(View view) {
            return t.y(view) && view.getGlobalVisibleRect(this.i);
        }

        /* access modifiers changed from: 0000 */
        public void b() {
            int i2;
            Drawable h2 = h();
            if (h2 != null) {
                h2.getPadding(y.this.i);
                i2 = bw.a(y.this) ? y.this.i.right : -y.this.i.left;
            } else {
                Rect b2 = y.this.i;
                y.this.i.right = 0;
                b2.left = 0;
                i2 = 0;
            }
            int paddingLeft = y.this.getPaddingLeft();
            int paddingRight = y.this.getPaddingRight();
            int width = y.this.getWidth();
            if (y.this.h == -2) {
                int a2 = y.this.a((SpinnerAdapter) this.f1348a, h());
                int i3 = (y.this.getContext().getResources().getDisplayMetrics().widthPixels - y.this.i.left) - y.this.i.right;
                if (a2 <= i3) {
                    i3 = a2;
                }
                g(Math.max(i3, (width - paddingLeft) - paddingRight));
            } else if (y.this.h == -1) {
                g((width - paddingLeft) - paddingRight);
            } else {
                g(y.this.h);
            }
            c(bw.a(y.this) ? ((width - paddingRight) - l()) + i2 : i2 + paddingLeft);
        }

        public void d() {
            boolean f = f();
            b();
            h(2);
            super.d();
            g().setChoiceMode(1);
            i(y.this.getSelectedItemPosition());
            if (!f) {
                ViewTreeObserver viewTreeObserver = y.this.getViewTreeObserver();
                if (viewTreeObserver != null) {
                    final AnonymousClass2 r1 = new OnGlobalLayoutListener() {
                        public void onGlobalLayout() {
                            if (!b.this.a((View) y.this)) {
                                b.this.e();
                                return;
                            }
                            b.this.b();
                            b.super.d();
                        }
                    };
                    viewTreeObserver.addOnGlobalLayoutListener(r1);
                    a((OnDismissListener) new OnDismissListener() {
                        public void onDismiss() {
                            ViewTreeObserver viewTreeObserver = y.this.getViewTreeObserver();
                            if (viewTreeObserver != null) {
                                viewTreeObserver.removeGlobalOnLayoutListener(r1);
                            }
                        }
                    });
                }
            }
        }
    }

    public y(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, C0026a.spinnerStyle);
    }

    public y(Context context, AttributeSet attributeSet, int i2) {
        this(context, attributeSet, i2, -1);
    }

    public y(Context context, AttributeSet attributeSet, int i2, int i3) {
        this(context, attributeSet, i2, i3, null);
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0045  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0083  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x009c  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00d2  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00db  */
    public y(Context context, AttributeSet attributeSet, int i2, int i3, Theme theme) {
        CharSequence[] f2;
        TypedArray typedArray;
        super(context, attributeSet, i2);
        this.i = new Rect();
        bo a2 = bo.a(context, attributeSet, j.Spinner, i2, 0);
        this.f1343b = new h(this);
        if (theme != null) {
            this.c = new d(context, theme);
        } else {
            int g2 = a2.g(j.Spinner_popupTheme, 0);
            if (g2 != 0) {
                this.c = new d(context, g2);
            } else {
                this.c = VERSION.SDK_INT < 23 ? context : null;
            }
        }
        if (this.c != null) {
            if (i3 == -1) {
                try {
                    typedArray = context.obtainStyledAttributes(attributeSet, f1342a, i2, 0);
                    try {
                        if (typedArray.hasValue(0)) {
                            i3 = typedArray.getInt(0, 0);
                        }
                        if (typedArray != null) {
                            typedArray.recycle();
                        }
                    } catch (Exception e2) {
                        e = e2;
                        try {
                            Log.i("AppCompatSpinner", "Could not read android:spinnerMode", e);
                            if (typedArray != null) {
                                typedArray.recycle();
                            }
                            if (i3 == 1) {
                            }
                            f2 = a2.f(j.Spinner_android_entries);
                            if (f2 != null) {
                            }
                            a2.a();
                            this.f = true;
                            if (this.e != null) {
                            }
                            this.f1343b.a(attributeSet, i2);
                        } catch (Throwable th) {
                            th = th;
                            if (typedArray != null) {
                            }
                            throw th;
                        }
                    }
                } catch (Exception e3) {
                    e = e3;
                    typedArray = null;
                    Log.i("AppCompatSpinner", "Could not read android:spinnerMode", e);
                    if (typedArray != null) {
                    }
                    if (i3 == 1) {
                    }
                    f2 = a2.f(j.Spinner_android_entries);
                    if (f2 != null) {
                    }
                    a2.a();
                    this.f = true;
                    if (this.e != null) {
                    }
                    this.f1343b.a(attributeSet, i2);
                } catch (Throwable th2) {
                    th = th2;
                    typedArray = null;
                    if (typedArray != null) {
                        typedArray.recycle();
                    }
                    throw th;
                }
            }
            if (i3 == 1) {
                final b bVar = new b(this.c, attributeSet, i2);
                bo a3 = bo.a(this.c, attributeSet, j.Spinner, i2, 0);
                this.h = a3.f(j.Spinner_android_dropDownWidth, -2);
                bVar.a(a3.a(j.Spinner_android_popupBackground));
                bVar.a((CharSequence) a2.d(j.Spinner_android_prompt));
                a3.a();
                this.g = bVar;
                this.d = new aq(this) {
                    public android.support.v7.view.menu.s a() {
                        return bVar;
                    }

                    public boolean b() {
                        if (!y.this.g.f()) {
                            y.this.g.d();
                        }
                        return true;
                    }
                };
            }
        }
        f2 = a2.f(j.Spinner_android_entries);
        if (f2 != null) {
            ArrayAdapter arrayAdapter = new ArrayAdapter(context, 17367048, f2);
            arrayAdapter.setDropDownViewResource(g.support_simple_spinner_dropdown_item);
            setAdapter((SpinnerAdapter) arrayAdapter);
        }
        a2.a();
        this.f = true;
        if (this.e != null) {
            setAdapter(this.e);
            this.e = null;
        }
        this.f1343b.a(attributeSet, i2);
    }

    /* access modifiers changed from: 0000 */
    public int a(SpinnerAdapter spinnerAdapter, Drawable drawable) {
        View view;
        if (spinnerAdapter == null) {
            return 0;
        }
        int makeMeasureSpec = MeasureSpec.makeMeasureSpec(getMeasuredWidth(), 0);
        int makeMeasureSpec2 = MeasureSpec.makeMeasureSpec(getMeasuredHeight(), 0);
        int max = Math.max(0, getSelectedItemPosition());
        int min = Math.min(spinnerAdapter.getCount(), max + 15);
        int max2 = Math.max(0, max - (15 - (min - max)));
        View view2 = null;
        int i2 = 0;
        int i3 = 0;
        while (max2 < min) {
            int itemViewType = spinnerAdapter.getItemViewType(max2);
            if (itemViewType != i3) {
                view = null;
            } else {
                itemViewType = i3;
                view = view2;
            }
            view2 = spinnerAdapter.getView(max2, view, this);
            if (view2.getLayoutParams() == null) {
                view2.setLayoutParams(new LayoutParams(-2, -2));
            }
            view2.measure(makeMeasureSpec, makeMeasureSpec2);
            i2 = Math.max(i2, view2.getMeasuredWidth());
            max2++;
            i3 = itemViewType;
        }
        if (drawable == null) {
            return i2;
        }
        drawable.getPadding(this.i);
        return this.i.left + this.i.right + i2;
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        if (this.f1343b != null) {
            this.f1343b.c();
        }
    }

    public int getDropDownHorizontalOffset() {
        if (this.g != null) {
            return this.g.j();
        }
        if (VERSION.SDK_INT >= 16) {
            return super.getDropDownHorizontalOffset();
        }
        return 0;
    }

    public int getDropDownVerticalOffset() {
        if (this.g != null) {
            return this.g.k();
        }
        if (VERSION.SDK_INT >= 16) {
            return super.getDropDownVerticalOffset();
        }
        return 0;
    }

    public int getDropDownWidth() {
        if (this.g != null) {
            return this.h;
        }
        if (VERSION.SDK_INT >= 16) {
            return super.getDropDownWidth();
        }
        return 0;
    }

    public Drawable getPopupBackground() {
        if (this.g != null) {
            return this.g.h();
        }
        if (VERSION.SDK_INT >= 16) {
            return super.getPopupBackground();
        }
        return null;
    }

    public Context getPopupContext() {
        if (this.g != null) {
            return this.c;
        }
        if (VERSION.SDK_INT >= 23) {
            return super.getPopupContext();
        }
        return null;
    }

    public CharSequence getPrompt() {
        return this.g != null ? this.g.a() : super.getPrompt();
    }

    public ColorStateList getSupportBackgroundTintList() {
        if (this.f1343b != null) {
            return this.f1343b.a();
        }
        return null;
    }

    public Mode getSupportBackgroundTintMode() {
        if (this.f1343b != null) {
            return this.f1343b.b();
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.g != null && this.g.f()) {
            this.g.e();
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
        if (this.g != null && MeasureSpec.getMode(i2) == Integer.MIN_VALUE) {
            setMeasuredDimension(Math.min(Math.max(getMeasuredWidth(), a(getAdapter(), getBackground())), MeasureSpec.getSize(i2)), getMeasuredHeight());
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (this.d == null || !this.d.onTouch(this, motionEvent)) {
            return super.onTouchEvent(motionEvent);
        }
        return true;
    }

    public boolean performClick() {
        if (this.g == null) {
            return super.performClick();
        }
        if (!this.g.f()) {
            this.g.d();
        }
        return true;
    }

    public void setAdapter(SpinnerAdapter spinnerAdapter) {
        if (!this.f) {
            this.e = spinnerAdapter;
            return;
        }
        super.setAdapter(spinnerAdapter);
        if (this.g != null) {
            this.g.a((ListAdapter) new a(spinnerAdapter, (this.c == null ? getContext() : this.c).getTheme()));
        }
    }

    public void setBackgroundDrawable(Drawable drawable) {
        super.setBackgroundDrawable(drawable);
        if (this.f1343b != null) {
            this.f1343b.a(drawable);
        }
    }

    public void setBackgroundResource(int i2) {
        super.setBackgroundResource(i2);
        if (this.f1343b != null) {
            this.f1343b.a(i2);
        }
    }

    public void setDropDownHorizontalOffset(int i2) {
        if (this.g != null) {
            this.g.c(i2);
        } else if (VERSION.SDK_INT >= 16) {
            super.setDropDownHorizontalOffset(i2);
        }
    }

    public void setDropDownVerticalOffset(int i2) {
        if (this.g != null) {
            this.g.d(i2);
        } else if (VERSION.SDK_INT >= 16) {
            super.setDropDownVerticalOffset(i2);
        }
    }

    public void setDropDownWidth(int i2) {
        if (this.g != null) {
            this.h = i2;
        } else if (VERSION.SDK_INT >= 16) {
            super.setDropDownWidth(i2);
        }
    }

    public void setPopupBackgroundDrawable(Drawable drawable) {
        if (this.g != null) {
            this.g.a(drawable);
        } else if (VERSION.SDK_INT >= 16) {
            super.setPopupBackgroundDrawable(drawable);
        }
    }

    public void setPopupBackgroundResource(int i2) {
        setPopupBackgroundDrawable(android.support.v7.c.a.b.b(getPopupContext(), i2));
    }

    public void setPrompt(CharSequence charSequence) {
        if (this.g != null) {
            this.g.a(charSequence);
        } else {
            super.setPrompt(charSequence);
        }
    }

    public void setSupportBackgroundTintList(ColorStateList colorStateList) {
        if (this.f1343b != null) {
            this.f1343b.a(colorStateList);
        }
    }

    public void setSupportBackgroundTintMode(Mode mode) {
        if (this.f1343b != null) {
            this.f1343b.a(mode);
        }
    }
}
