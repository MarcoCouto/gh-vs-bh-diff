package android.support.v7.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.a.a.C0026a;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RatingBar;

public class v extends RatingBar {

    /* renamed from: a reason: collision with root package name */
    private final t f1338a;

    public v(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, C0026a.ratingBarStyle);
    }

    public v(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f1338a = new t(this);
        this.f1338a.a(attributeSet, i);
    }

    /* access modifiers changed from: protected */
    public synchronized void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        Bitmap a2 = this.f1338a.a();
        if (a2 != null) {
            setMeasuredDimension(View.resolveSizeAndState(a2.getWidth() * getNumStars(), i, 0), getMeasuredHeight());
        }
    }
}
