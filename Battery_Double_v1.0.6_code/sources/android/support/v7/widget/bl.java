package android.support.v7.widget;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.os.Build.VERSION;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

public class bl extends ContextWrapper {

    /* renamed from: a reason: collision with root package name */
    private static final Object f1258a = new Object();

    /* renamed from: b reason: collision with root package name */
    private static ArrayList<WeakReference<bl>> f1259b;
    private final Resources c;
    private final Theme d;

    private bl(Context context) {
        super(context);
        if (bt.a()) {
            this.c = new bt(this, context.getResources());
            this.d = this.c.newTheme();
            this.d.setTo(context.getTheme());
            return;
        }
        this.c = new bn(this, context.getResources());
        this.d = null;
    }

    public static Context a(Context context) {
        if (!b(context)) {
            return context;
        }
        synchronized (f1258a) {
            if (f1259b == null) {
                f1259b = new ArrayList<>();
            } else {
                for (int size = f1259b.size() - 1; size >= 0; size--) {
                    WeakReference weakReference = (WeakReference) f1259b.get(size);
                    if (weakReference == null || weakReference.get() == null) {
                        f1259b.remove(size);
                    }
                }
                for (int size2 = f1259b.size() - 1; size2 >= 0; size2--) {
                    WeakReference weakReference2 = (WeakReference) f1259b.get(size2);
                    bl blVar = weakReference2 != null ? (bl) weakReference2.get() : null;
                    if (blVar != null && blVar.getBaseContext() == context) {
                        return blVar;
                    }
                }
            }
            bl blVar2 = new bl(context);
            f1259b.add(new WeakReference(blVar2));
            return blVar2;
        }
    }

    private static boolean b(Context context) {
        if ((context instanceof bl) || (context.getResources() instanceof bn) || (context.getResources() instanceof bt)) {
            return false;
        }
        return VERSION.SDK_INT < 21 || bt.a();
    }

    public AssetManager getAssets() {
        return this.c.getAssets();
    }

    public Resources getResources() {
        return this.c;
    }

    public Theme getTheme() {
        return this.d == null ? super.getTheme() : this.d;
    }

    public void setTheme(int i) {
        if (this.d == null) {
            super.setTheme(i);
        } else {
            this.d.applyStyle(i, true);
        }
    }
}
