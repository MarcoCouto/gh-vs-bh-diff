package android.support.v7.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.support.v4.i.s;
import android.support.v7.a.a.C0026a;
import android.util.AttributeSet;
import android.widget.EditText;
import android.widget.TextView;

public class n extends EditText implements s {

    /* renamed from: a reason: collision with root package name */
    private final h f1322a;

    /* renamed from: b reason: collision with root package name */
    private final z f1323b;

    public n(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, C0026a.editTextStyle);
    }

    public n(Context context, AttributeSet attributeSet, int i) {
        super(bl.a(context), attributeSet, i);
        this.f1322a = new h(this);
        this.f1322a.a(attributeSet, i);
        this.f1323b = z.a((TextView) this);
        this.f1323b.a(attributeSet, i);
        this.f1323b.a();
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        if (this.f1322a != null) {
            this.f1322a.c();
        }
        if (this.f1323b != null) {
            this.f1323b.a();
        }
    }

    public ColorStateList getSupportBackgroundTintList() {
        if (this.f1322a != null) {
            return this.f1322a.a();
        }
        return null;
    }

    public Mode getSupportBackgroundTintMode() {
        if (this.f1322a != null) {
            return this.f1322a.b();
        }
        return null;
    }

    public void setBackgroundDrawable(Drawable drawable) {
        super.setBackgroundDrawable(drawable);
        if (this.f1322a != null) {
            this.f1322a.a(drawable);
        }
    }

    public void setBackgroundResource(int i) {
        super.setBackgroundResource(i);
        if (this.f1322a != null) {
            this.f1322a.a(i);
        }
    }

    public void setSupportBackgroundTintList(ColorStateList colorStateList) {
        if (this.f1322a != null) {
            this.f1322a.a(colorStateList);
        }
    }

    public void setSupportBackgroundTintMode(Mode mode) {
        if (this.f1322a != null) {
            this.f1322a.a(mode);
        }
    }

    public void setTextAppearance(Context context, int i) {
        super.setTextAppearance(context, i);
        if (this.f1323b != null) {
            this.f1323b.a(context, i);
        }
    }
}
