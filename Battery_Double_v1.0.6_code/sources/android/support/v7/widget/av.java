package android.support.v7.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup.LayoutParams;
import android.widget.AbsListView;
import android.widget.ListAdapter;
import android.widget.ListView;
import java.lang.reflect.Field;

public class av extends ListView {
    private static final int[] g = {0};

    /* renamed from: a reason: collision with root package name */
    final Rect f1228a = new Rect();

    /* renamed from: b reason: collision with root package name */
    int f1229b = 0;
    int c = 0;
    int d = 0;
    int e = 0;
    protected int f;
    private Field h;
    private a i;

    private static class a extends android.support.v7.d.a.a {

        /* renamed from: a reason: collision with root package name */
        private boolean f1230a = true;

        public a(Drawable drawable) {
            super(drawable);
        }

        /* access modifiers changed from: 0000 */
        public void a(boolean z) {
            this.f1230a = z;
        }

        public void draw(Canvas canvas) {
            if (this.f1230a) {
                super.draw(canvas);
            }
        }

        public void setHotspot(float f, float f2) {
            if (this.f1230a) {
                super.setHotspot(f, f2);
            }
        }

        public void setHotspotBounds(int i, int i2, int i3, int i4) {
            if (this.f1230a) {
                super.setHotspotBounds(i, i2, i3, i4);
            }
        }

        public boolean setState(int[] iArr) {
            if (this.f1230a) {
                return super.setState(iArr);
            }
            return false;
        }

        public boolean setVisible(boolean z, boolean z2) {
            if (this.f1230a) {
                return super.setVisible(z, z2);
            }
            return false;
        }
    }

    public av(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        try {
            this.h = AbsListView.class.getDeclaredField("mIsChildViewEnabled");
            this.h.setAccessible(true);
        } catch (NoSuchFieldException e2) {
            e2.printStackTrace();
        }
    }

    public int a(int i2, int i3, int i4, int i5, int i6) {
        View view;
        int listPaddingTop = getListPaddingTop();
        int listPaddingBottom = getListPaddingBottom();
        getListPaddingLeft();
        getListPaddingRight();
        int dividerHeight = getDividerHeight();
        Drawable divider = getDivider();
        ListAdapter adapter = getAdapter();
        if (adapter == null) {
            return listPaddingTop + listPaddingBottom;
        }
        int i7 = listPaddingBottom + listPaddingTop;
        if (dividerHeight <= 0 || divider == null) {
            dividerHeight = 0;
        }
        int i8 = 0;
        View view2 = null;
        int i9 = 0;
        int count = adapter.getCount();
        int i10 = 0;
        while (i10 < count) {
            int itemViewType = adapter.getItemViewType(i10);
            if (itemViewType != i9) {
                int i11 = itemViewType;
                view = null;
                i9 = i11;
            } else {
                view = view2;
            }
            view2 = adapter.getView(i10, view, this);
            LayoutParams layoutParams = view2.getLayoutParams();
            if (layoutParams == null) {
                layoutParams = generateDefaultLayoutParams();
                view2.setLayoutParams(layoutParams);
            }
            view2.measure(i2, layoutParams.height > 0 ? MeasureSpec.makeMeasureSpec(layoutParams.height, 1073741824) : MeasureSpec.makeMeasureSpec(0, 0));
            view2.forceLayout();
            int measuredHeight = (i10 > 0 ? i7 + dividerHeight : i7) + view2.getMeasuredHeight();
            if (measuredHeight >= i5) {
                return (i6 < 0 || i10 <= i6 || i8 <= 0 || measuredHeight == i5) ? i5 : i8;
            }
            if (i6 >= 0 && i10 >= i6) {
                i8 = measuredHeight;
            }
            i10++;
            i7 = measuredHeight;
        }
        return i7;
    }

    /* access modifiers changed from: protected */
    public void a(int i2, View view) {
        boolean z = true;
        Drawable selector = getSelector();
        boolean z2 = (selector == null || i2 == -1) ? false : true;
        if (z2) {
            selector.setVisible(false, false);
        }
        b(i2, view);
        if (z2) {
            Rect rect = this.f1228a;
            float exactCenterX = rect.exactCenterX();
            float exactCenterY = rect.exactCenterY();
            if (getVisibility() != 0) {
                z = false;
            }
            selector.setVisible(z, false);
            android.support.v4.c.a.a.a(selector, exactCenterX, exactCenterY);
        }
    }

    /* access modifiers changed from: protected */
    public void a(int i2, View view, float f2, float f3) {
        a(i2, view);
        Drawable selector = getSelector();
        if (selector != null && i2 != -1) {
            android.support.v4.c.a.a.a(selector, f2, f3);
        }
    }

    /* access modifiers changed from: protected */
    public void a(Canvas canvas) {
        if (!this.f1228a.isEmpty()) {
            Drawable selector = getSelector();
            if (selector != null) {
                selector.setBounds(this.f1228a);
                selector.draw(canvas);
            }
        }
    }

    /* access modifiers changed from: protected */
    public boolean a() {
        return false;
    }

    /* access modifiers changed from: protected */
    public void b() {
        Drawable selector = getSelector();
        if (selector != null && c()) {
            selector.setState(getDrawableState());
        }
    }

    /* access modifiers changed from: protected */
    public void b(int i2, View view) {
        Rect rect = this.f1228a;
        rect.set(view.getLeft(), view.getTop(), view.getRight(), view.getBottom());
        rect.left -= this.f1229b;
        rect.top -= this.c;
        rect.right += this.d;
        rect.bottom += this.e;
        try {
            boolean z = this.h.getBoolean(this);
            if (view.isEnabled() != z) {
                this.h.set(this, Boolean.valueOf(!z));
                if (i2 != -1) {
                    refreshDrawableState();
                }
            }
        } catch (IllegalAccessException e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public boolean c() {
        return a() && isPressed();
    }

    /* access modifiers changed from: protected */
    public void dispatchDraw(Canvas canvas) {
        a(canvas);
        super.dispatchDraw(canvas);
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        setSelectorEnabled(true);
        b();
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
                this.f = pointToPosition((int) motionEvent.getX(), (int) motionEvent.getY());
                break;
        }
        return super.onTouchEvent(motionEvent);
    }

    public void setSelector(Drawable drawable) {
        this.i = drawable != null ? new a(drawable) : null;
        super.setSelector(this.i);
        Rect rect = new Rect();
        if (drawable != null) {
            drawable.getPadding(rect);
        }
        this.f1229b = rect.left;
        this.c = rect.top;
        this.d = rect.right;
        this.e = rect.bottom;
    }

    /* access modifiers changed from: protected */
    public void setSelectorEnabled(boolean z) {
        if (this.i != null) {
            this.i.a(z);
        }
    }
}
