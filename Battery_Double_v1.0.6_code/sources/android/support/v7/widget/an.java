package android.support.v7.widget;

import android.content.Context;
import android.os.Build.VERSION;
import android.support.v4.i.y;
import android.support.v4.widget.j;
import android.support.v7.a.a.C0026a;
import android.view.MotionEvent;
import android.view.View;

class an extends av {
    private boolean g;
    private boolean h;
    private boolean i;
    private y j;
    private j k;

    public an(Context context, boolean z) {
        super(context, null, C0026a.dropDownListViewStyle);
        this.h = z;
        setCacheColorHint(0);
    }

    private void a(View view, int i2) {
        performItemClick(view, i2, getItemIdAtPosition(i2));
    }

    private void a(View view, int i2, float f, float f2) {
        this.i = true;
        if (VERSION.SDK_INT >= 21) {
            drawableHotspotChanged(f, f2);
        }
        if (!isPressed()) {
            setPressed(true);
        }
        layoutChildren();
        if (this.f != -1) {
            View childAt = getChildAt(this.f - getFirstVisiblePosition());
            if (!(childAt == null || childAt == view || !childAt.isPressed())) {
                childAt.setPressed(false);
            }
        }
        this.f = i2;
        float left = f - ((float) view.getLeft());
        float top = f2 - ((float) view.getTop());
        if (VERSION.SDK_INT >= 21) {
            view.drawableHotspotChanged(left, top);
        }
        if (!view.isPressed()) {
            view.setPressed(true);
        }
        a(i2, view, f, f2);
        setSelectorEnabled(false);
        refreshDrawableState();
    }

    private void d() {
        this.i = false;
        setPressed(false);
        drawableStateChanged();
        View childAt = getChildAt(this.f - getFirstVisiblePosition());
        if (childAt != null) {
            childAt.setPressed(false);
        }
        if (this.j != null) {
            this.j.b();
            this.j = null;
        }
    }

    /* access modifiers changed from: protected */
    public boolean a() {
        return this.i || super.a();
    }

    public boolean a(MotionEvent motionEvent, int i2) {
        boolean z;
        boolean z2;
        boolean z3;
        int actionMasked = motionEvent.getActionMasked();
        switch (actionMasked) {
            case 1:
                z = false;
                break;
            case 2:
                z = true;
                break;
            case 3:
                z3 = false;
                z2 = false;
                break;
            default:
                z3 = false;
                z2 = true;
                break;
        }
        int findPointerIndex = motionEvent.findPointerIndex(i2);
        if (findPointerIndex < 0) {
            z3 = false;
            z2 = false;
        } else {
            int x = (int) motionEvent.getX(findPointerIndex);
            int y = (int) motionEvent.getY(findPointerIndex);
            int pointToPosition = pointToPosition(x, y);
            if (pointToPosition == -1) {
                z2 = z;
                z3 = true;
            } else {
                View childAt = getChildAt(pointToPosition - getFirstVisiblePosition());
                a(childAt, pointToPosition, (float) x, (float) y);
                if (actionMasked == 1) {
                    a(childAt, pointToPosition);
                }
                z3 = false;
                z2 = true;
            }
        }
        if (!z2 || z3) {
            d();
        }
        if (z2) {
            if (this.k == null) {
                this.k = new j(this);
            }
            this.k.a(true);
            this.k.onTouch(this, motionEvent);
        } else if (this.k != null) {
            this.k.a(false);
        }
        return z2;
    }

    public boolean hasFocus() {
        return this.h || super.hasFocus();
    }

    public boolean hasWindowFocus() {
        return this.h || super.hasWindowFocus();
    }

    public boolean isFocused() {
        return this.h || super.isFocused();
    }

    public boolean isInTouchMode() {
        return (this.h && this.g) || super.isInTouchMode();
    }

    /* access modifiers changed from: 0000 */
    public void setListSelectionHidden(boolean z) {
        this.g = z;
    }
}
