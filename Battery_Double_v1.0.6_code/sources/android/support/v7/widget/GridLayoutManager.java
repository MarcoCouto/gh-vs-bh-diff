package android.support.v7.widget;

import android.content.Context;
import android.graphics.Rect;
import android.support.v4.i.a.b.C0017b;
import android.support.v7.widget.RecyclerView.i;
import android.support.v7.widget.RecyclerView.o;
import android.support.v7.widget.RecyclerView.t;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseIntArray;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import java.util.Arrays;

public class GridLayoutManager extends LinearLayoutManager {

    /* renamed from: a reason: collision with root package name */
    boolean f1050a = false;

    /* renamed from: b reason: collision with root package name */
    int f1051b = -1;
    int[] c;
    View[] d;
    final SparseIntArray e = new SparseIntArray();
    final SparseIntArray f = new SparseIntArray();
    c g = new a();
    final Rect h = new Rect();

    public static final class a extends c {
        public int a(int i) {
            return 1;
        }

        public int a(int i, int i2) {
            return i % i2;
        }
    }

    public static class b extends i {

        /* renamed from: a reason: collision with root package name */
        int f1052a = -1;

        /* renamed from: b reason: collision with root package name */
        int f1053b = 0;

        public b(int i, int i2) {
            super(i, i2);
        }

        public b(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }

        public b(LayoutParams layoutParams) {
            super(layoutParams);
        }

        public b(MarginLayoutParams marginLayoutParams) {
            super(marginLayoutParams);
        }

        public int a() {
            return this.f1052a;
        }

        public int b() {
            return this.f1053b;
        }
    }

    public static abstract class c {

        /* renamed from: a reason: collision with root package name */
        final SparseIntArray f1054a = new SparseIntArray();

        /* renamed from: b reason: collision with root package name */
        private boolean f1055b = false;

        public abstract int a(int i);

        /* JADX WARNING: Removed duplicated region for block: B:11:0x002a  */
        /* JADX WARNING: Removed duplicated region for block: B:19:0x003f  */
        /* JADX WARNING: Removed duplicated region for block: B:26:? A[RETURN, SYNTHETIC] */
        public int a(int i, int i2) {
            int i3;
            int i4;
            int i5;
            int a2 = a(i);
            if (a2 == i2) {
                return 0;
            }
            if (this.f1055b && this.f1054a.size() > 0) {
                int b2 = b(i);
                if (b2 >= 0) {
                    i4 = this.f1054a.get(b2) + a(b2);
                    i3 = b2 + 1;
                    i5 = i3;
                    while (i5 < i) {
                        int a3 = a(i5);
                        int i6 = i4 + a3;
                        if (i6 == i2) {
                            a3 = 0;
                        } else if (i6 <= i2) {
                            a3 = i6;
                        }
                        i5++;
                        i4 = a3;
                    }
                    if (i4 + a2 > i2) {
                        return i4;
                    }
                    return 0;
                }
            }
            i3 = 0;
            i4 = 0;
            i5 = i3;
            while (i5 < i) {
            }
            if (i4 + a2 > i2) {
            }
        }

        public void a() {
            this.f1054a.clear();
        }

        /* access modifiers changed from: 0000 */
        public int b(int i) {
            int i2 = 0;
            int size = this.f1054a.size() - 1;
            while (i2 <= size) {
                int i3 = (i2 + size) >>> 1;
                if (this.f1054a.keyAt(i3) < i) {
                    i2 = i3 + 1;
                } else {
                    size = i3 - 1;
                }
            }
            int i4 = i2 - 1;
            if (i4 < 0 || i4 >= this.f1054a.size()) {
                return -1;
            }
            return this.f1054a.keyAt(i4);
        }

        /* access modifiers changed from: 0000 */
        public int b(int i, int i2) {
            if (!this.f1055b) {
                return a(i, i2);
            }
            int i3 = this.f1054a.get(i, -1);
            if (i3 != -1) {
                return i3;
            }
            int a2 = a(i, i2);
            this.f1054a.put(i, a2);
            return a2;
        }

        public int c(int i, int i2) {
            int a2 = a(i);
            int i3 = 0;
            int i4 = 0;
            int i5 = 0;
            while (i3 < i) {
                int a3 = a(i3);
                int i6 = i5 + a3;
                if (i6 == i2) {
                    i4++;
                    a3 = 0;
                } else if (i6 > i2) {
                    i4++;
                } else {
                    a3 = i6;
                }
                i3++;
                i5 = a3;
            }
            return i5 + a2 > i2 ? i4 + 1 : i4;
        }
    }

    public GridLayoutManager(Context context, int i) {
        super(context);
        a(i);
    }

    public GridLayoutManager(Context context, int i, int i2, boolean z) {
        super(context, i2, z);
        a(i);
    }

    public GridLayoutManager(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        a(a(context, attributeSet, i, i2).f1085b);
    }

    private void J() {
        this.e.clear();
        this.f.clear();
    }

    private void K() {
        int u = u();
        for (int i = 0; i < u; i++) {
            b bVar = (b) h(i).getLayoutParams();
            int f2 = bVar.f();
            this.e.put(f2, bVar.b());
            this.f.put(f2, bVar.a());
        }
    }

    private void L() {
        l(f() == 1 ? (x() - B()) - z() : (y() - C()) - A());
    }

    private void M() {
        if (this.d == null || this.d.length != this.f1051b) {
            this.d = new View[this.f1051b];
        }
    }

    private int a(o oVar, t tVar, int i) {
        if (!tVar.a()) {
            return this.g.c(i, this.f1051b);
        }
        int b2 = oVar.b(i);
        if (b2 != -1) {
            return this.g.c(b2, this.f1051b);
        }
        Log.w("GridLayoutManager", "Cannot find span size for pre layout position. " + i);
        return 0;
    }

    private void a(float f2, int i) {
        l(Math.max(Math.round(((float) this.f1051b) * f2), i));
    }

    private void a(o oVar, t tVar, int i, int i2, boolean z) {
        int i3;
        int i4;
        if (z) {
            i4 = 1;
            i3 = 0;
        } else {
            int i5 = i - 1;
            i = -1;
            i3 = i5;
            i4 = -1;
        }
        int i6 = 0;
        for (int i7 = i3; i7 != i; i7 += i4) {
            View view = this.d[i7];
            b bVar = (b) view.getLayoutParams();
            bVar.f1053b = c(oVar, tVar, d(view));
            bVar.f1052a = i6;
            i6 += bVar.f1053b;
        }
    }

    private void a(View view, int i, int i2, boolean z) {
        i iVar = (i) view.getLayoutParams();
        if (z ? a(view, i, i2, iVar) : b(view, i, i2, iVar)) {
            view.measure(i, i2);
        }
    }

    private void a(View view, int i, boolean z) {
        int a2;
        int i2;
        b bVar = (b) view.getLayoutParams();
        Rect rect = bVar.d;
        int i3 = rect.top + rect.bottom + bVar.topMargin + bVar.bottomMargin;
        int i4 = bVar.rightMargin + rect.right + rect.left + bVar.leftMargin;
        int a3 = a(bVar.f1052a, bVar.f1053b);
        if (this.i == 1) {
            a2 = a(a3, i, i4, bVar.width, false);
            i2 = a(this.j.f(), w(), i3, bVar.height, true);
        } else {
            int a4 = a(a3, i, i3, bVar.height, false);
            a2 = a(this.j.f(), v(), i4, bVar.width, true);
            i2 = a4;
        }
        a(view, a2, i2, z);
    }

    static int[] a(int[] iArr, int i, int i2) {
        int i3;
        int i4 = 0;
        if (!(iArr != null && iArr.length == i + 1 && iArr[iArr.length - 1] == i2)) {
            iArr = new int[(i + 1)];
        }
        iArr[0] = 0;
        int i5 = i2 / i;
        int i6 = i2 % i;
        int i7 = 0;
        for (int i8 = 1; i8 <= i; i8++) {
            i4 += i6;
            if (i4 <= 0 || i - i4 >= i6) {
                i3 = i5;
            } else {
                i3 = i5 + 1;
                i4 -= i;
            }
            i7 += i3;
            iArr[i8] = i7;
        }
        return iArr;
    }

    private int b(o oVar, t tVar, int i) {
        if (!tVar.a()) {
            return this.g.b(i, this.f1051b);
        }
        int i2 = this.f.get(i, -1);
        if (i2 != -1) {
            return i2;
        }
        int b2 = oVar.b(i);
        if (b2 != -1) {
            return this.g.b(b2, this.f1051b);
        }
        Log.w("GridLayoutManager", "Cannot find span size for pre layout position. It is not cached, not in the adapter. Pos:" + i);
        return 0;
    }

    private void b(o oVar, t tVar, a aVar, int i) {
        boolean z = true;
        if (i != 1) {
            z = false;
        }
        int b2 = b(oVar, tVar, aVar.f1058a);
        if (z) {
            while (b2 > 0 && aVar.f1058a > 0) {
                aVar.f1058a--;
                b2 = b(oVar, tVar, aVar.f1058a);
            }
            return;
        }
        int e2 = tVar.e() - 1;
        int i2 = aVar.f1058a;
        int i3 = b2;
        while (i2 < e2) {
            int b3 = b(oVar, tVar, i2 + 1);
            if (b3 <= i3) {
                break;
            }
            i2++;
            i3 = b3;
        }
        aVar.f1058a = i2;
    }

    private int c(o oVar, t tVar, int i) {
        if (!tVar.a()) {
            return this.g.a(i);
        }
        int i2 = this.e.get(i, -1);
        if (i2 != -1) {
            return i2;
        }
        int b2 = oVar.b(i);
        if (b2 != -1) {
            return this.g.a(b2);
        }
        Log.w("GridLayoutManager", "Cannot find span size for pre layout position. It is not cached, not in the adapter. Pos:" + i);
        return 1;
    }

    private void l(int i) {
        this.c = a(this.c, this.f1051b, i);
    }

    /* access modifiers changed from: 0000 */
    public int a(int i, int i2) {
        return (this.i != 1 || !g()) ? this.c[i + i2] - this.c[i] : this.c[this.f1051b - i] - this.c[(this.f1051b - i) - i2];
    }

    public int a(int i, o oVar, t tVar) {
        L();
        M();
        return super.a(i, oVar, tVar);
    }

    public int a(o oVar, t tVar) {
        if (this.i == 0) {
            return this.f1051b;
        }
        if (tVar.e() < 1) {
            return 0;
        }
        return a(oVar, tVar, tVar.e() - 1) + 1;
    }

    public i a() {
        return this.i == 0 ? new b(-2, -1) : new b(-1, -2);
    }

    public i a(Context context, AttributeSet attributeSet) {
        return new b(context, attributeSet);
    }

    public i a(LayoutParams layoutParams) {
        return layoutParams instanceof MarginLayoutParams ? new b((MarginLayoutParams) layoutParams) : new b(layoutParams);
    }

    /* access modifiers changed from: 0000 */
    public View a(o oVar, t tVar, int i, int i2, int i3) {
        View view;
        View view2 = null;
        h();
        int c2 = this.j.c();
        int d2 = this.j.d();
        int i4 = i2 > i ? 1 : -1;
        View view3 = null;
        while (i != i2) {
            View h2 = h(i);
            int d3 = d(h2);
            if (d3 >= 0 && d3 < i3) {
                if (b(oVar, tVar, d3) != 0) {
                    view = view2;
                    h2 = view3;
                } else if (((i) h2.getLayoutParams()).d()) {
                    if (view3 == null) {
                        view = view2;
                    }
                } else if (this.j.a(h2) < d2 && this.j.b(h2) >= c2) {
                    return h2;
                } else {
                    if (view2 == null) {
                        view = h2;
                        h2 = view3;
                    }
                }
                i += i4;
                view2 = view;
                view3 = h2;
            }
            view = view2;
            h2 = view3;
            i += i4;
            view2 = view;
            view3 = h2;
        }
        if (view2 == null) {
            view2 = view3;
        }
        return view2;
    }

    public View a(View view, int i, o oVar, t tVar) {
        int i2;
        int i3;
        int u;
        int i4;
        int min;
        View view2;
        int i5;
        int i6;
        View view3;
        View e2 = e(view);
        if (e2 == null) {
            return null;
        }
        b bVar = (b) e2.getLayoutParams();
        int i7 = bVar.f1052a;
        int i8 = bVar.f1052a + bVar.f1053b;
        if (super.a(view, i, oVar, tVar) == null) {
            return null;
        }
        if ((e(i) == 1) != this.k) {
            i2 = u() - 1;
            i3 = -1;
            u = -1;
        } else {
            i2 = 0;
            i3 = 1;
            u = u();
        }
        boolean z = this.i == 1 && g();
        View view4 = null;
        int i9 = -1;
        int i10 = 0;
        View view5 = null;
        int i11 = -1;
        int i12 = 0;
        int a2 = a(oVar, tVar, i2);
        int i13 = i2;
        while (i13 != u) {
            int a3 = a(oVar, tVar, i13);
            View h2 = h(i13);
            if (h2 == e2) {
                break;
            }
            if (h2.hasFocusable() && a3 != a2) {
                if (view4 != null) {
                    break;
                }
            } else {
                b bVar2 = (b) h2.getLayoutParams();
                int i14 = bVar2.f1052a;
                int i15 = bVar2.f1052a + bVar2.f1053b;
                if (h2.hasFocusable() && i14 == i7 && i15 == i8) {
                    return h2;
                }
                boolean z2 = false;
                if ((!h2.hasFocusable() || view4 != null) && (h2.hasFocusable() || view5 != null)) {
                    int min2 = Math.min(i15, i8) - Math.max(i14, i7);
                    if (h2.hasFocusable()) {
                        if (min2 > i10) {
                            z2 = true;
                        } else if (min2 == i10) {
                            if (z == (i14 > i9)) {
                                z2 = true;
                            }
                        }
                    } else if (view4 == null && a(h2, false, true)) {
                        if (min2 > i12) {
                            z2 = true;
                        } else if (min2 == i12) {
                            if (z == (i14 > i11)) {
                                z2 = true;
                            }
                        }
                    }
                } else {
                    z2 = true;
                }
                if (z2) {
                    if (h2.hasFocusable()) {
                        int i16 = bVar2.f1052a;
                        int i17 = i12;
                        i4 = i11;
                        view2 = view5;
                        i5 = Math.min(i15, i8) - Math.max(i14, i7);
                        min = i17;
                        int i18 = i16;
                        view3 = h2;
                        i6 = i18;
                    } else {
                        i4 = bVar2.f1052a;
                        min = Math.min(i15, i8) - Math.max(i14, i7);
                        view2 = h2;
                        i5 = i10;
                        i6 = i9;
                        view3 = view4;
                    }
                    i13 += i3;
                    view4 = view3;
                    i10 = i5;
                    i9 = i6;
                    view5 = view2;
                    i11 = i4;
                    i12 = min;
                }
            }
            min = i12;
            i6 = i9;
            i4 = i11;
            view2 = view5;
            i5 = i10;
            view3 = view4;
            i13 += i3;
            view4 = view3;
            i10 = i5;
            i9 = i6;
            view5 = view2;
            i11 = i4;
            i12 = min;
        }
        if (view4 == null) {
            view4 = view5;
        }
        return view4;
    }

    public void a(int i) {
        if (i != this.f1051b) {
            this.f1050a = true;
            if (i < 1) {
                throw new IllegalArgumentException("Span count should be at least 1. Provided " + i);
            }
            this.f1051b = i;
            this.g.a();
            n();
        }
    }

    public void a(Rect rect, int i, int i2) {
        int a2;
        int a3;
        if (this.c == null) {
            super.a(rect, i, i2);
        }
        int B = B() + z();
        int A = A() + C();
        if (this.i == 1) {
            a3 = a(i2, A + rect.height(), F());
            a2 = a(i, B + this.c[this.c.length - 1], E());
        } else {
            a2 = a(i, B + rect.width(), E());
            a3 = a(i2, A + this.c[this.c.length - 1], F());
        }
        g(a2, a3);
    }

    /* access modifiers changed from: 0000 */
    public void a(o oVar, t tVar, a aVar, int i) {
        super.a(oVar, tVar, aVar, i);
        L();
        if (tVar.e() > 0 && !tVar.a()) {
            b(oVar, tVar, aVar, i);
        }
        M();
    }

    /* access modifiers changed from: 0000 */
    public void a(o oVar, t tVar, c cVar, b bVar) {
        int i;
        int i2;
        int i3;
        int makeMeasureSpec;
        int a2;
        int i4 = this.j.i();
        boolean z = i4 != 1073741824;
        int i5 = u() > 0 ? this.c[this.f1051b] : 0;
        if (z) {
            L();
        }
        boolean z2 = cVar.e == 1;
        int i6 = 0;
        int i7 = 0;
        int i8 = this.f1051b;
        if (!z2) {
            i8 = b(oVar, tVar, cVar.d) + c(oVar, tVar, cVar.d);
        }
        while (i6 < this.f1051b && cVar.a(tVar) && i8 > 0) {
            int i9 = cVar.d;
            int c2 = c(oVar, tVar, i9);
            if (c2 <= this.f1051b) {
                i8 -= c2;
                if (i8 >= 0) {
                    View a3 = cVar.a(oVar);
                    if (a3 == null) {
                        break;
                    }
                    i7 += c2;
                    this.d[i6] = a3;
                    i6++;
                } else {
                    break;
                }
            } else {
                throw new IllegalArgumentException("Item at position " + i9 + " requires " + c2 + " spans but GridLayoutManager has only " + this.f1051b + " spans.");
            }
        }
        if (i6 == 0) {
            bVar.f1061b = true;
            return;
        }
        a(oVar, tVar, i6, i7, z2);
        int i10 = 0;
        float f2 = 0.0f;
        int i11 = 0;
        while (i10 < i6) {
            View view = this.d[i10];
            if (cVar.k == null) {
                if (z2) {
                    b(view);
                } else {
                    b(view, 0);
                }
            } else if (z2) {
                a(view);
            } else {
                a(view, 0);
            }
            b(view, this.h);
            a(view, i4, false);
            int e2 = this.j.e(view);
            if (e2 > i11) {
                i11 = e2;
            }
            float f3 = (((float) this.j.f(view)) * 1.0f) / ((float) ((b) view.getLayoutParams()).f1053b);
            if (f3 <= f2) {
                f3 = f2;
            }
            i10++;
            f2 = f3;
        }
        if (z) {
            a(f2, i5);
            i11 = 0;
            int i12 = 0;
            while (i12 < i6) {
                View view2 = this.d[i12];
                a(view2, 1073741824, true);
                int e3 = this.j.e(view2);
                if (e3 <= i11) {
                    e3 = i11;
                }
                i12++;
                i11 = e3;
            }
        }
        for (int i13 = 0; i13 < i6; i13++) {
            View view3 = this.d[i13];
            if (this.j.e(view3) != i11) {
                b bVar2 = (b) view3.getLayoutParams();
                Rect rect = bVar2.d;
                int i14 = rect.top + rect.bottom + bVar2.topMargin + bVar2.bottomMargin;
                int i15 = rect.right + rect.left + bVar2.leftMargin + bVar2.rightMargin;
                int a4 = a(bVar2.f1052a, bVar2.f1053b);
                if (this.i == 1) {
                    makeMeasureSpec = a(a4, 1073741824, i15, bVar2.width, false);
                    a2 = MeasureSpec.makeMeasureSpec(i11 - i14, 1073741824);
                } else {
                    makeMeasureSpec = MeasureSpec.makeMeasureSpec(i11 - i15, 1073741824);
                    a2 = a(a4, 1073741824, i14, bVar2.height, false);
                }
                a(view3, makeMeasureSpec, a2, true);
            }
        }
        bVar.f1060a = i11;
        int i16 = 0;
        if (this.i == 1) {
            if (cVar.f == -1) {
                i16 = cVar.f1063b;
                i3 = i16 - i11;
                i2 = 0;
                i = 0;
            } else {
                int i17 = cVar.f1063b;
                i16 = i17 + i11;
                i3 = i17;
                i2 = 0;
                i = 0;
            }
        } else if (cVar.f == -1) {
            int i18 = cVar.f1063b;
            i2 = i18;
            i = i18 - i11;
            i3 = 0;
        } else {
            i = cVar.f1063b;
            i2 = i11 + i;
            i3 = 0;
        }
        int i19 = i16;
        int i20 = i3;
        int i21 = i2;
        int i22 = i;
        for (int i23 = 0; i23 < i6; i23++) {
            View view4 = this.d[i23];
            b bVar3 = (b) view4.getLayoutParams();
            if (this.i != 1) {
                i20 = A() + this.c[bVar3.f1052a];
                i19 = i20 + this.j.f(view4);
            } else if (g()) {
                i21 = z() + this.c[this.f1051b - bVar3.f1052a];
                i22 = i21 - this.j.f(view4);
            } else {
                i22 = z() + this.c[bVar3.f1052a];
                i21 = i22 + this.j.f(view4);
            }
            a(view4, i22, i20, i21, i19);
            if (bVar3.d() || bVar3.e()) {
                bVar.c = true;
            }
            bVar.d |= view4.hasFocusable();
        }
        Arrays.fill(this.d, null);
    }

    public void a(o oVar, t tVar, View view, android.support.v4.i.a.b bVar) {
        LayoutParams layoutParams = view.getLayoutParams();
        if (!(layoutParams instanceof b)) {
            super.a(view, bVar);
            return;
        }
        b bVar2 = (b) layoutParams;
        int a2 = a(oVar, tVar, bVar2.f());
        if (this.i == 0) {
            bVar.b((Object) C0017b.a(bVar2.a(), bVar2.b(), a2, 1, this.f1051b > 1 && bVar2.b() == this.f1051b, false));
        } else {
            bVar.b((Object) C0017b.a(a2, 1, bVar2.a(), bVar2.b(), this.f1051b > 1 && bVar2.b() == this.f1051b, false));
        }
    }

    public void a(t tVar) {
        super.a(tVar);
        this.f1050a = false;
    }

    /* access modifiers changed from: 0000 */
    public void a(t tVar, c cVar, android.support.v7.widget.RecyclerView.h.a aVar) {
        int i = this.f1051b;
        for (int i2 = 0; i2 < this.f1051b && cVar.a(tVar) && i > 0; i2++) {
            int i3 = cVar.d;
            aVar.b(i3, Math.max(0, cVar.g));
            i -= this.g.a(i3);
            cVar.d += cVar.e;
        }
    }

    public void a(RecyclerView recyclerView) {
        this.g.a();
    }

    public void a(RecyclerView recyclerView, int i, int i2) {
        this.g.a();
    }

    public void a(RecyclerView recyclerView, int i, int i2, int i3) {
        this.g.a();
    }

    public void a(RecyclerView recyclerView, int i, int i2, Object obj) {
        this.g.a();
    }

    public void a(boolean z) {
        if (z) {
            throw new UnsupportedOperationException("GridLayoutManager does not support stack from end. Consider using reverse layout");
        }
        super.a(false);
    }

    public boolean a(i iVar) {
        return iVar instanceof b;
    }

    public int b(int i, o oVar, t tVar) {
        L();
        M();
        return super.b(i, oVar, tVar);
    }

    public int b(o oVar, t tVar) {
        if (this.i == 1) {
            return this.f1051b;
        }
        if (tVar.e() < 1) {
            return 0;
        }
        return a(oVar, tVar, tVar.e() - 1) + 1;
    }

    public void b(RecyclerView recyclerView, int i, int i2) {
        this.g.a();
    }

    public boolean b() {
        return this.n == null && !this.f1050a;
    }

    public void c(o oVar, t tVar) {
        if (tVar.a()) {
            K();
        }
        super.c(oVar, tVar);
        J();
    }
}
