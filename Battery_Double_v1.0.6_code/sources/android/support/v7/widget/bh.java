package android.support.v7.widget;

import android.support.v7.widget.RecyclerView.e;
import android.support.v7.widget.RecyclerView.e.c;
import android.support.v7.widget.RecyclerView.w;
import android.view.View;

public abstract class bh extends e {
    boolean h = true;

    public final void a(w wVar, boolean z) {
        d(wVar, z);
        f(wVar);
    }

    public abstract boolean a(w wVar);

    public abstract boolean a(w wVar, int i, int i2, int i3, int i4);

    public boolean a(w wVar, c cVar, c cVar2) {
        int i = cVar.f1077a;
        int i2 = cVar.f1078b;
        View view = wVar.f1102a;
        int i3 = cVar2 == null ? view.getLeft() : cVar2.f1077a;
        int i4 = cVar2 == null ? view.getTop() : cVar2.f1078b;
        if (wVar.q() || (i == i3 && i2 == i4)) {
            return a(wVar);
        }
        view.layout(i3, i4, view.getWidth() + i3, view.getHeight() + i4);
        return a(wVar, i, i2, i3, i4);
    }

    public abstract boolean a(w wVar, w wVar2, int i, int i2, int i3, int i4);

    public boolean a(w wVar, w wVar2, c cVar, c cVar2) {
        int i;
        int i2;
        int i3 = cVar.f1077a;
        int i4 = cVar.f1078b;
        if (wVar2.c()) {
            i = cVar.f1077a;
            i2 = cVar.f1078b;
        } else {
            i = cVar2.f1077a;
            i2 = cVar2.f1078b;
        }
        return a(wVar, wVar2, i3, i4, i, i2);
    }

    public final void b(w wVar, boolean z) {
        c(wVar, z);
    }

    public abstract boolean b(w wVar);

    public boolean b(w wVar, c cVar, c cVar2) {
        if (cVar == null || (cVar.f1077a == cVar2.f1077a && cVar.f1078b == cVar2.f1078b)) {
            return b(wVar);
        }
        return a(wVar, cVar.f1077a, cVar.f1078b, cVar2.f1077a, cVar2.f1078b);
    }

    public void c(w wVar, boolean z) {
    }

    public boolean c(w wVar, c cVar, c cVar2) {
        if (cVar.f1077a == cVar2.f1077a && cVar.f1078b == cVar2.f1078b) {
            j(wVar);
            return false;
        }
        return a(wVar, cVar.f1077a, cVar.f1078b, cVar2.f1077a, cVar2.f1078b);
    }

    public void d(w wVar, boolean z) {
    }

    public boolean h(w wVar) {
        return !this.h || wVar.n();
    }

    public final void i(w wVar) {
        p(wVar);
        f(wVar);
    }

    public final void j(w wVar) {
        t(wVar);
        f(wVar);
    }

    public final void k(w wVar) {
        r(wVar);
        f(wVar);
    }

    public final void l(w wVar) {
        o(wVar);
    }

    public final void m(w wVar) {
        s(wVar);
    }

    public final void n(w wVar) {
        q(wVar);
    }

    public void o(w wVar) {
    }

    public void p(w wVar) {
    }

    public void q(w wVar) {
    }

    public void r(w wVar) {
    }

    public void s(w wVar) {
    }

    public void t(w wVar) {
    }
}
