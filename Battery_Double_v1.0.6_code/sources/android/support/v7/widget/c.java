package android.support.v7.widget;

import android.graphics.Outline;

class c extends b {
    public c(ActionBarContainer actionBarContainer) {
        super(actionBarContainer);
    }

    public void getOutline(Outline outline) {
        if (this.f1236a.d) {
            if (this.f1236a.c != null) {
                this.f1236a.c.getOutline(outline);
            }
        } else if (this.f1236a.f1020a != null) {
            this.f1236a.f1020a.getOutline(outline);
        }
    }
}
