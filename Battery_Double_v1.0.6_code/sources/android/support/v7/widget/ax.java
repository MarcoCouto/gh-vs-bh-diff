package android.support.v7.widget;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Build.VERSION;
import android.support.v7.view.menu.ListMenuItemView;
import android.support.v7.view.menu.g;
import android.support.v7.view.menu.h;
import android.support.v7.view.menu.j;
import android.transition.Transition;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.widget.HeaderViewListAdapter;
import android.widget.ListAdapter;
import android.widget.PopupWindow;
import java.lang.reflect.Method;

public class ax extends au implements aw {

    /* renamed from: a reason: collision with root package name */
    private static Method f1231a;

    /* renamed from: b reason: collision with root package name */
    private aw f1232b;

    public static class a extends an {
        final int g;
        final int h;
        private aw i;
        private MenuItem j;

        public a(Context context, boolean z) {
            super(context, z);
            Configuration configuration = context.getResources().getConfiguration();
            if (VERSION.SDK_INT < 17 || 1 != configuration.getLayoutDirection()) {
                this.g = 22;
                this.h = 21;
                return;
            }
            this.g = 21;
            this.h = 22;
        }

        public /* bridge */ /* synthetic */ boolean a(MotionEvent motionEvent, int i2) {
            return super.a(motionEvent, i2);
        }

        public /* bridge */ /* synthetic */ boolean hasFocus() {
            return super.hasFocus();
        }

        public /* bridge */ /* synthetic */ boolean hasWindowFocus() {
            return super.hasWindowFocus();
        }

        public /* bridge */ /* synthetic */ boolean isFocused() {
            return super.isFocused();
        }

        public /* bridge */ /* synthetic */ boolean isInTouchMode() {
            return super.isInTouchMode();
        }

        /* JADX WARNING: Removed duplicated region for block: B:16:0x0044  */
        public boolean onHoverEvent(MotionEvent motionEvent) {
            int i2;
            g gVar;
            j jVar;
            MenuItem menuItem;
            if (this.i != null) {
                ListAdapter adapter = getAdapter();
                if (adapter instanceof HeaderViewListAdapter) {
                    HeaderViewListAdapter headerViewListAdapter = (HeaderViewListAdapter) adapter;
                    i2 = headerViewListAdapter.getHeadersCount();
                    gVar = (g) headerViewListAdapter.getWrappedAdapter();
                } else {
                    i2 = 0;
                    gVar = (g) adapter;
                }
                if (motionEvent.getAction() != 10) {
                    int pointToPosition = pointToPosition((int) motionEvent.getX(), (int) motionEvent.getY());
                    if (pointToPosition != -1) {
                        int i3 = pointToPosition - i2;
                        if (i3 >= 0 && i3 < gVar.getCount()) {
                            jVar = gVar.getItem(i3);
                            menuItem = this.j;
                            if (menuItem != jVar) {
                                h a2 = gVar.a();
                                if (menuItem != null) {
                                    this.i.a(a2, menuItem);
                                }
                                this.j = jVar;
                                if (jVar != null) {
                                    this.i.b(a2, jVar);
                                }
                            }
                        }
                    }
                }
                jVar = null;
                menuItem = this.j;
                if (menuItem != jVar) {
                }
            }
            return super.onHoverEvent(motionEvent);
        }

        public boolean onKeyDown(int i2, KeyEvent keyEvent) {
            ListMenuItemView listMenuItemView = (ListMenuItemView) getSelectedView();
            if (listMenuItemView != null && i2 == this.g) {
                if (listMenuItemView.isEnabled() && listMenuItemView.getItemData().hasSubMenu()) {
                    performItemClick(listMenuItemView, getSelectedItemPosition(), getSelectedItemId());
                }
                return true;
            } else if (listMenuItemView == null || i2 != this.h) {
                return super.onKeyDown(i2, keyEvent);
            } else {
                setSelection(-1);
                ((g) getAdapter()).a().a(false);
                return true;
            }
        }

        public void setHoverListener(aw awVar) {
            this.i = awVar;
        }
    }

    static {
        try {
            f1231a = PopupWindow.class.getDeclaredMethod("setTouchModal", new Class[]{Boolean.TYPE});
        } catch (NoSuchMethodException e) {
            Log.i("MenuPopupWindow", "Could not find method setTouchModal() on PopupWindow. Oh well.");
        }
    }

    public ax(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
    }

    /* access modifiers changed from: 0000 */
    public an a(Context context, boolean z) {
        a aVar = new a(context, z);
        aVar.setHoverListener(this);
        return aVar;
    }

    public void a(h hVar, MenuItem menuItem) {
        if (this.f1232b != null) {
            this.f1232b.a(hVar, menuItem);
        }
    }

    public void a(aw awVar) {
        this.f1232b = awVar;
    }

    public void a(Object obj) {
        if (VERSION.SDK_INT >= 23) {
            this.g.setEnterTransition((Transition) obj);
        }
    }

    public void b(h hVar, MenuItem menuItem) {
        if (this.f1232b != null) {
            this.f1232b.b(hVar, menuItem);
        }
    }

    public void b(Object obj) {
        if (VERSION.SDK_INT >= 23) {
            this.g.setExitTransition((Transition) obj);
        }
    }

    public void c(boolean z) {
        if (f1231a != null) {
            try {
                f1231a.invoke(this.g, new Object[]{Boolean.valueOf(z)});
            } catch (Exception e) {
                Log.i("MenuPopupWindow", "Could not invoke setTouchModal() on PopupWindow. Oh well.");
            }
        }
    }
}
