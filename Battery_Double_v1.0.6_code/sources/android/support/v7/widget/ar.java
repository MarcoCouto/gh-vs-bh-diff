package android.support.v7.widget;

import android.support.v4.f.d;
import android.support.v7.widget.RecyclerView.h;
import android.support.v7.widget.RecyclerView.o;
import android.support.v7.widget.RecyclerView.w;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.concurrent.TimeUnit;

final class ar implements Runnable {

    /* renamed from: a reason: collision with root package name */
    static final ThreadLocal<ar> f1209a = new ThreadLocal<>();
    static Comparator<b> e = new Comparator<b>() {
        /* renamed from: a */
        public int compare(b bVar, b bVar2) {
            int i = -1;
            if ((bVar.d == null) != (bVar2.d == null)) {
                return bVar.d == null ? 1 : -1;
            }
            if (bVar.f1213a != bVar2.f1213a) {
                if (!bVar.f1213a) {
                    i = 1;
                }
                return i;
            }
            int i2 = bVar2.f1214b - bVar.f1214b;
            if (i2 != 0) {
                return i2;
            }
            int i3 = bVar.c - bVar2.c;
            if (i3 == 0) {
                return 0;
            }
            return i3;
        }
    };

    /* renamed from: b reason: collision with root package name */
    ArrayList<RecyclerView> f1210b = new ArrayList<>();
    long c;
    long d;
    private ArrayList<b> f = new ArrayList<>();

    static class a implements android.support.v7.widget.RecyclerView.h.a {

        /* renamed from: a reason: collision with root package name */
        int f1211a;

        /* renamed from: b reason: collision with root package name */
        int f1212b;
        int[] c;
        int d;

        a() {
        }

        /* access modifiers changed from: 0000 */
        public void a() {
            if (this.c != null) {
                Arrays.fill(this.c, -1);
            }
            this.d = 0;
        }

        /* access modifiers changed from: 0000 */
        public void a(int i, int i2) {
            this.f1211a = i;
            this.f1212b = i2;
        }

        /* access modifiers changed from: 0000 */
        public void a(RecyclerView recyclerView, boolean z) {
            this.d = 0;
            if (this.c != null) {
                Arrays.fill(this.c, -1);
            }
            h hVar = recyclerView.m;
            if (recyclerView.l != null && hVar != null && hVar.o()) {
                if (z) {
                    if (!recyclerView.e.d()) {
                        hVar.a(recyclerView.l.a(), (android.support.v7.widget.RecyclerView.h.a) this);
                    }
                } else if (!recyclerView.w()) {
                    hVar.a(this.f1211a, this.f1212b, recyclerView.B, (android.support.v7.widget.RecyclerView.h.a) this);
                }
                if (this.d > hVar.x) {
                    hVar.x = this.d;
                    hVar.y = z;
                    recyclerView.d.b();
                }
            }
        }

        /* access modifiers changed from: 0000 */
        public boolean a(int i) {
            if (this.c == null) {
                return false;
            }
            int i2 = this.d * 2;
            for (int i3 = 0; i3 < i2; i3 += 2) {
                if (this.c[i3] == i) {
                    return true;
                }
            }
            return false;
        }

        public void b(int i, int i2) {
            if (i < 0) {
                throw new IllegalArgumentException("Layout positions must be non-negative");
            } else if (i2 < 0) {
                throw new IllegalArgumentException("Pixel distance must be non-negative");
            } else {
                int i3 = this.d * 2;
                if (this.c == null) {
                    this.c = new int[4];
                    Arrays.fill(this.c, -1);
                } else if (i3 >= this.c.length) {
                    int[] iArr = this.c;
                    this.c = new int[(i3 * 2)];
                    System.arraycopy(iArr, 0, this.c, 0, iArr.length);
                }
                this.c[i3] = i;
                this.c[i3 + 1] = i2;
                this.d++;
            }
        }
    }

    static class b {

        /* renamed from: a reason: collision with root package name */
        public boolean f1213a;

        /* renamed from: b reason: collision with root package name */
        public int f1214b;
        public int c;
        public RecyclerView d;
        public int e;

        b() {
        }

        public void a() {
            this.f1213a = false;
            this.f1214b = 0;
            this.c = 0;
            this.d = null;
            this.e = 0;
        }
    }

    ar() {
    }

    private w a(RecyclerView recyclerView, int i, long j) {
        if (a(recyclerView, i)) {
            return null;
        }
        o oVar = recyclerView.d;
        try {
            recyclerView.l();
            w a2 = oVar.a(i, false, j);
            if (a2 != null) {
                if (!a2.p() || a2.n()) {
                    oVar.a(a2, false);
                } else {
                    oVar.a(a2.f1102a);
                }
            }
            return a2;
        } finally {
            recyclerView.b(false);
        }
    }

    private void a() {
        b bVar;
        int i;
        int size = this.f1210b.size();
        int i2 = 0;
        int i3 = 0;
        while (i2 < size) {
            RecyclerView recyclerView = (RecyclerView) this.f1210b.get(i2);
            if (recyclerView.getWindowVisibility() == 0) {
                recyclerView.A.a(recyclerView, false);
                i = recyclerView.A.d + i3;
            } else {
                i = i3;
            }
            i2++;
            i3 = i;
        }
        this.f.ensureCapacity(i3);
        int i4 = 0;
        for (int i5 = 0; i5 < size; i5++) {
            RecyclerView recyclerView2 = (RecyclerView) this.f1210b.get(i5);
            if (recyclerView2.getWindowVisibility() == 0) {
                a aVar = recyclerView2.A;
                int abs = Math.abs(aVar.f1211a) + Math.abs(aVar.f1212b);
                int i6 = i4;
                for (int i7 = 0; i7 < aVar.d * 2; i7 += 2) {
                    if (i6 >= this.f.size()) {
                        bVar = new b();
                        this.f.add(bVar);
                    } else {
                        bVar = (b) this.f.get(i6);
                    }
                    int i8 = aVar.c[i7 + 1];
                    bVar.f1213a = i8 <= abs;
                    bVar.f1214b = abs;
                    bVar.c = i8;
                    bVar.d = recyclerView2;
                    bVar.e = aVar.c[i7];
                    i6++;
                }
                i4 = i6;
            }
        }
        Collections.sort(this.f, e);
    }

    private void a(RecyclerView recyclerView, long j) {
        if (recyclerView != null) {
            if (recyclerView.w && recyclerView.f.c() != 0) {
                recyclerView.c();
            }
            a aVar = recyclerView.A;
            aVar.a(recyclerView, true);
            if (aVar.d != 0) {
                try {
                    d.a("RV Nested Prefetch");
                    recyclerView.B.a(recyclerView.l);
                    for (int i = 0; i < aVar.d * 2; i += 2) {
                        a(recyclerView, aVar.c[i], j);
                    }
                } finally {
                    d.a();
                }
            }
        }
    }

    private void a(b bVar, long j) {
        w a2 = a(bVar.d, bVar.e, bVar.f1213a ? Long.MAX_VALUE : j);
        if (a2 != null && a2.f1103b != null && a2.p() && !a2.n()) {
            a((RecyclerView) a2.f1103b.get(), j);
        }
    }

    static boolean a(RecyclerView recyclerView, int i) {
        int c2 = recyclerView.f.c();
        for (int i2 = 0; i2 < c2; i2++) {
            w e2 = RecyclerView.e(recyclerView.f.d(i2));
            if (e2.c == i && !e2.n()) {
                return true;
            }
        }
        return false;
    }

    private void b(long j) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.f.size()) {
                b bVar = (b) this.f.get(i2);
                if (bVar.d != null) {
                    a(bVar, j);
                    bVar.a();
                    i = i2 + 1;
                } else {
                    return;
                }
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(long j) {
        a();
        b(j);
    }

    public void a(RecyclerView recyclerView) {
        this.f1210b.add(recyclerView);
    }

    /* access modifiers changed from: 0000 */
    public void a(RecyclerView recyclerView, int i, int i2) {
        if (recyclerView.isAttachedToWindow() && this.c == 0) {
            this.c = recyclerView.getNanoTime();
            recyclerView.post(this);
        }
        recyclerView.A.a(i, i2);
    }

    public void b(RecyclerView recyclerView) {
        this.f1210b.remove(recyclerView);
    }

    public void run() {
        try {
            d.a("RV Prefetch");
            if (!this.f1210b.isEmpty()) {
                int size = this.f1210b.size();
                int i = 0;
                long j = 0;
                while (i < size) {
                    RecyclerView recyclerView = (RecyclerView) this.f1210b.get(i);
                    i++;
                    j = recyclerView.getWindowVisibility() == 0 ? Math.max(recyclerView.getDrawingTime(), j) : j;
                }
                if (j == 0) {
                    this.c = 0;
                    d.a();
                    return;
                }
                a(TimeUnit.MILLISECONDS.toNanos(j) + this.d);
                this.c = 0;
                d.a();
            }
        } finally {
            this.c = 0;
            d.a();
        }
    }
}
