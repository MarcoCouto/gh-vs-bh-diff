package android.support.v7.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.support.v4.i.s;
import android.support.v7.a.a.C0026a;
import android.support.v7.c.a.b;
import android.util.AttributeSet;
import android.widget.MultiAutoCompleteTextView;
import android.widget.TextView;

public class r extends MultiAutoCompleteTextView implements s {

    /* renamed from: a reason: collision with root package name */
    private static final int[] f1330a = {16843126};

    /* renamed from: b reason: collision with root package name */
    private final h f1331b;
    private final z c;

    public r(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, C0026a.autoCompleteTextViewStyle);
    }

    public r(Context context, AttributeSet attributeSet, int i) {
        super(bl.a(context), attributeSet, i);
        bo a2 = bo.a(getContext(), attributeSet, f1330a, i, 0);
        if (a2.g(0)) {
            setDropDownBackgroundDrawable(a2.a(0));
        }
        a2.a();
        this.f1331b = new h(this);
        this.f1331b.a(attributeSet, i);
        this.c = z.a((TextView) this);
        this.c.a(attributeSet, i);
        this.c.a();
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        if (this.f1331b != null) {
            this.f1331b.c();
        }
        if (this.c != null) {
            this.c.a();
        }
    }

    public ColorStateList getSupportBackgroundTintList() {
        if (this.f1331b != null) {
            return this.f1331b.a();
        }
        return null;
    }

    public Mode getSupportBackgroundTintMode() {
        if (this.f1331b != null) {
            return this.f1331b.b();
        }
        return null;
    }

    public void setBackgroundDrawable(Drawable drawable) {
        super.setBackgroundDrawable(drawable);
        if (this.f1331b != null) {
            this.f1331b.a(drawable);
        }
    }

    public void setBackgroundResource(int i) {
        super.setBackgroundResource(i);
        if (this.f1331b != null) {
            this.f1331b.a(i);
        }
    }

    public void setDropDownBackgroundResource(int i) {
        setDropDownBackgroundDrawable(b.b(getContext(), i));
    }

    public void setSupportBackgroundTintList(ColorStateList colorStateList) {
        if (this.f1331b != null) {
            this.f1331b.a(colorStateList);
        }
    }

    public void setSupportBackgroundTintMode(Mode mode) {
        if (this.f1331b != null) {
            this.f1331b.a(mode);
        }
    }

    public void setTextAppearance(Context context, int i) {
        super.setTextAppearance(context, i);
        if (this.c != null) {
            this.c.a(context, i);
        }
    }
}
