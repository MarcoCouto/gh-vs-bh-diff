package android.support.v7.widget;

import android.annotation.TargetApi;
import android.os.Build.VERSION;
import android.view.View;

public class bq {

    /* renamed from: a reason: collision with root package name */
    private static final c f1271a;

    @TargetApi(26)
    private static class a implements c {
        private a() {
        }

        public void a(View view, CharSequence charSequence) {
            view.setTooltipText(charSequence);
        }
    }

    private static class b implements c {
        private b() {
        }

        public void a(View view, CharSequence charSequence) {
            br.a(view, charSequence);
        }
    }

    private interface c {
        void a(View view, CharSequence charSequence);
    }

    static {
        if (VERSION.SDK_INT >= 26) {
            f1271a = new a();
        } else {
            f1271a = new b();
        }
    }

    public static void a(View view, CharSequence charSequence) {
        f1271a.a(view, charSequence);
    }
}
