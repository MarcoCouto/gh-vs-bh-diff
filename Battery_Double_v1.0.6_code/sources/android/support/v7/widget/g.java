package android.support.v7.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.support.v4.i.s;
import android.support.v7.a.a.C0026a;
import android.support.v7.c.a.b;
import android.util.AttributeSet;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;

public class g extends AutoCompleteTextView implements s {

    /* renamed from: a reason: collision with root package name */
    private static final int[] f1310a = {16843126};

    /* renamed from: b reason: collision with root package name */
    private final h f1311b;
    private final z c;

    public g(Context context) {
        this(context, null);
    }

    public g(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, C0026a.autoCompleteTextViewStyle);
    }

    public g(Context context, AttributeSet attributeSet, int i) {
        super(bl.a(context), attributeSet, i);
        bo a2 = bo.a(getContext(), attributeSet, f1310a, i, 0);
        if (a2.g(0)) {
            setDropDownBackgroundDrawable(a2.a(0));
        }
        a2.a();
        this.f1311b = new h(this);
        this.f1311b.a(attributeSet, i);
        this.c = z.a((TextView) this);
        this.c.a(attributeSet, i);
        this.c.a();
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        if (this.f1311b != null) {
            this.f1311b.c();
        }
        if (this.c != null) {
            this.c.a();
        }
    }

    public ColorStateList getSupportBackgroundTintList() {
        if (this.f1311b != null) {
            return this.f1311b.a();
        }
        return null;
    }

    public Mode getSupportBackgroundTintMode() {
        if (this.f1311b != null) {
            return this.f1311b.b();
        }
        return null;
    }

    public void setBackgroundDrawable(Drawable drawable) {
        super.setBackgroundDrawable(drawable);
        if (this.f1311b != null) {
            this.f1311b.a(drawable);
        }
    }

    public void setBackgroundResource(int i) {
        super.setBackgroundResource(i);
        if (this.f1311b != null) {
            this.f1311b.a(i);
        }
    }

    public void setDropDownBackgroundResource(int i) {
        setDropDownBackgroundDrawable(b.b(getContext(), i));
    }

    public void setSupportBackgroundTintList(ColorStateList colorStateList) {
        if (this.f1311b != null) {
            this.f1311b.a(colorStateList);
        }
    }

    public void setSupportBackgroundTintMode(Mode mode) {
        if (this.f1311b != null) {
            this.f1311b.a(mode);
        }
    }

    public void setTextAppearance(Context context, int i) {
        super.setTextAppearance(context, i);
        if (this.c != null) {
            this.c.a(context, i);
        }
    }
}
