package android.support.v7.widget;

import android.support.v7.widget.RecyclerView.o;
import android.support.v7.widget.RecyclerView.t;
import android.view.View;

class as {

    /* renamed from: a reason: collision with root package name */
    boolean f1215a = true;

    /* renamed from: b reason: collision with root package name */
    int f1216b;
    int c;
    int d;
    int e;
    int f = 0;
    int g = 0;
    boolean h;
    boolean i;

    as() {
    }

    /* access modifiers changed from: 0000 */
    public View a(o oVar) {
        View c2 = oVar.c(this.c);
        this.c += this.d;
        return c2;
    }

    /* access modifiers changed from: 0000 */
    public boolean a(t tVar) {
        return this.c >= 0 && this.c < tVar.e();
    }

    public String toString() {
        return "LayoutState{mAvailable=" + this.f1216b + ", mCurrentPosition=" + this.c + ", mItemDirection=" + this.d + ", mLayoutDirection=" + this.e + ", mStartLine=" + this.f + ", mEndLine=" + this.g + '}';
    }
}
