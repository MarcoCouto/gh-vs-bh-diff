package android.support.v7.widget;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView.h;
import android.support.v7.widget.RecyclerView.i;
import android.view.View;

public abstract class az {

    /* renamed from: a reason: collision with root package name */
    protected final h f1234a;

    /* renamed from: b reason: collision with root package name */
    final Rect f1235b;
    private int c;

    private az(h hVar) {
        this.c = Integer.MIN_VALUE;
        this.f1235b = new Rect();
        this.f1234a = hVar;
    }

    public static az a(h hVar) {
        return new az(hVar) {
            public int a(View view) {
                return this.f1234a.h(view) - ((i) view.getLayoutParams()).leftMargin;
            }

            public void a(int i) {
                this.f1234a.i(i);
            }

            public int b(View view) {
                i iVar = (i) view.getLayoutParams();
                return iVar.rightMargin + this.f1234a.j(view);
            }

            public int c() {
                return this.f1234a.z();
            }

            public int c(View view) {
                this.f1234a.a(view, true, this.f1235b);
                return this.f1235b.right;
            }

            public int d() {
                return this.f1234a.x() - this.f1234a.B();
            }

            public int d(View view) {
                this.f1234a.a(view, true, this.f1235b);
                return this.f1235b.left;
            }

            public int e() {
                return this.f1234a.x();
            }

            public int e(View view) {
                i iVar = (i) view.getLayoutParams();
                return iVar.rightMargin + this.f1234a.f(view) + iVar.leftMargin;
            }

            public int f() {
                return (this.f1234a.x() - this.f1234a.z()) - this.f1234a.B();
            }

            public int f(View view) {
                i iVar = (i) view.getLayoutParams();
                return iVar.bottomMargin + this.f1234a.g(view) + iVar.topMargin;
            }

            public int g() {
                return this.f1234a.B();
            }

            public int h() {
                return this.f1234a.v();
            }

            public int i() {
                return this.f1234a.w();
            }
        };
    }

    public static az a(h hVar, int i) {
        switch (i) {
            case 0:
                return a(hVar);
            case 1:
                return b(hVar);
            default:
                throw new IllegalArgumentException("invalid orientation");
        }
    }

    public static az b(h hVar) {
        return new az(hVar) {
            public int a(View view) {
                return this.f1234a.i(view) - ((i) view.getLayoutParams()).topMargin;
            }

            public void a(int i) {
                this.f1234a.j(i);
            }

            public int b(View view) {
                i iVar = (i) view.getLayoutParams();
                return iVar.bottomMargin + this.f1234a.k(view);
            }

            public int c() {
                return this.f1234a.A();
            }

            public int c(View view) {
                this.f1234a.a(view, true, this.f1235b);
                return this.f1235b.bottom;
            }

            public int d() {
                return this.f1234a.y() - this.f1234a.C();
            }

            public int d(View view) {
                this.f1234a.a(view, true, this.f1235b);
                return this.f1235b.top;
            }

            public int e() {
                return this.f1234a.y();
            }

            public int e(View view) {
                i iVar = (i) view.getLayoutParams();
                return iVar.bottomMargin + this.f1234a.g(view) + iVar.topMargin;
            }

            public int f() {
                return (this.f1234a.y() - this.f1234a.A()) - this.f1234a.C();
            }

            public int f(View view) {
                i iVar = (i) view.getLayoutParams();
                return iVar.rightMargin + this.f1234a.f(view) + iVar.leftMargin;
            }

            public int g() {
                return this.f1234a.C();
            }

            public int h() {
                return this.f1234a.w();
            }

            public int i() {
                return this.f1234a.v();
            }
        };
    }

    public abstract int a(View view);

    public void a() {
        this.c = f();
    }

    public abstract void a(int i);

    public int b() {
        if (Integer.MIN_VALUE == this.c) {
            return 0;
        }
        return f() - this.c;
    }

    public abstract int b(View view);

    public abstract int c();

    public abstract int c(View view);

    public abstract int d();

    public abstract int d(View view);

    public abstract int e();

    public abstract int e(View view);

    public abstract int f();

    public abstract int f(View view);

    public abstract int g();

    public abstract int h();

    public abstract int i();
}
