package android.support.v7.widget;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.support.v4.i.t;
import android.support.v7.widget.RecyclerView.w;
import android.view.View;
import android.view.ViewPropertyAnimator;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class al extends bh {
    private static TimeInterpolator i;

    /* renamed from: a reason: collision with root package name */
    ArrayList<ArrayList<w>> f1174a = new ArrayList<>();

    /* renamed from: b reason: collision with root package name */
    ArrayList<ArrayList<b>> f1175b = new ArrayList<>();
    ArrayList<ArrayList<a>> c = new ArrayList<>();
    ArrayList<w> d = new ArrayList<>();
    ArrayList<w> e = new ArrayList<>();
    ArrayList<w> f = new ArrayList<>();
    ArrayList<w> g = new ArrayList<>();
    private ArrayList<w> j = new ArrayList<>();
    private ArrayList<w> k = new ArrayList<>();
    private ArrayList<b> l = new ArrayList<>();
    private ArrayList<a> m = new ArrayList<>();

    private static class a {

        /* renamed from: a reason: collision with root package name */
        public w f1192a;

        /* renamed from: b reason: collision with root package name */
        public w f1193b;
        public int c;
        public int d;
        public int e;
        public int f;

        private a(w wVar, w wVar2) {
            this.f1192a = wVar;
            this.f1193b = wVar2;
        }

        a(w wVar, w wVar2, int i, int i2, int i3, int i4) {
            this(wVar, wVar2);
            this.c = i;
            this.d = i2;
            this.e = i3;
            this.f = i4;
        }

        public String toString() {
            return "ChangeInfo{oldHolder=" + this.f1192a + ", newHolder=" + this.f1193b + ", fromX=" + this.c + ", fromY=" + this.d + ", toX=" + this.e + ", toY=" + this.f + '}';
        }
    }

    private static class b {

        /* renamed from: a reason: collision with root package name */
        public w f1194a;

        /* renamed from: b reason: collision with root package name */
        public int f1195b;
        public int c;
        public int d;
        public int e;

        b(w wVar, int i, int i2, int i3, int i4) {
            this.f1194a = wVar;
            this.f1195b = i;
            this.c = i2;
            this.d = i3;
            this.e = i4;
        }
    }

    private void a(List<a> list, w wVar) {
        for (int size = list.size() - 1; size >= 0; size--) {
            a aVar = (a) list.get(size);
            if (a(aVar, wVar) && aVar.f1192a == null && aVar.f1193b == null) {
                list.remove(aVar);
            }
        }
    }

    private boolean a(a aVar, w wVar) {
        boolean z = false;
        if (aVar.f1193b == wVar) {
            aVar.f1193b = null;
        } else if (aVar.f1192a != wVar) {
            return false;
        } else {
            aVar.f1192a = null;
            z = true;
        }
        wVar.f1102a.setAlpha(1.0f);
        wVar.f1102a.setTranslationX(0.0f);
        wVar.f1102a.setTranslationY(0.0f);
        a(wVar, z);
        return true;
    }

    private void b(a aVar) {
        if (aVar.f1192a != null) {
            a(aVar, aVar.f1192a);
        }
        if (aVar.f1193b != null) {
            a(aVar, aVar.f1193b);
        }
    }

    private void u(final w wVar) {
        final View view = wVar.f1102a;
        final ViewPropertyAnimator animate = view.animate();
        this.f.add(wVar);
        animate.setDuration(g()).alpha(0.0f).setListener(new AnimatorListenerAdapter() {
            public void onAnimationEnd(Animator animator) {
                animate.setListener(null);
                view.setAlpha(1.0f);
                al.this.i(wVar);
                al.this.f.remove(wVar);
                al.this.c();
            }

            public void onAnimationStart(Animator animator) {
                al.this.l(wVar);
            }
        }).start();
    }

    private void v(w wVar) {
        if (i == null) {
            i = new ValueAnimator().getInterpolator();
        }
        wVar.f1102a.animate().setInterpolator(i);
        d(wVar);
    }

    public void a() {
        boolean z = !this.j.isEmpty();
        boolean z2 = !this.l.isEmpty();
        boolean z3 = !this.m.isEmpty();
        boolean z4 = !this.k.isEmpty();
        if (z || z2 || z4 || z3) {
            Iterator it = this.j.iterator();
            while (it.hasNext()) {
                u((w) it.next());
            }
            this.j.clear();
            if (z2) {
                final ArrayList arrayList = new ArrayList();
                arrayList.addAll(this.l);
                this.f1175b.add(arrayList);
                this.l.clear();
                AnonymousClass1 r8 = new Runnable() {
                    public void run() {
                        Iterator it = arrayList.iterator();
                        while (it.hasNext()) {
                            b bVar = (b) it.next();
                            al.this.b(bVar.f1194a, bVar.f1195b, bVar.c, bVar.d, bVar.e);
                        }
                        arrayList.clear();
                        al.this.f1175b.remove(arrayList);
                    }
                };
                if (z) {
                    t.a(((b) arrayList.get(0)).f1194a.f1102a, (Runnable) r8, g());
                } else {
                    r8.run();
                }
            }
            if (z3) {
                final ArrayList arrayList2 = new ArrayList();
                arrayList2.addAll(this.m);
                this.c.add(arrayList2);
                this.m.clear();
                AnonymousClass2 r82 = new Runnable() {
                    public void run() {
                        Iterator it = arrayList2.iterator();
                        while (it.hasNext()) {
                            al.this.a((a) it.next());
                        }
                        arrayList2.clear();
                        al.this.c.remove(arrayList2);
                    }
                };
                if (z) {
                    t.a(((a) arrayList2.get(0)).f1192a.f1102a, (Runnable) r82, g());
                } else {
                    r82.run();
                }
            }
            if (z4) {
                final ArrayList arrayList3 = new ArrayList();
                arrayList3.addAll(this.k);
                this.f1174a.add(arrayList3);
                this.k.clear();
                AnonymousClass3 r12 = new Runnable() {
                    public void run() {
                        Iterator it = arrayList3.iterator();
                        while (it.hasNext()) {
                            al.this.c((w) it.next());
                        }
                        arrayList3.clear();
                        al.this.f1174a.remove(arrayList3);
                    }
                };
                if (z || z2 || z3) {
                    t.a(((w) arrayList3.get(0)).f1102a, (Runnable) r12, (z ? g() : 0) + Math.max(z2 ? e() : 0, z3 ? h() : 0));
                } else {
                    r12.run();
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(final a aVar) {
        final View view = null;
        w wVar = aVar.f1192a;
        final View view2 = wVar == null ? null : wVar.f1102a;
        w wVar2 = aVar.f1193b;
        if (wVar2 != null) {
            view = wVar2.f1102a;
        }
        if (view2 != null) {
            final ViewPropertyAnimator duration = view2.animate().setDuration(h());
            this.g.add(aVar.f1192a);
            duration.translationX((float) (aVar.e - aVar.c));
            duration.translationY((float) (aVar.f - aVar.d));
            duration.alpha(0.0f).setListener(new AnimatorListenerAdapter() {
                public void onAnimationEnd(Animator animator) {
                    duration.setListener(null);
                    view2.setAlpha(1.0f);
                    view2.setTranslationX(0.0f);
                    view2.setTranslationY(0.0f);
                    al.this.a(aVar.f1192a, true);
                    al.this.g.remove(aVar.f1192a);
                    al.this.c();
                }

                public void onAnimationStart(Animator animator) {
                    al.this.b(aVar.f1192a, true);
                }
            }).start();
        }
        if (view != null) {
            final ViewPropertyAnimator animate = view.animate();
            this.g.add(aVar.f1193b);
            animate.translationX(0.0f).translationY(0.0f).setDuration(h()).alpha(1.0f).setListener(new AnimatorListenerAdapter() {
                public void onAnimationEnd(Animator animator) {
                    animate.setListener(null);
                    view.setAlpha(1.0f);
                    view.setTranslationX(0.0f);
                    view.setTranslationY(0.0f);
                    al.this.a(aVar.f1193b, false);
                    al.this.g.remove(aVar.f1193b);
                    al.this.c();
                }

                public void onAnimationStart(Animator animator) {
                    al.this.b(aVar.f1193b, false);
                }
            }).start();
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(List<w> list) {
        for (int size = list.size() - 1; size >= 0; size--) {
            ((w) list.get(size)).f1102a.animate().cancel();
        }
    }

    public boolean a(w wVar) {
        v(wVar);
        this.j.add(wVar);
        return true;
    }

    public boolean a(w wVar, int i2, int i3, int i4, int i5) {
        View view = wVar.f1102a;
        int translationX = i2 + ((int) wVar.f1102a.getTranslationX());
        int translationY = i3 + ((int) wVar.f1102a.getTranslationY());
        v(wVar);
        int i6 = i4 - translationX;
        int i7 = i5 - translationY;
        if (i6 == 0 && i7 == 0) {
            j(wVar);
            return false;
        }
        if (i6 != 0) {
            view.setTranslationX((float) (-i6));
        }
        if (i7 != 0) {
            view.setTranslationY((float) (-i7));
        }
        this.l.add(new b(wVar, translationX, translationY, i4, i5));
        return true;
    }

    public boolean a(w wVar, w wVar2, int i2, int i3, int i4, int i5) {
        if (wVar == wVar2) {
            return a(wVar, i2, i3, i4, i5);
        }
        float translationX = wVar.f1102a.getTranslationX();
        float translationY = wVar.f1102a.getTranslationY();
        float alpha = wVar.f1102a.getAlpha();
        v(wVar);
        int i6 = (int) (((float) (i4 - i2)) - translationX);
        int i7 = (int) (((float) (i5 - i3)) - translationY);
        wVar.f1102a.setTranslationX(translationX);
        wVar.f1102a.setTranslationY(translationY);
        wVar.f1102a.setAlpha(alpha);
        if (wVar2 != null) {
            v(wVar2);
            wVar2.f1102a.setTranslationX((float) (-i6));
            wVar2.f1102a.setTranslationY((float) (-i7));
            wVar2.f1102a.setAlpha(0.0f);
        }
        this.m.add(new a(wVar, wVar2, i2, i3, i4, i5));
        return true;
    }

    public boolean a(w wVar, List<Object> list) {
        return !list.isEmpty() || super.a(wVar, list);
    }

    /* access modifiers changed from: 0000 */
    public void b(w wVar, int i2, int i3, int i4, int i5) {
        final View view = wVar.f1102a;
        final int i6 = i4 - i2;
        final int i7 = i5 - i3;
        if (i6 != 0) {
            view.animate().translationX(0.0f);
        }
        if (i7 != 0) {
            view.animate().translationY(0.0f);
        }
        final ViewPropertyAnimator animate = view.animate();
        this.e.add(wVar);
        final w wVar2 = wVar;
        animate.setDuration(e()).setListener(new AnimatorListenerAdapter() {
            public void onAnimationCancel(Animator animator) {
                if (i6 != 0) {
                    view.setTranslationX(0.0f);
                }
                if (i7 != 0) {
                    view.setTranslationY(0.0f);
                }
            }

            public void onAnimationEnd(Animator animator) {
                animate.setListener(null);
                al.this.j(wVar2);
                al.this.e.remove(wVar2);
                al.this.c();
            }

            public void onAnimationStart(Animator animator) {
                al.this.m(wVar2);
            }
        }).start();
    }

    public boolean b() {
        return !this.k.isEmpty() || !this.m.isEmpty() || !this.l.isEmpty() || !this.j.isEmpty() || !this.e.isEmpty() || !this.f.isEmpty() || !this.d.isEmpty() || !this.g.isEmpty() || !this.f1175b.isEmpty() || !this.f1174a.isEmpty() || !this.c.isEmpty();
    }

    public boolean b(w wVar) {
        v(wVar);
        wVar.f1102a.setAlpha(0.0f);
        this.k.add(wVar);
        return true;
    }

    /* access modifiers changed from: 0000 */
    public void c() {
        if (!b()) {
            i();
        }
    }

    /* access modifiers changed from: 0000 */
    public void c(final w wVar) {
        final View view = wVar.f1102a;
        final ViewPropertyAnimator animate = view.animate();
        this.d.add(wVar);
        animate.alpha(1.0f).setDuration(f()).setListener(new AnimatorListenerAdapter() {
            public void onAnimationCancel(Animator animator) {
                view.setAlpha(1.0f);
            }

            public void onAnimationEnd(Animator animator) {
                animate.setListener(null);
                al.this.k(wVar);
                al.this.d.remove(wVar);
                al.this.c();
            }

            public void onAnimationStart(Animator animator) {
                al.this.n(wVar);
            }
        }).start();
    }

    public void d() {
        for (int size = this.l.size() - 1; size >= 0; size--) {
            b bVar = (b) this.l.get(size);
            View view = bVar.f1194a.f1102a;
            view.setTranslationY(0.0f);
            view.setTranslationX(0.0f);
            j(bVar.f1194a);
            this.l.remove(size);
        }
        for (int size2 = this.j.size() - 1; size2 >= 0; size2--) {
            i((w) this.j.get(size2));
            this.j.remove(size2);
        }
        for (int size3 = this.k.size() - 1; size3 >= 0; size3--) {
            w wVar = (w) this.k.get(size3);
            wVar.f1102a.setAlpha(1.0f);
            k(wVar);
            this.k.remove(size3);
        }
        for (int size4 = this.m.size() - 1; size4 >= 0; size4--) {
            b((a) this.m.get(size4));
        }
        this.m.clear();
        if (b()) {
            for (int size5 = this.f1175b.size() - 1; size5 >= 0; size5--) {
                ArrayList arrayList = (ArrayList) this.f1175b.get(size5);
                for (int size6 = arrayList.size() - 1; size6 >= 0; size6--) {
                    b bVar2 = (b) arrayList.get(size6);
                    View view2 = bVar2.f1194a.f1102a;
                    view2.setTranslationY(0.0f);
                    view2.setTranslationX(0.0f);
                    j(bVar2.f1194a);
                    arrayList.remove(size6);
                    if (arrayList.isEmpty()) {
                        this.f1175b.remove(arrayList);
                    }
                }
            }
            for (int size7 = this.f1174a.size() - 1; size7 >= 0; size7--) {
                ArrayList arrayList2 = (ArrayList) this.f1174a.get(size7);
                for (int size8 = arrayList2.size() - 1; size8 >= 0; size8--) {
                    w wVar2 = (w) arrayList2.get(size8);
                    wVar2.f1102a.setAlpha(1.0f);
                    k(wVar2);
                    arrayList2.remove(size8);
                    if (arrayList2.isEmpty()) {
                        this.f1174a.remove(arrayList2);
                    }
                }
            }
            for (int size9 = this.c.size() - 1; size9 >= 0; size9--) {
                ArrayList arrayList3 = (ArrayList) this.c.get(size9);
                for (int size10 = arrayList3.size() - 1; size10 >= 0; size10--) {
                    b((a) arrayList3.get(size10));
                    if (arrayList3.isEmpty()) {
                        this.c.remove(arrayList3);
                    }
                }
            }
            a((List<w>) this.f);
            a((List<w>) this.e);
            a((List<w>) this.d);
            a((List<w>) this.g);
            i();
        }
    }

    public void d(w wVar) {
        View view = wVar.f1102a;
        view.animate().cancel();
        for (int size = this.l.size() - 1; size >= 0; size--) {
            if (((b) this.l.get(size)).f1194a == wVar) {
                view.setTranslationY(0.0f);
                view.setTranslationX(0.0f);
                j(wVar);
                this.l.remove(size);
            }
        }
        a((List<a>) this.m, wVar);
        if (this.j.remove(wVar)) {
            view.setAlpha(1.0f);
            i(wVar);
        }
        if (this.k.remove(wVar)) {
            view.setAlpha(1.0f);
            k(wVar);
        }
        for (int size2 = this.c.size() - 1; size2 >= 0; size2--) {
            ArrayList arrayList = (ArrayList) this.c.get(size2);
            a((List<a>) arrayList, wVar);
            if (arrayList.isEmpty()) {
                this.c.remove(size2);
            }
        }
        for (int size3 = this.f1175b.size() - 1; size3 >= 0; size3--) {
            ArrayList arrayList2 = (ArrayList) this.f1175b.get(size3);
            int size4 = arrayList2.size() - 1;
            while (true) {
                if (size4 < 0) {
                    break;
                } else if (((b) arrayList2.get(size4)).f1194a == wVar) {
                    view.setTranslationY(0.0f);
                    view.setTranslationX(0.0f);
                    j(wVar);
                    arrayList2.remove(size4);
                    if (arrayList2.isEmpty()) {
                        this.f1175b.remove(size3);
                    }
                } else {
                    size4--;
                }
            }
        }
        for (int size5 = this.f1174a.size() - 1; size5 >= 0; size5--) {
            ArrayList arrayList3 = (ArrayList) this.f1174a.get(size5);
            if (arrayList3.remove(wVar)) {
                view.setAlpha(1.0f);
                k(wVar);
                if (arrayList3.isEmpty()) {
                    this.f1174a.remove(size5);
                }
            }
        }
        if (this.f.remove(wVar)) {
        }
        if (this.d.remove(wVar)) {
        }
        if (this.g.remove(wVar)) {
        }
        if (this.e.remove(wVar)) {
        }
        c();
    }
}
