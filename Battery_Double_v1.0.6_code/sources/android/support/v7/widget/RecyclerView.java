package android.support.v7.widget;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.database.Observable;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.ClassLoaderCreator;
import android.os.Parcelable.Creator;
import android.os.SystemClock;
import android.support.v4.i.a.b.C0017b;
import android.support.v7.e.a.C0030a;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.view.Display;
import android.view.FocusFinder;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.view.animation.Interpolator;
import android.widget.EdgeEffect;
import android.widget.OverScroller;
import com.hmatalonga.greenhub.Config;
import java.lang.ref.WeakReference;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RecyclerView extends ViewGroup implements android.support.v4.i.k {
    static final Interpolator H = new Interpolator() {
        public float getInterpolation(float f) {
            float f2 = f - 1.0f;
            return (f2 * f2 * f2 * f2 * f2) + 1.0f;
        }
    };
    private static final int[] I = {16843830};
    private static final int[] J = {16842987};
    /* access modifiers changed from: private */
    public static final boolean K = (VERSION.SDK_INT >= 21);
    private static final boolean L = (VERSION.SDK_INT <= 15);
    private static final boolean M = (VERSION.SDK_INT <= 15);
    private static final Class<?>[] N = {Context.class, AttributeSet.class, Integer.TYPE, Integer.TYPE};

    /* renamed from: a reason: collision with root package name */
    static final boolean f1066a = (VERSION.SDK_INT == 18 || VERSION.SDK_INT == 19 || VERSION.SDK_INT == 20);

    /* renamed from: b reason: collision with root package name */
    static final boolean f1067b = (VERSION.SDK_INT >= 23);
    static final boolean c = (VERSION.SDK_INT >= 16);
    a A;
    final t B;
    boolean C;
    boolean D;
    boolean E;
    ba F;
    final List<w> G;
    private final q O;
    private r P;
    private final Rect Q;
    private final ArrayList<l> R;
    private l S;
    private int T;
    private boolean U;
    private int V;
    private final AccessibilityManager W;
    private android.support.v4.i.l aA;
    private final int[] aB;
    /* access modifiers changed from: private */
    public final int[] aC;
    private final int[] aD;
    private Runnable aE;
    private final b aF;
    private List<j> aa;
    private int ab;
    private int ac;
    private EdgeEffect ad;
    private EdgeEffect ae;
    private EdgeEffect af;
    private EdgeEffect ag;
    private int ah;
    private int ai;
    private VelocityTracker aj;
    private int ak;
    private int al;
    private int am;
    private int an;
    private int ao;
    private k ap;
    private final int aq;
    private final int ar;
    private float as;
    private float at;
    private boolean au;
    private m av;
    private List<m> aw;
    private b ax;
    private d ay;
    private final int[] az;
    final o d;
    f e;
    ai f;
    final bv g;
    boolean h;
    final Runnable i;
    final Rect j;
    final RectF k;
    a l;
    h m;
    p n;
    final ArrayList<g> o;
    boolean p;
    boolean q;
    boolean r;
    boolean s;
    boolean t;
    boolean u;
    boolean v;
    boolean w;
    e x;
    final v y;
    ar z;

    public static abstract class a<VH extends w> {

        /* renamed from: a reason: collision with root package name */
        private final b f1073a = new b();

        /* renamed from: b reason: collision with root package name */
        private boolean f1074b = false;

        public abstract int a();

        public int a(int i) {
            return 0;
        }

        public abstract VH a(ViewGroup viewGroup, int i);

        public void a(c cVar) {
            this.f1073a.registerObserver(cVar);
        }

        public void a(VH vh) {
        }

        public abstract void a(VH vh, int i);

        public void a(VH vh, int i, List<Object> list) {
            a(vh, i);
        }

        public void a(RecyclerView recyclerView) {
        }

        public long b(int i) {
            return -1;
        }

        public final VH b(ViewGroup viewGroup, int i) {
            android.support.v4.f.d.a("RV CreateView");
            VH a2 = a(viewGroup, i);
            a2.f = i;
            android.support.v4.f.d.a();
            return a2;
        }

        public void b(c cVar) {
            this.f1073a.unregisterObserver(cVar);
        }

        public final void b(VH vh, int i) {
            vh.c = i;
            if (b()) {
                vh.e = b(i);
            }
            vh.a(1, 519);
            android.support.v4.f.d.a("RV OnBindView");
            a(vh, i, vh.u());
            vh.t();
            LayoutParams layoutParams = vh.f1102a.getLayoutParams();
            if (layoutParams instanceof i) {
                ((i) layoutParams).e = true;
            }
            android.support.v4.f.d.a();
        }

        public void b(RecyclerView recyclerView) {
        }

        public final boolean b() {
            return this.f1074b;
        }

        public boolean b(VH vh) {
            return false;
        }

        public final void c() {
            this.f1073a.a();
        }

        public final void c(int i) {
            this.f1073a.a(i, 1);
        }

        public void c(VH vh) {
        }

        public final void d(int i) {
            this.f1073a.b(i, 1);
        }

        public void d(VH vh) {
        }
    }

    static class b extends Observable<c> {
        b() {
        }

        public void a() {
            for (int size = this.mObservers.size() - 1; size >= 0; size--) {
                ((c) this.mObservers.get(size)).a();
            }
        }

        public void a(int i, int i2) {
            a(i, i2, null);
        }

        public void a(int i, int i2, Object obj) {
            for (int size = this.mObservers.size() - 1; size >= 0; size--) {
                ((c) this.mObservers.get(size)).a(i, i2, obj);
            }
        }

        public void b(int i, int i2) {
            for (int size = this.mObservers.size() - 1; size >= 0; size--) {
                ((c) this.mObservers.get(size)).b(i, i2);
            }
        }
    }

    public static abstract class c {
        public void a() {
        }

        public void a(int i, int i2) {
        }

        public void a(int i, int i2, Object obj) {
            a(i, i2);
        }

        public void b(int i, int i2) {
        }
    }

    public interface d {
        int a(int i, int i2);
    }

    public static abstract class e {

        /* renamed from: a reason: collision with root package name */
        private b f1075a = null;

        /* renamed from: b reason: collision with root package name */
        private ArrayList<a> f1076b = new ArrayList<>();
        private long c = 120;
        private long d = 120;
        private long e = 250;
        private long f = 250;

        public interface a {
            void a();
        }

        interface b {
            void a(w wVar);
        }

        public static class c {

            /* renamed from: a reason: collision with root package name */
            public int f1077a;

            /* renamed from: b reason: collision with root package name */
            public int f1078b;
            public int c;
            public int d;

            public c a(w wVar) {
                return a(wVar, 0);
            }

            public c a(w wVar, int i) {
                View view = wVar.f1102a;
                this.f1077a = view.getLeft();
                this.f1078b = view.getTop();
                this.c = view.getRight();
                this.d = view.getBottom();
                return this;
            }
        }

        static int e(w wVar) {
            int d2 = wVar.n & 14;
            if (wVar.n()) {
                return 4;
            }
            if ((d2 & 4) != 0) {
                return d2;
            }
            int f2 = wVar.f();
            int e2 = wVar.e();
            return (f2 == -1 || e2 == -1 || f2 == e2) ? d2 : d2 | 2048;
        }

        public c a(t tVar, w wVar) {
            return j().a(wVar);
        }

        public c a(t tVar, w wVar, int i, List<Object> list) {
            return j().a(wVar);
        }

        public abstract void a();

        /* access modifiers changed from: 0000 */
        public void a(b bVar) {
            this.f1075a = bVar;
        }

        public final boolean a(a aVar) {
            boolean b2 = b();
            if (aVar != null) {
                if (!b2) {
                    aVar.a();
                } else {
                    this.f1076b.add(aVar);
                }
            }
            return b2;
        }

        public abstract boolean a(w wVar, c cVar, c cVar2);

        public abstract boolean a(w wVar, w wVar2, c cVar, c cVar2);

        public boolean a(w wVar, List<Object> list) {
            return h(wVar);
        }

        public abstract boolean b();

        public abstract boolean b(w wVar, c cVar, c cVar2);

        public abstract boolean c(w wVar, c cVar, c cVar2);

        public abstract void d();

        public abstract void d(w wVar);

        public long e() {
            return this.e;
        }

        public long f() {
            return this.c;
        }

        public final void f(w wVar) {
            g(wVar);
            if (this.f1075a != null) {
                this.f1075a.a(wVar);
            }
        }

        public long g() {
            return this.d;
        }

        public void g(w wVar) {
        }

        public long h() {
            return this.f;
        }

        public boolean h(w wVar) {
            return true;
        }

        public final void i() {
            int size = this.f1076b.size();
            for (int i = 0; i < size; i++) {
                ((a) this.f1076b.get(i)).a();
            }
            this.f1076b.clear();
        }

        public c j() {
            return new c();
        }
    }

    private class f implements b {
        f() {
        }

        public void a(w wVar) {
            wVar.a(true);
            if (wVar.h != null && wVar.i == null) {
                wVar.h = null;
            }
            wVar.i = null;
            if (!wVar.z() && !RecyclerView.this.a(wVar.f1102a) && wVar.r()) {
                RecyclerView.this.removeDetachedView(wVar.f1102a, false);
            }
        }
    }

    public static abstract class g {
        @Deprecated
        public void a(Canvas canvas, RecyclerView recyclerView) {
        }

        public void a(Canvas canvas, RecyclerView recyclerView, t tVar) {
            b(canvas, recyclerView);
        }

        @Deprecated
        public void a(Rect rect, int i, RecyclerView recyclerView) {
            rect.set(0, 0, 0, 0);
        }

        public void a(Rect rect, View view, RecyclerView recyclerView, t tVar) {
            a(rect, ((i) view.getLayoutParams()).f(), recyclerView);
        }

        @Deprecated
        public void b(Canvas canvas, RecyclerView recyclerView) {
        }

        public void b(Canvas canvas, RecyclerView recyclerView, t tVar) {
            a(canvas, recyclerView);
        }
    }

    public static abstract class h {

        /* renamed from: a reason: collision with root package name */
        private final b f1080a = new b() {
            public int a() {
                return h.this.z();
            }

            public int a(View view) {
                return h.this.h(view) - ((i) view.getLayoutParams()).leftMargin;
            }

            public View a(int i) {
                return h.this.h(i);
            }

            public int b() {
                return h.this.x() - h.this.B();
            }

            public int b(View view) {
                i iVar = (i) view.getLayoutParams();
                return iVar.rightMargin + h.this.j(view);
            }
        };

        /* renamed from: b reason: collision with root package name */
        private final b f1081b = new b() {
            public int a() {
                return h.this.A();
            }

            public int a(View view) {
                return h.this.i(view) - ((i) view.getLayoutParams()).topMargin;
            }

            public View a(int i) {
                return h.this.h(i);
            }

            public int b() {
                return h.this.y() - h.this.C();
            }

            public int b(View view) {
                i iVar = (i) view.getLayoutParams();
                return iVar.bottomMargin + h.this.k(view);
            }
        };
        private boolean c = true;
        private boolean d = true;
        private int e;
        private int f;
        private int g;
        private int h;
        ai p;
        RecyclerView q;
        bu r = new bu(this.f1080a);
        bu s = new bu(this.f1081b);
        s t;
        boolean u = false;
        boolean v = false;
        boolean w = false;
        int x;
        boolean y;

        public interface a {
            void b(int i, int i2);
        }

        public static class b {

            /* renamed from: a reason: collision with root package name */
            public int f1084a;

            /* renamed from: b reason: collision with root package name */
            public int f1085b;
            public boolean c;
            public boolean d;
        }

        public static int a(int i, int i2, int i3) {
            int mode = MeasureSpec.getMode(i);
            int size = MeasureSpec.getSize(i);
            switch (mode) {
                case Integer.MIN_VALUE:
                    return Math.min(size, Math.max(i2, i3));
                case 1073741824:
                    return size;
                default:
                    return Math.max(i2, i3);
            }
        }

        public static int a(int i, int i2, int i3, int i4, boolean z) {
            int i5 = 0;
            int max = Math.max(0, i - i3);
            if (z) {
                if (i4 >= 0) {
                    i5 = 1073741824;
                    max = i4;
                } else if (i4 == -1) {
                    switch (i2) {
                        case Integer.MIN_VALUE:
                        case 1073741824:
                            i5 = max;
                            break;
                        case 0:
                            i2 = 0;
                            break;
                        default:
                            i2 = 0;
                            break;
                    }
                    max = i5;
                    i5 = i2;
                } else {
                    if (i4 == -2) {
                        max = 0;
                    }
                    max = 0;
                }
            } else if (i4 >= 0) {
                i5 = 1073741824;
                max = i4;
            } else if (i4 == -1) {
                i5 = i2;
            } else {
                if (i4 == -2) {
                    if (i2 == Integer.MIN_VALUE || i2 == 1073741824) {
                        i5 = Integer.MIN_VALUE;
                    }
                }
                max = 0;
            }
            return MeasureSpec.makeMeasureSpec(max, i5);
        }

        public static b a(Context context, AttributeSet attributeSet, int i, int i2) {
            b bVar = new b();
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, android.support.v7.e.a.c.RecyclerView, i, i2);
            bVar.f1084a = obtainStyledAttributes.getInt(android.support.v7.e.a.c.RecyclerView_android_orientation, 1);
            bVar.f1085b = obtainStyledAttributes.getInt(android.support.v7.e.a.c.RecyclerView_spanCount, 1);
            bVar.c = obtainStyledAttributes.getBoolean(android.support.v7.e.a.c.RecyclerView_reverseLayout, false);
            bVar.d = obtainStyledAttributes.getBoolean(android.support.v7.e.a.c.RecyclerView_stackFromEnd, false);
            obtainStyledAttributes.recycle();
            return bVar;
        }

        private void a(int i, View view) {
            this.p.e(i);
        }

        private void a(o oVar, int i, View view) {
            w e2 = RecyclerView.e(view);
            if (!e2.c()) {
                if (!e2.n() || e2.q() || this.q.l.b()) {
                    g(i);
                    oVar.c(view);
                    this.q.g.h(e2);
                    return;
                }
                f(i);
                oVar.b(e2);
            }
        }

        /* access modifiers changed from: private */
        public void a(s sVar) {
            if (this.t == sVar) {
                this.t = null;
            }
        }

        private void a(View view, int i, boolean z) {
            w e2 = RecyclerView.e(view);
            if (z || e2.q()) {
                this.q.g.e(e2);
            } else {
                this.q.g.f(e2);
            }
            i iVar = (i) view.getLayoutParams();
            if (e2.k() || e2.i()) {
                if (e2.i()) {
                    e2.j();
                } else {
                    e2.l();
                }
                this.p.a(view, i, view.getLayoutParams(), false);
            } else if (view.getParent() == this.q) {
                int b2 = this.p.b(view);
                if (i == -1) {
                    i = this.p.b();
                }
                if (b2 == -1) {
                    throw new IllegalStateException("Added View has RecyclerView as parent but view is not a real child. Unfiltered index:" + this.q.indexOfChild(view) + this.q.a());
                } else if (b2 != i) {
                    this.q.m.f(b2, i);
                }
            } else {
                this.p.a(view, i, false);
                iVar.e = true;
                if (this.t != null && this.t.c()) {
                    this.t.b(view);
                }
            }
            if (iVar.f) {
                e2.f1102a.invalidate();
                iVar.f = false;
            }
        }

        private static boolean b(int i, int i2, int i3) {
            int mode = MeasureSpec.getMode(i2);
            int size = MeasureSpec.getSize(i2);
            if (i3 > 0 && i != i3) {
                return false;
            }
            switch (mode) {
                case Integer.MIN_VALUE:
                    return size >= i;
                case 0:
                    return true;
                case 1073741824:
                    return size == i;
                default:
                    return false;
            }
        }

        private int[] b(RecyclerView recyclerView, View view, Rect rect, boolean z) {
            int min;
            int[] iArr = new int[2];
            int z2 = z();
            int A = A();
            int x2 = x() - B();
            int y2 = y() - C();
            int left = (view.getLeft() + rect.left) - view.getScrollX();
            int top = (view.getTop() + rect.top) - view.getScrollY();
            int width = left + rect.width();
            int height = top + rect.height();
            int min2 = Math.min(0, left - z2);
            int min3 = Math.min(0, top - A);
            int max = Math.max(0, width - x2);
            int max2 = Math.max(0, height - y2);
            if (s() == 1) {
                if (max == 0) {
                    max = Math.max(min2, width - x2);
                }
                min = max;
            } else {
                min = min2 != 0 ? min2 : Math.min(left - z2, max);
            }
            int min4 = min3 != 0 ? min3 : Math.min(top - A, max2);
            iArr[0] = min;
            iArr[1] = min4;
            return iArr;
        }

        private boolean d(RecyclerView recyclerView, int i, int i2) {
            View focusedChild = recyclerView.getFocusedChild();
            if (focusedChild == null) {
                return false;
            }
            int z = z();
            int A = A();
            int x2 = x() - B();
            int y2 = y() - C();
            Rect rect = this.q.j;
            a(focusedChild, rect);
            return rect.left - i < x2 && rect.right - i > z && rect.top - i2 < y2 && rect.bottom - i2 > A;
        }

        public int A() {
            if (this.q != null) {
                return this.q.getPaddingTop();
            }
            return 0;
        }

        public int B() {
            if (this.q != null) {
                return this.q.getPaddingRight();
            }
            return 0;
        }

        public int C() {
            if (this.q != null) {
                return this.q.getPaddingBottom();
            }
            return 0;
        }

        public View D() {
            if (this.q == null) {
                return null;
            }
            View focusedChild = this.q.getFocusedChild();
            if (focusedChild == null || this.p.c(focusedChild)) {
                return null;
            }
            return focusedChild;
        }

        public int E() {
            return android.support.v4.i.t.h(this.q);
        }

        public int F() {
            return android.support.v4.i.t.i(this.q);
        }

        /* access modifiers changed from: 0000 */
        public void G() {
            if (this.t != null) {
                this.t.a();
            }
        }

        public void H() {
            this.u = true;
        }

        /* access modifiers changed from: 0000 */
        public boolean I() {
            int u2 = u();
            for (int i = 0; i < u2; i++) {
                LayoutParams layoutParams = h(i).getLayoutParams();
                if (layoutParams.width < 0 && layoutParams.height < 0) {
                    return true;
                }
            }
            return false;
        }

        public int a(int i, o oVar, t tVar) {
            return 0;
        }

        public int a(o oVar, t tVar) {
            if (this.q == null || this.q.l == null || !e()) {
                return 1;
            }
            return this.q.l.a();
        }

        public abstract i a();

        public i a(Context context, AttributeSet attributeSet) {
            return new i(context, attributeSet);
        }

        public i a(LayoutParams layoutParams) {
            return layoutParams instanceof i ? new i((i) layoutParams) : layoutParams instanceof MarginLayoutParams ? new i((MarginLayoutParams) layoutParams) : new i(layoutParams);
        }

        public View a(View view, int i, o oVar, t tVar) {
            return null;
        }

        public void a(int i, int i2, t tVar, a aVar) {
        }

        public void a(int i, a aVar) {
        }

        public void a(int i, o oVar) {
            View h2 = h(i);
            f(i);
            oVar.a(h2);
        }

        public void a(Rect rect, int i, int i2) {
            g(a(i, rect.width() + z() + B(), E()), a(i2, rect.height() + A() + C(), F()));
        }

        public void a(Parcelable parcelable) {
        }

        /* access modifiers changed from: 0000 */
        public void a(android.support.v4.i.a.b bVar) {
            a(this.q.d, this.q.B, bVar);
        }

        public void a(a aVar, a aVar2) {
        }

        public void a(o oVar) {
            for (int u2 = u() - 1; u2 >= 0; u2--) {
                a(oVar, u2, h(u2));
            }
        }

        public void a(o oVar, t tVar, int i, int i2) {
            this.q.e(i, i2);
        }

        public void a(o oVar, t tVar, android.support.v4.i.a.b bVar) {
            if (this.q.canScrollVertically(-1) || this.q.canScrollHorizontally(-1)) {
                bVar.a(8192);
                bVar.c(true);
            }
            if (this.q.canScrollVertically(1) || this.q.canScrollHorizontally(1)) {
                bVar.a(4096);
                bVar.c(true);
            }
            bVar.a((Object) android.support.v4.i.a.b.a.a(a(oVar, tVar), b(oVar, tVar), e(oVar, tVar), d(oVar, tVar)));
        }

        public void a(o oVar, t tVar, View view, android.support.v4.i.a.b bVar) {
            bVar.b((Object) C0017b.a(e() ? d(view) : 0, 1, d() ? d(view) : 0, 1, false, false));
        }

        public void a(o oVar, t tVar, AccessibilityEvent accessibilityEvent) {
            boolean z = true;
            if (this.q != null && accessibilityEvent != null) {
                if (!this.q.canScrollVertically(1) && !this.q.canScrollVertically(-1) && !this.q.canScrollHorizontally(-1) && !this.q.canScrollHorizontally(1)) {
                    z = false;
                }
                accessibilityEvent.setScrollable(z);
                if (this.q.l != null) {
                    accessibilityEvent.setItemCount(this.q.l.a());
                }
            }
        }

        public void a(t tVar) {
        }

        public void a(RecyclerView recyclerView) {
        }

        public void a(RecyclerView recyclerView, int i, int i2) {
        }

        public void a(RecyclerView recyclerView, int i, int i2, int i3) {
        }

        public void a(RecyclerView recyclerView, int i, int i2, Object obj) {
            c(recyclerView, i, i2);
        }

        public void a(RecyclerView recyclerView, o oVar) {
            e(recyclerView);
        }

        public void a(View view) {
            a(view, -1);
        }

        public void a(View view, int i) {
            a(view, i, true);
        }

        public void a(View view, int i, int i2) {
            i iVar = (i) view.getLayoutParams();
            Rect i3 = this.q.i(view);
            int i4 = i3.left + i3.right + i;
            int i5 = i3.bottom + i3.top + i2;
            int a2 = a(x(), v(), i4 + z() + B() + iVar.leftMargin + iVar.rightMargin, iVar.width, d());
            int a3 = a(y(), w(), i5 + A() + C() + iVar.topMargin + iVar.bottomMargin, iVar.height, e());
            if (b(view, a2, a3, iVar)) {
                view.measure(a2, a3);
            }
        }

        public void a(View view, int i, int i2, int i3, int i4) {
            i iVar = (i) view.getLayoutParams();
            Rect rect = iVar.d;
            view.layout(rect.left + i + iVar.leftMargin, rect.top + i2 + iVar.topMargin, (i3 - rect.right) - iVar.rightMargin, (i4 - rect.bottom) - iVar.bottomMargin);
        }

        public void a(View view, int i, i iVar) {
            w e2 = RecyclerView.e(view);
            if (e2.q()) {
                this.q.g.e(e2);
            } else {
                this.q.g.f(e2);
            }
            this.p.a(view, i, iVar, e2.q());
        }

        public void a(View view, Rect rect) {
            RecyclerView.a(view, rect);
        }

        /* access modifiers changed from: 0000 */
        public void a(View view, android.support.v4.i.a.b bVar) {
            w e2 = RecyclerView.e(view);
            if (e2 != null && !e2.q() && !this.p.c(e2.f1102a)) {
                a(this.q.d, this.q.B, view, bVar);
            }
        }

        public void a(View view, o oVar) {
            c(view);
            oVar.a(view);
        }

        public void a(View view, boolean z, Rect rect) {
            if (z) {
                Rect rect2 = ((i) view.getLayoutParams()).d;
                rect.set(-rect2.left, -rect2.top, view.getWidth() + rect2.right, rect2.bottom + view.getHeight());
            } else {
                rect.set(0, 0, view.getWidth(), view.getHeight());
            }
            if (this.q != null) {
                Matrix matrix = view.getMatrix();
                if (matrix != null && !matrix.isIdentity()) {
                    RectF rectF = this.q.k;
                    rectF.set(rect);
                    matrix.mapRect(rectF);
                    rect.set((int) Math.floor((double) rectF.left), (int) Math.floor((double) rectF.top), (int) Math.ceil((double) rectF.right), (int) Math.ceil((double) rectF.bottom));
                }
            }
            rect.offset(view.getLeft(), view.getTop());
        }

        public void a(AccessibilityEvent accessibilityEvent) {
            a(this.q.d, this.q.B, accessibilityEvent);
        }

        public void a(String str) {
            if (this.q != null) {
                this.q.a(str);
            }
        }

        /* access modifiers changed from: 0000 */
        public boolean a(int i, Bundle bundle) {
            return a(this.q.d, this.q.B, i, bundle);
        }

        public boolean a(i iVar) {
            return iVar != null;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:19:0x007a, code lost:
            r3 = r0;
            r0 = 0;
         */
        public boolean a(o oVar, t tVar, int i, Bundle bundle) {
            int i2;
            int i3;
            int i4;
            if (this.q == null) {
                return false;
            }
            switch (i) {
                case 4096:
                    i2 = this.q.canScrollVertically(1) ? (y() - A()) - C() : 0;
                    if (this.q.canScrollHorizontally(1)) {
                        i3 = i2;
                        i4 = (x() - z()) - B();
                        break;
                    }
                case 8192:
                    i2 = this.q.canScrollVertically(-1) ? -((y() - A()) - C()) : 0;
                    if (this.q.canScrollHorizontally(-1)) {
                        i3 = i2;
                        i4 = -((x() - z()) - B());
                        break;
                    }
                default:
                    i4 = 0;
                    i3 = 0;
                    break;
            }
            if (i3 == 0 && i4 == 0) {
                return false;
            }
            this.q.scrollBy(i4, i3);
            return true;
        }

        public boolean a(o oVar, t tVar, View view, int i, Bundle bundle) {
            return false;
        }

        public boolean a(RecyclerView recyclerView, t tVar, View view, View view2) {
            return a(recyclerView, view, view2);
        }

        public boolean a(RecyclerView recyclerView, View view, Rect rect, boolean z) {
            return a(recyclerView, view, rect, z, false);
        }

        public boolean a(RecyclerView recyclerView, View view, Rect rect, boolean z, boolean z2) {
            int[] b2 = b(recyclerView, view, rect, z);
            int i = b2[0];
            int i2 = b2[1];
            if (z2 && !d(recyclerView, i, i2)) {
                return false;
            }
            if (i == 0 && i2 == 0) {
                return false;
            }
            if (z) {
                recyclerView.scrollBy(i, i2);
            } else {
                recyclerView.a(i, i2);
            }
            return true;
        }

        @Deprecated
        public boolean a(RecyclerView recyclerView, View view, View view2) {
            return r() || recyclerView.o();
        }

        public boolean a(RecyclerView recyclerView, ArrayList<View> arrayList, int i, int i2) {
            return false;
        }

        /* access modifiers changed from: 0000 */
        public boolean a(View view, int i, int i2, i iVar) {
            return !this.c || !b(view.getMeasuredWidth(), i, iVar.width) || !b(view.getMeasuredHeight(), i2, iVar.height);
        }

        /* access modifiers changed from: 0000 */
        public boolean a(View view, int i, Bundle bundle) {
            return a(this.q.d, this.q.B, view, i, bundle);
        }

        public boolean a(View view, boolean z, boolean z2) {
            boolean z3 = this.r.a(view, 24579) && this.s.a(view, 24579);
            return z ? z3 : !z3;
        }

        public boolean a(Runnable runnable) {
            if (this.q != null) {
                return this.q.removeCallbacks(runnable);
            }
            return false;
        }

        public int b(int i, o oVar, t tVar) {
            return 0;
        }

        public int b(o oVar, t tVar) {
            if (this.q == null || this.q.l == null || !d()) {
                return 1;
            }
            return this.q.l.a();
        }

        /* access modifiers changed from: 0000 */
        public void b(o oVar) {
            int e2 = oVar.e();
            for (int i = e2 - 1; i >= 0; i--) {
                View e3 = oVar.e(i);
                w e4 = RecyclerView.e(e3);
                if (!e4.c()) {
                    e4.a(false);
                    if (e4.r()) {
                        this.q.removeDetachedView(e3, false);
                    }
                    if (this.q.x != null) {
                        this.q.x.d(e4);
                    }
                    e4.a(true);
                    oVar.b(e3);
                }
            }
            oVar.f();
            if (e2 > 0) {
                this.q.invalidate();
            }
        }

        /* access modifiers changed from: 0000 */
        public void b(RecyclerView recyclerView) {
            if (recyclerView == null) {
                this.q = null;
                this.p = null;
                this.g = 0;
                this.h = 0;
            } else {
                this.q = recyclerView;
                this.p = recyclerView.f;
                this.g = recyclerView.getWidth();
                this.h = recyclerView.getHeight();
            }
            this.e = 1073741824;
            this.f = 1073741824;
        }

        public void b(RecyclerView recyclerView, int i, int i2) {
        }

        /* access modifiers changed from: 0000 */
        public void b(RecyclerView recyclerView, o oVar) {
            this.v = false;
            a(recyclerView, oVar);
        }

        public void b(View view) {
            b(view, -1);
        }

        public void b(View view, int i) {
            a(view, i, false);
        }

        public void b(View view, Rect rect) {
            if (this.q == null) {
                rect.set(0, 0, 0, 0);
            } else {
                rect.set(this.q.i(view));
            }
        }

        public boolean b() {
            return false;
        }

        /* access modifiers changed from: 0000 */
        public boolean b(View view, int i, int i2, i iVar) {
            return view.isLayoutRequested() || !this.c || !b(view.getWidth(), i, iVar.width) || !b(view.getHeight(), i2, iVar.height);
        }

        public int c(t tVar) {
            return 0;
        }

        public Parcelable c() {
            return null;
        }

        public View c(int i) {
            int u2 = u();
            for (int i2 = 0; i2 < u2; i2++) {
                View h2 = h(i2);
                w e2 = RecyclerView.e(h2);
                if (e2 != null && e2.d() == i && !e2.c() && (this.q.B.a() || !e2.q())) {
                    return h2;
                }
            }
            return null;
        }

        public void c(o oVar) {
            for (int u2 = u() - 1; u2 >= 0; u2--) {
                if (!RecyclerView.e(h(u2)).c()) {
                    a(u2, oVar);
                }
            }
        }

        public void c(o oVar, t tVar) {
            Log.e("RecyclerView", "You must override onLayoutChildren(Recycler recycler, State state) ");
        }

        /* access modifiers changed from: 0000 */
        public void c(RecyclerView recyclerView) {
            this.v = true;
            d(recyclerView);
        }

        public void c(RecyclerView recyclerView, int i, int i2) {
        }

        public void c(View view) {
            this.p.a(view);
        }

        public void c(View view, int i) {
            a(view, i, (i) view.getLayoutParams());
        }

        public void c(boolean z) {
            this.w = z;
        }

        public int d(o oVar, t tVar) {
            return 0;
        }

        public int d(t tVar) {
            return 0;
        }

        public int d(View view) {
            return ((i) view.getLayoutParams()).f();
        }

        public View d(View view, int i) {
            return null;
        }

        public void d(int i) {
        }

        /* access modifiers changed from: 0000 */
        public void d(int i, int i2) {
            this.g = MeasureSpec.getSize(i);
            this.e = MeasureSpec.getMode(i);
            if (this.e == 0 && !RecyclerView.f1067b) {
                this.g = 0;
            }
            this.h = MeasureSpec.getSize(i2);
            this.f = MeasureSpec.getMode(i2);
            if (this.f == 0 && !RecyclerView.f1067b) {
                this.h = 0;
            }
        }

        public void d(RecyclerView recyclerView) {
        }

        public boolean d() {
            return false;
        }

        public int e(t tVar) {
            return 0;
        }

        public View e(View view) {
            if (this.q == null) {
                return null;
            }
            View c2 = this.q.c(view);
            if (c2 == null || this.p.c(c2)) {
                return null;
            }
            return c2;
        }

        /* access modifiers changed from: 0000 */
        public void e(int i, int i2) {
            int i3 = Integer.MAX_VALUE;
            int i4 = Integer.MIN_VALUE;
            int u2 = u();
            if (u2 == 0) {
                this.q.e(i, i2);
                return;
            }
            int i5 = Integer.MIN_VALUE;
            int i6 = Integer.MAX_VALUE;
            for (int i7 = 0; i7 < u2; i7++) {
                View h2 = h(i7);
                Rect rect = this.q.j;
                a(h2, rect);
                if (rect.left < i6) {
                    i6 = rect.left;
                }
                if (rect.right > i5) {
                    i5 = rect.right;
                }
                if (rect.top < i3) {
                    i3 = rect.top;
                }
                if (rect.bottom > i4) {
                    i4 = rect.bottom;
                }
            }
            this.q.j.set(i6, i3, i5, i4);
            a(this.q.j, i, i2);
        }

        @Deprecated
        public void e(RecyclerView recyclerView) {
        }

        public boolean e() {
            return false;
        }

        public boolean e(o oVar, t tVar) {
            return false;
        }

        public int f(t tVar) {
            return 0;
        }

        public int f(View view) {
            Rect rect = ((i) view.getLayoutParams()).d;
            return rect.right + view.getMeasuredWidth() + rect.left;
        }

        public void f(int i) {
            if (h(i) != null) {
                this.p.a(i);
            }
        }

        public void f(int i, int i2) {
            View h2 = h(i);
            if (h2 == null) {
                throw new IllegalArgumentException("Cannot move a child from non-existing index:" + i + this.q.toString());
            }
            g(i);
            c(h2, i2);
        }

        /* access modifiers changed from: 0000 */
        public void f(RecyclerView recyclerView) {
            d(MeasureSpec.makeMeasureSpec(recyclerView.getWidth(), 1073741824), MeasureSpec.makeMeasureSpec(recyclerView.getHeight(), 1073741824));
        }

        public int g(t tVar) {
            return 0;
        }

        public int g(View view) {
            Rect rect = ((i) view.getLayoutParams()).d;
            return rect.bottom + view.getMeasuredHeight() + rect.top;
        }

        public void g(int i) {
            a(i, h(i));
        }

        public void g(int i, int i2) {
            this.q.setMeasuredDimension(i, i2);
        }

        public int h(t tVar) {
            return 0;
        }

        public int h(View view) {
            return view.getLeft() - n(view);
        }

        public View h(int i) {
            if (this.p != null) {
                return this.p.b(i);
            }
            return null;
        }

        public int i(View view) {
            return view.getTop() - l(view);
        }

        public void i(int i) {
            if (this.q != null) {
                this.q.e(i);
            }
        }

        public int j(View view) {
            return view.getRight() + o(view);
        }

        public void j(int i) {
            if (this.q != null) {
                this.q.d(i);
            }
        }

        public int k(View view) {
            return view.getBottom() + m(view);
        }

        public void k(int i) {
        }

        /* access modifiers changed from: 0000 */
        public boolean k() {
            return false;
        }

        public int l(View view) {
            return ((i) view.getLayoutParams()).d.top;
        }

        public int m(View view) {
            return ((i) view.getLayoutParams()).d.bottom;
        }

        public int n(View view) {
            return ((i) view.getLayoutParams()).d.left;
        }

        public void n() {
            if (this.q != null) {
                this.q.requestLayout();
            }
        }

        public int o(View view) {
            return ((i) view.getLayoutParams()).d.right;
        }

        public final boolean o() {
            return this.d;
        }

        public boolean p() {
            return this.v;
        }

        public boolean q() {
            return this.q != null && this.q.h;
        }

        public boolean r() {
            return this.t != null && this.t.c();
        }

        public int s() {
            return android.support.v4.i.t.e(this.q);
        }

        public int t() {
            return -1;
        }

        public int u() {
            if (this.p != null) {
                return this.p.b();
            }
            return 0;
        }

        public int v() {
            return this.e;
        }

        public int w() {
            return this.f;
        }

        public int x() {
            return this.g;
        }

        public int y() {
            return this.h;
        }

        public int z() {
            if (this.q != null) {
                return this.q.getPaddingLeft();
            }
            return 0;
        }
    }

    public static class i extends MarginLayoutParams {
        w c;
        final Rect d = new Rect();
        boolean e = true;
        boolean f = false;

        public i(int i, int i2) {
            super(i, i2);
        }

        public i(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }

        public i(i iVar) {
            super(iVar);
        }

        public i(LayoutParams layoutParams) {
            super(layoutParams);
        }

        public i(MarginLayoutParams marginLayoutParams) {
            super(marginLayoutParams);
        }

        public boolean c() {
            return this.c.n();
        }

        public boolean d() {
            return this.c.q();
        }

        public boolean e() {
            return this.c.x();
        }

        public int f() {
            return this.c.d();
        }
    }

    public interface j {
        void a(View view);

        void b(View view);
    }

    public static abstract class k {
        public abstract boolean a(int i, int i2);
    }

    public interface l {
        void a(boolean z);

        boolean a(RecyclerView recyclerView, MotionEvent motionEvent);

        void b(RecyclerView recyclerView, MotionEvent motionEvent);
    }

    public static abstract class m {
        public void a(RecyclerView recyclerView, int i) {
        }

        public void a(RecyclerView recyclerView, int i, int i2) {
        }
    }

    public static class n {

        /* renamed from: a reason: collision with root package name */
        SparseArray<a> f1086a = new SparseArray<>();

        /* renamed from: b reason: collision with root package name */
        private int f1087b = 0;

        static class a {

            /* renamed from: a reason: collision with root package name */
            ArrayList<w> f1088a = new ArrayList<>();

            /* renamed from: b reason: collision with root package name */
            int f1089b = 5;
            long c = 0;
            long d = 0;

            a() {
            }
        }

        private a b(int i) {
            a aVar = (a) this.f1086a.get(i);
            if (aVar != null) {
                return aVar;
            }
            a aVar2 = new a();
            this.f1086a.put(i, aVar2);
            return aVar2;
        }

        /* access modifiers changed from: 0000 */
        public long a(long j, long j2) {
            return j == 0 ? j2 : ((j / 4) * 3) + (j2 / 4);
        }

        public w a(int i) {
            a aVar = (a) this.f1086a.get(i);
            if (aVar == null || aVar.f1088a.isEmpty()) {
                return null;
            }
            ArrayList<w> arrayList = aVar.f1088a;
            return (w) arrayList.remove(arrayList.size() - 1);
        }

        public void a() {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < this.f1086a.size()) {
                    ((a) this.f1086a.valueAt(i2)).f1088a.clear();
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }

        /* access modifiers changed from: 0000 */
        public void a(int i, long j) {
            a b2 = b(i);
            b2.c = a(b2.c, j);
        }

        /* access modifiers changed from: 0000 */
        public void a(a aVar) {
            this.f1087b++;
        }

        /* access modifiers changed from: 0000 */
        public void a(a aVar, a aVar2, boolean z) {
            if (aVar != null) {
                b();
            }
            if (!z && this.f1087b == 0) {
                a();
            }
            if (aVar2 != null) {
                a(aVar2);
            }
        }

        public void a(w wVar) {
            int h = wVar.h();
            ArrayList<w> arrayList = b(h).f1088a;
            if (((a) this.f1086a.get(h)).f1089b > arrayList.size()) {
                wVar.v();
                arrayList.add(wVar);
            }
        }

        /* access modifiers changed from: 0000 */
        public boolean a(int i, long j, long j2) {
            long j3 = b(i).c;
            return j3 == 0 || j3 + j < j2;
        }

        /* access modifiers changed from: 0000 */
        public void b() {
            this.f1087b--;
        }

        /* access modifiers changed from: 0000 */
        public void b(int i, long j) {
            a b2 = b(i);
            b2.d = a(b2.d, j);
        }

        /* access modifiers changed from: 0000 */
        public boolean b(int i, long j, long j2) {
            long j3 = b(i).d;
            return j3 == 0 || j3 + j < j2;
        }
    }

    public final class o {

        /* renamed from: a reason: collision with root package name */
        final ArrayList<w> f1090a = new ArrayList<>();

        /* renamed from: b reason: collision with root package name */
        ArrayList<w> f1091b = null;
        final ArrayList<w> c = new ArrayList<>();
        int d = 2;
        n e;
        private final List<w> g = Collections.unmodifiableList(this.f1090a);
        private int h = 2;
        private u i;

        public o() {
        }

        private void a(ViewGroup viewGroup, boolean z) {
            for (int childCount = viewGroup.getChildCount() - 1; childCount >= 0; childCount--) {
                View childAt = viewGroup.getChildAt(childCount);
                if (childAt instanceof ViewGroup) {
                    a((ViewGroup) childAt, true);
                }
            }
            if (z) {
                if (viewGroup.getVisibility() == 4) {
                    viewGroup.setVisibility(0);
                    viewGroup.setVisibility(4);
                    return;
                }
                int visibility = viewGroup.getVisibility();
                viewGroup.setVisibility(4);
                viewGroup.setVisibility(visibility);
            }
        }

        private boolean a(w wVar, int i2, int i3, long j) {
            wVar.m = RecyclerView.this;
            int h2 = wVar.h();
            long nanoTime = RecyclerView.this.getNanoTime();
            if (j != Long.MAX_VALUE && !this.e.b(h2, nanoTime, j)) {
                return false;
            }
            RecyclerView.this.l.b(wVar, i2);
            this.e.b(wVar.h(), RecyclerView.this.getNanoTime() - nanoTime);
            e(wVar);
            if (RecyclerView.this.B.a()) {
                wVar.g = i3;
            }
            return true;
        }

        private void e(w wVar) {
            if (RecyclerView.this.n()) {
                View view = wVar.f1102a;
                if (android.support.v4.i.t.d(view) == 0) {
                    android.support.v4.i.t.a(view, 1);
                }
                if (!android.support.v4.i.t.a(view)) {
                    wVar.b(16384);
                    android.support.v4.i.t.a(view, RecyclerView.this.F.c());
                }
            }
        }

        private void f(w wVar) {
            if (wVar.f1102a instanceof ViewGroup) {
                a((ViewGroup) wVar.f1102a, false);
            }
        }

        /* access modifiers changed from: 0000 */
        /* JADX WARNING: Removed duplicated region for block: B:45:0x0126  */
        /* JADX WARNING: Removed duplicated region for block: B:54:0x0174  */
        /* JADX WARNING: Removed duplicated region for block: B:60:0x018a  */
        public w a(int i2, boolean z, long j) {
            w wVar;
            boolean z2;
            w wVar2;
            boolean z3;
            boolean a2;
            i iVar;
            boolean z4;
            View a3;
            boolean z5 = true;
            if (i2 < 0 || i2 >= RecyclerView.this.B.e()) {
                throw new IndexOutOfBoundsException("Invalid item position " + i2 + "(" + i2 + "). Item count:" + RecyclerView.this.B.e() + RecyclerView.this.a());
            }
            if (RecyclerView.this.B.a()) {
                w f2 = f(i2);
                z2 = f2 != null;
                wVar = f2;
            } else {
                wVar = null;
                z2 = false;
            }
            if (wVar == null) {
                wVar = b(i2, z);
                if (wVar != null) {
                    if (!a(wVar)) {
                        if (!z) {
                            wVar.b(4);
                            if (wVar.i()) {
                                RecyclerView.this.removeDetachedView(wVar.f1102a, false);
                                wVar.j();
                            } else if (wVar.k()) {
                                wVar.l();
                            }
                            b(wVar);
                        }
                        wVar = null;
                    } else {
                        z2 = true;
                    }
                }
            }
            if (wVar == null) {
                int b2 = RecyclerView.this.e.b(i2);
                if (b2 < 0 || b2 >= RecyclerView.this.l.a()) {
                    throw new IndexOutOfBoundsException("Inconsistency detected. Invalid item position " + i2 + "(offset:" + b2 + ")." + "state:" + RecyclerView.this.B.e() + RecyclerView.this.a());
                }
                int a4 = RecyclerView.this.l.a(b2);
                if (RecyclerView.this.l.b()) {
                    wVar = a(RecyclerView.this.l.b(b2), a4, z);
                    if (wVar != null) {
                        wVar.c = b2;
                        z4 = true;
                        if (wVar == null && this.i != null) {
                            a3 = this.i.a(this, i2, a4);
                            if (a3 != null) {
                                wVar = RecyclerView.this.b(a3);
                                if (wVar == null) {
                                    throw new IllegalArgumentException("getViewForPositionAndType returned a view which does not have a ViewHolder" + RecyclerView.this.a());
                                } else if (wVar.c()) {
                                    throw new IllegalArgumentException("getViewForPositionAndType returned a view that is ignored. You must call stopIgnoring before returning this view." + RecyclerView.this.a());
                                }
                            }
                        }
                        if (wVar == null) {
                            wVar = g().a(a4);
                            if (wVar != null) {
                                wVar.v();
                                if (RecyclerView.f1066a) {
                                    f(wVar);
                                }
                            }
                        }
                        if (wVar == null) {
                            long nanoTime = RecyclerView.this.getNanoTime();
                            if (j != Long.MAX_VALUE && !this.e.a(a4, nanoTime, j)) {
                                return null;
                            }
                            wVar = RecyclerView.this.l.b((ViewGroup) RecyclerView.this, a4);
                            if (RecyclerView.K) {
                                RecyclerView j2 = RecyclerView.j(wVar.f1102a);
                                if (j2 != null) {
                                    wVar.f1103b = new WeakReference<>(j2);
                                }
                            }
                            this.e.a(a4, RecyclerView.this.getNanoTime() - nanoTime);
                        }
                        wVar2 = wVar;
                        z3 = z4;
                    }
                }
                z4 = z2;
                a3 = this.i.a(this, i2, a4);
                if (a3 != null) {
                }
                if (wVar == null) {
                }
                if (wVar == null) {
                }
                wVar2 = wVar;
                z3 = z4;
            } else {
                wVar2 = wVar;
                z3 = z2;
            }
            if (z3 && !RecyclerView.this.B.a() && wVar2.a(8192)) {
                wVar2.a(0, 8192);
                if (RecyclerView.this.B.i) {
                    RecyclerView.this.a(wVar2, RecyclerView.this.x.a(RecyclerView.this.B, wVar2, e.e(wVar2) | 4096, wVar2.u()));
                }
            }
            if (!RecyclerView.this.B.a() || !wVar2.p()) {
                a2 = (!wVar2.p() || wVar2.o() || wVar2.n()) ? a(wVar2, RecyclerView.this.e.b(i2), i2, j) : false;
            } else {
                wVar2.g = i2;
                a2 = false;
            }
            LayoutParams layoutParams = wVar2.f1102a.getLayoutParams();
            if (layoutParams == null) {
                iVar = (i) RecyclerView.this.generateDefaultLayoutParams();
                wVar2.f1102a.setLayoutParams(iVar);
            } else if (!RecyclerView.this.checkLayoutParams(layoutParams)) {
                iVar = (i) RecyclerView.this.generateLayoutParams(layoutParams);
                wVar2.f1102a.setLayoutParams(iVar);
            } else {
                iVar = (i) layoutParams;
            }
            iVar.c = wVar2;
            if (!z3 || !a2) {
                z5 = false;
            }
            iVar.f = z5;
            return wVar2;
        }

        /* access modifiers changed from: 0000 */
        public w a(long j, int i2, boolean z) {
            for (int size = this.f1090a.size() - 1; size >= 0; size--) {
                w wVar = (w) this.f1090a.get(size);
                if (wVar.g() == j && !wVar.k()) {
                    if (i2 == wVar.h()) {
                        wVar.b(32);
                        if (!wVar.q() || RecyclerView.this.B.a()) {
                            return wVar;
                        }
                        wVar.a(2, 14);
                        return wVar;
                    } else if (!z) {
                        this.f1090a.remove(size);
                        RecyclerView.this.removeDetachedView(wVar.f1102a, false);
                        b(wVar.f1102a);
                    }
                }
            }
            for (int size2 = this.c.size() - 1; size2 >= 0; size2--) {
                w wVar2 = (w) this.c.get(size2);
                if (wVar2.g() == j) {
                    if (i2 == wVar2.h()) {
                        if (z) {
                            return wVar2;
                        }
                        this.c.remove(size2);
                        return wVar2;
                    } else if (!z) {
                        d(size2);
                        return null;
                    }
                }
            }
            return null;
        }

        /* access modifiers changed from: 0000 */
        public View a(int i2, boolean z) {
            return a(i2, z, Long.MAX_VALUE).f1102a;
        }

        public void a() {
            this.f1090a.clear();
            d();
        }

        public void a(int i2) {
            this.h = i2;
            b();
        }

        /* access modifiers changed from: 0000 */
        public void a(int i2, int i3) {
            int i4;
            int i5;
            int i6;
            if (i2 < i3) {
                i4 = -1;
                i5 = i3;
                i6 = i2;
            } else {
                i4 = 1;
                i5 = i2;
                i6 = i3;
            }
            int size = this.c.size();
            for (int i7 = 0; i7 < size; i7++) {
                w wVar = (w) this.c.get(i7);
                if (wVar != null && wVar.c >= i6 && wVar.c <= i5) {
                    if (wVar.c == i2) {
                        wVar.a(i3 - i2, false);
                    } else {
                        wVar.a(i4, false);
                    }
                }
            }
        }

        /* access modifiers changed from: 0000 */
        public void a(int i2, int i3, boolean z) {
            int i4 = i2 + i3;
            for (int size = this.c.size() - 1; size >= 0; size--) {
                w wVar = (w) this.c.get(size);
                if (wVar != null) {
                    if (wVar.c >= i4) {
                        wVar.a(-i3, z);
                    } else if (wVar.c >= i2) {
                        wVar.b(8);
                        d(size);
                    }
                }
            }
        }

        /* access modifiers changed from: 0000 */
        public void a(a aVar, a aVar2, boolean z) {
            a();
            g().a(aVar, aVar2, z);
        }

        /* access modifiers changed from: 0000 */
        public void a(n nVar) {
            if (this.e != null) {
                this.e.b();
            }
            this.e = nVar;
            if (nVar != null) {
                this.e.a(RecyclerView.this.getAdapter());
            }
        }

        /* access modifiers changed from: 0000 */
        public void a(u uVar) {
            this.i = uVar;
        }

        /* access modifiers changed from: 0000 */
        public void a(w wVar, boolean z) {
            RecyclerView.c(wVar);
            if (wVar.a(16384)) {
                wVar.a(0, 16384);
                android.support.v4.i.t.a(wVar.f1102a, (android.support.v4.i.b) null);
            }
            if (z) {
                d(wVar);
            }
            wVar.m = null;
            g().a(wVar);
        }

        public void a(View view) {
            w e2 = RecyclerView.e(view);
            if (e2.r()) {
                RecyclerView.this.removeDetachedView(view, false);
            }
            if (e2.i()) {
                e2.j();
            } else if (e2.k()) {
                e2.l();
            }
            b(e2);
        }

        /* access modifiers changed from: 0000 */
        public boolean a(w wVar) {
            if (wVar.q()) {
                return RecyclerView.this.B.a();
            }
            if (wVar.c < 0 || wVar.c >= RecyclerView.this.l.a()) {
                throw new IndexOutOfBoundsException("Inconsistency detected. Invalid view holder adapter position" + wVar + RecyclerView.this.a());
            } else if (RecyclerView.this.B.a() || RecyclerView.this.l.a(wVar.c) == wVar.h()) {
                return !RecyclerView.this.l.b() || wVar.g() == RecyclerView.this.l.b(wVar.c);
            } else {
                return false;
            }
        }

        public int b(int i2) {
            if (i2 >= 0 && i2 < RecyclerView.this.B.e()) {
                return !RecyclerView.this.B.a() ? i2 : RecyclerView.this.e.b(i2);
            }
            throw new IndexOutOfBoundsException("invalid position " + i2 + ". State " + "item count is " + RecyclerView.this.B.e() + RecyclerView.this.a());
        }

        /* access modifiers changed from: 0000 */
        public w b(int i2, boolean z) {
            int i3 = 0;
            int size = this.f1090a.size();
            int i4 = 0;
            while (i4 < size) {
                w wVar = (w) this.f1090a.get(i4);
                if (wVar.k() || wVar.d() != i2 || wVar.n() || (!RecyclerView.this.B.f && wVar.q())) {
                    i4++;
                } else {
                    wVar.b(32);
                    return wVar;
                }
            }
            if (!z) {
                View c2 = RecyclerView.this.f.c(i2);
                if (c2 != null) {
                    w e2 = RecyclerView.e(c2);
                    RecyclerView.this.f.e(c2);
                    int b2 = RecyclerView.this.f.b(c2);
                    if (b2 == -1) {
                        throw new IllegalStateException("layout index should not be -1 after unhiding a view:" + e2 + RecyclerView.this.a());
                    }
                    RecyclerView.this.f.e(b2);
                    c(c2);
                    e2.b(8224);
                    return e2;
                }
            }
            int size2 = this.c.size();
            while (i3 < size2) {
                w wVar2 = (w) this.c.get(i3);
                if (wVar2.n() || wVar2.d() != i2) {
                    i3++;
                } else if (z) {
                    return wVar2;
                } else {
                    this.c.remove(i3);
                    return wVar2;
                }
            }
            return null;
        }

        /* access modifiers changed from: 0000 */
        public void b() {
            this.d = (RecyclerView.this.m != null ? RecyclerView.this.m.x : 0) + this.h;
            for (int size = this.c.size() - 1; size >= 0 && this.c.size() > this.d; size--) {
                d(size);
            }
        }

        /* access modifiers changed from: 0000 */
        public void b(int i2, int i3) {
            int size = this.c.size();
            for (int i4 = 0; i4 < size; i4++) {
                w wVar = (w) this.c.get(i4);
                if (wVar != null && wVar.c >= i2) {
                    wVar.a(i3, true);
                }
            }
        }

        /* access modifiers changed from: 0000 */
        public void b(w wVar) {
            boolean z;
            boolean z2 = false;
            if (wVar.i() || wVar.f1102a.getParent() != null) {
                throw new IllegalArgumentException("Scrapped or attached views may not be recycled. isScrap:" + wVar.i() + " isAttached:" + (wVar.f1102a.getParent() != null) + RecyclerView.this.a());
            } else if (wVar.r()) {
                throw new IllegalArgumentException("Tmp detached view should be removed from RecyclerView before it can be recycled: " + wVar + RecyclerView.this.a());
            } else if (wVar.c()) {
                throw new IllegalArgumentException("Trying to recycle an ignored view holder. You should first call stopIgnoringView(view) before calling recycle." + RecyclerView.this.a());
            } else {
                boolean a2 = wVar.A();
                if ((RecyclerView.this.l != null && a2 && RecyclerView.this.l.b(wVar)) || wVar.w()) {
                    if (this.d <= 0 || wVar.a(526)) {
                        z = false;
                    } else {
                        int size = this.c.size();
                        if (size >= this.d && size > 0) {
                            d(0);
                            size--;
                        }
                        if (RecyclerView.K && size > 0 && !RecyclerView.this.A.a(wVar.c)) {
                            int i2 = size - 1;
                            while (i2 >= 0) {
                                if (!RecyclerView.this.A.a(((w) this.c.get(i2)).c)) {
                                    break;
                                }
                                i2--;
                            }
                            size = i2 + 1;
                        }
                        this.c.add(size, wVar);
                        z = true;
                    }
                    if (!z) {
                        a(wVar, true);
                        z2 = true;
                    }
                } else {
                    z = false;
                }
                RecyclerView.this.g.g(wVar);
                if (!z && !z2 && a2) {
                    wVar.m = null;
                }
            }
        }

        /* access modifiers changed from: 0000 */
        public void b(View view) {
            w e2 = RecyclerView.e(view);
            e2.q = null;
            e2.r = false;
            e2.l();
            b(e2);
        }

        public View c(int i2) {
            return a(i2, false);
        }

        public List<w> c() {
            return this.g;
        }

        /* access modifiers changed from: 0000 */
        public void c(int i2, int i3) {
            int i4 = i2 + i3;
            for (int size = this.c.size() - 1; size >= 0; size--) {
                w wVar = (w) this.c.get(size);
                if (wVar != null) {
                    int i5 = wVar.c;
                    if (i5 >= i2 && i5 < i4) {
                        wVar.b(2);
                        d(size);
                    }
                }
            }
        }

        /* access modifiers changed from: 0000 */
        public void c(w wVar) {
            if (wVar.r) {
                this.f1091b.remove(wVar);
            } else {
                this.f1090a.remove(wVar);
            }
            wVar.q = null;
            wVar.r = false;
            wVar.l();
        }

        /* access modifiers changed from: 0000 */
        public void c(View view) {
            w e2 = RecyclerView.e(view);
            if (!e2.a(12) && e2.x() && !RecyclerView.this.b(e2)) {
                if (this.f1091b == null) {
                    this.f1091b = new ArrayList<>();
                }
                e2.a(this, true);
                this.f1091b.add(e2);
            } else if (!e2.n() || e2.q() || RecyclerView.this.l.b()) {
                e2.a(this, false);
                this.f1090a.add(e2);
            } else {
                throw new IllegalArgumentException("Called scrap view with an invalid view. Invalid views cannot be reused from scrap, they should rebound from recycler pool." + RecyclerView.this.a());
            }
        }

        /* access modifiers changed from: 0000 */
        public void d() {
            for (int size = this.c.size() - 1; size >= 0; size--) {
                d(size);
            }
            this.c.clear();
            if (RecyclerView.K) {
                RecyclerView.this.A.a();
            }
        }

        /* access modifiers changed from: 0000 */
        public void d(int i2) {
            a((w) this.c.get(i2), true);
            this.c.remove(i2);
        }

        /* access modifiers changed from: 0000 */
        public void d(w wVar) {
            if (RecyclerView.this.n != null) {
                RecyclerView.this.n.a(wVar);
            }
            if (RecyclerView.this.l != null) {
                RecyclerView.this.l.a(wVar);
            }
            if (RecyclerView.this.B != null) {
                RecyclerView.this.g.g(wVar);
            }
        }

        /* access modifiers changed from: 0000 */
        public int e() {
            return this.f1090a.size();
        }

        /* access modifiers changed from: 0000 */
        public View e(int i2) {
            return ((w) this.f1090a.get(i2)).f1102a;
        }

        /* access modifiers changed from: 0000 */
        public w f(int i2) {
            int i3 = 0;
            if (this.f1091b != null) {
                int size = this.f1091b.size();
                if (size != 0) {
                    int i4 = 0;
                    while (i4 < size) {
                        w wVar = (w) this.f1091b.get(i4);
                        if (wVar.k() || wVar.d() != i2) {
                            i4++;
                        } else {
                            wVar.b(32);
                            return wVar;
                        }
                    }
                    if (RecyclerView.this.l.b()) {
                        int b2 = RecyclerView.this.e.b(i2);
                        if (b2 > 0 && b2 < RecyclerView.this.l.a()) {
                            long b3 = RecyclerView.this.l.b(b2);
                            while (i3 < size) {
                                w wVar2 = (w) this.f1091b.get(i3);
                                if (wVar2.k() || wVar2.g() != b3) {
                                    i3++;
                                } else {
                                    wVar2.b(32);
                                    return wVar2;
                                }
                            }
                        }
                    }
                    return null;
                }
            }
            return null;
        }

        /* access modifiers changed from: 0000 */
        public void f() {
            this.f1090a.clear();
            if (this.f1091b != null) {
                this.f1091b.clear();
            }
        }

        /* access modifiers changed from: 0000 */
        public n g() {
            if (this.e == null) {
                this.e = new n();
            }
            return this.e;
        }

        /* access modifiers changed from: 0000 */
        public void h() {
            if (RecyclerView.this.l == null || !RecyclerView.this.l.b()) {
                d();
                return;
            }
            int size = this.c.size();
            for (int i2 = 0; i2 < size; i2++) {
                w wVar = (w) this.c.get(i2);
                if (wVar != null) {
                    wVar.b(6);
                    wVar.a((Object) null);
                }
            }
        }

        /* access modifiers changed from: 0000 */
        public void i() {
            int size = this.c.size();
            for (int i2 = 0; i2 < size; i2++) {
                ((w) this.c.get(i2)).a();
            }
            int size2 = this.f1090a.size();
            for (int i3 = 0; i3 < size2; i3++) {
                ((w) this.f1090a.get(i3)).a();
            }
            if (this.f1091b != null) {
                int size3 = this.f1091b.size();
                for (int i4 = 0; i4 < size3; i4++) {
                    ((w) this.f1091b.get(i4)).a();
                }
            }
        }

        /* access modifiers changed from: 0000 */
        public void j() {
            int size = this.c.size();
            for (int i2 = 0; i2 < size; i2++) {
                i iVar = (i) ((w) this.c.get(i2)).f1102a.getLayoutParams();
                if (iVar != null) {
                    iVar.e = true;
                }
            }
        }
    }

    public interface p {
        void a(w wVar);
    }

    private class q extends c {
        q() {
        }

        public void a() {
            RecyclerView.this.a((String) null);
            RecyclerView.this.B.e = true;
            RecyclerView.this.u();
            if (!RecyclerView.this.e.d()) {
                RecyclerView.this.requestLayout();
            }
        }

        public void a(int i, int i2, Object obj) {
            RecyclerView.this.a((String) null);
            if (RecyclerView.this.e.a(i, i2, obj)) {
                b();
            }
        }

        /* access modifiers changed from: 0000 */
        public void b() {
            if (!RecyclerView.c || !RecyclerView.this.q || !RecyclerView.this.p) {
                RecyclerView.this.v = true;
                RecyclerView.this.requestLayout();
                return;
            }
            android.support.v4.i.t.a((View) RecyclerView.this, RecyclerView.this.i);
        }

        public void b(int i, int i2) {
            RecyclerView.this.a((String) null);
            if (RecyclerView.this.e.b(i, i2)) {
                b();
            }
        }
    }

    public static class r extends android.support.v4.i.a {
        public static final Creator<r> CREATOR = new ClassLoaderCreator<r>() {
            /* renamed from: a */
            public r createFromParcel(Parcel parcel) {
                return new r(parcel, null);
            }

            /* renamed from: a */
            public r createFromParcel(Parcel parcel, ClassLoader classLoader) {
                return new r(parcel, classLoader);
            }

            /* renamed from: a */
            public r[] newArray(int i) {
                return new r[i];
            }
        };

        /* renamed from: a reason: collision with root package name */
        Parcelable f1093a;

        r(Parcel parcel, ClassLoader classLoader) {
            super(parcel, classLoader);
            if (classLoader == null) {
                classLoader = h.class.getClassLoader();
            }
            this.f1093a = parcel.readParcelable(classLoader);
        }

        r(Parcelable parcelable) {
            super(parcelable);
        }

        /* access modifiers changed from: 0000 */
        public void a(r rVar) {
            this.f1093a = rVar.f1093a;
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeParcelable(this.f1093a, 0);
        }
    }

    public static abstract class s {

        /* renamed from: a reason: collision with root package name */
        private int f1094a;

        /* renamed from: b reason: collision with root package name */
        private RecyclerView f1095b;
        private h c;
        private boolean d;
        private boolean e;
        private View f;
        private final a g;

        public static class a {

            /* renamed from: a reason: collision with root package name */
            private int f1096a;

            /* renamed from: b reason: collision with root package name */
            private int f1097b;
            private int c;
            private int d;
            private Interpolator e;
            private boolean f;
            private int g;

            private void b() {
                if (this.e != null && this.c < 1) {
                    throw new IllegalStateException("If you provide an interpolator, you must set a positive duration");
                } else if (this.c < 1) {
                    throw new IllegalStateException("Scroll duration must be a positive number");
                }
            }

            /* access modifiers changed from: 0000 */
            public void a(RecyclerView recyclerView) {
                if (this.d >= 0) {
                    int i = this.d;
                    this.d = -1;
                    recyclerView.b(i);
                    this.f = false;
                } else if (this.f) {
                    b();
                    if (this.e != null) {
                        recyclerView.y.a(this.f1096a, this.f1097b, this.c, this.e);
                    } else if (this.c == Integer.MIN_VALUE) {
                        recyclerView.y.b(this.f1096a, this.f1097b);
                    } else {
                        recyclerView.y.a(this.f1096a, this.f1097b, this.c);
                    }
                    this.g++;
                    if (this.g > 10) {
                        Log.e("RecyclerView", "Smooth Scroll action is being updated too frequently. Make sure you are not changing it unless necessary");
                    }
                    this.f = false;
                } else {
                    this.g = 0;
                }
            }

            /* access modifiers changed from: 0000 */
            public boolean a() {
                return this.d >= 0;
            }
        }

        /* access modifiers changed from: private */
        public void a(int i, int i2) {
            RecyclerView recyclerView = this.f1095b;
            if (!this.e || this.f1094a == -1 || recyclerView == null) {
                a();
            }
            this.d = false;
            if (this.f != null) {
                if (a(this.f) == this.f1094a) {
                    a(this.f, recyclerView.B, this.g);
                    this.g.a(recyclerView);
                    a();
                } else {
                    Log.e("RecyclerView", "Passed over target position while smooth scrolling.");
                    this.f = null;
                }
            }
            if (this.e) {
                a(i, i2, recyclerView.B, this.g);
                boolean a2 = this.g.a();
                this.g.a(recyclerView);
                if (!a2) {
                    return;
                }
                if (this.e) {
                    this.d = true;
                    recyclerView.y.a();
                    return;
                }
                a();
            }
        }

        public int a(View view) {
            return this.f1095b.f(view);
        }

        /* access modifiers changed from: protected */
        public final void a() {
            if (this.e) {
                e();
                this.f1095b.B.p = -1;
                this.f = null;
                this.f1094a = -1;
                this.d = false;
                this.e = false;
                this.c.a(this);
                this.c = null;
                this.f1095b = null;
            }
        }

        public void a(int i) {
            this.f1094a = i;
        }

        /* access modifiers changed from: protected */
        public abstract void a(int i, int i2, t tVar, a aVar);

        /* access modifiers changed from: protected */
        public abstract void a(View view, t tVar, a aVar);

        /* access modifiers changed from: protected */
        public void b(View view) {
            if (a(view) == d()) {
                this.f = view;
            }
        }

        public boolean b() {
            return this.d;
        }

        public boolean c() {
            return this.e;
        }

        public int d() {
            return this.f1094a;
        }

        /* access modifiers changed from: protected */
        public abstract void e();
    }

    public static class t {

        /* renamed from: a reason: collision with root package name */
        int f1098a = 0;

        /* renamed from: b reason: collision with root package name */
        int f1099b = 0;
        int c = 1;
        int d = 0;
        boolean e = false;
        boolean f = false;
        boolean g = false;
        boolean h = false;
        boolean i = false;
        boolean j = false;
        int k;
        long l;
        int m;
        int n;
        int o;
        /* access modifiers changed from: private */
        public int p = -1;
        private SparseArray<Object> q;

        /* access modifiers changed from: 0000 */
        public void a(int i2) {
            if ((this.c & i2) == 0) {
                throw new IllegalStateException("Layout state should be one of " + Integer.toBinaryString(i2) + " but it is " + Integer.toBinaryString(this.c));
            }
        }

        /* access modifiers changed from: 0000 */
        public void a(a aVar) {
            this.c = 1;
            this.d = aVar.a();
            this.f = false;
            this.g = false;
            this.h = false;
        }

        public boolean a() {
            return this.f;
        }

        public boolean b() {
            return this.j;
        }

        public int c() {
            return this.p;
        }

        public boolean d() {
            return this.p != -1;
        }

        public int e() {
            return this.f ? this.f1098a - this.f1099b : this.d;
        }

        public String toString() {
            return "State{mTargetPosition=" + this.p + ", mData=" + this.q + ", mItemCount=" + this.d + ", mPreviousLayoutItemCount=" + this.f1098a + ", mDeletedInvisibleItemCountSincePreviousLayout=" + this.f1099b + ", mStructureChanged=" + this.e + ", mInPreLayout=" + this.f + ", mRunSimpleAnimations=" + this.i + ", mRunPredictiveAnimations=" + this.j + '}';
        }
    }

    public static abstract class u {
        public abstract View a(o oVar, int i, int i2);
    }

    class v implements Runnable {

        /* renamed from: a reason: collision with root package name */
        Interpolator f1100a = RecyclerView.H;
        private int c;
        private int d;
        /* access modifiers changed from: private */
        public OverScroller e;
        private boolean f = false;
        private boolean g = false;

        v() {
            this.e = new OverScroller(RecyclerView.this.getContext(), RecyclerView.H);
        }

        private float a(float f2) {
            return (float) Math.sin((double) ((f2 - 0.5f) * 0.47123894f));
        }

        private int b(int i, int i2, int i3, int i4) {
            int i5;
            int abs = Math.abs(i);
            int abs2 = Math.abs(i2);
            boolean z = abs > abs2;
            int sqrt = (int) Math.sqrt((double) ((i3 * i3) + (i4 * i4)));
            int sqrt2 = (int) Math.sqrt((double) ((i * i) + (i2 * i2)));
            int height = z ? RecyclerView.this.getWidth() : RecyclerView.this.getHeight();
            int i6 = height / 2;
            float a2 = (a(Math.min(1.0f, (((float) sqrt2) * 1.0f) / ((float) height))) * ((float) i6)) + ((float) i6);
            if (sqrt > 0) {
                i5 = Math.round(1000.0f * Math.abs(a2 / ((float) sqrt))) * 4;
            } else {
                i5 = (int) (((((float) (z ? abs : abs2)) / ((float) height)) + 1.0f) * 300.0f);
            }
            return Math.min(i5, Config.STARTUP_CURRENT_INTERVAL);
        }

        private void c() {
            this.g = false;
            this.f = true;
        }

        private void d() {
            this.f = false;
            if (this.g) {
                a();
            }
        }

        /* access modifiers changed from: 0000 */
        public void a() {
            if (this.f) {
                this.g = true;
                return;
            }
            RecyclerView.this.removeCallbacks(this);
            android.support.v4.i.t.a((View) RecyclerView.this, (Runnable) this);
        }

        public void a(int i, int i2) {
            RecyclerView.this.setScrollState(2);
            this.d = 0;
            this.c = 0;
            this.e.fling(0, 0, i, i2, Integer.MIN_VALUE, Integer.MAX_VALUE, Integer.MIN_VALUE, Integer.MAX_VALUE);
            a();
        }

        public void a(int i, int i2, int i3) {
            a(i, i2, i3, RecyclerView.H);
        }

        public void a(int i, int i2, int i3, int i4) {
            a(i, i2, b(i, i2, i3, i4));
        }

        public void a(int i, int i2, int i3, Interpolator interpolator) {
            if (this.f1100a != interpolator) {
                this.f1100a = interpolator;
                this.e = new OverScroller(RecyclerView.this.getContext(), interpolator);
            }
            RecyclerView.this.setScrollState(2);
            this.d = 0;
            this.c = 0;
            this.e.startScroll(0, 0, i, i2, i3);
            if (VERSION.SDK_INT < 23) {
                this.e.computeScrollOffset();
            }
            a();
        }

        public void a(int i, int i2, Interpolator interpolator) {
            int b2 = b(i, i2, 0, 0);
            if (interpolator == null) {
                interpolator = RecyclerView.H;
            }
            a(i, i2, b2, interpolator);
        }

        public void b() {
            RecyclerView.this.removeCallbacks(this);
            this.e.abortAnimation();
        }

        public void b(int i, int i2) {
            a(i, i2, 0, 0);
        }

        public void run() {
            int i;
            int i2;
            int i3;
            int i4;
            int i5;
            int i6;
            int i7;
            int i8;
            int i9;
            int i10;
            if (RecyclerView.this.m == null) {
                b();
                return;
            }
            c();
            RecyclerView.this.d();
            OverScroller overScroller = this.e;
            s sVar = RecyclerView.this.m.t;
            if (overScroller.computeScrollOffset()) {
                int[] a2 = RecyclerView.this.aC;
                int currX = overScroller.getCurrX();
                int currY = overScroller.getCurrY();
                int i11 = currX - this.c;
                int i12 = currY - this.d;
                this.c = currX;
                this.d = currY;
                if (RecyclerView.this.a(i11, i12, a2, (int[]) null, 1)) {
                    i = i12 - a2[1];
                    i2 = i11 - a2[0];
                } else {
                    i = i12;
                    i2 = i11;
                }
                if (RecyclerView.this.l != null) {
                    RecyclerView.this.e();
                    RecyclerView.this.l();
                    android.support.v4.f.d.a("RV Scroll");
                    RecyclerView.this.a(RecyclerView.this.B);
                    if (i2 != 0) {
                        i8 = RecyclerView.this.m.a(i2, RecyclerView.this.d, RecyclerView.this.B);
                        i4 = i2 - i8;
                    } else {
                        i4 = 0;
                        i8 = 0;
                    }
                    if (i != 0) {
                        i10 = RecyclerView.this.m.b(i, RecyclerView.this.d, RecyclerView.this.B);
                        i9 = i - i10;
                    } else {
                        i9 = 0;
                        i10 = 0;
                    }
                    android.support.v4.f.d.a();
                    RecyclerView.this.x();
                    RecyclerView.this.m();
                    RecyclerView.this.a(false);
                    if (sVar != null && !sVar.b() && sVar.c()) {
                        int e2 = RecyclerView.this.B.e();
                        if (e2 == 0) {
                            sVar.a();
                            i3 = i9;
                            int i13 = i10;
                            i6 = i8;
                            i5 = i13;
                        } else if (sVar.d() >= e2) {
                            sVar.a(e2 - 1);
                            sVar.a(i2 - i4, i - i9);
                            i3 = i9;
                            int i14 = i10;
                            i6 = i8;
                            i5 = i14;
                        } else {
                            sVar.a(i2 - i4, i - i9);
                        }
                    }
                    i3 = i9;
                    int i15 = i10;
                    i6 = i8;
                    i5 = i15;
                } else {
                    i3 = 0;
                    i4 = 0;
                    i5 = 0;
                    i6 = 0;
                }
                if (!RecyclerView.this.o.isEmpty()) {
                    RecyclerView.this.invalidate();
                }
                if (RecyclerView.this.getOverScrollMode() != 2) {
                    RecyclerView.this.c(i2, i);
                }
                if (!RecyclerView.this.a(i6, i5, i4, i3, (int[]) null, 1) && !(i4 == 0 && i3 == 0)) {
                    int currVelocity = (int) overScroller.getCurrVelocity();
                    if (i4 != currX) {
                        int i16 = i4 < 0 ? -currVelocity : i4 > 0 ? currVelocity : 0;
                        i7 = i16;
                    } else {
                        i7 = 0;
                    }
                    if (i3 == currY) {
                        currVelocity = 0;
                    } else if (i3 < 0) {
                        currVelocity = -currVelocity;
                    } else if (i3 <= 0) {
                        currVelocity = 0;
                    }
                    if (RecyclerView.this.getOverScrollMode() != 2) {
                        RecyclerView.this.d(i7, currVelocity);
                    }
                    if ((i7 != 0 || i4 == currX || overScroller.getFinalX() == 0) && (currVelocity != 0 || i3 == currY || overScroller.getFinalY() == 0)) {
                        overScroller.abortAnimation();
                    }
                }
                if (!(i6 == 0 && i5 == 0)) {
                    RecyclerView.this.i(i6, i5);
                }
                if (!RecyclerView.this.awakenScrollBars()) {
                    RecyclerView.this.invalidate();
                }
                boolean z = (i2 == 0 && i == 0) || (i2 != 0 && RecyclerView.this.m.d() && i6 == i2) || (i != 0 && RecyclerView.this.m.e() && i5 == i);
                if (overScroller.isFinished() || (!z && !RecyclerView.this.i(1))) {
                    RecyclerView.this.setScrollState(0);
                    if (RecyclerView.K) {
                        RecyclerView.this.A.a();
                    }
                    RecyclerView.this.h(1);
                } else {
                    a();
                    if (RecyclerView.this.z != null) {
                        RecyclerView.this.z.a(RecyclerView.this, i2, i);
                    }
                }
            }
            if (sVar != null) {
                if (sVar.b()) {
                    sVar.a(0, 0);
                }
                if (!this.g) {
                    sVar.a();
                }
            }
            d();
        }
    }

    public static abstract class w {
        private static final List<Object> o = Collections.EMPTY_LIST;

        /* renamed from: a reason: collision with root package name */
        public final View f1102a;

        /* renamed from: b reason: collision with root package name */
        WeakReference<RecyclerView> f1103b;
        int c = -1;
        int d = -1;
        long e = -1;
        int f = -1;
        int g = -1;
        w h = null;
        w i = null;
        List<Object> j = null;
        List<Object> k = null;
        int l = -1;
        RecyclerView m;
        /* access modifiers changed from: private */
        public int n;
        private int p = 0;
        /* access modifiers changed from: private */
        public o q = null;
        /* access modifiers changed from: private */
        public boolean r = false;
        private int s = 0;

        public w(View view) {
            if (view == null) {
                throw new IllegalArgumentException("itemView may not be null");
            }
            this.f1102a = view;
        }

        /* access modifiers changed from: private */
        public boolean A() {
            return (this.n & 16) == 0 && android.support.v4.i.t.b(this.f1102a);
        }

        /* access modifiers changed from: private */
        public void a(RecyclerView recyclerView) {
            this.s = android.support.v4.i.t.d(this.f1102a);
            recyclerView.a(this, 4);
        }

        /* access modifiers changed from: private */
        public void b(RecyclerView recyclerView) {
            recyclerView.a(this, this.s);
            this.s = 0;
        }

        private void y() {
            if (this.j == null) {
                this.j = new ArrayList();
                this.k = Collections.unmodifiableList(this.j);
            }
        }

        /* access modifiers changed from: private */
        public boolean z() {
            return (this.n & 16) != 0;
        }

        /* access modifiers changed from: 0000 */
        public void a() {
            this.d = -1;
            this.g = -1;
        }

        /* access modifiers changed from: 0000 */
        public void a(int i2, int i3) {
            this.n = (this.n & (i3 ^ -1)) | (i2 & i3);
        }

        /* access modifiers changed from: 0000 */
        public void a(int i2, int i3, boolean z) {
            b(8);
            a(i3, z);
            this.c = i2;
        }

        /* access modifiers changed from: 0000 */
        public void a(int i2, boolean z) {
            if (this.d == -1) {
                this.d = this.c;
            }
            if (this.g == -1) {
                this.g = this.c;
            }
            if (z) {
                this.g += i2;
            }
            this.c += i2;
            if (this.f1102a.getLayoutParams() != null) {
                ((i) this.f1102a.getLayoutParams()).e = true;
            }
        }

        /* access modifiers changed from: 0000 */
        public void a(o oVar, boolean z) {
            this.q = oVar;
            this.r = z;
        }

        /* access modifiers changed from: 0000 */
        public void a(Object obj) {
            if (obj == null) {
                b(1024);
            } else if ((this.n & 1024) == 0) {
                y();
                this.j.add(obj);
            }
        }

        public final void a(boolean z) {
            this.p = z ? this.p - 1 : this.p + 1;
            if (this.p < 0) {
                this.p = 0;
                Log.e("View", "isRecyclable decremented below 0: unmatched pair of setIsRecyable() calls for " + this);
            } else if (!z && this.p == 1) {
                this.n |= 16;
            } else if (z && this.p == 0) {
                this.n &= -17;
            }
        }

        /* access modifiers changed from: 0000 */
        public boolean a(int i2) {
            return (this.n & i2) != 0;
        }

        /* access modifiers changed from: 0000 */
        public void b() {
            if (this.d == -1) {
                this.d = this.c;
            }
        }

        /* access modifiers changed from: 0000 */
        public void b(int i2) {
            this.n |= i2;
        }

        /* access modifiers changed from: 0000 */
        public boolean c() {
            return (this.n & 128) != 0;
        }

        public final int d() {
            return this.g == -1 ? this.c : this.g;
        }

        public final int e() {
            if (this.m == null) {
                return -1;
            }
            return this.m.d(this);
        }

        public final int f() {
            return this.d;
        }

        public final long g() {
            return this.e;
        }

        public final int h() {
            return this.f;
        }

        /* access modifiers changed from: 0000 */
        public boolean i() {
            return this.q != null;
        }

        /* access modifiers changed from: 0000 */
        public void j() {
            this.q.c(this);
        }

        /* access modifiers changed from: 0000 */
        public boolean k() {
            return (this.n & 32) != 0;
        }

        /* access modifiers changed from: 0000 */
        public void l() {
            this.n &= -33;
        }

        /* access modifiers changed from: 0000 */
        public void m() {
            this.n &= -257;
        }

        /* access modifiers changed from: 0000 */
        public boolean n() {
            return (this.n & 4) != 0;
        }

        /* access modifiers changed from: 0000 */
        public boolean o() {
            return (this.n & 2) != 0;
        }

        /* access modifiers changed from: 0000 */
        public boolean p() {
            return (this.n & 1) != 0;
        }

        /* access modifiers changed from: 0000 */
        public boolean q() {
            return (this.n & 8) != 0;
        }

        /* access modifiers changed from: 0000 */
        public boolean r() {
            return (this.n & 256) != 0;
        }

        /* access modifiers changed from: 0000 */
        public boolean s() {
            return (this.n & 512) != 0 || n();
        }

        /* access modifiers changed from: 0000 */
        public void t() {
            if (this.j != null) {
                this.j.clear();
            }
            this.n &= -1025;
        }

        public String toString() {
            StringBuilder sb = new StringBuilder("ViewHolder{" + Integer.toHexString(hashCode()) + " position=" + this.c + " id=" + this.e + ", oldPos=" + this.d + ", pLpos:" + this.g);
            if (i()) {
                sb.append(" scrap ").append(this.r ? "[changeScrap]" : "[attachedScrap]");
            }
            if (n()) {
                sb.append(" invalid");
            }
            if (!p()) {
                sb.append(" unbound");
            }
            if (o()) {
                sb.append(" update");
            }
            if (q()) {
                sb.append(" removed");
            }
            if (c()) {
                sb.append(" ignored");
            }
            if (r()) {
                sb.append(" tmpDetached");
            }
            if (!w()) {
                sb.append(" not recyclable(" + this.p + ")");
            }
            if (s()) {
                sb.append(" undefined adapter position");
            }
            if (this.f1102a.getParent() == null) {
                sb.append(" no parent");
            }
            sb.append("}");
            return sb.toString();
        }

        /* access modifiers changed from: 0000 */
        public List<Object> u() {
            return (this.n & 1024) == 0 ? (this.j == null || this.j.size() == 0) ? o : this.k : o;
        }

        /* access modifiers changed from: 0000 */
        public void v() {
            this.n = 0;
            this.c = -1;
            this.d = -1;
            this.e = -1;
            this.g = -1;
            this.p = 0;
            this.h = null;
            this.i = null;
            t();
            this.s = 0;
            this.l = -1;
            RecyclerView.c(this);
        }

        public final boolean w() {
            return (this.n & 16) == 0 && !android.support.v4.i.t.b(this.f1102a);
        }

        /* access modifiers changed from: 0000 */
        public boolean x() {
            return (this.n & 2) != 0;
        }
    }

    public RecyclerView(Context context) {
        this(context, null);
    }

    public RecyclerView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public RecyclerView(Context context, AttributeSet attributeSet, int i2) {
        boolean z2 = true;
        super(context, attributeSet, i2);
        this.O = new q();
        this.d = new o();
        this.g = new bv();
        this.i = new Runnable() {
            public void run() {
                if (RecyclerView.this.s && !RecyclerView.this.isLayoutRequested()) {
                    if (!RecyclerView.this.p) {
                        RecyclerView.this.requestLayout();
                    } else if (RecyclerView.this.u) {
                        RecyclerView.this.t = true;
                    } else {
                        RecyclerView.this.d();
                    }
                }
            }
        };
        this.j = new Rect();
        this.Q = new Rect();
        this.k = new RectF();
        this.o = new ArrayList<>();
        this.R = new ArrayList<>();
        this.T = 0;
        this.w = false;
        this.ab = 0;
        this.ac = 0;
        this.x = new al();
        this.ah = 0;
        this.ai = -1;
        this.as = Float.MIN_VALUE;
        this.at = Float.MIN_VALUE;
        this.au = true;
        this.y = new v();
        this.A = K ? new a() : null;
        this.B = new t();
        this.C = false;
        this.D = false;
        this.ax = new f();
        this.E = false;
        this.az = new int[2];
        this.aB = new int[2];
        this.aC = new int[2];
        this.aD = new int[2];
        this.G = new ArrayList();
        this.aE = new Runnable() {
            public void run() {
                if (RecyclerView.this.x != null) {
                    RecyclerView.this.x.a();
                }
                RecyclerView.this.E = false;
            }
        };
        this.aF = new b() {
            public void a(w wVar) {
                RecyclerView.this.m.a(wVar.f1102a, RecyclerView.this.d);
            }

            public void a(w wVar, c cVar, c cVar2) {
                RecyclerView.this.d.c(wVar);
                RecyclerView.this.b(wVar, cVar, cVar2);
            }

            public void b(w wVar, c cVar, c cVar2) {
                RecyclerView.this.a(wVar, cVar, cVar2);
            }

            public void c(w wVar, c cVar, c cVar2) {
                wVar.a(false);
                if (RecyclerView.this.w) {
                    if (RecyclerView.this.x.a(wVar, wVar, cVar, cVar2)) {
                        RecyclerView.this.p();
                    }
                } else if (RecyclerView.this.x.c(wVar, cVar, cVar2)) {
                    RecyclerView.this.p();
                }
            }
        };
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, J, i2, 0);
            this.h = obtainStyledAttributes.getBoolean(0, true);
            obtainStyledAttributes.recycle();
        } else {
            this.h = true;
        }
        setScrollContainer(true);
        setFocusableInTouchMode(true);
        ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
        this.ao = viewConfiguration.getScaledTouchSlop();
        this.as = android.support.v4.i.u.a(viewConfiguration, context);
        this.at = android.support.v4.i.u.b(viewConfiguration, context);
        this.aq = viewConfiguration.getScaledMinimumFlingVelocity();
        this.ar = viewConfiguration.getScaledMaximumFlingVelocity();
        setWillNotDraw(getOverScrollMode() == 2);
        this.x.a(this.ax);
        b();
        A();
        if (android.support.v4.i.t.d(this) == 0) {
            android.support.v4.i.t.a((View) this, 1);
        }
        this.W = (AccessibilityManager) getContext().getSystemService("accessibility");
        setAccessibilityDelegateCompat(new ba(this));
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes2 = context.obtainStyledAttributes(attributeSet, android.support.v7.e.a.c.RecyclerView, i2, 0);
            String string = obtainStyledAttributes2.getString(android.support.v7.e.a.c.RecyclerView_layoutManager);
            if (obtainStyledAttributes2.getInt(android.support.v7.e.a.c.RecyclerView_android_descendantFocusability, -1) == -1) {
                setDescendantFocusability(262144);
            }
            this.r = obtainStyledAttributes2.getBoolean(android.support.v7.e.a.c.RecyclerView_fastScrollEnabled, false);
            if (this.r) {
                a((StateListDrawable) obtainStyledAttributes2.getDrawable(android.support.v7.e.a.c.RecyclerView_fastScrollVerticalThumbDrawable), obtainStyledAttributes2.getDrawable(android.support.v7.e.a.c.RecyclerView_fastScrollVerticalTrackDrawable), (StateListDrawable) obtainStyledAttributes2.getDrawable(android.support.v7.e.a.c.RecyclerView_fastScrollHorizontalThumbDrawable), obtainStyledAttributes2.getDrawable(android.support.v7.e.a.c.RecyclerView_fastScrollHorizontalTrackDrawable));
            }
            obtainStyledAttributes2.recycle();
            a(context, string, attributeSet, i2, 0);
            if (VERSION.SDK_INT >= 21) {
                TypedArray obtainStyledAttributes3 = context.obtainStyledAttributes(attributeSet, I, i2, 0);
                z2 = obtainStyledAttributes3.getBoolean(0, true);
                obtainStyledAttributes3.recycle();
            }
        } else {
            setDescendantFocusability(262144);
        }
        setNestedScrollingEnabled(z2);
    }

    private void A() {
        this.f = new ai(new b() {
            public int a() {
                return RecyclerView.this.getChildCount();
            }

            public int a(View view) {
                return RecyclerView.this.indexOfChild(view);
            }

            public void a(int i) {
                View childAt = RecyclerView.this.getChildAt(i);
                if (childAt != null) {
                    RecyclerView.this.k(childAt);
                    childAt.clearAnimation();
                }
                RecyclerView.this.removeViewAt(i);
            }

            public void a(View view, int i) {
                RecyclerView.this.addView(view, i);
                RecyclerView.this.l(view);
            }

            public void a(View view, int i, LayoutParams layoutParams) {
                w e = RecyclerView.e(view);
                if (e != null) {
                    if (e.r() || e.c()) {
                        e.m();
                    } else {
                        throw new IllegalArgumentException("Called attach on a child which is not detached: " + e + RecyclerView.this.a());
                    }
                }
                RecyclerView.this.attachViewToParent(view, i, layoutParams);
            }

            public w b(View view) {
                return RecyclerView.e(view);
            }

            public View b(int i) {
                return RecyclerView.this.getChildAt(i);
            }

            public void b() {
                int a2 = a();
                for (int i = 0; i < a2; i++) {
                    View b2 = b(i);
                    RecyclerView.this.k(b2);
                    b2.clearAnimation();
                }
                RecyclerView.this.removeAllViews();
            }

            public void c(int i) {
                View b2 = b(i);
                if (b2 != null) {
                    w e = RecyclerView.e(b2);
                    if (e != null) {
                        if (!e.r() || e.c()) {
                            e.b(256);
                        } else {
                            throw new IllegalArgumentException("called detach on an already detached child " + e + RecyclerView.this.a());
                        }
                    }
                }
                RecyclerView.this.detachViewFromParent(i);
            }

            public void c(View view) {
                w e = RecyclerView.e(view);
                if (e != null) {
                    e.a(RecyclerView.this);
                }
            }

            public void d(View view) {
                w e = RecyclerView.e(view);
                if (e != null) {
                    e.b(RecyclerView.this);
                }
            }
        });
    }

    private boolean B() {
        int b2 = this.f.b();
        for (int i2 = 0; i2 < b2; i2++) {
            w e2 = e(this.f.b(i2));
            if (e2 != null && !e2.c() && e2.x()) {
                return true;
            }
        }
        return false;
    }

    private void C() {
        this.y.b();
        if (this.m != null) {
            this.m.G();
        }
    }

    private void D() {
        boolean z2 = false;
        if (this.ad != null) {
            this.ad.onRelease();
            z2 = this.ad.isFinished();
        }
        if (this.ae != null) {
            this.ae.onRelease();
            z2 |= this.ae.isFinished();
        }
        if (this.af != null) {
            this.af.onRelease();
            z2 |= this.af.isFinished();
        }
        if (this.ag != null) {
            this.ag.onRelease();
            z2 |= this.ag.isFinished();
        }
        if (z2) {
            android.support.v4.i.t.c(this);
        }
    }

    private void E() {
        if (this.aj != null) {
            this.aj.clear();
        }
        h(0);
        D();
    }

    private void F() {
        E();
        setScrollState(0);
    }

    private void G() {
        int i2 = this.V;
        this.V = 0;
        if (i2 != 0 && n()) {
            AccessibilityEvent obtain = AccessibilityEvent.obtain();
            obtain.setEventType(2048);
            android.support.v4.i.a.a.a(obtain, i2);
            sendAccessibilityEventUnchecked(obtain);
        }
    }

    private boolean H() {
        return this.x != null && this.m.b();
    }

    private void I() {
        boolean z2 = true;
        if (this.w) {
            this.e.a();
            this.m.a(this);
        }
        if (H()) {
            this.e.b();
        } else {
            this.e.e();
        }
        boolean z3 = this.C || this.D;
        this.B.i = this.s && this.x != null && (this.w || z3 || this.m.u) && (!this.w || this.l.b());
        t tVar = this.B;
        if (!this.B.i || !z3 || this.w || !H()) {
            z2 = false;
        }
        tVar.j = z2;
    }

    private void J() {
        View view = (!this.au || !hasFocus() || this.l == null) ? null : getFocusedChild();
        w d2 = view == null ? null : d(view);
        if (d2 == null) {
            K();
            return;
        }
        this.B.l = this.l.b() ? d2.g() : -1;
        t tVar = this.B;
        int e2 = this.w ? -1 : d2.q() ? d2.d : d2.e();
        tVar.k = e2;
        this.B.m = m(d2.f1102a);
    }

    private void K() {
        this.B.l = -1;
        this.B.k = -1;
        this.B.m = -1;
    }

    private View L() {
        int i2 = this.B.k != -1 ? this.B.k : 0;
        int e2 = this.B.e();
        int i3 = i2;
        while (i3 < e2) {
            w c2 = c(i3);
            if (c2 == null) {
                break;
            } else if (c2.f1102a.hasFocusable()) {
                return c2.f1102a;
            } else {
                i3++;
            }
        }
        for (int min = Math.min(e2, i2) - 1; min >= 0; min--) {
            w c3 = c(min);
            if (c3 == null) {
                return null;
            }
            if (c3.f1102a.hasFocusable()) {
                return c3.f1102a;
            }
        }
        return null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00a7, code lost:
        if (r0.isFocusable() != false) goto L_0x00a9;
     */
    private void M() {
        View view;
        View view2 = null;
        if (this.au && this.l != null && hasFocus() && getDescendantFocusability() != 393216) {
            if (getDescendantFocusability() != 131072 || !isFocused()) {
                if (!isFocused()) {
                    View focusedChild = getFocusedChild();
                    if (!M || (focusedChild.getParent() != null && focusedChild.hasFocus())) {
                        if (!this.f.c(focusedChild)) {
                            return;
                        }
                    } else if (this.f.b() == 0) {
                        requestFocus();
                        return;
                    }
                }
                w wVar = (this.B.l == -1 || !this.l.b()) ? null : a(this.B.l);
                if (wVar != null && !this.f.c(wVar.f1102a) && wVar.f1102a.hasFocusable()) {
                    view2 = wVar.f1102a;
                } else if (this.f.b() > 0) {
                    view2 = L();
                }
                if (view2 != null) {
                    if (((long) this.B.m) != -1) {
                        view = view2.findViewById(this.B.m);
                        if (view != null) {
                        }
                    }
                    view = view2;
                    view.requestFocus();
                }
            }
        }
    }

    private void N() {
        boolean z2 = true;
        this.B.a(1);
        a(this.B);
        this.B.h = false;
        e();
        this.g.a();
        l();
        I();
        J();
        t tVar = this.B;
        if (!this.B.i || !this.D) {
            z2 = false;
        }
        tVar.g = z2;
        this.D = false;
        this.C = false;
        this.B.f = this.B.j;
        this.B.d = this.l.a();
        a(this.az);
        if (this.B.i) {
            int b2 = this.f.b();
            for (int i2 = 0; i2 < b2; i2++) {
                w e2 = e(this.f.b(i2));
                if (!e2.c() && (!e2.n() || this.l.b())) {
                    this.g.a(e2, this.x.a(this.B, e2, e.e(e2), e2.u()));
                    if (this.B.g && e2.x() && !e2.q() && !e2.c() && !e2.n()) {
                        this.g.a(a(e2), e2);
                    }
                }
            }
        }
        if (this.B.j) {
            s();
            boolean z3 = this.B.e;
            this.B.e = false;
            this.m.c(this.d, this.B);
            this.B.e = z3;
            for (int i3 = 0; i3 < this.f.b(); i3++) {
                w e3 = e(this.f.b(i3));
                if (!e3.c() && !this.g.d(e3)) {
                    int e4 = e.e(e3);
                    boolean a2 = e3.a(8192);
                    if (!a2) {
                        e4 |= 4096;
                    }
                    c a3 = this.x.a(this.B, e3, e4, e3.u());
                    if (a2) {
                        a(e3, a3);
                    } else {
                        this.g.b(e3, a3);
                    }
                }
            }
            t();
        } else {
            t();
        }
        m();
        a(false);
        this.B.c = 2;
    }

    private void O() {
        e();
        l();
        this.B.a(6);
        this.e.e();
        this.B.d = this.l.a();
        this.B.f1099b = 0;
        this.B.f = false;
        this.m.c(this.d, this.B);
        this.B.e = false;
        this.P = null;
        this.B.i = this.B.i && this.x != null;
        this.B.c = 4;
        m();
        a(false);
    }

    private void P() {
        this.B.a(4);
        e();
        l();
        this.B.c = 1;
        if (this.B.i) {
            for (int b2 = this.f.b() - 1; b2 >= 0; b2--) {
                w e2 = e(this.f.b(b2));
                if (!e2.c()) {
                    long a2 = a(e2);
                    c a3 = this.x.a(this.B, e2);
                    w a4 = this.g.a(a2);
                    if (a4 == null || a4.c()) {
                        this.g.c(e2, a3);
                    } else {
                        boolean a5 = this.g.a(a4);
                        boolean a6 = this.g.a(e2);
                        if (!a5 || a4 != e2) {
                            c b3 = this.g.b(a4);
                            this.g.c(e2, a3);
                            c c2 = this.g.c(e2);
                            if (b3 == null) {
                                a(a2, e2, a4);
                            } else {
                                a(a4, e2, b3, c2, a5, a6);
                            }
                        } else {
                            this.g.c(e2, a3);
                        }
                    }
                }
            }
            this.g.a(this.aF);
        }
        this.m.b(this.d);
        this.B.f1098a = this.B.d;
        this.w = false;
        this.B.i = false;
        this.B.j = false;
        this.m.u = false;
        if (this.d.f1091b != null) {
            this.d.f1091b.clear();
        }
        if (this.m.y) {
            this.m.x = 0;
            this.m.y = false;
            this.d.b();
        }
        this.m.a(this.B);
        m();
        a(false);
        this.g.a();
        if (k(this.az[0], this.az[1])) {
            i(0, 0);
        }
        M();
        K();
    }

    private String a(Context context, String str) {
        return str.charAt(0) == '.' ? context.getPackageName() + str : !str.contains(".") ? RecyclerView.class.getPackage().getName() + '.' + str : str;
    }

    private void a(float f2, float f3, float f4, float f5) {
        boolean z2 = true;
        boolean z3 = false;
        if (f3 < 0.0f) {
            g();
            android.support.v4.widget.h.a(this.ad, (-f3) / ((float) getWidth()), 1.0f - (f4 / ((float) getHeight())));
            z3 = true;
        } else if (f3 > 0.0f) {
            h();
            android.support.v4.widget.h.a(this.af, f3 / ((float) getWidth()), f4 / ((float) getHeight()));
            z3 = true;
        }
        if (f5 < 0.0f) {
            i();
            android.support.v4.widget.h.a(this.ae, (-f5) / ((float) getHeight()), f2 / ((float) getWidth()));
        } else if (f5 > 0.0f) {
            j();
            android.support.v4.widget.h.a(this.ag, f5 / ((float) getHeight()), 1.0f - (f2 / ((float) getWidth())));
        } else {
            z2 = z3;
        }
        if (z2 || f3 != 0.0f || f5 != 0.0f) {
            android.support.v4.i.t.c(this);
        }
    }

    private void a(long j2, w wVar, w wVar2) {
        int b2 = this.f.b();
        int i2 = 0;
        while (i2 < b2) {
            w e2 = e(this.f.b(i2));
            if (e2 == wVar || a(e2) != j2) {
                i2++;
            } else if (this.l == null || !this.l.b()) {
                throw new IllegalStateException("Two different ViewHolders have the same change ID. This might happen due to inconsistent Adapter update events or if the LayoutManager lays out the same View multiple times.\n ViewHolder 1:" + e2 + " \n View Holder 2:" + wVar + a());
            } else {
                throw new IllegalStateException("Two different ViewHolders have the same stable ID. Stable IDs in your adapter MUST BE unique and SHOULD NOT change.\n ViewHolder 1:" + e2 + " \n View Holder 2:" + wVar + a());
            }
        }
        Log.e("RecyclerView", "Problem while matching changed view holders with the newones. The pre-layout information for the change holder " + wVar2 + " cannot be found but it is necessary for " + wVar + a());
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    private void a(Context context, String str, AttributeSet attributeSet, int i2, int i3) {
        Constructor constructor;
        Object[] objArr;
        if (str != null) {
            String trim = str.trim();
            if (!trim.isEmpty()) {
                String a2 = a(context, trim);
                try {
                    Class asSubclass = (isInEditMode() ? getClass().getClassLoader() : context.getClassLoader()).loadClass(a2).asSubclass(h.class);
                    try {
                        objArr = new Object[]{context, attributeSet, Integer.valueOf(i2), Integer.valueOf(i3)};
                        constructor = asSubclass.getConstructor(N);
                    } catch (NoSuchMethodException e2) {
                        constructor = asSubclass.getConstructor(new Class[0]);
                        objArr = null;
                    }
                    constructor.setAccessible(true);
                    setLayoutManager((h) constructor.newInstance(objArr));
                } catch (NoSuchMethodException e3) {
                    e3.initCause(e2);
                    throw new IllegalStateException(attributeSet.getPositionDescription() + ": Error creating LayoutManager " + a2, e3);
                } catch (ClassNotFoundException e4) {
                    throw new IllegalStateException(attributeSet.getPositionDescription() + ": Unable to find LayoutManager " + a2, e4);
                } catch (InvocationTargetException e5) {
                    throw new IllegalStateException(attributeSet.getPositionDescription() + ": Could not instantiate the LayoutManager: " + a2, e5);
                } catch (InstantiationException e6) {
                    throw new IllegalStateException(attributeSet.getPositionDescription() + ": Could not instantiate the LayoutManager: " + a2, e6);
                } catch (IllegalAccessException e7) {
                    throw new IllegalStateException(attributeSet.getPositionDescription() + ": Cannot access non-public constructor " + a2, e7);
                } catch (ClassCastException e8) {
                    throw new IllegalStateException(attributeSet.getPositionDescription() + ": Class is not a LayoutManager " + a2, e8);
                }
            }
        }
    }

    private void a(a aVar, boolean z2, boolean z3) {
        if (this.l != null) {
            this.l.b((c) this.O);
            this.l.b(this);
        }
        if (!z2 || z3) {
            c();
        }
        this.e.a();
        a aVar2 = this.l;
        this.l = aVar;
        if (aVar != null) {
            aVar.a((c) this.O);
            aVar.a(this);
        }
        if (this.m != null) {
            this.m.a(aVar2, this.l);
        }
        this.d.a(aVar2, this.l, z2);
        this.B.e = true;
        u();
    }

    private void a(w wVar, w wVar2, c cVar, c cVar2, boolean z2, boolean z3) {
        wVar.a(false);
        if (z2) {
            e(wVar);
        }
        if (wVar != wVar2) {
            if (z3) {
                e(wVar2);
            }
            wVar.h = wVar2;
            e(wVar);
            this.d.c(wVar);
            wVar2.a(false);
            wVar2.i = wVar;
        }
        if (this.x.a(wVar, wVar2, cVar, cVar2)) {
            p();
        }
    }

    static void a(View view, Rect rect) {
        i iVar = (i) view.getLayoutParams();
        Rect rect2 = iVar.d;
        rect.set((view.getLeft() - rect2.left) - iVar.leftMargin, (view.getTop() - rect2.top) - iVar.topMargin, view.getRight() + rect2.right + iVar.rightMargin, iVar.bottomMargin + rect2.bottom + view.getBottom());
    }

    private void a(View view, View view2) {
        boolean z2 = true;
        View view3 = view2 != null ? view2 : view;
        this.j.set(0, 0, view3.getWidth(), view3.getHeight());
        LayoutParams layoutParams = view3.getLayoutParams();
        if (layoutParams instanceof i) {
            i iVar = (i) layoutParams;
            if (!iVar.e) {
                Rect rect = iVar.d;
                this.j.left -= rect.left;
                this.j.right += rect.right;
                this.j.top -= rect.top;
                Rect rect2 = this.j;
                rect2.bottom = rect.bottom + rect2.bottom;
            }
        }
        if (view2 != null) {
            offsetDescendantRectToMyCoords(view2, this.j);
            offsetRectIntoDescendantCoords(view, this.j);
        }
        h hVar = this.m;
        Rect rect3 = this.j;
        boolean z3 = !this.s;
        if (view2 != null) {
            z2 = false;
        }
        hVar.a(this, view, rect3, z3, z2);
    }

    private void a(int[] iArr) {
        int b2 = this.f.b();
        if (b2 == 0) {
            iArr[0] = -1;
            iArr[1] = -1;
            return;
        }
        int i2 = Integer.MAX_VALUE;
        int i3 = Integer.MIN_VALUE;
        int i4 = 0;
        while (i4 < b2) {
            w e2 = e(this.f.b(i4));
            if (!e2.c()) {
                int d2 = e2.d();
                if (d2 < i2) {
                    i2 = d2;
                }
                if (d2 > i3) {
                    i3 = d2;
                }
            }
            i4++;
            i2 = i2;
        }
        iArr[0] = i2;
        iArr[1] = i3;
    }

    private boolean a(MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        if (action == 3 || action == 0) {
            this.S = null;
        }
        int size = this.R.size();
        int i2 = 0;
        while (i2 < size) {
            l lVar = (l) this.R.get(i2);
            if (!lVar.a(this, motionEvent) || action == 3) {
                i2++;
            } else {
                this.S = lVar;
                return true;
            }
        }
        return false;
    }

    private boolean a(View view, View view2, int i2) {
        char c2 = 65535;
        boolean z2 = false;
        if (view2 == null || view2 == this) {
            return false;
        }
        if (view == null) {
            return true;
        }
        this.j.set(0, 0, view.getWidth(), view.getHeight());
        this.Q.set(0, 0, view2.getWidth(), view2.getHeight());
        offsetDescendantRectToMyCoords(view, this.j);
        offsetDescendantRectToMyCoords(view2, this.Q);
        int i3 = this.m.s() == 1 ? -1 : 1;
        int i4 = ((this.j.left < this.Q.left || this.j.right <= this.Q.left) && this.j.right < this.Q.right) ? 1 : ((this.j.right > this.Q.right || this.j.left >= this.Q.right) && this.j.left > this.Q.left) ? -1 : 0;
        if ((this.j.top < this.Q.top || this.j.bottom <= this.Q.top) && this.j.bottom < this.Q.bottom) {
            c2 = 1;
        } else if ((this.j.bottom <= this.Q.bottom && this.j.top < this.Q.bottom) || this.j.top <= this.Q.top) {
            c2 = 0;
        }
        switch (i2) {
            case 1:
                if (c2 < 0 || (c2 == 0 && i3 * i4 <= 0)) {
                    z2 = true;
                }
                return z2;
            case 2:
                if (c2 > 0 || (c2 == 0 && i3 * i4 >= 0)) {
                    z2 = true;
                }
                return z2;
            case 17:
                return i4 < 0;
            case 33:
                return c2 < 0;
            case 66:
                return i4 > 0;
            case 130:
                return c2 > 0;
            default:
                throw new IllegalArgumentException("Invalid direction: " + i2 + a());
        }
    }

    private boolean b(MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        if (this.S != null) {
            if (action == 0) {
                this.S = null;
            } else {
                this.S.b(this, motionEvent);
                if (action == 3 || action == 1) {
                    this.S = null;
                }
                return true;
            }
        }
        if (action != 0) {
            int size = this.R.size();
            for (int i2 = 0; i2 < size; i2++) {
                l lVar = (l) this.R.get(i2);
                if (lVar.a(this, motionEvent)) {
                    this.S = lVar;
                    return true;
                }
            }
        }
        return false;
    }

    static void c(w wVar) {
        if (wVar.f1103b != null) {
            View view = (View) wVar.f1103b.get();
            while (view != null) {
                if (view != wVar.f1102a) {
                    ViewParent parent = view.getParent();
                    view = parent instanceof View ? (View) parent : null;
                } else {
                    return;
                }
            }
            wVar.f1103b = null;
        }
    }

    private void c(MotionEvent motionEvent) {
        int actionIndex = motionEvent.getActionIndex();
        if (motionEvent.getPointerId(actionIndex) == this.ai) {
            int i2 = actionIndex == 0 ? 1 : 0;
            this.ai = motionEvent.getPointerId(i2);
            int x2 = (int) (motionEvent.getX(i2) + 0.5f);
            this.am = x2;
            this.ak = x2;
            int y2 = (int) (motionEvent.getY(i2) + 0.5f);
            this.an = y2;
            this.al = y2;
        }
    }

    static w e(View view) {
        if (view == null) {
            return null;
        }
        return ((i) view.getLayoutParams()).c;
    }

    private void e(w wVar) {
        View view = wVar.f1102a;
        boolean z2 = view.getParent() == this;
        this.d.c(b(view));
        if (wVar.r()) {
            this.f.a(view, -1, view.getLayoutParams(), true);
        } else if (!z2) {
            this.f.a(view, true);
        } else {
            this.f.d(view);
        }
    }

    private android.support.v4.i.l getScrollingChildHelper() {
        if (this.aA == null) {
            this.aA = new android.support.v4.i.l(this);
        }
        return this.aA;
    }

    static RecyclerView j(View view) {
        if (!(view instanceof ViewGroup)) {
            return null;
        }
        if (view instanceof RecyclerView) {
            return (RecyclerView) view;
        }
        ViewGroup viewGroup = (ViewGroup) view;
        int childCount = viewGroup.getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            RecyclerView j2 = j(viewGroup.getChildAt(i2));
            if (j2 != null) {
                return j2;
            }
        }
        return null;
    }

    private boolean k(int i2, int i3) {
        a(this.az);
        return (this.az[0] == i2 && this.az[1] == i3) ? false : true;
    }

    private int m(View view) {
        int i2;
        int id = view.getId();
        while (true) {
            i2 = id;
            View view2 = view;
            if (view2.isFocused() || !(view2 instanceof ViewGroup) || !view2.hasFocus()) {
                return i2;
            }
            view = ((ViewGroup) view2).getFocusedChild();
            id = view.getId() != -1 ? view.getId() : i2;
        }
        return i2;
    }

    /* access modifiers changed from: 0000 */
    public long a(w wVar) {
        return this.l.b() ? wVar.g() : (long) wVar.c;
    }

    /* access modifiers changed from: 0000 */
    public w a(int i2, boolean z2) {
        int c2 = this.f.c();
        w wVar = null;
        for (int i3 = 0; i3 < c2; i3++) {
            w e2 = e(this.f.d(i3));
            if (e2 != null && !e2.q()) {
                if (z2) {
                    if (e2.c != i2) {
                        continue;
                    }
                } else if (e2.d() != i2) {
                    continue;
                }
                if (!this.f.c(e2.f1102a)) {
                    return e2;
                }
                wVar = e2;
            }
        }
        return wVar;
    }

    public w a(long j2) {
        if (this.l == null || !this.l.b()) {
            return null;
        }
        int c2 = this.f.c();
        int i2 = 0;
        w wVar = null;
        while (i2 < c2) {
            w e2 = e(this.f.d(i2));
            if (e2 == null || e2.q() || e2.g() != j2) {
                e2 = wVar;
            } else if (!this.f.c(e2.f1102a)) {
                return e2;
            }
            i2++;
            wVar = e2;
        }
        return wVar;
    }

    public View a(float f2, float f3) {
        for (int b2 = this.f.b() - 1; b2 >= 0; b2--) {
            View b3 = this.f.b(b2);
            float translationX = b3.getTranslationX();
            float translationY = b3.getTranslationY();
            if (f2 >= ((float) b3.getLeft()) + translationX && f2 <= translationX + ((float) b3.getRight()) && f3 >= ((float) b3.getTop()) + translationY && f3 <= ((float) b3.getBottom()) + translationY) {
                return b3;
            }
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    public String a() {
        return " " + super.toString() + ", adapter:" + this.l + ", layout:" + this.m + ", context:" + getContext();
    }

    public void a(int i2) {
        if (!this.u) {
            f();
            if (this.m == null) {
                Log.e("RecyclerView", "Cannot scroll to position a LayoutManager set. Call setLayoutManager with a non-null argument.");
                return;
            }
            this.m.d(i2);
            awakenScrollBars();
        }
    }

    public void a(int i2, int i3) {
        a(i2, i3, (Interpolator) null);
    }

    public void a(int i2, int i3, Interpolator interpolator) {
        int i4 = 0;
        if (this.m == null) {
            Log.e("RecyclerView", "Cannot smooth scroll without a LayoutManager set. Call setLayoutManager with a non-null argument.");
        } else if (!this.u) {
            if (!this.m.d()) {
                i2 = 0;
            }
            if (this.m.e()) {
                i4 = i3;
            }
            if (i2 != 0 || i4 != 0) {
                this.y.a(i2, i4, interpolator);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(int i2, int i3, Object obj) {
        int c2 = this.f.c();
        int i4 = i2 + i3;
        for (int i5 = 0; i5 < c2; i5++) {
            View d2 = this.f.d(i5);
            w e2 = e(d2);
            if (e2 != null && !e2.c() && e2.c >= i2 && e2.c < i4) {
                e2.b(2);
                e2.a(obj);
                ((i) d2.getLayoutParams()).e = true;
            }
        }
        this.d.c(i2, i3);
    }

    /* access modifiers changed from: 0000 */
    public void a(int i2, int i3, boolean z2) {
        int i4 = i2 + i3;
        int c2 = this.f.c();
        for (int i5 = 0; i5 < c2; i5++) {
            w e2 = e(this.f.d(i5));
            if (e2 != null && !e2.c()) {
                if (e2.c >= i4) {
                    e2.a(-i3, z2);
                    this.B.e = true;
                } else if (e2.c >= i2) {
                    e2.a(i2 - 1, -i3, z2);
                    this.B.e = true;
                }
            }
        }
        this.d.a(i2, i3, z2);
        requestLayout();
    }

    /* access modifiers changed from: 0000 */
    public void a(StateListDrawable stateListDrawable, Drawable drawable, StateListDrawable stateListDrawable2, Drawable drawable2) {
        if (stateListDrawable == null || drawable == null || stateListDrawable2 == null || drawable2 == null) {
            throw new IllegalArgumentException("Trying to set fast scroller without both required drawables." + a());
        }
        Resources resources = getContext().getResources();
        new ao(this, stateListDrawable, drawable, stateListDrawable2, drawable2, resources.getDimensionPixelSize(C0030a.fastscroll_default_thickness), resources.getDimensionPixelSize(C0030a.fastscroll_minimum_range), resources.getDimensionPixelOffset(C0030a.fastscroll_margin));
    }

    public void a(g gVar) {
        a(gVar, -1);
    }

    public void a(g gVar, int i2) {
        if (this.m != null) {
            this.m.a("Cannot add item decoration during a scroll  or layout");
        }
        if (this.o.isEmpty()) {
            setWillNotDraw(false);
        }
        if (i2 < 0) {
            this.o.add(gVar);
        } else {
            this.o.add(i2, gVar);
        }
        r();
        requestLayout();
    }

    public void a(j jVar) {
        if (this.aa == null) {
            this.aa = new ArrayList();
        }
        this.aa.add(jVar);
    }

    public void a(l lVar) {
        this.R.add(lVar);
    }

    public void a(m mVar) {
        if (this.aw == null) {
            this.aw = new ArrayList();
        }
        this.aw.add(mVar);
    }

    /* access modifiers changed from: 0000 */
    public final void a(t tVar) {
        if (getScrollState() == 2) {
            OverScroller a2 = this.y.e;
            tVar.n = a2.getFinalX() - a2.getCurrX();
            tVar.o = a2.getFinalY() - a2.getCurrY();
            return;
        }
        tVar.n = 0;
        tVar.o = 0;
    }

    /* access modifiers changed from: 0000 */
    public void a(w wVar, c cVar) {
        wVar.a(0, 8192);
        if (this.B.g && wVar.x() && !wVar.q() && !wVar.c()) {
            this.g.a(a(wVar), wVar);
        }
        this.g.a(wVar, cVar);
    }

    /* access modifiers changed from: 0000 */
    public void a(w wVar, c cVar, c cVar2) {
        wVar.a(false);
        if (this.x.b(wVar, cVar, cVar2)) {
            p();
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(String str) {
        if (o()) {
            if (str == null) {
                throw new IllegalStateException("Cannot call this method while RecyclerView is computing a layout or scrolling" + a());
            }
            throw new IllegalStateException(str);
        } else if (this.ac > 0) {
            Log.w("RecyclerView", "Cannot call this method in a scroll callback. Scroll callbacks mightbe run during a measure & layout pass where you cannot change theRecyclerView data. Any method call that might change the structureof the RecyclerView or the adapter contents should be postponed tothe next frame.", new IllegalStateException("" + a()));
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(boolean z2) {
        if (this.T < 1) {
            this.T = 1;
        }
        if (!z2) {
            this.t = false;
        }
        if (this.T == 1) {
            if (z2 && this.t && !this.u && this.m != null && this.l != null) {
                q();
            }
            if (!this.u) {
                this.t = false;
            }
        }
        this.T--;
    }

    public boolean a(int i2, int i3, int i4, int i5, int[] iArr, int i6) {
        return getScrollingChildHelper().a(i2, i3, i4, i5, iArr, i6);
    }

    /* access modifiers changed from: 0000 */
    public boolean a(int i2, int i3, MotionEvent motionEvent) {
        int i4;
        int i5;
        int i6;
        int i7;
        int i8;
        int i9;
        int i10;
        d();
        if (this.l != null) {
            e();
            l();
            android.support.v4.f.d.a("RV Scroll");
            a(this.B);
            if (i2 != 0) {
                i8 = this.m.a(i2, this.d, this.B);
                i7 = i2 - i8;
            } else {
                i8 = 0;
                i7 = 0;
            }
            if (i3 != 0) {
                i9 = this.m.b(i3, this.d, this.B);
                i10 = i3 - i9;
            } else {
                i9 = 0;
                i10 = 0;
            }
            android.support.v4.f.d.a();
            x();
            m();
            a(false);
            i6 = i10;
            i5 = i8;
            i4 = i9;
        } else {
            i4 = 0;
            i5 = 0;
            i6 = 0;
            i7 = 0;
        }
        if (!this.o.isEmpty()) {
            invalidate();
        }
        if (a(i5, i4, i7, i6, this.aB, 0)) {
            this.am -= this.aB[0];
            this.an -= this.aB[1];
            if (motionEvent != null) {
                motionEvent.offsetLocation((float) this.aB[0], (float) this.aB[1]);
            }
            int[] iArr = this.aD;
            iArr[0] = iArr[0] + this.aB[0];
            int[] iArr2 = this.aD;
            iArr2[1] = iArr2[1] + this.aB[1];
        } else if (getOverScrollMode() != 2) {
            if (motionEvent != null && !android.support.v4.i.i.a(motionEvent, 8194)) {
                a(motionEvent.getX(), (float) i7, motionEvent.getY(), (float) i6);
            }
            c(i2, i3);
        }
        if (!(i5 == 0 && i4 == 0)) {
            i(i5, i4);
        }
        if (!awakenScrollBars()) {
            invalidate();
        }
        return (i5 == 0 && i4 == 0) ? false : true;
    }

    public boolean a(int i2, int i3, int[] iArr, int[] iArr2, int i4) {
        return getScrollingChildHelper().a(i2, i3, iArr, iArr2, i4);
    }

    /* access modifiers changed from: 0000 */
    public boolean a(w wVar, int i2) {
        if (o()) {
            wVar.l = i2;
            this.G.add(wVar);
            return false;
        }
        android.support.v4.i.t.a(wVar.f1102a, i2);
        return true;
    }

    /* access modifiers changed from: 0000 */
    public boolean a(View view) {
        e();
        boolean f2 = this.f.f(view);
        if (f2) {
            w e2 = e(view);
            this.d.c(e2);
            this.d.b(e2);
        }
        a(!f2);
        return f2;
    }

    /* access modifiers changed from: 0000 */
    public boolean a(AccessibilityEvent accessibilityEvent) {
        int i2 = 0;
        if (!o()) {
            return false;
        }
        int i3 = accessibilityEvent != null ? android.support.v4.i.a.a.a(accessibilityEvent) : 0;
        if (i3 != 0) {
            i2 = i3;
        }
        this.V = i2 | this.V;
        return true;
    }

    public void addFocusables(ArrayList<View> arrayList, int i2, int i3) {
        if (this.m == null || !this.m.a(this, arrayList, i2, i3)) {
            super.addFocusables(arrayList, i2, i3);
        }
    }

    public w b(View view) {
        ViewParent parent = view.getParent();
        if (parent == null || parent == this) {
            return e(view);
        }
        throw new IllegalArgumentException("View " + view + " is not a direct child of " + this);
    }

    /* access modifiers changed from: 0000 */
    public void b() {
        this.e = new f(new a() {
            public w a(int i) {
                w a2 = RecyclerView.this.a(i, true);
                if (a2 != null && !RecyclerView.this.f.c(a2.f1102a)) {
                    return a2;
                }
                return null;
            }

            public void a(int i, int i2) {
                RecyclerView.this.a(i, i2, true);
                RecyclerView.this.C = true;
                RecyclerView.this.B.f1099b += i2;
            }

            public void a(int i, int i2, Object obj) {
                RecyclerView.this.a(i, i2, obj);
                RecyclerView.this.D = true;
            }

            public void a(b bVar) {
                c(bVar);
            }

            public void b(int i, int i2) {
                RecyclerView.this.a(i, i2, false);
                RecyclerView.this.C = true;
            }

            public void b(b bVar) {
                c(bVar);
            }

            public void c(int i, int i2) {
                RecyclerView.this.g(i, i2);
                RecyclerView.this.C = true;
            }

            /* access modifiers changed from: 0000 */
            public void c(b bVar) {
                switch (bVar.f1308a) {
                    case 1:
                        RecyclerView.this.m.a(RecyclerView.this, bVar.f1309b, bVar.d);
                        return;
                    case 2:
                        RecyclerView.this.m.b(RecyclerView.this, bVar.f1309b, bVar.d);
                        return;
                    case 4:
                        RecyclerView.this.m.a(RecyclerView.this, bVar.f1309b, bVar.d, bVar.c);
                        return;
                    case 8:
                        RecyclerView.this.m.a(RecyclerView.this, bVar.f1309b, bVar.d, 1);
                        return;
                    default:
                        return;
                }
            }

            public void d(int i, int i2) {
                RecyclerView.this.f(i, i2);
                RecyclerView.this.C = true;
            }
        });
    }

    /* access modifiers changed from: 0000 */
    public void b(int i2) {
        if (this.m != null) {
            this.m.d(i2);
            awakenScrollBars();
        }
    }

    public void b(g gVar) {
        if (this.m != null) {
            this.m.a("Cannot remove item decoration during a scroll  or layout");
        }
        this.o.remove(gVar);
        if (this.o.isEmpty()) {
            setWillNotDraw(getOverScrollMode() == 2);
        }
        r();
        requestLayout();
    }

    public void b(j jVar) {
        if (this.aa != null) {
            this.aa.remove(jVar);
        }
    }

    public void b(l lVar) {
        this.R.remove(lVar);
        if (this.S == lVar) {
            this.S = null;
        }
    }

    public void b(m mVar) {
        if (this.aw != null) {
            this.aw.remove(mVar);
        }
    }

    /* access modifiers changed from: 0000 */
    public void b(w wVar, c cVar, c cVar2) {
        e(wVar);
        wVar.a(false);
        if (this.x.a(wVar, cVar, cVar2)) {
            p();
        }
    }

    /* access modifiers changed from: 0000 */
    public void b(boolean z2) {
        this.ab--;
        if (this.ab < 1) {
            this.ab = 0;
            if (z2) {
                G();
                y();
            }
        }
    }

    public boolean b(int i2, int i3) {
        if (this.m == null) {
            Log.e("RecyclerView", "Cannot fling without a LayoutManager set. Call setLayoutManager with a non-null argument.");
            return false;
        } else if (this.u) {
            return false;
        } else {
            boolean d2 = this.m.d();
            boolean e2 = this.m.e();
            if (!d2 || Math.abs(i2) < this.aq) {
                i2 = 0;
            }
            if (!e2 || Math.abs(i3) < this.aq) {
                i3 = 0;
            }
            if ((i2 == 0 && i3 == 0) || dispatchNestedPreFling((float) i2, (float) i3)) {
                return false;
            }
            boolean z2 = d2 || e2;
            dispatchNestedFling((float) i2, (float) i3, z2);
            if (this.ap != null && this.ap.a(i2, i3)) {
                return true;
            }
            if (!z2) {
                return false;
            }
            int i4 = d2 ? 1 : 0;
            if (e2) {
                i4 |= 2;
            }
            j(i4, 1);
            this.y.a(Math.max(-this.ar, Math.min(i2, this.ar)), Math.max(-this.ar, Math.min(i3, this.ar)));
            return true;
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean b(w wVar) {
        return this.x == null || this.x.a(wVar, wVar.u());
    }

    public w c(int i2) {
        if (this.w) {
            return null;
        }
        int c2 = this.f.c();
        int i3 = 0;
        w wVar = null;
        while (i3 < c2) {
            w e2 = e(this.f.d(i3));
            if (e2 == null || e2.q() || d(e2) != i2) {
                e2 = wVar;
            } else if (!this.f.c(e2.f1102a)) {
                return e2;
            }
            i3++;
            wVar = e2;
        }
        return wVar;
    }

    public View c(View view) {
        ViewParent parent = view.getParent();
        View view2 = view;
        while (parent != null && parent != this && (parent instanceof View)) {
            View view3 = (View) parent;
            view2 = view3;
            parent = view3.getParent();
        }
        if (parent == this) {
            return view2;
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    public void c() {
        if (this.x != null) {
            this.x.d();
        }
        if (this.m != null) {
            this.m.c(this.d);
            this.m.b(this.d);
        }
        this.d.a();
    }

    /* access modifiers changed from: 0000 */
    public void c(int i2, int i3) {
        boolean z2 = false;
        if (this.ad != null && !this.ad.isFinished() && i2 > 0) {
            this.ad.onRelease();
            z2 = this.ad.isFinished();
        }
        if (this.af != null && !this.af.isFinished() && i2 < 0) {
            this.af.onRelease();
            z2 |= this.af.isFinished();
        }
        if (this.ae != null && !this.ae.isFinished() && i3 > 0) {
            this.ae.onRelease();
            z2 |= this.ae.isFinished();
        }
        if (this.ag != null && !this.ag.isFinished() && i3 < 0) {
            this.ag.onRelease();
            z2 |= this.ag.isFinished();
        }
        if (z2) {
            android.support.v4.i.t.c(this);
        }
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(LayoutParams layoutParams) {
        return (layoutParams instanceof i) && this.m.a((i) layoutParams);
    }

    public int computeHorizontalScrollExtent() {
        if (this.m != null && this.m.d()) {
            return this.m.e(this.B);
        }
        return 0;
    }

    public int computeHorizontalScrollOffset() {
        if (this.m != null && this.m.d()) {
            return this.m.c(this.B);
        }
        return 0;
    }

    public int computeHorizontalScrollRange() {
        if (this.m != null && this.m.d()) {
            return this.m.g(this.B);
        }
        return 0;
    }

    public int computeVerticalScrollExtent() {
        if (this.m != null && this.m.e()) {
            return this.m.f(this.B);
        }
        return 0;
    }

    public int computeVerticalScrollOffset() {
        if (this.m != null && this.m.e()) {
            return this.m.d(this.B);
        }
        return 0;
    }

    public int computeVerticalScrollRange() {
        if (this.m != null && this.m.e()) {
            return this.m.h(this.B);
        }
        return 0;
    }

    /* access modifiers changed from: 0000 */
    public int d(w wVar) {
        if (wVar.a(524) || !wVar.p()) {
            return -1;
        }
        return this.e.c(wVar.c);
    }

    public w d(View view) {
        View c2 = c(view);
        if (c2 == null) {
            return null;
        }
        return b(c2);
    }

    /* access modifiers changed from: 0000 */
    public void d() {
        if (!this.s || this.w) {
            android.support.v4.f.d.a("RV FullInvalidate");
            q();
            android.support.v4.f.d.a();
        } else if (!this.e.d()) {
        } else {
            if (this.e.a(4) && !this.e.a(11)) {
                android.support.v4.f.d.a("RV PartialInvalidate");
                e();
                l();
                this.e.b();
                if (!this.t) {
                    if (B()) {
                        q();
                    } else {
                        this.e.c();
                    }
                }
                a(true);
                m();
                android.support.v4.f.d.a();
            } else if (this.e.d()) {
                android.support.v4.f.d.a("RV FullInvalidate");
                q();
                android.support.v4.f.d.a();
            }
        }
    }

    public void d(int i2) {
        int b2 = this.f.b();
        for (int i3 = 0; i3 < b2; i3++) {
            this.f.b(i3).offsetTopAndBottom(i2);
        }
    }

    /* access modifiers changed from: 0000 */
    public void d(int i2, int i3) {
        if (i2 < 0) {
            g();
            this.ad.onAbsorb(-i2);
        } else if (i2 > 0) {
            h();
            this.af.onAbsorb(i2);
        }
        if (i3 < 0) {
            i();
            this.ae.onAbsorb(-i3);
        } else if (i3 > 0) {
            j();
            this.ag.onAbsorb(i3);
        }
        if (i2 != 0 || i3 != 0) {
            android.support.v4.i.t.c(this);
        }
    }

    public boolean dispatchNestedFling(float f2, float f3, boolean z2) {
        return getScrollingChildHelper().a(f2, f3, z2);
    }

    public boolean dispatchNestedPreFling(float f2, float f3) {
        return getScrollingChildHelper().a(f2, f3);
    }

    public boolean dispatchNestedPreScroll(int i2, int i3, int[] iArr, int[] iArr2) {
        return getScrollingChildHelper().a(i2, i3, iArr, iArr2);
    }

    public boolean dispatchNestedScroll(int i2, int i3, int i4, int i5, int[] iArr) {
        return getScrollingChildHelper().a(i2, i3, i4, i5, iArr);
    }

    /* access modifiers changed from: protected */
    public void dispatchRestoreInstanceState(SparseArray<Parcelable> sparseArray) {
        dispatchThawSelfOnly(sparseArray);
    }

    /* access modifiers changed from: protected */
    public void dispatchSaveInstanceState(SparseArray<Parcelable> sparseArray) {
        dispatchFreezeSelfOnly(sparseArray);
    }

    public void draw(Canvas canvas) {
        boolean z2;
        boolean z3 = true;
        boolean z4 = false;
        super.draw(canvas);
        int size = this.o.size();
        for (int i2 = 0; i2 < size; i2++) {
            ((g) this.o.get(i2)).a(canvas, this, this.B);
        }
        if (this.ad == null || this.ad.isFinished()) {
            z2 = false;
        } else {
            int save = canvas.save();
            int i3 = this.h ? getPaddingBottom() : 0;
            canvas.rotate(270.0f);
            canvas.translate((float) (i3 + (-getHeight())), 0.0f);
            z2 = this.ad != null && this.ad.draw(canvas);
            canvas.restoreToCount(save);
        }
        if (this.ae != null && !this.ae.isFinished()) {
            int save2 = canvas.save();
            if (this.h) {
                canvas.translate((float) getPaddingLeft(), (float) getPaddingTop());
            }
            z2 |= this.ae != null && this.ae.draw(canvas);
            canvas.restoreToCount(save2);
        }
        if (this.af != null && !this.af.isFinished()) {
            int save3 = canvas.save();
            int width = getWidth();
            int i4 = this.h ? getPaddingTop() : 0;
            canvas.rotate(90.0f);
            canvas.translate((float) (-i4), (float) (-width));
            z2 |= this.af != null && this.af.draw(canvas);
            canvas.restoreToCount(save3);
        }
        if (this.ag != null && !this.ag.isFinished()) {
            int save4 = canvas.save();
            canvas.rotate(180.0f);
            if (this.h) {
                canvas.translate((float) ((-getWidth()) + getPaddingRight()), (float) ((-getHeight()) + getPaddingBottom()));
            } else {
                canvas.translate((float) (-getWidth()), (float) (-getHeight()));
            }
            if (this.ag != null && this.ag.draw(canvas)) {
                z4 = true;
            }
            z2 |= z4;
            canvas.restoreToCount(save4);
        }
        if (z2 || this.x == null || this.o.size() <= 0 || !this.x.b()) {
            z3 = z2;
        }
        if (z3) {
            android.support.v4.i.t.c(this);
        }
    }

    public boolean drawChild(Canvas canvas, View view, long j2) {
        return super.drawChild(canvas, view, j2);
    }

    /* access modifiers changed from: 0000 */
    public void e() {
        this.T++;
        if (this.T == 1 && !this.u) {
            this.t = false;
        }
    }

    public void e(int i2) {
        int b2 = this.f.b();
        for (int i3 = 0; i3 < b2; i3++) {
            this.f.b(i3).offsetLeftAndRight(i2);
        }
    }

    /* access modifiers changed from: 0000 */
    public void e(int i2, int i3) {
        setMeasuredDimension(h.a(i2, getPaddingLeft() + getPaddingRight(), android.support.v4.i.t.h(this)), h.a(i3, getPaddingTop() + getPaddingBottom(), android.support.v4.i.t.i(this)));
    }

    public int f(View view) {
        w e2 = e(view);
        if (e2 != null) {
            return e2.d();
        }
        return -1;
    }

    public void f() {
        setScrollState(0);
        C();
    }

    public void f(int i2) {
    }

    /* access modifiers changed from: 0000 */
    public void f(int i2, int i3) {
        int i4;
        int i5;
        int i6;
        int c2 = this.f.c();
        if (i2 < i3) {
            i4 = -1;
            i5 = i3;
            i6 = i2;
        } else {
            i4 = 1;
            i5 = i2;
            i6 = i3;
        }
        for (int i7 = 0; i7 < c2; i7++) {
            w e2 = e(this.f.d(i7));
            if (e2 != null && e2.c >= i6 && e2.c <= i5) {
                if (e2.c == i2) {
                    e2.a(i3 - i2, false);
                } else {
                    e2.a(i4, false);
                }
                this.B.e = true;
            }
        }
        this.d.a(i2, i3);
        requestLayout();
    }

    public View focusSearch(View view, int i2) {
        View view2;
        boolean z2;
        boolean z3 = true;
        View d2 = this.m.d(view, i2);
        if (d2 != null) {
            return d2;
        }
        boolean z4 = this.l != null && this.m != null && !o() && !this.u;
        FocusFinder instance = FocusFinder.getInstance();
        if (!z4 || !(i2 == 2 || i2 == 1)) {
            View findNextFocus = instance.findNextFocus(this, view, i2);
            if (findNextFocus != null || !z4) {
                view2 = findNextFocus;
            } else {
                d();
                if (c(view) == null) {
                    return null;
                }
                e();
                view2 = this.m.a(view, i2, this.d, this.B);
                a(false);
            }
        } else {
            if (this.m.e()) {
                int i3 = i2 == 2 ? 130 : 33;
                boolean z5 = instance.findNextFocus(this, view, i3) == null;
                if (L) {
                    i2 = i3;
                    z2 = z5;
                } else {
                    z2 = z5;
                }
            } else {
                z2 = false;
            }
            if (z2 || !this.m.d()) {
                z3 = z2;
            } else {
                int i4 = (i2 == 2) ^ (this.m.s() == 1) ? 66 : 17;
                if (instance.findNextFocus(this, view, i4) != null) {
                    z3 = false;
                }
                if (L) {
                    i2 = i4;
                }
            }
            if (z3) {
                d();
                if (c(view) == null) {
                    return null;
                }
                e();
                this.m.a(view, i2, this.d, this.B);
                a(false);
            }
            view2 = instance.findNextFocus(this, view, i2);
        }
        if (view2 == null || view2.hasFocusable()) {
            if (!a(view, view2, i2)) {
                view2 = super.focusSearch(view, i2);
            }
            return view2;
        } else if (getFocusedChild() == null) {
            return super.focusSearch(view, i2);
        } else {
            a(view2, (View) null);
            return view;
        }
    }

    /* access modifiers changed from: 0000 */
    public void g() {
        if (this.ad == null) {
            this.ad = new EdgeEffect(getContext());
            if (this.h) {
                this.ad.setSize((getMeasuredHeight() - getPaddingTop()) - getPaddingBottom(), (getMeasuredWidth() - getPaddingLeft()) - getPaddingRight());
            } else {
                this.ad.setSize(getMeasuredHeight(), getMeasuredWidth());
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void g(int i2) {
        if (this.m != null) {
            this.m.k(i2);
        }
        f(i2);
        if (this.av != null) {
            this.av.a(this, i2);
        }
        if (this.aw != null) {
            for (int size = this.aw.size() - 1; size >= 0; size--) {
                ((m) this.aw.get(size)).a(this, i2);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void g(int i2, int i3) {
        int c2 = this.f.c();
        for (int i4 = 0; i4 < c2; i4++) {
            w e2 = e(this.f.d(i4));
            if (e2 != null && !e2.c() && e2.c >= i2) {
                e2.a(i3, false);
                this.B.e = true;
            }
        }
        this.d.b(i2, i3);
        requestLayout();
    }

    public void g(View view) {
    }

    /* access modifiers changed from: protected */
    public LayoutParams generateDefaultLayoutParams() {
        if (this.m != null) {
            return this.m.a();
        }
        throw new IllegalStateException("RecyclerView has no LayoutManager" + a());
    }

    public LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        if (this.m != null) {
            return this.m.a(getContext(), attributeSet);
        }
        throw new IllegalStateException("RecyclerView has no LayoutManager" + a());
    }

    /* access modifiers changed from: protected */
    public LayoutParams generateLayoutParams(LayoutParams layoutParams) {
        if (this.m != null) {
            return this.m.a(layoutParams);
        }
        throw new IllegalStateException("RecyclerView has no LayoutManager" + a());
    }

    public a getAdapter() {
        return this.l;
    }

    public int getBaseline() {
        return this.m != null ? this.m.t() : super.getBaseline();
    }

    /* access modifiers changed from: protected */
    public int getChildDrawingOrder(int i2, int i3) {
        return this.ay == null ? super.getChildDrawingOrder(i2, i3) : this.ay.a(i2, i3);
    }

    public boolean getClipToPadding() {
        return this.h;
    }

    public ba getCompatAccessibilityDelegate() {
        return this.F;
    }

    public e getItemAnimator() {
        return this.x;
    }

    public int getItemDecorationCount() {
        return this.o.size();
    }

    public h getLayoutManager() {
        return this.m;
    }

    public int getMaxFlingVelocity() {
        return this.ar;
    }

    public int getMinFlingVelocity() {
        return this.aq;
    }

    /* access modifiers changed from: 0000 */
    public long getNanoTime() {
        if (K) {
            return System.nanoTime();
        }
        return 0;
    }

    public k getOnFlingListener() {
        return this.ap;
    }

    public boolean getPreserveFocusAfterLayout() {
        return this.au;
    }

    public n getRecycledViewPool() {
        return this.d.g();
    }

    public int getScrollState() {
        return this.ah;
    }

    /* access modifiers changed from: 0000 */
    public void h() {
        if (this.af == null) {
            this.af = new EdgeEffect(getContext());
            if (this.h) {
                this.af.setSize((getMeasuredHeight() - getPaddingTop()) - getPaddingBottom(), (getMeasuredWidth() - getPaddingLeft()) - getPaddingRight());
            } else {
                this.af.setSize(getMeasuredHeight(), getMeasuredWidth());
            }
        }
    }

    public void h(int i2) {
        getScrollingChildHelper().c(i2);
    }

    public void h(int i2, int i3) {
    }

    public void h(View view) {
    }

    public boolean hasNestedScrollingParent() {
        return getScrollingChildHelper().b();
    }

    /* access modifiers changed from: 0000 */
    public Rect i(View view) {
        i iVar = (i) view.getLayoutParams();
        if (!iVar.e) {
            return iVar.d;
        }
        if (this.B.a() && (iVar.e() || iVar.c())) {
            return iVar.d;
        }
        Rect rect = iVar.d;
        rect.set(0, 0, 0, 0);
        int size = this.o.size();
        for (int i2 = 0; i2 < size; i2++) {
            this.j.set(0, 0, 0, 0);
            ((g) this.o.get(i2)).a(this.j, view, this, this.B);
            rect.left += this.j.left;
            rect.top += this.j.top;
            rect.right += this.j.right;
            rect.bottom += this.j.bottom;
        }
        iVar.e = false;
        return rect;
    }

    /* access modifiers changed from: 0000 */
    public void i() {
        if (this.ae == null) {
            this.ae = new EdgeEffect(getContext());
            if (this.h) {
                this.ae.setSize((getMeasuredWidth() - getPaddingLeft()) - getPaddingRight(), (getMeasuredHeight() - getPaddingTop()) - getPaddingBottom());
            } else {
                this.ae.setSize(getMeasuredWidth(), getMeasuredHeight());
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void i(int i2, int i3) {
        this.ac++;
        int scrollX = getScrollX();
        int scrollY = getScrollY();
        onScrollChanged(scrollX, scrollY, scrollX, scrollY);
        h(i2, i3);
        if (this.av != null) {
            this.av.a(this, i2, i3);
        }
        if (this.aw != null) {
            for (int size = this.aw.size() - 1; size >= 0; size--) {
                ((m) this.aw.get(size)).a(this, i2, i3);
            }
        }
        this.ac--;
    }

    public boolean i(int i2) {
        return getScrollingChildHelper().a(i2);
    }

    public boolean isAttachedToWindow() {
        return this.p;
    }

    public boolean isNestedScrollingEnabled() {
        return getScrollingChildHelper().a();
    }

    /* access modifiers changed from: 0000 */
    public void j() {
        if (this.ag == null) {
            this.ag = new EdgeEffect(getContext());
            if (this.h) {
                this.ag.setSize((getMeasuredWidth() - getPaddingLeft()) - getPaddingRight(), (getMeasuredHeight() - getPaddingTop()) - getPaddingBottom());
            } else {
                this.ag.setSize(getMeasuredWidth(), getMeasuredHeight());
            }
        }
    }

    public boolean j(int i2, int i3) {
        return getScrollingChildHelper().a(i2, i3);
    }

    /* access modifiers changed from: 0000 */
    public void k() {
        this.ag = null;
        this.ae = null;
        this.af = null;
        this.ad = null;
    }

    /* access modifiers changed from: 0000 */
    public void k(View view) {
        w e2 = e(view);
        h(view);
        if (!(this.l == null || e2 == null)) {
            this.l.d(e2);
        }
        if (this.aa != null) {
            for (int size = this.aa.size() - 1; size >= 0; size--) {
                ((j) this.aa.get(size)).b(view);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void l() {
        this.ab++;
    }

    /* access modifiers changed from: 0000 */
    public void l(View view) {
        w e2 = e(view);
        g(view);
        if (!(this.l == null || e2 == null)) {
            this.l.c(e2);
        }
        if (this.aa != null) {
            for (int size = this.aa.size() - 1; size >= 0; size--) {
                ((j) this.aa.get(size)).a(view);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void m() {
        b(true);
    }

    /* access modifiers changed from: 0000 */
    public boolean n() {
        return this.W != null && this.W.isEnabled();
    }

    public boolean o() {
        return this.ab > 0;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x004f, code lost:
        if (r0 >= 30.0f) goto L_0x0051;
     */
    public void onAttachedToWindow() {
        float f2;
        boolean z2 = true;
        super.onAttachedToWindow();
        this.ab = 0;
        this.p = true;
        if (!this.s || isLayoutRequested()) {
            z2 = false;
        }
        this.s = z2;
        if (this.m != null) {
            this.m.c(this);
        }
        this.E = false;
        if (K) {
            this.z = (ar) ar.f1209a.get();
            if (this.z == null) {
                this.z = new ar();
                Display A2 = android.support.v4.i.t.A(this);
                if (!isInEditMode() && A2 != null) {
                    f2 = A2.getRefreshRate();
                }
                f2 = 60.0f;
                this.z.d = (long) (1.0E9f / f2);
                ar.f1209a.set(this.z);
            }
            this.z.a(this);
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.x != null) {
            this.x.d();
        }
        f();
        this.p = false;
        if (this.m != null) {
            this.m.b(this, this.d);
        }
        this.G.clear();
        removeCallbacks(this.aE);
        this.g.b();
        if (K) {
            this.z.b(this);
            this.z = null;
        }
    }

    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int size = this.o.size();
        for (int i2 = 0; i2 < size; i2++) {
            ((g) this.o.get(i2)).b(canvas, this, this.B);
        }
    }

    public boolean onGenericMotionEvent(MotionEvent motionEvent) {
        float f2;
        float f3;
        if (this.m != null && !this.u && motionEvent.getAction() == 8) {
            if ((motionEvent.getSource() & 2) != 0) {
                float f4 = this.m.e() ? -motionEvent.getAxisValue(9) : 0.0f;
                if (this.m.d()) {
                    f3 = f4;
                    f2 = motionEvent.getAxisValue(10);
                } else {
                    f3 = f4;
                    f2 = 0.0f;
                }
            } else if ((motionEvent.getSource() & 4194304) != 0) {
                f2 = motionEvent.getAxisValue(26);
                if (this.m.e()) {
                    f3 = -f2;
                    f2 = 0.0f;
                } else if (this.m.d()) {
                    f3 = 0.0f;
                } else {
                    f2 = 0.0f;
                    f3 = 0.0f;
                }
            } else {
                f2 = 0.0f;
                f3 = 0.0f;
            }
            if (!(f3 == 0.0f && f2 == 0.0f)) {
                a((int) (f2 * this.as), (int) (this.at * f3), motionEvent);
            }
        }
        return false;
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        boolean z2;
        if (this.u) {
            return false;
        }
        if (a(motionEvent)) {
            F();
            return true;
        } else if (this.m == null) {
            return false;
        } else {
            boolean d2 = this.m.d();
            boolean e2 = this.m.e();
            if (this.aj == null) {
                this.aj = VelocityTracker.obtain();
            }
            this.aj.addMovement(motionEvent);
            int actionMasked = motionEvent.getActionMasked();
            int actionIndex = motionEvent.getActionIndex();
            switch (actionMasked) {
                case 0:
                    if (this.U) {
                        this.U = false;
                    }
                    this.ai = motionEvent.getPointerId(0);
                    int x2 = (int) (motionEvent.getX() + 0.5f);
                    this.am = x2;
                    this.ak = x2;
                    int y2 = (int) (motionEvent.getY() + 0.5f);
                    this.an = y2;
                    this.al = y2;
                    if (this.ah == 2) {
                        getParent().requestDisallowInterceptTouchEvent(true);
                        setScrollState(1);
                    }
                    int[] iArr = this.aD;
                    this.aD[1] = 0;
                    iArr[0] = 0;
                    int i2 = d2 ? 1 : 0;
                    if (e2) {
                        i2 |= 2;
                    }
                    j(i2, 0);
                    break;
                case 1:
                    this.aj.clear();
                    h(0);
                    break;
                case 2:
                    int findPointerIndex = motionEvent.findPointerIndex(this.ai);
                    if (findPointerIndex >= 0) {
                        int x3 = (int) (motionEvent.getX(findPointerIndex) + 0.5f);
                        int y3 = (int) (motionEvent.getY(findPointerIndex) + 0.5f);
                        if (this.ah != 1) {
                            int i3 = x3 - this.ak;
                            int i4 = y3 - this.al;
                            if (!d2 || Math.abs(i3) <= this.ao) {
                                z2 = false;
                            } else {
                                this.am = x3;
                                z2 = true;
                            }
                            if (e2 && Math.abs(i4) > this.ao) {
                                this.an = y3;
                                z2 = true;
                            }
                            if (z2) {
                                setScrollState(1);
                                break;
                            }
                        }
                    } else {
                        Log.e("RecyclerView", "Error processing scroll; pointer index for id " + this.ai + " not found. Did any MotionEvents get skipped?");
                        return false;
                    }
                    break;
                case 3:
                    F();
                    break;
                case 5:
                    this.ai = motionEvent.getPointerId(actionIndex);
                    int x4 = (int) (motionEvent.getX(actionIndex) + 0.5f);
                    this.am = x4;
                    this.ak = x4;
                    int y4 = (int) (motionEvent.getY(actionIndex) + 0.5f);
                    this.an = y4;
                    this.al = y4;
                    break;
                case 6:
                    c(motionEvent);
                    break;
            }
            return this.ah == 1;
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z2, int i2, int i3, int i4, int i5) {
        android.support.v4.f.d.a("RV OnLayout");
        q();
        android.support.v4.f.d.a();
        this.s = true;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        boolean z2 = false;
        if (this.m == null) {
            e(i2, i3);
        } else if (this.m.w) {
            int mode = MeasureSpec.getMode(i2);
            int mode2 = MeasureSpec.getMode(i3);
            if (mode == 1073741824 && mode2 == 1073741824) {
                z2 = true;
            }
            this.m.a(this.d, this.B, i2, i3);
            if (!z2 && this.l != null) {
                if (this.B.c == 1) {
                    N();
                }
                this.m.d(i2, i3);
                this.B.h = true;
                O();
                this.m.e(i2, i3);
                if (this.m.k()) {
                    this.m.d(MeasureSpec.makeMeasureSpec(getMeasuredWidth(), 1073741824), MeasureSpec.makeMeasureSpec(getMeasuredHeight(), 1073741824));
                    this.B.h = true;
                    O();
                    this.m.e(i2, i3);
                }
            }
        } else if (this.q) {
            this.m.a(this.d, this.B, i2, i3);
        } else {
            if (this.v) {
                e();
                l();
                I();
                m();
                if (this.B.j) {
                    this.B.f = true;
                } else {
                    this.e.e();
                    this.B.f = false;
                }
                this.v = false;
                a(false);
            } else if (this.B.j) {
                setMeasuredDimension(getMeasuredWidth(), getMeasuredHeight());
                return;
            }
            if (this.l != null) {
                this.B.d = this.l.a();
            } else {
                this.B.d = 0;
            }
            e();
            this.m.a(this.d, this.B, i2, i3);
            a(false);
            this.B.f = false;
        }
    }

    /* access modifiers changed from: protected */
    public boolean onRequestFocusInDescendants(int i2, Rect rect) {
        if (o()) {
            return false;
        }
        return super.onRequestFocusInDescendants(i2, rect);
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (!(parcelable instanceof r)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        this.P = (r) parcelable;
        super.onRestoreInstanceState(this.P.a());
        if (this.m != null && this.P.f1093a != null) {
            this.m.a(this.P.f1093a);
        }
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        r rVar = new r(super.onSaveInstanceState());
        if (this.P != null) {
            rVar.a(this.P);
        } else if (this.m != null) {
            rVar.f1093a = this.m.c();
        } else {
            rVar.f1093a = null;
        }
        return rVar;
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        if (i2 != i4 || i3 != i5) {
            k();
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        boolean z2;
        boolean z3 = false;
        if (this.u || this.U) {
            return false;
        }
        if (b(motionEvent)) {
            F();
            return true;
        } else if (this.m == null) {
            return false;
        } else {
            boolean d2 = this.m.d();
            boolean e2 = this.m.e();
            if (this.aj == null) {
                this.aj = VelocityTracker.obtain();
            }
            MotionEvent obtain = MotionEvent.obtain(motionEvent);
            int actionMasked = motionEvent.getActionMasked();
            int actionIndex = motionEvent.getActionIndex();
            if (actionMasked == 0) {
                int[] iArr = this.aD;
                this.aD[1] = 0;
                iArr[0] = 0;
            }
            obtain.offsetLocation((float) this.aD[0], (float) this.aD[1]);
            switch (actionMasked) {
                case 0:
                    this.ai = motionEvent.getPointerId(0);
                    int x2 = (int) (motionEvent.getX() + 0.5f);
                    this.am = x2;
                    this.ak = x2;
                    int y2 = (int) (motionEvent.getY() + 0.5f);
                    this.an = y2;
                    this.al = y2;
                    int i2 = d2 ? 1 : 0;
                    if (e2) {
                        i2 |= 2;
                    }
                    j(i2, 0);
                    break;
                case 1:
                    this.aj.addMovement(obtain);
                    this.aj.computeCurrentVelocity(1000, (float) this.ar);
                    float f2 = d2 ? -this.aj.getXVelocity(this.ai) : 0.0f;
                    float f3 = e2 ? -this.aj.getYVelocity(this.ai) : 0.0f;
                    if ((f2 == 0.0f && f3 == 0.0f) || !b((int) f2, (int) f3)) {
                        setScrollState(0);
                    }
                    E();
                    z3 = true;
                    break;
                case 2:
                    int findPointerIndex = motionEvent.findPointerIndex(this.ai);
                    if (findPointerIndex >= 0) {
                        int x3 = (int) (motionEvent.getX(findPointerIndex) + 0.5f);
                        int y3 = (int) (motionEvent.getY(findPointerIndex) + 0.5f);
                        int i3 = this.am - x3;
                        int i4 = this.an - y3;
                        if (a(i3, i4, this.aC, this.aB, 0)) {
                            i3 -= this.aC[0];
                            i4 -= this.aC[1];
                            obtain.offsetLocation((float) this.aB[0], (float) this.aB[1]);
                            int[] iArr2 = this.aD;
                            iArr2[0] = iArr2[0] + this.aB[0];
                            int[] iArr3 = this.aD;
                            iArr3[1] = iArr3[1] + this.aB[1];
                        }
                        if (this.ah != 1) {
                            if (!d2 || Math.abs(i3) <= this.ao) {
                                z2 = false;
                            } else {
                                i3 = i3 > 0 ? i3 - this.ao : this.ao + i3;
                                z2 = true;
                            }
                            if (e2 && Math.abs(i4) > this.ao) {
                                i4 = i4 > 0 ? i4 - this.ao : this.ao + i4;
                                z2 = true;
                            }
                            if (z2) {
                                setScrollState(1);
                            }
                        }
                        if (this.ah == 1) {
                            this.am = x3 - this.aB[0];
                            this.an = y3 - this.aB[1];
                            if (a(d2 ? i3 : 0, e2 ? i4 : 0, obtain)) {
                                getParent().requestDisallowInterceptTouchEvent(true);
                            }
                            if (!(this.z == null || (i3 == 0 && i4 == 0))) {
                                this.z.a(this, i3, i4);
                                break;
                            }
                        }
                    } else {
                        Log.e("RecyclerView", "Error processing scroll; pointer index for id " + this.ai + " not found. Did any MotionEvents get skipped?");
                        return false;
                    }
                    break;
                case 3:
                    F();
                    break;
                case 5:
                    this.ai = motionEvent.getPointerId(actionIndex);
                    int x4 = (int) (motionEvent.getX(actionIndex) + 0.5f);
                    this.am = x4;
                    this.ak = x4;
                    int y4 = (int) (motionEvent.getY(actionIndex) + 0.5f);
                    this.an = y4;
                    this.al = y4;
                    break;
                case 6:
                    c(motionEvent);
                    break;
            }
            if (!z3) {
                this.aj.addMovement(obtain);
            }
            obtain.recycle();
            return true;
        }
    }

    /* access modifiers changed from: 0000 */
    public void p() {
        if (!this.E && this.p) {
            android.support.v4.i.t.a((View) this, this.aE);
            this.E = true;
        }
    }

    /* access modifiers changed from: 0000 */
    public void q() {
        if (this.l == null) {
            Log.e("RecyclerView", "No adapter attached; skipping layout");
        } else if (this.m == null) {
            Log.e("RecyclerView", "No layout manager attached; skipping layout");
        } else {
            this.B.h = false;
            if (this.B.c == 1) {
                N();
                this.m.f(this);
                O();
            } else if (!this.e.f() && this.m.x() == getWidth() && this.m.y() == getHeight()) {
                this.m.f(this);
            } else {
                this.m.f(this);
                O();
            }
            P();
        }
    }

    /* access modifiers changed from: 0000 */
    public void r() {
        int c2 = this.f.c();
        for (int i2 = 0; i2 < c2; i2++) {
            ((i) this.f.d(i2).getLayoutParams()).e = true;
        }
        this.d.j();
    }

    /* access modifiers changed from: protected */
    public void removeDetachedView(View view, boolean z2) {
        w e2 = e(view);
        if (e2 != null) {
            if (e2.r()) {
                e2.m();
            } else if (!e2.c()) {
                throw new IllegalArgumentException("Called removeDetachedView with a view which is not flagged as tmp detached." + e2 + a());
            }
        }
        view.clearAnimation();
        k(view);
        super.removeDetachedView(view, z2);
    }

    public void requestChildFocus(View view, View view2) {
        if (!this.m.a(this, this.B, view, view2) && view2 != null) {
            a(view, view2);
        }
        super.requestChildFocus(view, view2);
    }

    public boolean requestChildRectangleOnScreen(View view, Rect rect, boolean z2) {
        return this.m.a(this, view, rect, z2);
    }

    public void requestDisallowInterceptTouchEvent(boolean z2) {
        int size = this.R.size();
        for (int i2 = 0; i2 < size; i2++) {
            ((l) this.R.get(i2)).a(z2);
        }
        super.requestDisallowInterceptTouchEvent(z2);
    }

    public void requestLayout() {
        if (this.T != 0 || this.u) {
            this.t = true;
        } else {
            super.requestLayout();
        }
    }

    /* access modifiers changed from: 0000 */
    public void s() {
        int c2 = this.f.c();
        for (int i2 = 0; i2 < c2; i2++) {
            w e2 = e(this.f.d(i2));
            if (!e2.c()) {
                e2.b();
            }
        }
    }

    public void scrollBy(int i2, int i3) {
        if (this.m == null) {
            Log.e("RecyclerView", "Cannot scroll without a LayoutManager set. Call setLayoutManager with a non-null argument.");
        } else if (!this.u) {
            boolean d2 = this.m.d();
            boolean e2 = this.m.e();
            if (d2 || e2) {
                if (!d2) {
                    i2 = 0;
                }
                if (!e2) {
                    i3 = 0;
                }
                a(i2, i3, (MotionEvent) null);
            }
        }
    }

    public void scrollTo(int i2, int i3) {
        Log.w("RecyclerView", "RecyclerView does not support scrolling to an absolute position. Use scrollToPosition instead");
    }

    public void sendAccessibilityEventUnchecked(AccessibilityEvent accessibilityEvent) {
        if (!a(accessibilityEvent)) {
            super.sendAccessibilityEventUnchecked(accessibilityEvent);
        }
    }

    public void setAccessibilityDelegateCompat(ba baVar) {
        this.F = baVar;
        android.support.v4.i.t.a((View) this, (android.support.v4.i.b) this.F);
    }

    public void setAdapter(a aVar) {
        setLayoutFrozen(false);
        a(aVar, false, true);
        requestLayout();
    }

    public void setChildDrawingOrderCallback(d dVar) {
        if (dVar != this.ay) {
            this.ay = dVar;
            setChildrenDrawingOrderEnabled(this.ay != null);
        }
    }

    public void setClipToPadding(boolean z2) {
        if (z2 != this.h) {
            k();
        }
        this.h = z2;
        super.setClipToPadding(z2);
        if (this.s) {
            requestLayout();
        }
    }

    public void setHasFixedSize(boolean z2) {
        this.q = z2;
    }

    public void setItemAnimator(e eVar) {
        if (this.x != null) {
            this.x.d();
            this.x.a((b) null);
        }
        this.x = eVar;
        if (this.x != null) {
            this.x.a(this.ax);
        }
    }

    public void setItemViewCacheSize(int i2) {
        this.d.a(i2);
    }

    public void setLayoutFrozen(boolean z2) {
        if (z2 != this.u) {
            a("Do not setLayoutFrozen in layout or scroll");
            if (!z2) {
                this.u = false;
                if (!(!this.t || this.m == null || this.l == null)) {
                    requestLayout();
                }
                this.t = false;
                return;
            }
            long uptimeMillis = SystemClock.uptimeMillis();
            onTouchEvent(MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, 0.0f, 0.0f, 0));
            this.u = true;
            this.U = true;
            f();
        }
    }

    public void setLayoutManager(h hVar) {
        if (hVar != this.m) {
            f();
            if (this.m != null) {
                if (this.x != null) {
                    this.x.d();
                }
                this.m.c(this.d);
                this.m.b(this.d);
                this.d.a();
                if (this.p) {
                    this.m.b(this, this.d);
                }
                this.m.b((RecyclerView) null);
                this.m = null;
            } else {
                this.d.a();
            }
            this.f.a();
            this.m = hVar;
            if (hVar != null) {
                if (hVar.q != null) {
                    throw new IllegalArgumentException("LayoutManager " + hVar + " is already attached to a RecyclerView:" + hVar.q.a());
                }
                this.m.b(this);
                if (this.p) {
                    this.m.c(this);
                }
            }
            this.d.b();
            requestLayout();
        }
    }

    public void setNestedScrollingEnabled(boolean z2) {
        getScrollingChildHelper().a(z2);
    }

    public void setOnFlingListener(k kVar) {
        this.ap = kVar;
    }

    @Deprecated
    public void setOnScrollListener(m mVar) {
        this.av = mVar;
    }

    public void setPreserveFocusAfterLayout(boolean z2) {
        this.au = z2;
    }

    public void setRecycledViewPool(n nVar) {
        this.d.a(nVar);
    }

    public void setRecyclerListener(p pVar) {
        this.n = pVar;
    }

    /* access modifiers changed from: 0000 */
    public void setScrollState(int i2) {
        if (i2 != this.ah) {
            this.ah = i2;
            if (i2 != 2) {
                C();
            }
            g(i2);
        }
    }

    public void setScrollingTouchSlop(int i2) {
        ViewConfiguration viewConfiguration = ViewConfiguration.get(getContext());
        switch (i2) {
            case 0:
                break;
            case 1:
                this.ao = viewConfiguration.getScaledPagingTouchSlop();
                return;
            default:
                Log.w("RecyclerView", "setScrollingTouchSlop(): bad argument constant " + i2 + "; using default value");
                break;
        }
        this.ao = viewConfiguration.getScaledTouchSlop();
    }

    public void setViewCacheExtension(u uVar) {
        this.d.a(uVar);
    }

    public boolean startNestedScroll(int i2) {
        return getScrollingChildHelper().b(i2);
    }

    public void stopNestedScroll() {
        getScrollingChildHelper().c();
    }

    /* access modifiers changed from: 0000 */
    public void t() {
        int c2 = this.f.c();
        for (int i2 = 0; i2 < c2; i2++) {
            w e2 = e(this.f.d(i2));
            if (!e2.c()) {
                e2.a();
            }
        }
        this.d.i();
    }

    /* access modifiers changed from: 0000 */
    public void u() {
        this.w = true;
        v();
    }

    /* access modifiers changed from: 0000 */
    public void v() {
        int c2 = this.f.c();
        for (int i2 = 0; i2 < c2; i2++) {
            w e2 = e(this.f.d(i2));
            if (e2 != null && !e2.c()) {
                e2.b(6);
            }
        }
        r();
        this.d.h();
    }

    public boolean w() {
        return !this.s || this.w || this.e.d();
    }

    /* access modifiers changed from: 0000 */
    public void x() {
        int b2 = this.f.b();
        for (int i2 = 0; i2 < b2; i2++) {
            View b3 = this.f.b(i2);
            w b4 = b(b3);
            if (!(b4 == null || b4.i == null)) {
                View view = b4.i.f1102a;
                int left = b3.getLeft();
                int top = b3.getTop();
                if (left != view.getLeft() || top != view.getTop()) {
                    view.layout(left, top, view.getWidth() + left, view.getHeight() + top);
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void y() {
        for (int size = this.G.size() - 1; size >= 0; size--) {
            w wVar = (w) this.G.get(size);
            if (wVar.f1102a.getParent() == this && !wVar.c()) {
                int i2 = wVar.l;
                if (i2 != -1) {
                    android.support.v4.i.t.a(wVar.f1102a, i2);
                    wVar.l = -1;
                }
            }
        }
        this.G.clear();
    }
}
