package android.support.v7.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.v7.a.a.j;
import android.util.AttributeSet;
import android.widget.TextView;

class aa extends z {

    /* renamed from: b reason: collision with root package name */
    private bm f1163b;
    private bm c;

    aa(TextView textView) {
        super(textView);
    }

    /* access modifiers changed from: 0000 */
    public void a() {
        super.a();
        if (this.f1163b != null || this.c != null) {
            Drawable[] compoundDrawablesRelative = this.f1355a.getCompoundDrawablesRelative();
            a(compoundDrawablesRelative[0], this.f1163b);
            a(compoundDrawablesRelative[2], this.c);
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(AttributeSet attributeSet, int i) {
        super.a(attributeSet, i);
        Context context = this.f1355a.getContext();
        m a2 = m.a();
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, j.AppCompatTextHelper, i, 0);
        if (obtainStyledAttributes.hasValue(j.AppCompatTextHelper_android_drawableStart)) {
            this.f1163b = a(context, a2, obtainStyledAttributes.getResourceId(j.AppCompatTextHelper_android_drawableStart, 0));
        }
        if (obtainStyledAttributes.hasValue(j.AppCompatTextHelper_android_drawableEnd)) {
            this.c = a(context, a2, obtainStyledAttributes.getResourceId(j.AppCompatTextHelper_android_drawableEnd, 0));
        }
        obtainStyledAttributes.recycle();
    }
}
