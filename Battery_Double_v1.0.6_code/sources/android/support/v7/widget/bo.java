package android.support.v7.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.v4.b.a.b;
import android.support.v4.b.a.b.a;
import android.util.AttributeSet;
import android.util.TypedValue;

public class bo {

    /* renamed from: a reason: collision with root package name */
    private final Context f1263a;

    /* renamed from: b reason: collision with root package name */
    private final TypedArray f1264b;
    private TypedValue c;

    private bo(Context context, TypedArray typedArray) {
        this.f1263a = context;
        this.f1264b = typedArray;
    }

    public static bo a(Context context, int i, int[] iArr) {
        return new bo(context, context.obtainStyledAttributes(i, iArr));
    }

    public static bo a(Context context, AttributeSet attributeSet, int[] iArr) {
        return new bo(context, context.obtainStyledAttributes(attributeSet, iArr));
    }

    public static bo a(Context context, AttributeSet attributeSet, int[] iArr, int i, int i2) {
        return new bo(context, context.obtainStyledAttributes(attributeSet, iArr, i, i2));
    }

    public float a(int i, float f) {
        return this.f1264b.getFloat(i, f);
    }

    public int a(int i, int i2) {
        return this.f1264b.getInt(i, i2);
    }

    public Typeface a(int i, int i2, a aVar) {
        int resourceId = this.f1264b.getResourceId(i, 0);
        if (resourceId == 0) {
            return null;
        }
        if (this.c == null) {
            this.c = new TypedValue();
        }
        return b.a(this.f1263a, resourceId, this.c, i2, aVar);
    }

    public Drawable a(int i) {
        if (this.f1264b.hasValue(i)) {
            int resourceId = this.f1264b.getResourceId(i, 0);
            if (resourceId != 0) {
                return android.support.v7.c.a.b.b(this.f1263a, resourceId);
            }
        }
        return this.f1264b.getDrawable(i);
    }

    public void a() {
        this.f1264b.recycle();
    }

    public boolean a(int i, boolean z) {
        return this.f1264b.getBoolean(i, z);
    }

    public int b(int i, int i2) {
        return this.f1264b.getColor(i, i2);
    }

    public Drawable b(int i) {
        if (this.f1264b.hasValue(i)) {
            int resourceId = this.f1264b.getResourceId(i, 0);
            if (resourceId != 0) {
                return m.a().a(this.f1263a, resourceId, true);
            }
        }
        return null;
    }

    public int c(int i, int i2) {
        return this.f1264b.getInteger(i, i2);
    }

    public CharSequence c(int i) {
        return this.f1264b.getText(i);
    }

    public int d(int i, int i2) {
        return this.f1264b.getDimensionPixelOffset(i, i2);
    }

    public String d(int i) {
        return this.f1264b.getString(i);
    }

    public int e(int i, int i2) {
        return this.f1264b.getDimensionPixelSize(i, i2);
    }

    public ColorStateList e(int i) {
        if (this.f1264b.hasValue(i)) {
            int resourceId = this.f1264b.getResourceId(i, 0);
            if (resourceId != 0) {
                ColorStateList a2 = android.support.v7.c.a.b.a(this.f1263a, resourceId);
                if (a2 != null) {
                    return a2;
                }
            }
        }
        return this.f1264b.getColorStateList(i);
    }

    public int f(int i, int i2) {
        return this.f1264b.getLayoutDimension(i, i2);
    }

    public CharSequence[] f(int i) {
        return this.f1264b.getTextArray(i);
    }

    public int g(int i, int i2) {
        return this.f1264b.getResourceId(i, i2);
    }

    public boolean g(int i) {
        return this.f1264b.hasValue(i);
    }
}
