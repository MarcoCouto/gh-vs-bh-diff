package android.support.v7.widget;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.support.v4.i.t;
import android.support.v7.widget.RecyclerView.g;
import android.support.v7.widget.RecyclerView.l;
import android.support.v7.widget.RecyclerView.m;
import android.view.MotionEvent;
import com.hmatalonga.greenhub.Config;

class ao extends g implements l {
    private static final int[] g = {16842919};
    private static final int[] h = new int[0];
    private final int[] A = new int[2];
    /* access modifiers changed from: private */
    public final ValueAnimator B = ValueAnimator.ofFloat(new float[]{0.0f, 1.0f});
    /* access modifiers changed from: private */
    public int C = 0;
    private final Runnable D = new Runnable() {
        public void run() {
            ao.this.a(500);
        }
    };
    private final m E = new m() {
        public void a(RecyclerView recyclerView, int i, int i2) {
            ao.this.a(recyclerView.computeHorizontalScrollOffset(), recyclerView.computeVerticalScrollOffset());
        }
    };

    /* renamed from: a reason: collision with root package name */
    int f1198a;

    /* renamed from: b reason: collision with root package name */
    int f1199b;
    float c;
    int d;
    int e;
    float f;
    private final int i;
    private final int j;
    /* access modifiers changed from: private */
    public final StateListDrawable k;
    /* access modifiers changed from: private */
    public final Drawable l;
    private final int m;
    private final int n;
    private final StateListDrawable o;
    private final Drawable p;
    private final int q;
    private final int r;
    private int s = 0;
    private int t = 0;
    private RecyclerView u;
    private boolean v = false;
    private boolean w = false;
    private int x = 0;
    private int y = 0;
    private final int[] z = new int[2];

    private class a extends AnimatorListenerAdapter {

        /* renamed from: b reason: collision with root package name */
        private boolean f1203b;

        private a() {
            this.f1203b = false;
        }

        public void onAnimationCancel(Animator animator) {
            this.f1203b = true;
        }

        public void onAnimationEnd(Animator animator) {
            if (this.f1203b) {
                this.f1203b = false;
            } else if (((Float) ao.this.B.getAnimatedValue()).floatValue() == 0.0f) {
                ao.this.C = 0;
                ao.this.b(0);
            } else {
                ao.this.C = 2;
                ao.this.d();
            }
        }
    }

    private class b implements AnimatorUpdateListener {
        private b() {
        }

        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            int floatValue = (int) (((Float) valueAnimator.getAnimatedValue()).floatValue() * 255.0f);
            ao.this.k.setAlpha(floatValue);
            ao.this.l.setAlpha(floatValue);
            ao.this.d();
        }
    }

    ao(RecyclerView recyclerView, StateListDrawable stateListDrawable, Drawable drawable, StateListDrawable stateListDrawable2, Drawable drawable2, int i2, int i3, int i4) {
        this.k = stateListDrawable;
        this.l = drawable;
        this.o = stateListDrawable2;
        this.p = drawable2;
        this.m = Math.max(i2, stateListDrawable.getIntrinsicWidth());
        this.n = Math.max(i2, drawable.getIntrinsicWidth());
        this.q = Math.max(i2, stateListDrawable2.getIntrinsicWidth());
        this.r = Math.max(i2, drawable2.getIntrinsicWidth());
        this.i = i3;
        this.j = i4;
        this.k.setAlpha(255);
        this.l.setAlpha(255);
        this.B.addListener(new a());
        this.B.addUpdateListener(new b());
        a(recyclerView);
    }

    private int a(float f2, float f3, int[] iArr, int i2, int i3, int i4) {
        int i5 = iArr[1] - iArr[0];
        if (i5 == 0) {
            return 0;
        }
        int i6 = i2 - i4;
        int i7 = (int) (((f3 - f2) / ((float) i5)) * ((float) i6));
        int i8 = i3 + i7;
        if (i8 >= i6 || i8 < 0) {
            return 0;
        }
        return i7;
    }

    private void a(float f2) {
        int[] g2 = g();
        float max = Math.max((float) g2[0], Math.min((float) g2[1], f2));
        if (Math.abs(((float) this.f1199b) - max) >= 2.0f) {
            int a2 = a(this.c, max, g2, this.u.computeVerticalScrollRange(), this.u.computeVerticalScrollOffset(), this.t);
            if (a2 != 0) {
                this.u.scrollBy(0, a2);
            }
            this.c = max;
        }
    }

    private void a(Canvas canvas) {
        int i2 = this.s - this.m;
        int i3 = this.f1199b - (this.f1198a / 2);
        this.k.setBounds(0, 0, this.m, this.f1198a);
        this.l.setBounds(0, 0, this.n, this.t);
        if (e()) {
            this.l.draw(canvas);
            canvas.translate((float) this.m, (float) i3);
            canvas.scale(-1.0f, 1.0f);
            this.k.draw(canvas);
            canvas.scale(1.0f, 1.0f);
            canvas.translate((float) (-this.m), (float) (-i3));
            return;
        }
        canvas.translate((float) i2, 0.0f);
        this.l.draw(canvas);
        canvas.translate(0.0f, (float) i3);
        this.k.draw(canvas);
        canvas.translate((float) (-i2), (float) (-i3));
    }

    private void b() {
        this.u.a((g) this);
        this.u.a((l) this);
        this.u.a(this.E);
    }

    private void b(float f2) {
        int[] h2 = h();
        float max = Math.max((float) h2[0], Math.min((float) h2[1], f2));
        if (Math.abs(((float) this.e) - max) >= 2.0f) {
            int a2 = a(this.f, max, h2, this.u.computeHorizontalScrollRange(), this.u.computeHorizontalScrollOffset(), this.s);
            if (a2 != 0) {
                this.u.scrollBy(a2, 0);
            }
            this.f = max;
        }
    }

    /* access modifiers changed from: private */
    public void b(int i2) {
        if (i2 == 2 && this.x != 2) {
            this.k.setState(g);
            f();
        }
        if (i2 == 0) {
            d();
        } else {
            a();
        }
        if (this.x == 2 && i2 != 2) {
            this.k.setState(h);
            c(1200);
        } else if (i2 == 1) {
            c((int) Config.PENDING_REMOVAL_TIMEOUT);
        }
        this.x = i2;
    }

    private void b(Canvas canvas) {
        int i2 = this.t - this.q;
        int i3 = this.e - (this.d / 2);
        this.o.setBounds(0, 0, this.d, this.q);
        this.p.setBounds(0, 0, this.s, this.r);
        canvas.translate(0.0f, (float) i2);
        this.p.draw(canvas);
        canvas.translate((float) i3, 0.0f);
        this.o.draw(canvas);
        canvas.translate((float) (-i3), (float) (-i2));
    }

    private void c() {
        this.u.b((g) this);
        this.u.b((l) this);
        this.u.b(this.E);
        f();
    }

    private void c(int i2) {
        f();
        this.u.postDelayed(this.D, (long) i2);
    }

    /* access modifiers changed from: private */
    public void d() {
        this.u.invalidate();
    }

    private boolean e() {
        return t.e(this.u) == 1;
    }

    private void f() {
        this.u.removeCallbacks(this.D);
    }

    private int[] g() {
        this.z[0] = this.j;
        this.z[1] = this.t - this.j;
        return this.z;
    }

    private int[] h() {
        this.A[0] = this.j;
        this.A[1] = this.s - this.j;
        return this.A;
    }

    public void a() {
        switch (this.C) {
            case 0:
                break;
            case 3:
                this.B.cancel();
                break;
            default:
                return;
        }
        this.C = 1;
        this.B.setFloatValues(new float[]{((Float) this.B.getAnimatedValue()).floatValue(), 1.0f});
        this.B.setDuration(500);
        this.B.setStartDelay(0);
        this.B.start();
    }

    /* access modifiers changed from: 0000 */
    public void a(int i2) {
        switch (this.C) {
            case 1:
                this.B.cancel();
                break;
            case 2:
                break;
            default:
                return;
        }
        this.C = 3;
        this.B.setFloatValues(new float[]{((Float) this.B.getAnimatedValue()).floatValue(), 0.0f});
        this.B.setDuration((long) i2);
        this.B.start();
    }

    /* access modifiers changed from: 0000 */
    public void a(int i2, int i3) {
        int computeVerticalScrollRange = this.u.computeVerticalScrollRange();
        int i4 = this.t;
        this.v = computeVerticalScrollRange - i4 > 0 && this.t >= this.i;
        int computeHorizontalScrollRange = this.u.computeHorizontalScrollRange();
        int i5 = this.s;
        this.w = computeHorizontalScrollRange - i5 > 0 && this.s >= this.i;
        if (this.v || this.w) {
            if (this.v) {
                this.f1199b = (int) (((((float) i3) + (((float) i4) / 2.0f)) * ((float) i4)) / ((float) computeVerticalScrollRange));
                this.f1198a = Math.min(i4, (i4 * i4) / computeVerticalScrollRange);
            }
            if (this.w) {
                this.e = (int) (((((float) i2) + (((float) i5) / 2.0f)) * ((float) i5)) / ((float) computeHorizontalScrollRange));
                this.d = Math.min(i5, (i5 * i5) / computeHorizontalScrollRange);
            }
            if (this.x == 0 || this.x == 1) {
                b(1);
            }
        } else if (this.x != 0) {
            b(0);
        }
    }

    public void a(Canvas canvas, RecyclerView recyclerView, RecyclerView.t tVar) {
        if (this.s != this.u.getWidth() || this.t != this.u.getHeight()) {
            this.s = this.u.getWidth();
            this.t = this.u.getHeight();
            b(0);
        } else if (this.C != 0) {
            if (this.v) {
                a(canvas);
            }
            if (this.w) {
                b(canvas);
            }
        }
    }

    public void a(RecyclerView recyclerView) {
        if (this.u != recyclerView) {
            if (this.u != null) {
                c();
            }
            this.u = recyclerView;
            if (this.u != null) {
                b();
            }
        }
    }

    public void a(boolean z2) {
    }

    /* access modifiers changed from: 0000 */
    public boolean a(float f2, float f3) {
        if (!e() ? f2 >= ((float) (this.s - this.m)) : f2 <= ((float) (this.m / 2))) {
            if (f3 >= ((float) (this.f1199b - (this.f1198a / 2))) && f3 <= ((float) (this.f1199b + (this.f1198a / 2)))) {
                return true;
            }
        }
        return false;
    }

    public boolean a(RecyclerView recyclerView, MotionEvent motionEvent) {
        if (this.x != 1) {
            return this.x == 2;
        }
        boolean a2 = a(motionEvent.getX(), motionEvent.getY());
        boolean b2 = b(motionEvent.getX(), motionEvent.getY());
        if (motionEvent.getAction() != 0 || (!a2 && !b2)) {
            return false;
        }
        if (b2) {
            this.y = 1;
            this.f = (float) ((int) motionEvent.getX());
        } else if (a2) {
            this.y = 2;
            this.c = (float) ((int) motionEvent.getY());
        }
        b(2);
        return true;
    }

    public void b(RecyclerView recyclerView, MotionEvent motionEvent) {
        if (this.x != 0) {
            if (motionEvent.getAction() == 0) {
                boolean a2 = a(motionEvent.getX(), motionEvent.getY());
                boolean b2 = b(motionEvent.getX(), motionEvent.getY());
                if (a2 || b2) {
                    if (b2) {
                        this.y = 1;
                        this.f = (float) ((int) motionEvent.getX());
                    } else if (a2) {
                        this.y = 2;
                        this.c = (float) ((int) motionEvent.getY());
                    }
                    b(2);
                }
            } else if (motionEvent.getAction() == 1 && this.x == 2) {
                this.c = 0.0f;
                this.f = 0.0f;
                b(1);
                this.y = 0;
            } else if (motionEvent.getAction() == 2 && this.x == 2) {
                a();
                if (this.y == 1) {
                    b(motionEvent.getX());
                }
                if (this.y == 2) {
                    a(motionEvent.getY());
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean b(float f2, float f3) {
        return f3 >= ((float) (this.t - this.q)) && f2 >= ((float) (this.e - (this.d / 2))) && f2 <= ((float) (this.e + (this.d / 2)));
    }
}
