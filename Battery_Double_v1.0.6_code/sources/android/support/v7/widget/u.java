package android.support.v7.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.support.v4.widget.o;
import android.support.v7.a.a.C0026a;
import android.support.v7.c.a.b;
import android.util.AttributeSet;
import android.widget.RadioButton;

public class u extends RadioButton implements o {

    /* renamed from: a reason: collision with root package name */
    private final l f1336a;

    /* renamed from: b reason: collision with root package name */
    private final z f1337b;

    public u(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, C0026a.radioButtonStyle);
    }

    public u(Context context, AttributeSet attributeSet, int i) {
        super(bl.a(context), attributeSet, i);
        this.f1336a = new l(this);
        this.f1336a.a(attributeSet, i);
        this.f1337b = new z(this);
        this.f1337b.a(attributeSet, i);
    }

    public int getCompoundPaddingLeft() {
        int compoundPaddingLeft = super.getCompoundPaddingLeft();
        return this.f1336a != null ? this.f1336a.a(compoundPaddingLeft) : compoundPaddingLeft;
    }

    public ColorStateList getSupportButtonTintList() {
        if (this.f1336a != null) {
            return this.f1336a.a();
        }
        return null;
    }

    public Mode getSupportButtonTintMode() {
        if (this.f1336a != null) {
            return this.f1336a.b();
        }
        return null;
    }

    public void setButtonDrawable(int i) {
        setButtonDrawable(b.b(getContext(), i));
    }

    public void setButtonDrawable(Drawable drawable) {
        super.setButtonDrawable(drawable);
        if (this.f1336a != null) {
            this.f1336a.c();
        }
    }

    public void setSupportButtonTintList(ColorStateList colorStateList) {
        if (this.f1336a != null) {
            this.f1336a.a(colorStateList);
        }
    }

    public void setSupportButtonTintMode(Mode mode) {
        if (this.f1336a != null) {
            this.f1336a.a(mode);
        }
    }
}
