package android.support.v7.widget;

import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.support.v4.c.a.a;
import android.support.v4.widget.e;
import android.support.v7.a.a.j;
import android.support.v7.c.a.b;
import android.util.AttributeSet;
import android.widget.CompoundButton;

class l {

    /* renamed from: a reason: collision with root package name */
    private final CompoundButton f1318a;

    /* renamed from: b reason: collision with root package name */
    private ColorStateList f1319b = null;
    private Mode c = null;
    private boolean d = false;
    private boolean e = false;
    private boolean f;

    l(CompoundButton compoundButton) {
        this.f1318a = compoundButton;
    }

    /* access modifiers changed from: 0000 */
    public int a(int i) {
        if (VERSION.SDK_INT >= 17) {
            return i;
        }
        Drawable a2 = e.a(this.f1318a);
        return a2 != null ? i + a2.getIntrinsicWidth() : i;
    }

    /* access modifiers changed from: 0000 */
    public ColorStateList a() {
        return this.f1319b;
    }

    /* access modifiers changed from: 0000 */
    public void a(ColorStateList colorStateList) {
        this.f1319b = colorStateList;
        this.d = true;
        d();
    }

    /* access modifiers changed from: 0000 */
    public void a(Mode mode) {
        this.c = mode;
        this.e = true;
        d();
    }

    /* access modifiers changed from: 0000 */
    public void a(AttributeSet attributeSet, int i) {
        TypedArray obtainStyledAttributes = this.f1318a.getContext().obtainStyledAttributes(attributeSet, j.CompoundButton, i, 0);
        try {
            if (obtainStyledAttributes.hasValue(j.CompoundButton_android_button)) {
                int resourceId = obtainStyledAttributes.getResourceId(j.CompoundButton_android_button, 0);
                if (resourceId != 0) {
                    this.f1318a.setButtonDrawable(b.b(this.f1318a.getContext(), resourceId));
                }
            }
            if (obtainStyledAttributes.hasValue(j.CompoundButton_buttonTint)) {
                e.a(this.f1318a, obtainStyledAttributes.getColorStateList(j.CompoundButton_buttonTint));
            }
            if (obtainStyledAttributes.hasValue(j.CompoundButton_buttonTintMode)) {
                e.a(this.f1318a, am.a(obtainStyledAttributes.getInt(j.CompoundButton_buttonTintMode, -1), null));
            }
        } finally {
            obtainStyledAttributes.recycle();
        }
    }

    /* access modifiers changed from: 0000 */
    public Mode b() {
        return this.c;
    }

    /* access modifiers changed from: 0000 */
    public void c() {
        if (this.f) {
            this.f = false;
            return;
        }
        this.f = true;
        d();
    }

    /* access modifiers changed from: 0000 */
    public void d() {
        Drawable a2 = e.a(this.f1318a);
        if (a2 == null) {
            return;
        }
        if (this.d || this.e) {
            Drawable mutate = a.f(a2).mutate();
            if (this.d) {
                a.a(mutate, this.f1319b);
            }
            if (this.e) {
                a.a(mutate, this.c);
            }
            if (mutate.isStateful()) {
                mutate.setState(this.f1318a.getDrawableState());
            }
            this.f1318a.setButtonDrawable(mutate);
        }
    }
}
