package android.support.v7.widget;

import android.view.View;

class bu {

    /* renamed from: a reason: collision with root package name */
    final b f1279a;

    /* renamed from: b reason: collision with root package name */
    a f1280b = new a();

    static class a {

        /* renamed from: a reason: collision with root package name */
        int f1281a = 0;

        /* renamed from: b reason: collision with root package name */
        int f1282b;
        int c;
        int d;
        int e;

        a() {
        }

        /* access modifiers changed from: 0000 */
        public int a(int i, int i2) {
            if (i > i2) {
                return 1;
            }
            return i == i2 ? 2 : 4;
        }

        /* access modifiers changed from: 0000 */
        public void a() {
            this.f1281a = 0;
        }

        /* access modifiers changed from: 0000 */
        public void a(int i) {
            this.f1281a |= i;
        }

        /* access modifiers changed from: 0000 */
        public void a(int i, int i2, int i3, int i4) {
            this.f1282b = i;
            this.c = i2;
            this.d = i3;
            this.e = i4;
        }

        /* access modifiers changed from: 0000 */
        public boolean b() {
            if ((this.f1281a & 7) != 0 && (this.f1281a & (a(this.d, this.f1282b) << 0)) == 0) {
                return false;
            }
            if ((this.f1281a & 112) != 0 && (this.f1281a & (a(this.d, this.c) << 4)) == 0) {
                return false;
            }
            if ((this.f1281a & 1792) == 0 || (this.f1281a & (a(this.e, this.f1282b) << 8)) != 0) {
                return (this.f1281a & 28672) == 0 || (this.f1281a & (a(this.e, this.c) << 12)) != 0;
            }
            return false;
        }
    }

    interface b {
        int a();

        int a(View view);

        View a(int i);

        int b();

        int b(View view);
    }

    bu(b bVar) {
        this.f1279a = bVar;
    }

    /* access modifiers changed from: 0000 */
    public View a(int i, int i2, int i3, int i4) {
        int a2 = this.f1279a.a();
        int b2 = this.f1279a.b();
        int i5 = i2 > i ? 1 : -1;
        View view = null;
        while (i != i2) {
            View a3 = this.f1279a.a(i);
            this.f1280b.a(a2, b2, this.f1279a.a(a3), this.f1279a.b(a3));
            if (i3 != 0) {
                this.f1280b.a();
                this.f1280b.a(i3);
                if (this.f1280b.b()) {
                    return a3;
                }
            }
            if (i4 != 0) {
                this.f1280b.a();
                this.f1280b.a(i4);
                if (this.f1280b.b()) {
                    i += i5;
                    view = a3;
                }
            }
            a3 = view;
            i += i5;
            view = a3;
        }
        return view;
    }

    /* access modifiers changed from: 0000 */
    public boolean a(View view, int i) {
        this.f1280b.a(this.f1279a.a(), this.f1279a.b(), this.f1279a.a(view), this.f1279a.b(view));
        if (i == 0) {
            return false;
        }
        this.f1280b.a();
        this.f1280b.a(i);
        return this.f1280b.b();
    }
}
