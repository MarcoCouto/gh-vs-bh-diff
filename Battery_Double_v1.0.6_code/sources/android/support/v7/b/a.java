package android.support.v7.b;

import com.mansoon.BatteryDouble.R;

public final class a {

    /* renamed from: android.support.v7.b.a$a reason: collision with other inner class name */
    public static final class C0029a {
        public static final int cardview_dark_background = 2131099695;
        public static final int cardview_light_background = 2131099696;
        public static final int cardview_shadow_end_color = 2131099697;
        public static final int cardview_shadow_start_color = 2131099698;
    }

    public static final class b {
        public static final int cardview_compat_inset_shadow = 2131165263;
        public static final int cardview_default_elevation = 2131165264;
        public static final int cardview_default_radius = 2131165265;
    }

    public static final class c {
        public static final int Base_CardView = 2131689487;
        public static final int CardView = 2131689647;
        public static final int CardView_Dark = 2131689648;
        public static final int CardView_Light = 2131689649;
    }

    public static final class d {
        public static final int[] CardView = {16843071, 16843072, R.attr.cardBackgroundColor, R.attr.cardCornerRadius, R.attr.cardElevation, R.attr.cardMaxElevation, R.attr.cardPreventCornerOverlap, R.attr.cardUseCompatPadding, R.attr.contentPadding, R.attr.contentPaddingBottom, R.attr.contentPaddingLeft, R.attr.contentPaddingRight, R.attr.contentPaddingTop};
        public static final int CardView_android_minHeight = 1;
        public static final int CardView_android_minWidth = 0;
        public static final int CardView_cardBackgroundColor = 2;
        public static final int CardView_cardCornerRadius = 3;
        public static final int CardView_cardElevation = 4;
        public static final int CardView_cardMaxElevation = 5;
        public static final int CardView_cardPreventCornerOverlap = 6;
        public static final int CardView_cardUseCompatPadding = 7;
        public static final int CardView_contentPadding = 8;
        public static final int CardView_contentPaddingBottom = 9;
        public static final int CardView_contentPaddingLeft = 10;
        public static final int CardView_contentPaddingRight = 11;
        public static final int CardView_contentPaddingTop = 12;
    }
}
