package android.support.v7.c.a;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.support.v7.widget.m;
import android.util.Log;
import android.util.SparseArray;
import android.util.TypedValue;
import java.util.WeakHashMap;

public final class b {

    /* renamed from: a reason: collision with root package name */
    private static final ThreadLocal<TypedValue> f946a = new ThreadLocal<>();

    /* renamed from: b reason: collision with root package name */
    private static final WeakHashMap<Context, SparseArray<a>> f947b = new WeakHashMap<>(0);
    private static final Object c = new Object();

    private static class a {

        /* renamed from: a reason: collision with root package name */
        final ColorStateList f948a;

        /* renamed from: b reason: collision with root package name */
        final Configuration f949b;

        a(ColorStateList colorStateList, Configuration configuration) {
            this.f948a = colorStateList;
            this.f949b = configuration;
        }
    }

    public static ColorStateList a(Context context, int i) {
        if (VERSION.SDK_INT >= 23) {
            return context.getColorStateList(i);
        }
        ColorStateList d = d(context, i);
        if (d != null) {
            return d;
        }
        ColorStateList c2 = c(context, i);
        if (c2 == null) {
            return android.support.v4.b.a.b(context, i);
        }
        a(context, i, c2);
        return c2;
    }

    private static TypedValue a() {
        TypedValue typedValue = (TypedValue) f946a.get();
        if (typedValue != null) {
            return typedValue;
        }
        TypedValue typedValue2 = new TypedValue();
        f946a.set(typedValue2);
        return typedValue2;
    }

    private static void a(Context context, int i, ColorStateList colorStateList) {
        synchronized (c) {
            SparseArray sparseArray = (SparseArray) f947b.get(context);
            if (sparseArray == null) {
                sparseArray = new SparseArray();
                f947b.put(context, sparseArray);
            }
            sparseArray.append(i, new a(colorStateList, context.getResources().getConfiguration()));
        }
    }

    public static Drawable b(Context context, int i) {
        return m.a().a(context, i);
    }

    private static ColorStateList c(Context context, int i) {
        ColorStateList colorStateList = null;
        if (e(context, i)) {
            return colorStateList;
        }
        Resources resources = context.getResources();
        try {
            return a.a(resources, resources.getXml(i), context.getTheme());
        } catch (Exception e) {
            Log.e("AppCompatResources", "Failed to inflate ColorStateList, leaving it to the framework", e);
            return colorStateList;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        return null;
     */
    private static ColorStateList d(Context context, int i) {
        synchronized (c) {
            SparseArray sparseArray = (SparseArray) f947b.get(context);
            if (sparseArray != null && sparseArray.size() > 0) {
                a aVar = (a) sparseArray.get(i);
                if (aVar != null) {
                    if (aVar.f949b.equals(context.getResources().getConfiguration())) {
                        ColorStateList colorStateList = aVar.f948a;
                        return colorStateList;
                    }
                    sparseArray.remove(i);
                }
            }
        }
    }

    private static boolean e(Context context, int i) {
        Resources resources = context.getResources();
        TypedValue a2 = a();
        resources.getValue(i, a2, true);
        return a2.type >= 28 && a2.type <= 31;
    }
}
