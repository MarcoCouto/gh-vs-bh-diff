package android.support.v7.view;

import android.support.v4.i.aa;
import android.support.v4.i.y;
import android.support.v4.i.z;
import android.view.View;
import android.view.animation.Interpolator;
import java.util.ArrayList;
import java.util.Iterator;

public class h {

    /* renamed from: a reason: collision with root package name */
    final ArrayList<y> f969a = new ArrayList<>();

    /* renamed from: b reason: collision with root package name */
    z f970b;
    private long c = -1;
    private Interpolator d;
    private boolean e;
    private final aa f = new aa() {

        /* renamed from: b reason: collision with root package name */
        private boolean f972b = false;
        private int c = 0;

        /* access modifiers changed from: 0000 */
        public void a() {
            this.c = 0;
            this.f972b = false;
            h.this.b();
        }

        public void a(View view) {
            if (!this.f972b) {
                this.f972b = true;
                if (h.this.f970b != null) {
                    h.this.f970b.a(null);
                }
            }
        }

        public void b(View view) {
            int i = this.c + 1;
            this.c = i;
            if (i == h.this.f969a.size()) {
                if (h.this.f970b != null) {
                    h.this.f970b.b(null);
                }
                a();
            }
        }
    };

    public h a(long j) {
        if (!this.e) {
            this.c = j;
        }
        return this;
    }

    public h a(y yVar) {
        if (!this.e) {
            this.f969a.add(yVar);
        }
        return this;
    }

    public h a(y yVar, y yVar2) {
        this.f969a.add(yVar);
        yVar2.b(yVar.a());
        this.f969a.add(yVar2);
        return this;
    }

    public h a(z zVar) {
        if (!this.e) {
            this.f970b = zVar;
        }
        return this;
    }

    public h a(Interpolator interpolator) {
        if (!this.e) {
            this.d = interpolator;
        }
        return this;
    }

    public void a() {
        if (!this.e) {
            Iterator it = this.f969a.iterator();
            while (it.hasNext()) {
                y yVar = (y) it.next();
                if (this.c >= 0) {
                    yVar.a(this.c);
                }
                if (this.d != null) {
                    yVar.a(this.d);
                }
                if (this.f970b != null) {
                    yVar.a((z) this.f);
                }
                yVar.c();
            }
            this.e = true;
        }
    }

    /* access modifiers changed from: 0000 */
    public void b() {
        this.e = false;
    }

    public void c() {
        if (this.e) {
            Iterator it = this.f969a.iterator();
            while (it.hasNext()) {
                ((y) it.next()).b();
            }
            this.e = false;
        }
    }
}
