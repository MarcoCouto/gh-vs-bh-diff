package android.support.v7.view;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Build.VERSION;
import android.support.v7.a.a.C0026a;
import android.support.v7.a.a.b;
import android.support.v7.a.a.d;
import android.support.v7.a.a.j;
import android.view.ViewConfiguration;

public class a {

    /* renamed from: a reason: collision with root package name */
    private Context f952a;

    private a(Context context) {
        this.f952a = context;
    }

    public static a a(Context context) {
        return new a(context);
    }

    public int a() {
        Configuration configuration = this.f952a.getResources().getConfiguration();
        int i = configuration.screenWidthDp;
        int i2 = configuration.screenHeightDp;
        if (configuration.smallestScreenWidthDp > 600 || i > 600 || ((i > 960 && i2 > 720) || (i > 720 && i2 > 960))) {
            return 5;
        }
        if (i >= 500 || ((i > 640 && i2 > 480) || (i > 480 && i2 > 640))) {
            return 4;
        }
        return i >= 360 ? 3 : 2;
    }

    public boolean b() {
        return VERSION.SDK_INT >= 19 || !ViewConfiguration.get(this.f952a).hasPermanentMenuKey();
    }

    public int c() {
        return this.f952a.getResources().getDisplayMetrics().widthPixels / 2;
    }

    public boolean d() {
        return this.f952a.getResources().getBoolean(b.abc_action_bar_embed_tabs);
    }

    public int e() {
        TypedArray obtainStyledAttributes = this.f952a.obtainStyledAttributes(null, j.ActionBar, C0026a.actionBarStyle, 0);
        int layoutDimension = obtainStyledAttributes.getLayoutDimension(j.ActionBar_height, 0);
        Resources resources = this.f952a.getResources();
        if (!d()) {
            layoutDimension = Math.min(layoutDimension, resources.getDimensionPixelSize(d.abc_action_bar_stacked_max_height));
        }
        obtainStyledAttributes.recycle();
        return layoutDimension;
    }

    public boolean f() {
        return this.f952a.getApplicationInfo().targetSdkVersion < 14;
    }

    public int g() {
        return this.f952a.getResources().getDimensionPixelSize(d.abc_action_bar_stacked_tab_max_width);
    }
}
