package android.support.v7.view.menu;

class d<T> {

    /* renamed from: b reason: collision with root package name */
    final T f984b;

    d(T t) {
        if (t == null) {
            throw new IllegalArgumentException("Wrapped Object can not be null.");
        }
        this.f984b = t;
    }
}
