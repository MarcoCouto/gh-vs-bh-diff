package android.support.v7.view.menu;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.support.v4.c.a.a;
import android.support.v4.d.a.b;
import android.support.v4.i.c;
import android.util.Log;
import android.view.ActionProvider;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MenuItem.OnActionExpandListener;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewDebug.CapturedViewProperty;
import android.widget.LinearLayout;

public final class j implements b {
    private static String F;
    private static String G;
    private static String H;
    private static String I;
    private View A;
    private c B;
    private OnActionExpandListener C;
    private boolean D = false;
    private ContextMenuInfo E;

    /* renamed from: a reason: collision with root package name */
    h f1004a;

    /* renamed from: b reason: collision with root package name */
    private final int f1005b;
    private final int c;
    private final int d;
    private final int e;
    private CharSequence f;
    private CharSequence g;
    private Intent h;
    private char i;
    private int j = 4096;
    private char k;
    private int l = 4096;
    private Drawable m;
    private int n = 0;
    private u o;
    private Runnable p;
    private OnMenuItemClickListener q;
    private CharSequence r;
    private CharSequence s;
    private ColorStateList t = null;
    private Mode u = null;
    private boolean v = false;
    private boolean w = false;
    private boolean x = false;
    private int y = 16;
    private int z = 0;

    j(h hVar, int i2, int i3, int i4, int i5, CharSequence charSequence, int i6) {
        this.f1004a = hVar;
        this.f1005b = i3;
        this.c = i2;
        this.d = i4;
        this.e = i5;
        this.f = charSequence;
        this.z = i6;
    }

    private Drawable a(Drawable drawable) {
        if (drawable != null && this.x && (this.v || this.w)) {
            drawable = a.f(drawable).mutate();
            if (this.v) {
                a.a(drawable, this.t);
            }
            if (this.w) {
                a.a(drawable, this.u);
            }
            this.x = false;
        }
        return drawable;
    }

    /* renamed from: a */
    public b setActionView(int i2) {
        Context e2 = this.f1004a.e();
        setActionView(LayoutInflater.from(e2).inflate(i2, new LinearLayout(e2), false));
        return this;
    }

    public b a(c cVar) {
        if (this.B != null) {
            this.B.f();
        }
        this.A = null;
        this.B = cVar;
        this.f1004a.b(true);
        if (this.B != null) {
            this.B.a((c.b) new c.b() {
                public void a(boolean z) {
                    j.this.f1004a.a(j.this);
                }
            });
        }
        return this;
    }

    /* renamed from: a */
    public b setActionView(View view) {
        this.A = view;
        this.B = null;
        if (view != null && view.getId() == -1 && this.f1005b > 0) {
            view.setId(this.f1005b);
        }
        this.f1004a.b(this);
        return this;
    }

    /* renamed from: a */
    public b setContentDescription(CharSequence charSequence) {
        this.r = charSequence;
        this.f1004a.b(false);
        return this;
    }

    public c a() {
        return this.B;
    }

    /* access modifiers changed from: 0000 */
    public CharSequence a(p.a aVar) {
        return (aVar == null || !aVar.a()) ? getTitle() : getTitleCondensed();
    }

    public void a(u uVar) {
        this.o = uVar;
        uVar.setHeaderTitle(getTitle());
    }

    /* access modifiers changed from: 0000 */
    public void a(ContextMenuInfo contextMenuInfo) {
        this.E = contextMenuInfo;
    }

    public void a(boolean z2) {
        this.y = (z2 ? 4 : 0) | (this.y & -5);
    }

    /* renamed from: b */
    public b setShowAsActionFlags(int i2) {
        setShowAsAction(i2);
        return this;
    }

    /* renamed from: b */
    public b setTooltipText(CharSequence charSequence) {
        this.s = charSequence;
        this.f1004a.b(false);
        return this;
    }

    /* access modifiers changed from: 0000 */
    public void b(boolean z2) {
        int i2 = this.y;
        this.y = (z2 ? 2 : 0) | (this.y & -3);
        if (i2 != this.y) {
            this.f1004a.b(false);
        }
    }

    public boolean b() {
        if ((this.q != null && this.q.onMenuItemClick(this)) || this.f1004a.a(this.f1004a, (MenuItem) this)) {
            return true;
        }
        if (this.p != null) {
            this.p.run();
            return true;
        }
        if (this.h != null) {
            try {
                this.f1004a.e().startActivity(this.h);
                return true;
            } catch (ActivityNotFoundException e2) {
                Log.e("MenuItemImpl", "Can't find activity to handle intent; ignoring", e2);
            }
        }
        return this.B != null && this.B.d();
    }

    public int c() {
        return this.e;
    }

    /* access modifiers changed from: 0000 */
    public boolean c(boolean z2) {
        int i2 = this.y;
        this.y = (z2 ? 0 : 8) | (this.y & -9);
        return i2 != this.y;
    }

    public boolean collapseActionView() {
        if ((this.z & 8) == 0) {
            return false;
        }
        if (this.A == null) {
            return true;
        }
        if (this.C == null || this.C.onMenuItemActionCollapse(this)) {
            return this.f1004a.d(this);
        }
        return false;
    }

    /* access modifiers changed from: 0000 */
    public char d() {
        return this.f1004a.b() ? this.k : this.i;
    }

    public void d(boolean z2) {
        if (z2) {
            this.y |= 32;
        } else {
            this.y &= -33;
        }
    }

    /* access modifiers changed from: 0000 */
    public String e() {
        char d2 = d();
        if (d2 == 0) {
            return "";
        }
        StringBuilder sb = new StringBuilder(F);
        switch (d2) {
            case 8:
                sb.append(H);
                break;
            case 10:
                sb.append(G);
                break;
            case ' ':
                sb.append(I);
                break;
            default:
                sb.append(d2);
                break;
        }
        return sb.toString();
    }

    public void e(boolean z2) {
        this.D = z2;
        this.f1004a.b(false);
    }

    public boolean expandActionView() {
        if (!n()) {
            return false;
        }
        if (this.C == null || this.C.onMenuItemActionExpand(this)) {
            return this.f1004a.c(this);
        }
        return false;
    }

    /* access modifiers changed from: 0000 */
    public boolean f() {
        return this.f1004a.c() && d() != 0;
    }

    public boolean g() {
        return (this.y & 4) != 0;
    }

    public ActionProvider getActionProvider() {
        throw new UnsupportedOperationException("This is not supported, use MenuItemCompat.getActionProvider()");
    }

    public View getActionView() {
        if (this.A != null) {
            return this.A;
        }
        if (this.B == null) {
            return null;
        }
        this.A = this.B.a((MenuItem) this);
        return this.A;
    }

    public int getAlphabeticModifiers() {
        return this.l;
    }

    public char getAlphabeticShortcut() {
        return this.k;
    }

    public CharSequence getContentDescription() {
        return this.r;
    }

    public int getGroupId() {
        return this.c;
    }

    public Drawable getIcon() {
        if (this.m != null) {
            return a(this.m);
        }
        if (this.n == 0) {
            return null;
        }
        Drawable b2 = android.support.v7.c.a.b.b(this.f1004a.e(), this.n);
        this.n = 0;
        this.m = b2;
        return a(b2);
    }

    public ColorStateList getIconTintList() {
        return this.t;
    }

    public Mode getIconTintMode() {
        return this.u;
    }

    public Intent getIntent() {
        return this.h;
    }

    @CapturedViewProperty
    public int getItemId() {
        return this.f1005b;
    }

    public ContextMenuInfo getMenuInfo() {
        return this.E;
    }

    public int getNumericModifiers() {
        return this.j;
    }

    public char getNumericShortcut() {
        return this.i;
    }

    public int getOrder() {
        return this.d;
    }

    public SubMenu getSubMenu() {
        return this.o;
    }

    @CapturedViewProperty
    public CharSequence getTitle() {
        return this.f;
    }

    public CharSequence getTitleCondensed() {
        CharSequence charSequence = this.g != null ? this.g : this.f;
        return (VERSION.SDK_INT >= 18 || charSequence == null || (charSequence instanceof String)) ? charSequence : charSequence.toString();
    }

    public CharSequence getTooltipText() {
        return this.s;
    }

    public void h() {
        this.f1004a.b(this);
    }

    public boolean hasSubMenu() {
        return this.o != null;
    }

    public boolean i() {
        return this.f1004a.q();
    }

    public boolean isActionViewExpanded() {
        return this.D;
    }

    public boolean isCheckable() {
        return (this.y & 1) == 1;
    }

    public boolean isChecked() {
        return (this.y & 2) == 2;
    }

    public boolean isEnabled() {
        return (this.y & 16) != 0;
    }

    public boolean isVisible() {
        return (this.B == null || !this.B.b()) ? (this.y & 8) == 0 : (this.y & 8) == 0 && this.B.c();
    }

    public boolean j() {
        return (this.y & 32) == 32;
    }

    public boolean k() {
        return (this.z & 1) == 1;
    }

    public boolean l() {
        return (this.z & 2) == 2;
    }

    public boolean m() {
        return (this.z & 4) == 4;
    }

    public boolean n() {
        if ((this.z & 8) == 0) {
            return false;
        }
        if (this.A == null && this.B != null) {
            this.A = this.B.a((MenuItem) this);
        }
        return this.A != null;
    }

    public MenuItem setActionProvider(ActionProvider actionProvider) {
        throw new UnsupportedOperationException("This is not supported, use MenuItemCompat.setActionProvider()");
    }

    public MenuItem setAlphabeticShortcut(char c2) {
        if (this.k != c2) {
            this.k = Character.toLowerCase(c2);
            this.f1004a.b(false);
        }
        return this;
    }

    public MenuItem setAlphabeticShortcut(char c2, int i2) {
        if (!(this.k == c2 && this.l == i2)) {
            this.k = Character.toLowerCase(c2);
            this.l = KeyEvent.normalizeMetaState(i2);
            this.f1004a.b(false);
        }
        return this;
    }

    public MenuItem setCheckable(boolean z2) {
        int i2 = this.y;
        this.y = (z2 ? 1 : 0) | (this.y & -2);
        if (i2 != this.y) {
            this.f1004a.b(false);
        }
        return this;
    }

    public MenuItem setChecked(boolean z2) {
        if ((this.y & 4) != 0) {
            this.f1004a.a((MenuItem) this);
        } else {
            b(z2);
        }
        return this;
    }

    public MenuItem setEnabled(boolean z2) {
        if (z2) {
            this.y |= 16;
        } else {
            this.y &= -17;
        }
        this.f1004a.b(false);
        return this;
    }

    public MenuItem setIcon(int i2) {
        this.m = null;
        this.n = i2;
        this.x = true;
        this.f1004a.b(false);
        return this;
    }

    public MenuItem setIcon(Drawable drawable) {
        this.n = 0;
        this.m = drawable;
        this.x = true;
        this.f1004a.b(false);
        return this;
    }

    public MenuItem setIconTintList(ColorStateList colorStateList) {
        this.t = colorStateList;
        this.v = true;
        this.x = true;
        this.f1004a.b(false);
        return this;
    }

    public MenuItem setIconTintMode(Mode mode) {
        this.u = mode;
        this.w = true;
        this.x = true;
        this.f1004a.b(false);
        return this;
    }

    public MenuItem setIntent(Intent intent) {
        this.h = intent;
        return this;
    }

    public MenuItem setNumericShortcut(char c2) {
        if (this.i != c2) {
            this.i = c2;
            this.f1004a.b(false);
        }
        return this;
    }

    public MenuItem setNumericShortcut(char c2, int i2) {
        if (!(this.i == c2 && this.j == i2)) {
            this.i = c2;
            this.j = KeyEvent.normalizeMetaState(i2);
            this.f1004a.b(false);
        }
        return this;
    }

    public MenuItem setOnActionExpandListener(OnActionExpandListener onActionExpandListener) {
        this.C = onActionExpandListener;
        return this;
    }

    public MenuItem setOnMenuItemClickListener(OnMenuItemClickListener onMenuItemClickListener) {
        this.q = onMenuItemClickListener;
        return this;
    }

    public MenuItem setShortcut(char c2, char c3) {
        this.i = c2;
        this.k = Character.toLowerCase(c3);
        this.f1004a.b(false);
        return this;
    }

    public MenuItem setShortcut(char c2, char c3, int i2, int i3) {
        this.i = c2;
        this.j = KeyEvent.normalizeMetaState(i2);
        this.k = Character.toLowerCase(c3);
        this.l = KeyEvent.normalizeMetaState(i3);
        this.f1004a.b(false);
        return this;
    }

    public void setShowAsAction(int i2) {
        switch (i2 & 3) {
            case 0:
            case 1:
            case 2:
                this.z = i2;
                this.f1004a.b(this);
                return;
            default:
                throw new IllegalArgumentException("SHOW_AS_ACTION_ALWAYS, SHOW_AS_ACTION_IF_ROOM, and SHOW_AS_ACTION_NEVER are mutually exclusive.");
        }
    }

    public MenuItem setTitle(int i2) {
        return setTitle((CharSequence) this.f1004a.e().getString(i2));
    }

    public MenuItem setTitle(CharSequence charSequence) {
        this.f = charSequence;
        this.f1004a.b(false);
        if (this.o != null) {
            this.o.setHeaderTitle(charSequence);
        }
        return this;
    }

    public MenuItem setTitleCondensed(CharSequence charSequence) {
        this.g = charSequence;
        if (charSequence == null) {
            CharSequence charSequence2 = this.f;
        }
        this.f1004a.b(false);
        return this;
    }

    public MenuItem setVisible(boolean z2) {
        if (c(z2)) {
            this.f1004a.a(this);
        }
        return this;
    }

    public String toString() {
        if (this.f != null) {
            return this.f.toString();
        }
        return null;
    }
}
