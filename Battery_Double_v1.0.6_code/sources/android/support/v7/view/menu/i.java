package android.support.v7.view.menu;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnDismissListener;
import android.content.DialogInterface.OnKeyListener;
import android.os.IBinder;
import android.support.v7.a.a.g;
import android.support.v7.app.b;
import android.support.v7.view.menu.o.a;
import android.view.KeyEvent;
import android.view.KeyEvent.DispatcherState;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import com.hmatalonga.greenhub.Config;

class i implements OnClickListener, OnDismissListener, OnKeyListener, a {

    /* renamed from: a reason: collision with root package name */
    f f1002a;

    /* renamed from: b reason: collision with root package name */
    private h f1003b;
    private b c;
    private a d;

    public i(h hVar) {
        this.f1003b = hVar;
    }

    public void a() {
        if (this.c != null) {
            this.c.dismiss();
        }
    }

    public void a(IBinder iBinder) {
        h hVar = this.f1003b;
        b.a aVar = new b.a(hVar.e());
        this.f1002a = new f(aVar.a(), g.abc_list_menu_item_layout);
        this.f1002a.a((a) this);
        this.f1003b.a((o) this.f1002a);
        aVar.a(this.f1002a.d(), (OnClickListener) this);
        View o = hVar.o();
        if (o != null) {
            aVar.a(o);
        } else {
            aVar.a(hVar.n()).a(hVar.m());
        }
        aVar.a((OnKeyListener) this);
        this.c = aVar.b();
        this.c.setOnDismissListener(this);
        LayoutParams attributes = this.c.getWindow().getAttributes();
        attributes.type = Config.NOTIFICATION_BATTERY_LOW;
        if (iBinder != null) {
            attributes.token = iBinder;
        }
        attributes.flags |= 131072;
        this.c.show();
    }

    public void a(h hVar, boolean z) {
        if (z || hVar == this.f1003b) {
            a();
        }
        if (this.d != null) {
            this.d.a(hVar, z);
        }
    }

    public boolean a(h hVar) {
        if (this.d != null) {
            return this.d.a(hVar);
        }
        return false;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        this.f1003b.a((MenuItem) (j) this.f1002a.d().getItem(i), 0);
    }

    public void onDismiss(DialogInterface dialogInterface) {
        this.f1002a.a(this.f1003b, true);
    }

    public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
        if (i == 82 || i == 4) {
            if (keyEvent.getAction() == 0 && keyEvent.getRepeatCount() == 0) {
                Window window = this.c.getWindow();
                if (window != null) {
                    View decorView = window.getDecorView();
                    if (decorView != null) {
                        DispatcherState keyDispatcherState = decorView.getKeyDispatcherState();
                        if (keyDispatcherState != null) {
                            keyDispatcherState.startTracking(keyEvent, this);
                            return true;
                        }
                    }
                }
            } else if (keyEvent.getAction() == 1 && !keyEvent.isCanceled()) {
                Window window2 = this.c.getWindow();
                if (window2 != null) {
                    View decorView2 = window2.getDecorView();
                    if (decorView2 != null) {
                        DispatcherState keyDispatcherState2 = decorView2.getKeyDispatcherState();
                        if (keyDispatcherState2 != null && keyDispatcherState2.isTracking(keyEvent)) {
                            this.f1003b.a(true);
                            dialogInterface.dismiss();
                            return true;
                        }
                    }
                }
            }
        }
        return this.f1003b.performShortcut(i, keyEvent, 0);
    }
}
