package android.support.v7.view.menu;

import android.content.Context;
import android.content.res.Resources;
import android.os.Parcelable;
import android.support.v7.a.a.d;
import android.support.v7.a.a.g;
import android.support.v7.view.menu.o.a;
import android.support.v7.widget.ax;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnAttachStateChangeListener;
import android.view.View.OnKeyListener;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.FrameLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.TextView;

final class t extends m implements o, OnKeyListener, OnItemClickListener, OnDismissListener {

    /* renamed from: a reason: collision with root package name */
    final ax f1016a;

    /* renamed from: b reason: collision with root package name */
    View f1017b;
    private final Context c;
    private final h d;
    private final g e;
    private final boolean f;
    private final int g;
    private final int h;
    private final int i;
    /* access modifiers changed from: private */
    public final OnGlobalLayoutListener j = new OnGlobalLayoutListener() {
        public void onGlobalLayout() {
            if (t.this.f() && !t.this.f1016a.c()) {
                View view = t.this.f1017b;
                if (view == null || !view.isShown()) {
                    t.this.e();
                } else {
                    t.this.f1016a.d();
                }
            }
        }
    };
    private final OnAttachStateChangeListener k = new OnAttachStateChangeListener() {
        public void onViewAttachedToWindow(View view) {
        }

        public void onViewDetachedFromWindow(View view) {
            if (t.this.o != null) {
                if (!t.this.o.isAlive()) {
                    t.this.o = view.getViewTreeObserver();
                }
                t.this.o.removeGlobalOnLayoutListener(t.this.j);
            }
            view.removeOnAttachStateChangeListener(this);
        }
    };
    private OnDismissListener l;
    private View m;
    private a n;
    /* access modifiers changed from: private */
    public ViewTreeObserver o;
    private boolean p;
    private boolean q;
    private int r;
    private int s = 0;
    private boolean t;

    public t(Context context, h hVar, View view, int i2, int i3, boolean z) {
        this.c = context;
        this.d = hVar;
        this.f = z;
        this.e = new g(hVar, LayoutInflater.from(context), this.f);
        this.h = i2;
        this.i = i3;
        Resources resources = context.getResources();
        this.g = Math.max(resources.getDisplayMetrics().widthPixels / 2, resources.getDimensionPixelSize(d.abc_config_prefDialogWidth));
        this.m = view;
        this.f1016a = new ax(this.c, null, this.h, this.i);
        hVar.a((o) this, context);
    }

    private boolean j() {
        if (f()) {
            return true;
        }
        if (this.p || this.m == null) {
            return false;
        }
        this.f1017b = this.m;
        this.f1016a.a((OnDismissListener) this);
        this.f1016a.a((OnItemClickListener) this);
        this.f1016a.a(true);
        View view = this.f1017b;
        boolean z = this.o == null;
        this.o = view.getViewTreeObserver();
        if (z) {
            this.o.addOnGlobalLayoutListener(this.j);
        }
        view.addOnAttachStateChangeListener(this.k);
        this.f1016a.b(view);
        this.f1016a.e(this.s);
        if (!this.q) {
            this.r = a(this.e, null, this.c, this.g);
            this.q = true;
        }
        this.f1016a.g(this.r);
        this.f1016a.h(2);
        this.f1016a.a(i());
        this.f1016a.d();
        ListView g2 = this.f1016a.g();
        g2.setOnKeyListener(this);
        if (this.t && this.d.m() != null) {
            FrameLayout frameLayout = (FrameLayout) LayoutInflater.from(this.c).inflate(g.abc_popup_menu_header_item_layout, g2, false);
            TextView textView = (TextView) frameLayout.findViewById(16908310);
            if (textView != null) {
                textView.setText(this.d.m());
            }
            frameLayout.setEnabled(false);
            g2.addHeaderView(frameLayout, null, false);
        }
        this.f1016a.a((ListAdapter) this.e);
        this.f1016a.d();
        return true;
    }

    public void a(int i2) {
        this.s = i2;
    }

    public void a(Parcelable parcelable) {
    }

    public void a(h hVar) {
    }

    public void a(h hVar, boolean z) {
        if (hVar == this.d) {
            e();
            if (this.n != null) {
                this.n.a(hVar, z);
            }
        }
    }

    public void a(a aVar) {
        this.n = aVar;
    }

    public void a(View view) {
        this.m = view;
    }

    public void a(OnDismissListener onDismissListener) {
        this.l = onDismissListener;
    }

    public void a(boolean z) {
        this.q = false;
        if (this.e != null) {
            this.e.notifyDataSetChanged();
        }
    }

    public boolean a() {
        return false;
    }

    public boolean a(u uVar) {
        if (uVar.hasVisibleItems()) {
            n nVar = new n(this.c, uVar, this.f1017b, this.f, this.h, this.i);
            nVar.a(this.n);
            nVar.a(m.b((h) uVar));
            nVar.a(this.s);
            nVar.a(this.l);
            this.l = null;
            this.d.a(false);
            if (nVar.a(this.f1016a.j(), this.f1016a.k())) {
                if (this.n != null) {
                    this.n.a(uVar);
                }
                return true;
            }
        }
        return false;
    }

    public void b(int i2) {
        this.f1016a.c(i2);
    }

    public void b(boolean z) {
        this.e.a(z);
    }

    public Parcelable c() {
        return null;
    }

    public void c(int i2) {
        this.f1016a.d(i2);
    }

    public void c(boolean z) {
        this.t = z;
    }

    public void d() {
        if (!j()) {
            throw new IllegalStateException("StandardMenuPopup cannot be used without an anchor");
        }
    }

    public void e() {
        if (f()) {
            this.f1016a.e();
        }
    }

    public boolean f() {
        return !this.p && this.f1016a.f();
    }

    public ListView g() {
        return this.f1016a.g();
    }

    public void onDismiss() {
        this.p = true;
        this.d.close();
        if (this.o != null) {
            if (!this.o.isAlive()) {
                this.o = this.f1017b.getViewTreeObserver();
            }
            this.o.removeGlobalOnLayoutListener(this.j);
            this.o = null;
        }
        this.f1017b.removeOnAttachStateChangeListener(this.k);
        if (this.l != null) {
            this.l.onDismiss();
        }
    }

    public boolean onKey(View view, int i2, KeyEvent keyEvent) {
        if (keyEvent.getAction() != 1 || i2 != 82) {
            return false;
        }
        e();
        return true;
    }
}
