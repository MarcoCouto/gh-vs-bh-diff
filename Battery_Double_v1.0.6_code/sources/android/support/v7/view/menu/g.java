package android.support.v7.view.menu;

import android.support.v7.view.menu.p.a;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import java.util.ArrayList;

public class g extends BaseAdapter {

    /* renamed from: a reason: collision with root package name */
    static final int f998a = android.support.v7.a.a.g.abc_popup_menu_item_layout;

    /* renamed from: b reason: collision with root package name */
    h f999b;
    private int c = -1;
    private boolean d;
    private final boolean e;
    private final LayoutInflater f;

    public g(h hVar, LayoutInflater layoutInflater, boolean z) {
        this.e = z;
        this.f = layoutInflater;
        this.f999b = hVar;
        b();
    }

    public h a() {
        return this.f999b;
    }

    /* renamed from: a */
    public j getItem(int i) {
        ArrayList i2 = this.e ? this.f999b.l() : this.f999b.i();
        if (this.c >= 0 && i >= this.c) {
            i++;
        }
        return (j) i2.get(i);
    }

    public void a(boolean z) {
        this.d = z;
    }

    /* access modifiers changed from: 0000 */
    public void b() {
        j r = this.f999b.r();
        if (r != null) {
            ArrayList l = this.f999b.l();
            int size = l.size();
            for (int i = 0; i < size; i++) {
                if (((j) l.get(i)) == r) {
                    this.c = i;
                    return;
                }
            }
        }
        this.c = -1;
    }

    public int getCount() {
        ArrayList i = this.e ? this.f999b.l() : this.f999b.i();
        return this.c < 0 ? i.size() : i.size() - 1;
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        View view2 = view == null ? this.f.inflate(f998a, viewGroup, false) : view;
        a aVar = (a) view2;
        if (this.d) {
            ((ListMenuItemView) view2).setForceShowIcon(true);
        }
        aVar.a(getItem(i), 0);
        return view2;
    }

    public void notifyDataSetChanged() {
        b();
        super.notifyDataSetChanged();
    }
}
