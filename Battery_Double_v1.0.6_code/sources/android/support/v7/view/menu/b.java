package android.support.v7.view.menu;

import android.content.Context;
import android.support.v7.view.menu.o.a;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;

public abstract class b implements o {

    /* renamed from: a reason: collision with root package name */
    protected Context f981a;

    /* renamed from: b reason: collision with root package name */
    protected Context f982b;
    protected h c;
    protected LayoutInflater d;
    protected LayoutInflater e;
    protected p f;
    private a g;
    private int h;
    private int i;
    private int j;

    public b(Context context, int i2, int i3) {
        this.f981a = context;
        this.d = LayoutInflater.from(context);
        this.h = i2;
        this.i = i3;
    }

    public p a(ViewGroup viewGroup) {
        if (this.f == null) {
            this.f = (p) this.d.inflate(this.h, viewGroup, false);
            this.f.a(this.c);
            a(true);
        }
        return this.f;
    }

    public View a(j jVar, View view, ViewGroup viewGroup) {
        p.a b2 = view instanceof p.a ? (p.a) view : b(viewGroup);
        a(jVar, b2);
        return (View) b2;
    }

    public void a(int i2) {
        this.j = i2;
    }

    public void a(Context context, h hVar) {
        this.f982b = context;
        this.e = LayoutInflater.from(this.f982b);
        this.c = hVar;
    }

    public void a(h hVar, boolean z) {
        if (this.g != null) {
            this.g.a(hVar, z);
        }
    }

    public abstract void a(j jVar, p.a aVar);

    public void a(a aVar) {
        this.g = aVar;
    }

    /* access modifiers changed from: protected */
    public void a(View view, int i2) {
        ViewGroup viewGroup = (ViewGroup) view.getParent();
        if (viewGroup != null) {
            viewGroup.removeView(view);
        }
        ((ViewGroup) this.f).addView(view, i2);
    }

    public void a(boolean z) {
        int i2;
        int i3;
        ViewGroup viewGroup = (ViewGroup) this.f;
        if (viewGroup != null) {
            if (this.c != null) {
                this.c.j();
                ArrayList i4 = this.c.i();
                int size = i4.size();
                int i5 = 0;
                i2 = 0;
                while (i5 < size) {
                    j jVar = (j) i4.get(i5);
                    if (a(i2, jVar)) {
                        View childAt = viewGroup.getChildAt(i2);
                        j jVar2 = childAt instanceof p.a ? ((p.a) childAt).getItemData() : null;
                        View a2 = a(jVar, childAt, viewGroup);
                        if (jVar != jVar2) {
                            a2.setPressed(false);
                            a2.jumpDrawablesToCurrentState();
                        }
                        if (a2 != childAt) {
                            a(a2, i2);
                        }
                        i3 = i2 + 1;
                    } else {
                        i3 = i2;
                    }
                    i5++;
                    i2 = i3;
                }
            } else {
                i2 = 0;
            }
            while (i2 < viewGroup.getChildCount()) {
                if (!a(viewGroup, i2)) {
                    i2++;
                }
            }
        }
    }

    public boolean a() {
        return false;
    }

    public boolean a(int i2, j jVar) {
        return true;
    }

    public boolean a(h hVar, j jVar) {
        return false;
    }

    public boolean a(u uVar) {
        if (this.g != null) {
            return this.g.a(uVar);
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean a(ViewGroup viewGroup, int i2) {
        viewGroup.removeViewAt(i2);
        return true;
    }

    public int b() {
        return this.j;
    }

    public p.a b(ViewGroup viewGroup) {
        return (p.a) this.d.inflate(this.i, viewGroup, false);
    }

    public boolean b(h hVar, j jVar) {
        return false;
    }

    public a d() {
        return this.g;
    }
}
