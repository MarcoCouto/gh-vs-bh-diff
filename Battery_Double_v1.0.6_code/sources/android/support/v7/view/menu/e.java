package android.support.v7.view.menu;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.Parcelable;
import android.os.SystemClock;
import android.support.v4.i.t;
import android.support.v7.a.a.d;
import android.support.v7.a.a.g;
import android.support.v7.widget.aw;
import android.support.v7.widget.ax;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnAttachStateChangeListener;
import android.view.View.OnKeyListener;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.FrameLayout;
import android.widget.HeaderViewListAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

final class e extends m implements o, OnKeyListener, OnDismissListener {

    /* renamed from: a reason: collision with root package name */
    final Handler f985a;

    /* renamed from: b reason: collision with root package name */
    final List<a> f986b = new ArrayList();
    View c;
    boolean d;
    private final Context e;
    private final int f;
    private final int g;
    private final int h;
    private final boolean i;
    private final List<h> j = new LinkedList();
    /* access modifiers changed from: private */
    public final OnGlobalLayoutListener k = new OnGlobalLayoutListener() {
        public void onGlobalLayout() {
            if (e.this.f() && e.this.f986b.size() > 0 && !((a) e.this.f986b.get(0)).f992a.c()) {
                View view = e.this.c;
                if (view == null || !view.isShown()) {
                    e.this.e();
                    return;
                }
                for (a aVar : e.this.f986b) {
                    aVar.f992a.d();
                }
            }
        }
    };
    private final OnAttachStateChangeListener l = new OnAttachStateChangeListener() {
        public void onViewAttachedToWindow(View view) {
        }

        public void onViewDetachedFromWindow(View view) {
            if (e.this.y != null) {
                if (!e.this.y.isAlive()) {
                    e.this.y = view.getViewTreeObserver();
                }
                e.this.y.removeGlobalOnLayoutListener(e.this.k);
            }
            view.removeOnAttachStateChangeListener(this);
        }
    };
    private final aw m = new aw() {
        public void a(h hVar, MenuItem menuItem) {
            e.this.f985a.removeCallbacksAndMessages(hVar);
        }

        public void b(final h hVar, final MenuItem menuItem) {
            int i;
            e.this.f985a.removeCallbacksAndMessages(null);
            int i2 = 0;
            int size = e.this.f986b.size();
            while (true) {
                if (i2 >= size) {
                    i = -1;
                    break;
                } else if (hVar == ((a) e.this.f986b.get(i2)).f993b) {
                    i = i2;
                    break;
                } else {
                    i2++;
                }
            }
            if (i != -1) {
                int i3 = i + 1;
                final a aVar = i3 < e.this.f986b.size() ? (a) e.this.f986b.get(i3) : null;
                e.this.f985a.postAtTime(new Runnable() {
                    public void run() {
                        if (aVar != null) {
                            e.this.d = true;
                            aVar.f993b.a(false);
                            e.this.d = false;
                        }
                        if (menuItem.isEnabled() && menuItem.hasSubMenu()) {
                            hVar.a(menuItem, 4);
                        }
                    }
                }, hVar, SystemClock.uptimeMillis() + 200);
            }
        }
    };
    private int n = 0;
    private int o = 0;
    private View p;
    private int q;
    private boolean r;
    private boolean s;
    private int t;
    private int u;
    private boolean v;
    private boolean w;
    private android.support.v7.view.menu.o.a x;
    /* access modifiers changed from: private */
    public ViewTreeObserver y;
    private OnDismissListener z;

    private static class a {

        /* renamed from: a reason: collision with root package name */
        public final ax f992a;

        /* renamed from: b reason: collision with root package name */
        public final h f993b;
        public final int c;

        public a(ax axVar, h hVar, int i) {
            this.f992a = axVar;
            this.f993b = hVar;
            this.c = i;
        }

        public ListView a() {
            return this.f992a.g();
        }
    }

    public e(Context context, View view, int i2, int i3, boolean z2) {
        this.e = context;
        this.p = view;
        this.g = i2;
        this.h = i3;
        this.i = z2;
        this.v = false;
        this.q = k();
        Resources resources = context.getResources();
        this.f = Math.max(resources.getDisplayMetrics().widthPixels / 2, resources.getDimensionPixelSize(d.abc_config_prefDialogWidth));
        this.f985a = new Handler();
    }

    private MenuItem a(h hVar, h hVar2) {
        int size = hVar.size();
        for (int i2 = 0; i2 < size; i2++) {
            MenuItem item = hVar.getItem(i2);
            if (item.hasSubMenu() && hVar2 == item.getSubMenu()) {
                return item;
            }
        }
        return null;
    }

    private View a(a aVar, h hVar) {
        g gVar;
        int i2;
        int i3;
        int i4 = 0;
        MenuItem a2 = a(aVar.f993b, hVar);
        if (a2 == null) {
            return null;
        }
        ListView a3 = aVar.a();
        ListAdapter adapter = a3.getAdapter();
        if (adapter instanceof HeaderViewListAdapter) {
            HeaderViewListAdapter headerViewListAdapter = (HeaderViewListAdapter) adapter;
            i2 = headerViewListAdapter.getHeadersCount();
            gVar = (g) headerViewListAdapter.getWrappedAdapter();
        } else {
            gVar = (g) adapter;
            i2 = 0;
        }
        int count = gVar.getCount();
        while (true) {
            if (i4 >= count) {
                i3 = -1;
                break;
            } else if (a2 == gVar.getItem(i4)) {
                i3 = i4;
                break;
            } else {
                i4++;
            }
        }
        if (i3 == -1) {
            return null;
        }
        int firstVisiblePosition = (i3 + i2) - a3.getFirstVisiblePosition();
        if (firstVisiblePosition < 0 || firstVisiblePosition >= a3.getChildCount()) {
            return null;
        }
        return a3.getChildAt(firstVisiblePosition);
    }

    private void c(h hVar) {
        View view;
        a aVar;
        int i2;
        int i3;
        LayoutInflater from = LayoutInflater.from(this.e);
        g gVar = new g(hVar, from, this.i);
        if (!f() && this.v) {
            gVar.a(true);
        } else if (f()) {
            gVar.a(m.b(hVar));
        }
        int a2 = a(gVar, null, this.e, this.f);
        ax j2 = j();
        j2.a((ListAdapter) gVar);
        j2.g(a2);
        j2.e(this.o);
        if (this.f986b.size() > 0) {
            a aVar2 = (a) this.f986b.get(this.f986b.size() - 1);
            view = a(aVar2, hVar);
            aVar = aVar2;
        } else {
            view = null;
            aVar = null;
        }
        if (view != null) {
            j2.c(false);
            j2.a((Object) null);
            int d2 = d(a2);
            boolean z2 = d2 == 1;
            this.q = d2;
            if (VERSION.SDK_INT >= 26) {
                j2.b(view);
                i3 = 0;
                i2 = 0;
            } else {
                int[] iArr = new int[2];
                this.p.getLocationOnScreen(iArr);
                int[] iArr2 = new int[2];
                view.getLocationOnScreen(iArr2);
                i2 = iArr2[0] - iArr[0];
                i3 = iArr2[1] - iArr[1];
            }
            int i4 = (this.o & 5) == 5 ? z2 ? i2 + a2 : i2 - view.getWidth() : z2 ? view.getWidth() + i2 : i2 - a2;
            j2.c(i4);
            j2.b(true);
            j2.d(i3);
        } else {
            if (this.r) {
                j2.c(this.t);
            }
            if (this.s) {
                j2.d(this.u);
            }
            j2.a(i());
        }
        this.f986b.add(new a(j2, hVar, this.q));
        j2.d();
        ListView g2 = j2.g();
        g2.setOnKeyListener(this);
        if (aVar == null && this.w && hVar.m() != null) {
            FrameLayout frameLayout = (FrameLayout) from.inflate(g.abc_popup_menu_header_item_layout, g2, false);
            TextView textView = (TextView) frameLayout.findViewById(16908310);
            frameLayout.setEnabled(false);
            textView.setText(hVar.m());
            g2.addHeaderView(frameLayout, null, false);
            j2.d();
        }
    }

    private int d(int i2) {
        ListView a2 = ((a) this.f986b.get(this.f986b.size() - 1)).a();
        int[] iArr = new int[2];
        a2.getLocationOnScreen(iArr);
        Rect rect = new Rect();
        this.c.getWindowVisibleDisplayFrame(rect);
        if (this.q != 1) {
            return iArr[0] - i2 < 0 ? 1 : 0;
        }
        return (a2.getWidth() + iArr[0]) + i2 > rect.right ? 0 : 1;
    }

    private int d(h hVar) {
        int size = this.f986b.size();
        for (int i2 = 0; i2 < size; i2++) {
            if (hVar == ((a) this.f986b.get(i2)).f993b) {
                return i2;
            }
        }
        return -1;
    }

    private ax j() {
        ax axVar = new ax(this.e, null, this.g, this.h);
        axVar.a(this.m);
        axVar.a((OnItemClickListener) this);
        axVar.a((OnDismissListener) this);
        axVar.b(this.p);
        axVar.e(this.o);
        axVar.a(true);
        axVar.h(2);
        return axVar;
    }

    private int k() {
        return t.e(this.p) == 1 ? 0 : 1;
    }

    public void a(int i2) {
        if (this.n != i2) {
            this.n = i2;
            this.o = android.support.v4.i.e.a(i2, t.e(this.p));
        }
    }

    public void a(Parcelable parcelable) {
    }

    public void a(h hVar) {
        hVar.a((o) this, this.e);
        if (f()) {
            c(hVar);
        } else {
            this.j.add(hVar);
        }
    }

    public void a(h hVar, boolean z2) {
        int d2 = d(hVar);
        if (d2 >= 0) {
            int i2 = d2 + 1;
            if (i2 < this.f986b.size()) {
                ((a) this.f986b.get(i2)).f993b.a(false);
            }
            a aVar = (a) this.f986b.remove(d2);
            aVar.f993b.b((o) this);
            if (this.d) {
                aVar.f992a.b(null);
                aVar.f992a.b(0);
            }
            aVar.f992a.e();
            int size = this.f986b.size();
            if (size > 0) {
                this.q = ((a) this.f986b.get(size - 1)).c;
            } else {
                this.q = k();
            }
            if (size == 0) {
                e();
                if (this.x != null) {
                    this.x.a(hVar, true);
                }
                if (this.y != null) {
                    if (this.y.isAlive()) {
                        this.y.removeGlobalOnLayoutListener(this.k);
                    }
                    this.y = null;
                }
                this.c.removeOnAttachStateChangeListener(this.l);
                this.z.onDismiss();
            } else if (z2) {
                ((a) this.f986b.get(0)).f993b.a(false);
            }
        }
    }

    public void a(android.support.v7.view.menu.o.a aVar) {
        this.x = aVar;
    }

    public void a(View view) {
        if (this.p != view) {
            this.p = view;
            this.o = android.support.v4.i.e.a(this.n, t.e(this.p));
        }
    }

    public void a(OnDismissListener onDismissListener) {
        this.z = onDismissListener;
    }

    public void a(boolean z2) {
        for (a a2 : this.f986b) {
            a(a2.a().getAdapter()).notifyDataSetChanged();
        }
    }

    public boolean a() {
        return false;
    }

    public boolean a(u uVar) {
        for (a aVar : this.f986b) {
            if (uVar == aVar.f993b) {
                aVar.a().requestFocus();
                return true;
            }
        }
        if (!uVar.hasVisibleItems()) {
            return false;
        }
        a((h) uVar);
        if (this.x != null) {
            this.x.a(uVar);
        }
        return true;
    }

    public void b(int i2) {
        this.r = true;
        this.t = i2;
    }

    public void b(boolean z2) {
        this.v = z2;
    }

    public Parcelable c() {
        return null;
    }

    public void c(int i2) {
        this.s = true;
        this.u = i2;
    }

    public void c(boolean z2) {
        this.w = z2;
    }

    public void d() {
        if (!f()) {
            for (h c2 : this.j) {
                c(c2);
            }
            this.j.clear();
            this.c = this.p;
            if (this.c != null) {
                boolean z2 = this.y == null;
                this.y = this.c.getViewTreeObserver();
                if (z2) {
                    this.y.addOnGlobalLayoutListener(this.k);
                }
                this.c.addOnAttachStateChangeListener(this.l);
            }
        }
    }

    public void e() {
        int size = this.f986b.size();
        if (size > 0) {
            a[] aVarArr = (a[]) this.f986b.toArray(new a[size]);
            for (int i2 = size - 1; i2 >= 0; i2--) {
                a aVar = aVarArr[i2];
                if (aVar.f992a.f()) {
                    aVar.f992a.e();
                }
            }
        }
    }

    public boolean f() {
        return this.f986b.size() > 0 && ((a) this.f986b.get(0)).f992a.f();
    }

    public ListView g() {
        if (this.f986b.isEmpty()) {
            return null;
        }
        return ((a) this.f986b.get(this.f986b.size() - 1)).a();
    }

    /* access modifiers changed from: protected */
    public boolean h() {
        return false;
    }

    public void onDismiss() {
        a aVar;
        int size = this.f986b.size();
        int i2 = 0;
        while (true) {
            if (i2 >= size) {
                aVar = null;
                break;
            }
            aVar = (a) this.f986b.get(i2);
            if (!aVar.f992a.f()) {
                break;
            }
            i2++;
        }
        if (aVar != null) {
            aVar.f993b.a(false);
        }
    }

    public boolean onKey(View view, int i2, KeyEvent keyEvent) {
        if (keyEvent.getAction() != 1 || i2 != 82) {
            return false;
        }
        e();
        return true;
    }
}
