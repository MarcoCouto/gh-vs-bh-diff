package android.support.v7.view.menu;

import android.content.Context;
import android.support.v7.view.menu.h.b;
import android.support.v7.widget.bo;
import android.util.AttributeSet;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public final class ExpandedMenuView extends ListView implements b, p, OnItemClickListener {

    /* renamed from: a reason: collision with root package name */
    private static final int[] f975a = {16842964, 16843049};

    /* renamed from: b reason: collision with root package name */
    private h f976b;
    private int c;

    public ExpandedMenuView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 16842868);
    }

    public ExpandedMenuView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet);
        setOnItemClickListener(this);
        bo a2 = bo.a(context, attributeSet, f975a, i, 0);
        if (a2.g(0)) {
            setBackgroundDrawable(a2.a(0));
        }
        if (a2.g(1)) {
            setDivider(a2.a(1));
        }
        a2.a();
    }

    public void a(h hVar) {
        this.f976b = hVar;
    }

    public boolean a(j jVar) {
        return this.f976b.a((MenuItem) jVar, 0);
    }

    public int getWindowAnimations() {
        return this.c;
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        setChildrenDrawingCacheEnabled(false);
    }

    public void onItemClick(AdapterView adapterView, View view, int i, long j) {
        a((j) getAdapter().getItem(i));
    }
}
