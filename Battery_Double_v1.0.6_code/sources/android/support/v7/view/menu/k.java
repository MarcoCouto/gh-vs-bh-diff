package android.support.v7.view.menu;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.ActionProvider;
import android.view.CollapsibleActionView;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.MenuItem;
import android.view.MenuItem.OnActionExpandListener;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.SubMenu;
import android.view.View;
import android.widget.FrameLayout;
import java.lang.reflect.Method;

public class k extends c<android.support.v4.d.a.b> implements MenuItem {
    private Method c;

    class a extends android.support.v4.i.c {

        /* renamed from: a reason: collision with root package name */
        final ActionProvider f1007a;

        public a(Context context, ActionProvider actionProvider) {
            super(context);
            this.f1007a = actionProvider;
        }

        public View a() {
            return this.f1007a.onCreateActionView();
        }

        public void a(SubMenu subMenu) {
            this.f1007a.onPrepareSubMenu(k.this.a(subMenu));
        }

        public boolean d() {
            return this.f1007a.onPerformDefaultAction();
        }

        public boolean e() {
            return this.f1007a.hasSubMenu();
        }
    }

    static class b extends FrameLayout implements android.support.v7.view.c {

        /* renamed from: a reason: collision with root package name */
        final CollapsibleActionView f1009a;

        b(View view) {
            super(view.getContext());
            this.f1009a = (CollapsibleActionView) view;
            addView(view);
        }

        public void a() {
            this.f1009a.onActionViewExpanded();
        }

        public void b() {
            this.f1009a.onActionViewCollapsed();
        }

        /* access modifiers changed from: 0000 */
        public View c() {
            return (View) this.f1009a;
        }
    }

    private class c extends d<OnActionExpandListener> implements OnActionExpandListener {
        c(OnActionExpandListener onActionExpandListener) {
            super(onActionExpandListener);
        }

        public boolean onMenuItemActionCollapse(MenuItem menuItem) {
            return ((OnActionExpandListener) this.f984b).onMenuItemActionCollapse(k.this.a(menuItem));
        }

        public boolean onMenuItemActionExpand(MenuItem menuItem) {
            return ((OnActionExpandListener) this.f984b).onMenuItemActionExpand(k.this.a(menuItem));
        }
    }

    private class d extends d<OnMenuItemClickListener> implements OnMenuItemClickListener {
        d(OnMenuItemClickListener onMenuItemClickListener) {
            super(onMenuItemClickListener);
        }

        public boolean onMenuItemClick(MenuItem menuItem) {
            return ((OnMenuItemClickListener) this.f984b).onMenuItemClick(k.this.a(menuItem));
        }
    }

    k(Context context, android.support.v4.d.a.b bVar) {
        super(context, bVar);
    }

    /* access modifiers changed from: 0000 */
    public a a(ActionProvider actionProvider) {
        return new a(this.f983a, actionProvider);
    }

    public void a(boolean z) {
        try {
            if (this.c == null) {
                this.c = ((android.support.v4.d.a.b) this.f984b).getClass().getDeclaredMethod("setExclusiveCheckable", new Class[]{Boolean.TYPE});
            }
            this.c.invoke(this.f984b, new Object[]{Boolean.valueOf(z)});
        } catch (Exception e) {
            Log.w("MenuItemWrapper", "Error while calling setExclusiveCheckable", e);
        }
    }

    public boolean collapseActionView() {
        return ((android.support.v4.d.a.b) this.f984b).collapseActionView();
    }

    public boolean expandActionView() {
        return ((android.support.v4.d.a.b) this.f984b).expandActionView();
    }

    public ActionProvider getActionProvider() {
        android.support.v4.i.c a2 = ((android.support.v4.d.a.b) this.f984b).a();
        if (a2 instanceof a) {
            return ((a) a2).f1007a;
        }
        return null;
    }

    public View getActionView() {
        View actionView = ((android.support.v4.d.a.b) this.f984b).getActionView();
        return actionView instanceof b ? ((b) actionView).c() : actionView;
    }

    public int getAlphabeticModifiers() {
        return ((android.support.v4.d.a.b) this.f984b).getAlphabeticModifiers();
    }

    public char getAlphabeticShortcut() {
        return ((android.support.v4.d.a.b) this.f984b).getAlphabeticShortcut();
    }

    public CharSequence getContentDescription() {
        return ((android.support.v4.d.a.b) this.f984b).getContentDescription();
    }

    public int getGroupId() {
        return ((android.support.v4.d.a.b) this.f984b).getGroupId();
    }

    public Drawable getIcon() {
        return ((android.support.v4.d.a.b) this.f984b).getIcon();
    }

    public ColorStateList getIconTintList() {
        return ((android.support.v4.d.a.b) this.f984b).getIconTintList();
    }

    public Mode getIconTintMode() {
        return ((android.support.v4.d.a.b) this.f984b).getIconTintMode();
    }

    public Intent getIntent() {
        return ((android.support.v4.d.a.b) this.f984b).getIntent();
    }

    public int getItemId() {
        return ((android.support.v4.d.a.b) this.f984b).getItemId();
    }

    public ContextMenuInfo getMenuInfo() {
        return ((android.support.v4.d.a.b) this.f984b).getMenuInfo();
    }

    public int getNumericModifiers() {
        return ((android.support.v4.d.a.b) this.f984b).getNumericModifiers();
    }

    public char getNumericShortcut() {
        return ((android.support.v4.d.a.b) this.f984b).getNumericShortcut();
    }

    public int getOrder() {
        return ((android.support.v4.d.a.b) this.f984b).getOrder();
    }

    public SubMenu getSubMenu() {
        return a(((android.support.v4.d.a.b) this.f984b).getSubMenu());
    }

    public CharSequence getTitle() {
        return ((android.support.v4.d.a.b) this.f984b).getTitle();
    }

    public CharSequence getTitleCondensed() {
        return ((android.support.v4.d.a.b) this.f984b).getTitleCondensed();
    }

    public CharSequence getTooltipText() {
        return ((android.support.v4.d.a.b) this.f984b).getTooltipText();
    }

    public boolean hasSubMenu() {
        return ((android.support.v4.d.a.b) this.f984b).hasSubMenu();
    }

    public boolean isActionViewExpanded() {
        return ((android.support.v4.d.a.b) this.f984b).isActionViewExpanded();
    }

    public boolean isCheckable() {
        return ((android.support.v4.d.a.b) this.f984b).isCheckable();
    }

    public boolean isChecked() {
        return ((android.support.v4.d.a.b) this.f984b).isChecked();
    }

    public boolean isEnabled() {
        return ((android.support.v4.d.a.b) this.f984b).isEnabled();
    }

    public boolean isVisible() {
        return ((android.support.v4.d.a.b) this.f984b).isVisible();
    }

    public MenuItem setActionProvider(ActionProvider actionProvider) {
        ((android.support.v4.d.a.b) this.f984b).a((android.support.v4.i.c) actionProvider != null ? a(actionProvider) : null);
        return this;
    }

    public MenuItem setActionView(int i) {
        ((android.support.v4.d.a.b) this.f984b).setActionView(i);
        View actionView = ((android.support.v4.d.a.b) this.f984b).getActionView();
        if (actionView instanceof CollapsibleActionView) {
            ((android.support.v4.d.a.b) this.f984b).setActionView((View) new b(actionView));
        }
        return this;
    }

    public MenuItem setActionView(View view) {
        if (view instanceof CollapsibleActionView) {
            view = new b(view);
        }
        ((android.support.v4.d.a.b) this.f984b).setActionView(view);
        return this;
    }

    public MenuItem setAlphabeticShortcut(char c2) {
        ((android.support.v4.d.a.b) this.f984b).setAlphabeticShortcut(c2);
        return this;
    }

    public MenuItem setAlphabeticShortcut(char c2, int i) {
        ((android.support.v4.d.a.b) this.f984b).setAlphabeticShortcut(c2, i);
        return this;
    }

    public MenuItem setCheckable(boolean z) {
        ((android.support.v4.d.a.b) this.f984b).setCheckable(z);
        return this;
    }

    public MenuItem setChecked(boolean z) {
        ((android.support.v4.d.a.b) this.f984b).setChecked(z);
        return this;
    }

    public MenuItem setContentDescription(CharSequence charSequence) {
        ((android.support.v4.d.a.b) this.f984b).a(charSequence);
        return this;
    }

    public MenuItem setEnabled(boolean z) {
        ((android.support.v4.d.a.b) this.f984b).setEnabled(z);
        return this;
    }

    public MenuItem setIcon(int i) {
        ((android.support.v4.d.a.b) this.f984b).setIcon(i);
        return this;
    }

    public MenuItem setIcon(Drawable drawable) {
        ((android.support.v4.d.a.b) this.f984b).setIcon(drawable);
        return this;
    }

    public MenuItem setIconTintList(ColorStateList colorStateList) {
        ((android.support.v4.d.a.b) this.f984b).setIconTintList(colorStateList);
        return this;
    }

    public MenuItem setIconTintMode(Mode mode) {
        ((android.support.v4.d.a.b) this.f984b).setIconTintMode(mode);
        return this;
    }

    public MenuItem setIntent(Intent intent) {
        ((android.support.v4.d.a.b) this.f984b).setIntent(intent);
        return this;
    }

    public MenuItem setNumericShortcut(char c2) {
        ((android.support.v4.d.a.b) this.f984b).setNumericShortcut(c2);
        return this;
    }

    public MenuItem setNumericShortcut(char c2, int i) {
        ((android.support.v4.d.a.b) this.f984b).setNumericShortcut(c2, i);
        return this;
    }

    public MenuItem setOnActionExpandListener(OnActionExpandListener onActionExpandListener) {
        ((android.support.v4.d.a.b) this.f984b).setOnActionExpandListener(onActionExpandListener != null ? new c(onActionExpandListener) : null);
        return this;
    }

    public MenuItem setOnMenuItemClickListener(OnMenuItemClickListener onMenuItemClickListener) {
        ((android.support.v4.d.a.b) this.f984b).setOnMenuItemClickListener(onMenuItemClickListener != null ? new d(onMenuItemClickListener) : null);
        return this;
    }

    public MenuItem setShortcut(char c2, char c3) {
        ((android.support.v4.d.a.b) this.f984b).setShortcut(c2, c3);
        return this;
    }

    public MenuItem setShortcut(char c2, char c3, int i, int i2) {
        ((android.support.v4.d.a.b) this.f984b).setShortcut(c2, c3, i, i2);
        return this;
    }

    public void setShowAsAction(int i) {
        ((android.support.v4.d.a.b) this.f984b).setShowAsAction(i);
    }

    public MenuItem setShowAsActionFlags(int i) {
        ((android.support.v4.d.a.b) this.f984b).setShowAsActionFlags(i);
        return this;
    }

    public MenuItem setTitle(int i) {
        ((android.support.v4.d.a.b) this.f984b).setTitle(i);
        return this;
    }

    public MenuItem setTitle(CharSequence charSequence) {
        ((android.support.v4.d.a.b) this.f984b).setTitle(charSequence);
        return this;
    }

    public MenuItem setTitleCondensed(CharSequence charSequence) {
        ((android.support.v4.d.a.b) this.f984b).setTitleCondensed(charSequence);
        return this;
    }

    public MenuItem setTooltipText(CharSequence charSequence) {
        ((android.support.v4.d.a.b) this.f984b).b(charSequence);
        return this;
    }

    public MenuItem setVisible(boolean z) {
        return ((android.support.v4.d.a.b) this.f984b).setVisible(z);
    }
}
