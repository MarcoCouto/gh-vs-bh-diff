package android.support.v7.view.menu;

import android.content.Context;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Build.VERSION;
import android.support.v4.i.e;
import android.support.v4.i.t;
import android.support.v7.a.a.d;
import android.support.v7.view.menu.o.a;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.PopupWindow.OnDismissListener;

public class n {

    /* renamed from: a reason: collision with root package name */
    private final Context f1013a;

    /* renamed from: b reason: collision with root package name */
    private final h f1014b;
    private final boolean c;
    private final int d;
    private final int e;
    private View f;
    private int g;
    private boolean h;
    private a i;
    private m j;
    private OnDismissListener k;
    private final OnDismissListener l;

    public n(Context context, h hVar, View view, boolean z, int i2) {
        this(context, hVar, view, z, i2, 0);
    }

    public n(Context context, h hVar, View view, boolean z, int i2, int i3) {
        this.g = 8388611;
        this.l = new OnDismissListener() {
            public void onDismiss() {
                n.this.e();
            }
        };
        this.f1013a = context;
        this.f1014b = hVar;
        this.f = view;
        this.c = z;
        this.d = i2;
        this.e = i3;
    }

    private void a(int i2, int i3, boolean z, boolean z2) {
        m b2 = b();
        b2.c(z2);
        if (z) {
            if ((e.a(this.g, t.e(this.f)) & 7) == 5) {
                i2 += this.f.getWidth();
            }
            b2.b(i2);
            b2.c(i3);
            int i4 = (int) ((this.f1013a.getResources().getDisplayMetrics().density * 48.0f) / 2.0f);
            b2.a(new Rect(i2 - i4, i3 - i4, i2 + i4, i4 + i3));
        }
        b2.d();
    }

    private m g() {
        Display defaultDisplay = ((WindowManager) this.f1013a.getSystemService("window")).getDefaultDisplay();
        Point point = new Point();
        if (VERSION.SDK_INT >= 17) {
            defaultDisplay.getRealSize(point);
        } else {
            defaultDisplay.getSize(point);
        }
        m tVar = Math.min(point.x, point.y) >= this.f1013a.getResources().getDimensionPixelSize(d.abc_cascading_menus_min_smallest_width) ? new e(this.f1013a, this.f, this.d, this.e, this.c) : new t(this.f1013a, this.f1014b, this.f, this.d, this.e, this.c);
        tVar.a(this.f1014b);
        tVar.a(this.l);
        tVar.a(this.f);
        tVar.a(this.i);
        tVar.b(this.h);
        tVar.a(this.g);
        return tVar;
    }

    public void a() {
        if (!c()) {
            throw new IllegalStateException("MenuPopupHelper cannot be used without an anchor");
        }
    }

    public void a(int i2) {
        this.g = i2;
    }

    public void a(a aVar) {
        this.i = aVar;
        if (this.j != null) {
            this.j.a(aVar);
        }
    }

    public void a(View view) {
        this.f = view;
    }

    public void a(OnDismissListener onDismissListener) {
        this.k = onDismissListener;
    }

    public void a(boolean z) {
        this.h = z;
        if (this.j != null) {
            this.j.b(z);
        }
    }

    public boolean a(int i2, int i3) {
        if (f()) {
            return true;
        }
        if (this.f == null) {
            return false;
        }
        a(i2, i3, true, true);
        return true;
    }

    public m b() {
        if (this.j == null) {
            this.j = g();
        }
        return this.j;
    }

    public boolean c() {
        if (f()) {
            return true;
        }
        if (this.f == null) {
            return false;
        }
        a(0, 0, false, false);
        return true;
    }

    public void d() {
        if (f()) {
            this.j.e();
        }
    }

    /* access modifiers changed from: protected */
    public void e() {
        this.j = null;
        if (this.k != null) {
            this.k.onDismiss();
        }
    }

    public boolean f() {
        return this.j != null && this.j.f();
    }
}
