package android.support.v7.view.menu;

import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcelable;
import android.support.v7.a.a.g;
import android.util.SparseArray;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import java.util.ArrayList;

public class f implements o, OnItemClickListener {

    /* renamed from: a reason: collision with root package name */
    Context f994a;

    /* renamed from: b reason: collision with root package name */
    LayoutInflater f995b;
    h c;
    ExpandedMenuView d;
    int e;
    int f;
    int g;
    a h;
    private android.support.v7.view.menu.o.a i;
    private int j;

    private class a extends BaseAdapter {

        /* renamed from: b reason: collision with root package name */
        private int f997b = -1;

        public a() {
            a();
        }

        /* renamed from: a */
        public j getItem(int i) {
            ArrayList l = f.this.c.l();
            int i2 = f.this.e + i;
            if (this.f997b >= 0 && i2 >= this.f997b) {
                i2++;
            }
            return (j) l.get(i2);
        }

        /* access modifiers changed from: 0000 */
        public void a() {
            j r = f.this.c.r();
            if (r != null) {
                ArrayList l = f.this.c.l();
                int size = l.size();
                for (int i = 0; i < size; i++) {
                    if (((j) l.get(i)) == r) {
                        this.f997b = i;
                        return;
                    }
                }
            }
            this.f997b = -1;
        }

        public int getCount() {
            int size = f.this.c.l().size() - f.this.e;
            return this.f997b < 0 ? size : size - 1;
        }

        public long getItemId(int i) {
            return (long) i;
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            View view2 = view == null ? f.this.f995b.inflate(f.this.g, viewGroup, false) : view;
            ((android.support.v7.view.menu.p.a) view2).a(getItem(i), 0);
            return view2;
        }

        public void notifyDataSetChanged() {
            a();
            super.notifyDataSetChanged();
        }
    }

    public f(int i2, int i3) {
        this.g = i2;
        this.f = i3;
    }

    public f(Context context, int i2) {
        this(i2, 0);
        this.f994a = context;
        this.f995b = LayoutInflater.from(this.f994a);
    }

    public p a(ViewGroup viewGroup) {
        if (this.d == null) {
            this.d = (ExpandedMenuView) this.f995b.inflate(g.abc_expanded_menu_layout, viewGroup, false);
            if (this.h == null) {
                this.h = new a();
            }
            this.d.setAdapter(this.h);
            this.d.setOnItemClickListener(this);
        }
        return this.d;
    }

    public void a(Context context, h hVar) {
        if (this.f != 0) {
            this.f994a = new ContextThemeWrapper(context, this.f);
            this.f995b = LayoutInflater.from(this.f994a);
        } else if (this.f994a != null) {
            this.f994a = context;
            if (this.f995b == null) {
                this.f995b = LayoutInflater.from(this.f994a);
            }
        }
        this.c = hVar;
        if (this.h != null) {
            this.h.notifyDataSetChanged();
        }
    }

    public void a(Bundle bundle) {
        SparseArray sparseArray = new SparseArray();
        if (this.d != null) {
            this.d.saveHierarchyState(sparseArray);
        }
        bundle.putSparseParcelableArray("android:menu:list", sparseArray);
    }

    public void a(Parcelable parcelable) {
        b((Bundle) parcelable);
    }

    public void a(h hVar, boolean z) {
        if (this.i != null) {
            this.i.a(hVar, z);
        }
    }

    public void a(android.support.v7.view.menu.o.a aVar) {
        this.i = aVar;
    }

    public void a(boolean z) {
        if (this.h != null) {
            this.h.notifyDataSetChanged();
        }
    }

    public boolean a() {
        return false;
    }

    public boolean a(h hVar, j jVar) {
        return false;
    }

    public boolean a(u uVar) {
        if (!uVar.hasVisibleItems()) {
            return false;
        }
        new i(uVar).a((IBinder) null);
        if (this.i != null) {
            this.i.a(uVar);
        }
        return true;
    }

    public int b() {
        return this.j;
    }

    public void b(Bundle bundle) {
        SparseArray sparseParcelableArray = bundle.getSparseParcelableArray("android:menu:list");
        if (sparseParcelableArray != null) {
            this.d.restoreHierarchyState(sparseParcelableArray);
        }
    }

    public boolean b(h hVar, j jVar) {
        return false;
    }

    public Parcelable c() {
        if (this.d == null) {
            return null;
        }
        Bundle bundle = new Bundle();
        a(bundle);
        return bundle;
    }

    public ListAdapter d() {
        if (this.h == null) {
            this.h = new a();
        }
        return this.h;
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int i2, long j2) {
        this.c.a((MenuItem) this.h.getItem(i2), (o) this, 0);
    }
}
