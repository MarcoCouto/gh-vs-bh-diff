package android.support.v7.view.menu;

import android.content.Context;
import android.support.v4.i.c.b;
import android.view.ActionProvider;
import android.view.ActionProvider.VisibilityListener;
import android.view.MenuItem;
import android.view.View;

class l extends k {

    class a extends a implements VisibilityListener {
        b c;

        public a(Context context, ActionProvider actionProvider) {
            super(context, actionProvider);
        }

        public View a(MenuItem menuItem) {
            return this.f1007a.onCreateActionView(menuItem);
        }

        public void a(b bVar) {
            this.c = bVar;
            ActionProvider actionProvider = this.f1007a;
            if (bVar == null) {
                this = null;
            }
            actionProvider.setVisibilityListener(this);
        }

        public boolean b() {
            return this.f1007a.overridesItemVisibility();
        }

        public boolean c() {
            return this.f1007a.isVisible();
        }

        public void onActionProviderVisibilityChanged(boolean z) {
            if (this.c != null) {
                this.c.a(z);
            }
        }
    }

    l(Context context, android.support.v4.d.a.b bVar) {
        super(context, bVar);
    }

    /* access modifiers changed from: 0000 */
    public a a(ActionProvider actionProvider) {
        return new a(this.f983a, actionProvider);
    }
}
