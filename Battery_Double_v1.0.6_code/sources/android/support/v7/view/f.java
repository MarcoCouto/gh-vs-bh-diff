package android.support.v7.view;

import android.content.Context;
import android.support.v4.d.a.b;
import android.support.v4.h.m;
import android.support.v7.view.menu.q;
import android.view.ActionMode;
import android.view.ActionMode.Callback;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import java.util.ArrayList;

public class f extends ActionMode {

    /* renamed from: a reason: collision with root package name */
    final Context f959a;

    /* renamed from: b reason: collision with root package name */
    final b f960b;

    public static class a implements android.support.v7.view.b.a {

        /* renamed from: a reason: collision with root package name */
        final Callback f961a;

        /* renamed from: b reason: collision with root package name */
        final Context f962b;
        final ArrayList<f> c = new ArrayList<>();
        final m<Menu, Menu> d = new m<>();

        public a(Context context, Callback callback) {
            this.f962b = context;
            this.f961a = callback;
        }

        private Menu a(Menu menu) {
            Menu menu2 = (Menu) this.d.get(menu);
            if (menu2 != null) {
                return menu2;
            }
            Menu a2 = q.a(this.f962b, (android.support.v4.d.a.a) menu);
            this.d.put(menu, a2);
            return a2;
        }

        public void a(b bVar) {
            this.f961a.onDestroyActionMode(b(bVar));
        }

        public boolean a(b bVar, Menu menu) {
            return this.f961a.onCreateActionMode(b(bVar), a(menu));
        }

        public boolean a(b bVar, MenuItem menuItem) {
            return this.f961a.onActionItemClicked(b(bVar), q.a(this.f962b, (b) menuItem));
        }

        public ActionMode b(b bVar) {
            int size = this.c.size();
            for (int i = 0; i < size; i++) {
                f fVar = (f) this.c.get(i);
                if (fVar != null && fVar.f960b == bVar) {
                    return fVar;
                }
            }
            f fVar2 = new f(this.f962b, bVar);
            this.c.add(fVar2);
            return fVar2;
        }

        public boolean b(b bVar, Menu menu) {
            return this.f961a.onPrepareActionMode(b(bVar), a(menu));
        }
    }

    public f(Context context, b bVar) {
        this.f959a = context;
        this.f960b = bVar;
    }

    public void finish() {
        this.f960b.c();
    }

    public View getCustomView() {
        return this.f960b.i();
    }

    public Menu getMenu() {
        return q.a(this.f959a, (android.support.v4.d.a.a) this.f960b.b());
    }

    public MenuInflater getMenuInflater() {
        return this.f960b.a();
    }

    public CharSequence getSubtitle() {
        return this.f960b.g();
    }

    public Object getTag() {
        return this.f960b.j();
    }

    public CharSequence getTitle() {
        return this.f960b.f();
    }

    public boolean getTitleOptionalHint() {
        return this.f960b.k();
    }

    public void invalidate() {
        this.f960b.d();
    }

    public boolean isTitleOptional() {
        return this.f960b.h();
    }

    public void setCustomView(View view) {
        this.f960b.a(view);
    }

    public void setSubtitle(int i) {
        this.f960b.b(i);
    }

    public void setSubtitle(CharSequence charSequence) {
        this.f960b.a(charSequence);
    }

    public void setTag(Object obj) {
        this.f960b.a(obj);
    }

    public void setTitle(int i) {
        this.f960b.a(i);
    }

    public void setTitle(CharSequence charSequence) {
        this.f960b.b(charSequence);
    }

    public void setTitleOptionalHint(boolean z) {
        this.f960b.a(z);
    }
}
