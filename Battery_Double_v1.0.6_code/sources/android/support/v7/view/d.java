package android.support.v7.view;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.AssetManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.os.Build.VERSION;
import android.support.v7.a.a.i;
import android.view.LayoutInflater;

public class d extends ContextWrapper {

    /* renamed from: a reason: collision with root package name */
    private int f955a;

    /* renamed from: b reason: collision with root package name */
    private Theme f956b;
    private LayoutInflater c;
    private Configuration d;
    private Resources e;

    public d() {
        super(null);
    }

    public d(Context context, int i) {
        super(context);
        this.f955a = i;
    }

    public d(Context context, Theme theme) {
        super(context);
        this.f956b = theme;
    }

    private Resources b() {
        if (this.e == null) {
            if (this.d == null) {
                this.e = super.getResources();
            } else if (VERSION.SDK_INT >= 17) {
                this.e = createConfigurationContext(this.d).getResources();
            }
        }
        return this.e;
    }

    private void c() {
        boolean z = this.f956b == null;
        if (z) {
            this.f956b = getResources().newTheme();
            Theme theme = getBaseContext().getTheme();
            if (theme != null) {
                this.f956b.setTo(theme);
            }
        }
        a(this.f956b, this.f955a, z);
    }

    public int a() {
        return this.f955a;
    }

    /* access modifiers changed from: protected */
    public void a(Theme theme, int i, boolean z) {
        theme.applyStyle(i, true);
    }

    /* access modifiers changed from: protected */
    public void attachBaseContext(Context context) {
        super.attachBaseContext(context);
    }

    public AssetManager getAssets() {
        return getResources().getAssets();
    }

    public Resources getResources() {
        return b();
    }

    public Object getSystemService(String str) {
        if (!"layout_inflater".equals(str)) {
            return getBaseContext().getSystemService(str);
        }
        if (this.c == null) {
            this.c = LayoutInflater.from(getBaseContext()).cloneInContext(this);
        }
        return this.c;
    }

    public Theme getTheme() {
        if (this.f956b != null) {
            return this.f956b;
        }
        if (this.f955a == 0) {
            this.f955a = i.Theme_AppCompat_Light;
        }
        c();
        return this.f956b;
    }

    public void setTheme(int i) {
        if (this.f955a != i) {
            this.f955a = i;
            c();
        }
    }
}
