package android.support.v7.view;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.graphics.PorterDuff.Mode;
import android.support.v4.i.c;
import android.support.v4.i.h;
import android.support.v7.view.menu.j;
import android.support.v7.view.menu.k;
import android.support.v7.widget.am;
import android.util.AttributeSet;
import android.util.Log;
import android.util.Xml;
import android.view.InflateException;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.SubMenu;
import android.view.View;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class g extends MenuInflater {

    /* renamed from: a reason: collision with root package name */
    static final Class<?>[] f963a = {Context.class};

    /* renamed from: b reason: collision with root package name */
    static final Class<?>[] f964b = f963a;
    final Object[] c;
    final Object[] d = this.c;
    Context e;
    private Object f;

    private static class a implements OnMenuItemClickListener {

        /* renamed from: a reason: collision with root package name */
        private static final Class<?>[] f965a = {MenuItem.class};

        /* renamed from: b reason: collision with root package name */
        private Object f966b;
        private Method c;

        public a(Object obj, String str) {
            this.f966b = obj;
            Class cls = obj.getClass();
            try {
                this.c = cls.getMethod(str, f965a);
            } catch (Exception e) {
                InflateException inflateException = new InflateException("Couldn't resolve menu item onClick handler " + str + " in class " + cls.getName());
                inflateException.initCause(e);
                throw inflateException;
            }
        }

        public boolean onMenuItemClick(MenuItem menuItem) {
            try {
                if (this.c.getReturnType() == Boolean.TYPE) {
                    return ((Boolean) this.c.invoke(this.f966b, new Object[]{menuItem})).booleanValue();
                }
                this.c.invoke(this.f966b, new Object[]{menuItem});
                return true;
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    private class b {
        private String A;
        private String B;
        private CharSequence C;
        private CharSequence D;
        private ColorStateList E = null;
        private Mode F = null;

        /* renamed from: a reason: collision with root package name */
        c f967a;
        private Menu c;
        private int d;
        private int e;
        private int f;
        private int g;
        private boolean h;
        private boolean i;
        private boolean j;
        private int k;
        private int l;
        private CharSequence m;
        private CharSequence n;
        private int o;
        private char p;
        private int q;
        private char r;
        private int s;
        private int t;
        private boolean u;
        private boolean v;
        private boolean w;
        private int x;
        private int y;
        private String z;

        public b(Menu menu) {
            this.c = menu;
            a();
        }

        private char a(String str) {
            if (str == null) {
                return 0;
            }
            return str.charAt(0);
        }

        private <T> T a(String str, Class<?>[] clsArr, Object[] objArr) {
            try {
                Constructor constructor = g.this.e.getClassLoader().loadClass(str).getConstructor(clsArr);
                constructor.setAccessible(true);
                return constructor.newInstance(objArr);
            } catch (Exception e2) {
                Log.w("SupportMenuInflater", "Cannot instantiate class: " + str, e2);
                return null;
            }
        }

        private void a(MenuItem menuItem) {
            boolean z2 = true;
            menuItem.setChecked(this.u).setVisible(this.v).setEnabled(this.w).setCheckable(this.t >= 1).setTitleCondensed(this.n).setIcon(this.o);
            if (this.x >= 0) {
                menuItem.setShowAsAction(this.x);
            }
            if (this.B != null) {
                if (g.this.e.isRestricted()) {
                    throw new IllegalStateException("The android:onClick attribute cannot be used within a restricted context");
                }
                menuItem.setOnMenuItemClickListener(new a(g.this.a(), this.B));
            }
            if (menuItem instanceof j) {
                j jVar = (j) menuItem;
            }
            if (this.t >= 2) {
                if (menuItem instanceof j) {
                    ((j) menuItem).a(true);
                } else if (menuItem instanceof k) {
                    ((k) menuItem).a(true);
                }
            }
            if (this.z != null) {
                menuItem.setActionView((View) a(this.z, g.f963a, g.this.c));
            } else {
                z2 = false;
            }
            if (this.y > 0) {
                if (!z2) {
                    menuItem.setActionView(this.y);
                } else {
                    Log.w("SupportMenuInflater", "Ignoring attribute 'itemActionViewLayout'. Action view already specified.");
                }
            }
            if (this.f967a != null) {
                h.a(menuItem, this.f967a);
            }
            h.a(menuItem, this.C);
            h.b(menuItem, this.D);
            h.b(menuItem, this.p, this.q);
            h.a(menuItem, this.r, this.s);
            if (this.F != null) {
                h.a(menuItem, this.F);
            }
            if (this.E != null) {
                h.a(menuItem, this.E);
            }
        }

        public void a() {
            this.d = 0;
            this.e = 0;
            this.f = 0;
            this.g = 0;
            this.h = true;
            this.i = true;
        }

        public void a(AttributeSet attributeSet) {
            TypedArray obtainStyledAttributes = g.this.e.obtainStyledAttributes(attributeSet, android.support.v7.a.a.j.MenuGroup);
            this.d = obtainStyledAttributes.getResourceId(android.support.v7.a.a.j.MenuGroup_android_id, 0);
            this.e = obtainStyledAttributes.getInt(android.support.v7.a.a.j.MenuGroup_android_menuCategory, 0);
            this.f = obtainStyledAttributes.getInt(android.support.v7.a.a.j.MenuGroup_android_orderInCategory, 0);
            this.g = obtainStyledAttributes.getInt(android.support.v7.a.a.j.MenuGroup_android_checkableBehavior, 0);
            this.h = obtainStyledAttributes.getBoolean(android.support.v7.a.a.j.MenuGroup_android_visible, true);
            this.i = obtainStyledAttributes.getBoolean(android.support.v7.a.a.j.MenuGroup_android_enabled, true);
            obtainStyledAttributes.recycle();
        }

        public void b() {
            this.j = true;
            a(this.c.add(this.d, this.k, this.l, this.m));
        }

        public void b(AttributeSet attributeSet) {
            boolean z2 = true;
            TypedArray obtainStyledAttributes = g.this.e.obtainStyledAttributes(attributeSet, android.support.v7.a.a.j.MenuItem);
            this.k = obtainStyledAttributes.getResourceId(android.support.v7.a.a.j.MenuItem_android_id, 0);
            this.l = (obtainStyledAttributes.getInt(android.support.v7.a.a.j.MenuItem_android_menuCategory, this.e) & -65536) | (obtainStyledAttributes.getInt(android.support.v7.a.a.j.MenuItem_android_orderInCategory, this.f) & 65535);
            this.m = obtainStyledAttributes.getText(android.support.v7.a.a.j.MenuItem_android_title);
            this.n = obtainStyledAttributes.getText(android.support.v7.a.a.j.MenuItem_android_titleCondensed);
            this.o = obtainStyledAttributes.getResourceId(android.support.v7.a.a.j.MenuItem_android_icon, 0);
            this.p = a(obtainStyledAttributes.getString(android.support.v7.a.a.j.MenuItem_android_alphabeticShortcut));
            this.q = obtainStyledAttributes.getInt(android.support.v7.a.a.j.MenuItem_alphabeticModifiers, 4096);
            this.r = a(obtainStyledAttributes.getString(android.support.v7.a.a.j.MenuItem_android_numericShortcut));
            this.s = obtainStyledAttributes.getInt(android.support.v7.a.a.j.MenuItem_numericModifiers, 4096);
            if (obtainStyledAttributes.hasValue(android.support.v7.a.a.j.MenuItem_android_checkable)) {
                this.t = obtainStyledAttributes.getBoolean(android.support.v7.a.a.j.MenuItem_android_checkable, false) ? 1 : 0;
            } else {
                this.t = this.g;
            }
            this.u = obtainStyledAttributes.getBoolean(android.support.v7.a.a.j.MenuItem_android_checked, false);
            this.v = obtainStyledAttributes.getBoolean(android.support.v7.a.a.j.MenuItem_android_visible, this.h);
            this.w = obtainStyledAttributes.getBoolean(android.support.v7.a.a.j.MenuItem_android_enabled, this.i);
            this.x = obtainStyledAttributes.getInt(android.support.v7.a.a.j.MenuItem_showAsAction, -1);
            this.B = obtainStyledAttributes.getString(android.support.v7.a.a.j.MenuItem_android_onClick);
            this.y = obtainStyledAttributes.getResourceId(android.support.v7.a.a.j.MenuItem_actionLayout, 0);
            this.z = obtainStyledAttributes.getString(android.support.v7.a.a.j.MenuItem_actionViewClass);
            this.A = obtainStyledAttributes.getString(android.support.v7.a.a.j.MenuItem_actionProviderClass);
            if (this.A == null) {
                z2 = false;
            }
            if (z2 && this.y == 0 && this.z == null) {
                this.f967a = (c) a(this.A, g.f964b, g.this.d);
            } else {
                if (z2) {
                    Log.w("SupportMenuInflater", "Ignoring attribute 'actionProviderClass'. Action view already specified.");
                }
                this.f967a = null;
            }
            this.C = obtainStyledAttributes.getText(android.support.v7.a.a.j.MenuItem_contentDescription);
            this.D = obtainStyledAttributes.getText(android.support.v7.a.a.j.MenuItem_tooltipText);
            if (obtainStyledAttributes.hasValue(android.support.v7.a.a.j.MenuItem_iconTintMode)) {
                this.F = am.a(obtainStyledAttributes.getInt(android.support.v7.a.a.j.MenuItem_iconTintMode, -1), this.F);
            } else {
                this.F = null;
            }
            if (obtainStyledAttributes.hasValue(android.support.v7.a.a.j.MenuItem_iconTint)) {
                this.E = obtainStyledAttributes.getColorStateList(android.support.v7.a.a.j.MenuItem_iconTint);
            } else {
                this.E = null;
            }
            obtainStyledAttributes.recycle();
            this.j = false;
        }

        public SubMenu c() {
            this.j = true;
            SubMenu addSubMenu = this.c.addSubMenu(this.d, this.k, this.l, this.m);
            a(addSubMenu.getItem());
            return addSubMenu;
        }

        public boolean d() {
            return this.j;
        }
    }

    public g(Context context) {
        super(context);
        this.e = context;
        this.c = new Object[]{context};
    }

    private Object a(Object obj) {
        return (!(obj instanceof Activity) && (obj instanceof ContextWrapper)) ? a(((ContextWrapper) obj).getBaseContext()) : obj;
    }

    private void a(XmlPullParser xmlPullParser, AttributeSet attributeSet, Menu menu) throws XmlPullParserException, IOException {
        boolean z;
        b bVar = new b(menu);
        int eventType = xmlPullParser.getEventType();
        while (true) {
            if (eventType != 2) {
                eventType = xmlPullParser.next();
                if (eventType == 1) {
                    break;
                }
            } else {
                String name = xmlPullParser.getName();
                if (name.equals("menu")) {
                    eventType = xmlPullParser.next();
                } else {
                    throw new RuntimeException("Expecting menu, got " + name);
                }
            }
        }
        String str = null;
        boolean z2 = false;
        int i = eventType;
        boolean z3 = false;
        while (!z3) {
            switch (i) {
                case 1:
                    throw new RuntimeException("Unexpected end of document");
                case 2:
                    if (!z2) {
                        String name2 = xmlPullParser.getName();
                        if (!name2.equals("group")) {
                            if (!name2.equals("item")) {
                                if (!name2.equals("menu")) {
                                    str = name2;
                                    z = true;
                                    break;
                                } else {
                                    a(xmlPullParser, attributeSet, bVar.c());
                                    z = z2;
                                    break;
                                }
                            } else {
                                bVar.b(attributeSet);
                                z = z2;
                                break;
                            }
                        } else {
                            bVar.a(attributeSet);
                            z = z2;
                            break;
                        }
                    } else {
                        z = z2;
                        break;
                    }
                case 3:
                    String name3 = xmlPullParser.getName();
                    if (!z2 || !name3.equals(str)) {
                        if (!name3.equals("group")) {
                            if (!name3.equals("item")) {
                                if (name3.equals("menu")) {
                                    z3 = true;
                                    z = z2;
                                    break;
                                }
                            } else if (!bVar.d()) {
                                if (bVar.f967a != null && bVar.f967a.e()) {
                                    bVar.c();
                                    z = z2;
                                    break;
                                } else {
                                    bVar.b();
                                    z = z2;
                                    break;
                                }
                            }
                        } else {
                            bVar.a();
                            z = z2;
                            break;
                        }
                    } else {
                        str = null;
                        z = false;
                        break;
                    }
                    break;
                default:
                    z = z2;
                    break;
            }
            boolean z4 = z;
            i = xmlPullParser.next();
            z2 = z4;
        }
    }

    /* access modifiers changed from: 0000 */
    public Object a() {
        if (this.f == null) {
            this.f = a(this.e);
        }
        return this.f;
    }

    public void inflate(int i, Menu menu) {
        if (!(menu instanceof android.support.v4.d.a.a)) {
            super.inflate(i, menu);
            return;
        }
        XmlResourceParser xmlResourceParser = null;
        try {
            xmlResourceParser = this.e.getResources().getLayout(i);
            a(xmlResourceParser, Xml.asAttributeSet(xmlResourceParser), menu);
            if (xmlResourceParser != null) {
                xmlResourceParser.close();
            }
        } catch (XmlPullParserException e2) {
            throw new InflateException("Error inflating menu XML", e2);
        } catch (IOException e3) {
            throw new InflateException("Error inflating menu XML", e3);
        } catch (Throwable th) {
            if (xmlResourceParser != null) {
                xmlResourceParser.close();
            }
            throw th;
        }
    }
}
