package android.support.v7.view;

import android.content.Context;
import android.support.v7.view.menu.h;
import android.support.v7.view.menu.h.a;
import android.support.v7.widget.ActionBarContextView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import java.lang.ref.WeakReference;

public class e extends b implements a {

    /* renamed from: a reason: collision with root package name */
    private Context f957a;

    /* renamed from: b reason: collision with root package name */
    private ActionBarContextView f958b;
    private b.a c;
    private WeakReference<View> d;
    private boolean e;
    private boolean f;
    private h g;

    public e(Context context, ActionBarContextView actionBarContextView, b.a aVar, boolean z) {
        this.f957a = context;
        this.f958b = actionBarContextView;
        this.c = aVar;
        this.g = new h(actionBarContextView.getContext()).a(1);
        this.g.a((a) this);
        this.f = z;
    }

    public MenuInflater a() {
        return new g(this.f958b.getContext());
    }

    public void a(int i) {
        b((CharSequence) this.f957a.getString(i));
    }

    public void a(h hVar) {
        d();
        this.f958b.a();
    }

    public void a(View view) {
        this.f958b.setCustomView(view);
        this.d = view != null ? new WeakReference<>(view) : null;
    }

    public void a(CharSequence charSequence) {
        this.f958b.setSubtitle(charSequence);
    }

    public void a(boolean z) {
        super.a(z);
        this.f958b.setTitleOptional(z);
    }

    public boolean a(h hVar, MenuItem menuItem) {
        return this.c.a((b) this, menuItem);
    }

    public Menu b() {
        return this.g;
    }

    public void b(int i) {
        a((CharSequence) this.f957a.getString(i));
    }

    public void b(CharSequence charSequence) {
        this.f958b.setTitle(charSequence);
    }

    public void c() {
        if (!this.e) {
            this.e = true;
            this.f958b.sendAccessibilityEvent(32);
            this.c.a(this);
        }
    }

    public void d() {
        this.c.b(this, this.g);
    }

    public CharSequence f() {
        return this.f958b.getTitle();
    }

    public CharSequence g() {
        return this.f958b.getSubtitle();
    }

    public boolean h() {
        return this.f958b.d();
    }

    public View i() {
        if (this.d != null) {
            return (View) this.d.get();
        }
        return null;
    }
}
