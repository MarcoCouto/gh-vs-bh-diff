package android.support.v7.e;

import com.mansoon.BatteryDouble.R;

public final class a {

    /* renamed from: android.support.v7.e.a$a reason: collision with other inner class name */
    public static final class C0030a {
        public static final int compat_button_inset_horizontal_material = 2131165266;
        public static final int compat_button_inset_vertical_material = 2131165267;
        public static final int compat_button_padding_horizontal_material = 2131165268;
        public static final int compat_button_padding_vertical_material = 2131165269;
        public static final int compat_control_corner_material = 2131165270;
        public static final int fastscroll_default_thickness = 2131165312;
        public static final int fastscroll_margin = 2131165313;
        public static final int fastscroll_minimum_range = 2131165314;
        public static final int item_touch_helper_max_drag_scroll_per_frame = 2131165323;
        public static final int item_touch_helper_swipe_escape_max_velocity = 2131165324;
        public static final int item_touch_helper_swipe_escape_velocity = 2131165325;
        public static final int notification_action_icon_size = 2131165332;
        public static final int notification_action_text_size = 2131165333;
        public static final int notification_big_circle_margin = 2131165334;
        public static final int notification_content_margin_start = 2131165335;
        public static final int notification_large_icon_height = 2131165336;
        public static final int notification_large_icon_width = 2131165337;
        public static final int notification_main_column_padding_top = 2131165338;
        public static final int notification_media_narrow_margin = 2131165339;
        public static final int notification_right_icon_size = 2131165340;
        public static final int notification_right_side_padding_top = 2131165341;
        public static final int notification_small_icon_background_padding = 2131165342;
        public static final int notification_small_icon_size_as_large = 2131165343;
        public static final int notification_subtext_size = 2131165344;
        public static final int notification_top_pad = 2131165345;
        public static final int notification_top_pad_large_text = 2131165346;
    }

    public static final class b {
        public static final int action_container = 2131296273;
        public static final int action_divider = 2131296275;
        public static final int action_image = 2131296276;
        public static final int action_text = 2131296287;
        public static final int actions = 2131296288;
        public static final int async = 2131296312;
        public static final int blocking = 2131296328;
        public static final int chronometer = 2131296348;
        public static final int forever = 2131296386;
        public static final int icon = 2131296392;
        public static final int icon_group = 2131296393;
        public static final int info = 2131296402;
        public static final int italic = 2131296406;
        public static final int item_touch_helper_previous_elevation = 2131296408;
        public static final int line1 = 2131296415;
        public static final int line3 = 2131296416;
        public static final int normal = 2131296449;
        public static final int notification_background = 2131296450;
        public static final int notification_main_column = 2131296451;
        public static final int notification_main_column_container = 2131296452;
        public static final int right_icon = 2131296465;
        public static final int right_side = 2131296466;
        public static final int tag_transition_group = 2131296519;
        public static final int text = 2131296529;
        public static final int text2 = 2131296530;
        public static final int time = 2131296536;
        public static final int title = 2131296537;
    }

    public static final class c {
        public static final int[] FontFamily = {R.attr.fontProviderAuthority, R.attr.fontProviderCerts, R.attr.fontProviderFetchStrategy, R.attr.fontProviderFetchTimeout, R.attr.fontProviderPackage, R.attr.fontProviderQuery};
        public static final int[] FontFamilyFont = {16844082, 16844083, 16844095, R.attr.font, R.attr.fontStyle, R.attr.fontWeight};
        public static final int FontFamilyFont_android_font = 0;
        public static final int FontFamilyFont_android_fontStyle = 2;
        public static final int FontFamilyFont_android_fontWeight = 1;
        public static final int FontFamilyFont_font = 3;
        public static final int FontFamilyFont_fontStyle = 4;
        public static final int FontFamilyFont_fontWeight = 5;
        public static final int FontFamily_fontProviderAuthority = 0;
        public static final int FontFamily_fontProviderCerts = 1;
        public static final int FontFamily_fontProviderFetchStrategy = 2;
        public static final int FontFamily_fontProviderFetchTimeout = 3;
        public static final int FontFamily_fontProviderPackage = 4;
        public static final int FontFamily_fontProviderQuery = 5;
        public static final int[] RecyclerView = {16842948, 16842993, R.attr.fastScrollEnabled, R.attr.fastScrollHorizontalThumbDrawable, R.attr.fastScrollHorizontalTrackDrawable, R.attr.fastScrollVerticalThumbDrawable, R.attr.fastScrollVerticalTrackDrawable, R.attr.layoutManager, R.attr.reverseLayout, R.attr.spanCount, R.attr.stackFromEnd};
        public static final int RecyclerView_android_descendantFocusability = 1;
        public static final int RecyclerView_android_orientation = 0;
        public static final int RecyclerView_fastScrollEnabled = 2;
        public static final int RecyclerView_fastScrollHorizontalThumbDrawable = 3;
        public static final int RecyclerView_fastScrollHorizontalTrackDrawable = 4;
        public static final int RecyclerView_fastScrollVerticalThumbDrawable = 5;
        public static final int RecyclerView_fastScrollVerticalTrackDrawable = 6;
        public static final int RecyclerView_layoutManager = 7;
        public static final int RecyclerView_reverseLayout = 8;
        public static final int RecyclerView_spanCount = 9;
        public static final int RecyclerView_stackFromEnd = 10;
    }
}
