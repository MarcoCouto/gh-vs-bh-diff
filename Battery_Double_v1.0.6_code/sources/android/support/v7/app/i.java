package android.support.v7.app;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.ActionMode;
import android.view.Window;
import android.view.Window.Callback;

class i extends h {
    private int t = -100;
    private boolean u;
    private boolean v = true;
    private b w;

    class a extends a {
        a(Callback callback) {
            super(callback);
        }

        /* access modifiers changed from: 0000 */
        public final ActionMode a(ActionMode.Callback callback) {
            android.support.v7.view.f.a aVar = new android.support.v7.view.f.a(i.this.f895a, callback);
            android.support.v7.view.b b2 = i.this.b((android.support.v7.view.b.a) aVar);
            if (b2 != null) {
                return aVar.b(b2);
            }
            return null;
        }

        public ActionMode onWindowStartingActionMode(ActionMode.Callback callback) {
            return i.this.o() ? a(callback) : super.onWindowStartingActionMode(callback);
        }
    }

    final class b {

        /* renamed from: b reason: collision with root package name */
        private q f901b;
        private boolean c;
        private BroadcastReceiver d;
        private IntentFilter e;

        b(q qVar) {
            this.f901b = qVar;
            this.c = qVar.a();
        }

        /* access modifiers changed from: 0000 */
        public final int a() {
            this.c = this.f901b.a();
            return this.c ? 2 : 1;
        }

        /* access modifiers changed from: 0000 */
        public final void b() {
            boolean a2 = this.f901b.a();
            if (a2 != this.c) {
                this.c = a2;
                i.this.i();
            }
        }

        /* access modifiers changed from: 0000 */
        public final void c() {
            d();
            if (this.d == null) {
                this.d = new BroadcastReceiver() {
                    public void onReceive(Context context, Intent intent) {
                        b.this.b();
                    }
                };
            }
            if (this.e == null) {
                this.e = new IntentFilter();
                this.e.addAction("android.intent.action.TIME_SET");
                this.e.addAction("android.intent.action.TIMEZONE_CHANGED");
                this.e.addAction("android.intent.action.TIME_TICK");
            }
            i.this.f895a.registerReceiver(this.d, this.e);
        }

        /* access modifiers changed from: 0000 */
        public final void d() {
            if (this.d != null) {
                i.this.f895a.unregisterReceiver(this.d);
                this.d = null;
            }
        }
    }

    i(Context context, Window window, d dVar) {
        super(context, window, dVar);
    }

    private boolean h(int i) {
        Resources resources = this.f895a.getResources();
        Configuration configuration = resources.getConfiguration();
        int i2 = configuration.uiMode & 48;
        int i3 = i == 2 ? 32 : 16;
        if (i2 == i3) {
            return false;
        }
        if (y()) {
            ((Activity) this.f895a).recreate();
        } else {
            Configuration configuration2 = new Configuration(configuration);
            DisplayMetrics displayMetrics = resources.getDisplayMetrics();
            configuration2.uiMode = i3 | (configuration2.uiMode & -49);
            resources.updateConfiguration(configuration2, displayMetrics);
            if (VERSION.SDK_INT < 26) {
                n.a(resources);
            }
        }
        return true;
    }

    private int w() {
        return this.t != -100 ? this.t : j();
    }

    private void x() {
        if (this.w == null) {
            this.w = new b(q.a(this.f895a));
        }
    }

    private boolean y() {
        if (!this.u || !(this.f895a instanceof Activity)) {
            return false;
        }
        try {
            return (this.f895a.getPackageManager().getActivityInfo(new ComponentName(this.f895a, this.f895a.getClass()), 0).configChanges & 512) == 0;
        } catch (NameNotFoundException e) {
            Log.d("AppCompatDelegate", "Exception while getting ActivityInfo", e);
            return true;
        }
    }

    /* access modifiers changed from: 0000 */
    public Callback a(Callback callback) {
        return new a(callback);
    }

    public void a(Bundle bundle) {
        super.a(bundle);
        if (bundle != null && this.t == -100) {
            this.t = bundle.getInt("appcompat:local_night_mode", -100);
        }
    }

    public void c() {
        super.c();
        i();
    }

    public void c(Bundle bundle) {
        super.c(bundle);
        if (this.t != -100) {
            bundle.putInt("appcompat:local_night_mode", this.t);
        }
    }

    /* access modifiers changed from: 0000 */
    public int d(int i) {
        switch (i) {
            case -100:
                return -1;
            case 0:
                x();
                return this.w.a();
            default:
                return i;
        }
    }

    public void d() {
        super.d();
        if (this.w != null) {
            this.w.d();
        }
    }

    public void g() {
        super.g();
        if (this.w != null) {
            this.w.d();
        }
    }

    public boolean i() {
        boolean z = false;
        int w2 = w();
        int d = d(w2);
        if (d != -1) {
            z = h(d);
        }
        if (w2 == 0) {
            x();
            this.w.c();
        }
        this.u = true;
        return z;
    }

    public boolean o() {
        return this.v;
    }
}
