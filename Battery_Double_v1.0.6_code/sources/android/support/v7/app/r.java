package android.support.v7.app;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.support.v4.i.aa;
import android.support.v4.i.ab;
import android.support.v4.i.t;
import android.support.v4.i.y;
import android.support.v4.i.z;
import android.support.v7.a.a.C0026a;
import android.support.v7.a.a.f;
import android.support.v7.a.a.j;
import android.support.v7.app.a.b;
import android.support.v7.view.g;
import android.support.v7.view.h;
import android.support.v7.widget.ActionBarContainer;
import android.support.v7.widget.ActionBarContextView;
import android.support.v7.widget.ActionBarOverlayLayout;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.ak;
import android.support.v7.widget.bg;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

public class r extends a implements android.support.v7.widget.ActionBarOverlayLayout.a {
    static final /* synthetic */ boolean s = (!r.class.desiredAssertionStatus());
    private static final Interpolator t = new AccelerateInterpolator();
    private static final Interpolator u = new DecelerateInterpolator();
    private boolean A;
    private boolean B;
    private ArrayList<b> C = new ArrayList<>();
    private boolean D;
    private int E = 0;
    private boolean F;
    private boolean G = true;
    private boolean H;

    /* renamed from: a reason: collision with root package name */
    Context f939a;

    /* renamed from: b reason: collision with root package name */
    ActionBarOverlayLayout f940b;
    ActionBarContainer c;
    ak d;
    ActionBarContextView e;
    View f;
    bg g;
    a h;
    android.support.v7.view.b i;
    android.support.v7.view.b.a j;
    boolean k = true;
    boolean l;
    boolean m;
    h n;
    boolean o;
    final z p = new aa() {
        public void b(View view) {
            if (r.this.k && r.this.f != null) {
                r.this.f.setTranslationY(0.0f);
                r.this.c.setTranslationY(0.0f);
            }
            r.this.c.setVisibility(8);
            r.this.c.setTransitioning(false);
            r.this.n = null;
            r.this.h();
            if (r.this.f940b != null) {
                t.n(r.this.f940b);
            }
        }
    };
    final z q = new aa() {
        public void b(View view) {
            r.this.n = null;
            r.this.c.requestLayout();
        }
    };
    final ab r = new ab() {
        public void a(View view) {
            ((View) r.this.c.getParent()).invalidate();
        }
    };
    private Context v;
    private Activity w;
    private Dialog x;
    private ArrayList<Object> y = new ArrayList<>();
    private int z = -1;

    public class a extends android.support.v7.view.b implements android.support.v7.view.menu.h.a {

        /* renamed from: b reason: collision with root package name */
        private final Context f945b;
        private final android.support.v7.view.menu.h c;
        private android.support.v7.view.b.a d;
        private WeakReference<View> e;

        public a(Context context, android.support.v7.view.b.a aVar) {
            this.f945b = context;
            this.d = aVar;
            this.c = new android.support.v7.view.menu.h(context).a(1);
            this.c.a((android.support.v7.view.menu.h.a) this);
        }

        public MenuInflater a() {
            return new g(this.f945b);
        }

        public void a(int i) {
            b((CharSequence) r.this.f939a.getResources().getString(i));
        }

        public void a(android.support.v7.view.menu.h hVar) {
            if (this.d != null) {
                d();
                r.this.e.a();
            }
        }

        public void a(View view) {
            r.this.e.setCustomView(view);
            this.e = new WeakReference<>(view);
        }

        public void a(CharSequence charSequence) {
            r.this.e.setSubtitle(charSequence);
        }

        public void a(boolean z) {
            super.a(z);
            r.this.e.setTitleOptional(z);
        }

        public boolean a(android.support.v7.view.menu.h hVar, MenuItem menuItem) {
            if (this.d != null) {
                return this.d.a((android.support.v7.view.b) this, menuItem);
            }
            return false;
        }

        public Menu b() {
            return this.c;
        }

        public void b(int i) {
            a((CharSequence) r.this.f939a.getResources().getString(i));
        }

        public void b(CharSequence charSequence) {
            r.this.e.setTitle(charSequence);
        }

        public void c() {
            if (r.this.h == this) {
                if (!r.a(r.this.l, r.this.m, false)) {
                    r.this.i = this;
                    r.this.j = this.d;
                } else {
                    this.d.a(this);
                }
                this.d = null;
                r.this.j(false);
                r.this.e.b();
                r.this.d.a().sendAccessibilityEvent(32);
                r.this.f940b.setHideOnContentScrollEnabled(r.this.o);
                r.this.h = null;
            }
        }

        public void d() {
            if (r.this.h == this) {
                this.c.g();
                try {
                    this.d.b(this, this.c);
                } finally {
                    this.c.h();
                }
            }
        }

        public boolean e() {
            this.c.g();
            try {
                return this.d.a((android.support.v7.view.b) this, (Menu) this.c);
            } finally {
                this.c.h();
            }
        }

        public CharSequence f() {
            return r.this.e.getTitle();
        }

        public CharSequence g() {
            return r.this.e.getSubtitle();
        }

        public boolean h() {
            return r.this.e.d();
        }

        public View i() {
            if (this.e != null) {
                return (View) this.e.get();
            }
            return null;
        }
    }

    public r(Activity activity, boolean z2) {
        this.w = activity;
        View decorView = activity.getWindow().getDecorView();
        a(decorView);
        if (!z2) {
            this.f = decorView.findViewById(16908290);
        }
    }

    public r(Dialog dialog) {
        this.x = dialog;
        a(dialog.getWindow().getDecorView());
    }

    private void a(View view) {
        this.f940b = (ActionBarOverlayLayout) view.findViewById(f.decor_content_parent);
        if (this.f940b != null) {
            this.f940b.setActionBarVisibilityCallback(this);
        }
        this.d = b(view.findViewById(f.action_bar));
        this.e = (ActionBarContextView) view.findViewById(f.action_context_bar);
        this.c = (ActionBarContainer) view.findViewById(f.action_bar_container);
        if (this.d == null || this.e == null || this.c == null) {
            throw new IllegalStateException(getClass().getSimpleName() + " can only be used " + "with a compatible window decor layout");
        }
        this.f939a = this.d.b();
        boolean z2 = (this.d.o() & 4) != 0;
        if (z2) {
            this.A = true;
        }
        android.support.v7.view.a a2 = android.support.v7.view.a.a(this.f939a);
        b(a2.f() || z2);
        k(a2.d());
        TypedArray obtainStyledAttributes = this.f939a.obtainStyledAttributes(null, j.ActionBar, C0026a.actionBarStyle, 0);
        if (obtainStyledAttributes.getBoolean(j.ActionBar_hideOnContentScroll, false)) {
            c(true);
        }
        int dimensionPixelSize = obtainStyledAttributes.getDimensionPixelSize(j.ActionBar_elevation, 0);
        if (dimensionPixelSize != 0) {
            a((float) dimensionPixelSize);
        }
        obtainStyledAttributes.recycle();
    }

    static boolean a(boolean z2, boolean z3, boolean z4) {
        if (z4) {
            return true;
        }
        return !z2 && !z3;
    }

    private ak b(View view) {
        if (view instanceof ak) {
            return (ak) view;
        }
        if (view instanceof Toolbar) {
            return ((Toolbar) view).getWrapper();
        }
        throw new IllegalStateException(new StringBuilder().append("Can't make a decor toolbar out of ").append(view).toString() != null ? view.getClass().getSimpleName() : "null");
    }

    private void k(boolean z2) {
        boolean z3 = true;
        this.D = z2;
        if (!this.D) {
            this.d.a((bg) null);
            this.c.setTabContainer(this.g);
        } else {
            this.c.setTabContainer(null);
            this.d.a(this.g);
        }
        boolean z4 = i() == 2;
        if (this.g != null) {
            if (z4) {
                this.g.setVisibility(0);
                if (this.f940b != null) {
                    t.n(this.f940b);
                }
            } else {
                this.g.setVisibility(8);
            }
        }
        this.d.a(!this.D && z4);
        ActionBarOverlayLayout actionBarOverlayLayout = this.f940b;
        if (this.D || !z4) {
            z3 = false;
        }
        actionBarOverlayLayout.setHasNonEmbeddedTabs(z3);
    }

    private void l(boolean z2) {
        if (a(this.l, this.m, this.F)) {
            if (!this.G) {
                this.G = true;
                h(z2);
            }
        } else if (this.G) {
            this.G = false;
            i(z2);
        }
    }

    private void n() {
        if (!this.F) {
            this.F = true;
            if (this.f940b != null) {
                this.f940b.setShowingForActionMode(true);
            }
            l(false);
        }
    }

    private void o() {
        if (this.F) {
            this.F = false;
            if (this.f940b != null) {
                this.f940b.setShowingForActionMode(false);
            }
            l(false);
        }
    }

    private boolean p() {
        return t.v(this.c);
    }

    public int a() {
        return this.d.o();
    }

    public android.support.v7.view.b a(android.support.v7.view.b.a aVar) {
        if (this.h != null) {
            this.h.c();
        }
        this.f940b.setHideOnContentScrollEnabled(false);
        this.e.c();
        a aVar2 = new a(this.e.getContext(), aVar);
        if (!aVar2.e()) {
            return null;
        }
        this.h = aVar2;
        aVar2.d();
        this.e.a(aVar2);
        j(true);
        this.e.sendAccessibilityEvent(32);
        return aVar2;
    }

    public void a(float f2) {
        t.a((View) this.c, f2);
    }

    public void a(int i2) {
        this.E = i2;
    }

    public void a(int i2, int i3) {
        int o2 = this.d.o();
        if ((i3 & 4) != 0) {
            this.A = true;
        }
        this.d.c((o2 & (i3 ^ -1)) | (i2 & i3));
    }

    public void a(Configuration configuration) {
        k(android.support.v7.view.a.a(this.f939a).d());
    }

    public void a(CharSequence charSequence) {
        this.d.a(charSequence);
    }

    public void a(boolean z2) {
        a(z2 ? 4 : 0, 4);
    }

    public boolean a(int i2, KeyEvent keyEvent) {
        if (this.h == null) {
            return false;
        }
        Menu b2 = this.h.b();
        if (b2 == null) {
            return false;
        }
        b2.setQwertyMode(KeyCharacterMap.load(keyEvent != null ? keyEvent.getDeviceId() : -1).getKeyboardType() != 1);
        return b2.performShortcut(i2, keyEvent, 0);
    }

    public Context b() {
        if (this.v == null) {
            TypedValue typedValue = new TypedValue();
            this.f939a.getTheme().resolveAttribute(C0026a.actionBarWidgetTheme, typedValue, true);
            int i2 = typedValue.resourceId;
            if (i2 != 0) {
                this.v = new ContextThemeWrapper(this.f939a, i2);
            } else {
                this.v = this.f939a;
            }
        }
        return this.v;
    }

    public void b(boolean z2) {
        this.d.b(z2);
    }

    public void c(boolean z2) {
        if (!z2 || this.f940b.a()) {
            this.o = z2;
            this.f940b.setHideOnContentScrollEnabled(z2);
            return;
        }
        throw new IllegalStateException("Action bar must be in overlay mode (Window.FEATURE_OVERLAY_ACTION_BAR) to enable hide on content scroll");
    }

    public void d(boolean z2) {
        if (!this.A) {
            a(z2);
        }
    }

    public void e(boolean z2) {
        this.H = z2;
        if (!z2 && this.n != null) {
            this.n.c();
        }
    }

    public void f(boolean z2) {
        if (z2 != this.B) {
            this.B = z2;
            int size = this.C.size();
            for (int i2 = 0; i2 < size; i2++) {
                ((b) this.C.get(i2)).a(z2);
            }
        }
    }

    public boolean f() {
        if (this.d == null || !this.d.c()) {
            return false;
        }
        this.d.d();
        return true;
    }

    public void g(boolean z2) {
        this.k = z2;
    }

    /* access modifiers changed from: 0000 */
    public void h() {
        if (this.j != null) {
            this.j.a(this.i);
            this.i = null;
            this.j = null;
        }
    }

    public void h(boolean z2) {
        if (this.n != null) {
            this.n.c();
        }
        this.c.setVisibility(0);
        if (this.E != 0 || (!this.H && !z2)) {
            this.c.setAlpha(1.0f);
            this.c.setTranslationY(0.0f);
            if (this.k && this.f != null) {
                this.f.setTranslationY(0.0f);
            }
            this.q.b(null);
        } else {
            this.c.setTranslationY(0.0f);
            float f2 = (float) (-this.c.getHeight());
            if (z2) {
                int[] iArr = {0, 0};
                this.c.getLocationInWindow(iArr);
                f2 -= (float) iArr[1];
            }
            this.c.setTranslationY(f2);
            h hVar = new h();
            y b2 = t.j(this.c).b(0.0f);
            b2.a(this.r);
            hVar.a(b2);
            if (this.k && this.f != null) {
                this.f.setTranslationY(f2);
                hVar.a(t.j(this.f).b(0.0f));
            }
            hVar.a(u);
            hVar.a(250);
            hVar.a(this.q);
            this.n = hVar;
            hVar.a();
        }
        if (this.f940b != null) {
            t.n(this.f940b);
        }
    }

    public int i() {
        return this.d.p();
    }

    public void i(boolean z2) {
        if (this.n != null) {
            this.n.c();
        }
        if (this.E != 0 || (!this.H && !z2)) {
            this.p.b(null);
            return;
        }
        this.c.setAlpha(1.0f);
        this.c.setTransitioning(true);
        h hVar = new h();
        float f2 = (float) (-this.c.getHeight());
        if (z2) {
            int[] iArr = {0, 0};
            this.c.getLocationInWindow(iArr);
            f2 -= (float) iArr[1];
        }
        y b2 = t.j(this.c).b(f2);
        b2.a(this.r);
        hVar.a(b2);
        if (this.k && this.f != null) {
            hVar.a(t.j(this.f).b(f2));
        }
        hVar.a(t);
        hVar.a(250);
        hVar.a(this.p);
        this.n = hVar;
        hVar.a();
    }

    public void j() {
        if (this.m) {
            this.m = false;
            l(true);
        }
    }

    public void j(boolean z2) {
        y a2;
        y a3;
        if (z2) {
            n();
        } else {
            o();
        }
        if (p()) {
            if (z2) {
                a3 = this.d.a(4, 100);
                a2 = this.e.a(0, 200);
            } else {
                a2 = this.d.a(0, 200);
                a3 = this.e.a(8, 100);
            }
            h hVar = new h();
            hVar.a(a3, a2);
            hVar.a();
        } else if (z2) {
            this.d.d(4);
            this.e.setVisibility(0);
        } else {
            this.d.d(0);
            this.e.setVisibility(8);
        }
    }

    public void k() {
        if (!this.m) {
            this.m = true;
            l(true);
        }
    }

    public void l() {
        if (this.n != null) {
            this.n.c();
            this.n = null;
        }
    }

    public void m() {
    }
}
