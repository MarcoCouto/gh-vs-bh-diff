package android.support.v7.app;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources.Theme;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.media.AudioManager;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.v4.i.aa;
import android.support.v4.i.ac;
import android.support.v4.i.t;
import android.support.v4.i.y;
import android.support.v4.i.z;
import android.support.v4.widget.l;
import android.support.v7.a.a.C0026a;
import android.support.v7.a.a.g;
import android.support.v7.a.a.i;
import android.support.v7.a.a.j;
import android.support.v7.view.menu.f;
import android.support.v7.view.menu.h;
import android.support.v7.view.menu.o;
import android.support.v7.view.menu.p;
import android.support.v7.widget.ActionBarContextView;
import android.support.v7.widget.ContentFrameLayout;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.ViewStubCompat;
import android.support.v7.widget.aj;
import android.support.v7.widget.ap;
import android.support.v7.widget.bt;
import android.support.v7.widget.bw;
import android.support.v7.widget.m;
import android.text.TextUtils;
import android.util.AndroidRuntimeException;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.LayoutInflater.Factory;
import android.view.LayoutInflater.Factory2;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.ViewParent;
import android.view.Window;
import android.view.Window.Callback;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import com.hmatalonga.greenhub.Config;
import org.xmlpull.v1.XmlPullParser;

class k extends f implements android.support.v7.view.menu.h.a, Factory2 {
    private static final boolean t = (VERSION.SDK_INT < 21);
    private View A;
    private boolean B;
    private boolean C;
    private boolean D;
    private d[] E;
    private d F;
    private boolean G;
    private final Runnable H = new Runnable() {
        public void run() {
            if ((k.this.s & 1) != 0) {
                k.this.f(0);
            }
            if ((k.this.s & 4096) != 0) {
                k.this.f(108);
            }
            k.this.r = false;
            k.this.s = 0;
        }
    };
    private boolean I;
    private Rect J;
    private Rect K;
    private m L;
    android.support.v7.view.b m;
    ActionBarContextView n;
    PopupWindow o;
    Runnable p;
    y q = null;
    boolean r;
    int s;
    private aj u;
    private a v;
    private e w;
    private boolean x;
    private ViewGroup y;
    private TextView z;

    private final class a implements android.support.v7.view.menu.o.a {
        a() {
        }

        public void a(h hVar, boolean z) {
            k.this.b(hVar);
        }

        public boolean a(h hVar) {
            Callback q = k.this.q();
            if (q != null) {
                q.onMenuOpened(108, hVar);
            }
            return true;
        }
    }

    class b implements android.support.v7.view.b.a {

        /* renamed from: b reason: collision with root package name */
        private android.support.v7.view.b.a f912b;

        public b(android.support.v7.view.b.a aVar) {
            this.f912b = aVar;
        }

        public void a(android.support.v7.view.b bVar) {
            this.f912b.a(bVar);
            if (k.this.o != null) {
                k.this.f896b.getDecorView().removeCallbacks(k.this.p);
            }
            if (k.this.n != null) {
                k.this.t();
                k.this.q = t.j(k.this.n).a(0.0f);
                k.this.q.a((z) new aa() {
                    public void b(View view) {
                        k.this.n.setVisibility(8);
                        if (k.this.o != null) {
                            k.this.o.dismiss();
                        } else if (k.this.n.getParent() instanceof View) {
                            t.n((View) k.this.n.getParent());
                        }
                        k.this.n.removeAllViews();
                        k.this.q.a((z) null);
                        k.this.q = null;
                    }
                });
            }
            if (k.this.e != null) {
                k.this.e.b(k.this.m);
            }
            k.this.m = null;
        }

        public boolean a(android.support.v7.view.b bVar, Menu menu) {
            return this.f912b.a(bVar, menu);
        }

        public boolean a(android.support.v7.view.b bVar, MenuItem menuItem) {
            return this.f912b.a(bVar, menuItem);
        }

        public boolean b(android.support.v7.view.b bVar, Menu menu) {
            return this.f912b.b(bVar, menu);
        }
    }

    private class c extends ContentFrameLayout {
        public c(Context context) {
            super(context);
        }

        private boolean a(int i, int i2) {
            return i < -5 || i2 < -5 || i > getWidth() + 5 || i2 > getHeight() + 5;
        }

        public boolean dispatchKeyEvent(KeyEvent keyEvent) {
            return k.this.a(keyEvent) || super.dispatchKeyEvent(keyEvent);
        }

        public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
            if (motionEvent.getAction() != 0 || !a((int) motionEvent.getX(), (int) motionEvent.getY())) {
                return super.onInterceptTouchEvent(motionEvent);
            }
            k.this.e(0);
            return true;
        }

        public void setBackgroundResource(int i) {
            setBackgroundDrawable(android.support.v7.c.a.b.b(getContext(), i));
        }
    }

    protected static final class d {

        /* renamed from: a reason: collision with root package name */
        int f915a;

        /* renamed from: b reason: collision with root package name */
        int f916b;
        int c;
        int d;
        int e;
        int f;
        ViewGroup g;
        View h;
        View i;
        h j;
        f k;
        Context l;
        boolean m;
        boolean n;
        boolean o;
        public boolean p;
        boolean q = false;
        boolean r;
        Bundle s;

        d(int i2) {
            this.f915a = i2;
        }

        /* access modifiers changed from: 0000 */
        public p a(android.support.v7.view.menu.o.a aVar) {
            if (this.j == null) {
                return null;
            }
            if (this.k == null) {
                this.k = new f(this.l, g.abc_list_menu_item_layout);
                this.k.a(aVar);
                this.j.a((o) this.k);
            }
            return this.k.a(this.g);
        }

        /* access modifiers changed from: 0000 */
        public void a(Context context) {
            TypedValue typedValue = new TypedValue();
            Theme newTheme = context.getResources().newTheme();
            newTheme.setTo(context.getTheme());
            newTheme.resolveAttribute(C0026a.actionBarPopupTheme, typedValue, true);
            if (typedValue.resourceId != 0) {
                newTheme.applyStyle(typedValue.resourceId, true);
            }
            newTheme.resolveAttribute(C0026a.panelMenuListTheme, typedValue, true);
            if (typedValue.resourceId != 0) {
                newTheme.applyStyle(typedValue.resourceId, true);
            } else {
                newTheme.applyStyle(i.Theme_AppCompat_CompactMenu, true);
            }
            android.support.v7.view.d dVar = new android.support.v7.view.d(context, 0);
            dVar.getTheme().setTo(newTheme);
            this.l = dVar;
            TypedArray obtainStyledAttributes = dVar.obtainStyledAttributes(j.AppCompatTheme);
            this.f916b = obtainStyledAttributes.getResourceId(j.AppCompatTheme_panelBackground, 0);
            this.f = obtainStyledAttributes.getResourceId(j.AppCompatTheme_android_windowAnimationStyle, 0);
            obtainStyledAttributes.recycle();
        }

        /* access modifiers changed from: 0000 */
        public void a(h hVar) {
            if (hVar != this.j) {
                if (this.j != null) {
                    this.j.b((o) this.k);
                }
                this.j = hVar;
                if (hVar != null && this.k != null) {
                    hVar.a((o) this.k);
                }
            }
        }

        public boolean a() {
            if (this.h == null) {
                return false;
            }
            return this.i != null || this.k.d().getCount() > 0;
        }
    }

    private final class e implements android.support.v7.view.menu.o.a {
        e() {
        }

        public void a(h hVar, boolean z) {
            h p = hVar.p();
            boolean z2 = p != hVar;
            k kVar = k.this;
            if (z2) {
                hVar = p;
            }
            d a2 = kVar.a((Menu) hVar);
            if (a2 == null) {
                return;
            }
            if (z2) {
                k.this.a(a2.f915a, a2, p);
                k.this.a(a2, true);
                return;
            }
            k.this.a(a2, z);
        }

        public boolean a(h hVar) {
            if (hVar == null && k.this.h) {
                Callback q = k.this.q();
                if (q != null && !k.this.p()) {
                    q.onMenuOpened(108, hVar);
                }
            }
            return true;
        }
    }

    k(Context context, Window window, d dVar) {
        super(context, window, dVar);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:57:0x00f0, code lost:
        if (r0.width == -1) goto L_0x00af;
     */
    private void a(d dVar, KeyEvent keyEvent) {
        int i = -1;
        if (!dVar.o && !p()) {
            if (dVar.f915a == 0) {
                if ((this.f895a.getResources().getConfiguration().screenLayout & 15) == 4) {
                    return;
                }
            }
            Callback q2 = q();
            if (q2 == null || q2.onMenuOpened(dVar.f915a, dVar.j)) {
                WindowManager windowManager = (WindowManager) this.f895a.getSystemService("window");
                if (windowManager != null && b(dVar, keyEvent)) {
                    if (dVar.g == null || dVar.q) {
                        if (dVar.g == null) {
                            if (!a(dVar) || dVar.g == null) {
                                return;
                            }
                        } else if (dVar.q && dVar.g.getChildCount() > 0) {
                            dVar.g.removeAllViews();
                        }
                        if (c(dVar) && dVar.a()) {
                            LayoutParams layoutParams = dVar.h.getLayoutParams();
                            LayoutParams layoutParams2 = layoutParams == null ? new LayoutParams(-2, -2) : layoutParams;
                            dVar.g.setBackgroundResource(dVar.f916b);
                            ViewParent parent = dVar.h.getParent();
                            if (parent != null && (parent instanceof ViewGroup)) {
                                ((ViewGroup) parent).removeView(dVar.h);
                            }
                            dVar.g.addView(dVar.h, layoutParams2);
                            if (!dVar.h.hasFocus()) {
                                dVar.h.requestFocus();
                            }
                            i = -2;
                        } else {
                            return;
                        }
                    } else {
                        if (dVar.i != null) {
                            LayoutParams layoutParams3 = dVar.i.getLayoutParams();
                            if (layoutParams3 != null) {
                            }
                        }
                        i = -2;
                    }
                    dVar.n = false;
                    WindowManager.LayoutParams layoutParams4 = new WindowManager.LayoutParams(i, -2, dVar.d, dVar.e, Config.NOTIFICATION_BATTERY_FULL, 8519680, -3);
                    layoutParams4.gravity = dVar.c;
                    layoutParams4.windowAnimations = dVar.f;
                    windowManager.addView(dVar.g, layoutParams4);
                    dVar.o = true;
                    return;
                }
                return;
            }
            a(dVar, true);
        }
    }

    private void a(h hVar, boolean z2) {
        if (this.u == null || !this.u.e() || (ViewConfiguration.get(this.f895a).hasPermanentMenuKey() && !this.u.g())) {
            d a2 = a(0, true);
            a2.q = true;
            a(a2, false);
            a(a2, (KeyEvent) null);
            return;
        }
        Callback q2 = q();
        if (this.u.f() && z2) {
            this.u.i();
            if (!p()) {
                q2.onPanelClosed(108, a(0, true).j);
            }
        } else if (q2 != null && !p()) {
            if (this.r && (this.s & 1) != 0) {
                this.f896b.getDecorView().removeCallbacks(this.H);
                this.H.run();
            }
            d a3 = a(0, true);
            if (a3.j != null && !a3.r && q2.onPreparePanel(0, a3.i, a3.j)) {
                q2.onMenuOpened(108, a3.j);
                this.u.h();
            }
        }
    }

    private boolean a(d dVar) {
        dVar.a(n());
        dVar.g = new c(dVar.l);
        dVar.c = 81;
        return true;
    }

    private boolean a(d dVar, int i, KeyEvent keyEvent, int i2) {
        boolean z2 = false;
        if (!keyEvent.isSystem()) {
            if ((dVar.m || b(dVar, keyEvent)) && dVar.j != null) {
                z2 = dVar.j.performShortcut(i, keyEvent, i2);
            }
            if (z2 && (i2 & 1) == 0 && this.u == null) {
                a(dVar, true);
            }
        }
        return z2;
    }

    private boolean a(ViewParent viewParent) {
        if (viewParent == null) {
            return false;
        }
        View decorView = this.f896b.getDecorView();
        for (ViewParent viewParent2 = viewParent; viewParent2 != null; viewParent2 = viewParent2.getParent()) {
            if (viewParent2 == decorView || !(viewParent2 instanceof View) || t.y((View) viewParent2)) {
                return false;
            }
        }
        return true;
    }

    private boolean b(d dVar) {
        Context context;
        Context context2 = this.f895a;
        if ((dVar.f915a == 0 || dVar.f915a == 108) && this.u != null) {
            TypedValue typedValue = new TypedValue();
            Theme theme = context2.getTheme();
            theme.resolveAttribute(C0026a.actionBarTheme, typedValue, true);
            Theme theme2 = null;
            if (typedValue.resourceId != 0) {
                theme2 = context2.getResources().newTheme();
                theme2.setTo(theme);
                theme2.applyStyle(typedValue.resourceId, true);
                theme2.resolveAttribute(C0026a.actionBarWidgetTheme, typedValue, true);
            } else {
                theme.resolveAttribute(C0026a.actionBarWidgetTheme, typedValue, true);
            }
            if (typedValue.resourceId != 0) {
                if (theme2 == null) {
                    theme2 = context2.getResources().newTheme();
                    theme2.setTo(theme);
                }
                theme2.applyStyle(typedValue.resourceId, true);
            }
            Theme theme3 = theme2;
            if (theme3 != null) {
                context = new android.support.v7.view.d(context2, 0);
                context.getTheme().setTo(theme3);
                h hVar = new h(context);
                hVar.a((android.support.v7.view.menu.h.a) this);
                dVar.a(hVar);
                return true;
            }
        }
        context = context2;
        h hVar2 = new h(context);
        hVar2.a((android.support.v7.view.menu.h.a) this);
        dVar.a(hVar2);
        return true;
    }

    private boolean b(d dVar, KeyEvent keyEvent) {
        if (p()) {
            return false;
        }
        if (dVar.m) {
            return true;
        }
        if (!(this.F == null || this.F == dVar)) {
            a(this.F, false);
        }
        Callback q2 = q();
        if (q2 != null) {
            dVar.i = q2.onCreatePanelView(dVar.f915a);
        }
        boolean z2 = dVar.f915a == 0 || dVar.f915a == 108;
        if (z2 && this.u != null) {
            this.u.j();
        }
        if (dVar.i == null && (!z2 || !(m() instanceof o))) {
            if (dVar.j == null || dVar.r) {
                if (dVar.j == null && (!b(dVar) || dVar.j == null)) {
                    return false;
                }
                if (z2 && this.u != null) {
                    if (this.v == null) {
                        this.v = new a();
                    }
                    this.u.a(dVar.j, this.v);
                }
                dVar.j.g();
                if (!q2.onCreatePanelMenu(dVar.f915a, dVar.j)) {
                    dVar.a((h) null);
                    if (!z2 || this.u == null) {
                        return false;
                    }
                    this.u.a(null, this.v);
                    return false;
                }
                dVar.r = false;
            }
            dVar.j.g();
            if (dVar.s != null) {
                dVar.j.d(dVar.s);
                dVar.s = null;
            }
            if (!q2.onPreparePanel(0, dVar.i, dVar.j)) {
                if (z2 && this.u != null) {
                    this.u.a(null, this.v);
                }
                dVar.j.h();
                return false;
            }
            dVar.p = KeyCharacterMap.load(keyEvent != null ? keyEvent.getDeviceId() : -1).getKeyboardType() != 1;
            dVar.j.setQwertyMode(dVar.p);
            dVar.j.h();
        }
        dVar.m = true;
        dVar.n = false;
        this.F = dVar;
        return true;
    }

    private boolean c(d dVar) {
        if (dVar.i != null) {
            dVar.h = dVar.i;
            return true;
        } else if (dVar.j == null) {
            return false;
        } else {
            if (this.w == null) {
                this.w = new e();
            }
            dVar.h = (View) dVar.a((android.support.v7.view.menu.o.a) this.w);
            return dVar.h != null;
        }
    }

    private void d(int i) {
        this.s |= 1 << i;
        if (!this.r) {
            t.a(this.f896b.getDecorView(), this.H);
            this.r = true;
        }
    }

    private boolean d(int i, KeyEvent keyEvent) {
        if (keyEvent.getRepeatCount() == 0) {
            d a2 = a(i, true);
            if (!a2.o) {
                return b(a2, keyEvent);
            }
        }
        return false;
    }

    private boolean e(int i, KeyEvent keyEvent) {
        boolean z2;
        boolean z3 = true;
        if (this.m != null) {
            return false;
        }
        d a2 = a(i, true);
        if (i != 0 || this.u == null || !this.u.e() || ViewConfiguration.get(this.f895a).hasPermanentMenuKey()) {
            if (a2.o || a2.n) {
                boolean z4 = a2.o;
                a(a2, true);
                z3 = z4;
            } else {
                if (a2.m) {
                    if (a2.r) {
                        a2.m = false;
                        z2 = b(a2, keyEvent);
                    } else {
                        z2 = true;
                    }
                    if (z2) {
                        a(a2, keyEvent);
                    }
                }
                z3 = false;
            }
        } else if (!this.u.f()) {
            if (!p() && b(a2, keyEvent)) {
                z3 = this.u.h();
            }
            z3 = false;
        } else {
            z3 = this.u.i();
        }
        if (z3) {
            AudioManager audioManager = (AudioManager) this.f895a.getSystemService("audio");
            if (audioManager != null) {
                audioManager.playSoundEffect(0);
            } else {
                Log.w("AppCompatDelegate", "Couldn't get audio manager");
            }
        }
        return z3;
    }

    private int h(int i) {
        if (i == 8) {
            Log.i("AppCompatDelegate", "You should now use the AppCompatDelegate.FEATURE_SUPPORT_ACTION_BAR id when requesting this feature.");
            return 108;
        } else if (i != 9) {
            return i;
        } else {
            Log.i("AppCompatDelegate", "You should now use the AppCompatDelegate.FEATURE_SUPPORT_ACTION_BAR_OVERLAY id when requesting this feature.");
            return 109;
        }
    }

    private void w() {
        if (!this.x) {
            this.y = x();
            CharSequence r2 = r();
            if (!TextUtils.isEmpty(r2)) {
                b(r2);
            }
            y();
            a(this.y);
            this.x = true;
            d a2 = a(0, false);
            if (p()) {
                return;
            }
            if (a2 == null || a2.j == null) {
                d(108);
            }
        }
    }

    private ViewGroup x() {
        ViewGroup viewGroup;
        TypedArray obtainStyledAttributes = this.f895a.obtainStyledAttributes(j.AppCompatTheme);
        if (!obtainStyledAttributes.hasValue(j.AppCompatTheme_windowActionBar)) {
            obtainStyledAttributes.recycle();
            throw new IllegalStateException("You need to use a Theme.AppCompat theme (or descendant) with this activity.");
        }
        if (obtainStyledAttributes.getBoolean(j.AppCompatTheme_windowNoTitle, false)) {
            c(1);
        } else if (obtainStyledAttributes.getBoolean(j.AppCompatTheme_windowActionBar, false)) {
            c(108);
        }
        if (obtainStyledAttributes.getBoolean(j.AppCompatTheme_windowActionBarOverlay, false)) {
            c(109);
        }
        if (obtainStyledAttributes.getBoolean(j.AppCompatTheme_windowActionModeOverlay, false)) {
            c(10);
        }
        this.k = obtainStyledAttributes.getBoolean(j.AppCompatTheme_android_windowIsFloating, false);
        obtainStyledAttributes.recycle();
        this.f896b.getDecorView();
        LayoutInflater from = LayoutInflater.from(this.f895a);
        if (this.l) {
            ViewGroup viewGroup2 = this.j ? (ViewGroup) from.inflate(g.abc_screen_simple_overlay_action_mode, null) : (ViewGroup) from.inflate(g.abc_screen_simple, null);
            if (VERSION.SDK_INT >= 21) {
                t.a((View) viewGroup2, (android.support.v4.i.p) new android.support.v4.i.p() {
                    public ac a(View view, ac acVar) {
                        int b2 = acVar.b();
                        int g = k.this.g(b2);
                        if (b2 != g) {
                            acVar = acVar.a(acVar.a(), g, acVar.c(), acVar.d());
                        }
                        return t.a(view, acVar);
                    }
                });
                viewGroup = viewGroup2;
            } else {
                ((ap) viewGroup2).setOnFitSystemWindowsListener(new android.support.v7.widget.ap.a() {
                    public void a(Rect rect) {
                        rect.top = k.this.g(rect.top);
                    }
                });
                viewGroup = viewGroup2;
            }
        } else if (this.k) {
            ViewGroup viewGroup3 = (ViewGroup) from.inflate(g.abc_dialog_title_material, null);
            this.i = false;
            this.h = false;
            viewGroup = viewGroup3;
        } else if (this.h) {
            TypedValue typedValue = new TypedValue();
            this.f895a.getTheme().resolveAttribute(C0026a.actionBarTheme, typedValue, true);
            ViewGroup viewGroup4 = (ViewGroup) LayoutInflater.from(typedValue.resourceId != 0 ? new android.support.v7.view.d(this.f895a, typedValue.resourceId) : this.f895a).inflate(g.abc_screen_toolbar, null);
            this.u = (aj) viewGroup4.findViewById(android.support.v7.a.a.f.decor_content_parent);
            this.u.setWindowCallback(q());
            if (this.i) {
                this.u.a(109);
            }
            if (this.B) {
                this.u.a(2);
            }
            if (this.C) {
                this.u.a(5);
            }
            viewGroup = viewGroup4;
        } else {
            viewGroup = null;
        }
        if (viewGroup == null) {
            throw new IllegalArgumentException("AppCompat does not support the current theme features: { windowActionBar: " + this.h + ", windowActionBarOverlay: " + this.i + ", android:windowIsFloating: " + this.k + ", windowActionModeOverlay: " + this.j + ", windowNoTitle: " + this.l + " }");
        }
        if (this.u == null) {
            this.z = (TextView) viewGroup.findViewById(android.support.v7.a.a.f.title);
        }
        bw.b(viewGroup);
        ContentFrameLayout contentFrameLayout = (ContentFrameLayout) viewGroup.findViewById(android.support.v7.a.a.f.action_bar_activity_content);
        ViewGroup viewGroup5 = (ViewGroup) this.f896b.findViewById(16908290);
        if (viewGroup5 != null) {
            while (viewGroup5.getChildCount() > 0) {
                View childAt = viewGroup5.getChildAt(0);
                viewGroup5.removeViewAt(0);
                contentFrameLayout.addView(childAt);
            }
            viewGroup5.setId(-1);
            contentFrameLayout.setId(16908290);
            if (viewGroup5 instanceof FrameLayout) {
                ((FrameLayout) viewGroup5).setForeground(null);
            }
        }
        this.f896b.setContentView(viewGroup);
        contentFrameLayout.setAttachListener(new android.support.v7.widget.ContentFrameLayout.a() {
            public void a() {
            }

            public void b() {
                k.this.v();
            }
        });
        return viewGroup;
    }

    private void y() {
        ContentFrameLayout contentFrameLayout = (ContentFrameLayout) this.y.findViewById(16908290);
        View decorView = this.f896b.getDecorView();
        contentFrameLayout.a(decorView.getPaddingLeft(), decorView.getPaddingTop(), decorView.getPaddingRight(), decorView.getPaddingBottom());
        TypedArray obtainStyledAttributes = this.f895a.obtainStyledAttributes(j.AppCompatTheme);
        obtainStyledAttributes.getValue(j.AppCompatTheme_windowMinWidthMajor, contentFrameLayout.getMinWidthMajor());
        obtainStyledAttributes.getValue(j.AppCompatTheme_windowMinWidthMinor, contentFrameLayout.getMinWidthMinor());
        if (obtainStyledAttributes.hasValue(j.AppCompatTheme_windowFixedWidthMajor)) {
            obtainStyledAttributes.getValue(j.AppCompatTheme_windowFixedWidthMajor, contentFrameLayout.getFixedWidthMajor());
        }
        if (obtainStyledAttributes.hasValue(j.AppCompatTheme_windowFixedWidthMinor)) {
            obtainStyledAttributes.getValue(j.AppCompatTheme_windowFixedWidthMinor, contentFrameLayout.getFixedWidthMinor());
        }
        if (obtainStyledAttributes.hasValue(j.AppCompatTheme_windowFixedHeightMajor)) {
            obtainStyledAttributes.getValue(j.AppCompatTheme_windowFixedHeightMajor, contentFrameLayout.getFixedHeightMajor());
        }
        if (obtainStyledAttributes.hasValue(j.AppCompatTheme_windowFixedHeightMinor)) {
            obtainStyledAttributes.getValue(j.AppCompatTheme_windowFixedHeightMinor, contentFrameLayout.getFixedHeightMinor());
        }
        obtainStyledAttributes.recycle();
        contentFrameLayout.requestLayout();
    }

    private void z() {
        if (this.x) {
            throw new AndroidRuntimeException("Window feature must be requested before adding content");
        }
    }

    /* access modifiers changed from: protected */
    public d a(int i, boolean z2) {
        d[] dVarArr = this.E;
        if (dVarArr == null || dVarArr.length <= i) {
            d[] dVarArr2 = new d[(i + 1)];
            if (dVarArr != null) {
                System.arraycopy(dVarArr, 0, dVarArr2, 0, dVarArr.length);
            }
            this.E = dVarArr2;
            dVarArr = dVarArr2;
        }
        d dVar = dVarArr[i];
        if (dVar != null) {
            return dVar;
        }
        d dVar2 = new d(i);
        dVarArr[i] = dVar2;
        return dVar2;
    }

    /* access modifiers changed from: 0000 */
    public d a(Menu menu) {
        d[] dVarArr = this.E;
        int i = dVarArr != null ? dVarArr.length : 0;
        for (int i2 = 0; i2 < i; i2++) {
            d dVar = dVarArr[i2];
            if (dVar != null && dVar.j == menu) {
                return dVar;
            }
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    public android.support.v7.view.b a(android.support.v7.view.b.a aVar) {
        android.support.v7.view.b bVar;
        Context context;
        t();
        if (this.m != null) {
            this.m.c();
        }
        if (!(aVar instanceof b)) {
            aVar = new b(aVar);
        }
        if (this.e == null || p()) {
            bVar = null;
        } else {
            try {
                bVar = this.e.a(aVar);
            } catch (AbstractMethodError e2) {
                bVar = null;
            }
        }
        if (bVar != null) {
            this.m = bVar;
        } else {
            if (this.n == null) {
                if (this.k) {
                    TypedValue typedValue = new TypedValue();
                    Theme theme = this.f895a.getTheme();
                    theme.resolveAttribute(C0026a.actionBarTheme, typedValue, true);
                    if (typedValue.resourceId != 0) {
                        Theme newTheme = this.f895a.getResources().newTheme();
                        newTheme.setTo(theme);
                        newTheme.applyStyle(typedValue.resourceId, true);
                        context = new android.support.v7.view.d(this.f895a, 0);
                        context.getTheme().setTo(newTheme);
                    } else {
                        context = this.f895a;
                    }
                    this.n = new ActionBarContextView(context);
                    this.o = new PopupWindow(context, null, C0026a.actionModePopupWindowStyle);
                    l.a(this.o, 2);
                    this.o.setContentView(this.n);
                    this.o.setWidth(-1);
                    context.getTheme().resolveAttribute(C0026a.actionBarSize, typedValue, true);
                    this.n.setContentHeight(TypedValue.complexToDimensionPixelSize(typedValue.data, context.getResources().getDisplayMetrics()));
                    this.o.setHeight(-2);
                    this.p = new Runnable() {
                        public void run() {
                            k.this.o.showAtLocation(k.this.n, 55, 0, 0);
                            k.this.t();
                            if (k.this.s()) {
                                k.this.n.setAlpha(0.0f);
                                k.this.q = t.j(k.this.n).a(1.0f);
                                k.this.q.a((z) new aa() {
                                    public void a(View view) {
                                        k.this.n.setVisibility(0);
                                    }

                                    public void b(View view) {
                                        k.this.n.setAlpha(1.0f);
                                        k.this.q.a((z) null);
                                        k.this.q = null;
                                    }
                                });
                                return;
                            }
                            k.this.n.setAlpha(1.0f);
                            k.this.n.setVisibility(0);
                        }
                    };
                } else {
                    ViewStubCompat viewStubCompat = (ViewStubCompat) this.y.findViewById(android.support.v7.a.a.f.action_mode_bar_stub);
                    if (viewStubCompat != null) {
                        viewStubCompat.setLayoutInflater(LayoutInflater.from(n()));
                        this.n = (ActionBarContextView) viewStubCompat.a();
                    }
                }
            }
            if (this.n != null) {
                t();
                this.n.c();
                android.support.v7.view.e eVar = new android.support.v7.view.e(this.n.getContext(), this.n, aVar, this.o == null);
                if (aVar.a((android.support.v7.view.b) eVar, eVar.b())) {
                    eVar.d();
                    this.n.a(eVar);
                    this.m = eVar;
                    if (s()) {
                        this.n.setAlpha(0.0f);
                        this.q = t.j(this.n).a(1.0f);
                        this.q.a((z) new aa() {
                            public void a(View view) {
                                k.this.n.setVisibility(0);
                                k.this.n.sendAccessibilityEvent(32);
                                if (k.this.n.getParent() instanceof View) {
                                    t.n((View) k.this.n.getParent());
                                }
                            }

                            public void b(View view) {
                                k.this.n.setAlpha(1.0f);
                                k.this.q.a((z) null);
                                k.this.q = null;
                            }
                        });
                    } else {
                        this.n.setAlpha(1.0f);
                        this.n.setVisibility(0);
                        this.n.sendAccessibilityEvent(32);
                        if (this.n.getParent() instanceof View) {
                            t.n((View) this.n.getParent());
                        }
                    }
                    if (this.o != null) {
                        this.f896b.getDecorView().post(this.p);
                    }
                } else {
                    this.m = null;
                }
            }
        }
        if (!(this.m == null || this.e == null)) {
            this.e.a(this.m);
        }
        return this.m;
    }

    public <T extends View> T a(int i) {
        w();
        return this.f896b.findViewById(i);
    }

    /* access modifiers changed from: 0000 */
    public View a(View view, String str, Context context, AttributeSet attributeSet) {
        if (this.c instanceof Factory) {
            View onCreateView = ((Factory) this.c).onCreateView(str, context, attributeSet);
            if (onCreateView != null) {
                return onCreateView;
            }
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    public void a(int i, d dVar, Menu menu) {
        if (menu == null) {
            if (dVar == null && i >= 0 && i < this.E.length) {
                dVar = this.E[i];
            }
            if (dVar != null) {
                menu = dVar.j;
            }
        }
        if ((dVar == null || dVar.o) && !p()) {
            this.c.onPanelClosed(i, menu);
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(int i, Menu menu) {
        if (i == 108) {
            a a2 = a();
            if (a2 != null) {
                a2.f(false);
            }
        } else if (i == 0) {
            d a3 = a(i, true);
            if (a3.o) {
                a(a3, false);
            }
        }
    }

    public void a(Configuration configuration) {
        if (this.h && this.x) {
            a a2 = a();
            if (a2 != null) {
                a2.a(configuration);
            }
        }
        m.a().a(this.f895a);
        i();
    }

    public void a(Bundle bundle) {
        if ((this.c instanceof Activity) && android.support.v4.a.y.b((Activity) this.c) != null) {
            a m2 = m();
            if (m2 == null) {
                this.I = true;
            } else {
                m2.d(true);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(d dVar, boolean z2) {
        if (!z2 || dVar.f915a != 0 || this.u == null || !this.u.f()) {
            WindowManager windowManager = (WindowManager) this.f895a.getSystemService("window");
            if (!(windowManager == null || !dVar.o || dVar.g == null)) {
                windowManager.removeView(dVar.g);
                if (z2) {
                    a(dVar.f915a, dVar, null);
                }
            }
            dVar.m = false;
            dVar.n = false;
            dVar.o = false;
            dVar.h = null;
            dVar.q = true;
            if (this.F == dVar) {
                this.F = null;
                return;
            }
            return;
        }
        b(dVar.j);
    }

    public void a(h hVar) {
        a(hVar, true);
    }

    public void a(Toolbar toolbar) {
        if (this.c instanceof Activity) {
            a a2 = a();
            if (a2 instanceof r) {
                throw new IllegalStateException("This Activity already has an action bar supplied by the window decor. Do not request Window.FEATURE_SUPPORT_ACTION_BAR and set windowActionBar to false in your theme to use a Toolbar instead.");
            }
            this.g = null;
            if (a2 != null) {
                a2.g();
            }
            if (toolbar != null) {
                o oVar = new o(toolbar, ((Activity) this.c).getTitle(), this.d);
                this.f = oVar;
                this.f896b.setCallback(oVar.h());
            } else {
                this.f = null;
                this.f896b.setCallback(this.d);
            }
            f();
        }
    }

    public void a(View view) {
        w();
        ViewGroup viewGroup = (ViewGroup) this.y.findViewById(16908290);
        viewGroup.removeAllViews();
        viewGroup.addView(view);
        this.c.onContentChanged();
    }

    public void a(View view, LayoutParams layoutParams) {
        w();
        ViewGroup viewGroup = (ViewGroup) this.y.findViewById(16908290);
        viewGroup.removeAllViews();
        viewGroup.addView(view, layoutParams);
        this.c.onContentChanged();
    }

    /* access modifiers changed from: 0000 */
    public void a(ViewGroup viewGroup) {
    }

    /* access modifiers changed from: 0000 */
    public boolean a(int i, KeyEvent keyEvent) {
        a a2 = a();
        if (a2 != null && a2.a(i, keyEvent)) {
            return true;
        }
        if (this.F == null || !a(this.F, keyEvent.getKeyCode(), keyEvent, 1)) {
            if (this.F == null) {
                d a3 = a(0, true);
                b(a3, keyEvent);
                boolean a4 = a(a3, keyEvent.getKeyCode(), keyEvent, 1);
                a3.m = false;
                if (a4) {
                    return true;
                }
            }
            return false;
        } else if (this.F == null) {
            return true;
        } else {
            this.F.n = true;
            return true;
        }
    }

    public boolean a(h hVar, MenuItem menuItem) {
        Callback q2 = q();
        if (q2 != null && !p()) {
            d a2 = a((Menu) hVar.p());
            if (a2 != null) {
                return q2.onMenuItemSelected(a2.f915a, menuItem);
            }
        }
        return false;
    }

    /* access modifiers changed from: 0000 */
    public boolean a(KeyEvent keyEvent) {
        boolean z2 = true;
        if (keyEvent.getKeyCode() == 82 && this.c.dispatchKeyEvent(keyEvent)) {
            return true;
        }
        int keyCode = keyEvent.getKeyCode();
        if (keyEvent.getAction() != 0) {
            z2 = false;
        }
        return z2 ? c(keyCode, keyEvent) : b(keyCode, keyEvent);
    }

    public android.support.v7.view.b b(android.support.v7.view.b.a aVar) {
        if (aVar == null) {
            throw new IllegalArgumentException("ActionMode callback can not be null.");
        }
        if (this.m != null) {
            this.m.c();
        }
        b bVar = new b(aVar);
        a a2 = a();
        if (a2 != null) {
            this.m = a2.a((android.support.v7.view.b.a) bVar);
            if (!(this.m == null || this.e == null)) {
                this.e.a(this.m);
            }
        }
        if (this.m == null) {
            this.m = a((android.support.v7.view.b.a) bVar);
        }
        return this.m;
    }

    public View b(View view, String str, Context context, AttributeSet attributeSet) {
        boolean z2;
        if (this.L == null) {
            this.L = new m();
        }
        if (t) {
            boolean a2 = attributeSet instanceof XmlPullParser ? ((XmlPullParser) attributeSet).getDepth() > 1 : a((ViewParent) view);
            z2 = a2;
        } else {
            z2 = false;
        }
        return this.L.a(view, str, context, attributeSet, z2, t, true, bt.a());
    }

    public void b(int i) {
        w();
        ViewGroup viewGroup = (ViewGroup) this.y.findViewById(16908290);
        viewGroup.removeAllViews();
        LayoutInflater.from(this.f895a).inflate(i, viewGroup);
        this.c.onContentChanged();
    }

    public void b(Bundle bundle) {
        w();
    }

    /* access modifiers changed from: 0000 */
    public void b(h hVar) {
        if (!this.D) {
            this.D = true;
            this.u.k();
            Callback q2 = q();
            if (q2 != null && !p()) {
                q2.onPanelClosed(108, hVar);
            }
            this.D = false;
        }
    }

    public void b(View view, LayoutParams layoutParams) {
        w();
        ((ViewGroup) this.y.findViewById(16908290)).addView(view, layoutParams);
        this.c.onContentChanged();
    }

    /* access modifiers changed from: 0000 */
    public void b(CharSequence charSequence) {
        if (this.u != null) {
            this.u.setWindowTitle(charSequence);
        } else if (m() != null) {
            m().a(charSequence);
        } else if (this.z != null) {
            this.z.setText(charSequence);
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean b(int i, KeyEvent keyEvent) {
        switch (i) {
            case 4:
                boolean z2 = this.G;
                this.G = false;
                d a2 = a(0, false);
                if (a2 == null || !a2.o) {
                    if (u()) {
                        return true;
                    }
                } else if (z2) {
                    return true;
                } else {
                    a(a2, true);
                    return true;
                }
                break;
            case 82:
                e(0, keyEvent);
                return true;
        }
        return false;
    }

    /* access modifiers changed from: 0000 */
    public boolean b(int i, Menu menu) {
        if (i != 108) {
            return false;
        }
        a a2 = a();
        if (a2 == null) {
            return true;
        }
        a2.f(true);
        return true;
    }

    public boolean c(int i) {
        int h = h(i);
        if (this.l && h == 108) {
            return false;
        }
        if (this.h && h == 1) {
            this.h = false;
        }
        switch (h) {
            case 1:
                z();
                this.l = true;
                return true;
            case 2:
                z();
                this.B = true;
                return true;
            case 5:
                z();
                this.C = true;
                return true;
            case 10:
                z();
                this.j = true;
                return true;
            case 108:
                z();
                this.h = true;
                return true;
            case 109:
                z();
                this.i = true;
                return true;
            default:
                return this.f896b.requestFeature(h);
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean c(int i, KeyEvent keyEvent) {
        boolean z2 = true;
        switch (i) {
            case 4:
                if ((keyEvent.getFlags() & 128) == 0) {
                    z2 = false;
                }
                this.G = z2;
                break;
            case 82:
                d(0, keyEvent);
                return true;
        }
        return false;
    }

    public void d() {
        a a2 = a();
        if (a2 != null) {
            a2.e(false);
        }
    }

    public void e() {
        a a2 = a();
        if (a2 != null) {
            a2.e(true);
        }
    }

    /* access modifiers changed from: 0000 */
    public void e(int i) {
        a(a(i, true), true);
    }

    public void f() {
        a a2 = a();
        if (a2 == null || !a2.e()) {
            d(0);
        }
    }

    /* access modifiers changed from: 0000 */
    public void f(int i) {
        d a2 = a(i, true);
        if (a2.j != null) {
            Bundle bundle = new Bundle();
            a2.j.c(bundle);
            if (bundle.size() > 0) {
                a2.s = bundle;
            }
            a2.j.g();
            a2.j.clear();
        }
        a2.r = true;
        a2.q = true;
        if ((i == 108 || i == 0) && this.u != null) {
            d a3 = a(0, false);
            if (a3 != null) {
                a3.m = false;
                b(a3, (KeyEvent) null);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public int g(int i) {
        boolean z2;
        boolean z3;
        boolean z4;
        boolean z5 = true;
        int i2 = 0;
        if (this.n == null || !(this.n.getLayoutParams() instanceof MarginLayoutParams)) {
            z2 = false;
        } else {
            MarginLayoutParams marginLayoutParams = (MarginLayoutParams) this.n.getLayoutParams();
            if (this.n.isShown()) {
                if (this.J == null) {
                    this.J = new Rect();
                    this.K = new Rect();
                }
                Rect rect = this.J;
                Rect rect2 = this.K;
                rect.set(0, i, 0, 0);
                bw.a(this.y, rect, rect2);
                if (marginLayoutParams.topMargin != (rect2.top == 0 ? i : 0)) {
                    marginLayoutParams.topMargin = i;
                    if (this.A == null) {
                        this.A = new View(this.f895a);
                        this.A.setBackgroundColor(this.f895a.getResources().getColor(android.support.v7.a.a.c.abc_input_method_navigation_guard));
                        this.y.addView(this.A, -1, new LayoutParams(-1, i));
                        z4 = true;
                    } else {
                        LayoutParams layoutParams = this.A.getLayoutParams();
                        if (layoutParams.height != i) {
                            layoutParams.height = i;
                            this.A.setLayoutParams(layoutParams);
                        }
                        z4 = true;
                    }
                } else {
                    z4 = false;
                }
                if (this.A == null) {
                    z5 = false;
                }
                if (!this.j && z5) {
                    i = 0;
                }
                boolean z6 = z4;
                z3 = z5;
                z5 = z6;
            } else if (marginLayoutParams.topMargin != 0) {
                marginLayoutParams.topMargin = 0;
                z3 = false;
            } else {
                z5 = false;
                z3 = false;
            }
            if (z5) {
                this.n.setLayoutParams(marginLayoutParams);
            }
            z2 = z3;
        }
        if (this.A != null) {
            View view = this.A;
            if (!z2) {
                i2 = 8;
            }
            view.setVisibility(i2);
        }
        return i;
    }

    public void g() {
        if (this.r) {
            this.f896b.getDecorView().removeCallbacks(this.H);
        }
        super.g();
        if (this.f != null) {
            this.f.g();
        }
    }

    public void h() {
        LayoutInflater from = LayoutInflater.from(this.f895a);
        if (from.getFactory() == null) {
            android.support.v4.i.f.b(from, this);
        } else if (!(from.getFactory2() instanceof k)) {
            Log.i("AppCompatDelegate", "The Activity's LayoutInflater already has a Factory installed so we can not install AppCompat's");
        }
    }

    public void l() {
        w();
        if (this.h && this.f == null) {
            if (this.c instanceof Activity) {
                this.f = new r((Activity) this.c, this.i);
            } else if (this.c instanceof Dialog) {
                this.f = new r((Dialog) this.c);
            }
            if (this.f != null) {
                this.f.d(this.I);
            }
        }
    }

    public final View onCreateView(View view, String str, Context context, AttributeSet attributeSet) {
        View a2 = a(view, str, context, attributeSet);
        return a2 != null ? a2 : b(view, str, context, attributeSet);
    }

    public View onCreateView(String str, Context context, AttributeSet attributeSet) {
        return onCreateView(null, str, context, attributeSet);
    }

    /* access modifiers changed from: 0000 */
    public final boolean s() {
        return this.x && this.y != null && t.v(this.y);
    }

    /* access modifiers changed from: 0000 */
    public void t() {
        if (this.q != null) {
            this.q.b();
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean u() {
        if (this.m != null) {
            this.m.c();
            return true;
        }
        a a2 = a();
        return a2 != null && a2.f();
    }

    /* access modifiers changed from: 0000 */
    public void v() {
        if (this.u != null) {
            this.u.k();
        }
        if (this.o != null) {
            this.f896b.getDecorView().removeCallbacks(this.p);
            if (this.o.isShowing()) {
                try {
                    this.o.dismiss();
                } catch (IllegalArgumentException e2) {
                }
            }
            this.o = null;
        }
        t();
        d a2 = a(0, false);
        if (a2 != null && a2.j != null) {
            a2.j.close();
        }
    }
}
