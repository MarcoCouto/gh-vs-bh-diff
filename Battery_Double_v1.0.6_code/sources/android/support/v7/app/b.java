package android.support.v7.app;

import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnKeyListener;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.a.a.C0026a;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ListAdapter;

public class b extends l implements DialogInterface {

    /* renamed from: a reason: collision with root package name */
    final AlertController f890a = new AlertController(getContext(), this, getWindow());

    public static class a {

        /* renamed from: a reason: collision with root package name */
        private final android.support.v7.app.AlertController.a f891a;

        /* renamed from: b reason: collision with root package name */
        private final int f892b;

        public a(Context context) {
            this(context, b.a(context, 0));
        }

        public a(Context context, int i) {
            this.f891a = new android.support.v7.app.AlertController.a(new ContextThemeWrapper(context, b.a(context, i)));
            this.f892b = i;
        }

        public Context a() {
            return this.f891a.f878a;
        }

        public a a(int i) {
            this.f891a.f = this.f891a.f878a.getText(i);
            return this;
        }

        public a a(int i, OnClickListener onClickListener) {
            this.f891a.i = this.f891a.f878a.getText(i);
            this.f891a.j = onClickListener;
            return this;
        }

        public a a(OnKeyListener onKeyListener) {
            this.f891a.r = onKeyListener;
            return this;
        }

        public a a(Drawable drawable) {
            this.f891a.d = drawable;
            return this;
        }

        public a a(View view) {
            this.f891a.g = view;
            return this;
        }

        public a a(ListAdapter listAdapter, OnClickListener onClickListener) {
            this.f891a.t = listAdapter;
            this.f891a.u = onClickListener;
            return this;
        }

        public a a(CharSequence charSequence) {
            this.f891a.f = charSequence;
            return this;
        }

        public a b(int i) {
            this.f891a.h = this.f891a.f878a.getText(i);
            return this;
        }

        public a b(int i, OnClickListener onClickListener) {
            this.f891a.k = this.f891a.f878a.getText(i);
            this.f891a.l = onClickListener;
            return this;
        }

        public a b(CharSequence charSequence) {
            this.f891a.h = charSequence;
            return this;
        }

        public b b() {
            b bVar = new b(this.f891a.f878a, this.f892b);
            this.f891a.a(bVar.f890a);
            bVar.setCancelable(this.f891a.o);
            if (this.f891a.o) {
                bVar.setCanceledOnTouchOutside(true);
            }
            bVar.setOnCancelListener(this.f891a.p);
            bVar.setOnDismissListener(this.f891a.q);
            if (this.f891a.r != null) {
                bVar.setOnKeyListener(this.f891a.r);
            }
            return bVar;
        }
    }

    protected b(Context context, int i) {
        super(context, a(context, i));
    }

    static int a(Context context, int i) {
        if (((i >>> 24) & 255) >= 1) {
            return i;
        }
        TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(C0026a.alertDialogTheme, typedValue, true);
        return typedValue.resourceId;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f890a.a();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (this.f890a.a(i, keyEvent)) {
            return true;
        }
        return super.onKeyDown(i, keyEvent);
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        if (this.f890a.b(i, keyEvent)) {
            return true;
        }
        return super.onKeyUp(i, keyEvent);
    }

    public void setTitle(CharSequence charSequence) {
        super.setTitle(charSequence);
        this.f890a.a(charSequence);
    }
}
