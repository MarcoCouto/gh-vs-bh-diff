package android.support.v7.app;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.b.c;
import android.util.Log;
import java.util.Calendar;

class q {

    /* renamed from: a reason: collision with root package name */
    private static q f935a;

    /* renamed from: b reason: collision with root package name */
    private final Context f936b;
    private final LocationManager c;
    private final a d = new a();

    private static class a {

        /* renamed from: a reason: collision with root package name */
        boolean f937a;

        /* renamed from: b reason: collision with root package name */
        long f938b;
        long c;
        long d;
        long e;
        long f;

        a() {
        }
    }

    q(Context context, LocationManager locationManager) {
        this.f936b = context;
        this.c = locationManager;
    }

    private Location a(String str) {
        try {
            if (this.c.isProviderEnabled(str)) {
                return this.c.getLastKnownLocation(str);
            }
        } catch (Exception e) {
            Log.d("TwilightManager", "Failed to get last known location", e);
        }
        return null;
    }

    static q a(Context context) {
        if (f935a == null) {
            Context applicationContext = context.getApplicationContext();
            f935a = new q(applicationContext, (LocationManager) applicationContext.getSystemService("location"));
        }
        return f935a;
    }

    private void a(Location location) {
        long j;
        a aVar = this.d;
        long currentTimeMillis = System.currentTimeMillis();
        p a2 = p.a();
        a2.a(currentTimeMillis - 86400000, location.getLatitude(), location.getLongitude());
        long j2 = a2.f933a;
        a2.a(currentTimeMillis, location.getLatitude(), location.getLongitude());
        boolean z = a2.c == 1;
        long j3 = a2.f934b;
        long j4 = a2.f933a;
        a2.a(86400000 + currentTimeMillis, location.getLatitude(), location.getLongitude());
        long j5 = a2.f934b;
        if (j3 == -1 || j4 == -1) {
            j = 43200000 + currentTimeMillis;
        } else {
            long j6 = currentTimeMillis > j4 ? 0 + j5 : currentTimeMillis > j3 ? 0 + j4 : 0 + j3;
            j = j6 + 60000;
        }
        aVar.f937a = z;
        aVar.f938b = j2;
        aVar.c = j3;
        aVar.d = j4;
        aVar.e = j5;
        aVar.f = j;
    }

    @SuppressLint({"MissingPermission"})
    private Location b() {
        Location location = null;
        Location location2 = c.a(this.f936b, "android.permission.ACCESS_COARSE_LOCATION") == 0 ? a("network") : null;
        if (c.a(this.f936b, "android.permission.ACCESS_FINE_LOCATION") == 0) {
            location = a("gps");
        }
        if (location != null && location2 != null) {
            return location.getTime() > location2.getTime() ? location : location2;
        }
        if (location == null) {
            location = location2;
        }
        return location;
    }

    private boolean c() {
        return this.d.f > System.currentTimeMillis();
    }

    /* access modifiers changed from: 0000 */
    public boolean a() {
        a aVar = this.d;
        if (c()) {
            return aVar.f937a;
        }
        Location b2 = b();
        if (b2 != null) {
            a(b2);
            return aVar.f937a;
        }
        Log.i("TwilightManager", "Could not get last known location. This is probably because the app does not have any location permissions. Falling back to hardcoded sunrise/sunset values.");
        int i = Calendar.getInstance().get(11);
        return i < 6 || i >= 22;
    }
}
