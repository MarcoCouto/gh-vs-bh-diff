package android.support.v7.app;

import android.content.Context;
import android.content.res.Configuration;
import android.support.v4.i.t;
import android.support.v7.view.i;
import android.support.v7.view.menu.h;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.ak;
import android.support.v7.widget.bp;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window.Callback;
import java.util.ArrayList;

class o extends a {

    /* renamed from: a reason: collision with root package name */
    ak f925a;

    /* renamed from: b reason: collision with root package name */
    boolean f926b;
    Callback c;
    private boolean d;
    private boolean e;
    private ArrayList<android.support.v7.app.a.b> f = new ArrayList<>();
    private final Runnable g = new Runnable() {
        public void run() {
            o.this.i();
        }
    };
    private final android.support.v7.widget.Toolbar.c h = new android.support.v7.widget.Toolbar.c() {
        public boolean a(MenuItem menuItem) {
            return o.this.c.onMenuItemSelected(0, menuItem);
        }
    };

    private final class a implements android.support.v7.view.menu.o.a {

        /* renamed from: b reason: collision with root package name */
        private boolean f930b;

        a() {
        }

        public void a(h hVar, boolean z) {
            if (!this.f930b) {
                this.f930b = true;
                o.this.f925a.n();
                if (o.this.c != null) {
                    o.this.c.onPanelClosed(108, hVar);
                }
                this.f930b = false;
            }
        }

        public boolean a(h hVar) {
            if (o.this.c == null) {
                return false;
            }
            o.this.c.onMenuOpened(108, hVar);
            return true;
        }
    }

    private final class b implements android.support.v7.view.menu.h.a {
        b() {
        }

        public void a(h hVar) {
            if (o.this.c == null) {
                return;
            }
            if (o.this.f925a.i()) {
                o.this.c.onPanelClosed(108, hVar);
            } else if (o.this.c.onPreparePanel(0, null, hVar)) {
                o.this.c.onMenuOpened(108, hVar);
            }
        }

        public boolean a(h hVar, MenuItem menuItem) {
            return false;
        }
    }

    private class c extends i {
        public c(Callback callback) {
            super(callback);
        }

        public View onCreatePanelView(int i) {
            return i == 0 ? new View(o.this.f925a.b()) : super.onCreatePanelView(i);
        }

        public boolean onPreparePanel(int i, View view, Menu menu) {
            boolean onPreparePanel = super.onPreparePanel(i, view, menu);
            if (onPreparePanel && !o.this.f926b) {
                o.this.f925a.m();
                o.this.f926b = true;
            }
            return onPreparePanel;
        }
    }

    o(Toolbar toolbar, CharSequence charSequence, Callback callback) {
        this.f925a = new bp(toolbar, false);
        this.c = new c(callback);
        this.f925a.a(this.c);
        toolbar.setOnMenuItemClickListener(this.h);
        this.f925a.a(charSequence);
    }

    private Menu j() {
        if (!this.d) {
            this.f925a.a((android.support.v7.view.menu.o.a) new a(), (android.support.v7.view.menu.h.a) new b());
            this.d = true;
        }
        return this.f925a.q();
    }

    public int a() {
        return this.f925a.o();
    }

    public void a(float f2) {
        t.a((View) this.f925a.a(), f2);
    }

    public void a(int i, int i2) {
        this.f925a.c((this.f925a.o() & (i2 ^ -1)) | (i & i2));
    }

    public void a(Configuration configuration) {
        super.a(configuration);
    }

    public void a(CharSequence charSequence) {
        this.f925a.a(charSequence);
    }

    public void a(boolean z) {
        a(z ? 4 : 0, 4);
    }

    public boolean a(int i, KeyEvent keyEvent) {
        Menu j = j();
        if (j == null) {
            return false;
        }
        j.setQwertyMode(KeyCharacterMap.load(keyEvent != null ? keyEvent.getDeviceId() : -1).getKeyboardType() != 1);
        return j.performShortcut(i, keyEvent, 0);
    }

    public boolean a(KeyEvent keyEvent) {
        if (keyEvent.getAction() == 1) {
            c();
        }
        return true;
    }

    public Context b() {
        return this.f925a.b();
    }

    public void b(boolean z) {
    }

    public boolean c() {
        return this.f925a.k();
    }

    public void d(boolean z) {
    }

    public boolean d() {
        return this.f925a.l();
    }

    public void e(boolean z) {
    }

    public boolean e() {
        this.f925a.a().removeCallbacks(this.g);
        t.a((View) this.f925a.a(), this.g);
        return true;
    }

    public void f(boolean z) {
        if (z != this.e) {
            this.e = z;
            int size = this.f.size();
            for (int i = 0; i < size; i++) {
                ((android.support.v7.app.a.b) this.f.get(i)).a(z);
            }
        }
    }

    public boolean f() {
        if (!this.f925a.c()) {
            return false;
        }
        this.f925a.d();
        return true;
    }

    /* access modifiers changed from: 0000 */
    public void g() {
        this.f925a.a().removeCallbacks(this.g);
    }

    public Callback h() {
        return this.c;
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: 0000 */
    public void i() {
        Menu j = j();
        h hVar = j instanceof h ? (h) j : null;
        if (hVar != null) {
            hVar.g();
        }
        try {
            j.clear();
            if (!this.c.onCreatePanelMenu(0, j) || !this.c.onPreparePanel(0, null, j)) {
                j.clear();
            }
            if (hVar != null) {
                hVar.h();
            }
        } catch (Throwable th) {
            if (hVar != null) {
                hVar.h();
            }
            throw th;
        }
    }
}
