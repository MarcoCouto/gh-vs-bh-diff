package android.support.transition;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroupOverlay;

class ad implements ae {

    /* renamed from: a reason: collision with root package name */
    private final ViewGroupOverlay f453a;

    ad(ViewGroup viewGroup) {
        this.f453a = viewGroup.getOverlay();
    }

    public void a(Drawable drawable) {
        this.f453a.add(drawable);
    }

    public void a(View view) {
        this.f453a.add(view);
    }

    public void b(Drawable drawable) {
        this.f453a.remove(drawable);
    }

    public void b(View view) {
        this.f453a.remove(view);
    }
}
