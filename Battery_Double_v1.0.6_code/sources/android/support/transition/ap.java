package android.support.transition;

import android.util.Log;
import android.view.View;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

class ap extends ao {

    /* renamed from: a reason: collision with root package name */
    private static Method f466a;

    /* renamed from: b reason: collision with root package name */
    private static boolean f467b;
    private static Method c;
    private static boolean d;

    ap() {
    }

    private void a() {
        if (!f467b) {
            try {
                f466a = View.class.getDeclaredMethod("setTransitionAlpha", new Class[]{Float.TYPE});
                f466a.setAccessible(true);
            } catch (NoSuchMethodException e) {
                Log.i("ViewUtilsApi19", "Failed to retrieve setTransitionAlpha method", e);
            }
            f467b = true;
        }
    }

    private void b() {
        if (!d) {
            try {
                c = View.class.getDeclaredMethod("getTransitionAlpha", new Class[0]);
                c.setAccessible(true);
            } catch (NoSuchMethodException e) {
                Log.i("ViewUtilsApi19", "Failed to retrieve getTransitionAlpha method", e);
            }
            d = true;
        }
    }

    public void a(View view, float f) {
        a();
        if (f466a != null) {
            try {
                f466a.invoke(view, new Object[]{Float.valueOf(f)});
            } catch (IllegalAccessException e) {
            } catch (InvocationTargetException e2) {
                throw new RuntimeException(e2.getCause());
            }
        } else {
            view.setAlpha(f);
        }
    }

    public float c(View view) {
        b();
        if (c != null) {
            try {
                return ((Float) c.invoke(view, new Object[0])).floatValue();
            } catch (IllegalAccessException e) {
            } catch (InvocationTargetException e2) {
                throw new RuntimeException(e2.getCause());
            }
        }
        return super.c(view);
    }

    public void d(View view) {
    }

    public void e(View view) {
    }
}
