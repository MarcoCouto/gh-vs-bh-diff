package android.support.transition;

import android.animation.LayoutTransition;
import android.support.transition.r.a;
import android.util.Log;
import android.view.ViewGroup;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

class ag implements ai {

    /* renamed from: a reason: collision with root package name */
    private static LayoutTransition f455a;

    /* renamed from: b reason: collision with root package name */
    private static Field f456b;
    private static boolean c;
    private static Method d;
    private static boolean e;

    ag() {
    }

    private static void a(LayoutTransition layoutTransition) {
        if (!e) {
            try {
                d = LayoutTransition.class.getDeclaredMethod("cancel", new Class[0]);
                d.setAccessible(true);
            } catch (NoSuchMethodException e2) {
                Log.i("ViewGroupUtilsApi14", "Failed to access cancel method by reflection");
            }
            e = true;
        }
        if (d != null) {
            try {
                d.invoke(layoutTransition, new Object[0]);
            } catch (IllegalAccessException e3) {
                Log.i("ViewGroupUtilsApi14", "Failed to access cancel method by reflection");
            } catch (InvocationTargetException e4) {
                Log.i("ViewGroupUtilsApi14", "Failed to invoke cancel method by reflection");
            }
        }
    }

    public ae a(ViewGroup viewGroup) {
        return ac.a(viewGroup);
    }

    public void a(ViewGroup viewGroup, boolean z) {
        boolean z2 = false;
        if (f455a == null) {
            f455a = new LayoutTransition() {
                public boolean isChangingLayout() {
                    return true;
                }
            };
            f455a.setAnimator(2, null);
            f455a.setAnimator(0, null);
            f455a.setAnimator(1, null);
            f455a.setAnimator(3, null);
            f455a.setAnimator(4, null);
        }
        if (z) {
            LayoutTransition layoutTransition = viewGroup.getLayoutTransition();
            if (layoutTransition != null) {
                if (layoutTransition.isRunning()) {
                    a(layoutTransition);
                }
                if (layoutTransition != f455a) {
                    viewGroup.setTag(a.transition_layout_save, layoutTransition);
                }
            }
            viewGroup.setLayoutTransition(f455a);
            return;
        }
        viewGroup.setLayoutTransition(null);
        if (!c) {
            try {
                f456b = ViewGroup.class.getDeclaredField("mLayoutSuppressed");
                f456b.setAccessible(true);
            } catch (NoSuchFieldException e2) {
                Log.i("ViewGroupUtilsApi14", "Failed to access mLayoutSuppressed field by reflection");
            }
            c = true;
        }
        if (f456b != null) {
            try {
                z2 = f456b.getBoolean(viewGroup);
                if (z2) {
                    f456b.setBoolean(viewGroup, false);
                }
            } catch (IllegalAccessException e3) {
                Log.i("ViewGroupUtilsApi14", "Failed to get mLayoutSuppressed field by reflection");
            }
        }
        if (z2) {
            viewGroup.requestLayout();
        }
        LayoutTransition layoutTransition2 = (LayoutTransition) viewGroup.getTag(a.transition_layout_save);
        if (layoutTransition2 != null) {
            viewGroup.setTag(a.transition_layout_save, null);
            viewGroup.setLayoutTransition(layoutTransition2);
        }
    }
}
