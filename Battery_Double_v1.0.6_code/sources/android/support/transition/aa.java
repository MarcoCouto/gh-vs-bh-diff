package android.support.transition;

import android.view.View;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class aa {

    /* renamed from: a reason: collision with root package name */
    public final Map<String, Object> f449a = new HashMap();

    /* renamed from: b reason: collision with root package name */
    public View f450b;
    final ArrayList<u> c = new ArrayList<>();

    public boolean equals(Object obj) {
        return (obj instanceof aa) && this.f450b == ((aa) obj).f450b && this.f449a.equals(((aa) obj).f449a);
    }

    public int hashCode() {
        return (this.f450b.hashCode() * 31) + this.f449a.hashCode();
    }

    public String toString() {
        String str = (("TransitionValues@" + Integer.toHexString(hashCode()) + ":\n") + "    view = " + this.f450b + "\n") + "    values:";
        Iterator it = this.f449a.keySet().iterator();
        while (true) {
            String str2 = str;
            if (!it.hasNext()) {
                return str2;
            }
            String str3 = (String) it.next();
            str = str2 + "    " + str3 + ": " + this.f449a.get(str3) + "\n";
        }
    }
}
