package android.support.transition;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.support.transition.u.c;
import android.support.v4.i.t;
import android.view.View;
import android.view.ViewGroup;

public class g extends at {

    private static class a extends AnimatorListenerAdapter {

        /* renamed from: a reason: collision with root package name */
        private final View f493a;

        /* renamed from: b reason: collision with root package name */
        private boolean f494b = false;

        a(View view) {
            this.f493a = view;
        }

        public void onAnimationEnd(Animator animator) {
            am.a(this.f493a, 1.0f);
            if (this.f494b) {
                this.f493a.setLayerType(0, null);
            }
        }

        public void onAnimationStart(Animator animator) {
            if (t.p(this.f493a) && this.f493a.getLayerType() == 0) {
                this.f494b = true;
                this.f493a.setLayerType(2, null);
            }
        }
    }

    public g() {
    }

    public g(int i) {
        a(i);
    }

    private static float a(aa aaVar, float f) {
        if (aaVar == null) {
            return f;
        }
        Float f2 = (Float) aaVar.f449a.get("android:fade:transitionAlpha");
        return f2 != null ? f2.floatValue() : f;
    }

    private Animator a(final View view, float f, float f2) {
        if (f == f2) {
            return null;
        }
        am.a(view, f);
        ObjectAnimator ofFloat = ObjectAnimator.ofFloat(view, am.f464a, new float[]{f2});
        ofFloat.addListener(new a(view));
        a((c) new v() {
            public void a(u uVar) {
                am.a(view, 1.0f);
                am.e(view);
                uVar.b((c) this);
            }
        });
        return ofFloat;
    }

    public Animator a(ViewGroup viewGroup, View view, aa aaVar, aa aaVar2) {
        float f = 0.0f;
        float a2 = a(aaVar, 0.0f);
        if (a2 != 1.0f) {
            f = a2;
        }
        return a(view, f, 1.0f);
    }

    public void a(aa aaVar) {
        super.a(aaVar);
        aaVar.f449a.put("android:fade:transitionAlpha", Float.valueOf(am.c(aaVar.f450b)));
    }

    public Animator b(ViewGroup viewGroup, View view, aa aaVar, aa aaVar2) {
        am.d(view);
        return a(view, a(aaVar, 1.0f), 0.0f);
    }
}
