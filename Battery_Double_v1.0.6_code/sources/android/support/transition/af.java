package android.support.transition;

import android.os.Build.VERSION;
import android.view.ViewGroup;

class af {

    /* renamed from: a reason: collision with root package name */
    private static final ai f454a;

    static {
        if (VERSION.SDK_INT >= 18) {
            f454a = new ah();
        } else {
            f454a = new ag();
        }
    }

    static ae a(ViewGroup viewGroup) {
        return f454a.a(viewGroup);
    }

    static void a(ViewGroup viewGroup, boolean z) {
        f454a.a(viewGroup, z);
    }
}
