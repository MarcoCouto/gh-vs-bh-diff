package android.support.transition;

import android.support.transition.u.c;
import android.support.v4.i.t;
import android.view.View;
import android.view.View.OnAttachStateChangeListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnPreDrawListener;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;

public class w {

    /* renamed from: a reason: collision with root package name */
    private static u f509a = new e();

    /* renamed from: b reason: collision with root package name */
    private static ThreadLocal<WeakReference<android.support.v4.h.a<ViewGroup, ArrayList<u>>>> f510b = new ThreadLocal<>();
    /* access modifiers changed from: private */
    public static ArrayList<ViewGroup> c = new ArrayList<>();

    private static class a implements OnAttachStateChangeListener, OnPreDrawListener {

        /* renamed from: a reason: collision with root package name */
        u f511a;

        /* renamed from: b reason: collision with root package name */
        ViewGroup f512b;

        a(u uVar, ViewGroup viewGroup) {
            this.f511a = uVar;
            this.f512b = viewGroup;
        }

        private void a() {
            this.f512b.getViewTreeObserver().removeOnPreDrawListener(this);
            this.f512b.removeOnAttachStateChangeListener(this);
        }

        public boolean onPreDraw() {
            ArrayList arrayList;
            ArrayList arrayList2;
            a();
            if (w.c.remove(this.f512b)) {
                final android.support.v4.h.a a2 = w.a();
                ArrayList arrayList3 = (ArrayList) a2.get(this.f512b);
                if (arrayList3 == null) {
                    ArrayList arrayList4 = new ArrayList();
                    a2.put(this.f512b, arrayList4);
                    arrayList = arrayList4;
                    arrayList2 = null;
                } else if (arrayList3.size() > 0) {
                    ArrayList arrayList5 = new ArrayList(arrayList3);
                    arrayList = arrayList3;
                    arrayList2 = arrayList5;
                } else {
                    arrayList = arrayList3;
                    arrayList2 = null;
                }
                arrayList.add(this.f511a);
                this.f511a.a((c) new v() {
                    public void a(u uVar) {
                        ((ArrayList) a2.get(a.this.f512b)).remove(uVar);
                    }
                });
                this.f511a.a(this.f512b, false);
                if (arrayList2 != null) {
                    Iterator it = arrayList2.iterator();
                    while (it.hasNext()) {
                        ((u) it.next()).e(this.f512b);
                    }
                }
                this.f511a.a(this.f512b);
            }
            return true;
        }

        public void onViewAttachedToWindow(View view) {
        }

        public void onViewDetachedFromWindow(View view) {
            a();
            w.c.remove(this.f512b);
            ArrayList arrayList = (ArrayList) w.a().get(this.f512b);
            if (arrayList != null && arrayList.size() > 0) {
                Iterator it = arrayList.iterator();
                while (it.hasNext()) {
                    ((u) it.next()).e(this.f512b);
                }
            }
            this.f511a.a(true);
        }
    }

    static android.support.v4.h.a<ViewGroup, ArrayList<u>> a() {
        WeakReference weakReference = (WeakReference) f510b.get();
        if (weakReference == null || weakReference.get() == null) {
            weakReference = new WeakReference(new android.support.v4.h.a());
            f510b.set(weakReference);
        }
        return (android.support.v4.h.a) weakReference.get();
    }

    public static void a(ViewGroup viewGroup, u uVar) {
        if (!c.contains(viewGroup) && t.v(viewGroup)) {
            c.add(viewGroup);
            if (uVar == null) {
                uVar = f509a;
            }
            u m = uVar.clone();
            c(viewGroup, m);
            t.a(viewGroup, null);
            b(viewGroup, m);
        }
    }

    private static void b(ViewGroup viewGroup, u uVar) {
        if (uVar != null && viewGroup != null) {
            a aVar = new a(uVar, viewGroup);
            viewGroup.addOnAttachStateChangeListener(aVar);
            viewGroup.getViewTreeObserver().addOnPreDrawListener(aVar);
        }
    }

    private static void c(ViewGroup viewGroup, u uVar) {
        ArrayList arrayList = (ArrayList) a().get(viewGroup);
        if (arrayList != null && arrayList.size() > 0) {
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                ((u) it.next()).d(viewGroup);
            }
        }
        if (uVar != null) {
            uVar.a(viewGroup, true);
        }
        t a2 = t.a(viewGroup);
        if (a2 != null) {
            a2.a();
        }
    }
}
