package android.support.transition;

import android.animation.TimeInterpolator;
import android.support.transition.u.b;
import android.support.transition.u.c;
import android.util.AndroidRuntimeException;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.Iterator;

public class y extends u {
    private ArrayList<u> g = new ArrayList<>();
    private boolean h = true;
    /* access modifiers changed from: private */
    public int i;
    /* access modifiers changed from: private */
    public boolean j = false;

    static class a extends v {

        /* renamed from: a reason: collision with root package name */
        y f517a;

        a(y yVar) {
            this.f517a = yVar;
        }

        public void a(u uVar) {
            y.b(this.f517a);
            if (this.f517a.i == 0) {
                this.f517a.j = false;
                this.f517a.k();
            }
            uVar.b((c) this);
        }

        public void d(u uVar) {
            if (!this.f517a.j) {
                this.f517a.j();
                this.f517a.j = true;
            }
        }
    }

    static /* synthetic */ int b(y yVar) {
        int i2 = yVar.i - 1;
        yVar.i = i2;
        return i2;
    }

    private void p() {
        a aVar = new a(this);
        Iterator it = this.g.iterator();
        while (it.hasNext()) {
            ((u) it.next()).a((c) aVar);
        }
        this.i = this.g.size();
    }

    public y a(int i2) {
        switch (i2) {
            case 0:
                this.h = true;
                break;
            case 1:
                this.h = false;
                break;
            default:
                throw new AndroidRuntimeException("Invalid parameter for TransitionSet ordering: " + i2);
        }
        return this;
    }

    /* access modifiers changed from: 0000 */
    public String a(String str) {
        String a2 = super.a(str);
        int i2 = 0;
        while (i2 < this.g.size()) {
            String str2 = a2 + "\n" + ((u) this.g.get(i2)).a(str + "  ");
            i2++;
            a2 = str2;
        }
        return a2;
    }

    public void a(aa aaVar) {
        if (a(aaVar.f450b)) {
            Iterator it = this.g.iterator();
            while (it.hasNext()) {
                u uVar = (u) it.next();
                if (uVar.a(aaVar.f450b)) {
                    uVar.a(aaVar);
                    aaVar.c.add(uVar);
                }
            }
        }
    }

    public void a(b bVar) {
        super.a(bVar);
        int size = this.g.size();
        for (int i2 = 0; i2 < size; i2++) {
            ((u) this.g.get(i2)).a(bVar);
        }
    }

    /* access modifiers changed from: protected */
    public void a(ViewGroup viewGroup, ab abVar, ab abVar2, ArrayList<aa> arrayList, ArrayList<aa> arrayList2) {
        long c = c();
        int size = this.g.size();
        for (int i2 = 0; i2 < size; i2++) {
            u uVar = (u) this.g.get(i2);
            if (c > 0 && (this.h || i2 == 0)) {
                long c2 = uVar.c();
                if (c2 > 0) {
                    uVar.b(c2 + c);
                } else {
                    uVar.b(c);
                }
            }
            uVar.a(viewGroup, abVar, abVar2, arrayList, arrayList2);
        }
    }

    public u b(int i2) {
        if (i2 < 0 || i2 >= this.g.size()) {
            return null;
        }
        return (u) this.g.get(i2);
    }

    /* renamed from: b */
    public y a(TimeInterpolator timeInterpolator) {
        return (y) super.a(timeInterpolator);
    }

    public y b(u uVar) {
        this.g.add(uVar);
        uVar.d = this;
        if (this.f502a >= 0) {
            uVar.a(this.f502a);
        }
        return this;
    }

    public void b(aa aaVar) {
        if (a(aaVar.f450b)) {
            Iterator it = this.g.iterator();
            while (it.hasNext()) {
                u uVar = (u) it.next();
                if (uVar.a(aaVar.f450b)) {
                    uVar.b(aaVar);
                    aaVar.c.add(uVar);
                }
            }
        }
    }

    /* renamed from: c */
    public y a(long j2) {
        super.a(j2);
        if (this.f502a >= 0) {
            int size = this.g.size();
            for (int i2 = 0; i2 < size; i2++) {
                ((u) this.g.get(i2)).a(j2);
            }
        }
        return this;
    }

    /* renamed from: c */
    public y a(c cVar) {
        return (y) super.a(cVar);
    }

    /* access modifiers changed from: 0000 */
    public void c(aa aaVar) {
        super.c(aaVar);
        int size = this.g.size();
        for (int i2 = 0; i2 < size; i2++) {
            ((u) this.g.get(i2)).c(aaVar);
        }
    }

    /* renamed from: d */
    public y b(long j2) {
        return (y) super.b(j2);
    }

    /* renamed from: d */
    public y b(c cVar) {
        return (y) super.b(cVar);
    }

    public void d(View view) {
        super.d(view);
        int size = this.g.size();
        for (int i2 = 0; i2 < size; i2++) {
            ((u) this.g.get(i2)).d(view);
        }
    }

    /* access modifiers changed from: protected */
    public void e() {
        if (this.g.isEmpty()) {
            j();
            k();
            return;
        }
        p();
        if (!this.h) {
            int i2 = 1;
            while (true) {
                int i3 = i2;
                if (i3 >= this.g.size()) {
                    break;
                }
                final u uVar = (u) this.g.get(i3);
                ((u) this.g.get(i3 - 1)).a((c) new v() {
                    public void a(u uVar) {
                        uVar.e();
                        uVar.b((c) this);
                    }
                });
                i2 = i3 + 1;
            }
            u uVar2 = (u) this.g.get(0);
            if (uVar2 != null) {
                uVar2.e();
                return;
            }
            return;
        }
        Iterator it = this.g.iterator();
        while (it.hasNext()) {
            ((u) it.next()).e();
        }
    }

    public void e(View view) {
        super.e(view);
        int size = this.g.size();
        for (int i2 = 0; i2 < size; i2++) {
            ((u) this.g.get(i2)).e(view);
        }
    }

    /* renamed from: f */
    public y b(View view) {
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= this.g.size()) {
                return (y) super.b(view);
            }
            ((u) this.g.get(i3)).b(view);
            i2 = i3 + 1;
        }
    }

    /* renamed from: g */
    public y c(View view) {
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= this.g.size()) {
                return (y) super.c(view);
            }
            ((u) this.g.get(i3)).c(view);
            i2 = i3 + 1;
        }
    }

    /* renamed from: m */
    public u clone() {
        y yVar = (y) super.clone();
        yVar.g = new ArrayList<>();
        int size = this.g.size();
        for (int i2 = 0; i2 < size; i2++) {
            yVar.b(((u) this.g.get(i2)).clone());
        }
        return yVar;
    }

    public int o() {
        return this.g.size();
    }
}
