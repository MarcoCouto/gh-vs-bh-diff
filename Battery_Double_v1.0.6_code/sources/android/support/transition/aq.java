package android.support.transition;

import android.graphics.Matrix;
import android.util.Log;
import android.view.View;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

class aq extends ap {

    /* renamed from: a reason: collision with root package name */
    private static Method f468a;

    /* renamed from: b reason: collision with root package name */
    private static boolean f469b;
    private static Method c;
    private static boolean d;

    aq() {
    }

    private void a() {
        if (!f469b) {
            try {
                f468a = View.class.getDeclaredMethod("transformMatrixToGlobal", new Class[]{Matrix.class});
                f468a.setAccessible(true);
            } catch (NoSuchMethodException e) {
                Log.i("ViewUtilsApi21", "Failed to retrieve transformMatrixToGlobal method", e);
            }
            f469b = true;
        }
    }

    private void b() {
        if (!d) {
            try {
                c = View.class.getDeclaredMethod("transformMatrixToLocal", new Class[]{Matrix.class});
                c.setAccessible(true);
            } catch (NoSuchMethodException e) {
                Log.i("ViewUtilsApi21", "Failed to retrieve transformMatrixToLocal method", e);
            }
            d = true;
        }
    }

    public void a(View view, Matrix matrix) {
        a();
        if (f468a != null) {
            try {
                f468a.invoke(view, new Object[]{matrix});
            } catch (IllegalAccessException e) {
            } catch (InvocationTargetException e2) {
                throw new RuntimeException(e2.getCause());
            }
        }
    }

    public void b(View view, Matrix matrix) {
        b();
        if (c != null) {
            try {
                c.invoke(view, new Object[]{matrix});
            } catch (IllegalAccessException e) {
            } catch (InvocationTargetException e2) {
                throw new RuntimeException(e2.getCause());
            }
        }
    }
}
