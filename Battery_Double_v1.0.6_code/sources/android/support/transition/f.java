package android.support.transition;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.transition.u.c;
import android.support.v4.i.t;
import android.util.Property;
import android.view.View;
import android.view.ViewGroup;

public class f extends u {
    private static final String[] g = {"android:changeBounds:bounds", "android:changeBounds:clip", "android:changeBounds:parent", "android:changeBounds:windowX", "android:changeBounds:windowY"};
    private static final Property<Drawable, PointF> h = new Property<Drawable, PointF>(PointF.class, "boundsOrigin") {

        /* renamed from: a reason: collision with root package name */
        private Rect f480a = new Rect();

        /* renamed from: a */
        public PointF get(Drawable drawable) {
            drawable.copyBounds(this.f480a);
            return new PointF((float) this.f480a.left, (float) this.f480a.top);
        }

        /* renamed from: a */
        public void set(Drawable drawable, PointF pointF) {
            drawable.copyBounds(this.f480a);
            this.f480a.offsetTo(Math.round(pointF.x), Math.round(pointF.y));
            drawable.setBounds(this.f480a);
        }
    };
    private static final Property<a, PointF> i = new Property<a, PointF>(PointF.class, "topLeft") {
        /* renamed from: a */
        public PointF get(a aVar) {
            return null;
        }

        /* renamed from: a */
        public void set(a aVar, PointF pointF) {
            aVar.a(pointF);
        }
    };
    private static final Property<a, PointF> j = new Property<a, PointF>(PointF.class, "bottomRight") {
        /* renamed from: a */
        public PointF get(a aVar) {
            return null;
        }

        /* renamed from: a */
        public void set(a aVar, PointF pointF) {
            aVar.b(pointF);
        }
    };
    private static final Property<View, PointF> k = new Property<View, PointF>(PointF.class, "bottomRight") {
        /* renamed from: a */
        public PointF get(View view) {
            return null;
        }

        /* renamed from: a */
        public void set(View view, PointF pointF) {
            am.a(view, view.getLeft(), view.getTop(), Math.round(pointF.x), Math.round(pointF.y));
        }
    };
    private static final Property<View, PointF> l = new Property<View, PointF>(PointF.class, "topLeft") {
        /* renamed from: a */
        public PointF get(View view) {
            return null;
        }

        /* renamed from: a */
        public void set(View view, PointF pointF) {
            am.a(view, Math.round(pointF.x), Math.round(pointF.y), view.getRight(), view.getBottom());
        }
    };
    private static final Property<View, PointF> m = new Property<View, PointF>(PointF.class, "position") {
        /* renamed from: a */
        public PointF get(View view) {
            return null;
        }

        /* renamed from: a */
        public void set(View view, PointF pointF) {
            int round = Math.round(pointF.x);
            int round2 = Math.round(pointF.y);
            am.a(view, round, round2, view.getWidth() + round, view.getHeight() + round2);
        }
    };
    private static s q = new s();
    private int[] n = new int[2];
    private boolean o = false;
    private boolean p = false;

    private static class a {

        /* renamed from: a reason: collision with root package name */
        private int f489a;

        /* renamed from: b reason: collision with root package name */
        private int f490b;
        private int c;
        private int d;
        private View e;
        private int f;
        private int g;

        a(View view) {
            this.e = view;
        }

        private void a() {
            am.a(this.e, this.f489a, this.f490b, this.c, this.d);
            this.f = 0;
            this.g = 0;
        }

        /* access modifiers changed from: 0000 */
        public void a(PointF pointF) {
            this.f489a = Math.round(pointF.x);
            this.f490b = Math.round(pointF.y);
            this.f++;
            if (this.f == this.g) {
                a();
            }
        }

        /* access modifiers changed from: 0000 */
        public void b(PointF pointF) {
            this.c = Math.round(pointF.x);
            this.d = Math.round(pointF.y);
            this.g++;
            if (this.f == this.g) {
                a();
            }
        }
    }

    private boolean a(View view, View view2) {
        if (!this.p) {
            return true;
        }
        aa b2 = b(view, true);
        return b2 == null ? view == view2 : view2 == b2.f450b;
    }

    private void d(aa aaVar) {
        View view = aaVar.f450b;
        if (t.v(view) || view.getWidth() != 0 || view.getHeight() != 0) {
            aaVar.f449a.put("android:changeBounds:bounds", new Rect(view.getLeft(), view.getTop(), view.getRight(), view.getBottom()));
            aaVar.f449a.put("android:changeBounds:parent", aaVar.f450b.getParent());
            if (this.p) {
                aaVar.f450b.getLocationInWindow(this.n);
                aaVar.f449a.put("android:changeBounds:windowX", Integer.valueOf(this.n[0]));
                aaVar.f449a.put("android:changeBounds:windowY", Integer.valueOf(this.n[1]));
            }
            if (this.o) {
                aaVar.f449a.put("android:changeBounds:clip", t.x(view));
            }
        }
    }

    public Animator a(ViewGroup viewGroup, aa aaVar, aa aaVar2) {
        ObjectAnimator objectAnimator;
        Animator a2;
        if (aaVar == null || aaVar2 == null) {
            return null;
        }
        ViewGroup viewGroup2 = (ViewGroup) aaVar.f449a.get("android:changeBounds:parent");
        ViewGroup viewGroup3 = (ViewGroup) aaVar2.f449a.get("android:changeBounds:parent");
        if (viewGroup2 == null || viewGroup3 == null) {
            return null;
        }
        final View view = aaVar2.f450b;
        if (a(viewGroup2, viewGroup3)) {
            Rect rect = (Rect) aaVar.f449a.get("android:changeBounds:bounds");
            Rect rect2 = (Rect) aaVar2.f449a.get("android:changeBounds:bounds");
            int i2 = rect.left;
            final int i3 = rect2.left;
            int i4 = rect.top;
            final int i5 = rect2.top;
            int i6 = rect.right;
            final int i7 = rect2.right;
            int i8 = rect.bottom;
            final int i9 = rect2.bottom;
            int i10 = i6 - i2;
            int i11 = i8 - i4;
            int i12 = i7 - i3;
            int i13 = i9 - i5;
            Rect rect3 = (Rect) aaVar.f449a.get("android:changeBounds:clip");
            final Rect rect4 = (Rect) aaVar2.f449a.get("android:changeBounds:clip");
            int i14 = 0;
            if (!((i10 == 0 || i11 == 0) && (i12 == 0 || i13 == 0))) {
                if (!(i2 == i3 && i4 == i5)) {
                    i14 = 1;
                }
                if (!(i6 == i7 && i8 == i9)) {
                    i14++;
                }
            }
            if ((rect3 != null && !rect3.equals(rect4)) || (rect3 == null && rect4 != null)) {
                i14++;
            }
            if (i14 > 0) {
                if (!this.o) {
                    am.a(view, i2, i4, i6, i8);
                    if (i14 != 2) {
                        a2 = (i2 == i3 && i4 == i5) ? h.a(view, k, l().a((float) i6, (float) i8, (float) i7, (float) i9)) : h.a(view, l, l().a((float) i2, (float) i4, (float) i3, (float) i5));
                    } else if (i10 == i12 && i11 == i13) {
                        a2 = h.a(view, m, l().a((float) i2, (float) i4, (float) i3, (float) i5));
                    } else {
                        final a aVar = new a(view);
                        ObjectAnimator a3 = h.a(aVar, i, l().a((float) i2, (float) i4, (float) i3, (float) i5));
                        ObjectAnimator a4 = h.a(aVar, j, l().a((float) i6, (float) i8, (float) i7, (float) i9));
                        AnimatorSet animatorSet = new AnimatorSet();
                        animatorSet.playTogether(new Animator[]{a3, a4});
                        animatorSet.addListener(new AnimatorListenerAdapter() {
                            private a mViewBounds = aVar;
                        });
                        a2 = animatorSet;
                    }
                } else {
                    am.a(view, i2, i4, Math.max(i10, i12) + i2, Math.max(i11, i13) + i4);
                    Animator a5 = (i2 == i3 && i4 == i5) ? null : h.a(view, m, l().a((float) i2, (float) i4, (float) i3, (float) i5));
                    Rect rect5 = rect3 == null ? new Rect(0, 0, i10, i11) : rect3;
                    Rect rect6 = rect4 == null ? new Rect(0, 0, i12, i13) : rect4;
                    if (!rect5.equals(rect6)) {
                        t.a(view, rect5);
                        ObjectAnimator ofObject = ObjectAnimator.ofObject(view, "clipBounds", q, new Object[]{rect5, rect6});
                        ofObject.addListener(new AnimatorListenerAdapter() {
                            private boolean h;

                            public void onAnimationCancel(Animator animator) {
                                this.h = true;
                            }

                            public void onAnimationEnd(Animator animator) {
                                if (!this.h) {
                                    t.a(view, rect4);
                                    am.a(view, i3, i5, i7, i9);
                                }
                            }
                        });
                        objectAnimator = ofObject;
                    } else {
                        objectAnimator = null;
                    }
                    a2 = z.a(a5, objectAnimator);
                }
                if (!(view.getParent() instanceof ViewGroup)) {
                    return a2;
                }
                final ViewGroup viewGroup4 = (ViewGroup) view.getParent();
                af.a(viewGroup4, true);
                a((c) new v() {

                    /* renamed from: a reason: collision with root package name */
                    boolean f481a = false;

                    public void a(u uVar) {
                        if (!this.f481a) {
                            af.a(viewGroup4, false);
                        }
                        uVar.b((c) this);
                    }

                    public void b(u uVar) {
                        af.a(viewGroup4, false);
                    }

                    public void c(u uVar) {
                        af.a(viewGroup4, true);
                    }
                });
                return a2;
            }
        } else {
            int intValue = ((Integer) aaVar.f449a.get("android:changeBounds:windowX")).intValue();
            int intValue2 = ((Integer) aaVar.f449a.get("android:changeBounds:windowY")).intValue();
            int intValue3 = ((Integer) aaVar2.f449a.get("android:changeBounds:windowX")).intValue();
            int intValue4 = ((Integer) aaVar2.f449a.get("android:changeBounds:windowY")).intValue();
            if (!(intValue == intValue3 && intValue2 == intValue4)) {
                viewGroup.getLocationInWindow(this.n);
                Bitmap createBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Config.ARGB_8888);
                view.draw(new Canvas(createBitmap));
                final BitmapDrawable bitmapDrawable = new BitmapDrawable(createBitmap);
                final float c = am.c(view);
                am.a(view, 0.0f);
                am.a(viewGroup).a(bitmapDrawable);
                ObjectAnimator ofPropertyValuesHolder = ObjectAnimator.ofPropertyValuesHolder(bitmapDrawable, new PropertyValuesHolder[]{n.a(h, l().a((float) (intValue - this.n[0]), (float) (intValue2 - this.n[1]), (float) (intValue3 - this.n[0]), (float) (intValue4 - this.n[1])))});
                final ViewGroup viewGroup5 = viewGroup;
                final View view2 = view;
                ofPropertyValuesHolder.addListener(new AnimatorListenerAdapter() {
                    public void onAnimationEnd(Animator animator) {
                        am.a(viewGroup5).b(bitmapDrawable);
                        am.a(view2, c);
                    }
                });
                return ofPropertyValuesHolder;
            }
        }
        return null;
    }

    public void a(aa aaVar) {
        d(aaVar);
    }

    public String[] a() {
        return g;
    }

    public void b(aa aaVar) {
        d(aaVar);
    }
}
