package android.support.transition;

import android.graphics.Rect;
import android.support.transition.u.b;
import android.support.transition.u.c;
import android.support.v4.a.v;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.List;

public class FragmentTransitionSupport extends v {
    private static boolean a(u uVar) {
        return !a(uVar.f()) || !a(uVar.h()) || !a(uVar.i());
    }

    public Object a(Object obj, Object obj2, Object obj3) {
        y yVar = new y();
        if (obj != null) {
            yVar.b((u) obj);
        }
        if (obj2 != null) {
            yVar.b((u) obj2);
        }
        if (obj3 != null) {
            yVar.b((u) obj3);
        }
        return yVar;
    }

    public void a(ViewGroup viewGroup, Object obj) {
        w.a(viewGroup, (u) obj);
    }

    public void a(Object obj, final Rect rect) {
        if (obj != null) {
            ((u) obj).a((b) new b() {
            });
        }
    }

    public void a(Object obj, View view) {
        if (view != null) {
            u uVar = (u) obj;
            final Rect rect = new Rect();
            a(view, rect);
            uVar.a((b) new b() {
            });
        }
    }

    public void a(Object obj, View view, ArrayList<View> arrayList) {
        y yVar = (y) obj;
        List g = yVar.g();
        g.clear();
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            a(g, (View) arrayList.get(i));
        }
        g.add(view);
        arrayList.add(view);
        a((Object) yVar, arrayList);
    }

    public void a(Object obj, Object obj2, ArrayList<View> arrayList, Object obj3, ArrayList<View> arrayList2, Object obj4, ArrayList<View> arrayList3) {
        final Object obj5 = obj2;
        final ArrayList<View> arrayList4 = arrayList;
        final Object obj6 = obj3;
        final ArrayList<View> arrayList5 = arrayList2;
        final Object obj7 = obj4;
        final ArrayList<View> arrayList6 = arrayList3;
        ((u) obj).a((c) new c() {
            public void a(u uVar) {
            }

            public void b(u uVar) {
            }

            public void c(u uVar) {
            }

            public void d(u uVar) {
                if (obj5 != null) {
                    FragmentTransitionSupport.this.b(obj5, arrayList4, null);
                }
                if (obj6 != null) {
                    FragmentTransitionSupport.this.b(obj6, arrayList5, null);
                }
                if (obj7 != null) {
                    FragmentTransitionSupport.this.b(obj7, arrayList6, null);
                }
            }
        });
    }

    public void a(Object obj, ArrayList<View> arrayList) {
        u uVar = (u) obj;
        if (uVar != null) {
            if (uVar instanceof y) {
                y yVar = (y) uVar;
                int o = yVar.o();
                for (int i = 0; i < o; i++) {
                    a((Object) yVar.b(i), arrayList);
                }
            } else if (!a(uVar) && a(uVar.g())) {
                int size = arrayList.size();
                for (int i2 = 0; i2 < size; i2++) {
                    uVar.b((View) arrayList.get(i2));
                }
            }
        }
    }

    public void a(Object obj, ArrayList<View> arrayList, ArrayList<View> arrayList2) {
        y yVar = (y) obj;
        if (yVar != null) {
            yVar.g().clear();
            yVar.g().addAll(arrayList2);
            b((Object) yVar, arrayList, arrayList2);
        }
    }

    public boolean a(Object obj) {
        return obj instanceof u;
    }

    public Object b(Object obj) {
        if (obj != null) {
            return ((u) obj).clone();
        }
        return null;
    }

    public Object b(Object obj, Object obj2, Object obj3) {
        u uVar = null;
        u uVar2 = (u) obj;
        u uVar3 = (u) obj2;
        u uVar4 = (u) obj3;
        if (uVar2 != null && uVar3 != null) {
            uVar = new y().b(uVar2).b(uVar3).a(1);
        } else if (uVar2 != null) {
            uVar = uVar2;
        } else if (uVar3 != null) {
            uVar = uVar3;
        }
        if (uVar4 == null) {
            return uVar;
        }
        y yVar = new y();
        if (uVar != null) {
            yVar.b(uVar);
        }
        yVar.b(uVar4);
        return yVar;
    }

    public void b(Object obj, View view) {
        if (obj != null) {
            ((u) obj).b(view);
        }
    }

    public void b(Object obj, final View view, final ArrayList<View> arrayList) {
        ((u) obj).a((c) new c() {
            public void a(u uVar) {
                uVar.b((c) this);
                view.setVisibility(8);
                int size = arrayList.size();
                for (int i = 0; i < size; i++) {
                    ((View) arrayList.get(i)).setVisibility(0);
                }
            }

            public void b(u uVar) {
            }

            public void c(u uVar) {
            }

            public void d(u uVar) {
            }
        });
    }

    public void b(Object obj, ArrayList<View> arrayList, ArrayList<View> arrayList2) {
        u uVar = (u) obj;
        if (uVar instanceof y) {
            y yVar = (y) uVar;
            int o = yVar.o();
            for (int i = 0; i < o; i++) {
                b((Object) yVar.b(i), arrayList, arrayList2);
            }
        } else if (!a(uVar)) {
            List g = uVar.g();
            if (g.size() == arrayList.size() && g.containsAll(arrayList)) {
                int size = arrayList2 == null ? 0 : arrayList2.size();
                for (int i2 = 0; i2 < size; i2++) {
                    uVar.b((View) arrayList2.get(i2));
                }
                for (int size2 = arrayList.size() - 1; size2 >= 0; size2--) {
                    uVar.c((View) arrayList.get(size2));
                }
            }
        }
    }

    public Object c(Object obj) {
        if (obj == null) {
            return null;
        }
        y yVar = new y();
        yVar.b((u) obj);
        return yVar;
    }

    public void c(Object obj, View view) {
        if (obj != null) {
            ((u) obj).c(view);
        }
    }
}
