package android.support.transition;

import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.PointF;
import android.util.Property;

class m<T> extends Property<T, Float> {

    /* renamed from: a reason: collision with root package name */
    private final Property<T, PointF> f496a;

    /* renamed from: b reason: collision with root package name */
    private final PathMeasure f497b;
    private final float c;
    private final float[] d = new float[2];
    private final PointF e = new PointF();
    private float f;

    m(Property<T, PointF> property, Path path) {
        super(Float.class, property.getName());
        this.f496a = property;
        this.f497b = new PathMeasure(path, false);
        this.c = this.f497b.getLength();
    }

    /* renamed from: a */
    public Float get(T t) {
        return Float.valueOf(this.f);
    }

    /* renamed from: a */
    public void set(T t, Float f2) {
        this.f = f2.floatValue();
        this.f497b.getPosTan(this.c * f2.floatValue(), this.d, null);
        this.e.x = this.d[0];
        this.e.y = this.d[1];
        this.f496a.set(t, this.e);
    }
}
