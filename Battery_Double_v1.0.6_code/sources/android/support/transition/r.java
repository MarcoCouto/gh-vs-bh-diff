package android.support.transition;

public final class r {

    public static final class a {
        public static final int action_container = 2131296273;
        public static final int action_divider = 2131296275;
        public static final int action_image = 2131296276;
        public static final int action_text = 2131296287;
        public static final int actions = 2131296288;
        public static final int async = 2131296312;
        public static final int blocking = 2131296328;
        public static final int chronometer = 2131296348;
        public static final int forever = 2131296386;
        public static final int ghost_view = 2131296388;
        public static final int icon = 2131296392;
        public static final int icon_group = 2131296393;
        public static final int info = 2131296402;
        public static final int italic = 2131296406;
        public static final int line1 = 2131296415;
        public static final int line3 = 2131296416;
        public static final int normal = 2131296449;
        public static final int notification_background = 2131296450;
        public static final int notification_main_column = 2131296451;
        public static final int notification_main_column_container = 2131296452;
        public static final int parent_matrix = 2131296455;
        public static final int right_icon = 2131296465;
        public static final int right_side = 2131296466;
        public static final int save_image_matrix = 2131296468;
        public static final int save_non_transition_alpha = 2131296469;
        public static final int save_scale_type = 2131296470;
        public static final int tag_transition_group = 2131296519;
        public static final int text = 2131296529;
        public static final int text2 = 2131296530;
        public static final int time = 2131296536;
        public static final int title = 2131296537;
        public static final int transition_current_scene = 2131296544;
        public static final int transition_layout_save = 2131296545;
        public static final int transition_position = 2131296546;
        public static final int transition_scene_layoutid_cache = 2131296547;
        public static final int transition_transform = 2131296548;
    }
}
