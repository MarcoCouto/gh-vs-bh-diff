package android.support.transition;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.os.Build.VERSION;

class a {

    /* renamed from: a reason: collision with root package name */
    private static final d f448a;

    static {
        if (VERSION.SDK_INT >= 19) {
            f448a = new c();
        } else {
            f448a = new b();
        }
    }

    static void a(Animator animator) {
        f448a.a(animator);
    }

    static void a(Animator animator, AnimatorListenerAdapter animatorListenerAdapter) {
        f448a.a(animator, animatorListenerAdapter);
    }

    static void b(Animator animator) {
        f448a.b(animator);
    }
}
