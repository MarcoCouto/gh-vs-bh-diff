package android.support.transition;

import android.animation.ObjectAnimator;
import android.graphics.Path;
import android.graphics.PointF;
import android.os.Build.VERSION;
import android.util.Property;

class h {

    /* renamed from: a reason: collision with root package name */
    private static final k f495a;

    static {
        if (VERSION.SDK_INT >= 21) {
            f495a = new j();
        } else {
            f495a = new i();
        }
    }

    static <T> ObjectAnimator a(T t, Property<T, PointF> property, Path path) {
        return f495a.a(t, property, path);
    }
}
