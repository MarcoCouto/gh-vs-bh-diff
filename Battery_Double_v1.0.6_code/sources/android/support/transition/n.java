package android.support.transition;

import android.animation.PropertyValuesHolder;
import android.graphics.Path;
import android.graphics.PointF;
import android.os.Build.VERSION;
import android.util.Property;

class n {

    /* renamed from: a reason: collision with root package name */
    private static final q f498a;

    static {
        if (VERSION.SDK_INT >= 21) {
            f498a = new p();
        } else {
            f498a = new o();
        }
    }

    static PropertyValuesHolder a(Property<?, PointF> property, Path path) {
        return f498a.a(property, path);
    }
}
