package android.support.transition;

import android.annotation.SuppressLint;
import android.util.Log;
import android.view.View;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

class ar extends aq {

    /* renamed from: a reason: collision with root package name */
    private static Method f470a;

    /* renamed from: b reason: collision with root package name */
    private static boolean f471b;

    ar() {
    }

    @SuppressLint({"PrivateApi"})
    private void a() {
        if (!f471b) {
            try {
                f470a = View.class.getDeclaredMethod("setLeftTopRightBottom", new Class[]{Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE});
                f470a.setAccessible(true);
            } catch (NoSuchMethodException e) {
                Log.i("ViewUtilsApi22", "Failed to retrieve setLeftTopRightBottom method", e);
            }
            f471b = true;
        }
    }

    public void a(View view, int i, int i2, int i3, int i4) {
        a();
        if (f470a != null) {
            try {
                f470a.invoke(view, new Object[]{Integer.valueOf(i), Integer.valueOf(i2), Integer.valueOf(i3), Integer.valueOf(i4)});
            } catch (IllegalAccessException e) {
            } catch (InvocationTargetException e2) {
                throw new RuntimeException(e2.getCause());
            }
        }
    }
}
