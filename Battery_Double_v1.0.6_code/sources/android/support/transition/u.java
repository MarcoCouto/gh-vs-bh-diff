package android.support.transition;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.TimeInterpolator;
import android.graphics.Path;
import android.support.v4.h.f;
import android.support.v4.h.m;
import android.support.v4.i.t;
import android.util.SparseArray;
import android.util.SparseIntArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public abstract class u implements Cloneable {
    private static final int[] g = {2, 1, 3, 4};
    private static final l h = new l() {
        public Path a(float f, float f2, float f3, float f4) {
            Path path = new Path();
            path.moveTo(f, f2);
            path.lineTo(f3, f4);
            return path;
        }
    };
    private static ThreadLocal<android.support.v4.h.a<Animator, a>> z = new ThreadLocal<>();
    private ViewGroup A = null;
    /* access modifiers changed from: private */
    public ArrayList<Animator> B = new ArrayList<>();
    private int C = 0;
    private boolean D = false;
    private boolean E = false;
    private ArrayList<c> F = null;
    private ArrayList<Animator> G = new ArrayList<>();
    private b H;
    private android.support.v4.h.a<String, String> I;
    private l J = h;

    /* renamed from: a reason: collision with root package name */
    long f502a = -1;

    /* renamed from: b reason: collision with root package name */
    ArrayList<Integer> f503b = new ArrayList<>();
    ArrayList<View> c = new ArrayList<>();
    y d = null;
    boolean e = false;
    x f;
    private String i = getClass().getName();
    private long j = -1;
    private TimeInterpolator k = null;
    private ArrayList<String> l = null;
    private ArrayList<Class> m = null;
    private ArrayList<Integer> n = null;
    private ArrayList<View> o = null;
    private ArrayList<Class> p = null;
    private ArrayList<String> q = null;
    private ArrayList<Integer> r = null;
    private ArrayList<View> s = null;
    private ArrayList<Class> t = null;
    private ab u = new ab();
    private ab v = new ab();
    private int[] w = g;
    private ArrayList<aa> x;
    private ArrayList<aa> y;

    private static class a {

        /* renamed from: a reason: collision with root package name */
        View f507a;

        /* renamed from: b reason: collision with root package name */
        String f508b;
        aa c;
        aw d;
        u e;

        a(View view, String str, u uVar, aw awVar, aa aaVar) {
            this.f507a = view;
            this.f508b = str;
            this.c = aaVar;
            this.d = awVar;
            this.e = uVar;
        }
    }

    public static abstract class b {
    }

    public interface c {
        void a(u uVar);

        void b(u uVar);

        void c(u uVar);

        void d(u uVar);
    }

    private void a(Animator animator, final android.support.v4.h.a<Animator, a> aVar) {
        if (animator != null) {
            animator.addListener(new AnimatorListenerAdapter() {
                public void onAnimationEnd(Animator animator) {
                    aVar.remove(animator);
                    u.this.B.remove(animator);
                }

                public void onAnimationStart(Animator animator) {
                    u.this.B.add(animator);
                }
            });
            a(animator);
        }
    }

    private void a(ab abVar, ab abVar2) {
        android.support.v4.h.a aVar = new android.support.v4.h.a((m) abVar.f451a);
        android.support.v4.h.a aVar2 = new android.support.v4.h.a((m) abVar2.f451a);
        for (int i2 : this.w) {
            switch (i2) {
                case 1:
                    a(aVar, aVar2);
                    break;
                case 2:
                    a(aVar, aVar2, abVar.d, abVar2.d);
                    break;
                case 3:
                    a(aVar, aVar2, abVar.f452b, abVar2.f452b);
                    break;
                case 4:
                    a(aVar, aVar2, abVar.c, abVar2.c);
                    break;
            }
        }
        b(aVar, aVar2);
    }

    private static void a(ab abVar, View view, aa aaVar) {
        abVar.f451a.put(view, aaVar);
        int id = view.getId();
        if (id >= 0) {
            if (abVar.f452b.indexOfKey(id) >= 0) {
                abVar.f452b.put(id, null);
            } else {
                abVar.f452b.put(id, view);
            }
        }
        String l2 = t.l(view);
        if (l2 != null) {
            if (abVar.d.containsKey(l2)) {
                abVar.d.put(l2, null);
            } else {
                abVar.d.put(l2, view);
            }
        }
        if (view.getParent() instanceof ListView) {
            ListView listView = (ListView) view.getParent();
            if (listView.getAdapter().hasStableIds()) {
                long itemIdAtPosition = listView.getItemIdAtPosition(listView.getPositionForView(view));
                if (abVar.c.c(itemIdAtPosition) >= 0) {
                    View view2 = (View) abVar.c.a(itemIdAtPosition);
                    if (view2 != null) {
                        t.a(view2, false);
                        abVar.c.b(itemIdAtPosition, null);
                        return;
                    }
                    return;
                }
                t.a(view, true);
                abVar.c.b(itemIdAtPosition, view);
            }
        }
    }

    private void a(android.support.v4.h.a<View, aa> aVar, android.support.v4.h.a<View, aa> aVar2) {
        for (int size = aVar.size() - 1; size >= 0; size--) {
            View view = (View) aVar.b(size);
            if (view != null && a(view)) {
                aa aaVar = (aa) aVar2.remove(view);
                if (!(aaVar == null || aaVar.f450b == null || !a(aaVar.f450b))) {
                    this.x.add((aa) aVar.d(size));
                    this.y.add(aaVar);
                }
            }
        }
    }

    private void a(android.support.v4.h.a<View, aa> aVar, android.support.v4.h.a<View, aa> aVar2, android.support.v4.h.a<String, View> aVar3, android.support.v4.h.a<String, View> aVar4) {
        int size = aVar3.size();
        for (int i2 = 0; i2 < size; i2++) {
            View view = (View) aVar3.c(i2);
            if (view != null && a(view)) {
                View view2 = (View) aVar4.get(aVar3.b(i2));
                if (view2 != null && a(view2)) {
                    aa aaVar = (aa) aVar.get(view);
                    aa aaVar2 = (aa) aVar2.get(view2);
                    if (!(aaVar == null || aaVar2 == null)) {
                        this.x.add(aaVar);
                        this.y.add(aaVar2);
                        aVar.remove(view);
                        aVar2.remove(view2);
                    }
                }
            }
        }
    }

    private void a(android.support.v4.h.a<View, aa> aVar, android.support.v4.h.a<View, aa> aVar2, f<View> fVar, f<View> fVar2) {
        int b2 = fVar.b();
        for (int i2 = 0; i2 < b2; i2++) {
            View view = (View) fVar.c(i2);
            if (view != null && a(view)) {
                View view2 = (View) fVar2.a(fVar.b(i2));
                if (view2 != null && a(view2)) {
                    aa aaVar = (aa) aVar.get(view);
                    aa aaVar2 = (aa) aVar2.get(view2);
                    if (!(aaVar == null || aaVar2 == null)) {
                        this.x.add(aaVar);
                        this.y.add(aaVar2);
                        aVar.remove(view);
                        aVar2.remove(view2);
                    }
                }
            }
        }
    }

    private void a(android.support.v4.h.a<View, aa> aVar, android.support.v4.h.a<View, aa> aVar2, SparseArray<View> sparseArray, SparseArray<View> sparseArray2) {
        int size = sparseArray.size();
        for (int i2 = 0; i2 < size; i2++) {
            View view = (View) sparseArray.valueAt(i2);
            if (view != null && a(view)) {
                View view2 = (View) sparseArray2.get(sparseArray.keyAt(i2));
                if (view2 != null && a(view2)) {
                    aa aaVar = (aa) aVar.get(view);
                    aa aaVar2 = (aa) aVar2.get(view2);
                    if (!(aaVar == null || aaVar2 == null)) {
                        this.x.add(aaVar);
                        this.y.add(aaVar2);
                        aVar.remove(view);
                        aVar2.remove(view2);
                    }
                }
            }
        }
    }

    private static boolean a(aa aaVar, aa aaVar2, String str) {
        Object obj = aaVar.f449a.get(str);
        Object obj2 = aaVar2.f449a.get(str);
        if (obj == null && obj2 == null) {
            return false;
        }
        return obj == null || obj2 == null || !obj.equals(obj2);
    }

    private void b(android.support.v4.h.a<View, aa> aVar, android.support.v4.h.a<View, aa> aVar2) {
        for (int i2 = 0; i2 < aVar.size(); i2++) {
            aa aaVar = (aa) aVar.c(i2);
            if (a(aaVar.f450b)) {
                this.x.add(aaVar);
                this.y.add(null);
            }
        }
        for (int i3 = 0; i3 < aVar2.size(); i3++) {
            aa aaVar2 = (aa) aVar2.c(i3);
            if (a(aaVar2.f450b)) {
                this.y.add(aaVar2);
                this.x.add(null);
            }
        }
    }

    private void c(View view, boolean z2) {
        if (view != null) {
            int id = view.getId();
            if (this.n != null && this.n.contains(Integer.valueOf(id))) {
                return;
            }
            if (this.o == null || !this.o.contains(view)) {
                if (this.p != null) {
                    int size = this.p.size();
                    int i2 = 0;
                    while (i2 < size) {
                        if (!((Class) this.p.get(i2)).isInstance(view)) {
                            i2++;
                        } else {
                            return;
                        }
                    }
                }
                if (view.getParent() instanceof ViewGroup) {
                    aa aaVar = new aa();
                    aaVar.f450b = view;
                    if (z2) {
                        a(aaVar);
                    } else {
                        b(aaVar);
                    }
                    aaVar.c.add(this);
                    c(aaVar);
                    if (z2) {
                        a(this.u, view, aaVar);
                    } else {
                        a(this.v, view, aaVar);
                    }
                }
                if (!(view instanceof ViewGroup)) {
                    return;
                }
                if (this.r != null && this.r.contains(Integer.valueOf(id))) {
                    return;
                }
                if (this.s == null || !this.s.contains(view)) {
                    if (this.t != null) {
                        int size2 = this.t.size();
                        int i3 = 0;
                        while (i3 < size2) {
                            if (!((Class) this.t.get(i3)).isInstance(view)) {
                                i3++;
                            } else {
                                return;
                            }
                        }
                    }
                    ViewGroup viewGroup = (ViewGroup) view;
                    for (int i4 = 0; i4 < viewGroup.getChildCount(); i4++) {
                        c(viewGroup.getChildAt(i4), z2);
                    }
                }
            }
        }
    }

    private static android.support.v4.h.a<Animator, a> o() {
        android.support.v4.h.a<Animator, a> aVar = (android.support.v4.h.a) z.get();
        if (aVar != null) {
            return aVar;
        }
        android.support.v4.h.a<Animator, a> aVar2 = new android.support.v4.h.a<>();
        z.set(aVar2);
        return aVar2;
    }

    public Animator a(ViewGroup viewGroup, aa aaVar, aa aaVar2) {
        return null;
    }

    public aa a(View view, boolean z2) {
        if (this.d != null) {
            return this.d.a(view, z2);
        }
        return (aa) (z2 ? this.u : this.v).f451a.get(view);
    }

    public u a(long j2) {
        this.f502a = j2;
        return this;
    }

    public u a(TimeInterpolator timeInterpolator) {
        this.k = timeInterpolator;
        return this;
    }

    public u a(c cVar) {
        if (this.F == null) {
            this.F = new ArrayList<>();
        }
        this.F.add(cVar);
        return this;
    }

    /* access modifiers changed from: 0000 */
    public String a(String str) {
        String str2;
        String str3 = str + getClass().getSimpleName() + "@" + Integer.toHexString(hashCode()) + ": ";
        if (this.f502a != -1) {
            str3 = str3 + "dur(" + this.f502a + ") ";
        }
        if (this.j != -1) {
            str3 = str3 + "dly(" + this.j + ") ";
        }
        if (this.k != null) {
            str3 = str3 + "interp(" + this.k + ") ";
        }
        if (this.f503b.size() <= 0 && this.c.size() <= 0) {
            return str3;
        }
        String str4 = str3 + "tgts(";
        if (this.f503b.size() > 0) {
            str2 = str4;
            for (int i2 = 0; i2 < this.f503b.size(); i2++) {
                if (i2 > 0) {
                    str2 = str2 + ", ";
                }
                str2 = str2 + this.f503b.get(i2);
            }
        } else {
            str2 = str4;
        }
        if (this.c.size() > 0) {
            for (int i3 = 0; i3 < this.c.size(); i3++) {
                if (i3 > 0) {
                    str2 = str2 + ", ";
                }
                str2 = str2 + this.c.get(i3);
            }
        }
        return str2 + ")";
    }

    /* access modifiers changed from: protected */
    public void a(Animator animator) {
        if (animator == null) {
            k();
            return;
        }
        if (b() >= 0) {
            animator.setDuration(b());
        }
        if (c() >= 0) {
            animator.setStartDelay(c());
        }
        if (d() != null) {
            animator.setInterpolator(d());
        }
        animator.addListener(new AnimatorListenerAdapter() {
            public void onAnimationEnd(Animator animator) {
                u.this.k();
                animator.removeListener(this);
            }
        });
        animator.start();
    }

    public abstract void a(aa aaVar);

    public void a(b bVar) {
        this.H = bVar;
    }

    /* access modifiers changed from: 0000 */
    public void a(ViewGroup viewGroup) {
        this.x = new ArrayList<>();
        this.y = new ArrayList<>();
        a(this.u, this.v);
        android.support.v4.h.a o2 = o();
        int size = o2.size();
        aw b2 = am.b(viewGroup);
        for (int i2 = size - 1; i2 >= 0; i2--) {
            Animator animator = (Animator) o2.b(i2);
            if (animator != null) {
                a aVar = (a) o2.get(animator);
                if (!(aVar == null || aVar.f507a == null || !b2.equals(aVar.d))) {
                    aa aaVar = aVar.c;
                    View view = aVar.f507a;
                    aa a2 = a(view, true);
                    aa b3 = b(view, true);
                    if (!(a2 == null && b3 == null) && aVar.e.a(aaVar, b3)) {
                        if (animator.isRunning() || animator.isStarted()) {
                            animator.cancel();
                        } else {
                            o2.remove(animator);
                        }
                    }
                }
            }
        }
        a(viewGroup, this.u, this.v, this.x, this.y);
        e();
    }

    /* access modifiers changed from: protected */
    public void a(ViewGroup viewGroup, ab abVar, ab abVar2, ArrayList<aa> arrayList, ArrayList<aa> arrayList2) {
        View view;
        aa aaVar;
        Animator animator;
        Animator animator2;
        android.support.v4.h.a o2 = o();
        long j2 = Long.MAX_VALUE;
        SparseIntArray sparseIntArray = new SparseIntArray();
        int size = arrayList.size();
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= size) {
                break;
            }
            aa aaVar2 = (aa) arrayList.get(i3);
            aa aaVar3 = (aa) arrayList2.get(i3);
            aa aaVar4 = (aaVar2 == null || aaVar2.c.contains(this)) ? aaVar2 : null;
            aa aaVar5 = (aaVar3 == null || aaVar3.c.contains(this)) ? aaVar3 : null;
            if (aaVar4 != null || aaVar5 != null) {
                if (aaVar4 == null || aaVar5 == null || a(aaVar4, aaVar5)) {
                    Animator a2 = a(viewGroup, aaVar4, aaVar5);
                    if (a2 != null) {
                        aa aaVar6 = null;
                        if (aaVar5 != null) {
                            View view2 = aaVar5.f450b;
                            String[] a3 = a();
                            if (view2 != null && a3 != null && a3.length > 0) {
                                aa aaVar7 = new aa();
                                aaVar7.f450b = view2;
                                aa aaVar8 = (aa) abVar2.f451a.get(view2);
                                if (aaVar8 != null) {
                                    for (int i4 = 0; i4 < a3.length; i4++) {
                                        aaVar7.f449a.put(a3[i4], aaVar8.f449a.get(a3[i4]));
                                    }
                                }
                                int size2 = o2.size();
                                int i5 = 0;
                                while (true) {
                                    if (i5 >= size2) {
                                        aaVar6 = aaVar7;
                                        animator2 = a2;
                                        break;
                                    }
                                    a aVar = (a) o2.get((Animator) o2.b(i5));
                                    if (aVar.c != null && aVar.f507a == view2 && aVar.f508b.equals(n()) && aVar.c.equals(aaVar7)) {
                                        animator2 = null;
                                        aaVar6 = aaVar7;
                                        break;
                                    }
                                    i5++;
                                }
                            } else {
                                animator2 = a2;
                            }
                            aaVar = aaVar6;
                            animator = animator2;
                            view = view2;
                        } else {
                            view = aaVar4.f450b;
                            aaVar = null;
                            animator = a2;
                        }
                        if (animator != null) {
                            if (this.f != null) {
                                long a4 = this.f.a(viewGroup, this, aaVar4, aaVar5);
                                sparseIntArray.put(this.G.size(), (int) a4);
                                j2 = Math.min(a4, j2);
                            }
                            o2.put(animator, new a(view, n(), this, am.b(viewGroup), aaVar));
                            this.G.add(animator);
                        }
                    }
                }
            }
            i2 = i3 + 1;
        }
        if (j2 != 0) {
            int i6 = 0;
            while (true) {
                int i7 = i6;
                if (i7 < sparseIntArray.size()) {
                    Animator animator3 = (Animator) this.G.get(sparseIntArray.keyAt(i7));
                    animator3.setStartDelay((((long) sparseIntArray.valueAt(i7)) - j2) + animator3.getStartDelay());
                    i6 = i7 + 1;
                } else {
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(ViewGroup viewGroup, boolean z2) {
        a(z2);
        if ((this.f503b.size() > 0 || this.c.size() > 0) && ((this.l == null || this.l.isEmpty()) && (this.m == null || this.m.isEmpty()))) {
            for (int i2 = 0; i2 < this.f503b.size(); i2++) {
                View findViewById = viewGroup.findViewById(((Integer) this.f503b.get(i2)).intValue());
                if (findViewById != null) {
                    aa aaVar = new aa();
                    aaVar.f450b = findViewById;
                    if (z2) {
                        a(aaVar);
                    } else {
                        b(aaVar);
                    }
                    aaVar.c.add(this);
                    c(aaVar);
                    if (z2) {
                        a(this.u, findViewById, aaVar);
                    } else {
                        a(this.v, findViewById, aaVar);
                    }
                }
            }
            for (int i3 = 0; i3 < this.c.size(); i3++) {
                View view = (View) this.c.get(i3);
                aa aaVar2 = new aa();
                aaVar2.f450b = view;
                if (z2) {
                    a(aaVar2);
                } else {
                    b(aaVar2);
                }
                aaVar2.c.add(this);
                c(aaVar2);
                if (z2) {
                    a(this.u, view, aaVar2);
                } else {
                    a(this.v, view, aaVar2);
                }
            }
        } else {
            c(viewGroup, z2);
        }
        if (!z2 && this.I != null) {
            int size = this.I.size();
            ArrayList arrayList = new ArrayList(size);
            for (int i4 = 0; i4 < size; i4++) {
                arrayList.add(this.u.d.remove((String) this.I.b(i4)));
            }
            for (int i5 = 0; i5 < size; i5++) {
                View view2 = (View) arrayList.get(i5);
                if (view2 != null) {
                    this.u.d.put((String) this.I.c(i5), view2);
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(boolean z2) {
        if (z2) {
            this.u.f451a.clear();
            this.u.f452b.clear();
            this.u.c.c();
            return;
        }
        this.v.f451a.clear();
        this.v.f452b.clear();
        this.v.c.c();
    }

    public boolean a(aa aaVar, aa aaVar2) {
        boolean z2;
        if (aaVar == null || aaVar2 == null) {
            return false;
        }
        String[] a2 = a();
        if (a2 != null) {
            int length = a2.length;
            int i2 = 0;
            while (true) {
                if (i2 >= length) {
                    z2 = false;
                    break;
                } else if (a(aaVar, aaVar2, a2[i2])) {
                    z2 = true;
                    break;
                } else {
                    i2++;
                }
            }
            return z2;
        }
        for (String a3 : aaVar.f449a.keySet()) {
            if (a(aaVar, aaVar2, a3)) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: 0000 */
    public boolean a(View view) {
        int id = view.getId();
        if (this.n != null && this.n.contains(Integer.valueOf(id))) {
            return false;
        }
        if (this.o != null && this.o.contains(view)) {
            return false;
        }
        if (this.p != null) {
            int size = this.p.size();
            for (int i2 = 0; i2 < size; i2++) {
                if (((Class) this.p.get(i2)).isInstance(view)) {
                    return false;
                }
            }
        }
        if (this.q != null && t.l(view) != null && this.q.contains(t.l(view))) {
            return false;
        }
        if (this.f503b.size() == 0 && this.c.size() == 0 && ((this.m == null || this.m.isEmpty()) && (this.l == null || this.l.isEmpty()))) {
            return true;
        }
        if (this.f503b.contains(Integer.valueOf(id)) || this.c.contains(view)) {
            return true;
        }
        if (this.l != null && this.l.contains(t.l(view))) {
            return true;
        }
        if (this.m == null) {
            return false;
        }
        for (int i3 = 0; i3 < this.m.size(); i3++) {
            if (((Class) this.m.get(i3)).isInstance(view)) {
                return true;
            }
        }
        return false;
    }

    public String[] a() {
        return null;
    }

    public long b() {
        return this.f502a;
    }

    /* access modifiers changed from: 0000 */
    public aa b(View view, boolean z2) {
        aa aaVar;
        if (this.d != null) {
            return this.d.b(view, z2);
        }
        ArrayList<aa> arrayList = z2 ? this.x : this.y;
        if (arrayList == null) {
            return null;
        }
        int size = arrayList.size();
        int i2 = 0;
        while (true) {
            if (i2 >= size) {
                i2 = -1;
                break;
            }
            aa aaVar2 = (aa) arrayList.get(i2);
            if (aaVar2 == null) {
                return null;
            }
            if (aaVar2.f450b == view) {
                break;
            }
            i2++;
        }
        if (i2 >= 0) {
            aaVar = (aa) (z2 ? this.y : this.x).get(i2);
        } else {
            aaVar = null;
        }
        return aaVar;
    }

    public u b(long j2) {
        this.j = j2;
        return this;
    }

    public u b(c cVar) {
        if (this.F != null) {
            this.F.remove(cVar);
            if (this.F.size() == 0) {
                this.F = null;
            }
        }
        return this;
    }

    public u b(View view) {
        this.c.add(view);
        return this;
    }

    public abstract void b(aa aaVar);

    public long c() {
        return this.j;
    }

    public u c(View view) {
        this.c.remove(view);
        return this;
    }

    /* access modifiers changed from: 0000 */
    public void c(aa aaVar) {
        boolean z2 = false;
        if (this.f != null && !aaVar.f449a.isEmpty()) {
            String[] a2 = this.f.a();
            if (a2 != null) {
                int i2 = 0;
                while (true) {
                    if (i2 >= a2.length) {
                        z2 = true;
                        break;
                    } else if (!aaVar.f449a.containsKey(a2[i2])) {
                        break;
                    } else {
                        i2++;
                    }
                }
                if (!z2) {
                    this.f.a(aaVar);
                }
            }
        }
    }

    public TimeInterpolator d() {
        return this.k;
    }

    public void d(View view) {
        if (!this.E) {
            android.support.v4.h.a o2 = o();
            int size = o2.size();
            aw b2 = am.b(view);
            for (int i2 = size - 1; i2 >= 0; i2--) {
                a aVar = (a) o2.c(i2);
                if (aVar.f507a != null && b2.equals(aVar.d)) {
                    a.a((Animator) o2.b(i2));
                }
            }
            if (this.F != null && this.F.size() > 0) {
                ArrayList arrayList = (ArrayList) this.F.clone();
                int size2 = arrayList.size();
                for (int i3 = 0; i3 < size2; i3++) {
                    ((c) arrayList.get(i3)).b(this);
                }
            }
            this.D = true;
        }
    }

    /* access modifiers changed from: protected */
    public void e() {
        j();
        android.support.v4.h.a o2 = o();
        Iterator it = this.G.iterator();
        while (it.hasNext()) {
            Animator animator = (Animator) it.next();
            if (o2.containsKey(animator)) {
                j();
                a(animator, o2);
            }
        }
        this.G.clear();
        k();
    }

    public void e(View view) {
        if (this.D) {
            if (!this.E) {
                android.support.v4.h.a o2 = o();
                int size = o2.size();
                aw b2 = am.b(view);
                for (int i2 = size - 1; i2 >= 0; i2--) {
                    a aVar = (a) o2.c(i2);
                    if (aVar.f507a != null && b2.equals(aVar.d)) {
                        a.b((Animator) o2.b(i2));
                    }
                }
                if (this.F != null && this.F.size() > 0) {
                    ArrayList arrayList = (ArrayList) this.F.clone();
                    int size2 = arrayList.size();
                    for (int i3 = 0; i3 < size2; i3++) {
                        ((c) arrayList.get(i3)).c(this);
                    }
                }
            }
            this.D = false;
        }
    }

    public List<Integer> f() {
        return this.f503b;
    }

    public List<View> g() {
        return this.c;
    }

    public List<String> h() {
        return this.l;
    }

    public List<Class> i() {
        return this.m;
    }

    /* access modifiers changed from: protected */
    public void j() {
        if (this.C == 0) {
            if (this.F != null && this.F.size() > 0) {
                ArrayList arrayList = (ArrayList) this.F.clone();
                int size = arrayList.size();
                for (int i2 = 0; i2 < size; i2++) {
                    ((c) arrayList.get(i2)).d(this);
                }
            }
            this.E = false;
        }
        this.C++;
    }

    /* access modifiers changed from: protected */
    public void k() {
        this.C--;
        if (this.C == 0) {
            if (this.F != null && this.F.size() > 0) {
                ArrayList arrayList = (ArrayList) this.F.clone();
                int size = arrayList.size();
                for (int i2 = 0; i2 < size; i2++) {
                    ((c) arrayList.get(i2)).a(this);
                }
            }
            for (int i3 = 0; i3 < this.u.c.b(); i3++) {
                View view = (View) this.u.c.c(i3);
                if (view != null) {
                    t.a(view, false);
                }
            }
            for (int i4 = 0; i4 < this.v.c.b(); i4++) {
                View view2 = (View) this.v.c.c(i4);
                if (view2 != null) {
                    t.a(view2, false);
                }
            }
            this.E = true;
        }
    }

    public l l() {
        return this.J;
    }

    /* renamed from: m */
    public u clone() {
        try {
            u uVar = (u) super.clone();
            uVar.G = new ArrayList<>();
            uVar.u = new ab();
            uVar.v = new ab();
            uVar.x = null;
            uVar.y = null;
            return uVar;
        } catch (CloneNotSupportedException e2) {
            return null;
        }
    }

    public String n() {
        return this.i;
    }

    public String toString() {
        return a("");
    }
}
