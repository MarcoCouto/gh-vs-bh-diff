package android.support.transition;

import android.support.transition.r.a;
import android.view.View;
import android.view.ViewGroup;

public class t {

    /* renamed from: a reason: collision with root package name */
    private ViewGroup f500a;

    /* renamed from: b reason: collision with root package name */
    private Runnable f501b;

    static t a(View view) {
        return (t) view.getTag(a.transition_current_scene);
    }

    static void a(View view, t tVar) {
        view.setTag(a.transition_current_scene, tVar);
    }

    public void a() {
        if (a(this.f500a) == this && this.f501b != null) {
            this.f501b.run();
        }
    }
}
