package android.support.transition;

import android.os.IBinder;

class au implements aw {

    /* renamed from: a reason: collision with root package name */
    private final IBinder f478a;

    au(IBinder iBinder) {
        this.f478a = iBinder;
    }

    public boolean equals(Object obj) {
        return (obj instanceof au) && ((au) obj).f478a.equals(this.f478a);
    }

    public int hashCode() {
        return this.f478a.hashCode();
    }
}
