package android.support.transition;

import android.view.View;
import android.view.WindowId;

class av implements aw {

    /* renamed from: a reason: collision with root package name */
    private final WindowId f479a;

    av(View view) {
        this.f479a = view.getWindowId();
    }

    public boolean equals(Object obj) {
        return (obj instanceof av) && ((av) obj).f479a.equals(this.f479a);
    }

    public int hashCode() {
        return this.f479a.hashCode();
    }
}
