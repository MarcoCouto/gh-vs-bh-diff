package android.support.transition;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewOverlay;

class ak implements al {

    /* renamed from: a reason: collision with root package name */
    private final ViewOverlay f463a;

    ak(View view) {
        this.f463a = view.getOverlay();
    }

    public void a(Drawable drawable) {
        this.f463a.add(drawable);
    }

    public void b(Drawable drawable) {
        this.f463a.remove(drawable);
    }
}
