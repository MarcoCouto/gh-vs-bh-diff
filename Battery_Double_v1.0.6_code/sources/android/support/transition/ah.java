package android.support.transition;

import android.util.Log;
import android.view.ViewGroup;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

class ah extends ag {

    /* renamed from: a reason: collision with root package name */
    private static Method f458a;

    /* renamed from: b reason: collision with root package name */
    private static boolean f459b;

    ah() {
    }

    private void a() {
        if (!f459b) {
            try {
                f458a = ViewGroup.class.getDeclaredMethod("suppressLayout", new Class[]{Boolean.TYPE});
                f458a.setAccessible(true);
            } catch (NoSuchMethodException e) {
                Log.i("ViewUtilsApi18", "Failed to retrieve suppressLayout method", e);
            }
            f459b = true;
        }
    }

    public ae a(ViewGroup viewGroup) {
        return new ad(viewGroup);
    }

    public void a(ViewGroup viewGroup, boolean z) {
        a();
        if (f458a != null) {
            try {
                f458a.invoke(viewGroup, new Object[]{Boolean.valueOf(z)});
            } catch (IllegalAccessException e) {
                Log.i("ViewUtilsApi18", "Failed to invoke suppressLayout method", e);
            } catch (InvocationTargetException e2) {
                Log.i("ViewUtilsApi18", "Error invoking suppressLayout method", e2);
            }
        }
    }
}
