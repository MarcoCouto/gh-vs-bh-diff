package android.support.transition;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.support.transition.u.c;
import android.view.View;
import android.view.ViewGroup;

public abstract class at extends u {
    private static final String[] g = {"android:visibility:visibility", "android:visibility:parent"};
    private int h = 3;

    private static class a extends AnimatorListenerAdapter implements a, c {

        /* renamed from: a reason: collision with root package name */
        boolean f474a = false;

        /* renamed from: b reason: collision with root package name */
        private final View f475b;
        private final int c;
        private final ViewGroup d;
        private final boolean e;
        private boolean f;

        a(View view, int i, boolean z) {
            this.f475b = view;
            this.c = i;
            this.d = (ViewGroup) view.getParent();
            this.e = z;
            a(true);
        }

        private void a() {
            if (!this.f474a) {
                am.a(this.f475b, this.c);
                if (this.d != null) {
                    this.d.invalidate();
                }
            }
            a(false);
        }

        private void a(boolean z) {
            if (this.e && this.f != z && this.d != null) {
                this.f = z;
                af.a(this.d, z);
            }
        }

        public void a(u uVar) {
            a();
            uVar.b((c) this);
        }

        public void b(u uVar) {
            a(false);
        }

        public void c(u uVar) {
            a(true);
        }

        public void d(u uVar) {
        }

        public void onAnimationCancel(Animator animator) {
            this.f474a = true;
        }

        public void onAnimationEnd(Animator animator) {
            a();
        }

        public void onAnimationPause(Animator animator) {
            if (!this.f474a) {
                am.a(this.f475b, this.c);
            }
        }

        public void onAnimationRepeat(Animator animator) {
        }

        public void onAnimationResume(Animator animator) {
            if (!this.f474a) {
                am.a(this.f475b, 0);
            }
        }

        public void onAnimationStart(Animator animator) {
        }
    }

    private static class b {

        /* renamed from: a reason: collision with root package name */
        boolean f476a;

        /* renamed from: b reason: collision with root package name */
        boolean f477b;
        int c;
        int d;
        ViewGroup e;
        ViewGroup f;

        private b() {
        }
    }

    private b b(aa aaVar, aa aaVar2) {
        b bVar = new b();
        bVar.f476a = false;
        bVar.f477b = false;
        if (aaVar == null || !aaVar.f449a.containsKey("android:visibility:visibility")) {
            bVar.c = -1;
            bVar.e = null;
        } else {
            bVar.c = ((Integer) aaVar.f449a.get("android:visibility:visibility")).intValue();
            bVar.e = (ViewGroup) aaVar.f449a.get("android:visibility:parent");
        }
        if (aaVar2 == null || !aaVar2.f449a.containsKey("android:visibility:visibility")) {
            bVar.d = -1;
            bVar.f = null;
        } else {
            bVar.d = ((Integer) aaVar2.f449a.get("android:visibility:visibility")).intValue();
            bVar.f = (ViewGroup) aaVar2.f449a.get("android:visibility:parent");
        }
        if (aaVar == null || aaVar2 == null) {
            if (aaVar == null && bVar.d == 0) {
                bVar.f477b = true;
                bVar.f476a = true;
            } else if (aaVar2 == null && bVar.c == 0) {
                bVar.f477b = false;
                bVar.f476a = true;
            }
        } else if (bVar.c == bVar.d && bVar.e == bVar.f) {
            return bVar;
        } else {
            if (bVar.c != bVar.d) {
                if (bVar.c == 0) {
                    bVar.f477b = false;
                    bVar.f476a = true;
                } else if (bVar.d == 0) {
                    bVar.f477b = true;
                    bVar.f476a = true;
                }
            } else if (bVar.f == null) {
                bVar.f477b = false;
                bVar.f476a = true;
            } else if (bVar.e == null) {
                bVar.f477b = true;
                bVar.f476a = true;
            }
        }
        return bVar;
    }

    private void d(aa aaVar) {
        aaVar.f449a.put("android:visibility:visibility", Integer.valueOf(aaVar.f450b.getVisibility()));
        aaVar.f449a.put("android:visibility:parent", aaVar.f450b.getParent());
        int[] iArr = new int[2];
        aaVar.f450b.getLocationOnScreen(iArr);
        aaVar.f449a.put("android:visibility:screenLocation", iArr);
    }

    public Animator a(ViewGroup viewGroup, aa aaVar, int i, aa aaVar2, int i2) {
        if ((this.h & 1) != 1 || aaVar2 == null) {
            return null;
        }
        if (aaVar == null) {
            View view = (View) aaVar2.f450b.getParent();
            if (b(b(view, false), a(view, false)).f476a) {
                return null;
            }
        }
        return a(viewGroup, aaVar2.f450b, aaVar, aaVar2);
    }

    public Animator a(ViewGroup viewGroup, aa aaVar, aa aaVar2) {
        b b2 = b(aaVar, aaVar2);
        if (!b2.f476a || (b2.e == null && b2.f == null)) {
            return null;
        }
        if (b2.f477b) {
            return a(viewGroup, aaVar, b2.c, aaVar2, b2.d);
        }
        return b(viewGroup, aaVar, b2.c, aaVar2, b2.d);
    }

    public Animator a(ViewGroup viewGroup, View view, aa aaVar, aa aaVar2) {
        return null;
    }

    public void a(int i) {
        if ((i & -4) != 0) {
            throw new IllegalArgumentException("Only MODE_IN and MODE_OUT flags are allowed");
        }
        this.h = i;
    }

    public void a(aa aaVar) {
        d(aaVar);
    }

    public boolean a(aa aaVar, aa aaVar2) {
        if (aaVar == null && aaVar2 == null) {
            return false;
        }
        if (aaVar != null && aaVar2 != null && aaVar2.f449a.containsKey("android:visibility:visibility") != aaVar.f449a.containsKey("android:visibility:visibility")) {
            return false;
        }
        b b2 = b(aaVar, aaVar2);
        if (b2.f476a) {
            return b2.c == 0 || b2.d == 0;
        }
        return false;
    }

    public String[] a() {
        return g;
    }

    public Animator b(ViewGroup viewGroup, aa aaVar, int i, aa aaVar2, int i2) {
        View view;
        Animator animator = null;
        if ((this.h & 2) == 2) {
            final View view2 = aaVar != null ? aaVar.f450b : null;
            View view3 = aaVar2 != null ? aaVar2.f450b : null;
            if (view3 == null || view3.getParent() == null) {
                if (view3 != null) {
                    view2 = view3;
                    view3 = null;
                } else {
                    if (view2 != null) {
                        if (view2.getParent() == null) {
                            view3 = null;
                        } else if (view2.getParent() instanceof View) {
                            View view4 = (View) view2.getParent();
                            if (!b(a(view4, true), b(view4, true)).f476a) {
                                view = z.a(viewGroup, view2, view4);
                            } else {
                                if (view4.getParent() == null) {
                                    int id = view4.getId();
                                    if (!(id == -1 || viewGroup.findViewById(id) == null || !this.e)) {
                                        view = view2;
                                    }
                                }
                                view = null;
                            }
                            view2 = view;
                            view3 = null;
                        }
                    }
                    view3 = null;
                    view2 = null;
                }
            } else if (i2 == 4) {
                view2 = null;
            } else if (view2 == view3) {
                view2 = null;
            } else {
                view3 = null;
            }
            if (view2 != null && aaVar != null) {
                int[] iArr = (int[]) aaVar.f449a.get("android:visibility:screenLocation");
                int i3 = iArr[0];
                int i4 = iArr[1];
                int[] iArr2 = new int[2];
                viewGroup.getLocationOnScreen(iArr2);
                view2.offsetLeftAndRight((i3 - iArr2[0]) - view2.getLeft());
                view2.offsetTopAndBottom((i4 - iArr2[1]) - view2.getTop());
                final ae a2 = af.a(viewGroup);
                a2.a(view2);
                animator = b(viewGroup, view2, aaVar, aaVar2);
                if (animator == null) {
                    a2.b(view2);
                } else {
                    animator.addListener(new AnimatorListenerAdapter() {
                        public void onAnimationEnd(Animator animator) {
                            a2.b(view2);
                        }
                    });
                }
            } else if (view3 != null) {
                int visibility = view3.getVisibility();
                am.a(view3, 0);
                animator = b(viewGroup, view3, aaVar, aaVar2);
                if (animator != null) {
                    a aVar = new a(view3, i2, true);
                    animator.addListener(aVar);
                    a.a(animator, aVar);
                    a((c) aVar);
                } else {
                    am.a(view3, visibility);
                }
            }
        }
        return animator;
    }

    public Animator b(ViewGroup viewGroup, View view, aa aaVar, aa aaVar2) {
        return null;
    }

    public void b(aa aaVar) {
        d(aaVar);
    }
}
