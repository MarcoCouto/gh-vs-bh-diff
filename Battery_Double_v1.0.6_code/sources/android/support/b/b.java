package android.support.b;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.RemoteException;
import android.support.b.f.a;
import android.text.TextUtils;

public class b {

    /* renamed from: a reason: collision with root package name */
    private final g f247a;

    /* renamed from: b reason: collision with root package name */
    private final ComponentName f248b;

    b(g gVar, ComponentName componentName) {
        this.f247a = gVar;
        this.f248b = componentName;
    }

    public static boolean a(Context context, String str, d dVar) {
        Intent intent = new Intent("android.support.customtabs.action.CustomTabsService");
        if (!TextUtils.isEmpty(str)) {
            intent.setPackage(str);
        }
        return context.bindService(intent, dVar, 33);
    }

    public e a(final a aVar) {
        AnonymousClass1 r1 = new a() {
            private Handler c = new Handler(Looper.getMainLooper());

            public void a(final int i, final Bundle bundle) {
                if (aVar != null) {
                    this.c.post(new Runnable() {
                        public void run() {
                            aVar.a(i, bundle);
                        }
                    });
                }
            }

            public void a(final Bundle bundle) throws RemoteException {
                if (aVar != null) {
                    this.c.post(new Runnable() {
                        public void run() {
                            aVar.a(bundle);
                        }
                    });
                }
            }

            public void a(final String str, final Bundle bundle) throws RemoteException {
                if (aVar != null) {
                    this.c.post(new Runnable() {
                        public void run() {
                            aVar.a(str, bundle);
                        }
                    });
                }
            }

            public void b(final String str, final Bundle bundle) throws RemoteException {
                if (aVar != null) {
                    this.c.post(new Runnable() {
                        public void run() {
                            aVar.b(str, bundle);
                        }
                    });
                }
            }
        };
        try {
            if (!this.f247a.a((f) r1)) {
                return null;
            }
            return new e(this.f247a, r1, this.f248b);
        } catch (RemoteException e) {
            return null;
        }
    }

    public boolean a(long j) {
        try {
            return this.f247a.a(j);
        } catch (RemoteException e) {
            return false;
        }
    }
}
