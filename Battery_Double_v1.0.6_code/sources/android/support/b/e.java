package android.support.b;

import android.content.ComponentName;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import java.util.List;

public final class e {

    /* renamed from: a reason: collision with root package name */
    private final Object f264a = new Object();

    /* renamed from: b reason: collision with root package name */
    private final g f265b;
    private final f c;
    private final ComponentName d;

    e(g gVar, f fVar, ComponentName componentName) {
        this.f265b = gVar;
        this.c = fVar;
        this.d = componentName;
    }

    /* access modifiers changed from: 0000 */
    public IBinder a() {
        return this.c.asBinder();
    }

    public boolean a(Uri uri, Bundle bundle, List<Bundle> list) {
        try {
            return this.f265b.a(this.c, uri, bundle, list);
        } catch (RemoteException e) {
            return false;
        }
    }

    /* access modifiers changed from: 0000 */
    public ComponentName b() {
        return this.d;
    }
}
