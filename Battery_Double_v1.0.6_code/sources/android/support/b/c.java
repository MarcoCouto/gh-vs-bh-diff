package android.support.b;

import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.a.g;
import java.util.ArrayList;

public final class c {

    /* renamed from: a reason: collision with root package name */
    public final Intent f259a;

    /* renamed from: b reason: collision with root package name */
    public final Bundle f260b;

    public static final class a {

        /* renamed from: a reason: collision with root package name */
        private final Intent f261a;

        /* renamed from: b reason: collision with root package name */
        private ArrayList<Bundle> f262b;
        private Bundle c;
        private ArrayList<Bundle> d;
        private boolean e;

        public a() {
            this(null);
        }

        public a(e eVar) {
            IBinder iBinder = null;
            this.f261a = new Intent("android.intent.action.VIEW");
            this.f262b = null;
            this.c = null;
            this.d = null;
            this.e = true;
            if (eVar != null) {
                this.f261a.setPackage(eVar.b().getPackageName());
            }
            Bundle bundle = new Bundle();
            String str = "android.support.customtabs.extra.SESSION";
            if (eVar != null) {
                iBinder = eVar.a();
            }
            g.a(bundle, str, iBinder);
            this.f261a.putExtras(bundle);
        }

        public c a() {
            if (this.f262b != null) {
                this.f261a.putParcelableArrayListExtra("android.support.customtabs.extra.MENU_ITEMS", this.f262b);
            }
            if (this.d != null) {
                this.f261a.putParcelableArrayListExtra("android.support.customtabs.extra.TOOLBAR_ITEMS", this.d);
            }
            this.f261a.putExtra("android.support.customtabs.extra.EXTRA_ENABLE_INSTANT_APPS", this.e);
            return new c(this.f261a, this.c);
        }
    }

    private c(Intent intent, Bundle bundle) {
        this.f259a = intent;
        this.f260b = bundle;
    }
}
