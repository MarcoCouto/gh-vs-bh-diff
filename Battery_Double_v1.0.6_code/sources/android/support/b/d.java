package android.support.b;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.b.g.a;

public abstract class d implements ServiceConnection {
    public abstract void a(ComponentName componentName, b bVar);

    public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        a(componentName, new b(a.a(iBinder), componentName) {
        });
    }
}
