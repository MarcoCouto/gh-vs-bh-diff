package android.support.d.a;

import android.app.Fragment;
import android.os.Build.VERSION;

public class a {

    /* renamed from: a reason: collision with root package name */
    static final e f290a;

    /* renamed from: android.support.d.a.a$a reason: collision with other inner class name */
    static class C0008a extends d {
        C0008a() {
        }

        public void a(Fragment fragment, boolean z) {
            fragment.setUserVisibleHint(z);
        }
    }

    static class b extends C0008a {
        b() {
        }
    }

    static class c extends b {
        c() {
        }

        public void a(Fragment fragment, boolean z) {
            fragment.setUserVisibleHint(z);
        }
    }

    static class d implements e {
        d() {
        }

        public void a(Fragment fragment, boolean z) {
        }
    }

    interface e {
        void a(Fragment fragment, boolean z);
    }

    static {
        if (VERSION.SDK_INT >= 24) {
            f290a = new c();
        } else if (VERSION.SDK_INT >= 23) {
            f290a = new b();
        } else if (VERSION.SDK_INT >= 15) {
            f290a = new C0008a();
        } else {
            f290a = new d();
        }
    }

    @Deprecated
    public static void a(Fragment fragment, boolean z) {
        fragment.setMenuVisibility(z);
    }

    public static void b(Fragment fragment, boolean z) {
        f290a.a(fragment, z);
    }
}
