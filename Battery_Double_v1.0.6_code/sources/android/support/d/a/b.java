package android.support.d.a;

import android.app.Fragment;
import android.app.Fragment.SavedState;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.i.q;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;

public abstract class b extends q {

    /* renamed from: a reason: collision with root package name */
    private final FragmentManager f291a;

    /* renamed from: b reason: collision with root package name */
    private FragmentTransaction f292b = null;
    private ArrayList<SavedState> c = new ArrayList<>();
    private ArrayList<Fragment> d = new ArrayList<>();
    private Fragment e = null;

    public b(FragmentManager fragmentManager) {
        this.f291a = fragmentManager;
    }

    public abstract Fragment a(int i);

    public Parcelable a() {
        Bundle bundle = null;
        if (this.c.size() > 0) {
            bundle = new Bundle();
            SavedState[] savedStateArr = new SavedState[this.c.size()];
            this.c.toArray(savedStateArr);
            bundle.putParcelableArray("states", savedStateArr);
        }
        Bundle bundle2 = bundle;
        for (int i = 0; i < this.d.size(); i++) {
            Fragment fragment = (Fragment) this.d.get(i);
            if (fragment != null && fragment.isAdded()) {
                if (bundle2 == null) {
                    bundle2 = new Bundle();
                }
                this.f291a.putFragment(bundle2, "f" + i, fragment);
            }
        }
        return bundle2;
    }

    public Object a(ViewGroup viewGroup, int i) {
        if (this.d.size() > i) {
            Fragment fragment = (Fragment) this.d.get(i);
            if (fragment != null) {
                return fragment;
            }
        }
        if (this.f292b == null) {
            this.f292b = this.f291a.beginTransaction();
        }
        Fragment a2 = a(i);
        if (this.c.size() > i) {
            SavedState savedState = (SavedState) this.c.get(i);
            if (savedState != null) {
                a2.setInitialSavedState(savedState);
            }
        }
        while (this.d.size() <= i) {
            this.d.add(null);
        }
        a2.setMenuVisibility(false);
        a.b(a2, false);
        this.d.set(i, a2);
        this.f292b.add(viewGroup.getId(), a2);
        return a2;
    }

    public void a(Parcelable parcelable, ClassLoader classLoader) {
        if (parcelable != null) {
            Bundle bundle = (Bundle) parcelable;
            bundle.setClassLoader(classLoader);
            Parcelable[] parcelableArray = bundle.getParcelableArray("states");
            this.c.clear();
            this.d.clear();
            if (parcelableArray != null) {
                for (Parcelable parcelable2 : parcelableArray) {
                    this.c.add((SavedState) parcelable2);
                }
            }
            for (String str : bundle.keySet()) {
                if (str.startsWith("f")) {
                    int parseInt = Integer.parseInt(str.substring(1));
                    Fragment fragment = this.f291a.getFragment(bundle, str);
                    if (fragment != null) {
                        while (this.d.size() <= parseInt) {
                            this.d.add(null);
                        }
                        a.a(fragment, false);
                        this.d.set(parseInt, fragment);
                    } else {
                        Log.w("FragStatePagerAdapter", "Bad fragment at key " + str);
                    }
                }
            }
        }
    }

    public void a(ViewGroup viewGroup) {
        if (viewGroup.getId() == -1) {
            throw new IllegalStateException("ViewPager with adapter " + this + " requires a view id");
        }
    }

    public void a(ViewGroup viewGroup, int i, Object obj) {
        Fragment fragment = (Fragment) obj;
        if (this.f292b == null) {
            this.f292b = this.f291a.beginTransaction();
        }
        while (this.c.size() <= i) {
            this.c.add(null);
        }
        this.c.set(i, fragment.isAdded() ? this.f291a.saveFragmentInstanceState(fragment) : null);
        this.d.set(i, null);
        this.f292b.remove(fragment);
    }

    public boolean a(View view, Object obj) {
        return ((Fragment) obj).getView() == view;
    }

    public void b(ViewGroup viewGroup) {
        if (this.f292b != null) {
            this.f292b.commitAllowingStateLoss();
            this.f292b = null;
            this.f291a.executePendingTransactions();
        }
    }

    public void b(ViewGroup viewGroup, int i, Object obj) {
        Fragment fragment = (Fragment) obj;
        if (fragment != this.e) {
            if (this.e != null) {
                this.e.setMenuVisibility(false);
                a.b(this.e, false);
            }
            if (fragment != null) {
                fragment.setMenuVisibility(true);
                a.b(fragment, true);
            }
            this.e = fragment;
        }
    }
}
