package android.support.v4.a;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Parcelable;
import android.support.v4.h.m;
import android.util.AttributeSet;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import java.io.FileDescriptor;
import java.io.PrintWriter;

public class l {

    /* renamed from: a reason: collision with root package name */
    private final m<?> f561a;

    private l(m<?> mVar) {
        this.f561a = mVar;
    }

    public static final l a(m<?> mVar) {
        return new l(mVar);
    }

    public i a(String str) {
        return this.f561a.d.b(str);
    }

    public n a() {
        return this.f561a.i();
    }

    public View a(View view, String str, Context context, AttributeSet attributeSet) {
        return this.f561a.d.onCreateView(view, str, context, attributeSet);
    }

    public void a(Configuration configuration) {
        this.f561a.d.a(configuration);
    }

    public void a(Parcelable parcelable, p pVar) {
        this.f561a.d.a(parcelable, pVar);
    }

    public void a(i iVar) {
        this.f561a.d.a((m) this.f561a, (k) this.f561a, iVar);
    }

    public void a(m<String, w> mVar) {
        this.f561a.a(mVar);
    }

    public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        this.f561a.b(str, fileDescriptor, printWriter, strArr);
    }

    public void a(boolean z) {
        this.f561a.d.a(z);
    }

    public boolean a(Menu menu) {
        return this.f561a.d.a(menu);
    }

    public boolean a(Menu menu, MenuInflater menuInflater) {
        return this.f561a.d.a(menu, menuInflater);
    }

    public boolean a(MenuItem menuItem) {
        return this.f561a.d.a(menuItem);
    }

    public void b() {
        this.f561a.d.l();
    }

    public void b(Menu menu) {
        this.f561a.d.b(menu);
    }

    public void b(boolean z) {
        this.f561a.d.b(z);
    }

    public boolean b(MenuItem menuItem) {
        return this.f561a.d.b(menuItem);
    }

    public Parcelable c() {
        return this.f561a.d.k();
    }

    public void c(boolean z) {
        this.f561a.a(z);
    }

    public p d() {
        return this.f561a.d.i();
    }

    public void e() {
        this.f561a.d.m();
    }

    public void f() {
        this.f561a.d.n();
    }

    public void g() {
        this.f561a.d.o();
    }

    public void h() {
        this.f561a.d.p();
    }

    public void i() {
        this.f561a.d.q();
    }

    public void j() {
        this.f561a.d.r();
    }

    public void k() {
        this.f561a.d.s();
    }

    public void l() {
        this.f561a.d.u();
    }

    public void m() {
        this.f561a.d.v();
    }

    public boolean n() {
        return this.f561a.d.f();
    }

    public void o() {
        this.f561a.k();
    }

    public void p() {
        this.f561a.l();
    }

    public void q() {
        this.f561a.m();
    }

    public m<String, w> r() {
        return this.f561a.n();
    }
}
