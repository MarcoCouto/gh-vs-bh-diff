package android.support.v4.a;

import android.view.View;
import android.view.View.OnAttachStateChangeListener;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnPreDrawListener;

class ad implements OnAttachStateChangeListener, OnPreDrawListener {

    /* renamed from: a reason: collision with root package name */
    private final View f531a;

    /* renamed from: b reason: collision with root package name */
    private ViewTreeObserver f532b;
    private final Runnable c;

    private ad(View view, Runnable runnable) {
        this.f531a = view;
        this.f532b = view.getViewTreeObserver();
        this.c = runnable;
    }

    public static ad a(View view, Runnable runnable) {
        ad adVar = new ad(view, runnable);
        view.getViewTreeObserver().addOnPreDrawListener(adVar);
        view.addOnAttachStateChangeListener(adVar);
        return adVar;
    }

    public void a() {
        if (this.f532b.isAlive()) {
            this.f532b.removeOnPreDrawListener(this);
        } else {
            this.f531a.getViewTreeObserver().removeOnPreDrawListener(this);
        }
        this.f531a.removeOnAttachStateChangeListener(this);
    }

    public boolean onPreDraw() {
        a();
        this.c.run();
        return true;
    }

    public void onViewAttachedToWindow(View view) {
        this.f532b = view.getViewTreeObserver();
    }

    public void onViewDetachedFromWindow(View view) {
        a();
    }
}
