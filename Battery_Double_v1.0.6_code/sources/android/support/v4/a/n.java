package android.support.v4.a;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.List;

public abstract class n {

    public static abstract class a {
        public void a(n nVar, i iVar) {
        }

        public void a(n nVar, i iVar, Context context) {
        }

        public void a(n nVar, i iVar, Bundle bundle) {
        }

        public void a(n nVar, i iVar, View view, Bundle bundle) {
        }

        public void b(n nVar, i iVar) {
        }

        public void b(n nVar, i iVar, Context context) {
        }

        public void b(n nVar, i iVar, Bundle bundle) {
        }

        public void c(n nVar, i iVar) {
        }

        public void c(n nVar, i iVar, Bundle bundle) {
        }

        public void d(n nVar, i iVar) {
        }

        public void d(n nVar, i iVar, Bundle bundle) {
        }

        public void e(n nVar, i iVar) {
        }

        public void f(n nVar, i iVar) {
        }

        public void g(n nVar, i iVar) {
        }
    }

    public interface b {
        void a();
    }

    public abstract s a();

    public abstract void a(int i, int i2);

    public abstract void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr);

    public abstract boolean b();

    public abstract List<i> c();

    public abstract boolean d();
}
