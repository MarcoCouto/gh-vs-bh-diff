package android.support.v4.a;

import android.support.v4.h.e;
import android.util.Log;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.lang.reflect.Modifier;
import java.util.ArrayList;

final class c extends s implements f {

    /* renamed from: a reason: collision with root package name */
    final o f540a;

    /* renamed from: b reason: collision with root package name */
    ArrayList<a> f541b = new ArrayList<>();
    int c;
    int d;
    int e;
    int f;
    int g;
    int h;
    boolean i;
    boolean j = true;
    String k;
    boolean l;
    int m = -1;
    int n;
    CharSequence o;
    int p;
    CharSequence q;
    ArrayList<String> r;
    ArrayList<String> s;
    boolean t = false;
    ArrayList<Runnable> u;

    static final class a {

        /* renamed from: a reason: collision with root package name */
        int f542a;

        /* renamed from: b reason: collision with root package name */
        i f543b;
        int c;
        int d;
        int e;
        int f;

        a() {
        }

        a(int i, i iVar) {
            this.f542a = i;
            this.f543b = iVar;
        }
    }

    public c(o oVar) {
        this.f540a = oVar;
    }

    private void a(int i2, i iVar, String str, int i3) {
        Class cls = iVar.getClass();
        int modifiers = cls.getModifiers();
        if (cls.isAnonymousClass() || !Modifier.isPublic(modifiers) || (cls.isMemberClass() && !Modifier.isStatic(modifiers))) {
            throw new IllegalStateException("Fragment " + cls.getCanonicalName() + " must be a public static class to be  properly recreated from" + " instance state.");
        }
        iVar.A = this.f540a;
        if (str != null) {
            if (iVar.H == null || str.equals(iVar.H)) {
                iVar.H = str;
            } else {
                throw new IllegalStateException("Can't change tag of fragment " + iVar + ": was " + iVar.H + " now " + str);
            }
        }
        if (i2 != 0) {
            if (i2 == -1) {
                throw new IllegalArgumentException("Can't add fragment " + iVar + " with tag " + str + " to container view with no id");
            } else if (iVar.F == 0 || iVar.F == i2) {
                iVar.F = i2;
                iVar.G = i2;
            } else {
                throw new IllegalStateException("Can't change container ID of fragment " + iVar + ": was " + iVar.F + " now " + i2);
            }
        }
        a(new a(i3, iVar));
    }

    private static boolean b(a aVar) {
        i iVar = aVar.f543b;
        return iVar != null && iVar.t && iVar.Q != null && !iVar.J && !iVar.I && iVar.V();
    }

    /* access modifiers changed from: 0000 */
    public int a(boolean z) {
        if (this.l) {
            throw new IllegalStateException("commit already called");
        }
        if (o.f564a) {
            Log.v("FragmentManager", "Commit: " + this);
            PrintWriter printWriter = new PrintWriter(new e("FragmentManager"));
            a("  ", (FileDescriptor) null, printWriter, (String[]) null);
            printWriter.close();
        }
        this.l = true;
        if (this.i) {
            this.m = this.f540a.a(this);
        } else {
            this.m = -1;
        }
        this.f540a.a((f) this, z);
        return this.m;
    }

    /* access modifiers changed from: 0000 */
    public i a(ArrayList<i> arrayList, i iVar) {
        boolean z;
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= this.f541b.size()) {
                return iVar;
            }
            a aVar = (a) this.f541b.get(i3);
            switch (aVar.f542a) {
                case 1:
                case 7:
                    arrayList.add(aVar.f543b);
                    break;
                case 2:
                    i iVar2 = aVar.f543b;
                    int i4 = iVar2.G;
                    boolean z2 = false;
                    int size = arrayList.size() - 1;
                    i iVar3 = iVar;
                    int i5 = i3;
                    while (size >= 0) {
                        i iVar4 = (i) arrayList.get(size);
                        if (iVar4.G != i4) {
                            z = z2;
                        } else if (iVar4 == iVar2) {
                            z = true;
                        } else {
                            if (iVar4 == iVar3) {
                                this.f541b.add(i5, new a(9, iVar4));
                                i5++;
                                iVar3 = null;
                            }
                            a aVar2 = new a(3, iVar4);
                            aVar2.c = aVar.c;
                            aVar2.e = aVar.e;
                            aVar2.d = aVar.d;
                            aVar2.f = aVar.f;
                            this.f541b.add(i5, aVar2);
                            arrayList.remove(iVar4);
                            i5++;
                            z = z2;
                        }
                        size--;
                        z2 = z;
                    }
                    if (z2) {
                        this.f541b.remove(i5);
                        i5--;
                    } else {
                        aVar.f542a = 1;
                        arrayList.add(iVar2);
                    }
                    i3 = i5;
                    iVar = iVar3;
                    break;
                case 3:
                case 6:
                    arrayList.remove(aVar.f543b);
                    if (aVar.f543b != iVar) {
                        break;
                    } else {
                        this.f541b.add(i3, new a(9, aVar.f543b));
                        i3++;
                        iVar = null;
                        break;
                    }
                case 8:
                    this.f541b.add(i3, new a(9, iVar));
                    i3++;
                    iVar = aVar.f543b;
                    break;
            }
            i2 = i3 + 1;
        }
    }

    public s a(i iVar) {
        a(new a(3, iVar));
        return this;
    }

    public s a(i iVar, String str) {
        a(0, iVar, str, 1);
        return this;
    }

    public void a() {
        if (this.u != null) {
            int size = this.u.size();
            for (int i2 = 0; i2 < size; i2++) {
                ((Runnable) this.u.get(i2)).run();
            }
            this.u = null;
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(int i2) {
        if (this.i) {
            if (o.f564a) {
                Log.v("FragmentManager", "Bump nesting in " + this + " by " + i2);
            }
            int size = this.f541b.size();
            for (int i3 = 0; i3 < size; i3++) {
                a aVar = (a) this.f541b.get(i3);
                if (aVar.f543b != null) {
                    aVar.f543b.z += i2;
                    if (o.f564a) {
                        Log.v("FragmentManager", "Bump nesting of " + aVar.f543b + " to " + aVar.f543b.z);
                    }
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(a aVar) {
        this.f541b.add(aVar);
        aVar.c = this.c;
        aVar.d = this.d;
        aVar.e = this.e;
        aVar.f = this.f;
    }

    /* access modifiers changed from: 0000 */
    public void a(c cVar) {
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < this.f541b.size()) {
                a aVar = (a) this.f541b.get(i3);
                if (b(aVar)) {
                    aVar.f543b.a(cVar);
                }
                i2 = i3 + 1;
            } else {
                return;
            }
        }
    }

    public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        a(str, printWriter, true);
    }

    public void a(String str, PrintWriter printWriter, boolean z) {
        String str2;
        if (z) {
            printWriter.print(str);
            printWriter.print("mName=");
            printWriter.print(this.k);
            printWriter.print(" mIndex=");
            printWriter.print(this.m);
            printWriter.print(" mCommitted=");
            printWriter.println(this.l);
            if (this.g != 0) {
                printWriter.print(str);
                printWriter.print("mTransition=#");
                printWriter.print(Integer.toHexString(this.g));
                printWriter.print(" mTransitionStyle=#");
                printWriter.println(Integer.toHexString(this.h));
            }
            if (!(this.c == 0 && this.d == 0)) {
                printWriter.print(str);
                printWriter.print("mEnterAnim=#");
                printWriter.print(Integer.toHexString(this.c));
                printWriter.print(" mExitAnim=#");
                printWriter.println(Integer.toHexString(this.d));
            }
            if (!(this.e == 0 && this.f == 0)) {
                printWriter.print(str);
                printWriter.print("mPopEnterAnim=#");
                printWriter.print(Integer.toHexString(this.e));
                printWriter.print(" mPopExitAnim=#");
                printWriter.println(Integer.toHexString(this.f));
            }
            if (!(this.n == 0 && this.o == null)) {
                printWriter.print(str);
                printWriter.print("mBreadCrumbTitleRes=#");
                printWriter.print(Integer.toHexString(this.n));
                printWriter.print(" mBreadCrumbTitleText=");
                printWriter.println(this.o);
            }
            if (!(this.p == 0 && this.q == null)) {
                printWriter.print(str);
                printWriter.print("mBreadCrumbShortTitleRes=#");
                printWriter.print(Integer.toHexString(this.p));
                printWriter.print(" mBreadCrumbShortTitleText=");
                printWriter.println(this.q);
            }
        }
        if (!this.f541b.isEmpty()) {
            printWriter.print(str);
            printWriter.println("Operations:");
            str + "    ";
            int size = this.f541b.size();
            for (int i2 = 0; i2 < size; i2++) {
                a aVar = (a) this.f541b.get(i2);
                switch (aVar.f542a) {
                    case 0:
                        str2 = "NULL";
                        break;
                    case 1:
                        str2 = "ADD";
                        break;
                    case 2:
                        str2 = "REPLACE";
                        break;
                    case 3:
                        str2 = "REMOVE";
                        break;
                    case 4:
                        str2 = "HIDE";
                        break;
                    case 5:
                        str2 = "SHOW";
                        break;
                    case 6:
                        str2 = "DETACH";
                        break;
                    case 7:
                        str2 = "ATTACH";
                        break;
                    case 8:
                        str2 = "SET_PRIMARY_NAV";
                        break;
                    case 9:
                        str2 = "UNSET_PRIMARY_NAV";
                        break;
                    default:
                        str2 = "cmd=" + aVar.f542a;
                        break;
                }
                printWriter.print(str);
                printWriter.print("  Op #");
                printWriter.print(i2);
                printWriter.print(": ");
                printWriter.print(str2);
                printWriter.print(" ");
                printWriter.println(aVar.f543b);
                if (z) {
                    if (!(aVar.c == 0 && aVar.d == 0)) {
                        printWriter.print(str);
                        printWriter.print("enterAnim=#");
                        printWriter.print(Integer.toHexString(aVar.c));
                        printWriter.print(" exitAnim=#");
                        printWriter.println(Integer.toHexString(aVar.d));
                    }
                    if (aVar.e != 0 || aVar.f != 0) {
                        printWriter.print(str);
                        printWriter.print("popEnterAnim=#");
                        printWriter.print(Integer.toHexString(aVar.e));
                        printWriter.print(" popExitAnim=#");
                        printWriter.println(Integer.toHexString(aVar.f));
                    }
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean a(ArrayList<c> arrayList, int i2, int i3) {
        int i4;
        if (i3 == i2) {
            return false;
        }
        int size = this.f541b.size();
        int i5 = -1;
        int i6 = 0;
        while (i6 < size) {
            a aVar = (a) this.f541b.get(i6);
            int i7 = aVar.f543b != null ? aVar.f543b.G : 0;
            if (i7 == 0 || i7 == i5) {
                i4 = i5;
            } else {
                for (int i8 = i2; i8 < i3; i8++) {
                    c cVar = (c) arrayList.get(i8);
                    int size2 = cVar.f541b.size();
                    for (int i9 = 0; i9 < size2; i9++) {
                        a aVar2 = (a) cVar.f541b.get(i9);
                        if ((aVar2.f543b != null ? aVar2.f543b.G : 0) == i7) {
                            return true;
                        }
                    }
                }
                i4 = i7;
            }
            i6++;
            i5 = i4;
        }
        return false;
    }

    public boolean a(ArrayList<c> arrayList, ArrayList<Boolean> arrayList2) {
        if (o.f564a) {
            Log.v("FragmentManager", "Run: " + this);
        }
        arrayList.add(this);
        arrayList2.add(Boolean.valueOf(false));
        if (this.i) {
            this.f540a.b(this);
        }
        return true;
    }

    public int b() {
        return a(false);
    }

    /* access modifiers changed from: 0000 */
    public i b(ArrayList<i> arrayList, i iVar) {
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= this.f541b.size()) {
                return iVar;
            }
            a aVar = (a) this.f541b.get(i3);
            switch (aVar.f542a) {
                case 1:
                case 7:
                    arrayList.remove(aVar.f543b);
                    break;
                case 3:
                case 6:
                    arrayList.add(aVar.f543b);
                    break;
                case 8:
                    iVar = null;
                    break;
                case 9:
                    iVar = aVar.f543b;
                    break;
            }
            i2 = i3 + 1;
        }
    }

    /* access modifiers changed from: 0000 */
    public void b(boolean z) {
        for (int size = this.f541b.size() - 1; size >= 0; size--) {
            a aVar = (a) this.f541b.get(size);
            i iVar = aVar.f543b;
            if (iVar != null) {
                iVar.a(o.d(this.g), this.h);
            }
            switch (aVar.f542a) {
                case 1:
                    iVar.a(aVar.f);
                    this.f540a.h(iVar);
                    break;
                case 3:
                    iVar.a(aVar.e);
                    this.f540a.a(iVar, false);
                    break;
                case 4:
                    iVar.a(aVar.e);
                    this.f540a.j(iVar);
                    break;
                case 5:
                    iVar.a(aVar.f);
                    this.f540a.i(iVar);
                    break;
                case 6:
                    iVar.a(aVar.e);
                    this.f540a.l(iVar);
                    break;
                case 7:
                    iVar.a(aVar.f);
                    this.f540a.k(iVar);
                    break;
                case 8:
                    this.f540a.o(null);
                    break;
                case 9:
                    this.f540a.o(iVar);
                    break;
                default:
                    throw new IllegalArgumentException("Unknown cmd: " + aVar.f542a);
            }
            if (!(this.t || aVar.f542a == 3 || iVar == null)) {
                this.f540a.e(iVar);
            }
        }
        if (!this.t && z) {
            this.f540a.a(this.f540a.l, true);
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean b(int i2) {
        int size = this.f541b.size();
        for (int i3 = 0; i3 < size; i3++) {
            a aVar = (a) this.f541b.get(i3);
            int i4 = aVar.f543b != null ? aVar.f543b.G : 0;
            if (i4 != 0 && i4 == i2) {
                return true;
            }
        }
        return false;
    }

    public int c() {
        return a(true);
    }

    /* access modifiers changed from: 0000 */
    public void d() {
        int size = this.f541b.size();
        for (int i2 = 0; i2 < size; i2++) {
            a aVar = (a) this.f541b.get(i2);
            i iVar = aVar.f543b;
            if (iVar != null) {
                iVar.a(this.g, this.h);
            }
            switch (aVar.f542a) {
                case 1:
                    iVar.a(aVar.c);
                    this.f540a.a(iVar, false);
                    break;
                case 3:
                    iVar.a(aVar.d);
                    this.f540a.h(iVar);
                    break;
                case 4:
                    iVar.a(aVar.d);
                    this.f540a.i(iVar);
                    break;
                case 5:
                    iVar.a(aVar.c);
                    this.f540a.j(iVar);
                    break;
                case 6:
                    iVar.a(aVar.d);
                    this.f540a.k(iVar);
                    break;
                case 7:
                    iVar.a(aVar.c);
                    this.f540a.l(iVar);
                    break;
                case 8:
                    this.f540a.o(iVar);
                    break;
                case 9:
                    this.f540a.o(null);
                    break;
                default:
                    throw new IllegalArgumentException("Unknown cmd: " + aVar.f542a);
            }
            if (!(this.t || aVar.f542a == 1 || iVar == null)) {
                this.f540a.e(iVar);
            }
        }
        if (!this.t) {
            this.f540a.a(this.f540a.l, true);
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean e() {
        for (int i2 = 0; i2 < this.f541b.size(); i2++) {
            if (b((a) this.f541b.get(i2))) {
                return true;
            }
        }
        return false;
    }

    public String f() {
        return this.k;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(128);
        sb.append("BackStackEntry{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        if (this.m >= 0) {
            sb.append(" #");
            sb.append(this.m);
        }
        if (this.k != null) {
            sb.append(" ");
            sb.append(this.k);
        }
        sb.append("}");
        return sb.toString();
    }
}
