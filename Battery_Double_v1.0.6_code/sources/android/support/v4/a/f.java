package android.support.v4.a;

import android.content.Intent;
import android.content.IntentSender;
import android.content.IntentSender.SendIntentException;
import android.os.Bundle;

abstract class f extends e {

    /* renamed from: b reason: collision with root package name */
    boolean f547b;

    f() {
    }

    public void startActivityForResult(Intent intent, int i, Bundle bundle) {
        if (!this.f547b && i != -1) {
            b(i);
        }
        super.startActivityForResult(intent, i, bundle);
    }

    public void startIntentSenderForResult(IntentSender intentSender, int i, Intent intent, int i2, int i3, int i4, Bundle bundle) throws SendIntentException {
        if (!this.f546a && i != -1) {
            b(i);
        }
        super.startIntentSenderForResult(intentSender, i, intent, i2, i3, i4, bundle);
    }
}
