package android.support.v4.a;

import android.os.Bundle;
import android.support.v4.b.b;
import android.support.v4.b.b.C0013b;
import android.support.v4.h.d;
import android.support.v4.h.n;
import android.util.Log;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.lang.reflect.Modifier;

class x extends w {

    /* renamed from: a reason: collision with root package name */
    static boolean f616a = false;

    /* renamed from: b reason: collision with root package name */
    final n<a> f617b = new n<>();
    final n<a> c = new n<>();
    final String d;
    boolean e;
    boolean f;
    m g;

    final class a implements android.support.v4.b.b.a<Object>, C0013b<Object> {

        /* renamed from: a reason: collision with root package name */
        final int f618a;

        /* renamed from: b reason: collision with root package name */
        final Bundle f619b;
        android.support.v4.a.w.a<Object> c;
        b<Object> d;
        boolean e;
        boolean f;
        Object g;
        boolean h;
        boolean i;
        boolean j;
        boolean k;
        boolean l;
        boolean m;
        a n;
        final /* synthetic */ x o;

        /* access modifiers changed from: 0000 */
        public void a() {
            if (this.i && this.j) {
                this.h = true;
            } else if (!this.h) {
                this.h = true;
                if (x.f616a) {
                    Log.v("LoaderManager", "  Starting: " + this);
                }
                if (this.d == null && this.c != null) {
                    this.d = this.c.a(this.f618a, this.f619b);
                }
                if (this.d == null) {
                    return;
                }
                if (!this.d.getClass().isMemberClass() || Modifier.isStatic(this.d.getClass().getModifiers())) {
                    if (!this.m) {
                        this.d.a(this.f618a, this);
                        this.d.a((android.support.v4.b.b.a<D>) this);
                        this.m = true;
                    }
                    this.d.a();
                    return;
                }
                throw new IllegalArgumentException("Object returned from onCreateLoader must not be a non-static inner member class: " + this.d);
            }
        }

        /* access modifiers changed from: 0000 */
        public void a(b<Object> bVar, Object obj) {
            String str;
            if (this.c != null) {
                if (this.o.g != null) {
                    String str2 = this.o.g.d.u;
                    this.o.g.d.u = "onLoadFinished";
                    str = str2;
                } else {
                    str = null;
                }
                try {
                    if (x.f616a) {
                        Log.v("LoaderManager", "  onLoadFinished in " + bVar + ": " + bVar.a(obj));
                    }
                    this.c.a(bVar, obj);
                    this.f = true;
                } finally {
                    if (this.o.g != null) {
                        this.o.g.d.u = str;
                    }
                }
            }
        }

        public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
            printWriter.print(str);
            printWriter.print("mId=");
            printWriter.print(this.f618a);
            printWriter.print(" mArgs=");
            printWriter.println(this.f619b);
            printWriter.print(str);
            printWriter.print("mCallbacks=");
            printWriter.println(this.c);
            printWriter.print(str);
            printWriter.print("mLoader=");
            printWriter.println(this.d);
            if (this.d != null) {
                this.d.a(str + "  ", fileDescriptor, printWriter, strArr);
            }
            if (this.e || this.f) {
                printWriter.print(str);
                printWriter.print("mHaveData=");
                printWriter.print(this.e);
                printWriter.print("  mDeliveredData=");
                printWriter.println(this.f);
                printWriter.print(str);
                printWriter.print("mData=");
                printWriter.println(this.g);
            }
            printWriter.print(str);
            printWriter.print("mStarted=");
            printWriter.print(this.h);
            printWriter.print(" mReportNextStart=");
            printWriter.print(this.k);
            printWriter.print(" mDestroyed=");
            printWriter.println(this.l);
            printWriter.print(str);
            printWriter.print("mRetaining=");
            printWriter.print(this.i);
            printWriter.print(" mRetainingStarted=");
            printWriter.print(this.j);
            printWriter.print(" mListenerRegistered=");
            printWriter.println(this.m);
            if (this.n != null) {
                printWriter.print(str);
                printWriter.println("Pending Loader ");
                printWriter.print(this.n);
                printWriter.println(":");
                this.n.a(str + "  ", fileDescriptor, printWriter, strArr);
            }
        }

        /* access modifiers changed from: 0000 */
        public void b() {
            if (x.f616a) {
                Log.v("LoaderManager", "  Retaining: " + this);
            }
            this.i = true;
            this.j = this.h;
            this.h = false;
            this.c = null;
        }

        /* access modifiers changed from: 0000 */
        public void c() {
            if (this.i) {
                if (x.f616a) {
                    Log.v("LoaderManager", "  Finished Retaining: " + this);
                }
                this.i = false;
                if (this.h != this.j && !this.h) {
                    e();
                }
            }
            if (this.h && this.e && !this.k) {
                a(this.d, this.g);
            }
        }

        /* access modifiers changed from: 0000 */
        public void d() {
            if (this.h && this.k) {
                this.k = false;
                if (this.e && !this.i) {
                    a(this.d, this.g);
                }
            }
        }

        /* access modifiers changed from: 0000 */
        public void e() {
            if (x.f616a) {
                Log.v("LoaderManager", "  Stopping: " + this);
            }
            this.h = false;
            if (!this.i && this.d != null && this.m) {
                this.m = false;
                this.d.a((C0013b<D>) this);
                this.d.b(this);
                this.d.c();
            }
        }

        /* access modifiers changed from: 0000 */
        public void f() {
            String str;
            if (x.f616a) {
                Log.v("LoaderManager", "  Destroying: " + this);
            }
            this.l = true;
            boolean z = this.f;
            this.f = false;
            if (this.c != null && this.d != null && this.e && z) {
                if (x.f616a) {
                    Log.v("LoaderManager", "  Resetting: " + this);
                }
                if (this.o.g != null) {
                    String str2 = this.o.g.d.u;
                    this.o.g.d.u = "onLoaderReset";
                    str = str2;
                } else {
                    str = null;
                }
                try {
                    this.c.a(this.d);
                } finally {
                    if (this.o.g != null) {
                        this.o.g.d.u = str;
                    }
                }
            }
            this.c = null;
            this.g = null;
            this.e = false;
            if (this.d != null) {
                if (this.m) {
                    this.m = false;
                    this.d.a((C0013b<D>) this);
                    this.d.b(this);
                }
                this.d.e();
            }
            if (this.n != null) {
                this.n.f();
            }
        }

        public String toString() {
            StringBuilder sb = new StringBuilder(64);
            sb.append("LoaderInfo{");
            sb.append(Integer.toHexString(System.identityHashCode(this)));
            sb.append(" #");
            sb.append(this.f618a);
            sb.append(" : ");
            d.a(this.d, sb);
            sb.append("}}");
            return sb.toString();
        }
    }

    x(String str, m mVar, boolean z) {
        this.d = str;
        this.g = mVar;
        this.e = z;
    }

    /* access modifiers changed from: 0000 */
    public void a(m mVar) {
        this.g = mVar;
    }

    public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        if (this.f617b.b() > 0) {
            printWriter.print(str);
            printWriter.println("Active Loaders:");
            String str2 = str + "    ";
            for (int i = 0; i < this.f617b.b(); i++) {
                a aVar = (a) this.f617b.e(i);
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(this.f617b.d(i));
                printWriter.print(": ");
                printWriter.println(aVar.toString());
                aVar.a(str2, fileDescriptor, printWriter, strArr);
            }
        }
        if (this.c.b() > 0) {
            printWriter.print(str);
            printWriter.println("Inactive Loaders:");
            String str3 = str + "    ";
            for (int i2 = 0; i2 < this.c.b(); i2++) {
                a aVar2 = (a) this.c.e(i2);
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(this.c.d(i2));
                printWriter.print(": ");
                printWriter.println(aVar2.toString());
                aVar2.a(str3, fileDescriptor, printWriter, strArr);
            }
        }
    }

    public boolean a() {
        boolean z = false;
        for (int i = 0; i < this.f617b.b(); i++) {
            a aVar = (a) this.f617b.e(i);
            z |= aVar.h && !aVar.f;
        }
        return z;
    }

    /* access modifiers changed from: 0000 */
    public void b() {
        if (f616a) {
            Log.v("LoaderManager", "Starting in " + this);
        }
        if (this.e) {
            RuntimeException runtimeException = new RuntimeException("here");
            runtimeException.fillInStackTrace();
            Log.w("LoaderManager", "Called doStart when already started: " + this, runtimeException);
            return;
        }
        this.e = true;
        for (int b2 = this.f617b.b() - 1; b2 >= 0; b2--) {
            ((a) this.f617b.e(b2)).a();
        }
    }

    /* access modifiers changed from: 0000 */
    public void c() {
        if (f616a) {
            Log.v("LoaderManager", "Stopping in " + this);
        }
        if (!this.e) {
            RuntimeException runtimeException = new RuntimeException("here");
            runtimeException.fillInStackTrace();
            Log.w("LoaderManager", "Called doStop when not started: " + this, runtimeException);
            return;
        }
        for (int b2 = this.f617b.b() - 1; b2 >= 0; b2--) {
            ((a) this.f617b.e(b2)).e();
        }
        this.e = false;
    }

    /* access modifiers changed from: 0000 */
    public void d() {
        if (f616a) {
            Log.v("LoaderManager", "Retaining in " + this);
        }
        if (!this.e) {
            RuntimeException runtimeException = new RuntimeException("here");
            runtimeException.fillInStackTrace();
            Log.w("LoaderManager", "Called doRetain when not started: " + this, runtimeException);
            return;
        }
        this.f = true;
        this.e = false;
        for (int b2 = this.f617b.b() - 1; b2 >= 0; b2--) {
            ((a) this.f617b.e(b2)).b();
        }
    }

    /* access modifiers changed from: 0000 */
    public void e() {
        if (this.f) {
            if (f616a) {
                Log.v("LoaderManager", "Finished Retaining in " + this);
            }
            this.f = false;
            for (int b2 = this.f617b.b() - 1; b2 >= 0; b2--) {
                ((a) this.f617b.e(b2)).c();
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void f() {
        for (int b2 = this.f617b.b() - 1; b2 >= 0; b2--) {
            ((a) this.f617b.e(b2)).k = true;
        }
    }

    /* access modifiers changed from: 0000 */
    public void g() {
        for (int b2 = this.f617b.b() - 1; b2 >= 0; b2--) {
            ((a) this.f617b.e(b2)).d();
        }
    }

    /* access modifiers changed from: 0000 */
    public void h() {
        if (!this.f) {
            if (f616a) {
                Log.v("LoaderManager", "Destroying Active in " + this);
            }
            for (int b2 = this.f617b.b() - 1; b2 >= 0; b2--) {
                ((a) this.f617b.e(b2)).f();
            }
            this.f617b.c();
        }
        if (f616a) {
            Log.v("LoaderManager", "Destroying Inactive in " + this);
        }
        for (int b3 = this.c.b() - 1; b3 >= 0; b3--) {
            ((a) this.c.e(b3)).f();
        }
        this.c.c();
        this.g = null;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(128);
        sb.append("LoaderManager{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        sb.append(" in ");
        d.a(this.g, sb);
        sb.append("}}");
        return sb.toString();
    }
}
