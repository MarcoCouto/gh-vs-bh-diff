package android.support.v4.a;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.util.Log;

final class r implements Parcelable {
    public static final Creator<r> CREATOR = new Creator<r>() {
        /* renamed from: a */
        public r createFromParcel(Parcel parcel) {
            return new r(parcel);
        }

        /* renamed from: a */
        public r[] newArray(int i) {
            return new r[i];
        }
    };

    /* renamed from: a reason: collision with root package name */
    final String f589a;

    /* renamed from: b reason: collision with root package name */
    final int f590b;
    final boolean c;
    final int d;
    final int e;
    final String f;
    final boolean g;
    final boolean h;
    final Bundle i;
    final boolean j;
    Bundle k;
    i l;

    r(Parcel parcel) {
        boolean z = true;
        this.f589a = parcel.readString();
        this.f590b = parcel.readInt();
        this.c = parcel.readInt() != 0;
        this.d = parcel.readInt();
        this.e = parcel.readInt();
        this.f = parcel.readString();
        this.g = parcel.readInt() != 0;
        this.h = parcel.readInt() != 0;
        this.i = parcel.readBundle();
        if (parcel.readInt() == 0) {
            z = false;
        }
        this.j = z;
        this.k = parcel.readBundle();
    }

    r(i iVar) {
        this.f589a = iVar.getClass().getName();
        this.f590b = iVar.n;
        this.c = iVar.v;
        this.d = iVar.F;
        this.e = iVar.G;
        this.f = iVar.H;
        this.g = iVar.K;
        this.h = iVar.J;
        this.i = iVar.p;
        this.j = iVar.I;
    }

    public i a(m mVar, k kVar, i iVar, p pVar) {
        if (this.l == null) {
            Context g2 = mVar.g();
            if (this.i != null) {
                this.i.setClassLoader(g2.getClassLoader());
            }
            if (kVar != null) {
                this.l = kVar.a(g2, this.f589a, this.i);
            } else {
                this.l = i.a(g2, this.f589a, this.i);
            }
            if (this.k != null) {
                this.k.setClassLoader(g2.getClassLoader());
                this.l.l = this.k;
            }
            this.l.a(this.f590b, iVar);
            this.l.v = this.c;
            this.l.x = true;
            this.l.F = this.d;
            this.l.G = this.e;
            this.l.H = this.f;
            this.l.K = this.g;
            this.l.J = this.h;
            this.l.I = this.j;
            this.l.A = mVar.d;
            if (o.f564a) {
                Log.v("FragmentManager", "Instantiated fragment " + this.l);
            }
        }
        this.l.D = pVar;
        return this.l;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        int i3 = 1;
        parcel.writeString(this.f589a);
        parcel.writeInt(this.f590b);
        parcel.writeInt(this.c ? 1 : 0);
        parcel.writeInt(this.d);
        parcel.writeInt(this.e);
        parcel.writeString(this.f);
        parcel.writeInt(this.g ? 1 : 0);
        parcel.writeInt(this.h ? 1 : 0);
        parcel.writeBundle(this.i);
        if (!this.j) {
            i3 = 0;
        }
        parcel.writeInt(i3);
        parcel.writeBundle(this.k);
    }
}
