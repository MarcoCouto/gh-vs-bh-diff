package android.support.v4.a;

import android.app.Notification;
import android.app.Notification.Builder;
import android.os.Bundle;
import android.support.v4.a.aa.a;
import android.util.Log;
import android.util.SparseArray;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

class ac {

    /* renamed from: a reason: collision with root package name */
    private static final Object f529a = new Object();

    /* renamed from: b reason: collision with root package name */
    private static Field f530b;
    private static boolean c;
    private static final Object d = new Object();

    public static Bundle a(Builder builder, a aVar) {
        builder.addAction(aVar.a(), aVar.b(), aVar.c());
        Bundle bundle = new Bundle(aVar.d());
        if (aVar.f() != null) {
            bundle.putParcelableArray("android.support.remoteInputs", a(aVar.f()));
        }
        if (aVar.g() != null) {
            bundle.putParcelableArray("android.support.dataRemoteInputs", a(aVar.g()));
        }
        bundle.putBoolean("android.support.allowGeneratedReplies", aVar.e());
        return bundle;
    }

    public static Bundle a(Notification notification) {
        synchronized (f529a) {
            if (c) {
                return null;
            }
            try {
                if (f530b == null) {
                    Field declaredField = Notification.class.getDeclaredField("extras");
                    if (!Bundle.class.isAssignableFrom(declaredField.getType())) {
                        Log.e("NotificationCompat", "Notification.extras field is not of type Bundle");
                        c = true;
                        return null;
                    }
                    declaredField.setAccessible(true);
                    f530b = declaredField;
                }
                Bundle bundle = (Bundle) f530b.get(notification);
                if (bundle == null) {
                    bundle = new Bundle();
                    f530b.set(notification, bundle);
                }
                return bundle;
            } catch (IllegalAccessException e) {
                Log.e("NotificationCompat", "Unable to access notification extras", e);
                c = true;
                return null;
            } catch (NoSuchFieldException e2) {
                Log.e("NotificationCompat", "Unable to access notification extras", e2);
                c = true;
                return null;
            }
        }
    }

    private static Bundle a(ae aeVar) {
        Bundle bundle = new Bundle();
        bundle.putString("resultKey", aeVar.a());
        bundle.putCharSequence("label", aeVar.b());
        bundle.putCharSequenceArray("choices", aeVar.c());
        bundle.putBoolean("allowFreeFormInput", aeVar.e());
        bundle.putBundle("extras", aeVar.f());
        Set<String> d2 = aeVar.d();
        if (d2 != null && !d2.isEmpty()) {
            ArrayList arrayList = new ArrayList(d2.size());
            for (String add : d2) {
                arrayList.add(add);
            }
            bundle.putStringArrayList("allowedDataTypes", arrayList);
        }
        return bundle;
    }

    public static SparseArray<Bundle> a(List<Bundle> list) {
        SparseArray<Bundle> sparseArray = null;
        int size = list.size();
        for (int i = 0; i < size; i++) {
            Bundle bundle = (Bundle) list.get(i);
            if (bundle != null) {
                if (sparseArray == null) {
                    sparseArray = new SparseArray<>();
                }
                sparseArray.put(i, bundle);
            }
        }
        return sparseArray;
    }

    private static Bundle[] a(ae[] aeVarArr) {
        if (aeVarArr == null) {
            return null;
        }
        Bundle[] bundleArr = new Bundle[aeVarArr.length];
        for (int i = 0; i < aeVarArr.length; i++) {
            bundleArr[i] = a(aeVarArr[i]);
        }
        return bundleArr;
    }
}
