package android.support.v4.a;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources.NotFoundException;
import android.content.res.TypedArray;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Looper;
import android.os.Parcelable;
import android.support.v4.h.j;
import android.support.v4.i.t;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater.Factory2;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.ScaleAnimation;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

final class o extends n implements Factory2 {
    static final Interpolator E = new DecelerateInterpolator(2.5f);
    static final Interpolator F = new DecelerateInterpolator(1.5f);
    static final Interpolator G = new AccelerateInterpolator(2.5f);
    static final Interpolator H = new AccelerateInterpolator(1.5f);

    /* renamed from: a reason: collision with root package name */
    static boolean f564a = false;
    static Field q = null;
    SparseArray<Parcelable> A = null;
    ArrayList<h> B;
    p C;
    Runnable D = new Runnable() {
        public void run() {
            o.this.f();
        }
    };
    private final CopyOnWriteArrayList<j<android.support.v4.a.n.a, Boolean>> I = new CopyOnWriteArrayList<>();

    /* renamed from: b reason: collision with root package name */
    ArrayList<f> f565b;
    boolean c;
    int d = 0;
    final ArrayList<i> e = new ArrayList<>();
    SparseArray<i> f;
    ArrayList<c> g;
    ArrayList<i> h;
    ArrayList<c> i;
    ArrayList<Integer> j;
    ArrayList<android.support.v4.a.n.b> k;
    int l = 0;
    m m;
    k n;
    i o;
    i p;
    boolean r;
    boolean s;
    boolean t;
    String u;
    boolean v;
    ArrayList<c> w;
    ArrayList<Boolean> x;
    ArrayList<i> y;
    Bundle z = null;

    private static class a extends b {

        /* renamed from: a reason: collision with root package name */
        View f574a;

        a(View view, AnimationListener animationListener) {
            super(animationListener);
            this.f574a = view;
        }

        public void onAnimationEnd(Animation animation) {
            if (t.y(this.f574a) || VERSION.SDK_INT >= 24) {
                this.f574a.post(new Runnable() {
                    public void run() {
                        a.this.f574a.setLayerType(0, null);
                    }
                });
            } else {
                this.f574a.setLayerType(0, null);
            }
            super.onAnimationEnd(animation);
        }
    }

    private static class b implements AnimationListener {

        /* renamed from: a reason: collision with root package name */
        private final AnimationListener f576a;

        private b(AnimationListener animationListener) {
            this.f576a = animationListener;
        }

        public void onAnimationEnd(Animation animation) {
            if (this.f576a != null) {
                this.f576a.onAnimationEnd(animation);
            }
        }

        public void onAnimationRepeat(Animation animation) {
            if (this.f576a != null) {
                this.f576a.onAnimationRepeat(animation);
            }
        }

        public void onAnimationStart(Animation animation) {
            if (this.f576a != null) {
                this.f576a.onAnimationStart(animation);
            }
        }
    }

    private static class c {

        /* renamed from: a reason: collision with root package name */
        public final Animation f577a;

        /* renamed from: b reason: collision with root package name */
        public final Animator f578b;

        private c(Animator animator) {
            this.f577a = null;
            this.f578b = animator;
            if (animator == null) {
                throw new IllegalStateException("Animator cannot be null");
            }
        }

        private c(Animation animation) {
            this.f577a = animation;
            this.f578b = null;
            if (animation == null) {
                throw new IllegalStateException("Animation cannot be null");
            }
        }
    }

    private static class d extends AnimatorListenerAdapter {

        /* renamed from: a reason: collision with root package name */
        View f579a;

        d(View view) {
            this.f579a = view;
        }

        public void onAnimationEnd(Animator animator) {
            this.f579a.setLayerType(0, null);
            animator.removeListener(this);
        }

        public void onAnimationStart(Animator animator) {
            this.f579a.setLayerType(2, null);
        }
    }

    static class e {

        /* renamed from: a reason: collision with root package name */
        public static final int[] f580a = {16842755, 16842960, 16842961};
    }

    interface f {
        boolean a(ArrayList<c> arrayList, ArrayList<Boolean> arrayList2);
    }

    private class g implements f {

        /* renamed from: a reason: collision with root package name */
        final String f581a;

        /* renamed from: b reason: collision with root package name */
        final int f582b;
        final int c;

        g(String str, int i, int i2) {
            this.f581a = str;
            this.f582b = i;
            this.c = i2;
        }

        public boolean a(ArrayList<c> arrayList, ArrayList<Boolean> arrayList2) {
            if (o.this.p != null && this.f582b < 0 && this.f581a == null) {
                n m = o.this.p.m();
                if (m != null && m.b()) {
                    return false;
                }
            }
            return o.this.a(arrayList, arrayList2, this.f581a, this.f582b, this.c);
        }
    }

    static class h implements c {
        /* access modifiers changed from: private */

        /* renamed from: a reason: collision with root package name */
        public final boolean f583a;
        /* access modifiers changed from: private */

        /* renamed from: b reason: collision with root package name */
        public final c f584b;
        private int c;

        h(c cVar, boolean z) {
            this.f583a = z;
            this.f584b = cVar;
        }

        public void a() {
            this.c--;
            if (this.c == 0) {
                this.f584b.f540a.z();
            }
        }

        public void b() {
            this.c++;
        }

        public boolean c() {
            return this.c == 0;
        }

        public void d() {
            boolean z = false;
            boolean z2 = this.c > 0;
            o oVar = this.f584b.f540a;
            int size = oVar.e.size();
            for (int i = 0; i < size; i++) {
                i iVar = (i) oVar.e.get(i);
                iVar.a((c) null);
                if (z2 && iVar.V()) {
                    iVar.B();
                }
            }
            o oVar2 = this.f584b.f540a;
            c cVar = this.f584b;
            boolean z3 = this.f583a;
            if (!z2) {
                z = true;
            }
            oVar2.a(cVar, z3, z, true);
        }

        public void e() {
            this.f584b.f540a.a(this.f584b, this.f583a, false, false);
        }
    }

    o() {
    }

    private void A() {
        this.c = false;
        this.x.clear();
        this.w.clear();
    }

    private void B() {
        if (this.B != null) {
            while (!this.B.isEmpty()) {
                ((h) this.B.remove(0)).d();
            }
        }
    }

    private void C() {
        int size = this.f == null ? 0 : this.f.size();
        for (int i2 = 0; i2 < size; i2++) {
            i iVar = (i) this.f.valueAt(i2);
            if (iVar != null) {
                if (iVar.S() != null) {
                    int U = iVar.U();
                    View S = iVar.S();
                    Animation animation = S.getAnimation();
                    if (animation != null) {
                        animation.cancel();
                        S.clearAnimation();
                    }
                    iVar.a((View) null);
                    a(iVar, U, 0, 0, false);
                } else if (iVar.T() != null) {
                    iVar.T().end();
                }
            }
        }
    }

    private void D() {
        if (this.f != null) {
            for (int size = this.f.size() - 1; size >= 0; size--) {
                if (this.f.valueAt(size) == null) {
                    this.f.delete(this.f.keyAt(size));
                }
            }
        }
    }

    private int a(ArrayList<c> arrayList, ArrayList<Boolean> arrayList2, int i2, int i3, android.support.v4.h.b<i> bVar) {
        int i4;
        int i5 = i3 - 1;
        int i6 = i3;
        while (i5 >= i2) {
            c cVar = (c) arrayList.get(i5);
            boolean booleanValue = ((Boolean) arrayList2.get(i5)).booleanValue();
            if (cVar.e() && !cVar.a(arrayList, i5 + 1, i3)) {
                if (this.B == null) {
                    this.B = new ArrayList<>();
                }
                h hVar = new h(cVar, booleanValue);
                this.B.add(hVar);
                cVar.a((c) hVar);
                if (booleanValue) {
                    cVar.d();
                } else {
                    cVar.b(false);
                }
                int i7 = i6 - 1;
                if (i5 != i7) {
                    arrayList.remove(i5);
                    arrayList.add(i7, cVar);
                }
                b(bVar);
                i4 = i7;
            } else {
                i4 = i6;
            }
            i5--;
            i6 = i4;
        }
        return i6;
    }

    static c a(Context context, float f2, float f3) {
        AlphaAnimation alphaAnimation = new AlphaAnimation(f2, f3);
        alphaAnimation.setInterpolator(F);
        alphaAnimation.setDuration(220);
        return new c((Animation) alphaAnimation);
    }

    static c a(Context context, float f2, float f3, float f4, float f5) {
        AnimationSet animationSet = new AnimationSet(false);
        ScaleAnimation scaleAnimation = new ScaleAnimation(f2, f3, f2, f3, 1, 0.5f, 1, 0.5f);
        scaleAnimation.setInterpolator(E);
        scaleAnimation.setDuration(220);
        animationSet.addAnimation(scaleAnimation);
        AlphaAnimation alphaAnimation = new AlphaAnimation(f4, f5);
        alphaAnimation.setInterpolator(F);
        alphaAnimation.setDuration(220);
        animationSet.addAnimation(alphaAnimation);
        return new c((Animation) animationSet);
    }

    private static AnimationListener a(Animation animation) {
        try {
            if (q == null) {
                q = Animation.class.getDeclaredField("mListener");
                q.setAccessible(true);
            }
            return (AnimationListener) q.get(animation);
        } catch (NoSuchFieldException e2) {
            Log.e("FragmentManager", "No field with the name mListener is found in Animation class", e2);
            return null;
        } catch (IllegalAccessException e3) {
            Log.e("FragmentManager", "Cannot access Animation's mListener field", e3);
            return null;
        }
    }

    /* access modifiers changed from: private */
    public void a(c cVar, boolean z2, boolean z3, boolean z4) {
        if (z2) {
            cVar.b(z4);
        } else {
            cVar.d();
        }
        ArrayList arrayList = new ArrayList(1);
        ArrayList arrayList2 = new ArrayList(1);
        arrayList.add(cVar);
        arrayList2.add(Boolean.valueOf(z2));
        if (z3) {
            t.a(this, arrayList, arrayList2, 0, 1, true);
        }
        if (z4) {
            a(this.l, true);
        }
        if (this.f != null) {
            int size = this.f.size();
            for (int i2 = 0; i2 < size; i2++) {
                i iVar = (i) this.f.valueAt(i2);
                if (iVar != null && iVar.Q != null && iVar.Y && cVar.b(iVar.G)) {
                    if (iVar.aa > 0.0f) {
                        iVar.Q.setAlpha(iVar.aa);
                    }
                    if (z4) {
                        iVar.aa = 0.0f;
                    } else {
                        iVar.aa = -1.0f;
                        iVar.Y = false;
                    }
                }
            }
        }
    }

    private void a(final i iVar, c cVar, int i2) {
        final View view = iVar.Q;
        final ViewGroup viewGroup = iVar.P;
        viewGroup.startViewTransition(view);
        iVar.b(i2);
        if (cVar.f577a != null) {
            Animation animation = cVar.f577a;
            iVar.a(iVar.Q);
            final i iVar2 = iVar;
            animation.setAnimationListener(new b(a(animation)) {
                public void onAnimationEnd(Animation animation) {
                    super.onAnimationEnd(animation);
                    viewGroup.post(new Runnable() {
                        public void run() {
                            viewGroup.endViewTransition(view);
                            if (iVar2.S() != null) {
                                iVar2.a((View) null);
                                o.this.a(iVar2, iVar2.U(), 0, 0, false);
                            }
                        }
                    });
                }
            });
            b(view, cVar);
            iVar.Q.startAnimation(animation);
            return;
        }
        Animator animator = cVar.f578b;
        iVar.a(cVar.f578b);
        animator.addListener(new AnimatorListenerAdapter() {
            public void onAnimationEnd(Animator animator) {
                viewGroup.endViewTransition(view);
                Animator T = iVar.T();
                iVar.a((Animator) null);
                if (T != null && viewGroup.indexOfChild(view) < 0) {
                    o.this.a(iVar, iVar.U(), 0, 0, false);
                }
            }
        });
        animator.setTarget(iVar.Q);
        b(iVar.Q, cVar);
        animator.start();
    }

    private static void a(p pVar) {
        if (pVar != null) {
            List<i> a2 = pVar.a();
            if (a2 != null) {
                for (i iVar : a2) {
                    iVar.L = true;
                }
            }
            List<p> b2 = pVar.b();
            if (b2 != null) {
                for (p a3 : b2) {
                    a(a3);
                }
            }
        }
    }

    private void a(android.support.v4.h.b<i> bVar) {
        int size = bVar.size();
        for (int i2 = 0; i2 < size; i2++) {
            i iVar = (i) bVar.b(i2);
            if (!iVar.t) {
                View n2 = iVar.n();
                iVar.aa = n2.getAlpha();
                n2.setAlpha(0.0f);
            }
        }
    }

    private void a(RuntimeException runtimeException) {
        Log.e("FragmentManager", runtimeException.getMessage());
        Log.e("FragmentManager", "Activity state:");
        PrintWriter printWriter = new PrintWriter(new android.support.v4.h.e("FragmentManager"));
        if (this.m != null) {
            try {
                this.m.a("  ", (FileDescriptor) null, printWriter, new String[0]);
            } catch (Exception e2) {
                Log.e("FragmentManager", "Failed dumping state", e2);
            }
        } else {
            try {
                a("  ", (FileDescriptor) null, printWriter, new String[0]);
            } catch (Exception e3) {
                Log.e("FragmentManager", "Failed dumping state", e3);
            }
        }
        throw runtimeException;
    }

    private void a(ArrayList<c> arrayList, ArrayList<Boolean> arrayList2) {
        int i2 = 0;
        int size = this.B == null ? 0 : this.B.size();
        while (i2 < size) {
            h hVar = (h) this.B.get(i2);
            if (arrayList != null && !hVar.f583a) {
                int indexOf = arrayList.indexOf(hVar.f584b);
                if (indexOf != -1 && ((Boolean) arrayList2.get(indexOf)).booleanValue()) {
                    hVar.e();
                    i2++;
                    size = size;
                }
            }
            if (hVar.c() || (arrayList != null && hVar.f584b.a(arrayList, 0, arrayList.size()))) {
                this.B.remove(i2);
                i2--;
                size--;
                if (arrayList != null && !hVar.f583a) {
                    int indexOf2 = arrayList.indexOf(hVar.f584b);
                    if (indexOf2 != -1 && ((Boolean) arrayList2.get(indexOf2)).booleanValue()) {
                        hVar.e();
                    }
                }
                hVar.d();
            }
            i2++;
            size = size;
        }
    }

    private void a(ArrayList<c> arrayList, ArrayList<Boolean> arrayList2, int i2, int i3) {
        int i4;
        boolean z2 = ((c) arrayList.get(i2)).t;
        if (this.y == null) {
            this.y = new ArrayList<>();
        } else {
            this.y.clear();
        }
        this.y.addAll(this.e);
        int i5 = i2;
        i w2 = w();
        boolean z3 = false;
        while (i5 < i3) {
            c cVar = (c) arrayList.get(i5);
            i b2 = !((Boolean) arrayList2.get(i5)).booleanValue() ? cVar.a(this.y, w2) : cVar.b(this.y, w2);
            i5++;
            w2 = b2;
            z3 = z3 || cVar.i;
        }
        this.y.clear();
        if (!z2) {
            t.a(this, arrayList, arrayList2, i2, i3, false);
        }
        b(arrayList, arrayList2, i2, i3);
        if (z2) {
            android.support.v4.h.b bVar = new android.support.v4.h.b();
            b(bVar);
            i4 = a(arrayList, arrayList2, i2, i3, bVar);
            a(bVar);
        } else {
            i4 = i3;
        }
        if (i4 != i2 && z2) {
            t.a(this, arrayList, arrayList2, i2, i4, true);
            a(this.l, true);
        }
        while (i2 < i3) {
            c cVar2 = (c) arrayList.get(i2);
            if (((Boolean) arrayList2.get(i2)).booleanValue() && cVar2.m >= 0) {
                c(cVar2.m);
                cVar2.m = -1;
            }
            cVar2.a();
            i2++;
        }
        if (z3) {
            h();
        }
    }

    static boolean a(Animator animator) {
        if (animator == null) {
            return false;
        }
        if (animator instanceof ValueAnimator) {
            PropertyValuesHolder[] values = ((ValueAnimator) animator).getValues();
            for (PropertyValuesHolder propertyName : values) {
                if ("alpha".equals(propertyName.getPropertyName())) {
                    return true;
                }
            }
            return false;
        } else if (!(animator instanceof AnimatorSet)) {
            return false;
        } else {
            ArrayList childAnimations = ((AnimatorSet) animator).getChildAnimations();
            for (int i2 = 0; i2 < childAnimations.size(); i2++) {
                if (a((Animator) childAnimations.get(i2))) {
                    return true;
                }
            }
            return false;
        }
    }

    static boolean a(c cVar) {
        if (cVar.f577a instanceof AlphaAnimation) {
            return true;
        }
        if (!(cVar.f577a instanceof AnimationSet)) {
            return a(cVar.f578b);
        }
        List animations = ((AnimationSet) cVar.f577a).getAnimations();
        for (int i2 = 0; i2 < animations.size(); i2++) {
            if (animations.get(i2) instanceof AlphaAnimation) {
                return true;
            }
        }
        return false;
    }

    static boolean a(View view, c cVar) {
        return view != null && cVar != null && VERSION.SDK_INT >= 19 && view.getLayerType() == 0 && t.p(view) && a(cVar);
    }

    private boolean a(String str, int i2, int i3) {
        f();
        c(true);
        if (this.p != null && i2 < 0 && str == null) {
            n m2 = this.p.m();
            if (m2 != null && m2.b()) {
                return true;
            }
        }
        boolean a2 = a(this.w, this.x, str, i2, i3);
        if (a2) {
            this.c = true;
            try {
                b(this.w, this.x);
            } finally {
                A();
            }
        }
        g();
        D();
        return a2;
    }

    public static int b(int i2, boolean z2) {
        switch (i2) {
            case 4097:
                return z2 ? 1 : 2;
            case 4099:
                return z2 ? 5 : 6;
            case 8194:
                return z2 ? 3 : 4;
            default:
                return -1;
        }
    }

    private void b(android.support.v4.h.b<i> bVar) {
        if (this.l >= 1) {
            int min = Math.min(this.l, 4);
            int size = this.e.size();
            for (int i2 = 0; i2 < size; i2++) {
                i iVar = (i) this.e.get(i2);
                if (iVar.k < min) {
                    a(iVar, min, iVar.N(), iVar.O(), false);
                    if (iVar.Q != null && !iVar.I && iVar.Y) {
                        bVar.add(iVar);
                    }
                }
            }
        }
    }

    private static void b(View view, c cVar) {
        if (view != null && cVar != null && a(view, cVar)) {
            if (cVar.f578b != null) {
                cVar.f578b.addListener(new d(view));
                return;
            }
            AnimationListener a2 = a(cVar.f577a);
            view.setLayerType(2, null);
            cVar.f577a.setAnimationListener(new a(view, a2));
        }
    }

    private void b(ArrayList<c> arrayList, ArrayList<Boolean> arrayList2) {
        int i2;
        int i3 = 0;
        if (arrayList != null && !arrayList.isEmpty()) {
            if (arrayList2 == null || arrayList.size() != arrayList2.size()) {
                throw new IllegalStateException("Internal error with the back stack records");
            }
            a(arrayList, arrayList2);
            int size = arrayList.size();
            int i4 = 0;
            while (i3 < size) {
                if (!((c) arrayList.get(i3)).t) {
                    if (i4 != i3) {
                        a(arrayList, arrayList2, i4, i3);
                    }
                    int i5 = i3 + 1;
                    if (((Boolean) arrayList2.get(i3)).booleanValue()) {
                        while (i5 < size && ((Boolean) arrayList2.get(i5)).booleanValue() && !((c) arrayList.get(i5)).t) {
                            i5++;
                        }
                    }
                    int i6 = i5;
                    a(arrayList, arrayList2, i3, i6);
                    i4 = i6;
                    i2 = i6 - 1;
                } else {
                    i2 = i3;
                }
                i3 = i2 + 1;
            }
            if (i4 != size) {
                a(arrayList, arrayList2, i4, size);
            }
        }
    }

    private static void b(ArrayList<c> arrayList, ArrayList<Boolean> arrayList2, int i2, int i3) {
        while (i2 < i3) {
            c cVar = (c) arrayList.get(i2);
            if (((Boolean) arrayList2.get(i2)).booleanValue()) {
                cVar.a(-1);
                cVar.b(i2 == i3 + -1);
            } else {
                cVar.a(1);
                cVar.d();
            }
            i2++;
        }
    }

    private void c(boolean z2) {
        if (this.c) {
            throw new IllegalStateException("FragmentManager is already executing transactions");
        } else if (Looper.myLooper() != this.m.h().getLooper()) {
            throw new IllegalStateException("Must be called from main thread of fragment host");
        } else {
            if (!z2) {
                y();
            }
            if (this.w == null) {
                this.w = new ArrayList<>();
                this.x = new ArrayList<>();
            }
            this.c = true;
            try {
                a(null, null);
            } finally {
                this.c = false;
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
        return false;
     */
    private boolean c(ArrayList<c> arrayList, ArrayList<Boolean> arrayList2) {
        synchronized (this) {
            if (this.f565b != null && this.f565b.size() != 0) {
                boolean z2 = false;
                for (int i2 = 0; i2 < this.f565b.size(); i2++) {
                    z2 |= ((f) this.f565b.get(i2)).a(arrayList, arrayList2);
                }
                this.f565b.clear();
                this.m.h().removeCallbacks(this.D);
                return z2;
            }
        }
    }

    public static int d(int i2) {
        switch (i2) {
            case 4097:
                return 8194;
            case 4099:
                return 4099;
            case 8194:
                return 4097;
            default:
                return 0;
        }
    }

    /* JADX INFO: finally extract failed */
    private void e(int i2) {
        try {
            this.c = true;
            a(i2, false);
            this.c = false;
            f();
        } catch (Throwable th) {
            this.c = false;
            throw th;
        }
    }

    private i p(i iVar) {
        ViewGroup viewGroup = iVar.P;
        View view = iVar.Q;
        if (viewGroup == null || view == null) {
            return null;
        }
        for (int indexOf = this.e.indexOf(iVar) - 1; indexOf >= 0; indexOf--) {
            i iVar2 = (i) this.e.get(indexOf);
            if (iVar2.P == viewGroup && iVar2.Q != null) {
                return iVar2;
            }
        }
        return null;
    }

    private void y() {
        if (this.s) {
            throw new IllegalStateException("Can not perform this action after onSaveInstanceState");
        } else if (this.u != null) {
            throw new IllegalStateException("Can not perform this action inside of " + this.u);
        }
    }

    /* access modifiers changed from: private */
    public void z() {
        boolean z2 = true;
        synchronized (this) {
            boolean z3 = this.B != null && !this.B.isEmpty();
            if (this.f565b == null || this.f565b.size() != 1) {
                z2 = false;
            }
            if (z3 || z2) {
                this.m.h().removeCallbacks(this.D);
                this.m.h().post(this.D);
            }
        }
    }

    public int a(c cVar) {
        int i2;
        synchronized (this) {
            if (this.j == null || this.j.size() <= 0) {
                if (this.i == null) {
                    this.i = new ArrayList<>();
                }
                i2 = this.i.size();
                if (f564a) {
                    Log.v("FragmentManager", "Setting back stack index " + i2 + " to " + cVar);
                }
                this.i.add(cVar);
            } else {
                i2 = ((Integer) this.j.remove(this.j.size() - 1)).intValue();
                if (f564a) {
                    Log.v("FragmentManager", "Adding back stack index " + i2 + " with " + cVar);
                }
                this.i.set(i2, cVar);
            }
        }
        return i2;
    }

    public i a(Bundle bundle, String str) {
        int i2 = bundle.getInt(str, -1);
        if (i2 == -1) {
            return null;
        }
        i iVar = (i) this.f.get(i2);
        if (iVar != null) {
            return iVar;
        }
        a((RuntimeException) new IllegalStateException("Fragment no longer exists for key " + str + ": index " + i2));
        return iVar;
    }

    public i a(String str) {
        if (str != null) {
            for (int size = this.e.size() - 1; size >= 0; size--) {
                i iVar = (i) this.e.get(size);
                if (iVar != null && str.equals(iVar.H)) {
                    return iVar;
                }
            }
        }
        if (!(this.f == null || str == null)) {
            for (int size2 = this.f.size() - 1; size2 >= 0; size2--) {
                i iVar2 = (i) this.f.valueAt(size2);
                if (iVar2 != null && str.equals(iVar2.H)) {
                    return iVar2;
                }
            }
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    public c a(i iVar, int i2, boolean z2, int i3) {
        boolean z3;
        int N = iVar.N();
        Animation a2 = iVar.a(i2, z2, N);
        if (a2 != null) {
            return new c(a2);
        }
        Animator b2 = iVar.b(i2, z2, N);
        if (b2 != null) {
            return new c(b2);
        }
        if (N != 0) {
            boolean equals = "anim".equals(this.m.g().getResources().getResourceTypeName(N));
            if (equals) {
                try {
                    Animation loadAnimation = AnimationUtils.loadAnimation(this.m.g(), N);
                    if (loadAnimation != null) {
                        return new c(loadAnimation);
                    }
                    z3 = true;
                } catch (NotFoundException e2) {
                    throw e2;
                } catch (RuntimeException e3) {
                    z3 = false;
                }
            } else {
                z3 = false;
            }
            if (!z3) {
                try {
                    Animator loadAnimator = AnimatorInflater.loadAnimator(this.m.g(), N);
                    if (loadAnimator != null) {
                        return new c(loadAnimator);
                    }
                } catch (RuntimeException e4) {
                    if (equals) {
                        throw e4;
                    }
                    Animation loadAnimation2 = AnimationUtils.loadAnimation(this.m.g(), N);
                    if (loadAnimation2 != null) {
                        return new c(loadAnimation2);
                    }
                }
            }
        }
        if (i2 == 0) {
            return null;
        }
        int b3 = b(i2, z2);
        if (b3 < 0) {
            return null;
        }
        switch (b3) {
            case 1:
                return a(this.m.g(), 1.125f, 1.0f, 0.0f, 1.0f);
            case 2:
                return a(this.m.g(), 1.0f, 0.975f, 1.0f, 0.0f);
            case 3:
                return a(this.m.g(), 0.975f, 1.0f, 0.0f, 1.0f);
            case 4:
                return a(this.m.g(), 1.0f, 1.075f, 1.0f, 0.0f);
            case 5:
                return a(this.m.g(), 0.0f, 1.0f);
            case 6:
                return a(this.m.g(), 1.0f, 0.0f);
            default:
                if (i3 == 0 && this.m.d()) {
                    i3 = this.m.e();
                }
                return i3 == 0 ? null : null;
        }
    }

    public s a() {
        return new c(this);
    }

    public void a(int i2, int i3) {
        if (i2 < 0) {
            throw new IllegalArgumentException("Bad id: " + i2);
        }
        a((f) new g(null, i2, i3), false);
    }

    public void a(int i2, c cVar) {
        synchronized (this) {
            if (this.i == null) {
                this.i = new ArrayList<>();
            }
            int size = this.i.size();
            if (i2 < size) {
                if (f564a) {
                    Log.v("FragmentManager", "Setting back stack index " + i2 + " to " + cVar);
                }
                this.i.set(i2, cVar);
            } else {
                while (size < i2) {
                    this.i.add(null);
                    if (this.j == null) {
                        this.j = new ArrayList<>();
                    }
                    if (f564a) {
                        Log.v("FragmentManager", "Adding available back stack index " + size);
                    }
                    this.j.add(Integer.valueOf(size));
                    size++;
                }
                if (f564a) {
                    Log.v("FragmentManager", "Adding back stack index " + i2 + " with " + cVar);
                }
                this.i.add(cVar);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(int i2, boolean z2) {
        boolean z3;
        if (this.m == null && i2 != 0) {
            throw new IllegalStateException("No activity");
        } else if (z2 || i2 != this.l) {
            this.l = i2;
            if (this.f != null) {
                int size = this.e.size();
                int i3 = 0;
                boolean z4 = false;
                while (i3 < size) {
                    i iVar = (i) this.e.get(i3);
                    e(iVar);
                    i3++;
                    z4 = iVar.U != null ? iVar.U.a() | z4 : z4;
                }
                int size2 = this.f.size();
                int i4 = 0;
                while (i4 < size2) {
                    i iVar2 = (i) this.f.valueAt(i4);
                    if (iVar2 != null && ((iVar2.u || iVar2.J) && !iVar2.Y)) {
                        e(iVar2);
                        if (iVar2.U != null) {
                            z3 = iVar2.U.a() | z4;
                            i4++;
                            z4 = z3;
                        }
                    }
                    z3 = z4;
                    i4++;
                    z4 = z3;
                }
                if (!z4) {
                    e();
                }
                if (this.r && this.m != null && this.l == 5) {
                    this.m.c();
                    this.r = false;
                }
            }
        }
    }

    public void a(Configuration configuration) {
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < this.e.size()) {
                i iVar = (i) this.e.get(i3);
                if (iVar != null) {
                    iVar.a(configuration);
                }
                i2 = i3 + 1;
            } else {
                return;
            }
        }
    }

    public void a(Bundle bundle, String str, i iVar) {
        if (iVar.n < 0) {
            a((RuntimeException) new IllegalStateException("Fragment " + iVar + " is not currently in the FragmentManager"));
        }
        bundle.putInt(str, iVar.n);
    }

    /* access modifiers changed from: 0000 */
    public void a(Parcelable parcelable, p pVar) {
        List list;
        if (parcelable != null) {
            q qVar = (q) parcelable;
            if (qVar.f587a != null) {
                if (pVar != null) {
                    List a2 = pVar.a();
                    List b2 = pVar.b();
                    int i2 = a2 != null ? a2.size() : 0;
                    for (int i3 = 0; i3 < i2; i3++) {
                        i iVar = (i) a2.get(i3);
                        if (f564a) {
                            Log.v("FragmentManager", "restoreAllState: re-attaching retained " + iVar);
                        }
                        int i4 = 0;
                        while (i4 < qVar.f587a.length && qVar.f587a[i4].f590b != iVar.n) {
                            i4++;
                        }
                        if (i4 == qVar.f587a.length) {
                            a((RuntimeException) new IllegalStateException("Could not find active fragment with index " + iVar.n));
                        }
                        r rVar = qVar.f587a[i4];
                        rVar.l = iVar;
                        iVar.m = null;
                        iVar.z = 0;
                        iVar.w = false;
                        iVar.t = false;
                        iVar.q = null;
                        if (rVar.k != null) {
                            rVar.k.setClassLoader(this.m.g().getClassLoader());
                            iVar.m = rVar.k.getSparseParcelableArray("android:view_state");
                            iVar.l = rVar.k;
                        }
                    }
                    list = b2;
                } else {
                    list = null;
                }
                this.f = new SparseArray<>(qVar.f587a.length);
                int i5 = 0;
                while (i5 < qVar.f587a.length) {
                    r rVar2 = qVar.f587a[i5];
                    if (rVar2 != null) {
                        i a3 = rVar2.a(this.m, this.n, this.o, (list == null || i5 >= list.size()) ? null : (p) list.get(i5));
                        if (f564a) {
                            Log.v("FragmentManager", "restoreAllState: active #" + i5 + ": " + a3);
                        }
                        this.f.put(a3.n, a3);
                        rVar2.l = null;
                    }
                    i5++;
                }
                if (pVar != null) {
                    List a4 = pVar.a();
                    int i6 = a4 != null ? a4.size() : 0;
                    for (int i7 = 0; i7 < i6; i7++) {
                        i iVar2 = (i) a4.get(i7);
                        if (iVar2.r >= 0) {
                            iVar2.q = (i) this.f.get(iVar2.r);
                            if (iVar2.q == null) {
                                Log.w("FragmentManager", "Re-attaching retained fragment " + iVar2 + " target no longer exists: " + iVar2.r);
                            }
                        }
                    }
                }
                this.e.clear();
                if (qVar.f588b != null) {
                    for (int i8 = 0; i8 < qVar.f588b.length; i8++) {
                        i iVar3 = (i) this.f.get(qVar.f588b[i8]);
                        if (iVar3 == null) {
                            a((RuntimeException) new IllegalStateException("No instantiated fragment for index #" + qVar.f588b[i8]));
                        }
                        iVar3.t = true;
                        if (f564a) {
                            Log.v("FragmentManager", "restoreAllState: added #" + i8 + ": " + iVar3);
                        }
                        if (this.e.contains(iVar3)) {
                            throw new IllegalStateException("Already added!");
                        }
                        synchronized (this.e) {
                            this.e.add(iVar3);
                        }
                    }
                }
                if (qVar.c != null) {
                    this.g = new ArrayList<>(qVar.c.length);
                    for (int i9 = 0; i9 < qVar.c.length; i9++) {
                        c a5 = qVar.c[i9].a(this);
                        if (f564a) {
                            Log.v("FragmentManager", "restoreAllState: back stack #" + i9 + " (index " + a5.m + "): " + a5);
                            PrintWriter printWriter = new PrintWriter(new android.support.v4.h.e("FragmentManager"));
                            a5.a("  ", printWriter, false);
                            printWriter.close();
                        }
                        this.g.add(a5);
                        if (a5.m >= 0) {
                            a(a5.m, a5);
                        }
                    }
                } else {
                    this.g = null;
                }
                if (qVar.d >= 0) {
                    this.p = (i) this.f.get(qVar.d);
                }
                this.d = qVar.e;
            }
        }
    }

    public void a(i iVar) {
        if (!iVar.S) {
            return;
        }
        if (this.c) {
            this.v = true;
            return;
        }
        iVar.S = false;
        a(iVar, this.l, 0, 0, false);
    }

    /* JADX INFO: used method not loaded: android.support.v4.a.m.a(android.support.v4.a.i):null, types can be incorrect */
    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Code restructure failed: missing block: B:128:0x0324, code lost:
        r1 = com.hmatalonga.greenhub.models.Network.TYPE_UNKNOWN;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:135:0x0339, code lost:
        if (r12 >= 1) goto L_0x005f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:137:0x033d, code lost:
        if (r10.t == false) goto L_0x034f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:139:0x0343, code lost:
        if (r11.S() == null) goto L_0x044b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:140:0x0345, code lost:
        r0 = r11.S();
        r11.a((android.view.View) null);
        r0.clearAnimation();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:142:0x0353, code lost:
        if (r11.S() != null) goto L_0x035b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:144:0x0359, code lost:
        if (r11.T() == null) goto L_0x045d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:145:0x035b, code lost:
        r11.b(r12);
        r12 = 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:152:0x0386, code lost:
        if (r12 >= 4) goto L_0x03aa;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:154:0x038a, code lost:
        if (f564a == false) goto L_0x03a4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:155:0x038c, code lost:
        android.util.Log.v("FragmentManager", "movefrom STARTED: " + r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:156:0x03a4, code lost:
        r11.I();
        e(r11, false);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:157:0x03aa, code lost:
        if (r12 >= 3) goto L_0x03cb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:159:0x03ae, code lost:
        if (f564a == false) goto L_0x03c8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:160:0x03b0, code lost:
        android.util.Log.v("FragmentManager", "movefrom STOPPED: " + r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:161:0x03c8, code lost:
        r11.J();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:163:0x03cc, code lost:
        if (r12 >= 2) goto L_0x0339;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:165:0x03d0, code lost:
        if (f564a == false) goto L_0x03ea;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:166:0x03d2, code lost:
        android.util.Log.v("FragmentManager", "movefrom ACTIVITY_CREATED: " + r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:168:0x03ec, code lost:
        if (r11.Q == null) goto L_0x03fd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:170:0x03f4, code lost:
        if (r10.m.a(r11) == false) goto L_0x03fd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:172:0x03f8, code lost:
        if (r11.m != null) goto L_0x03fd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:173:0x03fa, code lost:
        m(r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:174:0x03fd, code lost:
        r11.K();
        f(r11, false);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:175:0x0405, code lost:
        if (r11.Q == null) goto L_0x0441;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:177:0x0409, code lost:
        if (r11.P == null) goto L_0x0441;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:178:0x040b, code lost:
        r11.Q.clearAnimation();
        r11.P.endViewTransition(r11.Q);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:179:0x0419, code lost:
        if (r10.l <= 0) goto L_0x049f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:181:0x041d, code lost:
        if (r10.t != false) goto L_0x049f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:183:0x0425, code lost:
        if (r11.Q.getVisibility() != 0) goto L_0x049f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:185:0x042c, code lost:
        if (r11.aa < 0.0f) goto L_0x049f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:186:0x042e, code lost:
        r0 = a(r11, r13, false, r14);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:187:0x0432, code lost:
        r11.aa = 0.0f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:188:0x0435, code lost:
        if (r0 == null) goto L_0x043a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:189:0x0437, code lost:
        a(r11, r0, r12);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:190:0x043a, code lost:
        r11.P.removeView(r11.Q);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:191:0x0441, code lost:
        r11.P = null;
        r11.Q = null;
        r11.R = null;
        r11.w = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:193:0x044f, code lost:
        if (r11.T() == null) goto L_0x034f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:194:0x0451, code lost:
        r0 = r11.T();
        r11.a((android.animation.Animator) null);
        r0.cancel();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:196:0x045f, code lost:
        if (f564a == false) goto L_0x0479;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:197:0x0461, code lost:
        android.util.Log.v("FragmentManager", "movefrom CREATED: " + r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:199:0x047b, code lost:
        if (r11.L != false) goto L_0x0494;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:200:0x047d, code lost:
        r11.L();
        g(r11, false);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:201:0x0483, code lost:
        r11.M();
        h(r11, false);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:202:0x0489, code lost:
        if (r15 != false) goto L_0x005f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:204:0x048d, code lost:
        if (r11.L != false) goto L_0x0497;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:205:0x048f, code lost:
        g(r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:206:0x0494, code lost:
        r11.k = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:207:0x0497, code lost:
        r11.B = null;
        r11.E = null;
        r11.A = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:208:0x049f, code lost:
        r0 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:209:0x04a1, code lost:
        r0 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x01c2, code lost:
        c(r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x01c5, code lost:
        if (r12 <= 1) goto L_0x02c0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x01c9, code lost:
        if (f564a == false) goto L_0x01e3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x01cb, code lost:
        android.util.Log.v("FragmentManager", "moveto ACTIVITY_CREATED: " + r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x01e5, code lost:
        if (r11.v != false) goto L_0x02ab;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x01e9, code lost:
        if (r11.G == 0) goto L_0x04a1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x01ee, code lost:
        if (r11.G != -1) goto L_0x0211;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:0x01f0, code lost:
        a((java.lang.RuntimeException) new java.lang.IllegalArgumentException("Cannot create fragment " + r11 + " for a container view with no id"));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:0x0211, code lost:
        r0 = (android.view.ViewGroup) r10.n.a(r11.G);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:0x021b, code lost:
        if (r0 != null) goto L_0x0260;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:0x021f, code lost:
        if (r11.x != false) goto L_0x0260;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:?, code lost:
        r1 = r11.j().getResourceName(r11.G);
     */
    /* JADX WARNING: Removed duplicated region for block: B:211:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0063  */
    public void a(i iVar, int i2, int i3, int i4, boolean z2) {
        ViewGroup viewGroup;
        String str;
        boolean z3 = true;
        if ((!iVar.t || iVar.J) && i2 > 1) {
            i2 = 1;
        }
        if (iVar.u && i2 > iVar.k) {
            i2 = (iVar.k != 0 || !iVar.g()) ? iVar.k : 1;
        }
        if (iVar.S && iVar.k < 4 && i2 > 3) {
            i2 = 3;
        }
        if (iVar.k > i2) {
            if (iVar.k > i2) {
                switch (iVar.k) {
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                    case 4:
                        break;
                    case 5:
                        if (i2 < 5) {
                            if (f564a) {
                                Log.v("FragmentManager", "movefrom RESUMED: " + iVar);
                            }
                            iVar.H();
                            d(iVar, false);
                            break;
                        }
                        break;
                }
            }
        } else if (!iVar.v || iVar.w) {
            if (!(iVar.S() == null && iVar.T() == null)) {
                iVar.a((View) null);
                iVar.a((Animator) null);
                a(iVar, iVar.U(), 0, 0, true);
            }
            switch (iVar.k) {
                case 0:
                    if (i2 > 0) {
                        if (f564a) {
                            Log.v("FragmentManager", "moveto CREATED: " + iVar);
                        }
                        if (iVar.l != null) {
                            iVar.l.setClassLoader(this.m.g().getClassLoader());
                            iVar.m = iVar.l.getSparseParcelableArray("android:view_state");
                            iVar.q = a(iVar.l, "android:target_state");
                            if (iVar.q != null) {
                                iVar.s = iVar.l.getInt("android:target_req_state", 0);
                            }
                            iVar.T = iVar.l.getBoolean("android:user_visible_hint", true);
                            if (!iVar.T) {
                                iVar.S = true;
                                if (i2 > 3) {
                                    i2 = 3;
                                }
                            }
                        }
                        iVar.B = this.m;
                        iVar.E = this.o;
                        iVar.A = this.o != null ? this.o.C : this.m.i();
                        if (iVar.q != null) {
                            if (this.f.get(iVar.q.n) != iVar.q) {
                                throw new IllegalStateException("Fragment " + iVar + " declared target fragment " + iVar.q + " that does not belong to this FragmentManager!");
                            } else if (iVar.q.k < 1) {
                                a(iVar.q, 1, 0, 0, true);
                            }
                        }
                        a(iVar, this.m.g(), false);
                        iVar.O = false;
                        iVar.a(this.m.g());
                        if (iVar.O) {
                            if (iVar.E == null) {
                                this.m.b(iVar);
                            } else {
                                iVar.E.a(iVar);
                            }
                            b(iVar, this.m.g(), false);
                            if (!iVar.ac) {
                                a(iVar, iVar.l, false);
                                iVar.l(iVar.l);
                                b(iVar, iVar.l, false);
                            } else {
                                iVar.j(iVar.l);
                                iVar.k = 1;
                            }
                            iVar.L = false;
                            break;
                        } else {
                            throw new ah("Fragment " + iVar + " did not call through to super.onAttach()");
                        }
                    }
                    break;
                case 1:
                    break;
                case 2:
                    break;
                case 3:
                    break;
                case 4:
                    break;
            }
        } else {
            return;
        }
        if (iVar.k == i2) {
            Log.w("FragmentManager", "moveToState: Fragment state for " + iVar + " not updated inline; " + "expected state " + i2 + " found " + iVar.k);
            iVar.k = i2;
            return;
        }
        return;
        a((RuntimeException) new IllegalArgumentException("No view found for id 0x" + Integer.toHexString(iVar.G) + " (" + str + ") for fragment " + iVar));
        iVar.P = viewGroup;
        iVar.Q = iVar.b(iVar.h(iVar.l), viewGroup, iVar.l);
        if (iVar.Q != null) {
            iVar.R = iVar.Q;
            iVar.Q.setSaveFromParentEnabled(false);
            if (viewGroup != null) {
                viewGroup.addView(iVar.Q);
            }
            if (iVar.I) {
                iVar.Q.setVisibility(8);
            }
            iVar.a(iVar.Q, iVar.l);
            a(iVar, iVar.Q, iVar.l, false);
            if (iVar.Q.getVisibility() != 0 || iVar.P == null) {
                z3 = false;
            }
            iVar.Y = z3;
        } else {
            iVar.R = null;
        }
        iVar.m(iVar.l);
        c(iVar, iVar.l, false);
        if (iVar.Q != null) {
            iVar.f(iVar.l);
        }
        iVar.l = null;
        if (i2 > 2) {
            iVar.k = 3;
        }
        if (i2 > 3) {
            if (f564a) {
                Log.v("FragmentManager", "moveto STARTED: " + iVar);
            }
            iVar.D();
            b(iVar, false);
        }
        if (i2 > 4) {
            if (f564a) {
                Log.v("FragmentManager", "moveto RESUMED: " + iVar);
            }
            iVar.E();
            c(iVar, false);
            iVar.l = null;
            iVar.m = null;
        }
        if (iVar.k == i2) {
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(i iVar, Context context, boolean z2) {
        if (this.o != null) {
            n k2 = this.o.k();
            if (k2 instanceof o) {
                ((o) k2).a(iVar, context, true);
            }
        }
        Iterator it = this.I.iterator();
        while (it.hasNext()) {
            j jVar = (j) it.next();
            if (!z2 || ((Boolean) jVar.f704b).booleanValue()) {
                ((android.support.v4.a.n.a) jVar.f703a).a((n) this, iVar, context);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(i iVar, Bundle bundle, boolean z2) {
        if (this.o != null) {
            n k2 = this.o.k();
            if (k2 instanceof o) {
                ((o) k2).a(iVar, bundle, true);
            }
        }
        Iterator it = this.I.iterator();
        while (it.hasNext()) {
            j jVar = (j) it.next();
            if (!z2 || ((Boolean) jVar.f704b).booleanValue()) {
                ((android.support.v4.a.n.a) jVar.f703a).a((n) this, iVar, bundle);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(i iVar, View view, Bundle bundle, boolean z2) {
        if (this.o != null) {
            n k2 = this.o.k();
            if (k2 instanceof o) {
                ((o) k2).a(iVar, view, bundle, true);
            }
        }
        Iterator it = this.I.iterator();
        while (it.hasNext()) {
            j jVar = (j) it.next();
            if (!z2 || ((Boolean) jVar.f704b).booleanValue()) {
                ((android.support.v4.a.n.a) jVar.f703a).a(this, iVar, view, bundle);
            }
        }
    }

    public void a(i iVar, boolean z2) {
        if (f564a) {
            Log.v("FragmentManager", "add: " + iVar);
        }
        f(iVar);
        if (iVar.J) {
            return;
        }
        if (this.e.contains(iVar)) {
            throw new IllegalStateException("Fragment already added: " + iVar);
        }
        synchronized (this.e) {
            this.e.add(iVar);
        }
        iVar.t = true;
        iVar.u = false;
        if (iVar.Q == null) {
            iVar.Z = false;
        }
        if (iVar.M && iVar.N) {
            this.r = true;
        }
        if (z2) {
            b(iVar);
        }
    }

    public void a(m mVar, k kVar, i iVar) {
        if (this.m != null) {
            throw new IllegalStateException("Already attached");
        }
        this.m = mVar;
        this.n = kVar;
        this.o = iVar;
    }

    public void a(f fVar, boolean z2) {
        if (!z2) {
            y();
        }
        synchronized (this) {
            if (!this.t && this.m != null) {
                if (this.f565b == null) {
                    this.f565b = new ArrayList<>();
                }
                this.f565b.add(fVar);
                z();
            } else if (!z2) {
                throw new IllegalStateException("Activity has been destroyed");
            }
        }
    }

    public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        String str2 = str + "    ";
        if (this.f != null) {
            int size = this.f.size();
            if (size > 0) {
                printWriter.print(str);
                printWriter.print("Active Fragments in ");
                printWriter.print(Integer.toHexString(System.identityHashCode(this)));
                printWriter.println(":");
                for (int i2 = 0; i2 < size; i2++) {
                    i iVar = (i) this.f.valueAt(i2);
                    printWriter.print(str);
                    printWriter.print("  #");
                    printWriter.print(i2);
                    printWriter.print(": ");
                    printWriter.println(iVar);
                    if (iVar != null) {
                        iVar.a(str2, fileDescriptor, printWriter, strArr);
                    }
                }
            }
        }
        int size2 = this.e.size();
        if (size2 > 0) {
            printWriter.print(str);
            printWriter.println("Added Fragments:");
            for (int i3 = 0; i3 < size2; i3++) {
                i iVar2 = (i) this.e.get(i3);
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(i3);
                printWriter.print(": ");
                printWriter.println(iVar2.toString());
            }
        }
        if (this.h != null) {
            int size3 = this.h.size();
            if (size3 > 0) {
                printWriter.print(str);
                printWriter.println("Fragments Created Menus:");
                for (int i4 = 0; i4 < size3; i4++) {
                    i iVar3 = (i) this.h.get(i4);
                    printWriter.print(str);
                    printWriter.print("  #");
                    printWriter.print(i4);
                    printWriter.print(": ");
                    printWriter.println(iVar3.toString());
                }
            }
        }
        if (this.g != null) {
            int size4 = this.g.size();
            if (size4 > 0) {
                printWriter.print(str);
                printWriter.println("Back Stack:");
                for (int i5 = 0; i5 < size4; i5++) {
                    c cVar = (c) this.g.get(i5);
                    printWriter.print(str);
                    printWriter.print("  #");
                    printWriter.print(i5);
                    printWriter.print(": ");
                    printWriter.println(cVar.toString());
                    cVar.a(str2, fileDescriptor, printWriter, strArr);
                }
            }
        }
        synchronized (this) {
            if (this.i != null) {
                int size5 = this.i.size();
                if (size5 > 0) {
                    printWriter.print(str);
                    printWriter.println("Back Stack Indices:");
                    for (int i6 = 0; i6 < size5; i6++) {
                        c cVar2 = (c) this.i.get(i6);
                        printWriter.print(str);
                        printWriter.print("  #");
                        printWriter.print(i6);
                        printWriter.print(": ");
                        printWriter.println(cVar2);
                    }
                }
            }
            if (this.j != null && this.j.size() > 0) {
                printWriter.print(str);
                printWriter.print("mAvailBackStackIndices: ");
                printWriter.println(Arrays.toString(this.j.toArray()));
            }
        }
        if (this.f565b != null) {
            int size6 = this.f565b.size();
            if (size6 > 0) {
                printWriter.print(str);
                printWriter.println("Pending Actions:");
                for (int i7 = 0; i7 < size6; i7++) {
                    f fVar = (f) this.f565b.get(i7);
                    printWriter.print(str);
                    printWriter.print("  #");
                    printWriter.print(i7);
                    printWriter.print(": ");
                    printWriter.println(fVar);
                }
            }
        }
        printWriter.print(str);
        printWriter.println("FragmentManager misc state:");
        printWriter.print(str);
        printWriter.print("  mHost=");
        printWriter.println(this.m);
        printWriter.print(str);
        printWriter.print("  mContainer=");
        printWriter.println(this.n);
        if (this.o != null) {
            printWriter.print(str);
            printWriter.print("  mParent=");
            printWriter.println(this.o);
        }
        printWriter.print(str);
        printWriter.print("  mCurState=");
        printWriter.print(this.l);
        printWriter.print(" mStateSaved=");
        printWriter.print(this.s);
        printWriter.print(" mDestroyed=");
        printWriter.println(this.t);
        if (this.r) {
            printWriter.print(str);
            printWriter.print("  mNeedMenuInvalidate=");
            printWriter.println(this.r);
        }
        if (this.u != null) {
            printWriter.print(str);
            printWriter.print("  mNoTransactionsBecause=");
            printWriter.println(this.u);
        }
    }

    public void a(boolean z2) {
        for (int size = this.e.size() - 1; size >= 0; size--) {
            i iVar = (i) this.e.get(size);
            if (iVar != null) {
                iVar.f(z2);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean a(int i2) {
        return this.l >= i2;
    }

    public boolean a(Menu menu) {
        if (this.l < 1) {
            return false;
        }
        boolean z2 = false;
        for (int i2 = 0; i2 < this.e.size(); i2++) {
            i iVar = (i) this.e.get(i2);
            if (iVar != null && iVar.c(menu)) {
                z2 = true;
            }
        }
        return z2;
    }

    public boolean a(Menu menu, MenuInflater menuInflater) {
        boolean z2;
        if (this.l < 1) {
            return false;
        }
        ArrayList<i> arrayList = null;
        int i2 = 0;
        boolean z3 = false;
        while (i2 < this.e.size()) {
            i iVar = (i) this.e.get(i2);
            if (iVar == null || !iVar.b(menu, menuInflater)) {
                z2 = z3;
            } else {
                if (arrayList == null) {
                    arrayList = new ArrayList<>();
                }
                arrayList.add(iVar);
                z2 = true;
            }
            i2++;
            z3 = z2;
        }
        if (this.h != null) {
            for (int i3 = 0; i3 < this.h.size(); i3++) {
                i iVar2 = (i) this.h.get(i3);
                if (arrayList == null || !arrayList.contains(iVar2)) {
                    iVar2.s();
                }
            }
        }
        this.h = arrayList;
        return z3;
    }

    public boolean a(MenuItem menuItem) {
        if (this.l < 1) {
            return false;
        }
        for (int i2 = 0; i2 < this.e.size(); i2++) {
            i iVar = (i) this.e.get(i2);
            if (iVar != null && iVar.c(menuItem)) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: 0000 */
    public boolean a(ArrayList<c> arrayList, ArrayList<Boolean> arrayList2, String str, int i2, int i3) {
        int i4;
        if (this.g == null) {
            return false;
        }
        if (str == null && i2 < 0 && (i3 & 1) == 0) {
            int size = this.g.size() - 1;
            if (size < 0) {
                return false;
            }
            arrayList.add(this.g.remove(size));
            arrayList2.add(Boolean.valueOf(true));
        } else {
            int i5 = -1;
            if (str != null || i2 >= 0) {
                int size2 = this.g.size() - 1;
                while (i4 >= 0) {
                    c cVar = (c) this.g.get(i4);
                    if ((str != null && str.equals(cVar.f())) || (i2 >= 0 && i2 == cVar.m)) {
                        break;
                    }
                    size2 = i4 - 1;
                }
                if (i4 < 0) {
                    return false;
                }
                if ((i3 & 1) != 0) {
                    i4--;
                    while (i4 >= 0) {
                        c cVar2 = (c) this.g.get(i4);
                        if ((str == null || !str.equals(cVar2.f())) && (i2 < 0 || i2 != cVar2.m)) {
                            break;
                        }
                        i4--;
                    }
                }
                i5 = i4;
            }
            if (i5 == this.g.size() - 1) {
                return false;
            }
            for (int size3 = this.g.size() - 1; size3 > i5; size3--) {
                arrayList.add(this.g.remove(size3));
                arrayList2.add(Boolean.valueOf(true));
            }
        }
        return true;
    }

    public i b(int i2) {
        for (int size = this.e.size() - 1; size >= 0; size--) {
            i iVar = (i) this.e.get(size);
            if (iVar != null && iVar.F == i2) {
                return iVar;
            }
        }
        if (this.f != null) {
            for (int size2 = this.f.size() - 1; size2 >= 0; size2--) {
                i iVar2 = (i) this.f.valueAt(size2);
                if (iVar2 != null && iVar2.F == i2) {
                    return iVar2;
                }
            }
        }
        return null;
    }

    public i b(String str) {
        if (!(this.f == null || str == null)) {
            for (int size = this.f.size() - 1; size >= 0; size--) {
                i iVar = (i) this.f.valueAt(size);
                if (iVar != null) {
                    i a2 = iVar.a(str);
                    if (a2 != null) {
                        return a2;
                    }
                }
            }
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    public void b(c cVar) {
        if (this.g == null) {
            this.g = new ArrayList<>();
        }
        this.g.add(cVar);
    }

    /* access modifiers changed from: 0000 */
    public void b(i iVar) {
        a(iVar, this.l, 0, 0, false);
    }

    /* access modifiers changed from: 0000 */
    public void b(i iVar, Context context, boolean z2) {
        if (this.o != null) {
            n k2 = this.o.k();
            if (k2 instanceof o) {
                ((o) k2).b(iVar, context, true);
            }
        }
        Iterator it = this.I.iterator();
        while (it.hasNext()) {
            j jVar = (j) it.next();
            if (!z2 || ((Boolean) jVar.f704b).booleanValue()) {
                ((android.support.v4.a.n.a) jVar.f703a).b((n) this, iVar, context);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void b(i iVar, Bundle bundle, boolean z2) {
        if (this.o != null) {
            n k2 = this.o.k();
            if (k2 instanceof o) {
                ((o) k2).b(iVar, bundle, true);
            }
        }
        Iterator it = this.I.iterator();
        while (it.hasNext()) {
            j jVar = (j) it.next();
            if (!z2 || ((Boolean) jVar.f704b).booleanValue()) {
                ((android.support.v4.a.n.a) jVar.f703a).b((n) this, iVar, bundle);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void b(i iVar, boolean z2) {
        if (this.o != null) {
            n k2 = this.o.k();
            if (k2 instanceof o) {
                ((o) k2).b(iVar, true);
            }
        }
        Iterator it = this.I.iterator();
        while (it.hasNext()) {
            j jVar = (j) it.next();
            if (!z2 || ((Boolean) jVar.f704b).booleanValue()) {
                ((android.support.v4.a.n.a) jVar.f703a).a(this, iVar);
            }
        }
    }

    public void b(Menu menu) {
        if (this.l >= 1) {
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 < this.e.size()) {
                    i iVar = (i) this.e.get(i3);
                    if (iVar != null) {
                        iVar.d(menu);
                    }
                    i2 = i3 + 1;
                } else {
                    return;
                }
            }
        }
    }

    public void b(boolean z2) {
        for (int size = this.e.size() - 1; size >= 0; size--) {
            i iVar = (i) this.e.get(size);
            if (iVar != null) {
                iVar.g(z2);
            }
        }
    }

    public boolean b() {
        y();
        return a((String) null, -1, 0);
    }

    public boolean b(MenuItem menuItem) {
        if (this.l < 1) {
            return false;
        }
        for (int i2 = 0; i2 < this.e.size(); i2++) {
            i iVar = (i) this.e.get(i2);
            if (iVar != null && iVar.d(menuItem)) {
                return true;
            }
        }
        return false;
    }

    public List<i> c() {
        List<i> list;
        if (this.e.isEmpty()) {
            return Collections.EMPTY_LIST;
        }
        synchronized (this.e) {
            list = (List) this.e.clone();
        }
        return list;
    }

    public void c(int i2) {
        synchronized (this) {
            this.i.set(i2, null);
            if (this.j == null) {
                this.j = new ArrayList<>();
            }
            if (f564a) {
                Log.v("FragmentManager", "Freeing back stack index " + i2);
            }
            this.j.add(Integer.valueOf(i2));
        }
    }

    /* access modifiers changed from: 0000 */
    public void c(i iVar) {
        if (iVar.v && !iVar.y) {
            iVar.Q = iVar.b(iVar.h(iVar.l), (ViewGroup) null, iVar.l);
            if (iVar.Q != null) {
                iVar.R = iVar.Q;
                iVar.Q.setSaveFromParentEnabled(false);
                if (iVar.I) {
                    iVar.Q.setVisibility(8);
                }
                iVar.a(iVar.Q, iVar.l);
                a(iVar, iVar.Q, iVar.l, false);
                return;
            }
            iVar.R = null;
        }
    }

    /* access modifiers changed from: 0000 */
    public void c(i iVar, Bundle bundle, boolean z2) {
        if (this.o != null) {
            n k2 = this.o.k();
            if (k2 instanceof o) {
                ((o) k2).c(iVar, bundle, true);
            }
        }
        Iterator it = this.I.iterator();
        while (it.hasNext()) {
            j jVar = (j) it.next();
            if (!z2 || ((Boolean) jVar.f704b).booleanValue()) {
                ((android.support.v4.a.n.a) jVar.f703a).c(this, iVar, bundle);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void c(i iVar, boolean z2) {
        if (this.o != null) {
            n k2 = this.o.k();
            if (k2 instanceof o) {
                ((o) k2).c(iVar, true);
            }
        }
        Iterator it = this.I.iterator();
        while (it.hasNext()) {
            j jVar = (j) it.next();
            if (!z2 || ((Boolean) jVar.f704b).booleanValue()) {
                ((android.support.v4.a.n.a) jVar.f703a).b(this, iVar);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void d(final i iVar) {
        if (iVar.Q != null) {
            c a2 = a(iVar, iVar.O(), !iVar.I, iVar.P());
            if (a2 == null || a2.f578b == null) {
                if (a2 != null) {
                    b(iVar.Q, a2);
                    iVar.Q.startAnimation(a2.f577a);
                    a2.f577a.start();
                }
                iVar.Q.setVisibility((!iVar.I || iVar.W()) ? 0 : 8);
                if (iVar.W()) {
                    iVar.h(false);
                }
            } else {
                a2.f578b.setTarget(iVar.Q);
                if (!iVar.I) {
                    iVar.Q.setVisibility(0);
                } else if (iVar.W()) {
                    iVar.h(false);
                } else {
                    final ViewGroup viewGroup = iVar.P;
                    final View view = iVar.Q;
                    viewGroup.startViewTransition(view);
                    a2.f578b.addListener(new AnimatorListenerAdapter() {
                        public void onAnimationEnd(Animator animator) {
                            viewGroup.endViewTransition(view);
                            animator.removeListener(this);
                            if (iVar.Q != null) {
                                iVar.Q.setVisibility(8);
                            }
                        }
                    });
                }
                b(iVar.Q, a2);
                a2.f578b.start();
            }
        }
        if (iVar.t && iVar.M && iVar.N) {
            this.r = true;
        }
        iVar.Z = false;
        iVar.c(iVar.I);
    }

    /* access modifiers changed from: 0000 */
    public void d(i iVar, Bundle bundle, boolean z2) {
        if (this.o != null) {
            n k2 = this.o.k();
            if (k2 instanceof o) {
                ((o) k2).d(iVar, bundle, true);
            }
        }
        Iterator it = this.I.iterator();
        while (it.hasNext()) {
            j jVar = (j) it.next();
            if (!z2 || ((Boolean) jVar.f704b).booleanValue()) {
                ((android.support.v4.a.n.a) jVar.f703a).d(this, iVar, bundle);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void d(i iVar, boolean z2) {
        if (this.o != null) {
            n k2 = this.o.k();
            if (k2 instanceof o) {
                ((o) k2).d(iVar, true);
            }
        }
        Iterator it = this.I.iterator();
        while (it.hasNext()) {
            j jVar = (j) it.next();
            if (!z2 || ((Boolean) jVar.f704b).booleanValue()) {
                ((android.support.v4.a.n.a) jVar.f703a).c(this, iVar);
            }
        }
    }

    public boolean d() {
        return this.s;
    }

    /* access modifiers changed from: 0000 */
    public void e() {
        if (this.f != null) {
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 < this.f.size()) {
                    i iVar = (i) this.f.valueAt(i3);
                    if (iVar != null) {
                        a(iVar);
                    }
                    i2 = i3 + 1;
                } else {
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void e(i iVar) {
        if (iVar != null) {
            int i2 = this.l;
            if (iVar.u) {
                i2 = iVar.g() ? Math.min(i2, 1) : Math.min(i2, 0);
            }
            a(iVar, i2, iVar.O(), iVar.P(), false);
            if (iVar.Q != null) {
                i p2 = p(iVar);
                if (p2 != null) {
                    View view = p2.Q;
                    ViewGroup viewGroup = iVar.P;
                    int indexOfChild = viewGroup.indexOfChild(view);
                    int indexOfChild2 = viewGroup.indexOfChild(iVar.Q);
                    if (indexOfChild2 < indexOfChild) {
                        viewGroup.removeViewAt(indexOfChild2);
                        viewGroup.addView(iVar.Q, indexOfChild);
                    }
                }
                if (iVar.Y && iVar.P != null) {
                    if (iVar.aa > 0.0f) {
                        iVar.Q.setAlpha(iVar.aa);
                    }
                    iVar.aa = 0.0f;
                    iVar.Y = false;
                    c a2 = a(iVar, iVar.O(), true, iVar.P());
                    if (a2 != null) {
                        b(iVar.Q, a2);
                        if (a2.f577a != null) {
                            iVar.Q.startAnimation(a2.f577a);
                        } else {
                            a2.f578b.setTarget(iVar.Q);
                            a2.f578b.start();
                        }
                    }
                }
            }
            if (iVar.Z) {
                d(iVar);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void e(i iVar, boolean z2) {
        if (this.o != null) {
            n k2 = this.o.k();
            if (k2 instanceof o) {
                ((o) k2).e(iVar, true);
            }
        }
        Iterator it = this.I.iterator();
        while (it.hasNext()) {
            j jVar = (j) it.next();
            if (!z2 || ((Boolean) jVar.f704b).booleanValue()) {
                ((android.support.v4.a.n.a) jVar.f703a).d(this, iVar);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void f(i iVar) {
        if (iVar.n < 0) {
            int i2 = this.d;
            this.d = i2 + 1;
            iVar.a(i2, this.o);
            if (this.f == null) {
                this.f = new SparseArray<>();
            }
            this.f.put(iVar.n, iVar);
            if (f564a) {
                Log.v("FragmentManager", "Allocated fragment index " + iVar);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void f(i iVar, boolean z2) {
        if (this.o != null) {
            n k2 = this.o.k();
            if (k2 instanceof o) {
                ((o) k2).f(iVar, true);
            }
        }
        Iterator it = this.I.iterator();
        while (it.hasNext()) {
            j jVar = (j) it.next();
            if (!z2 || ((Boolean) jVar.f704b).booleanValue()) {
                ((android.support.v4.a.n.a) jVar.f703a).e(this, iVar);
            }
        }
    }

    /* JADX INFO: finally extract failed */
    public boolean f() {
        c(true);
        boolean z2 = false;
        while (c(this.w, this.x)) {
            this.c = true;
            try {
                b(this.w, this.x);
                A();
                z2 = true;
            } catch (Throwable th) {
                A();
                throw th;
            }
        }
        g();
        D();
        return z2;
    }

    /* access modifiers changed from: 0000 */
    public void g() {
        if (this.v) {
            boolean z2 = false;
            for (int i2 = 0; i2 < this.f.size(); i2++) {
                i iVar = (i) this.f.valueAt(i2);
                if (!(iVar == null || iVar.U == null)) {
                    z2 |= iVar.U.a();
                }
            }
            if (!z2) {
                this.v = false;
                e();
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void g(i iVar) {
        if (iVar.n >= 0) {
            if (f564a) {
                Log.v("FragmentManager", "Freeing fragment index " + iVar);
            }
            this.f.put(iVar.n, null);
            this.m.a(iVar.o);
            iVar.r();
        }
    }

    /* access modifiers changed from: 0000 */
    public void g(i iVar, boolean z2) {
        if (this.o != null) {
            n k2 = this.o.k();
            if (k2 instanceof o) {
                ((o) k2).g(iVar, true);
            }
        }
        Iterator it = this.I.iterator();
        while (it.hasNext()) {
            j jVar = (j) it.next();
            if (!z2 || ((Boolean) jVar.f704b).booleanValue()) {
                ((android.support.v4.a.n.a) jVar.f703a).f(this, iVar);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void h() {
        if (this.k != null) {
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 < this.k.size()) {
                    ((android.support.v4.a.n.b) this.k.get(i3)).a();
                    i2 = i3 + 1;
                } else {
                    return;
                }
            }
        }
    }

    public void h(i iVar) {
        if (f564a) {
            Log.v("FragmentManager", "remove: " + iVar + " nesting=" + iVar.z);
        }
        boolean z2 = !iVar.g();
        if (!iVar.J || z2) {
            synchronized (this.e) {
                this.e.remove(iVar);
            }
            if (iVar.M && iVar.N) {
                this.r = true;
            }
            iVar.t = false;
            iVar.u = true;
        }
    }

    /* access modifiers changed from: 0000 */
    public void h(i iVar, boolean z2) {
        if (this.o != null) {
            n k2 = this.o.k();
            if (k2 instanceof o) {
                ((o) k2).h(iVar, true);
            }
        }
        Iterator it = this.I.iterator();
        while (it.hasNext()) {
            j jVar = (j) it.next();
            if (!z2 || ((Boolean) jVar.f704b).booleanValue()) {
                ((android.support.v4.a.n.a) jVar.f703a).g(this, iVar);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public p i() {
        a(this.C);
        return this.C;
    }

    public void i(i iVar) {
        boolean z2 = true;
        if (f564a) {
            Log.v("FragmentManager", "hide: " + iVar);
        }
        if (!iVar.I) {
            iVar.I = true;
            if (iVar.Z) {
                z2 = false;
            }
            iVar.Z = z2;
        }
    }

    /* access modifiers changed from: 0000 */
    public void j() {
        ArrayList arrayList;
        ArrayList arrayList2;
        ArrayList arrayList3;
        p pVar;
        if (this.f != null) {
            int i2 = 0;
            arrayList2 = null;
            arrayList = null;
            while (i2 < this.f.size()) {
                i iVar = (i) this.f.valueAt(i2);
                if (iVar != null) {
                    if (iVar.K) {
                        if (arrayList == null) {
                            arrayList = new ArrayList();
                        }
                        arrayList.add(iVar);
                        iVar.r = iVar.q != null ? iVar.q.n : -1;
                        if (f564a) {
                            Log.v("FragmentManager", "retainNonConfig: keeping retained " + iVar);
                        }
                    }
                    if (iVar.C != null) {
                        iVar.C.j();
                        pVar = iVar.C.C;
                    } else {
                        pVar = iVar.D;
                    }
                    if (arrayList2 == null && pVar != null) {
                        arrayList2 = new ArrayList(this.f.size());
                        for (int i3 = 0; i3 < i2; i3++) {
                            arrayList2.add(null);
                        }
                    }
                    arrayList3 = arrayList2;
                    if (arrayList3 != null) {
                        arrayList3.add(pVar);
                    }
                } else {
                    arrayList3 = arrayList2;
                }
                i2++;
                arrayList = arrayList;
                arrayList2 = arrayList3;
            }
        } else {
            arrayList2 = null;
            arrayList = null;
        }
        if (arrayList == null && arrayList2 == null) {
            this.C = null;
        } else {
            this.C = new p(arrayList, arrayList2);
        }
    }

    public void j(i iVar) {
        boolean z2 = false;
        if (f564a) {
            Log.v("FragmentManager", "show: " + iVar);
        }
        if (iVar.I) {
            iVar.I = false;
            if (!iVar.Z) {
                z2 = true;
            }
            iVar.Z = z2;
        }
    }

    /* access modifiers changed from: 0000 */
    public Parcelable k() {
        int[] iArr;
        boolean z2;
        d[] dVarArr = null;
        B();
        C();
        f();
        this.s = true;
        this.C = null;
        if (this.f == null || this.f.size() <= 0) {
            return null;
        }
        int size = this.f.size();
        r[] rVarArr = new r[size];
        int i2 = 0;
        boolean z3 = false;
        while (i2 < size) {
            i iVar = (i) this.f.valueAt(i2);
            if (iVar != null) {
                if (iVar.n < 0) {
                    a((RuntimeException) new IllegalStateException("Failure saving state: active " + iVar + " has cleared index: " + iVar.n));
                }
                r rVar = new r(iVar);
                rVarArr[i2] = rVar;
                if (iVar.k <= 0 || rVar.k != null) {
                    rVar.k = iVar.l;
                } else {
                    rVar.k = n(iVar);
                    if (iVar.q != null) {
                        if (iVar.q.n < 0) {
                            a((RuntimeException) new IllegalStateException("Failure saving state: " + iVar + " has target not in fragment manager: " + iVar.q));
                        }
                        if (rVar.k == null) {
                            rVar.k = new Bundle();
                        }
                        a(rVar.k, "android:target_state", iVar.q);
                        if (iVar.s != 0) {
                            rVar.k.putInt("android:target_req_state", iVar.s);
                        }
                    }
                }
                if (f564a) {
                    Log.v("FragmentManager", "Saved state of " + iVar + ": " + rVar.k);
                }
                z2 = true;
            } else {
                z2 = z3;
            }
            i2++;
            z3 = z2;
        }
        if (z3) {
            int size2 = this.e.size();
            if (size2 > 0) {
                iArr = new int[size2];
                for (int i3 = 0; i3 < size2; i3++) {
                    iArr[i3] = ((i) this.e.get(i3)).n;
                    if (iArr[i3] < 0) {
                        a((RuntimeException) new IllegalStateException("Failure saving state: active " + this.e.get(i3) + " has cleared index: " + iArr[i3]));
                    }
                    if (f564a) {
                        Log.v("FragmentManager", "saveAllState: adding fragment #" + i3 + ": " + this.e.get(i3));
                    }
                }
            } else {
                iArr = null;
            }
            if (this.g != null) {
                int size3 = this.g.size();
                if (size3 > 0) {
                    dVarArr = new d[size3];
                    for (int i4 = 0; i4 < size3; i4++) {
                        dVarArr[i4] = new d((c) this.g.get(i4));
                        if (f564a) {
                            Log.v("FragmentManager", "saveAllState: adding back stack #" + i4 + ": " + this.g.get(i4));
                        }
                    }
                }
            }
            q qVar = new q();
            qVar.f587a = rVarArr;
            qVar.f588b = iArr;
            qVar.c = dVarArr;
            if (this.p != null) {
                qVar.d = this.p.n;
            }
            qVar.e = this.d;
            j();
            return qVar;
        } else if (!f564a) {
            return null;
        } else {
            Log.v("FragmentManager", "saveAllState: no fragments!");
            return null;
        }
    }

    public void k(i iVar) {
        if (f564a) {
            Log.v("FragmentManager", "detach: " + iVar);
        }
        if (!iVar.J) {
            iVar.J = true;
            if (iVar.t) {
                if (f564a) {
                    Log.v("FragmentManager", "remove from detach: " + iVar);
                }
                synchronized (this.e) {
                    this.e.remove(iVar);
                }
                if (iVar.M && iVar.N) {
                    this.r = true;
                }
                iVar.t = false;
            }
        }
    }

    public void l() {
        this.C = null;
        this.s = false;
        int size = this.e.size();
        for (int i2 = 0; i2 < size; i2++) {
            i iVar = (i) this.e.get(i2);
            if (iVar != null) {
                iVar.F();
            }
        }
    }

    public void l(i iVar) {
        if (f564a) {
            Log.v("FragmentManager", "attach: " + iVar);
        }
        if (iVar.J) {
            iVar.J = false;
            if (iVar.t) {
                return;
            }
            if (this.e.contains(iVar)) {
                throw new IllegalStateException("Fragment already added: " + iVar);
            }
            if (f564a) {
                Log.v("FragmentManager", "add from attach: " + iVar);
            }
            synchronized (this.e) {
                this.e.add(iVar);
            }
            iVar.t = true;
            if (iVar.M && iVar.N) {
                this.r = true;
            }
        }
    }

    public void m() {
        this.s = false;
        e(1);
    }

    /* access modifiers changed from: 0000 */
    public void m(i iVar) {
        if (iVar.R != null) {
            if (this.A == null) {
                this.A = new SparseArray<>();
            } else {
                this.A.clear();
            }
            iVar.R.saveHierarchyState(this.A);
            if (this.A.size() > 0) {
                iVar.m = this.A;
                this.A = null;
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public Bundle n(i iVar) {
        Bundle bundle;
        if (this.z == null) {
            this.z = new Bundle();
        }
        iVar.n(this.z);
        d(iVar, this.z, false);
        if (!this.z.isEmpty()) {
            bundle = this.z;
            this.z = null;
        } else {
            bundle = null;
        }
        if (iVar.Q != null) {
            m(iVar);
        }
        if (iVar.m != null) {
            if (bundle == null) {
                bundle = new Bundle();
            }
            bundle.putSparseParcelableArray("android:view_state", iVar.m);
        }
        if (!iVar.T) {
            if (bundle == null) {
                bundle = new Bundle();
            }
            bundle.putBoolean("android:user_visible_hint", iVar.T);
        }
        return bundle;
    }

    public void n() {
        this.s = false;
        e(2);
    }

    public void o() {
        this.s = false;
        e(4);
    }

    public void o(i iVar) {
        if (iVar == null || (this.f.get(iVar.n) == iVar && (iVar.B == null || iVar.k() == this))) {
            this.p = iVar;
            return;
        }
        throw new IllegalArgumentException("Fragment " + iVar + " is not an active fragment of FragmentManager " + this);
    }

    public View onCreateView(View view, String str, Context context, AttributeSet attributeSet) {
        i iVar;
        if (!"fragment".equals(str)) {
            return null;
        }
        String attributeValue = attributeSet.getAttributeValue(null, "class");
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, e.f580a);
        String str2 = attributeValue == null ? obtainStyledAttributes.getString(0) : attributeValue;
        int resourceId = obtainStyledAttributes.getResourceId(1, -1);
        String string = obtainStyledAttributes.getString(2);
        obtainStyledAttributes.recycle();
        if (!i.a(this.m.g(), str2)) {
            return null;
        }
        int i2 = view != null ? view.getId() : 0;
        if (i2 == -1 && resourceId == -1 && string == null) {
            throw new IllegalArgumentException(attributeSet.getPositionDescription() + ": Must specify unique android:id, android:tag, or have a parent with an id for " + str2);
        }
        i iVar2 = resourceId != -1 ? b(resourceId) : null;
        if (iVar2 == null && string != null) {
            iVar2 = a(string);
        }
        if (iVar2 == null && i2 != -1) {
            iVar2 = b(i2);
        }
        if (f564a) {
            Log.v("FragmentManager", "onCreateView: id=0x" + Integer.toHexString(resourceId) + " fname=" + str2 + " existing=" + iVar2);
        }
        if (iVar2 == null) {
            i a2 = this.n.a(context, str2, null);
            a2.v = true;
            a2.F = resourceId != 0 ? resourceId : i2;
            a2.G = i2;
            a2.H = string;
            a2.w = true;
            a2.A = this;
            a2.B = this.m;
            a2.a(this.m.g(), attributeSet, a2.l);
            a(a2, true);
            iVar = a2;
        } else if (iVar2.w) {
            throw new IllegalArgumentException(attributeSet.getPositionDescription() + ": Duplicate id 0x" + Integer.toHexString(resourceId) + ", tag " + string + ", or parent id 0x" + Integer.toHexString(i2) + " with another fragment for " + str2);
        } else {
            iVar2.w = true;
            iVar2.B = this.m;
            if (!iVar2.L) {
                iVar2.a(this.m.g(), attributeSet, iVar2.l);
            }
            iVar = iVar2;
        }
        if (this.l >= 1 || !iVar.v) {
            b(iVar);
        } else {
            a(iVar, 1, 0, 0, false);
        }
        if (iVar.Q == null) {
            throw new IllegalStateException("Fragment " + str2 + " did not create a view.");
        }
        if (resourceId != 0) {
            iVar.Q.setId(resourceId);
        }
        if (iVar.Q.getTag() == null) {
            iVar.Q.setTag(string);
        }
        return iVar.Q;
    }

    public View onCreateView(String str, Context context, AttributeSet attributeSet) {
        return onCreateView(null, str, context, attributeSet);
    }

    public void p() {
        this.s = false;
        e(5);
    }

    public void q() {
        e(4);
    }

    public void r() {
        this.s = true;
        e(3);
    }

    public void s() {
        e(2);
    }

    public void t() {
        e(1);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(128);
        sb.append("FragmentManager{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        sb.append(" in ");
        if (this.o != null) {
            android.support.v4.h.d.a(this.o, sb);
        } else {
            android.support.v4.h.d.a(this.m, sb);
        }
        sb.append("}}");
        return sb.toString();
    }

    public void u() {
        this.t = true;
        f();
        e(0);
        this.m = null;
        this.n = null;
        this.o = null;
    }

    public void v() {
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < this.e.size()) {
                i iVar = (i) this.e.get(i3);
                if (iVar != null) {
                    iVar.G();
                }
                i2 = i3 + 1;
            } else {
                return;
            }
        }
    }

    public i w() {
        return this.p;
    }

    /* access modifiers changed from: 0000 */
    public Factory2 x() {
        return this;
    }
}
