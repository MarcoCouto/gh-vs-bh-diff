package android.support.v4.a;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import java.io.FileDescriptor;
import java.io.PrintWriter;

public abstract class m<E> extends k {

    /* renamed from: a reason: collision with root package name */
    private final Activity f562a;

    /* renamed from: b reason: collision with root package name */
    final Context f563b;
    final int c;
    final o d;
    private final Handler e;
    private android.support.v4.h.m<String, w> f;
    private boolean g;
    private x h;
    private boolean i;
    private boolean j;

    m(Activity activity, Context context, Handler handler, int i2) {
        this.d = new o();
        this.f562a = activity;
        this.f563b = context;
        this.e = handler;
        this.c = i2;
    }

    m(j jVar) {
        this(jVar, jVar, jVar.c, 0);
    }

    /* access modifiers changed from: 0000 */
    public x a(String str, boolean z, boolean z2) {
        if (this.f == null) {
            this.f = new android.support.v4.h.m<>();
        }
        x xVar = (x) this.f.get(str);
        if (xVar == null && z2) {
            x xVar2 = new x(str, this, z);
            this.f.put(str, xVar2);
            return xVar2;
        } else if (!z || xVar == null || xVar.e) {
            return xVar;
        } else {
            xVar.b();
            return xVar;
        }
    }

    public View a(int i2) {
        return null;
    }

    public void a(i iVar, Intent intent, int i2, Bundle bundle) {
        if (i2 != -1) {
            throw new IllegalStateException("Starting activity with a requestCode requires a FragmentActivity host");
        }
        this.f563b.startActivity(intent);
    }

    /* access modifiers changed from: 0000 */
    public void a(android.support.v4.h.m<String, w> mVar) {
        if (mVar != null) {
            int size = mVar.size();
            for (int i2 = 0; i2 < size; i2++) {
                ((x) mVar.c(i2)).a(this);
            }
        }
        this.f = mVar;
    }

    /* access modifiers changed from: 0000 */
    public void a(String str) {
        if (this.f != null) {
            x xVar = (x) this.f.get(str);
            if (xVar != null && !xVar.f) {
                xVar.h();
                this.f.remove(str);
            }
        }
    }

    public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
    }

    /* access modifiers changed from: 0000 */
    public void a(boolean z) {
        this.g = z;
        if (this.h != null && this.j) {
            this.j = false;
            if (z) {
                this.h.d();
            } else {
                this.h.c();
            }
        }
    }

    public boolean a() {
        return true;
    }

    public boolean a(i iVar) {
        return true;
    }

    public LayoutInflater b() {
        return LayoutInflater.from(this.f563b);
    }

    /* access modifiers changed from: 0000 */
    public void b(i iVar) {
    }

    /* access modifiers changed from: 0000 */
    public void b(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        printWriter.print(str);
        printWriter.print("mLoadersStarted=");
        printWriter.println(this.j);
        if (this.h != null) {
            printWriter.print(str);
            printWriter.print("Loader Manager ");
            printWriter.print(Integer.toHexString(System.identityHashCode(this.h)));
            printWriter.println(":");
            this.h.a(str + "  ", fileDescriptor, printWriter, strArr);
        }
    }

    public void c() {
    }

    public boolean d() {
        return true;
    }

    public int e() {
        return this.c;
    }

    /* access modifiers changed from: 0000 */
    public Activity f() {
        return this.f562a;
    }

    /* access modifiers changed from: 0000 */
    public Context g() {
        return this.f563b;
    }

    /* access modifiers changed from: 0000 */
    public Handler h() {
        return this.e;
    }

    /* access modifiers changed from: 0000 */
    public o i() {
        return this.d;
    }

    /* access modifiers changed from: 0000 */
    public boolean j() {
        return this.g;
    }

    /* access modifiers changed from: 0000 */
    public void k() {
        if (!this.j) {
            this.j = true;
            if (this.h != null) {
                this.h.b();
            } else if (!this.i) {
                this.h = a("(root)", this.j, false);
                if (this.h != null && !this.h.e) {
                    this.h.b();
                }
            }
            this.i = true;
        }
    }

    /* access modifiers changed from: 0000 */
    public void l() {
        if (this.h != null) {
            this.h.h();
        }
    }

    /* access modifiers changed from: 0000 */
    public void m() {
        if (this.f != null) {
            int size = this.f.size();
            x[] xVarArr = new x[size];
            for (int i2 = size - 1; i2 >= 0; i2--) {
                xVarArr[i2] = (x) this.f.c(i2);
            }
            for (int i3 = 0; i3 < size; i3++) {
                x xVar = xVarArr[i3];
                xVar.e();
                xVar.g();
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public android.support.v4.h.m<String, w> n() {
        boolean z;
        if (this.f != null) {
            int size = this.f.size();
            x[] xVarArr = new x[size];
            for (int i2 = size - 1; i2 >= 0; i2--) {
                xVarArr[i2] = (x) this.f.c(i2);
            }
            boolean j2 = j();
            z = false;
            for (int i3 = 0; i3 < size; i3++) {
                x xVar = xVarArr[i3];
                if (!xVar.f && j2) {
                    if (!xVar.e) {
                        xVar.b();
                    }
                    xVar.d();
                }
                if (xVar.f) {
                    z = true;
                } else {
                    xVar.h();
                    this.f.remove(xVar.d);
                }
            }
        } else {
            z = false;
        }
        if (z) {
            return this.f;
        }
        return null;
    }
}
