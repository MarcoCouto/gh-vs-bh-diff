package android.support.v4.a;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.util.Log;
import java.util.ArrayList;
import java.util.Iterator;

public final class aj implements Iterable<Intent> {

    /* renamed from: a reason: collision with root package name */
    private static final c f538a;

    /* renamed from: b reason: collision with root package name */
    private final ArrayList<Intent> f539b = new ArrayList<>();
    private final Context c;

    public interface a {
        Intent a_();
    }

    static class b extends c {
        b() {
        }
    }

    static class c {
        c() {
        }
    }

    static {
        if (VERSION.SDK_INT >= 16) {
            f538a = new b();
        } else {
            f538a = new c();
        }
    }

    private aj(Context context) {
        this.c = context;
    }

    public static aj a(Context context) {
        return new aj(context);
    }

    public aj a(Activity activity) {
        Intent intent = null;
        if (activity instanceof a) {
            intent = ((a) activity).a_();
        }
        Intent intent2 = intent == null ? y.a(activity) : intent;
        if (intent2 != null) {
            ComponentName component = intent2.getComponent();
            if (component == null) {
                component = intent2.resolveActivity(this.c.getPackageManager());
            }
            a(component);
            a(intent2);
        }
        return this;
    }

    public aj a(ComponentName componentName) {
        int size = this.f539b.size();
        try {
            Intent a2 = y.a(this.c, componentName);
            while (a2 != null) {
                this.f539b.add(size, a2);
                a2 = y.a(this.c, a2.getComponent());
            }
            return this;
        } catch (NameNotFoundException e) {
            Log.e("TaskStackBuilder", "Bad ComponentName while traversing activity parent metadata");
            throw new IllegalArgumentException(e);
        }
    }

    public aj a(Intent intent) {
        this.f539b.add(intent);
        return this;
    }

    public void a() {
        a((Bundle) null);
    }

    public void a(Bundle bundle) {
        if (this.f539b.isEmpty()) {
            throw new IllegalStateException("No intents added to TaskStackBuilder; cannot startActivities");
        }
        Intent[] intentArr = (Intent[]) this.f539b.toArray(new Intent[this.f539b.size()]);
        intentArr[0] = new Intent(intentArr[0]).addFlags(268484608);
        if (!android.support.v4.b.a.a(this.c, intentArr, bundle)) {
            Intent intent = new Intent(intentArr[intentArr.length - 1]);
            intent.addFlags(268435456);
            this.c.startActivity(intent);
        }
    }

    @Deprecated
    public Iterator<Intent> iterator() {
        return this.f539b.iterator();
    }
}
