package android.support.v4.a;

import android.app.Notification;
import android.app.Notification.BigTextStyle;
import android.app.PendingIntent;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.widget.RemoteViews;
import java.util.ArrayList;

public class aa {

    public static class a {

        /* renamed from: a reason: collision with root package name */
        final Bundle f521a;

        /* renamed from: b reason: collision with root package name */
        public int f522b;
        public CharSequence c;
        public PendingIntent d;
        private final ae[] e;
        private final ae[] f;
        private boolean g;

        public int a() {
            return this.f522b;
        }

        public CharSequence b() {
            return this.c;
        }

        public PendingIntent c() {
            return this.d;
        }

        public Bundle d() {
            return this.f521a;
        }

        public boolean e() {
            return this.g;
        }

        public ae[] f() {
            return this.e;
        }

        public ae[] g() {
            return this.f;
        }
    }

    public static class b extends d {
        private CharSequence e;

        public b a(CharSequence charSequence) {
            this.e = c.d(charSequence);
            return this;
        }

        public void a(z zVar) {
            if (VERSION.SDK_INT >= 16) {
                BigTextStyle bigText = new BigTextStyle(zVar.a()).setBigContentTitle(this.f526b).bigText(this.e);
                if (this.d) {
                    bigText.setSummaryText(this.c);
                }
            }
        }
    }

    public static class c {
        Bundle A;
        int B;
        int C;
        Notification D;
        RemoteViews E;
        RemoteViews F;
        RemoteViews G;
        String H;
        int I;
        String J;
        long K;
        int L;
        Notification M;
        @Deprecated
        public ArrayList<String> N;

        /* renamed from: a reason: collision with root package name */
        public Context f523a;

        /* renamed from: b reason: collision with root package name */
        public ArrayList<a> f524b;
        CharSequence c;
        CharSequence d;
        PendingIntent e;
        PendingIntent f;
        RemoteViews g;
        Bitmap h;
        CharSequence i;
        int j;
        int k;
        boolean l;
        boolean m;
        d n;
        CharSequence o;
        CharSequence[] p;
        int q;
        int r;
        boolean s;
        String t;
        boolean u;
        String v;
        boolean w;
        boolean x;
        boolean y;
        String z;

        @Deprecated
        public c(Context context) {
            this(context, null);
        }

        public c(Context context, String str) {
            this.f524b = new ArrayList<>();
            this.l = true;
            this.w = false;
            this.B = 0;
            this.C = 0;
            this.I = 0;
            this.L = 0;
            this.M = new Notification();
            this.f523a = context;
            this.H = str;
            this.M.when = System.currentTimeMillis();
            this.M.audioStreamType = -1;
            this.k = 0;
            this.N = new ArrayList<>();
        }

        private void a(int i2, boolean z2) {
            if (z2) {
                this.M.flags |= i2;
                return;
            }
            this.M.flags &= i2 ^ -1;
        }

        protected static CharSequence d(CharSequence charSequence) {
            return (charSequence != null && charSequence.length() > 5120) ? charSequence.subSequence(0, 5120) : charSequence;
        }

        public Notification a() {
            return new ab(this).b();
        }

        public c a(int i2) {
            this.M.icon = i2;
            return this;
        }

        public c a(int i2, int i3, int i4) {
            int i5 = 1;
            this.M.ledARGB = i2;
            this.M.ledOnMS = i3;
            this.M.ledOffMS = i4;
            boolean z2 = (this.M.ledOnMS == 0 || this.M.ledOffMS == 0) ? false : true;
            Notification notification = this.M;
            int i6 = this.M.flags & -2;
            if (!z2) {
                i5 = 0;
            }
            notification.flags = i6 | i5;
            return this;
        }

        public c a(long j2) {
            this.M.when = j2;
            return this;
        }

        public c a(PendingIntent pendingIntent) {
            this.e = pendingIntent;
            return this;
        }

        public c a(d dVar) {
            if (this.n != dVar) {
                this.n = dVar;
                if (this.n != null) {
                    this.n.a(this);
                }
            }
            return this;
        }

        public c a(CharSequence charSequence) {
            this.c = d(charSequence);
            return this;
        }

        public c a(String str) {
            this.H = str;
            return this;
        }

        public c a(boolean z2) {
            a(2, z2);
            return this;
        }

        public c a(long[] jArr) {
            this.M.vibrate = jArr;
            return this;
        }

        public c b(int i2) {
            this.k = i2;
            return this;
        }

        public c b(CharSequence charSequence) {
            this.d = d(charSequence);
            return this;
        }

        public c b(boolean z2) {
            a(16, z2);
            return this;
        }

        public c c(int i2) {
            this.C = i2;
            return this;
        }

        public c c(CharSequence charSequence) {
            this.M.tickerText = d(charSequence);
            return this;
        }

        public c c(boolean z2) {
            this.w = z2;
            return this;
        }
    }

    public static abstract class d {

        /* renamed from: a reason: collision with root package name */
        protected c f525a;

        /* renamed from: b reason: collision with root package name */
        CharSequence f526b;
        CharSequence c;
        boolean d = false;

        public void a(Bundle bundle) {
        }

        public void a(c cVar) {
            if (this.f525a != cVar) {
                this.f525a = cVar;
                if (this.f525a != null) {
                    this.f525a.a(this);
                }
            }
        }

        public void a(z zVar) {
        }

        public RemoteViews b(z zVar) {
            return null;
        }

        public RemoteViews c(z zVar) {
            return null;
        }

        public RemoteViews d(z zVar) {
            return null;
        }
    }

    public static Bundle a(Notification notification) {
        if (VERSION.SDK_INT >= 19) {
            return notification.extras;
        }
        if (VERSION.SDK_INT >= 16) {
            return ac.a(notification);
        }
        return null;
    }
}
