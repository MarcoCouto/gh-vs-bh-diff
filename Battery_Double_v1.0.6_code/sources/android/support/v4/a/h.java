package android.support.v4.a;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnDismissListener;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

public class h extends i implements OnCancelListener, OnDismissListener {

    /* renamed from: a reason: collision with root package name */
    int f550a = 0;

    /* renamed from: b reason: collision with root package name */
    int f551b = 0;
    boolean c = true;
    boolean d = true;
    int e = -1;
    Dialog f;
    boolean g;
    boolean h;
    boolean i;

    public void a(Dialog dialog, int i2) {
        switch (i2) {
            case 1:
            case 2:
                break;
            case 3:
                dialog.getWindow().addFlags(24);
                break;
            default:
                return;
        }
        dialog.requestWindowFeature(1);
    }

    public void a(Context context) {
        super.a(context);
        if (!this.i) {
            this.h = false;
        }
    }

    public void a(Bundle bundle) {
        super.a(bundle);
        this.d = this.G == 0;
        if (bundle != null) {
            this.f550a = bundle.getInt("android:style", 0);
            this.f551b = bundle.getInt("android:theme", 0);
            this.c = bundle.getBoolean("android:cancelable", true);
            this.d = bundle.getBoolean("android:showsDialog", this.d);
            this.e = bundle.getInt("android:backStackId", -1);
        }
    }

    public void a(n nVar, String str) {
        this.h = false;
        this.i = true;
        s a2 = nVar.a();
        a2.a(this, str);
        a2.b();
    }

    /* access modifiers changed from: 0000 */
    public void a(boolean z) {
        if (!this.h) {
            this.h = true;
            this.i = false;
            if (this.f != null) {
                this.f.dismiss();
                this.f = null;
            }
            this.g = true;
            if (this.e >= 0) {
                k().a(this.e, 1);
                this.e = -1;
                return;
            }
            s a2 = k().a();
            a2.a(this);
            if (z) {
                a2.c();
            } else {
                a2.b();
            }
        }
    }

    public int b() {
        return this.f551b;
    }

    public LayoutInflater b(Bundle bundle) {
        if (!this.d) {
            return super.b(bundle);
        }
        this.f = c(bundle);
        if (this.f == null) {
            return (LayoutInflater) this.B.g().getSystemService("layout_inflater");
        }
        a(this.f, this.f550a);
        return (LayoutInflater) this.f.getContext().getSystemService("layout_inflater");
    }

    public void b(boolean z) {
        this.d = z;
    }

    public Dialog c(Bundle bundle) {
        return new Dialog(i(), b());
    }

    public void c() {
        super.c();
        if (!this.i && !this.h) {
            this.h = true;
        }
    }

    public void d() {
        super.d();
        if (this.f != null) {
            this.g = false;
            this.f.show();
        }
    }

    public void d(Bundle bundle) {
        super.d(bundle);
        if (this.d) {
            View n = n();
            if (n != null) {
                if (n.getParent() != null) {
                    throw new IllegalStateException("DialogFragment can not be attached to a container view");
                }
                this.f.setContentView(n);
            }
            j i2 = i();
            if (i2 != null) {
                this.f.setOwnerActivity(i2);
            }
            this.f.setCancelable(this.c);
            this.f.setOnCancelListener(this);
            this.f.setOnDismissListener(this);
            if (bundle != null) {
                Bundle bundle2 = bundle.getBundle("android:savedDialogState");
                if (bundle2 != null) {
                    this.f.onRestoreInstanceState(bundle2);
                }
            }
        }
    }

    public void e() {
        super.e();
        if (this.f != null) {
            this.f.hide();
        }
    }

    public void e(Bundle bundle) {
        super.e(bundle);
        if (this.f != null) {
            Bundle onSaveInstanceState = this.f.onSaveInstanceState();
            if (onSaveInstanceState != null) {
                bundle.putBundle("android:savedDialogState", onSaveInstanceState);
            }
        }
        if (this.f550a != 0) {
            bundle.putInt("android:style", this.f550a);
        }
        if (this.f551b != 0) {
            bundle.putInt("android:theme", this.f551b);
        }
        if (!this.c) {
            bundle.putBoolean("android:cancelable", this.c);
        }
        if (!this.d) {
            bundle.putBoolean("android:showsDialog", this.d);
        }
        if (this.e != -1) {
            bundle.putInt("android:backStackId", this.e);
        }
    }

    public void f() {
        super.f();
        if (this.f != null) {
            this.g = true;
            this.f.dismiss();
            this.f = null;
        }
    }

    public void onCancel(DialogInterface dialogInterface) {
    }

    public void onDismiss(DialogInterface dialogInterface) {
        if (!this.g) {
            a(true);
        }
    }
}
