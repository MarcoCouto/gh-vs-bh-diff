package android.support.v4.a;

import android.app.RemoteInput;
import android.app.RemoteInput.Builder;
import android.os.Bundle;
import android.support.v4.a.af.a;
import java.util.Set;

public final class ae extends a {

    /* renamed from: a reason: collision with root package name */
    private final String f533a;

    /* renamed from: b reason: collision with root package name */
    private final CharSequence f534b;
    private final CharSequence[] c;
    private final boolean d;
    private final Bundle e;
    private final Set<String> f;

    static RemoteInput a(ae aeVar) {
        return new Builder(aeVar.a()).setLabel(aeVar.b()).setChoices(aeVar.c()).setAllowFreeFormInput(aeVar.e()).addExtras(aeVar.f()).build();
    }

    static RemoteInput[] a(ae[] aeVarArr) {
        if (aeVarArr == null) {
            return null;
        }
        RemoteInput[] remoteInputArr = new RemoteInput[aeVarArr.length];
        for (int i = 0; i < aeVarArr.length; i++) {
            remoteInputArr[i] = a(aeVarArr[i]);
        }
        return remoteInputArr;
    }

    public String a() {
        return this.f533a;
    }

    public CharSequence b() {
        return this.f534b;
    }

    public CharSequence[] c() {
        return this.c;
    }

    public Set<String> d() {
        return this.f;
    }

    public boolean e() {
        return this.d;
    }

    public Bundle f() {
        return this.e;
    }
}
