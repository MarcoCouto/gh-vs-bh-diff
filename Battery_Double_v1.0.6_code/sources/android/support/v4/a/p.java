package android.support.v4.a;

import java.util.List;

public class p {

    /* renamed from: a reason: collision with root package name */
    private final List<i> f585a;

    /* renamed from: b reason: collision with root package name */
    private final List<p> f586b;

    p(List<i> list, List<p> list2) {
        this.f585a = list;
        this.f586b = list2;
    }

    /* access modifiers changed from: 0000 */
    public List<i> a() {
        return this.f585a;
    }

    /* access modifiers changed from: 0000 */
    public List<p> b() {
        return this.f586b;
    }
}
