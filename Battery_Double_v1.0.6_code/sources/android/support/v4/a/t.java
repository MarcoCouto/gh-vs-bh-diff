package android.support.v4.a;

import android.graphics.Rect;
import android.os.Build.VERSION;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

class t {

    /* renamed from: a reason: collision with root package name */
    private static final int[] f591a = {0, 3, 0, 1, 5, 4, 7, 6, 9, 8};

    /* renamed from: b reason: collision with root package name */
    private static final v f592b = (VERSION.SDK_INT >= 21 ? new u() : null);
    private static final v c = a();

    static class a {

        /* renamed from: a reason: collision with root package name */
        public i f600a;

        /* renamed from: b reason: collision with root package name */
        public boolean f601b;
        public c c;
        public i d;
        public boolean e;
        public c f;

        a() {
        }
    }

    private static a a(a aVar, SparseArray<a> sparseArray, int i) {
        if (aVar != null) {
            return aVar;
        }
        a aVar2 = new a();
        sparseArray.put(i, aVar2);
        return aVar2;
    }

    private static v a() {
        try {
            return (v) Class.forName("android.support.transition.FragmentTransitionSupport").getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception e) {
            return null;
        }
    }

    private static v a(i iVar, i iVar2) {
        ArrayList arrayList = new ArrayList();
        if (iVar != null) {
            Object v = iVar.v();
            if (v != null) {
                arrayList.add(v);
            }
            Object u = iVar.u();
            if (u != null) {
                arrayList.add(u);
            }
            Object y = iVar.y();
            if (y != null) {
                arrayList.add(y);
            }
        }
        if (iVar2 != null) {
            Object t = iVar2.t();
            if (t != null) {
                arrayList.add(t);
            }
            Object w = iVar2.w();
            if (w != null) {
                arrayList.add(w);
            }
            Object x = iVar2.x();
            if (x != null) {
                arrayList.add(x);
            }
        }
        if (arrayList.isEmpty()) {
            return null;
        }
        if (f592b != null && a(f592b, (List<Object>) arrayList)) {
            return f592b;
        }
        if (c != null && a(c, (List<Object>) arrayList)) {
            return c;
        }
        if (f592b == null && c == null) {
            return null;
        }
        throw new IllegalArgumentException("Invalid Transition types");
    }

    private static android.support.v4.h.a<String, String> a(int i, ArrayList<c> arrayList, ArrayList<Boolean> arrayList2, int i2, int i3) {
        ArrayList<String> arrayList3;
        ArrayList<String> arrayList4;
        android.support.v4.h.a<String, String> aVar = new android.support.v4.h.a<>();
        for (int i4 = i3 - 1; i4 >= i2; i4--) {
            c cVar = (c) arrayList.get(i4);
            if (cVar.b(i)) {
                boolean booleanValue = ((Boolean) arrayList2.get(i4)).booleanValue();
                if (cVar.r != null) {
                    int size = cVar.r.size();
                    if (booleanValue) {
                        arrayList3 = cVar.r;
                        arrayList4 = cVar.s;
                    } else {
                        ArrayList<String> arrayList5 = cVar.r;
                        arrayList3 = cVar.s;
                        arrayList4 = arrayList5;
                    }
                    for (int i5 = 0; i5 < size; i5++) {
                        String str = (String) arrayList4.get(i5);
                        String str2 = (String) arrayList3.get(i5);
                        String str3 = (String) aVar.remove(str2);
                        if (str3 != null) {
                            aVar.put(str, str3);
                        } else {
                            aVar.put(str, str2);
                        }
                    }
                }
            }
        }
        return aVar;
    }

    private static Object a(v vVar, i iVar, i iVar2, boolean z) {
        if (iVar == null || iVar2 == null) {
            return null;
        }
        return vVar.c(vVar.b(z ? iVar2.y() : iVar.x()));
    }

    private static Object a(v vVar, i iVar, boolean z) {
        if (iVar == null) {
            return null;
        }
        return vVar.b(z ? iVar.w() : iVar.t());
    }

    private static Object a(v vVar, ViewGroup viewGroup, View view, android.support.v4.h.a<String, String> aVar, a aVar2, ArrayList<View> arrayList, ArrayList<View> arrayList2, Object obj, Object obj2) {
        Object obj3;
        final Rect rect;
        final View view2;
        i iVar = aVar2.f600a;
        i iVar2 = aVar2.d;
        if (iVar != null) {
            iVar.n().setVisibility(0);
        }
        if (iVar == null || iVar2 == null) {
            return null;
        }
        boolean z = aVar2.f601b;
        Object a2 = aVar.isEmpty() ? null : a(vVar, iVar, iVar2, z);
        android.support.v4.h.a b2 = b(vVar, aVar, a2, aVar2);
        final android.support.v4.h.a c2 = c(vVar, aVar, a2, aVar2);
        if (aVar.isEmpty()) {
            obj3 = null;
            if (b2 != null) {
                b2.clear();
            }
            if (c2 != null) {
                c2.clear();
            }
        } else {
            a(arrayList, b2, (Collection<String>) aVar.keySet());
            a(arrayList2, c2, aVar.values());
            obj3 = a2;
        }
        if (obj == null && obj2 == null && obj3 == null) {
            return null;
        }
        b(iVar, iVar2, z, b2, true);
        if (obj3 != null) {
            arrayList2.add(view);
            vVar.a(obj3, view, arrayList);
            a(vVar, obj3, obj2, b2, aVar2.e, aVar2.f);
            rect = new Rect();
            view2 = b(c2, aVar2, obj, z);
            if (view2 != null) {
                vVar.a(obj, rect);
            }
        } else {
            rect = null;
            view2 = null;
        }
        final i iVar3 = iVar;
        final i iVar4 = iVar2;
        final boolean z2 = z;
        final v vVar2 = vVar;
        ad.a(viewGroup, new Runnable() {
            public void run() {
                t.b(iVar3, iVar4, z2, c2, false);
                if (view2 != null) {
                    vVar2.a(view2, rect);
                }
            }
        });
        return obj3;
    }

    private static Object a(v vVar, Object obj, Object obj2, Object obj3, i iVar, boolean z) {
        boolean z2 = true;
        if (!(obj == null || obj2 == null || iVar == null)) {
            z2 = z ? iVar.A() : iVar.z();
        }
        return z2 ? vVar.a(obj2, obj, obj3) : vVar.b(obj2, obj, obj3);
    }

    private static String a(android.support.v4.h.a<String, String> aVar, String str) {
        int size = aVar.size();
        for (int i = 0; i < size; i++) {
            if (str.equals(aVar.c(i))) {
                return (String) aVar.b(i);
            }
        }
        return null;
    }

    private static void a(c cVar, a aVar, SparseArray<a> sparseArray, boolean z, boolean z2) {
        boolean z3;
        boolean z4;
        boolean z5;
        boolean z6;
        a aVar2;
        a aVar3;
        i iVar = aVar.f543b;
        if (iVar != null) {
            int i = iVar.G;
            if (i != 0) {
                switch (z ? f591a[aVar.f542a] : aVar.f542a) {
                    case 1:
                    case 7:
                        boolean z7 = z2 ? iVar.Y : !iVar.t && !iVar.I;
                        z3 = true;
                        z4 = false;
                        z5 = false;
                        z6 = z7;
                        break;
                    case 3:
                    case 6:
                        boolean z8 = z2 ? !iVar.t && iVar.Q != null && iVar.Q.getVisibility() == 0 && iVar.aa >= 0.0f : iVar.t && !iVar.I;
                        z3 = false;
                        z4 = z8;
                        z5 = true;
                        z6 = false;
                        break;
                    case 4:
                        boolean z9 = z2 ? iVar.Z && iVar.t && iVar.I : iVar.t && !iVar.I;
                        z3 = false;
                        z4 = z9;
                        z5 = true;
                        z6 = false;
                        break;
                    case 5:
                        boolean z10 = z2 ? iVar.Z && !iVar.I && iVar.t : iVar.I;
                        z3 = true;
                        z4 = false;
                        z5 = false;
                        z6 = z10;
                        break;
                    default:
                        z3 = false;
                        z4 = false;
                        z5 = false;
                        z6 = false;
                        break;
                }
                a aVar4 = (a) sparseArray.get(i);
                if (z6) {
                    aVar2 = a(aVar4, sparseArray, i);
                    aVar2.f600a = iVar;
                    aVar2.f601b = z;
                    aVar2.c = cVar;
                } else {
                    aVar2 = aVar4;
                }
                if (!z2 && z3) {
                    if (aVar2 != null && aVar2.d == iVar) {
                        aVar2.d = null;
                    }
                    o oVar = cVar.f540a;
                    if (iVar.k < 1 && oVar.l >= 1 && !cVar.t) {
                        oVar.f(iVar);
                        oVar.a(iVar, 1, 0, 0, false);
                    }
                }
                if (!z4 || !(aVar2 == null || aVar2.d == null)) {
                    aVar3 = aVar2;
                } else {
                    aVar3 = a(aVar2, sparseArray, i);
                    aVar3.d = iVar;
                    aVar3.e = z;
                    aVar3.f = cVar;
                }
                if (!z2 && z5 && aVar3 != null && aVar3.f600a == iVar) {
                    aVar3.f600a = null;
                }
            }
        }
    }

    public static void a(c cVar, SparseArray<a> sparseArray, boolean z) {
        int size = cVar.f541b.size();
        for (int i = 0; i < size; i++) {
            a(cVar, (a) cVar.f541b.get(i), sparseArray, false, z);
        }
    }

    private static void a(o oVar, int i, a aVar, View view, android.support.v4.h.a<String, String> aVar2) {
        ViewGroup viewGroup = null;
        if (oVar.n.a()) {
            viewGroup = (ViewGroup) oVar.n.a(i);
        }
        if (viewGroup != null) {
            i iVar = aVar.f600a;
            i iVar2 = aVar.d;
            v a2 = a(iVar2, iVar);
            if (a2 != null) {
                boolean z = aVar.f601b;
                boolean z2 = aVar.e;
                ArrayList arrayList = new ArrayList();
                ArrayList arrayList2 = new ArrayList();
                Object a3 = a(a2, iVar, z);
                Object b2 = b(a2, iVar2, z2);
                Object a4 = a(a2, viewGroup, view, aVar2, aVar, arrayList2, arrayList, a3, b2);
                if (a3 != null || a4 != null || b2 != null) {
                    ArrayList b3 = b(a2, b2, iVar2, arrayList2, view);
                    ArrayList b4 = b(a2, a3, iVar, arrayList, view);
                    b(b4, 4);
                    Object a5 = a(a2, a3, b2, a4, iVar, z);
                    if (a5 != null) {
                        a(a2, b2, iVar2, b3);
                        ArrayList a6 = a2.a(arrayList);
                        a2.a(a5, a3, b4, b2, b3, a4, arrayList);
                        a2.a(viewGroup, a5);
                        a2.a(viewGroup, arrayList2, arrayList, a6, aVar2);
                        b(b4, 0);
                        a2.a(a4, arrayList2, arrayList);
                    }
                }
            }
        }
    }

    static void a(o oVar, ArrayList<c> arrayList, ArrayList<Boolean> arrayList2, int i, int i2, boolean z) {
        if (oVar.l >= 1) {
            SparseArray sparseArray = new SparseArray();
            for (int i3 = i; i3 < i2; i3++) {
                c cVar = (c) arrayList.get(i3);
                if (((Boolean) arrayList2.get(i3)).booleanValue()) {
                    b(cVar, sparseArray, z);
                } else {
                    a(cVar, sparseArray, z);
                }
            }
            if (sparseArray.size() != 0) {
                View view = new View(oVar.m.g());
                int size = sparseArray.size();
                for (int i4 = 0; i4 < size; i4++) {
                    int keyAt = sparseArray.keyAt(i4);
                    android.support.v4.h.a a2 = a(keyAt, arrayList, arrayList2, i, i2);
                    a aVar = (a) sparseArray.valueAt(i4);
                    if (z) {
                        a(oVar, keyAt, aVar, view, a2);
                    } else {
                        b(oVar, keyAt, aVar, view, a2);
                    }
                }
            }
        }
    }

    private static void a(v vVar, ViewGroup viewGroup, i iVar, View view, ArrayList<View> arrayList, Object obj, ArrayList<View> arrayList2, Object obj2, ArrayList<View> arrayList3) {
        final Object obj3 = obj;
        final v vVar2 = vVar;
        final View view2 = view;
        final i iVar2 = iVar;
        final ArrayList<View> arrayList4 = arrayList;
        final ArrayList<View> arrayList5 = arrayList2;
        final ArrayList<View> arrayList6 = arrayList3;
        final Object obj4 = obj2;
        ad.a(viewGroup, new Runnable() {
            public void run() {
                if (obj3 != null) {
                    vVar2.c(obj3, view2);
                    arrayList5.addAll(t.b(vVar2, obj3, iVar2, arrayList4, view2));
                }
                if (arrayList6 != null) {
                    if (obj4 != null) {
                        ArrayList arrayList = new ArrayList();
                        arrayList.add(view2);
                        vVar2.b(obj4, arrayList6, arrayList);
                    }
                    arrayList6.clear();
                    arrayList6.add(view2);
                }
            }
        });
    }

    private static void a(v vVar, Object obj, i iVar, final ArrayList<View> arrayList) {
        if (iVar != null && obj != null && iVar.t && iVar.I && iVar.Z) {
            iVar.h(true);
            vVar.b(obj, iVar.n(), arrayList);
            ad.a(iVar.P, new Runnable() {
                public void run() {
                    t.b(arrayList, 4);
                }
            });
        }
    }

    private static void a(v vVar, Object obj, Object obj2, android.support.v4.h.a<String, View> aVar, boolean z, c cVar) {
        if (cVar.r != null && !cVar.r.isEmpty()) {
            View view = (View) aVar.get(z ? (String) cVar.s.get(0) : (String) cVar.r.get(0));
            vVar.a(obj, view);
            if (obj2 != null) {
                vVar.a(obj2, view);
            }
        }
    }

    private static void a(android.support.v4.h.a<String, String> aVar, android.support.v4.h.a<String, View> aVar2) {
        for (int size = aVar.size() - 1; size >= 0; size--) {
            if (!aVar2.containsKey((String) aVar.c(size))) {
                aVar.d(size);
            }
        }
    }

    private static void a(ArrayList<View> arrayList, android.support.v4.h.a<String, View> aVar, Collection<String> collection) {
        for (int size = aVar.size() - 1; size >= 0; size--) {
            View view = (View) aVar.c(size);
            if (collection.contains(android.support.v4.i.t.l(view))) {
                arrayList.add(view);
            }
        }
    }

    private static boolean a(v vVar, List<Object> list) {
        int size = list.size();
        for (int i = 0; i < size; i++) {
            if (!vVar.a(list.get(i))) {
                return false;
            }
        }
        return true;
    }

    private static android.support.v4.h.a<String, View> b(v vVar, android.support.v4.h.a<String, String> aVar, Object obj, a aVar2) {
        ArrayList<String> arrayList;
        ag agVar;
        if (aVar.isEmpty() || obj == null) {
            aVar.clear();
            return null;
        }
        i iVar = aVar2.d;
        android.support.v4.h.a aVar3 = new android.support.v4.h.a();
        vVar.a((Map<String, View>) aVar3, iVar.n());
        c cVar = aVar2.f;
        if (aVar2.e) {
            ag Q = iVar.Q();
            arrayList = cVar.s;
            agVar = Q;
        } else {
            ag R = iVar.R();
            arrayList = cVar.r;
            agVar = R;
        }
        aVar3.a(arrayList);
        if (agVar != null) {
            agVar.a(arrayList, aVar3);
            for (int size = arrayList.size() - 1; size >= 0; size--) {
                String str = (String) arrayList.get(size);
                View view = (View) aVar3.get(str);
                if (view == null) {
                    aVar.remove(str);
                } else if (!str.equals(android.support.v4.i.t.l(view))) {
                    aVar.put(android.support.v4.i.t.l(view), (String) aVar.remove(str));
                }
            }
        } else {
            aVar.a(aVar3.keySet());
        }
        return aVar3;
    }

    /* access modifiers changed from: private */
    public static View b(android.support.v4.h.a<String, View> aVar, a aVar2, Object obj, boolean z) {
        c cVar = aVar2.c;
        if (obj == null || aVar == null || cVar.r == null || cVar.r.isEmpty()) {
            return null;
        }
        return (View) aVar.get(z ? (String) cVar.r.get(0) : (String) cVar.s.get(0));
    }

    private static Object b(v vVar, i iVar, boolean z) {
        if (iVar == null) {
            return null;
        }
        return vVar.b(z ? iVar.u() : iVar.v());
    }

    private static Object b(v vVar, ViewGroup viewGroup, View view, android.support.v4.h.a<String, String> aVar, a aVar2, ArrayList<View> arrayList, ArrayList<View> arrayList2, Object obj, Object obj2) {
        Object obj3;
        final Rect rect;
        final i iVar = aVar2.f600a;
        final i iVar2 = aVar2.d;
        if (iVar == null || iVar2 == null) {
            return null;
        }
        final boolean z = aVar2.f601b;
        Object a2 = aVar.isEmpty() ? null : a(vVar, iVar, iVar2, z);
        android.support.v4.h.a b2 = b(vVar, aVar, a2, aVar2);
        if (aVar.isEmpty()) {
            obj3 = null;
        } else {
            arrayList.addAll(b2.values());
            obj3 = a2;
        }
        if (obj == null && obj2 == null && obj3 == null) {
            return null;
        }
        b(iVar, iVar2, z, b2, true);
        if (obj3 != null) {
            rect = new Rect();
            vVar.a(obj3, view, arrayList);
            a(vVar, obj3, obj2, b2, aVar2.e, aVar2.f);
            if (obj != null) {
                vVar.a(obj, rect);
            }
        } else {
            rect = null;
        }
        final v vVar2 = vVar;
        final android.support.v4.h.a<String, String> aVar3 = aVar;
        final Object obj4 = obj3;
        final a aVar4 = aVar2;
        final ArrayList<View> arrayList3 = arrayList2;
        final View view2 = view;
        final ArrayList<View> arrayList4 = arrayList;
        final Object obj5 = obj;
        ad.a(viewGroup, new Runnable() {
            public void run() {
                android.support.v4.h.a a2 = t.c(vVar2, aVar3, obj4, aVar4);
                if (a2 != null) {
                    arrayList3.addAll(a2.values());
                    arrayList3.add(view2);
                }
                t.b(iVar, iVar2, z, a2, false);
                if (obj4 != null) {
                    vVar2.a(obj4, arrayList4, arrayList3);
                    View a3 = t.b(a2, aVar4, obj5, z);
                    if (a3 != null) {
                        vVar2.a(a3, rect);
                    }
                }
            }
        });
        return obj3;
    }

    /* access modifiers changed from: private */
    public static ArrayList<View> b(v vVar, Object obj, i iVar, ArrayList<View> arrayList, View view) {
        ArrayList<View> arrayList2 = null;
        if (obj != null) {
            arrayList2 = new ArrayList<>();
            View n = iVar.n();
            if (n != null) {
                vVar.a(arrayList2, n);
            }
            if (arrayList != null) {
                arrayList2.removeAll(arrayList);
            }
            if (!arrayList2.isEmpty()) {
                arrayList2.add(view);
                vVar.a(obj, arrayList2);
            }
        }
        return arrayList2;
    }

    public static void b(c cVar, SparseArray<a> sparseArray, boolean z) {
        if (cVar.f540a.n.a()) {
            for (int size = cVar.f541b.size() - 1; size >= 0; size--) {
                a(cVar, (a) cVar.f541b.get(size), sparseArray, true, z);
            }
        }
    }

    /* access modifiers changed from: private */
    public static void b(i iVar, i iVar2, boolean z, android.support.v4.h.a<String, View> aVar, boolean z2) {
        ag Q = z ? iVar2.Q() : iVar.Q();
        if (Q != null) {
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            int size = aVar == null ? 0 : aVar.size();
            for (int i = 0; i < size; i++) {
                arrayList2.add(aVar.b(i));
                arrayList.add(aVar.c(i));
            }
            if (z2) {
                Q.a(arrayList2, arrayList, null);
            } else {
                Q.b(arrayList2, arrayList, null);
            }
        }
    }

    private static void b(o oVar, int i, a aVar, View view, android.support.v4.h.a<String, String> aVar2) {
        ViewGroup viewGroup = null;
        if (oVar.n.a()) {
            viewGroup = (ViewGroup) oVar.n.a(i);
        }
        if (viewGroup != null) {
            i iVar = aVar.f600a;
            i iVar2 = aVar.d;
            v a2 = a(iVar2, iVar);
            if (a2 != null) {
                boolean z = aVar.f601b;
                boolean z2 = aVar.e;
                Object a3 = a(a2, iVar, z);
                Object b2 = b(a2, iVar2, z2);
                ArrayList arrayList = new ArrayList();
                ArrayList arrayList2 = new ArrayList();
                Object b3 = b(a2, viewGroup, view, aVar2, aVar, arrayList, arrayList2, a3, b2);
                if (a3 != null || b3 != null || b2 != null) {
                    ArrayList b4 = b(a2, b2, iVar2, arrayList, view);
                    Object obj = (b4 == null || b4.isEmpty()) ? null : b2;
                    a2.b(a3, view);
                    Object a4 = a(a2, a3, obj, b3, iVar, aVar.f601b);
                    if (a4 != null) {
                        ArrayList arrayList3 = new ArrayList();
                        a2.a(a4, a3, arrayList3, obj, b4, b3, arrayList2);
                        a(a2, viewGroup, iVar, view, arrayList2, a3, arrayList3, obj, b4);
                        a2.a((View) viewGroup, arrayList2, (Map<String, String>) aVar2);
                        a2.a(viewGroup, a4);
                        a2.a(viewGroup, arrayList2, (Map<String, String>) aVar2);
                    }
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public static void b(ArrayList<View> arrayList, int i) {
        if (arrayList != null) {
            for (int size = arrayList.size() - 1; size >= 0; size--) {
                ((View) arrayList.get(size)).setVisibility(i);
            }
        }
    }

    /* access modifiers changed from: private */
    public static android.support.v4.h.a<String, View> c(v vVar, android.support.v4.h.a<String, String> aVar, Object obj, a aVar2) {
        ArrayList<String> arrayList;
        ag agVar;
        i iVar = aVar2.f600a;
        View n = iVar.n();
        if (aVar.isEmpty() || obj == null || n == null) {
            aVar.clear();
            return null;
        }
        android.support.v4.h.a aVar3 = new android.support.v4.h.a();
        vVar.a((Map<String, View>) aVar3, n);
        c cVar = aVar2.c;
        if (aVar2.f601b) {
            ag R = iVar.R();
            arrayList = cVar.r;
            agVar = R;
        } else {
            ag Q = iVar.Q();
            arrayList = cVar.s;
            agVar = Q;
        }
        if (arrayList != null) {
            aVar3.a(arrayList);
        }
        if (agVar != null) {
            agVar.a(arrayList, aVar3);
            for (int size = arrayList.size() - 1; size >= 0; size--) {
                String str = (String) arrayList.get(size);
                View view = (View) aVar3.get(str);
                if (view == null) {
                    String a2 = a(aVar, str);
                    if (a2 != null) {
                        aVar.remove(a2);
                    }
                } else if (!str.equals(android.support.v4.i.t.l(view))) {
                    String a3 = a(aVar, str);
                    if (a3 != null) {
                        aVar.put(a3, android.support.v4.i.t.l(view));
                    }
                }
            }
        } else {
            a(aVar, aVar3);
        }
        return aVar3;
    }
}
