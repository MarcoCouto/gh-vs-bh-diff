package android.support.v4.a;

import android.arch.lifecycle.b.C0004b;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.IntentSender.SendIntentException;
import android.content.res.Configuration;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.support.v4.a.a.C0011a;
import android.support.v4.a.a.c;
import android.support.v4.h.m;
import android.support.v4.h.n;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import java.io.FileDescriptor;
import java.io.PrintWriter;

public class j extends f implements C0011a, c {
    final Handler c = new Handler() {
        public void handleMessage(Message message) {
            switch (message.what) {
                case 1:
                    if (j.this.g) {
                        j.this.a(false);
                        return;
                    }
                    return;
                case 2:
                    j.this.y_();
                    j.this.d.n();
                    return;
                default:
                    super.handleMessage(message);
                    return;
            }
        }
    };
    final l d = l.a((m<?>) new a<Object>());
    boolean e;
    boolean f;
    boolean g = true;
    boolean h = true;
    boolean i;
    boolean j;
    int k;
    n<String> l;

    class a extends m<j> {
        public a() {
            super(j.this);
        }

        public View a(int i) {
            return j.this.findViewById(i);
        }

        public void a(i iVar, Intent intent, int i, Bundle bundle) {
            j.this.a(iVar, intent, i, bundle);
        }

        public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
            j.this.dump(str, fileDescriptor, printWriter, strArr);
        }

        public boolean a() {
            Window window = j.this.getWindow();
            return (window == null || window.peekDecorView() == null) ? false : true;
        }

        public boolean a(i iVar) {
            return !j.this.isFinishing();
        }

        public LayoutInflater b() {
            return j.this.getLayoutInflater().cloneInContext(j.this);
        }

        public void b(i iVar) {
            j.this.a(iVar);
        }

        public void c() {
            j.this.d();
        }

        public boolean d() {
            return j.this.getWindow() != null;
        }

        public int e() {
            Window window = j.this.getWindow();
            if (window == null) {
                return 0;
            }
            return window.getAttributes().windowAnimations;
        }
    }

    static final class b {

        /* renamed from: a reason: collision with root package name */
        Object f559a;

        /* renamed from: b reason: collision with root package name */
        p f560b;
        m<String, w> c;

        b() {
        }
    }

    private static void a(n nVar, C0004b bVar) {
        for (i iVar : nVar.c()) {
            if (iVar != null) {
                iVar.ad.a(bVar);
                a(iVar.l(), bVar);
            }
        }
    }

    private int b(i iVar) {
        if (this.l.b() >= 65534) {
            throw new IllegalStateException("Too many pending Fragment activity results.");
        }
        while (this.l.f(this.k) >= 0) {
            this.k = (this.k + 1) % 65534;
        }
        int i2 = this.k;
        this.l.b(i2, iVar.o);
        this.k = (this.k + 1) % 65534;
        return i2;
    }

    public android.arch.lifecycle.b a() {
        return super.a();
    }

    /* access modifiers changed from: 0000 */
    public final View a(View view, String str, Context context, AttributeSet attributeSet) {
        return this.d.a(view, str, context, attributeSet);
    }

    public final void a(int i2) {
        if (!this.j && i2 != -1) {
            b(i2);
        }
    }

    public void a(i iVar) {
    }

    public void a(i iVar, Intent intent, int i2, Bundle bundle) {
        this.f547b = true;
        if (i2 == -1) {
            try {
                a.a(this, intent, -1, bundle);
            } finally {
                this.f547b = false;
            }
        } else {
            b(i2);
            a.a(this, intent, ((b(iVar) + 1) << 16) + (65535 & i2), bundle);
            this.f547b = false;
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(boolean z) {
        if (!this.h) {
            this.h = true;
            this.i = z;
            this.c.removeMessages(1);
            e();
        } else if (z) {
            this.d.o();
            this.d.c(true);
        }
    }

    /* access modifiers changed from: protected */
    public boolean a(View view, Menu menu) {
        return super.onPreparePanel(0, view, menu);
    }

    public Object c() {
        return null;
    }

    @Deprecated
    public void d() {
        invalidateOptionsMenu();
    }

    public void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        super.dump(str, fileDescriptor, printWriter, strArr);
        printWriter.print(str);
        printWriter.print("Local FragmentActivity ");
        printWriter.print(Integer.toHexString(System.identityHashCode(this)));
        printWriter.println(" State:");
        String str2 = str + "  ";
        printWriter.print(str2);
        printWriter.print("mCreated=");
        printWriter.print(this.e);
        printWriter.print("mResumed=");
        printWriter.print(this.f);
        printWriter.print(" mStopped=");
        printWriter.print(this.g);
        printWriter.print(" mReallyStopped=");
        printWriter.println(this.h);
        this.d.a(str2, fileDescriptor, printWriter, strArr);
        this.d.a().a(str, fileDescriptor, printWriter, strArr);
    }

    /* access modifiers changed from: 0000 */
    public void e() {
        this.d.c(this.i);
        this.d.k();
    }

    public n f() {
        return this.d.a();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        this.d.b();
        int i4 = i2 >> 16;
        if (i4 != 0) {
            int i5 = i4 - 1;
            String str = (String) this.l.a(i5);
            this.l.c(i5);
            if (str == null) {
                Log.w("FragmentActivity", "Activity result delivered for unknown Fragment.");
                return;
            }
            i a2 = this.d.a(str);
            if (a2 == null) {
                Log.w("FragmentActivity", "Activity result no fragment exists for who: " + str);
            } else {
                a2.a(65535 & i2, i3, intent);
            }
        } else {
            android.support.v4.a.a.b a3 = a.a();
            if (a3 == null || !a3.a(this, i2, i3, intent)) {
                super.onActivityResult(i2, i3, intent);
            }
        }
    }

    public void onBackPressed() {
        n a2 = this.d.a();
        boolean d2 = a2.d();
        if (d2 && VERSION.SDK_INT <= 25) {
            return;
        }
        if (d2 || !a2.b()) {
            super.onBackPressed();
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        this.d.a(configuration);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        this.d.a((i) null);
        super.onCreate(bundle);
        b bVar = (b) getLastNonConfigurationInstance();
        if (bVar != null) {
            this.d.a(bVar.c);
        }
        if (bundle != null) {
            this.d.a(bundle.getParcelable("android:support:fragments"), bVar != null ? bVar.f560b : null);
            if (bundle.containsKey("android:support:next_request_index")) {
                this.k = bundle.getInt("android:support:next_request_index");
                int[] intArray = bundle.getIntArray("android:support:request_indicies");
                String[] stringArray = bundle.getStringArray("android:support:request_fragment_who");
                if (intArray == null || stringArray == null || intArray.length != stringArray.length) {
                    Log.w("FragmentActivity", "Invalid requestCode mapping in savedInstanceState.");
                } else {
                    this.l = new n<>(intArray.length);
                    for (int i2 = 0; i2 < intArray.length; i2++) {
                        this.l.b(intArray[i2], stringArray[i2]);
                    }
                }
            }
        }
        if (this.l == null) {
            this.l = new n<>();
            this.k = 0;
        }
        this.d.e();
    }

    public boolean onCreatePanelMenu(int i2, Menu menu) {
        return i2 == 0 ? super.onCreatePanelMenu(i2, menu) | this.d.a(menu, getMenuInflater()) : super.onCreatePanelMenu(i2, menu);
    }

    public /* bridge */ /* synthetic */ View onCreateView(View view, String str, Context context, AttributeSet attributeSet) {
        return super.onCreateView(view, str, context, attributeSet);
    }

    public /* bridge */ /* synthetic */ View onCreateView(String str, Context context, AttributeSet attributeSet) {
        return super.onCreateView(str, context, attributeSet);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        a(false);
        this.d.l();
        this.d.p();
    }

    public void onLowMemory() {
        super.onLowMemory();
        this.d.m();
    }

    public boolean onMenuItemSelected(int i2, MenuItem menuItem) {
        if (super.onMenuItemSelected(i2, menuItem)) {
            return true;
        }
        switch (i2) {
            case 0:
                return this.d.a(menuItem);
            case 6:
                return this.d.b(menuItem);
            default:
                return false;
        }
    }

    public void onMultiWindowModeChanged(boolean z) {
        this.d.a(z);
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        this.d.b();
    }

    public void onPanelClosed(int i2, Menu menu) {
        switch (i2) {
            case 0:
                this.d.b(menu);
                break;
        }
        super.onPanelClosed(i2, menu);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.f = false;
        if (this.c.hasMessages(2)) {
            this.c.removeMessages(2);
            y_();
        }
        this.d.i();
    }

    public void onPictureInPictureModeChanged(boolean z) {
        this.d.b(z);
    }

    /* access modifiers changed from: protected */
    public void onPostResume() {
        super.onPostResume();
        this.c.removeMessages(2);
        y_();
        this.d.n();
    }

    public boolean onPreparePanel(int i2, View view, Menu menu) {
        return (i2 != 0 || menu == null) ? super.onPreparePanel(i2, view, menu) : a(view, menu) | this.d.a(menu);
    }

    public void onRequestPermissionsResult(int i2, String[] strArr, int[] iArr) {
        this.d.b();
        int i3 = (i2 >> 16) & 65535;
        if (i3 != 0) {
            int i4 = i3 - 1;
            String str = (String) this.l.a(i4);
            this.l.c(i4);
            if (str == null) {
                Log.w("FragmentActivity", "Activity result delivered for unknown Fragment.");
                return;
            }
            i a2 = this.d.a(str);
            if (a2 == null) {
                Log.w("FragmentActivity", "Activity result no fragment exists for who: " + str);
            } else {
                a2.a(i2 & 65535, strArr, iArr);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.c.sendEmptyMessage(2);
        this.f = true;
        this.d.n();
    }

    public final Object onRetainNonConfigurationInstance() {
        if (this.g) {
            a(true);
        }
        Object c2 = c();
        p d2 = this.d.d();
        m<String, w> r = this.d.r();
        if (d2 == null && r == null && c2 == null) {
            return null;
        }
        b bVar = new b();
        bVar.f559a = c2;
        bVar.f560b = d2;
        bVar.c = r;
        return bVar;
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        a(f(), C0004b.CREATED);
        Parcelable c2 = this.d.c();
        if (c2 != null) {
            bundle.putParcelable("android:support:fragments", c2);
        }
        if (this.l.b() > 0) {
            bundle.putInt("android:support:next_request_index", this.k);
            int[] iArr = new int[this.l.b()];
            String[] strArr = new String[this.l.b()];
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 < this.l.b()) {
                    iArr[i3] = this.l.d(i3);
                    strArr[i3] = (String) this.l.e(i3);
                    i2 = i3 + 1;
                } else {
                    bundle.putIntArray("android:support:request_indicies", iArr);
                    bundle.putStringArray("android:support:request_fragment_who", strArr);
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        this.g = false;
        this.h = false;
        this.c.removeMessages(1);
        if (!this.e) {
            this.e = true;
            this.d.f();
        }
        this.d.b();
        this.d.n();
        this.d.o();
        this.d.g();
        this.d.q();
    }

    public void onStateNotSaved() {
        this.d.b();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        this.g = true;
        a(f(), C0004b.CREATED);
        this.c.sendEmptyMessage(1);
        this.d.j();
    }

    public void startActivityForResult(Intent intent, int i2) {
        if (!this.f547b && i2 != -1) {
            b(i2);
        }
        super.startActivityForResult(intent, i2);
    }

    public /* bridge */ /* synthetic */ void startActivityForResult(Intent intent, int i2, Bundle bundle) {
        super.startActivityForResult(intent, i2, bundle);
    }

    public /* bridge */ /* synthetic */ void startIntentSenderForResult(IntentSender intentSender, int i2, Intent intent, int i3, int i4, int i5) throws SendIntentException {
        super.startIntentSenderForResult(intentSender, i2, intent, i3, i4, i5);
    }

    public /* bridge */ /* synthetic */ void startIntentSenderForResult(IntentSender intentSender, int i2, Intent intent, int i3, int i4, int i5, Bundle bundle) throws SendIntentException {
        super.startIntentSenderForResult(intentSender, i2, intent, i3, i4, i5, bundle);
    }

    /* access modifiers changed from: protected */
    public void y_() {
        this.d.h();
    }
}
