package android.support.v4.a;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import android.util.Log;
import java.util.ArrayList;

final class d implements Parcelable {
    public static final Creator<d> CREATOR = new Creator<d>() {
        /* renamed from: a */
        public d createFromParcel(Parcel parcel) {
            return new d(parcel);
        }

        /* renamed from: a */
        public d[] newArray(int i) {
            return new d[i];
        }
    };

    /* renamed from: a reason: collision with root package name */
    final int[] f544a;

    /* renamed from: b reason: collision with root package name */
    final int f545b;
    final int c;
    final String d;
    final int e;
    final int f;
    final CharSequence g;
    final int h;
    final CharSequence i;
    final ArrayList<String> j;
    final ArrayList<String> k;
    final boolean l;

    public d(Parcel parcel) {
        this.f544a = parcel.createIntArray();
        this.f545b = parcel.readInt();
        this.c = parcel.readInt();
        this.d = parcel.readString();
        this.e = parcel.readInt();
        this.f = parcel.readInt();
        this.g = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.h = parcel.readInt();
        this.i = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.j = parcel.createStringArrayList();
        this.k = parcel.createStringArrayList();
        this.l = parcel.readInt() != 0;
    }

    public d(c cVar) {
        int size = cVar.f541b.size();
        this.f544a = new int[(size * 6)];
        if (!cVar.i) {
            throw new IllegalStateException("Not on back stack");
        }
        int i2 = 0;
        for (int i3 = 0; i3 < size; i3++) {
            a aVar = (a) cVar.f541b.get(i3);
            int i4 = i2 + 1;
            this.f544a[i2] = aVar.f542a;
            int i5 = i4 + 1;
            this.f544a[i4] = aVar.f543b != null ? aVar.f543b.n : -1;
            int i6 = i5 + 1;
            this.f544a[i5] = aVar.c;
            int i7 = i6 + 1;
            this.f544a[i6] = aVar.d;
            int i8 = i7 + 1;
            this.f544a[i7] = aVar.e;
            i2 = i8 + 1;
            this.f544a[i8] = aVar.f;
        }
        this.f545b = cVar.g;
        this.c = cVar.h;
        this.d = cVar.k;
        this.e = cVar.m;
        this.f = cVar.n;
        this.g = cVar.o;
        this.h = cVar.p;
        this.i = cVar.q;
        this.j = cVar.r;
        this.k = cVar.s;
        this.l = cVar.t;
    }

    public c a(o oVar) {
        int i2 = 0;
        c cVar = new c(oVar);
        int i3 = 0;
        while (i2 < this.f544a.length) {
            a aVar = new a();
            int i4 = i2 + 1;
            aVar.f542a = this.f544a[i2];
            if (o.f564a) {
                Log.v("FragmentManager", "Instantiate " + cVar + " op #" + i3 + " base fragment #" + this.f544a[i4]);
            }
            int i5 = i4 + 1;
            int i6 = this.f544a[i4];
            if (i6 >= 0) {
                aVar.f543b = (i) oVar.f.get(i6);
            } else {
                aVar.f543b = null;
            }
            int i7 = i5 + 1;
            aVar.c = this.f544a[i5];
            int i8 = i7 + 1;
            aVar.d = this.f544a[i7];
            int i9 = i8 + 1;
            aVar.e = this.f544a[i8];
            int i10 = i9 + 1;
            aVar.f = this.f544a[i9];
            cVar.c = aVar.c;
            cVar.d = aVar.d;
            cVar.e = aVar.e;
            cVar.f = aVar.f;
            cVar.a(aVar);
            i3++;
            i2 = i10;
        }
        cVar.g = this.f545b;
        cVar.h = this.c;
        cVar.k = this.d;
        cVar.m = this.e;
        cVar.i = true;
        cVar.n = this.f;
        cVar.o = this.g;
        cVar.p = this.h;
        cVar.q = this.i;
        cVar.r = this.j;
        cVar.s = this.k;
        cVar.t = this.l;
        cVar.a(1);
        return cVar;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        int i3 = 0;
        parcel.writeIntArray(this.f544a);
        parcel.writeInt(this.f545b);
        parcel.writeInt(this.c);
        parcel.writeString(this.d);
        parcel.writeInt(this.e);
        parcel.writeInt(this.f);
        TextUtils.writeToParcel(this.g, parcel, 0);
        parcel.writeInt(this.h);
        TextUtils.writeToParcel(this.i, parcel, 0);
        parcel.writeStringList(this.j);
        parcel.writeStringList(this.k);
        if (this.l) {
            i3 = 1;
        }
        parcel.writeInt(i3);
    }
}
