package android.support.v4.a;

import android.app.Notification;
import android.app.Notification.Action;
import android.app.Notification.Builder;
import android.app.PendingIntent;
import android.app.RemoteInput;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.v4.a.aa.a;
import android.support.v4.a.aa.c;
import android.support.v4.a.aa.d;
import android.util.SparseArray;
import android.widget.RemoteViews;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

class ab implements z {

    /* renamed from: a reason: collision with root package name */
    private final Builder f527a;

    /* renamed from: b reason: collision with root package name */
    private final c f528b;
    private RemoteViews c;
    private RemoteViews d;
    private final List<Bundle> e = new ArrayList();
    private final Bundle f = new Bundle();
    private int g;
    private RemoteViews h;

    ab(c cVar) {
        boolean z = false;
        this.f528b = cVar;
        if (VERSION.SDK_INT >= 26) {
            this.f527a = new Builder(cVar.f523a, cVar.H);
        } else {
            this.f527a = new Builder(cVar.f523a);
        }
        Notification notification = cVar.M;
        Builder deleteIntent = this.f527a.setWhen(notification.when).setSmallIcon(notification.icon, notification.iconLevel).setContent(notification.contentView).setTicker(notification.tickerText, cVar.g).setSound(notification.sound, notification.audioStreamType).setVibrate(notification.vibrate).setLights(notification.ledARGB, notification.ledOnMS, notification.ledOffMS).setOngoing((notification.flags & 2) != 0).setOnlyAlertOnce((notification.flags & 8) != 0).setAutoCancel((notification.flags & 16) != 0).setDefaults(notification.defaults).setContentTitle(cVar.c).setContentText(cVar.d).setContentInfo(cVar.i).setContentIntent(cVar.e).setDeleteIntent(notification.deleteIntent);
        PendingIntent pendingIntent = cVar.f;
        if ((notification.flags & 128) != 0) {
            z = true;
        }
        deleteIntent.setFullScreenIntent(pendingIntent, z).setLargeIcon(cVar.h).setNumber(cVar.j).setProgress(cVar.q, cVar.r, cVar.s);
        if (VERSION.SDK_INT >= 16) {
            this.f527a.setSubText(cVar.o).setUsesChronometer(cVar.m).setPriority(cVar.k);
            Iterator it = cVar.f524b.iterator();
            while (it.hasNext()) {
                a((a) it.next());
            }
            if (cVar.A != null) {
                this.f.putAll(cVar.A);
            }
            if (VERSION.SDK_INT < 20) {
                if (cVar.w) {
                    this.f.putBoolean("android.support.localOnly", true);
                }
                if (cVar.t != null) {
                    this.f.putString("android.support.groupKey", cVar.t);
                    if (cVar.u) {
                        this.f.putBoolean("android.support.isGroupSummary", true);
                    } else {
                        this.f.putBoolean("android.support.useSideChannel", true);
                    }
                }
                if (cVar.v != null) {
                    this.f.putString("android.support.sortKey", cVar.v);
                }
            }
            this.c = cVar.E;
            this.d = cVar.F;
        }
        if (VERSION.SDK_INT >= 19) {
            this.f527a.setShowWhen(cVar.l);
            if (VERSION.SDK_INT < 21 && cVar.N != null && !cVar.N.isEmpty()) {
                this.f.putStringArray("android.people", (String[]) cVar.N.toArray(new String[cVar.N.size()]));
            }
        }
        if (VERSION.SDK_INT >= 20) {
            this.f527a.setLocalOnly(cVar.w).setGroup(cVar.t).setGroupSummary(cVar.u).setSortKey(cVar.v);
            this.g = cVar.L;
        }
        if (VERSION.SDK_INT >= 21) {
            this.f527a.setCategory(cVar.z).setColor(cVar.B).setVisibility(cVar.C).setPublicVersion(cVar.D);
            Iterator it2 = cVar.N.iterator();
            while (it2.hasNext()) {
                this.f527a.addPerson((String) it2.next());
            }
            this.h = cVar.G;
        }
        if (VERSION.SDK_INT >= 24) {
            this.f527a.setExtras(cVar.A).setRemoteInputHistory(cVar.p);
            if (cVar.E != null) {
                this.f527a.setCustomContentView(cVar.E);
            }
            if (cVar.F != null) {
                this.f527a.setCustomBigContentView(cVar.F);
            }
            if (cVar.G != null) {
                this.f527a.setCustomHeadsUpContentView(cVar.G);
            }
        }
        if (VERSION.SDK_INT >= 26) {
            this.f527a.setBadgeIconType(cVar.I).setShortcutId(cVar.J).setTimeoutAfter(cVar.K).setGroupAlertBehavior(cVar.L);
            if (cVar.y) {
                this.f527a.setColorized(cVar.x);
            }
        }
    }

    private void a(Notification notification) {
        notification.sound = null;
        notification.vibrate = null;
        notification.defaults &= -2;
        notification.defaults &= -3;
    }

    private void a(a aVar) {
        if (VERSION.SDK_INT >= 20) {
            Action.Builder builder = new Action.Builder(aVar.a(), aVar.b(), aVar.c());
            if (aVar.f() != null) {
                for (RemoteInput addRemoteInput : ae.a(aVar.f())) {
                    builder.addRemoteInput(addRemoteInput);
                }
            }
            Bundle bundle = aVar.d() != null ? new Bundle(aVar.d()) : new Bundle();
            bundle.putBoolean("android.support.allowGeneratedReplies", aVar.e());
            if (VERSION.SDK_INT >= 24) {
                builder.setAllowGeneratedReplies(aVar.e());
            }
            builder.addExtras(bundle);
            this.f527a.addAction(builder.build());
        } else if (VERSION.SDK_INT >= 16) {
            this.e.add(ac.a(this.f527a, aVar));
        }
    }

    public Builder a() {
        return this.f527a;
    }

    public Notification b() {
        d dVar = this.f528b.n;
        if (dVar != null) {
            dVar.a((z) this);
        }
        RemoteViews remoteViews = dVar != null ? dVar.b(this) : null;
        Notification c2 = c();
        if (remoteViews != null) {
            c2.contentView = remoteViews;
        } else if (this.f528b.E != null) {
            c2.contentView = this.f528b.E;
        }
        if (VERSION.SDK_INT >= 16 && dVar != null) {
            RemoteViews c3 = dVar.c(this);
            if (c3 != null) {
                c2.bigContentView = c3;
            }
        }
        if (VERSION.SDK_INT >= 21 && dVar != null) {
            RemoteViews d2 = this.f528b.n.d(this);
            if (d2 != null) {
                c2.headsUpContentView = d2;
            }
        }
        if (VERSION.SDK_INT >= 16 && dVar != null) {
            Bundle a2 = aa.a(c2);
            if (a2 != null) {
                dVar.a(a2);
            }
        }
        return c2;
    }

    /* access modifiers changed from: protected */
    public Notification c() {
        if (VERSION.SDK_INT >= 26) {
            return this.f527a.build();
        }
        if (VERSION.SDK_INT >= 24) {
            Notification build = this.f527a.build();
            if (this.g == 0) {
                return build;
            }
            if (!(build.getGroup() == null || (build.flags & 512) == 0 || this.g != 2)) {
                a(build);
            }
            if (build.getGroup() == null || (build.flags & 512) != 0 || this.g != 1) {
                return build;
            }
            a(build);
            return build;
        } else if (VERSION.SDK_INT >= 21) {
            this.f527a.setExtras(this.f);
            Notification build2 = this.f527a.build();
            if (this.c != null) {
                build2.contentView = this.c;
            }
            if (this.d != null) {
                build2.bigContentView = this.d;
            }
            if (this.h != null) {
                build2.headsUpContentView = this.h;
            }
            if (this.g == 0) {
                return build2;
            }
            if (!(build2.getGroup() == null || (build2.flags & 512) == 0 || this.g != 2)) {
                a(build2);
            }
            if (build2.getGroup() == null || (build2.flags & 512) != 0 || this.g != 1) {
                return build2;
            }
            a(build2);
            return build2;
        } else if (VERSION.SDK_INT >= 20) {
            this.f527a.setExtras(this.f);
            Notification build3 = this.f527a.build();
            if (this.c != null) {
                build3.contentView = this.c;
            }
            if (this.d != null) {
                build3.bigContentView = this.d;
            }
            if (this.g == 0) {
                return build3;
            }
            if (!(build3.getGroup() == null || (build3.flags & 512) == 0 || this.g != 2)) {
                a(build3);
            }
            if (build3.getGroup() == null || (build3.flags & 512) != 0 || this.g != 1) {
                return build3;
            }
            a(build3);
            return build3;
        } else if (VERSION.SDK_INT >= 19) {
            SparseArray a2 = ac.a(this.e);
            if (a2 != null) {
                this.f.putSparseParcelableArray("android.support.actionExtras", a2);
            }
            this.f527a.setExtras(this.f);
            Notification build4 = this.f527a.build();
            if (this.c != null) {
                build4.contentView = this.c;
            }
            if (this.d == null) {
                return build4;
            }
            build4.bigContentView = this.d;
            return build4;
        } else if (VERSION.SDK_INT < 16) {
            return this.f527a.getNotification();
        } else {
            Notification build5 = this.f527a.build();
            Bundle a3 = aa.a(build5);
            Bundle bundle = new Bundle(this.f);
            for (String str : this.f.keySet()) {
                if (a3.containsKey(str)) {
                    bundle.remove(str);
                }
            }
            a3.putAll(bundle);
            SparseArray a4 = ac.a(this.e);
            if (a4 != null) {
                aa.a(build5).putSparseParcelableArray("android.support.actionExtras", a4);
            }
            if (this.c != null) {
                build5.contentView = this.c;
            }
            if (this.d != null) {
                build5.bigContentView = this.d;
            }
            return build5;
        }
    }
}
