package android.support.v4.a;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

final class q implements Parcelable {
    public static final Creator<q> CREATOR = new Creator<q>() {
        /* renamed from: a */
        public q createFromParcel(Parcel parcel) {
            return new q(parcel);
        }

        /* renamed from: a */
        public q[] newArray(int i) {
            return new q[i];
        }
    };

    /* renamed from: a reason: collision with root package name */
    r[] f587a;

    /* renamed from: b reason: collision with root package name */
    int[] f588b;
    d[] c;
    int d = -1;
    int e;

    public q() {
    }

    public q(Parcel parcel) {
        this.f587a = (r[]) parcel.createTypedArray(r.CREATOR);
        this.f588b = parcel.createIntArray();
        this.c = (d[]) parcel.createTypedArray(d.CREATOR);
        this.d = parcel.readInt();
        this.e = parcel.readInt();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeTypedArray(this.f587a, i);
        parcel.writeIntArray(this.f588b);
        parcel.writeTypedArray(this.c, i);
        parcel.writeInt(this.d);
        parcel.writeInt(this.e);
    }
}
