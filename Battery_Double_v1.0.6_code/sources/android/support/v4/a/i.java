package android.support.v4.a;

import android.animation.Animator;
import android.app.Activity;
import android.arch.lifecycle.d;
import android.content.ComponentCallbacks;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Looper;
import android.os.Parcelable;
import android.support.v4.h.m;
import android.support.v4.i.f;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnCreateContextMenuListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;

public class i implements android.arch.lifecycle.c, ComponentCallbacks, OnCreateContextMenuListener {

    /* renamed from: a reason: collision with root package name */
    private static final m<String, Class<?>> f552a = new m<>();
    static final Object j = new Object();
    o A;
    m B;
    o C;
    p D;
    i E;
    int F;
    int G;
    String H;
    boolean I;
    boolean J;
    boolean K;
    boolean L;
    boolean M;
    boolean N = true;
    boolean O;
    ViewGroup P;
    View Q;
    View R;
    boolean S;
    boolean T = true;
    x U;
    boolean V;
    boolean W;
    a X;
    boolean Y;
    boolean Z;
    float aa;
    LayoutInflater ab;
    boolean ac;
    d ad = new d(this);
    int k = 0;
    Bundle l;
    SparseArray<Parcelable> m;
    int n = -1;
    String o;
    Bundle p;
    i q;
    int r = -1;
    int s;
    boolean t;
    boolean u;
    boolean v;
    boolean w;
    boolean x;
    boolean y;
    int z;

    static class a {

        /* renamed from: a reason: collision with root package name */
        View f555a;

        /* renamed from: b reason: collision with root package name */
        Animator f556b;
        int c;
        int d;
        int e;
        int f;
        ag g = null;
        ag h = null;
        boolean i;
        c j;
        boolean k;
        /* access modifiers changed from: private */
        public Object l = null;
        /* access modifiers changed from: private */
        public Object m = i.j;
        /* access modifiers changed from: private */
        public Object n = null;
        /* access modifiers changed from: private */
        public Object o = i.j;
        /* access modifiers changed from: private */
        public Object p = null;
        /* access modifiers changed from: private */
        public Object q = i.j;
        /* access modifiers changed from: private */
        public Boolean r;
        /* access modifiers changed from: private */
        public Boolean s;

        a() {
        }
    }

    public static class b extends RuntimeException {
        public b(String str, Exception exc) {
            super(str, exc);
        }
    }

    interface c {
        void a();

        void b();
    }

    private a X() {
        if (this.X == null) {
            this.X = new a();
        }
        return this.X;
    }

    public static i a(Context context, String str, Bundle bundle) {
        try {
            Class cls = (Class) f552a.get(str);
            if (cls == null) {
                cls = context.getClassLoader().loadClass(str);
                f552a.put(str, cls);
            }
            i iVar = (i) cls.getConstructor(new Class[0]).newInstance(new Object[0]);
            if (bundle != null) {
                bundle.setClassLoader(iVar.getClass().getClassLoader());
                iVar.g(bundle);
            }
            return iVar;
        } catch (ClassNotFoundException e) {
            throw new b("Unable to instantiate fragment " + str + ": make sure class name exists, is public, and has an" + " empty constructor that is public", e);
        } catch (InstantiationException e2) {
            throw new b("Unable to instantiate fragment " + str + ": make sure class name exists, is public, and has an" + " empty constructor that is public", e2);
        } catch (IllegalAccessException e3) {
            throw new b("Unable to instantiate fragment " + str + ": make sure class name exists, is public, and has an" + " empty constructor that is public", e3);
        } catch (NoSuchMethodException e4) {
            throw new b("Unable to instantiate fragment " + str + ": could not find Fragment constructor", e4);
        } catch (InvocationTargetException e5) {
            throw new b("Unable to instantiate fragment " + str + ": calling Fragment constructor caused an exception", e5);
        }
    }

    static boolean a(Context context, String str) {
        try {
            Class cls = (Class) f552a.get(str);
            if (cls == null) {
                cls = context.getClassLoader().loadClass(str);
                f552a.put(str, cls);
            }
            return i.class.isAssignableFrom(cls);
        } catch (ClassNotFoundException e) {
            return false;
        }
    }

    /* access modifiers changed from: private */
    public void b() {
        c cVar = null;
        if (this.X != null) {
            this.X.i = false;
            c cVar2 = this.X.j;
            this.X.j = null;
            cVar = cVar2;
        }
        if (cVar != null) {
            cVar.a();
        }
    }

    public boolean A() {
        if (this.X == null || this.X.r == null) {
            return true;
        }
        return this.X.r.booleanValue();
    }

    public void B() {
        if (this.A == null || this.A.m == null) {
            X().i = false;
        } else if (Looper.myLooper() != this.A.m.h().getLooper()) {
            this.A.m.h().postAtFrontOfQueue(new Runnable() {
                public void run() {
                    i.this.b();
                }
            });
        } else {
            b();
        }
    }

    /* access modifiers changed from: 0000 */
    public void C() {
        if (this.B == null) {
            throw new IllegalStateException("Fragment has not been attached yet.");
        }
        this.C = new o();
        this.C.a(this.B, (k) new k() {
            public i a(Context context, String str, Bundle bundle) {
                return i.this.B.a(context, str, bundle);
            }

            public View a(int i) {
                if (i.this.Q != null) {
                    return i.this.Q.findViewById(i);
                }
                throw new IllegalStateException("Fragment does not have a view");
            }

            public boolean a() {
                return i.this.Q != null;
            }
        }, this);
    }

    /* access modifiers changed from: 0000 */
    public void D() {
        if (this.C != null) {
            this.C.l();
            this.C.f();
        }
        this.k = 4;
        this.O = false;
        d();
        if (!this.O) {
            throw new ah("Fragment " + this + " did not call through to super.onStart()");
        }
        if (this.C != null) {
            this.C.o();
        }
        if (this.U != null) {
            this.U.g();
        }
        this.ad.a(android.arch.lifecycle.b.a.ON_START);
    }

    /* access modifiers changed from: 0000 */
    public void E() {
        if (this.C != null) {
            this.C.l();
            this.C.f();
        }
        this.k = 5;
        this.O = false;
        o();
        if (!this.O) {
            throw new ah("Fragment " + this + " did not call through to super.onResume()");
        }
        if (this.C != null) {
            this.C.p();
            this.C.f();
        }
        this.ad.a(android.arch.lifecycle.b.a.ON_RESUME);
    }

    /* access modifiers changed from: 0000 */
    public void F() {
        if (this.C != null) {
            this.C.l();
        }
    }

    /* access modifiers changed from: 0000 */
    public void G() {
        onLowMemory();
        if (this.C != null) {
            this.C.v();
        }
    }

    /* access modifiers changed from: 0000 */
    public void H() {
        this.ad.a(android.arch.lifecycle.b.a.ON_PAUSE);
        if (this.C != null) {
            this.C.q();
        }
        this.k = 4;
        this.O = false;
        p();
        if (!this.O) {
            throw new ah("Fragment " + this + " did not call through to super.onPause()");
        }
    }

    /* access modifiers changed from: 0000 */
    public void I() {
        this.ad.a(android.arch.lifecycle.b.a.ON_STOP);
        if (this.C != null) {
            this.C.r();
        }
        this.k = 3;
        this.O = false;
        e();
        if (!this.O) {
            throw new ah("Fragment " + this + " did not call through to super.onStop()");
        }
    }

    /* access modifiers changed from: 0000 */
    public void J() {
        if (this.C != null) {
            this.C.s();
        }
        this.k = 2;
        if (this.V) {
            this.V = false;
            if (!this.W) {
                this.W = true;
                this.U = this.B.a(this.o, this.V, false);
            }
            if (this.U == null) {
                return;
            }
            if (this.B.j()) {
                this.U.d();
            } else {
                this.U.c();
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void K() {
        if (this.C != null) {
            this.C.t();
        }
        this.k = 1;
        this.O = false;
        f();
        if (!this.O) {
            throw new ah("Fragment " + this + " did not call through to super.onDestroyView()");
        }
        if (this.U != null) {
            this.U.f();
        }
        this.y = false;
    }

    /* access modifiers changed from: 0000 */
    public void L() {
        this.ad.a(android.arch.lifecycle.b.a.ON_DESTROY);
        if (this.C != null) {
            this.C.u();
        }
        this.k = 0;
        this.O = false;
        this.ac = false;
        q();
        if (!this.O) {
            throw new ah("Fragment " + this + " did not call through to super.onDestroy()");
        }
        this.C = null;
    }

    /* access modifiers changed from: 0000 */
    public void M() {
        this.O = false;
        c();
        this.ab = null;
        if (!this.O) {
            throw new ah("Fragment " + this + " did not call through to super.onDetach()");
        } else if (this.C == null) {
        } else {
            if (!this.L) {
                throw new IllegalStateException("Child FragmentManager of " + this + " was not " + " destroyed and this fragment is not retaining instance");
            }
            this.C.u();
            this.C = null;
        }
    }

    /* access modifiers changed from: 0000 */
    public int N() {
        if (this.X == null) {
            return 0;
        }
        return this.X.d;
    }

    /* access modifiers changed from: 0000 */
    public int O() {
        if (this.X == null) {
            return 0;
        }
        return this.X.e;
    }

    /* access modifiers changed from: 0000 */
    public int P() {
        if (this.X == null) {
            return 0;
        }
        return this.X.f;
    }

    /* access modifiers changed from: 0000 */
    public ag Q() {
        if (this.X == null) {
            return null;
        }
        return this.X.g;
    }

    /* access modifiers changed from: 0000 */
    public ag R() {
        if (this.X == null) {
            return null;
        }
        return this.X.h;
    }

    /* access modifiers changed from: 0000 */
    public View S() {
        if (this.X == null) {
            return null;
        }
        return this.X.f555a;
    }

    /* access modifiers changed from: 0000 */
    public Animator T() {
        if (this.X == null) {
            return null;
        }
        return this.X.f556b;
    }

    /* access modifiers changed from: 0000 */
    public int U() {
        if (this.X == null) {
            return 0;
        }
        return this.X.c;
    }

    /* access modifiers changed from: 0000 */
    public boolean V() {
        if (this.X == null) {
            return false;
        }
        return this.X.i;
    }

    /* access modifiers changed from: 0000 */
    public boolean W() {
        if (this.X == null) {
            return false;
        }
        return this.X.k;
    }

    public android.arch.lifecycle.b a() {
        return this.ad;
    }

    /* access modifiers changed from: 0000 */
    public i a(String str) {
        if (str.equals(this.o)) {
            return this;
        }
        if (this.C != null) {
            return this.C.b(str);
        }
        return null;
    }

    public View a(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return null;
    }

    public Animation a(int i, boolean z2, int i2) {
        return null;
    }

    /* access modifiers changed from: 0000 */
    public void a(int i) {
        if (this.X != null || i != 0) {
            X().d = i;
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(int i, int i2) {
        if (this.X != null || i != 0 || i2 != 0) {
            X();
            this.X.e = i;
            this.X.f = i2;
        }
    }

    public void a(int i, int i2, Intent intent) {
    }

    /* access modifiers changed from: 0000 */
    public final void a(int i, i iVar) {
        this.n = i;
        if (iVar != null) {
            this.o = iVar.o + ":" + this.n;
        } else {
            this.o = "android:fragment:" + this.n;
        }
    }

    public void a(int i, String[] strArr, int[] iArr) {
    }

    /* access modifiers changed from: 0000 */
    public void a(Animator animator) {
        X().f556b = animator;
    }

    @Deprecated
    public void a(Activity activity) {
        this.O = true;
    }

    @Deprecated
    public void a(Activity activity, AttributeSet attributeSet, Bundle bundle) {
        this.O = true;
    }

    public void a(Context context) {
        this.O = true;
        Activity f = this.B == null ? null : this.B.f();
        if (f != null) {
            this.O = false;
            a(f);
        }
    }

    public void a(Context context, AttributeSet attributeSet, Bundle bundle) {
        this.O = true;
        Activity f = this.B == null ? null : this.B.f();
        if (f != null) {
            this.O = false;
            a(f, attributeSet, bundle);
        }
    }

    public void a(Intent intent, int i) {
        a(intent, i, (Bundle) null);
    }

    public void a(Intent intent, int i, Bundle bundle) {
        if (this.B == null) {
            throw new IllegalStateException("Fragment " + this + " not attached to Activity");
        }
        this.B.a(this, intent, i, bundle);
    }

    /* access modifiers changed from: 0000 */
    public void a(Configuration configuration) {
        onConfigurationChanged(configuration);
        if (this.C != null) {
            this.C.a(configuration);
        }
    }

    public void a(Bundle bundle) {
        this.O = true;
        j(bundle);
        if (this.C != null && !this.C.a(1)) {
            this.C.m();
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(c cVar) {
        X();
        if (cVar != this.X.j) {
            if (cVar == null || this.X.j == null) {
                if (this.X.i) {
                    this.X.j = cVar;
                }
                if (cVar != null) {
                    cVar.b();
                    return;
                }
                return;
            }
            throw new IllegalStateException("Trying to set a replacement startPostponedEnterTransition on " + this);
        }
    }

    public void a(i iVar) {
    }

    public void a(Menu menu) {
    }

    public void a(Menu menu, MenuInflater menuInflater) {
    }

    /* access modifiers changed from: 0000 */
    public void a(View view) {
        X().f555a = view;
    }

    public void a(View view, Bundle bundle) {
    }

    public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        printWriter.print(str);
        printWriter.print("mFragmentId=#");
        printWriter.print(Integer.toHexString(this.F));
        printWriter.print(" mContainerId=#");
        printWriter.print(Integer.toHexString(this.G));
        printWriter.print(" mTag=");
        printWriter.println(this.H);
        printWriter.print(str);
        printWriter.print("mState=");
        printWriter.print(this.k);
        printWriter.print(" mIndex=");
        printWriter.print(this.n);
        printWriter.print(" mWho=");
        printWriter.print(this.o);
        printWriter.print(" mBackStackNesting=");
        printWriter.println(this.z);
        printWriter.print(str);
        printWriter.print("mAdded=");
        printWriter.print(this.t);
        printWriter.print(" mRemoving=");
        printWriter.print(this.u);
        printWriter.print(" mFromLayout=");
        printWriter.print(this.v);
        printWriter.print(" mInLayout=");
        printWriter.println(this.w);
        printWriter.print(str);
        printWriter.print("mHidden=");
        printWriter.print(this.I);
        printWriter.print(" mDetached=");
        printWriter.print(this.J);
        printWriter.print(" mMenuVisible=");
        printWriter.print(this.N);
        printWriter.print(" mHasMenu=");
        printWriter.println(this.M);
        printWriter.print(str);
        printWriter.print("mRetainInstance=");
        printWriter.print(this.K);
        printWriter.print(" mRetaining=");
        printWriter.print(this.L);
        printWriter.print(" mUserVisibleHint=");
        printWriter.println(this.T);
        if (this.A != null) {
            printWriter.print(str);
            printWriter.print("mFragmentManager=");
            printWriter.println(this.A);
        }
        if (this.B != null) {
            printWriter.print(str);
            printWriter.print("mHost=");
            printWriter.println(this.B);
        }
        if (this.E != null) {
            printWriter.print(str);
            printWriter.print("mParentFragment=");
            printWriter.println(this.E);
        }
        if (this.p != null) {
            printWriter.print(str);
            printWriter.print("mArguments=");
            printWriter.println(this.p);
        }
        if (this.l != null) {
            printWriter.print(str);
            printWriter.print("mSavedFragmentState=");
            printWriter.println(this.l);
        }
        if (this.m != null) {
            printWriter.print(str);
            printWriter.print("mSavedViewState=");
            printWriter.println(this.m);
        }
        if (this.q != null) {
            printWriter.print(str);
            printWriter.print("mTarget=");
            printWriter.print(this.q);
            printWriter.print(" mTargetRequestCode=");
            printWriter.println(this.s);
        }
        if (N() != 0) {
            printWriter.print(str);
            printWriter.print("mNextAnim=");
            printWriter.println(N());
        }
        if (this.P != null) {
            printWriter.print(str);
            printWriter.print("mContainer=");
            printWriter.println(this.P);
        }
        if (this.Q != null) {
            printWriter.print(str);
            printWriter.print("mView=");
            printWriter.println(this.Q);
        }
        if (this.R != null) {
            printWriter.print(str);
            printWriter.print("mInnerView=");
            printWriter.println(this.Q);
        }
        if (S() != null) {
            printWriter.print(str);
            printWriter.print("mAnimatingAway=");
            printWriter.println(S());
            printWriter.print(str);
            printWriter.print("mStateAfterAnimating=");
            printWriter.println(U());
        }
        if (this.U != null) {
            printWriter.print(str);
            printWriter.println("Loader Manager:");
            this.U.a(str + "  ", fileDescriptor, printWriter, strArr);
        }
        if (this.C != null) {
            printWriter.print(str);
            printWriter.println("Child " + this.C + ":");
            this.C.a(str + "  ", fileDescriptor, printWriter, strArr);
        }
    }

    public boolean a(MenuItem menuItem) {
        return false;
    }

    public Animator b(int i, boolean z2, int i2) {
        return null;
    }

    public LayoutInflater b(Bundle bundle) {
        return i(bundle);
    }

    /* access modifiers changed from: 0000 */
    public View b(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        if (this.C != null) {
            this.C.l();
        }
        this.y = true;
        return a(layoutInflater, viewGroup, bundle);
    }

    /* access modifiers changed from: 0000 */
    public void b(int i) {
        X().c = i;
    }

    public void b(Menu menu) {
    }

    /* access modifiers changed from: 0000 */
    public boolean b(Menu menu, MenuInflater menuInflater) {
        boolean z2 = false;
        if (this.I) {
            return false;
        }
        if (this.M && this.N) {
            z2 = true;
            a(menu, menuInflater);
        }
        return this.C != null ? z2 | this.C.a(menu, menuInflater) : z2;
    }

    public boolean b(MenuItem menuItem) {
        return false;
    }

    public void c() {
        this.O = true;
    }

    public void c(boolean z2) {
    }

    /* access modifiers changed from: 0000 */
    public boolean c(Menu menu) {
        boolean z2 = false;
        if (this.I) {
            return false;
        }
        if (this.M && this.N) {
            z2 = true;
            a(menu);
        }
        return this.C != null ? z2 | this.C.a(menu) : z2;
    }

    /* access modifiers changed from: 0000 */
    public boolean c(MenuItem menuItem) {
        if (!this.I) {
            if (this.M && this.N && a(menuItem)) {
                return true;
            }
            if (this.C != null && this.C.a(menuItem)) {
                return true;
            }
        }
        return false;
    }

    public void d() {
        this.O = true;
        if (!this.V) {
            this.V = true;
            if (!this.W) {
                this.W = true;
                this.U = this.B.a(this.o, this.V, false);
            } else if (this.U != null) {
                this.U.b();
            }
        }
    }

    public void d(Bundle bundle) {
        this.O = true;
    }

    /* access modifiers changed from: 0000 */
    public void d(Menu menu) {
        if (!this.I) {
            if (this.M && this.N) {
                b(menu);
            }
            if (this.C != null) {
                this.C.b(menu);
            }
        }
    }

    public void d(boolean z2) {
    }

    /* access modifiers changed from: 0000 */
    public boolean d(MenuItem menuItem) {
        if (!this.I) {
            if (b(menuItem)) {
                return true;
            }
            if (this.C != null && this.C.b(menuItem)) {
                return true;
            }
        }
        return false;
    }

    public void e() {
        this.O = true;
    }

    public void e(Bundle bundle) {
    }

    public void e(boolean z2) {
    }

    public final boolean equals(Object obj) {
        return super.equals(obj);
    }

    public void f() {
        this.O = true;
    }

    /* access modifiers changed from: 0000 */
    public final void f(Bundle bundle) {
        if (this.m != null) {
            this.R.restoreHierarchyState(this.m);
            this.m = null;
        }
        this.O = false;
        k(bundle);
        if (!this.O) {
            throw new ah("Fragment " + this + " did not call through to super.onViewStateRestored()");
        }
    }

    /* access modifiers changed from: 0000 */
    public void f(boolean z2) {
        d(z2);
        if (this.C != null) {
            this.C.a(z2);
        }
    }

    public void g(Bundle bundle) {
        if (this.n < 0 || !h()) {
            this.p = bundle;
            return;
        }
        throw new IllegalStateException("Fragment already active and state has been saved");
    }

    /* access modifiers changed from: 0000 */
    public void g(boolean z2) {
        e(z2);
        if (this.C != null) {
            this.C.b(z2);
        }
    }

    /* access modifiers changed from: 0000 */
    public final boolean g() {
        return this.z > 0;
    }

    /* access modifiers changed from: 0000 */
    public LayoutInflater h(Bundle bundle) {
        this.ab = b(bundle);
        return this.ab;
    }

    /* access modifiers changed from: 0000 */
    public void h(boolean z2) {
        X().k = z2;
    }

    public final boolean h() {
        if (this.A == null) {
            return false;
        }
        return this.A.d();
    }

    public final int hashCode() {
        return super.hashCode();
    }

    public final j i() {
        if (this.B == null) {
            return null;
        }
        return (j) this.B.f();
    }

    @Deprecated
    public LayoutInflater i(Bundle bundle) {
        if (this.B == null) {
            throw new IllegalStateException("onGetLayoutInflater() cannot be executed until the Fragment is attached to the FragmentManager.");
        }
        LayoutInflater b2 = this.B.b();
        l();
        f.b(b2, this.C.x());
        return b2;
    }

    public final Resources j() {
        if (this.B != null) {
            return this.B.g().getResources();
        }
        throw new IllegalStateException("Fragment " + this + " not attached to Activity");
    }

    /* access modifiers changed from: 0000 */
    public void j(Bundle bundle) {
        if (bundle != null) {
            Parcelable parcelable = bundle.getParcelable("android:support:fragments");
            if (parcelable != null) {
                if (this.C == null) {
                    C();
                }
                this.C.a(parcelable, this.D);
                this.D = null;
                this.C.m();
            }
        }
    }

    public final n k() {
        return this.A;
    }

    public void k(Bundle bundle) {
        this.O = true;
    }

    public final n l() {
        if (this.C == null) {
            C();
            if (this.k >= 5) {
                this.C.p();
            } else if (this.k >= 4) {
                this.C.o();
            } else if (this.k >= 2) {
                this.C.n();
            } else if (this.k >= 1) {
                this.C.m();
            }
        }
        return this.C;
    }

    /* access modifiers changed from: 0000 */
    public void l(Bundle bundle) {
        if (this.C != null) {
            this.C.l();
        }
        this.k = 1;
        this.O = false;
        a(bundle);
        this.ac = true;
        if (!this.O) {
            throw new ah("Fragment " + this + " did not call through to super.onCreate()");
        }
        this.ad.a(android.arch.lifecycle.b.a.ON_CREATE);
    }

    /* access modifiers changed from: 0000 */
    public n m() {
        return this.C;
    }

    /* access modifiers changed from: 0000 */
    public void m(Bundle bundle) {
        if (this.C != null) {
            this.C.l();
        }
        this.k = 2;
        this.O = false;
        d(bundle);
        if (!this.O) {
            throw new ah("Fragment " + this + " did not call through to super.onActivityCreated()");
        } else if (this.C != null) {
            this.C.n();
        }
    }

    public View n() {
        return this.Q;
    }

    /* access modifiers changed from: 0000 */
    public void n(Bundle bundle) {
        e(bundle);
        if (this.C != null) {
            Parcelable k2 = this.C.k();
            if (k2 != null) {
                bundle.putParcelable("android:support:fragments", k2);
            }
        }
    }

    public void o() {
        this.O = true;
    }

    public void onConfigurationChanged(Configuration configuration) {
        this.O = true;
    }

    public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenuInfo contextMenuInfo) {
        i().onCreateContextMenu(contextMenu, view, contextMenuInfo);
    }

    public void onLowMemory() {
        this.O = true;
    }

    public void p() {
        this.O = true;
    }

    public void q() {
        this.O = true;
        if (!this.W) {
            this.W = true;
            this.U = this.B.a(this.o, this.V, false);
        }
        if (this.U != null) {
            this.U.h();
        }
    }

    /* access modifiers changed from: 0000 */
    public void r() {
        this.n = -1;
        this.o = null;
        this.t = false;
        this.u = false;
        this.v = false;
        this.w = false;
        this.x = false;
        this.z = 0;
        this.A = null;
        this.C = null;
        this.B = null;
        this.F = 0;
        this.G = 0;
        this.H = null;
        this.I = false;
        this.J = false;
        this.L = false;
        this.U = null;
        this.V = false;
        this.W = false;
    }

    public void s() {
    }

    public Object t() {
        if (this.X == null) {
            return null;
        }
        return this.X.l;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(128);
        android.support.v4.h.d.a(this, sb);
        if (this.n >= 0) {
            sb.append(" #");
            sb.append(this.n);
        }
        if (this.F != 0) {
            sb.append(" id=0x");
            sb.append(Integer.toHexString(this.F));
        }
        if (this.H != null) {
            sb.append(" ");
            sb.append(this.H);
        }
        sb.append('}');
        return sb.toString();
    }

    public Object u() {
        if (this.X == null) {
            return null;
        }
        return this.X.m == j ? t() : this.X.m;
    }

    public Object v() {
        if (this.X == null) {
            return null;
        }
        return this.X.n;
    }

    public Object w() {
        if (this.X == null) {
            return null;
        }
        return this.X.o == j ? v() : this.X.o;
    }

    public Object x() {
        if (this.X == null) {
            return null;
        }
        return this.X.p;
    }

    public Object y() {
        if (this.X == null) {
            return null;
        }
        return this.X.q == j ? x() : this.X.q;
    }

    public boolean z() {
        if (this.X == null || this.X.s == null) {
            return true;
        }
        return this.X.s.booleanValue();
    }
}
