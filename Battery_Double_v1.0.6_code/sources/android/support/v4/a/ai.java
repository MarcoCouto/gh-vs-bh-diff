package android.support.v4.a;

import android.app.Activity;
import android.arch.lifecycle.b;
import android.arch.lifecycle.b.C0004b;
import android.arch.lifecycle.c;
import android.arch.lifecycle.d;
import android.arch.lifecycle.g;
import android.os.Bundle;
import android.support.v4.h.m;

public class ai extends Activity implements c {

    /* renamed from: a reason: collision with root package name */
    private m<Class<? extends Object>, Object> f536a = new m<>();

    /* renamed from: b reason: collision with root package name */
    private d f537b = new d(this);

    public b a() {
        return this.f537b;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        g.a((Activity) this);
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        this.f537b.a(C0004b.CREATED);
        super.onSaveInstanceState(bundle);
    }
}
