package android.support.v4.b.a;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.Resources.NotFoundException;
import android.content.res.Resources.Theme;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.b.a.a.C0012a;
import android.support.v4.c.c;
import android.util.Log;
import android.util.TypedValue;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public final class b {

    public static abstract class a {
        public abstract void a(int i);

        public final void a(final int i, Handler handler) {
            if (handler == null) {
                handler = new Handler(Looper.getMainLooper());
            }
            handler.post(new Runnable() {
                public void run() {
                    a.this.a(i);
                }
            });
        }

        public abstract void a(Typeface typeface);

        public final void a(final Typeface typeface, Handler handler) {
            if (handler == null) {
                handler = new Handler(Looper.getMainLooper());
            }
            handler.post(new Runnable() {
                public void run() {
                    a.this.a(typeface);
                }
            });
        }
    }

    public static Typeface a(Context context, int i, TypedValue typedValue, int i2, a aVar) throws NotFoundException {
        if (context.isRestricted()) {
            return null;
        }
        return a(context, i, typedValue, i2, aVar, null, true);
    }

    private static Typeface a(Context context, int i, TypedValue typedValue, int i2, a aVar, Handler handler, boolean z) {
        Resources resources = context.getResources();
        resources.getValue(i, typedValue, true);
        Typeface a2 = a(context, resources, typedValue, i, i2, aVar, handler, z);
        if (a2 != null || aVar != null) {
            return a2;
        }
        throw new NotFoundException("Font resource ID #0x" + Integer.toHexString(i) + " could not be retrieved.");
    }

    private static Typeface a(Context context, Resources resources, TypedValue typedValue, int i, int i2, a aVar, Handler handler, boolean z) {
        if (typedValue.string == null) {
            throw new NotFoundException("Resource \"" + resources.getResourceName(i) + "\" (" + Integer.toHexString(i) + ") is not a Font: " + typedValue);
        }
        String charSequence = typedValue.string.toString();
        if (!charSequence.startsWith("res/")) {
            if (aVar != null) {
                aVar.a(-3, handler);
            }
            return null;
        }
        Typeface a2 = c.a(resources, i, i2);
        if (a2 == null) {
            try {
                if (charSequence.toLowerCase().endsWith(".xml")) {
                    C0012a a3 = a.a((XmlPullParser) resources.getXml(i), resources);
                    if (a3 != null) {
                        return c.a(context, a3, resources, i, i2, aVar, handler, z);
                    }
                    Log.e("ResourcesCompat", "Failed to find font-family tag");
                    if (aVar != null) {
                        aVar.a(-3, handler);
                    }
                    return null;
                }
                Typeface a4 = c.a(context, resources, i, charSequence, i2);
                if (aVar == null) {
                    return a4;
                }
                if (a4 != null) {
                    aVar.a(a4, handler);
                    return a4;
                }
                aVar.a(-3, handler);
                return a4;
            } catch (XmlPullParserException e) {
                Log.e("ResourcesCompat", "Failed to parse xml resource " + charSequence, e);
            } catch (IOException e2) {
                Log.e("ResourcesCompat", "Failed to read xml resource " + charSequence, e2);
            }
        } else if (aVar == null) {
            return a2;
        } else {
            aVar.a(a2, handler);
            return a2;
        }
        if (aVar != null) {
            aVar.a(-3, handler);
        }
        return null;
    }

    public static Drawable a(Resources resources, int i, Theme theme) throws NotFoundException {
        return VERSION.SDK_INT >= 21 ? resources.getDrawable(i, theme) : resources.getDrawable(i);
    }
}
