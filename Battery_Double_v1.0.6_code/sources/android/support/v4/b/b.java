package android.support.v4.b;

import android.support.v4.h.d;
import java.io.FileDescriptor;
import java.io.PrintWriter;

public class b<D> {

    /* renamed from: a reason: collision with root package name */
    int f631a;

    /* renamed from: b reason: collision with root package name */
    C0013b<D> f632b;
    a<D> c;
    boolean d;
    boolean e;
    boolean f;
    boolean g;
    boolean h;

    public interface a<D> {
    }

    /* renamed from: android.support.v4.b.b$b reason: collision with other inner class name */
    public interface C0013b<D> {
    }

    public String a(D d2) {
        StringBuilder sb = new StringBuilder(64);
        d.a(d2, sb);
        sb.append("}");
        return sb.toString();
    }

    public final void a() {
        this.d = true;
        this.f = false;
        this.e = false;
        b();
    }

    public void a(int i, C0013b<D> bVar) {
        if (this.f632b != null) {
            throw new IllegalStateException("There is already a listener registered");
        }
        this.f632b = bVar;
        this.f631a = i;
    }

    public void a(a<D> aVar) {
        if (this.c != null) {
            throw new IllegalStateException("There is already a listener registered");
        }
        this.c = aVar;
    }

    public void a(C0013b<D> bVar) {
        if (this.f632b == null) {
            throw new IllegalStateException("No listener register");
        } else if (this.f632b != bVar) {
            throw new IllegalArgumentException("Attempting to unregister the wrong listener");
        } else {
            this.f632b = null;
        }
    }

    public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        printWriter.print(str);
        printWriter.print("mId=");
        printWriter.print(this.f631a);
        printWriter.print(" mListener=");
        printWriter.println(this.f632b);
        if (this.d || this.g || this.h) {
            printWriter.print(str);
            printWriter.print("mStarted=");
            printWriter.print(this.d);
            printWriter.print(" mContentChanged=");
            printWriter.print(this.g);
            printWriter.print(" mProcessingChange=");
            printWriter.println(this.h);
        }
        if (this.e || this.f) {
            printWriter.print(str);
            printWriter.print("mAbandoned=");
            printWriter.print(this.e);
            printWriter.print(" mReset=");
            printWriter.println(this.f);
        }
    }

    /* access modifiers changed from: protected */
    public void b() {
    }

    public void b(a<D> aVar) {
        if (this.c == null) {
            throw new IllegalStateException("No listener register");
        } else if (this.c != aVar) {
            throw new IllegalArgumentException("Attempting to unregister the wrong listener");
        } else {
            this.c = null;
        }
    }

    public void c() {
        this.d = false;
        d();
    }

    /* access modifiers changed from: protected */
    public void d() {
    }

    public void e() {
        f();
        this.f = true;
        this.d = false;
        this.e = false;
        this.g = false;
        this.h = false;
    }

    /* access modifiers changed from: protected */
    public void f() {
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(64);
        d.a(this, sb);
        sb.append(" id=");
        sb.append(this.f631a);
        sb.append("}");
        return sb.toString();
    }
}
