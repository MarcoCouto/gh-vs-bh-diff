package android.support.v4.widget;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Paint.Cap;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Path.FillType;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.support.v4.h.l;
import android.support.v4.i.b.b;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;

public class d extends Drawable implements Animatable {

    /* renamed from: a reason: collision with root package name */
    private static final Interpolator f837a = new LinearInterpolator();

    /* renamed from: b reason: collision with root package name */
    private static final Interpolator f838b = new b();
    private static final int[] c = {-16777216};
    private final a d = new a();
    private float e;
    private Resources f;
    private Animator g;
    /* access modifiers changed from: private */
    public float h;
    /* access modifiers changed from: private */
    public boolean i;

    private static class a {

        /* renamed from: a reason: collision with root package name */
        final RectF f843a = new RectF();

        /* renamed from: b reason: collision with root package name */
        final Paint f844b = new Paint();
        final Paint c = new Paint();
        final Paint d = new Paint();
        float e = 0.0f;
        float f = 0.0f;
        float g = 0.0f;
        float h = 5.0f;
        int[] i;
        int j;
        float k;
        float l;
        float m;
        boolean n;
        Path o;
        float p = 1.0f;
        float q;
        int r;
        int s;
        int t = 255;
        int u;

        a() {
            this.f844b.setStrokeCap(Cap.SQUARE);
            this.f844b.setAntiAlias(true);
            this.f844b.setStyle(Style.STROKE);
            this.c.setStyle(Style.FILL);
            this.c.setAntiAlias(true);
            this.d.setColor(0);
        }

        /* access modifiers changed from: 0000 */
        public int a() {
            return this.i[b()];
        }

        /* access modifiers changed from: 0000 */
        public void a(float f2) {
            this.h = f2;
            this.f844b.setStrokeWidth(f2);
        }

        /* access modifiers changed from: 0000 */
        public void a(float f2, float f3) {
            this.r = (int) f2;
            this.s = (int) f3;
        }

        /* access modifiers changed from: 0000 */
        public void a(int i2) {
            this.u = i2;
        }

        /* access modifiers changed from: 0000 */
        public void a(Canvas canvas, float f2, float f3, RectF rectF) {
            if (this.n) {
                if (this.o == null) {
                    this.o = new Path();
                    this.o.setFillType(FillType.EVEN_ODD);
                } else {
                    this.o.reset();
                }
                float min = Math.min(rectF.width(), rectF.height()) / 2.0f;
                float f4 = (((float) this.r) * this.p) / 2.0f;
                this.o.moveTo(0.0f, 0.0f);
                this.o.lineTo(((float) this.r) * this.p, 0.0f);
                this.o.lineTo((((float) this.r) * this.p) / 2.0f, ((float) this.s) * this.p);
                this.o.offset((min + rectF.centerX()) - f4, rectF.centerY() + (this.h / 2.0f));
                this.o.close();
                this.c.setColor(this.u);
                this.c.setAlpha(this.t);
                canvas.save();
                canvas.rotate(f2 + f3, rectF.centerX(), rectF.centerY());
                canvas.drawPath(this.o, this.c);
                canvas.restore();
            }
        }

        /* access modifiers changed from: 0000 */
        public void a(Canvas canvas, Rect rect) {
            RectF rectF = this.f843a;
            float f2 = this.q + (this.h / 2.0f);
            if (this.q <= 0.0f) {
                f2 = (((float) Math.min(rect.width(), rect.height())) / 2.0f) - Math.max((((float) this.r) * this.p) / 2.0f, this.h / 2.0f);
            }
            rectF.set(((float) rect.centerX()) - f2, ((float) rect.centerY()) - f2, ((float) rect.centerX()) + f2, f2 + ((float) rect.centerY()));
            float f3 = (this.e + this.g) * 360.0f;
            float f4 = ((this.f + this.g) * 360.0f) - f3;
            this.f844b.setColor(this.u);
            this.f844b.setAlpha(this.t);
            float f5 = this.h / 2.0f;
            rectF.inset(f5, f5);
            canvas.drawCircle(rectF.centerX(), rectF.centerY(), rectF.width() / 2.0f, this.d);
            rectF.inset(-f5, -f5);
            canvas.drawArc(rectF, f3, f4, false, this.f844b);
            a(canvas, f3, f4, rectF);
        }

        /* access modifiers changed from: 0000 */
        public void a(ColorFilter colorFilter) {
            this.f844b.setColorFilter(colorFilter);
        }

        /* access modifiers changed from: 0000 */
        public void a(boolean z) {
            if (this.n != z) {
                this.n = z;
            }
        }

        /* access modifiers changed from: 0000 */
        public void a(int[] iArr) {
            this.i = iArr;
            b(0);
        }

        /* access modifiers changed from: 0000 */
        public int b() {
            return (this.j + 1) % this.i.length;
        }

        /* access modifiers changed from: 0000 */
        public void b(float f2) {
            this.e = f2;
        }

        /* access modifiers changed from: 0000 */
        public void b(int i2) {
            this.j = i2;
            this.u = this.i[this.j];
        }

        /* access modifiers changed from: 0000 */
        public void c() {
            b(b());
        }

        /* access modifiers changed from: 0000 */
        public void c(float f2) {
            this.f = f2;
        }

        /* access modifiers changed from: 0000 */
        public void c(int i2) {
            this.t = i2;
        }

        /* access modifiers changed from: 0000 */
        public int d() {
            return this.t;
        }

        /* access modifiers changed from: 0000 */
        public void d(float f2) {
            this.g = f2;
        }

        /* access modifiers changed from: 0000 */
        public float e() {
            return this.e;
        }

        /* access modifiers changed from: 0000 */
        public void e(float f2) {
            this.q = f2;
        }

        /* access modifiers changed from: 0000 */
        public float f() {
            return this.k;
        }

        /* access modifiers changed from: 0000 */
        public void f(float f2) {
            if (f2 != this.p) {
                this.p = f2;
            }
        }

        /* access modifiers changed from: 0000 */
        public float g() {
            return this.l;
        }

        /* access modifiers changed from: 0000 */
        public int h() {
            return this.i[this.j];
        }

        /* access modifiers changed from: 0000 */
        public float i() {
            return this.f;
        }

        /* access modifiers changed from: 0000 */
        public float j() {
            return this.m;
        }

        /* access modifiers changed from: 0000 */
        public void k() {
            this.k = this.e;
            this.l = this.f;
            this.m = this.g;
        }

        /* access modifiers changed from: 0000 */
        public void l() {
            this.k = 0.0f;
            this.l = 0.0f;
            this.m = 0.0f;
            b(0.0f);
            c(0.0f);
            d(0.0f);
        }
    }

    public d(Context context) {
        this.f = ((Context) l.a(context)).getResources();
        this.d.a(c);
        a(2.5f);
        a();
    }

    private int a(float f2, int i2, int i3) {
        int i4 = (i2 >> 24) & 255;
        int i5 = (i2 >> 16) & 255;
        int i6 = (i2 >> 8) & 255;
        int i7 = i2 & 255;
        return ((i4 + ((int) (((float) (((i3 >> 24) & 255) - i4)) * f2))) << 24) | ((i5 + ((int) (((float) (((i3 >> 16) & 255) - i5)) * f2))) << 16) | ((((int) (((float) (((i3 >> 8) & 255) - i6)) * f2)) + i6) << 8) | (((int) (((float) ((i3 & 255) - i7)) * f2)) + i7);
    }

    private void a() {
        final a aVar = this.d;
        ValueAnimator ofFloat = ValueAnimator.ofFloat(new float[]{0.0f, 1.0f});
        ofFloat.addUpdateListener(new AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float floatValue = ((Float) valueAnimator.getAnimatedValue()).floatValue();
                d.this.a(floatValue, aVar);
                d.this.a(floatValue, aVar, false);
                d.this.invalidateSelf();
            }
        });
        ofFloat.setRepeatCount(-1);
        ofFloat.setRepeatMode(1);
        ofFloat.setInterpolator(f837a);
        ofFloat.addListener(new AnimatorListener() {
            public void onAnimationCancel(Animator animator) {
            }

            public void onAnimationEnd(Animator animator) {
            }

            public void onAnimationRepeat(Animator animator) {
                d.this.a(1.0f, aVar, true);
                aVar.k();
                aVar.c();
                if (d.this.i) {
                    d.this.i = false;
                    animator.cancel();
                    animator.setDuration(1332);
                    animator.start();
                    aVar.a(false);
                    return;
                }
                d.this.h = d.this.h + 1.0f;
            }

            public void onAnimationStart(Animator animator) {
                d.this.h = 0.0f;
            }
        });
        this.g = ofFloat;
    }

    private void a(float f2, float f3, float f4, float f5) {
        a aVar = this.d;
        float f6 = this.f.getDisplayMetrics().density;
        aVar.a(f3 * f6);
        aVar.e(f2 * f6);
        aVar.b(0);
        aVar.a(f4 * f6, f6 * f5);
    }

    /* access modifiers changed from: private */
    public void a(float f2, a aVar) {
        if (f2 > 0.75f) {
            aVar.a(a((f2 - 0.75f) / 0.25f, aVar.h(), aVar.a()));
        } else {
            aVar.a(aVar.h());
        }
    }

    /* access modifiers changed from: private */
    public void a(float f2, a aVar, boolean z) {
        float f3;
        float interpolation;
        if (this.i) {
            b(f2, aVar);
        } else if (f2 != 1.0f || z) {
            float j = aVar.j();
            if (f2 < 0.5f) {
                float f4 = f2 / 0.5f;
                interpolation = aVar.f();
                f3 = (f838b.getInterpolation(f4) * 0.79f) + 0.01f + interpolation;
            } else {
                f3 = aVar.f() + 0.79f;
                interpolation = f3 - (((1.0f - f838b.getInterpolation((f2 - 0.5f) / 0.5f)) * 0.79f) + 0.01f);
            }
            float f5 = j + (0.20999998f * f2);
            float f6 = 216.0f * (this.h + f2);
            aVar.b(interpolation);
            aVar.c(f3);
            aVar.d(f5);
            d(f6);
        }
    }

    private void b(float f2, a aVar) {
        a(f2, aVar);
        float floor = (float) (Math.floor((double) (aVar.j() / 0.8f)) + 1.0d);
        aVar.b(aVar.f() + (((aVar.g() - 0.01f) - aVar.f()) * f2));
        aVar.c(aVar.g());
        aVar.d(((floor - aVar.j()) * f2) + aVar.j());
    }

    private void d(float f2) {
        this.e = f2;
    }

    public void a(float f2) {
        this.d.a(f2);
        invalidateSelf();
    }

    public void a(float f2, float f3) {
        this.d.b(f2);
        this.d.c(f3);
        invalidateSelf();
    }

    public void a(int i2) {
        if (i2 == 0) {
            a(11.0f, 3.0f, 12.0f, 6.0f);
        } else {
            a(7.5f, 2.5f, 10.0f, 5.0f);
        }
        invalidateSelf();
    }

    public void a(boolean z) {
        this.d.a(z);
        invalidateSelf();
    }

    public void a(int... iArr) {
        this.d.a(iArr);
        this.d.b(0);
        invalidateSelf();
    }

    public void b(float f2) {
        this.d.f(f2);
        invalidateSelf();
    }

    public void c(float f2) {
        this.d.d(f2);
        invalidateSelf();
    }

    public void draw(Canvas canvas) {
        Rect bounds = getBounds();
        canvas.save();
        canvas.rotate(this.e, bounds.exactCenterX(), bounds.exactCenterY());
        this.d.a(canvas, bounds);
        canvas.restore();
    }

    public int getAlpha() {
        return this.d.d();
    }

    public int getOpacity() {
        return -3;
    }

    public boolean isRunning() {
        return this.g.isRunning();
    }

    public void setAlpha(int i2) {
        this.d.c(i2);
        invalidateSelf();
    }

    public void setColorFilter(ColorFilter colorFilter) {
        this.d.a(colorFilter);
        invalidateSelf();
    }

    public void start() {
        this.g.cancel();
        this.d.k();
        if (this.d.i() != this.d.e()) {
            this.i = true;
            this.g.setDuration(666);
            this.g.start();
            return;
        }
        this.d.b(0);
        this.d.l();
        this.g.setDuration(1332);
        this.g.start();
    }

    public void stop() {
        this.g.cancel();
        d(0.0f);
        this.d.a(false);
        this.d.b(0);
        this.d.l();
        invalidateSelf();
    }
}
