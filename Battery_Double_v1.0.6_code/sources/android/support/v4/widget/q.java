package android.support.v4.widget;

import android.content.Context;
import android.support.v4.i.t;
import android.util.Log;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.animation.Interpolator;
import android.widget.OverScroller;
import java.util.Arrays;

public class q {
    private static final Interpolator v = new Interpolator() {
        public float getInterpolation(float f) {
            float f2 = f - 1.0f;
            return (f2 * f2 * f2 * f2 * f2) + 1.0f;
        }
    };

    /* renamed from: a reason: collision with root package name */
    private int f862a;

    /* renamed from: b reason: collision with root package name */
    private int f863b;
    private int c = -1;
    private float[] d;
    private float[] e;
    private float[] f;
    private float[] g;
    private int[] h;
    private int[] i;
    private int[] j;
    private int k;
    private VelocityTracker l;
    private float m;
    private float n;
    private int o;
    private int p;
    private OverScroller q;
    private final a r;
    private View s;
    private boolean t;
    private final ViewGroup u;
    private final Runnable w = new Runnable() {
        public void run() {
            q.this.b(0);
        }
    };

    public static abstract class a {
        public int a(View view) {
            return 0;
        }

        public int a(View view, int i, int i2) {
            return 0;
        }

        public void a(int i) {
        }

        public void a(int i, int i2) {
        }

        public void a(View view, float f, float f2) {
        }

        public void a(View view, int i, int i2, int i3, int i4) {
        }

        public abstract boolean a(View view, int i);

        public int b(View view) {
            return 0;
        }

        public int b(View view, int i, int i2) {
            return 0;
        }

        public void b(int i, int i2) {
        }

        public void b(View view, int i) {
        }

        public boolean b(int i) {
            return false;
        }

        public int c(int i) {
            return i;
        }
    }

    private q(Context context, ViewGroup viewGroup, a aVar) {
        if (viewGroup == null) {
            throw new IllegalArgumentException("Parent view may not be null");
        } else if (aVar == null) {
            throw new IllegalArgumentException("Callback may not be null");
        } else {
            this.u = viewGroup;
            this.r = aVar;
            ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
            this.o = (int) ((context.getResources().getDisplayMetrics().density * 20.0f) + 0.5f);
            this.f863b = viewConfiguration.getScaledTouchSlop();
            this.m = (float) viewConfiguration.getScaledMaximumFlingVelocity();
            this.n = (float) viewConfiguration.getScaledMinimumFlingVelocity();
            this.q = new OverScroller(context, v);
        }
    }

    private float a(float f2) {
        return (float) Math.sin((double) ((f2 - 0.5f) * 0.47123894f));
    }

    private float a(float f2, float f3, float f4) {
        float abs = Math.abs(f2);
        if (abs < f3) {
            return 0.0f;
        }
        return abs > f4 ? f2 <= 0.0f ? -f4 : f4 : f2;
    }

    private int a(int i2, int i3, int i4) {
        if (i2 == 0) {
            return 0;
        }
        int width = this.u.getWidth();
        int i5 = width / 2;
        float a2 = (a(Math.min(1.0f, ((float) Math.abs(i2)) / ((float) width))) * ((float) i5)) + ((float) i5);
        int abs = Math.abs(i3);
        return Math.min(abs > 0 ? Math.round(Math.abs(a2 / ((float) abs)) * 1000.0f) * 4 : (int) (((((float) Math.abs(i2)) / ((float) i4)) + 1.0f) * 256.0f), 600);
    }

    private int a(View view, int i2, int i3, int i4, int i5) {
        int b2 = b(i4, (int) this.n, (int) this.m);
        int b3 = b(i5, (int) this.n, (int) this.m);
        int abs = Math.abs(i2);
        int abs2 = Math.abs(i3);
        int abs3 = Math.abs(b2);
        int abs4 = Math.abs(b3);
        int i6 = abs3 + abs4;
        int i7 = abs + abs2;
        return (int) (((b3 != 0 ? ((float) abs4) / ((float) i6) : ((float) abs2) / ((float) i7)) * ((float) a(i3, b3, this.r.a(view)))) + ((b2 != 0 ? ((float) abs3) / ((float) i6) : ((float) abs) / ((float) i7)) * ((float) a(i2, b2, this.r.b(view)))));
    }

    public static q a(ViewGroup viewGroup, float f2, a aVar) {
        q a2 = a(viewGroup, aVar);
        a2.f863b = (int) (((float) a2.f863b) * (1.0f / f2));
        return a2;
    }

    public static q a(ViewGroup viewGroup, a aVar) {
        return new q(viewGroup.getContext(), viewGroup, aVar);
    }

    private void a(float f2, float f3) {
        this.t = true;
        this.r.a(this.s, f2, f3);
        this.t = false;
        if (this.f862a == 1) {
            b(0);
        }
    }

    private void a(float f2, float f3, int i2) {
        d(i2);
        float[] fArr = this.d;
        this.f[i2] = f2;
        fArr[i2] = f2;
        float[] fArr2 = this.e;
        this.g[i2] = f3;
        fArr2[i2] = f3;
        this.h[i2] = d((int) f2, (int) f3);
        this.k |= 1 << i2;
    }

    private boolean a(float f2, float f3, int i2, int i3) {
        float abs = Math.abs(f2);
        float abs2 = Math.abs(f3);
        if ((this.h[i2] & i3) != i3 || (this.p & i3) == 0 || (this.j[i2] & i3) == i3 || (this.i[i2] & i3) == i3) {
            return false;
        }
        if (abs <= ((float) this.f863b) && abs2 <= ((float) this.f863b)) {
            return false;
        }
        if (abs >= abs2 * 0.5f || !this.r.b(i3)) {
            return (this.i[i2] & i3) == 0 && abs > ((float) this.f863b);
        }
        int[] iArr = this.j;
        iArr[i2] = iArr[i2] | i3;
        return false;
    }

    private boolean a(int i2, int i3, int i4, int i5) {
        int left = this.s.getLeft();
        int top = this.s.getTop();
        int i6 = i2 - left;
        int i7 = i3 - top;
        if (i6 == 0 && i7 == 0) {
            this.q.abortAnimation();
            b(0);
            return false;
        }
        this.q.startScroll(left, top, i6, i7, a(this.s, i6, i7, i4, i5));
        b(2);
        return true;
    }

    private boolean a(View view, float f2, float f3) {
        if (view == null) {
            return false;
        }
        boolean z = this.r.b(view) > 0;
        boolean z2 = this.r.a(view) > 0;
        if (z && z2) {
            return (f2 * f2) + (f3 * f3) > ((float) (this.f863b * this.f863b));
        }
        if (z) {
            return Math.abs(f2) > ((float) this.f863b);
        }
        if (z2) {
            return Math.abs(f3) > ((float) this.f863b);
        }
        return false;
    }

    private int b(int i2, int i3, int i4) {
        int abs = Math.abs(i2);
        if (abs < i3) {
            return 0;
        }
        return abs > i4 ? i2 <= 0 ? -i4 : i4 : i2;
    }

    private void b(float f2, float f3, int i2) {
        int i3 = 1;
        if (!a(f2, f3, i2, 1)) {
            i3 = 0;
        }
        if (a(f3, f2, i2, 4)) {
            i3 |= 4;
        }
        if (a(f2, f3, i2, 2)) {
            i3 |= 2;
        }
        if (a(f3, f2, i2, 8)) {
            i3 |= 8;
        }
        if (i3 != 0) {
            int[] iArr = this.i;
            iArr[i2] = iArr[i2] | i3;
            this.r.b(i3, i2);
        }
    }

    private void b(int i2, int i3, int i4, int i5) {
        int i6;
        int i7;
        int left = this.s.getLeft();
        int top = this.s.getTop();
        if (i4 != 0) {
            i6 = this.r.b(this.s, i2, i4);
            t.d(this.s, i6 - left);
        } else {
            i6 = i2;
        }
        if (i5 != 0) {
            i7 = this.r.a(this.s, i3, i5);
            t.c(this.s, i7 - top);
        } else {
            i7 = i3;
        }
        if (i4 != 0 || i5 != 0) {
            this.r.a(this.s, i6, i7, i6 - left, i7 - top);
        }
    }

    private void c() {
        if (this.d != null) {
            Arrays.fill(this.d, 0.0f);
            Arrays.fill(this.e, 0.0f);
            Arrays.fill(this.f, 0.0f);
            Arrays.fill(this.g, 0.0f);
            Arrays.fill(this.h, 0);
            Arrays.fill(this.i, 0);
            Arrays.fill(this.j, 0);
            this.k = 0;
        }
    }

    private void c(int i2) {
        if (this.d != null && a(i2)) {
            this.d[i2] = 0.0f;
            this.e[i2] = 0.0f;
            this.f[i2] = 0.0f;
            this.g[i2] = 0.0f;
            this.h[i2] = 0;
            this.i[i2] = 0;
            this.j[i2] = 0;
            this.k &= (1 << i2) ^ -1;
        }
    }

    private void c(MotionEvent motionEvent) {
        int pointerCount = motionEvent.getPointerCount();
        for (int i2 = 0; i2 < pointerCount; i2++) {
            int pointerId = motionEvent.getPointerId(i2);
            if (e(pointerId)) {
                float x = motionEvent.getX(i2);
                float y = motionEvent.getY(i2);
                this.f[pointerId] = x;
                this.g[pointerId] = y;
            }
        }
    }

    private int d(int i2, int i3) {
        int i4 = 0;
        if (i2 < this.u.getLeft() + this.o) {
            i4 = 1;
        }
        if (i3 < this.u.getTop() + this.o) {
            i4 |= 4;
        }
        if (i2 > this.u.getRight() - this.o) {
            i4 |= 2;
        }
        return i3 > this.u.getBottom() - this.o ? i4 | 8 : i4;
    }

    private void d() {
        this.l.computeCurrentVelocity(1000, this.m);
        a(a(this.l.getXVelocity(this.c), this.n, this.m), a(this.l.getYVelocity(this.c), this.n, this.m));
    }

    private void d(int i2) {
        if (this.d == null || this.d.length <= i2) {
            float[] fArr = new float[(i2 + 1)];
            float[] fArr2 = new float[(i2 + 1)];
            float[] fArr3 = new float[(i2 + 1)];
            float[] fArr4 = new float[(i2 + 1)];
            int[] iArr = new int[(i2 + 1)];
            int[] iArr2 = new int[(i2 + 1)];
            int[] iArr3 = new int[(i2 + 1)];
            if (this.d != null) {
                System.arraycopy(this.d, 0, fArr, 0, this.d.length);
                System.arraycopy(this.e, 0, fArr2, 0, this.e.length);
                System.arraycopy(this.f, 0, fArr3, 0, this.f.length);
                System.arraycopy(this.g, 0, fArr4, 0, this.g.length);
                System.arraycopy(this.h, 0, iArr, 0, this.h.length);
                System.arraycopy(this.i, 0, iArr2, 0, this.i.length);
                System.arraycopy(this.j, 0, iArr3, 0, this.j.length);
            }
            this.d = fArr;
            this.e = fArr2;
            this.f = fArr3;
            this.g = fArr4;
            this.h = iArr;
            this.i = iArr2;
            this.j = iArr3;
        }
    }

    private boolean e(int i2) {
        if (a(i2)) {
            return true;
        }
        Log.e("ViewDragHelper", "Ignoring pointerId=" + i2 + " because ACTION_DOWN was not received " + "for this pointer before ACTION_MOVE. It likely happened because " + " ViewDragHelper did not receive all the events in the event stream.");
        return false;
    }

    public int a() {
        return this.f863b;
    }

    public void a(View view, int i2) {
        if (view.getParent() != this.u) {
            throw new IllegalArgumentException("captureChildView: parameter must be a descendant of the ViewDragHelper's tracked parent view (" + this.u + ")");
        }
        this.s = view;
        this.c = i2;
        this.r.b(view, i2);
        b(1);
    }

    public boolean a(int i2) {
        return (this.k & (1 << i2)) != 0;
    }

    public boolean a(int i2, int i3) {
        if (this.t) {
            return a(i2, i3, (int) this.l.getXVelocity(this.c), (int) this.l.getYVelocity(this.c));
        }
        throw new IllegalStateException("Cannot settleCapturedViewAt outside of a call to Callback#onViewReleased");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00ff, code lost:
        if (r8 != r7) goto L_0x010e;
     */
    public boolean a(MotionEvent motionEvent) {
        int actionMasked = motionEvent.getActionMasked();
        int actionIndex = motionEvent.getActionIndex();
        if (actionMasked == 0) {
            b();
        }
        if (this.l == null) {
            this.l = VelocityTracker.obtain();
        }
        this.l.addMovement(motionEvent);
        switch (actionMasked) {
            case 0:
                float x = motionEvent.getX();
                float y = motionEvent.getY();
                int pointerId = motionEvent.getPointerId(0);
                a(x, y, pointerId);
                View c2 = c((int) x, (int) y);
                if (c2 == this.s && this.f862a == 2) {
                    b(c2, pointerId);
                }
                int i2 = this.h[pointerId];
                if ((this.p & i2) != 0) {
                    this.r.a(i2 & this.p, pointerId);
                    break;
                }
                break;
            case 1:
            case 3:
                b();
                break;
            case 2:
                if (!(this.d == null || this.e == null)) {
                    int pointerCount = motionEvent.getPointerCount();
                    for (int i3 = 0; i3 < pointerCount; i3++) {
                        int pointerId2 = motionEvent.getPointerId(i3);
                        if (e(pointerId2)) {
                            float x2 = motionEvent.getX(i3);
                            float y2 = motionEvent.getY(i3);
                            float f2 = x2 - this.d[pointerId2];
                            float f3 = y2 - this.e[pointerId2];
                            View c3 = c((int) x2, (int) y2);
                            boolean z = c3 != null && a(c3, f2, f3);
                            if (z) {
                                int left = c3.getLeft();
                                int b2 = this.r.b(c3, ((int) f2) + left, (int) f2);
                                int top = c3.getTop();
                                int a2 = this.r.a(c3, ((int) f3) + top, (int) f3);
                                int b3 = this.r.b(c3);
                                int a3 = this.r.a(c3);
                                if (b3 != 0) {
                                    if (b3 > 0) {
                                    }
                                }
                                if (a3 != 0) {
                                    if (a3 > 0 && a2 == top) {
                                    }
                                }
                                c(motionEvent);
                                break;
                            }
                            b(f2, f3, pointerId2);
                            if (this.f862a != 1) {
                                if (z && b(c3, pointerId2)) {
                                }
                            }
                            c(motionEvent);
                        }
                    }
                    c(motionEvent);
                }
                break;
            case 5:
                int pointerId3 = motionEvent.getPointerId(actionIndex);
                float x3 = motionEvent.getX(actionIndex);
                float y3 = motionEvent.getY(actionIndex);
                a(x3, y3, pointerId3);
                if (this.f862a != 0) {
                    if (this.f862a == 2) {
                        View c4 = c((int) x3, (int) y3);
                        if (c4 == this.s) {
                            b(c4, pointerId3);
                            break;
                        }
                    }
                } else {
                    int i4 = this.h[pointerId3];
                    if ((this.p & i4) != 0) {
                        this.r.a(i4 & this.p, pointerId3);
                        break;
                    }
                }
                break;
            case 6:
                c(motionEvent.getPointerId(actionIndex));
                break;
        }
        return this.f862a == 1;
    }

    public boolean a(View view, int i2, int i3) {
        this.s = view;
        this.c = -1;
        boolean a2 = a(i2, i3, 0, 0);
        if (!a2 && this.f862a == 0 && this.s != null) {
            this.s = null;
        }
        return a2;
    }

    public boolean a(boolean z) {
        boolean z2;
        if (this.f862a == 2) {
            boolean computeScrollOffset = this.q.computeScrollOffset();
            int currX = this.q.getCurrX();
            int currY = this.q.getCurrY();
            int left = currX - this.s.getLeft();
            int top = currY - this.s.getTop();
            if (left != 0) {
                t.d(this.s, left);
            }
            if (top != 0) {
                t.c(this.s, top);
            }
            if (!(left == 0 && top == 0)) {
                this.r.a(this.s, currX, currY, left, top);
            }
            if (computeScrollOffset && currX == this.q.getFinalX() && currY == this.q.getFinalY()) {
                this.q.abortAnimation();
                z2 = false;
            } else {
                z2 = computeScrollOffset;
            }
            if (!z2) {
                if (z) {
                    this.u.post(this.w);
                } else {
                    b(0);
                }
            }
        }
        return this.f862a == 2;
    }

    public void b() {
        this.c = -1;
        c();
        if (this.l != null) {
            this.l.recycle();
            this.l = null;
        }
    }

    /* access modifiers changed from: 0000 */
    public void b(int i2) {
        this.u.removeCallbacks(this.w);
        if (this.f862a != i2) {
            this.f862a = i2;
            this.r.a(i2);
            if (this.f862a == 0) {
                this.s = null;
            }
        }
    }

    public void b(MotionEvent motionEvent) {
        int i2;
        int i3 = 0;
        int actionMasked = motionEvent.getActionMasked();
        int actionIndex = motionEvent.getActionIndex();
        if (actionMasked == 0) {
            b();
        }
        if (this.l == null) {
            this.l = VelocityTracker.obtain();
        }
        this.l.addMovement(motionEvent);
        switch (actionMasked) {
            case 0:
                float x = motionEvent.getX();
                float y = motionEvent.getY();
                int pointerId = motionEvent.getPointerId(0);
                View c2 = c((int) x, (int) y);
                a(x, y, pointerId);
                b(c2, pointerId);
                int i4 = this.h[pointerId];
                if ((this.p & i4) != 0) {
                    this.r.a(i4 & this.p, pointerId);
                    return;
                }
                return;
            case 1:
                if (this.f862a == 1) {
                    d();
                }
                b();
                return;
            case 2:
                if (this.f862a != 1) {
                    int pointerCount = motionEvent.getPointerCount();
                    while (i3 < pointerCount) {
                        int pointerId2 = motionEvent.getPointerId(i3);
                        if (e(pointerId2)) {
                            float x2 = motionEvent.getX(i3);
                            float y2 = motionEvent.getY(i3);
                            float f2 = x2 - this.d[pointerId2];
                            float f3 = y2 - this.e[pointerId2];
                            b(f2, f3, pointerId2);
                            if (this.f862a != 1) {
                                View c3 = c((int) x2, (int) y2);
                                if (a(c3, f2, f3) && b(c3, pointerId2)) {
                                }
                            }
                            c(motionEvent);
                            return;
                        }
                        i3++;
                    }
                    c(motionEvent);
                    return;
                } else if (e(this.c)) {
                    int findPointerIndex = motionEvent.findPointerIndex(this.c);
                    float x3 = motionEvent.getX(findPointerIndex);
                    int i5 = (int) (x3 - this.f[this.c]);
                    int y3 = (int) (motionEvent.getY(findPointerIndex) - this.g[this.c]);
                    b(this.s.getLeft() + i5, this.s.getTop() + y3, i5, y3);
                    c(motionEvent);
                    return;
                } else {
                    return;
                }
            case 3:
                if (this.f862a == 1) {
                    a(0.0f, 0.0f);
                }
                b();
                return;
            case 5:
                int pointerId3 = motionEvent.getPointerId(actionIndex);
                float x4 = motionEvent.getX(actionIndex);
                float y4 = motionEvent.getY(actionIndex);
                a(x4, y4, pointerId3);
                if (this.f862a == 0) {
                    b(c((int) x4, (int) y4), pointerId3);
                    int i6 = this.h[pointerId3];
                    if ((this.p & i6) != 0) {
                        this.r.a(i6 & this.p, pointerId3);
                        return;
                    }
                    return;
                } else if (b((int) x4, (int) y4)) {
                    b(this.s, pointerId3);
                    return;
                } else {
                    return;
                }
            case 6:
                int pointerId4 = motionEvent.getPointerId(actionIndex);
                if (this.f862a == 1 && pointerId4 == this.c) {
                    int pointerCount2 = motionEvent.getPointerCount();
                    while (true) {
                        if (i3 < pointerCount2) {
                            int pointerId5 = motionEvent.getPointerId(i3);
                            if (pointerId5 != this.c) {
                                if (c((int) motionEvent.getX(i3), (int) motionEvent.getY(i3)) == this.s && b(this.s, pointerId5)) {
                                    i2 = this.c;
                                }
                            }
                            i3++;
                        } else {
                            i2 = -1;
                        }
                    }
                    if (i2 == -1) {
                        d();
                    }
                }
                c(pointerId4);
                return;
            default:
                return;
        }
    }

    public boolean b(int i2, int i3) {
        return b(this.s, i2, i3);
    }

    /* access modifiers changed from: 0000 */
    public boolean b(View view, int i2) {
        if (view == this.s && this.c == i2) {
            return true;
        }
        if (view == null || !this.r.a(view, i2)) {
            return false;
        }
        this.c = i2;
        a(view, i2);
        return true;
    }

    public boolean b(View view, int i2, int i3) {
        return view != null && i2 >= view.getLeft() && i2 < view.getRight() && i3 >= view.getTop() && i3 < view.getBottom();
    }

    public View c(int i2, int i3) {
        for (int childCount = this.u.getChildCount() - 1; childCount >= 0; childCount--) {
            View childAt = this.u.getChildAt(this.r.c(childCount));
            if (i2 >= childAt.getLeft() && i2 < childAt.getRight() && i3 >= childAt.getTop() && i3 < childAt.getBottom()) {
                return childAt;
            }
        }
        return null;
    }
}
