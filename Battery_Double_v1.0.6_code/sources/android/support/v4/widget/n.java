package android.support.v4.widget;

import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.util.Log;
import android.widget.TextView;
import java.lang.reflect.Field;

public final class n {

    /* renamed from: a reason: collision with root package name */
    static final f f859a;

    static class a extends f {
        a() {
        }

        public int a(TextView textView) {
            return textView.getMaxLines();
        }
    }

    static class b extends a {
        b() {
        }

        public void a(TextView textView, Drawable drawable, Drawable drawable2, Drawable drawable3, Drawable drawable4) {
            boolean z = textView.getLayoutDirection() == 1;
            Drawable drawable5 = z ? drawable3 : drawable;
            if (!z) {
                drawable = drawable3;
            }
            textView.setCompoundDrawables(drawable5, drawable2, drawable, drawable4);
        }
    }

    static class c extends b {
        c() {
        }

        public void a(TextView textView, Drawable drawable, Drawable drawable2, Drawable drawable3, Drawable drawable4) {
            textView.setCompoundDrawablesRelative(drawable, drawable2, drawable3, drawable4);
        }
    }

    static class d extends c {
        d() {
        }

        public void a(TextView textView, int i) {
            textView.setTextAppearance(i);
        }
    }

    static class e extends d {
        e() {
        }
    }

    static class f {

        /* renamed from: a reason: collision with root package name */
        private static Field f860a;

        /* renamed from: b reason: collision with root package name */
        private static boolean f861b;
        private static Field c;
        private static boolean d;

        f() {
        }

        private static int a(Field field, TextView textView) {
            try {
                return field.getInt(textView);
            } catch (IllegalAccessException e) {
                Log.d("TextViewCompatBase", "Could not retrieve value of " + field.getName() + " field.");
                return -1;
            }
        }

        private static Field a(String str) {
            Field field = null;
            try {
                field = TextView.class.getDeclaredField(str);
                field.setAccessible(true);
                return field;
            } catch (NoSuchFieldException e) {
                Log.e("TextViewCompatBase", "Could not retrieve " + str + " field.");
                return field;
            }
        }

        public int a(TextView textView) {
            if (!d) {
                c = a("mMaxMode");
                d = true;
            }
            if (c != null && a(c, textView) == 1) {
                if (!f861b) {
                    f860a = a("mMaximum");
                    f861b = true;
                }
                if (f860a != null) {
                    return a(f860a, textView);
                }
            }
            return -1;
        }

        public void a(TextView textView, int i) {
            textView.setTextAppearance(textView.getContext(), i);
        }

        public void a(TextView textView, Drawable drawable, Drawable drawable2, Drawable drawable3, Drawable drawable4) {
            textView.setCompoundDrawables(drawable, drawable2, drawable3, drawable4);
        }
    }

    static {
        if (android.support.v4.f.a.a()) {
            f859a = new e();
        } else if (VERSION.SDK_INT >= 23) {
            f859a = new d();
        } else if (VERSION.SDK_INT >= 18) {
            f859a = new c();
        } else if (VERSION.SDK_INT >= 17) {
            f859a = new b();
        } else if (VERSION.SDK_INT >= 16) {
            f859a = new a();
        } else {
            f859a = new f();
        }
    }

    public static int a(TextView textView) {
        return f859a.a(textView);
    }

    public static void a(TextView textView, int i) {
        f859a.a(textView, i);
    }

    public static void a(TextView textView, Drawable drawable, Drawable drawable2, Drawable drawable3, Drawable drawable4) {
        f859a.a(textView, drawable, drawable2, drawable3, drawable4);
    }
}
