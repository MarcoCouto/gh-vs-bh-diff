package android.support.v4.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build.VERSION;
import android.support.v4.i.j;
import android.support.v4.i.l;
import android.support.v4.i.m;
import android.support.v4.i.o;
import android.support.v4.i.t;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Transformation;
import android.widget.AbsListView;
import android.widget.ListView;

public class SwipeRefreshLayout extends ViewGroup implements j, m {
    private static final int[] D = {16842766};
    private static final String m = SwipeRefreshLayout.class.getSimpleName();
    private int A;
    private boolean B;
    private final DecelerateInterpolator C;
    private int E;
    private Animation F;
    private Animation G;
    private Animation H;
    private Animation I;
    private Animation J;
    private int K;
    private a L;
    private AnimationListener M;
    private final Animation N;
    private final Animation O;

    /* renamed from: a reason: collision with root package name */
    b f816a;

    /* renamed from: b reason: collision with root package name */
    boolean f817b;
    int c;
    boolean d;
    c e;
    protected int f;
    float g;
    protected int h;
    int i;
    d j;
    boolean k;
    boolean l;
    private View n;
    private int o;
    private float p;
    private float q;
    private final o r;
    private final l s;
    private final int[] t;
    private final int[] u;
    private boolean v;
    private int w;
    private float x;
    private float y;
    private boolean z;

    public interface a {
        boolean a(SwipeRefreshLayout swipeRefreshLayout, View view);
    }

    public interface b {
        void a();
    }

    public SwipeRefreshLayout(Context context) {
        this(context, null);
    }

    public SwipeRefreshLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f817b = false;
        this.p = -1.0f;
        this.t = new int[2];
        this.u = new int[2];
        this.A = -1;
        this.E = -1;
        this.M = new AnimationListener() {
            public void onAnimationEnd(Animation animation) {
                if (SwipeRefreshLayout.this.f817b) {
                    SwipeRefreshLayout.this.j.setAlpha(255);
                    SwipeRefreshLayout.this.j.start();
                    if (SwipeRefreshLayout.this.k && SwipeRefreshLayout.this.f816a != null) {
                        SwipeRefreshLayout.this.f816a.a();
                    }
                    SwipeRefreshLayout.this.c = SwipeRefreshLayout.this.e.getTop();
                    return;
                }
                SwipeRefreshLayout.this.a();
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationStart(Animation animation) {
            }
        };
        this.N = new Animation() {
            public void applyTransformation(float f, Transformation transformation) {
                SwipeRefreshLayout.this.setTargetOffsetTopAndBottom((((int) (((float) ((!SwipeRefreshLayout.this.l ? SwipeRefreshLayout.this.i - Math.abs(SwipeRefreshLayout.this.h) : SwipeRefreshLayout.this.i) - SwipeRefreshLayout.this.f)) * f)) + SwipeRefreshLayout.this.f) - SwipeRefreshLayout.this.e.getTop());
                SwipeRefreshLayout.this.j.b(1.0f - f);
            }
        };
        this.O = new Animation() {
            public void applyTransformation(float f, Transformation transformation) {
                SwipeRefreshLayout.this.a(f);
            }
        };
        this.o = ViewConfiguration.get(context).getScaledTouchSlop();
        this.w = getResources().getInteger(17694721);
        setWillNotDraw(false);
        this.C = new DecelerateInterpolator(2.0f);
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        this.K = (int) (40.0f * displayMetrics.density);
        c();
        setChildrenDrawingOrderEnabled(true);
        this.i = (int) (displayMetrics.density * 64.0f);
        this.p = (float) this.i;
        this.r = new o(this);
        this.s = new l(this);
        setNestedScrollingEnabled(true);
        int i2 = -this.K;
        this.c = i2;
        this.h = i2;
        a(1.0f);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, D);
        setEnabled(obtainStyledAttributes.getBoolean(0, true));
        obtainStyledAttributes.recycle();
    }

    private Animation a(final int i2, final int i3) {
        AnonymousClass4 r0 = new Animation() {
            public void applyTransformation(float f, Transformation transformation) {
                SwipeRefreshLayout.this.j.setAlpha((int) (((float) i2) + (((float) (i3 - i2)) * f)));
            }
        };
        r0.setDuration(300);
        this.e.a(null);
        this.e.clearAnimation();
        this.e.startAnimation(r0);
        return r0;
    }

    private void a(int i2, AnimationListener animationListener) {
        this.f = i2;
        this.N.reset();
        this.N.setDuration(200);
        this.N.setInterpolator(this.C);
        if (animationListener != null) {
            this.e.a(animationListener);
        }
        this.e.clearAnimation();
        this.e.startAnimation(this.N);
    }

    private void a(MotionEvent motionEvent) {
        int actionIndex = motionEvent.getActionIndex();
        if (motionEvent.getPointerId(actionIndex) == this.A) {
            this.A = motionEvent.getPointerId(actionIndex == 0 ? 1 : 0);
        }
    }

    private void a(boolean z2, boolean z3) {
        if (this.f817b != z2) {
            this.k = z3;
            f();
            this.f817b = z2;
            if (this.f817b) {
                a(this.c, this.M);
            } else {
                a(this.M);
            }
        }
    }

    private boolean a(Animation animation) {
        return animation != null && animation.hasStarted() && !animation.hasEnded();
    }

    private void b(float f2) {
        this.j.a(true);
        float min = Math.min(1.0f, Math.abs(f2 / this.p));
        float max = (((float) Math.max(((double) min) - 0.4d, 0.0d)) * 5.0f) / 3.0f;
        float abs = Math.abs(f2) - this.p;
        float f3 = this.l ? (float) (this.i - this.h) : (float) this.i;
        float max2 = Math.max(0.0f, Math.min(abs, f3 * 2.0f) / f3);
        float pow = ((float) (((double) (max2 / 4.0f)) - Math.pow((double) (max2 / 4.0f), 2.0d))) * 2.0f;
        int i2 = ((int) ((f3 * min) + (f3 * pow * 2.0f))) + this.h;
        if (this.e.getVisibility() != 0) {
            this.e.setVisibility(0);
        }
        if (!this.d) {
            this.e.setScaleX(1.0f);
            this.e.setScaleY(1.0f);
        }
        if (this.d) {
            setAnimationProgress(Math.min(1.0f, f2 / this.p));
        }
        if (f2 < this.p) {
            if (this.j.getAlpha() > 76 && !a(this.H)) {
                d();
            }
        } else if (this.j.getAlpha() < 255 && !a(this.I)) {
            e();
        }
        this.j.a(0.0f, Math.min(0.8f, max * 0.8f));
        this.j.b(Math.min(1.0f, max));
        this.j.c((-0.25f + (max * 0.4f) + (pow * 2.0f)) * 0.5f);
        setTargetOffsetTopAndBottom(i2 - this.c);
    }

    private void b(int i2, AnimationListener animationListener) {
        if (this.d) {
            c(i2, animationListener);
            return;
        }
        this.f = i2;
        this.O.reset();
        this.O.setDuration(200);
        this.O.setInterpolator(this.C);
        if (animationListener != null) {
            this.e.a(animationListener);
        }
        this.e.clearAnimation();
        this.e.startAnimation(this.O);
    }

    private void b(AnimationListener animationListener) {
        this.e.setVisibility(0);
        if (VERSION.SDK_INT >= 11) {
            this.j.setAlpha(255);
        }
        this.F = new Animation() {
            public void applyTransformation(float f, Transformation transformation) {
                SwipeRefreshLayout.this.setAnimationProgress(f);
            }
        };
        this.F.setDuration((long) this.w);
        if (animationListener != null) {
            this.e.a(animationListener);
        }
        this.e.clearAnimation();
        this.e.startAnimation(this.F);
    }

    private void c() {
        this.e = new c(getContext(), -328966);
        this.j = new d(getContext());
        this.j.a(1);
        this.e.setImageDrawable(this.j);
        this.e.setVisibility(8);
        addView(this.e);
    }

    private void c(float f2) {
        if (f2 > this.p) {
            a(true, true);
            return;
        }
        this.f817b = false;
        this.j.a(0.0f, 0.0f);
        AnonymousClass5 r0 = null;
        if (!this.d) {
            r0 = new AnimationListener() {
                public void onAnimationEnd(Animation animation) {
                    if (!SwipeRefreshLayout.this.d) {
                        SwipeRefreshLayout.this.a((AnimationListener) null);
                    }
                }

                public void onAnimationRepeat(Animation animation) {
                }

                public void onAnimationStart(Animation animation) {
                }
            };
        }
        b(this.c, r0);
        this.j.a(false);
    }

    private void c(int i2, AnimationListener animationListener) {
        this.f = i2;
        this.g = this.e.getScaleX();
        this.J = new Animation() {
            public void applyTransformation(float f, Transformation transformation) {
                SwipeRefreshLayout.this.setAnimationProgress(SwipeRefreshLayout.this.g + ((-SwipeRefreshLayout.this.g) * f));
                SwipeRefreshLayout.this.a(f);
            }
        };
        this.J.setDuration(150);
        if (animationListener != null) {
            this.e.a(animationListener);
        }
        this.e.clearAnimation();
        this.e.startAnimation(this.J);
    }

    private void d() {
        this.H = a(this.j.getAlpha(), 76);
    }

    private void d(float f2) {
        if (f2 - this.y > ((float) this.o) && !this.z) {
            this.x = this.y + ((float) this.o);
            this.z = true;
            this.j.setAlpha(76);
        }
    }

    private void e() {
        this.I = a(this.j.getAlpha(), 255);
    }

    private void f() {
        if (this.n == null) {
            for (int i2 = 0; i2 < getChildCount(); i2++) {
                View childAt = getChildAt(i2);
                if (!childAt.equals(this.e)) {
                    this.n = childAt;
                    return;
                }
            }
        }
    }

    private void setColorViewAlpha(int i2) {
        this.e.getBackground().setAlpha(i2);
        this.j.setAlpha(i2);
    }

    /* access modifiers changed from: 0000 */
    public void a() {
        this.e.clearAnimation();
        this.j.stop();
        this.e.setVisibility(8);
        setColorViewAlpha(255);
        if (this.d) {
            setAnimationProgress(0.0f);
        } else {
            setTargetOffsetTopAndBottom(this.h - this.c);
        }
        this.c = this.e.getTop();
    }

    /* access modifiers changed from: 0000 */
    public void a(float f2) {
        setTargetOffsetTopAndBottom((this.f + ((int) (((float) (this.h - this.f)) * f2))) - this.e.getTop());
    }

    /* access modifiers changed from: 0000 */
    public void a(AnimationListener animationListener) {
        this.G = new Animation() {
            public void applyTransformation(float f, Transformation transformation) {
                SwipeRefreshLayout.this.setAnimationProgress(1.0f - f);
            }
        };
        this.G.setDuration(150);
        this.e.a(animationListener);
        this.e.clearAnimation();
        this.e.startAnimation(this.G);
    }

    public boolean b() {
        return this.L != null ? this.L.a(this, this.n) : this.n instanceof ListView ? k.b((ListView) this.n, -1) : this.n.canScrollVertically(-1);
    }

    public boolean dispatchNestedFling(float f2, float f3, boolean z2) {
        return this.s.a(f2, f3, z2);
    }

    public boolean dispatchNestedPreFling(float f2, float f3) {
        return this.s.a(f2, f3);
    }

    public boolean dispatchNestedPreScroll(int i2, int i3, int[] iArr, int[] iArr2) {
        return this.s.a(i2, i3, iArr, iArr2);
    }

    public boolean dispatchNestedScroll(int i2, int i3, int i4, int i5, int[] iArr) {
        return this.s.a(i2, i3, i4, i5, iArr);
    }

    /* access modifiers changed from: protected */
    public int getChildDrawingOrder(int i2, int i3) {
        return this.E < 0 ? i3 : i3 == i2 + -1 ? this.E : i3 >= this.E ? i3 + 1 : i3;
    }

    public int getNestedScrollAxes() {
        return this.r.a();
    }

    public int getProgressCircleDiameter() {
        return this.K;
    }

    public int getProgressViewEndOffset() {
        return this.i;
    }

    public int getProgressViewStartOffset() {
        return this.h;
    }

    public boolean hasNestedScrollingParent() {
        return this.s.b();
    }

    public boolean isNestedScrollingEnabled() {
        return this.s.a();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        a();
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        f();
        int actionMasked = motionEvent.getActionMasked();
        if (this.B && actionMasked == 0) {
            this.B = false;
        }
        if (!isEnabled() || this.B || b() || this.f817b || this.v) {
            return false;
        }
        switch (actionMasked) {
            case 0:
                setTargetOffsetTopAndBottom(this.h - this.e.getTop());
                this.A = motionEvent.getPointerId(0);
                this.z = false;
                int findPointerIndex = motionEvent.findPointerIndex(this.A);
                if (findPointerIndex >= 0) {
                    this.y = motionEvent.getY(findPointerIndex);
                    break;
                } else {
                    return false;
                }
            case 1:
            case 3:
                this.z = false;
                this.A = -1;
                break;
            case 2:
                if (this.A == -1) {
                    Log.e(m, "Got ACTION_MOVE event but don't have an active pointer id.");
                    return false;
                }
                int findPointerIndex2 = motionEvent.findPointerIndex(this.A);
                if (findPointerIndex2 >= 0) {
                    d(motionEvent.getY(findPointerIndex2));
                    break;
                } else {
                    return false;
                }
            case 6:
                a(motionEvent);
                break;
        }
        return this.z;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z2, int i2, int i3, int i4, int i5) {
        int measuredWidth = getMeasuredWidth();
        int measuredHeight = getMeasuredHeight();
        if (getChildCount() != 0) {
            if (this.n == null) {
                f();
            }
            if (this.n != null) {
                View view = this.n;
                int paddingLeft = getPaddingLeft();
                int paddingTop = getPaddingTop();
                view.layout(paddingLeft, paddingTop, ((measuredWidth - getPaddingLeft()) - getPaddingRight()) + paddingLeft, ((measuredHeight - getPaddingTop()) - getPaddingBottom()) + paddingTop);
                int measuredWidth2 = this.e.getMeasuredWidth();
                this.e.layout((measuredWidth / 2) - (measuredWidth2 / 2), this.c, (measuredWidth / 2) + (measuredWidth2 / 2), this.c + this.e.getMeasuredHeight());
            }
        }
    }

    public void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
        if (this.n == null) {
            f();
        }
        if (this.n != null) {
            this.n.measure(MeasureSpec.makeMeasureSpec((getMeasuredWidth() - getPaddingLeft()) - getPaddingRight(), 1073741824), MeasureSpec.makeMeasureSpec((getMeasuredHeight() - getPaddingTop()) - getPaddingBottom(), 1073741824));
            this.e.measure(MeasureSpec.makeMeasureSpec(this.K, 1073741824), MeasureSpec.makeMeasureSpec(this.K, 1073741824));
            this.E = -1;
            for (int i4 = 0; i4 < getChildCount(); i4++) {
                if (getChildAt(i4) == this.e) {
                    this.E = i4;
                    return;
                }
            }
        }
    }

    public boolean onNestedFling(View view, float f2, float f3, boolean z2) {
        return dispatchNestedFling(f2, f3, z2);
    }

    public boolean onNestedPreFling(View view, float f2, float f3) {
        return dispatchNestedPreFling(f2, f3);
    }

    public void onNestedPreScroll(View view, int i2, int i3, int[] iArr) {
        if (i3 > 0 && this.q > 0.0f) {
            if (((float) i3) > this.q) {
                iArr[1] = i3 - ((int) this.q);
                this.q = 0.0f;
            } else {
                this.q -= (float) i3;
                iArr[1] = i3;
            }
            b(this.q);
        }
        if (this.l && i3 > 0 && this.q == 0.0f && Math.abs(i3 - iArr[1]) > 0) {
            this.e.setVisibility(8);
        }
        int[] iArr2 = this.t;
        if (dispatchNestedPreScroll(i2 - iArr[0], i3 - iArr[1], iArr2, null)) {
            iArr[0] = iArr[0] + iArr2[0];
            iArr[1] = iArr2[1] + iArr[1];
        }
    }

    public void onNestedScroll(View view, int i2, int i3, int i4, int i5) {
        dispatchNestedScroll(i2, i3, i4, i5, this.u);
        int i6 = this.u[1] + i5;
        if (i6 < 0 && !b()) {
            this.q = ((float) Math.abs(i6)) + this.q;
            b(this.q);
        }
    }

    public void onNestedScrollAccepted(View view, View view2, int i2) {
        this.r.a(view, view2, i2);
        startNestedScroll(i2 & 2);
        this.q = 0.0f;
        this.v = true;
    }

    public boolean onStartNestedScroll(View view, View view2, int i2) {
        return isEnabled() && !this.B && !this.f817b && (i2 & 2) != 0;
    }

    public void onStopNestedScroll(View view) {
        this.r.a(view);
        this.v = false;
        if (this.q > 0.0f) {
            c(this.q);
            this.q = 0.0f;
        }
        stopNestedScroll();
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        int actionMasked = motionEvent.getActionMasked();
        if (this.B && actionMasked == 0) {
            this.B = false;
        }
        if (!isEnabled() || this.B || b() || this.f817b || this.v) {
            return false;
        }
        switch (actionMasked) {
            case 0:
                this.A = motionEvent.getPointerId(0);
                this.z = false;
                break;
            case 1:
                int findPointerIndex = motionEvent.findPointerIndex(this.A);
                if (findPointerIndex < 0) {
                    Log.e(m, "Got ACTION_UP event but don't have an active pointer id.");
                    return false;
                }
                if (this.z) {
                    float y2 = (motionEvent.getY(findPointerIndex) - this.x) * 0.5f;
                    this.z = false;
                    c(y2);
                }
                this.A = -1;
                return false;
            case 2:
                int findPointerIndex2 = motionEvent.findPointerIndex(this.A);
                if (findPointerIndex2 < 0) {
                    Log.e(m, "Got ACTION_MOVE event but have an invalid active pointer id.");
                    return false;
                }
                float y3 = motionEvent.getY(findPointerIndex2);
                d(y3);
                if (this.z) {
                    float f2 = (y3 - this.x) * 0.5f;
                    if (f2 > 0.0f) {
                        b(f2);
                        break;
                    } else {
                        return false;
                    }
                }
                break;
            case 3:
                return false;
            case 5:
                int actionIndex = motionEvent.getActionIndex();
                if (actionIndex >= 0) {
                    this.A = motionEvent.getPointerId(actionIndex);
                    break;
                } else {
                    Log.e(m, "Got ACTION_POINTER_DOWN event but have an invalid action index.");
                    return false;
                }
            case 6:
                a(motionEvent);
                break;
        }
        return true;
    }

    public void requestDisallowInterceptTouchEvent(boolean z2) {
        if (VERSION.SDK_INT < 21 && (this.n instanceof AbsListView)) {
            return;
        }
        if (this.n == null || t.t(this.n)) {
            super.requestDisallowInterceptTouchEvent(z2);
        }
    }

    /* access modifiers changed from: 0000 */
    public void setAnimationProgress(float f2) {
        this.e.setScaleX(f2);
        this.e.setScaleY(f2);
    }

    @Deprecated
    public void setColorScheme(int... iArr) {
        setColorSchemeResources(iArr);
    }

    public void setColorSchemeColors(int... iArr) {
        f();
        this.j.a(iArr);
    }

    public void setColorSchemeResources(int... iArr) {
        Context context = getContext();
        int[] iArr2 = new int[iArr.length];
        for (int i2 = 0; i2 < iArr.length; i2++) {
            iArr2[i2] = android.support.v4.b.a.c(context, iArr[i2]);
        }
        setColorSchemeColors(iArr2);
    }

    public void setDistanceToTriggerSync(int i2) {
        this.p = (float) i2;
    }

    public void setEnabled(boolean z2) {
        super.setEnabled(z2);
        if (!z2) {
            a();
        }
    }

    public void setNestedScrollingEnabled(boolean z2) {
        this.s.a(z2);
    }

    public void setOnChildScrollUpCallback(a aVar) {
        this.L = aVar;
    }

    public void setOnRefreshListener(b bVar) {
        this.f816a = bVar;
    }

    @Deprecated
    public void setProgressBackgroundColor(int i2) {
        setProgressBackgroundColorSchemeResource(i2);
    }

    public void setProgressBackgroundColorSchemeColor(int i2) {
        this.e.setBackgroundColor(i2);
    }

    public void setProgressBackgroundColorSchemeResource(int i2) {
        setProgressBackgroundColorSchemeColor(android.support.v4.b.a.c(getContext(), i2));
    }

    public void setRefreshing(boolean z2) {
        if (!z2 || this.f817b == z2) {
            a(z2, false);
            return;
        }
        this.f817b = z2;
        setTargetOffsetTopAndBottom((!this.l ? this.i + this.h : this.i) - this.c);
        this.k = false;
        b(this.M);
    }

    public void setSize(int i2) {
        if (i2 == 0 || i2 == 1) {
            DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
            if (i2 == 0) {
                this.K = (int) (displayMetrics.density * 56.0f);
            } else {
                this.K = (int) (displayMetrics.density * 40.0f);
            }
            this.e.setImageDrawable(null);
            this.j.a(i2);
            this.e.setImageDrawable(this.j);
        }
    }

    /* access modifiers changed from: 0000 */
    public void setTargetOffsetTopAndBottom(int i2) {
        this.e.bringToFront();
        t.c(this.e, i2);
        this.c = this.e.getTop();
    }

    public boolean startNestedScroll(int i2) {
        return this.s.b(i2);
    }

    public void stopNestedScroll() {
        this.s.c();
    }
}
