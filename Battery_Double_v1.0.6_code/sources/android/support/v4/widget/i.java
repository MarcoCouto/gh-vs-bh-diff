package android.support.v4.widget;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.widget.ImageView;

public class i {

    /* renamed from: a reason: collision with root package name */
    static final b f854a;

    static class a implements b {
        a() {
        }

        public ColorStateList a(ImageView imageView) {
            if (imageView instanceof p) {
                return ((p) imageView).getSupportImageTintList();
            }
            return null;
        }

        public void a(ImageView imageView, ColorStateList colorStateList) {
            if (imageView instanceof p) {
                ((p) imageView).setSupportImageTintList(colorStateList);
            }
        }

        public void a(ImageView imageView, Mode mode) {
            if (imageView instanceof p) {
                ((p) imageView).setSupportImageTintMode(mode);
            }
        }

        public Mode b(ImageView imageView) {
            if (imageView instanceof p) {
                return ((p) imageView).getSupportImageTintMode();
            }
            return null;
        }
    }

    interface b {
        ColorStateList a(ImageView imageView);

        void a(ImageView imageView, ColorStateList colorStateList);

        void a(ImageView imageView, Mode mode);

        Mode b(ImageView imageView);
    }

    static class c extends a {
        c() {
        }

        public ColorStateList a(ImageView imageView) {
            return imageView.getImageTintList();
        }

        public void a(ImageView imageView, ColorStateList colorStateList) {
            imageView.setImageTintList(colorStateList);
            if (VERSION.SDK_INT == 21) {
                Drawable drawable = imageView.getDrawable();
                boolean z = (imageView.getImageTintList() == null || imageView.getImageTintMode() == null) ? false : true;
                if (drawable != null && z) {
                    if (drawable.isStateful()) {
                        drawable.setState(imageView.getDrawableState());
                    }
                    imageView.setImageDrawable(drawable);
                }
            }
        }

        public void a(ImageView imageView, Mode mode) {
            imageView.setImageTintMode(mode);
            if (VERSION.SDK_INT == 21) {
                Drawable drawable = imageView.getDrawable();
                boolean z = (imageView.getImageTintList() == null || imageView.getImageTintMode() == null) ? false : true;
                if (drawable != null && z) {
                    if (drawable.isStateful()) {
                        drawable.setState(imageView.getDrawableState());
                    }
                    imageView.setImageDrawable(drawable);
                }
            }
        }

        public Mode b(ImageView imageView) {
            return imageView.getImageTintMode();
        }
    }

    static {
        if (VERSION.SDK_INT >= 21) {
            f854a = new c();
        } else {
            f854a = new a();
        }
    }

    public static ColorStateList a(ImageView imageView) {
        return f854a.a(imageView);
    }

    public static void a(ImageView imageView, ColorStateList colorStateList) {
        f854a.a(imageView, colorStateList);
    }

    public static void a(ImageView imageView, Mode mode) {
        f854a.a(imageView, mode);
    }

    public static Mode b(ImageView imageView) {
        return f854a.b(imageView);
    }
}
