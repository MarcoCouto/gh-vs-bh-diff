package android.support.v4.widget;

import android.content.res.Resources;
import android.os.SystemClock;
import android.support.v4.i.t;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewConfiguration;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;

public abstract class a implements OnTouchListener {
    private static final int r = ViewConfiguration.getTapTimeout();

    /* renamed from: a reason: collision with root package name */
    final C0025a f827a = new C0025a();

    /* renamed from: b reason: collision with root package name */
    final View f828b;
    boolean c;
    boolean d;
    boolean e;
    private final Interpolator f = new AccelerateInterpolator();
    private Runnable g;
    private float[] h = {0.0f, 0.0f};
    private float[] i = {Float.MAX_VALUE, Float.MAX_VALUE};
    private int j;
    private int k;
    private float[] l = {0.0f, 0.0f};
    private float[] m = {0.0f, 0.0f};
    private float[] n = {Float.MAX_VALUE, Float.MAX_VALUE};
    private boolean o;
    private boolean p;
    private boolean q;

    /* renamed from: android.support.v4.widget.a$a reason: collision with other inner class name */
    private static class C0025a {

        /* renamed from: a reason: collision with root package name */
        private int f829a;

        /* renamed from: b reason: collision with root package name */
        private int f830b;
        private float c;
        private float d;
        private long e = Long.MIN_VALUE;
        private long f = 0;
        private int g = 0;
        private int h = 0;
        private long i = -1;
        private float j;
        private int k;

        C0025a() {
        }

        private float a(float f2) {
            return (-4.0f * f2 * f2) + (4.0f * f2);
        }

        private float a(long j2) {
            if (j2 < this.e) {
                return 0.0f;
            }
            if (this.i < 0 || j2 < this.i) {
                return a.a(((float) (j2 - this.e)) / ((float) this.f829a), 0.0f, 1.0f) * 0.5f;
            }
            long j3 = j2 - this.i;
            return (a.a(((float) j3) / ((float) this.k), 0.0f, 1.0f) * this.j) + (1.0f - this.j);
        }

        public void a() {
            this.e = AnimationUtils.currentAnimationTimeMillis();
            this.i = -1;
            this.f = this.e;
            this.j = 0.5f;
            this.g = 0;
            this.h = 0;
        }

        public void a(float f2, float f3) {
            this.c = f2;
            this.d = f3;
        }

        public void a(int i2) {
            this.f829a = i2;
        }

        public void b() {
            long currentAnimationTimeMillis = AnimationUtils.currentAnimationTimeMillis();
            this.k = a.a((int) (currentAnimationTimeMillis - this.e), 0, this.f830b);
            this.j = a(currentAnimationTimeMillis);
            this.i = currentAnimationTimeMillis;
        }

        public void b(int i2) {
            this.f830b = i2;
        }

        public boolean c() {
            return this.i > 0 && AnimationUtils.currentAnimationTimeMillis() > this.i + ((long) this.k);
        }

        public void d() {
            if (this.f == 0) {
                throw new RuntimeException("Cannot compute scroll delta before calling start()");
            }
            long currentAnimationTimeMillis = AnimationUtils.currentAnimationTimeMillis();
            float a2 = a(a(currentAnimationTimeMillis));
            long j2 = currentAnimationTimeMillis - this.f;
            this.f = currentAnimationTimeMillis;
            this.g = (int) (((float) j2) * a2 * this.c);
            this.h = (int) (((float) j2) * a2 * this.d);
        }

        public int e() {
            return (int) (this.c / Math.abs(this.c));
        }

        public int f() {
            return (int) (this.d / Math.abs(this.d));
        }

        public int g() {
            return this.g;
        }

        public int h() {
            return this.h;
        }
    }

    private class b implements Runnable {
        b() {
        }

        public void run() {
            if (a.this.e) {
                if (a.this.c) {
                    a.this.c = false;
                    a.this.f827a.a();
                }
                C0025a aVar = a.this.f827a;
                if (aVar.c() || !a.this.a()) {
                    a.this.e = false;
                    return;
                }
                if (a.this.d) {
                    a.this.d = false;
                    a.this.b();
                }
                aVar.d();
                a.this.a(aVar.g(), aVar.h());
                t.a(a.this.f828b, (Runnable) this);
            }
        }
    }

    public a(View view) {
        this.f828b = view;
        DisplayMetrics displayMetrics = Resources.getSystem().getDisplayMetrics();
        int i2 = (int) ((1575.0f * displayMetrics.density) + 0.5f);
        int i3 = (int) ((displayMetrics.density * 315.0f) + 0.5f);
        a((float) i2, (float) i2);
        b((float) i3, (float) i3);
        a(1);
        e(Float.MAX_VALUE, Float.MAX_VALUE);
        d(0.2f, 0.2f);
        c(1.0f, 1.0f);
        b(r);
        c(500);
        d(500);
    }

    static float a(float f2, float f3, float f4) {
        return f2 > f4 ? f4 : f2 < f3 ? f3 : f2;
    }

    private float a(float f2, float f3, float f4, float f5) {
        float interpolation;
        float a2 = a(f2 * f3, 0.0f, f4);
        float f6 = f(f3 - f5, a2) - f(f5, a2);
        if (f6 < 0.0f) {
            interpolation = -this.f.getInterpolation(-f6);
        } else if (f6 <= 0.0f) {
            return 0.0f;
        } else {
            interpolation = this.f.getInterpolation(f6);
        }
        return a(interpolation, -1.0f, 1.0f);
    }

    private float a(int i2, float f2, float f3, float f4) {
        float a2 = a(this.h[i2], f3, this.i[i2], f2);
        if (a2 == 0.0f) {
            return 0.0f;
        }
        float f5 = this.l[i2];
        float f6 = this.m[i2];
        float f7 = this.n[i2];
        float f8 = f5 * f4;
        return a2 > 0.0f ? a(a2 * f8, f6, f7) : -a((-a2) * f8, f6, f7);
    }

    static int a(int i2, int i3, int i4) {
        return i2 > i4 ? i4 : i2 < i3 ? i3 : i2;
    }

    private void c() {
        if (this.g == null) {
            this.g = new b();
        }
        this.e = true;
        this.c = true;
        if (this.o || this.k <= 0) {
            this.g.run();
        } else {
            t.a(this.f828b, this.g, (long) this.k);
        }
        this.o = true;
    }

    private void d() {
        if (this.c) {
            this.e = false;
        } else {
            this.f827a.b();
        }
    }

    private float f(float f2, float f3) {
        if (f3 == 0.0f) {
            return 0.0f;
        }
        switch (this.j) {
            case 0:
            case 1:
                if (f2 < f3) {
                    return f2 >= 0.0f ? 1.0f - (f2 / f3) : (!this.e || this.j != 1) ? 0.0f : 1.0f;
                }
                return 0.0f;
            case 2:
                if (f2 < 0.0f) {
                    return f2 / (-f3);
                }
                return 0.0f;
            default:
                return 0.0f;
        }
    }

    public a a(float f2, float f3) {
        this.n[0] = f2 / 1000.0f;
        this.n[1] = f3 / 1000.0f;
        return this;
    }

    public a a(int i2) {
        this.j = i2;
        return this;
    }

    public a a(boolean z) {
        if (this.p && !z) {
            d();
        }
        this.p = z;
        return this;
    }

    public abstract void a(int i2, int i3);

    /* access modifiers changed from: 0000 */
    public boolean a() {
        C0025a aVar = this.f827a;
        int f2 = aVar.f();
        int e2 = aVar.e();
        return (f2 != 0 && f(f2)) || (e2 != 0 && e(e2));
    }

    public a b(float f2, float f3) {
        this.m[0] = f2 / 1000.0f;
        this.m[1] = f3 / 1000.0f;
        return this;
    }

    public a b(int i2) {
        this.k = i2;
        return this;
    }

    /* access modifiers changed from: 0000 */
    public void b() {
        long uptimeMillis = SystemClock.uptimeMillis();
        MotionEvent obtain = MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, 0.0f, 0.0f, 0);
        this.f828b.onTouchEvent(obtain);
        obtain.recycle();
    }

    public a c(float f2, float f3) {
        this.l[0] = f2 / 1000.0f;
        this.l[1] = f3 / 1000.0f;
        return this;
    }

    public a c(int i2) {
        this.f827a.a(i2);
        return this;
    }

    public a d(float f2, float f3) {
        this.h[0] = f2;
        this.h[1] = f3;
        return this;
    }

    public a d(int i2) {
        this.f827a.b(i2);
        return this;
    }

    public a e(float f2, float f3) {
        this.i[0] = f2;
        this.i[1] = f3;
        return this;
    }

    public abstract boolean e(int i2);

    public abstract boolean f(int i2);

    public boolean onTouch(View view, MotionEvent motionEvent) {
        boolean z = true;
        if (!this.p) {
            return false;
        }
        switch (motionEvent.getActionMasked()) {
            case 0:
                this.d = true;
                this.o = false;
                break;
            case 1:
            case 3:
                d();
                break;
            case 2:
                break;
        }
        this.f827a.a(a(0, motionEvent.getX(), (float) view.getWidth(), (float) this.f828b.getWidth()), a(1, motionEvent.getY(), (float) view.getHeight(), (float) this.f828b.getHeight()));
        if (!this.e && a()) {
            c();
        }
        if (!this.q || !this.e) {
            z = false;
        }
        return z;
    }
}
