package android.support.v4.widget;

import android.os.Build.VERSION;
import android.support.v4.i.e;
import android.support.v4.i.t;
import android.util.Log;
import android.view.View;
import android.widget.PopupWindow;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public final class l {

    /* renamed from: a reason: collision with root package name */
    static final d f855a;

    static class a extends d {
        a() {
        }

        public void a(PopupWindow popupWindow, View view, int i, int i2, int i3) {
            popupWindow.showAsDropDown(view, i, i2, i3);
        }
    }

    static class b extends a {

        /* renamed from: a reason: collision with root package name */
        private static Field f856a;

        static {
            try {
                f856a = PopupWindow.class.getDeclaredField("mOverlapAnchor");
                f856a.setAccessible(true);
            } catch (NoSuchFieldException e) {
                Log.i("PopupWindowCompatApi21", "Could not fetch mOverlapAnchor field from PopupWindow", e);
            }
        }

        b() {
        }

        public void a(PopupWindow popupWindow, boolean z) {
            if (f856a != null) {
                try {
                    f856a.set(popupWindow, Boolean.valueOf(z));
                } catch (IllegalAccessException e) {
                    Log.i("PopupWindowCompatApi21", "Could not set overlap anchor field in PopupWindow", e);
                }
            }
        }
    }

    static class c extends b {
        c() {
        }

        public void a(PopupWindow popupWindow, int i) {
            popupWindow.setWindowLayoutType(i);
        }

        public void a(PopupWindow popupWindow, boolean z) {
            popupWindow.setOverlapAnchor(z);
        }
    }

    static class d {

        /* renamed from: a reason: collision with root package name */
        private static Method f857a;

        /* renamed from: b reason: collision with root package name */
        private static boolean f858b;

        d() {
        }

        public void a(PopupWindow popupWindow, int i) {
            if (!f858b) {
                try {
                    f857a = PopupWindow.class.getDeclaredMethod("setWindowLayoutType", new Class[]{Integer.TYPE});
                    f857a.setAccessible(true);
                } catch (Exception e) {
                }
                f858b = true;
            }
            if (f857a != null) {
                try {
                    f857a.invoke(popupWindow, new Object[]{Integer.valueOf(i)});
                } catch (Exception e2) {
                }
            }
        }

        public void a(PopupWindow popupWindow, View view, int i, int i2, int i3) {
            if ((e.a(i3, t.e(view)) & 7) == 5) {
                i -= popupWindow.getWidth() - view.getWidth();
            }
            popupWindow.showAsDropDown(view, i, i2);
        }

        public void a(PopupWindow popupWindow, boolean z) {
        }
    }

    static {
        if (VERSION.SDK_INT >= 23) {
            f855a = new c();
        } else if (VERSION.SDK_INT >= 21) {
            f855a = new b();
        } else if (VERSION.SDK_INT >= 19) {
            f855a = new a();
        } else {
            f855a = new d();
        }
    }

    public static void a(PopupWindow popupWindow, int i) {
        f855a.a(popupWindow, i);
    }

    public static void a(PopupWindow popupWindow, View view, int i, int i2, int i3) {
        f855a.a(popupWindow, view, i, i2, i3);
    }

    public static void a(PopupWindow popupWindow, boolean z) {
        f855a.a(popupWindow, z);
    }
}
