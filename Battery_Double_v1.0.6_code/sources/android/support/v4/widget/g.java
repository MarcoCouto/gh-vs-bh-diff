package android.support.v4.widget;

import android.database.Cursor;
import android.widget.Filter;
import android.widget.Filter.FilterResults;

class g extends Filter {

    /* renamed from: a reason: collision with root package name */
    a f852a;

    interface a {
        Cursor a();

        Cursor a(CharSequence charSequence);

        void a(Cursor cursor);

        CharSequence c(Cursor cursor);
    }

    g(a aVar) {
        this.f852a = aVar;
    }

    public CharSequence convertResultToString(Object obj) {
        return this.f852a.c((Cursor) obj);
    }

    /* access modifiers changed from: protected */
    public FilterResults performFiltering(CharSequence charSequence) {
        Cursor a2 = this.f852a.a(charSequence);
        FilterResults filterResults = new FilterResults();
        if (a2 != null) {
            filterResults.count = a2.getCount();
            filterResults.values = a2;
        } else {
            filterResults.count = 0;
            filterResults.values = null;
        }
        return filterResults;
    }

    /* access modifiers changed from: protected */
    public void publishResults(CharSequence charSequence, FilterResults filterResults) {
        Cursor a2 = this.f852a.a();
        if (filterResults.values != null && filterResults.values != a2) {
            this.f852a.a((Cursor) filterResults.values);
        }
    }
}
