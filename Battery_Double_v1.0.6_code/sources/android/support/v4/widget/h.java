package android.support.v4.widget;

import android.os.Build.VERSION;
import android.widget.EdgeEffect;

public final class h {

    /* renamed from: a reason: collision with root package name */
    private static final b f853a;

    static class a extends b {
        a() {
        }

        public void a(EdgeEffect edgeEffect, float f, float f2) {
            edgeEffect.onPull(f, f2);
        }
    }

    static class b {
        b() {
        }

        public void a(EdgeEffect edgeEffect, float f, float f2) {
            edgeEffect.onPull(f);
        }
    }

    static {
        if (VERSION.SDK_INT >= 21) {
            f853a = new a();
        } else {
            f853a = new b();
        }
    }

    public static void a(EdgeEffect edgeEffect, float f, float f2) {
        f853a.a(edgeEffect, f, f2);
    }
}
