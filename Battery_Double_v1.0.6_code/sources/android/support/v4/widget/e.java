package android.support.v4.widget;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.util.Log;
import android.widget.CompoundButton;
import java.lang.reflect.Field;

public final class e {

    /* renamed from: a reason: collision with root package name */
    private static final c f845a;

    static class a extends c {
        a() {
        }

        public void a(CompoundButton compoundButton, ColorStateList colorStateList) {
            compoundButton.setButtonTintList(colorStateList);
        }

        public void a(CompoundButton compoundButton, Mode mode) {
            compoundButton.setButtonTintMode(mode);
        }
    }

    static class b extends a {
        b() {
        }

        public Drawable a(CompoundButton compoundButton) {
            return compoundButton.getButtonDrawable();
        }
    }

    static class c {

        /* renamed from: a reason: collision with root package name */
        private static Field f846a;

        /* renamed from: b reason: collision with root package name */
        private static boolean f847b;

        c() {
        }

        public Drawable a(CompoundButton compoundButton) {
            if (!f847b) {
                try {
                    f846a = CompoundButton.class.getDeclaredField("mButtonDrawable");
                    f846a.setAccessible(true);
                } catch (NoSuchFieldException e) {
                    Log.i("CompoundButtonCompat", "Failed to retrieve mButtonDrawable field", e);
                }
                f847b = true;
            }
            if (f846a != null) {
                try {
                    return (Drawable) f846a.get(compoundButton);
                } catch (IllegalAccessException e2) {
                    Log.i("CompoundButtonCompat", "Failed to get button drawable via reflection", e2);
                    f846a = null;
                }
            }
            return null;
        }

        public void a(CompoundButton compoundButton, ColorStateList colorStateList) {
            if (compoundButton instanceof o) {
                ((o) compoundButton).setSupportButtonTintList(colorStateList);
            }
        }

        public void a(CompoundButton compoundButton, Mode mode) {
            if (compoundButton instanceof o) {
                ((o) compoundButton).setSupportButtonTintMode(mode);
            }
        }
    }

    static {
        if (VERSION.SDK_INT >= 23) {
            f845a = new b();
        } else if (VERSION.SDK_INT >= 21) {
            f845a = new a();
        } else {
            f845a = new c();
        }
    }

    public static Drawable a(CompoundButton compoundButton) {
        return f845a.a(compoundButton);
    }

    public static void a(CompoundButton compoundButton, ColorStateList colorStateList) {
        f845a.a(compoundButton, colorStateList);
    }

    public static void a(CompoundButton compoundButton, Mode mode) {
        f845a.a(compoundButton, mode);
    }
}
