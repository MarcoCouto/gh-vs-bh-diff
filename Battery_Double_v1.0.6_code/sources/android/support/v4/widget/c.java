package android.support.v4.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RadialGradient;
import android.graphics.Shader.TileMode;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.os.Build.VERSION;
import android.support.v4.i.t;
import android.view.View;
import android.view.animation.Animation.AnimationListener;
import android.widget.ImageView;

class c extends ImageView {

    /* renamed from: a reason: collision with root package name */
    int f833a;

    /* renamed from: b reason: collision with root package name */
    private AnimationListener f834b;

    private class a extends OvalShape {

        /* renamed from: b reason: collision with root package name */
        private RadialGradient f836b;
        private Paint c = new Paint();

        a(int i) {
            c.this.f833a = i;
            a((int) rect().width());
        }

        private void a(int i) {
            this.f836b = new RadialGradient((float) (i / 2), (float) (i / 2), (float) c.this.f833a, new int[]{1023410176, 0}, null, TileMode.CLAMP);
            this.c.setShader(this.f836b);
        }

        public void draw(Canvas canvas, Paint paint) {
            int width = c.this.getWidth();
            int height = c.this.getHeight();
            canvas.drawCircle((float) (width / 2), (float) (height / 2), (float) (width / 2), this.c);
            canvas.drawCircle((float) (width / 2), (float) (height / 2), (float) ((width / 2) - c.this.f833a), paint);
        }

        /* access modifiers changed from: protected */
        public void onResize(float f, float f2) {
            super.onResize(f, f2);
            a((int) f);
        }
    }

    c(Context context, int i) {
        ShapeDrawable shapeDrawable;
        super(context);
        float f = getContext().getResources().getDisplayMetrics().density;
        int i2 = (int) (1.75f * f);
        int i3 = (int) (0.0f * f);
        this.f833a = (int) (3.5f * f);
        if (a()) {
            shapeDrawable = new ShapeDrawable(new OvalShape());
            t.a((View) this, f * 4.0f);
        } else {
            shapeDrawable = new ShapeDrawable(new a(this.f833a));
            setLayerType(1, shapeDrawable.getPaint());
            shapeDrawable.getPaint().setShadowLayer((float) this.f833a, (float) i3, (float) i2, 503316480);
            int i4 = this.f833a;
            setPadding(i4, i4, i4, i4);
        }
        shapeDrawable.getPaint().setColor(i);
        t.a((View) this, (Drawable) shapeDrawable);
    }

    private boolean a() {
        return VERSION.SDK_INT >= 21;
    }

    public void a(AnimationListener animationListener) {
        this.f834b = animationListener;
    }

    public void onAnimationEnd() {
        super.onAnimationEnd();
        if (this.f834b != null) {
            this.f834b.onAnimationEnd(getAnimation());
        }
    }

    public void onAnimationStart() {
        super.onAnimationStart();
        if (this.f834b != null) {
            this.f834b.onAnimationStart(getAnimation());
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        if (!a()) {
            setMeasuredDimension(getMeasuredWidth() + (this.f833a * 2), getMeasuredHeight() + (this.f833a * 2));
        }
    }

    public void setBackgroundColor(int i) {
        if (getBackground() instanceof ShapeDrawable) {
            ((ShapeDrawable) getBackground()).getPaint().setColor(i);
        }
    }
}
