package android.support.v4.c;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.CancellationSignal;
import android.support.v4.b.a.a.b;
import android.support.v4.b.a.a.c;
import android.support.v4.g.b.C0016b;
import android.support.v4.h.m;
import android.util.Log;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.ByteBuffer;
import java.util.List;

class e extends g {

    /* renamed from: a reason: collision with root package name */
    private static final Class f646a;

    /* renamed from: b reason: collision with root package name */
    private static final Constructor f647b;
    private static final Method c;
    private static final Method d;

    static {
        Method method;
        Constructor constructor;
        Class cls;
        Method method2 = null;
        try {
            Class cls2 = Class.forName("android.graphics.FontFamily");
            Constructor constructor2 = cls2.getConstructor(new Class[0]);
            Method method3 = cls2.getMethod("addFontWeightStyle", new Class[]{ByteBuffer.class, Integer.TYPE, List.class, Integer.TYPE, Boolean.TYPE});
            method = Typeface.class.getMethod("createFromFamiliesWithDefault", new Class[]{Array.newInstance(cls2, 1).getClass()});
            method2 = method3;
            constructor = constructor2;
            cls = cls2;
        } catch (ClassNotFoundException | NoSuchMethodException e) {
            Log.e("TypefaceCompatApi24Impl", e.getClass().getName(), e);
            method = null;
            constructor = null;
            cls = null;
        }
        f647b = constructor;
        f646a = cls;
        c = method2;
        d = method;
    }

    e() {
    }

    private static Typeface a(Object obj) {
        try {
            Object newInstance = Array.newInstance(f646a, 1);
            Array.set(newInstance, 0, obj);
            return (Typeface) d.invoke(null, new Object[]{newInstance});
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }

    public static boolean a() {
        if (c == null) {
            Log.w("TypefaceCompatApi24Impl", "Unable to collect necessary private methods.Fallback to legacy implementation.");
        }
        return c != null;
    }

    private static boolean a(Object obj, ByteBuffer byteBuffer, int i, int i2, boolean z) {
        try {
            return ((Boolean) c.invoke(obj, new Object[]{byteBuffer, Integer.valueOf(i), null, Integer.valueOf(i2), Boolean.valueOf(z)})).booleanValue();
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }

    private static Object b() {
        try {
            return f647b.newInstance(new Object[0]);
        } catch (IllegalAccessException | InstantiationException | InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }

    public Typeface a(Context context, CancellationSignal cancellationSignal, C0016b[] bVarArr, int i) {
        Object b2 = b();
        m mVar = new m();
        for (C0016b bVar : bVarArr) {
            Uri a2 = bVar.a();
            ByteBuffer byteBuffer = (ByteBuffer) mVar.get(a2);
            if (byteBuffer == null) {
                byteBuffer = h.a(context, cancellationSignal, a2);
                mVar.put(a2, byteBuffer);
            }
            if (!a(b2, byteBuffer, bVar.b(), bVar.c(), bVar.d())) {
                return null;
            }
        }
        return Typeface.create(a(b2), i);
    }

    public Typeface a(Context context, b bVar, Resources resources, int i) {
        c[] a2;
        Object b2 = b();
        for (c cVar : bVar.a()) {
            ByteBuffer a3 = h.a(context, resources, cVar.d());
            if (a3 == null || !a(b2, a3, 0, cVar.b(), cVar.c())) {
                return null;
            }
        }
        return a(b2);
    }
}
