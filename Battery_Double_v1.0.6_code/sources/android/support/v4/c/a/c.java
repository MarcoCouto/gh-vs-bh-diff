package android.support.v4.c.a;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff.Mode;
import android.graphics.Rect;
import android.graphics.Region;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Drawable.Callback;
import android.graphics.drawable.Drawable.ConstantState;

class c extends Drawable implements Callback, b, f {

    /* renamed from: a reason: collision with root package name */
    static final Mode f636a = Mode.SRC_IN;

    /* renamed from: b reason: collision with root package name */
    a f637b;
    Drawable c;
    private int d;
    private Mode e;
    private boolean f;
    private boolean g;

    protected static abstract class a extends ConstantState {

        /* renamed from: a reason: collision with root package name */
        int f638a;

        /* renamed from: b reason: collision with root package name */
        ConstantState f639b;
        ColorStateList c = null;
        Mode d = c.f636a;

        a(a aVar, Resources resources) {
            if (aVar != null) {
                this.f638a = aVar.f638a;
                this.f639b = aVar.f639b;
                this.c = aVar.c;
                this.d = aVar.d;
            }
        }

        /* access modifiers changed from: 0000 */
        public boolean a() {
            return this.f639b != null;
        }

        public int getChangingConfigurations() {
            return (this.f639b != null ? this.f639b.getChangingConfigurations() : 0) | this.f638a;
        }

        public Drawable newDrawable() {
            return newDrawable(null);
        }

        public abstract Drawable newDrawable(Resources resources);
    }

    private static class b extends a {
        b(a aVar, Resources resources) {
            super(aVar, resources);
        }

        public Drawable newDrawable(Resources resources) {
            return new c(this, resources);
        }
    }

    c(Drawable drawable) {
        this.f637b = b();
        a(drawable);
    }

    c(a aVar, Resources resources) {
        this.f637b = aVar;
        a(resources);
    }

    private void a(Resources resources) {
        if (this.f637b != null && this.f637b.f639b != null) {
            a(a(this.f637b.f639b, resources));
        }
    }

    private boolean a(int[] iArr) {
        if (!c()) {
            return false;
        }
        ColorStateList colorStateList = this.f637b.c;
        Mode mode = this.f637b.d;
        if (colorStateList == null || mode == null) {
            this.f = false;
            clearColorFilter();
            return false;
        }
        int colorForState = colorStateList.getColorForState(iArr, colorStateList.getDefaultColor());
        if (this.f && colorForState == this.d && mode == this.e) {
            return false;
        }
        setColorFilter(colorForState, mode);
        this.d = colorForState;
        this.e = mode;
        this.f = true;
        return true;
    }

    public final Drawable a() {
        return this.c;
    }

    /* access modifiers changed from: protected */
    public Drawable a(ConstantState constantState, Resources resources) {
        return constantState.newDrawable(resources);
    }

    public final void a(Drawable drawable) {
        if (this.c != null) {
            this.c.setCallback(null);
        }
        this.c = drawable;
        if (drawable != null) {
            drawable.setCallback(this);
            setVisible(drawable.isVisible(), true);
            setState(drawable.getState());
            setLevel(drawable.getLevel());
            setBounds(drawable.getBounds());
            if (this.f637b != null) {
                this.f637b.f639b = drawable.getConstantState();
            }
        }
        invalidateSelf();
    }

    /* access modifiers changed from: 0000 */
    public a b() {
        return new b(this.f637b, null);
    }

    /* access modifiers changed from: protected */
    public boolean c() {
        return true;
    }

    public void draw(Canvas canvas) {
        this.c.draw(canvas);
    }

    public int getChangingConfigurations() {
        return (this.f637b != null ? this.f637b.getChangingConfigurations() : 0) | super.getChangingConfigurations() | this.c.getChangingConfigurations();
    }

    public ConstantState getConstantState() {
        if (this.f637b == null || !this.f637b.a()) {
            return null;
        }
        this.f637b.f638a = getChangingConfigurations();
        return this.f637b;
    }

    public Drawable getCurrent() {
        return this.c.getCurrent();
    }

    public int getIntrinsicHeight() {
        return this.c.getIntrinsicHeight();
    }

    public int getIntrinsicWidth() {
        return this.c.getIntrinsicWidth();
    }

    public int getMinimumHeight() {
        return this.c.getMinimumHeight();
    }

    public int getMinimumWidth() {
        return this.c.getMinimumWidth();
    }

    public int getOpacity() {
        return this.c.getOpacity();
    }

    public boolean getPadding(Rect rect) {
        return this.c.getPadding(rect);
    }

    public int[] getState() {
        return this.c.getState();
    }

    public Region getTransparentRegion() {
        return this.c.getTransparentRegion();
    }

    public void invalidateDrawable(Drawable drawable) {
        invalidateSelf();
    }

    public boolean isStateful() {
        ColorStateList colorStateList = (!c() || this.f637b == null) ? null : this.f637b.c;
        return (colorStateList != null && colorStateList.isStateful()) || this.c.isStateful();
    }

    public void jumpToCurrentState() {
        this.c.jumpToCurrentState();
    }

    public Drawable mutate() {
        if (!this.g && super.mutate() == this) {
            this.f637b = b();
            if (this.c != null) {
                this.c.mutate();
            }
            if (this.f637b != null) {
                this.f637b.f639b = this.c != null ? this.c.getConstantState() : null;
            }
            this.g = true;
        }
        return this;
    }

    /* access modifiers changed from: protected */
    public void onBoundsChange(Rect rect) {
        if (this.c != null) {
            this.c.setBounds(rect);
        }
    }

    /* access modifiers changed from: protected */
    public boolean onLevelChange(int i) {
        return this.c.setLevel(i);
    }

    public void scheduleDrawable(Drawable drawable, Runnable runnable, long j) {
        scheduleSelf(runnable, j);
    }

    public void setAlpha(int i) {
        this.c.setAlpha(i);
    }

    public void setChangingConfigurations(int i) {
        this.c.setChangingConfigurations(i);
    }

    public void setColorFilter(ColorFilter colorFilter) {
        this.c.setColorFilter(colorFilter);
    }

    public void setDither(boolean z) {
        this.c.setDither(z);
    }

    public void setFilterBitmap(boolean z) {
        this.c.setFilterBitmap(z);
    }

    public boolean setState(int[] iArr) {
        return a(iArr) || this.c.setState(iArr);
    }

    public void setTint(int i) {
        setTintList(ColorStateList.valueOf(i));
    }

    public void setTintList(ColorStateList colorStateList) {
        this.f637b.c = colorStateList;
        a(getState());
    }

    public void setTintMode(Mode mode) {
        this.f637b.d = mode;
        a(getState());
    }

    public boolean setVisible(boolean z, boolean z2) {
        return super.setVisible(z, z2) || this.c.setVisible(z, z2);
    }

    public void unscheduleDrawable(Drawable drawable, Runnable runnable) {
        unscheduleSelf(runnable);
    }
}
