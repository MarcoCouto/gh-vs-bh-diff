package android.support.v4.c;

import android.content.Context;
import android.content.res.Resources;
import android.net.Uri;
import android.os.CancellationSignal;
import android.os.ParcelFileDescriptor;
import android.os.Process;
import android.util.Log;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileChannel.MapMode;

public class h {
    public static File a(Context context) {
        String str = ".font" + Process.myPid() + "-" + Process.myTid() + "-";
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= 100) {
                return null;
            }
            File file = new File(context.getCacheDir(), str + i2);
            try {
                if (file.createNewFile()) {
                    return file;
                }
                i = i2 + 1;
            } catch (IOException e) {
            }
        }
    }

    public static ByteBuffer a(Context context, Resources resources, int i) {
        ByteBuffer byteBuffer = null;
        File a2 = a(context);
        if (a2 != null) {
            try {
                if (a(a2, resources, i)) {
                    byteBuffer = a(a2);
                    a2.delete();
                }
            } finally {
                a2.delete();
            }
        }
        return byteBuffer;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0035, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
        r10.addSuppressed(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0042, code lost:
        if (r1 != null) goto L_0x0044;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:?, code lost:
        r7.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x004f, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0050, code lost:
        r1 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x0069, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x006a, code lost:
        r1.addSuppressed(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x0072, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:?, code lost:
        r1.addSuppressed(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x0077, code lost:
        r7.close();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0042  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x004f A[ExcHandler: all (th java.lang.Throwable), Splitter:B:4:0x000c] */
    public static ByteBuffer a(Context context, CancellationSignal cancellationSignal, Uri uri) {
        Throwable th;
        try {
            ParcelFileDescriptor openFileDescriptor = context.getContentResolver().openFileDescriptor(uri, "r", cancellationSignal);
            Throwable th2 = null;
            try {
                FileInputStream fileInputStream = new FileInputStream(openFileDescriptor.getFileDescriptor());
                Throwable th3 = null;
                try {
                    FileChannel channel = fileInputStream.getChannel();
                    MappedByteBuffer map = channel.map(MapMode.READ_ONLY, 0, channel.size());
                    if (fileInputStream != null) {
                        if (0 != 0) {
                            fileInputStream.close();
                        } else {
                            fileInputStream.close();
                        }
                    }
                    if (openFileDescriptor == null) {
                        return map;
                    }
                    if (0 != 0) {
                        try {
                            openFileDescriptor.close();
                            return map;
                        } catch (Throwable th4) {
                            th2.addSuppressed(th4);
                            return map;
                        }
                    } else {
                        openFileDescriptor.close();
                        return map;
                    }
                } catch (Throwable th5) {
                    Throwable th6 = th5;
                    th = r0;
                    th = th6;
                }
                if (openFileDescriptor != null) {
                }
                throw th;
                if (fileInputStream != null) {
                    if (th != null) {
                        fileInputStream.close();
                    } else {
                        fileInputStream.close();
                    }
                }
                throw th;
                throw th;
                throw th;
            } catch (Throwable th7) {
                Throwable th8 = th7;
                Throwable th9 = r0;
                th = th8;
            }
        } catch (IOException e) {
            return null;
        }
    }

    private static ByteBuffer a(File file) {
        Throwable th;
        try {
            FileInputStream fileInputStream = new FileInputStream(file);
            Throwable th2 = null;
            try {
                FileChannel channel = fileInputStream.getChannel();
                MappedByteBuffer map = channel.map(MapMode.READ_ONLY, 0, channel.size());
                if (fileInputStream == null) {
                    return map;
                }
                if (0 != 0) {
                    try {
                        fileInputStream.close();
                        return map;
                    } catch (Throwable th3) {
                        th2.addSuppressed(th3);
                        return map;
                    }
                } else {
                    fileInputStream.close();
                    return map;
                }
            } catch (Throwable th4) {
                Throwable th5 = th4;
                th = r0;
                th = th5;
            }
            if (fileInputStream != null) {
                if (th != null) {
                    try {
                        fileInputStream.close();
                    } catch (Throwable th6) {
                        th.addSuppressed(th6);
                    }
                } else {
                    fileInputStream.close();
                }
            }
            throw th;
            throw th;
        } catch (IOException e) {
            return null;
        }
    }

    public static void a(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {
            }
        }
    }

    public static boolean a(File file, Resources resources, int i) {
        InputStream inputStream = null;
        try {
            inputStream = resources.openRawResource(i);
            return a(file, inputStream);
        } finally {
            a((Closeable) inputStream);
        }
    }

    public static boolean a(File file, InputStream inputStream) {
        FileOutputStream fileOutputStream;
        try {
            fileOutputStream = new FileOutputStream(file, false);
            try {
                byte[] bArr = new byte[1024];
                while (true) {
                    int read = inputStream.read(bArr);
                    if (read != -1) {
                        fileOutputStream.write(bArr, 0, read);
                    } else {
                        a((Closeable) fileOutputStream);
                        return true;
                    }
                }
            } catch (IOException e) {
                e = e;
                try {
                    Log.e("TypefaceCompatUtil", "Error copying resource contents to temp file: " + e.getMessage());
                    a((Closeable) fileOutputStream);
                    return false;
                } catch (Throwable th) {
                    th = th;
                    a((Closeable) fileOutputStream);
                    throw th;
                }
            }
        } catch (IOException e2) {
            e = e2;
            fileOutputStream = null;
            Log.e("TypefaceCompatUtil", "Error copying resource contents to temp file: " + e.getMessage());
            a((Closeable) fileOutputStream);
            return false;
        } catch (Throwable th2) {
            th = th2;
            fileOutputStream = null;
            a((Closeable) fileOutputStream);
            throw th;
        }
    }
}
