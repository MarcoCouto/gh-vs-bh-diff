package android.support.v4.c;

import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.graphics.Typeface.Builder;
import android.graphics.fonts.FontVariationAxis;
import android.os.CancellationSignal;
import android.os.ParcelFileDescriptor;
import android.support.v4.b.a.a;
import android.support.v4.b.a.a.c;
import android.support.v4.g.b;
import android.support.v4.g.b.C0016b;
import android.util.Log;
import java.io.IOException;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.ByteBuffer;
import java.util.Map;

public class f extends d {

    /* renamed from: a reason: collision with root package name */
    private static final Class f648a;

    /* renamed from: b reason: collision with root package name */
    private static final Constructor f649b;
    private static final Method c;
    private static final Method d;
    private static final Method e;
    private static final Method f;
    private static final Method g;

    static {
        Method method;
        Method method2;
        Method method3;
        Method method4;
        Constructor constructor;
        Class cls;
        Method method5 = null;
        try {
            Class cls2 = Class.forName("android.graphics.FontFamily");
            Constructor constructor2 = cls2.getConstructor(new Class[0]);
            Method method6 = cls2.getMethod("addFontFromAssetManager", new Class[]{AssetManager.class, String.class, Integer.TYPE, Boolean.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, FontVariationAxis[].class});
            Method method7 = cls2.getMethod("addFontFromBuffer", new Class[]{ByteBuffer.class, Integer.TYPE, FontVariationAxis[].class, Integer.TYPE, Integer.TYPE});
            Method method8 = cls2.getMethod("freeze", new Class[0]);
            Method method9 = cls2.getMethod("abortCreation", new Class[0]);
            method = Typeface.class.getDeclaredMethod("createFromFamiliesWithDefault", new Class[]{Array.newInstance(cls2, 1).getClass(), Integer.TYPE, Integer.TYPE});
            method.setAccessible(true);
            method5 = method9;
            method2 = method8;
            method3 = method7;
            method4 = method6;
            constructor = constructor2;
            cls = cls2;
        } catch (ClassNotFoundException | NoSuchMethodException e2) {
            Log.e("TypefaceCompatApi26Impl", "Unable to collect necessary methods for class " + e2.getClass().getName(), e2);
            method = null;
            method2 = null;
            method3 = null;
            method4 = null;
            constructor = null;
            cls = null;
        }
        f649b = constructor;
        f648a = cls;
        c = method4;
        d = method3;
        e = method2;
        f = method5;
        g = method;
    }

    private static Typeface a(Object obj) {
        try {
            Object newInstance = Array.newInstance(f648a, 1);
            Array.set(newInstance, 0, obj);
            return (Typeface) g.invoke(null, new Object[]{newInstance, Integer.valueOf(-1), Integer.valueOf(-1)});
        } catch (IllegalAccessException | InvocationTargetException e2) {
            throw new RuntimeException(e2);
        }
    }

    private static boolean a() {
        if (c == null) {
            Log.w("TypefaceCompatApi26Impl", "Unable to collect necessary private methods. Fallback to legacy implementation.");
        }
        return c != null;
    }

    private static boolean a(Context context, Object obj, String str, int i, int i2, int i3) {
        try {
            return ((Boolean) c.invoke(obj, new Object[]{context.getAssets(), str, Integer.valueOf(0), Boolean.valueOf(false), Integer.valueOf(i), Integer.valueOf(i2), Integer.valueOf(i3), null})).booleanValue();
        } catch (IllegalAccessException | InvocationTargetException e2) {
            throw new RuntimeException(e2);
        }
    }

    private static boolean a(Object obj, ByteBuffer byteBuffer, int i, int i2, int i3) {
        try {
            return ((Boolean) d.invoke(obj, new Object[]{byteBuffer, Integer.valueOf(i), null, Integer.valueOf(i2), Integer.valueOf(i3)})).booleanValue();
        } catch (IllegalAccessException | InvocationTargetException e2) {
            throw new RuntimeException(e2);
        }
    }

    private static Object b() {
        try {
            return f649b.newInstance(new Object[0]);
        } catch (IllegalAccessException | InstantiationException | InvocationTargetException e2) {
            throw new RuntimeException(e2);
        }
    }

    private static boolean b(Object obj) {
        try {
            return ((Boolean) e.invoke(obj, new Object[0])).booleanValue();
        } catch (IllegalAccessException | InvocationTargetException e2) {
            throw new RuntimeException(e2);
        }
    }

    private static boolean c(Object obj) {
        try {
            return ((Boolean) f.invoke(obj, new Object[0])).booleanValue();
        } catch (IllegalAccessException | InvocationTargetException e2) {
            throw new RuntimeException(e2);
        }
    }

    public Typeface a(Context context, Resources resources, int i, String str, int i2) {
        if (!a()) {
            return super.a(context, resources, i, str, i2);
        }
        Object b2 = b();
        if (!a(context, b2, str, 0, -1, -1)) {
            c(b2);
            return null;
        } else if (!b(b2)) {
            return null;
        } else {
            return a(b2);
        }
    }

    public Typeface a(Context context, CancellationSignal cancellationSignal, C0016b[] bVarArr, int i) {
        boolean z;
        ParcelFileDescriptor openFileDescriptor;
        Throwable th;
        if (bVarArr.length < 1) {
            return null;
        }
        if (!a()) {
            C0016b a2 = a(bVarArr, i);
            try {
                openFileDescriptor = context.getContentResolver().openFileDescriptor(a2.a(), "r", cancellationSignal);
                th = null;
                try {
                    Typeface build = new Builder(openFileDescriptor.getFileDescriptor()).setWeight(a2.c()).setItalic(a2.d()).build();
                    if (openFileDescriptor == null) {
                        return build;
                    }
                    if (th != null) {
                        try {
                            openFileDescriptor.close();
                            return build;
                        } catch (Throwable th2) {
                            th.addSuppressed(th2);
                            return build;
                        }
                    } else {
                        openFileDescriptor.close();
                        return build;
                    }
                } catch (Throwable th3) {
                    Throwable th4 = th3;
                    th = r0;
                    th = th4;
                }
            } catch (IOException e2) {
                return null;
            }
        } else {
            Map a3 = b.a(context, bVarArr, cancellationSignal);
            Object b2 = b();
            boolean z2 = false;
            int length = bVarArr.length;
            int i2 = 0;
            while (i2 < length) {
                C0016b bVar = bVarArr[i2];
                ByteBuffer byteBuffer = (ByteBuffer) a3.get(bVar.a());
                if (byteBuffer == null) {
                    z = z2;
                } else {
                    if (!a(b2, byteBuffer, bVar.b(), bVar.c(), bVar.d() ? 1 : 0)) {
                        c(b2);
                        return null;
                    }
                    z = true;
                }
                i2++;
                z2 = z;
            }
            if (!z2) {
                c(b2);
                return null;
            } else if (!b(b2)) {
                return null;
            } else {
                return Typeface.create(a(b2), i);
            }
        }
        if (openFileDescriptor != null) {
            if (th != null) {
                try {
                    openFileDescriptor.close();
                } catch (Throwable th5) {
                    th.addSuppressed(th5);
                }
            } else {
                openFileDescriptor.close();
            }
        }
        throw th;
        throw th;
    }

    public Typeface a(Context context, a.b bVar, Resources resources, int i) {
        c[] a2;
        if (!a()) {
            return super.a(context, bVar, resources, i);
        }
        Object b2 = b();
        for (c cVar : bVar.a()) {
            if (!a(context, b2, cVar.a(), 0, cVar.b(), cVar.c() ? 1 : 0)) {
                c(b2);
                return null;
            }
        }
        if (!b(b2)) {
            return null;
        }
        return a(b2);
    }
}
