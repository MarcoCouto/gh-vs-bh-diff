package android.support.v4.c;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Build.VERSION;
import android.os.CancellationSignal;
import android.os.Handler;
import android.support.v4.b.a.a.C0012a;
import android.support.v4.b.a.a.b;
import android.support.v4.b.a.a.d;
import android.support.v4.g.b.C0016b;
import android.support.v4.h.g;

public class c {

    /* renamed from: a reason: collision with root package name */
    private static final a f644a;

    /* renamed from: b reason: collision with root package name */
    private static final g<String, Typeface> f645b = new g<>(16);

    interface a {
        Typeface a(Context context, Resources resources, int i, String str, int i2);

        Typeface a(Context context, CancellationSignal cancellationSignal, C0016b[] bVarArr, int i);

        Typeface a(Context context, b bVar, Resources resources, int i);
    }

    static {
        if (VERSION.SDK_INT >= 26) {
            f644a = new f();
        } else if (VERSION.SDK_INT >= 24 && e.a()) {
            f644a = new e();
        } else if (VERSION.SDK_INT >= 21) {
            f644a = new d();
        } else {
            f644a = new g();
        }
    }

    public static Typeface a(Context context, Resources resources, int i, String str, int i2) {
        Typeface a2 = f644a.a(context, resources, i, str, i2);
        if (a2 != null) {
            f645b.a(b(resources, i, i2), a2);
        }
        return a2;
    }

    public static Typeface a(Context context, CancellationSignal cancellationSignal, C0016b[] bVarArr, int i) {
        return f644a.a(context, cancellationSignal, bVarArr, i);
    }

    public static Typeface a(Context context, C0012a aVar, Resources resources, int i, int i2, android.support.v4.b.a.b.a aVar2, Handler handler, boolean z) {
        Typeface a2;
        boolean z2 = true;
        if (aVar instanceof d) {
            d dVar = (d) aVar;
            if (z) {
                if (dVar.b() != 0) {
                    z2 = false;
                }
            } else if (aVar2 != null) {
                z2 = false;
            }
            a2 = android.support.v4.g.b.a(context, dVar.a(), aVar2, handler, z2, z ? dVar.c() : -1, i2);
        } else {
            a2 = f644a.a(context, (b) aVar, resources, i2);
            if (aVar2 != null) {
                if (a2 != null) {
                    aVar2.a(a2, handler);
                } else {
                    aVar2.a(-3, handler);
                }
            }
        }
        if (a2 != null) {
            f645b.a(b(resources, i, i2), a2);
        }
        return a2;
    }

    public static Typeface a(Resources resources, int i, int i2) {
        return (Typeface) f645b.a(b(resources, i, i2));
    }

    private static String b(Resources resources, int i, int i2) {
        return resources.getResourcePackageName(i) + "-" + i + "-" + i2;
    }
}
