package android.support.v4.c;

import android.content.Context;
import android.graphics.Typeface;
import android.os.CancellationSignal;
import android.os.ParcelFileDescriptor;
import android.support.v4.g.b.C0016b;
import android.system.ErrnoException;
import android.system.Os;
import android.system.OsConstants;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

class d extends g {
    d() {
    }

    private File a(ParcelFileDescriptor parcelFileDescriptor) {
        try {
            String readlink = Os.readlink("/proc/self/fd/" + parcelFileDescriptor.getFd());
            if (OsConstants.S_ISREG(Os.stat(readlink).st_mode)) {
                return new File(readlink);
            }
            return null;
        } catch (ErrnoException e) {
            return null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0043, code lost:
        r4 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
        r5.addSuppressed(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0050, code lost:
        if (r2 != null) goto L_0x0052;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:?, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x005c, code lost:
        r1 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x005d, code lost:
        r2 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x0073, code lost:
        r4 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x0074, code lost:
        r2.addSuppressed(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x0093, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x0094, code lost:
        r2.addSuppressed(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x0098, code lost:
        r3.close();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0050  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x005c A[ExcHandler: all (th java.lang.Throwable), Splitter:B:6:0x0019] */
    public Typeface a(Context context, CancellationSignal cancellationSignal, C0016b[] bVarArr, int i) {
        ParcelFileDescriptor openFileDescriptor;
        FileInputStream fileInputStream;
        Throwable th;
        if (bVarArr.length < 1) {
            return null;
        }
        C0016b a2 = a(bVarArr, i);
        try {
            openFileDescriptor = context.getContentResolver().openFileDescriptor(a2.a(), "r", cancellationSignal);
            Throwable th2 = null;
            try {
                File a3 = a(openFileDescriptor);
                if (a3 == null || !a3.canRead()) {
                    fileInputStream = new FileInputStream(openFileDescriptor.getFileDescriptor());
                    Throwable th3 = null;
                    try {
                        Typeface a4 = super.a(context, (InputStream) fileInputStream);
                        if (fileInputStream != null) {
                            if (0 != 0) {
                                fileInputStream.close();
                            } else {
                                fileInputStream.close();
                            }
                        }
                        if (openFileDescriptor != null) {
                            if (0 != 0) {
                                try {
                                    openFileDescriptor.close();
                                } catch (Throwable th4) {
                                    th2.addSuppressed(th4);
                                }
                            } else {
                                openFileDescriptor.close();
                            }
                        }
                        return a4;
                    } catch (Throwable th5) {
                        th = th5;
                    }
                } else {
                    Typeface createFromFile = Typeface.createFromFile(a3);
                    if (openFileDescriptor != null) {
                        if (0 != 0) {
                            try {
                                openFileDescriptor.close();
                            } catch (Throwable th6) {
                                th2.addSuppressed(th6);
                            }
                        } else {
                            openFileDescriptor.close();
                        }
                    }
                    return createFromFile;
                }
            } catch (Throwable th7) {
                Throwable th8 = th7;
                Throwable th9 = r1;
                th = th8;
            }
        } catch (IOException e) {
            return null;
        }
        throw th;
        throw th;
        if (fileInputStream != null) {
            if (th != null) {
                fileInputStream.close();
            } else {
                fileInputStream.close();
            }
        }
        throw th;
        if (openFileDescriptor != null) {
        }
        throw th;
    }
}
