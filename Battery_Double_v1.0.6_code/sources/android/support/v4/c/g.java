package android.support.v4.c;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.CancellationSignal;
import android.support.v4.b.a.a.b;
import android.support.v4.b.a.a.c;
import android.support.v4.g.b.C0016b;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

class g implements a {

    private interface a<T> {
        boolean a(T t);

        int b(T t);
    }

    g() {
    }

    private c a(b bVar, int i) {
        return (c) a(bVar.a(), i, new a<c>() {
            /* renamed from: a */
            public int b(c cVar) {
                return cVar.b();
            }

            /* renamed from: b */
            public boolean a(c cVar) {
                return cVar.c();
            }
        });
    }

    private static <T> T a(T[] tArr, int i, a<T> aVar) {
        T t;
        int i2 = (i & 1) == 0 ? 400 : 700;
        boolean z = (i & 2) != 0;
        T t2 = null;
        int i3 = Integer.MAX_VALUE;
        int length = tArr.length;
        int i4 = 0;
        while (i4 < length) {
            T t3 = tArr[i4];
            int abs = (aVar.a(t3) == z ? 0 : 1) + (Math.abs(aVar.b(t3) - i2) * 2);
            if (t2 == null || i3 > abs) {
                i3 = abs;
                t = t3;
            } else {
                t = t2;
            }
            i4++;
            t2 = t;
        }
        return t2;
    }

    public Typeface a(Context context, Resources resources, int i, String str, int i2) {
        Typeface typeface = null;
        File a2 = h.a(context);
        if (a2 != null) {
            try {
                if (h.a(a2, resources, i)) {
                    typeface = Typeface.createFromFile(a2.getPath());
                    a2.delete();
                }
            } catch (RuntimeException e) {
            } finally {
                a2.delete();
            }
        }
        return typeface;
    }

    /* JADX WARNING: type inference failed for: r1v2, types: [java.io.Closeable] */
    /* JADX WARNING: type inference failed for: r1v4 */
    /* JADX WARNING: type inference failed for: r1v5, types: [java.io.Closeable] */
    /* JADX WARNING: type inference failed for: r1v7 */
    /* JADX WARNING: type inference failed for: r1v10 */
    /* JADX WARNING: type inference failed for: r1v11 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 2 */
    public Typeface a(Context context, CancellationSignal cancellationSignal, C0016b[] bVarArr, int i) {
        ? r1;
        Throwable th;
        ? r12;
        Typeface typeface = 0;
        if (bVarArr.length >= 1) {
            try {
                InputStream openInputStream = context.getContentResolver().openInputStream(a(bVarArr, i).a());
                try {
                    Typeface a2 = a(context, openInputStream);
                    h.a((Closeable) openInputStream);
                    typeface = a2;
                } catch (IOException e) {
                    r12 = openInputStream;
                    h.a((Closeable) r12);
                    return typeface;
                } catch (Throwable th2) {
                    th = th2;
                    r1 = openInputStream;
                    h.a((Closeable) r1);
                    throw th;
                }
            } catch (IOException e2) {
                r12 = typeface;
            } catch (Throwable th3) {
                Throwable th4 = th3;
                r1 = typeface;
                th = th4;
                h.a((Closeable) r1);
                throw th;
            }
        }
        return typeface;
    }

    public Typeface a(Context context, b bVar, Resources resources, int i) {
        c a2 = a(bVar, i);
        if (a2 == null) {
            return null;
        }
        return c.a(context, resources, a2.d(), a2.a(), i);
    }

    /* access modifiers changed from: protected */
    public Typeface a(Context context, InputStream inputStream) {
        Typeface typeface = null;
        File a2 = h.a(context);
        if (a2 != null) {
            try {
                if (h.a(a2, inputStream)) {
                    typeface = Typeface.createFromFile(a2.getPath());
                    a2.delete();
                }
            } catch (RuntimeException e) {
            } finally {
                a2.delete();
            }
        }
        return typeface;
    }

    /* access modifiers changed from: protected */
    public C0016b a(C0016b[] bVarArr, int i) {
        return (C0016b) a(bVarArr, i, new a<C0016b>() {
            /* renamed from: a */
            public int b(C0016b bVar) {
                return bVar.c();
            }

            /* renamed from: b */
            public boolean a(C0016b bVar) {
                return bVar.d();
            }
        });
    }
}
