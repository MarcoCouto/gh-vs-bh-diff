package android.support.v4.media.session;

import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder.DeathRecipient;
import android.os.Message;
import android.os.RemoteException;
import android.os.ResultReceiver;
import android.support.v4.a.g;
import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.session.MediaSessionCompat.QueueItem;
import android.support.v4.media.session.a.C0021a;
import android.util.Log;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.List;

public final class MediaControllerCompat {

    static class MediaControllerImplApi21 {

        /* renamed from: a reason: collision with root package name */
        private final List<a> f788a;
        /* access modifiers changed from: private */

        /* renamed from: b reason: collision with root package name */
        public b f789b;
        private HashMap<a, a> c;

        private static class ExtraBinderRequestResultReceiver extends ResultReceiver {

            /* renamed from: a reason: collision with root package name */
            private WeakReference<MediaControllerImplApi21> f790a;

            /* access modifiers changed from: protected */
            public void onReceiveResult(int i, Bundle bundle) {
                MediaControllerImplApi21 mediaControllerImplApi21 = (MediaControllerImplApi21) this.f790a.get();
                if (mediaControllerImplApi21 != null && bundle != null) {
                    mediaControllerImplApi21.f789b = android.support.v4.media.session.b.a.a(g.a(bundle, "android.support.v4.media.session.EXTRA_BINDER"));
                    mediaControllerImplApi21.a();
                }
            }
        }

        private static class a extends c {
            a(a aVar) {
                super(aVar);
            }

            public void a() throws RemoteException {
                throw new AssertionError();
            }

            public void a(Bundle bundle) throws RemoteException {
                throw new AssertionError();
            }

            public void a(MediaMetadataCompat mediaMetadataCompat) throws RemoteException {
                throw new AssertionError();
            }

            public void a(ParcelableVolumeInfo parcelableVolumeInfo) throws RemoteException {
                throw new AssertionError();
            }

            public void a(CharSequence charSequence) throws RemoteException {
                throw new AssertionError();
            }

            public void a(List<QueueItem> list) throws RemoteException {
                throw new AssertionError();
            }
        }

        /* access modifiers changed from: private */
        public void a() {
            if (this.f789b != null) {
                synchronized (this.f788a) {
                    for (a aVar : this.f788a) {
                        a aVar2 = new a(aVar);
                        this.c.put(aVar, aVar2);
                        aVar.f792b = true;
                        try {
                            this.f789b.a((a) aVar2);
                            aVar.a();
                        } catch (RemoteException e) {
                            Log.e("MediaControllerCompat", "Dead object in registerCallback.", e);
                        }
                    }
                    this.f788a.clear();
                }
            }
        }
    }

    public static abstract class a implements DeathRecipient {

        /* renamed from: a reason: collision with root package name */
        C0020a f791a;

        /* renamed from: b reason: collision with root package name */
        boolean f792b;
        private final Object c;

        /* renamed from: android.support.v4.media.session.MediaControllerCompat$a$a reason: collision with other inner class name */
        private class C0020a extends Handler {

            /* renamed from: a reason: collision with root package name */
            boolean f793a;

            /* renamed from: b reason: collision with root package name */
            final /* synthetic */ a f794b;

            public void handleMessage(Message message) {
                if (this.f793a) {
                    switch (message.what) {
                        case 1:
                            this.f794b.a((String) message.obj, message.getData());
                            return;
                        case 2:
                            this.f794b.a((PlaybackStateCompat) message.obj);
                            return;
                        case 3:
                            this.f794b.a((MediaMetadataCompat) message.obj);
                            return;
                        case 4:
                            this.f794b.a((b) message.obj);
                            return;
                        case 5:
                            this.f794b.a((List) message.obj);
                            return;
                        case 6:
                            this.f794b.a((CharSequence) message.obj);
                            return;
                        case 7:
                            this.f794b.a((Bundle) message.obj);
                            return;
                        case 8:
                            this.f794b.b();
                            return;
                        case 9:
                            this.f794b.a(((Integer) message.obj).intValue());
                            return;
                        case 11:
                            this.f794b.a(((Boolean) message.obj).booleanValue());
                            return;
                        case 12:
                            this.f794b.b(((Integer) message.obj).intValue());
                            return;
                        case 13:
                            this.f794b.a();
                            return;
                        default:
                            return;
                    }
                }
            }
        }

        private static class b implements android.support.v4.media.session.c.a {

            /* renamed from: a reason: collision with root package name */
            private final WeakReference<a> f795a;

            b(a aVar) {
                this.f795a = new WeakReference<>(aVar);
            }

            public void a() {
                a aVar = (a) this.f795a.get();
                if (aVar != null) {
                    aVar.b();
                }
            }

            public void a(int i, int i2, int i3, int i4, int i5) {
                a aVar = (a) this.f795a.get();
                if (aVar != null) {
                    aVar.a(new b(i, i2, i3, i4, i5));
                }
            }

            public void a(Bundle bundle) {
                a aVar = (a) this.f795a.get();
                if (aVar != null) {
                    aVar.a(bundle);
                }
            }

            public void a(CharSequence charSequence) {
                a aVar = (a) this.f795a.get();
                if (aVar != null) {
                    aVar.a(charSequence);
                }
            }

            public void a(Object obj) {
                a aVar = (a) this.f795a.get();
                if (aVar != null && !aVar.f792b) {
                    aVar.a(PlaybackStateCompat.a(obj));
                }
            }

            public void a(String str, Bundle bundle) {
                a aVar = (a) this.f795a.get();
                if (aVar == null) {
                    return;
                }
                if (!aVar.f792b || VERSION.SDK_INT >= 23) {
                    aVar.a(str, bundle);
                }
            }

            public void a(List<?> list) {
                a aVar = (a) this.f795a.get();
                if (aVar != null) {
                    aVar.a(QueueItem.a(list));
                }
            }

            public void b(Object obj) {
                a aVar = (a) this.f795a.get();
                if (aVar != null) {
                    aVar.a(MediaMetadataCompat.a(obj));
                }
            }
        }

        private static class c extends C0021a {

            /* renamed from: a reason: collision with root package name */
            private final WeakReference<a> f796a;

            c(a aVar) {
                this.f796a = new WeakReference<>(aVar);
            }

            public void a() throws RemoteException {
                a aVar = (a) this.f796a.get();
                if (aVar != null) {
                    aVar.a(8, null, null);
                }
            }

            public void a(int i) throws RemoteException {
                a aVar = (a) this.f796a.get();
                if (aVar != null) {
                    aVar.a(9, Integer.valueOf(i), null);
                }
            }

            public void a(Bundle bundle) throws RemoteException {
                a aVar = (a) this.f796a.get();
                if (aVar != null) {
                    aVar.a(7, bundle, null);
                }
            }

            public void a(MediaMetadataCompat mediaMetadataCompat) throws RemoteException {
                a aVar = (a) this.f796a.get();
                if (aVar != null) {
                    aVar.a(3, mediaMetadataCompat, null);
                }
            }

            public void a(ParcelableVolumeInfo parcelableVolumeInfo) throws RemoteException {
                a aVar = (a) this.f796a.get();
                if (aVar != null) {
                    aVar.a(4, parcelableVolumeInfo != null ? new b(parcelableVolumeInfo.f804a, parcelableVolumeInfo.f805b, parcelableVolumeInfo.c, parcelableVolumeInfo.d, parcelableVolumeInfo.e) : null, null);
                }
            }

            public void a(PlaybackStateCompat playbackStateCompat) throws RemoteException {
                a aVar = (a) this.f796a.get();
                if (aVar != null) {
                    aVar.a(2, playbackStateCompat, null);
                }
            }

            public void a(CharSequence charSequence) throws RemoteException {
                a aVar = (a) this.f796a.get();
                if (aVar != null) {
                    aVar.a(6, charSequence, null);
                }
            }

            public void a(String str, Bundle bundle) throws RemoteException {
                a aVar = (a) this.f796a.get();
                if (aVar != null) {
                    aVar.a(1, str, bundle);
                }
            }

            public void a(List<QueueItem> list) throws RemoteException {
                a aVar = (a) this.f796a.get();
                if (aVar != null) {
                    aVar.a(5, list, null);
                }
            }

            public void a(boolean z) throws RemoteException {
            }

            public void b() throws RemoteException {
                a aVar = (a) this.f796a.get();
                if (aVar != null) {
                    aVar.a(13, null, null);
                }
            }

            public void b(int i) throws RemoteException {
                a aVar = (a) this.f796a.get();
                if (aVar != null) {
                    aVar.a(12, Integer.valueOf(i), null);
                }
            }

            public void b(boolean z) throws RemoteException {
                a aVar = (a) this.f796a.get();
                if (aVar != null) {
                    aVar.a(11, Boolean.valueOf(z), null);
                }
            }
        }

        public a() {
            if (VERSION.SDK_INT >= 21) {
                this.c = c.a(new b(this));
            } else {
                this.c = new c(this);
            }
        }

        public void a() {
        }

        public void a(int i) {
        }

        /* access modifiers changed from: 0000 */
        public void a(int i, Object obj, Bundle bundle) {
            if (this.f791a != null) {
                Message obtainMessage = this.f791a.obtainMessage(i, obj);
                obtainMessage.setData(bundle);
                obtainMessage.sendToTarget();
            }
        }

        public void a(Bundle bundle) {
        }

        public void a(MediaMetadataCompat mediaMetadataCompat) {
        }

        public void a(b bVar) {
        }

        public void a(PlaybackStateCompat playbackStateCompat) {
        }

        public void a(CharSequence charSequence) {
        }

        public void a(String str, Bundle bundle) {
        }

        public void a(List<QueueItem> list) {
        }

        public void a(boolean z) {
        }

        public void b() {
        }

        public void b(int i) {
        }
    }

    public static final class b {

        /* renamed from: a reason: collision with root package name */
        private final int f797a;

        /* renamed from: b reason: collision with root package name */
        private final int f798b;
        private final int c;
        private final int d;
        private final int e;

        b(int i, int i2, int i3, int i4, int i5) {
            this.f797a = i;
            this.f798b = i2;
            this.c = i3;
            this.d = i4;
            this.e = i5;
        }
    }
}
