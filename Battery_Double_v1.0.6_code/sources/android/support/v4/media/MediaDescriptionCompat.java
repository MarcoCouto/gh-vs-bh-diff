package android.support.v4.media;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.text.TextUtils;

public final class MediaDescriptionCompat implements Parcelable {
    public static final Creator<MediaDescriptionCompat> CREATOR = new Creator<MediaDescriptionCompat>() {
        /* renamed from: a */
        public MediaDescriptionCompat createFromParcel(Parcel parcel) {
            return VERSION.SDK_INT < 21 ? new MediaDescriptionCompat(parcel) : MediaDescriptionCompat.a(a.a(parcel));
        }

        /* renamed from: a */
        public MediaDescriptionCompat[] newArray(int i) {
            return new MediaDescriptionCompat[i];
        }
    };

    /* renamed from: a reason: collision with root package name */
    private final String f780a;

    /* renamed from: b reason: collision with root package name */
    private final CharSequence f781b;
    private final CharSequence c;
    private final CharSequence d;
    private final Bitmap e;
    private final Uri f;
    private final Bundle g;
    private final Uri h;
    private Object i;

    public static final class a {

        /* renamed from: a reason: collision with root package name */
        private String f782a;

        /* renamed from: b reason: collision with root package name */
        private CharSequence f783b;
        private CharSequence c;
        private CharSequence d;
        private Bitmap e;
        private Uri f;
        private Bundle g;
        private Uri h;

        public a a(Bitmap bitmap) {
            this.e = bitmap;
            return this;
        }

        public a a(Uri uri) {
            this.f = uri;
            return this;
        }

        public a a(Bundle bundle) {
            this.g = bundle;
            return this;
        }

        public a a(CharSequence charSequence) {
            this.f783b = charSequence;
            return this;
        }

        public a a(String str) {
            this.f782a = str;
            return this;
        }

        public MediaDescriptionCompat a() {
            return new MediaDescriptionCompat(this.f782a, this.f783b, this.c, this.d, this.e, this.f, this.g, this.h);
        }

        public a b(Uri uri) {
            this.h = uri;
            return this;
        }

        public a b(CharSequence charSequence) {
            this.c = charSequence;
            return this;
        }

        public a c(CharSequence charSequence) {
            this.d = charSequence;
            return this;
        }
    }

    MediaDescriptionCompat(Parcel parcel) {
        this.f780a = parcel.readString();
        this.f781b = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.c = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.d = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.e = (Bitmap) parcel.readParcelable(null);
        this.f = (Uri) parcel.readParcelable(null);
        this.g = parcel.readBundle();
        this.h = (Uri) parcel.readParcelable(null);
    }

    MediaDescriptionCompat(String str, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, Bitmap bitmap, Uri uri, Bundle bundle, Uri uri2) {
        this.f780a = str;
        this.f781b = charSequence;
        this.c = charSequence2;
        this.d = charSequence3;
        this.e = bitmap;
        this.f = uri;
        this.g = bundle;
        this.h = uri2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0056  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0076  */
    public static MediaDescriptionCompat a(Object obj) {
        Bundle bundle;
        if (obj == null || VERSION.SDK_INT < 21) {
            return null;
        }
        a aVar = new a();
        aVar.a(a.a(obj));
        aVar.a(a.b(obj));
        aVar.b(a.c(obj));
        aVar.c(a.d(obj));
        aVar.a(a.e(obj));
        aVar.a(a.f(obj));
        Bundle g2 = a.g(obj);
        Uri uri = g2 == null ? null : (Uri) g2.getParcelable("android.support.v4.media.description.MEDIA_URI");
        if (uri != null) {
            if (!g2.containsKey("android.support.v4.media.description.NULL_BUNDLE_FLAG") || g2.size() != 2) {
                g2.remove("android.support.v4.media.description.MEDIA_URI");
                g2.remove("android.support.v4.media.description.NULL_BUNDLE_FLAG");
            } else {
                bundle = null;
                aVar.a(bundle);
                if (uri == null) {
                    aVar.b(uri);
                } else if (VERSION.SDK_INT >= 23) {
                    aVar.b(b.h(obj));
                }
                MediaDescriptionCompat a2 = aVar.a();
                a2.i = obj;
                return a2;
            }
        }
        bundle = g2;
        aVar.a(bundle);
        if (uri == null) {
        }
        MediaDescriptionCompat a22 = aVar.a();
        a22.i = obj;
        return a22;
    }

    public Object a() {
        if (this.i != null || VERSION.SDK_INT < 21) {
            return this.i;
        }
        Object a2 = C0019a.a();
        C0019a.a(a2, this.f780a);
        C0019a.a(a2, this.f781b);
        C0019a.b(a2, this.c);
        C0019a.c(a2, this.d);
        C0019a.a(a2, this.e);
        C0019a.a(a2, this.f);
        Bundle bundle = this.g;
        if (VERSION.SDK_INT < 23 && this.h != null) {
            if (bundle == null) {
                bundle = new Bundle();
                bundle.putBoolean("android.support.v4.media.description.NULL_BUNDLE_FLAG", true);
            }
            bundle.putParcelable("android.support.v4.media.description.MEDIA_URI", this.h);
        }
        C0019a.a(a2, bundle);
        if (VERSION.SDK_INT >= 23) {
            a.b(a2, this.h);
        }
        this.i = C0019a.a(a2);
        return this.i;
    }

    public int describeContents() {
        return 0;
    }

    public String toString() {
        return this.f781b + ", " + this.c + ", " + this.d;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        if (VERSION.SDK_INT < 21) {
            parcel.writeString(this.f780a);
            TextUtils.writeToParcel(this.f781b, parcel, i2);
            TextUtils.writeToParcel(this.c, parcel, i2);
            TextUtils.writeToParcel(this.d, parcel, i2);
            parcel.writeParcelable(this.e, i2);
            parcel.writeParcelable(this.f, i2);
            parcel.writeBundle(this.g);
            parcel.writeParcelable(this.h, i2);
            return;
        }
        a.a(a(), parcel, i2);
    }
}
