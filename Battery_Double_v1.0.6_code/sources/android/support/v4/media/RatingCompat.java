package android.support.v4.media;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public final class RatingCompat implements Parcelable {
    public static final Creator<RatingCompat> CREATOR = new Creator<RatingCompat>() {
        /* renamed from: a */
        public RatingCompat createFromParcel(Parcel parcel) {
            return new RatingCompat(parcel.readInt(), parcel.readFloat());
        }

        /* renamed from: a */
        public RatingCompat[] newArray(int i) {
            return new RatingCompat[i];
        }
    };

    /* renamed from: a reason: collision with root package name */
    private final int f786a;

    /* renamed from: b reason: collision with root package name */
    private final float f787b;

    RatingCompat(int i, float f) {
        this.f786a = i;
        this.f787b = f;
    }

    public int describeContents() {
        return this.f786a;
    }

    public String toString() {
        return "Rating:style=" + this.f786a + " rating=" + (this.f787b < 0.0f ? "unrated" : String.valueOf(this.f787b));
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.f786a);
        parcel.writeFloat(this.f787b);
    }
}
