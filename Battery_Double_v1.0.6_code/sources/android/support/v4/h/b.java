package android.support.v4.h;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public final class b<E> implements Collection<E>, Set<E> {

    /* renamed from: a reason: collision with root package name */
    static Object[] f684a;

    /* renamed from: b reason: collision with root package name */
    static int f685b;
    static Object[] c;
    static int d;
    private static final int[] j = new int[0];
    private static final Object[] k = new Object[0];
    final boolean e;
    int[] f;
    Object[] g;
    int h;
    h<E, E> i;

    public b() {
        this(0, false);
    }

    public b(int i2) {
        this(i2, false);
    }

    public b(int i2, boolean z) {
        this.e = z;
        if (i2 == 0) {
            this.f = j;
            this.g = k;
        } else {
            d(i2);
        }
        this.h = 0;
    }

    private int a() {
        int i2 = this.h;
        if (i2 == 0) {
            return -1;
        }
        int a2 = c.a(this.f, i2, 0);
        if (a2 < 0 || this.g[a2] == null) {
            return a2;
        }
        int i3 = a2 + 1;
        while (i3 < i2 && this.f[i3] == 0) {
            if (this.g[i3] == null) {
                return i3;
            }
            i3++;
        }
        int i4 = a2 - 1;
        while (i4 >= 0 && this.f[i4] == 0) {
            if (this.g[i4] == null) {
                return i4;
            }
            i4--;
        }
        return i3 ^ -1;
    }

    private int a(Object obj, int i2) {
        int i3 = this.h;
        if (i3 == 0) {
            return -1;
        }
        int a2 = c.a(this.f, i3, i2);
        if (a2 < 0 || obj.equals(this.g[a2])) {
            return a2;
        }
        int i4 = a2 + 1;
        while (i4 < i3 && this.f[i4] == i2) {
            if (obj.equals(this.g[i4])) {
                return i4;
            }
            i4++;
        }
        int i5 = a2 - 1;
        while (i5 >= 0 && this.f[i5] == i2) {
            if (obj.equals(this.g[i5])) {
                return i5;
            }
            i5--;
        }
        return i4 ^ -1;
    }

    private static void a(int[] iArr, Object[] objArr, int i2) {
        if (iArr.length == 8) {
            synchronized (b.class) {
                if (d < 10) {
                    objArr[0] = c;
                    objArr[1] = iArr;
                    for (int i3 = i2 - 1; i3 >= 2; i3--) {
                        objArr[i3] = null;
                    }
                    c = objArr;
                    d++;
                }
            }
        } else if (iArr.length == 4) {
            synchronized (b.class) {
                if (f685b < 10) {
                    objArr[0] = f684a;
                    objArr[1] = iArr;
                    for (int i4 = i2 - 1; i4 >= 2; i4--) {
                        objArr[i4] = null;
                    }
                    f684a = objArr;
                    f685b++;
                }
            }
        }
    }

    private h<E, E> b() {
        if (this.i == null) {
            this.i = new h<E, E>() {
                /* access modifiers changed from: protected */
                public int a() {
                    return b.this.h;
                }

                /* access modifiers changed from: protected */
                public int a(Object obj) {
                    return b.this.a(obj);
                }

                /* access modifiers changed from: protected */
                public Object a(int i, int i2) {
                    return b.this.g[i];
                }

                /* access modifiers changed from: protected */
                public E a(int i, E e) {
                    throw new UnsupportedOperationException("not a map");
                }

                /* access modifiers changed from: protected */
                public void a(int i) {
                    b.this.c(i);
                }

                /* access modifiers changed from: protected */
                public void a(E e, E e2) {
                    b.this.add(e);
                }

                /* access modifiers changed from: protected */
                public int b(Object obj) {
                    return b.this.a(obj);
                }

                /* access modifiers changed from: protected */
                public Map<E, E> b() {
                    throw new UnsupportedOperationException("not a map");
                }

                /* access modifiers changed from: protected */
                public void c() {
                    b.this.clear();
                }
            };
        }
        return this.i;
    }

    private void d(int i2) {
        if (i2 == 8) {
            synchronized (b.class) {
                if (c != null) {
                    Object[] objArr = c;
                    this.g = objArr;
                    c = (Object[]) objArr[0];
                    this.f = (int[]) objArr[1];
                    objArr[1] = null;
                    objArr[0] = null;
                    d--;
                    return;
                }
            }
        } else if (i2 == 4) {
            synchronized (b.class) {
                if (f684a != null) {
                    Object[] objArr2 = f684a;
                    this.g = objArr2;
                    f684a = (Object[]) objArr2[0];
                    this.f = (int[]) objArr2[1];
                    objArr2[1] = null;
                    objArr2[0] = null;
                    f685b--;
                    return;
                }
            }
        }
        this.f = new int[i2];
        this.g = new Object[i2];
    }

    public int a(Object obj) {
        if (obj == null) {
            return a();
        }
        return a(obj, this.e ? System.identityHashCode(obj) : obj.hashCode());
    }

    public void a(int i2) {
        if (this.f.length < i2) {
            int[] iArr = this.f;
            Object[] objArr = this.g;
            d(i2);
            if (this.h > 0) {
                System.arraycopy(iArr, 0, this.f, 0, this.h);
                System.arraycopy(objArr, 0, this.g, 0, this.h);
            }
            a(iArr, objArr, this.h);
        }
    }

    public boolean add(E e2) {
        int i2;
        int a2;
        if (e2 == null) {
            a2 = a();
            i2 = 0;
        } else {
            int hashCode = this.e ? System.identityHashCode(e2) : e2.hashCode();
            i2 = hashCode;
            a2 = a(e2, hashCode);
        }
        if (a2 >= 0) {
            return false;
        }
        int i3 = a2 ^ -1;
        if (this.h >= this.f.length) {
            int i4 = this.h >= 8 ? this.h + (this.h >> 1) : this.h >= 4 ? 8 : 4;
            int[] iArr = this.f;
            Object[] objArr = this.g;
            d(i4);
            if (this.f.length > 0) {
                System.arraycopy(iArr, 0, this.f, 0, iArr.length);
                System.arraycopy(objArr, 0, this.g, 0, objArr.length);
            }
            a(iArr, objArr, this.h);
        }
        if (i3 < this.h) {
            System.arraycopy(this.f, i3, this.f, i3 + 1, this.h - i3);
            System.arraycopy(this.g, i3, this.g, i3 + 1, this.h - i3);
        }
        this.f[i3] = i2;
        this.g[i3] = e2;
        this.h++;
        return true;
    }

    public boolean addAll(Collection<? extends E> collection) {
        a(this.h + collection.size());
        boolean z = false;
        for (Object add : collection) {
            z |= add(add);
        }
        return z;
    }

    public E b(int i2) {
        return this.g[i2];
    }

    public E c(int i2) {
        int i3 = 8;
        E e2 = this.g[i2];
        if (this.h <= 1) {
            a(this.f, this.g, this.h);
            this.f = j;
            this.g = k;
            this.h = 0;
        } else if (this.f.length <= 8 || this.h >= this.f.length / 3) {
            this.h--;
            if (i2 < this.h) {
                System.arraycopy(this.f, i2 + 1, this.f, i2, this.h - i2);
                System.arraycopy(this.g, i2 + 1, this.g, i2, this.h - i2);
            }
            this.g[this.h] = null;
        } else {
            if (this.h > 8) {
                i3 = this.h + (this.h >> 1);
            }
            int[] iArr = this.f;
            Object[] objArr = this.g;
            d(i3);
            this.h--;
            if (i2 > 0) {
                System.arraycopy(iArr, 0, this.f, 0, i2);
                System.arraycopy(objArr, 0, this.g, 0, i2);
            }
            if (i2 < this.h) {
                System.arraycopy(iArr, i2 + 1, this.f, i2, this.h - i2);
                System.arraycopy(objArr, i2 + 1, this.g, i2, this.h - i2);
            }
        }
        return e2;
    }

    public void clear() {
        if (this.h != 0) {
            a(this.f, this.g, this.h);
            this.f = j;
            this.g = k;
            this.h = 0;
        }
    }

    public boolean contains(Object obj) {
        return a(obj) >= 0;
    }

    public boolean containsAll(Collection<?> collection) {
        for (Object contains : collection) {
            if (!contains(contains)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Set)) {
            return false;
        }
        Set set = (Set) obj;
        if (size() != set.size()) {
            return false;
        }
        int i2 = 0;
        while (i2 < this.h) {
            try {
                if (!set.contains(b(i2))) {
                    return false;
                }
                i2++;
            } catch (NullPointerException e2) {
                return false;
            } catch (ClassCastException e3) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        int[] iArr = this.f;
        int i2 = 0;
        for (int i3 = 0; i3 < this.h; i3++) {
            i2 += iArr[i3];
        }
        return i2;
    }

    public boolean isEmpty() {
        return this.h <= 0;
    }

    public Iterator<E> iterator() {
        return b().e().iterator();
    }

    public boolean remove(Object obj) {
        int a2 = a(obj);
        if (a2 < 0) {
            return false;
        }
        c(a2);
        return true;
    }

    public boolean removeAll(Collection<?> collection) {
        boolean z = false;
        for (Object remove : collection) {
            z |= remove(remove);
        }
        return z;
    }

    public boolean retainAll(Collection<?> collection) {
        boolean z = false;
        for (int i2 = this.h - 1; i2 >= 0; i2--) {
            if (!collection.contains(this.g[i2])) {
                c(i2);
                z = true;
            }
        }
        return z;
    }

    public int size() {
        return this.h;
    }

    public Object[] toArray() {
        Object[] objArr = new Object[this.h];
        System.arraycopy(this.g, 0, objArr, 0, this.h);
        return objArr;
    }

    public <T> T[] toArray(T[] tArr) {
        T[] tArr2 = tArr.length < this.h ? (Object[]) Array.newInstance(tArr.getClass().getComponentType(), this.h) : tArr;
        System.arraycopy(this.g, 0, tArr2, 0, this.h);
        if (tArr2.length > this.h) {
            tArr2[this.h] = null;
        }
        return tArr2;
    }

    public String toString() {
        if (isEmpty()) {
            return "{}";
        }
        StringBuilder sb = new StringBuilder(this.h * 14);
        sb.append('{');
        for (int i2 = 0; i2 < this.h; i2++) {
            if (i2 > 0) {
                sb.append(", ");
            }
            Object b2 = b(i2);
            if (b2 != this) {
                sb.append(b2);
            } else {
                sb.append("(this Set)");
            }
        }
        sb.append('}');
        return sb.toString();
    }
}
