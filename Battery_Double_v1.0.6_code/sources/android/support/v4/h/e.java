package android.support.v4.h;

import android.util.Log;
import java.io.Writer;

public class e extends Writer {

    /* renamed from: a reason: collision with root package name */
    private final String f689a;

    /* renamed from: b reason: collision with root package name */
    private StringBuilder f690b = new StringBuilder(128);

    public e(String str) {
        this.f689a = str;
    }

    private void a() {
        if (this.f690b.length() > 0) {
            Log.d(this.f689a, this.f690b.toString());
            this.f690b.delete(0, this.f690b.length());
        }
    }

    public void close() {
        a();
    }

    public void flush() {
        a();
    }

    public void write(char[] cArr, int i, int i2) {
        for (int i3 = 0; i3 < i2; i3++) {
            char c = cArr[i + i3];
            if (c == 10) {
                a();
            } else {
                this.f690b.append(c);
            }
        }
    }
}
