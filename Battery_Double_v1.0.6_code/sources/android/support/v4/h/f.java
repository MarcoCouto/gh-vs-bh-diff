package android.support.v4.h;

public class f<E> implements Cloneable {

    /* renamed from: a reason: collision with root package name */
    private static final Object f691a = new Object();

    /* renamed from: b reason: collision with root package name */
    private boolean f692b;
    private long[] c;
    private Object[] d;
    private int e;

    public f() {
        this(10);
    }

    public f(int i) {
        this.f692b = false;
        if (i == 0) {
            this.c = c.f688b;
            this.d = c.c;
        } else {
            int b2 = c.b(i);
            this.c = new long[b2];
            this.d = new Object[b2];
        }
        this.e = 0;
    }

    private void d() {
        int i = this.e;
        long[] jArr = this.c;
        Object[] objArr = this.d;
        int i2 = 0;
        for (int i3 = 0; i3 < i; i3++) {
            Object obj = objArr[i3];
            if (obj != f691a) {
                if (i3 != i2) {
                    jArr[i2] = jArr[i3];
                    objArr[i2] = obj;
                    objArr[i3] = null;
                }
                i2++;
            }
        }
        this.f692b = false;
        this.e = i2;
    }

    /* renamed from: a */
    public f<E> clone() {
        try {
            f<E> fVar = (f) super.clone();
            try {
                fVar.c = (long[]) this.c.clone();
                fVar.d = (Object[]) this.d.clone();
                return fVar;
            } catch (CloneNotSupportedException e2) {
                return fVar;
            }
        } catch (CloneNotSupportedException e3) {
            return null;
        }
    }

    public E a(long j) {
        return a(j, null);
    }

    public E a(long j, E e2) {
        int a2 = c.a(this.c, this.e, j);
        return (a2 < 0 || this.d[a2] == f691a) ? e2 : this.d[a2];
    }

    public void a(int i) {
        if (this.d[i] != f691a) {
            this.d[i] = f691a;
            this.f692b = true;
        }
    }

    public int b() {
        if (this.f692b) {
            d();
        }
        return this.e;
    }

    public long b(int i) {
        if (this.f692b) {
            d();
        }
        return this.c[i];
    }

    public void b(long j) {
        int a2 = c.a(this.c, this.e, j);
        if (a2 >= 0 && this.d[a2] != f691a) {
            this.d[a2] = f691a;
            this.f692b = true;
        }
    }

    public void b(long j, E e2) {
        int a2 = c.a(this.c, this.e, j);
        if (a2 >= 0) {
            this.d[a2] = e2;
            return;
        }
        int i = a2 ^ -1;
        if (i >= this.e || this.d[i] != f691a) {
            if (this.f692b && this.e >= this.c.length) {
                d();
                i = c.a(this.c, this.e, j) ^ -1;
            }
            if (this.e >= this.c.length) {
                int b2 = c.b(this.e + 1);
                long[] jArr = new long[b2];
                Object[] objArr = new Object[b2];
                System.arraycopy(this.c, 0, jArr, 0, this.c.length);
                System.arraycopy(this.d, 0, objArr, 0, this.d.length);
                this.c = jArr;
                this.d = objArr;
            }
            if (this.e - i != 0) {
                System.arraycopy(this.c, i, this.c, i + 1, this.e - i);
                System.arraycopy(this.d, i, this.d, i + 1, this.e - i);
            }
            this.c[i] = j;
            this.d[i] = e2;
            this.e++;
            return;
        }
        this.c[i] = j;
        this.d[i] = e2;
    }

    public int c(long j) {
        if (this.f692b) {
            d();
        }
        return c.a(this.c, this.e, j);
    }

    public E c(int i) {
        if (this.f692b) {
            d();
        }
        return this.d[i];
    }

    public void c() {
        int i = this.e;
        Object[] objArr = this.d;
        for (int i2 = 0; i2 < i; i2++) {
            objArr[i2] = null;
        }
        this.e = 0;
        this.f692b = false;
    }

    public String toString() {
        if (b() <= 0) {
            return "{}";
        }
        StringBuilder sb = new StringBuilder(this.e * 28);
        sb.append('{');
        for (int i = 0; i < this.e; i++) {
            if (i > 0) {
                sb.append(", ");
            }
            sb.append(b(i));
            sb.append('=');
            Object c2 = c(i);
            if (c2 != this) {
                sb.append(c2);
            } else {
                sb.append("(this Map)");
            }
        }
        sb.append('}');
        return sb.toString();
    }
}
