package android.support.v4.h;

public final class k {

    public interface a<T> {
        T a();

        boolean a(T t);
    }

    public static class b<T> implements a<T> {

        /* renamed from: a reason: collision with root package name */
        private final Object[] f705a;

        /* renamed from: b reason: collision with root package name */
        private int f706b;

        public b(int i) {
            if (i <= 0) {
                throw new IllegalArgumentException("The max pool size must be > 0");
            }
            this.f705a = new Object[i];
        }

        private boolean b(T t) {
            for (int i = 0; i < this.f706b; i++) {
                if (this.f705a[i] == t) {
                    return true;
                }
            }
            return false;
        }

        public T a() {
            if (this.f706b <= 0) {
                return null;
            }
            int i = this.f706b - 1;
            T t = this.f705a[i];
            this.f705a[i] = null;
            this.f706b--;
            return t;
        }

        public boolean a(T t) {
            if (b(t)) {
                throw new IllegalStateException("Already in the pool!");
            } else if (this.f706b >= this.f705a.length) {
                return false;
            } else {
                this.f705a[this.f706b] = t;
                this.f706b++;
                return true;
            }
        }
    }

    public static class c<T> extends b<T> {

        /* renamed from: a reason: collision with root package name */
        private final Object f707a = new Object();

        public c(int i) {
            super(i);
        }

        public T a() {
            T a2;
            synchronized (this.f707a) {
                a2 = super.a();
            }
            return a2;
        }

        public boolean a(T t) {
            boolean a2;
            synchronized (this.f707a) {
                a2 = super.a(t);
            }
            return a2;
        }
    }
}
