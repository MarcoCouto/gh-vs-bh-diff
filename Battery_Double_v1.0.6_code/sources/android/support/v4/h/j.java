package android.support.v4.h;

public class j<F, S> {

    /* renamed from: a reason: collision with root package name */
    public final F f703a;

    /* renamed from: b reason: collision with root package name */
    public final S f704b;

    private static boolean a(Object obj, Object obj2) {
        return obj == obj2 || (obj != null && obj.equals(obj2));
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof j)) {
            return false;
        }
        j jVar = (j) obj;
        return a(jVar.f703a, this.f703a) && a(jVar.f704b, this.f704b);
    }

    public int hashCode() {
        int i = 0;
        int hashCode = this.f703a == null ? 0 : this.f703a.hashCode();
        if (this.f704b != null) {
            i = this.f704b.hashCode();
        }
        return hashCode ^ i;
    }

    public String toString() {
        return "Pair{" + String.valueOf(this.f703a) + " " + String.valueOf(this.f704b) + "}";
    }
}
