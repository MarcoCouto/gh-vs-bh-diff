package android.support.v4.g;

import android.content.ContentUris;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ProviderInfo;
import android.content.pm.Signature;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.net.Uri.Builder;
import android.os.Build.VERSION;
import android.os.CancellationSignal;
import android.os.Handler;
import android.support.v4.c.h;
import android.support.v4.h.g;
import android.support.v4.h.l;
import android.support.v4.h.m;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

public class b {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public static final g<String, Typeface> f660a = new g<>(16);

    /* renamed from: b reason: collision with root package name */
    private static final c f661b = new c("fonts", 10, 10000);
    /* access modifiers changed from: private */
    public static final Object c = new Object();
    /* access modifiers changed from: private */
    public static final m<String, ArrayList<android.support.v4.g.c.a<c>>> d = new m<>();
    private static final Comparator<byte[]> e = new Comparator<byte[]>() {
        /* renamed from: a */
        public int compare(byte[] bArr, byte[] bArr2) {
            if (bArr.length != bArr2.length) {
                return bArr.length - bArr2.length;
            }
            for (int i = 0; i < bArr.length; i++) {
                if (bArr[i] != bArr2[i]) {
                    return bArr[i] - bArr2[i];
                }
            }
            return 0;
        }
    };

    public static class a {

        /* renamed from: a reason: collision with root package name */
        private final int f667a;

        /* renamed from: b reason: collision with root package name */
        private final C0016b[] f668b;

        public a(int i, C0016b[] bVarArr) {
            this.f667a = i;
            this.f668b = bVarArr;
        }

        public int a() {
            return this.f667a;
        }

        public C0016b[] b() {
            return this.f668b;
        }
    }

    /* renamed from: android.support.v4.g.b$b reason: collision with other inner class name */
    public static class C0016b {

        /* renamed from: a reason: collision with root package name */
        private final Uri f669a;

        /* renamed from: b reason: collision with root package name */
        private final int f670b;
        private final int c;
        private final boolean d;
        private final int e;

        public C0016b(Uri uri, int i, int i2, boolean z, int i3) {
            this.f669a = (Uri) l.a(uri);
            this.f670b = i;
            this.c = i2;
            this.d = z;
            this.e = i3;
        }

        public Uri a() {
            return this.f669a;
        }

        public int b() {
            return this.f670b;
        }

        public int c() {
            return this.c;
        }

        public boolean d() {
            return this.d;
        }

        public int e() {
            return this.e;
        }
    }

    private static final class c {

        /* renamed from: a reason: collision with root package name */
        final Typeface f671a;

        /* renamed from: b reason: collision with root package name */
        final int f672b;

        c(Typeface typeface, int i) {
            this.f671a = typeface;
            this.f672b = i;
        }
    }

    public static ProviderInfo a(PackageManager packageManager, a aVar, Resources resources) throws NameNotFoundException {
        int i = 0;
        String a2 = aVar.a();
        ProviderInfo resolveContentProvider = packageManager.resolveContentProvider(a2, 0);
        if (resolveContentProvider == null) {
            throw new NameNotFoundException("No package found for authority: " + a2);
        } else if (!resolveContentProvider.packageName.equals(aVar.b())) {
            throw new NameNotFoundException("Found content provider " + a2 + ", but package was not " + aVar.b());
        } else {
            List a3 = a(packageManager.getPackageInfo(resolveContentProvider.packageName, 64).signatures);
            Collections.sort(a3, e);
            List a4 = a(aVar, resources);
            while (true) {
                int i2 = i;
                if (i2 >= a4.size()) {
                    return null;
                }
                ArrayList arrayList = new ArrayList((Collection) a4.get(i2));
                Collections.sort(arrayList, e);
                if (a(a3, (List<byte[]>) arrayList)) {
                    return resolveContentProvider;
                }
                i = i2 + 1;
            }
        }
    }

    /* JADX INFO: used method not loaded: android.support.v4.g.c.a(java.util.concurrent.Callable, android.support.v4.g.c$a):null, types can be incorrect */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0093, code lost:
        f661b.a((java.util.concurrent.Callable) r4, (android.support.v4.g.c.a) new android.support.v4.g.b.AnonymousClass3());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:?, code lost:
        return r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:?, code lost:
        return r2;
     */
    public static Typeface a(final Context context, final a aVar, final android.support.v4.b.a.b.a aVar2, final Handler handler, boolean z, int i, final int i2) {
        Typeface typeface = 0;
        final String str = aVar.f() + "-" + i2;
        Typeface typeface2 = (Typeface) f660a.a(str);
        if (typeface2 != null) {
            if (aVar2 != null) {
                aVar2.a(typeface2);
            }
            return typeface2;
        } else if (!z || i != -1) {
            AnonymousClass1 r4 = new Callable<c>() {
                /* renamed from: a */
                public c call() throws Exception {
                    c a2 = b.b(context, aVar, i2);
                    if (a2.f671a != null) {
                        b.f660a.a(str, a2.f671a);
                    }
                    return a2;
                }
            };
            if (z) {
                try {
                    return ((c) f661b.a((Callable<T>) r4, i)).f671a;
                } catch (InterruptedException e2) {
                    return typeface;
                }
            } else {
                Object r1 = aVar2 == null ? typeface : new android.support.v4.g.c.a<c>() {
                    public void a(c cVar) {
                        if (cVar.f672b == 0) {
                            aVar2.a(cVar.f671a, handler);
                        } else {
                            aVar2.a(cVar.f672b, handler);
                        }
                    }
                };
                synchronized (c) {
                    if (d.containsKey(str)) {
                        if (r1 != 0) {
                            ((ArrayList) d.get(str)).add(r1);
                        }
                    } else if (r1 != 0) {
                        ArrayList arrayList = new ArrayList();
                        arrayList.add(r1);
                        d.put(str, arrayList);
                    }
                }
            }
        } else {
            c b2 = b(context, aVar, i2);
            if (aVar2 != null) {
                if (b2.f672b == 0) {
                    aVar2.a(b2.f671a, handler);
                } else {
                    aVar2.a(b2.f672b, handler);
                }
            }
            return b2.f671a;
        }
    }

    public static a a(Context context, CancellationSignal cancellationSignal, a aVar) throws NameNotFoundException {
        ProviderInfo a2 = a(context.getPackageManager(), aVar, context.getResources());
        return a2 == null ? new a(1, null) : new a(0, a(context, aVar, a2.authority, cancellationSignal));
    }

    private static List<List<byte[]>> a(a aVar, Resources resources) {
        return aVar.d() != null ? aVar.d() : android.support.v4.b.a.a.a(resources, aVar.e());
    }

    private static List<byte[]> a(Signature[] signatureArr) {
        ArrayList arrayList = new ArrayList();
        for (Signature byteArray : signatureArr) {
            arrayList.add(byteArray.toByteArray());
        }
        return arrayList;
    }

    public static Map<Uri, ByteBuffer> a(Context context, C0016b[] bVarArr, CancellationSignal cancellationSignal) {
        HashMap hashMap = new HashMap();
        for (C0016b bVar : bVarArr) {
            if (bVar.e() == 0) {
                Uri a2 = bVar.a();
                if (!hashMap.containsKey(a2)) {
                    hashMap.put(a2, h.a(context, cancellationSignal, a2));
                }
            }
        }
        return Collections.unmodifiableMap(hashMap);
    }

    private static boolean a(List<byte[]> list, List<byte[]> list2) {
        if (list.size() != list2.size()) {
            return false;
        }
        for (int i = 0; i < list.size(); i++) {
            if (!Arrays.equals((byte[]) list.get(i), (byte[]) list2.get(i))) {
                return false;
            }
        }
        return true;
    }

    /* JADX WARNING: Removed duplicated region for block: B:45:0x0146  */
    static C0016b[] a(Context context, a aVar, String str, CancellationSignal cancellationSignal) {
        Cursor cursor;
        ArrayList arrayList;
        ArrayList arrayList2 = new ArrayList();
        Uri build = new Builder().scheme("content").authority(str).build();
        Uri build2 = new Builder().scheme("content").authority(str).appendPath("file").build();
        try {
            Cursor query = VERSION.SDK_INT > 16 ? context.getContentResolver().query(build, new String[]{"_id", "file_id", "font_ttc_index", "font_variation_settings", "font_weight", "font_italic", "result_code"}, "query = ?", new String[]{aVar.c()}, null, cancellationSignal) : context.getContentResolver().query(build, new String[]{"_id", "file_id", "font_ttc_index", "font_variation_settings", "font_weight", "font_italic", "result_code"}, "query = ?", new String[]{aVar.c()}, null);
            if (query != null) {
                try {
                    if (query.getCount() > 0) {
                        int columnIndex = query.getColumnIndex("result_code");
                        arrayList = new ArrayList();
                        int columnIndex2 = query.getColumnIndex("_id");
                        int columnIndex3 = query.getColumnIndex("file_id");
                        int columnIndex4 = query.getColumnIndex("font_ttc_index");
                        int columnIndex5 = query.getColumnIndex("font_weight");
                        int columnIndex6 = query.getColumnIndex("font_italic");
                        while (query.moveToNext()) {
                            int i = columnIndex != -1 ? query.getInt(columnIndex) : 0;
                            arrayList.add(new C0016b(columnIndex3 == -1 ? ContentUris.withAppendedId(build, query.getLong(columnIndex2)) : ContentUris.withAppendedId(build2, query.getLong(columnIndex3)), columnIndex4 != -1 ? query.getInt(columnIndex4) : 0, columnIndex5 != -1 ? query.getInt(columnIndex5) : 400, columnIndex6 != -1 && query.getInt(columnIndex6) == 1, i));
                        }
                        if (query != null) {
                            query.close();
                        }
                        return (C0016b[]) arrayList.toArray(new C0016b[0]);
                    }
                } catch (Throwable th) {
                    th = th;
                    cursor = query;
                }
            }
            arrayList = arrayList2;
            if (query != null) {
            }
            return (C0016b[]) arrayList.toArray(new C0016b[0]);
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
        }
        if (cursor != null) {
            cursor.close();
        }
        throw th;
    }

    /* access modifiers changed from: private */
    public static c b(Context context, a aVar, int i) {
        int i2 = -3;
        try {
            a a2 = a(context, (CancellationSignal) null, aVar);
            if (a2.a() == 0) {
                Typeface a3 = android.support.v4.c.c.a(context, null, a2.b(), i);
                if (a3 != null) {
                    i2 = 0;
                }
                return new c(a3, i2);
            }
            if (a2.a() == 1) {
                i2 = -2;
            }
            return new c(null, i2);
        } catch (NameNotFoundException e2) {
            return new c(null, -1);
        }
    }
}
