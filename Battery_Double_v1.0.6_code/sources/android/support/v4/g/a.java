package android.support.v4.g;

import android.support.v4.h.l;
import android.util.Base64;
import java.util.List;

public final class a {

    /* renamed from: a reason: collision with root package name */
    private final String f658a;

    /* renamed from: b reason: collision with root package name */
    private final String f659b;
    private final String c;
    private final List<List<byte[]>> d;
    private final int e = 0;
    private final String f = new StringBuilder(this.f658a).append("-").append(this.f659b).append("-").append(this.c).toString();

    public a(String str, String str2, String str3, List<List<byte[]>> list) {
        this.f658a = (String) l.a(str);
        this.f659b = (String) l.a(str2);
        this.c = (String) l.a(str3);
        this.d = (List) l.a(list);
    }

    public String a() {
        return this.f658a;
    }

    public String b() {
        return this.f659b;
    }

    public String c() {
        return this.c;
    }

    public List<List<byte[]>> d() {
        return this.d;
    }

    public int e() {
        return this.e;
    }

    public String f() {
        return this.f;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("FontRequest {mProviderAuthority: " + this.f658a + ", mProviderPackage: " + this.f659b + ", mQuery: " + this.c + ", mCertificates:");
        for (int i = 0; i < this.d.size(); i++) {
            sb.append(" [");
            List list = (List) this.d.get(i);
            for (int i2 = 0; i2 < list.size(); i2++) {
                sb.append(" \"");
                sb.append(Base64.encodeToString((byte[]) list.get(i2), 0));
                sb.append("\"");
            }
            sb.append(" ]");
        }
        sb.append("}");
        sb.append("mCertificatesArray: " + this.e);
        return sb.toString();
    }
}
