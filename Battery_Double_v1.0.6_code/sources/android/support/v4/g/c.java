package android.support.v4.g;

import android.os.Handler;
import android.os.Handler.Callback;
import android.os.HandlerThread;
import android.os.Message;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class c {

    /* renamed from: a reason: collision with root package name */
    private final Object f673a = new Object();

    /* renamed from: b reason: collision with root package name */
    private HandlerThread f674b;
    private Handler c;
    private int d;
    private Callback e = new Callback() {
        public boolean handleMessage(Message message) {
            switch (message.what) {
                case 0:
                    c.this.a();
                    break;
                case 1:
                    c.this.b((Runnable) message.obj);
                    break;
            }
            return true;
        }
    };
    private final int f;
    private final int g;
    private final String h;

    public interface a<T> {
        void a(T t);
    }

    public c(String str, int i, int i2) {
        this.h = str;
        this.g = i;
        this.f = i2;
        this.d = 0;
    }

    /* access modifiers changed from: private */
    public void a() {
        synchronized (this.f673a) {
            if (!this.c.hasMessages(1)) {
                this.f674b.quit();
                this.f674b = null;
                this.c = null;
            }
        }
    }

    private void a(Runnable runnable) {
        synchronized (this.f673a) {
            if (this.f674b == null) {
                this.f674b = new HandlerThread(this.h, this.g);
                this.f674b.start();
                this.c = new Handler(this.f674b.getLooper(), this.e);
                this.d++;
            }
            this.c.removeMessages(0);
            this.c.sendMessage(this.c.obtainMessage(1, runnable));
        }
    }

    /* access modifiers changed from: private */
    public void b(Runnable runnable) {
        runnable.run();
        synchronized (this.f673a) {
            this.c.removeMessages(0);
            this.c.sendMessageDelayed(this.c.obtainMessage(0), (long) this.f);
        }
    }

    public <T> T a(Callable<T> callable, int i) throws InterruptedException {
        T t;
        final ReentrantLock reentrantLock = new ReentrantLock();
        final Condition newCondition = reentrantLock.newCondition();
        final AtomicReference atomicReference = new AtomicReference();
        final AtomicBoolean atomicBoolean = new AtomicBoolean(true);
        final Callable<T> callable2 = callable;
        a((Runnable) new Runnable() {
            public void run() {
                try {
                    atomicReference.set(callable2.call());
                } catch (Exception e2) {
                }
                reentrantLock.lock();
                try {
                    atomicBoolean.set(false);
                    newCondition.signal();
                } finally {
                    reentrantLock.unlock();
                }
            }
        });
        reentrantLock.lock();
        try {
            if (!atomicBoolean.get()) {
                t = atomicReference.get();
            } else {
                long nanos = TimeUnit.MILLISECONDS.toNanos((long) i);
                do {
                    try {
                        nanos = newCondition.awaitNanos(nanos);
                    } catch (InterruptedException e2) {
                    }
                    if (!atomicBoolean.get()) {
                        t = atomicReference.get();
                        reentrantLock.unlock();
                    }
                } while (nanos > 0);
                throw new InterruptedException("timeout");
            }
            return t;
        } finally {
            reentrantLock.unlock();
        }
    }

    public <T> void a(final Callable<T> callable, final a<T> aVar) {
        final Handler handler = new Handler();
        a((Runnable) new Runnable() {
            public void run() {
                final Object obj;
                try {
                    obj = callable.call();
                } catch (Exception e) {
                    obj = null;
                }
                handler.post(new Runnable() {
                    public void run() {
                        aVar.a(obj);
                    }
                });
            }
        });
    }
}
