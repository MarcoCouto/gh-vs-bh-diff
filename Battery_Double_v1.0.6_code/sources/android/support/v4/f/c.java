package android.support.v4.f;

import android.os.Bundle;
import android.os.Handler;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class c implements Parcelable {
    public static final Creator<c> CREATOR = new Creator<c>() {
        /* renamed from: a */
        public c createFromParcel(Parcel parcel) {
            return new c(parcel);
        }

        /* renamed from: a */
        public c[] newArray(int i) {
            return new c[i];
        }
    };

    /* renamed from: a reason: collision with root package name */
    final boolean f653a = false;

    /* renamed from: b reason: collision with root package name */
    final Handler f654b = null;
    b c;

    class a extends android.support.v4.f.b.a {
        a() {
        }

        public void a(int i, Bundle bundle) {
            if (c.this.f654b != null) {
                c.this.f654b.post(new b(i, bundle));
            } else {
                c.this.a(i, bundle);
            }
        }
    }

    class b implements Runnable {

        /* renamed from: a reason: collision with root package name */
        final int f656a;

        /* renamed from: b reason: collision with root package name */
        final Bundle f657b;

        b(int i, Bundle bundle) {
            this.f656a = i;
            this.f657b = bundle;
        }

        public void run() {
            c.this.a(this.f656a, this.f657b);
        }
    }

    c(Parcel parcel) {
        this.c = android.support.v4.f.b.a.a(parcel.readStrongBinder());
    }

    /* access modifiers changed from: protected */
    public void a(int i, Bundle bundle) {
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        synchronized (this) {
            if (this.c == null) {
                this.c = new a();
            }
            parcel.writeStrongBinder(this.c.asBinder());
        }
    }
}
