package android.support.v4.i;

import android.os.Build.VERSION;
import android.support.a.a.C0005a;
import android.view.ViewGroup;

public final class v {

    /* renamed from: a reason: collision with root package name */
    static final c f754a;

    static class a extends c {
        a() {
        }
    }

    static class b extends a {
        b() {
        }

        public boolean a(ViewGroup viewGroup) {
            return viewGroup.isTransitionGroup();
        }
    }

    static class c {
        c() {
        }

        public boolean a(ViewGroup viewGroup) {
            Boolean bool = (Boolean) viewGroup.getTag(C0005a.tag_transition_group);
            return ((bool == null || !bool.booleanValue()) && viewGroup.getBackground() == null && t.l(viewGroup) == null) ? false : true;
        }
    }

    static {
        if (VERSION.SDK_INT >= 21) {
            f754a = new b();
        } else if (VERSION.SDK_INT >= 18) {
            f754a = new a();
        } else {
            f754a = new c();
        }
    }

    public static boolean a(ViewGroup viewGroup) {
        return f754a.a(viewGroup);
    }
}
