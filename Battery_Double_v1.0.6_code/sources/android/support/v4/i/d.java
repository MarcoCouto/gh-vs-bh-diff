package android.support.v4.i;

import android.content.Context;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.Message;
import android.view.GestureDetector;
import android.view.GestureDetector.OnDoubleTapListener;
import android.view.GestureDetector.OnGestureListener;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.ViewConfiguration;

public final class d {

    /* renamed from: a reason: collision with root package name */
    private final a f733a;

    interface a {
        boolean a(MotionEvent motionEvent);
    }

    static class b implements a {
        private static final int j = ViewConfiguration.getLongPressTimeout();
        private static final int k = ViewConfiguration.getTapTimeout();
        private static final int l = ViewConfiguration.getDoubleTapTimeout();

        /* renamed from: a reason: collision with root package name */
        final OnGestureListener f734a;

        /* renamed from: b reason: collision with root package name */
        OnDoubleTapListener f735b;
        boolean c;
        boolean d;
        MotionEvent e;
        private int f;
        private int g;
        private int h;
        private int i;
        private final Handler m;
        private boolean n;
        private boolean o;
        private boolean p;
        private MotionEvent q;
        private boolean r;
        private float s;
        private float t;
        private float u;
        private float v;
        private boolean w;
        private VelocityTracker x;

        private class a extends Handler {
            a() {
            }

            a(Handler handler) {
                super(handler.getLooper());
            }

            public void handleMessage(Message message) {
                switch (message.what) {
                    case 1:
                        b.this.f734a.onShowPress(b.this.e);
                        return;
                    case 2:
                        b.this.a();
                        return;
                    case 3:
                        if (b.this.f735b == null) {
                            return;
                        }
                        if (!b.this.c) {
                            b.this.f735b.onSingleTapConfirmed(b.this.e);
                            return;
                        } else {
                            b.this.d = true;
                            return;
                        }
                    default:
                        throw new RuntimeException("Unknown message " + message);
                }
            }
        }

        public b(Context context, OnGestureListener onGestureListener, Handler handler) {
            if (handler != null) {
                this.m = new a(handler);
            } else {
                this.m = new a();
            }
            this.f734a = onGestureListener;
            if (onGestureListener instanceof OnDoubleTapListener) {
                a((OnDoubleTapListener) onGestureListener);
            }
            a(context);
        }

        private void a(Context context) {
            if (context == null) {
                throw new IllegalArgumentException("Context must not be null");
            } else if (this.f734a == null) {
                throw new IllegalArgumentException("OnGestureListener must not be null");
            } else {
                this.w = true;
                ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
                int scaledTouchSlop = viewConfiguration.getScaledTouchSlop();
                int scaledDoubleTapSlop = viewConfiguration.getScaledDoubleTapSlop();
                this.h = viewConfiguration.getScaledMinimumFlingVelocity();
                this.i = viewConfiguration.getScaledMaximumFlingVelocity();
                this.f = scaledTouchSlop * scaledTouchSlop;
                this.g = scaledDoubleTapSlop * scaledDoubleTapSlop;
            }
        }

        private boolean a(MotionEvent motionEvent, MotionEvent motionEvent2, MotionEvent motionEvent3) {
            if (!this.p || motionEvent3.getEventTime() - motionEvent2.getEventTime() > ((long) l)) {
                return false;
            }
            int x2 = ((int) motionEvent.getX()) - ((int) motionEvent3.getX());
            int y = ((int) motionEvent.getY()) - ((int) motionEvent3.getY());
            return (x2 * x2) + (y * y) < this.g;
        }

        private void b() {
            this.m.removeMessages(1);
            this.m.removeMessages(2);
            this.m.removeMessages(3);
            this.x.recycle();
            this.x = null;
            this.r = false;
            this.c = false;
            this.o = false;
            this.p = false;
            this.d = false;
            if (this.n) {
                this.n = false;
            }
        }

        private void c() {
            this.m.removeMessages(1);
            this.m.removeMessages(2);
            this.m.removeMessages(3);
            this.r = false;
            this.o = false;
            this.p = false;
            this.d = false;
            if (this.n) {
                this.n = false;
            }
        }

        /* access modifiers changed from: 0000 */
        public void a() {
            this.m.removeMessages(3);
            this.d = false;
            this.n = true;
            this.f734a.onLongPress(this.e);
        }

        public void a(OnDoubleTapListener onDoubleTapListener) {
            this.f735b = onDoubleTapListener;
        }

        /* JADX WARNING: Removed duplicated region for block: B:43:0x00eb  */
        /* JADX WARNING: Removed duplicated region for block: B:46:0x0104  */
        public boolean a(MotionEvent motionEvent) {
            boolean onFling;
            boolean z;
            boolean z2;
            int action = motionEvent.getAction();
            if (this.x == null) {
                this.x = VelocityTracker.obtain();
            }
            this.x.addMovement(motionEvent);
            boolean z3 = (action & 255) == 6;
            int i2 = z3 ? motionEvent.getActionIndex() : -1;
            int pointerCount = motionEvent.getPointerCount();
            float f2 = 0.0f;
            float f3 = 0.0f;
            for (int i3 = 0; i3 < pointerCount; i3++) {
                if (i2 != i3) {
                    f3 += motionEvent.getX(i3);
                    f2 += motionEvent.getY(i3);
                }
            }
            int i4 = z3 ? pointerCount - 1 : pointerCount;
            float f4 = f3 / ((float) i4);
            float f5 = f2 / ((float) i4);
            switch (action & 255) {
                case 0:
                    if (this.f735b != null) {
                        boolean hasMessages = this.m.hasMessages(3);
                        if (hasMessages) {
                            this.m.removeMessages(3);
                        }
                        if (this.e == null || this.q == null || !hasMessages || !a(this.e, this.q, motionEvent)) {
                            this.m.sendEmptyMessageDelayed(3, (long) l);
                        } else {
                            this.r = true;
                            z2 = this.f735b.onDoubleTap(this.e) | false | this.f735b.onDoubleTapEvent(motionEvent);
                            this.s = f4;
                            this.u = f4;
                            this.t = f5;
                            this.v = f5;
                            if (this.e != null) {
                                this.e.recycle();
                            }
                            this.e = MotionEvent.obtain(motionEvent);
                            this.o = true;
                            this.p = true;
                            this.c = true;
                            this.n = false;
                            this.d = false;
                            if (this.w) {
                                this.m.removeMessages(2);
                                this.m.sendEmptyMessageAtTime(2, this.e.getDownTime() + ((long) k) + ((long) j));
                            }
                            this.m.sendEmptyMessageAtTime(1, this.e.getDownTime() + ((long) k));
                            return z2 | this.f734a.onDown(motionEvent);
                        }
                    }
                    z2 = false;
                    this.s = f4;
                    this.u = f4;
                    this.t = f5;
                    this.v = f5;
                    if (this.e != null) {
                    }
                    this.e = MotionEvent.obtain(motionEvent);
                    this.o = true;
                    this.p = true;
                    this.c = true;
                    this.n = false;
                    this.d = false;
                    if (this.w) {
                    }
                    this.m.sendEmptyMessageAtTime(1, this.e.getDownTime() + ((long) k));
                    return z2 | this.f734a.onDown(motionEvent);
                case 1:
                    this.c = false;
                    MotionEvent obtain = MotionEvent.obtain(motionEvent);
                    if (this.r) {
                        onFling = this.f735b.onDoubleTapEvent(motionEvent) | false;
                    } else if (this.n) {
                        this.m.removeMessages(3);
                        this.n = false;
                        onFling = false;
                    } else if (this.o) {
                        onFling = this.f734a.onSingleTapUp(motionEvent);
                        if (this.d && this.f735b != null) {
                            this.f735b.onSingleTapConfirmed(motionEvent);
                        }
                    } else {
                        VelocityTracker velocityTracker = this.x;
                        int pointerId = motionEvent.getPointerId(0);
                        velocityTracker.computeCurrentVelocity(1000, (float) this.i);
                        float yVelocity = velocityTracker.getYVelocity(pointerId);
                        float xVelocity = velocityTracker.getXVelocity(pointerId);
                        onFling = (Math.abs(yVelocity) > ((float) this.h) || Math.abs(xVelocity) > ((float) this.h)) ? this.f734a.onFling(this.e, motionEvent, xVelocity, yVelocity) : false;
                    }
                    if (this.q != null) {
                        this.q.recycle();
                    }
                    this.q = obtain;
                    if (this.x != null) {
                        this.x.recycle();
                        this.x = null;
                    }
                    this.r = false;
                    this.d = false;
                    this.m.removeMessages(1);
                    this.m.removeMessages(2);
                    return onFling;
                case 2:
                    if (this.n) {
                        return false;
                    }
                    float f6 = this.s - f4;
                    float f7 = this.t - f5;
                    if (this.r) {
                        return false | this.f735b.onDoubleTapEvent(motionEvent);
                    }
                    if (this.o) {
                        int i5 = (int) (f4 - this.u);
                        int i6 = (int) (f5 - this.v);
                        int i7 = (i5 * i5) + (i6 * i6);
                        if (i7 > this.f) {
                            z = this.f734a.onScroll(this.e, motionEvent, f6, f7);
                            this.s = f4;
                            this.t = f5;
                            this.o = false;
                            this.m.removeMessages(3);
                            this.m.removeMessages(1);
                            this.m.removeMessages(2);
                        } else {
                            z = false;
                        }
                        if (i7 > this.f) {
                            this.p = false;
                        }
                        return z;
                    } else if (Math.abs(f6) < 1.0f && Math.abs(f7) < 1.0f) {
                        return false;
                    } else {
                        boolean onScroll = this.f734a.onScroll(this.e, motionEvent, f6, f7);
                        this.s = f4;
                        this.t = f5;
                        return onScroll;
                    }
                case 3:
                    b();
                    return false;
                case 5:
                    this.s = f4;
                    this.u = f4;
                    this.t = f5;
                    this.v = f5;
                    c();
                    return false;
                case 6:
                    this.s = f4;
                    this.u = f4;
                    this.t = f5;
                    this.v = f5;
                    this.x.computeCurrentVelocity(1000, (float) this.i);
                    int actionIndex = motionEvent.getActionIndex();
                    int pointerId2 = motionEvent.getPointerId(actionIndex);
                    float xVelocity2 = this.x.getXVelocity(pointerId2);
                    float yVelocity2 = this.x.getYVelocity(pointerId2);
                    for (int i8 = 0; i8 < pointerCount; i8++) {
                        if (i8 != actionIndex) {
                            int pointerId3 = motionEvent.getPointerId(i8);
                            if ((this.x.getYVelocity(pointerId3) * yVelocity2) + (this.x.getXVelocity(pointerId3) * xVelocity2) < 0.0f) {
                                this.x.clear();
                                return false;
                            }
                        }
                    }
                    return false;
                default:
                    return false;
            }
        }
    }

    static class c implements a {

        /* renamed from: a reason: collision with root package name */
        private final GestureDetector f737a;

        public c(Context context, OnGestureListener onGestureListener, Handler handler) {
            this.f737a = new GestureDetector(context, onGestureListener, handler);
        }

        public boolean a(MotionEvent motionEvent) {
            return this.f737a.onTouchEvent(motionEvent);
        }
    }

    public d(Context context, OnGestureListener onGestureListener) {
        this(context, onGestureListener, null);
    }

    public d(Context context, OnGestureListener onGestureListener, Handler handler) {
        if (VERSION.SDK_INT > 17) {
            this.f733a = new c(context, onGestureListener, handler);
        } else {
            this.f733a = new b(context, onGestureListener, handler);
        }
    }

    public boolean a(MotionEvent motionEvent) {
        return this.f733a.a(motionEvent);
    }
}
