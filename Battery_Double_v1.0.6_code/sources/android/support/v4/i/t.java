package android.support.v4.i;

import android.animation.ValueAnimator;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff.Mode;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.view.Display;
import android.view.PointerIcon;
import android.view.View;
import android.view.View.OnApplyWindowInsetsListener;
import android.view.ViewParent;
import android.view.WindowInsets;
import android.view.WindowManager;
import java.lang.reflect.Field;
import java.util.WeakHashMap;

public class t {

    /* renamed from: a reason: collision with root package name */
    static final j f748a;

    static class a extends j {
        a() {
        }

        public boolean a(View view) {
            return view.hasOnClickListeners();
        }
    }

    static class b extends a {
        b() {
        }

        public void a(View view, int i) {
            if (i == 4) {
                i = 2;
            }
            view.setImportantForAccessibility(i);
        }

        public void a(View view, Drawable drawable) {
            view.setBackground(drawable);
        }

        public void a(View view, Runnable runnable) {
            view.postOnAnimation(runnable);
        }

        public void a(View view, Runnable runnable, long j) {
            view.postOnAnimationDelayed(runnable, j);
        }

        public void a(View view, boolean z) {
            view.setHasTransientState(z);
        }

        public boolean b(View view) {
            return view.hasTransientState();
        }

        public void c(View view) {
            view.postInvalidateOnAnimation();
        }

        public int d(View view) {
            return view.getImportantForAccessibility();
        }

        public int e(View view) {
            return view.getMinimumWidth();
        }

        public int f(View view) {
            return view.getMinimumHeight();
        }

        public void g(View view) {
            view.requestFitSystemWindows();
        }

        public boolean h(View view) {
            return view.getFitsSystemWindows();
        }

        public boolean i(View view) {
            return view.hasOverlappingRendering();
        }
    }

    static class c extends b {
        c() {
        }

        public void a(View view, int i, int i2, int i3, int i4) {
            view.setPaddingRelative(i, i2, i3, i4);
        }

        public int j(View view) {
            return view.getLayoutDirection();
        }

        public int k(View view) {
            return view.getPaddingStart();
        }

        public int l(View view) {
            return view.getPaddingEnd();
        }

        public int m(View view) {
            return view.getWindowSystemUiVisibility();
        }

        public boolean n(View view) {
            return view.isPaddingRelative();
        }

        public Display o(View view) {
            return view.getDisplay();
        }
    }

    static class d extends c {
        d() {
        }

        public void a(View view, Rect rect) {
            view.setClipBounds(rect);
        }

        public Rect p(View view) {
            return view.getClipBounds();
        }
    }

    static class e extends d {
        e() {
        }

        public void a(View view, int i) {
            view.setImportantForAccessibility(i);
        }

        public void b(View view, int i) {
            view.setAccessibilityLiveRegion(i);
        }

        public boolean q(View view) {
            return view.isLaidOut();
        }

        public boolean r(View view) {
            return view.isAttachedToWindow();
        }
    }

    static class f extends e {
        private static ThreadLocal<Rect> d;

        f() {
        }

        private static Rect b() {
            if (d == null) {
                d = new ThreadLocal<>();
            }
            Rect rect = (Rect) d.get();
            if (rect == null) {
                rect = new Rect();
                d.set(rect);
            }
            rect.setEmpty();
            return rect;
        }

        public ac a(View view, ac acVar) {
            WindowInsets windowInsets = (WindowInsets) ac.a(acVar);
            WindowInsets onApplyWindowInsets = view.onApplyWindowInsets(windowInsets);
            if (onApplyWindowInsets != windowInsets) {
                windowInsets = new WindowInsets(onApplyWindowInsets);
            }
            return ac.a((Object) windowInsets);
        }

        public void a(View view, float f) {
            view.setElevation(f);
        }

        public void a(View view, ColorStateList colorStateList) {
            view.setBackgroundTintList(colorStateList);
            if (VERSION.SDK_INT == 21) {
                Drawable background = view.getBackground();
                boolean z = (view.getBackgroundTintList() == null || view.getBackgroundTintMode() == null) ? false : true;
                if (background != null && z) {
                    if (background.isStateful()) {
                        background.setState(view.getDrawableState());
                    }
                    view.setBackground(background);
                }
            }
        }

        public void a(View view, Mode mode) {
            view.setBackgroundTintMode(mode);
            if (VERSION.SDK_INT == 21) {
                Drawable background = view.getBackground();
                boolean z = (view.getBackgroundTintList() == null || view.getBackgroundTintMode() == null) ? false : true;
                if (background != null && z) {
                    if (background.isStateful()) {
                        background.setState(view.getDrawableState());
                    }
                    view.setBackground(background);
                }
            }
        }

        public void a(View view, final p pVar) {
            if (pVar == null) {
                view.setOnApplyWindowInsetsListener(null);
            } else {
                view.setOnApplyWindowInsetsListener(new OnApplyWindowInsetsListener() {
                    public WindowInsets onApplyWindowInsets(View view, WindowInsets windowInsets) {
                        return (WindowInsets) ac.a(pVar.a(view, ac.a((Object) windowInsets)));
                    }
                });
            }
        }

        public void a(View view, String str) {
            view.setTransitionName(str);
        }

        public ac b(View view, ac acVar) {
            WindowInsets windowInsets = (WindowInsets) ac.a(acVar);
            WindowInsets dispatchApplyWindowInsets = view.dispatchApplyWindowInsets(windowInsets);
            if (dispatchApplyWindowInsets != windowInsets) {
                windowInsets = new WindowInsets(dispatchApplyWindowInsets);
            }
            return ac.a((Object) windowInsets);
        }

        public void c(View view, int i) {
            boolean z;
            Rect b2 = b();
            ViewParent parent = view.getParent();
            if (parent instanceof View) {
                View view2 = (View) parent;
                b2.set(view2.getLeft(), view2.getTop(), view2.getRight(), view2.getBottom());
                z = !b2.intersects(view.getLeft(), view.getTop(), view.getRight(), view.getBottom());
            } else {
                z = false;
            }
            super.c(view, i);
            if (z && b2.intersect(view.getLeft(), view.getTop(), view.getRight(), view.getBottom())) {
                ((View) parent).invalidate(b2);
            }
        }

        public void d(View view, int i) {
            boolean z;
            Rect b2 = b();
            ViewParent parent = view.getParent();
            if (parent instanceof View) {
                View view2 = (View) parent;
                b2.set(view2.getLeft(), view2.getTop(), view2.getRight(), view2.getBottom());
                z = !b2.intersects(view.getLeft(), view.getTop(), view.getRight(), view.getBottom());
            } else {
                z = false;
            }
            super.d(view, i);
            if (z && b2.intersect(view.getLeft(), view.getTop(), view.getRight(), view.getBottom())) {
                ((View) parent).invalidate(b2);
            }
        }

        public void g(View view) {
            view.requestApplyInsets();
        }

        public String s(View view) {
            return view.getTransitionName();
        }

        public float t(View view) {
            return view.getElevation();
        }

        public float u(View view) {
            return view.getTranslationZ();
        }

        public boolean v(View view) {
            return view.isNestedScrollingEnabled();
        }

        public void w(View view) {
            view.stopNestedScroll();
        }

        public ColorStateList x(View view) {
            return view.getBackgroundTintList();
        }

        public Mode y(View view) {
            return view.getBackgroundTintMode();
        }

        public float z(View view) {
            return view.getZ();
        }
    }

    static class g extends f {
        g() {
        }

        public void a(View view, int i, int i2) {
            view.setScrollIndicators(i, i2);
        }

        public void c(View view, int i) {
            view.offsetLeftAndRight(i);
        }

        public void d(View view, int i) {
            view.offsetTopAndBottom(i);
        }
    }

    static class h extends g {
        h() {
        }

        public void a(View view, r rVar) {
            view.setPointerIcon((PointerIcon) (rVar != null ? rVar.a() : null));
        }
    }

    static class i extends h {
        i() {
        }
    }

    static class j {

        /* renamed from: b reason: collision with root package name */
        static Field f751b;
        static boolean c = false;
        private static Field d;
        private static boolean e;
        private static Field f;
        private static boolean g;
        private static WeakHashMap<View, String> h;

        /* renamed from: a reason: collision with root package name */
        WeakHashMap<View, y> f752a = null;

        j() {
        }

        private static void C(View view) {
            float translationY = view.getTranslationY();
            view.setTranslationY(1.0f + translationY);
            view.setTranslationY(translationY);
        }

        public boolean A(View view) {
            boolean z = true;
            if (c) {
                return false;
            }
            if (f751b == null) {
                try {
                    f751b = View.class.getDeclaredField("mAccessibilityDelegate");
                    f751b.setAccessible(true);
                } catch (Throwable th) {
                    c = true;
                    return false;
                }
            }
            try {
                if (f751b.get(view) == null) {
                    z = false;
                }
                return z;
            } catch (Throwable th2) {
                c = true;
                return false;
            }
        }

        public y B(View view) {
            if (this.f752a == null) {
                this.f752a = new WeakHashMap<>();
            }
            y yVar = (y) this.f752a.get(view);
            if (yVar != null) {
                return yVar;
            }
            y yVar2 = new y(view);
            this.f752a.put(view, yVar2);
            return yVar2;
        }

        /* access modifiers changed from: 0000 */
        public long a() {
            return ValueAnimator.getFrameDelay();
        }

        public ac a(View view, ac acVar) {
            return acVar;
        }

        public void a(View view, float f2) {
        }

        public void a(View view, int i) {
        }

        public void a(View view, int i, int i2) {
        }

        public void a(View view, int i, int i2, int i3, int i4) {
            view.setPadding(i, i2, i3, i4);
        }

        public void a(View view, ColorStateList colorStateList) {
            if (view instanceof s) {
                ((s) view).setSupportBackgroundTintList(colorStateList);
            }
        }

        public void a(View view, Mode mode) {
            if (view instanceof s) {
                ((s) view).setSupportBackgroundTintMode(mode);
            }
        }

        public void a(View view, Rect rect) {
        }

        public void a(View view, Drawable drawable) {
            view.setBackgroundDrawable(drawable);
        }

        public void a(View view, b bVar) {
            view.setAccessibilityDelegate(bVar == null ? null : bVar.a());
        }

        public void a(View view, p pVar) {
        }

        public void a(View view, r rVar) {
        }

        public void a(View view, Runnable runnable) {
            view.postDelayed(runnable, a());
        }

        public void a(View view, Runnable runnable, long j) {
            view.postDelayed(runnable, a() + j);
        }

        public void a(View view, String str) {
            if (h == null) {
                h = new WeakHashMap<>();
            }
            h.put(view, str);
        }

        public void a(View view, boolean z) {
        }

        public boolean a(View view) {
            return false;
        }

        public ac b(View view, ac acVar) {
            return acVar;
        }

        public void b(View view, int i) {
        }

        public boolean b(View view) {
            return false;
        }

        public void c(View view) {
            view.postInvalidate();
        }

        public void c(View view, int i) {
            view.offsetLeftAndRight(i);
            if (view.getVisibility() == 0) {
                C(view);
                ViewParent parent = view.getParent();
                if (parent instanceof View) {
                    C((View) parent);
                }
            }
        }

        public int d(View view) {
            return 0;
        }

        public void d(View view, int i) {
            view.offsetTopAndBottom(i);
            if (view.getVisibility() == 0) {
                C(view);
                ViewParent parent = view.getParent();
                if (parent instanceof View) {
                    C((View) parent);
                }
            }
        }

        public int e(View view) {
            if (!e) {
                try {
                    d = View.class.getDeclaredField("mMinWidth");
                    d.setAccessible(true);
                } catch (NoSuchFieldException e2) {
                }
                e = true;
            }
            if (d != null) {
                try {
                    return ((Integer) d.get(view)).intValue();
                } catch (Exception e3) {
                }
            }
            return 0;
        }

        public int f(View view) {
            if (!g) {
                try {
                    f = View.class.getDeclaredField("mMinHeight");
                    f.setAccessible(true);
                } catch (NoSuchFieldException e2) {
                }
                g = true;
            }
            if (f != null) {
                try {
                    return ((Integer) f.get(view)).intValue();
                } catch (Exception e3) {
                }
            }
            return 0;
        }

        public void g(View view) {
        }

        public boolean h(View view) {
            return false;
        }

        public boolean i(View view) {
            return true;
        }

        public int j(View view) {
            return 0;
        }

        public int k(View view) {
            return view.getPaddingLeft();
        }

        public int l(View view) {
            return view.getPaddingRight();
        }

        public int m(View view) {
            return 0;
        }

        public boolean n(View view) {
            return false;
        }

        public Display o(View view) {
            if (r(view)) {
                return ((WindowManager) view.getContext().getSystemService("window")).getDefaultDisplay();
            }
            return null;
        }

        public Rect p(View view) {
            return null;
        }

        public boolean q(View view) {
            return view.getWidth() > 0 && view.getHeight() > 0;
        }

        public boolean r(View view) {
            return view.getWindowToken() != null;
        }

        public String s(View view) {
            if (h == null) {
                return null;
            }
            return (String) h.get(view);
        }

        public float t(View view) {
            return 0.0f;
        }

        public float u(View view) {
            return 0.0f;
        }

        public boolean v(View view) {
            if (view instanceof j) {
                return ((j) view).isNestedScrollingEnabled();
            }
            return false;
        }

        public void w(View view) {
            if (view instanceof j) {
                ((j) view).stopNestedScroll();
            }
        }

        public ColorStateList x(View view) {
            if (view instanceof s) {
                return ((s) view).getSupportBackgroundTintList();
            }
            return null;
        }

        public Mode y(View view) {
            if (view instanceof s) {
                return ((s) view).getSupportBackgroundTintMode();
            }
            return null;
        }

        public float z(View view) {
            return u(view) + t(view);
        }
    }

    static {
        if (VERSION.SDK_INT >= 26) {
            f748a = new i();
        } else if (VERSION.SDK_INT >= 24) {
            f748a = new h();
        } else if (VERSION.SDK_INT >= 23) {
            f748a = new g();
        } else if (VERSION.SDK_INT >= 21) {
            f748a = new f();
        } else if (VERSION.SDK_INT >= 19) {
            f748a = new e();
        } else if (VERSION.SDK_INT >= 18) {
            f748a = new d();
        } else if (VERSION.SDK_INT >= 17) {
            f748a = new c();
        } else if (VERSION.SDK_INT >= 16) {
            f748a = new b();
        } else if (VERSION.SDK_INT >= 15) {
            f748a = new a();
        } else {
            f748a = new j();
        }
    }

    public static Display A(View view) {
        return f748a.o(view);
    }

    public static ac a(View view, ac acVar) {
        return f748a.a(view, acVar);
    }

    public static void a(View view, float f2) {
        f748a.a(view, f2);
    }

    public static void a(View view, int i2) {
        f748a.a(view, i2);
    }

    public static void a(View view, int i2, int i3) {
        f748a.a(view, i2, i3);
    }

    public static void a(View view, int i2, int i3, int i4, int i5) {
        f748a.a(view, i2, i3, i4, i5);
    }

    public static void a(View view, ColorStateList colorStateList) {
        f748a.a(view, colorStateList);
    }

    public static void a(View view, Mode mode) {
        f748a.a(view, mode);
    }

    public static void a(View view, Rect rect) {
        f748a.a(view, rect);
    }

    public static void a(View view, Drawable drawable) {
        f748a.a(view, drawable);
    }

    public static void a(View view, b bVar) {
        f748a.a(view, bVar);
    }

    public static void a(View view, p pVar) {
        f748a.a(view, pVar);
    }

    public static void a(View view, r rVar) {
        f748a.a(view, rVar);
    }

    public static void a(View view, Runnable runnable) {
        f748a.a(view, runnable);
    }

    public static void a(View view, Runnable runnable, long j2) {
        f748a.a(view, runnable, j2);
    }

    public static void a(View view, String str) {
        f748a.a(view, str);
    }

    public static void a(View view, boolean z) {
        f748a.a(view, z);
    }

    public static boolean a(View view) {
        return f748a.A(view);
    }

    public static ac b(View view, ac acVar) {
        return f748a.b(view, acVar);
    }

    public static void b(View view, int i2) {
        f748a.b(view, i2);
    }

    @Deprecated
    public static void b(View view, boolean z) {
        view.setFitsSystemWindows(z);
    }

    public static boolean b(View view) {
        return f748a.b(view);
    }

    public static void c(View view) {
        f748a.c(view);
    }

    public static void c(View view, int i2) {
        f748a.d(view, i2);
    }

    public static int d(View view) {
        return f748a.d(view);
    }

    public static void d(View view, int i2) {
        f748a.c(view, i2);
    }

    public static int e(View view) {
        return f748a.j(view);
    }

    public static int f(View view) {
        return f748a.k(view);
    }

    public static int g(View view) {
        return f748a.l(view);
    }

    public static int h(View view) {
        return f748a.e(view);
    }

    public static int i(View view) {
        return f748a.f(view);
    }

    public static y j(View view) {
        return f748a.B(view);
    }

    public static float k(View view) {
        return f748a.t(view);
    }

    public static String l(View view) {
        return f748a.s(view);
    }

    public static int m(View view) {
        return f748a.m(view);
    }

    public static void n(View view) {
        f748a.g(view);
    }

    public static boolean o(View view) {
        return f748a.h(view);
    }

    public static boolean p(View view) {
        return f748a.i(view);
    }

    public static boolean q(View view) {
        return f748a.n(view);
    }

    public static ColorStateList r(View view) {
        return f748a.x(view);
    }

    public static Mode s(View view) {
        return f748a.y(view);
    }

    public static boolean t(View view) {
        return f748a.v(view);
    }

    public static void u(View view) {
        f748a.w(view);
    }

    public static boolean v(View view) {
        return f748a.q(view);
    }

    public static float w(View view) {
        return f748a.z(view);
    }

    public static Rect x(View view) {
        return f748a.p(view);
    }

    public static boolean y(View view) {
        return f748a.r(view);
    }

    public static boolean z(View view) {
        return f748a.a(view);
    }
}
