package android.support.v4.i;

import android.os.Build.VERSION;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.LayoutInflater.Factory;
import android.view.LayoutInflater.Factory2;
import java.lang.reflect.Field;

public final class f {

    /* renamed from: a reason: collision with root package name */
    static final b f738a;

    /* renamed from: b reason: collision with root package name */
    private static Field f739b;
    private static boolean c;

    static class a extends b {
        a() {
        }

        public void a(LayoutInflater layoutInflater, Factory2 factory2) {
            layoutInflater.setFactory2(factory2);
        }
    }

    static class b {
        b() {
        }

        public void a(LayoutInflater layoutInflater, Factory2 factory2) {
            layoutInflater.setFactory2(factory2);
            Factory factory = layoutInflater.getFactory();
            if (factory instanceof Factory2) {
                f.a(layoutInflater, (Factory2) factory);
            } else {
                f.a(layoutInflater, factory2);
            }
        }
    }

    static {
        if (VERSION.SDK_INT >= 21) {
            f738a = new a();
        } else {
            f738a = new b();
        }
    }

    static void a(LayoutInflater layoutInflater, Factory2 factory2) {
        if (!c) {
            try {
                f739b = LayoutInflater.class.getDeclaredField("mFactory2");
                f739b.setAccessible(true);
            } catch (NoSuchFieldException e) {
                Log.e("LayoutInflaterCompatHC", "forceSetFactory2 Could not find field 'mFactory2' on class " + LayoutInflater.class.getName() + "; inflation may have unexpected results.", e);
            }
            c = true;
        }
        if (f739b != null) {
            try {
                f739b.set(layoutInflater, factory2);
            } catch (IllegalAccessException e2) {
                Log.e("LayoutInflaterCompatHC", "forceSetFactory2 could not set the Factory2 on LayoutInflater " + layoutInflater + "; inflation may have unexpected results.", e2);
            }
        }
    }

    public static void b(LayoutInflater layoutInflater, Factory2 factory2) {
        f738a.a(layoutInflater, factory2);
    }
}
