package android.support.v4.i;

import android.content.Context;
import android.util.Log;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;

public abstract class c {

    /* renamed from: a reason: collision with root package name */
    private final Context f731a;

    /* renamed from: b reason: collision with root package name */
    private a f732b;
    private b c;

    public interface a {
        void b(boolean z);
    }

    public interface b {
        void a(boolean z);
    }

    public c(Context context) {
        this.f731a = context;
    }

    public abstract View a();

    public View a(MenuItem menuItem) {
        return a();
    }

    public void a(a aVar) {
        this.f732b = aVar;
    }

    public void a(b bVar) {
        if (!(this.c == null || bVar == null)) {
            Log.w("ActionProvider(support)", "setVisibilityListener: Setting a new ActionProvider.VisibilityListener when one is already set. Are you reusing this " + getClass().getSimpleName() + " instance while it is still in use somewhere else?");
        }
        this.c = bVar;
    }

    public void a(SubMenu subMenu) {
    }

    public void a(boolean z) {
        if (this.f732b != null) {
            this.f732b.b(z);
        }
    }

    public boolean b() {
        return false;
    }

    public boolean c() {
        return true;
    }

    public boolean d() {
        return false;
    }

    public boolean e() {
        return false;
    }

    public void f() {
        this.c = null;
        this.f732b = null;
    }
}
