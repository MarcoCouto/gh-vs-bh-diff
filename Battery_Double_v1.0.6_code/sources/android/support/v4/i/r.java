package android.support.v4.i;

import android.content.Context;
import android.os.Build.VERSION;
import android.view.PointerIcon;

public final class r {

    /* renamed from: a reason: collision with root package name */
    private Object f747a;

    private r(Object obj) {
        this.f747a = obj;
    }

    public static r a(Context context, int i) {
        return VERSION.SDK_INT >= 24 ? new r(PointerIcon.getSystemIcon(context, i)) : new r(null);
    }

    public Object a() {
        return this.f747a;
    }
}
