package android.support.v4.i;

import android.content.Context;
import android.content.res.Resources.NotFoundException;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.ClassLoaderCreator;
import android.os.Parcelable.Creator;
import android.util.AttributeSet;
import android.util.Log;
import android.view.FocusFinder;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SoundEffectConstants;
import android.view.VelocityTracker;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.animation.Interpolator;
import android.widget.EdgeEffect;
import android.widget.Scroller;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class w extends ViewGroup {

    /* renamed from: a reason: collision with root package name */
    static final int[] f755a = {16842931};
    private static final j ai = new j();
    private static final Comparator<b> e = new Comparator<b>() {
        /* renamed from: a */
        public int compare(b bVar, b bVar2) {
            return bVar.f761b - bVar2.f761b;
        }
    };
    private static final Interpolator f = new Interpolator() {
        public float getInterpolation(float f) {
            float f2 = f - 1.0f;
            return (f2 * f2 * f2 * f2 * f2) + 1.0f;
        }
    };
    private int A = 1;
    private boolean B;
    private boolean C;
    private int D;
    private int E;
    private int F;
    private float G;
    private float H;
    private float I;
    private float J;
    private int K = -1;
    private VelocityTracker L;
    private int M;
    private int N;
    private int O;
    private int P;
    private boolean Q;
    private EdgeEffect R;
    private EdgeEffect S;
    private boolean T = true;
    private boolean U = false;
    private boolean V;
    private int W;
    private List<f> aa;
    private f ab;
    private f ac;
    private List<e> ad;
    private g ae;
    private int af;
    private int ag;
    private ArrayList<View> ah;
    private final Runnable aj = new Runnable() {
        public void run() {
            w.this.setScrollState(0);
            w.this.c();
        }
    };
    private int ak = 0;

    /* renamed from: b reason: collision with root package name */
    q f756b;
    int c;
    private int d;
    private final ArrayList<b> g = new ArrayList<>();
    private final b h = new b();
    private final Rect i = new Rect();
    private int j = -1;
    private Parcelable k = null;
    private ClassLoader l = null;
    private Scroller m;
    private boolean n;
    private h o;
    private int p;
    private Drawable q;
    private int r;
    private int s;
    private float t = -3.4028235E38f;
    private float u = Float.MAX_VALUE;
    private int v;
    private int w;
    private boolean x;
    private boolean y;
    private boolean z;

    @Inherited
    @Target({ElementType.TYPE})
    @Retention(RetentionPolicy.RUNTIME)
    public @interface a {
    }

    static class b {

        /* renamed from: a reason: collision with root package name */
        Object f760a;

        /* renamed from: b reason: collision with root package name */
        int f761b;
        boolean c;
        float d;
        float e;

        b() {
        }
    }

    public static class c extends LayoutParams {

        /* renamed from: a reason: collision with root package name */
        public boolean f762a;

        /* renamed from: b reason: collision with root package name */
        public int f763b;
        float c = 0.0f;
        boolean d;
        int e;
        int f;

        public c() {
            super(-1, -1);
        }

        public c(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, w.f755a);
            this.f763b = obtainStyledAttributes.getInteger(0, 48);
            obtainStyledAttributes.recycle();
        }
    }

    class d extends b {
        d() {
        }

        private boolean b() {
            return w.this.f756b != null && w.this.f756b.b() > 1;
        }

        public void a(View view, android.support.v4.i.a.b bVar) {
            super.a(view, bVar);
            bVar.a((CharSequence) w.class.getName());
            bVar.c(b());
            if (w.this.canScrollHorizontally(1)) {
                bVar.a(4096);
            }
            if (w.this.canScrollHorizontally(-1)) {
                bVar.a(8192);
            }
        }

        public void a(View view, AccessibilityEvent accessibilityEvent) {
            super.a(view, accessibilityEvent);
            accessibilityEvent.setClassName(w.class.getName());
            accessibilityEvent.setScrollable(b());
            if (accessibilityEvent.getEventType() == 4096 && w.this.f756b != null) {
                accessibilityEvent.setItemCount(w.this.f756b.b());
                accessibilityEvent.setFromIndex(w.this.c);
                accessibilityEvent.setToIndex(w.this.c);
            }
        }

        public boolean a(View view, int i, Bundle bundle) {
            if (super.a(view, i, bundle)) {
                return true;
            }
            switch (i) {
                case 4096:
                    if (!w.this.canScrollHorizontally(1)) {
                        return false;
                    }
                    w.this.setCurrentItem(w.this.c + 1);
                    return true;
                case 8192:
                    if (!w.this.canScrollHorizontally(-1)) {
                        return false;
                    }
                    w.this.setCurrentItem(w.this.c - 1);
                    return true;
                default:
                    return false;
            }
        }
    }

    public interface e {
        void a(w wVar, q qVar, q qVar2);
    }

    public interface f {
        void a(int i);

        void a(int i, float f, int i2);

        void b(int i);
    }

    public interface g {
        void a(View view, float f);
    }

    private class h extends DataSetObserver {
        h() {
        }

        public void onChanged() {
            w.this.b();
        }

        public void onInvalidated() {
            w.this.b();
        }
    }

    public static class i extends a {
        public static final Creator<i> CREATOR = new ClassLoaderCreator<i>() {
            /* renamed from: a */
            public i createFromParcel(Parcel parcel) {
                return new i(parcel, null);
            }

            /* renamed from: a */
            public i createFromParcel(Parcel parcel, ClassLoader classLoader) {
                return new i(parcel, classLoader);
            }

            /* renamed from: a */
            public i[] newArray(int i) {
                return new i[i];
            }
        };

        /* renamed from: a reason: collision with root package name */
        int f766a;

        /* renamed from: b reason: collision with root package name */
        Parcelable f767b;
        ClassLoader c;

        i(Parcel parcel, ClassLoader classLoader) {
            super(parcel, classLoader);
            if (classLoader == null) {
                classLoader = getClass().getClassLoader();
            }
            this.f766a = parcel.readInt();
            this.f767b = parcel.readParcelable(classLoader);
            this.c = classLoader;
        }

        public i(Parcelable parcelable) {
            super(parcelable);
        }

        public String toString() {
            return "FragmentPager.SavedState{" + Integer.toHexString(System.identityHashCode(this)) + " position=" + this.f766a + "}";
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.f766a);
            parcel.writeParcelable(this.f767b, i);
        }
    }

    static class j implements Comparator<View> {
        j() {
        }

        /* renamed from: a */
        public int compare(View view, View view2) {
            c cVar = (c) view.getLayoutParams();
            c cVar2 = (c) view2.getLayoutParams();
            return cVar.f762a != cVar2.f762a ? cVar.f762a ? 1 : -1 : cVar.e - cVar2.e;
        }
    }

    public w(Context context) {
        super(context);
        a();
    }

    public w(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a();
    }

    private int a(int i2, float f2, int i3, int i4) {
        if (Math.abs(i4) <= this.O || Math.abs(i3) <= this.M) {
            i2 += (int) ((i2 >= this.c ? 0.4f : 0.6f) + f2);
        } else if (i3 <= 0) {
            i2++;
        }
        if (this.g.size() <= 0) {
            return i2;
        }
        return Math.max(((b) this.g.get(0)).f761b, Math.min(i2, ((b) this.g.get(this.g.size() - 1)).f761b));
    }

    private Rect a(Rect rect, View view) {
        Rect rect2 = rect == null ? new Rect() : rect;
        if (view == null) {
            rect2.set(0, 0, 0, 0);
            return rect2;
        }
        rect2.left = view.getLeft();
        rect2.right = view.getRight();
        rect2.top = view.getTop();
        rect2.bottom = view.getBottom();
        ViewParent parent = view.getParent();
        while ((parent instanceof ViewGroup) && parent != this) {
            ViewGroup viewGroup = (ViewGroup) parent;
            rect2.left += viewGroup.getLeft();
            rect2.right += viewGroup.getRight();
            rect2.top += viewGroup.getTop();
            rect2.bottom += viewGroup.getBottom();
            parent = viewGroup.getParent();
        }
        return rect2;
    }

    private void a(int i2, int i3, int i4, int i5) {
        if (i3 <= 0 || this.g.isEmpty()) {
            b b2 = b(this.c);
            int min = (int) ((b2 != null ? Math.min(b2.e, this.u) : 0.0f) * ((float) ((i2 - getPaddingLeft()) - getPaddingRight())));
            if (min != getScrollX()) {
                a(false);
                scrollTo(min, getScrollY());
            }
        } else if (!this.m.isFinished()) {
            this.m.setFinalX(getCurrentItem() * getClientWidth());
        } else {
            scrollTo((int) (((float) (((i2 - getPaddingLeft()) - getPaddingRight()) + i4)) * (((float) getScrollX()) / ((float) (((i3 - getPaddingLeft()) - getPaddingRight()) + i5)))), getScrollY());
        }
    }

    private void a(int i2, boolean z2, int i3, boolean z3) {
        int i4;
        b b2 = b(i2);
        if (b2 != null) {
            i4 = (int) (Math.max(this.t, Math.min(b2.e, this.u)) * ((float) getClientWidth()));
        } else {
            i4 = 0;
        }
        if (z2) {
            a(i4, 0, i3);
            if (z3) {
                e(i2);
                return;
            }
            return;
        }
        if (z3) {
            e(i2);
        }
        a(false);
        scrollTo(i4, 0);
        d(i4);
    }

    private void a(b bVar, int i2, b bVar2) {
        b bVar3;
        b bVar4;
        int b2 = this.f756b.b();
        int clientWidth = getClientWidth();
        float f2 = clientWidth > 0 ? ((float) this.p) / ((float) clientWidth) : 0.0f;
        if (bVar2 != null) {
            int i3 = bVar2.f761b;
            if (i3 < bVar.f761b) {
                float f3 = bVar2.e + bVar2.d + f2;
                int i4 = i3 + 1;
                int i5 = 0;
                while (i4 <= bVar.f761b && i5 < this.g.size()) {
                    Object obj = this.g.get(i5);
                    while (true) {
                        bVar4 = (b) obj;
                        if (i4 > bVar4.f761b && i5 < this.g.size() - 1) {
                            i5++;
                            obj = this.g.get(i5);
                        }
                    }
                    while (i4 < bVar4.f761b) {
                        f3 += this.f756b.c(i4) + f2;
                        i4++;
                    }
                    bVar4.e = f3;
                    f3 += bVar4.d + f2;
                    i4++;
                }
            } else if (i3 > bVar.f761b) {
                int size = this.g.size() - 1;
                float f4 = bVar2.e;
                int i6 = i3 - 1;
                while (i6 >= bVar.f761b && size >= 0) {
                    Object obj2 = this.g.get(size);
                    while (true) {
                        bVar3 = (b) obj2;
                        if (i6 < bVar3.f761b && size > 0) {
                            size--;
                            obj2 = this.g.get(size);
                        }
                    }
                    while (i6 > bVar3.f761b) {
                        f4 -= this.f756b.c(i6) + f2;
                        i6--;
                    }
                    f4 -= bVar3.d + f2;
                    bVar3.e = f4;
                    i6--;
                }
            }
        }
        int size2 = this.g.size();
        float f5 = bVar.e;
        int i7 = bVar.f761b - 1;
        this.t = bVar.f761b == 0 ? bVar.e : -3.4028235E38f;
        this.u = bVar.f761b == b2 + -1 ? (bVar.e + bVar.d) - 1.0f : Float.MAX_VALUE;
        for (int i8 = i2 - 1; i8 >= 0; i8--) {
            b bVar5 = (b) this.g.get(i8);
            float f6 = f5;
            while (i7 > bVar5.f761b) {
                f6 -= this.f756b.c(i7) + f2;
                i7--;
            }
            f5 = f6 - (bVar5.d + f2);
            bVar5.e = f5;
            if (bVar5.f761b == 0) {
                this.t = f5;
            }
            i7--;
        }
        float f7 = bVar.e + bVar.d + f2;
        int i9 = bVar.f761b + 1;
        for (int i10 = i2 + 1; i10 < size2; i10++) {
            b bVar6 = (b) this.g.get(i10);
            float f8 = f7;
            while (i9 < bVar6.f761b) {
                f8 = this.f756b.c(i9) + f2 + f8;
                i9++;
            }
            if (bVar6.f761b == b2 - 1) {
                this.u = (bVar6.d + f8) - 1.0f;
            }
            bVar6.e = f8;
            f7 = f8 + bVar6.d + f2;
            i9++;
        }
        this.U = false;
    }

    private void a(MotionEvent motionEvent) {
        int actionIndex = motionEvent.getActionIndex();
        if (motionEvent.getPointerId(actionIndex) == this.K) {
            int i2 = actionIndex == 0 ? 1 : 0;
            this.G = motionEvent.getX(i2);
            this.K = motionEvent.getPointerId(i2);
            if (this.L != null) {
                this.L.clear();
            }
        }
    }

    private void a(boolean z2) {
        boolean z3 = this.ak == 2;
        if (z3) {
            setScrollingCacheEnabled(false);
            if (!this.m.isFinished()) {
                this.m.abortAnimation();
                int scrollX = getScrollX();
                int scrollY = getScrollY();
                int currX = this.m.getCurrX();
                int currY = this.m.getCurrY();
                if (!(scrollX == currX && scrollY == currY)) {
                    scrollTo(currX, currY);
                    if (currX != scrollX) {
                        d(currX);
                    }
                }
            }
        }
        this.z = false;
        boolean z4 = z3;
        for (int i2 = 0; i2 < this.g.size(); i2++) {
            b bVar = (b) this.g.get(i2);
            if (bVar.c) {
                bVar.c = false;
                z4 = true;
            }
        }
        if (!z4) {
            return;
        }
        if (z2) {
            t.a((View) this, this.aj);
        } else {
            this.aj.run();
        }
    }

    private boolean a(float f2, float f3) {
        return (f2 < ((float) this.E) && f3 > 0.0f) || (f2 > ((float) (getWidth() - this.E)) && f3 < 0.0f);
    }

    private void b(int i2, float f2, int i3) {
        if (this.ab != null) {
            this.ab.a(i2, f2, i3);
        }
        if (this.aa != null) {
            int size = this.aa.size();
            for (int i4 = 0; i4 < size; i4++) {
                f fVar = (f) this.aa.get(i4);
                if (fVar != null) {
                    fVar.a(i2, f2, i3);
                }
            }
        }
        if (this.ac != null) {
            this.ac.a(i2, f2, i3);
        }
    }

    private void b(boolean z2) {
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            getChildAt(i2).setLayerType(z2 ? this.af : 0, null);
        }
    }

    private boolean b(float f2) {
        boolean z2;
        float f3;
        boolean z3;
        boolean z4 = true;
        float f4 = this.G - f2;
        this.G = f2;
        float scrollX = ((float) getScrollX()) + f4;
        int clientWidth = getClientWidth();
        float f5 = ((float) clientWidth) * this.t;
        float f6 = ((float) clientWidth) * this.u;
        b bVar = (b) this.g.get(0);
        b bVar2 = (b) this.g.get(this.g.size() - 1);
        if (bVar.f761b != 0) {
            f5 = bVar.e * ((float) clientWidth);
            z2 = false;
        } else {
            z2 = true;
        }
        if (bVar2.f761b != this.f756b.b() - 1) {
            f3 = bVar2.e * ((float) clientWidth);
            z3 = false;
        } else {
            f3 = f6;
            z3 = true;
        }
        if (scrollX < f5) {
            if (z2) {
                this.R.onPull(Math.abs(f5 - scrollX) / ((float) clientWidth));
            } else {
                z4 = false;
            }
        } else if (scrollX > f3) {
            if (z3) {
                this.S.onPull(Math.abs(scrollX - f3) / ((float) clientWidth));
            } else {
                z4 = false;
            }
            f5 = f3;
        } else {
            f5 = scrollX;
            z4 = false;
        }
        this.G += f5 - ((float) ((int) f5));
        scrollTo((int) f5, getScrollY());
        d((int) f5);
        return z4;
    }

    private void c(boolean z2) {
        ViewParent parent = getParent();
        if (parent != null) {
            parent.requestDisallowInterceptTouchEvent(z2);
        }
    }

    private static boolean c(View view) {
        return view.getClass().getAnnotation(a.class) != null;
    }

    private boolean d(int i2) {
        if (this.g.size() != 0) {
            b i3 = i();
            int clientWidth = getClientWidth();
            int i4 = this.p + clientWidth;
            float f2 = ((float) this.p) / ((float) clientWidth);
            int i5 = i3.f761b;
            float f3 = ((((float) i2) / ((float) clientWidth)) - i3.e) / (i3.d + f2);
            int i6 = (int) (((float) i4) * f3);
            this.V = false;
            a(i5, f3, i6);
            if (this.V) {
                return true;
            }
            throw new IllegalStateException("onPageScrolled did not call superclass implementation");
        } else if (this.T) {
            return false;
        } else {
            this.V = false;
            a(0, 0.0f, 0);
            if (this.V) {
                return false;
            }
            throw new IllegalStateException("onPageScrolled did not call superclass implementation");
        }
    }

    private void e(int i2) {
        if (this.ab != null) {
            this.ab.b(i2);
        }
        if (this.aa != null) {
            int size = this.aa.size();
            for (int i3 = 0; i3 < size; i3++) {
                f fVar = (f) this.aa.get(i3);
                if (fVar != null) {
                    fVar.b(i2);
                }
            }
        }
        if (this.ac != null) {
            this.ac.b(i2);
        }
    }

    private void f() {
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < getChildCount()) {
                if (!((c) getChildAt(i3).getLayoutParams()).f762a) {
                    removeViewAt(i3);
                    i3--;
                }
                i2 = i3 + 1;
            } else {
                return;
            }
        }
    }

    private void f(int i2) {
        if (this.ab != null) {
            this.ab.a(i2);
        }
        if (this.aa != null) {
            int size = this.aa.size();
            for (int i3 = 0; i3 < size; i3++) {
                f fVar = (f) this.aa.get(i3);
                if (fVar != null) {
                    fVar.a(i2);
                }
            }
        }
        if (this.ac != null) {
            this.ac.a(i2);
        }
    }

    private void g() {
        if (this.ag != 0) {
            if (this.ah == null) {
                this.ah = new ArrayList<>();
            } else {
                this.ah.clear();
            }
            int childCount = getChildCount();
            for (int i2 = 0; i2 < childCount; i2++) {
                this.ah.add(getChildAt(i2));
            }
            Collections.sort(this.ah, ai);
        }
    }

    private int getClientWidth() {
        return (getMeasuredWidth() - getPaddingLeft()) - getPaddingRight();
    }

    private boolean h() {
        this.K = -1;
        j();
        this.R.onRelease();
        this.S.onRelease();
        return this.R.isFinished() || this.S.isFinished();
    }

    private b i() {
        int i2;
        b bVar;
        int clientWidth = getClientWidth();
        float f2 = clientWidth > 0 ? ((float) getScrollX()) / ((float) clientWidth) : 0.0f;
        float f3 = clientWidth > 0 ? ((float) this.p) / ((float) clientWidth) : 0.0f;
        float f4 = 0.0f;
        float f5 = 0.0f;
        int i3 = -1;
        int i4 = 0;
        boolean z2 = true;
        b bVar2 = null;
        while (i4 < this.g.size()) {
            b bVar3 = (b) this.g.get(i4);
            if (z2 || bVar3.f761b == i3 + 1) {
                b bVar4 = bVar3;
                i2 = i4;
                bVar = bVar4;
            } else {
                b bVar5 = this.h;
                bVar5.e = f4 + f5 + f3;
                bVar5.f761b = i3 + 1;
                bVar5.d = this.f756b.c(bVar5.f761b);
                b bVar6 = bVar5;
                i2 = i4 - 1;
                bVar = bVar6;
            }
            float f6 = bVar.e;
            float f7 = bVar.d + f6 + f3;
            if (!z2 && f2 < f6) {
                return bVar2;
            }
            if (f2 < f7 || i2 == this.g.size() - 1) {
                return bVar;
            }
            f5 = f6;
            i3 = bVar.f761b;
            z2 = false;
            f4 = bVar.d;
            bVar2 = bVar;
            i4 = i2 + 1;
        }
        return bVar2;
    }

    private void j() {
        this.B = false;
        this.C = false;
        if (this.L != null) {
            this.L.recycle();
            this.L = null;
        }
    }

    private void setScrollingCacheEnabled(boolean z2) {
        if (this.y != z2) {
            this.y = z2;
        }
    }

    /* access modifiers changed from: 0000 */
    public float a(float f2) {
        return (float) Math.sin((double) ((f2 - 0.5f) * 0.47123894f));
    }

    /* access modifiers changed from: 0000 */
    public b a(int i2, int i3) {
        b bVar = new b();
        bVar.f761b = i2;
        bVar.f760a = this.f756b.a((ViewGroup) this, i2);
        bVar.d = this.f756b.c(i2);
        if (i3 < 0 || i3 >= this.g.size()) {
            this.g.add(bVar);
        } else {
            this.g.add(i3, bVar);
        }
        return bVar;
    }

    /* access modifiers changed from: 0000 */
    public b a(View view) {
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= this.g.size()) {
                return null;
            }
            b bVar = (b) this.g.get(i3);
            if (this.f756b.a(view, bVar.f760a)) {
                return bVar;
            }
            i2 = i3 + 1;
        }
    }

    /* access modifiers changed from: 0000 */
    public void a() {
        setWillNotDraw(false);
        setDescendantFocusability(262144);
        setFocusable(true);
        Context context = getContext();
        this.m = new Scroller(context, f);
        ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
        float f2 = context.getResources().getDisplayMetrics().density;
        this.F = viewConfiguration.getScaledPagingTouchSlop();
        this.M = (int) (400.0f * f2);
        this.N = viewConfiguration.getScaledMaximumFlingVelocity();
        this.R = new EdgeEffect(context);
        this.S = new EdgeEffect(context);
        this.O = (int) (25.0f * f2);
        this.P = (int) (2.0f * f2);
        this.D = (int) (16.0f * f2);
        t.a((View) this, (b) new d());
        if (t.d(this) == 0) {
            t.a((View) this, 1);
        }
        t.a((View) this, (p) new p() {

            /* renamed from: b reason: collision with root package name */
            private final Rect f759b = new Rect();

            public ac a(View view, ac acVar) {
                ac a2 = t.a(view, acVar);
                if (a2.e()) {
                    return a2;
                }
                Rect rect = this.f759b;
                rect.left = a2.a();
                rect.top = a2.b();
                rect.right = a2.c();
                rect.bottom = a2.d();
                int childCount = w.this.getChildCount();
                for (int i = 0; i < childCount; i++) {
                    ac b2 = t.b(w.this.getChildAt(i), a2);
                    rect.left = Math.min(b2.a(), rect.left);
                    rect.top = Math.min(b2.b(), rect.top);
                    rect.right = Math.min(b2.c(), rect.right);
                    rect.bottom = Math.min(b2.d(), rect.bottom);
                }
                return a2.a(rect.left, rect.top, rect.right, rect.bottom);
            }
        });
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x00f0, code lost:
        if (r2.f761b == r17.c) goto L_0x00f2;
     */
    public void a(int i2) {
        b bVar;
        int i3;
        b bVar2;
        String hexString;
        if (this.c != i2) {
            b b2 = b(this.c);
            this.c = i2;
            bVar = b2;
        } else {
            bVar = null;
        }
        if (this.f756b == null) {
            g();
        } else if (this.z) {
            g();
        } else if (getWindowToken() != null) {
            this.f756b.a((ViewGroup) this);
            int i4 = this.A;
            int max = Math.max(0, this.c - i4);
            int b3 = this.f756b.b();
            int min = Math.min(b3 - 1, i4 + this.c);
            if (b3 != this.d) {
                try {
                    hexString = getResources().getResourceName(getId());
                } catch (NotFoundException e2) {
                    hexString = Integer.toHexString(getId());
                }
                throw new IllegalStateException("The application's PagerAdapter changed the adapter's contents without calling PagerAdapter#notifyDataSetChanged! Expected adapter item count: " + this.d + ", found: " + b3 + " Pager id: " + hexString + " Pager class: " + getClass() + " Problematic adapter: " + this.f756b.getClass());
            }
            int i5 = 0;
            while (true) {
                i3 = i5;
                if (i3 >= this.g.size()) {
                    break;
                }
                bVar2 = (b) this.g.get(i3);
                if (bVar2.f761b < this.c) {
                    i5 = i3 + 1;
                }
            }
            bVar2 = null;
            b bVar3 = (bVar2 != null || b3 <= 0) ? bVar2 : a(this.c, i3);
            if (bVar3 != null) {
                int i6 = i3 - 1;
                b bVar4 = i6 >= 0 ? (b) this.g.get(i6) : null;
                int clientWidth = getClientWidth();
                float paddingLeft = clientWidth <= 0 ? 0.0f : (2.0f - bVar3.d) + (((float) getPaddingLeft()) / ((float) clientWidth));
                float f2 = 0.0f;
                int i7 = i6;
                int i8 = i3;
                int i9 = i7;
                for (int i10 = this.c - 1; i10 >= 0; i10--) {
                    if (f2 < paddingLeft || i10 >= max) {
                        if (bVar4 == null || i10 != bVar4.f761b) {
                            f2 += a(i10, i9 + 1).d;
                            i8++;
                            bVar4 = i9 >= 0 ? (b) this.g.get(i9) : null;
                        } else {
                            f2 += bVar4.d;
                            i9--;
                            bVar4 = i9 >= 0 ? (b) this.g.get(i9) : null;
                        }
                    } else if (bVar4 == null) {
                        break;
                    } else if (i10 == bVar4.f761b && !bVar4.c) {
                        this.g.remove(i9);
                        this.f756b.a((ViewGroup) this, i10, bVar4.f760a);
                        i9--;
                        i8--;
                        bVar4 = i9 >= 0 ? (b) this.g.get(i9) : null;
                    }
                }
                float f3 = bVar3.d;
                int i11 = i8 + 1;
                if (f3 < 2.0f) {
                    b bVar5 = i11 < this.g.size() ? (b) this.g.get(i11) : null;
                    float paddingRight = clientWidth <= 0 ? 0.0f : (((float) getPaddingRight()) / ((float) clientWidth)) + 2.0f;
                    b bVar6 = bVar5;
                    int i12 = i11;
                    int i13 = this.c + 1;
                    while (i13 < b3) {
                        if (f3 < paddingRight || i13 <= min) {
                            if (bVar6 == null || i13 != bVar6.f761b) {
                                b a2 = a(i13, i12);
                                i12++;
                                f3 += a2.d;
                                bVar6 = i12 < this.g.size() ? (b) this.g.get(i12) : null;
                            } else {
                                f3 += bVar6.d;
                                i12++;
                                bVar6 = i12 < this.g.size() ? (b) this.g.get(i12) : null;
                            }
                        } else if (bVar6 == null) {
                            break;
                        } else if (i13 == bVar6.f761b && !bVar6.c) {
                            this.g.remove(i12);
                            this.f756b.a((ViewGroup) this, i13, bVar6.f760a);
                            bVar6 = i12 < this.g.size() ? (b) this.g.get(i12) : null;
                        }
                        i13++;
                        bVar6 = bVar6;
                        f3 = f3;
                    }
                }
                a(bVar3, i8, bVar);
            }
            this.f756b.b((ViewGroup) this, this.c, bVar3 != null ? bVar3.f760a : null);
            this.f756b.b((ViewGroup) this);
            int childCount = getChildCount();
            for (int i14 = 0; i14 < childCount; i14++) {
                View childAt = getChildAt(i14);
                c cVar = (c) childAt.getLayoutParams();
                cVar.f = i14;
                if (!cVar.f762a && cVar.c == 0.0f) {
                    b a3 = a(childAt);
                    if (a3 != null) {
                        cVar.c = a3.d;
                        cVar.e = a3.f761b;
                    }
                }
            }
            g();
            if (hasFocus()) {
                View findFocus = findFocus();
                b bVar7 = findFocus != null ? b(findFocus) : null;
                if (bVar7 == null || bVar7.f761b != this.c) {
                    int i15 = 0;
                    while (i15 < getChildCount()) {
                        View childAt2 = getChildAt(i15);
                        b a4 = a(childAt2);
                        if (a4 == null || a4.f761b != this.c || !childAt2.requestFocus(2)) {
                            i15++;
                        } else {
                            return;
                        }
                    }
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a(int i2, float f2, int i3) {
        int measuredWidth;
        int i4;
        int i5;
        if (this.W > 0) {
            int scrollX = getScrollX();
            int paddingLeft = getPaddingLeft();
            int paddingRight = getPaddingRight();
            int width = getWidth();
            int childCount = getChildCount();
            int i6 = 0;
            while (i6 < childCount) {
                View childAt = getChildAt(i6);
                c cVar = (c) childAt.getLayoutParams();
                if (!cVar.f762a) {
                    int i7 = paddingRight;
                    i4 = paddingLeft;
                    i5 = i7;
                } else {
                    switch (cVar.f763b & 7) {
                        case 1:
                            measuredWidth = Math.max((width - childAt.getMeasuredWidth()) / 2, paddingLeft);
                            int i8 = paddingRight;
                            i4 = paddingLeft;
                            i5 = i8;
                            break;
                        case 3:
                            int width2 = childAt.getWidth() + paddingLeft;
                            int i9 = paddingLeft;
                            i5 = paddingRight;
                            i4 = width2;
                            measuredWidth = i9;
                            break;
                        case 5:
                            measuredWidth = (width - paddingRight) - childAt.getMeasuredWidth();
                            int measuredWidth2 = paddingRight + childAt.getMeasuredWidth();
                            i4 = paddingLeft;
                            i5 = measuredWidth2;
                            break;
                        default:
                            measuredWidth = paddingLeft;
                            int i10 = paddingRight;
                            i4 = paddingLeft;
                            i5 = i10;
                            break;
                    }
                    int left = (measuredWidth + scrollX) - childAt.getLeft();
                    if (left != 0) {
                        childAt.offsetLeftAndRight(left);
                    }
                }
                i6++;
                int i11 = i5;
                paddingLeft = i4;
                paddingRight = i11;
            }
        }
        b(i2, f2, i3);
        if (this.ae != null) {
            int scrollX2 = getScrollX();
            int childCount2 = getChildCount();
            for (int i12 = 0; i12 < childCount2; i12++) {
                View childAt2 = getChildAt(i12);
                if (!((c) childAt2.getLayoutParams()).f762a) {
                    this.ae.a(childAt2, ((float) (childAt2.getLeft() - scrollX2)) / ((float) getClientWidth()));
                }
            }
        }
        this.V = true;
    }

    /* access modifiers changed from: 0000 */
    public void a(int i2, int i3, int i4) {
        int scrollX;
        if (getChildCount() == 0) {
            setScrollingCacheEnabled(false);
            return;
        }
        if (this.m != null && !this.m.isFinished()) {
            int startX = this.n ? this.m.getCurrX() : this.m.getStartX();
            this.m.abortAnimation();
            setScrollingCacheEnabled(false);
            scrollX = startX;
        } else {
            scrollX = getScrollX();
        }
        int scrollY = getScrollY();
        int i5 = i2 - scrollX;
        int i6 = i3 - scrollY;
        if (i5 == 0 && i6 == 0) {
            a(false);
            c();
            setScrollState(0);
            return;
        }
        setScrollingCacheEnabled(true);
        setScrollState(2);
        int clientWidth = getClientWidth();
        int i7 = clientWidth / 2;
        float a2 = (((float) i7) * a(Math.min(1.0f, (((float) Math.abs(i5)) * 1.0f) / ((float) clientWidth)))) + ((float) i7);
        int abs = Math.abs(i4);
        int min = Math.min(abs > 0 ? Math.round(1000.0f * Math.abs(a2 / ((float) abs))) * 4 : (int) (((((float) Math.abs(i5)) / ((((float) clientWidth) * this.f756b.c(this.c)) + ((float) this.p))) + 1.0f) * 100.0f), 600);
        this.n = false;
        this.m.startScroll(scrollX, scrollY, i5, i6, min);
        t.c(this);
    }

    public void a(int i2, boolean z2) {
        this.z = false;
        a(i2, z2, false);
    }

    /* access modifiers changed from: 0000 */
    public void a(int i2, boolean z2, boolean z3) {
        a(i2, z2, z3, 0);
    }

    /* access modifiers changed from: 0000 */
    public void a(int i2, boolean z2, boolean z3, int i3) {
        boolean z4 = false;
        if (this.f756b == null || this.f756b.b() <= 0) {
            setScrollingCacheEnabled(false);
        } else if (z3 || this.c != i2 || this.g.size() == 0) {
            if (i2 < 0) {
                i2 = 0;
            } else if (i2 >= this.f756b.b()) {
                i2 = this.f756b.b() - 1;
            }
            int i4 = this.A;
            if (i2 > this.c + i4 || i2 < this.c - i4) {
                for (int i5 = 0; i5 < this.g.size(); i5++) {
                    ((b) this.g.get(i5)).c = true;
                }
            }
            if (this.c != i2) {
                z4 = true;
            }
            if (this.T) {
                this.c = i2;
                if (z4) {
                    e(i2);
                }
                requestLayout();
                return;
            }
            a(i2);
            a(i2, z2, i3, z4);
        } else {
            setScrollingCacheEnabled(false);
        }
    }

    public void a(e eVar) {
        if (this.ad == null) {
            this.ad = new ArrayList();
        }
        this.ad.add(eVar);
    }

    public void a(f fVar) {
        if (this.aa == null) {
            this.aa = new ArrayList();
        }
        this.aa.add(fVar);
    }

    public boolean a(KeyEvent keyEvent) {
        if (keyEvent.getAction() != 0) {
            return false;
        }
        switch (keyEvent.getKeyCode()) {
            case 21:
                return keyEvent.hasModifiers(2) ? d() : c(17);
            case 22:
                return keyEvent.hasModifiers(2) ? e() : c(66);
            case 61:
                if (keyEvent.hasNoModifiers()) {
                    return c(2);
                }
                if (keyEvent.hasModifiers(1)) {
                    return c(1);
                }
                return false;
            default:
                return false;
        }
    }

    /* access modifiers changed from: protected */
    public boolean a(View view, boolean z2, int i2, int i3, int i4) {
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            int scrollX = view.getScrollX();
            int scrollY = view.getScrollY();
            for (int childCount = viewGroup.getChildCount() - 1; childCount >= 0; childCount--) {
                View childAt = viewGroup.getChildAt(childCount);
                if (i3 + scrollX >= childAt.getLeft() && i3 + scrollX < childAt.getRight() && i4 + scrollY >= childAt.getTop() && i4 + scrollY < childAt.getBottom()) {
                    if (a(childAt, true, i2, (i3 + scrollX) - childAt.getLeft(), (i4 + scrollY) - childAt.getTop())) {
                        return true;
                    }
                }
            }
        }
        return z2 && view.canScrollHorizontally(-i2);
    }

    public void addFocusables(ArrayList<View> arrayList, int i2, int i3) {
        int size = arrayList.size();
        int descendantFocusability = getDescendantFocusability();
        if (descendantFocusability != 393216) {
            for (int i4 = 0; i4 < getChildCount(); i4++) {
                View childAt = getChildAt(i4);
                if (childAt.getVisibility() == 0) {
                    b a2 = a(childAt);
                    if (a2 != null && a2.f761b == this.c) {
                        childAt.addFocusables(arrayList, i2, i3);
                    }
                }
            }
        }
        if ((descendantFocusability == 262144 && size != arrayList.size()) || !isFocusable()) {
            return;
        }
        if (((i3 & 1) != 1 || !isInTouchMode() || isFocusableInTouchMode()) && arrayList != null) {
            arrayList.add(this);
        }
    }

    public void addTouchables(ArrayList<View> arrayList) {
        for (int i2 = 0; i2 < getChildCount(); i2++) {
            View childAt = getChildAt(i2);
            if (childAt.getVisibility() == 0) {
                b a2 = a(childAt);
                if (a2 != null && a2.f761b == this.c) {
                    childAt.addTouchables(arrayList);
                }
            }
        }
    }

    public void addView(View view, int i2, LayoutParams layoutParams) {
        LayoutParams layoutParams2 = !checkLayoutParams(layoutParams) ? generateLayoutParams(layoutParams) : layoutParams;
        c cVar = (c) layoutParams2;
        cVar.f762a |= c(view);
        if (!this.x) {
            super.addView(view, i2, layoutParams2);
        } else if (cVar == null || !cVar.f762a) {
            cVar.d = true;
            addViewInLayout(view, i2, layoutParams2);
        } else {
            throw new IllegalStateException("Cannot add pager decor view during layout");
        }
    }

    /* access modifiers changed from: 0000 */
    public b b(int i2) {
        int i3 = 0;
        while (true) {
            int i4 = i3;
            if (i4 >= this.g.size()) {
                return null;
            }
            b bVar = (b) this.g.get(i4);
            if (bVar.f761b == i2) {
                return bVar;
            }
            i3 = i4 + 1;
        }
    }

    /* access modifiers changed from: 0000 */
    public b b(View view) {
        while (true) {
            ViewParent parent = view.getParent();
            if (parent == this) {
                return a(view);
            }
            if (parent != null && (parent instanceof View)) {
                view = (View) parent;
            }
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    public void b() {
        int i2;
        boolean z2;
        int i3;
        boolean z3;
        int b2 = this.f756b.b();
        this.d = b2;
        boolean z4 = this.g.size() < (this.A * 2) + 1 && this.g.size() < b2;
        boolean z5 = false;
        int i4 = this.c;
        boolean z6 = z4;
        int i5 = 0;
        while (i5 < this.g.size()) {
            b bVar = (b) this.g.get(i5);
            int a2 = this.f756b.a(bVar.f760a);
            if (a2 == -1) {
                i2 = i5;
                z2 = z5;
                i3 = i4;
                z3 = z6;
            } else if (a2 == -2) {
                this.g.remove(i5);
                int i6 = i5 - 1;
                if (!z5) {
                    this.f756b.a((ViewGroup) this);
                    z5 = true;
                }
                this.f756b.a((ViewGroup) this, bVar.f761b, bVar.f760a);
                if (this.c == bVar.f761b) {
                    i2 = i6;
                    z2 = z5;
                    i3 = Math.max(0, Math.min(this.c, b2 - 1));
                    z3 = true;
                } else {
                    i2 = i6;
                    z2 = z5;
                    i3 = i4;
                    z3 = true;
                }
            } else if (bVar.f761b != a2) {
                if (bVar.f761b == this.c) {
                    i4 = a2;
                }
                bVar.f761b = a2;
                i2 = i5;
                z2 = z5;
                i3 = i4;
                z3 = true;
            } else {
                i2 = i5;
                z2 = z5;
                i3 = i4;
                z3 = z6;
            }
            z6 = z3;
            i4 = i3;
            z5 = z2;
            i5 = i2 + 1;
        }
        if (z5) {
            this.f756b.b((ViewGroup) this);
        }
        Collections.sort(this.g, e);
        if (z6) {
            int childCount = getChildCount();
            for (int i7 = 0; i7 < childCount; i7++) {
                c cVar = (c) getChildAt(i7).getLayoutParams();
                if (!cVar.f762a) {
                    cVar.c = 0.0f;
                }
            }
            a(i4, false, true);
            requestLayout();
        }
    }

    public void b(e eVar) {
        if (this.ad != null) {
            this.ad.remove(eVar);
        }
    }

    public void b(f fVar) {
        if (this.aa != null) {
            this.aa.remove(fVar);
        }
    }

    /* access modifiers changed from: 0000 */
    public void c() {
        a(this.c);
    }

    public boolean c(int i2) {
        View view;
        boolean z2;
        boolean z3;
        View findFocus = findFocus();
        if (findFocus == this) {
            view = null;
        } else {
            if (findFocus != null) {
                ViewParent parent = findFocus.getParent();
                while (true) {
                    if (!(parent instanceof ViewGroup)) {
                        z2 = false;
                        break;
                    } else if (parent == this) {
                        z2 = true;
                        break;
                    } else {
                        parent = parent.getParent();
                    }
                }
                if (!z2) {
                    StringBuilder sb = new StringBuilder();
                    sb.append(findFocus.getClass().getSimpleName());
                    for (ViewParent parent2 = findFocus.getParent(); parent2 instanceof ViewGroup; parent2 = parent2.getParent()) {
                        sb.append(" => ").append(parent2.getClass().getSimpleName());
                    }
                    Log.e("ViewPager", "arrowScroll tried to find focus based on non-child current focused view " + sb.toString());
                    view = null;
                }
            }
            view = findFocus;
        }
        View findNextFocus = FocusFinder.getInstance().findNextFocus(this, view, i2);
        if (findNextFocus == null || findNextFocus == view) {
            if (i2 == 17 || i2 == 1) {
                z3 = d();
            } else {
                if (i2 == 66 || i2 == 2) {
                    z3 = e();
                }
                z3 = false;
            }
        } else if (i2 == 17) {
            z3 = (view == null || a(this.i, findNextFocus).left < a(this.i, view).left) ? findNextFocus.requestFocus() : d();
        } else {
            if (i2 == 66) {
                z3 = (view == null || a(this.i, findNextFocus).left > a(this.i, view).left) ? findNextFocus.requestFocus() : e();
            }
            z3 = false;
        }
        if (z3) {
            playSoundEffect(SoundEffectConstants.getContantForFocusDirection(i2));
        }
        return z3;
    }

    public boolean canScrollHorizontally(int i2) {
        boolean z2 = true;
        if (this.f756b == null) {
            return false;
        }
        int clientWidth = getClientWidth();
        int scrollX = getScrollX();
        if (i2 < 0) {
            if (scrollX <= ((int) (((float) clientWidth) * this.t))) {
                z2 = false;
            }
            return z2;
        } else if (i2 <= 0) {
            return false;
        } else {
            if (scrollX >= ((int) (((float) clientWidth) * this.u))) {
                z2 = false;
            }
            return z2;
        }
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(LayoutParams layoutParams) {
        return (layoutParams instanceof c) && super.checkLayoutParams(layoutParams);
    }

    public void computeScroll() {
        this.n = true;
        if (this.m.isFinished() || !this.m.computeScrollOffset()) {
            a(true);
            return;
        }
        int scrollX = getScrollX();
        int scrollY = getScrollY();
        int currX = this.m.getCurrX();
        int currY = this.m.getCurrY();
        if (!(scrollX == currX && scrollY == currY)) {
            scrollTo(currX, currY);
            if (!d(currX)) {
                this.m.abortAnimation();
                scrollTo(0, currY);
            }
        }
        t.c(this);
    }

    /* access modifiers changed from: 0000 */
    public boolean d() {
        if (this.c <= 0) {
            return false;
        }
        a(this.c - 1, true);
        return true;
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        return super.dispatchKeyEvent(keyEvent) || a(keyEvent);
    }

    public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        if (accessibilityEvent.getEventType() == 4096) {
            return super.dispatchPopulateAccessibilityEvent(accessibilityEvent);
        }
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = getChildAt(i2);
            if (childAt.getVisibility() == 0) {
                b a2 = a(childAt);
                if (a2 != null && a2.f761b == this.c && childAt.dispatchPopulateAccessibilityEvent(accessibilityEvent)) {
                    return true;
                }
            }
        }
        return false;
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
        boolean z2 = false;
        int overScrollMode = getOverScrollMode();
        if (overScrollMode == 0 || (overScrollMode == 1 && this.f756b != null && this.f756b.b() > 1)) {
            if (!this.R.isFinished()) {
                int save = canvas.save();
                int height = (getHeight() - getPaddingTop()) - getPaddingBottom();
                int width = getWidth();
                canvas.rotate(270.0f);
                canvas.translate((float) ((-height) + getPaddingTop()), this.t * ((float) width));
                this.R.setSize(height, width);
                z2 = false | this.R.draw(canvas);
                canvas.restoreToCount(save);
            }
            if (!this.S.isFinished()) {
                int save2 = canvas.save();
                int width2 = getWidth();
                int height2 = (getHeight() - getPaddingTop()) - getPaddingBottom();
                canvas.rotate(90.0f);
                canvas.translate((float) (-getPaddingTop()), (-(this.u + 1.0f)) * ((float) width2));
                this.S.setSize(height2, width2);
                z2 |= this.S.draw(canvas);
                canvas.restoreToCount(save2);
            }
        } else {
            this.R.finish();
            this.S.finish();
        }
        if (z2) {
            t.c(this);
        }
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        Drawable drawable = this.q;
        if (drawable != null && drawable.isStateful()) {
            drawable.setState(getDrawableState());
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean e() {
        if (this.f756b == null || this.c >= this.f756b.b() - 1) {
            return false;
        }
        a(this.c + 1, true);
        return true;
    }

    /* access modifiers changed from: protected */
    public LayoutParams generateDefaultLayoutParams() {
        return new c();
    }

    public LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new c(getContext(), attributeSet);
    }

    /* access modifiers changed from: protected */
    public LayoutParams generateLayoutParams(LayoutParams layoutParams) {
        return generateDefaultLayoutParams();
    }

    public q getAdapter() {
        return this.f756b;
    }

    /* access modifiers changed from: protected */
    public int getChildDrawingOrder(int i2, int i3) {
        if (this.ag == 2) {
            i3 = (i2 - 1) - i3;
        }
        return ((c) ((View) this.ah.get(i3)).getLayoutParams()).f;
    }

    public int getCurrentItem() {
        return this.c;
    }

    public int getOffscreenPageLimit() {
        return this.A;
    }

    public int getPageMargin() {
        return this.p;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.T = true;
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        removeCallbacks(this.aj);
        if (this.m != null && !this.m.isFinished()) {
            this.m.abortAnimation();
        }
        super.onDetachedFromWindow();
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        float f2;
        super.onDraw(canvas);
        if (this.p > 0 && this.q != null && this.g.size() > 0 && this.f756b != null) {
            int scrollX = getScrollX();
            int width = getWidth();
            float f3 = ((float) this.p) / ((float) width);
            b bVar = (b) this.g.get(0);
            float f4 = bVar.e;
            int size = this.g.size();
            int i2 = bVar.f761b;
            int i3 = ((b) this.g.get(size - 1)).f761b;
            int i4 = 0;
            int i5 = i2;
            while (i5 < i3) {
                while (i5 > bVar.f761b && i4 < size) {
                    i4++;
                    bVar = (b) this.g.get(i4);
                }
                if (i5 == bVar.f761b) {
                    f2 = (bVar.e + bVar.d) * ((float) width);
                    f4 = bVar.e + bVar.d + f3;
                } else {
                    float c2 = this.f756b.c(i5);
                    f2 = (f4 + c2) * ((float) width);
                    f4 += c2 + f3;
                }
                if (((float) this.p) + f2 > ((float) scrollX)) {
                    this.q.setBounds(Math.round(f2), this.r, Math.round(((float) this.p) + f2), this.s);
                    this.q.draw(canvas);
                }
                if (f2 <= ((float) (scrollX + width))) {
                    i5++;
                } else {
                    return;
                }
            }
        }
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        int action = motionEvent.getAction() & 255;
        if (action == 3 || action == 1) {
            h();
            return false;
        }
        if (action != 0) {
            if (this.B) {
                return true;
            }
            if (this.C) {
                return false;
            }
        }
        switch (action) {
            case 0:
                float x2 = motionEvent.getX();
                this.I = x2;
                this.G = x2;
                float y2 = motionEvent.getY();
                this.J = y2;
                this.H = y2;
                this.K = motionEvent.getPointerId(0);
                this.C = false;
                this.n = true;
                this.m.computeScrollOffset();
                if (this.ak == 2 && Math.abs(this.m.getFinalX() - this.m.getCurrX()) > this.P) {
                    this.m.abortAnimation();
                    this.z = false;
                    c();
                    this.B = true;
                    c(true);
                    setScrollState(1);
                    break;
                } else {
                    a(false);
                    this.B = false;
                    break;
                }
            case 2:
                int i2 = this.K;
                if (i2 != -1) {
                    int findPointerIndex = motionEvent.findPointerIndex(i2);
                    float x3 = motionEvent.getX(findPointerIndex);
                    float f2 = x3 - this.G;
                    float abs = Math.abs(f2);
                    float y3 = motionEvent.getY(findPointerIndex);
                    float abs2 = Math.abs(y3 - this.J);
                    if (f2 != 0.0f && !a(this.G, f2)) {
                        if (a(this, false, (int) f2, (int) x3, (int) y3)) {
                            this.G = x3;
                            this.H = y3;
                            this.C = true;
                            return false;
                        }
                    }
                    if (abs > ((float) this.F) && 0.5f * abs > abs2) {
                        this.B = true;
                        c(true);
                        setScrollState(1);
                        this.G = f2 > 0.0f ? this.I + ((float) this.F) : this.I - ((float) this.F);
                        this.H = y3;
                        setScrollingCacheEnabled(true);
                    } else if (abs2 > ((float) this.F)) {
                        this.C = true;
                    }
                    if (this.B && b(x3)) {
                        t.c(this);
                        break;
                    }
                }
                break;
            case 6:
                a(motionEvent);
                break;
        }
        if (this.L == null) {
            this.L = VelocityTracker.obtain();
        }
        this.L.addMovement(motionEvent);
        return this.B;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z2, int i2, int i3, int i4, int i5) {
        int i6;
        int i7;
        int i8;
        int measuredHeight;
        int i9;
        int i10;
        int childCount = getChildCount();
        int i11 = i4 - i2;
        int i12 = i5 - i3;
        int paddingLeft = getPaddingLeft();
        int paddingTop = getPaddingTop();
        int paddingRight = getPaddingRight();
        int paddingBottom = getPaddingBottom();
        int scrollX = getScrollX();
        int i13 = 0;
        int i14 = 0;
        while (i14 < childCount) {
            View childAt = getChildAt(i14);
            if (childAt.getVisibility() != 8) {
                c cVar = (c) childAt.getLayoutParams();
                if (cVar.f762a) {
                    int i15 = cVar.f763b & 112;
                    switch (cVar.f763b & 7) {
                        case 1:
                            i8 = Math.max((i11 - childAt.getMeasuredWidth()) / 2, paddingLeft);
                            break;
                        case 3:
                            i8 = paddingLeft;
                            paddingLeft = childAt.getMeasuredWidth() + paddingLeft;
                            break;
                        case 5:
                            int measuredWidth = (i11 - paddingRight) - childAt.getMeasuredWidth();
                            paddingRight += childAt.getMeasuredWidth();
                            i8 = measuredWidth;
                            break;
                        default:
                            i8 = paddingLeft;
                            break;
                    }
                    switch (i15) {
                        case 16:
                            measuredHeight = Math.max((i12 - childAt.getMeasuredHeight()) / 2, paddingTop);
                            int i16 = paddingBottom;
                            i9 = paddingTop;
                            i10 = i16;
                            break;
                        case 48:
                            int measuredHeight2 = childAt.getMeasuredHeight() + paddingTop;
                            int i17 = paddingTop;
                            i10 = paddingBottom;
                            i9 = measuredHeight2;
                            measuredHeight = i17;
                            break;
                        case 80:
                            measuredHeight = (i12 - paddingBottom) - childAt.getMeasuredHeight();
                            int measuredHeight3 = paddingBottom + childAt.getMeasuredHeight();
                            i9 = paddingTop;
                            i10 = measuredHeight3;
                            break;
                        default:
                            measuredHeight = paddingTop;
                            int i18 = paddingBottom;
                            i9 = paddingTop;
                            i10 = i18;
                            break;
                    }
                    int i19 = i8 + scrollX;
                    childAt.layout(i19, measuredHeight, childAt.getMeasuredWidth() + i19, childAt.getMeasuredHeight() + measuredHeight);
                    i6 = i13 + 1;
                    i7 = i9;
                    paddingBottom = i10;
                    int i20 = paddingRight;
                    i14++;
                    paddingLeft = paddingLeft;
                    paddingRight = i20;
                    paddingTop = i7;
                    i13 = i6;
                }
            }
            i6 = i13;
            i7 = paddingTop;
            int i202 = paddingRight;
            i14++;
            paddingLeft = paddingLeft;
            paddingRight = i202;
            paddingTop = i7;
            i13 = i6;
        }
        int i21 = (i11 - paddingLeft) - paddingRight;
        for (int i22 = 0; i22 < childCount; i22++) {
            View childAt2 = getChildAt(i22);
            if (childAt2.getVisibility() != 8) {
                c cVar2 = (c) childAt2.getLayoutParams();
                if (!cVar2.f762a) {
                    b a2 = a(childAt2);
                    if (a2 != null) {
                        int i23 = ((int) (a2.e * ((float) i21))) + paddingLeft;
                        if (cVar2.d) {
                            cVar2.d = false;
                            childAt2.measure(MeasureSpec.makeMeasureSpec((int) (cVar2.c * ((float) i21)), 1073741824), MeasureSpec.makeMeasureSpec((i12 - paddingTop) - paddingBottom, 1073741824));
                        }
                        childAt2.layout(i23, paddingTop, childAt2.getMeasuredWidth() + i23, childAt2.getMeasuredHeight() + paddingTop);
                    }
                }
            }
        }
        this.r = paddingTop;
        this.s = i12 - paddingBottom;
        this.W = i13;
        if (this.T) {
            a(this.c, false, 0, false);
        }
        this.T = false;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00a0  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00b4  */
    public void onMeasure(int i2, int i3) {
        int i4;
        int i5;
        int i6;
        setMeasuredDimension(getDefaultSize(0, i2), getDefaultSize(0, i3));
        int measuredWidth = getMeasuredWidth();
        this.E = Math.min(measuredWidth / 10, this.D);
        int paddingLeft = (measuredWidth - getPaddingLeft()) - getPaddingRight();
        int measuredHeight = (getMeasuredHeight() - getPaddingTop()) - getPaddingBottom();
        int childCount = getChildCount();
        for (int i7 = 0; i7 < childCount; i7++) {
            View childAt = getChildAt(i7);
            if (childAt.getVisibility() != 8) {
                c cVar = (c) childAt.getLayoutParams();
                if (cVar != null && cVar.f762a) {
                    int i8 = cVar.f763b & 7;
                    int i9 = cVar.f763b & 112;
                    int i10 = Integer.MIN_VALUE;
                    int i11 = Integer.MIN_VALUE;
                    boolean z2 = i9 == 48 || i9 == 80;
                    boolean z3 = i8 == 3 || i8 == 5;
                    if (z2) {
                        i10 = 1073741824;
                    } else if (z3) {
                        i11 = 1073741824;
                    }
                    if (cVar.width != -2) {
                        i4 = 1073741824;
                        i5 = cVar.width != -1 ? cVar.width : paddingLeft;
                    } else {
                        i4 = i10;
                        i5 = paddingLeft;
                    }
                    if (cVar.height != -2) {
                        i11 = 1073741824;
                        if (cVar.height != -1) {
                            i6 = cVar.height;
                            childAt.measure(MeasureSpec.makeMeasureSpec(i5, i4), MeasureSpec.makeMeasureSpec(i6, i11));
                            if (!z2) {
                                measuredHeight -= childAt.getMeasuredHeight();
                            } else if (z3) {
                                paddingLeft -= childAt.getMeasuredWidth();
                            }
                        }
                    }
                    i6 = measuredHeight;
                    childAt.measure(MeasureSpec.makeMeasureSpec(i5, i4), MeasureSpec.makeMeasureSpec(i6, i11));
                    if (!z2) {
                    }
                }
            }
        }
        this.v = MeasureSpec.makeMeasureSpec(paddingLeft, 1073741824);
        this.w = MeasureSpec.makeMeasureSpec(measuredHeight, 1073741824);
        this.x = true;
        c();
        this.x = false;
        int childCount2 = getChildCount();
        for (int i12 = 0; i12 < childCount2; i12++) {
            View childAt2 = getChildAt(i12);
            if (childAt2.getVisibility() != 8) {
                c cVar2 = (c) childAt2.getLayoutParams();
                if (cVar2 == null || !cVar2.f762a) {
                    childAt2.measure(MeasureSpec.makeMeasureSpec((int) (cVar2.c * ((float) paddingLeft)), 1073741824), this.w);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public boolean onRequestFocusInDescendants(int i2, Rect rect) {
        int i3;
        int i4 = -1;
        int childCount = getChildCount();
        if ((i2 & 2) != 0) {
            i4 = 1;
            i3 = 0;
        } else {
            i3 = childCount - 1;
            childCount = -1;
        }
        while (i3 != childCount) {
            View childAt = getChildAt(i3);
            if (childAt.getVisibility() == 0) {
                b a2 = a(childAt);
                if (a2 != null && a2.f761b == this.c && childAt.requestFocus(i2, rect)) {
                    return true;
                }
            }
            i3 += i4;
        }
        return false;
    }

    public void onRestoreInstanceState(Parcelable parcelable) {
        if (!(parcelable instanceof i)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        i iVar = (i) parcelable;
        super.onRestoreInstanceState(iVar.a());
        if (this.f756b != null) {
            this.f756b.a(iVar.f767b, iVar.c);
            a(iVar.f766a, false, true);
            return;
        }
        this.j = iVar.f766a;
        this.k = iVar.f767b;
        this.l = iVar.c;
    }

    public Parcelable onSaveInstanceState() {
        i iVar = new i(super.onSaveInstanceState());
        iVar.f766a = this.c;
        if (this.f756b != null) {
            iVar.f767b = this.f756b.a();
        }
        return iVar;
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        if (i2 != i4) {
            a(i2, i4, this.p, this.p);
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        boolean z2 = false;
        if (this.Q) {
            return true;
        }
        if (motionEvent.getAction() == 0 && motionEvent.getEdgeFlags() != 0) {
            return false;
        }
        if (this.f756b == null || this.f756b.b() == 0) {
            return false;
        }
        if (this.L == null) {
            this.L = VelocityTracker.obtain();
        }
        this.L.addMovement(motionEvent);
        switch (motionEvent.getAction() & 255) {
            case 0:
                this.m.abortAnimation();
                this.z = false;
                c();
                float x2 = motionEvent.getX();
                this.I = x2;
                this.G = x2;
                float y2 = motionEvent.getY();
                this.J = y2;
                this.H = y2;
                this.K = motionEvent.getPointerId(0);
                break;
            case 1:
                if (this.B) {
                    VelocityTracker velocityTracker = this.L;
                    velocityTracker.computeCurrentVelocity(1000, (float) this.N);
                    int xVelocity = (int) velocityTracker.getXVelocity(this.K);
                    this.z = true;
                    int clientWidth = getClientWidth();
                    int scrollX = getScrollX();
                    b i2 = i();
                    a(a(i2.f761b, ((((float) scrollX) / ((float) clientWidth)) - i2.e) / (i2.d + (((float) this.p) / ((float) clientWidth))), xVelocity, (int) (motionEvent.getX(motionEvent.findPointerIndex(this.K)) - this.I)), true, true, xVelocity);
                    z2 = h();
                    break;
                }
                break;
            case 2:
                if (!this.B) {
                    int findPointerIndex = motionEvent.findPointerIndex(this.K);
                    if (findPointerIndex == -1) {
                        z2 = h();
                        break;
                    } else {
                        float x3 = motionEvent.getX(findPointerIndex);
                        float abs = Math.abs(x3 - this.G);
                        float y3 = motionEvent.getY(findPointerIndex);
                        float abs2 = Math.abs(y3 - this.H);
                        if (abs > ((float) this.F) && abs > abs2) {
                            this.B = true;
                            c(true);
                            this.G = x3 - this.I > 0.0f ? this.I + ((float) this.F) : this.I - ((float) this.F);
                            this.H = y3;
                            setScrollState(1);
                            setScrollingCacheEnabled(true);
                            ViewParent parent = getParent();
                            if (parent != null) {
                                parent.requestDisallowInterceptTouchEvent(true);
                            }
                        }
                    }
                }
                if (this.B) {
                    z2 = false | b(motionEvent.getX(motionEvent.findPointerIndex(this.K)));
                    break;
                }
                break;
            case 3:
                if (this.B) {
                    a(this.c, true, 0, false);
                    z2 = h();
                    break;
                }
                break;
            case 5:
                int actionIndex = motionEvent.getActionIndex();
                this.G = motionEvent.getX(actionIndex);
                this.K = motionEvent.getPointerId(actionIndex);
                break;
            case 6:
                a(motionEvent);
                this.G = motionEvent.getX(motionEvent.findPointerIndex(this.K));
                break;
        }
        if (z2) {
            t.c(this);
        }
        return true;
    }

    public void removeView(View view) {
        if (this.x) {
            removeViewInLayout(view);
        } else {
            super.removeView(view);
        }
    }

    public void setAdapter(q qVar) {
        if (this.f756b != null) {
            this.f756b.c((DataSetObserver) null);
            this.f756b.a((ViewGroup) this);
            for (int i2 = 0; i2 < this.g.size(); i2++) {
                b bVar = (b) this.g.get(i2);
                this.f756b.a((ViewGroup) this, bVar.f761b, bVar.f760a);
            }
            this.f756b.b((ViewGroup) this);
            this.g.clear();
            f();
            this.c = 0;
            scrollTo(0, 0);
        }
        q qVar2 = this.f756b;
        this.f756b = qVar;
        this.d = 0;
        if (this.f756b != null) {
            if (this.o == null) {
                this.o = new h();
            }
            this.f756b.c((DataSetObserver) this.o);
            this.z = false;
            boolean z2 = this.T;
            this.T = true;
            this.d = this.f756b.b();
            if (this.j >= 0) {
                this.f756b.a(this.k, this.l);
                a(this.j, false, true);
                this.j = -1;
                this.k = null;
                this.l = null;
            } else if (!z2) {
                c();
            } else {
                requestLayout();
            }
        }
        if (this.ad != null && !this.ad.isEmpty()) {
            int size = this.ad.size();
            for (int i3 = 0; i3 < size; i3++) {
                ((e) this.ad.get(i3)).a(this, qVar2, qVar);
            }
        }
    }

    public void setCurrentItem(int i2) {
        this.z = false;
        a(i2, !this.T, false);
    }

    public void setOffscreenPageLimit(int i2) {
        if (i2 < 1) {
            Log.w("ViewPager", "Requested offscreen page limit " + i2 + " too small; defaulting to " + 1);
            i2 = 1;
        }
        if (i2 != this.A) {
            this.A = i2;
            c();
        }
    }

    @Deprecated
    public void setOnPageChangeListener(f fVar) {
        this.ab = fVar;
    }

    public void setPageMargin(int i2) {
        int i3 = this.p;
        this.p = i2;
        int width = getWidth();
        a(width, width, i2, i3);
        requestLayout();
    }

    public void setPageMarginDrawable(int i2) {
        setPageMarginDrawable(android.support.v4.b.a.a(getContext(), i2));
    }

    public void setPageMarginDrawable(Drawable drawable) {
        this.q = drawable;
        if (drawable != null) {
            refreshDrawableState();
        }
        setWillNotDraw(drawable == null);
        invalidate();
    }

    /* access modifiers changed from: 0000 */
    public void setScrollState(int i2) {
        if (this.ak != i2) {
            this.ak = i2;
            if (this.ae != null) {
                b(i2 != 0);
            }
            f(i2);
        }
    }

    /* access modifiers changed from: protected */
    public boolean verifyDrawable(Drawable drawable) {
        return super.verifyDrawable(drawable) || drawable == this.q;
    }
}
