package android.support.v4.i;

import android.os.Build.VERSION;
import android.view.WindowInsets;

public class ac {

    /* renamed from: a reason: collision with root package name */
    private final Object f719a;

    private ac(Object obj) {
        this.f719a = obj;
    }

    static ac a(Object obj) {
        if (obj == null) {
            return null;
        }
        return new ac(obj);
    }

    static Object a(ac acVar) {
        if (acVar == null) {
            return null;
        }
        return acVar.f719a;
    }

    public int a() {
        if (VERSION.SDK_INT >= 20) {
            return ((WindowInsets) this.f719a).getSystemWindowInsetLeft();
        }
        return 0;
    }

    public ac a(int i, int i2, int i3, int i4) {
        if (VERSION.SDK_INT >= 20) {
            return new ac(((WindowInsets) this.f719a).replaceSystemWindowInsets(i, i2, i3, i4));
        }
        return null;
    }

    public int b() {
        if (VERSION.SDK_INT >= 20) {
            return ((WindowInsets) this.f719a).getSystemWindowInsetTop();
        }
        return 0;
    }

    public int c() {
        if (VERSION.SDK_INT >= 20) {
            return ((WindowInsets) this.f719a).getSystemWindowInsetRight();
        }
        return 0;
    }

    public int d() {
        if (VERSION.SDK_INT >= 20) {
            return ((WindowInsets) this.f719a).getSystemWindowInsetBottom();
        }
        return 0;
    }

    public boolean e() {
        if (VERSION.SDK_INT >= 21) {
            return ((WindowInsets) this.f719a).isConsumed();
        }
        return false;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        ac acVar = (ac) obj;
        return this.f719a == null ? acVar.f719a == null : this.f719a.equals(acVar.f719a);
    }

    public int hashCode() {
        if (this.f719a == null) {
            return 0;
        }
        return this.f719a.hashCode();
    }
}
