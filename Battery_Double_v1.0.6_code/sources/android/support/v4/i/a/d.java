package android.support.v4.i.a;

import android.os.Build.VERSION;
import android.view.accessibility.AccessibilityRecord;

public class d {

    /* renamed from: a reason: collision with root package name */
    private final AccessibilityRecord f718a;

    public static void a(AccessibilityRecord accessibilityRecord, int i) {
        if (VERSION.SDK_INT >= 15) {
            accessibilityRecord.setMaxScrollX(i);
        }
    }

    public static void b(AccessibilityRecord accessibilityRecord, int i) {
        if (VERSION.SDK_INT >= 15) {
            accessibilityRecord.setMaxScrollY(i);
        }
    }

    @Deprecated
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        d dVar = (d) obj;
        return this.f718a == null ? dVar.f718a == null : this.f718a.equals(dVar.f718a);
    }

    @Deprecated
    public int hashCode() {
        if (this.f718a == null) {
            return 0;
        }
        return this.f718a.hashCode();
    }
}
