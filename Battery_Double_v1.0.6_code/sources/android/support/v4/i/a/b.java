package android.support.v4.i.a;

import android.graphics.Rect;
import android.os.Build.VERSION;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.accessibility.AccessibilityNodeInfo.CollectionInfo;
import android.view.accessibility.AccessibilityNodeInfo.CollectionItemInfo;

public class b {

    /* renamed from: a reason: collision with root package name */
    public int f712a = -1;

    /* renamed from: b reason: collision with root package name */
    private final AccessibilityNodeInfo f713b;

    public static class a {

        /* renamed from: a reason: collision with root package name */
        final Object f714a;

        a(Object obj) {
            this.f714a = obj;
        }

        public static a a(int i, int i2, boolean z, int i3) {
            return VERSION.SDK_INT >= 21 ? new a(CollectionInfo.obtain(i, i2, z, i3)) : VERSION.SDK_INT >= 19 ? new a(CollectionInfo.obtain(i, i2, z)) : new a(null);
        }
    }

    /* renamed from: android.support.v4.i.a.b$b reason: collision with other inner class name */
    public static class C0017b {

        /* renamed from: a reason: collision with root package name */
        final Object f715a;

        C0017b(Object obj) {
            this.f715a = obj;
        }

        public static C0017b a(int i, int i2, int i3, int i4, boolean z, boolean z2) {
            return VERSION.SDK_INT >= 21 ? new C0017b(CollectionItemInfo.obtain(i, i2, i3, i4, z, z2)) : VERSION.SDK_INT >= 19 ? new C0017b(CollectionItemInfo.obtain(i, i2, i3, i4, z)) : new C0017b(null);
        }
    }

    private b(AccessibilityNodeInfo accessibilityNodeInfo) {
        this.f713b = accessibilityNodeInfo;
    }

    public static b a(AccessibilityNodeInfo accessibilityNodeInfo) {
        return new b(accessibilityNodeInfo);
    }

    private static String b(int i) {
        switch (i) {
            case 1:
                return "ACTION_FOCUS";
            case 2:
                return "ACTION_CLEAR_FOCUS";
            case 4:
                return "ACTION_SELECT";
            case 8:
                return "ACTION_CLEAR_SELECTION";
            case 16:
                return "ACTION_CLICK";
            case 32:
                return "ACTION_LONG_CLICK";
            case 64:
                return "ACTION_ACCESSIBILITY_FOCUS";
            case 128:
                return "ACTION_CLEAR_ACCESSIBILITY_FOCUS";
            case 256:
                return "ACTION_NEXT_AT_MOVEMENT_GRANULARITY";
            case 512:
                return "ACTION_PREVIOUS_AT_MOVEMENT_GRANULARITY";
            case 1024:
                return "ACTION_NEXT_HTML_ELEMENT";
            case 2048:
                return "ACTION_PREVIOUS_HTML_ELEMENT";
            case 4096:
                return "ACTION_SCROLL_FORWARD";
            case 8192:
                return "ACTION_SCROLL_BACKWARD";
            case 16384:
                return "ACTION_COPY";
            case 32768:
                return "ACTION_PASTE";
            case 65536:
                return "ACTION_CUT";
            case 131072:
                return "ACTION_SET_SELECTION";
            default:
                return "ACTION_UNKNOWN";
        }
    }

    public AccessibilityNodeInfo a() {
        return this.f713b;
    }

    public void a(int i) {
        this.f713b.addAction(i);
    }

    public void a(Rect rect) {
        this.f713b.getBoundsInParent(rect);
    }

    public void a(CharSequence charSequence) {
        this.f713b.setClassName(charSequence);
    }

    public void a(Object obj) {
        if (VERSION.SDK_INT >= 19) {
            this.f713b.setCollectionInfo((CollectionInfo) ((a) obj).f714a);
        }
    }

    public void a(boolean z) {
        this.f713b.setCheckable(z);
    }

    public int b() {
        return this.f713b.getActions();
    }

    public void b(Rect rect) {
        this.f713b.getBoundsInScreen(rect);
    }

    public void b(Object obj) {
        if (VERSION.SDK_INT >= 19) {
            this.f713b.setCollectionItemInfo((CollectionItemInfo) ((C0017b) obj).f715a);
        }
    }

    public void b(boolean z) {
        this.f713b.setChecked(z);
    }

    public void c(boolean z) {
        this.f713b.setScrollable(z);
    }

    public boolean c() {
        return this.f713b.isCheckable();
    }

    public boolean d() {
        return this.f713b.isChecked();
    }

    public boolean e() {
        return this.f713b.isFocusable();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        b bVar = (b) obj;
        return this.f713b == null ? bVar.f713b == null : this.f713b.equals(bVar.f713b);
    }

    public boolean f() {
        return this.f713b.isFocused();
    }

    public boolean g() {
        return this.f713b.isSelected();
    }

    public boolean h() {
        return this.f713b.isClickable();
    }

    public int hashCode() {
        if (this.f713b == null) {
            return 0;
        }
        return this.f713b.hashCode();
    }

    public boolean i() {
        return this.f713b.isLongClickable();
    }

    public boolean j() {
        return this.f713b.isEnabled();
    }

    public boolean k() {
        return this.f713b.isPassword();
    }

    public boolean l() {
        return this.f713b.isScrollable();
    }

    public CharSequence m() {
        return this.f713b.getPackageName();
    }

    public CharSequence n() {
        return this.f713b.getClassName();
    }

    public CharSequence o() {
        return this.f713b.getText();
    }

    public CharSequence p() {
        return this.f713b.getContentDescription();
    }

    public String q() {
        if (VERSION.SDK_INT >= 18) {
            return this.f713b.getViewIdResourceName();
        }
        return null;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(super.toString());
        Rect rect = new Rect();
        a(rect);
        sb.append("; boundsInParent: " + rect);
        b(rect);
        sb.append("; boundsInScreen: " + rect);
        sb.append("; packageName: ").append(m());
        sb.append("; className: ").append(n());
        sb.append("; text: ").append(o());
        sb.append("; contentDescription: ").append(p());
        sb.append("; viewId: ").append(q());
        sb.append("; checkable: ").append(c());
        sb.append("; checked: ").append(d());
        sb.append("; focusable: ").append(e());
        sb.append("; focused: ").append(f());
        sb.append("; selected: ").append(g());
        sb.append("; clickable: ").append(h());
        sb.append("; longClickable: ").append(i());
        sb.append("; enabled: ").append(j());
        sb.append("; password: ").append(k());
        sb.append("; scrollable: " + l());
        sb.append("; [");
        int b2 = b();
        while (b2 != 0) {
            int numberOfTrailingZeros = 1 << Integer.numberOfTrailingZeros(b2);
            b2 &= numberOfTrailingZeros ^ -1;
            sb.append(b(numberOfTrailingZeros));
            if (b2 != 0) {
                sb.append(", ");
            }
        }
        sb.append("]");
        return sb.toString();
    }
}
