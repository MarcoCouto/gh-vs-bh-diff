package android.support.v4.i.a;

import android.os.Build.VERSION;
import android.os.Bundle;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.accessibility.AccessibilityNodeProvider;
import java.util.ArrayList;
import java.util.List;

public class c {

    /* renamed from: a reason: collision with root package name */
    private final Object f716a;

    static class a extends AccessibilityNodeProvider {

        /* renamed from: a reason: collision with root package name */
        final c f717a;

        a(c cVar) {
            this.f717a = cVar;
        }

        public AccessibilityNodeInfo createAccessibilityNodeInfo(int i) {
            b a2 = this.f717a.a(i);
            if (a2 == null) {
                return null;
            }
            return a2.a();
        }

        public List<AccessibilityNodeInfo> findAccessibilityNodeInfosByText(String str, int i) {
            List a2 = this.f717a.a(str, i);
            if (a2 == null) {
                return null;
            }
            ArrayList arrayList = new ArrayList();
            int size = a2.size();
            for (int i2 = 0; i2 < size; i2++) {
                arrayList.add(((b) a2.get(i2)).a());
            }
            return arrayList;
        }

        public boolean performAction(int i, int i2, Bundle bundle) {
            return this.f717a.a(i, i2, bundle);
        }
    }

    static class b extends a {
        b(c cVar) {
            super(cVar);
        }

        public AccessibilityNodeInfo findFocus(int i) {
            b b2 = this.f717a.b(i);
            if (b2 == null) {
                return null;
            }
            return b2.a();
        }
    }

    public c() {
        if (VERSION.SDK_INT >= 19) {
            this.f716a = new b(this);
        } else if (VERSION.SDK_INT >= 16) {
            this.f716a = new a(this);
        } else {
            this.f716a = null;
        }
    }

    public c(Object obj) {
        this.f716a = obj;
    }

    public b a(int i) {
        return null;
    }

    public Object a() {
        return this.f716a;
    }

    public List<b> a(String str, int i) {
        return null;
    }

    public boolean a(int i, int i2, Bundle bundle) {
        return false;
    }

    public b b(int i) {
        return null;
    }
}
