package android.support.v4.i.b;

import android.view.animation.Interpolator;

abstract class d implements Interpolator {

    /* renamed from: a reason: collision with root package name */
    private final float[] f729a;

    /* renamed from: b reason: collision with root package name */
    private final float f730b = (1.0f / ((float) (this.f729a.length - 1)));

    public d(float[] fArr) {
        this.f729a = fArr;
    }

    public float getInterpolation(float f) {
        if (f >= 1.0f) {
            return 1.0f;
        }
        if (f <= 0.0f) {
            return 0.0f;
        }
        int min = Math.min((int) (((float) (this.f729a.length - 1)) * f), this.f729a.length - 2);
        float f2 = (f - (((float) min) * this.f730b)) / this.f730b;
        return ((this.f729a[min + 1] - this.f729a[min]) * f2) + this.f729a[min];
    }
}
