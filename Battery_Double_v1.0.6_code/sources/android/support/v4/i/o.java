package android.support.v4.i;

import android.view.View;
import android.view.ViewGroup;

public class o {

    /* renamed from: a reason: collision with root package name */
    private final ViewGroup f743a;

    /* renamed from: b reason: collision with root package name */
    private int f744b;

    public o(ViewGroup viewGroup) {
        this.f743a = viewGroup;
    }

    public int a() {
        return this.f744b;
    }

    public void a(View view) {
        a(view, 0);
    }

    public void a(View view, int i) {
        this.f744b = 0;
    }

    public void a(View view, View view2, int i) {
        a(view, view2, i, 0);
    }

    public void a(View view, View view2, int i, int i2) {
        this.f744b = i;
    }
}
