package android.support.v4.i;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.os.Build.VERSION;
import android.view.View;
import android.view.animation.Interpolator;
import java.lang.ref.WeakReference;

public final class y {

    /* renamed from: a reason: collision with root package name */
    Runnable f769a = null;

    /* renamed from: b reason: collision with root package name */
    Runnable f770b = null;
    int c = -1;
    private WeakReference<View> d;

    static class a implements z {

        /* renamed from: a reason: collision with root package name */
        y f775a;

        /* renamed from: b reason: collision with root package name */
        boolean f776b;

        a(y yVar) {
            this.f775a = yVar;
        }

        public void a(View view) {
            this.f776b = false;
            if (this.f775a.c > -1) {
                view.setLayerType(2, null);
            }
            if (this.f775a.f769a != null) {
                Runnable runnable = this.f775a.f769a;
                this.f775a.f769a = null;
                runnable.run();
            }
            Object tag = view.getTag(2113929216);
            z zVar = tag instanceof z ? (z) tag : null;
            if (zVar != null) {
                zVar.a(view);
            }
        }

        public void b(View view) {
            if (this.f775a.c > -1) {
                view.setLayerType(this.f775a.c, null);
                this.f775a.c = -1;
            }
            if (VERSION.SDK_INT >= 16 || !this.f776b) {
                if (this.f775a.f770b != null) {
                    Runnable runnable = this.f775a.f770b;
                    this.f775a.f770b = null;
                    runnable.run();
                }
                Object tag = view.getTag(2113929216);
                z zVar = tag instanceof z ? (z) tag : null;
                if (zVar != null) {
                    zVar.b(view);
                }
                this.f776b = true;
            }
        }

        public void c(View view) {
            Object tag = view.getTag(2113929216);
            z zVar = tag instanceof z ? (z) tag : null;
            if (zVar != null) {
                zVar.c(view);
            }
        }
    }

    y(View view) {
        this.d = new WeakReference<>(view);
    }

    private void a(final View view, final z zVar) {
        if (zVar != null) {
            view.animate().setListener(new AnimatorListenerAdapter() {
                public void onAnimationCancel(Animator animator) {
                    zVar.c(view);
                }

                public void onAnimationEnd(Animator animator) {
                    zVar.b(view);
                }

                public void onAnimationStart(Animator animator) {
                    zVar.a(view);
                }
            });
        } else {
            view.animate().setListener(null);
        }
    }

    public long a() {
        View view = (View) this.d.get();
        if (view != null) {
            return view.animate().getDuration();
        }
        return 0;
    }

    public y a(float f) {
        View view = (View) this.d.get();
        if (view != null) {
            view.animate().alpha(f);
        }
        return this;
    }

    public y a(long j) {
        View view = (View) this.d.get();
        if (view != null) {
            view.animate().setDuration(j);
        }
        return this;
    }

    public y a(final ab abVar) {
        final View view = (View) this.d.get();
        if (view != null && VERSION.SDK_INT >= 19) {
            AnonymousClass2 r1 = null;
            if (abVar != null) {
                r1 = new AnimatorUpdateListener() {
                    public void onAnimationUpdate(ValueAnimator valueAnimator) {
                        abVar.a(view);
                    }
                };
            }
            view.animate().setUpdateListener(r1);
        }
        return this;
    }

    public y a(z zVar) {
        View view = (View) this.d.get();
        if (view != null) {
            if (VERSION.SDK_INT >= 16) {
                a(view, zVar);
            } else {
                view.setTag(2113929216, zVar);
                a(view, new a(this));
            }
        }
        return this;
    }

    public y a(Interpolator interpolator) {
        View view = (View) this.d.get();
        if (view != null) {
            view.animate().setInterpolator(interpolator);
        }
        return this;
    }

    public y b(float f) {
        View view = (View) this.d.get();
        if (view != null) {
            view.animate().translationY(f);
        }
        return this;
    }

    public y b(long j) {
        View view = (View) this.d.get();
        if (view != null) {
            view.animate().setStartDelay(j);
        }
        return this;
    }

    public void b() {
        View view = (View) this.d.get();
        if (view != null) {
            view.animate().cancel();
        }
    }

    public void c() {
        View view = (View) this.d.get();
        if (view != null) {
            view.animate().start();
        }
    }
}
