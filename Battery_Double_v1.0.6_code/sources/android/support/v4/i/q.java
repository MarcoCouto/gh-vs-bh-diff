package android.support.v4.i;

import android.database.DataSetObservable;
import android.database.DataSetObserver;
import android.os.Parcelable;
import android.view.View;
import android.view.ViewGroup;

public abstract class q {

    /* renamed from: a reason: collision with root package name */
    private final DataSetObservable f745a = new DataSetObservable();

    /* renamed from: b reason: collision with root package name */
    private DataSetObserver f746b;

    public int a(Object obj) {
        return -1;
    }

    public Parcelable a() {
        return null;
    }

    @Deprecated
    public Object a(View view, int i) {
        throw new UnsupportedOperationException("Required method instantiateItem was not overridden");
    }

    public Object a(ViewGroup viewGroup, int i) {
        return a((View) viewGroup, i);
    }

    public void a(DataSetObserver dataSetObserver) {
        this.f745a.registerObserver(dataSetObserver);
    }

    public void a(Parcelable parcelable, ClassLoader classLoader) {
    }

    @Deprecated
    public void a(View view) {
    }

    @Deprecated
    public void a(View view, int i, Object obj) {
        throw new UnsupportedOperationException("Required method destroyItem was not overridden");
    }

    public void a(ViewGroup viewGroup) {
        a((View) viewGroup);
    }

    public void a(ViewGroup viewGroup, int i, Object obj) {
        a((View) viewGroup, i, obj);
    }

    public abstract boolean a(View view, Object obj);

    public abstract int b();

    public CharSequence b(int i) {
        return null;
    }

    public void b(DataSetObserver dataSetObserver) {
        this.f745a.unregisterObserver(dataSetObserver);
    }

    @Deprecated
    public void b(View view) {
    }

    @Deprecated
    public void b(View view, int i, Object obj) {
    }

    public void b(ViewGroup viewGroup) {
        b((View) viewGroup);
    }

    public void b(ViewGroup viewGroup, int i, Object obj) {
        b((View) viewGroup, i, obj);
    }

    public float c(int i) {
        return 1.0f;
    }

    /* access modifiers changed from: 0000 */
    public void c(DataSetObserver dataSetObserver) {
        synchronized (this) {
            this.f746b = dataSetObserver;
        }
    }
}
