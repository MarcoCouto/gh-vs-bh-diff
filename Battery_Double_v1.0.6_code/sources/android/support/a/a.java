package android.support.a;

import com.mansoon.BatteryDouble.R;

public final class a {

    /* renamed from: android.support.a.a$a reason: collision with other inner class name */
    public static final class C0005a {
        public static final int action_container = 2131296273;
        public static final int action_divider = 2131296275;
        public static final int action_image = 2131296276;
        public static final int action_text = 2131296287;
        public static final int actions = 2131296288;
        public static final int async = 2131296312;
        public static final int blocking = 2131296328;
        public static final int chronometer = 2131296348;
        public static final int forever = 2131296386;
        public static final int icon = 2131296392;
        public static final int icon_group = 2131296393;
        public static final int info = 2131296402;
        public static final int italic = 2131296406;
        public static final int line1 = 2131296415;
        public static final int line3 = 2131296416;
        public static final int normal = 2131296449;
        public static final int notification_background = 2131296450;
        public static final int notification_main_column = 2131296451;
        public static final int notification_main_column_container = 2131296452;
        public static final int right_icon = 2131296465;
        public static final int right_side = 2131296466;
        public static final int tag_transition_group = 2131296519;
        public static final int text = 2131296529;
        public static final int text2 = 2131296530;
        public static final int time = 2131296536;
        public static final int title = 2131296537;
    }

    public static final class b {
        public static final int[] FontFamily = {R.attr.fontProviderAuthority, R.attr.fontProviderCerts, R.attr.fontProviderFetchStrategy, R.attr.fontProviderFetchTimeout, R.attr.fontProviderPackage, R.attr.fontProviderQuery};
        public static final int[] FontFamilyFont = {16844082, 16844083, 16844095, R.attr.font, R.attr.fontStyle, R.attr.fontWeight};
        public static final int FontFamilyFont_android_font = 0;
        public static final int FontFamilyFont_android_fontStyle = 2;
        public static final int FontFamilyFont_android_fontWeight = 1;
        public static final int FontFamilyFont_font = 3;
        public static final int FontFamilyFont_fontStyle = 4;
        public static final int FontFamilyFont_fontWeight = 5;
        public static final int FontFamily_fontProviderAuthority = 0;
        public static final int FontFamily_fontProviderCerts = 1;
        public static final int FontFamily_fontProviderFetchStrategy = 2;
        public static final int FontFamily_fontProviderFetchTimeout = 3;
        public static final int FontFamily_fontProviderPackage = 4;
        public static final int FontFamily_fontProviderQuery = 5;
    }
}
