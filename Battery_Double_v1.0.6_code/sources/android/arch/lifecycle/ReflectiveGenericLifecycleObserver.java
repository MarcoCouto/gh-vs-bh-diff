package android.arch.lifecycle;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

class ReflectiveGenericLifecycleObserver implements a {

    /* renamed from: a reason: collision with root package name */
    static final Map<Class, a> f232a = new HashMap();

    /* renamed from: b reason: collision with root package name */
    private final Object f233b;
    private final a c = a(this.f233b.getClass());

    static class a {

        /* renamed from: a reason: collision with root package name */
        final Map<android.arch.lifecycle.b.a, List<b>> f234a = new HashMap();

        /* renamed from: b reason: collision with root package name */
        final Map<b, android.arch.lifecycle.b.a> f235b;

        a(Map<b, android.arch.lifecycle.b.a> map) {
            this.f235b = map;
            for (Entry entry : map.entrySet()) {
                android.arch.lifecycle.b.a aVar = (android.arch.lifecycle.b.a) entry.getValue();
                List list = (List) this.f234a.get(aVar);
                if (list == null) {
                    list = new ArrayList();
                    this.f234a.put(aVar, list);
                }
                list.add(entry.getKey());
            }
        }
    }

    static class b {

        /* renamed from: a reason: collision with root package name */
        final int f236a;

        /* renamed from: b reason: collision with root package name */
        final Method f237b;

        b(int i, Method method) {
            this.f236a = i;
            this.f237b = method;
            this.f237b.setAccessible(true);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            b bVar = (b) obj;
            return this.f236a == bVar.f236a && this.f237b.getName().equals(bVar.f237b.getName());
        }

        public int hashCode() {
            return (this.f236a * 31) + this.f237b.getName().hashCode();
        }
    }

    ReflectiveGenericLifecycleObserver(Object obj) {
        this.f233b = obj;
    }

    private static a a(Class cls) {
        a aVar = (a) f232a.get(cls);
        return aVar != null ? aVar : b(cls);
    }

    private void a(a aVar, c cVar, android.arch.lifecycle.b.a aVar2) {
        a((List) aVar.f234a.get(aVar2), cVar, aVar2);
        a((List) aVar.f234a.get(android.arch.lifecycle.b.a.ON_ANY), cVar, aVar2);
    }

    private void a(b bVar, c cVar, android.arch.lifecycle.b.a aVar) {
        try {
            switch (bVar.f236a) {
                case 0:
                    bVar.f237b.invoke(this.f233b, new Object[0]);
                    return;
                case 1:
                    bVar.f237b.invoke(this.f233b, new Object[]{cVar});
                    return;
                case 2:
                    bVar.f237b.invoke(this.f233b, new Object[]{cVar, aVar});
                    return;
                default:
                    return;
            }
        } catch (InvocationTargetException e) {
            throw new RuntimeException("Failed to call observer method", e.getCause());
        } catch (IllegalAccessException e2) {
            throw new RuntimeException(e2);
        }
    }

    private void a(List<b> list, c cVar, android.arch.lifecycle.b.a aVar) {
        if (list != null) {
            for (int size = list.size() - 1; size >= 0; size--) {
                a((b) list.get(size), cVar, aVar);
            }
        }
    }

    private static void a(Map<b, android.arch.lifecycle.b.a> map, b bVar, android.arch.lifecycle.b.a aVar, Class cls) {
        android.arch.lifecycle.b.a aVar2 = (android.arch.lifecycle.b.a) map.get(bVar);
        if (aVar2 != null && aVar != aVar2) {
            throw new IllegalArgumentException("Method " + bVar.f237b.getName() + " in " + cls.getName() + " already declared with different @OnLifecycleEvent value: previous" + " value " + aVar2 + ", new value " + aVar);
        } else if (aVar2 == null) {
            map.put(bVar, aVar);
        }
    }

    private static a b(Class cls) {
        int i;
        Class superclass = cls.getSuperclass();
        HashMap hashMap = new HashMap();
        if (superclass != null) {
            a a2 = a(superclass);
            if (a2 != null) {
                hashMap.putAll(a2.f235b);
            }
        }
        Method[] declaredMethods = cls.getDeclaredMethods();
        for (Class a3 : cls.getInterfaces()) {
            for (Entry entry : a(a3).f235b.entrySet()) {
                a(hashMap, (b) entry.getKey(), (android.arch.lifecycle.b.a) entry.getValue(), cls);
            }
        }
        for (Method method : declaredMethods) {
            f fVar = (f) method.getAnnotation(f.class);
            if (fVar != null) {
                Class[] parameterTypes = method.getParameterTypes();
                if (parameterTypes.length <= 0) {
                    i = 0;
                } else if (!parameterTypes[0].isAssignableFrom(c.class)) {
                    throw new IllegalArgumentException("invalid parameter type. Must be one and instanceof LifecycleOwner");
                } else {
                    i = 1;
                }
                android.arch.lifecycle.b.a a4 = fVar.a();
                if (parameterTypes.length > 1) {
                    if (!parameterTypes[1].isAssignableFrom(android.arch.lifecycle.b.a.class)) {
                        throw new IllegalArgumentException("invalid parameter type. second arg must be an event");
                    } else if (a4 != android.arch.lifecycle.b.a.ON_ANY) {
                        throw new IllegalArgumentException("Second arg is supported only for ON_ANY value");
                    } else {
                        i = 2;
                    }
                }
                if (parameterTypes.length > 2) {
                    throw new IllegalArgumentException("cannot have more than 2 params");
                }
                a(hashMap, new b(i, method), a4, cls);
            }
        }
        a aVar = new a(hashMap);
        f232a.put(cls, aVar);
        return aVar;
    }

    public void a(c cVar, android.arch.lifecycle.b.a aVar) {
        a(this.c, cVar, aVar);
    }
}
