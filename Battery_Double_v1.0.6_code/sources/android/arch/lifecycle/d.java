package android.arch.lifecycle;

import android.arch.lifecycle.b.C0004b;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map.Entry;

public class d extends b {

    /* renamed from: a reason: collision with root package name */
    private android.arch.a.a.a<Object, a> f240a = new android.arch.a.a.a<>();

    /* renamed from: b reason: collision with root package name */
    private C0004b f241b;
    private final c c;
    private int d = 0;
    private boolean e = false;
    private boolean f = false;
    private ArrayList<C0004b> g = new ArrayList<>();

    static class a {

        /* renamed from: a reason: collision with root package name */
        C0004b f244a;

        /* renamed from: b reason: collision with root package name */
        a f245b;

        /* access modifiers changed from: 0000 */
        public void a(c cVar, android.arch.lifecycle.b.a aVar) {
            C0004b b2 = d.b(aVar);
            this.f244a = d.a(this.f244a, b2);
            this.f245b.a(cVar, aVar);
            this.f244a = b2;
        }
    }

    public d(c cVar) {
        this.c = cVar;
        this.f241b = C0004b.INITIALIZED;
    }

    static C0004b a(C0004b bVar, C0004b bVar2) {
        return (bVar2 == null || bVar2.compareTo(bVar) >= 0) ? bVar : bVar2;
    }

    private boolean a() {
        if (this.f240a.a() == 0) {
            return true;
        }
        C0004b bVar = ((a) this.f240a.d().getValue()).f244a;
        C0004b bVar2 = ((a) this.f240a.e().getValue()).f244a;
        return bVar == bVar2 && this.f241b == bVar2;
    }

    static C0004b b(android.arch.lifecycle.b.a aVar) {
        switch (aVar) {
            case ON_CREATE:
            case ON_STOP:
                return C0004b.CREATED;
            case ON_START:
            case ON_PAUSE:
                return C0004b.STARTED;
            case ON_RESUME:
                return C0004b.RESUMED;
            case ON_DESTROY:
                return C0004b.DESTROYED;
            default:
                throw new IllegalArgumentException("Unexpected event value " + aVar);
        }
    }

    private void b() {
        this.g.remove(this.g.size() - 1);
    }

    private void b(C0004b bVar) {
        this.g.add(bVar);
    }

    private static android.arch.lifecycle.b.a c(C0004b bVar) {
        switch (bVar) {
            case INITIALIZED:
                throw new IllegalArgumentException();
            case CREATED:
                return android.arch.lifecycle.b.a.ON_DESTROY;
            case STARTED:
                return android.arch.lifecycle.b.a.ON_STOP;
            case RESUMED:
                return android.arch.lifecycle.b.a.ON_PAUSE;
            case DESTROYED:
                throw new IllegalArgumentException();
            default:
                throw new IllegalArgumentException("Unexpected state value " + bVar);
        }
    }

    private void c() {
        d c2 = this.f240a.c();
        while (c2.hasNext() && !this.f) {
            Entry entry = (Entry) c2.next();
            a aVar = (a) entry.getValue();
            while (aVar.f244a.compareTo(this.f241b) < 0 && !this.f && this.f240a.a(entry.getKey())) {
                b(aVar.f244a);
                aVar.a(this.c, d(aVar.f244a));
                b();
            }
        }
    }

    private static android.arch.lifecycle.b.a d(C0004b bVar) {
        switch (bVar) {
            case INITIALIZED:
            case DESTROYED:
                return android.arch.lifecycle.b.a.ON_CREATE;
            case CREATED:
                return android.arch.lifecycle.b.a.ON_START;
            case STARTED:
                return android.arch.lifecycle.b.a.ON_RESUME;
            case RESUMED:
                throw new IllegalArgumentException();
            default:
                throw new IllegalArgumentException("Unexpected state value " + bVar);
        }
    }

    private void d() {
        Iterator b2 = this.f240a.b();
        while (b2.hasNext() && !this.f) {
            Entry entry = (Entry) b2.next();
            a aVar = (a) entry.getValue();
            while (aVar.f244a.compareTo(this.f241b) > 0 && !this.f && this.f240a.a(entry.getKey())) {
                android.arch.lifecycle.b.a c2 = c(aVar.f244a);
                b(b(c2));
                aVar.a(this.c, c2);
                b();
            }
        }
    }

    private void e() {
        while (!a()) {
            this.f = false;
            if (this.f241b.compareTo(((a) this.f240a.d().getValue()).f244a) < 0) {
                d();
            }
            Entry e2 = this.f240a.e();
            if (!this.f && e2 != null && this.f241b.compareTo(((a) e2.getValue()).f244a) > 0) {
                c();
            }
        }
        this.f = false;
    }

    public void a(android.arch.lifecycle.b.a aVar) {
        this.f241b = b(aVar);
        if (this.e || this.d != 0) {
            this.f = true;
            return;
        }
        this.e = true;
        e();
        this.e = false;
    }

    public void a(C0004b bVar) {
        this.f241b = bVar;
    }
}
