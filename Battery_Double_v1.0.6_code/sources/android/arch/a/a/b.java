package android.arch.a.a;

import java.util.Iterator;
import java.util.Map.Entry;
import java.util.WeakHashMap;

public class b<K, V> implements Iterable<Entry<K, V>> {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public c<K, V> f224a;

    /* renamed from: b reason: collision with root package name */
    private c<K, V> f225b;
    private WeakHashMap<Object<K, V>, Boolean> c = new WeakHashMap<>();
    private int d = 0;

    static class a<K, V> extends e<K, V> {
        a(c<K, V> cVar, c<K, V> cVar2) {
            super(cVar, cVar2);
        }

        /* access modifiers changed from: 0000 */
        public c<K, V> a(c<K, V> cVar) {
            return cVar.c;
        }
    }

    /* renamed from: android.arch.a.a.b$b reason: collision with other inner class name */
    private static class C0003b<K, V> extends e<K, V> {
        C0003b(c<K, V> cVar, c<K, V> cVar2) {
            super(cVar, cVar2);
        }

        /* access modifiers changed from: 0000 */
        public c<K, V> a(c<K, V> cVar) {
            return cVar.d;
        }
    }

    static class c<K, V> implements Entry<K, V> {

        /* renamed from: a reason: collision with root package name */
        final K f226a;

        /* renamed from: b reason: collision with root package name */
        final V f227b;
        c<K, V> c;
        c<K, V> d;

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof c)) {
                return false;
            }
            c cVar = (c) obj;
            return this.f226a.equals(cVar.f226a) && this.f227b.equals(cVar.f227b);
        }

        public K getKey() {
            return this.f226a;
        }

        public V getValue() {
            return this.f227b;
        }

        public V setValue(V v) {
            throw new UnsupportedOperationException("An entry modification is not supported");
        }

        public String toString() {
            return this.f226a + "=" + this.f227b;
        }
    }

    private class d implements Iterator<Entry<K, V>> {

        /* renamed from: b reason: collision with root package name */
        private c<K, V> f229b;
        private boolean c;

        private d() {
            this.c = true;
        }

        /* renamed from: a */
        public Entry<K, V> next() {
            if (this.c) {
                this.c = false;
                this.f229b = b.this.f224a;
            } else {
                this.f229b = this.f229b != null ? this.f229b.c : null;
            }
            return this.f229b;
        }

        public boolean hasNext() {
            return this.c ? b.this.f224a != null : (this.f229b == null || this.f229b.c == null) ? false : true;
        }
    }

    private static abstract class e<K, V> implements Iterator<Entry<K, V>> {

        /* renamed from: a reason: collision with root package name */
        c<K, V> f230a;

        /* renamed from: b reason: collision with root package name */
        c<K, V> f231b;

        e(c<K, V> cVar, c<K, V> cVar2) {
            this.f230a = cVar2;
            this.f231b = cVar;
        }

        private c<K, V> b() {
            if (this.f231b == this.f230a || this.f230a == null) {
                return null;
            }
            return a(this.f231b);
        }

        /* access modifiers changed from: 0000 */
        public abstract c<K, V> a(c<K, V> cVar);

        /* renamed from: a */
        public Entry<K, V> next() {
            c<K, V> cVar = this.f231b;
            this.f231b = b();
            return cVar;
        }

        public boolean hasNext() {
            return this.f231b != null;
        }
    }

    public int a() {
        return this.d;
    }

    public Iterator<Entry<K, V>> b() {
        C0003b bVar = new C0003b(this.f225b, this.f224a);
        this.c.put(bVar, Boolean.valueOf(false));
        return bVar;
    }

    public d c() {
        d dVar = new d<>();
        this.c.put(dVar, Boolean.valueOf(false));
        return dVar;
    }

    public Entry<K, V> d() {
        return this.f224a;
    }

    public Entry<K, V> e() {
        return this.f225b;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof b)) {
            return false;
        }
        b bVar = (b) obj;
        if (a() != bVar.a()) {
            return false;
        }
        Iterator it = iterator();
        Iterator it2 = bVar.iterator();
        while (it.hasNext() && it2.hasNext()) {
            Entry entry = (Entry) it.next();
            Object next = it2.next();
            if (entry == null && next != null) {
                return false;
            }
            if (entry != null && !entry.equals(next)) {
                return false;
            }
        }
        return !it.hasNext() && !it2.hasNext();
    }

    public Iterator<Entry<K, V>> iterator() {
        a aVar = new a(this.f224a, this.f225b);
        this.c.put(aVar, Boolean.valueOf(false));
        return aVar;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        Iterator it = iterator();
        while (it.hasNext()) {
            sb.append(((Entry) it.next()).toString());
            if (it.hasNext()) {
                sb.append(", ");
            }
        }
        sb.append("]");
        return sb.toString();
    }
}
