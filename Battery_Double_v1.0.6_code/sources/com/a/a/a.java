package com.a.a;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import java.io.Closeable;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class a implements com.a.a.c.a {
    private long a(InputStream inputStream, OutputStream outputStream) throws IOException {
        long j = 0;
        byte[] bArr = new byte[4096];
        while (true) {
            int read = inputStream.read(bArr);
            if (read == -1) {
                outputStream.flush();
                return j;
            }
            outputStream.write(bArr, 0, read);
            j += (long) read;
        }
    }

    private void a(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:30:0x007a, code lost:
        if (r2 == null) goto L_0x008d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0081, code lost:
        throw new com.a.a.b(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0082, code lost:
        r2 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0083, code lost:
        r3 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:?, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0094, code lost:
        throw new com.a.a.b(r16);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x0103, code lost:
        r3 = null;
        r2 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x0110, code lost:
        r4 = null;
        r2 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x011b, code lost:
        r4 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x011c, code lost:
        r12 = r4;
        r4 = null;
        r2 = r12;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [B:10:0x001e, B:53:0x00b7] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0086 A[SYNTHETIC, Splitter:B:36:0x0086] */
    public void a(Context context, String[] strArr, String str, File file, d dVar) {
        ZipFile zipFile;
        ZipEntry zipEntry;
        String str2;
        Throwable th;
        FileOutputStream fileOutputStream;
        InputStream inputStream;
        ZipFile zipFile2 = null;
        try {
            ApplicationInfo applicationInfo = context.getApplicationInfo();
            int i = 0;
            while (true) {
                int i2 = i + 1;
                if (i >= 5) {
                    zipFile = null;
                    break;
                }
                try {
                    zipFile = new ZipFile(new File(applicationInfo.sourceDir), 1);
                    break;
                } catch (IOException e) {
                    i = i2;
                }
            }
            if (zipFile == null) {
                dVar.a("FATAL! Couldn't find application APK!");
                if (zipFile != null) {
                    try {
                        zipFile.close();
                    } catch (IOException e2) {
                    }
                }
            } else {
                int i3 = 0;
                while (true) {
                    int i4 = i3 + 1;
                    if (i3 < 5) {
                        int length = strArr.length;
                        ZipEntry zipEntry2 = null;
                        String str3 = null;
                        int i5 = 0;
                        while (true) {
                            if (i5 >= length) {
                                zipEntry = zipEntry2;
                                str2 = str3;
                                break;
                            }
                            str3 = "lib" + File.separatorChar + strArr[i5] + File.separatorChar + str;
                            zipEntry2 = zipFile.getEntry(str3);
                            if (zipEntry2 != null) {
                                zipEntry = zipEntry2;
                                str2 = str3;
                                break;
                            }
                            i5++;
                        }
                        if (str2 != null) {
                            d dVar2 = dVar;
                            dVar2.a("Looking for %s in APK...", str2);
                        }
                        if (zipEntry == null) {
                            break;
                        }
                        d dVar3 = dVar;
                        dVar3.a("Found %s! Extracting...", str2);
                        try {
                            if (file.exists() || file.createNewFile()) {
                                FileOutputStream fileOutputStream2 = null;
                                InputStream inputStream2 = zipFile.getInputStream(zipEntry);
                                try {
                                    fileOutputStream = new FileOutputStream(file);
                                } catch (FileNotFoundException e3) {
                                    fileOutputStream = null;
                                    inputStream = inputStream2;
                                    a(inputStream);
                                    a(fileOutputStream);
                                    i3 = i4;
                                } catch (IOException e4) {
                                    fileOutputStream = null;
                                    a(inputStream2);
                                    a(fileOutputStream);
                                    i3 = i4;
                                } catch (Throwable th2) {
                                    th = th2;
                                    a(inputStream2);
                                    a(fileOutputStream2);
                                    throw th;
                                }
                                try {
                                    long a2 = a(inputStream2, fileOutputStream);
                                    fileOutputStream.getFD().sync();
                                    if (a2 != file.length()) {
                                        a(inputStream2);
                                        a(fileOutputStream);
                                        i3 = i4;
                                    } else {
                                        a(inputStream2);
                                        a(fileOutputStream);
                                        file.setReadable(true, false);
                                        file.setExecutable(true, false);
                                        file.setWritable(true);
                                        if (zipFile != null) {
                                            try {
                                                zipFile.close();
                                                return;
                                            } catch (IOException e5) {
                                                return;
                                            }
                                        } else {
                                            return;
                                        }
                                    }
                                } catch (FileNotFoundException e6) {
                                    inputStream = inputStream2;
                                    a(inputStream);
                                    a(fileOutputStream);
                                    i3 = i4;
                                } catch (IOException e7) {
                                    a(inputStream2);
                                    a(fileOutputStream);
                                    i3 = i4;
                                } catch (Throwable th3) {
                                    Throwable th4 = th3;
                                    fileOutputStream2 = fileOutputStream;
                                    th = th4;
                                    a(inputStream2);
                                    a(fileOutputStream2);
                                    throw th;
                                }
                            } else {
                                i3 = i4;
                            }
                        } catch (IOException e8) {
                            i3 = i4;
                        }
                    } else {
                        dVar.a("FATAL! Couldn't extract the library from the APK!");
                        if (zipFile != null) {
                            try {
                                zipFile.close();
                                return;
                            } catch (IOException e9) {
                                return;
                            }
                        } else {
                            return;
                        }
                    }
                }
            }
        } catch (Throwable th5) {
            th = th5;
            if (zipFile2 != null) {
            }
            throw th;
        }
    }
}
