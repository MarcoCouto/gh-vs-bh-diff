package com.a.a.a;

import com.a.a.a.c.C0036c;
import com.a.a.a.c.b;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class h extends C0036c {
    public h(f fVar, b bVar, long j) throws IOException {
        ByteBuffer allocate = ByteBuffer.allocate(8);
        allocate.order(bVar.f1399a ? ByteOrder.BIG_ENDIAN : ByteOrder.LITTLE_ENDIAN);
        long j2 = bVar.c + (((long) bVar.e) * j);
        this.f1401a = fVar.c(allocate, j2);
        this.f1402b = fVar.b(allocate, 8 + j2);
        this.c = fVar.b(allocate, 16 + j2);
        this.d = fVar.b(allocate, j2 + 40);
    }
}
