package com.a.a.a;

import com.a.a.a.c.C0036c;
import com.a.a.a.c.a;
import com.a.a.a.c.b;
import java.io.Closeable;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class f implements c, Closeable {

    /* renamed from: a reason: collision with root package name */
    private final int f1404a = 1179403647;

    /* renamed from: b reason: collision with root package name */
    private final FileChannel f1405b;

    public f(File file) throws FileNotFoundException {
        if (file == null || !file.exists()) {
            throw new IllegalArgumentException("File is null or does not exist");
        }
        this.f1405b = new FileInputStream(file).getChannel();
    }

    private long a(b bVar, long j, long j2) throws IOException {
        for (long j3 = 0; j3 < j; j3++) {
            C0036c a2 = bVar.a(j3);
            if (a2.f1401a == 1 && a2.c <= j2 && j2 <= a2.c + a2.d) {
                return (j2 - a2.c) + a2.f1402b;
            }
        }
        throw new IllegalStateException("Could not map vma to file offset!");
    }

    public b a() throws IOException {
        this.f1405b.position(0);
        ByteBuffer allocate = ByteBuffer.allocate(8);
        allocate.order(ByteOrder.LITTLE_ENDIAN);
        if (c(allocate, 0) != 1179403647) {
            throw new IllegalArgumentException("Invalid ELF Magic!");
        }
        short e = e(allocate, 4);
        boolean z = e(allocate, 5) == 2;
        if (e == 1) {
            return new d(z, this);
        }
        if (e == 2) {
            return new e(z, this);
        }
        throw new IllegalStateException("Invalid class type!");
    }

    /* access modifiers changed from: protected */
    public String a(ByteBuffer byteBuffer, long j) throws IOException {
        StringBuilder sb = new StringBuilder();
        while (true) {
            long j2 = 1 + j;
            short e = e(byteBuffer, j);
            if (e == 0) {
                return sb.toString();
            }
            sb.append((char) e);
            j = j2;
        }
    }

    /* access modifiers changed from: protected */
    public void a(ByteBuffer byteBuffer, long j, int i) throws IOException {
        byteBuffer.position(0);
        byteBuffer.limit(i);
        long j2 = 0;
        while (j2 < ((long) i)) {
            int read = this.f1405b.read(byteBuffer, j + j2);
            if (read == -1) {
                throw new EOFException();
            }
            j2 += (long) read;
        }
        byteBuffer.position(0);
    }

    /* access modifiers changed from: protected */
    public long b(ByteBuffer byteBuffer, long j) throws IOException {
        a(byteBuffer, j, 8);
        return byteBuffer.getLong();
    }

    public List<String> b() throws IOException {
        long j;
        a a2;
        this.f1405b.position(0);
        ArrayList arrayList = new ArrayList();
        b a3 = a();
        ByteBuffer allocate = ByteBuffer.allocate(8);
        allocate.order(a3.f1399a ? ByteOrder.BIG_ENDIAN : ByteOrder.LITTLE_ENDIAN);
        long j2 = (long) a3.f;
        if (j2 == 65535) {
            j2 = a3.a(0).f1403a;
        }
        long j3 = 0;
        while (true) {
            if (j3 >= j2) {
                j = 0;
                break;
            }
            C0036c a4 = a3.a(j3);
            if (a4.f1401a == 2) {
                j = a4.f1402b;
                break;
            }
            j3++;
        }
        if (j == 0) {
            return Collections.unmodifiableList(arrayList);
        }
        int i = 0;
        ArrayList<Long> arrayList2 = new ArrayList<>();
        long j4 = 0;
        do {
            a2 = a3.a(j, i);
            if (a2.f1397a == 1) {
                arrayList2.add(Long.valueOf(a2.f1398b));
            } else if (a2.f1397a == 5) {
                j4 = a2.f1398b;
            }
            i++;
        } while (a2.f1397a != 0);
        if (j4 == 0) {
            throw new IllegalStateException("String table offset not found!");
        }
        long a5 = a(a3, j2, j4);
        for (Long longValue : arrayList2) {
            arrayList.add(a(allocate, longValue.longValue() + a5));
        }
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public long c(ByteBuffer byteBuffer, long j) throws IOException {
        a(byteBuffer, j, 4);
        return ((long) byteBuffer.getInt()) & 4294967295L;
    }

    public void close() throws IOException {
        this.f1405b.close();
    }

    /* access modifiers changed from: protected */
    public int d(ByteBuffer byteBuffer, long j) throws IOException {
        a(byteBuffer, j, 2);
        return byteBuffer.getShort() & 65535;
    }

    /* access modifiers changed from: protected */
    public short e(ByteBuffer byteBuffer, long j) throws IOException {
        a(byteBuffer, j, 1);
        return (short) (byteBuffer.get() & 255);
    }
}
