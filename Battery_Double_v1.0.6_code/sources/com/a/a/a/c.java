package com.a.a.a;

import java.io.IOException;

public interface c {

    public static abstract class a {

        /* renamed from: a reason: collision with root package name */
        public long f1397a;

        /* renamed from: b reason: collision with root package name */
        public long f1398b;
    }

    public static abstract class b {

        /* renamed from: a reason: collision with root package name */
        public boolean f1399a;

        /* renamed from: b reason: collision with root package name */
        public int f1400b;
        public long c;
        public long d;
        public int e;
        public int f;
        public int g;
        public int h;
        public int i;

        public abstract a a(long j, int i2) throws IOException;

        public abstract C0036c a(long j) throws IOException;

        public abstract d a(int i2) throws IOException;
    }

    /* renamed from: com.a.a.a.c$c reason: collision with other inner class name */
    public static abstract class C0036c {

        /* renamed from: a reason: collision with root package name */
        public long f1401a;

        /* renamed from: b reason: collision with root package name */
        public long f1402b;
        public long c;
        public long d;
    }

    public static abstract class d {

        /* renamed from: a reason: collision with root package name */
        public long f1403a;
    }
}
