package com.a.a.a;

import com.a.a.a.c.C0036c;
import com.a.a.a.c.a;
import com.a.a.a.c.b;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class d extends b {
    private final f j;

    public d(boolean z, f fVar) throws IOException {
        this.f1399a = z;
        this.j = fVar;
        ByteBuffer allocate = ByteBuffer.allocate(4);
        allocate.order(z ? ByteOrder.BIG_ENDIAN : ByteOrder.LITTLE_ENDIAN);
        this.f1400b = fVar.d(allocate, 16);
        this.c = fVar.c(allocate, 28);
        this.d = fVar.c(allocate, 32);
        this.e = fVar.d(allocate, 42);
        this.f = fVar.d(allocate, 44);
        this.g = fVar.d(allocate, 46);
        this.h = fVar.d(allocate, 48);
        this.i = fVar.d(allocate, 50);
    }

    public a a(long j2, int i) throws IOException {
        return new a(this.j, this, j2, i);
    }

    public C0036c a(long j2) throws IOException {
        return new g(this.j, this, j2);
    }

    public com.a.a.a.c.d a(int i) throws IOException {
        return new i(this.j, this, i);
    }
}
