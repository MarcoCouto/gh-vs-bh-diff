package com.a.a.a;

import com.a.a.a.c.b;
import com.a.a.a.c.d;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class j extends d {
    public j(f fVar, b bVar, int i) throws IOException {
        ByteBuffer allocate = ByteBuffer.allocate(8);
        allocate.order(bVar.f1399a ? ByteOrder.BIG_ENDIAN : ByteOrder.LITTLE_ENDIAN);
        this.f1403a = fVar.c(allocate, bVar.d + ((long) (bVar.g * i)) + 44);
    }
}
