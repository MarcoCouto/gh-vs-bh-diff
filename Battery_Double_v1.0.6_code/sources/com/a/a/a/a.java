package com.a.a.a;

import com.a.a.a.c.b;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class a extends com.a.a.a.c.a {
    public a(f fVar, b bVar, long j, int i) throws IOException {
        ByteBuffer allocate = ByteBuffer.allocate(4);
        allocate.order(bVar.f1399a ? ByteOrder.BIG_ENDIAN : ByteOrder.LITTLE_ENDIAN);
        long j2 = ((long) (i * 8)) + j;
        this.f1397a = fVar.c(allocate, j2);
        this.f1398b = fVar.c(allocate, j2 + 4);
    }
}
