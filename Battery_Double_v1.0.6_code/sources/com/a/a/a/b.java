package com.a.a.a;

import com.a.a.a.c.a;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class b extends a {
    public b(f fVar, com.a.a.a.c.b bVar, long j, int i) throws IOException {
        ByteBuffer allocate = ByteBuffer.allocate(4);
        allocate.order(bVar.f1399a ? ByteOrder.BIG_ENDIAN : ByteOrder.LITTLE_ENDIAN);
        long j2 = ((long) (i * 16)) + j;
        this.f1397a = fVar.b(allocate, j2);
        this.f1398b = fVar.b(allocate, j2 + 8);
    }
}
