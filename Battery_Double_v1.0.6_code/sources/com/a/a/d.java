package com.a.a;

import android.content.Context;
import android.util.Log;
import com.a.a.a.f;
import com.a.a.c.C0037c;
import com.a.a.c.a;
import com.a.a.c.b;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

public class d {

    /* renamed from: a reason: collision with root package name */
    protected final Set<String> f1406a;

    /* renamed from: b reason: collision with root package name */
    protected final b f1407b;
    protected final a c;
    protected boolean d;
    protected boolean e;
    protected com.a.a.c.d f;

    protected d() {
        this(new e(), new a());
    }

    protected d(b bVar, a aVar) {
        this.f1406a = new HashSet();
        if (bVar == null) {
            throw new IllegalArgumentException("Cannot pass null library loader");
        } else if (aVar == null) {
            throw new IllegalArgumentException("Cannot pass null library installer");
        } else {
            this.f1407b = bVar;
            this.c = aVar;
        }
    }

    /* access modifiers changed from: private */
    public void c(Context context, String str, String str2) {
        if (!this.f1406a.contains(str) || this.d) {
            try {
                this.f1407b.a(str);
                this.f1406a.add(str);
                a("%s (%s) was loaded normally!", str, str2);
            } catch (UnsatisfiedLinkError e2) {
                a("Loading the library normally failed: %s", Log.getStackTraceString(e2));
                a("%s (%s) was not loaded normally, re-linking...", str, str2);
                File a2 = a(context, str, str2);
                if (!a2.exists() || this.d) {
                    if (this.d) {
                        a("Forcing a re-link of %s (%s)...", str, str2);
                    }
                    b(context, str, str2);
                    this.c.a(context, this.f1407b.a(), this.f1407b.c(str), a2, this);
                }
                try {
                    if (this.e) {
                        for (String d2 : new f(a2).b()) {
                            a(context, this.f1407b.d(d2));
                        }
                    }
                } catch (IOException e3) {
                }
                this.f1407b.b(a2.getAbsolutePath());
                this.f1406a.add(str);
                a("%s (%s) was re-linked!", str, str2);
            }
        } else {
            a("%s already loaded previously!", str);
        }
    }

    /* access modifiers changed from: protected */
    public File a(Context context) {
        return context.getDir("lib", 0);
    }

    /* access modifiers changed from: protected */
    public File a(Context context, String str, String str2) {
        String c2 = this.f1407b.c(str);
        return f.a(str2) ? new File(a(context), c2) : new File(a(context), c2 + "." + str2);
    }

    public void a(Context context, String str) {
        a(context, str, (String) null, (C0037c) null);
    }

    public void a(Context context, String str, String str2, C0037c cVar) {
        if (context == null) {
            throw new IllegalArgumentException("Given context is null");
        } else if (f.a(str)) {
            throw new IllegalArgumentException("Given library is either null or empty");
        } else {
            a("Beginning load of %s...", str);
            if (cVar == null) {
                c(context, str, str2);
                return;
            }
            final Context context2 = context;
            final String str3 = str;
            final String str4 = str2;
            final C0037c cVar2 = cVar;
            new Thread(new Runnable() {
                public void run() {
                    try {
                        d.this.c(context2, str3, str4);
                        cVar2.a();
                    } catch (UnsatisfiedLinkError e2) {
                        cVar2.a(e2);
                    } catch (b e3) {
                        cVar2.a(e3);
                    }
                }
            }).start();
        }
    }

    public void a(String str) {
        if (this.f != null) {
            this.f.a(str);
        }
    }

    public void a(String str, Object... objArr) {
        a(String.format(Locale.US, str, objArr));
    }

    /* access modifiers changed from: protected */
    public void b(Context context, String str, String str2) {
        File a2 = a(context);
        File a3 = a(context, str, str2);
        final String c2 = this.f1407b.c(str);
        File[] listFiles = a2.listFiles(new FilenameFilter() {
            public boolean accept(File file, String str) {
                return str.startsWith(c2);
            }
        });
        if (listFiles != null) {
            for (File file : listFiles) {
                if (this.d || !file.getAbsolutePath().equals(a3.getAbsolutePath())) {
                    file.delete();
                }
            }
        }
    }
}
