package com.github.mikephil.charting.d;

import android.annotation.SuppressLint;
import com.github.mikephil.charting.f.f;

@SuppressLint({"ParcelCreator"})
public class b extends i {

    /* renamed from: a reason: collision with root package name */
    private float[] f1700a;

    /* renamed from: b reason: collision with root package name */
    private f[] f1701b;
    private float c;
    private float d;

    public float[] a() {
        return this.f1700a;
    }

    public float b() {
        return super.b();
    }

    public f[] c() {
        return this.f1701b;
    }

    public boolean d() {
        return this.f1700a != null;
    }

    public float e() {
        return this.d;
    }

    public float f() {
        return this.c;
    }
}
