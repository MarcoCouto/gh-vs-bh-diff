package com.github.mikephil.charting.d;

import com.github.mikephil.charting.d.i;
import java.util.ArrayList;
import java.util.List;

public abstract class h<T extends i> extends e<T> {
    protected List<T> k = null;
    protected float l = -3.4028235E38f;
    protected float m = Float.MAX_VALUE;
    protected float n = -3.4028235E38f;
    protected float o = Float.MAX_VALUE;

    public enum a {
        UP,
        DOWN,
        CLOSEST
    }

    public h(List<T> list, String str) {
        super(str);
        this.k = list;
        if (this.k == null) {
            this.k = new ArrayList();
        }
        r();
    }

    public T a(float f, float f2, a aVar) {
        int b2 = b(f, f2, aVar);
        if (b2 > -1) {
            return (i) this.k.get(b2);
        }
        return null;
    }

    public List<T> a(float f) {
        int i;
        int i2;
        ArrayList arrayList = new ArrayList();
        int i3 = 0;
        int size = this.k.size() - 1;
        while (true) {
            if (i3 > size) {
                break;
            }
            int i4 = (size + i3) / 2;
            i iVar = (i) this.k.get(i4);
            if (f == iVar.h()) {
                int i5 = i4;
                while (i5 > 0 && ((i) this.k.get(i5 - 1)).h() == f) {
                    i5--;
                }
                int size2 = this.k.size();
                while (i5 < size2) {
                    i iVar2 = (i) this.k.get(i5);
                    if (iVar2.h() != f) {
                        break;
                    }
                    arrayList.add(iVar2);
                    i5++;
                }
            } else {
                if (f > iVar.h()) {
                    int i6 = size;
                    i2 = i4 + 1;
                    i = i6;
                } else {
                    i = i4 - 1;
                    i2 = i3;
                }
                i3 = i2;
                size = i;
            }
        }
        return arrayList;
    }

    public void a(float f, float f2) {
        if (this.k != null && !this.k.isEmpty()) {
            this.l = -3.4028235E38f;
            this.m = Float.MAX_VALUE;
            int b2 = b(f, Float.NaN, a.DOWN);
            int b3 = b(f2, Float.NaN, a.UP);
            for (int i = b2; i <= b3; i++) {
                c((i) this.k.get(i));
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a(T t) {
        if (t != null) {
            b(t);
            c(t);
        }
    }

    public int b(float f, float f2, a aVar) {
        int i;
        int i2;
        float f3;
        int i3;
        int i4;
        if (this.k == null || this.k.isEmpty()) {
            return -1;
        }
        int i5 = 0;
        int size = this.k.size() - 1;
        int i6 = size;
        while (i5 < i6) {
            int i7 = (i5 + i6) / 2;
            float h = ((i) this.k.get(i7)).h() - f;
            float h2 = ((i) this.k.get(i7 + 1)).h() - f;
            float abs = Math.abs(h);
            float abs2 = Math.abs(h2);
            if (abs2 < abs) {
                int i8 = i6;
                i4 = i7 + 1;
                i3 = i8;
            } else if (abs < abs2) {
                i3 = i7;
                i4 = i5;
            } else if (((double) h) >= 0.0d) {
                i3 = i7;
                i4 = i5;
            } else if (((double) h) < 0.0d) {
                int i9 = i6;
                i4 = i7 + 1;
                i3 = i9;
            } else {
                i3 = i6;
                i4 = i5;
            }
            size = i3;
            i5 = i4;
            i6 = i3;
        }
        if (size == -1) {
            return size;
        }
        float h3 = ((i) this.k.get(size)).h();
        if (aVar == a.UP) {
            if (h3 < f && size < this.k.size() - 1) {
                i = size + 1;
            }
            i = size;
        } else {
            if (aVar == a.DOWN && h3 > f && size > 0) {
                i = size - 1;
            }
            i = size;
        }
        if (Float.isNaN(f2)) {
            return i;
        }
        int i10 = i;
        while (i10 > 0 && ((i) this.k.get(i10 - 1)).h() == h3) {
            i10--;
        }
        float b2 = ((i) this.k.get(i10)).b();
        int i11 = i10;
        while (true) {
            int i12 = i11 + 1;
            if (i12 < this.k.size()) {
                i iVar = (i) this.k.get(i12);
                if (iVar.h() != h3) {
                    break;
                }
                if (Math.abs(iVar.b() - f2) < Math.abs(b2 - f2)) {
                    i2 = i12;
                    f3 = f2;
                } else {
                    i2 = i10;
                    f3 = b2;
                }
                b2 = f3;
                i10 = i2;
                i11 = i12;
            } else {
                break;
            }
        }
        return i10;
    }

    public T b(float f, float f2) {
        return a(f, f2, a.CLOSEST);
    }

    /* access modifiers changed from: protected */
    public void b(T t) {
        if (t.h() < this.o) {
            this.o = t.h();
        }
        if (t.h() > this.n) {
            this.n = t.h();
        }
    }

    /* access modifiers changed from: protected */
    public void c(T t) {
        if (t.b() < this.m) {
            this.m = t.b();
        }
        if (t.b() > this.l) {
            this.l = t.b();
        }
    }

    public int d(i iVar) {
        return this.k.indexOf(iVar);
    }

    public T d(int i) {
        return (i) this.k.get(i);
    }

    public void r() {
        if (this.k != null && !this.k.isEmpty()) {
            this.l = -3.4028235E38f;
            this.m = Float.MAX_VALUE;
            this.n = -3.4028235E38f;
            this.o = Float.MAX_VALUE;
            for (T a2 : this.k) {
                a(a2);
            }
        }
    }

    public int s() {
        return this.k.size();
    }

    public String t() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("DataSet, label: " + (e() == null ? "" : e()) + ", entries: " + this.k.size() + "\n");
        return stringBuffer.toString();
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(t());
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.k.size()) {
                return stringBuffer.toString();
            }
            stringBuffer.append(((i) this.k.get(i2)).toString() + " ");
            i = i2 + 1;
        }
    }

    public float u() {
        return this.m;
    }

    public float v() {
        return this.l;
    }

    public float w() {
        return this.o;
    }

    public float x() {
        return this.n;
    }
}
