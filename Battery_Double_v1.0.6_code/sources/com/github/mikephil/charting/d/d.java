package com.github.mikephil.charting.d;

import android.graphics.Color;
import com.github.mikephil.charting.d.i;
import com.github.mikephil.charting.g.b.b;
import java.util.List;

public abstract class d<T extends i> extends h<T> implements b<T> {

    /* renamed from: a reason: collision with root package name */
    protected int f1702a = Color.rgb(255, 187, 115);

    public d(List<T> list, String str) {
        super(list, str);
    }

    public int a() {
        return this.f1702a;
    }
}
