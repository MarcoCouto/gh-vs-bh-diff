package com.github.mikephil.charting.d;

import android.os.Parcel;
import android.os.ParcelFormatException;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class i extends f implements Parcelable {
    public static final Creator<i> CREATOR = new Creator<i>() {
        /* renamed from: a */
        public i createFromParcel(Parcel parcel) {
            return new i(parcel);
        }

        /* renamed from: a */
        public i[] newArray(int i) {
            return new i[i];
        }
    };

    /* renamed from: a reason: collision with root package name */
    private float f1711a = 0.0f;

    public i() {
    }

    public i(float f, float f2) {
        super(f2);
        this.f1711a = f;
    }

    protected i(Parcel parcel) {
        this.f1711a = parcel.readFloat();
        a(parcel.readFloat());
        if (parcel.readInt() == 1) {
            a((Object) parcel.readParcelable(Object.class.getClassLoader()));
        }
    }

    public int describeContents() {
        return 0;
    }

    public float h() {
        return this.f1711a;
    }

    public String toString() {
        return "Entry, x: " + this.f1711a + " y: " + b();
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeFloat(this.f1711a);
        parcel.writeFloat(b());
        if (g() == null) {
            parcel.writeInt(0);
        } else if (g() instanceof Parcelable) {
            parcel.writeInt(1);
            parcel.writeParcelable((Parcelable) g(), i);
        } else {
            throw new ParcelFormatException("Cannot parcel an Entry with non-parcelable data");
        }
    }
}
