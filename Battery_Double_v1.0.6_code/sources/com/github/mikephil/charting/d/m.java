package com.github.mikephil.charting.d;

import android.graphics.DashPathEffect;
import com.github.mikephil.charting.d.i;
import com.github.mikephil.charting.g.b.g;
import java.util.List;

public abstract class m<T extends i> extends d<T> implements g<T> {
    protected boolean q;
    protected boolean r;
    protected float s;
    protected DashPathEffect t;

    public m(List<T> list, String str) {
        super(list, str);
        this.q = true;
        this.r = true;
        this.s = 0.5f;
        this.t = null;
        this.s = com.github.mikephil.charting.j.i.a(0.5f);
    }

    public boolean Q() {
        return this.q;
    }

    public boolean R() {
        return this.r;
    }

    public float S() {
        return this.s;
    }

    public DashPathEffect T() {
        return this.t;
    }
}
