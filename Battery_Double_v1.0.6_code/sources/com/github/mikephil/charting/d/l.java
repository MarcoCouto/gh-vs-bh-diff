package com.github.mikephil.charting.d;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import com.github.mikephil.charting.d.i;
import com.github.mikephil.charting.g.b.f;
import java.util.List;

public abstract class l<T extends i> extends m<T> implements f<T> {
    protected Drawable p;
    private int u = Color.rgb(140, 234, 255);
    private int v = 85;
    private float w = 2.5f;
    private boolean x = false;

    public l(List<T> list, String str) {
        super(list, str);
    }

    public int L() {
        return this.u;
    }

    public Drawable M() {
        return this.p;
    }

    public int N() {
        return this.v;
    }

    public float O() {
        return this.w;
    }

    public boolean P() {
        return this.x;
    }

    public void b(float f) {
        float f2 = 10.0f;
        float f3 = 0.2f;
        if (f >= 0.2f) {
            f3 = f;
        }
        if (f3 <= 10.0f) {
            f2 = f3;
        }
        this.w = com.github.mikephil.charting.j.i.a(f2);
    }

    public void c(boolean z) {
        this.x = z;
    }

    public void g(int i) {
        this.u = i;
        this.p = null;
    }
}
