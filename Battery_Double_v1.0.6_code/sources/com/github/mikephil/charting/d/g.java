package com.github.mikephil.charting.d;

import com.github.mikephil.charting.c.j.a;
import com.github.mikephil.charting.f.c;
import com.github.mikephil.charting.g.b.d;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public abstract class g<T extends d<? extends i>> {

    /* renamed from: a reason: collision with root package name */
    protected float f1707a;

    /* renamed from: b reason: collision with root package name */
    protected float f1708b;
    protected float c;
    protected float d;
    protected float e;
    protected float f;
    protected float g;
    protected float h;
    protected List<T> i;

    public g() {
        this.f1707a = -3.4028235E38f;
        this.f1708b = Float.MAX_VALUE;
        this.c = -3.4028235E38f;
        this.d = Float.MAX_VALUE;
        this.e = -3.4028235E38f;
        this.f = Float.MAX_VALUE;
        this.g = -3.4028235E38f;
        this.h = Float.MAX_VALUE;
        this.i = new ArrayList();
    }

    public g(T... tArr) {
        this.f1707a = -3.4028235E38f;
        this.f1708b = Float.MAX_VALUE;
        this.c = -3.4028235E38f;
        this.d = Float.MAX_VALUE;
        this.e = -3.4028235E38f;
        this.f = Float.MAX_VALUE;
        this.g = -3.4028235E38f;
        this.h = Float.MAX_VALUE;
        this.i = a(tArr);
        b();
    }

    private List<T> a(T[] tArr) {
        ArrayList arrayList = new ArrayList();
        for (T add : tArr) {
            arrayList.add(add);
        }
        return arrayList;
    }

    public float a(a aVar) {
        return aVar == a.LEFT ? this.f == Float.MAX_VALUE ? this.h : this.f : this.h == Float.MAX_VALUE ? this.f : this.h;
    }

    public i a(c cVar) {
        if (cVar.e() >= this.i.size()) {
            return null;
        }
        return ((d) this.i.get(cVar.e())).b(cVar.a(), cVar.b());
    }

    public T a(int i2) {
        if (this.i == null || i2 < 0 || i2 >= this.i.size()) {
            return null;
        }
        return (d) this.i.get(i2);
    }

    /* access modifiers changed from: protected */
    public T a(List<T> list) {
        for (T t : list) {
            if (t.q() == a.LEFT) {
                return t;
            }
        }
        return null;
    }

    public void a(float f2, float f3) {
        for (T a2 : this.i) {
            a2.a(f2, f3);
        }
        c();
    }

    /* access modifiers changed from: protected */
    public void a(T t) {
        if (this.f1707a < t.v()) {
            this.f1707a = t.v();
        }
        if (this.f1708b > t.u()) {
            this.f1708b = t.u();
        }
        if (this.c < t.x()) {
            this.c = t.x();
        }
        if (this.d > t.w()) {
            this.d = t.w();
        }
        if (t.q() == a.LEFT) {
            if (this.e < t.v()) {
                this.e = t.v();
            }
            if (this.f > t.u()) {
                this.f = t.u();
                return;
            }
            return;
        }
        if (this.g < t.v()) {
            this.g = t.v();
        }
        if (this.h > t.u()) {
            this.h = t.u();
        }
    }

    public float b(a aVar) {
        return aVar == a.LEFT ? this.e == -3.4028235E38f ? this.g : this.e : this.g == -3.4028235E38f ? this.e : this.g;
    }

    public T b(List<T> list) {
        for (T t : list) {
            if (t.q() == a.RIGHT) {
                return t;
            }
        }
        return null;
    }

    public void b() {
        c();
    }

    /* access modifiers changed from: protected */
    public void c() {
        if (this.i != null) {
            this.f1707a = -3.4028235E38f;
            this.f1708b = Float.MAX_VALUE;
            this.c = -3.4028235E38f;
            this.d = Float.MAX_VALUE;
            for (T a2 : this.i) {
                a(a2);
            }
            this.e = -3.4028235E38f;
            this.f = Float.MAX_VALUE;
            this.g = -3.4028235E38f;
            this.h = Float.MAX_VALUE;
            d a3 = a(this.i);
            if (a3 != null) {
                this.e = a3.v();
                this.f = a3.u();
                for (T t : this.i) {
                    if (t.q() == a.LEFT) {
                        if (t.u() < this.f) {
                            this.f = t.u();
                        }
                        if (t.v() > this.e) {
                            this.e = t.v();
                        }
                    }
                }
            }
            d b2 = b(this.i);
            if (b2 != null) {
                this.g = b2.v();
                this.h = b2.u();
                for (T t2 : this.i) {
                    if (t2.q() == a.RIGHT) {
                        if (t2.u() < this.h) {
                            this.h = t2.u();
                        }
                        if (t2.v() > this.g) {
                            this.g = t2.v();
                        }
                    }
                }
            }
        }
    }

    public int d() {
        if (this.i == null) {
            return 0;
        }
        return this.i.size();
    }

    public float e() {
        return this.f1708b;
    }

    public float f() {
        return this.f1707a;
    }

    public float g() {
        return this.d;
    }

    public float h() {
        return this.c;
    }

    public List<T> i() {
        return this.i;
    }

    public int j() {
        int i2 = 0;
        Iterator it = this.i.iterator();
        while (true) {
            int i3 = i2;
            if (!it.hasNext()) {
                return i3;
            }
            i2 = ((d) it.next()).s() + i3;
        }
    }
}
