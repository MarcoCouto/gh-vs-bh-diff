package com.github.mikephil.charting.d;

public abstract class f {

    /* renamed from: a reason: collision with root package name */
    private float f1705a = 0.0f;

    /* renamed from: b reason: collision with root package name */
    private Object f1706b = null;

    public f() {
    }

    public f(float f) {
        this.f1705a = f;
    }

    public void a(float f) {
        this.f1705a = f;
    }

    public void a(Object obj) {
        this.f1706b = obj;
    }

    public float b() {
        return this.f1705a;
    }

    public Object g() {
        return this.f1706b;
    }
}
