package com.github.mikephil.charting.d;

import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Typeface;
import com.github.mikephil.charting.c.e.b;
import com.github.mikephil.charting.c.j.a;
import com.github.mikephil.charting.d.i;
import com.github.mikephil.charting.e.f;
import com.github.mikephil.charting.g.b.d;
import java.util.ArrayList;
import java.util.List;

public abstract class e<T extends i> implements d<T> {

    /* renamed from: a reason: collision with root package name */
    private String f1703a;

    /* renamed from: b reason: collision with root package name */
    protected List<Integer> f1704b;
    protected List<Integer> c;
    protected a d;
    protected boolean e;
    protected transient f f;
    protected Typeface g;
    protected boolean h;
    protected float i;
    protected boolean j;
    private b k;
    private float l;
    private float m;
    private DashPathEffect n;

    public e() {
        this.f1704b = null;
        this.c = null;
        this.f1703a = "DataSet";
        this.d = a.LEFT;
        this.e = true;
        this.k = b.DEFAULT;
        this.l = Float.NaN;
        this.m = Float.NaN;
        this.n = null;
        this.h = true;
        this.i = 17.0f;
        this.j = true;
        this.f1704b = new ArrayList();
        this.c = new ArrayList();
        this.f1704b.add(Integer.valueOf(Color.rgb(140, 234, 255)));
        this.c.add(Integer.valueOf(-16777216));
    }

    public e(String str) {
        this();
        this.f1703a = str;
    }

    public int a(int i2) {
        return ((Integer) this.f1704b.get(i2 % this.f1704b.size())).intValue();
    }

    public void a(f fVar) {
        if (fVar != null) {
            this.f = fVar;
        }
    }

    public void a(boolean z) {
        this.h = z;
    }

    public List<Integer> b() {
        return this.f1704b;
    }

    public void b(int i2) {
        d();
        this.f1704b.add(Integer.valueOf(i2));
    }

    public int c() {
        return ((Integer) this.f1704b.get(0)).intValue();
    }

    public int c(int i2) {
        return ((Integer) this.c.get(i2 % this.c.size())).intValue();
    }

    public void d() {
        if (this.f1704b == null) {
            this.f1704b = new ArrayList();
        }
        this.f1704b.clear();
    }

    public String e() {
        return this.f1703a;
    }

    public boolean f() {
        return this.e;
    }

    public f g() {
        return h() ? com.github.mikephil.charting.j.i.a() : this.f;
    }

    public boolean h() {
        return this.f == null;
    }

    public Typeface i() {
        return this.g;
    }

    public float j() {
        return this.i;
    }

    public b k() {
        return this.k;
    }

    public float l() {
        return this.l;
    }

    public float m() {
        return this.m;
    }

    public DashPathEffect n() {
        return this.n;
    }

    public boolean o() {
        return this.h;
    }

    public boolean p() {
        return this.j;
    }

    public a q() {
        return this.d;
    }
}
