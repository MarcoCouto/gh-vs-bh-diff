package com.github.mikephil.charting.d;

import android.annotation.SuppressLint;
import android.util.Log;

@SuppressLint({"ParcelCreator"})
public class n extends i {

    /* renamed from: a reason: collision with root package name */
    private String f1714a;

    public String a() {
        return this.f1714a;
    }

    @Deprecated
    public float h() {
        Log.i("DEPRECATED", "Pie entries do not have x values");
        return super.h();
    }
}
