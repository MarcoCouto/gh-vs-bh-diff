package com.github.mikephil.charting.d;

import android.graphics.Color;
import android.graphics.DashPathEffect;
import com.github.mikephil.charting.e.b;
import com.github.mikephil.charting.g.b.e;
import java.util.ArrayList;
import java.util.List;

public class k extends l<i> implements e {
    private DashPathEffect A = null;
    private com.github.mikephil.charting.e.e B = new b();
    private boolean C = true;
    private boolean D = true;
    private a u = a.LINEAR;
    private List<Integer> v = null;
    private int w = -1;
    private float x = 8.0f;
    private float y = 4.0f;
    private float z = 0.2f;

    public enum a {
        LINEAR,
        STEPPED,
        CUBIC_BEZIER,
        HORIZONTAL_BEZIER
    }

    public k(List<i> list, String str) {
        super(list, str);
        if (this.v == null) {
            this.v = new ArrayList();
        }
        this.v.clear();
        this.v.add(Integer.valueOf(Color.rgb(140, 234, 255)));
    }

    public float A() {
        return this.x;
    }

    public float B() {
        return this.y;
    }

    public boolean C() {
        return this.A != null;
    }

    public DashPathEffect D() {
        return this.A;
    }

    public boolean E() {
        return this.C;
    }

    @Deprecated
    public boolean F() {
        return this.u == a.STEPPED;
    }

    public int G() {
        return this.v.size();
    }

    public void H() {
        if (this.v == null) {
            this.v = new ArrayList();
        }
        this.v.clear();
    }

    public int I() {
        return this.w;
    }

    public boolean J() {
        return this.D;
    }

    public com.github.mikephil.charting.e.e K() {
        return this.B;
    }

    public void a(a aVar) {
        this.u = aVar;
    }

    public void b(boolean z2) {
        this.D = z2;
    }

    public int e(int i) {
        return ((Integer) this.v.get(i)).intValue();
    }

    public void f(int i) {
        H();
        this.v.add(Integer.valueOf(i));
    }

    public a y() {
        return this.u;
    }

    public float z() {
        return this.z;
    }
}
