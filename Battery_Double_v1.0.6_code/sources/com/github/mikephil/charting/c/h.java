package com.github.mikephil.charting.c;

import android.content.Context;
import android.graphics.Canvas;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.MeasureSpec;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.github.mikephil.charting.charts.c;
import com.github.mikephil.charting.d.i;
import com.github.mikephil.charting.j.e;
import java.lang.ref.WeakReference;

public class h extends RelativeLayout implements d {

    /* renamed from: a reason: collision with root package name */
    private e f1684a = new e();

    /* renamed from: b reason: collision with root package name */
    private e f1685b = new e();
    private WeakReference<c> c;

    public h(Context context, int i) {
        super(context);
        setupLayoutResource(i);
    }

    private void setupLayoutResource(int i) {
        View inflate = LayoutInflater.from(getContext()).inflate(i, this);
        inflate.setLayoutParams(new LayoutParams(-2, -2));
        inflate.measure(MeasureSpec.makeMeasureSpec(0, 0), MeasureSpec.makeMeasureSpec(0, 0));
        inflate.layout(0, 0, inflate.getMeasuredWidth(), inflate.getMeasuredHeight());
    }

    public e a(float f, float f2) {
        e offset = getOffset();
        this.f1685b.f1751a = offset.f1751a;
        this.f1685b.f1752b = offset.f1752b;
        c chartView = getChartView();
        float width = (float) getWidth();
        float height = (float) getHeight();
        if (this.f1685b.f1751a + f < 0.0f) {
            this.f1685b.f1751a = -f;
        } else if (chartView != null && f + width + this.f1685b.f1751a > ((float) chartView.getWidth())) {
            this.f1685b.f1751a = (((float) chartView.getWidth()) - f) - width;
        }
        if (this.f1685b.f1752b + f2 < 0.0f) {
            this.f1685b.f1752b = -f2;
        } else if (chartView != null && f2 + height + this.f1685b.f1752b > ((float) chartView.getHeight())) {
            this.f1685b.f1752b = (((float) chartView.getHeight()) - f2) - height;
        }
        return this.f1685b;
    }

    public void a(Canvas canvas, float f, float f2) {
        e a2 = a(f, f2);
        int save = canvas.save();
        canvas.translate(a2.f1751a + f, a2.f1752b + f2);
        draw(canvas);
        canvas.restoreToCount(save);
    }

    public void a(i iVar, com.github.mikephil.charting.f.c cVar) {
        measure(MeasureSpec.makeMeasureSpec(0, 0), MeasureSpec.makeMeasureSpec(0, 0));
        layout(0, 0, getMeasuredWidth(), getMeasuredHeight());
    }

    public c getChartView() {
        if (this.c == null) {
            return null;
        }
        return (c) this.c.get();
    }

    public e getOffset() {
        return this.f1684a;
    }

    public void setChartView(c cVar) {
        this.c = new WeakReference<>(cVar);
    }

    public void setOffset(e eVar) {
        this.f1684a = eVar;
        if (this.f1684a == null) {
            this.f1684a = new e();
        }
    }
}
