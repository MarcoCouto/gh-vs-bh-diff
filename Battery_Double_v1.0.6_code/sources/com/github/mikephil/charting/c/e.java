package com.github.mikephil.charting.c;

import android.graphics.DashPathEffect;
import android.graphics.Paint;
import com.github.mikephil.charting.j.i;
import com.github.mikephil.charting.j.j;
import java.util.ArrayList;
import java.util.List;

public class e extends b {
    private boolean B;
    private List<com.github.mikephil.charting.j.b> C;
    private List<Boolean> D;
    private List<com.github.mikephil.charting.j.b> E;

    /* renamed from: a reason: collision with root package name */
    public float f1662a;

    /* renamed from: b reason: collision with root package name */
    public float f1663b;
    public float c;
    public float d;
    private f[] e;
    private f[] f;
    private boolean g;
    private c h;
    private f i;
    private d j;
    private boolean k;
    private a l;
    private b m;
    private float n;
    private float o;
    private DashPathEffect p;
    private float q;
    private float r;
    private float s;
    private float t;
    private float u;

    /* renamed from: com.github.mikephil.charting.c.e$1 reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {

        /* renamed from: a reason: collision with root package name */
        static final /* synthetic */ int[] f1664a = new int[C0041e.values().length];

        static {
            f1665b = new int[d.values().length];
            try {
                f1665b[d.VERTICAL.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                f1665b[d.HORIZONTAL.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                f1664a[C0041e.LEFT_OF_CHART.ordinal()] = 1;
            } catch (NoSuchFieldError e3) {
            }
            try {
                f1664a[C0041e.LEFT_OF_CHART_INSIDE.ordinal()] = 2;
            } catch (NoSuchFieldError e4) {
            }
            try {
                f1664a[C0041e.LEFT_OF_CHART_CENTER.ordinal()] = 3;
            } catch (NoSuchFieldError e5) {
            }
            try {
                f1664a[C0041e.RIGHT_OF_CHART.ordinal()] = 4;
            } catch (NoSuchFieldError e6) {
            }
            try {
                f1664a[C0041e.RIGHT_OF_CHART_INSIDE.ordinal()] = 5;
            } catch (NoSuchFieldError e7) {
            }
            try {
                f1664a[C0041e.RIGHT_OF_CHART_CENTER.ordinal()] = 6;
            } catch (NoSuchFieldError e8) {
            }
            try {
                f1664a[C0041e.ABOVE_CHART_LEFT.ordinal()] = 7;
            } catch (NoSuchFieldError e9) {
            }
            try {
                f1664a[C0041e.ABOVE_CHART_CENTER.ordinal()] = 8;
            } catch (NoSuchFieldError e10) {
            }
            try {
                f1664a[C0041e.ABOVE_CHART_RIGHT.ordinal()] = 9;
            } catch (NoSuchFieldError e11) {
            }
            try {
                f1664a[C0041e.BELOW_CHART_LEFT.ordinal()] = 10;
            } catch (NoSuchFieldError e12) {
            }
            try {
                f1664a[C0041e.BELOW_CHART_CENTER.ordinal()] = 11;
            } catch (NoSuchFieldError e13) {
            }
            try {
                f1664a[C0041e.BELOW_CHART_RIGHT.ordinal()] = 12;
            } catch (NoSuchFieldError e14) {
            }
            try {
                f1664a[C0041e.PIECHART_CENTER.ordinal()] = 13;
            } catch (NoSuchFieldError e15) {
            }
        }
    }

    public enum a {
        LEFT_TO_RIGHT,
        RIGHT_TO_LEFT
    }

    public enum b {
        NONE,
        EMPTY,
        DEFAULT,
        SQUARE,
        CIRCLE,
        LINE
    }

    public enum c {
        LEFT,
        CENTER,
        RIGHT
    }

    public enum d {
        HORIZONTAL,
        VERTICAL
    }

    @Deprecated
    /* renamed from: com.github.mikephil.charting.c.e$e reason: collision with other inner class name */
    public enum C0041e {
        RIGHT_OF_CHART,
        RIGHT_OF_CHART_CENTER,
        RIGHT_OF_CHART_INSIDE,
        LEFT_OF_CHART,
        LEFT_OF_CHART_CENTER,
        LEFT_OF_CHART_INSIDE,
        BELOW_CHART_LEFT,
        BELOW_CHART_RIGHT,
        BELOW_CHART_CENTER,
        ABOVE_CHART_LEFT,
        ABOVE_CHART_RIGHT,
        ABOVE_CHART_CENTER,
        PIECHART_CENTER
    }

    public enum f {
        TOP,
        CENTER,
        BOTTOM
    }

    public e() {
        this.e = new f[0];
        this.g = false;
        this.h = c.LEFT;
        this.i = f.BOTTOM;
        this.j = d.HORIZONTAL;
        this.k = false;
        this.l = a.LEFT_TO_RIGHT;
        this.m = b.SQUARE;
        this.n = 8.0f;
        this.o = 3.0f;
        this.p = null;
        this.q = 6.0f;
        this.r = 0.0f;
        this.s = 5.0f;
        this.t = 3.0f;
        this.u = 0.95f;
        this.f1662a = 0.0f;
        this.f1663b = 0.0f;
        this.c = 0.0f;
        this.d = 0.0f;
        this.B = false;
        this.C = new ArrayList(16);
        this.D = new ArrayList(16);
        this.E = new ArrayList(16);
        this.z = i.a(10.0f);
        this.w = i.a(5.0f);
        this.x = i.a(3.0f);
    }

    public float a(Paint paint) {
        float a2;
        float f2 = 0.0f;
        float a3 = i.a(this.s);
        f[] fVarArr = this.e;
        int length = fVarArr.length;
        int i2 = 0;
        float f3 = 0.0f;
        while (i2 < length) {
            f fVar = fVarArr[i2];
            float a4 = i.a(Float.isNaN(fVar.c) ? this.n : fVar.c);
            if (a4 <= f2) {
                a4 = f2;
            }
            String str = fVar.f1678a;
            if (str == null) {
                a2 = f3;
            } else {
                a2 = (float) i.a(paint, str);
                if (a2 <= f3) {
                    a2 = f3;
                }
            }
            i2++;
            f3 = a2;
            f2 = a4;
        }
        return f3 + f2 + a3;
    }

    public void a(Paint paint, j jVar) {
        int i2;
        float f2;
        float f3;
        float f4;
        boolean z;
        float f5;
        float f6;
        float f7;
        float f8;
        float a2 = i.a(this.n);
        float a3 = i.a(this.t);
        float a4 = i.a(this.s);
        float a5 = i.a(this.q);
        float a6 = i.a(this.r);
        boolean z2 = this.B;
        f[] fVarArr = this.e;
        int length = fVarArr.length;
        this.d = a(paint);
        this.c = b(paint);
        switch (this.j) {
            case VERTICAL:
                float f9 = 0.0f;
                float f10 = 0.0f;
                float f11 = 0.0f;
                float a7 = i.a(paint);
                boolean z3 = false;
                int i3 = 0;
                while (i3 < length) {
                    f fVar = fVarArr[i3];
                    boolean z4 = fVar.f1679b != b.NONE;
                    float a8 = Float.isNaN(fVar.c) ? a2 : i.a(fVar.c);
                    String str = fVar.f1678a;
                    if (!z3) {
                        f11 = 0.0f;
                    }
                    if (z4) {
                        if (z3) {
                            f11 += a3;
                        }
                        f11 += a8;
                    }
                    if (str != null) {
                        if (z4 && !z3) {
                            f7 = f11 + a4;
                            f8 = f10;
                            z = z3;
                            f6 = f9;
                        } else if (z3) {
                            f6 = Math.max(f9, f11);
                            f8 = f10 + a7 + a6;
                            f7 = 0.0f;
                            z = false;
                        } else {
                            z = z3;
                            f7 = f11;
                            f8 = f10;
                            f6 = f9;
                        }
                        f5 = f7 + ((float) i.a(paint, str));
                        f10 = i3 < length + -1 ? a7 + a6 + f8 : f8;
                    } else {
                        z = true;
                        f5 = a8 + f11;
                        if (i3 < length - 1) {
                            f5 += a3;
                            f6 = f9;
                        } else {
                            f6 = f9;
                        }
                    }
                    f9 = Math.max(f6, f5);
                    i3++;
                    z3 = z;
                    f11 = f5;
                }
                this.f1662a = f9;
                this.f1663b = f10;
                break;
            case HORIZONTAL:
                float a9 = i.a(paint);
                float b2 = i.b(paint) + a6;
                float i4 = jVar.i() * this.u;
                float f12 = 0.0f;
                float f13 = 0.0f;
                int i5 = -1;
                this.D.clear();
                this.C.clear();
                this.E.clear();
                int i6 = 0;
                float f14 = 0.0f;
                while (i6 < length) {
                    f fVar2 = fVarArr[i6];
                    boolean z5 = fVar2.f1679b != b.NONE;
                    float a10 = Float.isNaN(fVar2.c) ? a2 : i.a(fVar2.c);
                    String str2 = fVar2.f1678a;
                    this.D.add(Boolean.valueOf(false));
                    float f15 = i5 == -1 ? 0.0f : f14 + a3;
                    if (str2 != null) {
                        this.C.add(i.c(paint, str2));
                        float f16 = z5 ? a4 + a10 : 0.0f;
                        i2 = i5;
                        f2 = ((com.github.mikephil.charting.j.b) this.C.get(i6)).f1747a + f15 + f16;
                    } else {
                        this.C.add(com.github.mikephil.charting.j.b.a(0.0f, 0.0f));
                        if (!z5) {
                            a10 = 0.0f;
                        }
                        float f17 = f15 + a10;
                        if (i5 == -1) {
                            i2 = i6;
                            f2 = f17;
                        } else {
                            i2 = i5;
                            f2 = f17;
                        }
                    }
                    if (str2 != null || i6 == length - 1) {
                        float f18 = f13 == 0.0f ? 0.0f : a5;
                        if (!z2 || f13 == 0.0f || i4 - f13 >= f18 + f2) {
                            f4 = f18 + f2 + f13;
                            f3 = f12;
                        } else {
                            this.E.add(com.github.mikephil.charting.j.b.a(f13, a9));
                            f3 = Math.max(f12, f13);
                            this.D.set(i2 > -1 ? i2 : i6, Boolean.valueOf(true));
                            f4 = f2;
                        }
                        if (i6 == length - 1) {
                            this.E.add(com.github.mikephil.charting.j.b.a(f4, a9));
                            f3 = Math.max(f3, f4);
                        }
                    } else {
                        f4 = f13;
                        f3 = f12;
                    }
                    if (str2 != null) {
                        i2 = -1;
                    }
                    i6++;
                    f14 = f2;
                    f12 = f3;
                    f13 = f4;
                    i5 = i2;
                }
                this.f1662a = f12;
                this.f1663b = (((float) (this.E.size() == 0 ? 0 : this.E.size() - 1)) * b2) + (a9 * ((float) this.E.size()));
                break;
        }
        this.f1663b += this.x;
        this.f1662a += this.w;
    }

    public void a(List<f> list) {
        this.e = (f[]) list.toArray(new f[list.size()]);
    }

    public f[] a() {
        return this.e;
    }

    public float b(Paint paint) {
        float f2 = 0.0f;
        for (f fVar : this.e) {
            String str = fVar.f1678a;
            if (str != null) {
                float b2 = (float) i.b(paint, str);
                if (b2 > f2) {
                    f2 = b2;
                }
            }
        }
        return f2;
    }

    public f[] b() {
        return this.f;
    }

    public boolean c() {
        return this.g;
    }

    public c d() {
        return this.h;
    }

    public f e() {
        return this.i;
    }

    public d f() {
        return this.j;
    }

    public boolean g() {
        return this.k;
    }

    public a h() {
        return this.l;
    }

    public b i() {
        return this.m;
    }

    public float j() {
        return this.n;
    }

    public float k() {
        return this.o;
    }

    public DashPathEffect l() {
        return this.p;
    }

    public float m() {
        return this.q;
    }

    public float n() {
        return this.r;
    }

    public float o() {
        return this.s;
    }

    public float p() {
        return this.t;
    }

    public float q() {
        return this.u;
    }

    public List<com.github.mikephil.charting.j.b> r() {
        return this.C;
    }

    public List<Boolean> y() {
        return this.D;
    }

    public List<com.github.mikephil.charting.j.b> z() {
        return this.E;
    }
}
