package com.github.mikephil.charting.c;

import android.graphics.DashPathEffect;
import com.github.mikephil.charting.c.e.b;

public class f {

    /* renamed from: a reason: collision with root package name */
    public String f1678a;

    /* renamed from: b reason: collision with root package name */
    public b f1679b = b.DEFAULT;
    public float c = Float.NaN;
    public float d = Float.NaN;
    public DashPathEffect e = null;
    public int f = 1122867;

    public f() {
    }

    public f(String str, b bVar, float f2, float f3, DashPathEffect dashPathEffect, int i) {
        this.f1678a = str;
        this.f1679b = bVar;
        this.c = f2;
        this.d = f3;
        this.e = dashPathEffect;
        this.f = i;
    }
}
