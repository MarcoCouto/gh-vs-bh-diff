package com.github.mikephil.charting.c;

import android.graphics.Paint.Align;
import com.github.mikephil.charting.j.e;
import com.github.mikephil.charting.j.i;

public class c extends b {

    /* renamed from: a reason: collision with root package name */
    private String f1660a;

    /* renamed from: b reason: collision with root package name */
    private e f1661b;
    private Align c;

    public c() {
        this.f1660a = "Description Label";
        this.c = Align.RIGHT;
        this.z = i.a(8.0f);
    }

    public String a() {
        return this.f1660a;
    }

    public e b() {
        return this.f1661b;
    }

    public Align c() {
        return this.c;
    }
}
