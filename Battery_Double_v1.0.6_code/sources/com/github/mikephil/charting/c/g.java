package com.github.mikephil.charting.c;

import android.graphics.DashPathEffect;
import android.graphics.Paint.Style;

public class g extends b {

    /* renamed from: a reason: collision with root package name */
    private float f1680a;

    /* renamed from: b reason: collision with root package name */
    private float f1681b;
    private int c;
    private Style d;
    private String e;
    private DashPathEffect f;
    private a g;

    public enum a {
        LEFT_TOP,
        LEFT_BOTTOM,
        RIGHT_TOP,
        RIGHT_BOTTOM
    }

    public float a() {
        return this.f1680a;
    }

    public float b() {
        return this.f1681b;
    }

    public int c() {
        return this.c;
    }

    public DashPathEffect d() {
        return this.f;
    }

    public Style e() {
        return this.d;
    }

    public a f() {
        return this.g;
    }

    public String g() {
        return this.e;
    }
}
