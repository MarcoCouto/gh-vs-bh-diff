package com.github.mikephil.charting.c;

import android.graphics.Typeface;

public abstract class b {
    protected int A = -16777216;
    protected boolean v = true;
    protected float w = 5.0f;
    protected float x = 5.0f;
    protected Typeface y = null;
    protected float z = 10.0f;

    public void c(boolean z2) {
        this.v = z2;
    }

    public float s() {
        return this.w;
    }

    public float t() {
        return this.x;
    }

    public Typeface u() {
        return this.y;
    }

    public float v() {
        return this.z;
    }

    public int w() {
        return this.A;
    }

    public boolean x() {
        return this.v;
    }
}
