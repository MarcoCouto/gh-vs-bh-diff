package com.github.mikephil.charting.c;

public class i extends a {
    public int B;
    public int C;
    public int D;
    public int E;
    protected float F;
    private boolean G;
    private a H;

    public enum a {
        TOP,
        BOTTOM,
        BOTH_SIDED,
        TOP_INSIDE,
        BOTTOM_INSIDE
    }

    public i() {
        this.B = 1;
        this.C = 1;
        this.D = 1;
        this.E = 1;
        this.F = 0.0f;
        this.G = false;
        this.H = a.TOP;
        this.x = com.github.mikephil.charting.j.i.a(4.0f);
    }

    public boolean A() {
        return this.G;
    }

    public a y() {
        return this.H;
    }

    public float z() {
        return this.F;
    }
}
