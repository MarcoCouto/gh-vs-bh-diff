package com.github.mikephil.charting.e;

import com.github.mikephil.charting.d.i;
import com.github.mikephil.charting.j.j;
import com.hmatalonga.greenhub.Config;
import java.text.DecimalFormat;

public class c implements f {

    /* renamed from: a reason: collision with root package name */
    protected DecimalFormat f1717a;

    /* renamed from: b reason: collision with root package name */
    protected int f1718b;

    public c(int i) {
        a(i);
    }

    public String a(float f, i iVar, int i, j jVar) {
        return this.f1717a.format((double) f);
    }

    public void a(int i) {
        this.f1718b = i;
        StringBuffer stringBuffer = new StringBuffer();
        for (int i2 = 0; i2 < i; i2++) {
            if (i2 == 0) {
                stringBuffer.append(".");
            }
            stringBuffer.append(Config.NOTIFICATION_DEFAULT_PRIORITY);
        }
        this.f1717a = new DecimalFormat("###,###,###,##0" + stringBuffer.toString());
    }
}
