package com.github.mikephil.charting.e;

import com.github.mikephil.charting.d.j;
import com.github.mikephil.charting.g.a.d;
import com.github.mikephil.charting.g.b.e;

public class b implements e {
    public float a(e eVar, d dVar) {
        float yChartMax = dVar.getYChartMax();
        float yChartMin = dVar.getYChartMin();
        j lineData = dVar.getLineData();
        if (eVar.v() > 0.0f && eVar.u() < 0.0f) {
            return 0.0f;
        }
        if (lineData.f() > 0.0f) {
            yChartMax = 0.0f;
        }
        if (lineData.e() < 0.0f) {
            yChartMin = 0.0f;
        }
        if (eVar.u() < 0.0f) {
            yChartMin = yChartMax;
        }
        return yChartMin;
    }
}
