package com.github.mikephil.charting.e;

import com.hmatalonga.greenhub.Config;
import java.text.DecimalFormat;

public class a implements d {

    /* renamed from: a reason: collision with root package name */
    protected DecimalFormat f1715a;

    /* renamed from: b reason: collision with root package name */
    protected int f1716b = 0;

    public a(int i) {
        this.f1716b = i;
        StringBuffer stringBuffer = new StringBuffer();
        for (int i2 = 0; i2 < i; i2++) {
            if (i2 == 0) {
                stringBuffer.append(".");
            }
            stringBuffer.append(Config.NOTIFICATION_DEFAULT_PRIORITY);
        }
        this.f1715a = new DecimalFormat("###,###,###,##0" + stringBuffer.toString());
    }

    public int a() {
        return this.f1716b;
    }

    public String a(float f, com.github.mikephil.charting.c.a aVar) {
        return this.f1715a.format((double) f);
    }
}
