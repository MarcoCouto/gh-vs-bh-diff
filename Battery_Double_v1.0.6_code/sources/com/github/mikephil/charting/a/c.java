package com.github.mikephil.charting.a;

import android.animation.TimeInterpolator;
import android.annotation.SuppressLint;

@SuppressLint({"NewApi"})
public interface c extends TimeInterpolator {
    float getInterpolation(float f);
}
