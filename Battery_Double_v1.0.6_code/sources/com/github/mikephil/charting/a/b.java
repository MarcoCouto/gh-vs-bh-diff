package com.github.mikephil.charting.a;

public class b {

    private static class a {
        public static final c A = new c() {
            public float getInterpolation(float f) {
                if (f < 0.36363637f) {
                    return 7.5625f * f * f;
                }
                if (f < 0.72727275f) {
                    float f2 = f - 0.54545456f;
                    return (f2 * 7.5625f * f2) + 0.75f;
                } else if (f < 0.90909094f) {
                    float f3 = f - 0.8181818f;
                    return (f3 * 7.5625f * f3) + 0.9375f;
                } else {
                    float f4 = f - 0.95454544f;
                    return (f4 * 7.5625f * f4) + 0.984375f;
                }
            }
        };
        public static final c B = new c() {
            public float getInterpolation(float f) {
                return f < 0.5f ? a.z.getInterpolation(2.0f * f) * 0.5f : (a.A.getInterpolation((2.0f * f) - 1.0f) * 0.5f) + 0.5f;
            }
        };

        /* renamed from: a reason: collision with root package name */
        public static final c f1652a = new c() {
            public float getInterpolation(float f) {
                return f;
            }
        };

        /* renamed from: b reason: collision with root package name */
        public static final c f1653b = new c() {
            public float getInterpolation(float f) {
                return f * f;
            }
        };
        public static final c c = new c() {
            public float getInterpolation(float f) {
                return (-f) * (f - 2.0f);
            }
        };
        public static final c d = new c() {
            public float getInterpolation(float f) {
                float f2 = f / 0.5f;
                if (f2 < 1.0f) {
                    return f2 * 0.5f * f2;
                }
                float f3 = f2 - 1.0f;
                return ((f3 * (f3 - 2.0f)) - 1.0f) * -0.5f;
            }
        };
        public static final c e = new c() {
            public float getInterpolation(float f) {
                return f * f * f;
            }
        };
        public static final c f = new c() {
            public float getInterpolation(float f) {
                float f2 = f - 1.0f;
                return (f2 * f2 * f2) + 1.0f;
            }
        };
        public static final c g = new c() {
            public float getInterpolation(float f) {
                float f2 = f / 0.5f;
                if (f2 < 1.0f) {
                    return f2 * 0.5f * f2 * f2;
                }
                float f3 = f2 - 2.0f;
                return ((f3 * f3 * f3) + 2.0f) * 0.5f;
            }
        };
        public static final c h = new c() {
            public float getInterpolation(float f) {
                return f * f * f * f;
            }
        };
        public static final c i = new c() {
            public float getInterpolation(float f) {
                float f2 = f - 1.0f;
                return -((f2 * ((f2 * f2) * f2)) - 1.0f);
            }
        };
        public static final c j = new c() {
            public float getInterpolation(float f) {
                float f2 = f / 0.5f;
                if (f2 < 1.0f) {
                    return f2 * 0.5f * f2 * f2 * f2;
                }
                float f3 = f2 - 2.0f;
                return ((f3 * ((f3 * f3) * f3)) - 2.0f) * -0.5f;
            }
        };
        public static final c k = new c() {
            public float getInterpolation(float f) {
                return (-((float) Math.cos(((double) f) * 1.5707963267948966d))) + 1.0f;
            }
        };
        public static final c l = new c() {
            public float getInterpolation(float f) {
                return (float) Math.sin(((double) f) * 1.5707963267948966d);
            }
        };
        public static final c m = new c() {
            public float getInterpolation(float f) {
                return -0.5f * (((float) Math.cos(3.141592653589793d * ((double) f))) - 1.0f);
            }
        };
        public static final c n = new c() {
            public float getInterpolation(float f) {
                if (f == 0.0f) {
                    return 0.0f;
                }
                return (float) Math.pow(2.0d, (double) (10.0f * (f - 1.0f)));
            }
        };
        public static final c o = new c() {
            public float getInterpolation(float f) {
                if (f == 1.0f) {
                    return 1.0f;
                }
                return -((float) Math.pow(2.0d, (double) ((1.0f + f) * -10.0f)));
            }
        };
        public static final c p = new c() {
            public float getInterpolation(float f) {
                if (f == 0.0f) {
                    return 0.0f;
                }
                if (f == 1.0f) {
                    return 1.0f;
                }
                float f2 = f / 0.5f;
                return f2 < 1.0f ? ((float) Math.pow(2.0d, (double) ((f2 - 1.0f) * 10.0f))) * 0.5f : ((-((float) Math.pow(2.0d, (double) ((f2 - 1.0f) * -10.0f)))) + 2.0f) * 0.5f;
            }
        };
        public static final c q = new c() {
            public float getInterpolation(float f) {
                return -(((float) Math.sqrt((double) (1.0f - (f * f)))) - 1.0f);
            }
        };
        public static final c r = new c() {
            public float getInterpolation(float f) {
                float f2 = f - 1.0f;
                return (float) Math.sqrt((double) (1.0f - (f2 * f2)));
            }
        };
        public static final c s = new c() {
            public float getInterpolation(float f) {
                float f2 = f / 0.5f;
                if (f2 < 1.0f) {
                    return (((float) Math.sqrt((double) (1.0f - (f2 * f2)))) - 1.0f) * -0.5f;
                }
                float f3 = f2 - 2.0f;
                return (((float) Math.sqrt((double) (1.0f - (f3 * f3)))) + 1.0f) * 0.5f;
            }
        };
        public static final c t = new c() {
            public float getInterpolation(float f) {
                if (f == 0.0f) {
                    return 0.0f;
                }
                if (f == 1.0f) {
                    return 1.0f;
                }
                float f2 = f - 1.0f;
                return -(((float) Math.sin((((double) (f2 - ((0.3f / 6.2831855f) * ((float) Math.asin(1.0d))))) * 6.283185307179586d) / ((double) 0.3f))) * ((float) Math.pow(2.0d, (double) (10.0f * f2))));
            }
        };
        public static final c u = new c() {
            public float getInterpolation(float f) {
                if (f == 0.0f) {
                    return 0.0f;
                }
                if (f == 1.0f) {
                    return 1.0f;
                }
                return (((float) Math.sin((((double) (f - ((0.3f / 6.2831855f) * ((float) Math.asin(1.0d))))) * 6.283185307179586d) / ((double) 0.3f))) * ((float) Math.pow(2.0d, (double) (-10.0f * f)))) + 1.0f;
            }
        };
        public static final c v = new c() {
            public float getInterpolation(float f) {
                if (f == 0.0f) {
                    return 0.0f;
                }
                float f2 = f / 0.5f;
                if (f2 == 2.0f) {
                    return 1.0f;
                }
                float asin = (0.45000002f / 6.2831855f) * ((float) Math.asin(1.0d));
                if (f2 < 1.0f) {
                    float f3 = f2 - 1.0f;
                    return ((float) Math.sin((((double) ((f3 * 1.0f) - asin)) * 6.283185307179586d) / ((double) 0.45000002f))) * ((float) Math.pow(2.0d, (double) (10.0f * f3))) * -0.5f;
                }
                float f4 = f2 - 1.0f;
                return (((float) Math.sin((((double) ((f4 * 1.0f) - asin)) * 6.283185307179586d) / ((double) 0.45000002f))) * ((float) Math.pow(2.0d, (double) (-10.0f * f4))) * 0.5f) + 1.0f;
            }
        };
        public static final c w = new c() {
            public float getInterpolation(float f) {
                return f * f * ((2.70158f * f) - 1.70158f);
            }
        };
        public static final c x = new c() {
            public float getInterpolation(float f) {
                float f2 = f - 1.0f;
                return (((f2 * 2.70158f) + 1.70158f) * f2 * f2) + 1.0f;
            }
        };
        public static final c y = new c() {
            public float getInterpolation(float f) {
                float f2 = f / 0.5f;
                if (f2 < 1.0f) {
                    float f3 = 1.70158f * 1.525f;
                    return ((f2 * (1.0f + f3)) - f3) * f2 * f2 * 0.5f;
                }
                float f4 = f2 - 2.0f;
                float f5 = 1.70158f * 1.525f;
                return (((f5 + (f4 * (1.0f + f5))) * f4 * f4) + 2.0f) * 0.5f;
            }
        };
        public static final c z = new c() {
            public float getInterpolation(float f) {
                return 1.0f - a.A.getInterpolation(1.0f - f);
            }
        };
    }

    /* renamed from: com.github.mikephil.charting.a.b$b reason: collision with other inner class name */
    public enum C0040b {
        Linear,
        EaseInQuad,
        EaseOutQuad,
        EaseInOutQuad,
        EaseInCubic,
        EaseOutCubic,
        EaseInOutCubic,
        EaseInQuart,
        EaseOutQuart,
        EaseInOutQuart,
        EaseInSine,
        EaseOutSine,
        EaseInOutSine,
        EaseInExpo,
        EaseOutExpo,
        EaseInOutExpo,
        EaseInCirc,
        EaseOutCirc,
        EaseInOutCirc,
        EaseInElastic,
        EaseOutElastic,
        EaseInOutElastic,
        EaseInBack,
        EaseOutBack,
        EaseInOutBack,
        EaseInBounce,
        EaseOutBounce,
        EaseInOutBounce
    }

    public static c a(C0040b bVar) {
        switch (bVar) {
            case EaseInQuad:
                return a.f1653b;
            case EaseOutQuad:
                return a.c;
            case EaseInOutQuad:
                return a.d;
            case EaseInCubic:
                return a.e;
            case EaseOutCubic:
                return a.f;
            case EaseInOutCubic:
                return a.g;
            case EaseInQuart:
                return a.h;
            case EaseOutQuart:
                return a.i;
            case EaseInOutQuart:
                return a.j;
            case EaseInSine:
                return a.k;
            case EaseOutSine:
                return a.l;
            case EaseInOutSine:
                return a.m;
            case EaseInExpo:
                return a.n;
            case EaseOutExpo:
                return a.o;
            case EaseInOutExpo:
                return a.p;
            case EaseInCirc:
                return a.q;
            case EaseOutCirc:
                return a.r;
            case EaseInOutCirc:
                return a.s;
            case EaseInElastic:
                return a.t;
            case EaseOutElastic:
                return a.u;
            case EaseInOutElastic:
                return a.v;
            case EaseInBack:
                return a.w;
            case EaseOutBack:
                return a.x;
            case EaseInOutBack:
                return a.y;
            case EaseInBounce:
                return a.z;
            case EaseOutBounce:
                return a.A;
            case EaseInOutBounce:
                return a.B;
            default:
                return a.f1652a;
        }
    }
}
