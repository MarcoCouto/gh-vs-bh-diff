package com.github.mikephil.charting.a;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.os.Build.VERSION;
import com.github.mikephil.charting.a.b.C0040b;

public class a {

    /* renamed from: a reason: collision with root package name */
    protected float f1649a = 1.0f;

    /* renamed from: b reason: collision with root package name */
    protected float f1650b = 1.0f;
    private AnimatorUpdateListener c;

    public a() {
    }

    public a(AnimatorUpdateListener animatorUpdateListener) {
        this.c = animatorUpdateListener;
    }

    public float a() {
        return this.f1649a;
    }

    public void a(int i, C0040b bVar) {
        if (VERSION.SDK_INT >= 11) {
            ObjectAnimator ofFloat = ObjectAnimator.ofFloat(this, "phaseY", new float[]{0.0f, 1.0f});
            ofFloat.setInterpolator(b.a(bVar));
            ofFloat.setDuration((long) i);
            ofFloat.addUpdateListener(this.c);
            ofFloat.start();
        }
    }

    public float b() {
        return this.f1650b;
    }
}
