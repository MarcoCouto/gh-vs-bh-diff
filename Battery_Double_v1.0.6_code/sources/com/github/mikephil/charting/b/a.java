package com.github.mikephil.charting.b;

public abstract class a<T> {

    /* renamed from: a reason: collision with root package name */
    protected int f1656a;

    /* renamed from: b reason: collision with root package name */
    public final float[] f1657b;
    protected float c;
    protected float d;
    protected int e;
    protected int f;

    public a(int i) {
        this.f1656a = 0;
        this.c = 1.0f;
        this.d = 1.0f;
        this.e = 0;
        this.f = 0;
        this.f1656a = 0;
        this.f1657b = new float[i];
    }

    public void a() {
        this.f1656a = 0;
    }

    public void a(float f2, float f3) {
        this.c = f2;
        this.d = f3;
    }

    public int b() {
        return this.f1657b.length;
    }
}
