package com.github.mikephil.charting.b;

import com.github.mikephil.charting.g.b.a;

public class b extends a<a> {
    protected int g = 0;
    protected int h = 1;
    protected boolean i = false;
    protected boolean j = false;
    protected float k = 1.0f;

    public b(int i2, int i3, boolean z) {
        super(i2);
        this.h = i3;
        this.i = z;
    }

    public void a(float f) {
        this.k = f;
    }

    /* access modifiers changed from: protected */
    public void a(float f, float f2, float f3, float f4) {
        float[] fArr = this.f1657b;
        int i2 = this.f1656a;
        this.f1656a = i2 + 1;
        fArr[i2] = f;
        float[] fArr2 = this.f1657b;
        int i3 = this.f1656a;
        this.f1656a = i3 + 1;
        fArr2[i3] = f2;
        float[] fArr3 = this.f1657b;
        int i4 = this.f1656a;
        this.f1656a = i4 + 1;
        fArr3[i4] = f3;
        float[] fArr4 = this.f1657b;
        int i5 = this.f1656a;
        this.f1656a = i5 + 1;
        fArr4[i5] = f4;
    }

    public void a(int i2) {
        this.g = i2;
    }

    public void a(a aVar) {
        float f;
        float abs;
        float f2;
        float f3;
        float s = ((float) aVar.s()) * this.c;
        float f4 = this.k / 2.0f;
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (((float) i3) < s) {
                com.github.mikephil.charting.d.b bVar = (com.github.mikephil.charting.d.b) aVar.d(i3);
                if (bVar != null) {
                    float h2 = bVar.h();
                    float b2 = bVar.b();
                    float[] a2 = bVar.a();
                    if (!this.i || a2 == null) {
                        float f5 = h2 - f4;
                        float f6 = h2 + f4;
                        if (this.j) {
                            float f7 = b2 >= 0.0f ? b2 : 0.0f;
                            f = b2 <= 0.0f ? b2 : 0.0f;
                            b2 = f7;
                        } else {
                            f = b2 >= 0.0f ? b2 : 0.0f;
                            if (b2 > 0.0f) {
                                b2 = 0.0f;
                            }
                        }
                        if (f > 0.0f) {
                            f *= this.d;
                        } else {
                            b2 *= this.d;
                        }
                        a(f5, f, f6, b2);
                    } else {
                        float f8 = 0.0f;
                        float f9 = -bVar.f();
                        for (float f10 : a2) {
                            if (f10 >= 0.0f) {
                                abs = f8 + f10;
                                f2 = f8;
                                f8 = abs;
                            } else {
                                abs = Math.abs(f10) + f9;
                                float abs2 = Math.abs(f10) + f9;
                                f2 = f9;
                                f9 = abs2;
                            }
                            float f11 = h2 - f4;
                            float f12 = h2 + f4;
                            if (this.j) {
                                float f13 = f2 >= abs ? f2 : abs;
                                if (f2 > abs) {
                                    f2 = abs;
                                }
                                f3 = f13;
                            } else {
                                float f14 = f2 >= abs ? f2 : abs;
                                if (f2 > abs) {
                                    f2 = abs;
                                }
                                f3 = f2;
                                f2 = f14;
                            }
                            a(f11, f2 * this.d, f12, f3 * this.d);
                        }
                    }
                }
                i2 = i3 + 1;
            } else {
                a();
                return;
            }
        }
    }

    public void a(boolean z) {
        this.j = z;
    }
}
