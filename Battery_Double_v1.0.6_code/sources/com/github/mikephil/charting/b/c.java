package com.github.mikephil.charting.b;

import com.github.mikephil.charting.d.b;
import com.github.mikephil.charting.g.b.a;

public class c extends b {
    public c(int i, int i2, boolean z) {
        super(i, i2, z);
    }

    public void a(a aVar) {
        float f;
        float abs;
        float f2;
        float f3;
        float s = ((float) aVar.s()) * this.c;
        float f4 = this.k / 2.0f;
        int i = 0;
        while (true) {
            int i2 = i;
            if (((float) i2) < s) {
                b bVar = (b) aVar.d(i2);
                if (bVar != null) {
                    float h = bVar.h();
                    float b2 = bVar.b();
                    float[] a2 = bVar.a();
                    if (!this.i || a2 == null) {
                        float f5 = h - f4;
                        float f6 = h + f4;
                        if (this.j) {
                            float f7 = b2 >= 0.0f ? b2 : 0.0f;
                            f = b2 <= 0.0f ? b2 : 0.0f;
                            b2 = f7;
                        } else {
                            f = b2 >= 0.0f ? b2 : 0.0f;
                            if (b2 > 0.0f) {
                                b2 = 0.0f;
                            }
                        }
                        if (f > 0.0f) {
                            f *= this.d;
                        } else {
                            b2 *= this.d;
                        }
                        a(b2, f6, f, f5);
                    } else {
                        float f8 = 0.0f;
                        float f9 = -bVar.f();
                        for (float f10 : a2) {
                            if (f10 >= 0.0f) {
                                abs = f8 + f10;
                                f2 = f8;
                                f8 = abs;
                            } else {
                                abs = Math.abs(f10) + f9;
                                float abs2 = Math.abs(f10) + f9;
                                f2 = f9;
                                f9 = abs2;
                            }
                            float f11 = h - f4;
                            float f12 = h + f4;
                            if (this.j) {
                                float f13 = f2 >= abs ? f2 : abs;
                                if (f2 > abs) {
                                    f2 = abs;
                                }
                                f3 = f13;
                            } else {
                                float f14 = f2 >= abs ? f2 : abs;
                                if (f2 > abs) {
                                    f2 = abs;
                                }
                                f3 = f2;
                                f2 = f14;
                            }
                            a(f3 * this.d, f12, f2 * this.d, f11);
                        }
                    }
                }
                i = i2 + 1;
            } else {
                a();
                return;
            }
        }
    }
}
