package com.github.mikephil.charting.charts;

import android.graphics.RectF;
import android.util.Log;
import com.github.mikephil.charting.c.i;
import com.github.mikephil.charting.c.j.a;
import com.github.mikephil.charting.f.c;
import com.github.mikephil.charting.i.e;
import com.github.mikephil.charting.i.l;
import com.github.mikephil.charting.i.n;
import com.github.mikephil.charting.j.h;

public class d extends a {
    private RectF ac;

    public c a(float f, float f2) {
        if (this.E != null) {
            return getHighlighter().a(f2, f);
        }
        if (this.D) {
            Log.e("MPAndroidChart", "Can't select by touch. No data set.");
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.S = new com.github.mikephil.charting.j.c();
        super.a();
        this.s = new h(this.S);
        this.t = new h(this.S);
        this.Q = new e(this, this.T, this.S);
        setHighlighter(new com.github.mikephil.charting.f.d(this));
        this.q = new n(this.S, this.o, this.s);
        this.r = new n(this.S, this.p, this.t);
        this.u = new l(this.S, this.J, this.s, this);
    }

    /* access modifiers changed from: protected */
    public float[] a(c cVar) {
        return new float[]{cVar.i(), cVar.h()};
    }

    /* access modifiers changed from: protected */
    public void f() {
        this.t.a(this.p.t, this.p.u, this.J.u, this.J.t);
        this.s.a(this.o.t, this.o.u, this.J.u, this.J.t);
    }

    public float getHighestVisibleX() {
        a(a.LEFT).a(this.S.f(), this.S.e(), this.B);
        return (float) Math.min((double) this.J.s, this.B.f1750b);
    }

    public float getLowestVisibleX() {
        a(a.LEFT).a(this.S.f(), this.S.h(), this.A);
        return (float) Math.max((double) this.J.t, this.A.f1750b);
    }

    public void j() {
        a(this.ac);
        float f = 0.0f + this.ac.left;
        float f2 = this.ac.top + 0.0f;
        float f3 = 0.0f + this.ac.right;
        float f4 = this.ac.bottom + 0.0f;
        if (this.o.J()) {
            f2 += this.o.b(this.q.a());
        }
        if (this.p.J()) {
            f4 += this.p.b(this.r.a());
        }
        float f5 = (float) this.J.D;
        if (this.J.x()) {
            if (this.J.y() == i.a.BOTTOM) {
                f += f5;
            } else if (this.J.y() == i.a.TOP) {
                f3 += f5;
            } else if (this.J.y() == i.a.BOTH_SIDED) {
                f += f5;
                f3 += f5;
            }
        }
        float extraTopOffset = f2 + getExtraTopOffset();
        float extraRightOffset = f3 + getExtraRightOffset();
        float extraBottomOffset = f4 + getExtraBottomOffset();
        float extraLeftOffset = f + getExtraLeftOffset();
        float a2 = com.github.mikephil.charting.j.i.a(this.l);
        this.S.a(Math.max(a2, extraLeftOffset), Math.max(a2, extraTopOffset), Math.max(a2, extraRightOffset), Math.max(a2, extraBottomOffset));
        if (this.D) {
            Log.i("MPAndroidChart", "offsetLeft: " + extraLeftOffset + ", offsetTop: " + extraTopOffset + ", offsetRight: " + extraRightOffset + ", offsetBottom: " + extraBottomOffset);
            Log.i("MPAndroidChart", "Content: " + this.S.k().toString());
        }
        g();
        f();
    }

    public void setVisibleXRangeMaximum(float f) {
        this.S.c(this.J.u / f);
    }

    public void setVisibleXRangeMinimum(float f) {
        this.S.d(this.J.u / f);
    }
}
