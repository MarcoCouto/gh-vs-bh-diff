package com.github.mikephil.charting.charts;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import com.github.mikephil.charting.a.a;
import com.github.mikephil.charting.a.b.C0040b;
import com.github.mikephil.charting.c.e;
import com.github.mikephil.charting.d.g;
import com.github.mikephil.charting.d.i;
import com.github.mikephil.charting.g.b.d;
import com.github.mikephil.charting.h.b;
import com.github.mikephil.charting.i.f;
import com.github.mikephil.charting.j.j;
import java.util.ArrayList;
import java.util.Iterator;

@SuppressLint({"NewApi"})
public abstract class c<T extends g<? extends d<? extends i>>> extends ViewGroup implements com.github.mikephil.charting.g.a.c {
    protected boolean D = false;
    protected T E = null;
    protected boolean F = true;
    protected com.github.mikephil.charting.e.c G = new com.github.mikephil.charting.e.c(0);
    protected Paint H;
    protected Paint I;
    protected com.github.mikephil.charting.c.i J;
    protected boolean K = true;
    protected com.github.mikephil.charting.c.c L;
    protected e M;
    protected com.github.mikephil.charting.h.d N;
    protected b O;
    protected f P;
    protected com.github.mikephil.charting.i.d Q;
    protected com.github.mikephil.charting.f.e R;
    protected j S = new j();
    protected a T;
    protected com.github.mikephil.charting.f.c[] U;
    protected float V = 0.0f;
    protected boolean W = true;

    /* renamed from: a reason: collision with root package name */
    private boolean f1697a = true;
    protected com.github.mikephil.charting.c.d aa;
    protected ArrayList<Runnable> ab = new ArrayList<>();

    /* renamed from: b reason: collision with root package name */
    private float f1698b = 0.9f;
    private String c = "No chart data available.";
    private com.github.mikephil.charting.h.c d;
    private float e = 0.0f;
    private float f = 0.0f;
    private float g = 0.0f;
    private float h = 0.0f;
    private boolean i = false;
    private boolean j = false;

    public c(Context context) {
        super(context);
        a();
    }

    public c(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a();
    }

    public c(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        a();
    }

    private void a(View view) {
        if (view.getBackground() != null) {
            view.getBackground().setCallback(null);
        }
        if (view instanceof ViewGroup) {
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 < ((ViewGroup) view).getChildCount()) {
                    a(((ViewGroup) view).getChildAt(i3));
                    i2 = i3 + 1;
                } else {
                    ((ViewGroup) view).removeAllViews();
                    return;
                }
            }
        }
    }

    public boolean A() {
        return this.W;
    }

    public com.github.mikephil.charting.f.c a(float f2, float f3) {
        if (this.E != null) {
            return getHighlighter().a(f2, f3);
        }
        Log.e("MPAndroidChart", "Can't select by touch. No data set.");
        return null;
    }

    /* access modifiers changed from: protected */
    public void a() {
        setWillNotDraw(false);
        if (VERSION.SDK_INT < 11) {
            this.T = new a();
        } else {
            this.T = new a(new AnimatorUpdateListener() {
                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    c.this.postInvalidate();
                }
            });
        }
        com.github.mikephil.charting.j.i.a(getContext());
        this.V = com.github.mikephil.charting.j.i.a(500.0f);
        this.L = new com.github.mikephil.charting.c.c();
        this.M = new e();
        this.P = new f(this.S, this.M);
        this.J = new com.github.mikephil.charting.c.i();
        this.H = new Paint(1);
        this.I = new Paint(1);
        this.I.setColor(Color.rgb(247, 189, 51));
        this.I.setTextAlign(Align.CENTER);
        this.I.setTextSize(com.github.mikephil.charting.j.i.a(12.0f));
        if (this.D) {
            Log.i("", "Chart.init()");
        }
    }

    public void a(int i2, C0040b bVar) {
        this.T.a(i2, bVar);
    }

    public void a(com.github.mikephil.charting.f.c cVar, boolean z) {
        i iVar = null;
        if (cVar == null) {
            this.U = null;
        } else {
            if (this.D) {
                Log.i("MPAndroidChart", "Highlighted: " + cVar.toString());
            }
            i a2 = this.E.a(cVar);
            if (a2 == null) {
                this.U = null;
                cVar = null;
                iVar = a2;
            } else {
                this.U = new com.github.mikephil.charting.f.c[]{cVar};
                iVar = a2;
            }
        }
        setLastHighlighted(this.U);
        if (z && this.N != null) {
            if (!v()) {
                this.N.a();
            } else {
                this.N.a(iVar, cVar);
            }
        }
        invalidate();
    }

    /* access modifiers changed from: protected */
    public float[] a(com.github.mikephil.charting.f.c cVar) {
        return new float[]{cVar.h(), cVar.i()};
    }

    /* access modifiers changed from: protected */
    public abstract void b();

    /* access modifiers changed from: protected */
    public void b(Canvas canvas) {
        float f2;
        float f3;
        if (this.L != null && this.L.x()) {
            com.github.mikephil.charting.j.e b2 = this.L.b();
            this.H.setTypeface(this.L.u());
            this.H.setTextSize(this.L.v());
            this.H.setColor(this.L.w());
            this.H.setTextAlign(this.L.c());
            if (b2 == null) {
                f2 = (((float) getWidth()) - this.S.b()) - this.L.s();
                f3 = (((float) getHeight()) - this.S.d()) - this.L.t();
            } else {
                f2 = b2.f1751a;
                f3 = b2.f1752b;
            }
            canvas.drawText(this.L.a(), f2, f3, this.H);
        }
    }

    /* access modifiers changed from: protected */
    public void c(float f2, float f3) {
        this.G.a(com.github.mikephil.charting.j.i.b((this.E == null || this.E.j() < 2) ? Math.max(Math.abs(f2), Math.abs(f3)) : Math.abs(f3 - f2)));
    }

    /* access modifiers changed from: protected */
    public void c(Canvas canvas) {
        if (this.aa != null && A() && v()) {
            for (int i2 = 0; i2 < this.U.length; i2++) {
                com.github.mikephil.charting.f.c cVar = this.U[i2];
                d a2 = this.E.a(cVar.e());
                i a3 = this.E.a(this.U[i2]);
                int d2 = a2.d(a3);
                if (a3 != null && ((float) d2) <= ((float) a2.s()) * this.T.b()) {
                    float[] a4 = a(cVar);
                    if (this.S.b(a4[0], a4[1])) {
                        this.aa.a(a3, cVar);
                        this.aa.a(canvas, a4[0], a4[1]);
                    }
                }
            }
        }
    }

    public a getAnimator() {
        return this.T;
    }

    public com.github.mikephil.charting.j.e getCenter() {
        return com.github.mikephil.charting.j.e.a(((float) getWidth()) / 2.0f, ((float) getHeight()) / 2.0f);
    }

    public com.github.mikephil.charting.j.e getCenterOfView() {
        return getCenter();
    }

    public com.github.mikephil.charting.j.e getCenterOffsets() {
        return this.S.l();
    }

    public Bitmap getChartBitmap() {
        Bitmap createBitmap = Bitmap.createBitmap(getWidth(), getHeight(), Config.RGB_565);
        Canvas canvas = new Canvas(createBitmap);
        Drawable background = getBackground();
        if (background != null) {
            background.draw(canvas);
        } else {
            canvas.drawColor(-1);
        }
        draw(canvas);
        return createBitmap;
    }

    public RectF getContentRect() {
        return this.S.k();
    }

    public T getData() {
        return this.E;
    }

    public com.github.mikephil.charting.e.f getDefaultValueFormatter() {
        return this.G;
    }

    public com.github.mikephil.charting.c.c getDescription() {
        return this.L;
    }

    public float getDragDecelerationFrictionCoef() {
        return this.f1698b;
    }

    public float getExtraBottomOffset() {
        return this.g;
    }

    public float getExtraLeftOffset() {
        return this.h;
    }

    public float getExtraRightOffset() {
        return this.f;
    }

    public float getExtraTopOffset() {
        return this.e;
    }

    public com.github.mikephil.charting.f.c[] getHighlighted() {
        return this.U;
    }

    public com.github.mikephil.charting.f.e getHighlighter() {
        return this.R;
    }

    public ArrayList<Runnable> getJobs() {
        return this.ab;
    }

    public e getLegend() {
        return this.M;
    }

    public f getLegendRenderer() {
        return this.P;
    }

    public com.github.mikephil.charting.c.d getMarker() {
        return this.aa;
    }

    @Deprecated
    public com.github.mikephil.charting.c.d getMarkerView() {
        return getMarker();
    }

    public float getMaxHighlightDistance() {
        return this.V;
    }

    public com.github.mikephil.charting.h.c getOnChartGestureListener() {
        return this.d;
    }

    public b getOnTouchListener() {
        return this.O;
    }

    public com.github.mikephil.charting.i.d getRenderer() {
        return this.Q;
    }

    public j getViewPortHandler() {
        return this.S;
    }

    public com.github.mikephil.charting.c.i getXAxis() {
        return this.J;
    }

    public float getXChartMax() {
        return this.J.s;
    }

    public float getXChartMin() {
        return this.J.t;
    }

    public float getXRange() {
        return this.J.u;
    }

    public float getYMax() {
        return this.E.f();
    }

    public float getYMin() {
        return this.E.e();
    }

    public abstract void h();

    /* access modifiers changed from: protected */
    public abstract void j();

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.j) {
            a((View) this);
        }
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        boolean z = true;
        if (this.E == null) {
            if (TextUtils.isEmpty(this.c)) {
                z = false;
            }
            if (z) {
                com.github.mikephil.charting.j.e center = getCenter();
                canvas.drawText(this.c, center.f1751a, center.f1752b, this.I);
            }
        } else if (!this.i) {
            j();
            this.i = true;
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        for (int i6 = 0; i6 < getChildCount(); i6++) {
            getChildAt(i6).layout(i2, i3, i4, i5);
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
        int a2 = (int) com.github.mikephil.charting.j.i.a(50.0f);
        setMeasuredDimension(Math.max(getSuggestedMinimumWidth(), resolveSize(a2, i2)), Math.max(getSuggestedMinimumHeight(), resolveSize(a2, i3)));
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        if (this.D) {
            Log.i("MPAndroidChart", "OnSizeChanged()");
        }
        if (i2 > 0 && i3 > 0 && i2 < 10000 && i3 < 10000) {
            this.S.a((float) i2, (float) i3);
            if (this.D) {
                Log.i("MPAndroidChart", "Setting chart dimens, width: " + i2 + ", height: " + i3);
            }
            Iterator it = this.ab.iterator();
            while (it.hasNext()) {
                post((Runnable) it.next());
            }
            this.ab.clear();
        }
        h();
        super.onSizeChanged(i2, i3, i4, i5);
    }

    public void setData(T t) {
        this.E = t;
        this.i = false;
        if (t != null) {
            c(t.e(), t.f());
            for (d dVar : this.E.i()) {
                if (dVar.h() || dVar.g() == this.G) {
                    dVar.a((com.github.mikephil.charting.e.f) this.G);
                }
            }
            h();
            if (this.D) {
                Log.i("MPAndroidChart", "Data is set.");
            }
        }
    }

    public void setDescription(com.github.mikephil.charting.c.c cVar) {
        this.L = cVar;
    }

    public void setDragDecelerationEnabled(boolean z) {
        this.f1697a = z;
    }

    public void setDragDecelerationFrictionCoef(float f2) {
        float f3 = 0.0f;
        if (f2 >= 0.0f) {
            f3 = f2;
        }
        if (f3 >= 1.0f) {
            f3 = 0.999f;
        }
        this.f1698b = f3;
    }

    @Deprecated
    public void setDrawMarkerViews(boolean z) {
        setDrawMarkers(z);
    }

    public void setDrawMarkers(boolean z) {
        this.W = z;
    }

    public void setExtraBottomOffset(float f2) {
        this.g = com.github.mikephil.charting.j.i.a(f2);
    }

    public void setExtraLeftOffset(float f2) {
        this.h = com.github.mikephil.charting.j.i.a(f2);
    }

    public void setExtraRightOffset(float f2) {
        this.f = com.github.mikephil.charting.j.i.a(f2);
    }

    public void setExtraTopOffset(float f2) {
        this.e = com.github.mikephil.charting.j.i.a(f2);
    }

    public void setHardwareAccelerationEnabled(boolean z) {
        if (VERSION.SDK_INT < 11) {
            Log.e("MPAndroidChart", "Cannot enable/disable hardware acceleration for devices below API level 11.");
        } else if (z) {
            setLayerType(2, null);
        } else {
            setLayerType(1, null);
        }
    }

    public void setHighlightPerTapEnabled(boolean z) {
        this.F = z;
    }

    public void setHighlighter(com.github.mikephil.charting.f.b bVar) {
        this.R = bVar;
    }

    /* access modifiers changed from: protected */
    public void setLastHighlighted(com.github.mikephil.charting.f.c[] cVarArr) {
        if (cVarArr == null || cVarArr.length <= 0 || cVarArr[0] == null) {
            this.O.a((com.github.mikephil.charting.f.c) null);
        } else {
            this.O.a(cVarArr[0]);
        }
    }

    public void setLogEnabled(boolean z) {
        this.D = z;
    }

    public void setMarker(com.github.mikephil.charting.c.d dVar) {
        this.aa = dVar;
    }

    @Deprecated
    public void setMarkerView(com.github.mikephil.charting.c.d dVar) {
        setMarker(dVar);
    }

    public void setMaxHighlightDistance(float f2) {
        this.V = com.github.mikephil.charting.j.i.a(f2);
    }

    public void setNoDataText(String str) {
        this.c = str;
    }

    public void setNoDataTextColor(int i2) {
        this.I.setColor(i2);
    }

    public void setNoDataTextTypeface(Typeface typeface) {
        this.I.setTypeface(typeface);
    }

    public void setOnChartGestureListener(com.github.mikephil.charting.h.c cVar) {
        this.d = cVar;
    }

    public void setOnChartValueSelectedListener(com.github.mikephil.charting.h.d dVar) {
        this.N = dVar;
    }

    public void setOnTouchListener(b bVar) {
        this.O = bVar;
    }

    public void setRenderer(com.github.mikephil.charting.i.d dVar) {
        if (dVar != null) {
            this.Q = dVar;
        }
    }

    public void setTouchEnabled(boolean z) {
        this.K = z;
    }

    public void setUnbindEnabled(boolean z) {
        this.j = z;
    }

    public boolean u() {
        return this.F;
    }

    public boolean v() {
        return (this.U == null || this.U.length <= 0 || this.U[0] == null) ? false : true;
    }

    public boolean w() {
        return this.f1697a;
    }

    public boolean x() {
        return this.D;
    }

    public void y() {
        ViewParent parent = getParent();
        if (parent != null) {
            parent.requestDisallowInterceptTouchEvent(true);
        }
    }

    public void z() {
        ViewParent parent = getParent();
        if (parent != null) {
            parent.requestDisallowInterceptTouchEvent(false);
        }
    }
}
