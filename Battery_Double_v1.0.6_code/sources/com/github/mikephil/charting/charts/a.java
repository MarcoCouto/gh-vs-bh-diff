package com.github.mikephil.charting.charts;

import android.util.Log;
import com.github.mikephil.charting.f.c;
import com.github.mikephil.charting.i.b;

public class a extends b<com.github.mikephil.charting.d.a> implements com.github.mikephil.charting.g.a.a {

    /* renamed from: a reason: collision with root package name */
    protected boolean f1692a;
    private boolean ac;
    private boolean ad;
    private boolean ae;

    public c a(float f, float f2) {
        if (this.E == null) {
            Log.e("MPAndroidChart", "Can't select by touch. No data set.");
            return null;
        }
        c a2 = getHighlighter().a(f, f2);
        return (a2 == null || !e()) ? a2 : new c(a2.a(), a2.b(), a2.c(), a2.d(), a2.e(), -1, a2.g());
    }

    /* access modifiers changed from: protected */
    public void a() {
        super.a();
        this.Q = new b(this, this.T, this.S);
        setHighlighter(new com.github.mikephil.charting.f.a(this));
        getXAxis().c(0.5f);
        getXAxis().d(0.5f);
    }

    /* access modifiers changed from: protected */
    public void b() {
        if (this.ae) {
            this.J.a(((com.github.mikephil.charting.d.a) this.E).g() - (((com.github.mikephil.charting.d.a) this.E).a() / 2.0f), (((com.github.mikephil.charting.d.a) this.E).a() / 2.0f) + ((com.github.mikephil.charting.d.a) this.E).h());
        } else {
            this.J.a(((com.github.mikephil.charting.d.a) this.E).g(), ((com.github.mikephil.charting.d.a) this.E).h());
        }
        this.o.a(((com.github.mikephil.charting.d.a) this.E).a(com.github.mikephil.charting.c.j.a.LEFT), ((com.github.mikephil.charting.d.a) this.E).b(com.github.mikephil.charting.c.j.a.LEFT));
        this.p.a(((com.github.mikephil.charting.d.a) this.E).a(com.github.mikephil.charting.c.j.a.RIGHT), ((com.github.mikephil.charting.d.a) this.E).b(com.github.mikephil.charting.c.j.a.RIGHT));
    }

    public boolean c() {
        return this.ac;
    }

    public boolean d() {
        return this.ad;
    }

    public boolean e() {
        return this.f1692a;
    }

    public com.github.mikephil.charting.d.a getBarData() {
        return (com.github.mikephil.charting.d.a) this.E;
    }

    public void setDrawBarShadow(boolean z) {
        this.ad = z;
    }

    public void setDrawValueAboveBar(boolean z) {
        this.ac = z;
    }

    public void setFitBars(boolean z) {
        this.ae = z;
    }

    public void setHighlightFullBarEnabled(boolean z) {
        this.f1692a = z;
    }
}
