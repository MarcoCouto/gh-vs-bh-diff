package com.github.mikephil.charting.charts;

import android.content.Context;
import android.util.AttributeSet;
import com.github.mikephil.charting.d.j;
import com.github.mikephil.charting.g.a.d;
import com.github.mikephil.charting.i.g;

public class LineChart extends b<j> implements d {
    public LineChart(Context context) {
        super(context);
    }

    public LineChart(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public LineChart(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    /* access modifiers changed from: protected */
    public void a() {
        super.a();
        this.Q = new g(this, this.T, this.S);
    }

    public j getLineData() {
        return (j) this.E;
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        if (this.Q != null && (this.Q instanceof g)) {
            ((g) this.Q).b();
        }
        super.onDetachedFromWindow();
    }
}
