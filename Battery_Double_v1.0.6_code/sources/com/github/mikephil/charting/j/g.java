package com.github.mikephil.charting.j;

import android.graphics.Matrix;
import android.graphics.Path;
import android.graphics.RectF;
import com.github.mikephil.charting.d.i;
import com.github.mikephil.charting.g.b.e;

public class g {

    /* renamed from: a reason: collision with root package name */
    protected Matrix f1755a = new Matrix();

    /* renamed from: b reason: collision with root package name */
    protected Matrix f1756b = new Matrix();
    protected j c;
    protected float[] d = new float[1];
    protected float[] e = new float[1];
    protected float[] f = new float[1];
    protected float[] g = new float[1];
    protected Matrix h = new Matrix();
    float[] i = new float[2];
    private Matrix j = new Matrix();
    private Matrix k = new Matrix();

    public g(j jVar) {
        this.c = jVar;
    }

    public Matrix a() {
        this.j.set(this.f1755a);
        this.j.postConcat(this.c.f1759a);
        this.j.postConcat(this.f1756b);
        return this.j;
    }

    public d a(float f2, float f3) {
        d a2 = d.a(0.0d, 0.0d);
        a(f2, f3, a2);
        return a2;
    }

    public void a(float f2, float f3, float f4, float f5) {
        float f6 = 0.0f;
        float i2 = this.c.i() / f3;
        float j2 = this.c.j() / f4;
        if (Float.isInfinite(i2)) {
            i2 = 0.0f;
        }
        if (!Float.isInfinite(j2)) {
            f6 = j2;
        }
        this.f1755a.reset();
        this.f1755a.postTranslate(-f2, -f5);
        this.f1755a.postScale(i2, -f6);
    }

    public void a(float f2, float f3, d dVar) {
        this.i[0] = f2;
        this.i[1] = f3;
        b(this.i);
        dVar.f1749a = (double) this.i[0];
        dVar.f1750b = (double) this.i[1];
    }

    public void a(Path path) {
        path.transform(this.f1755a);
        path.transform(this.c.o());
        path.transform(this.f1756b);
    }

    public void a(RectF rectF) {
        this.f1755a.mapRect(rectF);
        this.c.o().mapRect(rectF);
        this.f1756b.mapRect(rectF);
    }

    public void a(RectF rectF, float f2) {
        rectF.top *= f2;
        rectF.bottom *= f2;
        this.f1755a.mapRect(rectF);
        this.c.o().mapRect(rectF);
        this.f1756b.mapRect(rectF);
    }

    public void a(boolean z) {
        this.f1756b.reset();
        if (!z) {
            this.f1756b.postTranslate(this.c.a(), this.c.m() - this.c.d());
            return;
        }
        this.f1756b.setTranslate(this.c.a(), -this.c.c());
        this.f1756b.postScale(1.0f, -1.0f);
    }

    public void a(float[] fArr) {
        this.f1755a.mapPoints(fArr);
        this.c.o().mapPoints(fArr);
        this.f1756b.mapPoints(fArr);
    }

    public float[] a(e eVar, float f2, float f3, int i2, int i3) {
        int i4 = (((int) (((float) (i3 - i2)) * f2)) + 1) * 2;
        if (this.f.length != i4) {
            this.f = new float[i4];
        }
        float[] fArr = this.f;
        for (int i5 = 0; i5 < i4; i5 += 2) {
            i d2 = eVar.d((i5 / 2) + i2);
            if (d2 != null) {
                fArr[i5] = d2.h();
                fArr[i5 + 1] = d2.b() * f3;
            } else {
                fArr[i5] = 0.0f;
                fArr[i5 + 1] = 0.0f;
            }
        }
        a().mapPoints(fArr);
        return fArr;
    }

    public d b(float f2, float f3) {
        this.i[0] = f2;
        this.i[1] = f3;
        a(this.i);
        return d.a((double) this.i[0], (double) this.i[1]);
    }

    public void b(RectF rectF, float f2) {
        rectF.left *= f2;
        rectF.right *= f2;
        this.f1755a.mapRect(rectF);
        this.c.o().mapRect(rectF);
        this.f1756b.mapRect(rectF);
    }

    public void b(float[] fArr) {
        Matrix matrix = this.h;
        matrix.reset();
        this.f1756b.invert(matrix);
        matrix.mapPoints(fArr);
        this.c.o().invert(matrix);
        matrix.mapPoints(fArr);
        this.f1755a.invert(matrix);
        matrix.mapPoints(fArr);
    }
}
