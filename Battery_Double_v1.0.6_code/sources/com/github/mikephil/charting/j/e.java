package com.github.mikephil.charting.j;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.github.mikephil.charting.j.f.a;

public class e extends a {
    public static final Creator<e> c = new Creator<e>() {
        /* renamed from: a */
        public e createFromParcel(Parcel parcel) {
            e eVar = new e(0.0f, 0.0f);
            eVar.a(parcel);
            return eVar;
        }

        /* renamed from: a */
        public e[] newArray(int i) {
            return new e[i];
        }
    };
    private static f<e> f = f.a(32, new e(0.0f, 0.0f));

    /* renamed from: a reason: collision with root package name */
    public float f1751a;

    /* renamed from: b reason: collision with root package name */
    public float f1752b;

    static {
        f.a(0.5f);
    }

    public e() {
    }

    public e(float f2, float f3) {
        this.f1751a = f2;
        this.f1752b = f3;
    }

    public static e a(float f2, float f3) {
        e eVar = (e) f.a();
        eVar.f1751a = f2;
        eVar.f1752b = f3;
        return eVar;
    }

    public static void a(e eVar) {
        f.a(eVar);
    }

    /* access modifiers changed from: protected */
    public a a() {
        return new e(0.0f, 0.0f);
    }

    public void a(Parcel parcel) {
        this.f1751a = parcel.readFloat();
        this.f1752b = parcel.readFloat();
    }
}
