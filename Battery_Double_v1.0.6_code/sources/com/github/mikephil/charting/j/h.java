package com.github.mikephil.charting.j;

public class h extends g {
    public h(j jVar) {
        super(jVar);
    }

    public void a(boolean z) {
        this.f1756b.reset();
        if (!z) {
            this.f1756b.postTranslate(this.c.a(), this.c.m() - this.c.d());
            return;
        }
        this.f1756b.setTranslate(-(this.c.n() - this.c.b()), this.c.m() - this.c.d());
        this.f1756b.postScale(-1.0f, 1.0f);
    }
}
