package com.github.mikephil.charting.j;

import com.github.mikephil.charting.j.f.a;

public final class b extends a {
    private static f<b> c = f.a(256, new b(0.0f, 0.0f));

    /* renamed from: a reason: collision with root package name */
    public float f1747a;

    /* renamed from: b reason: collision with root package name */
    public float f1748b;

    static {
        c.a(0.5f);
    }

    public b() {
    }

    public b(float f, float f2) {
        this.f1747a = f;
        this.f1748b = f2;
    }

    public static b a(float f, float f2) {
        b bVar = (b) c.a();
        bVar.f1747a = f;
        bVar.f1748b = f2;
        return bVar;
    }

    public static void a(b bVar) {
        c.a(bVar);
    }

    /* access modifiers changed from: protected */
    public a a() {
        return new b(0.0f, 0.0f);
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof b)) {
            return false;
        }
        b bVar = (b) obj;
        if (!(this.f1747a == bVar.f1747a && this.f1748b == bVar.f1748b)) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        return Float.floatToIntBits(this.f1747a) ^ Float.floatToIntBits(this.f1748b);
    }

    public String toString() {
        return this.f1747a + "x" + this.f1748b;
    }
}
