package com.github.mikephil.charting.j;

import com.github.mikephil.charting.j.f.a;

public class d extends a {
    private static f<d> c = f.a(64, new d(0.0d, 0.0d));

    /* renamed from: a reason: collision with root package name */
    public double f1749a;

    /* renamed from: b reason: collision with root package name */
    public double f1750b;

    static {
        c.a(0.5f);
    }

    private d(double d, double d2) {
        this.f1749a = d;
        this.f1750b = d2;
    }

    public static d a(double d, double d2) {
        d dVar = (d) c.a();
        dVar.f1749a = d;
        dVar.f1750b = d2;
        return dVar;
    }

    public static void a(d dVar) {
        c.a(dVar);
    }

    /* access modifiers changed from: protected */
    public a a() {
        return new d(0.0d, 0.0d);
    }

    public String toString() {
        return "MPPointD, x: " + this.f1749a + ", y: " + this.f1750b;
    }
}
