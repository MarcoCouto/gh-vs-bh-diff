package com.github.mikephil.charting.h;

import android.view.MotionEvent;
import com.github.mikephil.charting.h.b.a;

public interface c {
    void a(MotionEvent motionEvent);

    void a(MotionEvent motionEvent, float f, float f2);

    void a(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2);

    void a(MotionEvent motionEvent, a aVar);

    void b(MotionEvent motionEvent);

    void b(MotionEvent motionEvent, float f, float f2);

    void b(MotionEvent motionEvent, a aVar);

    void c(MotionEvent motionEvent);
}
