package com.github.mikephil.charting.h;

import android.annotation.SuppressLint;
import android.graphics.Matrix;
import android.util.Log;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.animation.AnimationUtils;
import com.github.mikephil.charting.charts.b;
import com.github.mikephil.charting.d.c;
import com.github.mikephil.charting.d.i;
import com.github.mikephil.charting.g.b.d;
import com.github.mikephil.charting.j.e;
import com.github.mikephil.charting.j.j;

public class a extends b<b<? extends c<? extends com.github.mikephil.charting.g.b.b<? extends i>>>> {
    private Matrix f = new Matrix();
    private Matrix g = new Matrix();
    private e h = e.a(0.0f, 0.0f);
    private e i = e.a(0.0f, 0.0f);
    private float j = 1.0f;
    private float k = 1.0f;
    private float l = 1.0f;
    private d m;
    private VelocityTracker n;
    private long o = 0;
    private e p = e.a(0.0f, 0.0f);
    private e q = e.a(0.0f, 0.0f);
    private float r;
    private float s;

    public a(b<? extends c<? extends com.github.mikephil.charting.g.b.b<? extends i>>> bVar, Matrix matrix, float f2) {
        super(bVar);
        this.f = matrix;
        this.r = com.github.mikephil.charting.j.i.a(f2);
        this.s = com.github.mikephil.charting.j.i.a(3.5f);
    }

    private static void a(e eVar, MotionEvent motionEvent) {
        float y = motionEvent.getY(0) + motionEvent.getY(1);
        eVar.f1751a = (motionEvent.getX(0) + motionEvent.getX(1)) / 2.0f;
        eVar.f1752b = y / 2.0f;
    }

    private void c(MotionEvent motionEvent) {
        this.g.set(this.f);
        this.h.f1751a = motionEvent.getX();
        this.h.f1752b = motionEvent.getY();
        this.m = ((b) this.e).b(motionEvent.getX(), motionEvent.getY());
    }

    private boolean c() {
        return (this.m == null && ((b) this.e).t()) || (this.m != null && ((b) this.e).c(this.m.q()));
    }

    private void d(MotionEvent motionEvent) {
        float x;
        float y;
        this.f1725a = com.github.mikephil.charting.h.b.a.DRAG;
        this.f.set(this.g);
        c onChartGestureListener = ((b) this.e).getOnChartGestureListener();
        if (!c()) {
            x = motionEvent.getX() - this.h.f1751a;
            y = motionEvent.getY() - this.h.f1752b;
        } else if (this.e instanceof com.github.mikephil.charting.charts.d) {
            x = -(motionEvent.getX() - this.h.f1751a);
            y = motionEvent.getY() - this.h.f1752b;
        } else {
            x = motionEvent.getX() - this.h.f1751a;
            y = -(motionEvent.getY() - this.h.f1752b);
        }
        this.f.postTranslate(x, y);
        if (onChartGestureListener != null) {
            onChartGestureListener.b(motionEvent, x, y);
        }
    }

    private void e(MotionEvent motionEvent) {
        boolean z = true;
        if (motionEvent.getPointerCount() >= 2) {
            c onChartGestureListener = ((b) this.e).getOnChartGestureListener();
            float g2 = g(motionEvent);
            if (g2 > this.s) {
                e a2 = a(this.i.f1751a, this.i.f1752b);
                j viewPortHandler = ((b) this.e).getViewPortHandler();
                if (this.f1726b == 4) {
                    this.f1725a = com.github.mikephil.charting.h.b.a.PINCH_ZOOM;
                    float f2 = g2 / this.l;
                    if (f2 >= 1.0f) {
                        z = false;
                    }
                    boolean w = z ? viewPortHandler.v() : viewPortHandler.w();
                    boolean y = z ? viewPortHandler.x() : viewPortHandler.y();
                    float f3 = ((b) this.e).m() ? f2 : 1.0f;
                    if (!((b) this.e).n()) {
                        f2 = 1.0f;
                    }
                    if (y || w) {
                        this.f.set(this.g);
                        this.f.postScale(f3, f2, a2.f1751a, a2.f1752b);
                        if (onChartGestureListener != null) {
                            onChartGestureListener.a(motionEvent, f3, f2);
                        }
                    }
                } else if (this.f1726b == 2 && ((b) this.e).m()) {
                    this.f1725a = com.github.mikephil.charting.h.b.a.X_ZOOM;
                    float h2 = h(motionEvent) / this.j;
                    if (h2 >= 1.0f) {
                        z = false;
                    }
                    if (z ? viewPortHandler.v() : viewPortHandler.w()) {
                        this.f.set(this.g);
                        this.f.postScale(h2, 1.0f, a2.f1751a, a2.f1752b);
                        if (onChartGestureListener != null) {
                            onChartGestureListener.a(motionEvent, h2, 1.0f);
                        }
                    }
                } else if (this.f1726b == 3 && ((b) this.e).n()) {
                    this.f1725a = com.github.mikephil.charting.h.b.a.Y_ZOOM;
                    float i2 = i(motionEvent) / this.k;
                    if ((i2 > 1.0f ? 1 : (i2 == 1.0f ? 0 : -1)) < 0 ? viewPortHandler.x() : viewPortHandler.y()) {
                        this.f.set(this.g);
                        this.f.postScale(1.0f, i2, a2.f1751a, a2.f1752b);
                        if (onChartGestureListener != null) {
                            onChartGestureListener.a(motionEvent, 1.0f, i2);
                        }
                    }
                }
                e.a(a2);
            }
        }
    }

    private void f(MotionEvent motionEvent) {
        com.github.mikephil.charting.f.c a2 = ((b) this.e).a(motionEvent.getX(), motionEvent.getY());
        if (a2 != null && !a2.a(this.c)) {
            this.c = a2;
            ((b) this.e).a(a2, true);
        }
    }

    private static float g(MotionEvent motionEvent) {
        float x = motionEvent.getX(0) - motionEvent.getX(1);
        float y = motionEvent.getY(0) - motionEvent.getY(1);
        return (float) Math.sqrt((double) ((x * x) + (y * y)));
    }

    private static float h(MotionEvent motionEvent) {
        return Math.abs(motionEvent.getX(0) - motionEvent.getX(1));
    }

    private static float i(MotionEvent motionEvent) {
        return Math.abs(motionEvent.getY(0) - motionEvent.getY(1));
    }

    public e a(float f2, float f3) {
        j viewPortHandler = ((b) this.e).getViewPortHandler();
        return e.a(f2 - viewPortHandler.a(), c() ? -(f3 - viewPortHandler.c()) : -((((float) ((b) this.e).getMeasuredHeight()) - f3) - viewPortHandler.d()));
    }

    public void a() {
        this.q.f1751a = 0.0f;
        this.q.f1752b = 0.0f;
    }

    public void b() {
        if (this.q.f1751a != 0.0f || this.q.f1752b != 0.0f) {
            long currentAnimationTimeMillis = AnimationUtils.currentAnimationTimeMillis();
            e eVar = this.q;
            eVar.f1751a = ((b) this.e).getDragDecelerationFrictionCoef() * eVar.f1751a;
            e eVar2 = this.q;
            eVar2.f1752b = ((b) this.e).getDragDecelerationFrictionCoef() * eVar2.f1752b;
            float f2 = ((float) (currentAnimationTimeMillis - this.o)) / 1000.0f;
            float f3 = this.q.f1751a * f2;
            float f4 = f2 * this.q.f1752b;
            e eVar3 = this.p;
            eVar3.f1751a = f3 + eVar3.f1751a;
            e eVar4 = this.p;
            eVar4.f1752b = f4 + eVar4.f1752b;
            MotionEvent obtain = MotionEvent.obtain(currentAnimationTimeMillis, currentAnimationTimeMillis, 2, this.p.f1751a, this.p.f1752b, 0);
            d(obtain);
            obtain.recycle();
            this.f = ((b) this.e).getViewPortHandler().a(this.f, this.e, false);
            this.o = currentAnimationTimeMillis;
            if (((double) Math.abs(this.q.f1751a)) >= 0.01d || ((double) Math.abs(this.q.f1752b)) >= 0.01d) {
                com.github.mikephil.charting.j.i.a((View) this.e);
                return;
            }
            ((b) this.e).j();
            ((b) this.e).postInvalidate();
            a();
        }
    }

    public boolean onDoubleTap(MotionEvent motionEvent) {
        float f2 = 1.4f;
        this.f1725a = com.github.mikephil.charting.h.b.a.DOUBLE_TAP;
        c onChartGestureListener = ((b) this.e).getOnChartGestureListener();
        if (onChartGestureListener != null) {
            onChartGestureListener.b(motionEvent);
        }
        if (((b) this.e).o() && ((c) ((b) this.e).getData()).j() > 0) {
            e a2 = a(motionEvent.getX(), motionEvent.getY());
            b bVar = (b) this.e;
            float f3 = ((b) this.e).m() ? 1.4f : 1.0f;
            if (!((b) this.e).n()) {
                f2 = 1.0f;
            }
            bVar.a(f3, f2, a2.f1751a, a2.f1752b);
            if (((b) this.e).x()) {
                Log.i("BarlineChartTouch", "Double-Tap, Zooming In, x: " + a2.f1751a + ", y: " + a2.f1752b);
            }
            e.a(a2);
        }
        return super.onDoubleTap(motionEvent);
    }

    public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f2, float f3) {
        this.f1725a = com.github.mikephil.charting.h.b.a.FLING;
        c onChartGestureListener = ((b) this.e).getOnChartGestureListener();
        if (onChartGestureListener != null) {
            onChartGestureListener.a(motionEvent, motionEvent2, f2, f3);
        }
        return super.onFling(motionEvent, motionEvent2, f2, f3);
    }

    public void onLongPress(MotionEvent motionEvent) {
        this.f1725a = com.github.mikephil.charting.h.b.a.LONG_PRESS;
        c onChartGestureListener = ((b) this.e).getOnChartGestureListener();
        if (onChartGestureListener != null) {
            onChartGestureListener.a(motionEvent);
        }
    }

    public boolean onSingleTapUp(MotionEvent motionEvent) {
        this.f1725a = com.github.mikephil.charting.h.b.a.SINGLE_TAP;
        c onChartGestureListener = ((b) this.e).getOnChartGestureListener();
        if (onChartGestureListener != null) {
            onChartGestureListener.c(motionEvent);
        }
        if (!((b) this.e).u()) {
            return false;
        }
        a(((b) this.e).a(motionEvent.getX(), motionEvent.getY()), motionEvent);
        return super.onSingleTapUp(motionEvent);
    }

    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouch(View view, MotionEvent motionEvent) {
        int i2 = 2;
        if (this.n == null) {
            this.n = VelocityTracker.obtain();
        }
        this.n.addMovement(motionEvent);
        if (motionEvent.getActionMasked() == 3 && this.n != null) {
            this.n.recycle();
            this.n = null;
        }
        if (this.f1726b == 0) {
            this.d.onTouchEvent(motionEvent);
        }
        if (((b) this.e).l() || ((b) this.e).m() || ((b) this.e).n()) {
            switch (motionEvent.getAction() & 255) {
                case 0:
                    a(motionEvent);
                    a();
                    c(motionEvent);
                    break;
                case 1:
                    VelocityTracker velocityTracker = this.n;
                    int pointerId = motionEvent.getPointerId(0);
                    velocityTracker.computeCurrentVelocity(1000, (float) com.github.mikephil.charting.j.i.c());
                    float yVelocity = velocityTracker.getYVelocity(pointerId);
                    float xVelocity = velocityTracker.getXVelocity(pointerId);
                    if ((Math.abs(xVelocity) > ((float) com.github.mikephil.charting.j.i.b()) || Math.abs(yVelocity) > ((float) com.github.mikephil.charting.j.i.b())) && this.f1726b == 1 && ((b) this.e).w()) {
                        a();
                        this.o = AnimationUtils.currentAnimationTimeMillis();
                        this.p.f1751a = motionEvent.getX();
                        this.p.f1752b = motionEvent.getY();
                        this.q.f1751a = xVelocity;
                        this.q.f1752b = yVelocity;
                        com.github.mikephil.charting.j.i.a((View) this.e);
                    }
                    if (this.f1726b == 2 || this.f1726b == 3 || this.f1726b == 4 || this.f1726b == 5) {
                        ((b) this.e).j();
                        ((b) this.e).postInvalidate();
                    }
                    this.f1726b = 0;
                    ((b) this.e).z();
                    if (this.n != null) {
                        this.n.recycle();
                        this.n = null;
                    }
                    b(motionEvent);
                    break;
                case 2:
                    if (this.f1726b != 1) {
                        if (this.f1726b != 2 && this.f1726b != 3 && this.f1726b != 4) {
                            if (this.f1726b == 0 && Math.abs(a(motionEvent.getX(), this.h.f1751a, motionEvent.getY(), this.h.f1752b)) > this.r) {
                                if (!((b) this.e).s()) {
                                    if (((b) this.e).l()) {
                                        this.f1725a = com.github.mikephil.charting.h.b.a.DRAG;
                                        this.f1726b = 1;
                                        break;
                                    }
                                } else if (!((b) this.e).q() && ((b) this.e).l()) {
                                    this.f1726b = 1;
                                    break;
                                } else {
                                    this.f1725a = com.github.mikephil.charting.h.b.a.DRAG;
                                    if (((b) this.e).k()) {
                                        f(motionEvent);
                                        break;
                                    }
                                }
                            }
                        } else {
                            ((b) this.e).y();
                            if (((b) this.e).m() || ((b) this.e).n()) {
                                e(motionEvent);
                                break;
                            }
                        }
                    } else {
                        ((b) this.e).y();
                        d(motionEvent);
                        break;
                    }
                    break;
                case 3:
                    this.f1726b = 0;
                    b(motionEvent);
                    break;
                case 5:
                    if (motionEvent.getPointerCount() >= 2) {
                        ((b) this.e).y();
                        c(motionEvent);
                        this.j = h(motionEvent);
                        this.k = i(motionEvent);
                        this.l = g(motionEvent);
                        if (this.l > 10.0f) {
                            if (((b) this.e).r()) {
                                this.f1726b = 4;
                            } else if (((b) this.e).m() != ((b) this.e).n()) {
                                this.f1726b = ((b) this.e).m() ? 2 : 3;
                            } else {
                                if (this.j <= this.k) {
                                    i2 = 3;
                                }
                                this.f1726b = i2;
                            }
                        }
                        a(this.i, motionEvent);
                        break;
                    }
                    break;
                case 6:
                    com.github.mikephil.charting.j.i.a(motionEvent, this.n);
                    this.f1726b = 5;
                    break;
            }
            this.f = ((b) this.e).getViewPortHandler().a(this.f, this.e, true);
        }
        return true;
    }
}
