package com.github.mikephil.charting.h;

import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View.OnTouchListener;
import com.github.mikephil.charting.charts.c;

public abstract class b<T extends c<?>> extends SimpleOnGestureListener implements OnTouchListener {

    /* renamed from: a reason: collision with root package name */
    protected a f1725a = a.NONE;

    /* renamed from: b reason: collision with root package name */
    protected int f1726b = 0;
    protected com.github.mikephil.charting.f.c c;
    protected GestureDetector d;
    protected T e;

    public enum a {
        NONE,
        DRAG,
        X_ZOOM,
        Y_ZOOM,
        PINCH_ZOOM,
        ROTATE,
        SINGLE_TAP,
        DOUBLE_TAP,
        LONG_PRESS,
        FLING
    }

    public b(T t) {
        this.e = t;
        this.d = new GestureDetector(t.getContext(), this);
    }

    protected static float a(float f, float f2, float f3, float f4) {
        float f5 = f - f2;
        float f6 = f3 - f4;
        return (float) Math.sqrt((double) ((f5 * f5) + (f6 * f6)));
    }

    public void a(MotionEvent motionEvent) {
        c onChartGestureListener = this.e.getOnChartGestureListener();
        if (onChartGestureListener != null) {
            onChartGestureListener.a(motionEvent, this.f1725a);
        }
    }

    public void a(com.github.mikephil.charting.f.c cVar) {
        this.c = cVar;
    }

    /* access modifiers changed from: protected */
    public void a(com.github.mikephil.charting.f.c cVar, MotionEvent motionEvent) {
        if (cVar == null || cVar.a(this.c)) {
            this.e.a((com.github.mikephil.charting.f.c) null, true);
            this.c = null;
            return;
        }
        this.e.a(cVar, true);
        this.c = cVar;
    }

    public void b(MotionEvent motionEvent) {
        c onChartGestureListener = this.e.getOnChartGestureListener();
        if (onChartGestureListener != null) {
            onChartGestureListener.b(motionEvent, this.f1725a);
        }
    }
}
