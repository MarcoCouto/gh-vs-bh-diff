package com.github.mikephil.charting.i;

import android.graphics.Canvas;
import android.graphics.Path;
import com.github.mikephil.charting.a.a;
import com.github.mikephil.charting.g.b.g;
import com.github.mikephil.charting.j.j;

public abstract class i extends c {

    /* renamed from: a reason: collision with root package name */
    private Path f1744a = new Path();

    public i(a aVar, j jVar) {
        super(aVar, jVar);
    }

    /* access modifiers changed from: protected */
    public void a(Canvas canvas, float f, float f2, g gVar) {
        this.i.setColor(gVar.a());
        this.i.setStrokeWidth(gVar.S());
        this.i.setPathEffect(gVar.T());
        if (gVar.Q()) {
            this.f1744a.reset();
            this.f1744a.moveTo(f, this.o.e());
            this.f1744a.lineTo(f, this.o.h());
            canvas.drawPath(this.f1744a, this.i);
        }
        if (gVar.R()) {
            this.f1744a.reset();
            this.f1744a.moveTo(this.o.f(), f2);
            this.f1744a.lineTo(this.o.g(), f2);
            canvas.drawPath(this.f1744a, this.i);
        }
    }
}
