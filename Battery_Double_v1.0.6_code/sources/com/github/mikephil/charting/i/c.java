package com.github.mikephil.charting.i;

import com.github.mikephil.charting.d.i;
import com.github.mikephil.charting.g.a.b;
import com.github.mikephil.charting.g.b.d;
import com.github.mikephil.charting.j.j;

public abstract class c extends d {
    protected a f = new a();

    protected class a {

        /* renamed from: a reason: collision with root package name */
        public int f1733a;

        /* renamed from: b reason: collision with root package name */
        public int f1734b;
        public int c;

        protected a() {
        }

        public void a(b bVar, com.github.mikephil.charting.g.b.b bVar2) {
            int i = 0;
            float max = Math.max(0.0f, Math.min(1.0f, c.this.g.b()));
            float lowestVisibleX = bVar.getLowestVisibleX();
            float highestVisibleX = bVar.getHighestVisibleX();
            i a2 = bVar2.a(lowestVisibleX, Float.NaN, com.github.mikephil.charting.d.h.a.DOWN);
            i a3 = bVar2.a(highestVisibleX, Float.NaN, com.github.mikephil.charting.d.h.a.UP);
            this.f1733a = a2 == null ? 0 : bVar2.d(a2);
            if (a3 != null) {
                i = bVar2.d(a3);
            }
            this.f1734b = i;
            this.c = (int) (((float) (this.f1734b - this.f1733a)) * max);
        }
    }

    public c(com.github.mikephil.charting.a.a aVar, j jVar) {
        super(aVar, jVar);
    }

    /* access modifiers changed from: protected */
    public boolean a(i iVar, com.github.mikephil.charting.g.b.b bVar) {
        if (iVar == null) {
            return false;
        }
        return iVar != null && ((float) bVar.d(iVar)) < ((float) bVar.s()) * this.g.b();
    }

    /* access modifiers changed from: protected */
    public boolean a(d dVar) {
        return dVar.p() && dVar.o();
    }
}
