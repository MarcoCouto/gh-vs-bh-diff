package com.github.mikephil.charting.i;

import android.graphics.Canvas;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.RectF;
import com.github.mikephil.charting.c.i;
import com.github.mikephil.charting.charts.a;
import com.github.mikephil.charting.j.b;
import com.github.mikephil.charting.j.d;
import com.github.mikephil.charting.j.e;
import com.github.mikephil.charting.j.g;
import com.github.mikephil.charting.j.j;
import java.util.List;

public class l extends k {
    protected a n;
    protected Path p = new Path();

    public l(j jVar, i iVar, g gVar, a aVar) {
        super(jVar, iVar, gVar);
        this.n = aVar;
    }

    public void a(float f, float f2, boolean z) {
        float f3;
        float f4;
        if (this.o.i() > 10.0f && !this.o.s()) {
            d a2 = this.f1730b.a(this.o.f(), this.o.h());
            d a3 = this.f1730b.a(this.o.f(), this.o.e());
            if (z) {
                f3 = (float) a3.f1750b;
                f4 = (float) a2.f1750b;
            } else {
                f3 = (float) a2.f1750b;
                f4 = (float) a3.f1750b;
            }
            d.a(a2);
            d.a(a3);
            f2 = f4;
            f = f3;
        }
        a(f, f2);
    }

    public void a(Canvas canvas) {
        if (this.g.x() && this.g.h()) {
            float s = this.g.s();
            this.d.setTypeface(this.g.u());
            this.d.setTextSize(this.g.v());
            this.d.setColor(this.g.w());
            e a2 = e.a(0.0f, 0.0f);
            if (this.g.y() == i.a.TOP) {
                a2.f1751a = 0.0f;
                a2.f1752b = 0.5f;
                a(canvas, s + this.o.g(), a2);
            } else if (this.g.y() == i.a.TOP_INSIDE) {
                a2.f1751a = 1.0f;
                a2.f1752b = 0.5f;
                a(canvas, this.o.g() - s, a2);
            } else if (this.g.y() == i.a.BOTTOM) {
                a2.f1751a = 1.0f;
                a2.f1752b = 0.5f;
                a(canvas, this.o.f() - s, a2);
            } else if (this.g.y() == i.a.BOTTOM_INSIDE) {
                a2.f1751a = 1.0f;
                a2.f1752b = 0.5f;
                a(canvas, s + this.o.f(), a2);
            } else {
                a2.f1751a = 0.0f;
                a2.f1752b = 0.5f;
                a(canvas, this.o.g() + s, a2);
                a2.f1751a = 1.0f;
                a2.f1752b = 0.5f;
                a(canvas, this.o.f() - s, a2);
            }
            e.a(a2);
        }
    }

    /* access modifiers changed from: protected */
    public void a(Canvas canvas, float f, float f2, Path path) {
        path.moveTo(this.o.g(), f2);
        path.lineTo(this.o.f(), f2);
        canvas.drawPath(path, this.c);
        path.reset();
    }

    /* access modifiers changed from: protected */
    public void a(Canvas canvas, float f, e eVar) {
        int i = 0;
        float z = this.g.z();
        boolean c = this.g.c();
        float[] fArr = new float[(this.g.d * 2)];
        for (int i2 = 0; i2 < fArr.length; i2 += 2) {
            if (c) {
                fArr[i2 + 1] = this.g.c[i2 / 2];
            } else {
                fArr[i2 + 1] = this.g.f1659b[i2 / 2];
            }
        }
        this.f1730b.a(fArr);
        while (true) {
            int i3 = i;
            if (i3 < fArr.length) {
                float f2 = fArr[i3 + 1];
                if (this.o.f(f2)) {
                    a(canvas, this.g.p().a(this.g.f1659b[i3 / 2], this.g), f, f2, eVar, z);
                }
                i = i3 + 2;
            } else {
                return;
            }
        }
    }

    public void b(Canvas canvas) {
        if (this.g.b() && this.g.x()) {
            this.e.setColor(this.g.g());
            this.e.setStrokeWidth(this.g.e());
            if (this.g.y() == i.a.TOP || this.g.y() == i.a.TOP_INSIDE || this.g.y() == i.a.BOTH_SIDED) {
                canvas.drawLine(this.o.g(), this.o.e(), this.o.g(), this.o.h(), this.e);
            }
            if (this.g.y() == i.a.BOTTOM || this.g.y() == i.a.BOTTOM_INSIDE || this.g.y() == i.a.BOTH_SIDED) {
                canvas.drawLine(this.o.f(), this.o.e(), this.o.f(), this.o.h(), this.e);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void c() {
        this.d.setTypeface(this.g.u());
        this.d.setTextSize(this.g.v());
        b c = com.github.mikephil.charting.j.i.c(this.d, this.g.o());
        float s = (float) ((int) (c.f1747a + (this.g.s() * 3.5f)));
        float f = c.f1748b;
        b a2 = com.github.mikephil.charting.j.i.a(c.f1747a, f, this.g.z());
        this.g.B = Math.round(s);
        this.g.C = Math.round(f);
        this.g.D = (int) (a2.f1747a + (this.g.s() * 3.5f));
        this.g.E = Math.round(a2.f1748b);
        b.a(a2);
    }

    public RectF d() {
        this.j.set(this.o.k());
        this.j.inset(0.0f, -this.f1729a.f());
        return this.j;
    }

    public void d(Canvas canvas) {
        int i = 0;
        List m = this.g.m();
        if (m != null && m.size() > 0) {
            float[] fArr = this.k;
            fArr[0] = 0.0f;
            fArr[1] = 0.0f;
            Path path = this.p;
            path.reset();
            while (true) {
                int i2 = i;
                if (i2 < m.size()) {
                    com.github.mikephil.charting.c.g gVar = (com.github.mikephil.charting.c.g) m.get(i2);
                    if (gVar.x()) {
                        int save = canvas.save();
                        this.l.set(this.o.k());
                        this.l.inset(0.0f, -gVar.b());
                        canvas.clipRect(this.l);
                        this.f.setStyle(Style.STROKE);
                        this.f.setColor(gVar.c());
                        this.f.setStrokeWidth(gVar.b());
                        this.f.setPathEffect(gVar.d());
                        fArr[1] = gVar.a();
                        this.f1730b.a(fArr);
                        path.moveTo(this.o.f(), fArr[1]);
                        path.lineTo(this.o.g(), fArr[1]);
                        canvas.drawPath(path, this.f);
                        path.reset();
                        String g = gVar.g();
                        if (g != null && !g.equals("")) {
                            this.f.setStyle(gVar.e());
                            this.f.setPathEffect(null);
                            this.f.setColor(gVar.w());
                            this.f.setStrokeWidth(0.5f);
                            this.f.setTextSize(gVar.v());
                            float b2 = (float) com.github.mikephil.charting.j.i.b(this.f, g);
                            float a2 = com.github.mikephil.charting.j.i.a(4.0f) + gVar.s();
                            float b3 = gVar.b() + b2 + gVar.t();
                            com.github.mikephil.charting.c.g.a f = gVar.f();
                            if (f == com.github.mikephil.charting.c.g.a.RIGHT_TOP) {
                                this.f.setTextAlign(Align.RIGHT);
                                canvas.drawText(g, this.o.g() - a2, b2 + (fArr[1] - b3), this.f);
                            } else if (f == com.github.mikephil.charting.c.g.a.RIGHT_BOTTOM) {
                                this.f.setTextAlign(Align.RIGHT);
                                canvas.drawText(g, this.o.g() - a2, fArr[1] + b3, this.f);
                            } else if (f == com.github.mikephil.charting.c.g.a.LEFT_TOP) {
                                this.f.setTextAlign(Align.LEFT);
                                canvas.drawText(g, this.o.f() + a2, b2 + (fArr[1] - b3), this.f);
                            } else {
                                this.f.setTextAlign(Align.LEFT);
                                canvas.drawText(g, this.o.a() + a2, fArr[1] + b3, this.f);
                            }
                        }
                        canvas.restoreToCount(save);
                    }
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }
    }
}
