package com.github.mikephil.charting.i;

import android.graphics.Canvas;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.RectF;
import com.github.mikephil.charting.c.j.a;
import com.github.mikephil.charting.c.j.b;
import com.github.mikephil.charting.j.d;
import com.github.mikephil.charting.j.g;
import com.github.mikephil.charting.j.i;
import com.github.mikephil.charting.j.j;
import java.util.List;

public class n extends m {
    protected Path r = new Path();
    protected Path s = new Path();
    protected float[] t = new float[4];

    public n(j jVar, com.github.mikephil.charting.c.j jVar2, g gVar) {
        super(jVar, jVar2, gVar);
        this.f.setTextAlign(Align.LEFT);
    }

    /* access modifiers changed from: protected */
    public Path a(Path path, int i, float[] fArr) {
        path.moveTo(fArr[i], this.o.e());
        path.lineTo(fArr[i], this.o.h());
        return path;
    }

    public void a(float f, float f2, boolean z) {
        float f3;
        float f4;
        if (this.o.j() > 10.0f && !this.o.t()) {
            d a2 = this.f1730b.a(this.o.f(), this.o.e());
            d a3 = this.f1730b.a(this.o.g(), this.o.e());
            if (!z) {
                f3 = (float) a2.f1749a;
                f4 = (float) a3.f1749a;
            } else {
                f3 = (float) a3.f1749a;
                f4 = (float) a2.f1749a;
            }
            d.a(a2);
            d.a(a3);
            f2 = f4;
            f = f3;
        }
        a(f, f2);
    }

    public void a(Canvas canvas) {
        if (this.g.x() && this.g.h()) {
            float[] c = c();
            this.d.setTypeface(this.g.u());
            this.d.setTextSize(this.g.v());
            this.d.setColor(this.g.w());
            this.d.setTextAlign(Align.CENTER);
            float a2 = i.a(2.5f);
            float b2 = (float) i.b(this.d, "Q");
            a y = this.g.y();
            b B = this.g.B();
            float h = y == a.LEFT ? B == b.OUTSIDE_CHART ? this.o.e() - a2 : this.o.e() - a2 : B == b.OUTSIDE_CHART ? a2 + b2 + this.o.h() : a2 + b2 + this.o.h();
            a(canvas, h, c, this.g.t());
        }
    }

    /* access modifiers changed from: protected */
    public void a(Canvas canvas, float f, float[] fArr, float f2) {
        this.d.setTypeface(this.g.u());
        this.d.setTextSize(this.g.v());
        this.d.setColor(this.g.w());
        int i = 0;
        while (i < this.g.d) {
            String b2 = this.g.b(i);
            if (this.g.C() || i < this.g.d - 1) {
                canvas.drawText(b2, fArr[i * 2], f - f2, this.d);
                i++;
            } else {
                return;
            }
        }
    }

    public RectF b() {
        this.j.set(this.o.k());
        this.j.inset(-this.f1729a.f(), 0.0f);
        return this.j;
    }

    public void b(Canvas canvas) {
        if (this.g.x() && this.g.b()) {
            this.e.setColor(this.g.g());
            this.e.setStrokeWidth(this.g.e());
            if (this.g.y() == a.LEFT) {
                canvas.drawLine(this.o.f(), this.o.e(), this.o.g(), this.o.e(), this.e);
                return;
            }
            canvas.drawLine(this.o.f(), this.o.h(), this.o.g(), this.o.h(), this.e);
        }
    }

    /* access modifiers changed from: protected */
    public float[] c() {
        if (this.k.length != this.g.d * 2) {
            this.k = new float[(this.g.d * 2)];
        }
        float[] fArr = this.k;
        for (int i = 0; i < fArr.length; i += 2) {
            fArr[i] = this.g.f1659b[i / 2];
        }
        this.f1730b.a(fArr);
        return fArr;
    }

    /* access modifiers changed from: protected */
    public void d(Canvas canvas) {
        int save = canvas.save();
        this.m.set(this.o.k());
        this.m.inset(-this.g.I(), 0.0f);
        canvas.clipRect(this.q);
        d b2 = this.f1730b.b(0.0f, 0.0f);
        this.h.setColor(this.g.H());
        this.h.setStrokeWidth(this.g.I());
        Path path = this.r;
        path.reset();
        path.moveTo(((float) b2.f1749a) - 1.0f, this.o.e());
        path.lineTo(((float) b2.f1749a) - 1.0f, this.o.h());
        canvas.drawPath(path, this.h);
        canvas.restoreToCount(save);
    }

    public void e(Canvas canvas) {
        List m = this.g.m();
        if (m != null && m.size() > 0) {
            float[] fArr = this.t;
            fArr[0] = 0.0f;
            fArr[1] = 0.0f;
            fArr[2] = 0.0f;
            fArr[3] = 0.0f;
            Path path = this.s;
            path.reset();
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < m.size()) {
                    com.github.mikephil.charting.c.g gVar = (com.github.mikephil.charting.c.g) m.get(i2);
                    if (gVar.x()) {
                        int save = canvas.save();
                        this.q.set(this.o.k());
                        this.q.inset(-gVar.b(), 0.0f);
                        canvas.clipRect(this.q);
                        fArr[0] = gVar.a();
                        fArr[2] = gVar.a();
                        this.f1730b.a(fArr);
                        fArr[1] = this.o.e();
                        fArr[3] = this.o.h();
                        path.moveTo(fArr[0], fArr[1]);
                        path.lineTo(fArr[2], fArr[3]);
                        this.f.setStyle(Style.STROKE);
                        this.f.setColor(gVar.c());
                        this.f.setPathEffect(gVar.d());
                        this.f.setStrokeWidth(gVar.b());
                        canvas.drawPath(path, this.f);
                        path.reset();
                        String g = gVar.g();
                        if (g != null && !g.equals("")) {
                            this.f.setStyle(gVar.e());
                            this.f.setPathEffect(null);
                            this.f.setColor(gVar.w());
                            this.f.setTypeface(gVar.u());
                            this.f.setStrokeWidth(0.5f);
                            this.f.setTextSize(gVar.v());
                            float b2 = gVar.b() + gVar.s();
                            float a2 = i.a(2.0f) + gVar.t();
                            com.github.mikephil.charting.c.g.a f = gVar.f();
                            if (f == com.github.mikephil.charting.c.g.a.RIGHT_TOP) {
                                float b3 = (float) i.b(this.f, g);
                                this.f.setTextAlign(Align.LEFT);
                                canvas.drawText(g, b2 + fArr[0], b3 + a2 + this.o.e(), this.f);
                            } else if (f == com.github.mikephil.charting.c.g.a.RIGHT_BOTTOM) {
                                this.f.setTextAlign(Align.LEFT);
                                canvas.drawText(g, fArr[0] + b2, this.o.h() - a2, this.f);
                            } else if (f == com.github.mikephil.charting.c.g.a.LEFT_TOP) {
                                this.f.setTextAlign(Align.RIGHT);
                                canvas.drawText(g, fArr[0] - b2, ((float) i.b(this.f, g)) + a2 + this.o.e(), this.f);
                            } else {
                                this.f.setTextAlign(Align.RIGHT);
                                canvas.drawText(g, fArr[0] - b2, this.o.h() - a2, this.f);
                            }
                        }
                        canvas.restoreToCount(save);
                    }
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }
    }
}
