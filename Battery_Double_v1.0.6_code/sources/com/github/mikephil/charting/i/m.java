package com.github.mikephil.charting.i;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.RectF;
import com.github.mikephil.charting.c.j;
import com.github.mikephil.charting.c.j.a;
import com.github.mikephil.charting.c.j.b;
import com.github.mikephil.charting.j.d;
import com.github.mikephil.charting.j.g;
import com.github.mikephil.charting.j.i;
import java.util.List;

public class m extends a {
    protected j g;
    protected Paint h;
    protected Path i = new Path();
    protected RectF j = new RectF();
    protected float[] k = new float[2];
    protected Path l = new Path();
    protected RectF m = new RectF();
    protected Path n = new Path();
    protected float[] p = new float[2];
    protected RectF q = new RectF();

    public m(com.github.mikephil.charting.j.j jVar, j jVar2, g gVar) {
        super(jVar, gVar, jVar2);
        this.g = jVar2;
        if (this.o != null) {
            this.d.setColor(-16777216);
            this.d.setTextSize(i.a(10.0f));
            this.h = new Paint(1);
            this.h.setColor(-7829368);
            this.h.setStrokeWidth(1.0f);
            this.h.setStyle(Style.STROKE);
        }
    }

    /* access modifiers changed from: protected */
    public Path a(Path path, int i2, float[] fArr) {
        path.moveTo(this.o.a(), fArr[i2 + 1]);
        path.lineTo(this.o.g(), fArr[i2 + 1]);
        return path;
    }

    public void a(Canvas canvas) {
        float g2;
        if (this.g.x() && this.g.h()) {
            float[] c = c();
            this.d.setTypeface(this.g.u());
            this.d.setTextSize(this.g.v());
            this.d.setColor(this.g.w());
            float s = this.g.s();
            float b2 = (((float) i.b(this.d, "A")) / 2.5f) + this.g.t();
            a y = this.g.y();
            b B = this.g.B();
            if (y == a.LEFT) {
                if (B == b.OUTSIDE_CHART) {
                    this.d.setTextAlign(Align.RIGHT);
                    g2 = this.o.a() - s;
                } else {
                    this.d.setTextAlign(Align.LEFT);
                    g2 = s + this.o.a();
                }
            } else if (B == b.OUTSIDE_CHART) {
                this.d.setTextAlign(Align.LEFT);
                g2 = s + this.o.g();
            } else {
                this.d.setTextAlign(Align.RIGHT);
                g2 = this.o.g() - s;
            }
            a(canvas, g2, c, b2);
        }
    }

    /* access modifiers changed from: protected */
    public void a(Canvas canvas, float f, float[] fArr, float f2) {
        int i2 = 0;
        while (i2 < this.g.d) {
            String b2 = this.g.b(i2);
            if (this.g.C() || i2 < this.g.d - 1) {
                canvas.drawText(b2, f, fArr[(i2 * 2) + 1] + f2, this.d);
                i2++;
            } else {
                return;
            }
        }
    }

    public RectF b() {
        this.j.set(this.o.k());
        this.j.inset(0.0f, -this.f1729a.f());
        return this.j;
    }

    public void b(Canvas canvas) {
        if (this.g.x() && this.g.b()) {
            this.e.setColor(this.g.g());
            this.e.setStrokeWidth(this.g.e());
            if (this.g.y() == a.LEFT) {
                canvas.drawLine(this.o.f(), this.o.e(), this.o.f(), this.o.h(), this.e);
                return;
            }
            canvas.drawLine(this.o.g(), this.o.e(), this.o.g(), this.o.h(), this.e);
        }
    }

    public void c(Canvas canvas) {
        if (this.g.x()) {
            if (this.g.a()) {
                int save = canvas.save();
                canvas.clipRect(b());
                float[] c = c();
                this.c.setColor(this.g.d());
                this.c.setStrokeWidth(this.g.f());
                this.c.setPathEffect(this.g.q());
                Path path = this.i;
                path.reset();
                for (int i2 = 0; i2 < c.length; i2 += 2) {
                    canvas.drawPath(a(path, i2, c), this.c);
                    path.reset();
                }
                canvas.restoreToCount(save);
            }
            if (this.g.G()) {
                d(canvas);
            }
        }
    }

    /* access modifiers changed from: protected */
    public float[] c() {
        if (this.k.length != this.g.d * 2) {
            this.k = new float[(this.g.d * 2)];
        }
        float[] fArr = this.k;
        for (int i2 = 0; i2 < fArr.length; i2 += 2) {
            fArr[i2 + 1] = this.g.f1659b[i2 / 2];
        }
        this.f1730b.a(fArr);
        return fArr;
    }

    /* access modifiers changed from: protected */
    public void d(Canvas canvas) {
        int save = canvas.save();
        this.m.set(this.o.k());
        this.m.inset(0.0f, -this.g.I());
        canvas.clipRect(this.m);
        d b2 = this.f1730b.b(0.0f, 0.0f);
        this.h.setColor(this.g.H());
        this.h.setStrokeWidth(this.g.I());
        Path path = this.l;
        path.reset();
        path.moveTo(this.o.f(), (float) b2.f1750b);
        path.lineTo(this.o.g(), (float) b2.f1750b);
        canvas.drawPath(path, this.h);
        canvas.restoreToCount(save);
    }

    public void e(Canvas canvas) {
        int i2 = 0;
        List m2 = this.g.m();
        if (m2 != null && m2.size() > 0) {
            float[] fArr = this.p;
            fArr[0] = 0.0f;
            fArr[1] = 0.0f;
            Path path = this.n;
            path.reset();
            while (true) {
                int i3 = i2;
                if (i3 < m2.size()) {
                    com.github.mikephil.charting.c.g gVar = (com.github.mikephil.charting.c.g) m2.get(i3);
                    if (gVar.x()) {
                        int save = canvas.save();
                        this.q.set(this.o.k());
                        this.q.inset(0.0f, -gVar.b());
                        canvas.clipRect(this.q);
                        this.f.setStyle(Style.STROKE);
                        this.f.setColor(gVar.c());
                        this.f.setStrokeWidth(gVar.b());
                        this.f.setPathEffect(gVar.d());
                        fArr[1] = gVar.a();
                        this.f1730b.a(fArr);
                        path.moveTo(this.o.f(), fArr[1]);
                        path.lineTo(this.o.g(), fArr[1]);
                        canvas.drawPath(path, this.f);
                        path.reset();
                        String g2 = gVar.g();
                        if (g2 != null && !g2.equals("")) {
                            this.f.setStyle(gVar.e());
                            this.f.setPathEffect(null);
                            this.f.setColor(gVar.w());
                            this.f.setTypeface(gVar.u());
                            this.f.setStrokeWidth(0.5f);
                            this.f.setTextSize(gVar.v());
                            float b2 = (float) i.b(this.f, g2);
                            float a2 = i.a(4.0f) + gVar.s();
                            float b3 = gVar.b() + b2 + gVar.t();
                            com.github.mikephil.charting.c.g.a f = gVar.f();
                            if (f == com.github.mikephil.charting.c.g.a.RIGHT_TOP) {
                                this.f.setTextAlign(Align.RIGHT);
                                canvas.drawText(g2, this.o.g() - a2, b2 + (fArr[1] - b3), this.f);
                            } else if (f == com.github.mikephil.charting.c.g.a.RIGHT_BOTTOM) {
                                this.f.setTextAlign(Align.RIGHT);
                                canvas.drawText(g2, this.o.g() - a2, fArr[1] + b3, this.f);
                            } else if (f == com.github.mikephil.charting.c.g.a.LEFT_TOP) {
                                this.f.setTextAlign(Align.LEFT);
                                canvas.drawText(g2, this.o.f() + a2, b2 + (fArr[1] - b3), this.f);
                            } else {
                                this.f.setTextAlign(Align.LEFT);
                                canvas.drawText(g2, this.o.a() + a2, fArr[1] + b3, this.f);
                            }
                        }
                        canvas.restoreToCount(save);
                    }
                    i2 = i3 + 1;
                } else {
                    return;
                }
            }
        }
    }
}
