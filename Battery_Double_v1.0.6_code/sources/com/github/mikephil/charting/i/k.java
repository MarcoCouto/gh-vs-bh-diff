package com.github.mikephil.charting.i;

import android.graphics.Canvas;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.RectF;
import com.github.mikephil.charting.c.i;
import com.github.mikephil.charting.c.i.a;
import com.github.mikephil.charting.j.b;
import com.github.mikephil.charting.j.d;
import com.github.mikephil.charting.j.e;
import com.github.mikephil.charting.j.g;
import com.github.mikephil.charting.j.j;
import java.util.List;

public class k extends a {
    protected i g;
    protected Path h = new Path();
    protected float[] i = new float[2];
    protected RectF j = new RectF();
    protected float[] k = new float[2];
    protected RectF l = new RectF();
    float[] m = new float[4];
    private Path n = new Path();

    public k(j jVar, i iVar, g gVar) {
        super(jVar, gVar, iVar);
        this.g = iVar;
        this.d.setColor(-16777216);
        this.d.setTextAlign(Align.CENTER);
        this.d.setTextSize(com.github.mikephil.charting.j.i.a(10.0f));
    }

    /* access modifiers changed from: protected */
    public void a(float f, float f2) {
        super.a(f, f2);
        c();
    }

    public void a(float f, float f2, boolean z) {
        float f3;
        float f4;
        if (this.o.i() > 10.0f && !this.o.t()) {
            d a2 = this.f1730b.a(this.o.f(), this.o.e());
            d a3 = this.f1730b.a(this.o.g(), this.o.e());
            if (z) {
                f3 = (float) a3.f1749a;
                f4 = (float) a2.f1749a;
            } else {
                f3 = (float) a2.f1749a;
                f4 = (float) a3.f1749a;
            }
            d.a(a2);
            d.a(a3);
            f2 = f4;
            f = f3;
        }
        a(f, f2);
    }

    public void a(Canvas canvas) {
        if (this.g.x() && this.g.h()) {
            float t = this.g.t();
            this.d.setTypeface(this.g.u());
            this.d.setTextSize(this.g.v());
            this.d.setColor(this.g.w());
            e a2 = e.a(0.0f, 0.0f);
            if (this.g.y() == a.TOP) {
                a2.f1751a = 0.5f;
                a2.f1752b = 1.0f;
                a(canvas, this.o.e() - t, a2);
            } else if (this.g.y() == a.TOP_INSIDE) {
                a2.f1751a = 0.5f;
                a2.f1752b = 1.0f;
                a(canvas, t + this.o.e() + ((float) this.g.E), a2);
            } else if (this.g.y() == a.BOTTOM) {
                a2.f1751a = 0.5f;
                a2.f1752b = 0.0f;
                a(canvas, t + this.o.h(), a2);
            } else if (this.g.y() == a.BOTTOM_INSIDE) {
                a2.f1751a = 0.5f;
                a2.f1752b = 0.0f;
                a(canvas, (this.o.h() - t) - ((float) this.g.E), a2);
            } else {
                a2.f1751a = 0.5f;
                a2.f1752b = 1.0f;
                a(canvas, this.o.e() - t, a2);
                a2.f1751a = 0.5f;
                a2.f1752b = 0.0f;
                a(canvas, t + this.o.h(), a2);
            }
            e.a(a2);
        }
    }

    /* access modifiers changed from: protected */
    public void a(Canvas canvas, float f, float f2, Path path) {
        path.moveTo(f, this.o.h());
        path.lineTo(f, this.o.e());
        canvas.drawPath(path, this.c);
        path.reset();
    }

    /* access modifiers changed from: protected */
    public void a(Canvas canvas, float f, e eVar) {
        float f2;
        int i2 = 0;
        float z = this.g.z();
        boolean c = this.g.c();
        float[] fArr = new float[(this.g.d * 2)];
        for (int i3 = 0; i3 < fArr.length; i3 += 2) {
            if (c) {
                fArr[i3] = this.g.c[i3 / 2];
            } else {
                fArr[i3] = this.g.f1659b[i3 / 2];
            }
        }
        this.f1730b.a(fArr);
        while (true) {
            int i4 = i2;
            if (i4 < fArr.length) {
                float f3 = fArr[i4];
                if (this.o.e(f3)) {
                    String a2 = this.g.p().a(this.g.f1659b[i4 / 2], this.g);
                    if (this.g.A()) {
                        if (i4 == this.g.d - 1 && this.g.d > 1) {
                            float a3 = (float) com.github.mikephil.charting.j.i.a(this.d, a2);
                            if (a3 > this.o.b() * 2.0f && f3 + a3 > this.o.n()) {
                                f3 -= a3 / 2.0f;
                            }
                            f2 = f3;
                            a(canvas, a2, f2, f, eVar, z);
                        } else if (i4 == 0) {
                            f2 = f3 + (((float) com.github.mikephil.charting.j.i.a(this.d, a2)) / 2.0f);
                            a(canvas, a2, f2, f, eVar, z);
                        }
                    }
                    f2 = f3;
                    a(canvas, a2, f2, f, eVar, z);
                }
                i2 = i4 + 2;
            } else {
                return;
            }
        }
    }

    public void a(Canvas canvas, com.github.mikephil.charting.c.g gVar, float[] fArr) {
        this.m[0] = fArr[0];
        this.m[1] = this.o.e();
        this.m[2] = fArr[0];
        this.m[3] = this.o.h();
        this.n.reset();
        this.n.moveTo(this.m[0], this.m[1]);
        this.n.lineTo(this.m[2], this.m[3]);
        this.f.setStyle(Style.STROKE);
        this.f.setColor(gVar.c());
        this.f.setStrokeWidth(gVar.b());
        this.f.setPathEffect(gVar.d());
        canvas.drawPath(this.n, this.f);
    }

    public void a(Canvas canvas, com.github.mikephil.charting.c.g gVar, float[] fArr, float f) {
        String g2 = gVar.g();
        if (g2 != null && !g2.equals("")) {
            this.f.setStyle(gVar.e());
            this.f.setPathEffect(null);
            this.f.setColor(gVar.w());
            this.f.setStrokeWidth(0.5f);
            this.f.setTextSize(gVar.v());
            float b2 = gVar.b() + gVar.s();
            com.github.mikephil.charting.c.g.a f2 = gVar.f();
            if (f2 == com.github.mikephil.charting.c.g.a.RIGHT_TOP) {
                float b3 = (float) com.github.mikephil.charting.j.i.b(this.f, g2);
                this.f.setTextAlign(Align.LEFT);
                canvas.drawText(g2, b2 + fArr[0], b3 + this.o.e() + f, this.f);
            } else if (f2 == com.github.mikephil.charting.c.g.a.RIGHT_BOTTOM) {
                this.f.setTextAlign(Align.LEFT);
                canvas.drawText(g2, b2 + fArr[0], this.o.h() - f, this.f);
            } else if (f2 == com.github.mikephil.charting.c.g.a.LEFT_TOP) {
                this.f.setTextAlign(Align.RIGHT);
                canvas.drawText(g2, fArr[0] - b2, ((float) com.github.mikephil.charting.j.i.b(this.f, g2)) + this.o.e() + f, this.f);
            } else {
                this.f.setTextAlign(Align.RIGHT);
                canvas.drawText(g2, fArr[0] - b2, this.o.h() - f, this.f);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a(Canvas canvas, String str, float f, float f2, e eVar, float f3) {
        com.github.mikephil.charting.j.i.a(canvas, str, f, f2, this.d, eVar, f3);
    }

    /* access modifiers changed from: protected */
    public void b() {
        this.c.setColor(this.g.d());
        this.c.setStrokeWidth(this.g.f());
        this.c.setPathEffect(this.g.q());
    }

    public void b(Canvas canvas) {
        if (this.g.b() && this.g.x()) {
            this.e.setColor(this.g.g());
            this.e.setStrokeWidth(this.g.e());
            this.e.setPathEffect(this.g.r());
            if (this.g.y() == a.TOP || this.g.y() == a.TOP_INSIDE || this.g.y() == a.BOTH_SIDED) {
                canvas.drawLine(this.o.f(), this.o.e(), this.o.g(), this.o.e(), this.e);
            }
            if (this.g.y() == a.BOTTOM || this.g.y() == a.BOTTOM_INSIDE || this.g.y() == a.BOTH_SIDED) {
                canvas.drawLine(this.o.f(), this.o.h(), this.o.g(), this.o.h(), this.e);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void c() {
        String o = this.g.o();
        this.d.setTypeface(this.g.u());
        this.d.setTextSize(this.g.v());
        b c = com.github.mikephil.charting.j.i.c(this.d, o);
        float f = c.f1747a;
        float b2 = (float) com.github.mikephil.charting.j.i.b(this.d, "Q");
        b a2 = com.github.mikephil.charting.j.i.a(f, b2, this.g.z());
        this.g.B = Math.round(f);
        this.g.C = Math.round(b2);
        this.g.D = Math.round(a2.f1747a);
        this.g.E = Math.round(a2.f1748b);
        b.a(a2);
        b.a(c);
    }

    public void c(Canvas canvas) {
        if (this.g.a() && this.g.x()) {
            int save = canvas.save();
            canvas.clipRect(d());
            if (this.i.length != this.f1729a.d * 2) {
                this.i = new float[(this.g.d * 2)];
            }
            float[] fArr = this.i;
            for (int i2 = 0; i2 < fArr.length; i2 += 2) {
                fArr[i2] = this.g.f1659b[i2 / 2];
                fArr[i2 + 1] = this.g.f1659b[i2 / 2];
            }
            this.f1730b.a(fArr);
            b();
            Path path = this.h;
            path.reset();
            for (int i3 = 0; i3 < fArr.length; i3 += 2) {
                a(canvas, fArr[i3], fArr[i3 + 1], path);
            }
            canvas.restoreToCount(save);
        }
    }

    public RectF d() {
        this.j.set(this.o.k());
        this.j.inset(-this.f1729a.f(), 0.0f);
        return this.j;
    }

    public void d(Canvas canvas) {
        List m2 = this.g.m();
        if (m2 != null && m2.size() > 0) {
            float[] fArr = this.k;
            fArr[0] = 0.0f;
            fArr[1] = 0.0f;
            for (int i2 = 0; i2 < m2.size(); i2++) {
                com.github.mikephil.charting.c.g gVar = (com.github.mikephil.charting.c.g) m2.get(i2);
                if (gVar.x()) {
                    int save = canvas.save();
                    this.l.set(this.o.k());
                    this.l.inset(-gVar.b(), 0.0f);
                    canvas.clipRect(this.l);
                    fArr[0] = gVar.a();
                    fArr[1] = 0.0f;
                    this.f1730b.a(fArr);
                    a(canvas, gVar, fArr);
                    a(canvas, gVar, fArr, 2.0f + gVar.t());
                    canvas.restoreToCount(save);
                }
            }
        }
    }
}
