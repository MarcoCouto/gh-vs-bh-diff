package com.github.mikephil.charting.i;

import android.graphics.Canvas;
import android.graphics.Paint.Align;
import android.graphics.RectF;
import com.github.mikephil.charting.b.c;
import com.github.mikephil.charting.d.b;
import com.github.mikephil.charting.e.f;
import com.github.mikephil.charting.g.a.a;
import com.github.mikephil.charting.g.b.d;
import com.github.mikephil.charting.j.g;
import com.github.mikephil.charting.j.i;
import com.github.mikephil.charting.j.j;
import java.util.List;

public class e extends b {
    private RectF l = new RectF();

    public e(a aVar, com.github.mikephil.charting.a.a aVar2, j jVar) {
        super(aVar, aVar2, jVar);
        this.k.setTextAlign(Align.LEFT);
    }

    public void a() {
        com.github.mikephil.charting.d.a barData = this.f1731a.getBarData();
        this.c = new c[barData.d()];
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.c.length) {
                com.github.mikephil.charting.g.b.a aVar = (com.github.mikephil.charting.g.b.a) barData.a(i2);
                this.c[i2] = new c((aVar.d() ? aVar.r() : 1) * aVar.s() * 4, barData.d(), aVar.d());
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a(float f, float f2, float f3, float f4, g gVar) {
        this.f1732b.set(f2, f - f4, f3, f + f4);
        gVar.b(this.f1732b, this.g.a());
    }

    /* access modifiers changed from: protected */
    public void a(Canvas canvas, com.github.mikephil.charting.g.b.a aVar, int i) {
        int i2 = 0;
        g a2 = this.f1731a.a(aVar.q());
        this.e.setColor(aVar.z());
        this.e.setStrokeWidth(i.a(aVar.y()));
        boolean z = aVar.y() > 0.0f;
        float b2 = this.g.b();
        float a3 = this.g.a();
        if (this.f1731a.d()) {
            this.d.setColor(aVar.t());
            float a4 = this.f1731a.getBarData().a() / 2.0f;
            int min = Math.min((int) Math.ceil((double) (((float) aVar.s()) * b2)), aVar.s());
            for (int i3 = 0; i3 < min; i3++) {
                float h = ((b) aVar.d(i3)).h();
                this.l.top = h - a4;
                this.l.bottom = h + a4;
                a2.a(this.l);
                if (this.o.i(this.l.bottom)) {
                    if (!this.o.j(this.l.top)) {
                        break;
                    }
                    this.l.left = this.o.f();
                    this.l.right = this.o.g();
                    canvas.drawRect(this.l, this.d);
                }
            }
        }
        com.github.mikephil.charting.b.b bVar = this.c[i];
        bVar.a(b2, a3);
        bVar.a(i);
        bVar.a(this.f1731a.c(aVar.q()));
        bVar.a(this.f1731a.getBarData().a());
        bVar.a(aVar);
        a2.a(bVar.f1657b);
        boolean z2 = aVar.b().size() == 1;
        if (z2) {
            this.h.setColor(aVar.c());
        }
        while (true) {
            int i4 = i2;
            if (i4 < bVar.b() && this.o.i(bVar.f1657b[i4 + 3])) {
                if (this.o.j(bVar.f1657b[i4 + 1])) {
                    if (!z2) {
                        this.h.setColor(aVar.a(i4 / 4));
                    }
                    canvas.drawRect(bVar.f1657b[i4], bVar.f1657b[i4 + 1], bVar.f1657b[i4 + 2], bVar.f1657b[i4 + 3], this.h);
                    if (z) {
                        canvas.drawRect(bVar.f1657b[i4], bVar.f1657b[i4 + 1], bVar.f1657b[i4 + 2], bVar.f1657b[i4 + 3], this.e);
                    }
                }
                i2 = i4 + 4;
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a(Canvas canvas, String str, float f, float f2, int i) {
        this.k.setColor(i);
        canvas.drawText(str, f, f2, this.k);
    }

    /* access modifiers changed from: protected */
    public void a(com.github.mikephil.charting.f.c cVar, RectF rectF) {
        cVar.a(rectF.centerY(), rectF.right);
    }

    /* access modifiers changed from: protected */
    public boolean a(com.github.mikephil.charting.g.a.c cVar) {
        return ((float) cVar.getData().j()) < ((float) cVar.getMaxVisibleCount()) * this.o.q();
    }

    public void b(Canvas canvas) {
        float f;
        if (a(this.f1731a)) {
            List i = this.f1731a.getBarData().i();
            float a2 = i.a(5.0f);
            boolean c = this.f1731a.c();
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 < this.f1731a.getBarData().d()) {
                    com.github.mikephil.charting.g.b.a aVar = (com.github.mikephil.charting.g.b.a) i.get(i3);
                    if (a(aVar)) {
                        boolean c2 = this.f1731a.c(aVar.q());
                        b((d) aVar);
                        float b2 = ((float) i.b(this.k, "10")) / 2.0f;
                        f g = aVar.g();
                        com.github.mikephil.charting.b.b bVar = this.c[i3];
                        float a3 = this.g.a();
                        if (!aVar.d()) {
                            int i4 = 0;
                            while (true) {
                                int i5 = i4;
                                if (((float) i5) >= ((float) bVar.f1657b.length) * this.g.b()) {
                                    break;
                                }
                                float f2 = (bVar.f1657b[i5 + 1] + bVar.f1657b[i5 + 3]) / 2.0f;
                                if (!this.o.i(bVar.f1657b[i5 + 1])) {
                                    break;
                                }
                                if (this.o.e(bVar.f1657b[i5]) && this.o.j(bVar.f1657b[i5 + 1])) {
                                    b bVar2 = (b) aVar.d(i5 / 4);
                                    float b3 = bVar2.b();
                                    String a4 = g.a(b3, bVar2, i3, this.o);
                                    float a5 = (float) i.a(this.k, a4);
                                    float f3 = c ? a2 : -(a5 + a2);
                                    float f4 = c ? -(a5 + a2) : a2;
                                    if (c2) {
                                        f3 = (-f3) - a5;
                                        f4 = (-f4) - a5;
                                    }
                                    float f5 = bVar.f1657b[i5 + 2];
                                    if (b3 < 0.0f) {
                                        f3 = f4;
                                    }
                                    a(canvas, a4, f5 + f3, f2 + b2, aVar.c(i5 / 2));
                                }
                                i4 = i5 + 4;
                            }
                        } else {
                            g a6 = this.f1731a.a(aVar.q());
                            int i6 = 0;
                            int i7 = 0;
                            while (((float) i6) < ((float) aVar.s()) * this.g.b()) {
                                b bVar3 = (b) aVar.d(i6);
                                int c3 = aVar.c(i6);
                                float[] a7 = bVar3.a();
                                if (a7 == null) {
                                    if (!this.o.i(bVar.f1657b[i7 + 1])) {
                                        break;
                                    } else if (this.o.e(bVar.f1657b[i7]) && this.o.j(bVar.f1657b[i7 + 1])) {
                                        String a8 = g.a(bVar3.b(), bVar3, i3, this.o);
                                        float a9 = (float) i.a(this.k, a8);
                                        float f6 = c ? a2 : -(a9 + a2);
                                        float f7 = c ? -(a9 + a2) : a2;
                                        if (c2) {
                                            f6 = (-f6) - a9;
                                            f7 = (-f7) - a9;
                                        }
                                        float f8 = bVar.f1657b[i7 + 2];
                                        if (bVar3.b() < 0.0f) {
                                            f6 = f7;
                                        }
                                        a(canvas, a8, f8 + f6, bVar.f1657b[i7 + 1] + b2, c3);
                                    }
                                } else {
                                    float[] fArr = new float[(a7.length * 2)];
                                    float f9 = 0.0f;
                                    float f10 = -bVar3.f();
                                    int i8 = 0;
                                    int i9 = 0;
                                    while (i8 < fArr.length) {
                                        float f11 = a7[i9];
                                        if (f11 >= 0.0f) {
                                            f9 += f11;
                                            f = f9;
                                        } else {
                                            float f12 = f10;
                                            f10 -= f11;
                                            f = f12;
                                        }
                                        fArr[i8] = f * a3;
                                        i8 += 2;
                                        i9++;
                                    }
                                    a6.a(fArr);
                                    int i10 = 0;
                                    while (true) {
                                        int i11 = i10;
                                        if (i11 >= fArr.length) {
                                            break;
                                        }
                                        float f13 = a7[i11 / 2];
                                        String a10 = g.a(f13, bVar3, i3, this.o);
                                        float a11 = (float) i.a(this.k, a10);
                                        float f14 = c ? a2 : -(a11 + a2);
                                        float f15 = c ? -(a11 + a2) : a2;
                                        if (c2) {
                                            f14 = (-f14) - a11;
                                            f15 = (-f15) - a11;
                                        }
                                        float f16 = fArr[i11];
                                        if (f13 < 0.0f) {
                                            f14 = f15;
                                        }
                                        float f17 = f16 + f14;
                                        float f18 = (bVar.f1657b[i7 + 1] + bVar.f1657b[i7 + 3]) / 2.0f;
                                        if (!this.o.i(f18)) {
                                            break;
                                        }
                                        if (this.o.e(f17) && this.o.j(f18)) {
                                            a(canvas, a10, f17, f18 + b2, c3);
                                        }
                                        i10 = i11 + 2;
                                    }
                                }
                                i6++;
                                i7 = a7 == null ? i7 + 4 : i7 + (a7.length * 4);
                            }
                        }
                    }
                    i2 = i3 + 1;
                } else {
                    return;
                }
            }
        }
    }
}
