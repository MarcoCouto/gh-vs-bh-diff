package com.github.mikephil.charting.i;

import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.FontMetrics;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Typeface;
import com.github.mikephil.charting.c.e;
import com.github.mikephil.charting.c.e.a;
import com.github.mikephil.charting.c.e.b;
import com.github.mikephil.charting.c.e.c;
import com.github.mikephil.charting.c.e.d;
import com.github.mikephil.charting.d.g;
import com.github.mikephil.charting.d.n;
import com.github.mikephil.charting.g.b.h;
import com.github.mikephil.charting.j.i;
import com.github.mikephil.charting.j.j;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class f extends j {

    /* renamed from: a reason: collision with root package name */
    protected Paint f1735a;

    /* renamed from: b reason: collision with root package name */
    protected Paint f1736b;
    protected e c;
    protected List<com.github.mikephil.charting.c.f> d = new ArrayList(16);
    protected FontMetrics e = new FontMetrics();
    private Path f = new Path();

    public f(j jVar, e eVar) {
        super(jVar);
        this.c = eVar;
        this.f1735a = new Paint(1);
        this.f1735a.setTextSize(i.a(9.0f));
        this.f1735a.setTextAlign(Align.LEFT);
        this.f1736b = new Paint(1);
        this.f1736b.setStyle(Style.FILL);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:161:0x041f, code lost:
        r8 = r2;
     */
    public void a(Canvas canvas) {
        float f2;
        float f3;
        float f4;
        float f5;
        boolean z;
        float f6;
        float f7;
        float f8;
        float f9;
        float f10;
        float f11;
        if (this.c.x()) {
            Typeface u = this.c.u();
            if (u != null) {
                this.f1735a.setTypeface(u);
            }
            this.f1735a.setTextSize(this.c.v());
            this.f1735a.setColor(this.c.w());
            float a2 = i.a(this.f1735a, this.e);
            float b2 = i.b(this.f1735a, this.e) + i.a(this.c.n());
            float b3 = a2 - (((float) i.b(this.f1735a, "ABC")) / 2.0f);
            com.github.mikephil.charting.c.f[] a3 = this.c.a();
            float a4 = i.a(this.c.o());
            float a5 = i.a(this.c.m());
            d f12 = this.c.f();
            c d2 = this.c.d();
            com.github.mikephil.charting.c.e.f e2 = this.c.e();
            a h = this.c.h();
            float a6 = i.a(this.c.j());
            float a7 = i.a(this.c.p());
            float t = this.c.t();
            float s = this.c.s();
            float f13 = 0.0f;
            switch (d2) {
                case LEFT:
                    if (f12 != d.VERTICAL) {
                        s += this.o.f();
                    }
                    if (h == a.RIGHT_TO_LEFT) {
                        f3 = f2 + this.c.f1662a;
                        break;
                    }
                case RIGHT:
                    f2 = f12 == d.VERTICAL ? this.o.n() - s : this.o.g() - s;
                    if (h == a.LEFT_TO_RIGHT) {
                        f3 = f2 - this.c.f1662a;
                        break;
                    }
                case CENTER:
                    f13 = (f12 == d.VERTICAL ? this.o.n() / 2.0f : this.o.f() + (this.o.i() / 2.0f)) + (h == a.LEFT_TO_RIGHT ? s : -s);
                    if (f12 == d.VERTICAL) {
                        f3 = (float) ((h == a.LEFT_TO_RIGHT ? ((double) s) + (((double) (-this.c.f1662a)) / 2.0d) : (((double) this.c.f1662a) / 2.0d) - ((double) s)) + ((double) f13));
                        break;
                    }
                default:
                    f3 = f13;
                    break;
            }
            switch (f12) {
                case HORIZONTAL:
                    List z2 = this.c.z();
                    List r = this.c.r();
                    List y = this.c.y();
                    float f14 = 0.0f;
                    switch (e2) {
                        case TOP:
                            f14 = t;
                            break;
                        case BOTTOM:
                            f14 = (this.o.m() - t) - this.c.f1663b;
                            break;
                        case CENTER:
                            f14 = ((this.o.m() - this.c.f1663b) / 2.0f) + t;
                            break;
                    }
                    int i = 0;
                    int length = a3.length;
                    int i2 = 0;
                    float f15 = f14;
                    float f16 = f3;
                    while (true) {
                        int i3 = i;
                        if (i2 < length) {
                            com.github.mikephil.charting.c.f fVar = a3[i2];
                            boolean z3 = fVar.f1679b != b.NONE;
                            float a8 = Float.isNaN(fVar.c) ? a6 : i.a(fVar.c);
                            if (i2 >= y.size() || !((Boolean) y.get(i2)).booleanValue()) {
                                f7 = f15;
                                f8 = f16;
                            } else {
                                f7 = a2 + b2 + f15;
                                f8 = f3;
                            }
                            if (f8 == f3 && d2 == c.CENTER && i3 < z2.size()) {
                                i = i3 + 1;
                                f9 = f8 + ((h == a.RIGHT_TO_LEFT ? ((com.github.mikephil.charting.j.b) z2.get(i3)).f1747a : -((com.github.mikephil.charting.j.b) z2.get(i3)).f1747a) / 2.0f);
                            } else {
                                i = i3;
                                f9 = f8;
                            }
                            boolean z4 = fVar.f1678a == null;
                            if (z3) {
                                float f17 = h == a.RIGHT_TO_LEFT ? f9 - a8 : f9;
                                a(canvas, f17, f7 + b3, fVar, this.c);
                                f10 = h == a.LEFT_TO_RIGHT ? f17 + a8 : f17;
                            } else {
                                f10 = f9;
                            }
                            if (!z4) {
                                if (z3) {
                                    f10 = (h == a.RIGHT_TO_LEFT ? -a4 : a4) + f10;
                                }
                                if (h == a.RIGHT_TO_LEFT) {
                                    f10 -= ((com.github.mikephil.charting.j.b) r.get(i2)).f1747a;
                                }
                                a(canvas, f10, f7 + a2, fVar.f1678a);
                                if (h == a.LEFT_TO_RIGHT) {
                                    f10 += ((com.github.mikephil.charting.j.b) r.get(i2)).f1747a;
                                }
                                f11 = h == a.RIGHT_TO_LEFT ? -a5 : a5;
                            } else {
                                f11 = h == a.RIGHT_TO_LEFT ? -a7 : a7;
                            }
                            i2++;
                            f15 = f7;
                            f16 = f11 + f10;
                        } else {
                            return;
                        }
                    }
                    break;
                case VERTICAL:
                    float f18 = 0.0f;
                    switch (e2) {
                        case TOP:
                            f18 = (d2 == c.CENTER ? 0.0f : this.o.e()) + t;
                            break;
                        case BOTTOM:
                            f18 = (d2 == c.CENTER ? this.o.m() : this.o.h()) - (this.c.f1663b + t);
                            break;
                        case CENTER:
                            f18 = ((this.o.m() / 2.0f) - (this.c.f1663b / 2.0f)) + this.c.t();
                            break;
                    }
                    int i4 = 0;
                    float f19 = f18;
                    boolean z5 = false;
                    float f20 = 0.0f;
                    while (i4 < a3.length) {
                        com.github.mikephil.charting.c.f fVar2 = a3[i4];
                        boolean z6 = fVar2.f1679b != b.NONE;
                        float a9 = Float.isNaN(fVar2.c) ? a6 : i.a(fVar2.c);
                        if (z6) {
                            f4 = h == a.LEFT_TO_RIGHT ? f3 + f20 : f3 - (a9 - f20);
                            a(canvas, f4, f19 + b3, fVar2, this.c);
                            if (h == a.LEFT_TO_RIGHT) {
                                f4 += a9;
                            }
                        } else {
                            f4 = f3;
                        }
                        if (fVar2.f1678a != null) {
                            if (z6 && !z5) {
                                f4 += h == a.LEFT_TO_RIGHT ? a4 : -a4;
                            } else if (z5) {
                                f4 = f3;
                            }
                            if (h == a.RIGHT_TO_LEFT) {
                                f4 -= (float) i.a(this.f1735a, fVar2.f1678a);
                            }
                            if (!z5) {
                                a(canvas, f4, f19 + a2, fVar2.f1678a);
                            } else {
                                f19 += a2 + b2;
                                a(canvas, f4, f19 + a2, fVar2.f1678a);
                            }
                            f6 = f19 + a2 + b2;
                            f5 = 0.0f;
                            z = z5;
                        } else {
                            f5 = f20 + a9 + a7;
                            z = true;
                            f6 = f19;
                        }
                        i4++;
                        f19 = f6;
                        z5 = z;
                        f20 = f5;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a(Canvas canvas, float f2, float f3, com.github.mikephil.charting.c.f fVar, e eVar) {
        if (fVar.f != 1122868 && fVar.f != 1122867 && fVar.f != 0) {
            int save = canvas.save();
            b bVar = fVar.f1679b;
            if (bVar == b.DEFAULT) {
                bVar = eVar.i();
            }
            this.f1736b.setColor(fVar.f);
            float a2 = i.a(Float.isNaN(fVar.c) ? eVar.j() : fVar.c);
            float f4 = a2 / 2.0f;
            switch (bVar) {
                case DEFAULT:
                case CIRCLE:
                    this.f1736b.setStyle(Style.FILL);
                    canvas.drawCircle(f2 + f4, f3, f4, this.f1736b);
                    break;
                case SQUARE:
                    this.f1736b.setStyle(Style.FILL);
                    canvas.drawRect(f2, f3 - f4, f2 + a2, f4 + f3, this.f1736b);
                    break;
                case LINE:
                    float a3 = i.a(Float.isNaN(fVar.d) ? eVar.k() : fVar.d);
                    DashPathEffect dashPathEffect = fVar.e == null ? eVar.l() : fVar.e;
                    this.f1736b.setStyle(Style.STROKE);
                    this.f1736b.setStrokeWidth(a3);
                    this.f1736b.setPathEffect(dashPathEffect);
                    this.f.reset();
                    this.f.moveTo(f2, f3);
                    this.f.lineTo(a2 + f2, f3);
                    canvas.drawPath(this.f, this.f1736b);
                    break;
            }
            canvas.restoreToCount(save);
        }
    }

    /* access modifiers changed from: protected */
    public void a(Canvas canvas, float f2, float f3, String str) {
        canvas.drawText(str, f2, f3, this.f1735a);
    }

    public void a(g<?> gVar) {
        if (!this.c.c()) {
            this.d.clear();
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= gVar.d()) {
                    break;
                }
                com.github.mikephil.charting.g.b.d a2 = gVar.a(i2);
                List b2 = a2.b();
                int s = a2.s();
                if ((a2 instanceof com.github.mikephil.charting.g.b.a) && ((com.github.mikephil.charting.g.b.a) a2).d()) {
                    com.github.mikephil.charting.g.b.a aVar = (com.github.mikephil.charting.g.b.a) a2;
                    String[] B = aVar.B();
                    int i3 = 0;
                    while (true) {
                        int i4 = i3;
                        if (i4 < b2.size() && i4 < aVar.r()) {
                            this.d.add(new com.github.mikephil.charting.c.f(B[i4 % B.length], a2.k(), a2.l(), a2.m(), a2.n(), ((Integer) b2.get(i4)).intValue()));
                            i3 = i4 + 1;
                        }
                    }
                    if (aVar.e() != null) {
                        this.d.add(new com.github.mikephil.charting.c.f(a2.e(), b.NONE, Float.NaN, Float.NaN, null, 1122867));
                    }
                } else if (a2 instanceof h) {
                    h hVar = (h) a2;
                    int i5 = 0;
                    while (true) {
                        int i6 = i5;
                        if (i6 < b2.size() && i6 < s) {
                            this.d.add(new com.github.mikephil.charting.c.f(((n) hVar.d(i6)).a(), a2.k(), a2.l(), a2.m(), a2.n(), ((Integer) b2.get(i6)).intValue()));
                            i5 = i6 + 1;
                        }
                    }
                    if (hVar.e() != null) {
                        this.d.add(new com.github.mikephil.charting.c.f(a2.e(), b.NONE, Float.NaN, Float.NaN, null, 1122867));
                    }
                } else if (!(a2 instanceof com.github.mikephil.charting.g.b.c) || ((com.github.mikephil.charting.g.b.c) a2).r() == 1122867) {
                    int i7 = 0;
                    while (true) {
                        int i8 = i7;
                        if (i8 >= b2.size() || i8 >= s) {
                            break;
                        }
                        this.d.add(new com.github.mikephil.charting.c.f((i8 >= b2.size() + -1 || i8 >= s + -1) ? gVar.a(i2).e() : null, a2.k(), a2.l(), a2.m(), a2.n(), ((Integer) b2.get(i8)).intValue()));
                        i7 = i8 + 1;
                    }
                } else {
                    int r = ((com.github.mikephil.charting.g.b.c) a2).r();
                    int d2 = ((com.github.mikephil.charting.g.b.c) a2).d();
                    this.d.add(new com.github.mikephil.charting.c.f(null, a2.k(), a2.l(), a2.m(), a2.n(), r));
                    this.d.add(new com.github.mikephil.charting.c.f(a2.e(), a2.k(), a2.l(), a2.m(), a2.n(), d2));
                }
                i = i2 + 1;
            }
            if (this.c.b() != null) {
                Collections.addAll(this.d, this.c.b());
            }
            this.c.a(this.d);
        }
        Typeface u = this.c.u();
        if (u != null) {
            this.f1735a.setTypeface(u);
        }
        this.f1735a.setTextSize(this.c.v());
        this.f1735a.setColor(this.c.w());
        this.c.a(this.f1735a, this.o);
    }
}
