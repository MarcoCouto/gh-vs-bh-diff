package com.github.mikephil.charting.i;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import com.github.mikephil.charting.a.a;
import com.github.mikephil.charting.e.f;
import com.github.mikephil.charting.f.c;
import com.github.mikephil.charting.j.i;
import com.github.mikephil.charting.j.j;

public abstract class d extends j {
    protected a g;
    protected Paint h = new Paint(1);
    protected Paint i;
    protected Paint j;
    protected Paint k;

    public d(a aVar, j jVar) {
        super(jVar);
        this.g = aVar;
        this.h.setStyle(Style.FILL);
        this.j = new Paint(4);
        this.k = new Paint(1);
        this.k.setColor(Color.rgb(63, 63, 63));
        this.k.setTextAlign(Align.CENTER);
        this.k.setTextSize(i.a(9.0f));
        this.i = new Paint(1);
        this.i.setStyle(Style.STROKE);
        this.i.setStrokeWidth(2.0f);
        this.i.setColor(Color.rgb(255, 187, 115));
    }

    public abstract void a();

    public abstract void a(Canvas canvas);

    public void a(Canvas canvas, f fVar, float f, com.github.mikephil.charting.d.i iVar, int i2, float f2, float f3, int i3) {
        this.k.setColor(i3);
        canvas.drawText(fVar.a(f, iVar, i2, this.o), f2, f3, this.k);
    }

    public abstract void a(Canvas canvas, c[] cVarArr);

    /* access modifiers changed from: protected */
    public boolean a(com.github.mikephil.charting.g.a.c cVar) {
        return ((float) cVar.getData().j()) < ((float) cVar.getMaxVisibleCount()) * this.o.p();
    }

    public abstract void b(Canvas canvas);

    /* access modifiers changed from: protected */
    public void b(com.github.mikephil.charting.g.b.d dVar) {
        this.k.setTypeface(dVar.i());
        this.k.setTextSize(dVar.j());
    }

    public abstract void c(Canvas canvas);
}
