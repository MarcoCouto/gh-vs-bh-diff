package com.github.mikephil.charting.i;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.RectF;
import com.github.mikephil.charting.f.c;
import com.github.mikephil.charting.f.f;
import com.github.mikephil.charting.g.a.a;
import com.github.mikephil.charting.g.b.d;
import com.github.mikephil.charting.j.g;
import com.github.mikephil.charting.j.i;
import com.github.mikephil.charting.j.j;
import java.util.List;

public class b extends c {

    /* renamed from: a reason: collision with root package name */
    protected a f1731a;

    /* renamed from: b reason: collision with root package name */
    protected RectF f1732b = new RectF();
    protected com.github.mikephil.charting.b.b[] c;
    protected Paint d;
    protected Paint e;
    private RectF l = new RectF();

    public b(a aVar, com.github.mikephil.charting.a.a aVar2, j jVar) {
        super(aVar2, jVar);
        this.f1731a = aVar;
        this.i = new Paint(1);
        this.i.setStyle(Style.FILL);
        this.i.setColor(Color.rgb(0, 0, 0));
        this.i.setAlpha(120);
        this.d = new Paint(1);
        this.d.setStyle(Style.FILL);
        this.e = new Paint(1);
        this.e.setStyle(Style.STROKE);
    }

    public void a() {
        com.github.mikephil.charting.d.a barData = this.f1731a.getBarData();
        this.c = new com.github.mikephil.charting.b.b[barData.d()];
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.c.length) {
                com.github.mikephil.charting.g.b.a aVar = (com.github.mikephil.charting.g.b.a) barData.a(i2);
                this.c[i2] = new com.github.mikephil.charting.b.b((aVar.d() ? aVar.r() : 1) * aVar.s() * 4, barData.d(), aVar.d());
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a(float f, float f2, float f3, float f4, g gVar) {
        this.f1732b.set(f - f4, f2, f + f4, f3);
        gVar.a(this.f1732b, this.g.a());
    }

    public void a(Canvas canvas) {
        com.github.mikephil.charting.d.a barData = this.f1731a.getBarData();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < barData.d()) {
                com.github.mikephil.charting.g.b.a aVar = (com.github.mikephil.charting.g.b.a) barData.a(i2);
                if (aVar.p()) {
                    a(canvas, aVar, i2);
                }
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a(Canvas canvas, com.github.mikephil.charting.g.b.a aVar, int i) {
        int i2 = 0;
        g a2 = this.f1731a.a(aVar.q());
        this.e.setColor(aVar.z());
        this.e.setStrokeWidth(i.a(aVar.y()));
        boolean z = aVar.y() > 0.0f;
        float b2 = this.g.b();
        float a3 = this.g.a();
        if (this.f1731a.d()) {
            this.d.setColor(aVar.t());
            float a4 = this.f1731a.getBarData().a() / 2.0f;
            int min = Math.min((int) Math.ceil((double) (((float) aVar.s()) * b2)), aVar.s());
            for (int i3 = 0; i3 < min; i3++) {
                float h = ((com.github.mikephil.charting.d.b) aVar.d(i3)).h();
                this.l.left = h - a4;
                this.l.right = h + a4;
                a2.a(this.l);
                if (this.o.g(this.l.right)) {
                    if (!this.o.h(this.l.left)) {
                        break;
                    }
                    this.l.top = this.o.e();
                    this.l.bottom = this.o.h();
                    canvas.drawRect(this.l, this.d);
                }
            }
        }
        com.github.mikephil.charting.b.b bVar = this.c[i];
        bVar.a(b2, a3);
        bVar.a(i);
        bVar.a(this.f1731a.c(aVar.q()));
        bVar.a(this.f1731a.getBarData().a());
        bVar.a(aVar);
        a2.a(bVar.f1657b);
        boolean z2 = aVar.b().size() == 1;
        if (z2) {
            this.h.setColor(aVar.c());
        }
        while (true) {
            int i4 = i2;
            if (i4 < bVar.b()) {
                if (this.o.g(bVar.f1657b[i4 + 2])) {
                    if (this.o.h(bVar.f1657b[i4])) {
                        if (!z2) {
                            this.h.setColor(aVar.a(i4 / 4));
                        }
                        canvas.drawRect(bVar.f1657b[i4], bVar.f1657b[i4 + 1], bVar.f1657b[i4 + 2], bVar.f1657b[i4 + 3], this.h);
                        if (z) {
                            canvas.drawRect(bVar.f1657b[i4], bVar.f1657b[i4 + 1], bVar.f1657b[i4 + 2], bVar.f1657b[i4 + 3], this.e);
                        }
                    } else {
                        return;
                    }
                }
                i2 = i4 + 4;
            } else {
                return;
            }
        }
    }

    public void a(Canvas canvas, c[] cVarArr) {
        float b2;
        float f;
        com.github.mikephil.charting.d.a barData = this.f1731a.getBarData();
        for (c cVar : cVarArr) {
            com.github.mikephil.charting.g.b.a aVar = (com.github.mikephil.charting.g.b.a) barData.a(cVar.e());
            if (aVar != null && aVar.f()) {
                com.github.mikephil.charting.d.b bVar = (com.github.mikephil.charting.d.b) aVar.b(cVar.a(), cVar.b());
                if (a(bVar, aVar)) {
                    g a2 = this.f1731a.a(aVar.q());
                    this.i.setColor(aVar.a());
                    this.i.setAlpha(aVar.A());
                    if (!(cVar.f() >= 0 && bVar.d())) {
                        b2 = bVar.b();
                        f = 0.0f;
                    } else if (this.f1731a.e()) {
                        b2 = bVar.e();
                        f = -bVar.f();
                    } else {
                        f fVar = bVar.c()[cVar.f()];
                        b2 = fVar.f1723a;
                        f = fVar.f1724b;
                    }
                    a(bVar.h(), b2, f, barData.a() / 2.0f, a2);
                    a(cVar, this.f1732b);
                    canvas.drawRect(this.f1732b, this.i);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a(c cVar, RectF rectF) {
        cVar.a(rectF.centerX(), rectF.top);
    }

    public void b(Canvas canvas) {
        float f;
        float f2;
        float f3;
        if (a((com.github.mikephil.charting.g.a.c) this.f1731a)) {
            List i = this.f1731a.getBarData().i();
            float a2 = i.a(4.5f);
            boolean c2 = this.f1731a.c();
            for (int i2 = 0; i2 < this.f1731a.getBarData().d(); i2++) {
                com.github.mikephil.charting.g.b.a aVar = (com.github.mikephil.charting.g.b.a) i.get(i2);
                if (a(aVar)) {
                    b((d) aVar);
                    boolean c3 = this.f1731a.c(aVar.q());
                    float b2 = (float) i.b(this.k, "8");
                    float f4 = c2 ? -a2 : b2 + a2;
                    float f5 = c2 ? b2 + a2 : -a2;
                    if (c3) {
                        f = (-f5) - b2;
                        f2 = (-f4) - b2;
                    } else {
                        f = f5;
                        f2 = f4;
                    }
                    com.github.mikephil.charting.b.b bVar = this.c[i2];
                    float a3 = this.g.a();
                    if (!aVar.d()) {
                        int i3 = 0;
                        while (true) {
                            int i4 = i3;
                            if (((float) i4) >= ((float) bVar.f1657b.length) * this.g.b()) {
                                break;
                            }
                            float f6 = (bVar.f1657b[i4] + bVar.f1657b[i4 + 2]) / 2.0f;
                            if (!this.o.h(f6)) {
                                break;
                            }
                            if (this.o.f(bVar.f1657b[i4 + 1]) && this.o.g(f6)) {
                                com.github.mikephil.charting.d.b bVar2 = (com.github.mikephil.charting.d.b) aVar.d(i4 / 4);
                                float b3 = bVar2.b();
                                a(canvas, aVar.g(), b3, bVar2, i2, f6, b3 >= 0.0f ? bVar.f1657b[i4 + 1] + f2 : bVar.f1657b[i4 + 3] + f, aVar.c(i4 / 4));
                            }
                            i3 = i4 + 4;
                        }
                    } else {
                        g a4 = this.f1731a.a(aVar.q());
                        int i5 = 0;
                        int i6 = 0;
                        while (((float) i5) < ((float) aVar.s()) * this.g.b()) {
                            com.github.mikephil.charting.d.b bVar3 = (com.github.mikephil.charting.d.b) aVar.d(i5);
                            float[] a5 = bVar3.a();
                            float f7 = (bVar.f1657b[i6] + bVar.f1657b[i6 + 2]) / 2.0f;
                            int c4 = aVar.c(i5);
                            if (a5 == null) {
                                if (!this.o.h(f7)) {
                                    break;
                                } else if (this.o.f(bVar.f1657b[i6 + 1]) && this.o.g(f7)) {
                                    a(canvas, aVar.g(), bVar3.b(), bVar3, i2, f7, bVar.f1657b[i6 + 1] + (bVar3.b() >= 0.0f ? f2 : f), c4);
                                }
                            } else {
                                float[] fArr = new float[(a5.length * 2)];
                                float f8 = 0.0f;
                                float f9 = -bVar3.f();
                                int i7 = 0;
                                int i8 = 0;
                                while (i7 < fArr.length) {
                                    float f10 = a5[i8];
                                    if (f10 >= 0.0f) {
                                        f8 += f10;
                                        f3 = f8;
                                    } else {
                                        float f11 = f9;
                                        f9 -= f10;
                                        f3 = f11;
                                    }
                                    fArr[i7 + 1] = f3 * a3;
                                    i7 += 2;
                                    i8++;
                                }
                                a4.a(fArr);
                                int i9 = 0;
                                while (true) {
                                    int i10 = i9;
                                    if (i10 >= fArr.length) {
                                        break;
                                    }
                                    float f12 = fArr[i10 + 1] + (a5[i10 / 2] >= 0.0f ? f2 : f);
                                    if (!this.o.h(f7)) {
                                        break;
                                    }
                                    if (this.o.f(f12) && this.o.g(f7)) {
                                        a(canvas, aVar.g(), a5[i10 / 2], bVar3, i2, f7, f12, c4);
                                    }
                                    i9 = i10 + 2;
                                }
                            }
                            i5++;
                            i6 = a5 == null ? i6 + 4 : i6 + (a5.length * 4);
                        }
                    }
                }
            }
        }
    }

    public void c(Canvas canvas) {
    }
}
