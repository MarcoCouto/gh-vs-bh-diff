package com.github.mikephil.charting.i;

import android.graphics.Paint;
import android.graphics.Paint.Style;
import com.github.mikephil.charting.j.d;
import com.github.mikephil.charting.j.g;
import com.github.mikephil.charting.j.i;
import com.github.mikephil.charting.j.j;

public abstract class a extends j {

    /* renamed from: a reason: collision with root package name */
    protected com.github.mikephil.charting.c.a f1729a;

    /* renamed from: b reason: collision with root package name */
    protected g f1730b;
    protected Paint c;
    protected Paint d;
    protected Paint e;
    protected Paint f;

    public a(j jVar, g gVar, com.github.mikephil.charting.c.a aVar) {
        super(jVar);
        this.f1730b = gVar;
        this.f1729a = aVar;
        if (this.o != null) {
            this.d = new Paint(1);
            this.c = new Paint();
            this.c.setColor(-7829368);
            this.c.setStrokeWidth(1.0f);
            this.c.setStyle(Style.STROKE);
            this.c.setAlpha(90);
            this.e = new Paint();
            this.e.setColor(-16777216);
            this.e.setStrokeWidth(1.0f);
            this.e.setStyle(Style.STROKE);
            this.f = new Paint(1);
            this.f.setStyle(Style.STROKE);
        }
    }

    public Paint a() {
        return this.d;
    }

    /* access modifiers changed from: protected */
    public void a(float f2, float f3) {
        int j = this.f1729a.j();
        double abs = (double) Math.abs(f3 - f2);
        if (j == 0 || abs <= 0.0d || Double.isInfinite(abs)) {
            this.f1729a.f1659b = new float[0];
            this.f1729a.c = new float[0];
            this.f1729a.d = 0;
            return;
        }
        double a2 = (double) i.a(abs / ((double) j));
        if (this.f1729a.k() && a2 < ((double) this.f1729a.l())) {
            a2 = (double) this.f1729a.l();
        }
        double a3 = (double) i.a(Math.pow(10.0d, (double) ((int) Math.log10(a2))));
        if (((int) (a2 / a3)) > 5) {
            a2 = Math.floor(10.0d * a3);
        }
        int i = this.f1729a.c() ? 1 : 0;
        if (this.f1729a.i()) {
            a2 = (double) (((float) abs) / ((float) (j - 1)));
            this.f1729a.d = j;
            if (this.f1729a.f1659b.length < j) {
                this.f1729a.f1659b = new float[j];
            }
            for (int i2 = 0; i2 < j; i2++) {
                this.f1729a.f1659b[i2] = f2;
                f2 = (float) (((double) f2) + a2);
            }
            i = j;
        } else {
            double ceil = a2 == 0.0d ? 0.0d : Math.ceil(((double) f2) / a2) * a2;
            if (this.f1729a.c()) {
                ceil -= a2;
            }
            double b2 = a2 == 0.0d ? 0.0d : i.b(Math.floor(((double) f3) / a2) * a2);
            if (a2 != 0.0d) {
                double d2 = ceil;
                while (d2 <= b2) {
                    d2 += a2;
                    i++;
                }
            }
            this.f1729a.d = i;
            if (this.f1729a.f1659b.length < i) {
                this.f1729a.f1659b = new float[i];
            }
            for (int i3 = 0; i3 < i; i3++) {
                if (ceil == 0.0d) {
                    ceil = 0.0d;
                }
                this.f1729a.f1659b[i3] = (float) ceil;
                ceil += a2;
            }
        }
        if (a2 < 1.0d) {
            this.f1729a.e = (int) Math.ceil(-Math.log10(a2));
        } else {
            this.f1729a.e = 0;
        }
        if (this.f1729a.c()) {
            if (this.f1729a.c.length < i) {
                this.f1729a.c = new float[i];
            }
            float f4 = ((float) a2) / 2.0f;
            for (int i4 = 0; i4 < i; i4++) {
                this.f1729a.c[i4] = this.f1729a.f1659b[i4] + f4;
            }
        }
    }

    public void a(float f2, float f3, boolean z) {
        float f4;
        float f5;
        if (this.o != null && this.o.i() > 10.0f && !this.o.s()) {
            d a2 = this.f1730b.a(this.o.f(), this.o.e());
            d a3 = this.f1730b.a(this.o.f(), this.o.h());
            if (!z) {
                f4 = (float) a3.f1750b;
                f5 = (float) a2.f1750b;
            } else {
                f4 = (float) a2.f1750b;
                f5 = (float) a3.f1750b;
            }
            d.a(a2);
            d.a(a3);
            f3 = f5;
            f2 = f4;
        }
        a(f2, f3);
    }
}
