package com.github.mikephil.charting.i;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Path.Direction;
import android.graphics.drawable.Drawable;
import com.github.mikephil.charting.d.i;
import com.github.mikephil.charting.f.c;
import com.github.mikephil.charting.g.a.d;
import com.github.mikephil.charting.g.b.e;
import com.github.mikephil.charting.j.j;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.List;

public class g extends h {

    /* renamed from: a reason: collision with root package name */
    protected d f1739a;

    /* renamed from: b reason: collision with root package name */
    protected Paint f1740b;
    protected WeakReference<Bitmap> c;
    protected Canvas d;
    protected Config e = Config.ARGB_8888;
    protected Path l = new Path();
    protected Path m = new Path();
    protected Path n = new Path();
    private float[] p = new float[4];
    private HashMap<com.github.mikephil.charting.g.b.d, a> q = new HashMap<>();
    private float[] r = new float[2];

    private class a {

        /* renamed from: b reason: collision with root package name */
        private Path f1743b;
        private Bitmap[] c;

        private a() {
            this.f1743b = new Path();
        }

        /* access modifiers changed from: protected */
        public Bitmap a(int i) {
            return this.c[i % this.c.length];
        }

        /* access modifiers changed from: protected */
        public void a(e eVar, boolean z, boolean z2) {
            int G = eVar.G();
            float A = eVar.A();
            float B = eVar.B();
            for (int i = 0; i < G; i++) {
                Bitmap createBitmap = Bitmap.createBitmap((int) (((double) A) * 2.1d), (int) (((double) A) * 2.1d), Config.ARGB_4444);
                Canvas canvas = new Canvas(createBitmap);
                this.c[i] = createBitmap;
                g.this.h.setColor(eVar.e(i));
                if (z2) {
                    this.f1743b.reset();
                    this.f1743b.addCircle(A, A, A, Direction.CW);
                    this.f1743b.addCircle(A, A, B, Direction.CCW);
                    canvas.drawPath(this.f1743b, g.this.h);
                } else {
                    canvas.drawCircle(A, A, A, g.this.h);
                    if (z) {
                        canvas.drawCircle(A, A, B, g.this.f1740b);
                    }
                }
            }
        }

        /* access modifiers changed from: protected */
        public boolean a(e eVar) {
            int G = eVar.G();
            if (this.c == null) {
                this.c = new Bitmap[G];
                return true;
            } else if (this.c.length == G) {
                return false;
            } else {
                this.c = new Bitmap[G];
                return true;
            }
        }
    }

    public g(d dVar, com.github.mikephil.charting.a.a aVar, j jVar) {
        super(aVar, jVar);
        this.f1739a = dVar;
        this.f1740b = new Paint(1);
        this.f1740b.setStyle(Style.FILL);
        this.f1740b.setColor(-1);
    }

    private void a(e eVar, int i, int i2, Path path) {
        i iVar = null;
        float a2 = eVar.K().a(eVar, this.f1739a);
        float a3 = this.g.a();
        boolean z = eVar.y() == com.github.mikephil.charting.d.k.a.STEPPED;
        path.reset();
        i d2 = eVar.d(i);
        path.moveTo(d2.h(), a2);
        path.lineTo(d2.h(), d2.b() * a3);
        int i3 = i + 1;
        i iVar2 = null;
        while (i3 <= i2) {
            iVar2 = eVar.d(i3);
            if (z && iVar != null) {
                path.lineTo(iVar2.h(), iVar.b() * a3);
            }
            path.lineTo(iVar2.h(), iVar2.b() * a3);
            i3++;
            iVar = iVar2;
        }
        if (iVar2 != null) {
            path.lineTo(iVar2.h(), a2);
        }
        path.close();
    }

    public void a() {
    }

    public void a(Canvas canvas) {
        int n2 = (int) this.o.n();
        int m2 = (int) this.o.m();
        if (!(this.c != null && ((Bitmap) this.c.get()).getWidth() == n2 && ((Bitmap) this.c.get()).getHeight() == m2)) {
            if (n2 > 0 && m2 > 0) {
                this.c = new WeakReference<>(Bitmap.createBitmap(n2, m2, this.e));
                this.d = new Canvas((Bitmap) this.c.get());
            } else {
                return;
            }
        }
        ((Bitmap) this.c.get()).eraseColor(0);
        for (e eVar : this.f1739a.getLineData().i()) {
            if (eVar.p()) {
                a(canvas, eVar);
            }
        }
        canvas.drawBitmap((Bitmap) this.c.get(), 0.0f, 0.0f, this.h);
    }

    /* access modifiers changed from: protected */
    public void a(Canvas canvas, e eVar) {
        if (eVar.s() >= 1) {
            this.h.setStrokeWidth(eVar.O());
            this.h.setPathEffect(eVar.D());
            switch (eVar.y()) {
                case CUBIC_BEZIER:
                    b(eVar);
                    break;
                case HORIZONTAL_BEZIER:
                    a(eVar);
                    break;
                default:
                    b(canvas, eVar);
                    break;
            }
            this.h.setPathEffect(null);
        }
    }

    /* access modifiers changed from: protected */
    public void a(Canvas canvas, e eVar, Path path, com.github.mikephil.charting.j.g gVar, a aVar) {
        float a2 = eVar.K().a(eVar, this.f1739a);
        path.lineTo(eVar.d(aVar.f1733a + aVar.c).h(), a2);
        path.lineTo(eVar.d(aVar.f1733a).h(), a2);
        path.close();
        gVar.a(path);
        Drawable M = eVar.M();
        if (M != null) {
            a(canvas, path, M);
        } else {
            a(canvas, path, eVar.L(), eVar.N());
        }
    }

    /* access modifiers changed from: protected */
    public void a(Canvas canvas, e eVar, com.github.mikephil.charting.j.g gVar, a aVar) {
        int i;
        int i2;
        Path path = this.n;
        int i3 = aVar.f1733a;
        int i4 = aVar.f1733a + aVar.c;
        int i5 = 0;
        do {
            i = i3 + (i5 * 128);
            i2 = i + 128;
            if (i2 > i4) {
                i2 = i4;
            }
            if (i <= i2) {
                a(eVar, i, i2, path);
                gVar.a(path);
                Drawable M = eVar.M();
                if (M != null) {
                    a(canvas, path, M);
                } else {
                    a(canvas, path, eVar.L(), eVar.N());
                }
            }
            i5++;
        } while (i <= i2);
    }

    public void a(Canvas canvas, c[] cVarArr) {
        com.github.mikephil.charting.d.j lineData = this.f1739a.getLineData();
        for (c cVar : cVarArr) {
            e eVar = (e) lineData.a(cVar.e());
            if (eVar != null && eVar.f()) {
                i b2 = eVar.b(cVar.a(), cVar.b());
                if (a(b2, eVar)) {
                    com.github.mikephil.charting.j.d b3 = this.f1739a.a(eVar.q()).b(b2.h(), b2.b() * this.g.a());
                    cVar.a((float) b3.f1749a, (float) b3.f1750b);
                    a(canvas, (float) b3.f1749a, (float) b3.f1750b, eVar);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a(e eVar) {
        float a2 = this.g.a();
        com.github.mikephil.charting.j.g a3 = this.f1739a.a(eVar.q());
        this.f.a(this.f1739a, eVar);
        this.l.reset();
        if (this.f.c >= 1) {
            i d2 = eVar.d(this.f.f1733a);
            this.l.moveTo(d2.h(), d2.b() * a2);
            int i = this.f.f1733a + 1;
            i iVar = d2;
            while (i <= this.f.c + this.f.f1733a) {
                i d3 = eVar.d(i);
                float h = ((d3.h() - iVar.h()) / 2.0f) + iVar.h();
                this.l.cubicTo(h, iVar.b() * a2, h, d3.b() * a2, d3.h(), d3.b() * a2);
                i++;
                iVar = d3;
            }
        }
        if (eVar.P()) {
            this.m.reset();
            this.m.addPath(this.l);
            a(this.d, eVar, this.m, a3, this.f);
        }
        this.h.setColor(eVar.c());
        this.h.setStyle(Style.STROKE);
        a3.a(this.l);
        this.d.drawPath(this.l, this.h);
        this.h.setPathEffect(null);
    }

    public void b() {
        if (this.d != null) {
            this.d.setBitmap(null);
            this.d = null;
        }
        if (this.c != null) {
            ((Bitmap) this.c.get()).recycle();
            this.c.clear();
            this.c = null;
        }
    }

    public void b(Canvas canvas) {
        if (a((com.github.mikephil.charting.g.a.c) this.f1739a)) {
            List i = this.f1739a.getLineData().i();
            for (int i2 = 0; i2 < i.size(); i2++) {
                e eVar = (e) i.get(i2);
                if (a(eVar)) {
                    b((com.github.mikephil.charting.g.b.d) eVar);
                    com.github.mikephil.charting.j.g a2 = this.f1739a.a(eVar.q());
                    int A = (int) (eVar.A() * 1.75f);
                    int i3 = !eVar.E() ? A / 2 : A;
                    this.f.a(this.f1739a, eVar);
                    float[] a3 = a2.a(eVar, this.g.b(), this.g.a(), this.f.f1733a, this.f.f1734b);
                    for (int i4 = 0; i4 < a3.length; i4 += 2) {
                        float f = a3[i4];
                        float f2 = a3[i4 + 1];
                        if (!this.o.h(f)) {
                            break;
                        }
                        if (this.o.g(f) && this.o.f(f2)) {
                            i d2 = eVar.d((i4 / 2) + this.f.f1733a);
                            a(canvas, eVar.g(), d2.b(), d2, i2, f, f2 - ((float) i3), eVar.c(i4 / 2));
                        }
                    }
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void b(Canvas canvas, e eVar) {
        int s = eVar.s();
        boolean F = eVar.F();
        int i = F ? 4 : 2;
        com.github.mikephil.charting.j.g a2 = this.f1739a.a(eVar.q());
        float a3 = this.g.a();
        this.h.setStyle(Style.STROKE);
        Canvas canvas2 = eVar.C() ? this.d : canvas;
        this.f.a(this.f1739a, eVar);
        if (eVar.P() && s > 0) {
            a(canvas, eVar, a2, this.f);
        }
        if (eVar.b().size() > 1) {
            if (this.p.length <= i * 2) {
                this.p = new float[(i * 4)];
            }
            for (int i2 = this.f.f1733a; i2 <= this.f.c + this.f.f1733a; i2++) {
                i d2 = eVar.d(i2);
                if (d2 != null) {
                    this.p[0] = d2.h();
                    this.p[1] = d2.b() * a3;
                    if (i2 < this.f.f1734b) {
                        i d3 = eVar.d(i2 + 1);
                        if (d3 == null) {
                            break;
                        } else if (F) {
                            this.p[2] = d3.h();
                            this.p[3] = this.p[1];
                            this.p[4] = this.p[2];
                            this.p[5] = this.p[3];
                            this.p[6] = d3.h();
                            this.p[7] = d3.b() * a3;
                        } else {
                            this.p[2] = d3.h();
                            this.p[3] = d3.b() * a3;
                        }
                    } else {
                        this.p[2] = this.p[0];
                        this.p[3] = this.p[1];
                    }
                    a2.a(this.p);
                    if (!this.o.h(this.p[0])) {
                        break;
                    } else if (this.o.g(this.p[2]) && (this.o.i(this.p[1]) || this.o.j(this.p[3]))) {
                        this.h.setColor(eVar.a(i2));
                        canvas2.drawLines(this.p, 0, i * 2, this.h);
                    }
                }
            }
        } else {
            if (this.p.length < Math.max(s * i, i) * 2) {
                this.p = new float[(Math.max(s * i, i) * 4)];
            }
            if (eVar.d(this.f.f1733a) != null) {
                int i3 = 0;
                int i4 = this.f.f1733a;
                while (i4 <= this.f.c + this.f.f1733a) {
                    i d4 = eVar.d(i4 == 0 ? 0 : i4 - 1);
                    i d5 = eVar.d(i4);
                    if (!(d4 == null || d5 == null)) {
                        int i5 = i3 + 1;
                        this.p[i3] = d4.h();
                        int i6 = i5 + 1;
                        this.p[i5] = d4.b() * a3;
                        if (F) {
                            int i7 = i6 + 1;
                            this.p[i6] = d5.h();
                            int i8 = i7 + 1;
                            this.p[i7] = d4.b() * a3;
                            int i9 = i8 + 1;
                            this.p[i8] = d5.h();
                            i6 = i9 + 1;
                            this.p[i9] = d4.b() * a3;
                        }
                        int i10 = i6 + 1;
                        this.p[i6] = d5.h();
                        i3 = i10 + 1;
                        this.p[i10] = d5.b() * a3;
                    }
                    i4++;
                }
                if (i3 > 0) {
                    a2.a(this.p);
                    int max = Math.max((this.f.c + 1) * i, i) * 2;
                    this.h.setColor(eVar.c());
                    canvas2.drawLines(this.p, 0, max, this.h);
                }
            }
        }
        this.h.setPathEffect(null);
    }

    /* access modifiers changed from: protected */
    public void b(e eVar) {
        Math.max(0.0f, Math.min(1.0f, this.g.b()));
        float a2 = this.g.a();
        com.github.mikephil.charting.j.g a3 = this.f1739a.a(eVar.q());
        this.f.a(this.f1739a, eVar);
        float z = eVar.z();
        this.l.reset();
        if (this.f.c >= 1) {
            int i = this.f.f1733a + 1;
            int i2 = this.f.f1733a + this.f.c;
            i d2 = eVar.d(Math.max(i - 2, 0));
            i d3 = eVar.d(Math.max(i - 1, 0));
            if (d3 != null) {
                this.l.moveTo(d3.h(), d3.b() * a2);
                int i3 = this.f.f1733a + 1;
                int i4 = -1;
                i iVar = d3;
                i iVar2 = d3;
                i iVar3 = d2;
                i iVar4 = iVar2;
                while (i3 <= this.f.c + this.f.f1733a) {
                    i d4 = i4 == i3 ? iVar : eVar.d(i3);
                    int i5 = i3 + 1 < eVar.s() ? i3 + 1 : i3;
                    i d5 = eVar.d(i5);
                    this.l.cubicTo(((d4.h() - iVar3.h()) * z) + iVar4.h(), (((d4.b() - iVar3.b()) * z) + iVar4.b()) * a2, d4.h() - ((d5.h() - iVar4.h()) * z), (d4.b() - ((d5.b() - iVar4.b()) * z)) * a2, d4.h(), d4.b() * a2);
                    i3++;
                    iVar = d5;
                    iVar3 = iVar4;
                    iVar4 = d4;
                    i4 = i5;
                }
            } else {
                return;
            }
        }
        if (eVar.P()) {
            this.m.reset();
            this.m.addPath(this.l);
            a(this.d, eVar, this.m, a3, this.f);
        }
        this.h.setColor(eVar.c());
        this.h.setStyle(Style.STROKE);
        a3.a(this.l);
        this.d.drawPath(this.l, this.h);
        this.h.setPathEffect(null);
    }

    public void c(Canvas canvas) {
        d(canvas);
    }

    /* access modifiers changed from: protected */
    public void d(Canvas canvas) {
        a aVar;
        this.h.setStyle(Style.FILL);
        float a2 = this.g.a();
        this.r[0] = 0.0f;
        this.r[1] = 0.0f;
        List i = this.f1739a.getLineData().i();
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < i.size()) {
                e eVar = (e) i.get(i3);
                if (eVar.p() && eVar.E() && eVar.s() != 0) {
                    this.f1740b.setColor(eVar.I());
                    com.github.mikephil.charting.j.g a3 = this.f1739a.a(eVar.q());
                    this.f.a(this.f1739a, eVar);
                    float A = eVar.A();
                    float B = eVar.B();
                    boolean z = eVar.J() && B < A && B > 0.0f;
                    boolean z2 = z && eVar.I() == 1122867;
                    if (this.q.containsKey(eVar)) {
                        aVar = (a) this.q.get(eVar);
                    } else {
                        aVar = new a();
                        this.q.put(eVar, aVar);
                    }
                    if (aVar.a(eVar)) {
                        aVar.a(eVar, z, z2);
                    }
                    int i4 = this.f.f1733a + this.f.c;
                    for (int i5 = this.f.f1733a; i5 <= i4; i5++) {
                        i d2 = eVar.d(i5);
                        if (d2 == null) {
                            break;
                        }
                        this.r[0] = d2.h();
                        this.r[1] = d2.b() * a2;
                        a3.a(this.r);
                        if (!this.o.h(this.r[0])) {
                            break;
                        }
                        if (this.o.g(this.r[0]) && this.o.f(this.r[1])) {
                            Bitmap a4 = aVar.a(i5);
                            if (a4 != null) {
                                canvas.drawBitmap(a4, this.r[0] - A, this.r[1] - A, this.h);
                            }
                        }
                    }
                }
                i2 = i3 + 1;
            } else {
                return;
            }
        }
    }
}
