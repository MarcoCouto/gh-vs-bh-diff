package com.github.mikephil.charting.f;

import com.github.mikephil.charting.d.h;
import com.github.mikephil.charting.d.i;
import com.github.mikephil.charting.g.a.a;
import java.util.ArrayList;
import java.util.List;

public class d extends a {
    public d(a aVar) {
        super(aVar);
    }

    /* access modifiers changed from: protected */
    public float a(float f, float f2, float f3, float f4) {
        return Math.abs(f2 - f4);
    }

    public c a(float f, float f2) {
        com.github.mikephil.charting.d.a barData = ((a) this.f1719a).getBarData();
        com.github.mikephil.charting.j.d b2 = b(f2, f);
        c a2 = a((float) b2.f1750b, f2, f);
        if (a2 == null) {
            return null;
        }
        com.github.mikephil.charting.g.b.a aVar = (com.github.mikephil.charting.g.b.a) barData.a(a2.e());
        if (aVar.d()) {
            return a(a2, aVar, (float) b2.f1750b, (float) b2.f1749a);
        }
        com.github.mikephil.charting.j.d.a(b2);
        return a2;
    }

    /* access modifiers changed from: protected */
    public List<c> a(com.github.mikephil.charting.g.b.d dVar, int i, float f, h.a aVar) {
        ArrayList arrayList = new ArrayList();
        List<i> a2 = dVar.a(f);
        if (a2.size() == 0) {
            i a3 = dVar.a(f, Float.NaN, aVar);
            if (a3 != null) {
                a2 = dVar.a(a3.h());
            }
        }
        if (a2.size() == 0) {
            return arrayList;
        }
        for (i iVar : a2) {
            com.github.mikephil.charting.j.d b2 = ((a) this.f1719a).a(dVar.q()).b(iVar.b(), iVar.h());
            arrayList.add(new c(iVar.h(), iVar.b(), (float) b2.f1749a, (float) b2.f1750b, i, dVar.q()));
        }
        return arrayList;
    }
}
