package com.github.mikephil.charting.f;

import com.github.mikephil.charting.c.j.a;

public class c {

    /* renamed from: a reason: collision with root package name */
    private float f1721a;

    /* renamed from: b reason: collision with root package name */
    private float f1722b;
    private float c;
    private float d;
    private int e;
    private int f;
    private int g;
    private a h;
    private float i;
    private float j;

    public c(float f2, float f3, float f4, float f5, int i2, int i3, a aVar) {
        this(f2, f3, f4, f5, i2, aVar);
        this.g = i3;
    }

    public c(float f2, float f3, float f4, float f5, int i2, a aVar) {
        this.f1721a = Float.NaN;
        this.f1722b = Float.NaN;
        this.e = -1;
        this.g = -1;
        this.f1721a = f2;
        this.f1722b = f3;
        this.c = f4;
        this.d = f5;
        this.f = i2;
        this.h = aVar;
    }

    public float a() {
        return this.f1721a;
    }

    public void a(float f2, float f3) {
        this.i = f2;
        this.j = f3;
    }

    public boolean a(c cVar) {
        return cVar != null && this.f == cVar.f && this.f1721a == cVar.f1721a && this.g == cVar.g && this.e == cVar.e;
    }

    public float b() {
        return this.f1722b;
    }

    public float c() {
        return this.c;
    }

    public float d() {
        return this.d;
    }

    public int e() {
        return this.f;
    }

    public int f() {
        return this.g;
    }

    public a g() {
        return this.h;
    }

    public float h() {
        return this.i;
    }

    public float i() {
        return this.j;
    }

    public String toString() {
        return "Highlight, x: " + this.f1721a + ", y: " + this.f1722b + ", dataSetIndex: " + this.f + ", stackIndex (only stacked barentry): " + this.g;
    }
}
