package com.github.mikephil.charting.f;

import com.github.mikephil.charting.d.b;
import com.github.mikephil.charting.d.c;
import com.github.mikephil.charting.j.d;

public class a extends b<com.github.mikephil.charting.g.a.a> {
    public a(com.github.mikephil.charting.g.a.a aVar) {
        super(aVar);
    }

    /* access modifiers changed from: protected */
    public float a(float f, float f2, float f3, float f4) {
        return Math.abs(f - f3);
    }

    /* access modifiers changed from: protected */
    public int a(f[] fVarArr, float f) {
        if (fVarArr == null || fVarArr.length == 0) {
            return 0;
        }
        int length = fVarArr.length;
        int i = 0;
        int i2 = 0;
        while (i < length) {
            if (fVarArr[i].a(f)) {
                return i2;
            }
            i++;
            i2++;
        }
        int max = Math.max(fVarArr.length - 1, 0);
        if (f <= fVarArr[max].f1724b) {
            return 0;
        }
        return max;
    }

    /* access modifiers changed from: protected */
    public c a() {
        return ((com.github.mikephil.charting.g.a.a) this.f1719a).getBarData();
    }

    public c a(float f, float f2) {
        c a2 = super.a(f, f2);
        if (a2 == null) {
            return null;
        }
        d b2 = b(f, f2);
        com.github.mikephil.charting.g.b.a aVar = (com.github.mikephil.charting.g.b.a) ((com.github.mikephil.charting.g.a.a) this.f1719a).getBarData().a(a2.e());
        if (aVar.d()) {
            return a(a2, aVar, (float) b2.f1749a, (float) b2.f1750b);
        }
        d.a(b2);
        return a2;
    }

    public c a(c cVar, com.github.mikephil.charting.g.b.a aVar, float f, float f2) {
        b bVar = (b) aVar.b(f, f2);
        if (bVar == null) {
            return null;
        }
        if (bVar.a() == null) {
            return cVar;
        }
        f[] c = bVar.c();
        if (c.length <= 0) {
            return null;
        }
        int a2 = a(c, f2);
        d b2 = ((com.github.mikephil.charting.g.a.a) this.f1719a).a(aVar.q()).b(cVar.a(), c[a2].f1724b);
        c cVar2 = new c(bVar.h(), bVar.b(), (float) b2.f1749a, (float) b2.f1750b, cVar.e(), a2, cVar.g());
        d.a(b2);
        return cVar2;
    }
}
