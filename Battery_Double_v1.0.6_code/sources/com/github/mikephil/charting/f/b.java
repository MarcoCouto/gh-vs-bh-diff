package com.github.mikephil.charting.f;

import com.github.mikephil.charting.c.j.a;
import com.github.mikephil.charting.d.c;
import com.github.mikephil.charting.d.h;
import com.github.mikephil.charting.d.i;
import com.github.mikephil.charting.g.a.b;
import com.github.mikephil.charting.j.d;
import java.util.ArrayList;
import java.util.List;

public class b<T extends com.github.mikephil.charting.g.a.b> implements e {

    /* renamed from: a reason: collision with root package name */
    protected T f1719a;

    /* renamed from: b reason: collision with root package name */
    protected List<c> f1720b = new ArrayList();

    public b(T t) {
        this.f1719a = t;
    }

    /* access modifiers changed from: protected */
    public float a(float f, float f2, float f3, float f4) {
        return (float) Math.hypot((double) (f - f3), (double) (f2 - f4));
    }

    /* access modifiers changed from: protected */
    public float a(c cVar) {
        return cVar.d();
    }

    /* access modifiers changed from: protected */
    public float a(List<c> list, float f, a aVar) {
        int i = 0;
        float f2 = Float.MAX_VALUE;
        while (true) {
            int i2 = i;
            if (i2 >= list.size()) {
                return f2;
            }
            c cVar = (c) list.get(i2);
            if (cVar.g() == aVar) {
                float abs = Math.abs(a(cVar) - f);
                if (abs < f2) {
                    f2 = abs;
                }
            }
            i = i2 + 1;
        }
    }

    /* access modifiers changed from: protected */
    public c a() {
        return this.f1719a.getData();
    }

    public c a(float f, float f2) {
        d b2 = b(f, f2);
        float f3 = (float) b2.f1749a;
        d.a(b2);
        return a(f3, f, f2);
    }

    /* access modifiers changed from: protected */
    public c a(float f, float f2, float f3) {
        List b2 = b(f, f2, f3);
        if (b2.isEmpty()) {
            return null;
        }
        return a(b2, f2, f3, a(b2, f3, a.LEFT) < a(b2, f3, a.RIGHT) ? a.LEFT : a.RIGHT, this.f1719a.getMaxHighlightDistance());
    }

    public c a(List<c> list, float f, float f2, a aVar, float f3) {
        float f4;
        c cVar;
        c cVar2 = null;
        int i = 0;
        float f5 = f3;
        while (i < list.size()) {
            c cVar3 = (c) list.get(i);
            if (aVar == null || cVar3.g() == aVar) {
                float a2 = a(f, f2, cVar3.c(), cVar3.d());
                if (a2 < f5) {
                    cVar = cVar3;
                    f4 = a2;
                    i++;
                    cVar2 = cVar;
                    f5 = f4;
                }
            }
            f4 = f5;
            cVar = cVar2;
            i++;
            cVar2 = cVar;
            f5 = f4;
        }
        return cVar2;
    }

    /* access modifiers changed from: protected */
    public List<c> a(com.github.mikephil.charting.g.b.d dVar, int i, float f, h.a aVar) {
        ArrayList arrayList = new ArrayList();
        List<i> a2 = dVar.a(f);
        if (a2.size() == 0) {
            i a3 = dVar.a(f, Float.NaN, aVar);
            if (a3 != null) {
                a2 = dVar.a(a3.h());
            }
        }
        if (a2.size() == 0) {
            return arrayList;
        }
        for (i iVar : a2) {
            d b2 = this.f1719a.a(dVar.q()).b(iVar.h(), iVar.b());
            arrayList.add(new c(iVar.h(), iVar.b(), (float) b2.f1749a, (float) b2.f1750b, i, dVar.q()));
        }
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public d b(float f, float f2) {
        return this.f1719a.a(a.LEFT).a(f, f2);
    }

    /* access modifiers changed from: protected */
    public List<c> b(float f, float f2, float f3) {
        this.f1720b.clear();
        c a2 = a();
        if (a2 == null) {
            return this.f1720b;
        }
        int d = a2.d();
        for (int i = 0; i < d; i++) {
            com.github.mikephil.charting.g.b.d a3 = a2.a(i);
            if (a3.f()) {
                this.f1720b.addAll(a(a3, i, f, h.a.CLOSEST));
            }
        }
        return this.f1720b;
    }
}
