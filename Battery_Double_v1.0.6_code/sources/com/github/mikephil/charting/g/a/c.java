package com.github.mikephil.charting.g.a;

import com.github.mikephil.charting.d.g;

public interface c {
    g getData();

    float getMaxHighlightDistance();

    int getMaxVisibleCount();

    float getYChartMax();

    float getYChartMin();
}
