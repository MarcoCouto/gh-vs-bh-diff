package com.github.mikephil.charting.g.b;

import android.graphics.DashPathEffect;
import com.github.mikephil.charting.d.i;

public interface g<T extends i> extends b<T> {
    boolean Q();

    boolean R();

    float S();

    DashPathEffect T();
}
