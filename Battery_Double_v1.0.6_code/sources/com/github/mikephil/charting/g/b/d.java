package com.github.mikephil.charting.g.b;

import android.graphics.DashPathEffect;
import android.graphics.Typeface;
import com.github.mikephil.charting.c.e.b;
import com.github.mikephil.charting.c.j;
import com.github.mikephil.charting.d.h.a;
import com.github.mikephil.charting.d.i;
import com.github.mikephil.charting.e.f;
import java.util.List;

public interface d<T extends i> {
    int a(int i);

    T a(float f, float f2, a aVar);

    List<T> a(float f);

    void a(float f, float f2);

    void a(f fVar);

    T b(float f, float f2);

    List<Integer> b();

    int c();

    int c(int i);

    int d(T t);

    T d(int i);

    String e();

    boolean f();

    f g();

    boolean h();

    Typeface i();

    float j();

    b k();

    float l();

    float m();

    DashPathEffect n();

    boolean o();

    boolean p();

    j.a q();

    int s();

    float u();

    float v();

    float w();

    float x();
}
