package com.github.mikephil.charting.g.b;

import android.graphics.DashPathEffect;
import com.github.mikephil.charting.d.i;
import com.github.mikephil.charting.d.k.a;

public interface e extends f<i> {
    float A();

    float B();

    boolean C();

    DashPathEffect D();

    boolean E();

    @Deprecated
    boolean F();

    int G();

    int I();

    boolean J();

    com.github.mikephil.charting.e.e K();

    int e(int i);

    a y();

    float z();
}
