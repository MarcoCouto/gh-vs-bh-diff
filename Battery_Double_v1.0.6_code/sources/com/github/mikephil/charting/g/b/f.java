package com.github.mikephil.charting.g.b;

import android.graphics.drawable.Drawable;
import com.github.mikephil.charting.d.i;

public interface f<T extends i> extends g<T> {
    int L();

    Drawable M();

    int N();

    float O();

    boolean P();
}
