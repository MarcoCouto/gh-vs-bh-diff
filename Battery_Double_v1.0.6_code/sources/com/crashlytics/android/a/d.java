package com.crashlytics.android.a;

import com.crashlytics.android.a.d;
import io.a.a.a.c;
import java.util.Map;

public abstract class d<T extends d> {

    /* renamed from: b reason: collision with root package name */
    final e f1441b = new e(20, 100, c.i());
    final c c = new c(this.f1441b);

    public T a(String str, String str2) {
        this.c.a(str, str2);
        return this;
    }

    /* access modifiers changed from: 0000 */
    public Map<String, Object> b() {
        return this.c.f1440b;
    }
}
