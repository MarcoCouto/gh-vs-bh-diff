package com.crashlytics.android.a;

import io.a.a.a.a.b.a;
import io.a.a.a.a.b.s;
import io.a.a.a.a.d.f;
import io.a.a.a.a.e.c;
import io.a.a.a.a.e.d;
import io.a.a.a.a.e.e;
import io.a.a.a.i;
import java.io.File;
import java.util.List;

class ab extends a implements f {

    /* renamed from: b reason: collision with root package name */
    private final String f1424b;

    public ab(i iVar, String str, String str2, e eVar, String str3) {
        super(iVar, str, str2, eVar, c.POST);
        this.f1424b = str3;
    }

    public boolean a(List<File> list) {
        d a2 = b().a("X-CRASHLYTICS-API-CLIENT-TYPE", "android").a("X-CRASHLYTICS-API-CLIENT-VERSION", this.f4362a.a()).a("X-CRASHLYTICS-API-KEY", this.f1424b);
        int i = 0;
        for (File file : list) {
            a2.a("session_analytics_file_" + i, file.getName(), "application/vnd.crashlytics.android.events", file);
            i++;
        }
        io.a.a.a.c.h().a("Answers", "Sending " + list.size() + " analytics files to " + a());
        int b2 = a2.b();
        io.a.a.a.c.h().a("Answers", "Response code for analytics file send is " + b2);
        return s.a(b2) == 0;
    }
}
