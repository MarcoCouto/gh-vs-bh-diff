package com.crashlytics.android.a;

import android.content.Context;
import io.a.a.a.a.b.i;
import io.a.a.a.a.b.p;
import io.a.a.a.a.b.p.a;
import java.util.Map;
import java.util.UUID;

class ah {

    /* renamed from: a reason: collision with root package name */
    private final Context f1435a;

    /* renamed from: b reason: collision with root package name */
    private final p f1436b;
    private final String c;
    private final String d;

    public ah(Context context, p pVar, String str, String str2) {
        this.f1435a = context;
        this.f1436b = pVar;
        this.c = str;
        this.d = str2;
    }

    public af a() {
        Map h = this.f1436b.h();
        return new af(this.f1436b.c(), UUID.randomUUID().toString(), this.f1436b.b(), (String) h.get(a.ANDROID_ID), (String) h.get(a.ANDROID_ADVERTISING_ID), this.f1436b.j(), (String) h.get(a.FONT_TOKEN), i.m(this.f1435a), this.f1436b.d(), this.f1436b.g(), this.c, this.d);
    }
}
