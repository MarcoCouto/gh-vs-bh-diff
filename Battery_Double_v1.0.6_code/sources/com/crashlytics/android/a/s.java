package com.crashlytics.android.a;

import android.content.Context;
import io.a.a.a.c;

class s {

    /* renamed from: a reason: collision with root package name */
    private final Context f1469a;

    /* renamed from: b reason: collision with root package name */
    private final u f1470b;
    private r c;

    public s(Context context) {
        this(context, new u());
    }

    public s(Context context, u uVar) {
        this.f1469a = context;
        this.f1470b = uVar;
    }

    public r a() {
        if (this.c == null) {
            this.c = k.a(this.f1469a);
        }
        return this.c;
    }

    public void a(ae aeVar) {
        r a2 = a();
        if (a2 == null) {
            c.h().a("Answers", "Firebase analytics logging was enabled, but not available...");
            return;
        }
        t a3 = this.f1470b.a(aeVar);
        if (a3 == null) {
            c.h().a("Answers", "Fabric event was not mappable to Firebase event: " + aeVar);
            return;
        }
        a2.a(a3.a(), a3.b());
        if ("levelEnd".equals(aeVar.g)) {
            a2.a("post_score", a3.b());
        }
    }
}
