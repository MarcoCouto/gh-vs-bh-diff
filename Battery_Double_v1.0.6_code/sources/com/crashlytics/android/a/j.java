package com.crashlytics.android.a;

import io.a.a.a.a.c.a.b;
import io.a.a.a.a.c.a.c;
import io.a.a.a.a.c.a.e;
import io.a.a.a.a.d.f;
import java.io.File;
import java.util.List;

class j implements f {

    /* renamed from: a reason: collision with root package name */
    private final ab f1459a;

    /* renamed from: b reason: collision with root package name */
    private final y f1460b;

    j(ab abVar, y yVar) {
        this.f1459a = abVar;
        this.f1460b = yVar;
    }

    public static j a(ab abVar) {
        return new j(abVar, new y(new e(new x(new c(1000, 8), 0.1d), new b(5))));
    }

    public boolean a(List<File> list) {
        long nanoTime = System.nanoTime();
        if (!this.f1460b.a(nanoTime)) {
            return false;
        }
        if (this.f1459a.a(list)) {
            this.f1460b.a();
            return true;
        }
        this.f1460b.b(nanoTime);
        return false;
    }
}
