package com.crashlytics.android.a;

import android.content.Context;
import android.os.Looper;
import io.a.a.a.a.b.t;
import io.a.a.a.a.f.a;
import java.io.IOException;

class g {

    /* renamed from: a reason: collision with root package name */
    final Context f1454a;

    /* renamed from: b reason: collision with root package name */
    final a f1455b;

    public g(Context context, a aVar) {
        this.f1454a = context;
        this.f1455b = aVar;
    }

    public aa a() throws IOException {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            throw new IllegalStateException("AnswersFilesManagerProvider cannot be called on the main thread");
        }
        return new aa(this.f1454a, new ag(), new t(), new io.a.a.a.a.d.g(this.f1454a, this.f1455b.a(), "session_analytics.tap", "session_analytics_to_send"));
    }
}
