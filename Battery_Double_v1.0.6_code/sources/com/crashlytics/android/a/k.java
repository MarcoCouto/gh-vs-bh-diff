package com.crashlytics.android.a;

import android.content.Context;
import android.os.Bundle;
import java.lang.reflect.Method;

public class k implements r {

    /* renamed from: a reason: collision with root package name */
    private final Method f1461a;

    /* renamed from: b reason: collision with root package name */
    private final Object f1462b;

    public k(Object obj, Method method) {
        this.f1462b = obj;
        this.f1461a = method;
    }

    public static r a(Context context) {
        Class b2 = b(context);
        if (b2 == null) {
            return null;
        }
        Object a2 = a(context, b2);
        if (a2 == null) {
            return null;
        }
        Method b3 = b(context, b2);
        if (b3 != null) {
            return new k(a2, b3);
        }
        return null;
    }

    private static Object a(Context context, Class cls) {
        try {
            return cls.getDeclaredMethod("getInstance", new Class[]{Context.class}).invoke(cls, new Object[]{context});
        } catch (Exception e) {
            return null;
        }
    }

    private static Class b(Context context) {
        try {
            return context.getClassLoader().loadClass("com.google.android.gms.measurement.AppMeasurement");
        } catch (Exception e) {
            return null;
        }
    }

    private static Method b(Context context, Class cls) {
        try {
            return cls.getDeclaredMethod("logEventInternal", new Class[]{String.class, String.class, Bundle.class});
        } catch (Exception e) {
            return null;
        }
    }

    public void a(String str, Bundle bundle) {
        a("fab", str, bundle);
    }

    public void a(String str, String str2, Bundle bundle) {
        try {
            this.f1461a.invoke(this.f1462b, new Object[]{str, str2, bundle});
        } catch (Exception e) {
        }
    }
}
