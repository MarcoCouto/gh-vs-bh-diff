package com.crashlytics.android.a;

import com.crashlytics.android.a.w;
import java.util.Map;

public abstract class w<T extends w> extends d<T> {
    final c d = new c(this.f1441b);

    /* access modifiers changed from: 0000 */
    public abstract String a();

    /* access modifiers changed from: 0000 */
    public Map<String, Object> c() {
        return this.d.f1440b;
    }

    public String toString() {
        return "{type:\"" + a() + '\"' + ", predefinedAttributes:" + this.d + ", customAttributes:" + this.c + "}";
    }
}
