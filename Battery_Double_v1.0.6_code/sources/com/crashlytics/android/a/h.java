package com.crashlytics.android.a;

import android.app.Activity;
import android.os.Bundle;
import io.a.a.a.a.b;

class h extends b {

    /* renamed from: a reason: collision with root package name */
    private final ac f1456a;

    /* renamed from: b reason: collision with root package name */
    private final l f1457b;

    public h(ac acVar, l lVar) {
        this.f1456a = acVar;
        this.f1457b = lVar;
    }

    public void a(Activity activity) {
        this.f1456a.a(activity, b.START);
    }

    public void a(Activity activity, Bundle bundle) {
    }

    public void b(Activity activity) {
        this.f1456a.a(activity, b.RESUME);
        this.f1457b.a();
    }

    public void b(Activity activity, Bundle bundle) {
    }

    public void c(Activity activity) {
        this.f1456a.a(activity, b.PAUSE);
        this.f1457b.b();
    }

    public void d(Activity activity) {
        this.f1456a.a(activity, b.STOP);
    }

    public void e(Activity activity) {
    }
}
