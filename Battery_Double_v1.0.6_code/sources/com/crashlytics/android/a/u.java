package com.crashlytics.android.a;

import android.os.Bundle;
import io.a.a.a.c;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class u {

    /* renamed from: a reason: collision with root package name */
    private static final Set<String> f1473a = new HashSet(Arrays.asList(new String[]{"app_clear_data", "app_exception", "app_remove", "app_upgrade", "app_install", "app_update", "firebase_campaign", "error", "first_open", "first_visit", "in_app_purchase", "notification_dismiss", "notification_foreground", "notification_open", "notification_receive", "os_update", "session_start", "user_engagement", "ad_exposure", "adunit_exposure", "ad_query", "ad_activeview", "ad_impression", "ad_click", "screen_view", "firebase_extra_parameter"}));

    private Double a(Object obj) {
        String valueOf = String.valueOf(obj);
        if (valueOf == null) {
            return null;
        }
        return Double.valueOf(valueOf);
    }

    private String a(String str) {
        if (str == null || str.length() == 0) {
            return "fabric_unnamed_event";
        }
        if (f1473a.contains(str)) {
            return "fabric_" + str;
        }
        String replaceAll = str.replaceAll("[^\\p{Alnum}_]+", "_");
        if (replaceAll.startsWith("ga_") || replaceAll.startsWith("google_") || replaceAll.startsWith("firebase_") || !Character.isLetter(replaceAll.charAt(0))) {
            replaceAll = "fabric_" + replaceAll;
        }
        return replaceAll.length() > 40 ? replaceAll.substring(0, 40) : replaceAll;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x004e, code lost:
        if (r6.equals("purchase") != false) goto L_0x0019;
     */
    private String a(String str, boolean z) {
        boolean z2;
        char c = 0;
        if (z) {
            switch (str.hashCode()) {
                case -902468296:
                    if (str.equals("signUp")) {
                        z2 = true;
                        break;
                    }
                case 103149417:
                    if (str.equals("login")) {
                        z2 = true;
                        break;
                    }
                case 1743324417:
                    if (str.equals("purchase")) {
                        z2 = false;
                        break;
                    }
                default:
                    z2 = true;
                    break;
            }
            switch (z2) {
                case false:
                    return "failed_ecommerce_purchase";
                case true:
                    return "failed_sign_up";
                case true:
                    return "failed_login";
            }
        }
        switch (str.hashCode()) {
            case -2131650889:
                if (str.equals("levelEnd")) {
                    c = 11;
                    break;
                }
            case -1183699191:
                if (str.equals("invite")) {
                    c = 9;
                    break;
                }
            case -938102371:
                if (str.equals("rating")) {
                    c = 6;
                    break;
                }
            case -906336856:
                if (str.equals("search")) {
                    c = 4;
                    break;
                }
            case -902468296:
                if (str.equals("signUp")) {
                    c = 7;
                    break;
                }
            case -389087554:
                if (str.equals("contentView")) {
                    c = 3;
                    break;
                }
            case 23457852:
                if (str.equals("addToCart")) {
                    c = 1;
                    break;
                }
            case 103149417:
                if (str.equals("login")) {
                    c = 8;
                    break;
                }
            case 109400031:
                if (str.equals("share")) {
                    c = 5;
                    break;
                }
            case 196004670:
                if (str.equals("levelStart")) {
                    c = 10;
                    break;
                }
            case 1664021448:
                if (str.equals("startCheckout")) {
                    c = 2;
                    break;
                }
            case 1743324417:
                break;
            default:
                c = 65535;
                break;
        }
        switch (c) {
            case 0:
                return "ecommerce_purchase";
            case 1:
                return "add_to_cart";
            case 2:
                return "begin_checkout";
            case 3:
                return "select_content";
            case 4:
                return "search";
            case 5:
                return "share";
            case 6:
                return "rate_content";
            case 7:
                return "sign_up";
            case 8:
                return "login";
            case 9:
                return "invite";
            case 10:
                return "level_start";
            case 11:
                return "level_end";
            default:
                return a(str);
        }
    }

    private void a(Bundle bundle, String str, Double d) {
        Double a2 = a((Object) d);
        if (a2 != null) {
            bundle.putDouble(str, a2.doubleValue());
        }
    }

    private void a(Bundle bundle, String str, Integer num) {
        if (num != null) {
            bundle.putInt(str, num.intValue());
        }
    }

    private void a(Bundle bundle, String str, Long l) {
        if (l != null) {
            bundle.putLong(str, l.longValue());
        }
    }

    private void a(Bundle bundle, String str, String str2) {
        if (str2 != null) {
            bundle.putString(str, str2);
        }
    }

    private void a(Bundle bundle, Map<String, Object> map) {
        for (Entry entry : map.entrySet()) {
            Object value = entry.getValue();
            String b2 = b((String) entry.getKey());
            if (value instanceof String) {
                bundle.putString(b2, entry.getValue().toString());
            } else if (value instanceof Double) {
                bundle.putDouble(b2, ((Double) entry.getValue()).doubleValue());
            } else if (value instanceof Long) {
                bundle.putLong(b2, ((Long) entry.getValue()).longValue());
            } else if (value instanceof Integer) {
                bundle.putInt(b2, ((Integer) entry.getValue()).intValue());
            }
        }
    }

    private Bundle b(ae aeVar) {
        Bundle bundle = new Bundle();
        if ("purchase".equals(aeVar.g)) {
            a(bundle, "item_id", (String) aeVar.h.get("itemId"));
            a(bundle, "item_name", (String) aeVar.h.get("itemName"));
            a(bundle, "item_category", (String) aeVar.h.get("itemType"));
            a(bundle, "value", b(aeVar.h.get("itemPrice")));
            a(bundle, "currency", (String) aeVar.h.get("currency"));
        } else if ("addToCart".equals(aeVar.g)) {
            a(bundle, "item_id", (String) aeVar.h.get("itemId"));
            a(bundle, "item_name", (String) aeVar.h.get("itemName"));
            a(bundle, "item_category", (String) aeVar.h.get("itemType"));
            a(bundle, "price", b(aeVar.h.get("itemPrice")));
            a(bundle, "value", b(aeVar.h.get("itemPrice")));
            a(bundle, "currency", (String) aeVar.h.get("currency"));
            bundle.putLong("quantity", 1);
        } else if ("startCheckout".equals(aeVar.g)) {
            a(bundle, "quantity", Long.valueOf((long) ((Integer) aeVar.h.get("itemCount")).intValue()));
            a(bundle, "value", b(aeVar.h.get("totalPrice")));
            a(bundle, "currency", (String) aeVar.h.get("currency"));
        } else if ("contentView".equals(aeVar.g)) {
            a(bundle, "content_type", (String) aeVar.h.get("contentType"));
            a(bundle, "item_id", (String) aeVar.h.get("contentId"));
            a(bundle, "item_name", (String) aeVar.h.get("contentName"));
        } else if ("search".equals(aeVar.g)) {
            a(bundle, "search_term", (String) aeVar.h.get("query"));
        } else if ("share".equals(aeVar.g)) {
            a(bundle, "method", (String) aeVar.h.get("method"));
            a(bundle, "content_type", (String) aeVar.h.get("contentType"));
            a(bundle, "item_id", (String) aeVar.h.get("contentId"));
            a(bundle, "item_name", (String) aeVar.h.get("contentName"));
        } else if ("rating".equals(aeVar.g)) {
            a(bundle, "rating", String.valueOf(aeVar.h.get("rating")));
            a(bundle, "content_type", (String) aeVar.h.get("contentType"));
            a(bundle, "item_id", (String) aeVar.h.get("contentId"));
            a(bundle, "item_name", (String) aeVar.h.get("contentName"));
        } else if ("signUp".equals(aeVar.g)) {
            a(bundle, "method", (String) aeVar.h.get("method"));
        } else if ("login".equals(aeVar.g)) {
            a(bundle, "method", (String) aeVar.h.get("method"));
        } else if ("invite".equals(aeVar.g)) {
            a(bundle, "method", (String) aeVar.h.get("method"));
        } else if ("levelStart".equals(aeVar.g)) {
            a(bundle, "level_name", (String) aeVar.h.get("levelName"));
        } else if ("levelEnd".equals(aeVar.g)) {
            a(bundle, "score", a(aeVar.h.get("score")));
            a(bundle, "level_name", (String) aeVar.h.get("levelName"));
            a(bundle, "success", c((String) aeVar.h.get("success")));
        }
        a(bundle, aeVar.f);
        return bundle;
    }

    private Double b(Object obj) {
        if (((Long) obj) == null) {
            return null;
        }
        return Double.valueOf(new BigDecimal(((Long) obj).longValue()).divide(a.f1423a).doubleValue());
    }

    private String b(String str) {
        if (str == null || str.length() == 0) {
            return "fabric_unnamed_parameter";
        }
        String replaceAll = str.replaceAll("[^\\p{Alnum}_]+", "_");
        if (replaceAll.startsWith("ga_") || replaceAll.startsWith("google_") || replaceAll.startsWith("firebase_") || !Character.isLetter(replaceAll.charAt(0))) {
            replaceAll = "fabric_" + replaceAll;
        }
        return replaceAll.length() > 40 ? replaceAll.substring(0, 40) : replaceAll;
    }

    private Integer c(String str) {
        if (str == null) {
            return null;
        }
        return Integer.valueOf(str.equals("true") ? 1 : 0);
    }

    public t a(ae aeVar) {
        Bundle bundle;
        String a2;
        boolean z = true;
        boolean z2 = b.CUSTOM.equals(aeVar.c) && aeVar.e != null;
        boolean z3 = b.PREDEFINED.equals(aeVar.c) && aeVar.g != null;
        if (!z2 && !z3) {
            return null;
        }
        if (z3) {
            bundle = b(aeVar);
        } else {
            Bundle bundle2 = new Bundle();
            if (aeVar.f != null) {
                a(bundle2, aeVar.f);
            }
            bundle = bundle2;
        }
        if (z3) {
            String str = (String) aeVar.h.get("success");
            if (str == null || Boolean.parseBoolean(str)) {
                z = false;
            }
            a2 = a(aeVar.g, z);
        } else {
            a2 = a(aeVar.e);
        }
        c.h().a("Answers", "Logging event into firebase...");
        return new t(a2, bundle);
    }
}
