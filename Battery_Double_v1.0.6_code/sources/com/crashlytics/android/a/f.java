package com.crashlytics.android.a;

import android.content.Context;
import io.a.a.a.a.d.d;
import io.a.a.a.a.e.e;
import io.a.a.a.a.g.b;
import io.a.a.a.c;
import io.a.a.a.i;
import java.util.concurrent.ScheduledExecutorService;

class f implements d {

    /* renamed from: a reason: collision with root package name */
    final ScheduledExecutorService f1444a;

    /* renamed from: b reason: collision with root package name */
    ad f1445b = new o();
    /* access modifiers changed from: private */
    public final i c;
    /* access modifiers changed from: private */
    public final Context d;
    /* access modifiers changed from: private */
    public final g e;
    /* access modifiers changed from: private */
    public final ah f;
    /* access modifiers changed from: private */
    public final e g;
    /* access modifiers changed from: private */
    public final s h;

    public f(i iVar, Context context, g gVar, ah ahVar, e eVar, ScheduledExecutorService scheduledExecutorService, s sVar) {
        this.c = iVar;
        this.d = context;
        this.e = gVar;
        this.f = ahVar;
        this.g = eVar;
        this.f1444a = scheduledExecutorService;
        this.h = sVar;
    }

    private void a(Runnable runnable) {
        try {
            this.f1444a.submit(runnable).get();
        } catch (Exception e2) {
            c.h().e("Answers", "Failed to run events task", e2);
        }
    }

    private void b(Runnable runnable) {
        try {
            this.f1444a.submit(runnable);
        } catch (Exception e2) {
            c.h().e("Answers", "Failed to submit events task", e2);
        }
    }

    public void a() {
        b((Runnable) new Runnable() {
            public void run() {
                try {
                    ad adVar = f.this.f1445b;
                    f.this.f1445b = new o();
                    adVar.b();
                } catch (Exception e) {
                    c.h().e("Answers", "Failed to disable events", e);
                }
            }
        });
    }

    public void a(a aVar) {
        a(aVar, false, false);
    }

    /* access modifiers changed from: 0000 */
    public void a(final a aVar, boolean z, final boolean z2) {
        AnonymousClass6 r0 = new Runnable() {
            public void run() {
                try {
                    f.this.f1445b.a(aVar);
                    if (z2) {
                        f.this.f1445b.c();
                    }
                } catch (Exception e) {
                    c.h().e("Answers", "Failed to process event", e);
                }
            }
        };
        if (z) {
            a((Runnable) r0);
        } else {
            b((Runnable) r0);
        }
    }

    public void a(final b bVar, final String str) {
        b((Runnable) new Runnable() {
            public void run() {
                try {
                    f.this.f1445b.a(bVar, str);
                } catch (Exception e) {
                    c.h().e("Answers", "Failed to set analytics settings data", e);
                }
            }
        });
    }

    public void a(String str) {
        b((Runnable) new Runnable() {
            public void run() {
                try {
                    f.this.f1445b.a();
                } catch (Exception e) {
                    c.h().e("Answers", "Failed to send events files", e);
                }
            }
        });
    }

    public void b() {
        b((Runnable) new Runnable() {
            public void run() {
                try {
                    af a2 = f.this.f.a();
                    aa a3 = f.this.e.a();
                    a3.a((d) f.this);
                    f.this.f1445b = new p(f.this.c, f.this.d, f.this.f1444a, a3, f.this.g, a2, f.this.h);
                } catch (Exception e) {
                    c.h().e("Answers", "Failed to enable events", e);
                }
            }
        });
    }

    public void b(a aVar) {
        a(aVar, false, true);
    }

    public void c() {
        b((Runnable) new Runnable() {
            public void run() {
                try {
                    f.this.f1445b.c();
                } catch (Exception e) {
                    c.h().e("Answers", "Failed to flush events", e);
                }
            }
        });
    }

    public void c(a aVar) {
        a(aVar, true, false);
    }
}
