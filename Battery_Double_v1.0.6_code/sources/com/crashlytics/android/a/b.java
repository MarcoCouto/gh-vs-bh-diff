package com.crashlytics.android.a;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build.VERSION;
import io.a.a.a.a.b.j.a;
import io.a.a.a.a.b.o;
import io.a.a.a.a.g.q;
import io.a.a.a.a.g.t;
import io.a.a.a.c;
import io.a.a.a.i;
import java.io.File;

public class b extends i<Boolean> {

    /* renamed from: a reason: collision with root package name */
    boolean f1437a = false;

    /* renamed from: b reason: collision with root package name */
    ac f1438b;

    private void a(String str) {
        c.h().d("Answers", "Method " + str + " is not supported when using Crashlytics through Firebase.");
    }

    public static b c() {
        return (b) c.a(b.class);
    }

    public String a() {
        return "1.4.1.19";
    }

    public void a(m mVar) {
        if (mVar == null) {
            throw new NullPointerException("event must not be null");
        } else if (this.f1437a) {
            a("logContentView");
        } else if (this.f1438b != null) {
            this.f1438b.a((w) mVar);
        }
    }

    public void a(n nVar) {
        if (nVar == null) {
            throw new NullPointerException("event must not be null");
        } else if (this.f1437a) {
            a("logCustom");
        } else if (this.f1438b != null) {
            this.f1438b.a(nVar);
        }
    }

    public void a(a aVar) {
        if (this.f1438b != null) {
            this.f1438b.a(aVar.a(), aVar.b());
        }
    }

    public String b() {
        return "com.crashlytics.sdk.android:answers";
    }

    /* access modifiers changed from: protected */
    @SuppressLint({"NewApi"})
    public boolean b_() {
        try {
            Context q = q();
            PackageManager packageManager = q.getPackageManager();
            String packageName = q.getPackageName();
            PackageInfo packageInfo = packageManager.getPackageInfo(packageName, 0);
            this.f1438b = ac.a(this, q, p(), Integer.toString(packageInfo.versionCode), packageInfo.versionName == null ? "0.0" : packageInfo.versionName, VERSION.SDK_INT >= 9 ? packageInfo.firstInstallTime : new File(packageManager.getApplicationInfo(packageName, 0).sourceDir).lastModified());
            this.f1438b.b();
            this.f1437a = new o().b(q);
            return true;
        } catch (Exception e) {
            c.h().e("Answers", "Error retrieving app properties", e);
            return false;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: f */
    public Boolean e() {
        try {
            t b2 = q.a().b();
            if (b2 == null) {
                c.h().e("Answers", "Failed to retrieve settings");
                return Boolean.valueOf(false);
            } else if (b2.d.d) {
                c.h().a("Answers", "Analytics collection enabled");
                this.f1438b.a(b2.e, g());
                return Boolean.valueOf(true);
            } else {
                c.h().a("Answers", "Analytics collection disabled");
                this.f1438b.c();
                return Boolean.valueOf(false);
            }
        } catch (Exception e) {
            c.h().e("Answers", "Error dealing with settings", e);
            return Boolean.valueOf(false);
        }
    }

    /* access modifiers changed from: 0000 */
    public String g() {
        return io.a.a.a.a.b.i.b(q(), "com.crashlytics.ApiEndpoint");
    }
}
