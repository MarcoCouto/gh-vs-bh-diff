package com.crashlytics.android.a;

import android.annotation.TargetApi;
import android.os.Build.VERSION;
import android.text.TextUtils;
import io.a.a.a.a.d.a;
import java.io.IOException;
import org.json.JSONException;
import org.json.JSONObject;

class ag implements a<ae> {
    ag() {
    }

    public byte[] a(ae aeVar) throws IOException {
        return b(aeVar).toString().getBytes("UTF-8");
    }

    @TargetApi(9)
    public JSONObject b(ae aeVar) throws IOException {
        try {
            JSONObject jSONObject = new JSONObject();
            af afVar = aeVar.f1427a;
            jSONObject.put("appBundleId", afVar.f1433a);
            jSONObject.put("executionId", afVar.f1434b);
            jSONObject.put("installationId", afVar.c);
            if (TextUtils.isEmpty(afVar.e)) {
                jSONObject.put("androidId", afVar.d);
            } else {
                jSONObject.put("advertisingId", afVar.e);
            }
            jSONObject.put("limitAdTrackingEnabled", afVar.f);
            jSONObject.put("betaDeviceToken", afVar.g);
            jSONObject.put("buildId", afVar.h);
            jSONObject.put("osVersion", afVar.i);
            jSONObject.put("deviceModel", afVar.j);
            jSONObject.put("appVersionCode", afVar.k);
            jSONObject.put("appVersionName", afVar.l);
            jSONObject.put("timestamp", aeVar.f1428b);
            jSONObject.put("type", aeVar.c.toString());
            if (aeVar.d != null) {
                jSONObject.put("details", new JSONObject(aeVar.d));
            }
            jSONObject.put("customType", aeVar.e);
            if (aeVar.f != null) {
                jSONObject.put("customAttributes", new JSONObject(aeVar.f));
            }
            jSONObject.put("predefinedType", aeVar.g);
            if (aeVar.h != null) {
                jSONObject.put("predefinedAttributes", new JSONObject(aeVar.h));
            }
            return jSONObject;
        } catch (JSONException e) {
            if (VERSION.SDK_INT >= 9) {
                throw new IOException(e.getMessage(), e);
            }
            throw new IOException(e.getMessage());
        }
    }
}
