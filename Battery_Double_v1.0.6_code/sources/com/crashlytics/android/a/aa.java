package com.crashlytics.android.a;

import android.content.Context;
import io.a.a.a.a.b.k;
import io.a.a.a.a.d.b;
import io.a.a.a.a.d.c;
import java.io.IOException;
import java.util.UUID;

class aa extends b<ae> {
    private io.a.a.a.a.g.b g;

    aa(Context context, ag agVar, k kVar, c cVar) throws IOException {
        super(context, agVar, kVar, cVar, 100);
    }

    /* access modifiers changed from: protected */
    public String a() {
        return "sa" + "_" + UUID.randomUUID().toString() + "_" + this.c.a() + ".tap";
    }

    /* access modifiers changed from: 0000 */
    public void a(io.a.a.a.a.g.b bVar) {
        this.g = bVar;
    }

    /* access modifiers changed from: protected */
    public int b() {
        return this.g == null ? super.b() : this.g.e;
    }

    /* access modifiers changed from: protected */
    public int c() {
        return this.g == null ? super.c() : this.g.c;
    }
}
