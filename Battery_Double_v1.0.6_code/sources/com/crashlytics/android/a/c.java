package com.crashlytics.android.a;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.json.JSONObject;

class c {

    /* renamed from: a reason: collision with root package name */
    final e f1439a;

    /* renamed from: b reason: collision with root package name */
    final Map<String, Object> f1440b = new ConcurrentHashMap();

    public c(e eVar) {
        this.f1439a = eVar;
    }

    /* access modifiers changed from: 0000 */
    public void a(String str, Object obj) {
        if (!this.f1439a.a(this.f1440b, str)) {
            this.f1440b.put(str, obj);
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(String str, String str2) {
        if (!this.f1439a.a((Object) str, "key") && !this.f1439a.a((Object) str2, "value")) {
            a(this.f1439a.a(str), (Object) this.f1439a.a(str2));
        }
    }

    public String toString() {
        return new JSONObject(this.f1440b).toString();
    }
}
