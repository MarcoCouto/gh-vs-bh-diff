package com.crashlytics.android.a;

import io.a.a.a.c;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

class l {

    /* renamed from: a reason: collision with root package name */
    final AtomicReference<ScheduledFuture<?>> f1463a = new AtomicReference<>();

    /* renamed from: b reason: collision with root package name */
    boolean f1464b = true;
    private final ScheduledExecutorService c;
    private final List<a> d = new ArrayList();
    private volatile boolean e = true;

    public interface a {
        void a();
    }

    public l(ScheduledExecutorService scheduledExecutorService) {
        this.c = scheduledExecutorService;
    }

    /* access modifiers changed from: private */
    public void c() {
        for (a a2 : this.d) {
            a2.a();
        }
    }

    public void a() {
        this.f1464b = false;
        ScheduledFuture scheduledFuture = (ScheduledFuture) this.f1463a.getAndSet(null);
        if (scheduledFuture != null) {
            scheduledFuture.cancel(false);
        }
    }

    public void a(a aVar) {
        this.d.add(aVar);
    }

    public void a(boolean z) {
        this.e = z;
    }

    public void b() {
        if (this.e && !this.f1464b) {
            this.f1464b = true;
            try {
                this.f1463a.compareAndSet(null, this.c.schedule(new Runnable() {
                    public void run() {
                        l.this.f1463a.set(null);
                        l.this.c();
                    }
                }, 5000, TimeUnit.MILLISECONDS));
            } catch (RejectedExecutionException e2) {
                c.h().a("Answers", "Failed to schedule background detector", (Throwable) e2);
            }
        }
    }
}
