package com.crashlytics.android.a;

import android.content.Context;
import com.hmatalonga.greenhub.Config;
import io.a.a.a.a.b.g;
import io.a.a.a.a.d.f;
import io.a.a.a.a.e.e;
import io.a.a.a.a.g.b;
import io.a.a.a.c;
import io.a.a.a.i;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

class p implements ad {

    /* renamed from: a reason: collision with root package name */
    final af f1467a;

    /* renamed from: b reason: collision with root package name */
    f f1468b;
    g c = new g();
    q d = new v();
    boolean e = true;
    boolean f = true;
    volatile int g = -1;
    boolean h = false;
    boolean i = false;
    private final i j;
    private final e k;
    private final Context l;
    private final aa m;
    private final ScheduledExecutorService n;
    private final AtomicReference<ScheduledFuture<?>> o = new AtomicReference<>();
    private final s p;

    public p(i iVar, Context context, ScheduledExecutorService scheduledExecutorService, aa aaVar, e eVar, af afVar, s sVar) {
        this.j = iVar;
        this.l = context;
        this.n = scheduledExecutorService;
        this.m = aaVar;
        this.k = eVar;
        this.f1467a = afVar;
        this.p = sVar;
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0054  */
    /* JADX WARNING: Removed duplicated region for block: B:27:? A[RETURN, SYNTHETIC] */
    public void a() {
        int i2;
        Exception e2;
        if (this.f1468b == null) {
            io.a.a.a.a.b.i.a(this.l, "skipping files send because we don't yet know the target endpoint");
            return;
        }
        io.a.a.a.a.b.i.a(this.l, "Sending all files");
        List e3 = this.m.e();
        int i3 = 0;
        while (e3.size() > 0) {
            try {
                io.a.a.a.a.b.i.a(this.l, String.format(Locale.US, "attempt to send batch of %d files", new Object[]{Integer.valueOf(e3.size())}));
                boolean a2 = this.f1468b.a(e3);
                if (a2) {
                    i2 = e3.size() + i3;
                    try {
                        this.m.a(e3);
                        i3 = i2;
                    } catch (Exception e4) {
                        e2 = e4;
                        io.a.a.a.a.b.i.a(this.l, "Failed to send batch of analytics files to server: " + e2.getMessage(), (Throwable) e2);
                        i3 = i2;
                        if (i3 == 0) {
                        }
                    }
                }
                if (!a2) {
                    break;
                }
                e3 = this.m.e();
            } catch (Exception e5) {
                Exception exc = e5;
                i2 = i3;
                e2 = exc;
                io.a.a.a.a.b.i.a(this.l, "Failed to send batch of analytics files to server: " + e2.getMessage(), (Throwable) e2);
                i3 = i2;
                if (i3 == 0) {
                }
            }
        }
        if (i3 == 0) {
            this.m.g();
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(long j2, long j3) {
        if (this.o.get() == null) {
            io.a.a.a.a.d.i iVar = new io.a.a.a.a.d.i(this.l, this);
            io.a.a.a.a.b.i.a(this.l, "Scheduling time based file roll over every " + j3 + " seconds");
            try {
                this.o.set(this.n.scheduleAtFixedRate(iVar, j2, j3, TimeUnit.SECONDS));
            } catch (RejectedExecutionException e2) {
                io.a.a.a.a.b.i.a(this.l, "Failed to schedule time based file roll over", (Throwable) e2);
            }
        }
    }

    public void a(a aVar) {
        ae a2 = aVar.a(this.f1467a);
        if (!this.e && b.CUSTOM.equals(a2.c)) {
            c.h().a("Answers", "Custom events tracking disabled - skipping event: " + a2);
        } else if (!this.f && b.PREDEFINED.equals(a2.c)) {
            c.h().a("Answers", "Predefined events tracking disabled - skipping event: " + a2);
        } else if (this.d.a(a2)) {
            c.h().a("Answers", "Skipping filtered event: " + a2);
        } else {
            try {
                this.m.a(a2);
            } catch (IOException e2) {
                c.h().e("Answers", "Failed to write event: " + a2, e2);
            }
            e();
            boolean z = b.CUSTOM.equals(a2.c) || b.PREDEFINED.equals(a2.c);
            boolean equals = "purchase".equals(a2.g);
            if (this.h && z) {
                if (!equals || this.i) {
                    try {
                        this.p.a(a2);
                    } catch (Exception e3) {
                        c.h().e("Answers", "Failed to map event to Firebase: " + a2, e3);
                    }
                }
            }
        }
    }

    public void a(b bVar, String str) {
        this.f1468b = j.a(new ab(this.j, str, bVar.f4469a, this.k, this.c.a(this.l)));
        this.m.a(bVar);
        this.h = bVar.f;
        this.i = bVar.g;
        c.h().a("Answers", "Firebase analytics forwarding " + (this.h ? "enabled" : Config.IMPORTANCE_DISABLED));
        c.h().a("Answers", "Firebase analytics including purchase events " + (this.i ? "enabled" : Config.IMPORTANCE_DISABLED));
        this.e = bVar.h;
        c.h().a("Answers", "Custom event tracking " + (this.e ? "enabled" : Config.IMPORTANCE_DISABLED));
        this.f = bVar.i;
        c.h().a("Answers", "Predefined event tracking " + (this.f ? "enabled" : Config.IMPORTANCE_DISABLED));
        if (bVar.k > 1) {
            c.h().a("Answers", "Event sampling enabled");
            this.d = new z(bVar.k);
        }
        this.g = bVar.f4470b;
        a(0, (long) this.g);
    }

    public void b() {
        this.m.f();
    }

    public boolean c() {
        try {
            return this.m.d();
        } catch (IOException e2) {
            io.a.a.a.a.b.i.a(this.l, "Failed to roll file over.", (Throwable) e2);
            return false;
        }
    }

    public void d() {
        if (this.o.get() != null) {
            io.a.a.a.a.b.i.a(this.l, "Cancelling time-based rollover because no events are currently being generated.");
            ((ScheduledFuture) this.o.get()).cancel(false);
            this.o.set(null);
        }
    }

    public void e() {
        if (this.g != -1) {
            a((long) this.g, (long) this.g);
        }
    }
}
