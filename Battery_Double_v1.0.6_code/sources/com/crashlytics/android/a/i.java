package com.crashlytics.android.a;

import android.annotation.SuppressLint;
import android.content.Context;
import io.a.a.a.a.f.c;
import io.a.a.a.a.f.d;

class i {

    /* renamed from: a reason: collision with root package name */
    private final c f1458a;

    i(c cVar) {
        this.f1458a = cVar;
    }

    public static i a(Context context) {
        return new i(new d(context, "settings"));
    }

    @SuppressLint({"CommitPrefEdits"})
    public void a() {
        this.f1458a.a(this.f1458a.b().putBoolean("analytics_launched", true));
    }

    @SuppressLint({"CommitPrefEdits"})
    public boolean b() {
        return this.f1458a.a().getBoolean("analytics_launched", false);
    }
}
