package com.crashlytics.android.a;

import io.a.a.a.c;
import java.util.Locale;
import java.util.Map;

class e {

    /* renamed from: a reason: collision with root package name */
    final int f1442a;

    /* renamed from: b reason: collision with root package name */
    final int f1443b;
    boolean c;

    public e(int i, int i2, boolean z) {
        this.f1442a = i;
        this.f1443b = i2;
        this.c = z;
    }

    private void a(RuntimeException runtimeException) {
        if (this.c) {
            throw runtimeException;
        }
        c.h().e("Answers", "Invalid user input detected", runtimeException);
    }

    public String a(String str) {
        if (str.length() <= this.f1443b) {
            return str;
        }
        a((RuntimeException) new IllegalArgumentException(String.format(Locale.US, "String is too long, truncating to %d characters", new Object[]{Integer.valueOf(this.f1443b)})));
        return str.substring(0, this.f1443b);
    }

    public boolean a(Object obj, String str) {
        if (obj != null) {
            return false;
        }
        a((RuntimeException) new NullPointerException(str + " must not be null"));
        return true;
    }

    public boolean a(Map<String, Object> map, String str) {
        if (map.size() < this.f1442a || map.containsKey(str)) {
            return false;
        }
        a((RuntimeException) new IllegalArgumentException(String.format(Locale.US, "Limit of %d attributes reached, skipping attribute", new Object[]{Integer.valueOf(this.f1442a)})));
        return true;
    }
}
