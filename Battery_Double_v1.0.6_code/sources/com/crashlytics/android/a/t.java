package com.crashlytics.android.a;

import android.os.Bundle;

public class t {

    /* renamed from: a reason: collision with root package name */
    private final String f1471a;

    /* renamed from: b reason: collision with root package name */
    private final Bundle f1472b;

    t(String str, Bundle bundle) {
        this.f1471a = str;
        this.f1472b = bundle;
    }

    public String a() {
        return this.f1471a;
    }

    public Bundle b() {
        return this.f1472b;
    }
}
