package com.crashlytics.android.a;

import java.util.HashSet;
import java.util.Set;

class z implements q {

    /* renamed from: b reason: collision with root package name */
    static final Set<b> f1478b = new HashSet<b>() {
        {
            add(b.START);
            add(b.RESUME);
            add(b.PAUSE);
            add(b.STOP);
        }
    };

    /* renamed from: a reason: collision with root package name */
    final int f1479a;

    public z(int i) {
        this.f1479a = i;
    }

    public boolean a(ae aeVar) {
        return (f1478b.contains(aeVar.c) && aeVar.f1427a.g == null) && (Math.abs(aeVar.f1427a.c.hashCode() % this.f1479a) != 0);
    }
}
