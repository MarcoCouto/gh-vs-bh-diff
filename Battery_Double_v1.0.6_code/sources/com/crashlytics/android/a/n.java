package com.crashlytics.android.a;

public class n extends d<n> {

    /* renamed from: a reason: collision with root package name */
    private final String f1466a;

    public n(String str) {
        if (str == null) {
            throw new NullPointerException("eventName must not be null");
        }
        this.f1466a = this.f1441b.a(str);
    }

    /* access modifiers changed from: 0000 */
    public String a() {
        return this.f1466a;
    }

    public String toString() {
        return "{eventName:\"" + this.f1466a + '\"' + ", customAttributes:" + this.c + "}";
    }
}
