package com.crashlytics.android.a;

import io.a.a.a.a.c.a.a;
import java.util.Random;

class x implements a {

    /* renamed from: a reason: collision with root package name */
    final a f1474a;

    /* renamed from: b reason: collision with root package name */
    final Random f1475b;
    final double c;

    public x(a aVar, double d) {
        this(aVar, d, new Random());
    }

    public x(a aVar, double d, Random random) {
        if (d < 0.0d || d > 1.0d) {
            throw new IllegalArgumentException("jitterPercent must be between 0.0 and 1.0");
        } else if (aVar == null) {
            throw new NullPointerException("backoff must not be null");
        } else if (random == null) {
            throw new NullPointerException("random must not be null");
        } else {
            this.f1474a = aVar;
            this.c = d;
            this.f1475b = random;
        }
    }

    /* access modifiers changed from: 0000 */
    public double a() {
        double d = 1.0d - this.c;
        return d + (((this.c + 1.0d) - d) * this.f1475b.nextDouble());
    }

    public long a(int i) {
        return (long) (a() * ((double) this.f1474a.a(i)));
    }
}
