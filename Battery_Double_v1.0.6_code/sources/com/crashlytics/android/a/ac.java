package com.crashlytics.android.a;

import android.app.Activity;
import android.content.Context;
import android.os.Looper;
import com.crashlytics.android.a.l.a;
import io.a.a.a.a.b.n;
import io.a.a.a.a.b.p;
import io.a.a.a.a.f.b;
import io.a.a.a.c;
import io.a.a.a.i;
import java.util.concurrent.ScheduledExecutorService;

class ac implements a {

    /* renamed from: a reason: collision with root package name */
    final f f1425a;

    /* renamed from: b reason: collision with root package name */
    final io.a.a.a.a f1426b;
    final l c;
    final i d;
    private final long e;

    ac(f fVar, io.a.a.a.a aVar, l lVar, i iVar, long j) {
        this.f1425a = fVar;
        this.f1426b = aVar;
        this.c = lVar;
        this.d = iVar;
        this.e = j;
    }

    public static ac a(i iVar, Context context, p pVar, String str, String str2, long j) {
        ah ahVar = new ah(context, pVar, str, str2);
        g gVar = new g(context, new b(iVar));
        io.a.a.a.a.e.b bVar = new io.a.a.a.a.e.b(c.h());
        io.a.a.a.a aVar = new io.a.a.a.a(context);
        ScheduledExecutorService b2 = n.b("Answers Events Handler");
        l lVar = new l(b2);
        return new ac(new f(iVar, context, gVar, ahVar, bVar, b2, new s(context)), aVar, lVar, i.a(context), j);
    }

    public void a() {
        c.h().a("Answers", "Flush events when app is backgrounded");
        this.f1425a.c();
    }

    public void a(long j) {
        c.h().a("Answers", "Logged install");
        this.f1425a.b(ae.a(j));
    }

    public void a(Activity activity, b bVar) {
        c.h().a("Answers", "Logged lifecycle event: " + bVar.name());
        this.f1425a.a(ae.a(bVar, activity));
    }

    public void a(n nVar) {
        c.h().a("Answers", "Logged custom event: " + nVar);
        this.f1425a.a(ae.a(nVar));
    }

    public void a(w wVar) {
        c.h().a("Answers", "Logged predefined event: " + wVar);
        this.f1425a.a(ae.a(wVar));
    }

    public void a(io.a.a.a.a.g.b bVar, String str) {
        this.c.a(bVar.j);
        this.f1425a.a(bVar, str);
    }

    public void a(String str, String str2) {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            throw new IllegalStateException("onCrash called from main thread!!!");
        }
        c.h().a("Answers", "Logged crash");
        this.f1425a.c(ae.a(str, str2));
    }

    public void b() {
        this.f1425a.b();
        this.f1426b.a(new h(this, this.c));
        this.c.a((a) this);
        if (d()) {
            a(this.e);
            this.d.a();
        }
    }

    public void c() {
        this.f1426b.a();
        this.f1425a.a();
    }

    /* access modifiers changed from: 0000 */
    public boolean d() {
        return !this.d.b();
    }
}
