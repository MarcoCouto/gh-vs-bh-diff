package com.crashlytics.android.a;

public class m extends w<m> {
    public m a(String str) {
        this.d.a("contentId", str);
        return this;
    }

    /* access modifiers changed from: 0000 */
    public String a() {
        return "contentView";
    }

    public m b(String str) {
        this.d.a("contentName", str);
        return this;
    }

    public m c(String str) {
        this.d.a("contentType", str);
        return this;
    }
}
