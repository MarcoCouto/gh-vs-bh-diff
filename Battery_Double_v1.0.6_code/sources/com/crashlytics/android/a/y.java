package com.crashlytics.android.a;

import io.a.a.a.a.c.a.e;

class y {

    /* renamed from: a reason: collision with root package name */
    long f1476a;

    /* renamed from: b reason: collision with root package name */
    private e f1477b;

    public y(e eVar) {
        if (eVar == null) {
            throw new NullPointerException("retryState must not be null");
        }
        this.f1477b = eVar;
    }

    public void a() {
        this.f1476a = 0;
        this.f1477b = this.f1477b.c();
    }

    public boolean a(long j) {
        return j - this.f1476a >= 1000000 * this.f1477b.a();
    }

    public void b(long j) {
        this.f1476a = j;
        this.f1477b = this.f1477b.b();
    }
}
