package com.crashlytics.android.a;

import android.app.Activity;
import java.util.Collections;
import java.util.Map;

final class ae {

    /* renamed from: a reason: collision with root package name */
    public final af f1427a;

    /* renamed from: b reason: collision with root package name */
    public final long f1428b;
    public final b c;
    public final Map<String, String> d;
    public final String e;
    public final Map<String, Object> f;
    public final String g;
    public final Map<String, Object> h;
    private String i;

    static class a {

        /* renamed from: a reason: collision with root package name */
        final b f1429a;

        /* renamed from: b reason: collision with root package name */
        final long f1430b = System.currentTimeMillis();
        Map<String, String> c = null;
        String d = null;
        Map<String, Object> e = null;
        String f = null;
        Map<String, Object> g = null;

        public a(b bVar) {
            this.f1429a = bVar;
        }

        public a a(String str) {
            this.d = str;
            return this;
        }

        public a a(Map<String, String> map) {
            this.c = map;
            return this;
        }

        public ae a(af afVar) {
            return new ae(afVar, this.f1430b, this.f1429a, this.c, this.d, this.e, this.f, this.g);
        }

        public a b(String str) {
            this.f = str;
            return this;
        }

        public a b(Map<String, Object> map) {
            this.e = map;
            return this;
        }

        public a c(Map<String, Object> map) {
            this.g = map;
            return this;
        }
    }

    enum b {
        START,
        RESUME,
        PAUSE,
        STOP,
        CRASH,
        INSTALL,
        CUSTOM,
        PREDEFINED
    }

    private ae(af afVar, long j, b bVar, Map<String, String> map, String str, Map<String, Object> map2, String str2, Map<String, Object> map3) {
        this.f1427a = afVar;
        this.f1428b = j;
        this.c = bVar;
        this.d = map;
        this.e = str;
        this.f = map2;
        this.g = str2;
        this.h = map3;
    }

    public static a a(long j) {
        return new a(b.INSTALL).a(Collections.singletonMap("installedAt", String.valueOf(j)));
    }

    public static a a(b bVar, Activity activity) {
        return new a(bVar).a(Collections.singletonMap("activity", activity.getClass().getName()));
    }

    public static a a(n nVar) {
        return new a(b.CUSTOM).a(nVar.a()).b(nVar.b());
    }

    public static a a(w<?> wVar) {
        return new a(b.PREDEFINED).b(wVar.a()).c(wVar.c()).b(wVar.b());
    }

    public static a a(String str) {
        return new a(b.CRASH).a(Collections.singletonMap("sessionId", str));
    }

    public static a a(String str, String str2) {
        return a(str).b(Collections.singletonMap("exceptionName", str2));
    }

    public String toString() {
        if (this.i == null) {
            this.i = "[" + getClass().getSimpleName() + ": " + "timestamp=" + this.f1428b + ", type=" + this.c + ", details=" + this.d + ", customType=" + this.e + ", customAttributes=" + this.f + ", predefinedType=" + this.g + ", predefinedAttributes=" + this.h + ", metadata=[" + this.f1427a + "]]";
        }
        return this.i;
    }
}
