package com.crashlytics.android.b;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

class d {

    /* renamed from: a reason: collision with root package name */
    public final String f1488a;

    /* renamed from: b reason: collision with root package name */
    public final String f1489b;
    public final String c;
    public final String d;

    d(String str, String str2, String str3, String str4) {
        this.f1488a = str;
        this.f1489b = str2;
        this.c = str3;
        this.d = str4;
    }

    public static d a(InputStream inputStream) throws IOException {
        Properties properties = new Properties();
        properties.load(inputStream);
        return a(properties);
    }

    public static d a(Properties properties) {
        return new d(properties.getProperty("version_code"), properties.getProperty("version_name"), properties.getProperty("build_id"), properties.getProperty("package_name"));
    }
}
