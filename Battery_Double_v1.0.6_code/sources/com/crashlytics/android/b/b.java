package com.crashlytics.android.b;

import android.annotation.TargetApi;
import android.app.Activity;
import io.a.a.a.a;
import java.util.concurrent.ExecutorService;

@TargetApi(14)
class b extends a {

    /* renamed from: a reason: collision with root package name */
    private final io.a.a.a.a.b f1482a = new io.a.a.a.a.b() {
        public void a(Activity activity) {
            if (b.this.a()) {
                b.this.f1483b.submit(new Runnable() {
                    public void run() {
                        b.this.c();
                    }
                });
            }
        }
    };
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public final ExecutorService f1483b;

    public b(a aVar, ExecutorService executorService) {
        this.f1483b = executorService;
        aVar.a(this.f1482a);
    }
}
