package com.crashlytics.android.b;

import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import io.a.a.a.a.a.d;
import io.a.a.a.c;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class h implements d<String> {
    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        io.a.a.a.c.h().a("Beta", "Beta by Crashlytics app is not installed");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x005e, code lost:
        if (r1 != null) goto L_0x0060;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0064, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0065, code lost:
        io.a.a.a.c.h().e("Beta", "Failed to close the APK file", r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0071, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0072, code lost:
        r8 = r2;
        r2 = r1;
        r1 = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0086, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0087, code lost:
        io.a.a.a.c.h().e("Beta", "Failed to close the APK file", r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0093, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0094, code lost:
        r8 = r2;
        r2 = r1;
        r1 = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00a9, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00aa, code lost:
        io.a.a.a.c.h().e("Beta", "Failed to close the APK file", r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00b7, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00b8, code lost:
        r2 = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00bf, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00c0, code lost:
        io.a.a.a.c.h().e("Beta", "Failed to close the APK file", r1);
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0052 A[ExcHandler: NameNotFoundException (e android.content.pm.PackageManager$NameNotFoundException), Splitter:B:1:0x0009] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0082 A[SYNTHETIC, Splitter:B:25:0x0082] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00a4 A[SYNTHETIC, Splitter:B:34:0x00a4] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00bb A[SYNTHETIC, Splitter:B:41:0x00bb] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:22:0x0075=Splitter:B:22:0x0075, B:31:0x0097=Splitter:B:31:0x0097} */
    /* renamed from: a */
    public String b(Context context) throws Exception {
        ZipInputStream zipInputStream;
        ZipInputStream a2;
        long nanoTime = System.nanoTime();
        String str = "";
        ZipInputStream zipInputStream2 = null;
        try {
            a2 = a(context, "io.crash.air");
            str = a(a2);
            if (a2 != null) {
                try {
                    a2.close();
                } catch (IOException e) {
                    c.h().e("Beta", "Failed to close the APK file", e);
                }
            }
        } catch (NameNotFoundException e2) {
        } catch (FileNotFoundException e3) {
            Throwable th = e3;
            zipInputStream = a2;
            Throwable th2 = th;
            try {
                c.h().e("Beta", "Failed to find the APK file", th2);
                if (zipInputStream != null) {
                }
            } catch (Throwable th3) {
                th = th3;
                if (zipInputStream != null) {
                }
                throw th;
            }
        } catch (IOException e4) {
            Throwable th4 = e4;
            ZipInputStream zipInputStream3 = a2;
            Throwable th5 = th4;
            c.h().e("Beta", "Failed to read the APK file", th5);
            if (zipInputStream != null) {
            }
        } catch (Throwable th6) {
            th = th6;
            zipInputStream = zipInputStream2;
            if (zipInputStream != null) {
            }
            throw th;
        }
        c.h().a("Beta", "Beta device token load took " + (((double) (System.nanoTime() - nanoTime)) / 1000000.0d) + "ms");
        return str;
    }

    /* access modifiers changed from: 0000 */
    public String a(ZipInputStream zipInputStream) throws IOException {
        ZipEntry nextEntry = zipInputStream.getNextEntry();
        if (nextEntry != null) {
            String name = nextEntry.getName();
            if (name.startsWith("assets/com.crashlytics.android.beta/dirfactor-device-token=")) {
                return name.substring("assets/com.crashlytics.android.beta/dirfactor-device-token=".length(), name.length() - 1);
            }
        }
        return "";
    }

    /* access modifiers changed from: 0000 */
    public ZipInputStream a(Context context, String str) throws NameNotFoundException, FileNotFoundException {
        return new ZipInputStream(new FileInputStream(context.getPackageManager().getApplicationInfo(str, 0).sourceDir));
    }
}
