package com.crashlytics.android.b;

import android.annotation.TargetApi;
import android.app.Application;
import android.content.Context;
import android.os.Build.VERSION;
import android.text.TextUtils;
import io.a.a.a.a.a.b;
import io.a.a.a.a.a.d;
import io.a.a.a.a.b.m;
import io.a.a.a.a.b.p;
import io.a.a.a.a.b.p.a;
import io.a.a.a.a.g.f;
import io.a.a.a.a.g.q;
import io.a.a.a.a.g.t;
import io.a.a.a.i;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

public class c extends i<Boolean> implements m {

    /* renamed from: a reason: collision with root package name */
    private final b<String> f1486a = new b<>();

    /* renamed from: b reason: collision with root package name */
    private final h f1487b = new h();
    private j c;

    /* JADX WARNING: type inference failed for: r1v0 */
    /* JADX WARNING: type inference failed for: r2v0, types: [java.io.InputStream] */
    /* JADX WARNING: type inference failed for: r2v2 */
    /* JADX WARNING: type inference failed for: r2v3, types: [java.io.InputStream] */
    /* JADX WARNING: type inference failed for: r2v5 */
    /* JADX WARNING: type inference failed for: r6v0 */
    /* JADX WARNING: type inference failed for: r0v8 */
    /* JADX WARNING: type inference failed for: r2v9 */
    /* JADX WARNING: type inference failed for: r2v10 */
    /* JADX WARNING: type inference failed for: r2v11 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0088 A[SYNTHETIC, Splitter:B:27:0x0088] */
    /* JADX WARNING: Unknown variable types count: 4 */
    private d a(Context context) {
        ? r2;
        ? r22;
        Throwable th;
        d dVar;
        d dVar2;
        d a2;
        ? r1 = 0;
        try {
            InputStream open = context.getAssets().open("crashlytics-build.properties");
            if (open != 0) {
                try {
                    a2 = d.a(open);
                } catch (Exception e) {
                    Throwable th2 = e;
                    dVar2 = r1;
                    th = th2;
                    r22 = open;
                }
                try {
                    io.a.a.a.c.h().a("Beta", a2.d + " build properties: " + a2.f1489b + " (" + a2.f1488a + ") - " + a2.c);
                    dVar = a2;
                } catch (Exception e2) {
                    Throwable th3 = e2;
                    dVar2 = a2;
                    th = th3;
                    r22 = open;
                    try {
                        io.a.a.a.c.h().e("Beta", "Error reading Beta build properties", th);
                        if (r22 != 0) {
                            try {
                                r22.close();
                            } catch (IOException e3) {
                                io.a.a.a.c.h().e("Beta", "Error closing Beta build properties asset", e3);
                            }
                        }
                        return dVar;
                    } catch (Throwable th4) {
                        th = th4;
                        r2 = r22;
                        if (r2 != 0) {
                            try {
                                r2.close();
                            } catch (IOException e4) {
                                io.a.a.a.c.h().e("Beta", "Error closing Beta build properties asset", e4);
                            }
                        }
                        throw th;
                    }
                }
            } else {
                dVar = r1;
            }
            if (open != 0) {
                try {
                    open.close();
                } catch (IOException e5) {
                    io.a.a.a.c.h().e("Beta", "Error closing Beta build properties asset", e5);
                }
            }
        } catch (Exception e6) {
            r22 = r1;
            ? r6 = r1;
            th = e6;
            dVar2 = r6;
        } catch (Throwable th5) {
            th = th5;
            r2 = r1;
            if (r2 != 0) {
            }
            throw th;
        }
        return dVar;
    }

    private String a(Context context, String str) {
        String str2;
        try {
            str2 = (String) this.f1486a.a(context, (d) this.f1487b);
            if ("".equals(str2)) {
                str2 = null;
            }
        } catch (Exception e) {
            io.a.a.a.c.h().e("Beta", "Failed to load the Beta device token", e);
            str2 = null;
        }
        io.a.a.a.c.h().a("Beta", "Beta device token present: " + (!TextUtils.isEmpty(str2)));
        return str2;
    }

    private f h() {
        t b2 = q.a().b();
        if (b2 != null) {
            return b2.f;
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    @TargetApi(14)
    public j a(int i, Application application) {
        return i >= 14 ? new b(r().e(), r().f()) : new i();
    }

    public String a() {
        return "1.2.7.19";
    }

    /* access modifiers changed from: 0000 */
    public boolean a(f fVar, d dVar) {
        return (fVar == null || TextUtils.isEmpty(fVar.f4477a) || dVar == null) ? false : true;
    }

    public String b() {
        return "com.crashlytics.sdk.android:beta";
    }

    /* access modifiers changed from: protected */
    @TargetApi(14)
    public boolean b_() {
        this.c = a(VERSION.SDK_INT, (Application) q().getApplicationContext());
        return true;
    }

    /* access modifiers changed from: protected */
    /* renamed from: c */
    public Boolean e() {
        io.a.a.a.c.h().a("Beta", "Beta kit initializing...");
        Context q = q();
        p p = p();
        if (TextUtils.isEmpty(a(q, p.i()))) {
            io.a.a.a.c.h().a("Beta", "A Beta device token was not found for this app");
            return Boolean.valueOf(false);
        }
        io.a.a.a.c.h().a("Beta", "Beta device token is present, checking for app updates.");
        f h = h();
        d a2 = a(q);
        if (a(h, a2)) {
            this.c.a(q, this, p, h, a2, new io.a.a.a.a.f.d(this), new io.a.a.a.a.b.t(), new io.a.a.a.a.e.b(io.a.a.a.c.h()));
        }
        return Boolean.valueOf(true);
    }

    public Map<a, String> f() {
        String a2 = a(q(), p().i());
        HashMap hashMap = new HashMap();
        if (!TextUtils.isEmpty(a2)) {
            hashMap.put(a.FONT_TOKEN, a2);
        }
        return hashMap;
    }

    /* access modifiers changed from: 0000 */
    public String g() {
        return io.a.a.a.a.b.i.b(q(), "com.crashlytics.ApiEndpoint");
    }
}
