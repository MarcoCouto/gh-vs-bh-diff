package com.crashlytics.android.b;

import java.io.IOException;
import org.json.JSONObject;

class g {
    g() {
    }

    public f a(JSONObject jSONObject) throws IOException {
        if (jSONObject == null) {
            return null;
        }
        return new f(jSONObject.optString("url", null), jSONObject.optString("version_string", null), jSONObject.optString("display_version", null), jSONObject.optString("build_version", null), jSONObject.optString("identifier", null), jSONObject.optString("instance_identifier", null));
    }
}
