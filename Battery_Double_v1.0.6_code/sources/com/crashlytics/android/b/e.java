package com.crashlytics.android.b;

import io.a.a.a.a.b.a;
import io.a.a.a.a.e.c;
import io.a.a.a.a.e.d;
import io.a.a.a.i;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;

class e extends a {

    /* renamed from: b reason: collision with root package name */
    private final g f1490b;

    public e(i iVar, String str, String str2, io.a.a.a.a.e.e eVar, g gVar) {
        super(iVar, str, str2, eVar, c.GET);
        this.f1490b = gVar;
    }

    private d a(d dVar, String str, String str2) {
        return dVar.a("Accept", "application/json").a("User-Agent", "Crashlytics Android SDK/" + this.f4362a.a()).a("X-CRASHLYTICS-DEVELOPER-TOKEN", "470fa2b4ae81cd56ecbcda9735803434cec591fa").a("X-CRASHLYTICS-API-CLIENT-TYPE", "android").a("X-CRASHLYTICS-API-CLIENT-VERSION", this.f4362a.a()).a("X-CRASHLYTICS-API-KEY", str).a("X-CRASHLYTICS-BETA-TOKEN", a(str2));
    }

    static String a(String str) {
        return "3:" + str;
    }

    private Map<String, String> a(d dVar) {
        HashMap hashMap = new HashMap();
        hashMap.put("build_version", dVar.f1488a);
        hashMap.put("display_version", dVar.f1489b);
        hashMap.put("instance", dVar.c);
        hashMap.put("source", "3");
        return hashMap;
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x0120  */
    public f a(String str, String str2, d dVar) {
        d dVar2;
        Throwable th;
        f fVar = null;
        try {
            Map a2 = a(dVar);
            try {
                dVar2 = a(a(a2), str, str2);
                io.a.a.a.c.h().a("Beta", "Checking for updates from " + a());
                io.a.a.a.c.h().a("Beta", "Checking for updates query params are: " + a2);
                if (dVar2.c()) {
                    io.a.a.a.c.h().a("Beta", "Checking for updates was successful");
                    fVar = this.f1490b.a(new JSONObject(dVar2.e()));
                    if (dVar2 != null) {
                        io.a.a.a.c.h().a("Fabric", "Checking for updates request ID: " + dVar2.b("X-REQUEST-ID"));
                    }
                } else {
                    io.a.a.a.c.h().e("Beta", "Checking for updates failed. Response code: " + dVar2.b());
                    if (dVar2 != null) {
                        io.a.a.a.c.h().a("Fabric", "Checking for updates request ID: " + dVar2.b("X-REQUEST-ID"));
                    }
                }
            } catch (Exception e) {
                e = e;
                try {
                    io.a.a.a.c.h().e("Beta", "Error while checking for updates from " + a(), e);
                    if (dVar2 != null) {
                        io.a.a.a.c.h().a("Fabric", "Checking for updates request ID: " + dVar2.b("X-REQUEST-ID"));
                    }
                    return fVar;
                } catch (Throwable th2) {
                    th = th2;
                    if (dVar2 != null) {
                        io.a.a.a.c.h().a("Fabric", "Checking for updates request ID: " + dVar2.b("X-REQUEST-ID"));
                    }
                    throw th;
                }
            }
        } catch (Exception e2) {
            e = e2;
            dVar2 = null;
        } catch (Throwable th3) {
            dVar2 = null;
            th = th3;
            if (dVar2 != null) {
            }
            throw th;
        }
        return fVar;
    }
}
