package com.crashlytics.android.b;

import android.annotation.SuppressLint;
import android.content.Context;
import io.a.a.a.a.b.g;
import io.a.a.a.a.b.k;
import io.a.a.a.a.b.p;
import io.a.a.a.a.e.e;
import io.a.a.a.a.f.c;
import io.a.a.a.a.g.f;
import java.util.concurrent.atomic.AtomicBoolean;

abstract class a implements j {

    /* renamed from: a reason: collision with root package name */
    private final AtomicBoolean f1480a;

    /* renamed from: b reason: collision with root package name */
    private final AtomicBoolean f1481b;
    private Context c;
    private c d;
    private p e;
    private f f;
    private d g;
    private c h;
    private k i;
    private e j;
    private long k;

    public a() {
        this(false);
    }

    public a(boolean z) {
        this.f1480a = new AtomicBoolean();
        this.k = 0;
        this.f1481b = new AtomicBoolean(z);
    }

    private void e() {
        io.a.a.a.c.h().a("Beta", "Performing update check");
        new e(this.d, this.d.g(), this.f.f4477a, this.j, new g()).a(new g().a(this.c), (String) this.e.h().get(io.a.a.a.a.b.p.a.FONT_TOKEN), this.g);
    }

    /* access modifiers changed from: 0000 */
    public void a(long j2) {
        this.k = j2;
    }

    public void a(Context context, c cVar, p pVar, f fVar, d dVar, c cVar2, k kVar, e eVar) {
        this.c = context;
        this.d = cVar;
        this.e = pVar;
        this.f = fVar;
        this.g = dVar;
        this.h = cVar2;
        this.i = kVar;
        this.j = eVar;
        if (b()) {
            c();
        }
    }

    /* access modifiers changed from: protected */
    public boolean a() {
        this.f1481b.set(true);
        return this.f1480a.get();
    }

    /* access modifiers changed from: 0000 */
    public boolean b() {
        this.f1480a.set(true);
        return this.f1481b.get();
    }

    /* access modifiers changed from: protected */
    @SuppressLint({"CommitPrefEdits"})
    public void c() {
        synchronized (this.h) {
            if (this.h.a().contains("last_update_check")) {
                this.h.a(this.h.b().remove("last_update_check"));
            }
        }
        long a2 = this.i.a();
        long j2 = ((long) this.f.f4478b) * 1000;
        io.a.a.a.c.h().a("Beta", "Check for updates delay: " + j2);
        io.a.a.a.c.h().a("Beta", "Check for updates last check time: " + d());
        long d2 = j2 + d();
        io.a.a.a.c.h().a("Beta", "Check for updates current time: " + a2 + ", next check time: " + d2);
        if (a2 >= d2) {
            try {
                e();
            } finally {
                a(a2);
            }
        } else {
            io.a.a.a.c.h().a("Beta", "Check for updates next check time was not passed");
        }
    }

    /* access modifiers changed from: 0000 */
    public long d() {
        return this.k;
    }
}
