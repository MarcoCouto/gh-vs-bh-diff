package com.crashlytics.android;

import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;

class b implements a {
    b() {
    }

    public boolean a(Context context) {
        try {
            Bundle bundle = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData;
            return bundle == null || bundle.getBoolean("firebase_crashlytics_collection_enabled", true);
        } catch (NameNotFoundException e) {
            return true;
        }
    }
}
