package com.crashlytics.android.c;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Arrays;
import java.util.Comparator;

final class am {

    /* renamed from: a reason: collision with root package name */
    private static final FilenameFilter f1527a = new FilenameFilter() {
        public boolean accept(File file, String str) {
            return true;
        }
    };

    static int a(File file, int i, Comparator<File> comparator) {
        return a(file, f1527a, i, comparator);
    }

    static int a(File file, FilenameFilter filenameFilter, int i, Comparator<File> comparator) {
        int i2 = 0;
        File[] listFiles = file.listFiles(filenameFilter);
        if (listFiles != null) {
            int length = listFiles.length;
            Arrays.sort(listFiles, comparator);
            int length2 = listFiles.length;
            i2 = length;
            int i3 = 0;
            while (i3 < length2) {
                File file2 = listFiles[i3];
                if (i2 <= i) {
                    break;
                }
                file2.delete();
                i3++;
                i2--;
            }
        }
        return i2;
    }
}
