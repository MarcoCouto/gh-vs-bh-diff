package com.crashlytics.android.c;

import io.a.a.a.c;
import java.lang.Thread.UncaughtExceptionHandler;
import java.util.concurrent.atomic.AtomicBoolean;

class m implements UncaughtExceptionHandler {

    /* renamed from: a reason: collision with root package name */
    private final a f1608a;

    /* renamed from: b reason: collision with root package name */
    private final UncaughtExceptionHandler f1609b;
    private final AtomicBoolean c = new AtomicBoolean(false);

    interface a {
        void a(Thread thread, Throwable th);
    }

    public m(a aVar, UncaughtExceptionHandler uncaughtExceptionHandler) {
        this.f1608a = aVar;
        this.f1609b = uncaughtExceptionHandler;
    }

    /* access modifiers changed from: 0000 */
    public boolean a() {
        return this.c.get();
    }

    public void uncaughtException(Thread thread, Throwable th) {
        this.c.set(true);
        try {
            this.f1608a.a(thread, th);
        } catch (Exception e) {
            c.h().e("CrashlyticsCore", "An error occurred in the uncaught exception handler", e);
        } finally {
            c.h().a("CrashlyticsCore", "Crashlytics completed exception processing. Invoking default exception handler.");
            this.f1609b.uncaughtException(thread, th);
            this.c.set(false);
        }
    }
}
