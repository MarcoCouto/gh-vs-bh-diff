package com.crashlytics.android.c;

import android.content.Context;
import io.a.a.a.a.b.i;
import io.a.a.a.a.g.o;

class r {

    /* renamed from: a reason: collision with root package name */
    private final Context f1616a;

    /* renamed from: b reason: collision with root package name */
    private final o f1617b;

    public r(Context context, o oVar) {
        this.f1616a = context;
        this.f1617b = oVar;
    }

    private String a(String str, String str2) {
        return b(i.b(this.f1616a, str), str2);
    }

    private boolean a(String str) {
        return str == null || str.length() == 0;
    }

    private String b(String str, String str2) {
        return a(str) ? str2 : str;
    }

    public String a() {
        return a("com.crashlytics.CrashSubmissionPromptTitle", this.f1617b.f4486a);
    }

    public String b() {
        return a("com.crashlytics.CrashSubmissionPromptMessage", this.f1617b.f4487b);
    }

    public String c() {
        return a("com.crashlytics.CrashSubmissionSendTitle", this.f1617b.c);
    }

    public String d() {
        return a("com.crashlytics.CrashSubmissionAlwaysSendTitle", this.f1617b.g);
    }

    public String e() {
        return a("com.crashlytics.CrashSubmissionCancelTitle", this.f1617b.e);
    }
}
