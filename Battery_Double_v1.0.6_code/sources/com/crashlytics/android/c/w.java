package com.crashlytics.android.c;

import io.a.a.a.a.b.i;
import io.a.a.a.c;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

class w {

    /* renamed from: a reason: collision with root package name */
    private static final Charset f1624a = Charset.forName("UTF-8");

    /* renamed from: b reason: collision with root package name */
    private final File f1625b;

    public w(File file) {
        this.f1625b = file;
    }

    private static String a(JSONObject jSONObject, String str) {
        if (!jSONObject.isNull(str)) {
            return jSONObject.optString(str, null);
        }
        return null;
    }

    private File c(String str) {
        return new File(this.f1625b, str + "user" + ".meta");
    }

    private File d(String str) {
        return new File(this.f1625b, str + "keys" + ".meta");
    }

    private static al e(String str) throws JSONException {
        JSONObject jSONObject = new JSONObject(str);
        return new al(a(jSONObject, "userId"), a(jSONObject, "userName"), a(jSONObject, "userEmail"));
    }

    private static Map<String, String> f(String str) throws JSONException {
        JSONObject jSONObject = new JSONObject(str);
        HashMap hashMap = new HashMap();
        Iterator keys = jSONObject.keys();
        while (keys.hasNext()) {
            String str2 = (String) keys.next();
            hashMap.put(str2, a(jSONObject, str2));
        }
        return hashMap;
    }

    public al a(String str) {
        FileInputStream fileInputStream;
        File c = c(str);
        if (!c.exists()) {
            return al.f1525a;
        }
        try {
            fileInputStream = new FileInputStream(c);
            try {
                al e = e(i.a((InputStream) fileInputStream));
                i.a((Closeable) fileInputStream, "Failed to close user metadata file.");
                return e;
            } catch (Exception e2) {
                e = e2;
                try {
                    c.h().e("CrashlyticsCore", "Error deserializing user metadata.", e);
                    i.a((Closeable) fileInputStream, "Failed to close user metadata file.");
                    return al.f1525a;
                } catch (Throwable th) {
                    th = th;
                    i.a((Closeable) fileInputStream, "Failed to close user metadata file.");
                    throw th;
                }
            }
        } catch (Exception e3) {
            e = e3;
            fileInputStream = null;
            c.h().e("CrashlyticsCore", "Error deserializing user metadata.", e);
            i.a((Closeable) fileInputStream, "Failed to close user metadata file.");
            return al.f1525a;
        } catch (Throwable th2) {
            th = th2;
            fileInputStream = null;
            i.a((Closeable) fileInputStream, "Failed to close user metadata file.");
            throw th;
        }
    }

    public Map<String, String> b(String str) {
        FileInputStream fileInputStream;
        File d = d(str);
        if (!d.exists()) {
            return Collections.emptyMap();
        }
        try {
            fileInputStream = new FileInputStream(d);
            try {
                Map<String, String> f = f(i.a((InputStream) fileInputStream));
                i.a((Closeable) fileInputStream, "Failed to close user metadata file.");
                return f;
            } catch (Exception e) {
                e = e;
                try {
                    c.h().e("CrashlyticsCore", "Error deserializing user metadata.", e);
                    i.a((Closeable) fileInputStream, "Failed to close user metadata file.");
                    return Collections.emptyMap();
                } catch (Throwable th) {
                    th = th;
                    i.a((Closeable) fileInputStream, "Failed to close user metadata file.");
                    throw th;
                }
            }
        } catch (Exception e2) {
            e = e2;
            fileInputStream = null;
            c.h().e("CrashlyticsCore", "Error deserializing user metadata.", e);
            i.a((Closeable) fileInputStream, "Failed to close user metadata file.");
            return Collections.emptyMap();
        } catch (Throwable th2) {
            th = th2;
            fileInputStream = null;
            i.a((Closeable) fileInputStream, "Failed to close user metadata file.");
            throw th;
        }
    }
}
