package com.crashlytics.android.c;

class x implements ai {

    /* renamed from: a reason: collision with root package name */
    private final int f1626a;

    /* renamed from: b reason: collision with root package name */
    private final ai[] f1627b;
    private final y c;

    public x(int i, ai... aiVarArr) {
        this.f1626a = i;
        this.f1627b = aiVarArr;
        this.c = new y(i);
    }

    public StackTraceElement[] a(StackTraceElement[] stackTraceElementArr) {
        if (stackTraceElementArr.length <= this.f1626a) {
            return stackTraceElementArr;
        }
        ai[] aiVarArr = this.f1627b;
        int length = aiVarArr.length;
        int i = 0;
        StackTraceElement[] stackTraceElementArr2 = stackTraceElementArr;
        while (i < length) {
            ai aiVar = aiVarArr[i];
            if (stackTraceElementArr2.length <= this.f1626a) {
                break;
            }
            i++;
            stackTraceElementArr2 = aiVar.a(stackTraceElementArr);
        }
        if (stackTraceElementArr2.length > this.f1626a) {
            stackTraceElementArr2 = this.c.a(stackTraceElementArr2);
        }
        return stackTraceElementArr2;
    }
}
