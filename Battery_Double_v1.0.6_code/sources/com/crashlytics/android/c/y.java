package com.crashlytics.android.c;

class y implements ai {

    /* renamed from: a reason: collision with root package name */
    private final int f1628a;

    public y(int i) {
        this.f1628a = i;
    }

    public StackTraceElement[] a(StackTraceElement[] stackTraceElementArr) {
        if (stackTraceElementArr.length <= this.f1628a) {
            return stackTraceElementArr;
        }
        int i = this.f1628a / 2;
        int i2 = this.f1628a - i;
        StackTraceElement[] stackTraceElementArr2 = new StackTraceElement[this.f1628a];
        System.arraycopy(stackTraceElementArr, 0, stackTraceElementArr2, 0, i2);
        System.arraycopy(stackTraceElementArr, stackTraceElementArr.length - i, stackTraceElementArr2, i2, i);
        return stackTraceElementArr2;
    }
}
