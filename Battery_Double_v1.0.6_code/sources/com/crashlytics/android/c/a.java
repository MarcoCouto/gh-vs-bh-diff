package com.crashlytics.android.c;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import io.a.a.a.a.b.p;

class a {

    /* renamed from: a reason: collision with root package name */
    public final String f1493a;

    /* renamed from: b reason: collision with root package name */
    public final String f1494b;
    public final String c;
    public final String d;
    public final String e;
    public final String f;

    a(String str, String str2, String str3, String str4, String str5, String str6) {
        this.f1493a = str;
        this.f1494b = str2;
        this.c = str3;
        this.d = str4;
        this.e = str5;
        this.f = str6;
    }

    public static a a(Context context, p pVar, String str, String str2) throws NameNotFoundException {
        String packageName = context.getPackageName();
        String i = pVar.i();
        PackageInfo packageInfo = context.getPackageManager().getPackageInfo(packageName, 0);
        return new a(str, str2, i, packageName, Integer.toString(packageInfo.versionCode), packageInfo.versionName == null ? "0.0" : packageInfo.versionName);
    }
}
