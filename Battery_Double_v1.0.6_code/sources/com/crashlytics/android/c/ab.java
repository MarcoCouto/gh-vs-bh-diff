package com.crashlytics.android.c;

import android.annotation.SuppressLint;
import io.a.a.a.a.f.c;
import io.a.a.a.a.f.d;

@SuppressLint({"CommitPrefEdits"})
class ab {

    /* renamed from: a reason: collision with root package name */
    private final c f1509a;

    public ab(c cVar) {
        this.f1509a = cVar;
    }

    public static ab a(c cVar, i iVar) {
        if (!cVar.a().getBoolean("preferences_migration_complete", false)) {
            d dVar = new d(iVar);
            if (!cVar.a().contains("always_send_reports_opt_in") && dVar.a().contains("always_send_reports_opt_in")) {
                cVar.a(cVar.b().putBoolean("always_send_reports_opt_in", dVar.a().getBoolean("always_send_reports_opt_in", false)));
            }
            cVar.a(cVar.b().putBoolean("preferences_migration_complete", true));
        }
        return new ab(cVar);
    }

    /* access modifiers changed from: 0000 */
    public void a(boolean z) {
        this.f1509a.a(this.f1509a.b().putBoolean("always_send_reports_opt_in", z));
    }

    /* access modifiers changed from: 0000 */
    public boolean a() {
        return this.f1509a.a().getBoolean("always_send_reports_opt_in", false);
    }
}
