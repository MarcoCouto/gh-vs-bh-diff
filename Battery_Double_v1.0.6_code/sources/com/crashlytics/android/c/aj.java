package com.crashlytics.android.c;

class aj {

    /* renamed from: a reason: collision with root package name */
    public final String f1523a;

    /* renamed from: b reason: collision with root package name */
    public final String f1524b;
    public final StackTraceElement[] c;
    public final aj d;

    public aj(Throwable th, ai aiVar) {
        this.f1523a = th.getLocalizedMessage();
        this.f1524b = th.getClass().getName();
        this.c = aiVar.a(th.getStackTrace());
        Throwable cause = th.getCause();
        this.d = cause != null ? new aj(cause, aiVar) : null;
    }
}
