package com.crashlytics.android.c;

import io.a.a.a.c;
import java.io.File;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

class ah implements ae {

    /* renamed from: a reason: collision with root package name */
    private final File f1521a;

    /* renamed from: b reason: collision with root package name */
    private final File[] f1522b;
    private final Map<String, String> c;

    public ah(File file) {
        this(file, Collections.emptyMap());
    }

    public ah(File file, Map<String, String> map) {
        this.f1521a = file;
        this.f1522b = new File[]{file};
        this.c = new HashMap(map);
        if (this.f1521a.length() == 0) {
            this.c.putAll(af.f1515a);
        }
    }

    public String a() {
        return c().getName();
    }

    public String b() {
        String a2 = a();
        return a2.substring(0, a2.lastIndexOf(46));
    }

    public File c() {
        return this.f1521a;
    }

    public File[] d() {
        return this.f1522b;
    }

    public Map<String, String> e() {
        return Collections.unmodifiableMap(this.c);
    }

    public void f() {
        c.h().a("CrashlyticsCore", "Removing report at " + this.f1521a.getPath());
        this.f1521a.delete();
    }
}
