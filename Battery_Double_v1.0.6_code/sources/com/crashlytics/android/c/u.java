package com.crashlytics.android.c;

import android.content.Context;
import io.a.a.a.a.b.i;
import io.a.a.a.c;
import java.io.File;
import java.util.Set;

class u {

    /* renamed from: a reason: collision with root package name */
    private static final b f1620a = new b();

    /* renamed from: b reason: collision with root package name */
    private final Context f1621b;
    private final a c;
    private s d;

    public interface a {
        File a();
    }

    private static final class b implements s {
        private b() {
        }

        public b a() {
            return null;
        }

        public void b() {
        }

        public void c() {
        }
    }

    u(Context context, a aVar) {
        this(context, aVar, null);
    }

    u(Context context, a aVar, String str) {
        this.f1621b = context;
        this.c = aVar;
        this.d = f1620a;
        a(str);
    }

    private String a(File file) {
        String name = file.getName();
        int lastIndexOf = name.lastIndexOf(".temp");
        return lastIndexOf == -1 ? name : name.substring("crashlytics-userlog-".length(), lastIndexOf);
    }

    private File b(String str) {
        return new File(this.c.a(), "crashlytics-userlog-" + str + ".temp");
    }

    /* access modifiers changed from: 0000 */
    public b a() {
        return this.d.a();
    }

    /* access modifiers changed from: 0000 */
    public void a(File file, int i) {
        this.d = new ac(file, i);
    }

    /* access modifiers changed from: 0000 */
    public final void a(String str) {
        this.d.b();
        this.d = f1620a;
        if (str != null) {
            if (!i.a(this.f1621b, "com.crashlytics.CollectCustomLogs", true)) {
                c.h().a("CrashlyticsCore", "Preferences requested no custom logs. Aborting log file creation.");
            } else {
                a(b(str), 65536);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(Set<String> set) {
        File[] listFiles = this.c.a().listFiles();
        if (listFiles != null) {
            for (File file : listFiles) {
                if (!set.contains(a(file))) {
                    file.delete();
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void b() {
        this.d.c();
    }
}
