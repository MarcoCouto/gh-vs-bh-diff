package com.crashlytics.android.c;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

final class b {

    /* renamed from: a reason: collision with root package name */
    public static final b f1530a = new b(new byte[0]);

    /* renamed from: b reason: collision with root package name */
    private final byte[] f1531b;
    private volatile int c = 0;

    private b(byte[] bArr) {
        this.f1531b = bArr;
    }

    public static b a(String str) {
        try {
            return new b(str.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("UTF-8 not supported.", e);
        }
    }

    public static b a(byte[] bArr, int i, int i2) {
        byte[] bArr2 = new byte[i2];
        System.arraycopy(bArr, i, bArr2, 0, i2);
        return new b(bArr2);
    }

    public int a() {
        return this.f1531b.length;
    }

    public void a(byte[] bArr, int i, int i2, int i3) {
        System.arraycopy(this.f1531b, i, bArr, i2, i3);
    }

    public InputStream b() {
        return new ByteArrayInputStream(this.f1531b);
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof b)) {
            return false;
        }
        b bVar = (b) obj;
        int length = this.f1531b.length;
        if (length != bVar.f1531b.length) {
            return false;
        }
        byte[] bArr = this.f1531b;
        byte[] bArr2 = bVar.f1531b;
        for (int i = 0; i < length; i++) {
            if (bArr[i] != bArr2[i]) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        int i = this.c;
        if (i == 0) {
            byte[] bArr = this.f1531b;
            int length = this.f1531b.length;
            int i2 = 0;
            i = length;
            while (i2 < length) {
                int i3 = bArr[i2] + (i * 31);
                i2++;
                i = i3;
            }
            if (i == 0) {
                i = 1;
            }
            this.c = i;
        }
        return i;
    }
}
