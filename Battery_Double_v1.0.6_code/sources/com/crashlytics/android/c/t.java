package com.crashlytics.android.c;

import io.a.a.a.c;
import java.io.File;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

class t implements ae {

    /* renamed from: a reason: collision with root package name */
    private final File[] f1618a;

    /* renamed from: b reason: collision with root package name */
    private final Map<String, String> f1619b = new HashMap(af.f1515a);
    private final String c;

    public t(String str, File[] fileArr) {
        this.f1618a = fileArr;
        this.c = str;
    }

    public String a() {
        return this.f1618a[0].getName();
    }

    public String b() {
        return this.c;
    }

    public File c() {
        return this.f1618a[0];
    }

    public File[] d() {
        return this.f1618a;
    }

    public Map<String, String> e() {
        return Collections.unmodifiableMap(this.f1619b);
    }

    public void f() {
        File[] fileArr;
        for (File file : this.f1618a) {
            c.h().a("CrashlyticsCore", "Removing invalid report file at " + file.getPath());
            file.delete();
        }
    }
}
