package com.crashlytics.android.c;

import io.a.a.a.a.f.a;
import io.a.a.a.c;
import java.io.File;
import java.io.IOException;

class j {

    /* renamed from: a reason: collision with root package name */
    private final String f1605a;

    /* renamed from: b reason: collision with root package name */
    private final a f1606b;

    public j(String str, a aVar) {
        this.f1605a = str;
        this.f1606b = aVar;
    }

    private File d() {
        return new File(this.f1606b.a(), this.f1605a);
    }

    public boolean a() {
        boolean z = false;
        try {
            return d().createNewFile();
        } catch (IOException e) {
            c.h().e("CrashlyticsCore", "Error creating marker: " + this.f1605a, e);
            return z;
        }
    }

    public boolean b() {
        return d().exists();
    }

    public boolean c() {
        return d().delete();
    }
}
