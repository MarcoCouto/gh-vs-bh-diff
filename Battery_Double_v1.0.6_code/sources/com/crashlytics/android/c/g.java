package com.crashlytics.android.c;

import android.os.Looper;
import io.a.a.a.c;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;

class g {

    /* renamed from: a reason: collision with root package name */
    private final ExecutorService f1546a;

    public g(ExecutorService executorService) {
        this.f1546a = executorService;
    }

    /* access modifiers changed from: 0000 */
    public <T> T a(Callable<T> callable) {
        try {
            return Looper.getMainLooper() == Looper.myLooper() ? this.f1546a.submit(callable).get(4, TimeUnit.SECONDS) : this.f1546a.submit(callable).get();
        } catch (RejectedExecutionException e) {
            c.h().a("CrashlyticsCore", "Executor is shut down because we're handling a fatal crash.");
            return null;
        } catch (Exception e2) {
            c.h().e("CrashlyticsCore", "Failed to execute task.", e2);
            return null;
        }
    }

    /* access modifiers changed from: 0000 */
    public Future<?> a(final Runnable runnable) {
        try {
            return this.f1546a.submit(new Runnable() {
                public void run() {
                    try {
                        runnable.run();
                    } catch (Exception e) {
                        c.h().e("CrashlyticsCore", "Failed to execute task.", e);
                    }
                }
            });
        } catch (RejectedExecutionException e) {
            c.h().a("CrashlyticsCore", "Executor is shut down because we're handling a fatal crash.");
            return null;
        }
    }

    /* access modifiers changed from: 0000 */
    public <T> Future<T> b(final Callable<T> callable) {
        try {
            return this.f1546a.submit(new Callable<T>() {
                public T call() throws Exception {
                    try {
                        return callable.call();
                    } catch (Exception e) {
                        c.h().e("CrashlyticsCore", "Failed to execute task.", e);
                        return null;
                    }
                }
            });
        } catch (RejectedExecutionException e) {
            c.h().a("CrashlyticsCore", "Executor is shut down because we're handling a fatal crash.");
            return null;
        }
    }
}
