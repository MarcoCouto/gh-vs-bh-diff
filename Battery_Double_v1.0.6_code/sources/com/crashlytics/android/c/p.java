package com.crashlytics.android.c;

import io.a.a.a.a.b.a;
import io.a.a.a.a.b.s;
import io.a.a.a.a.e.c;
import io.a.a.a.a.e.d;
import io.a.a.a.a.e.e;
import io.a.a.a.i;
import java.io.File;
import java.util.Iterator;
import java.util.Map.Entry;

class p extends a implements o {
    public p(i iVar, String str, String str2, e eVar) {
        super(iVar, str, str2, eVar, c.POST);
    }

    private d a(d dVar, ae aeVar) {
        File[] d;
        dVar.e("report[identifier]", aeVar.b());
        if (aeVar.d().length == 1) {
            io.a.a.a.c.h().a("CrashlyticsCore", "Adding single file " + aeVar.a() + " to report " + aeVar.b());
            return dVar.a("report[file]", aeVar.a(), "application/octet-stream", aeVar.c());
        }
        int i = 0;
        for (File file : aeVar.d()) {
            io.a.a.a.c.h().a("CrashlyticsCore", "Adding file " + file.getName() + " to report " + aeVar.b());
            dVar.a("report[file" + i + "]", file.getName(), "application/octet-stream", file);
            i++;
        }
        return dVar;
    }

    private d a(d dVar, n nVar) {
        d a2 = dVar.a("X-CRASHLYTICS-API-KEY", nVar.f1610a).a("X-CRASHLYTICS-API-CLIENT-TYPE", "android").a("X-CRASHLYTICS-API-CLIENT-VERSION", this.f4362a.a());
        Iterator it = nVar.f1611b.e().entrySet().iterator();
        while (true) {
            d dVar2 = a2;
            if (!it.hasNext()) {
                return dVar2;
            }
            a2 = dVar2.a((Entry) it.next());
        }
    }

    public boolean a(n nVar) {
        d a2 = a(a(b(), nVar), nVar.f1611b);
        io.a.a.a.c.h().a("CrashlyticsCore", "Sending report to: " + a());
        int b2 = a2.b();
        io.a.a.a.c.h().a("CrashlyticsCore", "Create report request ID: " + a2.b("X-REQUEST-ID"));
        io.a.a.a.c.h().a("CrashlyticsCore", "Result was: " + b2);
        return s.a(b2) == 0;
    }
}
