package com.crashlytics.android.c;

import io.a.a.a.a.e.g;
import java.io.InputStream;

class l implements g {

    /* renamed from: a reason: collision with root package name */
    private final aa f1607a;

    public l(aa aaVar) {
        this.f1607a = aaVar;
    }

    public InputStream a() {
        return this.f1607a.a();
    }

    public String b() {
        return this.f1607a.b();
    }

    public String[] c() {
        return this.f1607a.c();
    }

    public long d() {
        return -1;
    }
}
