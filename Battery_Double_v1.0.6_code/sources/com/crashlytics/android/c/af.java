package com.crashlytics.android.c;

import io.a.a.a.a.b.h;
import java.io.File;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

class af {

    /* renamed from: a reason: collision with root package name */
    static final Map<String, String> f1515a = Collections.singletonMap("X-CRASHLYTICS-INVALID-SESSION", "1");
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public static final short[] f1516b = {10, 20, 30, 60, 120, 300};
    private final Object c = new Object();
    private final o d;
    private final String e;
    private final c f;
    /* access modifiers changed from: private */
    public final b g;
    /* access modifiers changed from: private */
    public Thread h;

    static final class a implements d {
        a() {
        }

        public boolean a() {
            return true;
        }
    }

    interface b {
        boolean a();
    }

    interface c {
        File[] a();

        File[] b();
    }

    interface d {
        boolean a();
    }

    private class e extends h {

        /* renamed from: b reason: collision with root package name */
        private final float f1518b;
        private final d c;

        e(float f, d dVar) {
            this.f1518b = f;
            this.c = dVar;
        }

        private void b() {
            io.a.a.a.c.h().a("CrashlyticsCore", "Starting report processing in " + this.f1518b + " second(s)...");
            if (this.f1518b > 0.0f) {
                try {
                    Thread.sleep((long) (this.f1518b * 1000.0f));
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                    return;
                }
            }
            List<ae> a2 = af.this.a();
            if (!af.this.g.a()) {
                if (a2.isEmpty() || this.c.a()) {
                    List<ae> list = a2;
                    int i = 0;
                    while (!list.isEmpty() && !af.this.g.a()) {
                        io.a.a.a.c.h().a("CrashlyticsCore", "Attempting to send " + list.size() + " report(s)");
                        for (ae a3 : list) {
                            af.this.a(a3);
                        }
                        List a4 = af.this.a();
                        if (!a4.isEmpty()) {
                            int i2 = i + 1;
                            long j = (long) af.f1516b[Math.min(i, af.f1516b.length - 1)];
                            io.a.a.a.c.h().a("CrashlyticsCore", "Report submisson: scheduling delayed retry in " + j + " seconds");
                            try {
                                Thread.sleep(j * 1000);
                                i = i2;
                                list = a4;
                            } catch (InterruptedException e2) {
                                Thread.currentThread().interrupt();
                                return;
                            }
                        } else {
                            list = a4;
                        }
                    }
                    return;
                }
                io.a.a.a.c.h().a("CrashlyticsCore", "User declined to send. Removing " + a2.size() + " Report(s).");
                for (ae f : a2) {
                    f.f();
                }
            }
        }

        public void a() {
            try {
                b();
            } catch (Exception e) {
                io.a.a.a.c.h().e("CrashlyticsCore", "An unexpected error occurred while attempting to upload crash reports.", e);
            }
            af.this.h = null;
        }
    }

    public af(String str, o oVar, c cVar, b bVar) {
        if (oVar == null) {
            throw new IllegalArgumentException("createReportCall must not be null.");
        }
        this.d = oVar;
        this.e = str;
        this.f = cVar;
        this.g = bVar;
    }

    /* access modifiers changed from: 0000 */
    public List<ae> a() {
        File[] a2;
        File[] b2;
        io.a.a.a.c.h().a("CrashlyticsCore", "Checking for crash reports...");
        synchronized (this.c) {
            a2 = this.f.a();
            b2 = this.f.b();
        }
        LinkedList linkedList = new LinkedList();
        if (a2 != null) {
            for (File file : a2) {
                io.a.a.a.c.h().a("CrashlyticsCore", "Found crash report " + file.getPath());
                linkedList.add(new ah(file));
            }
        }
        HashMap hashMap = new HashMap();
        if (b2 != null) {
            for (File file2 : b2) {
                String a3 = h.a(file2);
                if (!hashMap.containsKey(a3)) {
                    hashMap.put(a3, new LinkedList());
                }
                ((List) hashMap.get(a3)).add(file2);
            }
        }
        for (String str : hashMap.keySet()) {
            io.a.a.a.c.h().a("CrashlyticsCore", "Found invalid session: " + str);
            List list = (List) hashMap.get(str);
            linkedList.add(new t(str, (File[]) list.toArray(new File[list.size()])));
        }
        if (linkedList.isEmpty()) {
            io.a.a.a.c.h().a("CrashlyticsCore", "No reports found.");
        }
        return linkedList;
    }

    public synchronized void a(float f2, d dVar) {
        if (this.h != null) {
            io.a.a.a.c.h().a("CrashlyticsCore", "Report upload has already been started.");
        } else {
            this.h = new Thread(new e(f2, dVar), "Crashlytics Report Uploader");
            this.h.start();
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean a(ae aeVar) {
        boolean z = false;
        synchronized (this.c) {
            try {
                boolean a2 = this.d.a(new n(this.e, aeVar));
                io.a.a.a.c.h().c("CrashlyticsCore", "Crashlytics report upload " + (a2 ? "complete: " : "FAILED: ") + aeVar.b());
                if (a2) {
                    aeVar.f();
                    z = true;
                }
            } catch (Exception e2) {
                io.a.a.a.c.h().e("CrashlyticsCore", "Error occurred sending report " + aeVar, e2);
            }
        }
        return z;
    }
}
