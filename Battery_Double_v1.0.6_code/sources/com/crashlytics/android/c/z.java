package com.crashlytics.android.c;

import java.io.IOException;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

class z {

    /* renamed from: a reason: collision with root package name */
    private static final com.crashlytics.android.c.a.a.e f1629a = new com.crashlytics.android.c.a.a.e("", "", 0);
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public static final j[] f1630b = new j[0];
    private static final m[] c = new m[0];
    private static final g[] d = new g[0];
    private static final b[] e = new b[0];
    private static final c[] f = new c[0];

    private static final class a extends j {
        public a(f fVar, k kVar) {
            super(3, fVar, kVar);
        }
    }

    private static final class b extends j {

        /* renamed from: a reason: collision with root package name */
        private final long f1631a;

        /* renamed from: b reason: collision with root package name */
        private final long f1632b;
        private final String c;
        private final String d;

        public b(com.crashlytics.android.c.a.a.a aVar) {
            super(4, new j[0]);
            this.f1631a = aVar.f1495a;
            this.f1632b = aVar.f1496b;
            this.c = aVar.c;
            this.d = aVar.d;
        }

        public int a() {
            int b2 = e.b(1, this.f1631a);
            return b2 + e.b(3, b.a(this.c)) + e.b(2, this.f1632b) + e.b(4, b.a(this.d));
        }

        public void a(e eVar) throws IOException {
            eVar.a(1, this.f1631a);
            eVar.a(2, this.f1632b);
            eVar.a(3, b.a(this.c));
            eVar.a(4, b.a(this.d));
        }
    }

    private static final class c extends j {

        /* renamed from: a reason: collision with root package name */
        private final String f1633a;

        /* renamed from: b reason: collision with root package name */
        private final String f1634b;

        public c(com.crashlytics.android.c.a.a.b bVar) {
            super(2, new j[0]);
            this.f1633a = bVar.f1497a;
            this.f1634b = bVar.f1498b;
        }

        public int a() {
            return e.b(2, b.a(this.f1634b == null ? "" : this.f1634b)) + e.b(1, b.a(this.f1633a));
        }

        public void a(e eVar) throws IOException {
            eVar.a(1, b.a(this.f1633a));
            eVar.a(2, b.a(this.f1634b == null ? "" : this.f1634b));
        }
    }

    private static final class d extends j {

        /* renamed from: a reason: collision with root package name */
        private final float f1635a;

        /* renamed from: b reason: collision with root package name */
        private final int f1636b;
        private final boolean c;
        private final int d;
        private final long e;
        private final long f;

        public d(float f2, int i, boolean z, int i2, long j, long j2) {
            super(5, new j[0]);
            this.f1635a = f2;
            this.f1636b = i;
            this.c = z;
            this.d = i2;
            this.e = j;
            this.f = j2;
        }

        public int a() {
            return 0 + e.b(1, this.f1635a) + e.f(2, this.f1636b) + e.b(3, this.c) + e.d(4, this.d) + e.b(5, this.e) + e.b(6, this.f);
        }

        public void a(e eVar) throws IOException {
            eVar.a(1, this.f1635a);
            eVar.c(2, this.f1636b);
            eVar.a(3, this.c);
            eVar.a(4, this.d);
            eVar.a(5, this.e);
            eVar.a(6, this.f);
        }
    }

    private static final class e extends j {

        /* renamed from: a reason: collision with root package name */
        private final long f1637a;

        /* renamed from: b reason: collision with root package name */
        private final String f1638b;

        public e(long j, String str, j... jVarArr) {
            super(10, jVarArr);
            this.f1637a = j;
            this.f1638b = str;
        }

        public int a() {
            return e.b(1, this.f1637a) + e.b(2, b.a(this.f1638b));
        }

        public void a(e eVar) throws IOException {
            eVar.a(1, this.f1637a);
            eVar.a(2, b.a(this.f1638b));
        }
    }

    private static final class f extends j {
        public f(l lVar, k kVar, k kVar2) {
            super(1, kVar, lVar, kVar2);
        }
    }

    private static final class g extends j {

        /* renamed from: a reason: collision with root package name */
        private final long f1639a;

        /* renamed from: b reason: collision with root package name */
        private final String f1640b;
        private final String c;
        private final long d;
        private final int e;

        public g(com.crashlytics.android.c.a.a.f.a aVar) {
            super(3, new j[0]);
            this.f1639a = aVar.f1507a;
            this.f1640b = aVar.f1508b;
            this.c = aVar.c;
            this.d = aVar.d;
            this.e = aVar.e;
        }

        public int a() {
            return e.b(1, this.f1639a) + e.b(2, b.a(this.f1640b)) + e.b(3, b.a(this.c)) + e.b(4, this.d) + e.d(5, this.e);
        }

        public void a(e eVar) throws IOException {
            eVar.a(1, this.f1639a);
            eVar.a(2, b.a(this.f1640b));
            eVar.a(3, b.a(this.c));
            eVar.a(4, this.d);
            eVar.a(5, this.e);
        }
    }

    private static final class h extends j {

        /* renamed from: a reason: collision with root package name */
        b f1641a;

        public h(b bVar) {
            super(6, new j[0]);
            this.f1641a = bVar;
        }

        public int a() {
            return e.b(1, this.f1641a);
        }

        public void a(e eVar) throws IOException {
            eVar.a(1, this.f1641a);
        }
    }

    private static final class i extends j {
        public i() {
            super(0, new j[0]);
        }

        public int b() {
            return 0;
        }

        public void b(e eVar) throws IOException {
        }
    }

    private static abstract class j {

        /* renamed from: a reason: collision with root package name */
        private final int f1642a;

        /* renamed from: b reason: collision with root package name */
        private final j[] f1643b;

        public j(int i, j... jVarArr) {
            this.f1642a = i;
            if (jVarArr == null) {
                jVarArr = z.f1630b;
            }
            this.f1643b = jVarArr;
        }

        public int a() {
            return 0;
        }

        public void a(e eVar) throws IOException {
        }

        public int b() {
            int c = c();
            return c + e.l(c) + e.j(this.f1642a);
        }

        public void b(e eVar) throws IOException {
            eVar.g(this.f1642a, 2);
            eVar.k(c());
            a(eVar);
            for (j b2 : this.f1643b) {
                b2.b(eVar);
            }
        }

        public int c() {
            int a2 = a();
            for (j b2 : this.f1643b) {
                a2 += b2.b();
            }
            return a2;
        }
    }

    private static final class k extends j {

        /* renamed from: a reason: collision with root package name */
        private final j[] f1644a;

        public k(j... jVarArr) {
            super(0, new j[0]);
            this.f1644a = jVarArr;
        }

        public int b() {
            int i = 0;
            for (j b2 : this.f1644a) {
                i += b2.b();
            }
            return i;
        }

        public void b(e eVar) throws IOException {
            for (j b2 : this.f1644a) {
                b2.b(eVar);
            }
        }
    }

    private static final class l extends j {

        /* renamed from: a reason: collision with root package name */
        private final String f1645a;

        /* renamed from: b reason: collision with root package name */
        private final String f1646b;
        private final long c;

        public l(com.crashlytics.android.c.a.a.e eVar) {
            super(3, new j[0]);
            this.f1645a = eVar.f1503a;
            this.f1646b = eVar.f1504b;
            this.c = eVar.c;
        }

        public int a() {
            return e.b(1, b.a(this.f1645a)) + e.b(2, b.a(this.f1646b)) + e.b(3, this.c);
        }

        public void a(e eVar) throws IOException {
            eVar.a(1, b.a(this.f1645a));
            eVar.a(2, b.a(this.f1646b));
            eVar.a(3, this.c);
        }
    }

    private static final class m extends j {

        /* renamed from: a reason: collision with root package name */
        private final String f1647a;

        /* renamed from: b reason: collision with root package name */
        private final int f1648b;

        public m(com.crashlytics.android.c.a.a.f fVar, k kVar) {
            super(1, kVar);
            this.f1647a = fVar.f1505a;
            this.f1648b = fVar.f1506b;
        }

        private boolean d() {
            return this.f1647a != null && this.f1647a.length() > 0;
        }

        public int a() {
            return (d() ? e.b(1, b.a(this.f1647a)) : 0) + e.d(2, this.f1648b);
        }

        public void a(e eVar) throws IOException {
            if (d()) {
                eVar.a(1, b.a(this.f1647a));
            }
            eVar.a(2, this.f1648b);
        }
    }

    private static e a(com.crashlytics.android.c.a.a.d dVar, u uVar, Map<String, String> map) throws IOException {
        a aVar = new a(new f(new l(dVar.f1502b != null ? dVar.f1502b : f1629a), a(dVar.c), a(dVar.d)), a(a(dVar.e, map)));
        j a2 = a(dVar.f);
        b a3 = uVar.a();
        if (a3 == null) {
            io.a.a.a.c.h().a("CrashlyticsCore", "No log data to include with this event.");
        }
        uVar.b();
        return new e(dVar.f1501a, "ndk-crash", aVar, a2, a3 != null ? new h(a3) : new i());
    }

    private static j a(com.crashlytics.android.c.a.a.c cVar) {
        return cVar == null ? new i() : new d(((float) cVar.f) / 100.0f, cVar.g, cVar.h, cVar.f1499a, cVar.f1500b - cVar.d, cVar.c - cVar.e);
    }

    private static k a(com.crashlytics.android.c.a.a.a[] aVarArr) {
        b[] bVarArr = aVarArr != null ? new b[aVarArr.length] : e;
        for (int i2 = 0; i2 < bVarArr.length; i2++) {
            bVarArr[i2] = new b(aVarArr[i2]);
        }
        return new k(bVarArr);
    }

    private static k a(com.crashlytics.android.c.a.a.b[] bVarArr) {
        c[] cVarArr = bVarArr != null ? new c[bVarArr.length] : f;
        for (int i2 = 0; i2 < cVarArr.length; i2++) {
            cVarArr[i2] = new c(bVarArr[i2]);
        }
        return new k(cVarArr);
    }

    private static k a(com.crashlytics.android.c.a.a.f.a[] aVarArr) {
        g[] gVarArr = aVarArr != null ? new g[aVarArr.length] : d;
        for (int i2 = 0; i2 < gVarArr.length; i2++) {
            gVarArr[i2] = new g(aVarArr[i2]);
        }
        return new k(gVarArr);
    }

    private static k a(com.crashlytics.android.c.a.a.f[] fVarArr) {
        m[] mVarArr = fVarArr != null ? new m[fVarArr.length] : c;
        for (int i2 = 0; i2 < mVarArr.length; i2++) {
            com.crashlytics.android.c.a.a.f fVar = fVarArr[i2];
            mVarArr[i2] = new m(fVar, a(fVar.c));
        }
        return new k(mVarArr);
    }

    public static void a(com.crashlytics.android.c.a.a.d dVar, u uVar, Map<String, String> map, e eVar) throws IOException {
        a(dVar, uVar, map).b(eVar);
    }

    private static com.crashlytics.android.c.a.a.b[] a(com.crashlytics.android.c.a.a.b[] bVarArr, Map<String, String> map) {
        int i2 = 0;
        TreeMap treeMap = new TreeMap(map);
        if (bVarArr != null) {
            for (com.crashlytics.android.c.a.a.b bVar : bVarArr) {
                treeMap.put(bVar.f1497a, bVar.f1498b);
            }
        }
        Entry[] entryArr = (Entry[]) treeMap.entrySet().toArray(new Entry[treeMap.size()]);
        com.crashlytics.android.c.a.a.b[] bVarArr2 = new com.crashlytics.android.c.a.a.b[entryArr.length];
        while (true) {
            int i3 = i2;
            if (i3 >= bVarArr2.length) {
                return bVarArr2;
            }
            bVarArr2[i3] = new com.crashlytics.android.c.a.a.b((String) entryArr[i3].getKey(), (String) entryArr[i3].getValue());
            i2 = i3 + 1;
        }
    }
}
