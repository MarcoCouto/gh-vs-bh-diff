package com.crashlytics.android.c;

import android.content.Context;
import android.os.Bundle;

class v implements ak {

    /* renamed from: a reason: collision with root package name */
    private final Context f1622a;

    /* renamed from: b reason: collision with root package name */
    private final String f1623b;

    public v(Context context, String str) {
        this.f1622a = context;
        this.f1623b = str;
    }

    public String a() {
        try {
            Bundle bundle = this.f1622a.getPackageManager().getApplicationInfo(this.f1623b, 128).metaData;
            if (bundle != null) {
                return bundle.getString("io.fabric.unity.crashlytics.version");
            }
            return null;
        } catch (Exception e) {
            return null;
        }
    }
}
