package com.crashlytics.android.c;

import android.content.Context;
import android.util.Log;
import io.a.a.a.a.b.n;
import io.a.a.a.a.b.o;
import io.a.a.a.a.b.p;
import io.a.a.a.a.c.d;
import io.a.a.a.a.c.g;
import io.a.a.a.a.c.l;
import io.a.a.a.a.c.m;
import io.a.a.a.a.e.e;
import io.a.a.a.a.g.q;
import io.a.a.a.a.g.t;
import io.a.a.a.c;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

@d(a = {com.crashlytics.android.c.a.a.class})
public class i extends io.a.a.a.i<Void> {

    /* renamed from: a reason: collision with root package name */
    private final long f1598a;

    /* renamed from: b reason: collision with root package name */
    private final ConcurrentHashMap<String, String> f1599b;
    /* access modifiers changed from: private */
    public j c;
    private j d;
    private k k;
    private h l;
    private String m;
    private String n;
    private String o;
    private float p;
    private boolean q;
    private final aa r;
    private e s;
    private g t;
    private com.crashlytics.android.c.a.a u;

    private static final class a implements Callable<Boolean> {

        /* renamed from: a reason: collision with root package name */
        private final j f1604a;

        public a(j jVar) {
            this.f1604a = jVar;
        }

        /* renamed from: a */
        public Boolean call() throws Exception {
            if (!this.f1604a.b()) {
                return Boolean.FALSE;
            }
            c.h().a("CrashlyticsCore", "Found previous crash marker.");
            this.f1604a.c();
            return Boolean.TRUE;
        }
    }

    private static final class b implements k {
        private b() {
        }

        public void a() {
        }
    }

    public i() {
        this(1.0f, null, null, false);
    }

    i(float f, k kVar, aa aaVar, boolean z) {
        this(f, kVar, aaVar, z, n.a("Crashlytics Exception Handler"));
    }

    i(float f, k kVar, aa aaVar, boolean z, ExecutorService executorService) {
        this.m = null;
        this.n = null;
        this.o = null;
        this.p = f;
        if (kVar == null) {
            kVar = new b();
        }
        this.k = kVar;
        this.r = aaVar;
        this.q = z;
        this.t = new g(executorService);
        this.f1599b = new ConcurrentHashMap<>();
        this.f1598a = System.currentTimeMillis();
    }

    static boolean a(String str, boolean z) {
        if (!z) {
            c.h().a("CrashlyticsCore", "Configured not to require a build ID.");
            return true;
        } else if (!io.a.a.a.a.b.i.d(str)) {
            return true;
        } else {
            Log.e("CrashlyticsCore", ".");
            Log.e("CrashlyticsCore", ".     |  | ");
            Log.e("CrashlyticsCore", ".     |  |");
            Log.e("CrashlyticsCore", ".     |  |");
            Log.e("CrashlyticsCore", ".   \\ |  | /");
            Log.e("CrashlyticsCore", ".    \\    /");
            Log.e("CrashlyticsCore", ".     \\  /");
            Log.e("CrashlyticsCore", ".      \\/");
            Log.e("CrashlyticsCore", ".");
            Log.e("CrashlyticsCore", "This app relies on Crashlytics. Please sign up for access at https://fabric.io/sign_up,\ninstall an Android build tool and ask a team member to invite you to this app's organization.");
            Log.e("CrashlyticsCore", ".");
            Log.e("CrashlyticsCore", ".      /\\");
            Log.e("CrashlyticsCore", ".     /  \\");
            Log.e("CrashlyticsCore", ".    /    \\");
            Log.e("CrashlyticsCore", ".   / |  | \\");
            Log.e("CrashlyticsCore", ".     |  |");
            Log.e("CrashlyticsCore", ".     |  |");
            Log.e("CrashlyticsCore", ".     |  |");
            Log.e("CrashlyticsCore", ".");
            return false;
        }
    }

    private void v() {
        AnonymousClass1 r1 = new g<Void>() {
            /* renamed from: a */
            public Void call() throws Exception {
                return i.this.e();
            }

            public io.a.a.a.a.c.e b() {
                return io.a.a.a.a.c.e.IMMEDIATE;
            }
        };
        for (l a2 : u()) {
            r1.a(a2);
        }
        Future submit = r().f().submit(r1);
        c.h().a("CrashlyticsCore", "Crashlytics detected incomplete initialization on previous app launch. Will initialize synchronously.");
        try {
            submit.get(4, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            c.h().e("CrashlyticsCore", "Crashlytics was interrupted during initialization.", e);
        } catch (ExecutionException e2) {
            c.h().e("CrashlyticsCore", "Problem encountered during Crashlytics initialization.", e2);
        } catch (TimeoutException e3) {
            c.h().e("CrashlyticsCore", "Crashlytics timed out during initialization.", e3);
        }
    }

    private void w() {
        if (Boolean.TRUE.equals((Boolean) this.t.a((Callable<T>) new a<T>(this.d)))) {
            try {
                this.k.a();
            } catch (Exception e) {
                c.h().e("CrashlyticsCore", "Exception thrown by CrashlyticsListener while notifying of previous crash.", e);
            }
        }
    }

    public String a() {
        return "2.4.1.19";
    }

    /* access modifiers changed from: 0000 */
    public boolean a(Context context) {
        if (this.q) {
            return false;
        }
        String a2 = new io.a.a.a.a.b.g().a(context);
        if (a2 == null) {
            return false;
        }
        String m2 = io.a.a.a.a.b.i.m(context);
        if (!a(m2, io.a.a.a.a.b.i.a(context, "com.crashlytics.RequireBuildId", true))) {
            throw new m("This app relies on Crashlytics. Please sign up for access at https://fabric.io/sign_up,\ninstall an Android build tool and ask a team member to invite you to this app's organization.");
        }
        try {
            c.h().c("CrashlyticsCore", "Initializing Crashlytics " + a());
            io.a.a.a.a.f.b bVar = new io.a.a.a.a.f.b(this);
            this.d = new j("crash_marker", bVar);
            this.c = new j("initialization_marker", bVar);
            ab a3 = ab.a(new io.a.a.a.a.f.d(q(), "com.crashlytics.android.core.CrashlyticsCore"), this);
            l lVar = this.r != null ? new l(this.r) : null;
            this.s = new io.a.a.a.a.e.b(c.h());
            this.s.a(lVar);
            p p2 = p();
            a a4 = a.a(context, p2, a2, m2);
            v vVar = new v(context, a4.d);
            c.h().a("CrashlyticsCore", "Installer package name is: " + a4.c);
            this.l = new h(this, this.t, this.s, p2, a3, bVar, a4, vVar, new o().b(context));
            boolean l2 = l();
            w();
            this.l.a(Thread.getDefaultUncaughtExceptionHandler());
            if (!l2 || !io.a.a.a.a.b.i.n(context)) {
                c.h().a("CrashlyticsCore", "Exception handling initialization successful");
                return true;
            }
            c.h().a("CrashlyticsCore", "Crashlytics did not finish previous background initialization. Initializing synchronously.");
            v();
            return false;
        } catch (Exception e) {
            c.h().e("CrashlyticsCore", "Crashlytics was not started due to an exception during initialization", e);
            this.l = null;
            return false;
        }
    }

    public String b() {
        return "com.crashlytics.sdk.android.crashlytics-core";
    }

    /* access modifiers changed from: protected */
    public boolean b_() {
        return a(super.q());
    }

    /* access modifiers changed from: protected */
    /* renamed from: c */
    public Void e() {
        j();
        com.crashlytics.android.c.a.a.d m2 = m();
        if (m2 != null) {
            this.l.a(m2);
        }
        this.l.d();
        try {
            t b2 = q.a().b();
            if (b2 == null) {
                c.h().d("CrashlyticsCore", "Received null settings, skipping report submission!");
            } else if (!b2.d.c) {
                c.h().a("CrashlyticsCore", "Collection of crash reports disabled in Crashlytics settings.");
                k();
            } else {
                if (!this.l.a(b2.f4496b)) {
                    c.h().a("CrashlyticsCore", "Could not finalize previous sessions.");
                }
                this.l.a(this.p, b2);
                k();
            }
        } catch (Exception e) {
            c.h().e("CrashlyticsCore", "Crashlytics encountered a problem during asynchronous initialization.", e);
        } finally {
            k();
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    public Map<String, String> f() {
        return Collections.unmodifiableMap(this.f1599b);
    }

    /* access modifiers changed from: 0000 */
    public String g() {
        if (p().a()) {
            return this.m;
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    public String h() {
        if (p().a()) {
            return this.n;
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    public String i() {
        if (p().a()) {
            return this.o;
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    public void j() {
        this.t.a((Callable<T>) new Callable<Void>() {
            /* renamed from: a */
            public Void call() throws Exception {
                i.this.c.a();
                c.h().a("CrashlyticsCore", "Initialization marker file created.");
                return null;
            }
        });
    }

    /* access modifiers changed from: 0000 */
    public void k() {
        this.t.b(new Callable<Boolean>() {
            /* renamed from: a */
            public Boolean call() throws Exception {
                try {
                    boolean c = i.this.c.c();
                    c.h().a("CrashlyticsCore", "Initialization marker file removed: " + c);
                    return Boolean.valueOf(c);
                } catch (Exception e) {
                    c.h().e("CrashlyticsCore", "Problem encountered deleting Crashlytics initialization marker.", e);
                    return Boolean.valueOf(false);
                }
            }
        });
    }

    /* access modifiers changed from: 0000 */
    public boolean l() {
        return ((Boolean) this.t.a((Callable<T>) new Callable<Boolean>() {
            /* renamed from: a */
            public Boolean call() throws Exception {
                return Boolean.valueOf(i.this.c.b());
            }
        })).booleanValue();
    }

    /* access modifiers changed from: 0000 */
    public com.crashlytics.android.c.a.a.d m() {
        if (this.u != null) {
            return this.u.a();
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    public void n() {
        this.d.a();
    }
}
