package com.crashlytics.android.c;

import java.util.HashMap;

class ad implements ai {

    /* renamed from: a reason: collision with root package name */
    private final int f1514a;

    public ad() {
        this(1);
    }

    public ad(int i) {
        this.f1514a = i;
    }

    private static boolean a(StackTraceElement[] stackTraceElementArr, int i, int i2) {
        int i3 = i2 - i;
        if (i2 + i3 > stackTraceElementArr.length) {
            return false;
        }
        for (int i4 = 0; i4 < i3; i4++) {
            if (!stackTraceElementArr[i + i4].equals(stackTraceElementArr[i2 + i4])) {
                return false;
            }
        }
        return true;
    }

    private static StackTraceElement[] a(StackTraceElement[] stackTraceElementArr, int i) {
        int i2;
        HashMap hashMap = new HashMap();
        StackTraceElement[] stackTraceElementArr2 = new StackTraceElement[stackTraceElementArr.length];
        int i3 = 0;
        int i4 = 1;
        int i5 = 0;
        while (i3 < stackTraceElementArr.length) {
            StackTraceElement stackTraceElement = stackTraceElementArr[i3];
            Integer num = (Integer) hashMap.get(stackTraceElement);
            if (num == null || !a(stackTraceElementArr, num.intValue(), i3)) {
                stackTraceElementArr2[i5] = stackTraceElementArr[i3];
                i5++;
                i2 = i3;
                i4 = 1;
            } else {
                int intValue = i3 - num.intValue();
                if (i4 < i) {
                    System.arraycopy(stackTraceElementArr, i3, stackTraceElementArr2, i5, intValue);
                    i5 += intValue;
                    i4++;
                }
                i2 = (intValue - 1) + i3;
            }
            hashMap.put(stackTraceElement, Integer.valueOf(i3));
            i3 = i2 + 1;
        }
        StackTraceElement[] stackTraceElementArr3 = new StackTraceElement[i5];
        System.arraycopy(stackTraceElementArr2, 0, stackTraceElementArr3, 0, stackTraceElementArr3.length);
        return stackTraceElementArr3;
    }

    public StackTraceElement[] a(StackTraceElement[] stackTraceElementArr) {
        StackTraceElement[] a2 = a(stackTraceElementArr, this.f1514a);
        return a2.length < stackTraceElementArr.length ? a2 : stackTraceElementArr;
    }
}
