package com.crashlytics.android.c;

import io.a.a.a.a.b.i;
import io.a.a.a.a.b.r;
import io.a.a.a.c;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

class ac implements s {

    /* renamed from: a reason: collision with root package name */
    private final File f1510a;

    /* renamed from: b reason: collision with root package name */
    private final int f1511b;
    private r c;

    public ac(File file, int i) {
        this.f1510a = file;
        this.f1511b = i;
    }

    private void d() {
        if (this.c == null) {
            try {
                this.c = new r(this.f1510a);
            } catch (IOException e) {
                c.h().e("CrashlyticsCore", "Could not open log file: " + this.f1510a, e);
            }
        }
    }

    public b a() {
        if (!this.f1510a.exists()) {
            return null;
        }
        d();
        if (this.c == null) {
            return null;
        }
        final int[] iArr = {0};
        final byte[] bArr = new byte[this.c.a()];
        try {
            this.c.a((r.c) new r.c() {
                public void a(InputStream inputStream, int i) throws IOException {
                    try {
                        inputStream.read(bArr, iArr[0], i);
                        int[] iArr = iArr;
                        iArr[0] = iArr[0] + i;
                    } finally {
                        inputStream.close();
                    }
                }
            });
        } catch (IOException e) {
            c.h().e("CrashlyticsCore", "A problem occurred while reading the Crashlytics log file.", e);
        }
        return b.a(bArr, 0, iArr[0]);
    }

    public void b() {
        i.a((Closeable) this.c, "There was a problem closing the Crashlytics log file.");
        this.c = null;
    }

    public void c() {
        b();
        this.f1510a.delete();
    }
}
