package com.crashlytics.android.c;

import android.app.Activity;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.Context;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Environment;
import android.os.StatFs;
import android.text.TextUtils;
import com.crashlytics.android.a.r;
import com.google.android.gms.measurement.AppMeasurement;
import io.a.a.a.a.b.l;
import io.a.a.a.a.b.p;
import io.a.a.a.a.g.o;
import io.a.a.a.a.g.q;
import io.a.a.a.a.g.t;
import java.io.Closeable;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.Flushable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Thread.UncaughtExceptionHandler;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.json.JSONObject;

class h {

    /* renamed from: a reason: collision with root package name */
    static final FilenameFilter f1551a = new c("BeginSession") {
        public boolean accept(File file, String str) {
            return super.accept(file, str) && str.endsWith(".cls");
        }
    };

    /* renamed from: b reason: collision with root package name */
    static final FilenameFilter f1552b = new FilenameFilter() {
        public boolean accept(File file, String str) {
            return str.length() == ".cls".length() + 35 && str.endsWith(".cls");
        }
    };
    static final FileFilter c = new FileFilter() {
        public boolean accept(File file) {
            return file.isDirectory() && file.getName().length() == 35;
        }
    };
    static final Comparator<File> d = new Comparator<File>() {
        /* renamed from: a */
        public int compare(File file, File file2) {
            return file2.getName().compareTo(file.getName());
        }
    };
    static final Comparator<File> e = new Comparator<File>() {
        /* renamed from: a */
        public int compare(File file, File file2) {
            return file.getName().compareTo(file2.getName());
        }
    };
    /* access modifiers changed from: private */
    public static final Pattern f = Pattern.compile("([\\d|A-Z|a-z]{12}\\-[\\d|A-Z|a-z]{4}\\-[\\d|A-Z|a-z]{4}\\-[\\d|A-Z|a-z]{12}).+");
    private static final Map<String, String> g = Collections.singletonMap("X-CRASHLYTICS-SEND-FLAGS", "1");
    private static final String[] h = {"SessionUser", "SessionApp", "SessionOS", "SessionDevice"};
    private final AtomicInteger i = new AtomicInteger(0);
    /* access modifiers changed from: private */
    public final i j;
    private final g k;
    private final io.a.a.a.a.e.e l;
    private final p m;
    private final ab n;
    private final io.a.a.a.a.f.a o;
    /* access modifiers changed from: private */
    public final a p;
    private final f q;
    private final u r;
    private final c s;
    private final b t;
    private final q u;
    private final ai v;
    /* access modifiers changed from: private */
    public final String w;
    private final r x;
    private final boolean y;
    private m z;

    private static class a implements FilenameFilter {
        private a() {
        }

        public boolean accept(File file, String str) {
            return !h.f1552b.accept(file, str) && h.f.matcher(str).matches();
        }
    }

    private interface b {
        void a(e eVar) throws Exception;
    }

    static class c implements FilenameFilter {

        /* renamed from: a reason: collision with root package name */
        private final String f1586a;

        public c(String str) {
            this.f1586a = str;
        }

        public boolean accept(File file, String str) {
            return str.contains(this.f1586a) && !str.endsWith(".cls_temp");
        }
    }

    private interface d {
        void a(FileOutputStream fileOutputStream) throws Exception;
    }

    static class e implements FilenameFilter {
        e() {
        }

        public boolean accept(File file, String str) {
            return d.f1534a.accept(file, str) || str.contains("SessionMissingBinaryImages");
        }
    }

    private static final class f implements com.crashlytics.android.c.u.a {

        /* renamed from: a reason: collision with root package name */
        private final io.a.a.a.a.f.a f1587a;

        public f(io.a.a.a.a.f.a aVar) {
            this.f1587a = aVar;
        }

        public File a() {
            File file = new File(this.f1587a.a(), "log-files");
            if (!file.exists()) {
                file.mkdirs();
            }
            return file;
        }
    }

    private static final class g implements d {

        /* renamed from: a reason: collision with root package name */
        private final io.a.a.a.i f1588a;
        /* access modifiers changed from: private */

        /* renamed from: b reason: collision with root package name */
        public final ab f1589b;
        private final o c;

        public g(io.a.a.a.i iVar, ab abVar, o oVar) {
            this.f1588a = iVar;
            this.f1589b = abVar;
            this.c = oVar;
        }

        public boolean a() {
            Activity b2 = this.f1588a.r().b();
            if (b2 == null || b2.isFinishing()) {
                return true;
            }
            final f a2 = f.a(b2, this.c, new a() {
                public void a(boolean z) {
                    g.this.f1589b.a(z);
                }
            });
            b2.runOnUiThread(new Runnable() {
                public void run() {
                    a2.a();
                }
            });
            io.a.a.a.c.h().a("CrashlyticsCore", "Waiting for user opt-in.");
            a2.b();
            return a2.c();
        }
    }

    /* renamed from: com.crashlytics.android.c.h$h reason: collision with other inner class name */
    private final class C0039h implements c {
        private C0039h() {
        }

        public File[] a() {
            return h.this.b();
        }

        public File[] b() {
            return h.this.i().listFiles();
        }
    }

    private final class i implements b {
        private i() {
        }

        public boolean a() {
            return h.this.e();
        }
    }

    private static final class j implements Runnable {

        /* renamed from: a reason: collision with root package name */
        private final Context f1595a;

        /* renamed from: b reason: collision with root package name */
        private final ae f1596b;
        private final af c;

        public j(Context context, ae aeVar, af afVar) {
            this.f1595a = context;
            this.f1596b = aeVar;
            this.c = afVar;
        }

        public void run() {
            if (io.a.a.a.a.b.i.n(this.f1595a)) {
                io.a.a.a.c.h().a("CrashlyticsCore", "Attempting to send crash report at time of crash...");
                this.c.a(this.f1596b);
            }
        }
    }

    static class k implements FilenameFilter {

        /* renamed from: a reason: collision with root package name */
        private final String f1597a;

        public k(String str) {
            this.f1597a = str;
        }

        public boolean accept(File file, String str) {
            return !str.equals(new StringBuilder().append(this.f1597a).append(".cls").toString()) && str.contains(this.f1597a) && !str.endsWith(".cls_temp");
        }
    }

    h(i iVar, g gVar, io.a.a.a.a.e.e eVar, p pVar, ab abVar, io.a.a.a.a.f.a aVar, a aVar2, ak akVar, boolean z2) {
        this.j = iVar;
        this.k = gVar;
        this.l = eVar;
        this.m = pVar;
        this.n = abVar;
        this.o = aVar;
        this.p = aVar2;
        this.w = akVar.a();
        this.y = z2;
        Context q2 = iVar.q();
        this.q = new f(aVar);
        this.r = new u(q2, this.q);
        this.s = new C0039h();
        this.t = new i();
        this.u = new q(q2);
        this.v = new x(1024, new ad(10));
        this.x = com.crashlytics.android.a.k.a(q2);
    }

    static String a(File file) {
        return file.getName().substring(0, 35);
    }

    private void a(long j2) {
        if (p()) {
            io.a.a.a.c.h().a("CrashlyticsCore", "Skipping logging Crashlytics event to Firebase, FirebaseCrash exists");
        } else if (!this.y) {
        } else {
            if (this.x != null) {
                io.a.a.a.c.h().a("CrashlyticsCore", "Logging Crashlytics event to Firebase");
                Bundle bundle = new Bundle();
                bundle.putInt("_r", 1);
                bundle.putInt("fatal", 1);
                bundle.putLong("timestamp", j2);
                this.x.a("clx", "_ae", bundle);
                return;
            }
            io.a.a.a.c.h().a("CrashlyticsCore", "Skipping logging Crashlytics event to Firebase, no Firebase Analytics");
        }
    }

    private void a(d dVar) {
        if (dVar != null) {
            try {
                dVar.a();
            } catch (IOException e2) {
                io.a.a.a.c.h().e("CrashlyticsCore", "Error closing session file stream in the presence of an exception", e2);
            }
        }
    }

    private static void a(e eVar, File file) throws IOException {
        FileInputStream fileInputStream;
        if (!file.exists()) {
            io.a.a.a.c.h().e("CrashlyticsCore", "Tried to include a file that doesn't exist: " + file.getName(), null);
            return;
        }
        try {
            fileInputStream = new FileInputStream(file);
            try {
                a((InputStream) fileInputStream, eVar, (int) file.length());
                io.a.a.a.a.b.i.a((Closeable) fileInputStream, "Failed to close file input stream.");
            } catch (Throwable th) {
                th = th;
                io.a.a.a.a.b.i.a((Closeable) fileInputStream, "Failed to close file input stream.");
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            fileInputStream = null;
            io.a.a.a.a.b.i.a((Closeable) fileInputStream, "Failed to close file input stream.");
            throw th;
        }
    }

    private void a(e eVar, String str) throws IOException {
        String[] strArr;
        for (String str2 : h) {
            File[] a2 = a((FilenameFilter) new c(str + str2 + ".cls"));
            if (a2.length == 0) {
                io.a.a.a.c.h().e("CrashlyticsCore", "Can't find " + str2 + " data for session ID " + str, null);
            } else {
                io.a.a.a.c.h().a("CrashlyticsCore", "Collecting " + str2 + " data for session ID " + str);
                a(eVar, a2[0]);
            }
        }
    }

    private void a(e eVar, Date date, Thread thread, Throwable th, String str, boolean z2) throws Exception {
        Thread[] threadArr;
        Map treeMap;
        aj ajVar = new aj(th, this.v);
        Context q2 = this.j.q();
        long time = date.getTime() / 1000;
        Float c2 = io.a.a.a.a.b.i.c(q2);
        int a2 = io.a.a.a.a.b.i.a(q2, this.u.a());
        boolean d2 = io.a.a.a.a.b.i.d(q2);
        int i2 = q2.getResources().getConfiguration().orientation;
        long b2 = io.a.a.a.a.b.i.b() - io.a.a.a.a.b.i.b(q2);
        long c3 = io.a.a.a.a.b.i.c(Environment.getDataDirectory().getPath());
        RunningAppProcessInfo a3 = io.a.a.a.a.b.i.a(q2.getPackageName(), q2);
        LinkedList linkedList = new LinkedList();
        StackTraceElement[] stackTraceElementArr = ajVar.c;
        String str2 = this.p.f1494b;
        String c4 = this.m.c();
        if (z2) {
            Map allStackTraces = Thread.getAllStackTraces();
            threadArr = new Thread[allStackTraces.size()];
            int i3 = 0;
            Iterator it = allStackTraces.entrySet().iterator();
            while (true) {
                int i4 = i3;
                if (!it.hasNext()) {
                    break;
                }
                Entry entry = (Entry) it.next();
                threadArr[i4] = (Thread) entry.getKey();
                linkedList.add(this.v.a((StackTraceElement[]) entry.getValue()));
                i3 = i4 + 1;
            }
        } else {
            threadArr = new Thread[0];
        }
        if (!io.a.a.a.a.b.i.a(q2, "com.crashlytics.CollectCustomKeys", true)) {
            treeMap = new TreeMap();
        } else {
            Map f2 = this.j.f();
            treeMap = (f2 == null || f2.size() <= 1) ? f2 : new TreeMap(f2);
        }
        ag.a(eVar, time, str, ajVar, thread, stackTraceElementArr, threadArr, (List<StackTraceElement[]>) linkedList, treeMap, this.r, a3, i2, c4, str2, c2, a2, d2, b2, c3);
    }

    private static void a(e eVar, File[] fileArr, String str) {
        Arrays.sort(fileArr, io.a.a.a.a.b.i.f4374a);
        for (File file : fileArr) {
            try {
                io.a.a.a.c.h().a("CrashlyticsCore", String.format(Locale.US, "Found Non Fatal for session ID %s in %s ", new Object[]{str, file.getName()}));
                a(eVar, file);
            } catch (Exception e2) {
                io.a.a.a.c.h().e("CrashlyticsCore", "Error writting non-fatal to session.", e2);
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(io.a.a.a.a.g.p pVar, boolean z2) throws Exception {
        int i2 = z2 ? 1 : 0;
        b(i2 + 8);
        File[] n2 = n();
        if (n2.length <= i2) {
            io.a.a.a.c.h().a("CrashlyticsCore", "No open sessions to be closed.");
            return;
        }
        f(a(n2[i2]));
        if (pVar == null) {
            io.a.a.a.c.h().a("CrashlyticsCore", "Unable to close session. Settings are not loaded.");
        } else {
            a(n2, i2, pVar.c);
        }
    }

    private void a(File file, String str, int i2) {
        io.a.a.a.c.h().a("CrashlyticsCore", "Collecting session parts for ID " + str);
        File[] a2 = a((FilenameFilter) new c(str + "SessionCrash"));
        boolean z2 = a2 != null && a2.length > 0;
        io.a.a.a.c.h().a("CrashlyticsCore", String.format(Locale.US, "Session %s has fatal exception: %s", new Object[]{str, Boolean.valueOf(z2)}));
        File[] a3 = a((FilenameFilter) new c(str + "SessionEvent"));
        boolean z3 = a3 != null && a3.length > 0;
        io.a.a.a.c.h().a("CrashlyticsCore", String.format(Locale.US, "Session %s has non-fatal exceptions: %s", new Object[]{str, Boolean.valueOf(z3)}));
        if (z2 || z3) {
            a(file, str, a(str, a3, i2), z2 ? a2[0] : null);
        } else {
            io.a.a.a.c.h().a("CrashlyticsCore", "No events present for session ID " + str);
        }
        io.a.a.a.c.h().a("CrashlyticsCore", "Removing session part files for ID " + str);
        a(str);
    }

    private void a(File file, String str, File[] fileArr, File file2) {
        d dVar;
        boolean z2 = file2 != null;
        File h2 = z2 ? g() : h();
        if (!h2.exists()) {
            h2.mkdirs();
        }
        try {
            dVar = new d(h2, str);
            try {
                e a2 = e.a((OutputStream) dVar);
                io.a.a.a.c.h().a("CrashlyticsCore", "Collecting SessionStart data for session ID " + str);
                a(a2, file);
                a2.a(4, new Date().getTime() / 1000);
                a2.a(5, z2);
                a2.a(11, 1);
                a2.b(12, 3);
                a(a2, str);
                a(a2, fileArr, str);
                if (z2) {
                    a(a2, file2);
                }
                io.a.a.a.a.b.i.a((Flushable) a2, "Error flushing session file stream");
                io.a.a.a.a.b.i.a((Closeable) dVar, "Failed to close CLS file");
            } catch (Exception e2) {
                e = e2;
                try {
                    io.a.a.a.c.h().e("CrashlyticsCore", "Failed to write session file for session ID: " + str, e);
                    io.a.a.a.a.b.i.a((Flushable) null, "Error flushing session file stream");
                    a(dVar);
                } catch (Throwable th) {
                    th = th;
                    io.a.a.a.a.b.i.a((Flushable) null, "Error flushing session file stream");
                    io.a.a.a.a.b.i.a((Closeable) dVar, "Failed to close CLS file");
                    throw th;
                }
            }
        } catch (Exception e3) {
            e = e3;
            dVar = null;
            io.a.a.a.c.h().e("CrashlyticsCore", "Failed to write session file for session ID: " + str, e);
            io.a.a.a.a.b.i.a((Flushable) null, "Error flushing session file stream");
            a(dVar);
        } catch (Throwable th2) {
            th = th2;
            dVar = null;
            io.a.a.a.a.b.i.a((Flushable) null, "Error flushing session file stream");
            io.a.a.a.a.b.i.a((Closeable) dVar, "Failed to close CLS file");
            throw th;
        }
    }

    private static void a(InputStream inputStream, e eVar, int i2) throws IOException {
        byte[] bArr = new byte[i2];
        int i3 = 0;
        while (i3 < bArr.length) {
            int read = inputStream.read(bArr, i3, bArr.length - i3);
            if (read < 0) {
                break;
            }
            i3 += read;
        }
        eVar.a(bArr);
    }

    private void a(String str) {
        for (File delete : b(str)) {
            delete.delete();
        }
    }

    private void a(String str, int i2) {
        am.a(f(), new c(str + "SessionEvent"), i2, e);
    }

    private static void a(String str, String str2) {
        com.crashlytics.android.a.b bVar = (com.crashlytics.android.a.b) io.a.a.a.c.a(com.crashlytics.android.a.b.class);
        if (bVar == null) {
            io.a.a.a.c.h().a("CrashlyticsCore", "Answers is not available");
        } else {
            bVar.a(new io.a.a.a.a.b.j.a(str, str2));
        }
    }

    private void a(String str, String str2, b bVar) throws Exception {
        d dVar;
        e eVar = null;
        try {
            dVar = new d(f(), str + str2);
            try {
                eVar = e.a((OutputStream) dVar);
                bVar.a(eVar);
                io.a.a.a.a.b.i.a((Flushable) eVar, "Failed to flush to session " + str2 + " file.");
                io.a.a.a.a.b.i.a((Closeable) dVar, "Failed to close session " + str2 + " file.");
            } catch (Throwable th) {
                th = th;
                io.a.a.a.a.b.i.a((Flushable) eVar, "Failed to flush to session " + str2 + " file.");
                io.a.a.a.a.b.i.a((Closeable) dVar, "Failed to close session " + str2 + " file.");
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            dVar = null;
            io.a.a.a.a.b.i.a((Flushable) eVar, "Failed to flush to session " + str2 + " file.");
            io.a.a.a.a.b.i.a((Closeable) dVar, "Failed to close session " + str2 + " file.");
            throw th;
        }
    }

    private void a(String str, String str2, d dVar) throws Exception {
        FileOutputStream fileOutputStream;
        try {
            fileOutputStream = new FileOutputStream(new File(f(), str + str2));
            try {
                dVar.a(fileOutputStream);
                io.a.a.a.a.b.i.a((Closeable) fileOutputStream, "Failed to close " + str2 + " file.");
            } catch (Throwable th) {
                th = th;
                io.a.a.a.a.b.i.a((Closeable) fileOutputStream, "Failed to close " + str2 + " file.");
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            fileOutputStream = null;
            io.a.a.a.a.b.i.a((Closeable) fileOutputStream, "Failed to close " + str2 + " file.");
            throw th;
        }
    }

    private void a(String str, Date date) throws Exception {
        final String format = String.format(Locale.US, "Crashlytics Android SDK/%s", new Object[]{this.j.a()});
        final long time = date.getTime() / 1000;
        final String str2 = str;
        a(str, "BeginSession", (b) new b() {
            public void a(e eVar) throws Exception {
                ag.a(eVar, str2, format, time);
            }
        });
        final String str3 = str;
        a(str, "BeginSession.json", (d) new d() {
            public void a(FileOutputStream fileOutputStream) throws Exception {
                fileOutputStream.write(new JSONObject(new HashMap<String, Object>() {
                    {
                        put("session_id", str3);
                        put("generator", format);
                        put("started_at_seconds", Long.valueOf(time));
                    }
                }).toString().getBytes());
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(Date date, Thread thread, Throwable th) {
        d dVar;
        e eVar = null;
        try {
            String k2 = k();
            if (k2 == null) {
                io.a.a.a.c.h().e("CrashlyticsCore", "Tried to write a fatal exception while no session was open.", null);
                io.a.a.a.a.b.i.a((Flushable) null, "Failed to flush to session begin file.");
                io.a.a.a.a.b.i.a((Closeable) null, "Failed to close fatal exception file output stream.");
                return;
            }
            a(k2, th.getClass().getName());
            a(date.getTime());
            d dVar2 = new d(f(), k2 + "SessionCrash");
            try {
                eVar = e.a((OutputStream) dVar2);
                a(eVar, date, thread, th, AppMeasurement.CRASH_ORIGIN, true);
                io.a.a.a.a.b.i.a((Flushable) eVar, "Failed to flush to session begin file.");
                io.a.a.a.a.b.i.a((Closeable) dVar2, "Failed to close fatal exception file output stream.");
            } catch (Exception e2) {
                e = e2;
                dVar = dVar2;
                try {
                    io.a.a.a.c.h().e("CrashlyticsCore", "An error occurred in the fatal exception logger", e);
                    io.a.a.a.a.b.i.a((Flushable) eVar, "Failed to flush to session begin file.");
                    io.a.a.a.a.b.i.a((Closeable) dVar, "Failed to close fatal exception file output stream.");
                } catch (Throwable th2) {
                    th = th2;
                    io.a.a.a.a.b.i.a((Flushable) eVar, "Failed to flush to session begin file.");
                    io.a.a.a.a.b.i.a((Closeable) dVar, "Failed to close fatal exception file output stream.");
                    throw th;
                }
            } catch (Throwable th3) {
                th = th3;
                dVar = dVar2;
                io.a.a.a.a.b.i.a((Flushable) eVar, "Failed to flush to session begin file.");
                io.a.a.a.a.b.i.a((Closeable) dVar, "Failed to close fatal exception file output stream.");
                throw th;
            }
        } catch (Exception e3) {
            e = e3;
            dVar = null;
            io.a.a.a.c.h().e("CrashlyticsCore", "An error occurred in the fatal exception logger", e);
            io.a.a.a.a.b.i.a((Flushable) eVar, "Failed to flush to session begin file.");
            io.a.a.a.a.b.i.a((Closeable) dVar, "Failed to close fatal exception file output stream.");
        } catch (Throwable th4) {
            th = th4;
            dVar = null;
            io.a.a.a.a.b.i.a((Flushable) eVar, "Failed to flush to session begin file.");
            io.a.a.a.a.b.i.a((Closeable) dVar, "Failed to close fatal exception file output stream.");
            throw th;
        }
    }

    private void a(File[] fileArr, int i2, int i3) {
        io.a.a.a.c.h().a("CrashlyticsCore", "Closing open sessions.");
        while (i2 < fileArr.length) {
            File file = fileArr[i2];
            String a2 = a(file);
            io.a.a.a.c.h().a("CrashlyticsCore", "Closing session: " + a2);
            a(file, a2, i3);
            i2++;
        }
    }

    private void a(File[] fileArr, Set<String> set) {
        for (File file : fileArr) {
            String name = file.getName();
            Matcher matcher = f.matcher(name);
            if (!matcher.matches()) {
                io.a.a.a.c.h().a("CrashlyticsCore", "Deleting unknown file: " + name);
                file.delete();
            } else if (!set.contains(matcher.group(1))) {
                io.a.a.a.c.h().a("CrashlyticsCore", "Trimming session file: " + name);
                file.delete();
            }
        }
    }

    /* access modifiers changed from: private */
    public boolean a(t tVar) {
        return tVar != null && tVar.d.f4482a && !this.n.a();
    }

    private File[] a(File file, FilenameFilter filenameFilter) {
        return b(file.listFiles(filenameFilter));
    }

    /* access modifiers changed from: private */
    public File[] a(FilenameFilter filenameFilter) {
        return a(f(), filenameFilter);
    }

    private File[] a(String str, File[] fileArr, int i2) {
        if (fileArr.length <= i2) {
            return fileArr;
        }
        io.a.a.a.c.h().a("CrashlyticsCore", String.format(Locale.US, "Trimming down to %d logged exceptions.", new Object[]{Integer.valueOf(i2)}));
        a(str, i2);
        return a((FilenameFilter) new c(str + "SessionEvent"));
    }

    private void b(int i2) {
        HashSet hashSet = new HashSet();
        File[] n2 = n();
        int min = Math.min(i2, n2.length);
        for (int i3 = 0; i3 < min; i3++) {
            hashSet.add(a(n2[i3]));
        }
        this.r.a((Set<String>) hashSet);
        a(a((FilenameFilter) new a()), (Set<String>) hashSet);
    }

    /* access modifiers changed from: private */
    public void b(com.crashlytics.android.c.a.a.d dVar) throws IOException {
        d dVar2;
        boolean z2 = true;
        e eVar = null;
        try {
            String l2 = l();
            if (l2 == null) {
                io.a.a.a.c.h().e("CrashlyticsCore", "Tried to write a native crash while no session was open.", null);
                io.a.a.a.a.b.i.a((Flushable) null, "Failed to flush to session begin file.");
                io.a.a.a.a.b.i.a((Closeable) null, "Failed to close fatal exception file output stream.");
                return;
            }
            a(l2, String.format(Locale.US, "<native-crash [%s (%s)]>", new Object[]{dVar.f1502b.f1504b, dVar.f1502b.f1503a}));
            if (dVar.d == null || dVar.d.length <= 0) {
                z2 = false;
            }
            dVar2 = new d(f(), l2 + (z2 ? "SessionCrash" : "SessionMissingBinaryImages"));
            try {
                eVar = e.a((OutputStream) dVar2);
                z.a(dVar, new u(this.j.q(), this.q, l2), new w(f()).b(l2), eVar);
                io.a.a.a.a.b.i.a((Flushable) eVar, "Failed to flush to session begin file.");
                io.a.a.a.a.b.i.a((Closeable) dVar2, "Failed to close fatal exception file output stream.");
            } catch (Exception e2) {
                e = e2;
                try {
                    io.a.a.a.c.h().e("CrashlyticsCore", "An error occurred in the native crash logger", e);
                    io.a.a.a.a.b.i.a((Flushable) eVar, "Failed to flush to session begin file.");
                    io.a.a.a.a.b.i.a((Closeable) dVar2, "Failed to close fatal exception file output stream.");
                } catch (Throwable th) {
                    th = th;
                    io.a.a.a.a.b.i.a((Flushable) eVar, "Failed to flush to session begin file.");
                    io.a.a.a.a.b.i.a((Closeable) dVar2, "Failed to close fatal exception file output stream.");
                    throw th;
                }
            }
        } catch (Exception e3) {
            e = e3;
            dVar2 = null;
            io.a.a.a.c.h().e("CrashlyticsCore", "An error occurred in the native crash logger", e);
            io.a.a.a.a.b.i.a((Flushable) eVar, "Failed to flush to session begin file.");
            io.a.a.a.a.b.i.a((Closeable) dVar2, "Failed to close fatal exception file output stream.");
        } catch (Throwable th2) {
            th = th2;
            dVar2 = null;
            io.a.a.a.a.b.i.a((Flushable) eVar, "Failed to flush to session begin file.");
            io.a.a.a.a.b.i.a((Closeable) dVar2, "Failed to close fatal exception file output stream.");
            throw th;
        }
    }

    /* access modifiers changed from: private */
    public void b(t tVar) {
        if (tVar == null) {
            io.a.a.a.c.h().d("CrashlyticsCore", "Cannot send reports. Settings are unavailable.");
            return;
        }
        Context q2 = this.j.q();
        af afVar = new af(this.p.f1493a, h(tVar.f4495a.d), this.s, this.t);
        for (File ahVar : b()) {
            this.k.a((Runnable) new j(q2, new ah(ahVar, g), afVar));
        }
    }

    private File[] b(File file) {
        return b(file.listFiles());
    }

    private File[] b(String str) {
        return a((FilenameFilter) new k(str));
    }

    private File[] b(File[] fileArr) {
        return fileArr == null ? new File[0] : fileArr;
    }

    private void c(String str) throws Exception {
        final String c2 = this.m.c();
        final String str2 = this.p.e;
        final String str3 = this.p.f;
        final String b2 = this.m.b();
        final int a2 = l.a(this.p.c).a();
        a(str, "SessionApp", (b) new b() {
            public void a(e eVar) throws Exception {
                ag.a(eVar, c2, h.this.p.f1493a, str2, str3, b2, a2, h.this.w);
            }
        });
        a(str, "SessionApp.json", (d) new d() {
            public void a(FileOutputStream fileOutputStream) throws Exception {
                fileOutputStream.write(new JSONObject(new HashMap<String, Object>() {
                    {
                        put("app_identifier", c2);
                        put("api_key", h.this.p.f1493a);
                        put("version_code", str2);
                        put("version_name", str3);
                        put("install_uuid", b2);
                        put("delivery_mechanism", Integer.valueOf(a2));
                        put("unity_version", TextUtils.isEmpty(h.this.w) ? "" : h.this.w);
                    }
                }).toString().getBytes());
            }
        });
    }

    private void d(String str) throws Exception {
        final boolean g2 = io.a.a.a.a.b.i.g(this.j.q());
        a(str, "SessionOS", (b) new b() {
            public void a(e eVar) throws Exception {
                ag.a(eVar, VERSION.RELEASE, VERSION.CODENAME, g2);
            }
        });
        a(str, "SessionOS.json", (d) new d() {
            public void a(FileOutputStream fileOutputStream) throws Exception {
                fileOutputStream.write(new JSONObject(new HashMap<String, Object>() {
                    {
                        put("version", VERSION.RELEASE);
                        put("build_version", VERSION.CODENAME);
                        put("is_rooted", Boolean.valueOf(g2));
                    }
                }).toString().getBytes());
            }
        });
    }

    private void e(String str) throws Exception {
        Context q2 = this.j.q();
        StatFs statFs = new StatFs(Environment.getDataDirectory().getPath());
        final int a2 = io.a.a.a.a.b.i.a();
        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        final long b2 = io.a.a.a.a.b.i.b();
        final long blockCount = ((long) statFs.getBlockCount()) * ((long) statFs.getBlockSize());
        final boolean f2 = io.a.a.a.a.b.i.f(q2);
        final Map h2 = this.m.h();
        final int h3 = io.a.a.a.a.b.i.h(q2);
        a(str, "SessionDevice", (b) new b() {
            public void a(e eVar) throws Exception {
                ag.a(eVar, a2, Build.MODEL, availableProcessors, b2, blockCount, f2, h2, h3, Build.MANUFACTURER, Build.PRODUCT);
            }
        });
        a(str, "SessionDevice.json", (d) new d() {
            public void a(FileOutputStream fileOutputStream) throws Exception {
                fileOutputStream.write(new JSONObject(new HashMap<String, Object>() {
                    {
                        put("arch", Integer.valueOf(a2));
                        put("build_model", Build.MODEL);
                        put("available_processors", Integer.valueOf(availableProcessors));
                        put("total_ram", Long.valueOf(b2));
                        put("disk_space", Long.valueOf(blockCount));
                        put("is_emulator", Boolean.valueOf(f2));
                        put("ids", h2);
                        put("state", Integer.valueOf(h3));
                        put("build_manufacturer", Build.MANUFACTURER);
                        put("build_product", Build.PRODUCT);
                    }
                }).toString().getBytes());
            }
        });
    }

    private void f(String str) throws Exception {
        final al g2 = g(str);
        a(str, "SessionUser", (b) new b() {
            public void a(e eVar) throws Exception {
                ag.a(eVar, g2.f1526b, g2.c, g2.d);
            }
        });
    }

    private al g(String str) {
        return e() ? new al(this.j.g(), this.j.i(), this.j.h()) : new w(f()).a(str);
    }

    private o h(String str) {
        return new p(this.j, io.a.a.a.a.b.i.b(this.j.q(), "com.crashlytics.ApiEndpoint"), str, this.l);
    }

    private String k() {
        File[] n2 = n();
        if (n2.length > 0) {
            return a(n2[0]);
        }
        return null;
    }

    private String l() {
        File[] n2 = n();
        if (n2.length > 1) {
            return a(n2[1]);
        }
        return null;
    }

    /* access modifiers changed from: private */
    public void m() throws Exception {
        Date date = new Date();
        String cVar = new c(this.m).toString();
        io.a.a.a.c.h().a("CrashlyticsCore", "Opening a new session with ID " + cVar);
        a(cVar, date);
        c(cVar);
        d(cVar);
        e(cVar);
        this.r.a(cVar);
    }

    private File[] n() {
        File[] c2 = c();
        Arrays.sort(c2, d);
        return c2;
    }

    private void o() {
        File i2 = i();
        if (i2.exists()) {
            File[] a2 = a(i2, (FilenameFilter) new e());
            Arrays.sort(a2, Collections.reverseOrder());
            HashSet hashSet = new HashSet();
            for (int i3 = 0; i3 < a2.length && hashSet.size() < 4; i3++) {
                hashSet.add(a(a2[i3]));
            }
            a(b(i2), (Set<String>) hashSet);
        }
    }

    private boolean p() {
        try {
            Class.forName("com.google.firebase.crash.FirebaseCrash");
            return true;
        } catch (ClassNotFoundException e2) {
            return false;
        }
    }

    /* access modifiers changed from: 0000 */
    public void a() {
        this.k.b(new Callable<Void>() {
            /* renamed from: a */
            public Void call() throws Exception {
                h.this.m();
                return null;
            }
        });
    }

    /* access modifiers changed from: 0000 */
    public void a(float f2, t tVar) {
        if (tVar == null) {
            io.a.a.a.c.h().d("CrashlyticsCore", "Could not send reports. Settings are not available.");
            return;
        }
        new af(this.p.f1493a, h(tVar.f4495a.d), this.s, this.t).a(f2, a(tVar) ? new g(this.j, this.n, tVar.c) : new a());
    }

    /* access modifiers changed from: 0000 */
    public void a(int i2) {
        int a2 = i2 - am.a(g(), i2, e);
        am.a(f(), f1552b, a2 - am.a(h(), a2, e), e);
    }

    /* access modifiers changed from: 0000 */
    public void a(final com.crashlytics.android.c.a.a.d dVar) {
        this.k.b(new Callable<Void>() {
            /* renamed from: a */
            public Void call() throws Exception {
                if (!h.this.e()) {
                    h.this.b(dVar);
                }
                return null;
            }
        });
    }

    /* access modifiers changed from: 0000 */
    public void a(UncaughtExceptionHandler uncaughtExceptionHandler) {
        a();
        this.z = new m(new a() {
            public void a(Thread thread, Throwable th) {
                h.this.a(thread, th);
            }
        }, uncaughtExceptionHandler);
        Thread.setDefaultUncaughtExceptionHandler(this.z);
    }

    /* access modifiers changed from: 0000 */
    public synchronized void a(final Thread thread, final Throwable th) {
        io.a.a.a.c.h().a("CrashlyticsCore", "Crashlytics is handling uncaught exception \"" + th + "\" from thread " + thread.getName());
        this.u.b();
        final Date date = new Date();
        this.k.a((Callable<T>) new Callable<Void>() {
            /* renamed from: a */
            public Void call() throws Exception {
                h.this.j.n();
                h.this.a(date, thread, th);
                t b2 = q.a().b();
                io.a.a.a.a.g.p pVar = b2 != null ? b2.f4496b : null;
                h.this.b(pVar);
                h.this.m();
                if (pVar != null) {
                    h.this.a(pVar.g);
                }
                if (!h.this.a(b2)) {
                    h.this.b(b2);
                }
                return null;
            }
        });
    }

    /* access modifiers changed from: 0000 */
    public void a(File[] fileArr) {
        File[] a2;
        final HashSet hashSet = new HashSet();
        for (File file : fileArr) {
            io.a.a.a.c.h().a("CrashlyticsCore", "Found invalid session part file: " + file);
            hashSet.add(a(file));
        }
        if (!hashSet.isEmpty()) {
            File i2 = i();
            if (!i2.exists()) {
                i2.mkdir();
            }
            for (File file2 : a((FilenameFilter) new FilenameFilter() {
                public boolean accept(File file, String str) {
                    if (str.length() < 35) {
                        return false;
                    }
                    return hashSet.contains(str.substring(0, 35));
                }
            })) {
                io.a.a.a.c.h().a("CrashlyticsCore", "Moving session file: " + file2);
                if (!file2.renameTo(new File(i2, file2.getName()))) {
                    io.a.a.a.c.h().a("CrashlyticsCore", "Could not move session file. Deleting " + file2);
                    file2.delete();
                }
            }
            o();
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean a(final io.a.a.a.a.g.p pVar) {
        return ((Boolean) this.k.a((Callable<T>) new Callable<Boolean>() {
            /* renamed from: a */
            public Boolean call() throws Exception {
                if (h.this.e()) {
                    io.a.a.a.c.h().a("CrashlyticsCore", "Skipping session finalization because a crash has already occurred.");
                    return Boolean.FALSE;
                }
                io.a.a.a.c.h().a("CrashlyticsCore", "Finalizing previously open sessions.");
                h.this.a(pVar, true);
                io.a.a.a.c.h().a("CrashlyticsCore", "Closed all previously open sessions");
                return Boolean.TRUE;
            }
        })).booleanValue();
    }

    /* access modifiers changed from: 0000 */
    public void b(io.a.a.a.a.g.p pVar) throws Exception {
        a(pVar, false);
    }

    /* access modifiers changed from: 0000 */
    public File[] b() {
        LinkedList linkedList = new LinkedList();
        Collections.addAll(linkedList, a(g(), f1552b));
        Collections.addAll(linkedList, a(h(), f1552b));
        Collections.addAll(linkedList, a(f(), f1552b));
        return (File[]) linkedList.toArray(new File[linkedList.size()]);
    }

    /* access modifiers changed from: 0000 */
    public File[] c() {
        return a(f1551a);
    }

    /* access modifiers changed from: 0000 */
    public void d() {
        this.k.a((Runnable) new Runnable() {
            public void run() {
                h.this.a(h.this.a((FilenameFilter) new e()));
            }
        });
    }

    /* access modifiers changed from: 0000 */
    public boolean e() {
        return this.z != null && this.z.a();
    }

    /* access modifiers changed from: 0000 */
    public File f() {
        return this.o.a();
    }

    /* access modifiers changed from: 0000 */
    public File g() {
        return new File(f(), "fatal-sessions");
    }

    /* access modifiers changed from: 0000 */
    public File h() {
        return new File(f(), "nonfatal-sessions");
    }

    /* access modifiers changed from: 0000 */
    public File i() {
        return new File(f(), "invalidClsFiles");
    }
}
