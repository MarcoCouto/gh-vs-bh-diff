package com.crashlytics.android.c;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;

class d extends FileOutputStream {

    /* renamed from: a reason: collision with root package name */
    public static final FilenameFilter f1534a = new FilenameFilter() {
        public boolean accept(File file, String str) {
            return str.endsWith(".cls_temp");
        }
    };

    /* renamed from: b reason: collision with root package name */
    private final String f1535b;
    private File c;
    private File d;
    private boolean e = false;

    public d(File file, String str) throws FileNotFoundException {
        super(new File(file, str + ".cls_temp"));
        this.f1535b = file + File.separator + str;
        this.c = new File(this.f1535b + ".cls_temp");
    }

    public void a() throws IOException {
        if (!this.e) {
            this.e = true;
            super.flush();
            super.close();
        }
    }

    public synchronized void close() throws IOException {
        if (!this.e) {
            this.e = true;
            super.flush();
            super.close();
            File file = new File(this.f1535b + ".cls");
            if (this.c.renameTo(file)) {
                this.c = null;
                this.d = file;
            } else {
                String str = "";
                if (file.exists()) {
                    str = " (target already exists)";
                } else if (!this.c.exists()) {
                    str = " (source does not exist)";
                }
                throw new IOException("Could not rename temp file: " + this.c + " -> " + file + str);
            }
        }
    }
}
