package com.crashlytics.android.c;

import android.app.ActivityManager.RunningAppProcessInfo;
import com.hmatalonga.greenhub.Config;
import io.a.a.a.a.b.p.a;
import io.a.a.a.c;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

class ag {

    /* renamed from: a reason: collision with root package name */
    private static final b f1519a = b.a(Config.NOTIFICATION_DEFAULT_PRIORITY);

    /* renamed from: b reason: collision with root package name */
    private static final b f1520b = b.a("Unity");

    private static int a() {
        return 0 + e.b(1, f1519a) + e.b(2, f1519a) + e.b(3, 0);
    }

    private static int a(int i, b bVar, int i2, long j, long j2, boolean z, Map<a, String> map, int i3, b bVar2, b bVar3) {
        int i4;
        int b2 = (bVar == null ? 0 : e.b(4, bVar)) + e.e(3, i) + 0 + e.d(5, i2) + e.b(6, j) + e.b(7, j2) + e.b(10, z);
        if (map != null) {
            Iterator it = map.entrySet().iterator();
            while (true) {
                i4 = b2;
                if (!it.hasNext()) {
                    break;
                }
                Entry entry = (Entry) it.next();
                int a2 = a((a) entry.getKey(), (String) entry.getValue());
                b2 = a2 + e.j(11) + e.l(a2) + i4;
            }
        } else {
            i4 = b2;
        }
        return (bVar3 == null ? 0 : e.b(14, bVar3)) + i4 + e.d(12, i3) + (bVar2 == null ? 0 : e.b(13, bVar2));
    }

    private static int a(long j, String str, aj ajVar, Thread thread, StackTraceElement[] stackTraceElementArr, Thread[] threadArr, List<StackTraceElement[]> list, int i, Map<String, String> map, RunningAppProcessInfo runningAppProcessInfo, int i2, b bVar, b bVar2, Float f, int i3, boolean z, long j2, long j3, b bVar3) {
        int b2 = 0 + e.b(1, j) + e.b(2, b.a(str));
        int a2 = a(ajVar, thread, stackTraceElementArr, threadArr, list, i, bVar, bVar2, map, runningAppProcessInfo, i2);
        int j4 = b2 + a2 + e.j(3) + e.l(a2);
        int a3 = a(f, i3, z, i2, j2, j3);
        int j5 = a3 + e.j(5) + e.l(a3) + j4;
        if (bVar3 == null) {
            return j5;
        }
        int b3 = b(bVar3);
        return j5 + b3 + e.j(6) + e.l(b3);
    }

    private static int a(aj ajVar, int i, int i2) {
        int i3 = 0;
        int b2 = e.b(1, b.a(ajVar.f1524b)) + 0;
        String str = ajVar.f1523a;
        if (str != null) {
            b2 += e.b(3, b.a(str));
        }
        StackTraceElement[] stackTraceElementArr = ajVar.c;
        int length = stackTraceElementArr.length;
        int i4 = 0;
        while (i4 < length) {
            int a2 = a(stackTraceElementArr[i4], true);
            i4++;
            b2 = a2 + e.j(4) + e.l(a2) + b2;
        }
        aj ajVar2 = ajVar.d;
        if (ajVar2 == null) {
            return b2;
        }
        if (i < i2) {
            int a3 = a(ajVar2, i + 1, i2);
            return b2 + a3 + e.j(6) + e.l(a3);
        }
        while (ajVar2 != null) {
            ajVar2 = ajVar2.d;
            i3++;
        }
        return b2 + e.d(7, i3);
    }

    private static int a(aj ajVar, Thread thread, StackTraceElement[] stackTraceElementArr, Thread[] threadArr, List<StackTraceElement[]> list, int i, b bVar, b bVar2) {
        int a2 = a(thread, stackTraceElementArr, 4, true);
        int j = a2 + e.j(1) + e.l(a2) + 0;
        int length = threadArr.length;
        int i2 = j;
        for (int i3 = 0; i3 < length; i3++) {
            int a3 = a(threadArr[i3], (StackTraceElement[]) list.get(i3), 0, false);
            i2 += a3 + e.j(1) + e.l(a3);
        }
        int a4 = a(ajVar, 1, i);
        int j2 = a4 + e.j(2) + e.l(a4) + i2;
        int a5 = a();
        int j3 = j2 + a5 + e.j(3) + e.l(a5);
        int a6 = a(bVar, bVar2);
        return j3 + a6 + e.j(3) + e.l(a6);
    }

    private static int a(aj ajVar, Thread thread, StackTraceElement[] stackTraceElementArr, Thread[] threadArr, List<StackTraceElement[]> list, int i, b bVar, b bVar2, Map<String, String> map, RunningAppProcessInfo runningAppProcessInfo, int i2) {
        int i3;
        int i4;
        int i5;
        int a2 = a(ajVar, thread, stackTraceElementArr, threadArr, list, i, bVar, bVar2);
        int j = 0 + a2 + e.j(1) + e.l(a2);
        if (map != null) {
            Iterator it = map.entrySet().iterator();
            while (true) {
                i5 = j;
                if (!it.hasNext()) {
                    break;
                }
                Entry entry = (Entry) it.next();
                int a3 = a((String) entry.getKey(), (String) entry.getValue());
                j = a3 + e.j(2) + e.l(a3) + i5;
            }
            i3 = i5;
        } else {
            i3 = j;
        }
        if (runningAppProcessInfo != null) {
            i4 = e.b(3, runningAppProcessInfo.importance != 100) + i3;
        } else {
            i4 = i3;
        }
        return i4 + e.d(4, i2);
    }

    private static int a(b bVar) {
        return 0 + e.b(1, bVar);
    }

    private static int a(b bVar, b bVar2) {
        int b2 = 0 + e.b(1, 0) + e.b(2, 0) + e.b(3, bVar);
        return bVar2 != null ? b2 + e.b(4, bVar2) : b2;
    }

    private static int a(b bVar, b bVar2, b bVar3, b bVar4, b bVar5, int i, b bVar6) {
        int b2 = 0 + e.b(1, bVar) + e.b(2, bVar3) + e.b(3, bVar4);
        int a2 = a(bVar2);
        int j = b2 + a2 + e.j(5) + e.l(a2) + e.b(6, bVar5);
        if (bVar6 != null) {
            j = j + e.b(8, f1520b) + e.b(9, bVar6);
        }
        return j + e.e(10, i);
    }

    private static int a(b bVar, b bVar2, boolean z) {
        return 0 + e.e(1, 3) + e.b(2, bVar) + e.b(3, bVar2) + e.b(4, z);
    }

    private static int a(a aVar, String str) {
        return e.e(1, aVar.h) + e.b(2, b.a(str));
    }

    private static int a(Float f, int i, boolean z, int i2, long j, long j2) {
        int i3 = 0;
        if (f != null) {
            i3 = 0 + e.b(1, f.floatValue());
        }
        return i3 + e.f(2, i) + e.b(3, z) + e.d(4, i2) + e.b(5, j) + e.b(6, j2);
    }

    private static int a(StackTraceElement stackTraceElement, boolean z) {
        int b2 = (stackTraceElement.isNativeMethod() ? e.b(1, (long) Math.max(stackTraceElement.getLineNumber(), 0)) + 0 : e.b(1, 0) + 0) + e.b(2, b.a(stackTraceElement.getClassName() + "." + stackTraceElement.getMethodName()));
        if (stackTraceElement.getFileName() != null) {
            b2 += e.b(3, b.a(stackTraceElement.getFileName()));
        }
        return e.d(5, z ? 2 : 0) + ((stackTraceElement.isNativeMethod() || stackTraceElement.getLineNumber() <= 0) ? b2 : b2 + e.b(4, (long) stackTraceElement.getLineNumber()));
    }

    private static int a(String str, String str2) {
        int b2 = e.b(1, b.a(str));
        if (str2 == null) {
            str2 = "";
        }
        return b2 + e.b(2, b.a(str2));
    }

    private static int a(Thread thread, StackTraceElement[] stackTraceElementArr, int i, boolean z) {
        int d = e.d(2, i) + e.b(1, b.a(thread.getName()));
        for (StackTraceElement a2 : stackTraceElementArr) {
            int a3 = a(a2, z);
            d += a3 + e.j(3) + e.l(a3);
        }
        return d;
    }

    private static b a(String str) {
        if (str == null) {
            return null;
        }
        return b.a(str);
    }

    private static void a(e eVar, int i, StackTraceElement stackTraceElement, boolean z) throws Exception {
        int i2 = 4;
        eVar.g(i, 2);
        eVar.k(a(stackTraceElement, z));
        if (stackTraceElement.isNativeMethod()) {
            eVar.a(1, (long) Math.max(stackTraceElement.getLineNumber(), 0));
        } else {
            eVar.a(1, 0);
        }
        eVar.a(2, b.a(stackTraceElement.getClassName() + "." + stackTraceElement.getMethodName()));
        if (stackTraceElement.getFileName() != null) {
            eVar.a(3, b.a(stackTraceElement.getFileName()));
        }
        if (!stackTraceElement.isNativeMethod() && stackTraceElement.getLineNumber() > 0) {
            eVar.a(4, (long) stackTraceElement.getLineNumber());
        }
        if (!z) {
            i2 = 0;
        }
        eVar.a(5, i2);
    }

    public static void a(e eVar, int i, String str, int i2, long j, long j2, boolean z, Map<a, String> map, int i3, String str2, String str3) throws Exception {
        b a2 = a(str);
        b a3 = a(str3);
        b a4 = a(str2);
        eVar.g(9, 2);
        eVar.k(a(i, a2, i2, j, j2, z, map, i3, a4, a3));
        eVar.b(3, i);
        eVar.a(4, a2);
        eVar.a(5, i2);
        eVar.a(6, j);
        eVar.a(7, j2);
        eVar.a(10, z);
        for (Entry entry : map.entrySet()) {
            eVar.g(11, 2);
            eVar.k(a((a) entry.getKey(), (String) entry.getValue()));
            eVar.b(1, ((a) entry.getKey()).h);
            eVar.a(2, b.a((String) entry.getValue()));
        }
        eVar.a(12, i3);
        if (a4 != null) {
            eVar.a(13, a4);
        }
        if (a3 != null) {
            eVar.a(14, a3);
        }
    }

    public static void a(e eVar, long j, String str, aj ajVar, Thread thread, StackTraceElement[] stackTraceElementArr, Thread[] threadArr, List<StackTraceElement[]> list, Map<String, String> map, u uVar, RunningAppProcessInfo runningAppProcessInfo, int i, String str2, String str3, Float f, int i2, boolean z, long j2, long j3) throws Exception {
        b a2;
        b a3 = b.a(str2);
        if (str3 == null) {
            a2 = null;
        } else {
            a2 = b.a(str3.replace("-", ""));
        }
        b a4 = uVar.a();
        if (a4 == null) {
            c.h().a("CrashlyticsCore", "No log data to include with this event.");
        }
        uVar.b();
        eVar.g(10, 2);
        eVar.k(a(j, str, ajVar, thread, stackTraceElementArr, threadArr, list, 8, map, runningAppProcessInfo, i, a3, a2, f, i2, z, j2, j3, a4));
        eVar.a(1, j);
        eVar.a(2, b.a(str));
        a(eVar, ajVar, thread, stackTraceElementArr, threadArr, list, 8, a3, a2, map, runningAppProcessInfo, i);
        a(eVar, f, i2, z, i, j2, j3);
        a(eVar, a4);
    }

    private static void a(e eVar, aj ajVar, int i, int i2, int i3) throws Exception {
        int i4 = 0;
        eVar.g(i3, 2);
        eVar.k(a(ajVar, 1, i2));
        eVar.a(1, b.a(ajVar.f1524b));
        String str = ajVar.f1523a;
        if (str != null) {
            eVar.a(3, b.a(str));
        }
        for (StackTraceElement a2 : ajVar.c) {
            a(eVar, 4, a2, true);
        }
        aj ajVar2 = ajVar.d;
        if (ajVar2 == null) {
            return;
        }
        if (i < i2) {
            a(eVar, ajVar2, i + 1, i2, 6);
            return;
        }
        while (ajVar2 != null) {
            ajVar2 = ajVar2.d;
            i4++;
        }
        eVar.a(7, i4);
    }

    private static void a(e eVar, aj ajVar, Thread thread, StackTraceElement[] stackTraceElementArr, Thread[] threadArr, List<StackTraceElement[]> list, int i, b bVar, b bVar2) throws Exception {
        eVar.g(1, 2);
        eVar.k(a(ajVar, thread, stackTraceElementArr, threadArr, list, i, bVar, bVar2));
        a(eVar, thread, stackTraceElementArr, 4, true);
        int length = threadArr.length;
        for (int i2 = 0; i2 < length; i2++) {
            a(eVar, threadArr[i2], (StackTraceElement[]) list.get(i2), 0, false);
        }
        a(eVar, ajVar, 1, i, 2);
        eVar.g(3, 2);
        eVar.k(a());
        eVar.a(1, f1519a);
        eVar.a(2, f1519a);
        eVar.a(3, 0);
        eVar.g(4, 2);
        eVar.k(a(bVar, bVar2));
        eVar.a(1, 0);
        eVar.a(2, 0);
        eVar.a(3, bVar);
        if (bVar2 != null) {
            eVar.a(4, bVar2);
        }
    }

    private static void a(e eVar, aj ajVar, Thread thread, StackTraceElement[] stackTraceElementArr, Thread[] threadArr, List<StackTraceElement[]> list, int i, b bVar, b bVar2, Map<String, String> map, RunningAppProcessInfo runningAppProcessInfo, int i2) throws Exception {
        eVar.g(3, 2);
        eVar.k(a(ajVar, thread, stackTraceElementArr, threadArr, list, i, bVar, bVar2, map, runningAppProcessInfo, i2));
        a(eVar, ajVar, thread, stackTraceElementArr, threadArr, list, i, bVar, bVar2);
        if (map != null && !map.isEmpty()) {
            a(eVar, map);
        }
        if (runningAppProcessInfo != null) {
            eVar.a(3, runningAppProcessInfo.importance != 100);
        }
        eVar.a(4, i2);
    }

    private static void a(e eVar, b bVar) throws Exception {
        if (bVar != null) {
            eVar.g(6, 2);
            eVar.k(b(bVar));
            eVar.a(1, bVar);
        }
    }

    private static void a(e eVar, Float f, int i, boolean z, int i2, long j, long j2) throws Exception {
        eVar.g(5, 2);
        eVar.k(a(f, i, z, i2, j, j2));
        if (f != null) {
            eVar.a(1, f.floatValue());
        }
        eVar.c(2, i);
        eVar.a(3, z);
        eVar.a(4, i2);
        eVar.a(5, j);
        eVar.a(6, j2);
    }

    public static void a(e eVar, String str, String str2, long j) throws Exception {
        eVar.a(1, b.a(str2));
        eVar.a(2, b.a(str));
        eVar.a(3, j);
    }

    public static void a(e eVar, String str, String str2, String str3) throws Exception {
        if (str == null) {
            str = "";
        }
        b a2 = b.a(str);
        b a3 = a(str2);
        b a4 = a(str3);
        int b2 = 0 + e.b(1, a2);
        if (str2 != null) {
            b2 += e.b(2, a3);
        }
        if (str3 != null) {
            b2 += e.b(3, a4);
        }
        eVar.g(6, 2);
        eVar.k(b2);
        eVar.a(1, a2);
        if (str2 != null) {
            eVar.a(2, a3);
        }
        if (str3 != null) {
            eVar.a(3, a4);
        }
    }

    public static void a(e eVar, String str, String str2, String str3, String str4, String str5, int i, String str6) throws Exception {
        b a2 = b.a(str);
        b a3 = b.a(str2);
        b a4 = b.a(str3);
        b a5 = b.a(str4);
        b a6 = b.a(str5);
        b bVar = str6 != null ? b.a(str6) : null;
        eVar.g(7, 2);
        eVar.k(a(a2, a3, a4, a5, a6, i, bVar));
        eVar.a(1, a2);
        eVar.a(2, a4);
        eVar.a(3, a5);
        eVar.g(5, 2);
        eVar.k(a(a3));
        eVar.a(1, a3);
        eVar.a(6, a6);
        if (bVar != null) {
            eVar.a(8, f1520b);
            eVar.a(9, bVar);
        }
        eVar.b(10, i);
    }

    public static void a(e eVar, String str, String str2, boolean z) throws Exception {
        b a2 = b.a(str);
        b a3 = b.a(str2);
        eVar.g(8, 2);
        eVar.k(a(a2, a3, z));
        eVar.b(1, 3);
        eVar.a(2, a2);
        eVar.a(3, a3);
        eVar.a(4, z);
    }

    private static void a(e eVar, Thread thread, StackTraceElement[] stackTraceElementArr, int i, boolean z) throws Exception {
        eVar.g(1, 2);
        eVar.k(a(thread, stackTraceElementArr, i, z));
        eVar.a(1, b.a(thread.getName()));
        eVar.a(2, i);
        for (StackTraceElement a2 : stackTraceElementArr) {
            a(eVar, 3, a2, z);
        }
    }

    private static void a(e eVar, Map<String, String> map) throws Exception {
        for (Entry entry : map.entrySet()) {
            eVar.g(2, 2);
            eVar.k(a((String) entry.getKey(), (String) entry.getValue()));
            eVar.a(1, b.a((String) entry.getKey()));
            String str = (String) entry.getValue();
            if (str == null) {
                str = "";
            }
            eVar.a(2, b.a(str));
        }
    }

    private static int b(b bVar) {
        return e.b(1, bVar);
    }
}
