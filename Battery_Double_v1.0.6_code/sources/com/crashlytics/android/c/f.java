package com.crashlytics.android.c;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.widget.ScrollView;
import android.widget.TextView;
import io.a.a.a.a.g.o;
import java.util.concurrent.CountDownLatch;

class f {

    /* renamed from: a reason: collision with root package name */
    private final b f1538a;

    /* renamed from: b reason: collision with root package name */
    private final Builder f1539b;

    interface a {
        void a(boolean z);
    }

    private static class b {

        /* renamed from: a reason: collision with root package name */
        private boolean f1544a;

        /* renamed from: b reason: collision with root package name */
        private final CountDownLatch f1545b;

        private b() {
            this.f1544a = false;
            this.f1545b = new CountDownLatch(1);
        }

        /* access modifiers changed from: 0000 */
        public void a(boolean z) {
            this.f1544a = z;
            this.f1545b.countDown();
        }

        /* access modifiers changed from: 0000 */
        public boolean a() {
            return this.f1544a;
        }

        /* access modifiers changed from: 0000 */
        public void b() {
            try {
                this.f1545b.await();
            } catch (InterruptedException e) {
            }
        }
    }

    private f(Builder builder, b bVar) {
        this.f1538a = bVar;
        this.f1539b = builder;
    }

    private static int a(float f, int i) {
        return (int) (((float) i) * f);
    }

    private static ScrollView a(Activity activity, String str) {
        float f = activity.getResources().getDisplayMetrics().density;
        int a2 = a(f, 5);
        TextView textView = new TextView(activity);
        textView.setAutoLinkMask(15);
        textView.setText(str);
        textView.setTextAppearance(activity, 16973892);
        textView.setPadding(a2, a2, a2, a2);
        textView.setFocusable(false);
        ScrollView scrollView = new ScrollView(activity);
        scrollView.setPadding(a(f, 14), a(f, 2), a(f, 10), a(f, 12));
        scrollView.addView(textView);
        return scrollView;
    }

    public static f a(Activity activity, o oVar, final a aVar) {
        final b bVar = new b();
        r rVar = new r(activity, oVar);
        Builder builder = new Builder(activity);
        ScrollView a2 = a(activity, rVar.b());
        builder.setView(a2).setTitle(rVar.a()).setCancelable(false).setNeutralButton(rVar.c(), new OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                bVar.a(true);
                dialogInterface.dismiss();
            }
        });
        if (oVar.d) {
            builder.setNegativeButton(rVar.e(), new OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    bVar.a(false);
                    dialogInterface.dismiss();
                }
            });
        }
        if (oVar.f) {
            builder.setPositiveButton(rVar.d(), new OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    aVar.a(true);
                    bVar.a(true);
                    dialogInterface.dismiss();
                }
            });
        }
        return new f(builder, bVar);
    }

    public void a() {
        this.f1539b.show();
    }

    public void b() {
        this.f1538a.b();
    }

    public boolean c() {
        return this.f1538a.a();
    }
}
