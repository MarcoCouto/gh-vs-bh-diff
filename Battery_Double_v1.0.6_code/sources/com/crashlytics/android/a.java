package com.crashlytics.android;

import com.crashlytics.android.a.b;
import com.crashlytics.android.b.c;
import io.a.a.a.i;
import io.a.a.a.j;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

public class a extends i<Void> implements j {

    /* renamed from: a reason: collision with root package name */
    public final b f1421a;

    /* renamed from: b reason: collision with root package name */
    public final c f1422b;
    public final com.crashlytics.android.c.i c;
    public final Collection<? extends i> d;

    public a() {
        this(new b(), new c(), new com.crashlytics.android.c.i());
    }

    a(b bVar, c cVar, com.crashlytics.android.c.i iVar) {
        this.f1421a = bVar;
        this.f1422b = cVar;
        this.c = iVar;
        this.d = Collections.unmodifiableCollection(Arrays.asList(new i[]{bVar, cVar, iVar}));
    }

    public String a() {
        return "2.7.1.19";
    }

    public String b() {
        return "com.crashlytics.sdk.android:crashlytics";
    }

    public Collection<? extends i> c() {
        return this.d;
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public Void e() {
        return null;
    }
}
