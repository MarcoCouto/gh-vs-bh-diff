package com.b.a.a;

import android.util.Log;
import com.b.a.a.a.a.C0038a;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class a {

    /* renamed from: a reason: collision with root package name */
    private static boolean f1412a;

    public static List<com.b.a.a.a.a> a() {
        File[] listFiles;
        ArrayList arrayList = new ArrayList();
        for (File file : new File("/proc").listFiles()) {
            if (file.isDirectory()) {
                try {
                    int parseInt = Integer.parseInt(file.getName());
                    try {
                        arrayList.add(new com.b.a.a.a.a(parseInt));
                    } catch (C0038a e) {
                    } catch (IOException e2) {
                        a(e2, "Error reading from /proc/%d.", Integer.valueOf(parseInt));
                    }
                } catch (NumberFormatException e3) {
                }
            }
        }
        return arrayList;
    }

    public static void a(String str, Object... objArr) {
        if (f1412a) {
            String str2 = "AndroidProcesses";
            if (objArr.length != 0) {
                str = String.format(str, objArr);
            }
            Log.d(str2, str);
        }
    }

    public static void a(Throwable th, String str, Object... objArr) {
        if (f1412a) {
            String str2 = "AndroidProcesses";
            if (objArr.length != 0) {
                str = String.format(str, objArr);
            }
            Log.d(str2, str, th);
        }
    }
}
