package com.b.a.a.a;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import java.io.IOException;

public final class g extends e {
    public static final Creator<g> CREATOR = new Creator<g>() {
        /* renamed from: a */
        public g createFromParcel(Parcel parcel) {
            return new g(parcel);
        }

        /* renamed from: a */
        public g[] newArray(int i) {
            return new g[i];
        }
    };

    /* renamed from: a reason: collision with root package name */
    public final String[] f1420a;

    private g(Parcel parcel) {
        super(parcel);
        this.f1420a = parcel.createStringArray();
    }

    private g(String str) throws IOException {
        super(str);
        this.f1420a = this.f1418b.split("\\s+");
    }

    public static g a(int i) throws IOException {
        return new g(String.format("/proc/%d/statm", new Object[]{Integer.valueOf(i)}));
    }

    public long a() {
        return Long.parseLong(this.f1420a[1]) * 1024;
    }

    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeStringArray(this.f1420a);
    }
}
