package com.b.a.a.a;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import java.io.IOException;

public class b implements Parcelable {
    public static final Creator<b> CREATOR = new Creator<b>() {
        /* renamed from: a */
        public b createFromParcel(Parcel parcel) {
            return new b(parcel);
        }

        /* renamed from: a */
        public b[] newArray(int i) {
            return new b[i];
        }
    };
    public final String c;
    public final int d;

    public b(int i) throws IOException {
        this.d = i;
        this.c = a(i);
    }

    protected b(Parcel parcel) {
        this.c = parcel.readString();
        this.d = parcel.readInt();
    }

    static String a(int i) throws IOException {
        String str = null;
        try {
            str = e.b(String.format("/proc/%d/cmdline", new Object[]{Integer.valueOf(i)})).trim();
        } catch (IOException e) {
        }
        return TextUtils.isEmpty(str) ? f.a(i).a() : str;
    }

    public c b() throws IOException {
        return c.a(this.d);
    }

    public f c() throws IOException {
        return f.a(this.d);
    }

    public g d() throws IOException {
        return g.a(this.d);
    }

    public int describeContents() {
        return 0;
    }

    public h e() throws IOException {
        return h.a(this.d);
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.c);
        parcel.writeInt(this.d);
    }
}
