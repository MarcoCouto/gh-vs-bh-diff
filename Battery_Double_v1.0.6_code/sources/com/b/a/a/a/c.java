package com.b.a.a.a;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

public final class c extends e {
    public static final Creator<c> CREATOR = new Creator<c>() {
        /* renamed from: a */
        public c createFromParcel(Parcel parcel) {
            return new c(parcel);
        }

        /* renamed from: a */
        public c[] newArray(int i) {
            return new c[i];
        }
    };

    /* renamed from: a reason: collision with root package name */
    public final ArrayList<d> f1415a;

    private c(Parcel parcel) {
        super(parcel);
        this.f1415a = parcel.createTypedArrayList(d.CREATOR);
    }

    private c(String str) throws IOException {
        super(str);
        String[] split = this.f1418b.split("\n");
        this.f1415a = new ArrayList<>();
        for (String dVar : split) {
            try {
                this.f1415a.add(new d(dVar));
            } catch (Exception e) {
            }
        }
    }

    public static c a(int i) throws IOException {
        return new c(String.format("/proc/%d/cgroup", new Object[]{Integer.valueOf(i)}));
    }

    public d a(String str) {
        Iterator it = this.f1415a.iterator();
        while (it.hasNext()) {
            d dVar = (d) it.next();
            String[] split = dVar.f1417b.split(",");
            int length = split.length;
            int i = 0;
            while (true) {
                if (i < length) {
                    if (split[i].equals(str)) {
                        return dVar;
                    }
                    i++;
                }
            }
        }
        return null;
    }

    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeTypedList(this.f1415a);
    }
}
