package com.b.a.a.a;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Build.VERSION;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import java.io.File;
import java.io.IOException;

public class a extends b {
    public static final Creator<a> CREATOR = new Creator<a>() {
        /* renamed from: a */
        public a createFromParcel(Parcel parcel) {
            return new a(parcel);
        }

        /* renamed from: a */
        public a[] newArray(int i) {
            return new a[i];
        }
    };
    private static final boolean e = new File("/dev/cpuctl/tasks").exists();

    /* renamed from: a reason: collision with root package name */
    public final boolean f1413a;

    /* renamed from: b reason: collision with root package name */
    public final int f1414b;

    /* renamed from: com.b.a.a.a.a$a reason: collision with other inner class name */
    public static final class C0038a extends Exception {
        public C0038a(int i) {
            super(String.format("The process %d does not belong to any application", new Object[]{Integer.valueOf(i)}));
        }
    }

    public a(int i) throws IOException, C0038a {
        boolean z;
        int a2;
        int a3;
        super(i);
        if (this.c == null || !this.c.matches("^([\\p{L}]{1}[\\p{L}\\p{N}_]*[\\.:])*[\\p{L}][\\p{L}\\p{N}_]*$") || !new File("/data/data", a()).exists()) {
            throw new C0038a(i);
        }
        if (e) {
            c b2 = b();
            d a4 = b2.a("cpuacct");
            d a5 = b2.a("cpu");
            if (VERSION.SDK_INT >= 21) {
                if (a5 == null || a4 == null || !a4.c.contains("pid_")) {
                    throw new C0038a(i);
                }
                z = !a5.c.contains("bg_non_interactive");
                try {
                    a2 = Integer.parseInt(a4.c.split("/")[1].replace("uid_", ""));
                } catch (Exception e2) {
                    a2 = e().a();
                }
                com.b.a.a.a.a("name=%s, pid=%d, uid=%d, foreground=%b, cpuacct=%s, cpu=%s", this.c, Integer.valueOf(i), Integer.valueOf(a2), Boolean.valueOf(z), a4.toString(), a5.toString());
            } else if (a5 == null || a4 == null || !a5.c.contains("apps")) {
                throw new C0038a(i);
            } else {
                z = !a5.c.contains("bg_non_interactive");
                try {
                    a3 = Integer.parseInt(a4.c.substring(a4.c.lastIndexOf("/") + 1));
                } catch (Exception e3) {
                    a3 = e().a();
                }
                com.b.a.a.a.a("name=%s, pid=%d, uid=%d foreground=%b, cpuacct=%s, cpu=%s", this.c, Integer.valueOf(i), Integer.valueOf(a2), Boolean.valueOf(z), a4.toString(), a5.toString());
            }
        } else {
            f c = c();
            h e4 = e();
            z = c.b() == 0;
            a2 = e4.a();
            com.b.a.a.a.a("name=%s, pid=%d, uid=%d foreground=%b", this.c, Integer.valueOf(i), Integer.valueOf(a2), Boolean.valueOf(z));
        }
        this.f1413a = z;
        this.f1414b = a2;
    }

    protected a(Parcel parcel) {
        super(parcel);
        this.f1413a = parcel.readByte() != 0;
        this.f1414b = parcel.readInt();
    }

    public PackageInfo a(Context context, int i) throws NameNotFoundException {
        return context.getPackageManager().getPackageInfo(a(), i);
    }

    public String a() {
        return this.c.split(":")[0];
    }

    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeByte((byte) (this.f1413a ? 1 : 0));
        parcel.writeInt(this.f1414b);
    }
}
