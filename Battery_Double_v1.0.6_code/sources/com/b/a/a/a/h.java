package com.b.a.a.a;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import java.io.IOException;

public final class h extends e {
    public static final Creator<h> CREATOR = new Creator<h>() {
        /* renamed from: a */
        public h createFromParcel(Parcel parcel) {
            return new h(parcel);
        }

        /* renamed from: a */
        public h[] newArray(int i) {
            return new h[i];
        }
    };

    private h(Parcel parcel) {
        super(parcel);
    }

    private h(String str) throws IOException {
        super(str);
    }

    public static h a(int i) throws IOException {
        return new h(String.format("/proc/%d/status", new Object[]{Integer.valueOf(i)}));
    }

    public int a() {
        try {
            return Integer.parseInt(a("Uid").split("\\s+")[0]);
        } catch (Exception e) {
            return -1;
        }
    }

    public String a(String str) {
        String[] split;
        for (String str2 : this.f1418b.split("\n")) {
            if (str2.startsWith(str + ":")) {
                return str2.split(str + ":")[1].trim();
            }
        }
        return null;
    }
}
