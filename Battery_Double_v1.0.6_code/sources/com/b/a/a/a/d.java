package com.b.a.a.a;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class d implements Parcelable {
    public static final Creator<d> CREATOR = new Creator<d>() {
        /* renamed from: a */
        public d createFromParcel(Parcel parcel) {
            return new d(parcel);
        }

        /* renamed from: a */
        public d[] newArray(int i) {
            return new d[i];
        }
    };

    /* renamed from: a reason: collision with root package name */
    public final int f1416a;

    /* renamed from: b reason: collision with root package name */
    public final String f1417b;
    public final String c;

    protected d(Parcel parcel) {
        this.f1416a = parcel.readInt();
        this.f1417b = parcel.readString();
        this.c = parcel.readString();
    }

    protected d(String str) throws NumberFormatException, IndexOutOfBoundsException {
        String[] split = str.split(":");
        this.f1416a = Integer.parseInt(split[0]);
        this.f1417b = split[1];
        this.c = split[2];
    }

    public int describeContents() {
        return 0;
    }

    public String toString() {
        return String.format("%d:%s:%s", new Object[]{Integer.valueOf(this.f1416a), this.f1417b, this.c});
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.f1416a);
        parcel.writeString(this.f1417b);
        parcel.writeString(this.c);
    }
}
