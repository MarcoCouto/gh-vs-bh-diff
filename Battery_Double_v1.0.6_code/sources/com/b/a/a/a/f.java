package com.b.a.a.a;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import java.io.IOException;

public final class f extends e {
    public static final Creator<f> CREATOR = new Creator<f>() {
        /* renamed from: a */
        public f createFromParcel(Parcel parcel) {
            return new f(parcel);
        }

        /* renamed from: a */
        public f[] newArray(int i) {
            return new f[i];
        }
    };

    /* renamed from: a reason: collision with root package name */
    private final String[] f1419a;

    private f(Parcel parcel) {
        super(parcel);
        this.f1419a = parcel.createStringArray();
    }

    private f(String str) throws IOException {
        super(str);
        this.f1419a = this.f1418b.split("\\s+");
    }

    public static f a(int i) throws IOException {
        return new f(String.format("/proc/%d/stat", new Object[]{Integer.valueOf(i)}));
    }

    public String a() {
        return this.f1419a[1].replace("(", "").replace(")", "");
    }

    public int b() {
        return Integer.parseInt(this.f1419a[40]);
    }

    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeStringArray(this.f1419a);
    }
}
