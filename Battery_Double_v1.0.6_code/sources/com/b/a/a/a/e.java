package com.b.a.a.a;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class e extends File implements Parcelable {
    public static final Creator<e> CREATOR = new Creator<e>() {
        /* renamed from: a */
        public e createFromParcel(Parcel parcel) {
            return new e(parcel);
        }

        /* renamed from: a */
        public e[] newArray(int i) {
            return new e[i];
        }
    };

    /* renamed from: b reason: collision with root package name */
    public final String f1418b;

    protected e(Parcel parcel) {
        super(parcel.readString());
        this.f1418b = parcel.readString();
    }

    protected e(String str) throws IOException {
        super(str);
        this.f1418b = b(str);
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0034 A[SYNTHETIC, Splitter:B:15:0x0034] */
    protected static String b(String str) throws IOException {
        BufferedReader bufferedReader;
        try {
            StringBuilder sb = new StringBuilder();
            bufferedReader = new BufferedReader(new FileReader(str));
            try {
                String str2 = "";
                for (String readLine = bufferedReader.readLine(); readLine != null; readLine = bufferedReader.readLine()) {
                    sb.append(str2).append(readLine);
                    str2 = "\n";
                }
                String sb2 = sb.toString();
                if (bufferedReader != null) {
                    try {
                        bufferedReader.close();
                    } catch (IOException e) {
                    }
                }
                return sb2;
            } catch (Throwable th) {
                th = th;
                if (bufferedReader != null) {
                }
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            bufferedReader = null;
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException e2) {
                }
            }
            throw th;
        }
    }

    public int describeContents() {
        return 0;
    }

    public long length() {
        return (long) this.f1418b.length();
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(getAbsolutePath());
        parcel.writeString(this.f1418b);
    }
}
