package com.google.ads.mediation;

import com.google.android.gms.ads.reward.a;
import com.google.android.gms.ads.reward.b;
import com.google.android.gms.ads.reward.mediation.MediationRewardedVideoAdAdapter;

final class g implements b {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ AbstractAdViewAdapter f1915a;

    g(AbstractAdViewAdapter abstractAdViewAdapter) {
        this.f1915a = abstractAdViewAdapter;
    }

    public final void a() {
        this.f1915a.zzhb.b(this.f1915a);
    }

    public final void a(int i) {
        this.f1915a.zzhb.a((MediationRewardedVideoAdAdapter) this.f1915a, i);
    }

    public final void a(a aVar) {
        this.f1915a.zzhb.a((MediationRewardedVideoAdAdapter) this.f1915a, aVar);
    }

    public final void b() {
        this.f1915a.zzhb.c(this.f1915a);
    }

    public final void c() {
        this.f1915a.zzhb.d(this.f1915a);
    }

    public final void d() {
        this.f1915a.zzhb.e(this.f1915a);
        this.f1915a.zzha = null;
    }

    public final void e() {
        this.f1915a.zzhb.f(this.f1915a);
    }

    public final void f() {
        this.f1915a.zzhb.g(this.f1915a);
    }
}
