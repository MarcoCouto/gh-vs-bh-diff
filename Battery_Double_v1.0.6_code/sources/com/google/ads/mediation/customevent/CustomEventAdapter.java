package com.google.ads.mediation.customevent;

import android.app.Activity;
import android.view.View;
import com.google.ads.a.C0043a;
import com.google.ads.mediation.MediationBannerAdapter;
import com.google.ads.mediation.MediationInterstitialAdapter;
import com.google.ads.mediation.d;
import com.google.android.gms.ads.mediation.customevent.c;
import com.google.android.gms.common.annotation.KeepName;
import com.google.android.gms.internal.ads.ms;

@KeepName
public final class CustomEventAdapter implements MediationBannerAdapter<c, d>, MediationInterstitialAdapter<c, d> {

    /* renamed from: a reason: collision with root package name */
    private View f1907a;

    /* renamed from: b reason: collision with root package name */
    private CustomEventBanner f1908b;
    private CustomEventInterstitial c;

    static final class a implements b {

        /* renamed from: a reason: collision with root package name */
        private final CustomEventAdapter f1909a;

        /* renamed from: b reason: collision with root package name */
        private final com.google.ads.mediation.c f1910b;

        public a(CustomEventAdapter customEventAdapter, com.google.ads.mediation.c cVar) {
            this.f1909a = customEventAdapter;
            this.f1910b = cVar;
        }
    }

    class b implements c {

        /* renamed from: a reason: collision with root package name */
        private final CustomEventAdapter f1911a;

        /* renamed from: b reason: collision with root package name */
        private final d f1912b;

        public b(CustomEventAdapter customEventAdapter, d dVar) {
            this.f1911a = customEventAdapter;
            this.f1912b = dVar;
        }
    }

    private static <T> T a(String str) {
        try {
            return Class.forName(str).newInstance();
        } catch (Throwable th) {
            String message = th.getMessage();
            ms.e(new StringBuilder(String.valueOf(str).length() + 46 + String.valueOf(message).length()).append("Could not instantiate custom event adapter: ").append(str).append(". ").append(message).toString());
            return null;
        }
    }

    public final void destroy() {
        if (this.f1908b != null) {
            this.f1908b.a();
        }
        if (this.c != null) {
            this.c.a();
        }
    }

    public final Class<c> getAdditionalParametersType() {
        return c.class;
    }

    public final View getBannerView() {
        return this.f1907a;
    }

    public final Class<d> getServerParametersType() {
        return d.class;
    }

    public final void requestBannerAd(com.google.ads.mediation.c cVar, Activity activity, d dVar, com.google.ads.b bVar, com.google.ads.mediation.a aVar, c cVar2) {
        this.f1908b = (CustomEventBanner) a(dVar.f1914b);
        if (this.f1908b == null) {
            cVar.a(this, C0043a.INTERNAL_ERROR);
        } else {
            this.f1908b.requestBannerAd(new a(this, cVar), activity, dVar.f1913a, dVar.c, bVar, aVar, cVar2 == null ? null : cVar2.a(dVar.f1913a));
        }
    }

    public final void requestInterstitialAd(d dVar, Activity activity, d dVar2, com.google.ads.mediation.a aVar, c cVar) {
        this.c = (CustomEventInterstitial) a(dVar2.f1914b);
        if (this.c == null) {
            dVar.a(this, C0043a.INTERNAL_ERROR);
        } else {
            this.c.requestInterstitialAd(new b(this, dVar), activity, dVar2.f1913a, dVar2.c, aVar, cVar == null ? null : cVar.a(dVar2.f1913a));
        }
    }

    public final void showInterstitial() {
        this.c.showInterstitial();
    }
}
