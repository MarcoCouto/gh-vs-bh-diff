package com.google.ads.mediation;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.b.i;
import com.google.android.gms.ads.g;
import com.google.android.gms.ads.mediation.MediationBannerAdapter;
import com.google.android.gms.ads.mediation.MediationNativeAdapter;
import com.google.android.gms.ads.mediation.h;
import com.google.android.gms.ads.mediation.k;
import com.google.android.gms.ads.mediation.l;
import com.google.android.gms.ads.mediation.m;
import com.google.android.gms.ads.reward.mediation.MediationRewardedVideoAdAdapter;
import com.google.android.gms.internal.ads.aoj;
import com.google.android.gms.internal.ads.ape;
import com.google.android.gms.internal.ads.aqs;
import com.google.android.gms.internal.ads.cm;
import com.google.android.gms.internal.ads.mh;
import com.google.android.gms.internal.ads.ms;
import com.google.android.gms.internal.ads.zzatm;
import java.util.Date;
import java.util.Map;
import java.util.Set;

@cm
public abstract class AbstractAdViewAdapter implements MediationBannerAdapter, MediationNativeAdapter, k, m, MediationRewardedVideoAdAdapter, zzatm {
    public static final String AD_UNIT_ID_PARAMETER = "pubid";
    private AdView zzgw;
    private g zzgx;
    private com.google.android.gms.ads.b zzgy;
    private Context zzgz;
    /* access modifiers changed from: private */
    public g zzha;
    /* access modifiers changed from: private */
    public com.google.android.gms.ads.reward.mediation.a zzhb;
    private final com.google.android.gms.ads.reward.b zzhc = new g(this);

    static class a extends com.google.android.gms.ads.mediation.g {
        private final com.google.android.gms.ads.b.g e;

        public a(com.google.android.gms.ads.b.g gVar) {
            this.e = gVar;
            a(gVar.b().toString());
            a(gVar.c());
            b(gVar.d().toString());
            a(gVar.e());
            c(gVar.f().toString());
            if (gVar.g() != null) {
                a(gVar.g().doubleValue());
            }
            if (gVar.h() != null) {
                d(gVar.h().toString());
            }
            if (gVar.i() != null) {
                e(gVar.i().toString());
            }
            a(true);
            b(true);
            a(gVar.j());
        }

        public final void a(View view) {
            if (view instanceof com.google.android.gms.ads.b.e) {
                ((com.google.android.gms.ads.b.e) view).setNativeAd(this.e);
            }
            com.google.android.gms.ads.b.f fVar = (com.google.android.gms.ads.b.f) com.google.android.gms.ads.b.f.f1929a.get(view);
            if (fVar != null) {
                fVar.a((com.google.android.gms.ads.b.c) this.e);
            }
        }
    }

    static class b extends h {
        private final com.google.android.gms.ads.b.h e;

        public b(com.google.android.gms.ads.b.h hVar) {
            this.e = hVar;
            a(hVar.b().toString());
            a(hVar.c());
            b(hVar.d().toString());
            if (hVar.e() != null) {
                a(hVar.e());
            }
            c(hVar.f().toString());
            d(hVar.g().toString());
            a(true);
            b(true);
            a(hVar.h());
        }

        public final void a(View view) {
            if (view instanceof com.google.android.gms.ads.b.e) {
                ((com.google.android.gms.ads.b.e) view).setNativeAd(this.e);
            }
            com.google.android.gms.ads.b.f fVar = (com.google.android.gms.ads.b.f) com.google.android.gms.ads.b.f.f1929a.get(view);
            if (fVar != null) {
                fVar.a((com.google.android.gms.ads.b.c) this.e);
            }
        }
    }

    static class c extends l {

        /* renamed from: a reason: collision with root package name */
        private final com.google.android.gms.ads.b.k f1898a;

        public c(com.google.android.gms.ads.b.k kVar) {
            this.f1898a = kVar;
            a(kVar.a());
            a(kVar.b());
            b(kVar.c());
            a(kVar.d());
            c(kVar.e());
            d(kVar.f());
            a(kVar.g());
            e(kVar.h());
            f(kVar.i());
            a(kVar.l());
            a(true);
            b(true);
            a(kVar.j());
        }

        public final void a(View view, Map<String, View> map, Map<String, View> map2) {
            if (view instanceof com.google.android.gms.ads.b.l) {
                ((com.google.android.gms.ads.b.l) view).setNativeAd(this.f1898a);
                return;
            }
            com.google.android.gms.ads.b.f fVar = (com.google.android.gms.ads.b.f) com.google.android.gms.ads.b.f.f1929a.get(view);
            if (fVar != null) {
                fVar.a(this.f1898a);
            }
        }
    }

    static final class d extends com.google.android.gms.ads.a implements com.google.android.gms.ads.a.a, aoj {

        /* renamed from: a reason: collision with root package name */
        private final AbstractAdViewAdapter f1899a;

        /* renamed from: b reason: collision with root package name */
        private final com.google.android.gms.ads.mediation.c f1900b;

        public d(AbstractAdViewAdapter abstractAdViewAdapter, com.google.android.gms.ads.mediation.c cVar) {
            this.f1899a = abstractAdViewAdapter;
            this.f1900b = cVar;
        }

        public final void a() {
            this.f1900b.a(this.f1899a);
        }

        public final void a(int i) {
            this.f1900b.a(this.f1899a, i);
        }

        public final void a(String str, String str2) {
            this.f1900b.a(this.f1899a, str, str2);
        }

        public final void b() {
            this.f1900b.b(this.f1899a);
        }

        public final void c() {
            this.f1900b.c(this.f1899a);
        }

        public final void d() {
            this.f1900b.d(this.f1899a);
        }

        public final void e() {
            this.f1900b.e(this.f1899a);
        }
    }

    static final class e extends com.google.android.gms.ads.a implements aoj {

        /* renamed from: a reason: collision with root package name */
        private final AbstractAdViewAdapter f1901a;

        /* renamed from: b reason: collision with root package name */
        private final com.google.android.gms.ads.mediation.d f1902b;

        public e(AbstractAdViewAdapter abstractAdViewAdapter, com.google.android.gms.ads.mediation.d dVar) {
            this.f1901a = abstractAdViewAdapter;
            this.f1902b = dVar;
        }

        public final void a() {
            this.f1902b.a(this.f1901a);
        }

        public final void a(int i) {
            this.f1902b.a(this.f1901a, i);
        }

        public final void b() {
            this.f1902b.b(this.f1901a);
        }

        public final void c() {
            this.f1902b.c(this.f1901a);
        }

        public final void d() {
            this.f1902b.d(this.f1901a);
        }

        public final void e() {
            this.f1902b.e(this.f1901a);
        }
    }

    static final class f extends com.google.android.gms.ads.a implements com.google.android.gms.ads.b.g.a, com.google.android.gms.ads.b.h.a, com.google.android.gms.ads.b.i.a, com.google.android.gms.ads.b.i.b, com.google.android.gms.ads.b.k.a {

        /* renamed from: a reason: collision with root package name */
        private final AbstractAdViewAdapter f1903a;

        /* renamed from: b reason: collision with root package name */
        private final com.google.android.gms.ads.mediation.e f1904b;

        public f(AbstractAdViewAdapter abstractAdViewAdapter, com.google.android.gms.ads.mediation.e eVar) {
            this.f1903a = abstractAdViewAdapter;
            this.f1904b = eVar;
        }

        public final void a() {
        }

        public final void a(int i) {
            this.f1904b.a((MediationNativeAdapter) this.f1903a, i);
        }

        public final void a(com.google.android.gms.ads.b.g gVar) {
            this.f1904b.a((MediationNativeAdapter) this.f1903a, (com.google.android.gms.ads.mediation.f) new a(gVar));
        }

        public final void a(com.google.android.gms.ads.b.h hVar) {
            this.f1904b.a((MediationNativeAdapter) this.f1903a, (com.google.android.gms.ads.mediation.f) new b(hVar));
        }

        public final void a(i iVar) {
            this.f1904b.a((MediationNativeAdapter) this.f1903a, iVar);
        }

        public final void a(i iVar, String str) {
            this.f1904b.a(this.f1903a, iVar, str);
        }

        public final void a(com.google.android.gms.ads.b.k kVar) {
            this.f1904b.a((MediationNativeAdapter) this.f1903a, (l) new c(kVar));
        }

        public final void b() {
            this.f1904b.a(this.f1903a);
        }

        public final void c() {
            this.f1904b.b(this.f1903a);
        }

        public final void d() {
            this.f1904b.c(this.f1903a);
        }

        public final void e() {
            this.f1904b.d(this.f1903a);
        }

        public final void f() {
            this.f1904b.e(this.f1903a);
        }
    }

    private final com.google.android.gms.ads.c zza(Context context, com.google.android.gms.ads.mediation.a aVar, Bundle bundle, Bundle bundle2) {
        com.google.android.gms.ads.c.a aVar2 = new com.google.android.gms.ads.c.a();
        Date a2 = aVar.a();
        if (a2 != null) {
            aVar2.a(a2);
        }
        int b2 = aVar.b();
        if (b2 != 0) {
            aVar2.a(b2);
        }
        Set<String> c2 = aVar.c();
        if (c2 != null) {
            for (String a3 : c2) {
                aVar2.a(a3);
            }
        }
        Location d2 = aVar.d();
        if (d2 != null) {
            aVar2.a(d2);
        }
        if (aVar.f()) {
            ape.a();
            aVar2.b(mh.a(context));
        }
        if (aVar.e() != -1) {
            aVar2.a(aVar.e() == 1);
        }
        aVar2.b(aVar.g());
        aVar2.a(AdMobAdapter.class, zza(bundle, bundle2));
        return aVar2.a();
    }

    public String getAdUnitId(Bundle bundle) {
        return bundle.getString(AD_UNIT_ID_PARAMETER);
    }

    public View getBannerView() {
        return this.zzgw;
    }

    public Bundle getInterstitialAdapterInfo() {
        return new com.google.android.gms.ads.mediation.b.a().a(1).a();
    }

    public aqs getVideoController() {
        if (this.zzgw != null) {
            com.google.android.gms.ads.i videoController = this.zzgw.getVideoController();
            if (videoController != null) {
                return videoController.a();
            }
        }
        return null;
    }

    public void initialize(Context context, com.google.android.gms.ads.mediation.a aVar, String str, com.google.android.gms.ads.reward.mediation.a aVar2, Bundle bundle, Bundle bundle2) {
        this.zzgz = context.getApplicationContext();
        this.zzhb = aVar2;
        this.zzhb.a((MediationRewardedVideoAdAdapter) this);
    }

    public boolean isInitialized() {
        return this.zzhb != null;
    }

    public void loadAd(com.google.android.gms.ads.mediation.a aVar, Bundle bundle, Bundle bundle2) {
        if (this.zzgz == null || this.zzhb == null) {
            ms.c("AdMobAdapter.loadAd called before initialize.");
            return;
        }
        this.zzha = new g(this.zzgz);
        this.zzha.a(true);
        this.zzha.a(getAdUnitId(bundle));
        this.zzha.a(this.zzhc);
        this.zzha.a((com.google.android.gms.ads.reward.c) new h(this));
        this.zzha.a(zza(this.zzgz, aVar, bundle2, bundle));
    }

    public void onDestroy() {
        if (this.zzgw != null) {
            this.zzgw.c();
            this.zzgw = null;
        }
        if (this.zzgx != null) {
            this.zzgx = null;
        }
        if (this.zzgy != null) {
            this.zzgy = null;
        }
        if (this.zzha != null) {
            this.zzha = null;
        }
    }

    public void onImmersiveModeUpdated(boolean z) {
        if (this.zzgx != null) {
            this.zzgx.b(z);
        }
        if (this.zzha != null) {
            this.zzha.b(z);
        }
    }

    public void onPause() {
        if (this.zzgw != null) {
            this.zzgw.b();
        }
    }

    public void onResume() {
        if (this.zzgw != null) {
            this.zzgw.a();
        }
    }

    public void requestBannerAd(Context context, com.google.android.gms.ads.mediation.c cVar, Bundle bundle, com.google.android.gms.ads.d dVar, com.google.android.gms.ads.mediation.a aVar, Bundle bundle2) {
        this.zzgw = new AdView(context);
        this.zzgw.setAdSize(new com.google.android.gms.ads.d(dVar.b(), dVar.a()));
        this.zzgw.setAdUnitId(getAdUnitId(bundle));
        this.zzgw.setAdListener(new d(this, cVar));
        this.zzgw.a(zza(context, aVar, bundle2, bundle));
    }

    public void requestInterstitialAd(Context context, com.google.android.gms.ads.mediation.d dVar, Bundle bundle, com.google.android.gms.ads.mediation.a aVar, Bundle bundle2) {
        this.zzgx = new g(context);
        this.zzgx.a(getAdUnitId(bundle));
        this.zzgx.a((com.google.android.gms.ads.a) new e(this, dVar));
        this.zzgx.a(zza(context, aVar, bundle2, bundle));
    }

    public void requestNativeAd(Context context, com.google.android.gms.ads.mediation.e eVar, Bundle bundle, com.google.android.gms.ads.mediation.i iVar, Bundle bundle2) {
        f fVar = new f(this, eVar);
        com.google.android.gms.ads.b.a a2 = new com.google.android.gms.ads.b.a(context, bundle.getString(AD_UNIT_ID_PARAMETER)).a((com.google.android.gms.ads.a) fVar);
        com.google.android.gms.ads.b.d h = iVar.h();
        if (h != null) {
            a2.a(h);
        }
        if (iVar.j()) {
            a2.a((com.google.android.gms.ads.b.k.a) fVar);
        }
        if (iVar.i()) {
            a2.a((com.google.android.gms.ads.b.g.a) fVar);
        }
        if (iVar.k()) {
            a2.a((com.google.android.gms.ads.b.h.a) fVar);
        }
        if (iVar.l()) {
            for (String str : iVar.m().keySet()) {
                a2.a(str, fVar, ((Boolean) iVar.m().get(str)).booleanValue() ? fVar : null);
            }
        }
        this.zzgy = a2.a();
        this.zzgy.a(zza(context, iVar, bundle2, bundle));
    }

    public void showInterstitial() {
        this.zzgx.b();
    }

    public void showVideo() {
        this.zzha.b();
    }

    /* access modifiers changed from: protected */
    public abstract Bundle zza(Bundle bundle, Bundle bundle2);
}
