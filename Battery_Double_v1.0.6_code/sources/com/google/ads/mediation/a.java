package com.google.ads.mediation;

import android.location.Location;
import com.google.ads.a.b;
import java.util.Date;
import java.util.Set;

@Deprecated
public class a {

    /* renamed from: a reason: collision with root package name */
    private final Date f1905a;

    /* renamed from: b reason: collision with root package name */
    private final b f1906b;
    private final Set<String> c;
    private final boolean d;
    private final Location e;

    public a(Date date, b bVar, Set<String> set, boolean z, Location location) {
        this.f1905a = date;
        this.f1906b = bVar;
        this.c = set;
        this.d = z;
        this.e = location;
    }
}
