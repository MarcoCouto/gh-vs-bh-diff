package com.google.firebase;

import android.annotation.TargetApi;
import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;
import com.google.android.gms.common.api.internal.a.C0054a;
import com.google.android.gms.common.internal.aa;
import com.google.android.gms.common.internal.z;
import com.google.android.gms.common.util.n;
import com.google.firebase.components.l;
import com.google.firebase.components.p;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

public class a {

    /* renamed from: a reason: collision with root package name */
    static final Map<String, a> f4148a = new android.support.v4.h.a();

    /* renamed from: b reason: collision with root package name */
    private static final List<String> f4149b = Arrays.asList(new String[]{"com.google.firebase.auth.FirebaseAuth", "com.google.firebase.iid.FirebaseInstanceId"});
    private static final List<String> c = Collections.singletonList("com.google.firebase.crash.FirebaseCrash");
    private static final List<String> d = Arrays.asList(new String[]{"com.google.android.gms.measurement.AppMeasurement"});
    private static final List<String> e = Arrays.asList(new String[0]);
    private static final Set<String> f = Collections.emptySet();
    /* access modifiers changed from: private */
    public static final Object g = new Object();
    private final Context h;
    private final String i;
    private final b j;
    private final p k;
    private final AtomicBoolean l = new AtomicBoolean(false);
    private final AtomicBoolean m = new AtomicBoolean();
    private final List<Object> n = new CopyOnWriteArrayList();
    private final List<C0068a> o = new CopyOnWriteArrayList();
    private final List<Object> p = new CopyOnWriteArrayList();
    private b q;

    /* renamed from: com.google.firebase.a$a reason: collision with other inner class name */
    public interface C0068a {
        void a(boolean z);
    }

    public interface b {
    }

    @TargetApi(24)
    private static class c extends BroadcastReceiver {

        /* renamed from: a reason: collision with root package name */
        private static AtomicReference<c> f4150a = new AtomicReference<>();

        /* renamed from: b reason: collision with root package name */
        private final Context f4151b;

        private c(Context context) {
            this.f4151b = context;
        }

        /* access modifiers changed from: private */
        public static void b(Context context) {
            if (f4150a.get() == null) {
                c cVar = new c(context);
                if (f4150a.compareAndSet(null, cVar)) {
                    context.registerReceiver(cVar, new IntentFilter("android.intent.action.USER_UNLOCKED"));
                }
            }
        }

        public final void onReceive(Context context, Intent intent) {
            synchronized (a.g) {
                for (a a2 : a.f4148a.values()) {
                    a2.h();
                }
            }
            this.f4151b.unregisterReceiver(this);
        }
    }

    private a(Context context, String str, b bVar) {
        this.h = (Context) aa.a(context);
        this.i = aa.a(str);
        this.j = (b) aa.a(bVar);
        this.q = new com.google.firebase.b.a();
        this.k = new p(new l(this.h).a(), com.google.firebase.components.a.a(Context.class, this.h), com.google.firebase.components.a.a(a.class, this), com.google.firebase.components.a.a(b.class, this.j));
    }

    public static a a(Context context) {
        a a2;
        synchronized (g) {
            if (f4148a.containsKey("[DEFAULT]")) {
                a2 = d();
            } else {
                b a3 = b.a(context);
                a2 = a3 == null ? null : a(context, a3);
            }
        }
        return a2;
    }

    public static a a(Context context, b bVar) {
        return a(context, bVar, "[DEFAULT]");
    }

    public static a a(Context context, b bVar, String str) {
        a aVar;
        com.google.firebase.b.b.a(context);
        if (n.b() && (context.getApplicationContext() instanceof Application)) {
            com.google.android.gms.common.api.internal.a.a((Application) context.getApplicationContext());
            com.google.android.gms.common.api.internal.a.a().a((C0054a) new c());
        }
        String trim = str.trim();
        if (context.getApplicationContext() != null) {
            context = context.getApplicationContext();
        }
        synchronized (g) {
            aa.a(!f4148a.containsKey(trim), (Object) new StringBuilder(String.valueOf(trim).length() + 33).append("FirebaseApp name ").append(trim).append(" already exists!").toString());
            aa.a(context, (Object) "Application context cannot be null.");
            aVar = new a(context, trim, bVar);
            f4148a.put(trim, aVar);
        }
        com.google.firebase.b.b.a(aVar);
        aVar.h();
        return aVar;
    }

    private static <T> void a(Class<T> cls, T t, Iterable<String> iterable, boolean z) {
        for (String str : iterable) {
            if (z) {
                try {
                    if (!e.contains(str)) {
                    }
                } catch (ClassNotFoundException e2) {
                    if (f.contains(str)) {
                        throw new IllegalStateException(String.valueOf(str).concat(" is missing, but is required. Check if it has been removed by Proguard."));
                    }
                    Log.d("FirebaseApp", String.valueOf(str).concat(" is not linked. Skipping initialization."));
                } catch (NoSuchMethodException e3) {
                    throw new IllegalStateException(String.valueOf(str).concat("#getInstance has been removed by Proguard. Add keep rule to prevent it."));
                } catch (InvocationTargetException e4) {
                    Log.wtf("FirebaseApp", "Firebase API initialization failure.", e4);
                } catch (IllegalAccessException e5) {
                    String str2 = "FirebaseApp";
                    String str3 = "Failed to initialize ";
                    String valueOf = String.valueOf(str);
                    Log.wtf(str2, valueOf.length() != 0 ? str3.concat(valueOf) : new String(str3), e5);
                }
            }
            Method method = Class.forName(str).getMethod("getInstance", new Class[]{cls});
            int modifiers = method.getModifiers();
            if (Modifier.isPublic(modifiers) && Modifier.isStatic(modifiers)) {
                method.invoke(null, new Object[]{t});
            }
        }
    }

    public static void a(boolean z) {
        synchronized (g) {
            ArrayList arrayList = new ArrayList(f4148a.values());
            int size = arrayList.size();
            int i2 = 0;
            while (i2 < size) {
                Object obj = arrayList.get(i2);
                i2++;
                a aVar = (a) obj;
                if (aVar.l.get()) {
                    aVar.b(z);
                }
            }
        }
    }

    private final void b(boolean z) {
        Log.d("FirebaseApp", "Notifying background state change listeners.");
        for (C0068a a2 : this.o) {
            a2.a(z);
        }
    }

    public static a d() {
        a aVar;
        synchronized (g) {
            aVar = (a) f4148a.get("[DEFAULT]");
            if (aVar == null) {
                String a2 = com.google.android.gms.common.util.p.a();
                throw new IllegalStateException(new StringBuilder(String.valueOf(a2).length() + 116).append("Default FirebaseApp is not initialized in this process ").append(a2).append(". Make sure to call FirebaseApp.initializeApp(Context) first.").toString());
            }
        }
        return aVar;
    }

    private final void g() {
        aa.a(!this.m.get(), (Object) "FirebaseApp was deleted");
    }

    /* access modifiers changed from: private */
    public final void h() {
        boolean b2 = android.support.v4.b.a.b(this.h);
        if (b2) {
            c.b(this.h);
        } else {
            this.k.a(e());
        }
        a(a.class, this, f4149b, b2);
        if (e()) {
            a(a.class, this, c, b2);
            a(Context.class, this.h, d, b2);
        }
    }

    public Context a() {
        g();
        return this.h;
    }

    public <T> T a(Class<T> cls) {
        g();
        return this.k.a((Class) cls);
    }

    public String b() {
        g();
        return this.i;
    }

    public b c() {
        g();
        return this.j;
    }

    public boolean e() {
        return "[DEFAULT]".equals(b());
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof a)) {
            return false;
        }
        return this.i.equals(((a) obj).b());
    }

    public int hashCode() {
        return this.i.hashCode();
    }

    public String toString() {
        return z.a((Object) this).a("name", this.i).a("options", this.j).toString();
    }
}
