package com.google.firebase.components;

public final class f {

    /* renamed from: a reason: collision with root package name */
    private final Class<?> f4159a;

    /* renamed from: b reason: collision with root package name */
    private final int f4160b;
    private final int c;

    private f(Class<?> cls, int i, int i2) {
        this.f4159a = (Class) t.a(cls, "Null dependency interface.");
        this.f4160b = i;
        this.c = i2;
    }

    public static f a(Class<?> cls) {
        return new f(cls, 1, 0);
    }

    public final Class<?> a() {
        return this.f4159a;
    }

    public final boolean b() {
        return this.f4160b == 1;
    }

    public final boolean c() {
        return this.c == 0;
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof f)) {
            return false;
        }
        f fVar = (f) obj;
        return this.f4159a == fVar.f4159a && this.f4160b == fVar.f4160b && this.c == fVar.c;
    }

    public final int hashCode() {
        return ((((this.f4159a.hashCode() ^ 1000003) * 1000003) ^ this.f4160b) * 1000003) ^ this.c;
    }

    public final String toString() {
        boolean z = true;
        StringBuilder append = new StringBuilder("Dependency{interface=").append(this.f4159a).append(", required=").append(this.f4160b == 1).append(", direct=");
        if (this.c != 0) {
            z = false;
        }
        return append.append(z).append("}").toString();
    }
}
