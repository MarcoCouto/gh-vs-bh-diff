package com.google.firebase.components;

import com.google.firebase.a.a;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public final class u implements b {

    /* renamed from: a reason: collision with root package name */
    private final Set<Class<?>> f4173a;

    /* renamed from: b reason: collision with root package name */
    private final Set<Class<?>> f4174b;
    private final b c;

    public u(Iterable<f> iterable, b bVar) {
        HashSet hashSet = new HashSet();
        HashSet hashSet2 = new HashSet();
        for (f fVar : iterable) {
            if (fVar.c()) {
                hashSet.add(fVar.a());
            } else {
                hashSet2.add(fVar.a());
            }
        }
        this.f4173a = Collections.unmodifiableSet(hashSet);
        this.f4174b = Collections.unmodifiableSet(hashSet2);
        this.c = bVar;
    }

    public final <T> T a(Class<T> cls) {
        if (this.f4173a.contains(cls)) {
            return this.c.a(cls);
        }
        throw new IllegalArgumentException(String.format("Requesting %s is not allowed.", new Object[]{cls}));
    }

    public final <T> a<T> b(Class<T> cls) {
        if (this.f4174b.contains(cls)) {
            return this.c.b(cls);
        }
        throw new IllegalArgumentException(String.format("Requesting Provider<%s> is not allowed.", new Object[]{cls}));
    }
}
