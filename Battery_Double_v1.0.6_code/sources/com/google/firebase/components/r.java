package com.google.firebase.components;

import com.google.firebase.a.a;

final class r<T> implements a<T> {

    /* renamed from: a reason: collision with root package name */
    private static final Object f4169a = new Object();

    /* renamed from: b reason: collision with root package name */
    private volatile Object f4170b = f4169a;
    private volatile a<T> c;

    r(d<T> dVar, b bVar) {
        this.c = new s(dVar, bVar);
    }

    public final T a() {
        T t = this.f4170b;
        if (t == f4169a) {
            synchronized (this) {
                t = this.f4170b;
                if (t == f4169a) {
                    t = this.c.a();
                    this.f4170b = t;
                    this.c = null;
                }
            }
        }
        return t;
    }
}
