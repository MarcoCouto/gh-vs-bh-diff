package com.google.firebase.components;

import java.util.HashSet;
import java.util.Set;

final class q {

    /* renamed from: a reason: collision with root package name */
    private final a<?> f4167a;

    /* renamed from: b reason: collision with root package name */
    private final Set<q> f4168b = new HashSet();
    private final Set<q> c = new HashSet();

    q(a<?> aVar) {
        this.f4167a = aVar;
    }

    /* access modifiers changed from: 0000 */
    public final Set<q> a() {
        return this.f4168b;
    }

    /* access modifiers changed from: 0000 */
    public final void a(q qVar) {
        this.f4168b.add(qVar);
    }

    /* access modifiers changed from: 0000 */
    public final a<?> b() {
        return this.f4167a;
    }

    /* access modifiers changed from: 0000 */
    public final void b(q qVar) {
        this.c.add(qVar);
    }

    /* access modifiers changed from: 0000 */
    public final void c(q qVar) {
        this.c.remove(qVar);
    }

    /* access modifiers changed from: 0000 */
    public final boolean c() {
        return this.c.isEmpty();
    }

    /* access modifiers changed from: 0000 */
    public final boolean d() {
        return this.f4168b.isEmpty();
    }
}
