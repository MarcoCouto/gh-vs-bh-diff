package com.google.firebase.components;

import java.util.Arrays;
import java.util.List;

public class g extends h {

    /* renamed from: a reason: collision with root package name */
    private final List<a<?>> f4161a;

    public g(List<a<?>> list) {
        String str = "Dependency cycle detected: ";
        String valueOf = String.valueOf(Arrays.toString(list.toArray()));
        super(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
        this.f4161a = list;
    }
}
