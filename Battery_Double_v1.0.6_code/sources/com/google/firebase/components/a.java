package com.google.firebase.components;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public final class a<T> {

    /* renamed from: a reason: collision with root package name */
    private final Set<Class<? super T>> f4155a;

    /* renamed from: b reason: collision with root package name */
    private final Set<f> f4156b;
    private final int c;
    private final d<T> d;

    /* renamed from: com.google.firebase.components.a$a reason: collision with other inner class name */
    public static class C0069a<T> {

        /* renamed from: a reason: collision with root package name */
        private final Set<Class<? super T>> f4157a;

        /* renamed from: b reason: collision with root package name */
        private final Set<f> f4158b;
        private int c;
        private d<T> d;

        private C0069a(Class<T> cls, Class<? super T>... clsArr) {
            this.f4157a = new HashSet();
            this.f4158b = new HashSet();
            this.c = 0;
            t.a(cls, "Null interface");
            this.f4157a.add(cls);
            for (Class<? super T> a2 : clsArr) {
                t.a(a2, "Null interface");
            }
            Collections.addAll(this.f4157a, clsArr);
        }

        private final C0069a<T> a(int i) {
            t.a(this.c == 0, "Instantiation type has already been set.");
            this.c = i;
            return this;
        }

        public C0069a<T> a() {
            return a(1);
        }

        public C0069a<T> a(d<T> dVar) {
            this.d = (d) t.a(dVar, "Null factory");
            return this;
        }

        public C0069a<T> a(f fVar) {
            t.a(fVar, "Null dependency");
            String str = "Components are not allowed to depend on interfaces they themselves provide.";
            if (!(!this.f4157a.contains(fVar.a()))) {
                throw new IllegalArgumentException(str);
            }
            this.f4158b.add(fVar);
            return this;
        }

        public a<T> b() {
            t.a(this.d != null, "Missing required property: factory.");
            return new a<>(new HashSet(this.f4157a), new HashSet(this.f4158b), this.c, this.d);
        }
    }

    private a(Set<Class<? super T>> set, Set<f> set2, int i, d<T> dVar) {
        this.f4155a = Collections.unmodifiableSet(set);
        this.f4156b = Collections.unmodifiableSet(set2);
        this.c = i;
        this.d = dVar;
    }

    public static <T> C0069a<T> a(Class<T> cls) {
        return new C0069a<>(cls, new Class[0]);
    }

    public static <T> a<T> a(Class<T> cls, T t) {
        return a(cls).a((d<T>) new j<T>(t)).b();
    }

    public final Set<Class<? super T>> a() {
        return this.f4155a;
    }

    public final Set<f> b() {
        return this.f4156b;
    }

    public final d<T> c() {
        return this.d;
    }

    public final boolean d() {
        return this.c == 1;
    }

    public final boolean e() {
        return this.c == 2;
    }

    public final String toString() {
        return "Component<" + Arrays.toString(this.f4155a.toArray()) + ">{" + this.c + ", deps=" + Arrays.toString(this.f4156b.toArray()) + "}";
    }
}
