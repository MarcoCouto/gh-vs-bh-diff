package com.google.firebase.components;

import android.content.Context;
import android.util.Log;
import java.util.ArrayList;
import java.util.List;

public final class l {

    /* renamed from: a reason: collision with root package name */
    private final Context f4163a;

    /* renamed from: b reason: collision with root package name */
    private final o f4164b;

    public l(Context context) {
        this(context, new n());
    }

    private l(Context context, o oVar) {
        this.f4163a = context;
        this.f4164b = oVar;
    }

    private static List<e> a(List<String> list) {
        ArrayList arrayList = new ArrayList();
        for (String str : list) {
            try {
                Class cls = Class.forName(str);
                if (!e.class.isAssignableFrom(cls)) {
                    Log.w("ComponentDiscovery", String.format("Class %s is not an instance of %s", new Object[]{str, "com.google.firebase.components.ComponentRegistrar"}));
                } else {
                    arrayList.add((e) cls.newInstance());
                }
            } catch (ClassNotFoundException e) {
                Log.w("ComponentDiscovery", String.format("Class %s is not an found.", new Object[]{str}), e);
            } catch (IllegalAccessException e2) {
                Log.w("ComponentDiscovery", String.format("Could not instantiate %s.", new Object[]{str}), e2);
            } catch (InstantiationException e3) {
                Log.w("ComponentDiscovery", String.format("Could not instantiate %s.", new Object[]{str}), e3);
            }
        }
        return arrayList;
    }

    public final List<e> a() {
        return a(this.f4164b.a(this.f4163a));
    }
}
