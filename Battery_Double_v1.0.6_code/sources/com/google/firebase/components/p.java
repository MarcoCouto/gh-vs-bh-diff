package com.google.firebase.components;

import com.google.firebase.a.a;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public final class p implements b {

    /* renamed from: a reason: collision with root package name */
    private final List<a<?>> f4165a;

    /* renamed from: b reason: collision with root package name */
    private final Map<Class<?>, r<?>> f4166b = new HashMap();

    public p(Iterable<e> iterable, a<?>... aVarArr) {
        ArrayList<a> arrayList = new ArrayList<>();
        for (e components : iterable) {
            arrayList.addAll(components.getComponents());
        }
        Collections.addAll(arrayList, aVarArr);
        HashMap hashMap = new HashMap(arrayList.size());
        for (a aVar : arrayList) {
            q qVar = new q(aVar);
            Iterator it = aVar.a().iterator();
            while (true) {
                if (it.hasNext()) {
                    Class cls = (Class) it.next();
                    if (hashMap.put(cls, qVar) != null) {
                        throw new IllegalArgumentException(String.format("Multiple components provide %s.", new Object[]{cls}));
                    }
                }
            }
        }
        for (q qVar2 : hashMap.values()) {
            for (f fVar : qVar2.b().b()) {
                if (fVar.c()) {
                    q qVar3 = (q) hashMap.get(fVar.a());
                    if (qVar3 != null) {
                        qVar2.a(qVar3);
                        qVar3.b(qVar2);
                    }
                }
            }
        }
        HashSet<q> hashSet = new HashSet<>(hashMap.values());
        HashSet hashSet2 = new HashSet();
        for (q qVar4 : hashSet) {
            if (qVar4.c()) {
                hashSet2.add(qVar4);
            }
        }
        ArrayList arrayList2 = new ArrayList();
        while (!hashSet2.isEmpty()) {
            q qVar5 = (q) hashSet2.iterator().next();
            hashSet2.remove(qVar5);
            arrayList2.add(qVar5.b());
            for (q qVar6 : qVar5.a()) {
                qVar6.c(qVar5);
                if (qVar6.c()) {
                    hashSet2.add(qVar6);
                }
            }
        }
        if (arrayList2.size() == arrayList.size()) {
            Collections.reverse(arrayList2);
            this.f4165a = Collections.unmodifiableList(arrayList2);
            for (a aVar2 : this.f4165a) {
                r rVar = new r(aVar2.c(), new u(aVar2.b(), this));
                for (Class put : aVar2.a()) {
                    this.f4166b.put(put, rVar);
                }
            }
            for (a aVar3 : this.f4165a) {
                Iterator it2 = aVar3.b().iterator();
                while (true) {
                    if (it2.hasNext()) {
                        f fVar2 = (f) it2.next();
                        if (fVar2.b() && !this.f4166b.containsKey(fVar2.a())) {
                            throw new i(String.format("Unsatisfied dependency for component %s: %s", new Object[]{aVar3, fVar2.a()}));
                        }
                    }
                }
            }
            return;
        }
        ArrayList arrayList3 = new ArrayList();
        for (q qVar7 : hashSet) {
            if (!qVar7.c() && !qVar7.d()) {
                arrayList3.add(qVar7.b());
            }
        }
        throw new g(arrayList3);
    }

    public final Object a(Class cls) {
        return c.a(this, cls);
    }

    public final void a(boolean z) {
        for (a aVar : this.f4165a) {
            if (aVar.d() || (aVar.e() && z)) {
                a((Class) aVar.a().iterator().next());
            }
        }
    }

    public final <T> a<T> b(Class<T> cls) {
        t.a(cls, "Null interface requested.");
        return (a) this.f4166b.get(cls);
    }
}
