package com.google.firebase.analytics;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Keep;
import com.google.android.gms.common.internal.aa;
import com.google.android.gms.d.g;
import com.google.android.gms.internal.c.ce;

@Keep
public final class FirebaseAnalytics {
    private final ce zzacw;

    public static class a {
    }

    public static class b {
    }

    public static class c {
    }

    public FirebaseAnalytics(ce ceVar) {
        aa.a(ceVar);
        this.zzacw = ceVar;
    }

    @Keep
    public static FirebaseAnalytics getInstance(Context context) {
        return ce.a(context).l();
    }

    public final g<String> getAppInstanceId() {
        return this.zzacw.h().A();
    }

    public final void logEvent(String str, Bundle bundle) {
        this.zzacw.i().logEvent(str, bundle);
    }

    public final void resetAnalyticsData() {
        this.zzacw.h().C();
    }

    public final void setAnalyticsCollectionEnabled(boolean z) {
        this.zzacw.i().setMeasurementEnabled(z);
    }

    @Keep
    public final void setCurrentScreen(Activity activity, String str, String str2) {
        this.zzacw.r().a(activity, str, str2);
    }

    public final void setMinimumSessionDuration(long j) {
        this.zzacw.i().setMinimumSessionDuration(j);
    }

    public final void setSessionTimeoutDuration(long j) {
        this.zzacw.i().setSessionTimeoutDuration(j);
    }

    public final void setUserId(String str) {
        this.zzacw.i().setUserPropertyInternal("app", "_id", str);
    }

    public final void setUserProperty(String str, String str2) {
        this.zzacw.i().setUserProperty(str, str2);
    }
}
