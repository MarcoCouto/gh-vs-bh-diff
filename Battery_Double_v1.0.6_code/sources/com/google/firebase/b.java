package com.google.firebase;

import android.content.Context;
import android.text.TextUtils;
import com.google.android.gms.common.internal.aa;
import com.google.android.gms.common.internal.ag;
import com.google.android.gms.common.internal.z;
import com.google.android.gms.common.util.r;

public final class b {

    /* renamed from: a reason: collision with root package name */
    private final String f4152a;

    /* renamed from: b reason: collision with root package name */
    private final String f4153b;
    private final String c;
    private final String d;
    private final String e;
    private final String f;
    private final String g;

    private b(String str, String str2, String str3, String str4, String str5, String str6, String str7) {
        aa.a(!r.b(str), (Object) "ApplicationId must be set.");
        this.f4153b = str;
        this.f4152a = str2;
        this.c = str3;
        this.d = str4;
        this.e = str5;
        this.f = str6;
        this.g = str7;
    }

    public static b a(Context context) {
        ag agVar = new ag(context);
        String a2 = agVar.a("google_app_id");
        if (TextUtils.isEmpty(a2)) {
            return null;
        }
        return new b(a2, agVar.a("google_api_key"), agVar.a("firebase_database_url"), agVar.a("ga_trackingId"), agVar.a("gcm_defaultSenderId"), agVar.a("google_storage_bucket"), agVar.a("project_id"));
    }

    public final String a() {
        return this.f4153b;
    }

    public final String b() {
        return this.e;
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof b)) {
            return false;
        }
        b bVar = (b) obj;
        return z.a(this.f4153b, bVar.f4153b) && z.a(this.f4152a, bVar.f4152a) && z.a(this.c, bVar.c) && z.a(this.d, bVar.d) && z.a(this.e, bVar.e) && z.a(this.f, bVar.f) && z.a(this.g, bVar.g);
    }

    public final int hashCode() {
        return z.a(this.f4153b, this.f4152a, this.c, this.d, this.e, this.f, this.g);
    }

    public final String toString() {
        return z.a((Object) this).a("applicationId", this.f4153b).a("apiKey", this.f4152a).a("databaseUrl", this.c).a("gcmSenderId", this.e).a("storageBucket", this.f).a("projectId", this.g).toString();
    }
}
