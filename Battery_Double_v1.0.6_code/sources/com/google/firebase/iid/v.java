package com.google.firebase.iid;

import android.content.Intent;
import android.util.Log;

final class v implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ Intent f4229a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ u f4230b;

    v(u uVar, Intent intent) {
        this.f4230b = uVar;
        this.f4229a = intent;
    }

    public final void run() {
        String action = this.f4229a.getAction();
        Log.w("EnhancedIntentService", new StringBuilder(String.valueOf(action).length() + 61).append("Service took too long to process intent: ").append(action).append(" App may get closed.").toString());
        this.f4230b.a();
    }
}
