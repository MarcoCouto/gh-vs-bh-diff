package com.google.firebase.iid;

import android.os.Bundle;
import com.google.android.gms.d.a;
import com.google.android.gms.d.g;
import java.io.IOException;

final class ae implements a<Bundle, String> {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ ac f4186a;

    ae(ac acVar) {
        this.f4186a = acVar;
    }

    public final /* synthetic */ Object a(g gVar) throws Exception {
        return this.f4186a.a((Bundle) gVar.a(IOException.class));
    }
}
