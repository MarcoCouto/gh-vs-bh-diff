package com.google.firebase.iid;

import android.util.Base64;
import com.google.android.gms.common.internal.z;
import java.security.KeyPair;

final class ah {

    /* renamed from: a reason: collision with root package name */
    private final KeyPair f4187a;
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public final long f4188b;

    ah(KeyPair keyPair, long j) {
        this.f4187a = keyPair;
        this.f4188b = j;
    }

    /* access modifiers changed from: private */
    public final String b() {
        return Base64.encodeToString(this.f4187a.getPublic().getEncoded(), 11);
    }

    /* access modifiers changed from: private */
    public final String c() {
        return Base64.encodeToString(this.f4187a.getPrivate().getEncoded(), 11);
    }

    /* access modifiers changed from: 0000 */
    public final KeyPair a() {
        return this.f4187a;
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof ah)) {
            return false;
        }
        ah ahVar = (ah) obj;
        return this.f4188b == ahVar.f4188b && this.f4187a.getPublic().equals(ahVar.f4187a.getPublic()) && this.f4187a.getPrivate().equals(ahVar.f4187a.getPrivate());
    }

    public final int hashCode() {
        return z.a(this.f4187a.getPublic(), this.f4187a.getPrivate(), Long.valueOf(this.f4188b));
    }
}
