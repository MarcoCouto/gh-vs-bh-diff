package com.google.firebase.iid;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

final class m extends Handler {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ l f4213a;

    m(l lVar, Looper looper) {
        this.f4213a = lVar;
        super(looper);
    }

    public final void handleMessage(Message message) {
        this.f4213a.a(message);
    }
}
