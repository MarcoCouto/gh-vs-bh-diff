package com.google.firebase.iid;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.Keep;
import android.util.Log;
import com.google.android.gms.d.g;
import com.google.android.gms.d.j;
import com.google.android.gms.measurement.AppMeasurement;
import com.google.firebase.a;
import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class FirebaseInstanceId {

    /* renamed from: a reason: collision with root package name */
    private static final long f4175a = TimeUnit.HOURS.toSeconds(8);

    /* renamed from: b reason: collision with root package name */
    private static o f4176b;
    private static ScheduledThreadPoolExecutor c;
    private final a d;
    private final e e;
    private final af f;
    private final h g;
    private boolean h;
    private boolean i;

    FirebaseInstanceId(a aVar) {
        this(aVar, new e(aVar.a()));
    }

    private FirebaseInstanceId(a aVar, e eVar) {
        this.g = new h();
        this.h = false;
        if (e.a(aVar) == null) {
            throw new IllegalStateException("FirebaseInstanceId failed to initialize, FirebaseApp is missing project ID");
        }
        synchronized (FirebaseInstanceId.class) {
            if (f4176b == null) {
                f4176b = new o(aVar.a());
            }
        }
        this.d = aVar;
        this.e = eVar;
        this.f = new ac(aVar, this, eVar);
        this.i = n();
        if (k()) {
            l();
        }
    }

    public static FirebaseInstanceId a() {
        return getInstance(a.d());
    }

    private static <T> T a(g<T> gVar) throws IOException {
        try {
            return j.a(gVar);
        } catch (ExecutionException e2) {
            Throwable cause = e2.getCause();
            if (cause instanceof IOException) {
                throw ((IOException) cause);
            } else if (cause instanceof RuntimeException) {
                throw new IOException(cause);
            } else {
                throw new IOException(e2);
            }
        } catch (InterruptedException e3) {
            throw new IOException("SERVICE_NOT_AVAILABLE");
        }
    }

    private final String a(String str, String str2, Bundle bundle) throws IOException {
        return ((ac) this.f).a(str, str2, bundle);
    }

    static void a(Runnable runnable, long j) {
        synchronized (FirebaseInstanceId.class) {
            if (c == null) {
                c = new ScheduledThreadPoolExecutor(1);
            }
            c.schedule(runnable, j, TimeUnit.SECONDS);
        }
    }

    private static String c(String str) {
        return (str.isEmpty() || str.equalsIgnoreCase(AppMeasurement.FCM_ORIGIN) || str.equalsIgnoreCase("gcm")) ? "*" : str;
    }

    static String d() {
        return e.a(f4176b.b("").a());
    }

    static o g() {
        return f4176b;
    }

    @Keep
    public static synchronized FirebaseInstanceId getInstance(a aVar) {
        FirebaseInstanceId firebaseInstanceId;
        synchronized (FirebaseInstanceId.class) {
            firebaseInstanceId = (FirebaseInstanceId) aVar.a(FirebaseInstanceId.class);
        }
        return firebaseInstanceId;
    }

    static boolean h() {
        return Log.isLoggable("FirebaseInstanceId", 3) || (VERSION.SDK_INT == 23 && Log.isLoggable("FirebaseInstanceId", 3));
    }

    private final void l() {
        p e2 = e();
        if (e2 == null || e2.b(this.e.b()) || f4176b.a() != null) {
            m();
        }
    }

    private final synchronized void m() {
        if (!this.h) {
            a(0);
        }
    }

    private final boolean n() {
        Context a2 = this.d.a();
        SharedPreferences sharedPreferences = a2.getSharedPreferences("com.google.firebase.messaging", 0);
        if (sharedPreferences.contains("auto_init")) {
            return sharedPreferences.getBoolean("auto_init", true);
        }
        try {
            PackageManager packageManager = a2.getPackageManager();
            if (packageManager != null) {
                ApplicationInfo applicationInfo = packageManager.getApplicationInfo(a2.getPackageName(), 128);
                if (!(applicationInfo == null || applicationInfo.metaData == null || !applicationInfo.metaData.containsKey("firebase_messaging_auto_init_enabled"))) {
                    return applicationInfo.metaData.getBoolean("firebase_messaging_auto_init_enabled");
                }
            }
        } catch (NameNotFoundException e2) {
        }
        return o();
    }

    private final boolean o() {
        try {
            Class.forName("com.google.firebase.messaging.FirebaseMessaging");
            return true;
        } catch (ClassNotFoundException e2) {
            Context a2 = this.d.a();
            Intent intent = new Intent("com.google.firebase.MESSAGING_EVENT");
            intent.setPackage(a2.getPackageName());
            ResolveInfo resolveService = a2.getPackageManager().resolveService(intent, 0);
            return (resolveService == null || resolveService.serviceInfo == null) ? false : true;
        }
    }

    public String a(String str, String str2) throws IOException {
        if (Looper.getMainLooper() == Looper.myLooper()) {
            throw new IOException("MAIN_THREAD");
        }
        String c2 = c(str2);
        p a2 = f4176b.a("", str, c2);
        return (a2 == null || a2.b(this.e.b())) ? this.g.a(str, c2, (k) new ab(this, str, c2)) : a2.f4219a;
    }

    /* access modifiers changed from: 0000 */
    public final synchronized void a(long j) {
        a((Runnable) new q(this, this.e, Math.min(Math.max(30, j << 1), f4175a)), j);
        this.h = true;
    }

    /* access modifiers changed from: 0000 */
    public final void a(String str) throws IOException {
        p e2 = e();
        if (e2 == null || e2.b(this.e.b())) {
            throw new IOException("token not available");
        }
        Bundle bundle = new Bundle();
        String str2 = "gcm.topic";
        String valueOf = String.valueOf("/topics/");
        String valueOf2 = String.valueOf(str);
        bundle.putString(str2, valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf));
        String str3 = e2.f4219a;
        String valueOf3 = String.valueOf("/topics/");
        String valueOf4 = String.valueOf(str);
        a(str3, valueOf4.length() != 0 ? valueOf3.concat(valueOf4) : new String(valueOf3), bundle);
    }

    /* access modifiers changed from: 0000 */
    public final synchronized void a(boolean z) {
        this.h = z;
    }

    /* access modifiers changed from: 0000 */
    public final a b() {
        return this.d;
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ String b(String str, String str2) throws IOException {
        String str3 = (String) a(this.f.a(str, str2));
        f4176b.a("", str, str2, str3, this.e.b());
        return str3;
    }

    /* access modifiers changed from: 0000 */
    public final void b(String str) throws IOException {
        p e2 = e();
        if (e2 == null || e2.b(this.e.b())) {
            throw new IOException("token not available");
        }
        Bundle bundle = new Bundle();
        String str2 = "gcm.topic";
        String valueOf = String.valueOf("/topics/");
        String valueOf2 = String.valueOf(str);
        bundle.putString(str2, valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf));
        bundle.putString("delete", "1");
        String str3 = e2.f4219a;
        String valueOf3 = String.valueOf("/topics/");
        String valueOf4 = String.valueOf(str);
        a(str3, valueOf4.length() != 0 ? valueOf3.concat(valueOf4) : new String(valueOf3), bundle);
    }

    public String c() {
        l();
        return d();
    }

    /* access modifiers changed from: 0000 */
    public final p e() {
        return f4176b.a("", e.a(this.d), "*");
    }

    /* access modifiers changed from: 0000 */
    public final String f() throws IOException {
        return a(e.a(this.d), "*");
    }

    /* access modifiers changed from: 0000 */
    public final synchronized void i() {
        f4176b.b();
        if (k()) {
            m();
        }
    }

    /* access modifiers changed from: 0000 */
    public final void j() {
        f4176b.c("");
        m();
    }

    public final synchronized boolean k() {
        return this.i;
    }
}
