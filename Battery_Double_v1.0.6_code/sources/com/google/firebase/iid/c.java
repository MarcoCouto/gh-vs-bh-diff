package com.google.firebase.iid;

public final class c extends Exception {

    /* renamed from: a reason: collision with root package name */
    private final int f4202a;

    public c(int i, String str) {
        super(str);
        this.f4202a = i;
    }

    public final int a() {
        return this.f4202a;
    }
}
