package com.google.firebase.iid;

final /* synthetic */ class ab implements k {

    /* renamed from: a reason: collision with root package name */
    private final FirebaseInstanceId f4180a;

    /* renamed from: b reason: collision with root package name */
    private final String f4181b;
    private final String c;

    ab(FirebaseInstanceId firebaseInstanceId, String str, String str2) {
        this.f4180a = firebaseInstanceId;
        this.f4181b = str;
        this.c = str2;
    }

    public final String a() {
        return this.f4180a.b(this.f4181b, this.c);
    }
}
