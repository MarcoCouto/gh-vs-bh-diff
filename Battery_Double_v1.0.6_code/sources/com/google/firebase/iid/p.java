package com.google.firebase.iid;

import android.text.TextUtils;
import android.util.Log;
import java.util.concurrent.TimeUnit;
import org.json.JSONException;
import org.json.JSONObject;

final class p {

    /* renamed from: b reason: collision with root package name */
    private static final long f4218b = TimeUnit.DAYS.toMillis(7);

    /* renamed from: a reason: collision with root package name */
    final String f4219a;
    private final String c;
    private final long d;

    private p(String str, String str2, long j) {
        this.f4219a = str;
        this.c = str2;
        this.d = j;
    }

    static p a(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        if (!str.startsWith("{")) {
            return new p(str, null, 0);
        }
        try {
            JSONObject jSONObject = new JSONObject(str);
            return new p(jSONObject.getString("token"), jSONObject.getString("appVersion"), jSONObject.getLong("timestamp"));
        } catch (JSONException e) {
            String valueOf = String.valueOf(e);
            Log.w("FirebaseInstanceId", new StringBuilder(String.valueOf(valueOf).length() + 23).append("Failed to parse token: ").append(valueOf).toString());
            return null;
        }
    }

    static String a(String str, String str2, long j) {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("token", str);
            jSONObject.put("appVersion", str2);
            jSONObject.put("timestamp", j);
            return jSONObject.toString();
        } catch (JSONException e) {
            String valueOf = String.valueOf(e);
            Log.w("FirebaseInstanceId", new StringBuilder(String.valueOf(valueOf).length() + 24).append("Failed to encode token: ").append(valueOf).toString());
            return null;
        }
    }

    /* access modifiers changed from: 0000 */
    public final boolean b(String str) {
        return System.currentTimeMillis() > this.d + f4218b || !str.equals(this.c);
    }
}
