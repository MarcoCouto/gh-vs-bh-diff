package com.google.firebase.iid;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.support.v4.b.d;
import android.support.v4.h.m;
import android.util.Log;
import java.util.ArrayDeque;
import java.util.Queue;

public final class n {

    /* renamed from: b reason: collision with root package name */
    private static n f4214b;

    /* renamed from: a reason: collision with root package name */
    final Queue<Intent> f4215a = new ArrayDeque();
    private final m<String, String> c = new m<>();
    private Boolean d = null;
    private final Queue<Intent> e = new ArrayDeque();

    private n() {
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x002c A[Catch:{ SecurityException -> 0x0115, IllegalStateException -> 0x0124 }] */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0043 A[Catch:{ SecurityException -> 0x0115, IllegalStateException -> 0x0124 }] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0049 A[Catch:{ SecurityException -> 0x0115, IllegalStateException -> 0x0124 }] */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x0108 A[SYNTHETIC, Splitter:B:57:0x0108] */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x0121  */
    private final int a(Context context, Intent intent) {
        String str;
        ComponentName startService;
        synchronized (this.c) {
            str = (String) this.c.get(intent.getAction());
        }
        if (str == null) {
            ResolveInfo resolveService = context.getPackageManager().resolveService(intent, 0);
            if (resolveService == null || resolveService.serviceInfo == null) {
                Log.e("FirebaseInstanceId", "Failed to resolve target intent service, skipping classname enforcement");
                if (this.d == null) {
                    this.d = Boolean.valueOf(context.checkCallingOrSelfPermission("android.permission.WAKE_LOCK") == 0);
                }
                if (this.d.booleanValue()) {
                    startService = d.startWakefulService(context, intent);
                } else {
                    startService = context.startService(intent);
                    Log.d("FirebaseInstanceId", "Missing wake lock permission, service start may be delayed");
                }
                if (startService != null) {
                    return -1;
                }
                Log.e("FirebaseInstanceId", "Error while delivering the message: ServiceIntent not found.");
                return 404;
            }
            ServiceInfo serviceInfo = resolveService.serviceInfo;
            if (!context.getPackageName().equals(serviceInfo.packageName) || serviceInfo.name == null) {
                String str2 = serviceInfo.packageName;
                String str3 = serviceInfo.name;
                Log.e("FirebaseInstanceId", new StringBuilder(String.valueOf(str2).length() + 94 + String.valueOf(str3).length()).append("Error resolving target intent service, skipping classname enforcement. Resolved service was: ").append(str2).append("/").append(str3).toString());
                if (this.d == null) {
                }
                if (this.d.booleanValue()) {
                }
                if (startService != null) {
                }
            } else {
                str = serviceInfo.name;
                if (str.startsWith(".")) {
                    String valueOf = String.valueOf(context.getPackageName());
                    String valueOf2 = String.valueOf(str);
                    str = valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf);
                }
                synchronized (this.c) {
                    this.c.put(intent.getAction(), str);
                }
            }
        }
        if (Log.isLoggable("FirebaseInstanceId", 3)) {
            String str4 = "FirebaseInstanceId";
            String str5 = "Restricting intent to a specific service: ";
            String valueOf3 = String.valueOf(str);
            Log.d(str4, valueOf3.length() != 0 ? str5.concat(valueOf3) : new String(str5));
        }
        intent.setClassName(context.getPackageName(), str);
        try {
            if (this.d == null) {
            }
            if (this.d.booleanValue()) {
            }
            if (startService != null) {
            }
        } catch (SecurityException e2) {
            Log.e("FirebaseInstanceId", "Error while delivering the message to the serviceIntent", e2);
            return 401;
        } catch (IllegalStateException e3) {
            String valueOf4 = String.valueOf(e3);
            Log.e("FirebaseInstanceId", new StringBuilder(String.valueOf(valueOf4).length() + 45).append("Failed to start service while in background: ").append(valueOf4).toString());
            return 402;
        }
    }

    public static synchronized n a() {
        n nVar;
        synchronized (n.class) {
            if (f4214b == null) {
                f4214b = new n();
            }
            nVar = f4214b;
        }
        return nVar;
    }

    public final int a(Context context, String str, Intent intent) {
        char c2 = 65535;
        switch (str.hashCode()) {
            case -842411455:
                if (str.equals("com.google.firebase.INSTANCE_ID_EVENT")) {
                    c2 = 0;
                    break;
                }
                break;
            case 41532704:
                if (str.equals("com.google.firebase.MESSAGING_EVENT")) {
                    c2 = 1;
                    break;
                }
                break;
        }
        switch (c2) {
            case 0:
                this.f4215a.offer(intent);
                break;
            case 1:
                this.e.offer(intent);
                break;
            default:
                String str2 = "FirebaseInstanceId";
                String str3 = "Unknown service action: ";
                String valueOf = String.valueOf(str);
                Log.w(str2, valueOf.length() != 0 ? str3.concat(valueOf) : new String(str3));
                return 500;
        }
        Intent intent2 = new Intent(str);
        intent2.setPackage(context.getPackageName());
        return a(context, intent2);
    }
}
