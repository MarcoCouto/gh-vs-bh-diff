package com.google.firebase.iid;

import android.os.Bundle;
import android.util.Log;
import com.google.android.gms.d.h;

abstract class b<T> {

    /* renamed from: a reason: collision with root package name */
    final int f4200a;

    /* renamed from: b reason: collision with root package name */
    final h<T> f4201b = new h<>();
    final int c;
    final Bundle d;

    b(int i, int i2, Bundle bundle) {
        this.f4200a = i;
        this.c = i2;
        this.d = bundle;
    }

    /* access modifiers changed from: 0000 */
    public abstract void a(Bundle bundle);

    /* access modifiers changed from: 0000 */
    public final void a(c cVar) {
        if (Log.isLoggable("MessengerIpcClient", 3)) {
            String valueOf = String.valueOf(this);
            String valueOf2 = String.valueOf(cVar);
            Log.d("MessengerIpcClient", new StringBuilder(String.valueOf(valueOf).length() + 14 + String.valueOf(valueOf2).length()).append("Failing ").append(valueOf).append(" with ").append(valueOf2).toString());
        }
        this.f4201b.a((Exception) cVar);
    }

    /* access modifiers changed from: 0000 */
    public final void a(T t) {
        if (Log.isLoggable("MessengerIpcClient", 3)) {
            String valueOf = String.valueOf(this);
            String valueOf2 = String.valueOf(t);
            Log.d("MessengerIpcClient", new StringBuilder(String.valueOf(valueOf).length() + 16 + String.valueOf(valueOf2).length()).append("Finishing ").append(valueOf).append(" with ").append(valueOf2).toString());
        }
        this.f4201b.a(t);
    }

    /* access modifiers changed from: 0000 */
    public abstract boolean a();

    public String toString() {
        int i = this.c;
        int i2 = this.f4200a;
        return "Request { what=" + i + " id=" + i2 + " oneWay=" + a() + "}";
    }
}
