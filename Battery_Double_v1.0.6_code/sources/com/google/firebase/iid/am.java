package com.google.firebase.iid;

import android.os.Handler.Callback;
import android.os.Message;

final /* synthetic */ class am implements Callback {

    /* renamed from: a reason: collision with root package name */
    private final al f4193a;

    am(al alVar) {
        this.f4193a = alVar;
    }

    public final boolean handleMessage(Message message) {
        return this.f4193a.a(message);
    }
}
