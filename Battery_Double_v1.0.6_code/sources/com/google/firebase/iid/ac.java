package com.google.firebase.iid;

import android.os.Build.VERSION;
import android.os.Bundle;
import android.util.Log;
import com.google.android.gms.d.g;
import com.google.android.gms.d.h;
import com.google.firebase.a;
import java.io.IOException;
import java.util.concurrent.Executor;
import java.util.concurrent.ScheduledThreadPoolExecutor;

final class ac implements af {

    /* renamed from: a reason: collision with root package name */
    private final a f4182a;

    /* renamed from: b reason: collision with root package name */
    private final FirebaseInstanceId f4183b;
    private final e c;
    private final l d;
    private final ScheduledThreadPoolExecutor e = new ScheduledThreadPoolExecutor(1);

    ac(a aVar, FirebaseInstanceId firebaseInstanceId, e eVar) {
        this.f4182a = aVar;
        this.f4183b = firebaseInstanceId;
        this.c = eVar;
        this.d = new l(aVar.a(), eVar);
    }

    /* access modifiers changed from: private */
    public final String a(Bundle bundle) throws IOException {
        if (bundle == null) {
            throw new IOException("SERVICE_NOT_AVAILABLE");
        }
        String string = bundle.getString("registration_id");
        if (string == null) {
            string = bundle.getString("unregistered");
            if (string == null) {
                String string2 = bundle.getString("error");
                if ("RST".equals(string2)) {
                    this.f4183b.i();
                    throw new IOException("INSTANCE_ID_RESET");
                } else if (string2 != null) {
                    throw new IOException(string2);
                } else {
                    String valueOf = String.valueOf(bundle);
                    Log.w("FirebaseInstanceId", new StringBuilder(String.valueOf(valueOf).length() + 21).append("Unexpected response: ").append(valueOf).toString(), new Throwable());
                    throw new IOException("SERVICE_NOT_AVAILABLE");
                }
            }
        }
        return string;
    }

    private final Bundle b(String str, String str2, Bundle bundle) {
        bundle.putString("scope", str2);
        bundle.putString("sender", str);
        bundle.putString("subtype", str);
        bundle.putString("appid", FirebaseInstanceId.d());
        bundle.putString("gmp_app_id", this.f4182a.c().a());
        bundle.putString("gmsv", Integer.toString(this.c.d()));
        bundle.putString("osv", Integer.toString(VERSION.SDK_INT));
        bundle.putString("app_ver", this.c.b());
        bundle.putString("app_ver_name", this.c.c());
        bundle.putString("cliv", "fiid-12451000");
        return bundle;
    }

    public final g<String> a(String str, String str2) {
        Bundle bundle = new Bundle();
        b(str, str2, bundle);
        h hVar = new h();
        this.e.execute(new ad(this, bundle, hVar));
        return hVar.a().a((Executor) this.e, (com.google.android.gms.d.a<TResult, TContinuationResult>) new ae<TResult,TContinuationResult>(this));
    }

    /* access modifiers changed from: 0000 */
    public final String a(String str, String str2, Bundle bundle) throws IOException {
        b(str, str2, bundle);
        return a(this.d.a(bundle));
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ void a(Bundle bundle, h hVar) {
        try {
            hVar.a(this.d.a(bundle));
        } catch (IOException e2) {
            hVar.a((Exception) e2);
        }
    }
}
