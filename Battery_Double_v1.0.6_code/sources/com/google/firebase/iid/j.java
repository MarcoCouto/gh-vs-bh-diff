package com.google.firebase.iid;

import android.util.Pair;
import com.google.android.gms.d.h;

final /* synthetic */ class j implements k {

    /* renamed from: a reason: collision with root package name */
    private final h f4209a;

    /* renamed from: b reason: collision with root package name */
    private final k f4210b;
    private final h c;
    private final Pair d;

    j(h hVar, k kVar, h hVar2, Pair pair) {
        this.f4209a = hVar;
        this.f4210b = kVar;
        this.c = hVar2;
        this.d = pair;
    }

    public final String a() {
        return this.f4209a.a(this.f4210b, this.c, this.d);
    }
}
