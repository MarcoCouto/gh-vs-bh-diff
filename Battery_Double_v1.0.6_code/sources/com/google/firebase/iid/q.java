package com.google.firebase.iid;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.util.Log;
import java.io.IOException;

final class q implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final long f4220a;

    /* renamed from: b reason: collision with root package name */
    private final WakeLock f4221b = ((PowerManager) a().getSystemService("power")).newWakeLock(1, "fiid-sync");
    private final FirebaseInstanceId c;
    private final e d;

    q(FirebaseInstanceId firebaseInstanceId, e eVar, long j) {
        this.c = firebaseInstanceId;
        this.d = eVar;
        this.f4220a = j;
        this.f4221b.setReferenceCounted(false);
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x0056  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0072  */
    private final boolean a(String str) {
        String[] split = str.split("!");
        if (split.length != 2) {
            return true;
        }
        String str2 = split[0];
        String str3 = split[1];
        char c2 = 65535;
        try {
            switch (str2.hashCode()) {
                case 83:
                    if (str2.equals("S")) {
                        c2 = 0;
                        break;
                    }
                    break;
                case 85:
                    if (str2.equals("U")) {
                        c2 = 1;
                        break;
                    }
                    break;
            }
            switch (c2) {
                case 0:
                    this.c.a(str3);
                    if (!FirebaseInstanceId.h()) {
                        return true;
                    }
                    Log.d("FirebaseInstanceId", "subscribe operation succeeded");
                    return true;
                case 1:
                    this.c.b(str3);
                    if (!FirebaseInstanceId.h()) {
                        return true;
                    }
                    Log.d("FirebaseInstanceId", "unsubscribe operation succeeded");
                    return true;
                default:
                    return true;
            }
        } catch (IOException e) {
            String str4 = "FirebaseInstanceId";
            String str5 = "Topic sync failed: ";
            String valueOf = String.valueOf(e.getMessage());
            Log.e(str4, valueOf.length() == 0 ? str5.concat(valueOf) : new String(str5));
            return false;
        }
        String str42 = "FirebaseInstanceId";
        String str52 = "Topic sync failed: ";
        String valueOf2 = String.valueOf(e.getMessage());
        Log.e(str42, valueOf2.length() == 0 ? str52.concat(valueOf2) : new String(str52));
        return false;
    }

    private final boolean c() {
        p e = this.c.e();
        if (e != null && !e.b(this.d.b())) {
            return true;
        }
        try {
            String f = this.c.f();
            if (f == null) {
                Log.e("FirebaseInstanceId", "Token retrieval failed: null");
                return false;
            }
            if (Log.isLoggable("FirebaseInstanceId", 3)) {
                Log.d("FirebaseInstanceId", "Token successfully retrieved");
            }
            if (e != null && (e == null || f.equals(e.f4219a))) {
                return true;
            }
            Context a2 = a();
            Intent intent = new Intent("com.google.firebase.iid.TOKEN_REFRESH");
            Intent intent2 = new Intent("com.google.firebase.INSTANCE_ID_EVENT");
            intent2.setClass(a2, FirebaseInstanceIdReceiver.class);
            intent2.putExtra("wrapped_intent", intent);
            a2.sendBroadcast(intent2);
            return true;
        } catch (IOException | SecurityException e2) {
            String str = "FirebaseInstanceId";
            String str2 = "Token retrieval failed: ";
            String valueOf = String.valueOf(e2.getMessage());
            Log.e(str, valueOf.length() != 0 ? str2.concat(valueOf) : new String(str2));
            return false;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:?, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001c, code lost:
        if (a(r0) != false) goto L_0x0023;
     */
    private final boolean d() {
        String a2;
        while (true) {
            synchronized (this.c) {
                a2 = FirebaseInstanceId.g().a();
                if (a2 == null) {
                    Log.d("FirebaseInstanceId", "topic sync succeeded");
                    return true;
                }
            }
            FirebaseInstanceId.g().a(a2);
        }
        while (true) {
        }
    }

    /* access modifiers changed from: 0000 */
    public final Context a() {
        return this.c.b().a();
    }

    /* access modifiers changed from: 0000 */
    public final boolean b() {
        ConnectivityManager connectivityManager = (ConnectivityManager) a().getSystemService("connectivity");
        NetworkInfo networkInfo = connectivityManager != null ? connectivityManager.getActiveNetworkInfo() : null;
        return networkInfo != null && networkInfo.isConnected();
    }

    public final void run() {
        boolean z = true;
        this.f4221b.acquire();
        try {
            this.c.a(true);
            if (this.d.a() == 0) {
                z = false;
            }
            if (!z) {
                this.c.a(false);
            } else if (!b()) {
                new r(this).a();
                this.f4221b.release();
            } else {
                if (!c() || !d()) {
                    this.c.a(this.f4220a);
                } else {
                    this.c.a(false);
                }
                this.f4221b.release();
            }
        } finally {
            this.f4221b.release();
        }
    }
}
