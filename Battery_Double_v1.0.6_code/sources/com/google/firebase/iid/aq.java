package com.google.firebase.iid;

import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;

final class aq {

    /* renamed from: a reason: collision with root package name */
    private final Messenger f4198a;

    /* renamed from: b reason: collision with root package name */
    private final z f4199b;

    aq(IBinder iBinder) throws RemoteException {
        String interfaceDescriptor = iBinder.getInterfaceDescriptor();
        if ("android.os.IMessenger".equals(interfaceDescriptor)) {
            this.f4198a = new Messenger(iBinder);
            this.f4199b = null;
        } else if ("com.google.android.gms.iid.IMessengerCompat".equals(interfaceDescriptor)) {
            this.f4199b = new z(iBinder);
            this.f4198a = null;
        } else {
            String str = "MessengerIpcClient";
            String str2 = "Invalid interface descriptor: ";
            String valueOf = String.valueOf(interfaceDescriptor);
            Log.w(str, valueOf.length() != 0 ? str2.concat(valueOf) : new String(str2));
            throw new RemoteException();
        }
    }

    /* access modifiers changed from: 0000 */
    public final void a(Message message) throws RemoteException {
        if (this.f4198a != null) {
            this.f4198a.send(message);
        } else if (this.f4199b != null) {
            this.f4199b.a(message);
        } else {
            throw new IllegalStateException("Both messengers are null");
        }
    }
}
