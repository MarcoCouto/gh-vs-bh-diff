package com.google.firebase.iid;

import android.content.Intent;

final class t implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ Intent f4225a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ Intent f4226b;
    private final /* synthetic */ s c;

    t(s sVar, Intent intent, Intent intent2) {
        this.c = sVar;
        this.f4225a = intent;
        this.f4226b = intent2;
    }

    public final void run() {
        this.c.b(this.f4225a);
        this.c.d(this.f4226b);
    }
}
