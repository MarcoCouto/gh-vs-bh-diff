package com.google.firebase.iid;

import android.util.Log;

final class x implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ u f4232a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ w f4233b;

    x(w wVar, u uVar) {
        this.f4233b = wVar;
        this.f4232a = uVar;
    }

    public final void run() {
        if (Log.isLoggable("EnhancedIntentService", 3)) {
            Log.d("EnhancedIntentService", "bg processing of the intent starting now");
        }
        this.f4233b.f4231a.b(this.f4232a.f4227a);
        this.f4232a.a();
    }
}
