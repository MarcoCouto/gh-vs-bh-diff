package com.google.firebase.iid;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import android.util.SparseArray;
import com.google.android.gms.common.a.a;
import com.google.android.gms.common.internal.aa;
import java.util.ArrayDeque;
import java.util.Queue;
import java.util.concurrent.TimeUnit;

final class al implements ServiceConnection {

    /* renamed from: a reason: collision with root package name */
    int f4191a;

    /* renamed from: b reason: collision with root package name */
    final Messenger f4192b;
    aq c;
    final Queue<b<?>> d;
    final SparseArray<b<?>> e;
    final /* synthetic */ aj f;

    private al(aj ajVar) {
        this.f = ajVar;
        this.f4191a = 0;
        this.f4192b = new Messenger(new Handler(Looper.getMainLooper(), new am(this)));
        this.d = new ArrayDeque();
        this.e = new SparseArray<>();
    }

    private final void c() {
        this.f.c.execute(new ao(this));
    }

    /* access modifiers changed from: 0000 */
    public final synchronized void a() {
        if (this.f4191a == 2 && this.d.isEmpty() && this.e.size() == 0) {
            if (Log.isLoggable("MessengerIpcClient", 2)) {
                Log.v("MessengerIpcClient", "Finished handling requests, unbinding");
            }
            this.f4191a = 3;
            a.a().a(this.f.f4190b, this);
        }
    }

    /* access modifiers changed from: 0000 */
    public final synchronized void a(int i) {
        b bVar = (b) this.e.get(i);
        if (bVar != null) {
            Log.w("MessengerIpcClient", "Timing out request: " + i);
            this.e.remove(i);
            bVar.a(new c(3, "Timed out waiting for response"));
            a();
        }
    }

    /* access modifiers changed from: 0000 */
    public final synchronized void a(int i, String str) {
        if (Log.isLoggable("MessengerIpcClient", 3)) {
            String str2 = "MessengerIpcClient";
            String str3 = "Disconnected: ";
            String valueOf = String.valueOf(str);
            Log.d(str2, valueOf.length() != 0 ? str3.concat(valueOf) : new String(str3));
        }
        switch (this.f4191a) {
            case 0:
                throw new IllegalStateException();
            case 1:
            case 2:
                if (Log.isLoggable("MessengerIpcClient", 2)) {
                    Log.v("MessengerIpcClient", "Unbinding service");
                }
                this.f4191a = 4;
                a.a().a(this.f.f4190b, this);
                c cVar = new c(i, str);
                for (b a2 : this.d) {
                    a2.a(cVar);
                }
                this.d.clear();
                for (int i2 = 0; i2 < this.e.size(); i2++) {
                    ((b) this.e.valueAt(i2)).a(cVar);
                }
                this.e.clear();
                break;
            case 3:
                this.f4191a = 4;
                break;
            case 4:
                break;
            default:
                throw new IllegalStateException("Unknown state: " + this.f4191a);
        }
    }

    /* access modifiers changed from: 0000 */
    public final boolean a(Message message) {
        int i = message.arg1;
        if (Log.isLoggable("MessengerIpcClient", 3)) {
            Log.d("MessengerIpcClient", "Received response to request: " + i);
        }
        synchronized (this) {
            b bVar = (b) this.e.get(i);
            if (bVar == null) {
                Log.w("MessengerIpcClient", "Received response for unknown request: " + i);
            } else {
                this.e.remove(i);
                a();
                Bundle data = message.getData();
                if (data.getBoolean("unsupported", false)) {
                    bVar.a(new c(4, "Not supported by GmsCore"));
                } else {
                    bVar.a(data);
                }
            }
        }
        return true;
    }

    /* access modifiers changed from: 0000 */
    public final synchronized boolean a(b bVar) {
        boolean z = false;
        boolean z2 = true;
        synchronized (this) {
            switch (this.f4191a) {
                case 0:
                    this.d.add(bVar);
                    if (this.f4191a == 0) {
                        z = true;
                    }
                    aa.a(z);
                    if (Log.isLoggable("MessengerIpcClient", 2)) {
                        Log.v("MessengerIpcClient", "Starting bind to GmsCore");
                    }
                    this.f4191a = 1;
                    Intent intent = new Intent("com.google.android.c2dm.intent.REGISTER");
                    intent.setPackage("com.google.android.gms");
                    if (a.a().a(this.f.f4190b, intent, (ServiceConnection) this, 1)) {
                        this.f.c.schedule(new an(this), 30, TimeUnit.SECONDS);
                        break;
                    } else {
                        a(0, "Unable to bind to service");
                        break;
                    }
                case 1:
                    this.d.add(bVar);
                    break;
                case 2:
                    this.d.add(bVar);
                    c();
                    break;
                case 3:
                case 4:
                    z2 = false;
                    break;
                default:
                    throw new IllegalStateException("Unknown state: " + this.f4191a);
            }
        }
        return z2;
    }

    /* access modifiers changed from: 0000 */
    public final synchronized void b() {
        if (this.f4191a == 1) {
            a(1, "Timed out while binding");
        }
    }

    public final synchronized void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        if (Log.isLoggable("MessengerIpcClient", 2)) {
            Log.v("MessengerIpcClient", "Service connected");
        }
        if (iBinder == null) {
            a(0, "Null service connection");
        } else {
            try {
                this.c = new aq(iBinder);
                this.f4191a = 2;
                c();
            } catch (RemoteException e2) {
                a(0, e2.getMessage());
            }
        }
        return;
    }

    public final synchronized void onServiceDisconnected(ComponentName componentName) {
        if (Log.isLoggable("MessengerIpcClient", 2)) {
            Log.v("MessengerIpcClient", "Service disconnected");
        }
        a(2, "Service disconnected");
    }
}
