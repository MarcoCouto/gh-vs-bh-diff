package com.google.firebase.iid;

import android.support.v4.h.a;
import android.util.Log;
import android.util.Pair;
import com.google.android.gms.d.j;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ExecutionException;

final class h {

    /* renamed from: a reason: collision with root package name */
    private final Map<Pair<String, String>, com.google.android.gms.d.h<String>> f4207a = new a();

    h() {
    }

    private static String a(k kVar, com.google.android.gms.d.h<String> hVar) throws IOException {
        try {
            String a2 = kVar.a();
            hVar.a(a2);
            return a2;
        } catch (IOException | RuntimeException e) {
            hVar.a(e);
            throw e;
        }
    }

    private final synchronized k b(String str, String str2, k kVar) {
        k jVar;
        Pair pair = new Pair(str, str2);
        com.google.android.gms.d.h hVar = (com.google.android.gms.d.h) this.f4207a.get(pair);
        if (hVar != null) {
            if (Log.isLoggable("FirebaseInstanceId", 3)) {
                String valueOf = String.valueOf(pair);
                Log.d("FirebaseInstanceId", new StringBuilder(String.valueOf(valueOf).length() + 29).append("Joining ongoing request for: ").append(valueOf).toString());
            }
            jVar = new i(hVar);
        } else {
            if (Log.isLoggable("FirebaseInstanceId", 3)) {
                String valueOf2 = String.valueOf(pair);
                Log.d("FirebaseInstanceId", new StringBuilder(String.valueOf(valueOf2).length() + 24).append("Making new request for: ").append(valueOf2).toString());
            }
            com.google.android.gms.d.h hVar2 = new com.google.android.gms.d.h();
            this.f4207a.put(pair, hVar2);
            jVar = new j(this, kVar, hVar2, pair);
        }
        return jVar;
    }

    /* access modifiers changed from: private */
    public static String b(com.google.android.gms.d.h<String> hVar) throws IOException {
        try {
            return (String) j.a(hVar.a());
        } catch (ExecutionException e) {
            Throwable cause = e.getCause();
            if (cause instanceof IOException) {
                throw ((IOException) cause);
            } else if (cause instanceof RuntimeException) {
                throw ((RuntimeException) cause);
            } else {
                throw new IOException(cause);
            }
        } catch (InterruptedException e2) {
            throw new IOException(e2);
        }
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ String a(k kVar, com.google.android.gms.d.h hVar, Pair pair) throws IOException {
        try {
            String a2 = a(kVar, hVar);
            synchronized (this) {
                this.f4207a.remove(pair);
            }
            return a2;
        } catch (Throwable th) {
            synchronized (this) {
                this.f4207a.remove(pair);
                throw th;
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public final String a(String str, String str2, k kVar) throws IOException {
        return b(str, str2, kVar).a();
    }
}
