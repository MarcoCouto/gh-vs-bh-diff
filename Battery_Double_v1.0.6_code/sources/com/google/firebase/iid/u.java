package com.google.firebase.iid;

import android.content.BroadcastReceiver.PendingResult;
import android.content.Intent;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

final class u {

    /* renamed from: a reason: collision with root package name */
    final Intent f4227a;

    /* renamed from: b reason: collision with root package name */
    private final PendingResult f4228b;
    private boolean c = false;
    private final ScheduledFuture<?> d;

    u(Intent intent, PendingResult pendingResult, ScheduledExecutorService scheduledExecutorService) {
        this.f4227a = intent;
        this.f4228b = pendingResult;
        this.d = scheduledExecutorService.schedule(new v(this, intent), 9500, TimeUnit.MILLISECONDS);
    }

    /* access modifiers changed from: 0000 */
    public final synchronized void a() {
        if (!this.c) {
            this.f4228b.finish();
            this.d.cancel(false);
            this.c = true;
        }
    }
}
