package com.google.firebase.iid;

import android.os.Binder;
import android.os.Process;
import android.util.Log;

public final class w extends Binder {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public final s f4231a;

    w(s sVar) {
        this.f4231a = sVar;
    }

    public final void a(u uVar) {
        if (Binder.getCallingUid() != Process.myUid()) {
            throw new SecurityException("Binding only allowed within app");
        }
        if (Log.isLoggable("EnhancedIntentService", 3)) {
            Log.d("EnhancedIntentService", "service received new intent via bind strategy");
        }
        if (this.f4231a.c(uVar.f4227a)) {
            uVar.a();
            return;
        }
        if (Log.isLoggable("EnhancedIntentService", 3)) {
            Log.d("EnhancedIntentService", "intent being queued for bg execution");
        }
        this.f4231a.f4223a.execute(new x(this, uVar));
    }
}
