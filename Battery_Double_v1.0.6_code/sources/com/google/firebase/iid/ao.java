package com.google.firebase.iid;

import java.util.concurrent.TimeUnit;

final /* synthetic */ class ao implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final al f4195a;

    ao(al alVar) {
        this.f4195a = alVar;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0043, code lost:
        if (android.util.Log.isLoggable("MessengerIpcClient", 3) == false) goto L_0x006b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0045, code lost:
        r3 = java.lang.String.valueOf(r0);
        android.util.Log.d("MessengerIpcClient", new java.lang.StringBuilder(java.lang.String.valueOf(r3).length() + 8).append("Sending ").append(r3).toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x006b, code lost:
        r2 = r1.f.f4190b;
        r3 = r1.f4192b;
        r4 = android.os.Message.obtain();
        r4.what = r0.c;
        r4.arg1 = r0.f4200a;
        r4.replyTo = r3;
        r3 = new android.os.Bundle();
        r3.putBoolean("oneWay", r0.a());
        r3.putString("pkg", r2.getPackageName());
        r3.putBundle("data", r0.d);
        r4.setData(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:?, code lost:
        r1.c.a(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x00a9, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x00aa, code lost:
        r1.a(2, r0.getMessage());
     */
    public final void run() {
        al alVar = this.f4195a;
        while (true) {
            synchronized (alVar) {
                if (alVar.f4191a == 2) {
                    if (alVar.d.isEmpty()) {
                        alVar.a();
                        return;
                    }
                    b bVar = (b) alVar.d.poll();
                    alVar.e.put(bVar.f4200a, bVar);
                    alVar.f.c.schedule(new ap(alVar, bVar), 30, TimeUnit.SECONDS);
                } else {
                    return;
                }
            }
        }
    }
}
