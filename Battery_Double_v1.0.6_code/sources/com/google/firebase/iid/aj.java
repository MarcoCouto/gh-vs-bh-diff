package com.google.firebase.iid;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import com.google.android.gms.d.g;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

public final class aj {

    /* renamed from: a reason: collision with root package name */
    private static aj f4189a;
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public final Context f4190b;
    /* access modifiers changed from: private */
    public final ScheduledExecutorService c;
    private al d = new al(this);
    private int e = 1;

    private aj(Context context, ScheduledExecutorService scheduledExecutorService) {
        this.c = scheduledExecutorService;
        this.f4190b = context.getApplicationContext();
    }

    private final synchronized int a() {
        int i;
        i = this.e;
        this.e = i + 1;
        return i;
    }

    private final synchronized <T> g<T> a(b<T> bVar) {
        if (Log.isLoggable("MessengerIpcClient", 3)) {
            String valueOf = String.valueOf(bVar);
            Log.d("MessengerIpcClient", new StringBuilder(String.valueOf(valueOf).length() + 9).append("Queueing ").append(valueOf).toString());
        }
        if (!this.d.a((b) bVar)) {
            this.d = new al(this);
            this.d.a((b) bVar);
        }
        return bVar.f4201b.a();
    }

    public static synchronized aj a(Context context) {
        aj ajVar;
        synchronized (aj.class) {
            if (f4189a == null) {
                f4189a = new aj(context, Executors.newSingleThreadScheduledExecutor());
            }
            ajVar = f4189a;
        }
        return ajVar;
    }

    public final g<Bundle> a(int i, Bundle bundle) {
        return a((b<T>) new d<T>(a(), 1, bundle));
    }
}
