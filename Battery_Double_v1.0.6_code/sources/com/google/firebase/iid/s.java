package com.google.firebase.iid;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.v4.b.d;
import android.util.Log;
import com.google.android.gms.common.util.a.a;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public abstract class s extends Service {

    /* renamed from: a reason: collision with root package name */
    final ExecutorService f4223a;

    /* renamed from: b reason: collision with root package name */
    private Binder f4224b;
    private final Object c;
    private int d;
    private int e;

    public s() {
        String str = "Firebase-";
        String valueOf = String.valueOf(getClass().getSimpleName());
        this.f4223a = Executors.newSingleThreadExecutor(new a(valueOf.length() != 0 ? str.concat(valueOf) : new String(str)));
        this.c = new Object();
        this.e = 0;
    }

    /* access modifiers changed from: private */
    public final void d(Intent intent) {
        if (intent != null) {
            d.completeWakefulIntent(intent);
        }
        synchronized (this.c) {
            this.e--;
            if (this.e == 0) {
                stopSelfResult(this.d);
            }
        }
    }

    /* access modifiers changed from: protected */
    public Intent a(Intent intent) {
        return intent;
    }

    public abstract void b(Intent intent);

    public boolean c(Intent intent) {
        return false;
    }

    public final synchronized IBinder onBind(Intent intent) {
        if (Log.isLoggable("EnhancedIntentService", 3)) {
            Log.d("EnhancedIntentService", "Service received bind request");
        }
        if (this.f4224b == null) {
            this.f4224b = new w(this);
        }
        return this.f4224b;
    }

    public final int onStartCommand(Intent intent, int i, int i2) {
        synchronized (this.c) {
            this.d = i2;
            this.e++;
        }
        Intent a2 = a(intent);
        if (a2 == null) {
            d(intent);
            return 2;
        } else if (c(a2)) {
            d(intent);
            return 2;
        } else {
            this.f4223a.execute(new t(this, a2, intent));
            return 3;
        }
    }
}
