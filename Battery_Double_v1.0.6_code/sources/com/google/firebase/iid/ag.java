package com.google.firebase.iid;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.support.v4.b.a;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import com.google.android.gms.internal.b.h;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Properties;

final class ag {
    ag() {
    }

    private static ah a(SharedPreferences sharedPreferences, String str) throws ai {
        String string = sharedPreferences.getString(o.a(str, "|P|"), null);
        String string2 = sharedPreferences.getString(o.a(str, "|K|"), null);
        if (string == null || string2 == null) {
            return null;
        }
        return new ah(a(string, string2), b(sharedPreferences, str));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0043, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0044, code lost:
        r6 = r1;
        r1 = r0;
        r0 = r6;
     */
    private static ah a(File file) throws ai, IOException {
        Throwable th = null;
        FileInputStream fileInputStream = new FileInputStream(file);
        try {
            Properties properties = new Properties();
            properties.load(fileInputStream);
            String property = properties.getProperty("pub");
            String property2 = properties.getProperty("pri");
            if (property == null || property2 == null) {
                a((Throwable) null, fileInputStream);
                return null;
            }
            ah ahVar = new ah(a(property, property2), Long.parseLong(properties.getProperty("cre")));
            a((Throwable) null, fileInputStream);
            return ahVar;
        } catch (NumberFormatException e) {
            throw new ai(e);
        } catch (Throwable th2) {
            th = th2;
            a(th, fileInputStream);
            throw th;
        }
    }

    private static KeyPair a(String str, String str2) throws ai {
        try {
            byte[] decode = Base64.decode(str, 8);
            byte[] decode2 = Base64.decode(str2, 8);
            try {
                KeyFactory instance = KeyFactory.getInstance("RSA");
                return new KeyPair(instance.generatePublic(new X509EncodedKeySpec(decode)), instance.generatePrivate(new PKCS8EncodedKeySpec(decode2)));
            } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
                String valueOf = String.valueOf(e);
                Log.w("FirebaseInstanceId", new StringBuilder(String.valueOf(valueOf).length() + 19).append("Invalid key stored ").append(valueOf).toString());
                throw new ai(e);
            }
        } catch (IllegalArgumentException e2) {
            throw new ai(e2);
        }
    }

    static void a(Context context) {
        File[] listFiles;
        for (File file : b(context).listFiles()) {
            if (file.getName().startsWith("com.google.InstanceId")) {
                file.delete();
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x004c, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
        a(r1, r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0050, code lost:
        throw r0;
     */
    private static void a(Context context, String str, ah ahVar) {
        try {
            if (Log.isLoggable("FirebaseInstanceId", 3)) {
                Log.d("FirebaseInstanceId", "Writing key to properties file");
            }
            File e = e(context, str);
            e.createNewFile();
            Properties properties = new Properties();
            properties.setProperty("pub", ahVar.b());
            properties.setProperty("pri", ahVar.c());
            properties.setProperty("cre", String.valueOf(ahVar.f4188b));
            FileOutputStream fileOutputStream = new FileOutputStream(e);
            properties.store(fileOutputStream, null);
            a((Throwable) null, fileOutputStream);
        } catch (IOException e2) {
            String valueOf = String.valueOf(e2);
            Log.w("FirebaseInstanceId", new StringBuilder(String.valueOf(valueOf).length() + 21).append("Failed to write key: ").append(valueOf).toString());
        }
    }

    private static /* synthetic */ void a(Throwable th, FileInputStream fileInputStream) {
        if (th != null) {
            try {
                fileInputStream.close();
            } catch (Throwable th2) {
                h.a(th, th2);
            }
        } else {
            fileInputStream.close();
        }
    }

    private static /* synthetic */ void a(Throwable th, FileOutputStream fileOutputStream) {
        if (th != null) {
            try {
                fileOutputStream.close();
            } catch (Throwable th2) {
                h.a(th, th2);
            }
        } else {
            fileOutputStream.close();
        }
    }

    private static long b(SharedPreferences sharedPreferences, String str) {
        String string = sharedPreferences.getString(o.a(str, "cre"), null);
        if (string != null) {
            try {
                return Long.parseLong(string);
            } catch (NumberFormatException e) {
            }
        }
        return 0;
    }

    private static File b(Context context) {
        File a2 = a.a(context);
        if (a2 != null && a2.isDirectory()) {
            return a2;
        }
        Log.w("FirebaseInstanceId", "noBackupFilesDir doesn't exist, using regular files directory instead");
        return context.getFilesDir();
    }

    private final void b(Context context, String str, ah ahVar) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("com.google.android.gms.appid", 0);
        try {
            if (ahVar.equals(a(sharedPreferences, str))) {
                return;
            }
        } catch (ai e) {
        }
        if (Log.isLoggable("FirebaseInstanceId", 3)) {
            Log.d("FirebaseInstanceId", "Writing key to shared preferences");
        }
        Editor edit = sharedPreferences.edit();
        edit.putString(o.a(str, "|P|"), ahVar.b());
        edit.putString(o.a(str, "|K|"), ahVar.c());
        edit.putString(o.a(str, "cre"), String.valueOf(ahVar.f4188b));
        edit.commit();
    }

    private final ah c(Context context, String str) throws ai {
        ai aiVar;
        try {
            ah d = d(context, str);
            if (d != null) {
                b(context, str, d);
                return d;
            }
            aiVar = null;
            try {
                ah a2 = a(context.getSharedPreferences("com.google.android.gms.appid", 0), str);
                if (a2 != null) {
                    a(context, str, a2);
                    return a2;
                }
                e = aiVar;
                if (e == null) {
                    return null;
                }
                throw e;
            } catch (ai e) {
                e = e;
            }
        } catch (ai e2) {
            aiVar = e2;
        }
    }

    private final ah d(Context context, String str) throws ai {
        File e = e(context, str);
        if (!e.exists()) {
            return null;
        }
        try {
            return a(e);
        } catch (IOException e2) {
            if (Log.isLoggable("FirebaseInstanceId", 3)) {
                String valueOf = String.valueOf(e2);
                Log.d("FirebaseInstanceId", new StringBuilder(String.valueOf(valueOf).length() + 40).append("Failed to read key from file, retrying: ").append(valueOf).toString());
            }
            try {
                return a(e);
            } catch (IOException e3) {
                String valueOf2 = String.valueOf(e3);
                Log.w("FirebaseInstanceId", new StringBuilder(String.valueOf(valueOf2).length() + 45).append("IID file exists, but failed to read from it: ").append(valueOf2).toString());
                throw new ai(e3);
            }
        }
    }

    private static File e(Context context, String str) {
        String sb;
        if (TextUtils.isEmpty(str)) {
            sb = "com.google.InstanceId.properties";
        } else {
            try {
                String encodeToString = Base64.encodeToString(str.getBytes("UTF-8"), 11);
                sb = new StringBuilder(String.valueOf(encodeToString).length() + 33).append("com.google.InstanceId_").append(encodeToString).append(".properties").toString();
            } catch (UnsupportedEncodingException e) {
                throw new AssertionError(e);
            }
        }
        return new File(b(context), sb);
    }

    /* access modifiers changed from: 0000 */
    public final ah a(Context context, String str) throws ai {
        ah c = c(context, str);
        return c != null ? c : b(context, str);
    }

    /* access modifiers changed from: 0000 */
    public final ah b(Context context, String str) {
        ah ahVar = new ah(a.a(), System.currentTimeMillis());
        try {
            ah c = c(context, str);
            if (c != null) {
                if (!Log.isLoggable("FirebaseInstanceId", 3)) {
                    return c;
                }
                Log.d("FirebaseInstanceId", "Loaded key after generating new one, using loaded one");
                return c;
            }
        } catch (ai e) {
        }
        if (Log.isLoggable("FirebaseInstanceId", 3)) {
            Log.d("FirebaseInstanceId", "Generated new key");
        }
        a(context, str, ahVar);
        b(context, str, ahVar);
        return ahVar;
    }
}
