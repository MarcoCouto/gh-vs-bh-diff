package com.google.firebase.iid;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.Parcelable;
import android.os.RemoteException;
import android.support.v4.h.m;
import android.util.Log;
import com.google.android.gms.d.h;
import com.google.android.gms.d.j;
import com.google.firebase.iid.z.a;
import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

final class l {

    /* renamed from: a reason: collision with root package name */
    private static int f4211a = 0;

    /* renamed from: b reason: collision with root package name */
    private static PendingIntent f4212b;
    private final m<String, h<Bundle>> c = new m<>();
    private final Context d;
    private final e e;
    private Messenger f;
    private Messenger g;
    private z h;

    public l(Context context, e eVar) {
        this.d = context;
        this.e = eVar;
        this.f = new Messenger(new m(this, Looper.getMainLooper()));
    }

    private static synchronized String a() {
        String num;
        synchronized (l.class) {
            int i = f4211a;
            f4211a = i + 1;
            num = Integer.toString(i);
        }
        return num;
    }

    private static synchronized void a(Context context, Intent intent) {
        synchronized (l.class) {
            if (f4212b == null) {
                Intent intent2 = new Intent();
                intent2.setPackage("com.google.example.invalidpackage");
                f4212b = PendingIntent.getBroadcast(context, 0, intent2, 0);
            }
            intent.putExtra("app", f4212b);
        }
    }

    /* access modifiers changed from: private */
    public final void a(Message message) {
        if (message == null || !(message.obj instanceof Intent)) {
            Log.w("FirebaseInstanceId", "Dropping invalid message");
            return;
        }
        Intent intent = (Intent) message.obj;
        intent.setExtrasClassLoader(new a());
        if (intent.hasExtra("google.messenger")) {
            Parcelable parcelableExtra = intent.getParcelableExtra("google.messenger");
            if (parcelableExtra instanceof z) {
                this.h = (z) parcelableExtra;
            }
            if (parcelableExtra instanceof Messenger) {
                this.g = (Messenger) parcelableExtra;
            }
        }
        Intent intent2 = (Intent) message.obj;
        String action = intent2.getAction();
        if ("com.google.android.c2dm.intent.REGISTRATION".equals(action)) {
            String stringExtra = intent2.getStringExtra("registration_id");
            if (stringExtra == null) {
                stringExtra = intent2.getStringExtra("unregistered");
            }
            if (stringExtra == null) {
                String stringExtra2 = intent2.getStringExtra("error");
                if (stringExtra2 == null) {
                    String valueOf = String.valueOf(intent2.getExtras());
                    Log.w("FirebaseInstanceId", new StringBuilder(String.valueOf(valueOf).length() + 49).append("Unexpected response, no error or registration id ").append(valueOf).toString());
                    return;
                }
                if (Log.isLoggable("FirebaseInstanceId", 3)) {
                    String str = "FirebaseInstanceId";
                    String str2 = "Received InstanceID error ";
                    String valueOf2 = String.valueOf(stringExtra2);
                    Log.d(str, valueOf2.length() != 0 ? str2.concat(valueOf2) : new String(str2));
                }
                if (stringExtra2.startsWith("|")) {
                    String[] split = stringExtra2.split("\\|");
                    if (split.length <= 2 || !"ID".equals(split[1])) {
                        String str3 = "FirebaseInstanceId";
                        String str4 = "Unexpected structured response ";
                        String valueOf3 = String.valueOf(stringExtra2);
                        Log.w(str3, valueOf3.length() != 0 ? str4.concat(valueOf3) : new String(str4));
                        return;
                    }
                    String str5 = split[2];
                    String str6 = split[3];
                    if (str6.startsWith(":")) {
                        str6 = str6.substring(1);
                    }
                    a(str5, intent2.putExtra("error", str6).getExtras());
                    return;
                }
                synchronized (this.c) {
                    for (int i = 0; i < this.c.size(); i++) {
                        a((String) this.c.b(i), intent2.getExtras());
                    }
                }
                return;
            }
            Matcher matcher = Pattern.compile("\\|ID\\|([^|]+)\\|:?+(.*)").matcher(stringExtra);
            if (matcher.matches()) {
                String group = matcher.group(1);
                String group2 = matcher.group(2);
                Bundle extras = intent2.getExtras();
                extras.putString("registration_id", group2);
                a(group, extras);
            } else if (Log.isLoggable("FirebaseInstanceId", 3)) {
                String str7 = "FirebaseInstanceId";
                String str8 = "Unexpected response string: ";
                String valueOf4 = String.valueOf(stringExtra);
                Log.d(str7, valueOf4.length() != 0 ? str8.concat(valueOf4) : new String(str8));
            }
        } else if (Log.isLoggable("FirebaseInstanceId", 3)) {
            String str9 = "FirebaseInstanceId";
            String str10 = "Unexpected response action: ";
            String valueOf5 = String.valueOf(action);
            Log.d(str9, valueOf5.length() != 0 ? str10.concat(valueOf5) : new String(str10));
        }
    }

    private final void a(String str, Bundle bundle) {
        synchronized (this.c) {
            h hVar = (h) this.c.remove(str);
            if (hVar == null) {
                String str2 = "FirebaseInstanceId";
                String str3 = "Missing callback for ";
                String valueOf = String.valueOf(str);
                Log.w(str2, valueOf.length() != 0 ? str3.concat(valueOf) : new String(str3));
                return;
            }
            hVar.a(bundle);
        }
    }

    private final Bundle b(Bundle bundle) throws IOException {
        Bundle c2 = c(bundle);
        if (c2 == null || !c2.containsKey("google.messenger")) {
            return c2;
        }
        Bundle c3 = c(bundle);
        if (c3 == null || !c3.containsKey("google.messenger")) {
            return c3;
        }
        return null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:32:0x00cf A[SYNTHETIC] */
    private final Bundle c(Bundle bundle) throws IOException {
        String a2 = a();
        h hVar = new h();
        synchronized (this.c) {
            this.c.put(a2, hVar);
        }
        if (this.e.a() == 0) {
            throw new IOException("MISSING_INSTANCEID_SERVICE");
        }
        Intent intent = new Intent();
        intent.setPackage("com.google.android.gms");
        if (this.e.a() == 2) {
            intent.setAction("com.google.iid.TOKEN_REQUEST");
        } else {
            intent.setAction("com.google.android.c2dm.intent.REGISTER");
        }
        intent.putExtras(bundle);
        a(this.d, intent);
        intent.putExtra("kid", new StringBuilder(String.valueOf(a2).length() + 5).append("|ID|").append(a2).append("|").toString());
        if (Log.isLoggable("FirebaseInstanceId", 3)) {
            String valueOf = String.valueOf(intent.getExtras());
            Log.d("FirebaseInstanceId", new StringBuilder(String.valueOf(valueOf).length() + 8).append("Sending ").append(valueOf).toString());
        }
        intent.putExtra("google.messenger", this.f);
        if (!(this.g == null && this.h == null)) {
            Message obtain = Message.obtain();
            obtain.obj = intent;
            try {
                if (this.g != null) {
                    this.g.send(obtain);
                } else {
                    this.h.a(obtain);
                }
            } catch (RemoteException e2) {
                if (Log.isLoggable("FirebaseInstanceId", 3)) {
                    Log.d("FirebaseInstanceId", "Messenger failed, fallback to startService");
                }
            }
            Bundle bundle2 = (Bundle) j.a(hVar.a(), 30000, TimeUnit.MILLISECONDS);
            synchronized (this.c) {
                this.c.remove(a2);
            }
            return bundle2;
        }
        if (this.e.a() == 2) {
            this.d.sendBroadcast(intent);
        } else {
            this.d.startService(intent);
        }
        try {
            Bundle bundle22 = (Bundle) j.a(hVar.a(), 30000, TimeUnit.MILLISECONDS);
            synchronized (this.c) {
            }
            return bundle22;
        } catch (InterruptedException | TimeoutException e3) {
            Log.w("FirebaseInstanceId", "No response");
            throw new IOException("TIMEOUT");
        } catch (ExecutionException e4) {
            throw new IOException(e4);
        } catch (Throwable th) {
            synchronized (this.c) {
                this.c.remove(a2);
                throw th;
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public final Bundle a(Bundle bundle) throws IOException {
        if (this.e.d() < 12000000) {
            return b(bundle);
        }
        try {
            return (Bundle) j.a(aj.a(this.d).a(1, bundle));
        } catch (InterruptedException | ExecutionException e2) {
            if (Log.isLoggable("FirebaseInstanceId", 3)) {
                String valueOf = String.valueOf(e2);
                Log.d("FirebaseInstanceId", new StringBuilder(String.valueOf(valueOf).length() + 22).append("Error making request: ").append(valueOf).toString());
            }
            if (!(e2.getCause() instanceof c) || ((c) e2.getCause()).a() != 4) {
                return null;
            }
            return b(bundle);
        }
    }
}
