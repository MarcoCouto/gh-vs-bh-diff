package com.google.firebase.iid;

import android.os.Bundle;
import com.google.android.gms.d.h;

final /* synthetic */ class ad implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final ac f4184a;

    /* renamed from: b reason: collision with root package name */
    private final Bundle f4185b;
    private final h c;

    ad(ac acVar, Bundle bundle, h hVar) {
        this.f4184a = acVar;
        this.f4185b = bundle;
        this.c = hVar;
    }

    public final void run() {
        this.f4184a.a(this.f4185b, this.c);
    }
}
