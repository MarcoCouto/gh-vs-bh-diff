package com.google.firebase.iid;

import android.os.Build.VERSION;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.internal.b.e;
import com.google.android.gms.internal.b.f;

public class z implements Parcelable {
    public static final Creator<z> CREATOR = new aa();

    /* renamed from: a reason: collision with root package name */
    private Messenger f4236a;

    /* renamed from: b reason: collision with root package name */
    private e f4237b;

    public static final class a extends ClassLoader {
        /* access modifiers changed from: protected */
        public final Class<?> loadClass(String str, boolean z) throws ClassNotFoundException {
            if (!"com.google.android.gms.iid.MessengerCompat".equals(str)) {
                return super.loadClass(str, z);
            }
            if (FirebaseInstanceId.h()) {
                Log.d("FirebaseInstanceId", "Using renamed FirebaseIidMessengerCompat class");
            }
            return z.class;
        }
    }

    public z(IBinder iBinder) {
        if (VERSION.SDK_INT >= 21) {
            this.f4236a = new Messenger(iBinder);
        } else {
            this.f4237b = f.a(iBinder);
        }
    }

    private final IBinder a() {
        return this.f4236a != null ? this.f4236a.getBinder() : this.f4237b.asBinder();
    }

    public final void a(Message message) throws RemoteException {
        if (this.f4236a != null) {
            this.f4236a.send(message);
        } else {
            this.f4237b.a(message);
        }
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        boolean z = false;
        if (obj == null) {
            return z;
        }
        try {
            return a().equals(((z) obj).a());
        } catch (ClassCastException e) {
            return z;
        }
    }

    public int hashCode() {
        return a().hashCode();
    }

    public void writeToParcel(Parcel parcel, int i) {
        if (this.f4236a != null) {
            parcel.writeStrongBinder(this.f4236a.getBinder());
        } else {
            parcel.writeStrongBinder(this.f4237b.asBinder());
        }
    }
}
