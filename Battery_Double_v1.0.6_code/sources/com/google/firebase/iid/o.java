package com.google.firebase.iid;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.support.v4.h.a;
import android.text.TextUtils;
import android.util.Log;
import java.io.File;
import java.io.IOException;
import java.util.Map;

final class o {

    /* renamed from: a reason: collision with root package name */
    private final SharedPreferences f4216a;

    /* renamed from: b reason: collision with root package name */
    private final Context f4217b;
    private final ag c;
    private final Map<String, ah> d;

    public o(Context context) {
        this(context, new ag());
    }

    private o(Context context, ag agVar) {
        this.d = new a();
        this.f4217b = context;
        this.f4216a = context.getSharedPreferences("com.google.android.gms.appid", 0);
        this.c = agVar;
        File file = new File(android.support.v4.b.a.a(this.f4217b), "com.google.android.gms.appid-no-backup");
        if (!file.exists()) {
            try {
                if (file.createNewFile() && !c()) {
                    Log.i("FirebaseInstanceId", "App restored, clearing state");
                    b();
                    FirebaseInstanceId.a().i();
                }
            } catch (IOException e) {
                if (Log.isLoggable("FirebaseInstanceId", 3)) {
                    String str = "FirebaseInstanceId";
                    String str2 = "Error creating file in no backup dir: ";
                    String valueOf = String.valueOf(e.getMessage());
                    Log.d(str, valueOf.length() != 0 ? str2.concat(valueOf) : new String(str2));
                }
            }
        }
    }

    static String a(String str, String str2) {
        return new StringBuilder(String.valueOf(str).length() + 3 + String.valueOf(str2).length()).append(str).append("|S|").append(str2).toString();
    }

    private static String b(String str, String str2, String str3) {
        return new StringBuilder(String.valueOf(str).length() + 4 + String.valueOf(str2).length() + String.valueOf(str3).length()).append(str).append("|T|").append(str2).append("|").append(str3).toString();
    }

    private final synchronized boolean c() {
        return this.f4216a.getAll().isEmpty();
    }

    public final synchronized p a(String str, String str2, String str3) {
        return p.a(this.f4216a.getString(b(str, str2, str3), null));
    }

    public final synchronized String a() {
        String str = null;
        synchronized (this) {
            String string = this.f4216a.getString("topic_operaion_queue", null);
            if (string != null) {
                String[] split = string.split(",");
                if (split.length > 1 && !TextUtils.isEmpty(split[1])) {
                    str = split[1];
                }
            }
        }
        return str;
    }

    public final synchronized void a(String str, String str2, String str3, String str4, String str5) {
        String a2 = p.a(str4, str5, System.currentTimeMillis());
        if (a2 != null) {
            Editor edit = this.f4216a.edit();
            edit.putString(b(str, str2, str3), a2);
            edit.commit();
        }
    }

    public final synchronized boolean a(String str) {
        boolean z;
        String string = this.f4216a.getString("topic_operaion_queue", "");
        String valueOf = String.valueOf(",");
        String valueOf2 = String.valueOf(str);
        if (string.startsWith(valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf))) {
            String valueOf3 = String.valueOf(",");
            String valueOf4 = String.valueOf(str);
            this.f4216a.edit().putString("topic_operaion_queue", string.substring((valueOf4.length() != 0 ? valueOf3.concat(valueOf4) : new String(valueOf3)).length())).apply();
            z = true;
        } else {
            z = false;
        }
        return z;
    }

    public final synchronized ah b(String str) {
        ah ahVar;
        ahVar = (ah) this.d.get(str);
        if (ahVar == null) {
            try {
                ahVar = this.c.a(this.f4217b, str);
            } catch (ai e) {
                Log.w("FirebaseInstanceId", "Stored data is corrupt, generating new identity");
                FirebaseInstanceId.a().i();
                ahVar = this.c.b(this.f4217b, str);
            }
            this.d.put(str, ahVar);
        }
        return ahVar;
    }

    public final synchronized void b() {
        this.d.clear();
        ag.a(this.f4217b);
        this.f4216a.edit().clear().commit();
    }

    public final synchronized void c(String str) {
        String concat = String.valueOf(str).concat("|T|");
        Editor edit = this.f4216a.edit();
        for (String str2 : this.f4216a.getAll().keySet()) {
            if (str2.startsWith(concat)) {
                edit.remove(str2);
            }
        }
        edit.commit();
    }
}
