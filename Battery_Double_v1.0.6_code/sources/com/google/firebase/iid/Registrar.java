package com.google.firebase.iid;

import android.support.annotation.Keep;
import com.google.firebase.components.e;
import com.google.firebase.components.f;
import java.util.Arrays;
import java.util.List;

@Keep
public final class Registrar implements e {

    private static class a implements com.google.firebase.iid.a.a {

        /* renamed from: a reason: collision with root package name */
        private final FirebaseInstanceId f4179a;

        public a(FirebaseInstanceId firebaseInstanceId) {
            this.f4179a = firebaseInstanceId;
        }
    }

    @Keep
    public final List<com.google.firebase.components.a<?>> getComponents() {
        return Arrays.asList(new com.google.firebase.components.a[]{com.google.firebase.components.a.a(FirebaseInstanceId.class).a(f.a(com.google.firebase.a.class)).a(f.f4205a).a().b(), com.google.firebase.components.a.a(com.google.firebase.iid.a.a.class).a(f.a(FirebaseInstanceId.class)).a(g.f4206a).b()});
    }
}
