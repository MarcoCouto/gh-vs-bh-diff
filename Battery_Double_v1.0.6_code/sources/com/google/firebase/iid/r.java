package com.google.firebase.iid;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;

final class r extends BroadcastReceiver {

    /* renamed from: a reason: collision with root package name */
    private q f4222a;

    public r(q qVar) {
        this.f4222a = qVar;
    }

    public final void a() {
        if (FirebaseInstanceId.h()) {
            Log.d("FirebaseInstanceId", "Connectivity change received registered");
        }
        this.f4222a.a().registerReceiver(this, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
    }

    public final void onReceive(Context context, Intent intent) {
        if (this.f4222a != null && this.f4222a.b()) {
            if (FirebaseInstanceId.h()) {
                Log.d("FirebaseInstanceId", "Connectivity changed. Starting background sync.");
            }
            FirebaseInstanceId.a((Runnable) this.f4222a, 0);
            this.f4222a.a().unregisterReceiver(this);
            this.f4222a = null;
        }
    }
}
