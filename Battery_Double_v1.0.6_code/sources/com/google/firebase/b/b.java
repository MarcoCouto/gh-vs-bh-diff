package com.google.firebase.b;

import android.content.Context;
import com.google.firebase.a;
import java.util.concurrent.atomic.AtomicReference;

public final class b {

    /* renamed from: a reason: collision with root package name */
    private static final AtomicReference<b> f4154a = new AtomicReference<>();

    private b(Context context) {
    }

    public static b a(Context context) {
        f4154a.compareAndSet(null, new b(context));
        return (b) f4154a.get();
    }

    public static void a(a aVar) {
    }
}
