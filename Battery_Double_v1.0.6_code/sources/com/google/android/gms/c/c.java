package com.google.android.gms.c;

import com.google.android.gms.common.api.a.d.e;

public final class c implements e {

    /* renamed from: a reason: collision with root package name */
    public static final c f2149a = new a().a();

    /* renamed from: b reason: collision with root package name */
    private final boolean f2150b;
    private final boolean c;
    private final String d;
    private final boolean e;
    private final String f;
    private final boolean g;
    private final Long h;
    private final Long i;

    public static final class a {

        /* renamed from: a reason: collision with root package name */
        private boolean f2151a;

        /* renamed from: b reason: collision with root package name */
        private boolean f2152b;
        private String c;
        private boolean d;
        private String e;
        private boolean f;
        private Long g;
        private Long h;

        public final c a() {
            return new c(this.f2151a, this.f2152b, this.c, this.d, this.e, this.f, this.g, this.h);
        }
    }

    private c(boolean z, boolean z2, String str, boolean z3, String str2, boolean z4, Long l, Long l2) {
        this.f2150b = z;
        this.c = z2;
        this.d = str;
        this.e = z3;
        this.g = z4;
        this.f = str2;
        this.h = l;
        this.i = l2;
    }

    public final boolean a() {
        return this.f2150b;
    }

    public final boolean b() {
        return this.c;
    }

    public final String c() {
        return this.d;
    }

    public final boolean d() {
        return this.e;
    }

    public final String e() {
        return this.f;
    }

    public final boolean f() {
        return this.g;
    }

    public final Long g() {
        return this.h;
    }

    public final Long h() {
        return this.i;
    }
}
