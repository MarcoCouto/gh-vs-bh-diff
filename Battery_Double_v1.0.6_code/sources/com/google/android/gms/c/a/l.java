package com.google.android.gms.c.a;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.a.b;
import com.google.android.gms.common.internal.ab;

public class l implements Creator<k> {
    /* renamed from: a */
    public k createFromParcel(Parcel parcel) {
        int b2 = b.b(parcel);
        int i = 0;
        ab abVar = null;
        while (parcel.dataPosition() < b2) {
            int a2 = b.a(parcel);
            switch (b.a(a2)) {
                case 1:
                    i = b.d(parcel, a2);
                    break;
                case 2:
                    abVar = (ab) b.a(parcel, a2, ab.CREATOR);
                    break;
                default:
                    b.b(parcel, a2);
                    break;
            }
        }
        b.r(parcel, b2);
        return new k(i, abVar);
    }

    /* renamed from: a */
    public k[] newArray(int i) {
        return new k[i];
    }
}
