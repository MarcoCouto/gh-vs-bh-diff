package com.google.android.gms.c.a;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.internal.d.b;
import com.google.android.gms.internal.d.c;

public interface f extends IInterface {

    public static abstract class a extends b implements f {

        /* renamed from: com.google.android.gms.c.a.f$a$a reason: collision with other inner class name */
        public static class C0049a extends com.google.android.gms.internal.d.a implements f {
            C0049a(IBinder iBinder) {
                super(iBinder, "com.google.android.gms.signin.internal.ISignInCallbacks");
            }

            public void a(m mVar) throws RemoteException {
                Parcel d = d();
                c.a(d, (Parcelable) mVar);
                b(8, d);
            }

            public void a(Status status) throws RemoteException {
                Parcel d = d();
                c.a(d, (Parcelable) status);
                b(4, d);
            }

            public void a(Status status, GoogleSignInAccount googleSignInAccount) throws RemoteException {
                Parcel d = d();
                c.a(d, (Parcelable) status);
                c.a(d, (Parcelable) googleSignInAccount);
                b(7, d);
            }

            public void a(com.google.android.gms.common.b bVar, a aVar) throws RemoteException {
                Parcel d = d();
                c.a(d, (Parcelable) bVar);
                c.a(d, (Parcelable) aVar);
                b(3, d);
            }

            public void b(Status status) throws RemoteException {
                Parcel d = d();
                c.a(d, (Parcelable) status);
                b(6, d);
            }
        }

        public a() {
            super("com.google.android.gms.signin.internal.ISignInCallbacks");
        }

        public static f a(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.signin.internal.ISignInCallbacks");
            return queryLocalInterface instanceof f ? (f) queryLocalInterface : new C0049a(iBinder);
        }

        /* access modifiers changed from: protected */
        public boolean a(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
            switch (i) {
                case 3:
                    a((com.google.android.gms.common.b) c.a(parcel, com.google.android.gms.common.b.CREATOR), (a) c.a(parcel, a.CREATOR));
                    break;
                case 4:
                    a((Status) c.a(parcel, Status.CREATOR));
                    break;
                case 6:
                    b((Status) c.a(parcel, Status.CREATOR));
                    break;
                case 7:
                    a((Status) c.a(parcel, Status.CREATOR), (GoogleSignInAccount) c.a(parcel, GoogleSignInAccount.CREATOR));
                    break;
                case 8:
                    a((m) c.a(parcel, m.CREATOR));
                    break;
                default:
                    return false;
            }
            parcel2.writeNoException();
            return true;
        }
    }

    void a(m mVar) throws RemoteException;

    void a(Status status) throws RemoteException;

    void a(Status status, GoogleSignInAccount googleSignInAccount) throws RemoteException;

    void a(com.google.android.gms.common.b bVar, a aVar) throws RemoteException;

    void b(Status status) throws RemoteException;
}
