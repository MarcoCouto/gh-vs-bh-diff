package com.google.android.gms.c.a;

import android.accounts.Account;
import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Looper;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.c.b;
import com.google.android.gms.c.c;
import com.google.android.gms.common.api.f.a;
import com.google.android.gms.common.internal.aa;
import com.google.android.gms.common.internal.ab;
import com.google.android.gms.common.internal.e.d;
import com.google.android.gms.common.internal.f;
import com.google.android.gms.common.internal.m;

public class j extends m<g> implements b {
    private final boolean e;
    private final f f;
    private final Bundle g;
    private Integer h;

    public j(Context context, Looper looper, boolean z, f fVar, Bundle bundle, a aVar, com.google.android.gms.common.api.f.b bVar) {
        super(context, looper, 44, fVar, aVar, bVar);
        this.e = z;
        this.f = fVar;
        this.g = bundle;
        this.h = fVar.h();
    }

    public j(Context context, Looper looper, boolean z, f fVar, c cVar, a aVar, com.google.android.gms.common.api.f.b bVar) {
        this(context, looper, z, fVar, a(fVar), aVar, bVar);
    }

    public static Bundle a(f fVar) {
        c g2 = fVar.g();
        Integer h2 = fVar.h();
        Bundle bundle = new Bundle();
        bundle.putParcelable("com.google.android.gms.signin.internal.clientRequestedAccount", fVar.a());
        if (h2 != null) {
            bundle.putInt("com.google.android.gms.common.internal.ClientSettings.sessionId", h2.intValue());
        }
        if (g2 != null) {
            bundle.putBoolean("com.google.android.gms.signin.internal.offlineAccessRequested", g2.a());
            bundle.putBoolean("com.google.android.gms.signin.internal.idTokenRequested", g2.b());
            bundle.putString("com.google.android.gms.signin.internal.serverClientId", g2.c());
            bundle.putBoolean("com.google.android.gms.signin.internal.usePromptModeForAuthCode", true);
            bundle.putBoolean("com.google.android.gms.signin.internal.forceCodeForRefreshToken", g2.d());
            bundle.putString("com.google.android.gms.signin.internal.hostedDomain", g2.e());
            bundle.putBoolean("com.google.android.gms.signin.internal.waitForAccessTokenRefresh", g2.f());
            if (g2.g() != null) {
                bundle.putLong("com.google.android.gms.signin.internal.authApiSignInModuleVersion", g2.g().longValue());
            }
            if (g2.h() != null) {
                bundle.putLong("com.google.android.gms.signin.internal.realClientLibraryVersion", g2.h().longValue());
            }
        }
        return bundle;
    }

    public void a(f fVar) {
        aa.a(fVar, (Object) "Expecting a valid ISignInCallbacks");
        try {
            Account b2 = this.f.b();
            GoogleSignInAccount googleSignInAccount = null;
            if ("<<default account>>".equals(b2.name)) {
                googleSignInAccount = com.google.android.gms.auth.api.signin.a.a.a(p()).a();
            }
            ((g) x()).a(new k(new ab(b2, this.h.intValue(), googleSignInAccount)), fVar);
        } catch (RemoteException e2) {
            Log.w("SignInClientImpl", "Remote service probably died when signIn is called");
            try {
                fVar.a(new m(8));
            } catch (RemoteException e3) {
                Log.wtf("SignInClientImpl", "ISignInCallbacks#onSignInComplete should be executed from the same process, unexpected RemoteException.", e2);
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public g a(IBinder iBinder) {
        return g.a.a(iBinder);
    }

    public boolean d() {
        return this.e;
    }

    public int g() {
        return 12451000;
    }

    /* access modifiers changed from: protected */
    public String i() {
        return "com.google.android.gms.signin.service.START";
    }

    public void j() {
        a((d) new g());
    }

    /* access modifiers changed from: protected */
    public String l() {
        return "com.google.android.gms.signin.internal.ISignInService";
    }

    /* access modifiers changed from: protected */
    public Bundle u() {
        if (!p().getPackageName().equals(this.f.e())) {
            this.g.putString("com.google.android.gms.signin.internal.realClientPackageName", this.f.e());
        }
        return this.g;
    }
}
