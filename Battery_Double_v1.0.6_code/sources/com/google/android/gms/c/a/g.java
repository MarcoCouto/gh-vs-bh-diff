package com.google.android.gms.c.a;

import android.accounts.Account;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.google.android.gms.common.internal.ab;
import com.google.android.gms.common.internal.r;
import com.google.android.gms.common.internal.x;
import com.google.android.gms.internal.d.b;
import com.google.android.gms.internal.d.c;

public interface g extends IInterface {

    public static abstract class a extends b implements g {

        /* renamed from: com.google.android.gms.c.a.g$a$a reason: collision with other inner class name */
        public static class C0050a extends com.google.android.gms.internal.d.a implements g {
            C0050a(IBinder iBinder) {
                super(iBinder, "com.google.android.gms.signin.internal.ISignInService");
            }

            public void a(int i) throws RemoteException {
                Parcel d = d();
                d.writeInt(i);
                b(7, d);
            }

            public void a(int i, Account account, f fVar) throws RemoteException {
                Parcel d = d();
                d.writeInt(i);
                c.a(d, (Parcelable) account);
                c.a(d, (IInterface) fVar);
                b(8, d);
            }

            public void a(d dVar) throws RemoteException {
                Parcel d = d();
                c.a(d, (Parcelable) dVar);
                b(3, d);
            }

            public void a(f fVar) throws RemoteException {
                Parcel d = d();
                c.a(d, (IInterface) fVar);
                b(11, d);
            }

            public void a(h hVar, f fVar) throws RemoteException {
                Parcel d = d();
                c.a(d, (Parcelable) hVar);
                c.a(d, (IInterface) fVar);
                b(10, d);
            }

            public void a(k kVar, f fVar) throws RemoteException {
                Parcel d = d();
                c.a(d, (Parcelable) kVar);
                c.a(d, (IInterface) fVar);
                b(12, d);
            }

            public void a(ab abVar, x xVar) throws RemoteException {
                Parcel d = d();
                c.a(d, (Parcelable) abVar);
                c.a(d, (IInterface) xVar);
                b(5, d);
            }

            public void a(com.google.android.gms.common.internal.c cVar, f fVar) throws RemoteException {
                Parcel d = d();
                c.a(d, (Parcelable) cVar);
                c.a(d, (IInterface) fVar);
                b(2, d);
            }

            public void a(r rVar, int i, boolean z) throws RemoteException {
                Parcel d = d();
                c.a(d, (IInterface) rVar);
                d.writeInt(i);
                c.a(d, z);
                b(9, d);
            }

            public void a(boolean z) throws RemoteException {
                Parcel d = d();
                c.a(d, z);
                b(4, d);
            }

            public void b(boolean z) throws RemoteException {
                Parcel d = d();
                c.a(d, z);
                b(13, d);
            }
        }

        public static g a(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.signin.internal.ISignInService");
            return queryLocalInterface instanceof g ? (g) queryLocalInterface : new C0050a(iBinder);
        }

        /* access modifiers changed from: protected */
        public boolean a(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
            switch (i) {
                case 2:
                    a((com.google.android.gms.common.internal.c) c.a(parcel, com.google.android.gms.common.internal.c.CREATOR), com.google.android.gms.c.a.f.a.a(parcel.readStrongBinder()));
                    break;
                case 3:
                    a((d) c.a(parcel, d.CREATOR));
                    break;
                case 4:
                    a(c.a(parcel));
                    break;
                case 5:
                    a((ab) c.a(parcel, ab.CREATOR), com.google.android.gms.common.internal.x.a.a(parcel.readStrongBinder()));
                    break;
                case 7:
                    a(parcel.readInt());
                    break;
                case 8:
                    a(parcel.readInt(), (Account) c.a(parcel, Account.CREATOR), com.google.android.gms.c.a.f.a.a(parcel.readStrongBinder()));
                    break;
                case 9:
                    a(com.google.android.gms.common.internal.r.a.a(parcel.readStrongBinder()), parcel.readInt(), c.a(parcel));
                    break;
                case 10:
                    a((h) c.a(parcel, h.CREATOR), com.google.android.gms.c.a.f.a.a(parcel.readStrongBinder()));
                    break;
                case 11:
                    a(com.google.android.gms.c.a.f.a.a(parcel.readStrongBinder()));
                    break;
                case 12:
                    a((k) c.a(parcel, k.CREATOR), com.google.android.gms.c.a.f.a.a(parcel.readStrongBinder()));
                    break;
                case 13:
                    b(c.a(parcel));
                    break;
                default:
                    return false;
            }
            parcel2.writeNoException();
            return true;
        }
    }

    void a(int i) throws RemoteException;

    void a(int i, Account account, f fVar) throws RemoteException;

    void a(d dVar) throws RemoteException;

    void a(f fVar) throws RemoteException;

    void a(h hVar, f fVar) throws RemoteException;

    void a(k kVar, f fVar) throws RemoteException;

    void a(ab abVar, x xVar) throws RemoteException;

    void a(com.google.android.gms.common.internal.c cVar, f fVar) throws RemoteException;

    void a(r rVar, int i, boolean z) throws RemoteException;

    void a(boolean z) throws RemoteException;

    void b(boolean z) throws RemoteException;
}
