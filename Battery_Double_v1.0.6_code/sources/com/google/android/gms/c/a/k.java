package com.google.android.gms.c.a;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.a.a;
import com.google.android.gms.common.internal.a.c;
import com.google.android.gms.common.internal.ab;

public class k extends a {
    public static final Creator<k> CREATOR = new l();

    /* renamed from: a reason: collision with root package name */
    private final int f2145a;

    /* renamed from: b reason: collision with root package name */
    private final ab f2146b;

    k(int i, ab abVar) {
        this.f2145a = i;
        this.f2146b = abVar;
    }

    public k(ab abVar) {
        this(1, abVar);
    }

    public ab a() {
        return this.f2146b;
    }

    public void writeToParcel(Parcel parcel, int i) {
        int a2 = c.a(parcel);
        c.a(parcel, 1, this.f2145a);
        c.a(parcel, 2, (Parcelable) a(), i, false);
        c.a(parcel, a2);
    }
}
