package com.google.android.gms.c.a;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable.Creator;

public class b implements Creator<a> {
    /* renamed from: a */
    public a createFromParcel(Parcel parcel) {
        int i = 0;
        int b2 = com.google.android.gms.common.internal.a.b.b(parcel);
        Intent intent = null;
        int i2 = 0;
        while (parcel.dataPosition() < b2) {
            int a2 = com.google.android.gms.common.internal.a.b.a(parcel);
            switch (com.google.android.gms.common.internal.a.b.a(a2)) {
                case 1:
                    i2 = com.google.android.gms.common.internal.a.b.d(parcel, a2);
                    break;
                case 2:
                    i = com.google.android.gms.common.internal.a.b.d(parcel, a2);
                    break;
                case 3:
                    intent = (Intent) com.google.android.gms.common.internal.a.b.a(parcel, a2, Intent.CREATOR);
                    break;
                default:
                    com.google.android.gms.common.internal.a.b.b(parcel, a2);
                    break;
            }
        }
        com.google.android.gms.common.internal.a.b.r(parcel, b2);
        return new a(i2, i, intent);
    }

    /* renamed from: a */
    public a[] newArray(int i) {
        return new a[i];
    }
}
