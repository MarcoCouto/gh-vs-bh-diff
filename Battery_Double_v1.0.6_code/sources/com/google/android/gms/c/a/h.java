package com.google.android.gms.c.a;

import android.accounts.Account;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.a.a;
import com.google.android.gms.common.internal.a.c;

public class h extends a {
    public static final Creator<h> CREATOR = new i();

    /* renamed from: a reason: collision with root package name */
    private final int f2143a;

    /* renamed from: b reason: collision with root package name */
    private final Account f2144b;
    private final Scope[] c;
    private final String d;

    h(int i, Account account, Scope[] scopeArr, String str) {
        this.f2143a = i;
        this.f2144b = account;
        this.c = scopeArr;
        this.d = str;
    }

    public Account a() {
        return this.f2144b;
    }

    public Scope[] b() {
        return this.c;
    }

    public String c() {
        return this.d;
    }

    public void writeToParcel(Parcel parcel, int i) {
        int a2 = c.a(parcel);
        c.a(parcel, 1, this.f2143a);
        c.a(parcel, 2, (Parcelable) a(), i, false);
        c.a(parcel, 3, (T[]) b(), i, false);
        c.a(parcel, 4, c(), false);
        c.a(parcel, a2);
    }
}
