package com.google.android.gms.c.a;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.b;
import com.google.android.gms.common.internal.a.a;
import com.google.android.gms.common.internal.a.c;
import com.google.android.gms.common.internal.ad;

public class m extends a {
    public static final Creator<m> CREATOR = new n();

    /* renamed from: a reason: collision with root package name */
    private final int f2147a;

    /* renamed from: b reason: collision with root package name */
    private final b f2148b;
    private final ad c;

    public m(int i) {
        this(new b(i, null), null);
    }

    m(int i, b bVar, ad adVar) {
        this.f2147a = i;
        this.f2148b = bVar;
        this.c = adVar;
    }

    public m(b bVar, ad adVar) {
        this(1, bVar, adVar);
    }

    public b a() {
        return this.f2148b;
    }

    public ad b() {
        return this.c;
    }

    public void writeToParcel(Parcel parcel, int i) {
        int a2 = c.a(parcel);
        c.a(parcel, 1, this.f2147a);
        c.a(parcel, 2, (Parcelable) a(), i, false);
        c.a(parcel, 3, (Parcelable) b(), i, false);
        c.a(parcel, a2);
    }
}
