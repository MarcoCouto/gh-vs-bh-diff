package com.google.android.gms.c.a;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.a.b;
import com.google.android.gms.common.internal.ad;

public class n implements Creator<m> {
    /* renamed from: a */
    public m createFromParcel(Parcel parcel) {
        int b2 = b.b(parcel);
        com.google.android.gms.common.b bVar = null;
        int i = 0;
        ad adVar = null;
        while (parcel.dataPosition() < b2) {
            int a2 = b.a(parcel);
            switch (b.a(a2)) {
                case 1:
                    i = b.d(parcel, a2);
                    break;
                case 2:
                    bVar = (com.google.android.gms.common.b) b.a(parcel, a2, com.google.android.gms.common.b.CREATOR);
                    break;
                case 3:
                    adVar = (ad) b.a(parcel, a2, ad.CREATOR);
                    break;
                default:
                    b.b(parcel, a2);
                    break;
            }
        }
        b.r(parcel, b2);
        return new m(i, bVar, adVar);
    }

    /* renamed from: a */
    public m[] newArray(int i) {
        return new m[i];
    }
}
