package com.google.android.gms.c.a;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.a.b;
import java.util.ArrayList;

public class e implements Creator<d> {
    /* renamed from: a */
    public d createFromParcel(Parcel parcel) {
        boolean z = false;
        int b2 = b.b(parcel);
        ArrayList arrayList = null;
        int i = 0;
        while (parcel.dataPosition() < b2) {
            int a2 = b.a(parcel);
            switch (b.a(a2)) {
                case 1:
                    i = b.d(parcel, a2);
                    break;
                case 2:
                    z = b.c(parcel, a2);
                    break;
                case 3:
                    arrayList = b.c(parcel, a2, Scope.CREATOR);
                    break;
                default:
                    b.b(parcel, a2);
                    break;
            }
        }
        b.r(parcel, b2);
        return new d(i, z, arrayList);
    }

    /* renamed from: a */
    public d[] newArray(int i) {
        return new d[i];
    }
}
