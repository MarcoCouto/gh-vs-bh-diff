package com.google.android.gms.c.a;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.i;
import com.google.android.gms.common.internal.a.c;

public class a extends com.google.android.gms.common.internal.a.a implements i {
    public static final Creator<a> CREATOR = new b();

    /* renamed from: a reason: collision with root package name */
    private final int f2139a;

    /* renamed from: b reason: collision with root package name */
    private int f2140b;
    private Intent c;

    public a() {
        this(0, null);
    }

    a(int i, int i2, Intent intent) {
        this.f2139a = i;
        this.f2140b = i2;
        this.c = intent;
    }

    public a(int i, Intent intent) {
        this(2, i, intent);
    }

    public Status a() {
        return this.f2140b == 0 ? Status.f2160a : Status.e;
    }

    public int b() {
        return this.f2140b;
    }

    public Intent c() {
        return this.c;
    }

    public void writeToParcel(Parcel parcel, int i) {
        int a2 = c.a(parcel);
        c.a(parcel, 1, this.f2139a);
        c.a(parcel, 2, b());
        c.a(parcel, 3, (Parcelable) c(), i, false);
        c.a(parcel, a2);
    }
}
