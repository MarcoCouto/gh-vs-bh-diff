package com.google.android.gms.c.a;

import android.accounts.Account;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.a.b;

public class i implements Creator<h> {
    /* renamed from: a */
    public h createFromParcel(Parcel parcel) {
        int b2 = b.b(parcel);
        Scope[] scopeArr = null;
        Account account = null;
        int i = 0;
        String str = null;
        while (parcel.dataPosition() < b2) {
            int a2 = b.a(parcel);
            switch (b.a(a2)) {
                case 1:
                    i = b.d(parcel, a2);
                    break;
                case 2:
                    account = (Account) b.a(parcel, a2, Account.CREATOR);
                    break;
                case 3:
                    scopeArr = (Scope[]) b.b(parcel, a2, Scope.CREATOR);
                    break;
                case 4:
                    str = b.k(parcel, a2);
                    break;
                default:
                    b.b(parcel, a2);
                    break;
            }
        }
        b.r(parcel, b2);
        return new h(i, account, scopeArr, str);
    }

    /* renamed from: a */
    public h[] newArray(int i) {
        return new h[i];
    }
}
