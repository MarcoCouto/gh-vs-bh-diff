package com.google.android.gms.c.a;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.a.a;
import com.google.android.gms.common.internal.a.c;
import java.util.List;

public class d extends a {
    public static final Creator<d> CREATOR = new e();

    /* renamed from: a reason: collision with root package name */
    private final int f2141a;

    /* renamed from: b reason: collision with root package name */
    private final boolean f2142b;
    private final List<Scope> c;

    d(int i, boolean z, List<Scope> list) {
        this.f2141a = i;
        this.f2142b = z;
        this.c = list;
    }

    public void writeToParcel(Parcel parcel, int i) {
        int a2 = c.a(parcel);
        c.a(parcel, 1, this.f2141a);
        c.a(parcel, 2, this.f2142b);
        c.c(parcel, 3, this.c, false);
        c.a(parcel, a2);
    }
}
