package com.google.android.gms.c;

import android.content.Context;
import android.os.Looper;
import com.google.android.gms.c.a.C0048a;
import com.google.android.gms.c.a.j;
import com.google.android.gms.common.api.a.C0051a;
import com.google.android.gms.common.api.a.f;
import com.google.android.gms.common.api.f.a;
import com.google.android.gms.common.api.f.b;

final class e extends C0051a<j, C0048a> {
    e() {
    }

    public final /* synthetic */ f a(Context context, Looper looper, com.google.android.gms.common.internal.f fVar, Object obj, a aVar, b bVar) {
        return new j(context, looper, false, fVar, ((C0048a) obj).a(), aVar, bVar);
    }
}
