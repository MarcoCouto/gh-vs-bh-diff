package com.google.android.gms.c;

import android.content.Context;
import android.os.Looper;
import com.google.android.gms.c.a.j;
import com.google.android.gms.common.api.a.C0051a;
import com.google.android.gms.common.api.a.f;
import com.google.android.gms.common.api.f.a;
import com.google.android.gms.common.api.f.b;

final class d extends C0051a<j, c> {
    d() {
    }

    public final /* synthetic */ f a(Context context, Looper looper, com.google.android.gms.common.internal.f fVar, Object obj, a aVar, b bVar) {
        c cVar = (c) obj;
        return new j(context, looper, true, fVar, cVar == null ? c.f2149a : cVar, aVar, bVar);
    }
}
