package com.google.android.gms.c;

import android.os.Bundle;
import com.google.android.gms.c.a.j;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.a.C0051a;
import com.google.android.gms.common.api.a.d.c;
import com.google.android.gms.common.api.a.g;

public final class a {

    /* renamed from: a reason: collision with root package name */
    public static final g<j> f2136a = new g<>();

    /* renamed from: b reason: collision with root package name */
    public static final g<j> f2137b = new g<>();
    public static final C0051a<j, c> c = new d();
    public static final Scope d = new Scope("profile");
    public static final Scope e = new Scope("email");
    public static final com.google.android.gms.common.api.a<c> f = new com.google.android.gms.common.api.a<>("SignIn.API", c, f2136a);
    public static final com.google.android.gms.common.api.a<C0048a> g = new com.google.android.gms.common.api.a<>("SignIn.INTERNAL_API", h, f2137b);
    private static final C0051a<j, C0048a> h = new e();

    /* renamed from: com.google.android.gms.c.a$a reason: collision with other inner class name */
    public static class C0048a implements c {

        /* renamed from: a reason: collision with root package name */
        private final Bundle f2138a;

        public Bundle a() {
            return this.f2138a;
        }
    }
}
