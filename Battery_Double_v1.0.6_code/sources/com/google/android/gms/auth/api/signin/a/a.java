package com.google.android.gms.auth.api.signin.a;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.internal.aa;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import org.json.JSONException;

public class a {

    /* renamed from: a reason: collision with root package name */
    private static final Lock f2131a = new ReentrantLock();

    /* renamed from: b reason: collision with root package name */
    private static a f2132b;
    private final Lock c = new ReentrantLock();
    private final SharedPreferences d;

    private a(Context context) {
        this.d = context.getSharedPreferences("com.google.android.gms.signin", 0);
    }

    public static a a(Context context) {
        aa.a(context);
        f2131a.lock();
        try {
            if (f2132b == null) {
                f2132b = new a(context.getApplicationContext());
            }
            return f2132b;
        } finally {
            f2131a.unlock();
        }
    }

    private static String a(String str, String str2) {
        return new StringBuilder(String.valueOf(str).length() + 1 + String.valueOf(str2).length()).append(str).append(":").append(str2).toString();
    }

    private final GoogleSignInAccount b(String str) {
        GoogleSignInAccount googleSignInAccount = null;
        if (TextUtils.isEmpty(str)) {
            return googleSignInAccount;
        }
        String a2 = a(a("googleSignInAccount", str));
        if (a2 == null) {
            return googleSignInAccount;
        }
        try {
            return GoogleSignInAccount.a(a2);
        } catch (JSONException e) {
            return googleSignInAccount;
        }
    }

    public GoogleSignInAccount a() {
        return b(a("defaultGoogleSignInAccount"));
    }

    /* access modifiers changed from: protected */
    public String a(String str) {
        this.c.lock();
        try {
            return this.d.getString(str, null);
        } finally {
            this.c.unlock();
        }
    }
}
