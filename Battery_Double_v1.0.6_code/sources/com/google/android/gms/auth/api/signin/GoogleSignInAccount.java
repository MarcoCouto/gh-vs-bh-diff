package com.google.android.gms.auth.api.signin;

import android.accounts.Account;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.a.a;
import com.google.android.gms.common.internal.a.c;
import com.google.android.gms.common.internal.aa;
import com.google.android.gms.common.util.e;
import com.google.android.gms.common.util.h;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class GoogleSignInAccount extends a implements ReflectedParcelable {
    public static final Creator<GoogleSignInAccount> CREATOR = new a();

    /* renamed from: a reason: collision with root package name */
    public static e f2129a = h.d();

    /* renamed from: b reason: collision with root package name */
    private final int f2130b;
    private String c;
    private String d;
    private String e;
    private String f;
    private Uri g;
    private String h;
    private long i;
    private String j;
    private List<Scope> k;
    private String l;
    private String m;
    private Set<Scope> n = new HashSet();

    GoogleSignInAccount(int i2, String str, String str2, String str3, String str4, Uri uri, String str5, long j2, String str6, List<Scope> list, String str7, String str8) {
        this.f2130b = i2;
        this.c = str;
        this.d = str2;
        this.e = str3;
        this.f = str4;
        this.g = uri;
        this.h = str5;
        this.i = j2;
        this.j = str6;
        this.k = list;
        this.l = str7;
        this.m = str8;
    }

    public static GoogleSignInAccount a(String str) throws JSONException {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        JSONObject jSONObject = new JSONObject(str);
        String optString = jSONObject.optString("photoUrl", null);
        Uri uri = !TextUtils.isEmpty(optString) ? Uri.parse(optString) : null;
        long parseLong = Long.parseLong(jSONObject.getString("expirationTime"));
        HashSet hashSet = new HashSet();
        JSONArray jSONArray = jSONObject.getJSONArray("grantedScopes");
        int length = jSONArray.length();
        for (int i2 = 0; i2 < length; i2++) {
            hashSet.add(new Scope(jSONArray.getString(i2)));
        }
        return a(jSONObject.optString("id"), jSONObject.optString("tokenId", null), jSONObject.optString("email", null), jSONObject.optString("displayName", null), jSONObject.optString("givenName", null), jSONObject.optString("familyName", null), uri, Long.valueOf(parseLong), jSONObject.getString("obfuscatedIdentifier"), hashSet).b(jSONObject.optString("serverAuthCode", null));
    }

    public static GoogleSignInAccount a(String str, String str2, String str3, String str4, String str5, String str6, Uri uri, Long l2, String str7, Set<Scope> set) {
        if (l2 == null) {
            l2 = Long.valueOf(f2129a.a() / 1000);
        }
        return new GoogleSignInAccount(3, str, str2, str3, str4, uri, null, l2.longValue(), aa.a(str7), new ArrayList((Collection) aa.a(set)), str5, str6);
    }

    public String a() {
        return this.c;
    }

    public GoogleSignInAccount b(String str) {
        this.h = str;
        return this;
    }

    public String b() {
        return this.d;
    }

    public String c() {
        return this.e;
    }

    public Account d() {
        if (this.e == null) {
            return null;
        }
        return new Account(this.e, "com.google");
    }

    public String e() {
        return this.f;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof GoogleSignInAccount)) {
            return false;
        }
        GoogleSignInAccount googleSignInAccount = (GoogleSignInAccount) obj;
        return googleSignInAccount.k().equals(k()) && googleSignInAccount.l().equals(l());
    }

    public String f() {
        return this.l;
    }

    public String g() {
        return this.m;
    }

    public Uri h() {
        return this.g;
    }

    public int hashCode() {
        return ((k().hashCode() + 527) * 31) + l().hashCode();
    }

    public String i() {
        return this.h;
    }

    public long j() {
        return this.i;
    }

    public String k() {
        return this.j;
    }

    public Set<Scope> l() {
        HashSet hashSet = new HashSet(this.k);
        hashSet.addAll(this.n);
        return hashSet;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        int a2 = c.a(parcel);
        c.a(parcel, 1, this.f2130b);
        c.a(parcel, 2, a(), false);
        c.a(parcel, 3, b(), false);
        c.a(parcel, 4, c(), false);
        c.a(parcel, 5, e(), false);
        c.a(parcel, 6, (Parcelable) h(), i2, false);
        c.a(parcel, 7, i(), false);
        c.a(parcel, 8, j());
        c.a(parcel, 9, k(), false);
        c.c(parcel, 10, this.k, false);
        c.a(parcel, 11, f(), false);
        c.a(parcel, 12, g(), false);
        c.a(parcel, a2);
    }
}
