package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.b.a;
import java.util.List;

public interface bdk extends IInterface {
    String a() throws RemoteException;

    void a(a aVar) throws RemoteException;

    void a(a aVar, a aVar2, a aVar3) throws RemoteException;

    List b() throws RemoteException;

    void b(a aVar) throws RemoteException;

    String c() throws RemoteException;

    auw d() throws RemoteException;

    String e() throws RemoteException;

    String f() throws RemoteException;

    double g() throws RemoteException;

    String h() throws RemoteException;

    String i() throws RemoteException;

    aqs j() throws RemoteException;

    aus k() throws RemoteException;

    a l() throws RemoteException;

    a m() throws RemoteException;

    a n() throws RemoteException;

    Bundle o() throws RemoteException;

    boolean p() throws RemoteException;

    boolean q() throws RemoteException;

    void r() throws RemoteException;
}
