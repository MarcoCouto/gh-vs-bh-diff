package com.google.android.gms.internal.ads;

import java.util.Map;

final class lk extends ut {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ byte[] f3483a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ Map f3484b;
    private final /* synthetic */ ml c;

    lk(lf lfVar, int i, String str, bdx bdx, bde bde, byte[] bArr, Map map, ml mlVar) {
        this.f3483a = bArr;
        this.f3484b = map;
        this.c = mlVar;
        super(i, str, bdx, bde);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        a((String) obj);
    }

    /* access modifiers changed from: protected */
    public final void a(String str) {
        this.c.a(str);
        super.a(str);
    }

    public final byte[] a() throws a {
        return this.f3483a == null ? super.a() : this.f3483a;
    }

    public final Map<String, String> b() throws a {
        return this.f3484b == null ? super.b() : this.f3484b;
    }
}
