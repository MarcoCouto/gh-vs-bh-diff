package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.content.Context;
import com.google.android.gms.ads.internal.bu;

@cm
public interface pm extends azf, rq, ru {
    pd a();

    void a(rd rdVar);

    void a(boolean z);

    rd b();

    ast c();

    Activity d();

    bu e();

    void f();

    String g();

    Context getContext();

    asu j();

    mu k();

    int l();

    int m();

    void setBackgroundColor(int i);
}
