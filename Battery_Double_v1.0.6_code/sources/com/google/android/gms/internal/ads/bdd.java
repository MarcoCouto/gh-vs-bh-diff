package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.b.a;
import java.util.List;

public interface bdd extends IInterface {
    String a() throws RemoteException;

    void a(a aVar) throws RemoteException;

    void a(a aVar, a aVar2, a aVar3) throws RemoteException;

    List b() throws RemoteException;

    void b(a aVar) throws RemoteException;

    String c() throws RemoteException;

    void c(a aVar) throws RemoteException;

    auw d() throws RemoteException;

    String e() throws RemoteException;

    double f() throws RemoteException;

    String g() throws RemoteException;

    String h() throws RemoteException;

    void i() throws RemoteException;

    boolean j() throws RemoteException;

    boolean k() throws RemoteException;

    Bundle l() throws RemoteException;

    aqs m() throws RemoteException;

    a n() throws RemoteException;

    aus o() throws RemoteException;

    a p() throws RemoteException;

    a q() throws RemoteException;
}
