package com.google.android.gms.internal.ads;

import java.io.IOException;

public final class vu extends afh<vu> {

    /* renamed from: a reason: collision with root package name */
    public String f3748a;

    /* renamed from: b reason: collision with root package name */
    public Long f3749b;
    private String c;
    private String d;
    private String e;
    private Long f;
    private Long g;
    private String h;
    private Long i;
    private String j;

    public vu() {
        this.f3748a = null;
        this.f3749b = null;
        this.c = null;
        this.d = null;
        this.e = null;
        this.f = null;
        this.g = null;
        this.h = null;
        this.i = null;
        this.j = null;
        this.Z = -1;
    }

    /* access modifiers changed from: protected */
    public final int a() {
        int a2 = super.a();
        if (this.f3748a != null) {
            a2 += aff.b(1, this.f3748a);
        }
        if (this.f3749b != null) {
            a2 += aff.d(2, this.f3749b.longValue());
        }
        if (this.c != null) {
            a2 += aff.b(3, this.c);
        }
        if (this.d != null) {
            a2 += aff.b(4, this.d);
        }
        if (this.e != null) {
            a2 += aff.b(5, this.e);
        }
        if (this.f != null) {
            a2 += aff.d(6, this.f.longValue());
        }
        if (this.g != null) {
            a2 += aff.d(7, this.g.longValue());
        }
        if (this.h != null) {
            a2 += aff.b(8, this.h);
        }
        if (this.i != null) {
            a2 += aff.d(9, this.i.longValue());
        }
        return this.j != null ? a2 + aff.b(10, this.j) : a2;
    }

    public final /* synthetic */ afn a(afd afd) throws IOException {
        while (true) {
            int a2 = afd.a();
            switch (a2) {
                case 0:
                    break;
                case 10:
                    this.f3748a = afd.e();
                    continue;
                case 16:
                    this.f3749b = Long.valueOf(afd.h());
                    continue;
                case 26:
                    this.c = afd.e();
                    continue;
                case 34:
                    this.d = afd.e();
                    continue;
                case 42:
                    this.e = afd.e();
                    continue;
                case 48:
                    this.f = Long.valueOf(afd.h());
                    continue;
                case 56:
                    this.g = Long.valueOf(afd.h());
                    continue;
                case 66:
                    this.h = afd.e();
                    continue;
                case 72:
                    this.i = Long.valueOf(afd.h());
                    continue;
                case 82:
                    this.j = afd.e();
                    continue;
                default:
                    if (!super.a(afd, a2)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    public final void a(aff aff) throws IOException {
        if (this.f3748a != null) {
            aff.a(1, this.f3748a);
        }
        if (this.f3749b != null) {
            aff.b(2, this.f3749b.longValue());
        }
        if (this.c != null) {
            aff.a(3, this.c);
        }
        if (this.d != null) {
            aff.a(4, this.d);
        }
        if (this.e != null) {
            aff.a(5, this.e);
        }
        if (this.f != null) {
            aff.b(6, this.f.longValue());
        }
        if (this.g != null) {
            aff.b(7, this.g.longValue());
        }
        if (this.h != null) {
            aff.a(8, this.h);
        }
        if (this.i != null) {
            aff.b(9, this.i.longValue());
        }
        if (this.j != null) {
            aff.a(10, this.j);
        }
        super.a(aff);
    }
}
