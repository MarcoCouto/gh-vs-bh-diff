package com.google.android.gms.internal.ads;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;

public final class aqx extends ajk implements aqv {
    aqx(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.client.IVideoLifecycleCallbacks");
    }

    public final void a() throws RemoteException {
        b(1, r_());
    }

    public final void a(boolean z) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, z);
        b(5, r_);
    }

    public final void b() throws RemoteException {
        b(2, r_());
    }

    public final void c() throws RemoteException {
        b(3, r_());
    }

    public final void d() throws RemoteException {
        b(4, r_());
    }
}
