package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.ads.internal.ae;
import com.google.android.gms.ads.internal.ax;
import com.google.android.gms.ads.internal.bv;
import com.google.android.gms.ads.internal.gmsg.c;
import org.json.JSONObject;

@cm
public final class bu implements bq<qn> {

    /* renamed from: a reason: collision with root package name */
    private nn<qn> f3206a;

    /* renamed from: b reason: collision with root package name */
    private final c f3207b = new c(this.d);
    private final aue c;
    private final Context d;
    private final mu e;
    private final ae f;
    private final ahh g;
    /* access modifiers changed from: private */
    public String h;

    public bu(Context context, ae aeVar, String str, ahh ahh, mu muVar) {
        jm.d("Webview loading for native ads.");
        this.d = context;
        this.f = aeVar;
        this.g = ahh;
        this.e = muVar;
        this.h = str;
        ax.f();
        nn a2 = qu.a(this.d, this.e, (String) ape.f().a(asi.bX), this.g, this.f.i());
        this.c = new aue(aeVar, str);
        this.f3206a = nc.a(a2, (mx<? super A, ? extends B>) new bv<Object,Object>(this), nt.f3559b);
        na.a(this.f3206a, "WebViewNativeAdsUtil.constructor");
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ nn a(qn qnVar) throws Exception {
        jm.d("Javascript has loaded for native ads.");
        qnVar.v().a(this.f, this.f, this.f, this.f, this.f, false, null, new bv(this.d, null, null), null, null);
        qnVar.a("/logScionEvent", (com.google.android.gms.ads.internal.gmsg.ae<? super qn>) this.f3207b);
        qnVar.a("/logScionEvent", (com.google.android.gms.ads.internal.gmsg.ae<? super qn>) this.c);
        return nc.a(qnVar);
    }

    public final nn<JSONObject> a(JSONObject jSONObject) {
        return nc.a(this.f3206a, (mx<? super A, ? extends B>) new bw<Object,Object>(this, jSONObject), nt.f3558a);
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ nn a(JSONObject jSONObject, qn qnVar) throws Exception {
        jSONObject.put("ads_id", this.h);
        qnVar.b("google.afma.nativeAds.handleDownloadedImpressionPing", jSONObject);
        return nc.a(new JSONObject());
    }

    public final void a() {
        nc.a(this.f3206a, (mz<V>) new cf<V>(this), nt.f3558a);
    }

    public final void a(String str, com.google.android.gms.ads.internal.gmsg.ae<? super qn> aeVar) {
        nc.a(this.f3206a, (mz<V>) new cb<V>(this, str, aeVar), nt.f3558a);
    }

    public final void a(String str, JSONObject jSONObject) {
        nc.a(this.f3206a, (mz<V>) new cd<V>(this, str, jSONObject), nt.f3558a);
    }

    public final nn<JSONObject> b(JSONObject jSONObject) {
        return nc.a(this.f3206a, (mx<? super A, ? extends B>) new bx<Object,Object>(this, jSONObject), nt.f3558a);
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ nn b(JSONObject jSONObject, qn qnVar) throws Exception {
        jSONObject.put("ads_id", this.h);
        qnVar.b("google.afma.nativeAds.handleImpressionPing", jSONObject);
        return nc.a(new JSONObject());
    }

    public final void b(String str, com.google.android.gms.ads.internal.gmsg.ae<? super qn> aeVar) {
        nc.a(this.f3206a, (mz<V>) new cc<V>(this, str, aeVar), nt.f3558a);
    }

    public final nn<JSONObject> c(JSONObject jSONObject) {
        return nc.a(this.f3206a, (mx<? super A, ? extends B>) new by<Object,Object>(this, jSONObject), nt.f3558a);
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ nn c(JSONObject jSONObject, qn qnVar) throws Exception {
        jSONObject.put("ads_id", this.h);
        qnVar.b("google.afma.nativeAds.handleClickGmsg", jSONObject);
        return nc.a(new JSONObject());
    }

    public final nn<JSONObject> d(JSONObject jSONObject) {
        return nc.a(this.f3206a, (mx<? super A, ? extends B>) new bz<Object,Object>(this, jSONObject), nt.f3558a);
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ nn d(JSONObject jSONObject, qn qnVar) throws Exception {
        jSONObject.put("ads_id", this.h);
        ny nyVar = new ny();
        qnVar.a("/nativeAdPreProcess", (com.google.android.gms.ads.internal.gmsg.ae<? super qn>) new ca<Object>(this, qnVar, nyVar));
        qnVar.b("google.afma.nativeAds.preProcessJsonGmsg", jSONObject);
        return nyVar;
    }
}
