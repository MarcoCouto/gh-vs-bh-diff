package com.google.android.gms.internal.ads;

final /* synthetic */ class bl implements rw {

    /* renamed from: a reason: collision with root package name */
    private final bi f3193a;

    /* renamed from: b reason: collision with root package name */
    private final ny f3194b;
    private final qn c;

    bl(bi biVar, ny nyVar, qn qnVar) {
        this.f3193a = biVar;
        this.f3194b = nyVar;
        this.c = qnVar;
    }

    public final void a(boolean z) {
        this.f3193a.a(this.f3194b, this.c, z);
    }
}
