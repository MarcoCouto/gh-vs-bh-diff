package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.xe.b;
import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.interfaces.ECPrivateKey;
import java.security.interfaces.ECPublicKey;
import java.security.spec.ECPoint;
import java.security.spec.ECPrivateKeySpec;

final class uu implements tz<tx> {
    uu() {
    }

    /* access modifiers changed from: private */
    /* renamed from: d */
    public final tx a(aah aah) throws GeneralSecurityException {
        try {
            wn a2 = wn.a(aah);
            if (!(a2 instanceof wn)) {
                throw new GeneralSecurityException("expected EciesAeadHkdfPrivateKey proto");
            }
            wn wnVar = a2;
            zo.a(wnVar.a(), 0);
            vb.a(wnVar.b().b());
            wl b2 = wnVar.b().b();
            wr a3 = b2.a();
            ys a4 = vb.a(a3.a());
            byte[] b3 = wnVar.c().b();
            return new yl((ECPrivateKey) ((KeyFactory) yv.e.a("EC")).generatePrivate(new ECPrivateKeySpec(new BigInteger(1, b3), yq.a(a4))), a3.c().b(), vb.a(a3.b()), vb.a(b2.c()), new vd(b2.b().a()));
        } catch (abv e) {
            throw new GeneralSecurityException("expected serialized EciesAeadHkdfPrivateKey proto", e);
        }
    }

    public final int a() {
        return 0;
    }

    public final /* synthetic */ Object a(acw acw) throws GeneralSecurityException {
        if (!(acw instanceof wn)) {
            throw new GeneralSecurityException("expected EciesAeadHkdfPrivateKey proto");
        }
        wn wnVar = (wn) acw;
        zo.a(wnVar.a(), 0);
        vb.a(wnVar.b().b());
        wl b2 = wnVar.b().b();
        wr a2 = b2.a();
        ys a3 = vb.a(a2.a());
        byte[] b3 = wnVar.c().b();
        return new yl((ECPrivateKey) ((KeyFactory) yv.e.a("EC")).generatePrivate(new ECPrivateKeySpec(new BigInteger(1, b3), yq.a(a3))), a2.c().b(), vb.a(a2.b()), vb.a(b2.c()), new vd(b2.b().a()));
    }

    public final acw b(aah aah) throws GeneralSecurityException {
        try {
            return b((acw) wj.a(aah));
        } catch (abv e) {
            throw new GeneralSecurityException("invalid EciesAeadHkdf key format", e);
        }
    }

    public final acw b(acw acw) throws GeneralSecurityException {
        if (!(acw instanceof wj)) {
            throw new GeneralSecurityException("expected EciesAeadHkdfKeyFormat proto");
        }
        wj wjVar = (wj) acw;
        vb.a(wjVar.a());
        KeyPair a2 = yq.a(yq.a(vb.a(wjVar.a().a().a())));
        ECPublicKey eCPublicKey = (ECPublicKey) a2.getPublic();
        ECPrivateKey eCPrivateKey = (ECPrivateKey) a2.getPrivate();
        ECPoint w = eCPublicKey.getW();
        return wn.d().a(0).a((wp) wp.e().a(0).a(wjVar.a()).a(aah.a(w.getAffineX().toByteArray())).b(aah.a(w.getAffineY().toByteArray())).c()).a(aah.a(eCPrivateKey.getS().toByteArray())).c();
    }

    public final xe c(aah aah) throws GeneralSecurityException {
        return (xe) xe.d().a("type.googleapis.com/google.crypto.tink.EciesAeadHkdfPrivateKey").a(((wn) b(aah)).h()).a(b.ASYMMETRIC_PRIVATE).c();
    }
}
