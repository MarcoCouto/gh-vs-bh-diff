package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.gmsg.ae;
import java.util.Map;

final class fd implements ae<Object> {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ fa f3307a;

    fd(fa faVar) {
        this.f3307a = faVar;
    }

    public final void zza(Object obj, Map<String, String> map) {
        synchronized (this.f3307a.f3304b) {
            if (!this.f3307a.e.isDone()) {
                fg fgVar = new fg(-2, map);
                if (this.f3307a.c.equals(fgVar.h())) {
                    this.f3307a.e.b(fgVar);
                }
            }
        }
    }
}
