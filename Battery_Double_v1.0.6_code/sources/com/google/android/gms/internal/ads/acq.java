package com.google.android.gms.internal.ads;

import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public final class acq<K, V> extends LinkedHashMap<K, V> {

    /* renamed from: b reason: collision with root package name */
    private static final acq f2463b;

    /* renamed from: a reason: collision with root package name */
    private boolean f2464a = true;

    static {
        acq acq = new acq();
        f2463b = acq;
        acq.f2464a = false;
    }

    private acq() {
    }

    private acq(Map<K, V> map) {
        super(map);
    }

    private static int a(Object obj) {
        if (obj instanceof byte[]) {
            return abr.c((byte[]) obj);
        }
        if (!(obj instanceof abs)) {
            return obj.hashCode();
        }
        throw new UnsupportedOperationException();
    }

    public static <K, V> acq<K, V> a() {
        return f2463b;
    }

    private final void e() {
        if (!this.f2464a) {
            throw new UnsupportedOperationException();
        }
    }

    public final void a(acq<K, V> acq) {
        e();
        if (!acq.isEmpty()) {
            putAll(acq);
        }
    }

    public final acq<K, V> b() {
        return isEmpty() ? new acq<>() : new acq<>(this);
    }

    public final void c() {
        this.f2464a = false;
    }

    public final void clear() {
        e();
        super.clear();
    }

    public final boolean d() {
        return this.f2464a;
    }

    public final Set<Entry<K, V>> entrySet() {
        return isEmpty() ? Collections.emptySet() : super.entrySet();
    }

    /* JADX WARNING: Removed duplicated region for block: B:8:0x0017  */
    public final boolean equals(Object obj) {
        boolean z;
        boolean equals;
        if (obj instanceof Map) {
            Map map = (Map) obj;
            if (this != map) {
                if (size() == map.size()) {
                    Iterator it = entrySet().iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            break;
                        }
                        Entry entry = (Entry) it.next();
                        if (map.containsKey(entry.getKey())) {
                            Object value = entry.getValue();
                            Object obj2 = map.get(entry.getKey());
                            if (!(value instanceof byte[]) || !(obj2 instanceof byte[])) {
                                equals = value.equals(obj2);
                                continue;
                            } else {
                                equals = Arrays.equals((byte[]) value, (byte[]) obj2);
                                continue;
                            }
                            if (!equals) {
                                z = false;
                                break;
                            }
                        } else {
                            z = false;
                            break;
                        }
                    }
                } else {
                    z = false;
                }
                if (z) {
                    return true;
                }
            }
            z = true;
            if (z) {
            }
        }
        return false;
    }

    public final int hashCode() {
        int i = 0;
        Iterator it = entrySet().iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                return i2;
            }
            Entry entry = (Entry) it.next();
            i = (a(entry.getValue()) ^ a(entry.getKey())) + i2;
        }
    }

    public final V put(K k, V v) {
        e();
        abr.a(k);
        abr.a(v);
        return super.put(k, v);
    }

    public final void putAll(Map<? extends K, ? extends V> map) {
        e();
        for (Object next : map.keySet()) {
            abr.a(next);
            abr.a(map.get(next));
        }
        super.putAll(map);
    }

    public final V remove(Object obj) {
        e();
        return super.remove(obj);
    }
}
