package com.google.android.gms.internal.ads;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

final class afk implements Cloneable {

    /* renamed from: a reason: collision with root package name */
    private afi<?, ?> f2528a;

    /* renamed from: b reason: collision with root package name */
    private Object f2529b;
    private List<afp> c = new ArrayList();

    afk() {
    }

    private final byte[] b() throws IOException {
        byte[] bArr = new byte[a()];
        a(aff.a(bArr));
        return bArr;
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public final afk clone() {
        int i = 0;
        afk afk = new afk();
        try {
            afk.f2528a = this.f2528a;
            if (this.c == null) {
                afk.c = null;
            } else {
                afk.c.addAll(this.c);
            }
            if (this.f2529b != null) {
                if (this.f2529b instanceof afn) {
                    afk.f2529b = (afn) ((afn) this.f2529b).clone();
                } else if (this.f2529b instanceof byte[]) {
                    afk.f2529b = ((byte[]) this.f2529b).clone();
                } else if (this.f2529b instanceof byte[][]) {
                    byte[][] bArr = (byte[][]) this.f2529b;
                    byte[][] bArr2 = new byte[bArr.length][];
                    afk.f2529b = bArr2;
                    for (int i2 = 0; i2 < bArr.length; i2++) {
                        bArr2[i2] = (byte[]) bArr[i2].clone();
                    }
                } else if (this.f2529b instanceof boolean[]) {
                    afk.f2529b = ((boolean[]) this.f2529b).clone();
                } else if (this.f2529b instanceof int[]) {
                    afk.f2529b = ((int[]) this.f2529b).clone();
                } else if (this.f2529b instanceof long[]) {
                    afk.f2529b = ((long[]) this.f2529b).clone();
                } else if (this.f2529b instanceof float[]) {
                    afk.f2529b = ((float[]) this.f2529b).clone();
                } else if (this.f2529b instanceof double[]) {
                    afk.f2529b = ((double[]) this.f2529b).clone();
                } else if (this.f2529b instanceof afn[]) {
                    afn[] afnArr = (afn[]) this.f2529b;
                    afn[] afnArr2 = new afn[afnArr.length];
                    afk.f2529b = afnArr2;
                    while (true) {
                        int i3 = i;
                        if (i3 >= afnArr.length) {
                            break;
                        }
                        afnArr2[i3] = (afn) afnArr[i3].clone();
                        i = i3 + 1;
                    }
                }
            }
            return afk;
        } catch (CloneNotSupportedException e) {
            throw new AssertionError(e);
        }
    }

    /* access modifiers changed from: 0000 */
    public final int a() {
        int i = 0;
        if (this.f2529b != null) {
            throw new NoSuchMethodError();
        }
        Iterator it = this.c.iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                return i2;
            }
            afp afp = (afp) it.next();
            i = afp.f2533b.length + aff.d(afp.f2532a) + 0 + i2;
        }
    }

    /* access modifiers changed from: 0000 */
    public final void a(aff aff) throws IOException {
        if (this.f2529b != null) {
            throw new NoSuchMethodError();
        }
        for (afp afp : this.c) {
            aff.c(afp.f2532a);
            aff.c(afp.f2533b);
        }
    }

    /* access modifiers changed from: 0000 */
    public final void a(afp afp) throws IOException {
        if (this.c != null) {
            this.c.add(afp);
        } else if (this.f2529b instanceof afn) {
            byte[] bArr = afp.f2533b;
            afd a2 = afd.a(bArr, 0, bArr.length);
            int g = a2.g();
            if (g != bArr.length - aff.a(g)) {
                throw afm.a();
            }
            afn a3 = ((afn) this.f2529b).a(a2);
            this.f2528a = this.f2528a;
            this.f2529b = a3;
            this.c = null;
        } else if (this.f2529b instanceof afn[]) {
            Collections.singletonList(afp);
            throw new NoSuchMethodError();
        } else {
            Collections.singletonList(afp);
            throw new NoSuchMethodError();
        }
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof afk)) {
            return false;
        }
        afk afk = (afk) obj;
        if (this.f2529b == null || afk.f2529b == null) {
            if (this.c != null && afk.c != null) {
                return this.c.equals(afk.c);
            }
            try {
                return Arrays.equals(b(), afk.b());
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        } else if (this.f2528a == afk.f2528a) {
            return !this.f2528a.f2525a.isArray() ? this.f2529b.equals(afk.f2529b) : this.f2529b instanceof byte[] ? Arrays.equals((byte[]) this.f2529b, (byte[]) afk.f2529b) : this.f2529b instanceof int[] ? Arrays.equals((int[]) this.f2529b, (int[]) afk.f2529b) : this.f2529b instanceof long[] ? Arrays.equals((long[]) this.f2529b, (long[]) afk.f2529b) : this.f2529b instanceof float[] ? Arrays.equals((float[]) this.f2529b, (float[]) afk.f2529b) : this.f2529b instanceof double[] ? Arrays.equals((double[]) this.f2529b, (double[]) afk.f2529b) : this.f2529b instanceof boolean[] ? Arrays.equals((boolean[]) this.f2529b, (boolean[]) afk.f2529b) : Arrays.deepEquals((Object[]) this.f2529b, (Object[]) afk.f2529b);
        } else {
            return false;
        }
    }

    public final int hashCode() {
        try {
            return Arrays.hashCode(b()) + 527;
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }
}
