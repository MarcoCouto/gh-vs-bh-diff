package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import com.google.ads.mediation.admob.AdMobAdapter;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import org.json.JSONObject;

@cm
public final class au extends ak {
    protected bci g;
    private bcr h;
    private bbz i;
    private bcb j;
    private final asv k;
    /* access modifiers changed from: private */
    public final qn l;
    /* access modifiers changed from: private */
    public boolean m;

    au(Context context, is isVar, bcr bcr, ap apVar, asv asv, qn qnVar) {
        super(context, isVar, apVar);
        this.h = bcr;
        this.j = isVar.c;
        this.k = asv;
        this.l = qnVar;
    }

    /* access modifiers changed from: protected */
    public final ir a(int i2) {
        String str;
        int i3;
        dl dlVar = this.e.f3397a;
        aop aop = dlVar.c;
        qn qnVar = this.l;
        List<String> list = this.f.c;
        List<String> list2 = this.f.e;
        List<String> list3 = this.f.i;
        int i4 = this.f.k;
        long j2 = this.f.j;
        String str2 = dlVar.i;
        boolean z = this.f.g;
        bca bca = this.g != null ? this.g.f3133b : null;
        bcu bcu = this.g != null ? this.g.c : null;
        String name = this.g != null ? this.g.d : AdMobAdapter.class.getName();
        bcb bcb = this.j;
        bce bce = this.g != null ? this.g.e : null;
        long j3 = this.f.h;
        aot aot = this.e.d;
        long j4 = this.f.f;
        long j5 = this.e.f;
        long j6 = this.f.m;
        String str3 = this.f.n;
        JSONObject jSONObject = this.e.h;
        hp hpVar = this.f.A;
        List<String> list4 = this.f.B;
        List<String> list5 = this.f.C;
        boolean z2 = this.j != null ? this.j.o : false;
        dr drVar = this.f.E;
        if (this.i != null) {
            List<bci> b2 = this.i.b();
            String str4 = "";
            if (b2 == null) {
                str = str4.toString();
            } else {
                String str5 = str4;
                for (bci bci : b2) {
                    if (!(bci == null || bci.f3133b == null || TextUtils.isEmpty(bci.f3133b.d))) {
                        String valueOf = String.valueOf(str5);
                        String str6 = bci.f3133b.d;
                        switch (bci.f3132a) {
                            case -1:
                                i3 = 4;
                                break;
                            case 0:
                                i3 = 0;
                                break;
                            case 1:
                                i3 = 1;
                                break;
                            case 3:
                                i3 = 2;
                                break;
                            case 4:
                                i3 = 3;
                                break;
                            case 5:
                                i3 = 5;
                                break;
                            default:
                                i3 = 6;
                                break;
                        }
                        long j7 = bci.g;
                        StringBuilder sb = new StringBuilder(String.valueOf(str6).length() + 33);
                        String sb2 = sb.append(str6).append(".").append(i3).append(".").append(j7).toString();
                        StringBuilder sb3 = new StringBuilder(String.valueOf(valueOf).length() + 1 + String.valueOf(sb2).length());
                        str5 = sb3.append(valueOf).append(sb2).append("_").toString();
                    }
                }
                str = str5.substring(0, Math.max(0, str5.length() - 1));
            }
        } else {
            str = null;
        }
        return new ir(aop, qnVar, list, i2, list2, list3, i4, j2, str2, z, bca, bcu, name, bcb, bce, j3, aot, j4, j5, j6, str3, jSONObject, null, hpVar, list4, list5, z2, drVar, str, this.f.H, this.f.L, this.e.i, this.f.O, this.e.j, this.f.Q, this.f.R, this.f.S, this.f.T);
    }

    /* access modifiers changed from: protected */
    public final void a(long j2) throws an {
        bbz bco;
        synchronized (this.d) {
            if (this.j.m != -1) {
                bco = new bcl(this.f2654b, this.e.f3397a, this.h, this.j, this.f.s, this.f.z, this.f.J, j2, ((Long) ape.f().a(asi.bB)).longValue(), 2, this.e.j);
            } else {
                bco = new bco(this.f2654b, this.e.f3397a, this.h, this.j, this.f.s, this.f.z, this.f.J, j2, ((Long) ape.f().a(asi.bB)).longValue(), this.k, this.e.j);
            }
            this.i = bco;
        }
        ArrayList arrayList = new ArrayList(this.j.f3121a);
        boolean z = false;
        Bundle bundle = this.e.f3397a.c.m;
        String str = "com.google.ads.mediation.admob.AdMobAdapter";
        if (bundle != null) {
            Bundle bundle2 = bundle.getBundle(str);
            if (bundle2 != null) {
                z = bundle2.getBoolean("_skipMediation");
            }
        }
        if (z) {
            ListIterator listIterator = arrayList.listIterator();
            while (listIterator.hasNext()) {
                if (!((bca) listIterator.next()).c.contains(str)) {
                    listIterator.remove();
                }
            }
        }
        this.g = this.i.a(arrayList);
        switch (this.g.f3132a) {
            case 0:
                if (this.g.f3133b != null && this.g.f3133b.o != null) {
                    CountDownLatch countDownLatch = new CountDownLatch(1);
                    jv.f3440a.post(new av(this, countDownLatch));
                    try {
                        countDownLatch.await(10, TimeUnit.SECONDS);
                        synchronized (this.d) {
                            if (!this.m) {
                                throw new an("View could not be prepared", 0);
                            } else if (this.l.A()) {
                                throw new an("Assets not loaded, web view is destroyed", 0);
                            }
                        }
                        return;
                    } catch (InterruptedException e) {
                        String valueOf = String.valueOf(e);
                        throw new an(new StringBuilder(String.valueOf(valueOf).length() + 38).append("Interrupted while waiting for latch : ").append(valueOf).toString(), 0);
                    }
                } else {
                    return;
                }
            case 1:
                throw new an("No fill from any mediation ad networks.", 3);
            default:
                throw new an("Unexpected mediation result: " + this.g.f3132a, 0);
        }
    }

    public final void c_() {
        synchronized (this.d) {
            super.c_();
            if (this.i != null) {
                this.i.a();
            }
        }
    }
}
