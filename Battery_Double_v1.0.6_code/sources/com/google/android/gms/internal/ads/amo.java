package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.DeadObjectException;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.google.android.gms.common.internal.e;
import com.google.android.gms.common.internal.e.a;
import com.google.android.gms.common.internal.e.b;

@cm
public final class amo extends e<ams> {
    amo(Context context, Looper looper, a aVar, b bVar) {
        super(context, looper, 123, aVar, bVar, null);
    }

    public final ams A() throws DeadObjectException {
        return (ams) super.x();
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ IInterface a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.ads.internal.cache.ICacheService");
        return queryLocalInterface instanceof ams ? (ams) queryLocalInterface : new amt(iBinder);
    }

    /* access modifiers changed from: protected */
    public final String i() {
        return "com.google.android.gms.ads.service.CACHE";
    }

    /* access modifiers changed from: protected */
    public final String l() {
        return "com.google.android.gms.ads.internal.cache.ICacheService";
    }
}
