package com.google.android.gms.internal.ads;

import android.view.MotionEvent;
import android.view.View;

final class aur implements atw {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ View f2947a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ aup f2948b;

    aur(aup aup, View view) {
        this.f2948b = aup;
        this.f2947a = view;
    }

    public final void a() {
        if (this.f2948b.a(aup.f2943a)) {
            this.f2948b.onClick(this.f2947a);
        }
    }

    public final void a(MotionEvent motionEvent) {
        this.f2948b.onTouch(null, motionEvent);
    }
}
