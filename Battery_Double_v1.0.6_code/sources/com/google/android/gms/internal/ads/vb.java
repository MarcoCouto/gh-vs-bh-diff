package com.google.android.gms.internal.ads;

import java.security.GeneralSecurityException;
import java.security.NoSuchAlgorithmException;

final class vb {
    public static ys a(wt wtVar) throws GeneralSecurityException {
        switch (vc.f3736b[wtVar.ordinal()]) {
            case 1:
                return ys.NIST_P256;
            case 2:
                return ys.NIST_P384;
            case 3:
                return ys.NIST_P521;
            default:
                String valueOf = String.valueOf(wtVar);
                throw new GeneralSecurityException(new StringBuilder(String.valueOf(valueOf).length() + 20).append("unknown curve type: ").append(valueOf).toString());
        }
    }

    public static yt a(wf wfVar) throws GeneralSecurityException {
        switch (vc.c[wfVar.ordinal()]) {
            case 1:
                return yt.UNCOMPRESSED;
            case 2:
                return yt.COMPRESSED;
            default:
                String valueOf = String.valueOf(wfVar);
                throw new GeneralSecurityException(new StringBuilder(String.valueOf(valueOf).length() + 22).append("unknown point format: ").append(valueOf).toString());
        }
    }

    public static String a(ww wwVar) throws NoSuchAlgorithmException {
        switch (vc.f3735a[wwVar.ordinal()]) {
            case 1:
                return "HmacSha1";
            case 2:
                return "HmacSha256";
            case 3:
                return "HmacSha512";
            default:
                String valueOf = String.valueOf(wwVar);
                throw new NoSuchAlgorithmException(new StringBuilder(String.valueOf(valueOf).length() + 27).append("hash unsupported for HMAC: ").append(valueOf).toString());
        }
    }

    public static void a(wl wlVar) throws GeneralSecurityException {
        yq.a(a(wlVar.a().a()));
        a(wlVar.a().b());
        if (wlVar.c() == wf.UNKNOWN_FORMAT) {
            throw new GeneralSecurityException("unknown EC point format");
        }
        uh.a(wlVar.b().a());
    }
}
