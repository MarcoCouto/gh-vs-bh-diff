package com.google.android.gms.internal.ads;

import java.io.IOException;

public final class aed extends afh<aed> {

    /* renamed from: a reason: collision with root package name */
    public byte[] f2500a;

    /* renamed from: b reason: collision with root package name */
    public byte[] f2501b;
    public byte[] c;
    public byte[] d;

    public aed() {
        this.f2500a = null;
        this.f2501b = null;
        this.c = null;
        this.d = null;
        this.Z = -1;
    }

    /* access modifiers changed from: protected */
    public final int a() {
        int a2 = super.a();
        if (this.f2500a != null) {
            a2 += aff.b(1, this.f2500a);
        }
        if (this.f2501b != null) {
            a2 += aff.b(2, this.f2501b);
        }
        if (this.c != null) {
            a2 += aff.b(3, this.c);
        }
        return this.d != null ? a2 + aff.b(4, this.d) : a2;
    }

    public final /* synthetic */ afn a(afd afd) throws IOException {
        while (true) {
            int a2 = afd.a();
            switch (a2) {
                case 0:
                    break;
                case 10:
                    this.f2500a = afd.f();
                    continue;
                case 18:
                    this.f2501b = afd.f();
                    continue;
                case 26:
                    this.c = afd.f();
                    continue;
                case 34:
                    this.d = afd.f();
                    continue;
                default:
                    if (!super.a(afd, a2)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    public final void a(aff aff) throws IOException {
        if (this.f2500a != null) {
            aff.a(1, this.f2500a);
        }
        if (this.f2501b != null) {
            aff.a(2, this.f2501b);
        }
        if (this.c != null) {
            aff.a(3, this.c);
        }
        if (this.d != null) {
            aff.a(4, this.d);
        }
        super.a(aff);
    }
}
