package com.google.android.gms.internal.ads;

final class axb implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ String f2972a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ long f2973b;
    private final /* synthetic */ awb c;

    axb(awb awb, String str, long j) {
        this.c = awb;
        this.f2972a = str;
        this.f2973b = j;
    }

    public final void run() {
        this.c.f2963a.a(this.f2972a, this.f2973b);
        this.c.f2963a.a(toString());
    }
}
