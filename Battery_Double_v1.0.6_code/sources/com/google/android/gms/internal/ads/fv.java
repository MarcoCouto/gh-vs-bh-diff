package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.ads.internal.ax;
import org.json.JSONObject;

@cm
public final class fv implements ey {

    /* renamed from: a reason: collision with root package name */
    private bbi<JSONObject, JSONObject> f3328a;

    /* renamed from: b reason: collision with root package name */
    private bbi<JSONObject, JSONObject> f3329b;

    public fv(Context context) {
        this.f3328a = ax.s().a(context, mu.a()).a("google.afma.request.getAdDictionary", bbn.f3104a, bbn.f3104a);
        this.f3329b = ax.s().a(context, mu.a()).a("google.afma.sdkConstants.getSdkConstants", bbn.f3104a, bbn.f3104a);
    }

    public final bbi<JSONObject, JSONObject> a() {
        return this.f3328a;
    }

    public final bbi<JSONObject, JSONObject> b() {
        return this.f3329b;
    }
}
