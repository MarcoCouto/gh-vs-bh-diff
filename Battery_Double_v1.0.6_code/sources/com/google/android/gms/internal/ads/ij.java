package com.google.android.gms.internal.ads;

import android.os.SystemClock;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeSet;

public class ij implements are {

    /* renamed from: a reason: collision with root package name */
    private static final boolean f3390a = eg.f3276a;
    @Deprecated

    /* renamed from: b reason: collision with root package name */
    private final qr f3391b;
    private final hi c;
    private final jk d;

    public ij(hi hiVar) {
        this(hiVar, new jk(4096));
    }

    private ij(hi hiVar, jk jkVar) {
        this.c = hiVar;
        this.f3391b = hiVar;
        this.d = jkVar;
    }

    @Deprecated
    public ij(qr qrVar) {
        this(qrVar, new jk(4096));
    }

    @Deprecated
    private ij(qr qrVar, jk jkVar) {
        this.f3391b = qrVar;
        this.c = new gi(qrVar);
        this.d = jkVar;
    }

    private static void a(String str, awb<?> awb, df dfVar) throws df {
        ac j = awb.j();
        int i = awb.i();
        try {
            j.a(dfVar);
            awb.b(String.format("%s-retry [timeout=%s]", new Object[]{str, Integer.valueOf(i)}));
        } catch (df e) {
            awb.b(String.format("%s-timeout-giveup [timeout=%s]", new Object[]{str, Integer.valueOf(i)}));
            throw e;
        }
    }

    private final byte[] a(InputStream inputStream, int i) throws IOException, bd {
        ts tsVar = new ts(this.d, i);
        if (inputStream == null) {
            try {
                throw new bd();
            } catch (Throwable th) {
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException e) {
                        eg.a("Error occurred when closing InputStream", new Object[0]);
                    }
                }
                this.d.a((byte[]) null);
                tsVar.close();
                throw th;
            }
        } else {
            byte[] a2 = this.d.a(1024);
            while (true) {
                int read = inputStream.read(a2);
                if (read == -1) {
                    break;
                }
                tsVar.write(a2, 0, read);
            }
            byte[] byteArray = tsVar.toByteArray();
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e2) {
                    eg.a("Error occurred when closing InputStream", new Object[0]);
                }
            }
            this.d.a(a2);
            tsVar.close();
            return byteArray;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:100:0x0212, code lost:
        throw new com.google.android.gms.internal.ads.alc(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:102:0x0215, code lost:
        if (r3 < 500) goto L_0x0221;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:106:0x0220, code lost:
        throw new com.google.android.gms.internal.ads.bd(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:108:0x0226, code lost:
        throw new com.google.android.gms.internal.ads.bd(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:109:0x0227, code lost:
        a("network", r21, new com.google.android.gms.internal.ads.asy());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:110:0x0235, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:111:0x0236, code lost:
        r4 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:132:?, code lost:
        return new com.google.android.gms.internal.ads.atz(304, r4.f2576a, true, android.os.SystemClock.elapsedRealtime() - r18, (java.util.List<com.google.android.gms.internal.ads.aqd>) r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0065, code lost:
        a("socket", r21, new com.google.android.gms.internal.ads.ce());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0097, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0098, code lost:
        r3 = r2;
        r5 = "Bad URL ";
        r2 = java.lang.String.valueOf(r21.e());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00a9, code lost:
        if (r2.length() != 0) goto L_0x00ab;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00ab, code lost:
        r2 = r5.concat(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00b2, code lost:
        throw new java.lang.RuntimeException(r2, r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00e8, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00e9, code lost:
        r4 = null;
        r3 = r17;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00ee, code lost:
        r3 = r3.a();
        com.google.android.gms.internal.ads.eg.c("Unexpected response code %d for %s", java.lang.Integer.valueOf(r3), r21.e());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x0108, code lost:
        if (r4 != null) goto L_0x010a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x010a, code lost:
        r2 = new com.google.android.gms.internal.ads.atz(r3, r4, false, android.os.SystemClock.elapsedRealtime() - r18, r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x0118, code lost:
        if (r3 == 401) goto L_0x011e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x011e, code lost:
        a("auth", r21, new com.google.android.gms.internal.ads.a(r2));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:0x01f8, code lost:
        r2 = new java.lang.String(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:94:0x0204, code lost:
        throw new com.google.android.gms.internal.ads.ava(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:0x0207, code lost:
        if (r3 < 400) goto L_0x0213;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:119:0x01ff A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0064 A[ExcHandler: SocketTimeoutException (e java.net.SocketTimeoutException), Splitter:B:35:0x00b3] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0097 A[ExcHandler: MalformedURLException (r2v5 'e' java.net.MalformedURLException A[CUSTOM_DECLARE]), Splitter:B:2:0x000a] */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x00ee  */
    public atz a(awb<?> awb) throws df {
        Map hashMap;
        pq a2;
        long elapsedRealtime = SystemClock.elapsedRealtime();
        while (true) {
            pq pqVar = null;
            List<aqd> emptyList = Collections.emptyList();
            try {
                agy f = awb.f();
                if (f == null) {
                    hashMap = Collections.emptyMap();
                } else {
                    hashMap = new HashMap();
                    if (f.f2577b != null) {
                        hashMap.put("If-None-Match", f.f2577b);
                    }
                    if (f.d > 0) {
                        hashMap.put("If-Modified-Since", op.a(f.d));
                    }
                }
                a2 = this.c.a(awb, hashMap);
                int a3 = a2.a();
                emptyList = a2.b();
                if (a3 == 304) {
                    agy f2 = awb.f();
                    if (f2 == null) {
                        return new atz(304, (byte[]) null, true, SystemClock.elapsedRealtime() - elapsedRealtime, emptyList);
                    }
                    TreeSet treeSet = new TreeSet(String.CASE_INSENSITIVE_ORDER);
                    if (!emptyList.isEmpty()) {
                        for (aqd a4 : emptyList) {
                            treeSet.add(a4.a());
                        }
                    }
                    ArrayList arrayList = new ArrayList(emptyList);
                    if (f2.h != null) {
                        if (!f2.h.isEmpty()) {
                            for (aqd aqd : f2.h) {
                                if (!treeSet.contains(aqd.a())) {
                                    arrayList.add(aqd);
                                }
                            }
                        }
                    } else if (!f2.g.isEmpty()) {
                        for (Entry entry : f2.g.entrySet()) {
                            if (!treeSet.contains(entry.getKey())) {
                                arrayList.add(new aqd((String) entry.getKey(), (String) entry.getValue()));
                            }
                        }
                    }
                } else {
                    InputStream d2 = a2.d();
                    byte[] bArr = d2 != null ? a(d2, a2.c()) : new byte[0];
                    long elapsedRealtime2 = SystemClock.elapsedRealtime() - elapsedRealtime;
                    if (f3390a || elapsedRealtime2 > 3000) {
                        String str = "HTTP response for request=<%s> [lifetime=%d], [size=%s], [rc=%d], [retryCount=%s]";
                        Object[] objArr = new Object[5];
                        objArr[0] = awb;
                        objArr[1] = Long.valueOf(elapsedRealtime2);
                        objArr[2] = bArr != null ? Integer.valueOf(bArr.length) : "null";
                        objArr[3] = Integer.valueOf(a3);
                        objArr[4] = Integer.valueOf(awb.j().b());
                        eg.b(str, objArr);
                    }
                    if (a3 >= 200 && a3 <= 299) {
                        return new atz(a3, bArr, false, SystemClock.elapsedRealtime() - elapsedRealtime, emptyList);
                    }
                }
            } catch (SocketTimeoutException e) {
            } catch (MalformedURLException e2) {
            } catch (IOException e3) {
                e = e3;
                pqVar = a2;
                if (pqVar != null) {
                }
            }
        }
        throw new IOException();
    }
}
