package com.google.android.gms.internal.ads;

import java.nio.ByteBuffer;
import java.security.GeneralSecurityException;
import java.util.Arrays;

public final class yu implements tr {

    /* renamed from: a reason: collision with root package name */
    private final zg f3807a;

    /* renamed from: b reason: collision with root package name */
    private final ud f3808b;
    private final int c;

    public yu(zg zgVar, ud udVar, int i) {
        this.f3807a = zgVar;
        this.f3808b = udVar;
        this.c = i;
    }

    public final byte[] a(byte[] bArr, byte[] bArr2) throws GeneralSecurityException {
        byte[] a2 = this.f3807a.a(bArr);
        if (bArr2 == null) {
            bArr2 = new byte[0];
        }
        byte[] copyOf = Arrays.copyOf(ByteBuffer.allocate(8).putLong(8 * ((long) bArr2.length)).array(), 8);
        return yh.a(a2, this.f3808b.a(yh.a(bArr2, a2, copyOf)));
    }
}
