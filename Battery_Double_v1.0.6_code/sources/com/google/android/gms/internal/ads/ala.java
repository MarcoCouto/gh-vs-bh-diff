package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.app.Application.ActivityLifecycleCallbacks;

final class ala implements alf {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ Activity f2687a;

    ala(akw akw, Activity activity) {
        this.f2687a = activity;
    }

    public final void a(ActivityLifecycleCallbacks activityLifecycleCallbacks) {
        activityLifecycleCallbacks.onActivityPaused(this.f2687a);
    }
}
