package com.google.android.gms.internal.ads;

import java.util.Arrays;

final class aaj implements aal {
    private aaj() {
    }

    /* synthetic */ aaj(aai aai) {
        this();
    }

    public final byte[] a(byte[] bArr, int i, int i2) {
        return Arrays.copyOfRange(bArr, i, i + i2);
    }
}
