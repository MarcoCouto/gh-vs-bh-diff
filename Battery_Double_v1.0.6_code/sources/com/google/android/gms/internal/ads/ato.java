package com.google.android.gms.internal.ads;

final class ato implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ atn f2908a;

    ato(atn atn) {
        this.f2908a = atn;
    }

    public final void run() {
        if (this.f2908a.p != null) {
            this.f2908a.p.i();
            this.f2908a.p.h();
            this.f2908a.p.k();
        }
        this.f2908a.p = null;
    }
}
