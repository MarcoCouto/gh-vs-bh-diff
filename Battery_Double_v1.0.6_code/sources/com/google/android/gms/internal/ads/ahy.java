package com.google.android.gms.internal.ads;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build.VERSION;
import android.util.Log;
import android.util.Pair;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.common.h;
import com.google.android.gms.common.l;
import com.google.android.gms.common.m;
import dalvik.system.DexClassLoader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class ahy {

    /* renamed from: b reason: collision with root package name */
    private static final String f2606b = ahy.class.getSimpleName();

    /* renamed from: a reason: collision with root package name */
    protected Context f2607a;
    private ExecutorService c;
    private DexClassLoader d;
    private ahj e;
    private byte[] f;
    private volatile AdvertisingIdClient g = null;
    private volatile boolean h = false;
    private Future i = null;
    private boolean j;
    /* access modifiers changed from: private */
    public volatile zz k = null;
    private Future l = null;
    private ahb m;
    private boolean n = false;
    private boolean o = false;
    private Map<Pair<String, String>, ajh> p;
    private boolean q = false;
    /* access modifiers changed from: private */
    public boolean r = true;
    private boolean s = false;

    final class a extends BroadcastReceiver {
        private a() {
        }

        /* synthetic */ a(ahy ahy, aia aia) {
            this();
        }

        public final void onReceive(Context context, Intent intent) {
            if ("android.intent.action.USER_PRESENT".equals(intent.getAction())) {
                ahy.this.r = true;
            } else if ("android.intent.action.SCREEN_OFF".equals(intent.getAction())) {
                ahy.this.r = false;
            }
        }
    }

    private ahy(Context context) {
        boolean z = true;
        Context applicationContext = context.getApplicationContext();
        if (applicationContext == null) {
            z = false;
        }
        this.j = z;
        if (this.j) {
            context = applicationContext;
        }
        this.f2607a = context;
        this.p = new HashMap();
    }

    public static ahy a(Context context, String str, String str2, boolean z) {
        File file;
        String str3;
        File file2;
        boolean z2 = true;
        ahy ahy = new ahy(context);
        try {
            ahy.c = Executors.newCachedThreadPool(new aia());
            ahy.h = z;
            if (z) {
                ahy.i = ahy.c.submit(new aib(ahy));
            }
            ahy.c.execute(new aid(ahy));
            try {
                h b2 = h.b();
                ahy.n = b2.b(ahy.f2607a) > 0;
                if (b2.a(ahy.f2607a) != 0) {
                    z2 = false;
                }
                ahy.o = z2;
            } catch (Throwable th) {
            }
            ahy.a(0, true);
            if (aig.a()) {
                if (((Boolean) ape.f().a(asi.bM)).booleanValue()) {
                    throw new IllegalStateException("Task Context initialization must not be called from the UI thread.");
                }
            }
            ahy.e = new ahj(null);
            ahy.f = ahy.e.a(str);
            File cacheDir = ahy.f2607a.getCacheDir();
            if (cacheDir == null) {
                cacheDir = ahy.f2607a.getDir("dex", 0);
                if (cacheDir == null) {
                    throw new ahv();
                }
            }
            file = cacheDir;
            str3 = "1521499837408";
            file2 = new File(String.format("%s/%s.jar", new Object[]{file, str3}));
            if (!file2.exists()) {
                byte[] a2 = ahy.e.a(ahy.f, str2);
                file2.createNewFile();
                FileOutputStream fileOutputStream = new FileOutputStream(file2);
                fileOutputStream.write(a2, 0, a2.length);
                fileOutputStream.close();
            }
            ahy.b(file, str3);
            ahy.d = new DexClassLoader(file2.getAbsolutePath(), file.getAbsolutePath(), null, ahy.f2607a.getClassLoader());
            a(file2);
            ahy.a(file, str3);
            a(String.format("%s/%s.dex", new Object[]{file, str3}));
            if (!ahy.s) {
                IntentFilter intentFilter = new IntentFilter();
                intentFilter.addAction("android.intent.action.USER_PRESENT");
                intentFilter.addAction("android.intent.action.SCREEN_OFF");
                ahy.f2607a.registerReceiver(new a(ahy, null), intentFilter);
                ahy.s = true;
            }
            ahy.m = new ahb(ahy);
            ahy.q = true;
        } catch (ahk e2) {
            throw new ahv(e2);
        } catch (FileNotFoundException e3) {
            throw new ahv(e3);
        } catch (IOException e4) {
            throw new ahv(e4);
        } catch (ahk e5) {
            throw new ahv(e5);
        } catch (NullPointerException e6) {
            throw new ahv(e6);
        } catch (ahv e7) {
        } catch (Throwable th2) {
            a(file2);
            ahy.a(file, str3);
            a(String.format("%s/%s.dex", new Object[]{file, str3}));
            throw th2;
        }
        return ahy;
    }

    private static void a(File file) {
        if (!file.exists()) {
            Log.d(f2606b, String.format("File %s not found. No need for deletion", new Object[]{file.getAbsolutePath()}));
            return;
        }
        file.delete();
    }

    /* JADX WARNING: type inference failed for: r1v0 */
    /* JADX WARNING: type inference failed for: r0v10 */
    /* JADX WARNING: type inference failed for: r0v12 */
    /* JADX WARNING: type inference failed for: r2v2, types: [java.io.FileInputStream] */
    /* JADX WARNING: type inference failed for: r1v1, types: [java.io.FileOutputStream] */
    /* JADX WARNING: type inference failed for: r2v4 */
    /* JADX WARNING: type inference failed for: r1v3, types: [java.io.FileInputStream] */
    /* JADX WARNING: type inference failed for: r0v15, types: [java.io.FileOutputStream] */
    /* JADX WARNING: type inference failed for: r0v18 */
    /* JADX WARNING: type inference failed for: r0v20 */
    /* JADX WARNING: type inference failed for: r1v5 */
    /* JADX WARNING: type inference failed for: r0v22 */
    /* JADX WARNING: type inference failed for: r1v6 */
    /* JADX WARNING: type inference failed for: r0v24 */
    /* JADX WARNING: type inference failed for: r1v7 */
    /* JADX WARNING: type inference failed for: r1v9 */
    /* JADX WARNING: type inference failed for: r1v11 */
    /* JADX WARNING: type inference failed for: r1v13 */
    /* JADX WARNING: type inference failed for: r1v15 */
    /* JADX WARNING: type inference failed for: r1v18 */
    /* JADX WARNING: type inference failed for: r1v19 */
    /* JADX WARNING: type inference failed for: r1v20 */
    /* JADX WARNING: type inference failed for: r1v21 */
    /* JADX WARNING: type inference failed for: r1v22 */
    /* JADX WARNING: type inference failed for: r2v6 */
    /* JADX WARNING: type inference failed for: r2v7 */
    /* JADX WARNING: type inference failed for: r0v33 */
    /* JADX WARNING: type inference failed for: r0v34 */
    /* JADX WARNING: type inference failed for: r0v35 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x009b A[SYNTHETIC, Splitter:B:27:0x009b] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00a0 A[SYNTHETIC, Splitter:B:30:0x00a0] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00ac A[SYNTHETIC, Splitter:B:36:0x00ac] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00b1 A[SYNTHETIC, Splitter:B:39:0x00b1] */
    /* JADX WARNING: Unknown variable types count: 5 */
    private final void a(File file, String str) {
        ? r0;
        ? r2;
        ? r1;
        ? r12;
        ? r13 = 0;
        File file2 = new File(String.format("%s/%s.tmp", new Object[]{file, str}));
        if (!file2.exists()) {
            File file3 = new File(String.format("%s/%s.dex", new Object[]{file, str}));
            if (file3.exists()) {
                long length = file3.length();
                if (length > 0) {
                    byte[] bArr = new byte[((int) length)];
                    try {
                        FileInputStream fileInputStream = new FileInputStream(file3);
                        try {
                            if (fileInputStream.read(bArr) <= 0) {
                                try {
                                    fileInputStream.close();
                                } catch (IOException e2) {
                                }
                                a(file3);
                                return;
                            }
                            aed aed = new aed();
                            aed.d = VERSION.SDK.getBytes();
                            aed.c = str.getBytes();
                            byte[] bytes = this.e.a(this.f, bArr).getBytes();
                            aed.f2500a = bytes;
                            aed.f2501b = agi.a(bytes);
                            file2.createNewFile();
                            FileOutputStream fileOutputStream = new FileOutputStream(file2);
                            try {
                                byte[] a2 = afn.a((afn) aed);
                                fileOutputStream.write(a2, 0, a2.length);
                                fileOutputStream.close();
                                try {
                                    fileInputStream.close();
                                } catch (IOException e3) {
                                }
                                try {
                                    fileOutputStream.close();
                                } catch (IOException e4) {
                                }
                                a(file3);
                            } catch (IOException e5) {
                                r12 = fileInputStream;
                                r0 = fileOutputStream;
                                if (r12 != 0) {
                                }
                                if (r0 != 0) {
                                }
                                a(file3);
                            } catch (NoSuchAlgorithmException e6) {
                                r12 = fileInputStream;
                                r0 = fileOutputStream;
                                if (r12 != 0) {
                                }
                                if (r0 != 0) {
                                }
                                a(file3);
                            } catch (ahk e7) {
                                r12 = fileInputStream;
                                r0 = fileOutputStream;
                                if (r12 != 0) {
                                }
                                if (r0 != 0) {
                                }
                                a(file3);
                            } catch (Throwable th) {
                                Throwable th2 = th;
                                r1 = fileOutputStream;
                                th = th2;
                                r2 = fileInputStream;
                                if (r2 != 0) {
                                }
                                if (r1 != 0) {
                                }
                                a(file3);
                                throw th;
                            }
                        } catch (IOException e8) {
                            r0 = 0;
                            r12 = fileInputStream;
                        } catch (NoSuchAlgorithmException e9) {
                            r0 = 0;
                            r12 = fileInputStream;
                        } catch (ahk e10) {
                            r0 = 0;
                            r12 = fileInputStream;
                        } catch (Throwable th3) {
                            th = th3;
                            r1 = r13;
                            r2 = fileInputStream;
                            if (r2 != 0) {
                            }
                            if (r1 != 0) {
                            }
                            a(file3);
                            throw th;
                        }
                    } catch (IOException e11) {
                        r0 = 0;
                        r12 = r13;
                        if (r12 != 0) {
                            try {
                                r12.close();
                            } catch (IOException e12) {
                            }
                        }
                        if (r0 != 0) {
                            try {
                                r0.close();
                            } catch (IOException e13) {
                            }
                        }
                        a(file3);
                    } catch (NoSuchAlgorithmException e14) {
                        r0 = 0;
                        r12 = r13;
                        if (r12 != 0) {
                        }
                        if (r0 != 0) {
                        }
                        a(file3);
                    } catch (ahk e15) {
                        r0 = 0;
                        r12 = r13;
                        if (r12 != 0) {
                        }
                        if (r0 != 0) {
                        }
                        a(file3);
                    } catch (Throwable th4) {
                        th = th4;
                        r2 = 0;
                        r1 = r13;
                        if (r2 != 0) {
                            try {
                                r2.close();
                            } catch (IOException e16) {
                            }
                        }
                        if (r1 != 0) {
                            try {
                                r1.close();
                            } catch (IOException e17) {
                            }
                        }
                        a(file3);
                        throw th;
                    }
                }
            }
        }
    }

    private static void a(String str) {
        a(new File(str));
    }

    /* access modifiers changed from: private */
    public static boolean b(int i2, zz zzVar) {
        if (i2 < 4) {
            if (zzVar == null) {
                return true;
            }
            if (((Boolean) ape.f().a(asi.bP)).booleanValue() && (zzVar.n == null || zzVar.n.equals("0000000000000000000000000000000000000000000000000000000000000000"))) {
                return true;
            }
            if (((Boolean) ape.f().a(asi.bQ)).booleanValue() && (zzVar.X == null || zzVar.X.f2522a == null || zzVar.X.f2522a.longValue() == -2)) {
                return true;
            }
        }
        return false;
    }

    /* JADX WARNING: Removed duplicated region for block: B:42:0x00c9 A[SYNTHETIC, Splitter:B:42:0x00c9] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x00ce A[SYNTHETIC, Splitter:B:45:0x00ce] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x00d8 A[SYNTHETIC, Splitter:B:51:0x00d8] */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x00dd A[SYNTHETIC, Splitter:B:54:0x00dd] */
    private final boolean b(File file, String str) {
        FileOutputStream fileOutputStream;
        FileInputStream fileInputStream;
        FileInputStream fileInputStream2;
        FileOutputStream fileOutputStream2 = null;
        File file2 = new File(String.format("%s/%s.tmp", new Object[]{file, str}));
        if (!file2.exists()) {
            return false;
        }
        File file3 = new File(String.format("%s/%s.dex", new Object[]{file, str}));
        if (file3.exists()) {
            return false;
        }
        try {
            long length = file2.length();
            if (length <= 0) {
                a(file2);
                return false;
            }
            byte[] bArr = new byte[((int) length)];
            fileInputStream2 = new FileInputStream(file2);
            try {
                if (fileInputStream2.read(bArr) <= 0) {
                    Log.d(f2606b, "Cannot read the cache data.");
                    a(file2);
                    try {
                        fileInputStream2.close();
                    } catch (IOException e2) {
                    }
                    return false;
                }
                aed aed = (aed) afn.a(new aed(), bArr);
                if (!str.equals(new String(aed.c)) || !Arrays.equals(aed.f2501b, agi.a(aed.f2500a)) || !Arrays.equals(aed.d, VERSION.SDK.getBytes())) {
                    a(file2);
                    try {
                        fileInputStream2.close();
                    } catch (IOException e3) {
                    }
                    return false;
                }
                byte[] a2 = this.e.a(this.f, new String(aed.f2500a));
                file3.createNewFile();
                fileOutputStream = new FileOutputStream(file3);
                try {
                    fileOutputStream.write(a2, 0, a2.length);
                    try {
                        fileInputStream2.close();
                    } catch (IOException e4) {
                    }
                    try {
                        fileOutputStream.close();
                    } catch (IOException e5) {
                    }
                    return true;
                } catch (IOException e6) {
                    fileInputStream = fileInputStream2;
                    if (fileInputStream != null) {
                    }
                    if (fileOutputStream != null) {
                    }
                    return false;
                } catch (NoSuchAlgorithmException e7) {
                    fileInputStream = fileInputStream2;
                    if (fileInputStream != null) {
                    }
                    if (fileOutputStream != null) {
                    }
                    return false;
                } catch (ahk e8) {
                    fileInputStream = fileInputStream2;
                    if (fileInputStream != null) {
                    }
                    if (fileOutputStream != null) {
                    }
                    return false;
                } catch (Throwable th) {
                    fileOutputStream2 = fileOutputStream;
                    th = th;
                    if (fileInputStream2 != null) {
                    }
                    if (fileOutputStream2 != null) {
                    }
                    throw th;
                }
            } catch (IOException e9) {
                fileOutputStream = null;
                fileInputStream = fileInputStream2;
                if (fileInputStream != null) {
                    try {
                        fileInputStream.close();
                    } catch (IOException e10) {
                    }
                }
                if (fileOutputStream != null) {
                    try {
                        fileOutputStream.close();
                    } catch (IOException e11) {
                    }
                }
                return false;
            } catch (NoSuchAlgorithmException e12) {
                fileOutputStream = null;
                fileInputStream = fileInputStream2;
                if (fileInputStream != null) {
                }
                if (fileOutputStream != null) {
                }
                return false;
            } catch (ahk e13) {
                fileOutputStream = null;
                fileInputStream = fileInputStream2;
                if (fileInputStream != null) {
                }
                if (fileOutputStream != null) {
                }
                return false;
            } catch (Throwable th2) {
                th = th2;
                if (fileInputStream2 != null) {
                    try {
                        fileInputStream2.close();
                    } catch (IOException e14) {
                    }
                }
                if (fileOutputStream2 != null) {
                    try {
                        fileOutputStream2.close();
                    } catch (IOException e15) {
                    }
                }
                throw th;
            }
        } catch (IOException e16) {
            fileOutputStream = null;
            fileInputStream = null;
            if (fileInputStream != null) {
            }
            if (fileOutputStream != null) {
            }
            return false;
        } catch (NoSuchAlgorithmException e17) {
            fileOutputStream = null;
            fileInputStream = null;
            if (fileInputStream != null) {
            }
            if (fileOutputStream != null) {
            }
            return false;
        } catch (ahk e18) {
            fileOutputStream = null;
            fileInputStream = null;
            if (fileInputStream != null) {
            }
            if (fileOutputStream != null) {
            }
            return false;
        } catch (Throwable th3) {
            th = th3;
            fileInputStream2 = null;
            if (fileInputStream2 != null) {
            }
            if (fileOutputStream2 != null) {
            }
            throw th;
        }
    }

    /* access modifiers changed from: private */
    public final void o() {
        try {
            if (this.g == null && this.j) {
                AdvertisingIdClient advertisingIdClient = new AdvertisingIdClient(this.f2607a);
                advertisingIdClient.start();
                this.g = advertisingIdClient;
            }
        } catch (l | m | IOException e2) {
            this.g = null;
        }
    }

    private final zz p() {
        boolean z = false;
        try {
            return ti.a(this.f2607a, this.f2607a.getPackageName(), Integer.toString(this.f2607a.getPackageManager().getPackageInfo(this.f2607a.getPackageName(), 0).versionCode));
        } catch (Throwable th) {
            return z;
        }
    }

    public final Context a() {
        return this.f2607a;
    }

    public final Method a(String str, String str2) {
        ajh ajh = (ajh) this.p.get(new Pair(str, str2));
        if (ajh == null) {
            return null;
        }
        return ajh.a();
    }

    /* access modifiers changed from: 0000 */
    public final void a(int i2, boolean z) {
        if (this.o) {
            Future submit = this.c.submit(new aic(this, i2, z));
            if (i2 == 0) {
                this.l = submit;
            }
        }
    }

    public final boolean a(String str, String str2, Class<?>... clsArr) {
        if (this.p.containsKey(new Pair(str, str2))) {
            return false;
        }
        this.p.put(new Pair(str, str2), new ajh(this, str, str2, clsArr));
        return true;
    }

    /* access modifiers changed from: 0000 */
    public final zz b(int i2, boolean z) {
        if (i2 > 0 && z) {
            try {
                Thread.sleep((long) (i2 * 1000));
            } catch (InterruptedException e2) {
            }
        }
        return p();
    }

    public final boolean b() {
        return this.q;
    }

    public final ExecutorService c() {
        return this.c;
    }

    public final DexClassLoader d() {
        return this.d;
    }

    public final ahj e() {
        return this.e;
    }

    public final byte[] f() {
        return this.f;
    }

    public final boolean g() {
        return this.n;
    }

    public final ahb h() {
        return this.m;
    }

    public final boolean i() {
        return this.o;
    }

    public final boolean j() {
        return this.r;
    }

    public final zz k() {
        return this.k;
    }

    public final Future l() {
        return this.l;
    }

    public final AdvertisingIdClient m() {
        if (!this.h) {
            return null;
        }
        if (this.g != null) {
            return this.g;
        }
        if (this.i != null) {
            try {
                this.i.get(2000, TimeUnit.MILLISECONDS);
                this.i = null;
            } catch (InterruptedException | ExecutionException e2) {
            } catch (TimeoutException e3) {
                this.i.cancel(true);
            }
        }
        return this.g;
    }

    public final int n() {
        if (this.m != null) {
            return ahb.a();
        }
        return Integer.MIN_VALUE;
    }
}
