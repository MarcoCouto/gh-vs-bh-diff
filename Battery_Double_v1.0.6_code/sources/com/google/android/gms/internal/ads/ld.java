package com.google.android.gms.internal.ads;

import com.google.android.gms.common.internal.z;

public final class ld {

    /* renamed from: a reason: collision with root package name */
    public final String f3473a;

    /* renamed from: b reason: collision with root package name */
    public final double f3474b;
    public final int c;
    private final double d;
    private final double e;

    public ld(String str, double d2, double d3, double d4, int i) {
        this.f3473a = str;
        this.e = d2;
        this.d = d3;
        this.f3474b = d4;
        this.c = i;
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof ld)) {
            return false;
        }
        ld ldVar = (ld) obj;
        return z.a(this.f3473a, ldVar.f3473a) && this.d == ldVar.d && this.e == ldVar.e && this.c == ldVar.c && Double.compare(this.f3474b, ldVar.f3474b) == 0;
    }

    public final int hashCode() {
        return z.a(this.f3473a, Double.valueOf(this.d), Double.valueOf(this.e), Double.valueOf(this.f3474b), Integer.valueOf(this.c));
    }

    public final String toString() {
        return z.a((Object) this).a("name", this.f3473a).a("minBound", Double.valueOf(this.e)).a("maxBound", Double.valueOf(this.d)).a("percent", Double.valueOf(this.f3474b)).a("count", Integer.valueOf(this.c)).toString();
    }
}
