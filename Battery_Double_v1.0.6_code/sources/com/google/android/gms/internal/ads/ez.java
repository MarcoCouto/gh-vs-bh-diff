package com.google.android.gms.internal.ads;

import android.content.Context;
import android.location.Location;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Debug.MemoryInfo;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.ax;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@cm
public final class ez {

    /* renamed from: a reason: collision with root package name */
    private static final SimpleDateFormat f3300a = new SimpleDateFormat("yyyyMMdd", Locale.US);

    /* JADX WARNING: Removed duplicated region for block: B:62:0x016b A[Catch:{ JSONException -> 0x0288 }] */
    public static dp a(Context context, dl dlVar, String str) {
        String str2;
        long j;
        boolean optBoolean;
        try {
            JSONObject jSONObject = new JSONObject(str);
            String optString = jSONObject.optString("ad_base_url", null);
            String optString2 = jSONObject.optString("ad_url", null);
            String optString3 = jSONObject.optString("ad_size", null);
            String optString4 = jSONObject.optString("ad_slot_size", optString3);
            boolean z = (dlVar == null || dlVar.m == 0) ? false : true;
            String optString5 = jSONObject.optString("ad_json", null);
            if (optString5 == null) {
                optString5 = jSONObject.optString("ad_html", null);
            }
            if (optString5 == null) {
                optString5 = jSONObject.optString("body", null);
            }
            if (optString5 == null) {
                if (jSONObject.has("ads")) {
                    optString5 = jSONObject.toString();
                }
            }
            long j2 = -1;
            String optString6 = jSONObject.optString("debug_dialog", null);
            String optString7 = jSONObject.optString("debug_signals", null);
            long j3 = jSONObject.has("interstitial_timeout") ? (long) (jSONObject.getDouble("interstitial_timeout") * 1000.0d) : -1;
            String optString8 = jSONObject.optString("orientation", null);
            int i = -1;
            if ("portrait".equals(optString8)) {
                i = ax.g().b();
            } else if ("landscape".equals(optString8)) {
                i = ax.g().a();
            }
            dp dpVar = null;
            if (!TextUtils.isEmpty(optString5) || TextUtils.isEmpty(optString2)) {
                str2 = optString5;
            } else {
                dpVar = eu.a(dlVar, context, dlVar.k.f3528a, optString2, null, null, null, null, null);
                optString = dpVar.f3265a;
                str2 = dpVar.f3266b;
                j2 = dpVar.m;
            }
            if (str2 == null) {
                return new dp(0);
            }
            JSONArray optJSONArray = jSONObject.optJSONArray("click_urls");
            List<String> list = dpVar == null ? null : dpVar.c;
            if (optJSONArray != null) {
                list = a(optJSONArray, list);
            }
            JSONArray optJSONArray2 = jSONObject.optJSONArray("impression_urls");
            List<String> list2 = dpVar == null ? null : dpVar.e;
            if (optJSONArray2 != null) {
                list2 = a(optJSONArray2, list2);
            }
            JSONArray optJSONArray3 = jSONObject.optJSONArray("downloaded_impression_urls");
            List<String> list3 = dpVar == null ? null : dpVar.R;
            if (optJSONArray3 != null) {
                list3 = a(optJSONArray3, list3);
            }
            JSONArray optJSONArray4 = jSONObject.optJSONArray("manual_impression_urls");
            List<String> list4 = dpVar == null ? null : dpVar.i;
            if (optJSONArray4 != null) {
                list4 = a(optJSONArray4, list4);
            }
            if (dpVar != null) {
                if (dpVar.k != -1) {
                    i = dpVar.k;
                }
                if (dpVar.f > 0) {
                    j = dpVar.f;
                    String optString9 = jSONObject.optString("active_view");
                    String str3 = null;
                    optBoolean = jSONObject.optBoolean("ad_is_javascript", false);
                    if (optBoolean) {
                        str3 = jSONObject.optString("ad_passback_url", null);
                    }
                    return new dp(dlVar, optString, str2, list, list2, j, jSONObject.optBoolean("mediation", false), jSONObject.optLong("mediation_config_cache_time_milliseconds", -1), list4, jSONObject.optLong("refresh_interval_milliseconds", -1), i, optString3, j2, optString6, optBoolean, str3, optString9, jSONObject.optBoolean("custom_render_allowed", false), z, dlVar.p, jSONObject.optBoolean("content_url_opted_out", true), jSONObject.optBoolean("prefetch", false), jSONObject.optString("gws_query_id", ""), "height".equals(jSONObject.optString("fluid", "")), jSONObject.optBoolean("native_express", false), hp.a(jSONObject.optJSONArray("rewards")), a(jSONObject.optJSONArray("video_start_urls"), null), a(jSONObject.optJSONArray("video_complete_urls"), null), jSONObject.optBoolean("use_displayed_impression", false), dr.a(jSONObject.optJSONObject("auto_protection_configuration")), dlVar.G, jSONObject.optString("set_cookie", ""), a(jSONObject.optJSONArray("remote_ping_urls"), null), jSONObject.optBoolean("render_in_browser", dlVar.K), optString4, hz.a(jSONObject.optJSONObject("safe_browsing")), optString7, jSONObject.optBoolean("content_vertical_opted_out", true), dlVar.U, jSONObject.optBoolean("custom_close_blocked"), 0, jSONObject.optBoolean("enable_omid", false), list3, jSONObject.optBoolean("disable_closable_area", false), jSONObject.optString("omid_settings", null));
                }
            }
            j = j3;
            String optString92 = jSONObject.optString("active_view");
            String str32 = null;
            optBoolean = jSONObject.optBoolean("ad_is_javascript", false);
            if (optBoolean) {
            }
            return new dp(dlVar, optString, str2, list, list2, j, jSONObject.optBoolean("mediation", false), jSONObject.optLong("mediation_config_cache_time_milliseconds", -1), list4, jSONObject.optLong("refresh_interval_milliseconds", -1), i, optString3, j2, optString6, optBoolean, str32, optString92, jSONObject.optBoolean("custom_render_allowed", false), z, dlVar.p, jSONObject.optBoolean("content_url_opted_out", true), jSONObject.optBoolean("prefetch", false), jSONObject.optString("gws_query_id", ""), "height".equals(jSONObject.optString("fluid", "")), jSONObject.optBoolean("native_express", false), hp.a(jSONObject.optJSONArray("rewards")), a(jSONObject.optJSONArray("video_start_urls"), null), a(jSONObject.optJSONArray("video_complete_urls"), null), jSONObject.optBoolean("use_displayed_impression", false), dr.a(jSONObject.optJSONObject("auto_protection_configuration")), dlVar.G, jSONObject.optString("set_cookie", ""), a(jSONObject.optJSONArray("remote_ping_urls"), null), jSONObject.optBoolean("render_in_browser", dlVar.K), optString4, hz.a(jSONObject.optJSONObject("safe_browsing")), optString7, jSONObject.optBoolean("content_vertical_opted_out", true), dlVar.U, jSONObject.optBoolean("custom_close_blocked"), 0, jSONObject.optBoolean("enable_omid", false), list3, jSONObject.optBoolean("disable_closable_area", false), jSONObject.optString("omid_settings", null));
        } catch (JSONException e) {
            String str4 = "Could not parse the inline ad response: ";
            String valueOf = String.valueOf(e.getMessage());
            jm.e(valueOf.length() != 0 ? str4.concat(valueOf) : new String(str4));
            return new dp(0);
        }
    }

    private static Integer a(boolean z) {
        return Integer.valueOf(z ? 1 : 0);
    }

    private static List<String> a(JSONArray jSONArray, List<String> list) throws JSONException {
        if (jSONArray == null) {
            return null;
        }
        if (list == null) {
            list = new ArrayList<>();
        }
        for (int i = 0; i < jSONArray.length(); i++) {
            list.add(jSONArray.getString(i));
        }
        return list;
    }

    private static JSONArray a(List<String> list) throws JSONException {
        JSONArray jSONArray = new JSONArray();
        for (String put : list) {
            jSONArray.put(put);
        }
        return jSONArray;
    }

    public static JSONObject a(Context context, es esVar) {
        aot[] aotArr;
        String[] split;
        String str;
        dl dlVar = esVar.j;
        Location location = esVar.d;
        fi fiVar = esVar.k;
        Bundle bundle = esVar.f3290a;
        JSONObject jSONObject = esVar.l;
        try {
            HashMap hashMap = new HashMap();
            hashMap.put("extra_caps", ape.f().a(asi.bT));
            if (esVar.c.size() > 0) {
                hashMap.put("eid", TextUtils.join(",", esVar.c));
            }
            if (dlVar.f3261b != null) {
                hashMap.put("ad_pos", dlVar.f3261b);
            }
            aop aop = dlVar.c;
            String a2 = jg.a();
            if (a2 != null) {
                hashMap.put("abf", a2);
            }
            if (aop.f2810b != -1) {
                hashMap.put("cust_age", f3300a.format(new Date(aop.f2810b)));
            }
            if (aop.c != null) {
                hashMap.put("extras", aop.c);
            }
            if (aop.d != -1) {
                hashMap.put("cust_gender", Integer.valueOf(aop.d));
            }
            if (aop.e != null) {
                hashMap.put("kw", aop.e);
            }
            if (aop.g != -1) {
                hashMap.put("tag_for_child_directed_treatment", Integer.valueOf(aop.g));
            }
            if (aop.f) {
                if (((Boolean) ape.f().a(asi.dk)).booleanValue()) {
                    hashMap.put("test_request", Boolean.valueOf(true));
                } else {
                    hashMap.put("adtest", "on");
                }
            }
            if (aop.f2809a >= 2) {
                if (aop.h) {
                    hashMap.put("d_imp_hdr", Integer.valueOf(1));
                }
                if (!TextUtils.isEmpty(aop.i)) {
                    hashMap.put("ppid", aop.i);
                }
            }
            if (aop.f2809a >= 3 && aop.l != null) {
                hashMap.put("url", aop.l);
            }
            if (aop.f2809a >= 5) {
                if (aop.n != null) {
                    hashMap.put("custom_targeting", aop.n);
                }
                if (aop.o != null) {
                    hashMap.put("category_exclusions", aop.o);
                }
                if (aop.p != null) {
                    hashMap.put("request_agent", aop.p);
                }
            }
            if (aop.f2809a >= 6 && aop.q != null) {
                hashMap.put("request_pkg", aop.q);
            }
            if (aop.f2809a >= 7) {
                hashMap.put("is_designed_for_families", Boolean.valueOf(aop.r));
            }
            if (dlVar.d.g != null) {
                boolean z = false;
                boolean z2 = false;
                for (aot aot : dlVar.d.g) {
                    if (!aot.i && !z2) {
                        hashMap.put("format", aot.f2814a);
                        z2 = true;
                    }
                    if (aot.i && !z) {
                        hashMap.put("fluid", "height");
                        z = true;
                    }
                    if (z2 && z) {
                        break;
                    }
                }
            } else {
                hashMap.put("format", dlVar.d.f2814a);
                if (dlVar.d.i) {
                    hashMap.put("fluid", "height");
                }
            }
            if (dlVar.d.e == -1) {
                hashMap.put("smart_w", "full");
            }
            if (dlVar.d.f2815b == -2) {
                hashMap.put("smart_h", "auto");
            }
            if (dlVar.d.g != null) {
                StringBuilder sb = new StringBuilder();
                aot[] aotArr2 = dlVar.d.g;
                int length = aotArr2.length;
                boolean z3 = false;
                for (int i = 0; i < length; i++) {
                    aot aot2 = aotArr2[i];
                    if (aot2.i) {
                        z3 = true;
                    } else {
                        if (sb.length() != 0) {
                            sb.append("|");
                        }
                        sb.append(aot2.e == -1 ? (int) (((float) aot2.f) / fiVar.s) : aot2.e);
                        sb.append("x");
                        sb.append(aot2.f2815b == -2 ? (int) (((float) aot2.c) / fiVar.s) : aot2.f2815b);
                    }
                }
                if (z3) {
                    if (sb.length() != 0) {
                        sb.insert(0, "|");
                    }
                    sb.insert(0, "320x50");
                }
                hashMap.put("sz", sb);
            }
            if (dlVar.m != 0) {
                hashMap.put("native_version", Integer.valueOf(dlVar.m));
                hashMap.put("native_templates", dlVar.n);
                String str2 = "native_image_orientation";
                aul aul = dlVar.y;
                if (aul != null) {
                    switch (aul.c) {
                        case 0:
                            str = "any";
                            break;
                        case 1:
                            str = "portrait";
                            break;
                        case 2:
                            str = "landscape";
                            break;
                        default:
                            str = "not_set";
                            break;
                    }
                } else {
                    str = "any";
                }
                hashMap.put(str2, str);
                if (!dlVar.z.isEmpty()) {
                    hashMap.put("native_custom_templates", dlVar.z);
                }
                if (dlVar.f3260a >= 24) {
                    hashMap.put("max_num_ads", Integer.valueOf(dlVar.Y));
                }
                if (!TextUtils.isEmpty(dlVar.W)) {
                    try {
                        hashMap.put("native_advanced_settings", new JSONArray(dlVar.W));
                    } catch (JSONException e) {
                        jm.c("Problem creating json from native advanced settings", e);
                    }
                }
            }
            if (dlVar.V != null && dlVar.V.size() > 0) {
                for (Integer num : dlVar.V) {
                    if (num.intValue() == 2) {
                        hashMap.put("iba", Boolean.valueOf(true));
                    } else if (num.intValue() == 1) {
                        hashMap.put("ina", Boolean.valueOf(true));
                    }
                }
            }
            if (dlVar.d.j) {
                hashMap.put("ene", Boolean.valueOf(true));
            }
            if (((Boolean) ape.f().a(asi.ax)).booleanValue()) {
                hashMap.put("xsrve", Boolean.valueOf(true));
            }
            if (dlVar.O != null) {
                hashMap.put("is_icon_ad", Boolean.valueOf(true));
                hashMap.put("icon_ad_expansion_behavior", Integer.valueOf(dlVar.O.f2841a));
            }
            hashMap.put("slotname", dlVar.e);
            hashMap.put("pn", dlVar.f.packageName);
            if (dlVar.g != null) {
                hashMap.put("vc", Integer.valueOf(dlVar.g.versionCode));
            }
            hashMap.put("ms", esVar.h);
            hashMap.put("seq_num", dlVar.i);
            hashMap.put("session_id", dlVar.j);
            hashMap.put("js", dlVar.k.f3528a);
            fs fsVar = esVar.e;
            Bundle bundle2 = dlVar.M;
            Bundle bundle3 = esVar.f3291b;
            hashMap.put("am", Integer.valueOf(fiVar.f3314a));
            hashMap.put("cog", a(fiVar.f3315b));
            hashMap.put("coh", a(fiVar.c));
            if (!TextUtils.isEmpty(fiVar.d)) {
                hashMap.put("carrier", fiVar.d);
            }
            hashMap.put("gl", fiVar.e);
            if (fiVar.f) {
                hashMap.put("simulator", Integer.valueOf(1));
            }
            if (fiVar.g) {
                hashMap.put("is_sidewinder", Integer.valueOf(1));
            }
            hashMap.put("ma", a(fiVar.h));
            hashMap.put("sp", a(fiVar.i));
            hashMap.put("hl", fiVar.j);
            if (!TextUtils.isEmpty(fiVar.k)) {
                hashMap.put("mv", fiVar.k);
            }
            hashMap.put("muv", Integer.valueOf(fiVar.m));
            if (fiVar.n != -2) {
                hashMap.put("cnt", Integer.valueOf(fiVar.n));
            }
            hashMap.put("gnt", Integer.valueOf(fiVar.o));
            hashMap.put("pt", Integer.valueOf(fiVar.p));
            hashMap.put("rm", Integer.valueOf(fiVar.q));
            hashMap.put("riv", Integer.valueOf(fiVar.r));
            Bundle bundle4 = new Bundle();
            bundle4.putString("build_build", fiVar.z);
            bundle4.putString("build_device", fiVar.A);
            Bundle bundle5 = new Bundle();
            bundle5.putBoolean("is_charging", fiVar.w);
            bundle5.putDouble("battery_level", fiVar.v);
            bundle4.putBundle("battery", bundle5);
            Bundle bundle6 = new Bundle();
            bundle6.putInt("active_network_state", fiVar.y);
            bundle6.putBoolean("active_network_metered", fiVar.x);
            if (fsVar != null) {
                Bundle bundle7 = new Bundle();
                bundle7.putInt("predicted_latency_micros", fsVar.f3326a);
                bundle7.putLong("predicted_down_throughput_bps", fsVar.f3327b);
                bundle7.putLong("predicted_up_throughput_bps", fsVar.c);
                bundle6.putBundle("predictions", bundle7);
            }
            bundle4.putBundle("network", bundle6);
            Bundle bundle8 = new Bundle();
            bundle8.putBoolean("is_browser_custom_tabs_capable", fiVar.B);
            bundle4.putBundle("browser", bundle8);
            if (bundle2 != null) {
                String str3 = "android_mem_info";
                Bundle bundle9 = new Bundle();
                bundle9.putString("runtime_free", Long.toString(bundle2.getLong("runtime_free_memory", -1)));
                bundle9.putString("runtime_max", Long.toString(bundle2.getLong("runtime_max_memory", -1)));
                bundle9.putString("runtime_total", Long.toString(bundle2.getLong("runtime_total_memory", -1)));
                bundle9.putString("web_view_count", Integer.toString(bundle2.getInt("web_view_count", 0)));
                MemoryInfo memoryInfo = (MemoryInfo) bundle2.getParcelable("debug_memory_info");
                if (memoryInfo != null) {
                    bundle9.putString("debug_info_dalvik_private_dirty", Integer.toString(memoryInfo.dalvikPrivateDirty));
                    bundle9.putString("debug_info_dalvik_pss", Integer.toString(memoryInfo.dalvikPss));
                    bundle9.putString("debug_info_dalvik_shared_dirty", Integer.toString(memoryInfo.dalvikSharedDirty));
                    bundle9.putString("debug_info_native_private_dirty", Integer.toString(memoryInfo.nativePrivateDirty));
                    bundle9.putString("debug_info_native_pss", Integer.toString(memoryInfo.nativePss));
                    bundle9.putString("debug_info_native_shared_dirty", Integer.toString(memoryInfo.nativeSharedDirty));
                    bundle9.putString("debug_info_other_private_dirty", Integer.toString(memoryInfo.otherPrivateDirty));
                    bundle9.putString("debug_info_other_pss", Integer.toString(memoryInfo.otherPss));
                    bundle9.putString("debug_info_other_shared_dirty", Integer.toString(memoryInfo.otherSharedDirty));
                }
                bundle4.putBundle(str3, bundle9);
            }
            Bundle bundle10 = new Bundle();
            bundle10.putBundle("parental_controls", bundle3);
            if (!TextUtils.isEmpty(fiVar.l)) {
                bundle10.putString("package_version", fiVar.l);
            }
            bundle4.putBundle("play_store", bundle10);
            hashMap.put("device", bundle4);
            Bundle bundle11 = new Bundle();
            bundle11.putString("doritos", esVar.f);
            bundle11.putString("doritos_v2", esVar.g);
            if (((Boolean) ape.f().a(asi.aJ)).booleanValue()) {
                String str4 = null;
                boolean z4 = false;
                if (esVar.i != null) {
                    str4 = esVar.i.getId();
                    z4 = esVar.i.isLimitAdTrackingEnabled();
                }
                if (!TextUtils.isEmpty(str4)) {
                    bundle11.putString("rdid", str4);
                    bundle11.putBoolean("is_lat", z4);
                    bundle11.putString("idtype", "adid");
                } else {
                    ape.a();
                    bundle11.putString("pdid", mh.b(context));
                    bundle11.putString("pdidtype", "ssaid");
                }
            }
            hashMap.put("pii", bundle11);
            hashMap.put("platform", Build.MANUFACTURER);
            hashMap.put("submodel", Build.MODEL);
            if (location != null) {
                a(hashMap, location);
            } else if (dlVar.c.f2809a >= 2 && dlVar.c.k != null) {
                a(hashMap, dlVar.c.k);
            }
            if (dlVar.f3260a >= 2) {
                hashMap.put("quality_signals", dlVar.l);
            }
            if (dlVar.f3260a >= 4 && dlVar.p) {
                hashMap.put("forceHttps", Boolean.valueOf(dlVar.p));
            }
            if (bundle != null) {
                hashMap.put("content_info", bundle);
            }
            if (dlVar.f3260a >= 5) {
                hashMap.put("u_sd", Float.valueOf(dlVar.s));
                hashMap.put("sh", Integer.valueOf(dlVar.r));
                hashMap.put("sw", Integer.valueOf(dlVar.q));
            } else {
                hashMap.put("u_sd", Float.valueOf(fiVar.s));
                hashMap.put("sh", Integer.valueOf(fiVar.u));
                hashMap.put("sw", Integer.valueOf(fiVar.t));
            }
            if (dlVar.f3260a >= 6) {
                if (!TextUtils.isEmpty(dlVar.t)) {
                    try {
                        hashMap.put("view_hierarchy", new JSONObject(dlVar.t));
                    } catch (JSONException e2) {
                        jm.c("Problem serializing view hierarchy to JSON", e2);
                    }
                }
                hashMap.put("correlation_id", Long.valueOf(dlVar.u));
            }
            if (dlVar.f3260a >= 7) {
                hashMap.put("request_id", dlVar.v);
            }
            if (dlVar.f3260a >= 12 && !TextUtils.isEmpty(dlVar.B)) {
                hashMap.put("anchor", dlVar.B);
            }
            if (dlVar.f3260a >= 13) {
                hashMap.put("android_app_volume", Float.valueOf(dlVar.C));
            }
            if (dlVar.f3260a >= 18) {
                hashMap.put("android_app_muted", Boolean.valueOf(dlVar.I));
            }
            if (dlVar.f3260a >= 14 && dlVar.D > 0) {
                hashMap.put("target_api", Integer.valueOf(dlVar.D));
            }
            if (dlVar.f3260a >= 15) {
                hashMap.put("scroll_index", Integer.valueOf(dlVar.E == -1 ? -1 : dlVar.E));
            }
            if (dlVar.f3260a >= 16) {
                hashMap.put("_activity_context", Boolean.valueOf(dlVar.F));
            }
            if (dlVar.f3260a >= 18) {
                if (!TextUtils.isEmpty(dlVar.J)) {
                    try {
                        hashMap.put("app_settings", new JSONObject(dlVar.J));
                    } catch (JSONException e3) {
                        jm.c("Problem creating json from app settings", e3);
                    }
                }
                hashMap.put("render_in_browser", Boolean.valueOf(dlVar.K));
            }
            if (dlVar.f3260a >= 18) {
                hashMap.put("android_num_video_cache_tasks", Integer.valueOf(dlVar.L));
            }
            mu muVar = dlVar.k;
            boolean z5 = dlVar.Z;
            boolean z6 = esVar.m;
            boolean z7 = dlVar.ab;
            Bundle bundle12 = new Bundle();
            Bundle bundle13 = new Bundle();
            bundle13.putString("cl", "193400285");
            bundle13.putString("rapid_rc", "dev");
            bundle13.putString("rapid_rollup", "HEAD");
            bundle12.putBundle("build_meta", bundle13);
            bundle12.putString("mf", Boolean.toString(((Boolean) ape.f().a(asi.bV)).booleanValue()));
            bundle12.putBoolean("instant_app", z5);
            bundle12.putBoolean("lite", muVar.e);
            bundle12.putBoolean("local_service", z6);
            bundle12.putBoolean("is_privileged_process", z7);
            hashMap.put("sdk_env", bundle12);
            hashMap.put("cache_state", jSONObject);
            if (dlVar.f3260a >= 19) {
                hashMap.put("gct", dlVar.N);
            }
            if (dlVar.f3260a >= 21 && dlVar.P) {
                hashMap.put("de", "1");
            }
            if (((Boolean) ape.f().a(asi.aT)).booleanValue()) {
                String str5 = dlVar.d.f2814a;
                boolean z8 = str5.equals("interstitial_mb") || str5.equals("reward_mb");
                Bundle bundle14 = dlVar.Q;
                boolean z9 = bundle14 != null;
                if (z8 && z9) {
                    Bundle bundle15 = new Bundle();
                    bundle15.putBundle("interstitial_pool", bundle14);
                    hashMap.put("counters", bundle15);
                }
            }
            if (dlVar.R != null) {
                hashMap.put("gmp_app_id", dlVar.R);
            }
            if (dlVar.S == null) {
                hashMap.put("fbs_aiid", "");
            } else if ("TIME_OUT".equals(dlVar.S)) {
                hashMap.put("sai_timeout", ape.f().a(asi.av));
            } else {
                hashMap.put("fbs_aiid", dlVar.S);
            }
            if (dlVar.T != null) {
                hashMap.put("fbs_aeid", dlVar.T);
            }
            if (dlVar.f3260a >= 24) {
                hashMap.put("disable_ml", Boolean.valueOf(dlVar.aa));
            }
            String str6 = (String) ape.f().a(asi.E);
            if (str6 != null && !str6.isEmpty()) {
                if (VERSION.SDK_INT >= ((Integer) ape.f().a(asi.F)).intValue()) {
                    HashMap hashMap2 = new HashMap();
                    for (String str7 : str6.split(",")) {
                        hashMap2.put(str7, mf.a(str7));
                    }
                    hashMap.put("video_decoders", hashMap2);
                }
            }
            if (((Boolean) ape.f().a(asi.dg)).booleanValue()) {
                hashMap.put("omid_v", ax.u().b(context));
            }
            if (dlVar.ac != null && !dlVar.ac.isEmpty()) {
                hashMap.put("android_permissions", dlVar.ac);
            }
            if (jm.a(2)) {
                String str8 = "Ad Request JSON: ";
                String valueOf = String.valueOf(ax.e().a((Map<String, ?>) hashMap).toString(2));
                jm.a(valueOf.length() != 0 ? str8.concat(valueOf) : new String(str8));
            }
            return ax.e().a((Map<String, ?>) hashMap);
        } catch (JSONException e4) {
            String str9 = "Problem serializing ad request to JSON: ";
            String valueOf2 = String.valueOf(e4.getMessage());
            jm.e(valueOf2.length() != 0 ? str9.concat(valueOf2) : new String(str9));
            return null;
        }
    }

    public static JSONObject a(dp dpVar) throws JSONException {
        JSONObject jSONObject = new JSONObject();
        if (dpVar.f3265a != null) {
            jSONObject.put("ad_base_url", dpVar.f3265a);
        }
        if (dpVar.l != null) {
            jSONObject.put("ad_size", dpVar.l);
        }
        jSONObject.put("native", dpVar.s);
        if (dpVar.s) {
            jSONObject.put("ad_json", dpVar.f3266b);
        } else {
            jSONObject.put("ad_html", dpVar.f3266b);
        }
        if (dpVar.n != null) {
            jSONObject.put("debug_dialog", dpVar.n);
        }
        if (dpVar.L != null) {
            jSONObject.put("debug_signals", dpVar.L);
        }
        if (dpVar.f != -1) {
            jSONObject.put("interstitial_timeout", ((double) dpVar.f) / 1000.0d);
        }
        if (dpVar.k == ax.g().b()) {
            jSONObject.put("orientation", "portrait");
        } else if (dpVar.k == ax.g().a()) {
            jSONObject.put("orientation", "landscape");
        }
        if (dpVar.c != null) {
            jSONObject.put("click_urls", a(dpVar.c));
        }
        if (dpVar.e != null) {
            jSONObject.put("impression_urls", a(dpVar.e));
        }
        if (dpVar.R != null) {
            jSONObject.put("downloaded_impression_urls", a(dpVar.R));
        }
        if (dpVar.i != null) {
            jSONObject.put("manual_impression_urls", a(dpVar.i));
        }
        if (dpVar.q != null) {
            jSONObject.put("active_view", dpVar.q);
        }
        jSONObject.put("ad_is_javascript", dpVar.o);
        if (dpVar.p != null) {
            jSONObject.put("ad_passback_url", dpVar.p);
        }
        jSONObject.put("mediation", dpVar.g);
        jSONObject.put("custom_render_allowed", dpVar.r);
        jSONObject.put("content_url_opted_out", dpVar.u);
        jSONObject.put("content_vertical_opted_out", dpVar.M);
        jSONObject.put("prefetch", dpVar.v);
        if (dpVar.j != -1) {
            jSONObject.put("refresh_interval_milliseconds", dpVar.j);
        }
        if (dpVar.h != -1) {
            jSONObject.put("mediation_config_cache_time_milliseconds", dpVar.h);
        }
        if (!TextUtils.isEmpty(dpVar.x)) {
            jSONObject.put("gws_query_id", dpVar.x);
        }
        jSONObject.put("fluid", dpVar.y ? "height" : "");
        jSONObject.put("native_express", dpVar.z);
        if (dpVar.B != null) {
            jSONObject.put("video_start_urls", a(dpVar.B));
        }
        if (dpVar.C != null) {
            jSONObject.put("video_complete_urls", a(dpVar.C));
        }
        if (dpVar.A != null) {
            hp hpVar = dpVar.A;
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("rb_type", hpVar.f3369a);
            jSONObject2.put("rb_amount", hpVar.f3370b);
            JSONArray jSONArray = new JSONArray();
            jSONArray.put(jSONObject2);
            jSONObject.put("rewards", jSONArray);
        }
        jSONObject.put("use_displayed_impression", dpVar.D);
        jSONObject.put("auto_protection_configuration", dpVar.E);
        jSONObject.put("render_in_browser", dpVar.I);
        jSONObject.put("disable_closable_area", dpVar.S);
        return jSONObject;
    }

    private static void a(HashMap<String, Object> hashMap, Location location) {
        HashMap hashMap2 = new HashMap();
        Float valueOf = Float.valueOf(location.getAccuracy() * 1000.0f);
        Long valueOf2 = Long.valueOf(location.getTime() * 1000);
        Long valueOf3 = Long.valueOf((long) (location.getLatitude() * 1.0E7d));
        Long valueOf4 = Long.valueOf((long) (location.getLongitude() * 1.0E7d));
        hashMap2.put("radius", valueOf);
        hashMap2.put("lat", valueOf3);
        hashMap2.put("long", valueOf4);
        hashMap2.put("time", valueOf2);
        hashMap.put("uule", hashMap2);
    }
}
