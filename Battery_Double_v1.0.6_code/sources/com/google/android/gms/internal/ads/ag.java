package com.google.android.gms.internal.ads;

import android.os.IBinder;
import android.os.IInterface;

public final class ag extends ajl implements af {
    public static af a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.ads.internal.purchase.client.IPlayStorePurchaseListener");
        return queryLocalInterface instanceof af ? (af) queryLocalInterface : new ah(iBinder);
    }
}
