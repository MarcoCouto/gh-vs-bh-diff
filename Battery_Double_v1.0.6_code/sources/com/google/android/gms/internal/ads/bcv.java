package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.b.a;
import com.google.android.gms.b.a.C0046a;
import java.util.List;

public abstract class bcv extends ajl implements bcu {
    public bcv() {
        super("com.google.android.gms.ads.internal.mediation.client.IMediationAdapter");
    }

    /* access modifiers changed from: protected */
    public final boolean dispatchTransaction(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        bcx bcz;
        bcx bcz2;
        bcx bcz3;
        bcx bcx = null;
        switch (i) {
            case 1:
                a a2 = C0046a.a(parcel.readStrongBinder());
                aot aot = (aot) ajm.a(parcel, aot.CREATOR);
                aop aop = (aop) ajm.a(parcel, aop.CREATOR);
                String readString = parcel.readString();
                IBinder readStrongBinder = parcel.readStrongBinder();
                if (readStrongBinder == null) {
                    bcz3 = null;
                } else {
                    IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.ads.internal.mediation.client.IMediationAdapterListener");
                    bcz3 = queryLocalInterface instanceof bcx ? (bcx) queryLocalInterface : new bcz(readStrongBinder);
                }
                a(a2, aot, aop, readString, bcz3);
                parcel2.writeNoException();
                break;
            case 2:
                a a3 = a();
                parcel2.writeNoException();
                ajm.a(parcel2, (IInterface) a3);
                break;
            case 3:
                a a4 = C0046a.a(parcel.readStrongBinder());
                aop aop2 = (aop) ajm.a(parcel, aop.CREATOR);
                String readString2 = parcel.readString();
                IBinder readStrongBinder2 = parcel.readStrongBinder();
                if (readStrongBinder2 != null) {
                    IInterface queryLocalInterface2 = readStrongBinder2.queryLocalInterface("com.google.android.gms.ads.internal.mediation.client.IMediationAdapterListener");
                    bcx = queryLocalInterface2 instanceof bcx ? (bcx) queryLocalInterface2 : new bcz(readStrongBinder2);
                }
                a(a4, aop2, readString2, bcx);
                parcel2.writeNoException();
                break;
            case 4:
                b();
                parcel2.writeNoException();
                break;
            case 5:
                c();
                parcel2.writeNoException();
                break;
            case 6:
                a a5 = C0046a.a(parcel.readStrongBinder());
                aot aot2 = (aot) ajm.a(parcel, aot.CREATOR);
                aop aop3 = (aop) ajm.a(parcel, aop.CREATOR);
                String readString3 = parcel.readString();
                String readString4 = parcel.readString();
                IBinder readStrongBinder3 = parcel.readStrongBinder();
                if (readStrongBinder3 != null) {
                    IInterface queryLocalInterface3 = readStrongBinder3.queryLocalInterface("com.google.android.gms.ads.internal.mediation.client.IMediationAdapterListener");
                    bcx = queryLocalInterface3 instanceof bcx ? (bcx) queryLocalInterface3 : new bcz(readStrongBinder3);
                }
                a(a5, aot2, aop3, readString3, readString4, bcx);
                parcel2.writeNoException();
                break;
            case 7:
                a a6 = C0046a.a(parcel.readStrongBinder());
                aop aop4 = (aop) ajm.a(parcel, aop.CREATOR);
                String readString5 = parcel.readString();
                String readString6 = parcel.readString();
                IBinder readStrongBinder4 = parcel.readStrongBinder();
                if (readStrongBinder4 == null) {
                    bcz2 = null;
                } else {
                    IInterface queryLocalInterface4 = readStrongBinder4.queryLocalInterface("com.google.android.gms.ads.internal.mediation.client.IMediationAdapterListener");
                    bcz2 = queryLocalInterface4 instanceof bcx ? (bcx) queryLocalInterface4 : new bcz(readStrongBinder4);
                }
                a(a6, aop4, readString5, readString6, bcz2);
                parcel2.writeNoException();
                break;
            case 8:
                d();
                parcel2.writeNoException();
                break;
            case 9:
                e();
                parcel2.writeNoException();
                break;
            case 10:
                a(C0046a.a(parcel.readStrongBinder()), (aop) ajm.a(parcel, aop.CREATOR), parcel.readString(), hm.a(parcel.readStrongBinder()), parcel.readString());
                parcel2.writeNoException();
                break;
            case 11:
                a((aop) ajm.a(parcel, aop.CREATOR), parcel.readString());
                parcel2.writeNoException();
                break;
            case 12:
                f();
                parcel2.writeNoException();
                break;
            case 13:
                boolean g = g();
                parcel2.writeNoException();
                ajm.a(parcel2, g);
                break;
            case 14:
                a a7 = C0046a.a(parcel.readStrongBinder());
                aop aop5 = (aop) ajm.a(parcel, aop.CREATOR);
                String readString7 = parcel.readString();
                String readString8 = parcel.readString();
                IBinder readStrongBinder5 = parcel.readStrongBinder();
                if (readStrongBinder5 == null) {
                    bcz = null;
                } else {
                    IInterface queryLocalInterface5 = readStrongBinder5.queryLocalInterface("com.google.android.gms.ads.internal.mediation.client.IMediationAdapterListener");
                    bcz = queryLocalInterface5 instanceof bcx ? (bcx) queryLocalInterface5 : new bcz(readStrongBinder5);
                }
                a(a7, aop5, readString7, readString8, bcz, (aul) ajm.a(parcel, aul.CREATOR), parcel.createStringArrayList());
                parcel2.writeNoException();
                break;
            case 15:
                bdd h = h();
                parcel2.writeNoException();
                ajm.a(parcel2, (IInterface) h);
                break;
            case 16:
                bdh i3 = i();
                parcel2.writeNoException();
                ajm.a(parcel2, (IInterface) i3);
                break;
            case 17:
                Bundle j = j();
                parcel2.writeNoException();
                ajm.b(parcel2, j);
                break;
            case 18:
                Bundle k = k();
                parcel2.writeNoException();
                ajm.b(parcel2, k);
                break;
            case 19:
                Bundle l = l();
                parcel2.writeNoException();
                ajm.b(parcel2, l);
                break;
            case 20:
                a((aop) ajm.a(parcel, aop.CREATOR), parcel.readString(), parcel.readString());
                parcel2.writeNoException();
                break;
            case 21:
                a(C0046a.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
            case 22:
                boolean m = m();
                parcel2.writeNoException();
                ajm.a(parcel2, m);
                break;
            case 23:
                a(C0046a.a(parcel.readStrongBinder()), hm.a(parcel.readStrongBinder()), (List<String>) parcel.createStringArrayList());
                parcel2.writeNoException();
                break;
            case 24:
                avt n = n();
                parcel2.writeNoException();
                ajm.a(parcel2, (IInterface) n);
                break;
            case 25:
                a(ajm.a(parcel));
                parcel2.writeNoException();
                break;
            case 26:
                aqs o = o();
                parcel2.writeNoException();
                ajm.a(parcel2, (IInterface) o);
                break;
            case 27:
                bdk p = p();
                parcel2.writeNoException();
                ajm.a(parcel2, (IInterface) p);
                break;
            default:
                return false;
        }
        return true;
    }
}
