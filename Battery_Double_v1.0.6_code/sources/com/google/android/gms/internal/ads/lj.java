package com.google.android.gms.internal.ads;

final class lj implements bde {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ String f3481a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ ln f3482b;

    lj(lf lfVar, String str, ln lnVar) {
        this.f3481a = str;
        this.f3482b = lnVar;
    }

    public final void a(df dfVar) {
        String str = this.f3481a;
        String dfVar2 = dfVar.toString();
        jm.e(new StringBuilder(String.valueOf(str).length() + 21 + String.valueOf(dfVar2).length()).append("Failed to load URL: ").append(str).append("\n").append(dfVar2).toString());
        this.f3482b.a(null);
    }
}
