package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.ads.internal.ax;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

@cm
public final class bcl implements bbz {

    /* renamed from: a reason: collision with root package name */
    private final dl f3134a;

    /* renamed from: b reason: collision with root package name */
    private final bcr f3135b;
    private final Context c;
    private final bcb d;
    private final boolean e;
    /* access modifiers changed from: private */
    public final long f;
    /* access modifiers changed from: private */
    public final long g;
    private final int h;
    /* access modifiers changed from: private */
    public final Object i = new Object();
    /* access modifiers changed from: private */
    public boolean j = false;
    /* access modifiers changed from: private */
    public final Map<nn<bci>, bcf> k = new HashMap();
    private final boolean l;
    private final String m;
    private List<bci> n = new ArrayList();
    private final boolean o;

    public bcl(Context context, dl dlVar, bcr bcr, bcb bcb, boolean z, boolean z2, String str, long j2, long j3, int i2, boolean z3) {
        this.c = context;
        this.f3134a = dlVar;
        this.f3135b = bcr;
        this.d = bcb;
        this.e = z;
        this.l = z2;
        this.m = str;
        this.f = j2;
        this.g = j3;
        this.h = 2;
        this.o = z3;
    }

    private final void a(nn<bci> nnVar) {
        jv.f3440a.post(new bcn(this, nnVar));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0018, code lost:
        if (r2.hasNext() == false) goto L_0x003f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001a, code lost:
        r0 = (com.google.android.gms.internal.ads.nn) r2.next();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
        r1 = (com.google.android.gms.internal.ads.bci) r0.get();
        r4.n.add(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x002b, code lost:
        if (r1 == null) goto L_0x0014;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x002f, code lost:
        if (r1.f3132a != 0) goto L_0x0014;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0031, code lost:
        a(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0035, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0036, code lost:
        com.google.android.gms.internal.ads.jm.c("Exception while processing an adapter; continuing with other adapters", r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x003f, code lost:
        a(null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:?, code lost:
        return new com.google.android.gms.internal.ads.bci(1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:?, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0010, code lost:
        r2 = r5.iterator();
     */
    private final bci b(List<nn<bci>> list) {
        synchronized (this.i) {
            if (this.j) {
                bci bci = new bci(-1);
                return bci;
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x001d, code lost:
        r0 = r15.d.n;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0021, code lost:
        r8 = r16.iterator();
        r6 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002a, code lost:
        if (r8.hasNext() == false) goto L_0x00b8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x002c, code lost:
        r0 = (com.google.android.gms.internal.ads.nn) r8.next();
        r10 = com.google.android.gms.ads.internal.ax.l().a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x003e, code lost:
        if (r6 != 0) goto L_0x0084;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0044, code lost:
        if (r0.isDone() == false) goto L_0x0084;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0046, code lost:
        r1 = (com.google.android.gms.internal.ads.bci) r0.get();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x004c, code lost:
        r15.n.add(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0051, code lost:
        if (r1 == null) goto L_0x00cb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0055, code lost:
        if (r1.f3132a != 0) goto L_0x00cb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0057, code lost:
        r5 = r1.f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0059, code lost:
        if (r5 == null) goto L_0x00cb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x005f, code lost:
        if (r5.a() <= r4) goto L_0x00cb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0061, code lost:
        r2 = r5.a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0064, code lost:
        r14 = r1;
        r1 = r0;
        r0 = r14;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0068, code lost:
        r6 = java.lang.Math.max(r6 - (com.google.android.gms.ads.internal.ax.l().a() - r10), 0);
        r3 = r1;
        r4 = r2;
        r2 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0081, code lost:
        r0 = 10000;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:?, code lost:
        r1 = (com.google.android.gms.internal.ads.bci) r0.get(r6, java.util.concurrent.TimeUnit.MILLISECONDS);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x008d, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:?, code lost:
        com.google.android.gms.internal.ads.jm.c("Exception while processing an adapter; continuing with other adapters", r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0093, code lost:
        r6 = java.lang.Math.max(r6 - (com.google.android.gms.ads.internal.ax.l().a() - r10), 0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00a6, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00a7, code lost:
        java.lang.Math.max(r6 - (com.google.android.gms.ads.internal.ax.l().a() - r10), 0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00b7, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00b8, code lost:
        a(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00bb, code lost:
        if (r2 != null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00cb, code lost:
        r0 = r2;
        r1 = r3;
        r2 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:?, code lost:
        return r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:?, code lost:
        return new com.google.android.gms.internal.ads.bci(1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0010, code lost:
        r4 = -1;
        r3 = null;
        r2 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001b, code lost:
        if (r15.d.n == -1) goto L_0x0081;
     */
    private final bci c(List<nn<bci>> list) {
        synchronized (this.i) {
            if (this.j) {
                bci bci = new bci(-1);
                return bci;
            }
        }
    }

    public final bci a(List<bca> list) {
        aot aot;
        jm.b("Starting mediation.");
        ArrayList arrayList = new ArrayList();
        aot aot2 = this.f3134a.d;
        int[] iArr = new int[2];
        if (aot2.g != null) {
            ax.x();
            if (bck.a(this.m, iArr)) {
                int i2 = iArr[0];
                int i3 = iArr[1];
                aot[] aotArr = aot2.g;
                int length = aotArr.length;
                int i4 = 0;
                while (true) {
                    if (i4 >= length) {
                        break;
                    }
                    aot = aotArr[i4];
                    if (i2 == aot.e && i3 == aot.f2815b) {
                        break;
                    }
                    i4++;
                }
            }
        }
        aot = aot2;
        for (bca bca : list) {
            String str = "Trying mediation network: ";
            String valueOf = String.valueOf(bca.f3120b);
            jm.d(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
            for (String bcf : bca.c) {
                bcf bcf2 = new bcf(this.c, bcf, this.f3135b, this.d, bca, this.f3134a.c, aot, this.f3134a.k, this.e, this.l, this.f3134a.y, this.f3134a.n, this.f3134a.z, this.f3134a.X, this.o);
                nn a2 = jt.a((Callable<T>) new bcm<T>(this, bcf2));
                this.k.put(a2, bcf2);
                arrayList.add(a2);
            }
        }
        switch (this.h) {
            case 2:
                return c((List<nn<bci>>) arrayList);
            default:
                return b((List<nn<bci>>) arrayList);
        }
    }

    public final void a() {
        synchronized (this.i) {
            this.j = true;
            for (bcf a2 : this.k.values()) {
                a2.a();
            }
        }
    }

    public final List<bci> b() {
        return this.n;
    }
}
