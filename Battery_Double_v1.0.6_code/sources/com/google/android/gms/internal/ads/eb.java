package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.ParcelFileDescriptor.AutoCloseInputStream;
import android.os.ParcelFileDescriptor.AutoCloseOutputStream;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.ads.internal.ax;
import com.google.android.gms.common.internal.a.a;
import com.google.android.gms.common.internal.a.c;
import com.google.android.gms.common.internal.a.d;
import com.google.android.gms.common.util.l;
import java.io.DataInputStream;
import java.io.IOException;

@cm
public final class eb extends a {
    public static final Creator<eb> CREATOR = new ed();

    /* renamed from: a reason: collision with root package name */
    private ParcelFileDescriptor f3270a;

    /* renamed from: b reason: collision with root package name */
    private Parcelable f3271b;
    private boolean c;

    public eb(ParcelFileDescriptor parcelFileDescriptor) {
        this.f3270a = parcelFileDescriptor;
        this.f3271b = null;
        this.c = true;
    }

    public eb(d dVar) {
        this.f3270a = null;
        this.f3271b = dVar;
        this.c = false;
    }

    /* JADX INFO: finally extract failed */
    private final ParcelFileDescriptor a() {
        if (this.f3270a == null) {
            Parcel obtain = Parcel.obtain();
            try {
                this.f3271b.writeToParcel(obtain, 0);
                byte[] marshall = obtain.marshall();
                obtain.recycle();
                this.f3270a = a(marshall);
            } catch (Throwable th) {
                obtain.recycle();
                throw th;
            }
        }
        return this.f3270a;
    }

    private final <T> ParcelFileDescriptor a(byte[] bArr) {
        AutoCloseOutputStream autoCloseOutputStream;
        AutoCloseOutputStream autoCloseOutputStream2 = null;
        try {
            ParcelFileDescriptor[] createPipe = ParcelFileDescriptor.createPipe();
            autoCloseOutputStream = new AutoCloseOutputStream(createPipe[1]);
            try {
                new Thread(new ec(this, autoCloseOutputStream, bArr)).start();
                return createPipe[0];
            } catch (IOException e) {
                e = e;
            }
        } catch (IOException e2) {
            e = e2;
            autoCloseOutputStream = autoCloseOutputStream2;
            jm.b("Error transporting the ad response", e);
            ax.i().a((Throwable) e, "LargeParcelTeleporter.pipeData.2");
            l.a(autoCloseOutputStream);
            return autoCloseOutputStream2;
        }
    }

    /* JADX INFO: finally extract failed */
    public final <T extends d> T a(Creator<T> creator) {
        if (this.c) {
            if (this.f3270a == null) {
                jm.c("File descriptor is empty, returning null.");
                return null;
            }
            DataInputStream dataInputStream = new DataInputStream(new AutoCloseInputStream(this.f3270a));
            try {
                byte[] bArr = new byte[dataInputStream.readInt()];
                dataInputStream.readFully(bArr, 0, bArr.length);
                l.a(dataInputStream);
                Parcel obtain = Parcel.obtain();
                try {
                    obtain.unmarshall(bArr, 0, bArr.length);
                    obtain.setDataPosition(0);
                    this.f3271b = (d) creator.createFromParcel(obtain);
                    obtain.recycle();
                    this.c = false;
                } catch (Throwable th) {
                    obtain.recycle();
                    throw th;
                }
            } catch (IOException e) {
                jm.b("Could not read from parcel file descriptor", e);
                l.a(dataInputStream);
                return null;
            } catch (Throwable th2) {
                l.a(dataInputStream);
                throw th2;
            }
        }
        return (d) this.f3271b;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        a();
        int a2 = c.a(parcel);
        c.a(parcel, 2, (Parcelable) this.f3270a, i, false);
        c.a(parcel, a2);
    }
}
