package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.gmsg.ae;
import org.json.JSONObject;

@cm
public interface bq<T> {
    nn<JSONObject> a(JSONObject jSONObject);

    void a();

    void a(String str, ae<? super T> aeVar);

    void a(String str, JSONObject jSONObject);

    nn<JSONObject> b(JSONObject jSONObject);

    void b(String str, ae<? super T> aeVar);

    nn<JSONObject> c(JSONObject jSONObject);

    nn<JSONObject> d(JSONObject jSONObject);
}
