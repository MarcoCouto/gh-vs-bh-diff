package com.google.android.gms.internal.ads;

import java.util.Iterator;

final class aen implements Iterator<String> {

    /* renamed from: a reason: collision with root package name */
    private Iterator<String> f2509a = this.f2510b.f2506a.iterator();

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ ael f2510b;

    aen(ael ael) {
        this.f2510b = ael;
    }

    public final boolean hasNext() {
        return this.f2509a.hasNext();
    }

    public final /* synthetic */ Object next() {
        return (String) this.f2509a.next();
    }

    public final void remove() {
        throw new UnsupportedOperationException();
    }
}
