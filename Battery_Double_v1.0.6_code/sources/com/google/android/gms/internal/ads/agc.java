package com.google.android.gms.internal.ads;

import android.content.ComponentName;
import android.support.b.b;
import android.support.b.d;
import java.lang.ref.WeakReference;

public final class agc extends d {

    /* renamed from: a reason: collision with root package name */
    private WeakReference<agd> f2556a;

    public agc(agd agd) {
        this.f2556a = new WeakReference<>(agd);
    }

    public final void a(ComponentName componentName, b bVar) {
        agd agd = (agd) this.f2556a.get();
        if (agd != null) {
            agd.a(bVar);
        }
    }

    public final void onServiceDisconnected(ComponentName componentName) {
        agd agd = (agd) this.f2556a.get();
        if (agd != null) {
            agd.a();
        }
    }
}
