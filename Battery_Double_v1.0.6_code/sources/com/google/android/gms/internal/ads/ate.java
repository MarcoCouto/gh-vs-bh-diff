package com.google.android.gms.internal.ads;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public final class ate extends ajk implements atc {
    ate(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.customrenderedad.client.IOnCustomRenderedAdLoadedListener");
    }

    public final void a(asz asz) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (IInterface) asz);
        b(1, r_);
    }
}
