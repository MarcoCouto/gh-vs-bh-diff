package com.google.android.gms.internal.ads;

import android.net.Uri;
import android.net.Uri.Builder;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.ax;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

@cm
public final class fe {
    private boolean A = false;
    private dr B;
    private boolean C = false;
    private String D;
    private List<String> E;
    private boolean F;
    private String G;
    private hz H;
    private boolean I;
    private boolean J;
    private boolean K;
    private boolean L;
    private String M;
    private final dl N;

    /* renamed from: a reason: collision with root package name */
    private String f3308a;

    /* renamed from: b reason: collision with root package name */
    private String f3309b;
    private String c;
    private List<String> d;
    private String e;
    private String f;
    private String g;
    private List<String> h;
    private List<String> i;
    private long j = -1;
    private boolean k = false;
    private final long l = -1;
    private List<String> m;
    private long n = -1;
    private int o = -1;
    private boolean p = false;
    private boolean q = false;
    private boolean r = false;
    private boolean s = true;
    private boolean t = true;
    private String u = "";
    private boolean v = false;
    private boolean w = false;
    private hp x;
    private List<String> y;
    private List<String> z;

    public fe(dl dlVar, String str) {
        this.f3309b = str;
        this.N = dlVar;
    }

    private static String a(Map<String, List<String>> map, String str) {
        List list = (List) map.get(str);
        if (list == null || list.isEmpty()) {
            return null;
        }
        return (String) list.get(0);
    }

    private static long b(Map<String, List<String>> map, String str) {
        List list = (List) map.get(str);
        if (list != null && !list.isEmpty()) {
            String str2 = (String) list.get(0);
            try {
                return (long) (Float.parseFloat(str2) * 1000.0f);
            } catch (NumberFormatException e2) {
                jm.e(new StringBuilder(String.valueOf(str).length() + 36 + String.valueOf(str2).length()).append("Could not parse float from ").append(str).append(" header: ").append(str2).toString());
            }
        }
        return -1;
    }

    private static List<String> c(Map<String, List<String>> map, String str) {
        List list = (List) map.get(str);
        if (list != null && !list.isEmpty()) {
            String str2 = (String) list.get(0);
            if (str2 != null) {
                return Arrays.asList(str2.trim().split("\\s+"));
            }
        }
        return null;
    }

    private static boolean d(Map<String, List<String>> map, String str) {
        List list = (List) map.get(str);
        if (list == null || list.isEmpty()) {
            return false;
        }
        return Boolean.valueOf((String) list.get(0)).booleanValue();
    }

    public final dp a(long j2, boolean z2) {
        return new dp(this.N, this.f3309b, this.c, this.d, this.h, this.j, this.k, -1, this.m, this.n, this.o, this.f3308a, j2, this.f, this.g, this.p, this.q, this.r, this.s, false, this.u, this.v, this.w, this.x, this.y, this.z, this.A, this.B, this.C, this.D, this.E, this.F, this.G, this.H, this.e, this.t, this.I, this.J, z2 ? 2 : 1, this.K, this.i, this.L, this.M);
    }

    public final void a(String str, Map<String, List<String>> map, String str2) {
        this.c = str2;
        a(map);
    }

    public final void a(Map<String, List<String>> map) {
        this.f3308a = a(map, "X-Afma-Ad-Size");
        this.G = a(map, "X-Afma-Ad-Slot-Size");
        List<String> c2 = c(map, "X-Afma-Click-Tracking-Urls");
        if (c2 != null) {
            this.d = c2;
        }
        this.e = a(map, "X-Afma-Debug-Signals");
        List list = (List) map.get("X-Afma-Debug-Dialog");
        if (list != null && !list.isEmpty()) {
            this.f = (String) list.get(0);
        }
        List<String> c3 = c(map, "X-Afma-Tracking-Urls");
        if (c3 != null) {
            this.h = c3;
        }
        List<String> c4 = c(map, "X-Afma-Downloaded-Impression-Urls");
        if (c4 != null) {
            this.i = c4;
        }
        long b2 = b(map, "X-Afma-Interstitial-Timeout");
        if (b2 != -1) {
            this.j = b2;
        }
        this.k |= d(map, "X-Afma-Mediation");
        List<String> c5 = c(map, "X-Afma-Manual-Tracking-Urls");
        if (c5 != null) {
            this.m = c5;
        }
        long b3 = b(map, "X-Afma-Refresh-Rate");
        if (b3 != -1) {
            this.n = b3;
        }
        List list2 = (List) map.get("X-Afma-Orientation");
        if (list2 != null && !list2.isEmpty()) {
            String str = (String) list2.get(0);
            if ("portrait".equalsIgnoreCase(str)) {
                this.o = ax.g().b();
            } else if ("landscape".equalsIgnoreCase(str)) {
                this.o = ax.g().a();
            }
        }
        this.g = a(map, "X-Afma-ActiveView");
        List list3 = (List) map.get("X-Afma-Use-HTTPS");
        if (list3 != null && !list3.isEmpty()) {
            this.r = Boolean.valueOf((String) list3.get(0)).booleanValue();
        }
        this.p |= d(map, "X-Afma-Custom-Rendering-Allowed");
        this.q = "native".equals(a(map, "X-Afma-Ad-Format"));
        List list4 = (List) map.get("X-Afma-Content-Url-Opted-Out");
        if (list4 != null && !list4.isEmpty()) {
            this.s = Boolean.valueOf((String) list4.get(0)).booleanValue();
        }
        List list5 = (List) map.get("X-Afma-Content-Vertical-Opted-Out");
        if (list5 != null && !list5.isEmpty()) {
            this.t = Boolean.valueOf((String) list5.get(0)).booleanValue();
        }
        List list6 = (List) map.get("X-Afma-Gws-Query-Id");
        if (list6 != null && !list6.isEmpty()) {
            this.u = (String) list6.get(0);
        }
        String a2 = a(map, "X-Afma-Fluid");
        if (a2 != null && a2.equals("height")) {
            this.v = true;
        }
        this.w = "native_express".equals(a(map, "X-Afma-Ad-Format"));
        this.x = hp.a(a(map, "X-Afma-Rewards"));
        if (this.y == null) {
            this.y = c(map, "X-Afma-Reward-Video-Start-Urls");
        }
        if (this.z == null) {
            this.z = c(map, "X-Afma-Reward-Video-Complete-Urls");
        }
        this.A |= d(map, "X-Afma-Use-Displayed-Impression");
        this.C |= d(map, "X-Afma-Auto-Collect-Location");
        this.D = a(map, "Set-Cookie");
        String a3 = a(map, "X-Afma-Auto-Protection-Configuration");
        if (a3 == null || TextUtils.isEmpty(a3)) {
            Builder buildUpon = Uri.parse("https://pagead2.googlesyndication.com/pagead/gen_204").buildUpon();
            buildUpon.appendQueryParameter("id", "gmob-apps-blocked-navigation");
            if (!TextUtils.isEmpty(this.f)) {
                buildUpon.appendQueryParameter("debugDialog", this.f);
            }
            boolean booleanValue = ((Boolean) ape.f().a(asi.g)).booleanValue();
            String builder = buildUpon.toString();
            this.B = new dr(booleanValue, Arrays.asList(new String[]{new StringBuilder(String.valueOf(builder).length() + 31).append(builder).append("&navigationURL={NAVIGATION_URL}").toString()}));
        } else {
            try {
                this.B = dr.a(new JSONObject(a3));
            } catch (JSONException e2) {
                jm.c("Error parsing configuration JSON", e2);
                this.B = new dr();
            }
        }
        List<String> c6 = c(map, "X-Afma-Remote-Ping-Urls");
        if (c6 != null) {
            this.E = c6;
        }
        String a4 = a(map, "X-Afma-Safe-Browsing");
        if (!TextUtils.isEmpty(a4)) {
            try {
                this.H = hz.a(new JSONObject(a4));
            } catch (JSONException e3) {
                jm.c("Error parsing safe browsing header", e3);
            }
        }
        this.F |= d(map, "X-Afma-Render-In-Browser");
        String a5 = a(map, "X-Afma-Pool");
        if (!TextUtils.isEmpty(a5)) {
            try {
                this.I = new JSONObject(a5).getBoolean("never_pool");
            } catch (JSONException e4) {
                jm.c("Error parsing interstitial pool header", e4);
            }
        }
        this.J = d(map, "X-Afma-Custom-Close-Blocked");
        this.K = d(map, "X-Afma-Enable-Omid");
        this.L = d(map, "X-Afma-Disable-Closable-Area");
        this.M = a(map, "X-Afma-Omid-Settings");
    }
}
