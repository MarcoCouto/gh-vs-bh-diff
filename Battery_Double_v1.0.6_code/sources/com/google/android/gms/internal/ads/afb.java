package com.google.android.gms.internal.ads;

public enum afb {
    INT(Integer.valueOf(0)),
    LONG(Long.valueOf(0)),
    FLOAT(Float.valueOf(0.0f)),
    DOUBLE(Double.valueOf(0.0d)),
    BOOLEAN(Boolean.valueOf(false)),
    STRING(""),
    BYTE_STRING(aah.f2393a),
    ENUM(null),
    MESSAGE(null);
    
    private final Object j;

    private afb(Object obj) {
        this.j = obj;
    }
}
