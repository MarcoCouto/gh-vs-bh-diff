package com.google.android.gms.internal.ads;

import android.view.View;

final class se implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ View f3677a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ ic f3678b;
    private final /* synthetic */ int c;
    private final /* synthetic */ sc d;

    se(sc scVar, View view, ic icVar, int i) {
        this.d = scVar;
        this.f3677a = view;
        this.f3678b = icVar;
        this.c = i;
    }

    public final void run() {
        this.d.a(this.f3677a, this.f3678b, this.c - 1);
    }
}
