package com.google.android.gms.internal.ads;

import android.annotation.TargetApi;
import android.net.http.SslError;
import android.view.KeyEvent;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;

@cm
final class sm extends WebViewClient {

    /* renamed from: a reason: collision with root package name */
    private final sq f3689a;

    /* renamed from: b reason: collision with root package name */
    private final sv f3690b;
    private final ss c;
    private final su d;
    private final sw e = new sw();

    sm(sq sqVar, sv svVar, ss ssVar, su suVar) {
        this.f3689a = sqVar;
        this.f3690b = svVar;
        this.c = ssVar;
        this.d = suVar;
    }

    private final boolean a(sn snVar) {
        return this.f3689a.c(snVar);
    }

    private final WebResourceResponse b(sn snVar) {
        return this.f3690b.d(snVar);
    }

    public final void onLoadResource(WebView webView, String str) {
        if (str != null) {
            String str2 = "Loading resource: ";
            String valueOf = String.valueOf(str);
            jm.a(valueOf.length() != 0 ? str2.concat(valueOf) : new String(str2));
            this.c.b(new sn(str));
        }
    }

    public final void onPageFinished(WebView webView, String str) {
        if (str != null) {
            this.d.a(new sn(str));
        }
    }

    public final void onReceivedError(WebView webView, int i, String str, String str2) {
        this.e.a(i, str2);
    }

    public final void onReceivedSslError(WebView webView, SslErrorHandler sslErrorHandler, SslError sslError) {
        this.e.a(sslError);
    }

    @TargetApi(24)
    public final WebResourceResponse shouldInterceptRequest(WebView webView, WebResourceRequest webResourceRequest) {
        if (webResourceRequest == null || webResourceRequest.getUrl() == null) {
            return null;
        }
        return b(new sn(webResourceRequest));
    }

    public final WebResourceResponse shouldInterceptRequest(WebView webView, String str) {
        if (str == null) {
            return null;
        }
        return b(new sn(str));
    }

    public final boolean shouldOverrideKeyEvent(WebView webView, KeyEvent keyEvent) {
        switch (keyEvent.getKeyCode()) {
            case 79:
            case 85:
            case 86:
            case 87:
            case 88:
            case 89:
            case 90:
            case 91:
            case 126:
            case 127:
            case 128:
            case 129:
            case 130:
            case 222:
                return true;
            default:
                return false;
        }
    }

    @TargetApi(24)
    public final boolean shouldOverrideUrlLoading(WebView webView, WebResourceRequest webResourceRequest) {
        if (webResourceRequest == null || webResourceRequest.getUrl() == null) {
            return false;
        }
        return a(new sn(webResourceRequest));
    }

    public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
        if (str == null) {
            return false;
        }
        return a(new sn(str));
    }
}
