package com.google.android.gms.internal.ads;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.b.a;

public final class awm extends ajk implements awk {
    awm(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.formats.client.IOnPublisherAdViewLoadedListener");
    }

    public final void a(apv apv, a aVar) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (IInterface) apv);
        ajm.a(r_, (IInterface) aVar);
        b(1, r_);
    }
}
