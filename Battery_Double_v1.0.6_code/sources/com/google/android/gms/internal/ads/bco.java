package com.google.android.gms.internal.ads;

import android.content.Context;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.ax;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@cm
public final class bco implements bbz {

    /* renamed from: a reason: collision with root package name */
    private final dl f3140a;

    /* renamed from: b reason: collision with root package name */
    private final bcr f3141b;
    private final Context c;
    private final Object d = new Object();
    private final bcb e;
    private final boolean f;
    private final long g;
    private final long h;
    private final asv i;
    private final boolean j;
    private final String k;
    private boolean l = false;
    private bcf m;
    private List<bci> n = new ArrayList();
    private final boolean o;

    public bco(Context context, dl dlVar, bcr bcr, bcb bcb, boolean z, boolean z2, String str, long j2, long j3, asv asv, boolean z3) {
        this.c = context;
        this.f3140a = dlVar;
        this.f3141b = bcr;
        this.e = bcb;
        this.f = z;
        this.j = z2;
        this.k = str;
        this.g = j2;
        this.h = j3;
        this.i = asv;
        this.o = z3;
    }

    /* JADX INFO: used method not loaded: com.google.android.gms.internal.ads.bcf.a(long, long):null, types can be incorrect */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00ed, code lost:
        r2 = r24.m.a(r24.g, r24.h);
        r24.n.add(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0106, code lost:
        if (r2.f3132a != 0) goto L_0x0154;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0108, code lost:
        com.google.android.gms.internal.ads.jm.b("Adapter succeeded.");
        r24.i.a("mediation_network_succeed", r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x011a, code lost:
        if (r18.isEmpty() != false) goto L_0x012d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x011c, code lost:
        r24.i.a("mediation_networks_fail", android.text.TextUtils.join(",", r18));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x012d, code lost:
        r24.i.a(r22, "mls");
        r24.i.a(r19, "ttm");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0154, code lost:
        r18.add(r4);
        r24.i.a(r22, "mlf");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x016c, code lost:
        if (r2.c == null) goto L_0x006d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x016e, code lost:
        com.google.android.gms.internal.ads.jv.f3440a.post(new com.google.android.gms.internal.ads.bcp(r24, r2));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:?, code lost:
        return r2;
     */
    public final bci a(List<bca> list) {
        aot aot;
        jm.b("Starting mediation.");
        ArrayList arrayList = new ArrayList();
        ast a2 = this.i.a();
        aot aot2 = this.f3140a.d;
        int[] iArr = new int[2];
        if (aot2.g != null) {
            ax.x();
            if (bck.a(this.k, iArr)) {
                int i2 = iArr[0];
                int i3 = iArr[1];
                aot[] aotArr = aot2.g;
                int length = aotArr.length;
                int i4 = 0;
                while (true) {
                    if (i4 >= length) {
                        break;
                    }
                    aot = aotArr[i4];
                    if (i2 == aot.e && i3 == aot.f2815b) {
                        break;
                    }
                    i4++;
                }
            }
        }
        aot = aot2;
        for (bca bca : list) {
            String str = "Trying mediation network: ";
            String valueOf = String.valueOf(bca.f3120b);
            jm.d(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
            Iterator it = bca.c.iterator();
            while (true) {
                if (it.hasNext()) {
                    String str2 = (String) it.next();
                    ast a3 = this.i.a();
                    synchronized (this.d) {
                        if (this.l) {
                            bci bci = new bci(-1);
                            return bci;
                        }
                        this.m = new bcf(this.c, str2, this.f3141b, this.e, bca, this.f3140a.c, aot, this.f3140a.k, this.f, this.j, this.f3140a.y, this.f3140a.n, this.f3140a.z, this.f3140a.X, this.o);
                    }
                }
            }
        }
        if (!arrayList.isEmpty()) {
            this.i.a("mediation_networks_fail", TextUtils.join(",", arrayList));
        }
        return new bci(1);
    }

    public final void a() {
        synchronized (this.d) {
            this.l = true;
            if (this.m != null) {
                this.m.a();
            }
        }
    }

    public final List<bci> b() {
        return this.n;
    }
}
