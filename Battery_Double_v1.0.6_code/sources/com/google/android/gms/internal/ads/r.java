package com.google.android.gms.internal.ads;

import android.content.Intent;
import android.os.Bundle;
import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.b.a;

public interface r extends IInterface {
    void a(int i, int i2, Intent intent) throws RemoteException;

    void a(Bundle bundle) throws RemoteException;

    void a(a aVar) throws RemoteException;

    void b(Bundle bundle) throws RemoteException;

    void d() throws RemoteException;

    boolean e() throws RemoteException;

    void f() throws RemoteException;

    void g() throws RemoteException;

    void h() throws RemoteException;

    void i() throws RemoteException;

    void j() throws RemoteException;

    void k() throws RemoteException;

    void l() throws RemoteException;
}
