package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.ax;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@cm
public final class px implements Iterable<pv> {

    /* renamed from: a reason: collision with root package name */
    private final List<pv> f3618a = new ArrayList();

    public static boolean a(pm pmVar) {
        pv b2 = b(pmVar);
        if (b2 == null) {
            return false;
        }
        b2.f3616b.b();
        return true;
    }

    static pv b(pm pmVar) {
        Iterator it = ax.z().iterator();
        while (it.hasNext()) {
            pv pvVar = (pv) it.next();
            if (pvVar.f3615a == pmVar) {
                return pvVar;
            }
        }
        return null;
    }

    public final int a() {
        return this.f3618a.size();
    }

    public final void a(pv pvVar) {
        this.f3618a.add(pvVar);
    }

    public final void b(pv pvVar) {
        this.f3618a.remove(pvVar);
    }

    public final Iterator<pv> iterator() {
        return this.f3618a.iterator();
    }
}
