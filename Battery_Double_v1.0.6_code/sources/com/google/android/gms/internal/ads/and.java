package com.google.android.gms.internal.ads;

import android.os.RemoteException;

public final class and {

    /* renamed from: a reason: collision with root package name */
    private final byte[] f2747a;

    /* renamed from: b reason: collision with root package name */
    private int f2748b;
    private int c;
    private final /* synthetic */ anb d;

    private and(anb anb, byte[] bArr) {
        this.d = anb;
        this.f2747a = bArr;
    }

    public final and a(int i) {
        this.f2748b = i;
        return this;
    }

    public final synchronized void a() {
        try {
            if (this.d.f2746b) {
                this.d.f2745a.a(this.f2747a);
                this.d.f2745a.a(this.f2748b);
                this.d.f2745a.b(this.c);
                this.d.f2745a.a((int[]) null);
                this.d.f2745a.a();
            }
        } catch (RemoteException e) {
            ms.a("Clearcut log failed", e);
        }
        return;
    }

    public final and b(int i) {
        this.c = i;
        return this;
    }
}
