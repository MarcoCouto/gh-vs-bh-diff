package com.google.android.gms.internal.ads;

import android.annotation.TargetApi;
import android.content.Context;
import android.media.AudioManager;
import android.media.AudioManager.OnAudioFocusChangeListener;

@TargetApi(14)
@cm
public final class pp implements OnAudioFocusChangeListener {

    /* renamed from: a reason: collision with root package name */
    private final AudioManager f3610a;

    /* renamed from: b reason: collision with root package name */
    private final pr f3611b;
    private boolean c;
    private boolean d;
    private boolean e;
    private float f = 1.0f;

    public pp(Context context, pr prVar) {
        this.f3610a = (AudioManager) context.getSystemService("audio");
        this.f3611b = prVar;
    }

    private final void d() {
        boolean z = true;
        boolean z2 = this.d && !this.e && this.f > 0.0f;
        if (z2 && !this.c) {
            if (this.f3610a != null && !this.c) {
                if (this.f3610a.requestAudioFocus(this, 3, 2) != 1) {
                    z = false;
                }
                this.c = z;
            }
            this.f3611b.e();
        } else if (!z2 && this.c) {
            if (this.f3610a != null && this.c) {
                if (this.f3610a.abandonAudioFocus(this) != 0) {
                    z = false;
                }
                this.c = z;
            }
            this.f3611b.e();
        }
    }

    public final float a() {
        float f2 = this.e ? 0.0f : this.f;
        if (this.c) {
            return f2;
        }
        return 0.0f;
    }

    public final void a(float f2) {
        this.f = f2;
        d();
    }

    public final void a(boolean z) {
        this.e = z;
        d();
    }

    public final void b() {
        this.d = true;
        d();
    }

    public final void c() {
        this.d = false;
        d();
    }

    public final void onAudioFocusChange(int i) {
        this.c = i > 0;
        this.f3611b.e();
    }
}
