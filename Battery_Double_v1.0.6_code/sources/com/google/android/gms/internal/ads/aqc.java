package com.google.android.gms.internal.ads;

import android.os.IBinder;
import android.os.RemoteException;

public final class aqc extends ajk implements aqa {
    aqc(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.client.IAdMetadataListener");
    }

    public final void a() throws RemoteException {
        b(1, r_());
    }
}
