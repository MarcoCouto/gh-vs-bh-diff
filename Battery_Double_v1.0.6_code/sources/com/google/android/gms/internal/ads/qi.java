package com.google.android.gms.internal.ads;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.webkit.JsResult;

final class qi implements OnClickListener {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ JsResult f3631a;

    qi(JsResult jsResult) {
        this.f3631a = jsResult;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f3631a.confirm();
    }
}
