package com.google.android.gms.internal.ads;

@cm
final class aze {

    /* renamed from: a reason: collision with root package name */
    private static final azb f3030a = azb.a();

    /* renamed from: b reason: collision with root package name */
    private static final float f3031b = ((Float) ape.f().a(asi.bf)).floatValue();
    private static final long c = ((Long) ape.f().a(asi.bd)).longValue();
    private static final float d = ((Float) ape.f().a(asi.bg)).floatValue();
    private static final long e = ((Long) ape.f().a(asi.be)).longValue();

    private static int a(long j, int i) {
        return (int) ((j >>> ((i % 16) * 4)) & 15);
    }

    static boolean a() {
        int i = Integer.MAX_VALUE;
        int h = f3030a.h();
        int i2 = f3030a.i();
        int f = f3030a.f() + f3030a.g();
        int i3 = (h >= 16 || e == 0) ? d != 0.0f ? ((int) (d * ((float) h))) + 1 : Integer.MAX_VALUE : a(e, h);
        if (i2 <= i3) {
            if (h < 16 && c != 0) {
                i = a(c, h);
            } else if (f3031b != 0.0f) {
                i = (int) (f3031b * ((float) h));
            }
            if (f <= i) {
                return true;
            }
        }
        return false;
    }
}
