package com.google.android.gms.internal.ads;

import android.os.IBinder;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable;
import android.os.RemoteException;

public final class axk extends ajk implements axj {
    axk(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.httpcache.IHttpAssetsCacheService");
    }

    public final ParcelFileDescriptor a(axf axf) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (Parcelable) axf);
        Parcel a2 = a(1, r_);
        ParcelFileDescriptor parcelFileDescriptor = (ParcelFileDescriptor) ajm.a(a2, ParcelFileDescriptor.CREATOR);
        a2.recycle();
        return parcelFileDescriptor;
    }
}
