package com.google.android.gms.internal.ads;

import android.content.Context;
import android.net.Uri;
import android.view.View;
import android.view.View.OnAttachStateChangeListener;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.ViewTreeObserver.OnScrollChangedListener;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import com.google.android.gms.ads.internal.ax;
import com.google.android.gms.ads.internal.bv;
import com.google.android.gms.ads.internal.gmsg.a;
import com.google.android.gms.ads.internal.gmsg.ae;
import com.google.android.gms.ads.internal.gmsg.ah;
import com.google.android.gms.ads.internal.gmsg.ai;
import com.google.android.gms.ads.internal.gmsg.d;
import com.google.android.gms.ads.internal.gmsg.e;
import com.google.android.gms.ads.internal.gmsg.k;
import com.google.android.gms.ads.internal.gmsg.m;
import com.google.android.gms.ads.internal.gmsg.o;
import com.google.android.gms.ads.internal.overlay.AdOverlayInfoParcel;
import com.google.android.gms.ads.internal.overlay.c;
import com.google.android.gms.ads.internal.overlay.l;
import com.google.android.gms.ads.internal.overlay.n;
import com.google.android.gms.ads.internal.overlay.t;
import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Map.Entry;

@cm
public final class sc extends azt<qn> implements rv, sq, ss, su, sv {
    private OnAttachStateChangeListener A;

    /* renamed from: a reason: collision with root package name */
    private qn f3674a;

    /* renamed from: b reason: collision with root package name */
    private final Object f3675b;
    private aoj c;
    private n d;
    private rw e;
    private rx f;
    private k g;
    private m h;
    private ry i;
    private boolean j;
    private ai k;
    private boolean l;
    private boolean m;
    private OnGlobalLayoutListener n;
    private OnScrollChangedListener o;
    private boolean p;
    private t q;
    private final m r;
    private bv s;
    private d t;
    private o u;
    private rz v;
    private ic w;
    private boolean x;
    private boolean y;
    private int z;

    public sc(qn qnVar, boolean z2) {
        this(qnVar, z2, new m(qnVar, qnVar.q(), new art(qnVar.getContext())), null);
    }

    private sc(qn qnVar, boolean z2, m mVar, d dVar) {
        this.f3675b = new Object();
        this.j = false;
        this.f3674a = qnVar;
        this.l = z2;
        this.r = mVar;
        this.t = null;
    }

    /* access modifiers changed from: private */
    public final void a(View view, ic icVar, int i2) {
        if (icVar.b() && i2 > 0) {
            icVar.a(view);
            if (icVar.b()) {
                jv.f3440a.postDelayed(new se(this, view, icVar, i2), 100);
            }
        }
    }

    private final void a(AdOverlayInfoParcel adOverlayInfoParcel) {
        boolean z2 = false;
        boolean z3 = this.t != null ? this.t.a() : false;
        ax.c();
        Context context = this.f3674a.getContext();
        if (!z3) {
            z2 = true;
        }
        l.a(context, adOverlayInfoParcel, z2);
        if (this.w != null) {
            String str = adOverlayInfoParcel.l;
            if (str == null && adOverlayInfoParcel.f2076a != null) {
                str = adOverlayInfoParcel.f2076a.f2078a;
            }
            this.w.a(str);
        }
    }

    private final WebResourceResponse e(sn snVar) throws IOException {
        HttpURLConnection httpURLConnection;
        URL url = new URL(snVar.f3691a);
        int i2 = 0;
        while (true) {
            int i3 = i2 + 1;
            if (i3 <= 20) {
                URLConnection openConnection = url.openConnection();
                openConnection.setConnectTimeout(10000);
                openConnection.setReadTimeout(10000);
                for (Entry entry : snVar.c.entrySet()) {
                    openConnection.addRequestProperty((String) entry.getKey(), (String) entry.getValue());
                }
                if (!(openConnection instanceof HttpURLConnection)) {
                    throw new IOException("Invalid protocol.");
                }
                httpURLConnection = (HttpURLConnection) openConnection;
                ax.e().a(this.f3674a.getContext(), this.f3674a.k().f3528a, false, httpURLConnection);
                ml mlVar = new ml();
                mlVar.a(httpURLConnection, (byte[]) null);
                int responseCode = httpURLConnection.getResponseCode();
                mlVar.a(httpURLConnection, responseCode);
                if (responseCode < 300 || responseCode >= 400) {
                    ax.e();
                } else {
                    String headerField = httpURLConnection.getHeaderField("Location");
                    if (headerField == null) {
                        throw new IOException("Missing Location header in redirect");
                    }
                    URL url2 = new URL(url, headerField);
                    String protocol = url2.getProtocol();
                    if (protocol == null) {
                        jm.e("Protocol is null");
                        return null;
                    } else if (protocol.equals("http") || protocol.equals("https")) {
                        String str = "Redirecting to ";
                        String valueOf = String.valueOf(headerField);
                        jm.b(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
                        httpURLConnection.disconnect();
                        i2 = i3;
                        url = url2;
                    } else {
                        String str2 = "Unsupported scheme: ";
                        String valueOf2 = String.valueOf(protocol);
                        jm.e(valueOf2.length() != 0 ? str2.concat(valueOf2) : new String(str2));
                        return null;
                    }
                }
            } else {
                throw new IOException("Too many redirects (20)");
            }
        }
        ax.e();
        return jv.a(httpURLConnection);
    }

    private final void q() {
        if (this.A != null) {
            this.f3674a.getView().removeOnAttachStateChangeListener(this.A);
        }
    }

    private final void r() {
        if (this.e != null && ((this.x && this.z <= 0) || this.y)) {
            this.e.a(!this.y);
            this.e = null;
        }
        this.f3674a.G();
    }

    public final bv a() {
        return this.s;
    }

    public final void a(int i2, int i3) {
        if (this.t != null) {
            this.t.a(i2, i3);
        }
    }

    public final void a(int i2, int i3, boolean z2) {
        this.r.a(i2, i3);
        if (this.t != null) {
            this.t.a(i2, i3, z2);
        }
    }

    public final void a(OnGlobalLayoutListener onGlobalLayoutListener, OnScrollChangedListener onScrollChangedListener) {
        synchronized (this.f3675b) {
            this.m = true;
            this.f3674a.F();
            this.n = onGlobalLayoutListener;
            this.o = onScrollChangedListener;
        }
    }

    public final void a(c cVar) {
        n nVar = null;
        boolean z2 = this.f3674a.z();
        aoj aoj = (!z2 || this.f3674a.t().d()) ? this.c : null;
        if (!z2) {
            nVar = this.d;
        }
        a(new AdOverlayInfoParcel(cVar, aoj, nVar, this.q, this.f3674a.k()));
    }

    public final void a(aoj aoj, k kVar, n nVar, m mVar, t tVar, boolean z2, ai aiVar, bv bvVar, o oVar, ic icVar) {
        bv bvVar2 = bvVar == null ? new bv(this.f3674a.getContext(), icVar, null) : bvVar;
        this.t = new d(this.f3674a, oVar);
        this.w = icVar;
        if (((Boolean) ape.f().a(asi.aF)).booleanValue()) {
            a("/adMetadata", (ae<? super ReferenceT>) new a<Object>(kVar));
        }
        a("/appEvent", (ae<? super ReferenceT>) new com.google.android.gms.ads.internal.gmsg.l<Object>(mVar));
        a("/backButton", o.j);
        a("/refresh", o.k);
        a("/canOpenURLs", o.f2056a);
        a("/canOpenIntents", o.f2057b);
        a("/click", o.c);
        a("/close", o.d);
        a("/customClose", o.e);
        a("/instrument", o.n);
        a("/delayPageLoaded", o.p);
        a("/delayPageClosed", o.q);
        a("/getLocationInfo", o.r);
        a("/httpTrack", o.f);
        a("/log", o.g);
        a("/mraid", (ae<? super ReferenceT>) new d<Object>(bvVar2, this.t, oVar));
        a("/mraidLoaded", (ae<? super ReferenceT>) this.r);
        a("/open", (ae<? super ReferenceT>) new e<Object>(this.f3674a.getContext(), this.f3674a.k(), this.f3674a.y(), tVar, aoj, kVar, mVar, nVar, bvVar2, this.t));
        a("/precache", (ae<? super ReferenceT>) new qc<Object>());
        a("/touch", o.i);
        a("/video", o.l);
        a("/videoMeta", o.m);
        if (ax.B().a(this.f3674a.getContext())) {
            a("/logScionEvent", (ae<? super ReferenceT>) new com.google.android.gms.ads.internal.gmsg.c<Object>(this.f3674a.getContext()));
        }
        if (aiVar != null) {
            a("/setInterstitialProperties", (ae<? super ReferenceT>) new ah<Object>(aiVar));
        }
        this.c = aoj;
        this.d = nVar;
        this.g = kVar;
        this.h = mVar;
        this.q = tVar;
        this.s = bvVar2;
        this.u = oVar;
        this.k = aiVar;
        this.j = z2;
    }

    public final void a(rw rwVar) {
        this.e = rwVar;
    }

    public final void a(rx rxVar) {
        this.f = rxVar;
    }

    public final void a(ry ryVar) {
        this.i = ryVar;
    }

    public final void a(rz rzVar) {
        this.v = rzVar;
    }

    public final void a(sn snVar) {
        this.x = true;
        if (this.f != null) {
            this.f.a();
            this.f = null;
        }
        r();
    }

    public final void a(boolean z2) {
        this.j = z2;
    }

    public final void a(boolean z2, int i2) {
        a(new AdOverlayInfoParcel((!this.f3674a.z() || this.f3674a.t().d()) ? this.c : null, this.d, this.q, this.f3674a, z2, i2, this.f3674a.k()));
    }

    public final void a(boolean z2, int i2, String str) {
        sg sgVar = null;
        boolean z3 = this.f3674a.z();
        aoj aoj = (!z3 || this.f3674a.t().d()) ? this.c : null;
        if (!z3) {
            sgVar = new sg(this.f3674a, this.d);
        }
        a(new AdOverlayInfoParcel(aoj, sgVar, this.g, this.h, this.q, this.f3674a, z2, i2, str, this.f3674a.k()));
    }

    public final void a(boolean z2, int i2, String str, String str2) {
        boolean z3 = this.f3674a.z();
        a(new AdOverlayInfoParcel((!z3 || this.f3674a.t().d()) ? this.c : null, z3 ? null : new sg(this.f3674a, this.d), this.g, this.h, this.q, this.f3674a, z2, i2, str, str2, this.f3674a.k()));
    }

    public final void b(sn snVar) {
        a(snVar.f3692b);
    }

    public final boolean b() {
        boolean z2;
        synchronized (this.f3675b) {
            z2 = this.l;
        }
        return z2;
    }

    public final boolean c() {
        boolean z2;
        synchronized (this.f3675b) {
            z2 = this.m;
        }
        return z2;
    }

    public final boolean c(sn snVar) {
        Uri uri;
        String str = "AdWebView shouldOverrideUrlLoading: ";
        String valueOf = String.valueOf(snVar.f3691a);
        jm.a(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
        Uri uri2 = snVar.f3692b;
        if (a(uri2)) {
            return true;
        }
        if (this.j) {
            String scheme = uri2.getScheme();
            if ("http".equalsIgnoreCase(scheme) || "https".equalsIgnoreCase(scheme)) {
                if (this.c != null) {
                    if (((Boolean) ape.f().a(asi.aj)).booleanValue()) {
                        this.c.e();
                        if (this.w != null) {
                            this.w.a(snVar.f3691a);
                        }
                        this.c = null;
                    }
                }
                return false;
            }
        }
        if (!this.f3674a.getWebView().willNotDraw()) {
            try {
                ahh y2 = this.f3674a.y();
                if (y2 != null && y2.a(uri2)) {
                    uri2 = y2.a(uri2, this.f3674a.getContext(), this.f3674a.getView(), this.f3674a.d());
                }
                uri = uri2;
            } catch (ahi e2) {
                String str2 = "Unable to append parameter to URL: ";
                String valueOf2 = String.valueOf(snVar.f3691a);
                jm.e(valueOf2.length() != 0 ? str2.concat(valueOf2) : new String(str2));
                uri = uri2;
            }
            if (this.s == null || this.s.b()) {
                a(new c("android.intent.action.VIEW", uri.toString(), null, null, null, null, null));
            } else {
                this.s.a(snVar.f3691a);
            }
        } else {
            String str3 = "AdWebView unable to handle URL: ";
            String valueOf3 = String.valueOf(snVar.f3691a);
            jm.e(valueOf3.length() != 0 ? str3.concat(valueOf3) : new String(str3));
        }
        return true;
    }

    public final OnGlobalLayoutListener d() {
        OnGlobalLayoutListener onGlobalLayoutListener;
        synchronized (this.f3675b) {
            onGlobalLayoutListener = this.n;
        }
        return onGlobalLayoutListener;
    }

    public final WebResourceResponse d(sn snVar) {
        String str;
        WebResourceResponse c2;
        if (this.w != null) {
            this.w.a(snVar.f3691a, snVar.c, 1);
        }
        if (!"mraid.js".equalsIgnoreCase(new File(snVar.f3691a).getName())) {
            c2 = null;
        } else {
            n();
            if (this.f3674a.t().d()) {
                str = (String) ape.f().a(asi.K);
            } else if (this.f3674a.z()) {
                str = (String) ape.f().a(asi.J);
            } else {
                str = (String) ape.f().a(asi.I);
            }
            ax.e();
            c2 = jv.c(this.f3674a.getContext(), this.f3674a.k().f3528a, str);
        }
        if (c2 != null) {
            return c2;
        }
        try {
            if (!il.a(snVar.f3691a, this.f3674a.getContext()).equals(snVar.f3691a)) {
                return e(snVar);
            }
            amp a2 = amp.a(snVar.f3691a);
            if (a2 != null) {
                amm a3 = ax.k().a(a2);
                if (a3 != null && a3.a()) {
                    return new WebResourceResponse("", "", a3.b());
                }
            }
            if (ml.c()) {
                if (((Boolean) ape.f().a(asi.bi)).booleanValue()) {
                    return e(snVar);
                }
            }
            return null;
        } catch (Exception | NoClassDefFoundError e2) {
            ax.i().a(e2, "AdWebViewClient.interceptRequest");
            return null;
        }
    }

    public final OnScrollChangedListener e() {
        OnScrollChangedListener onScrollChangedListener;
        synchronized (this.f3675b) {
            onScrollChangedListener = this.o;
        }
        return onScrollChangedListener;
    }

    public final boolean f() {
        boolean z2;
        synchronized (this.f3675b) {
            z2 = this.p;
        }
        return z2;
    }

    public final void g() {
        ic icVar = this.w;
        if (icVar != null) {
            WebView webView = this.f3674a.getWebView();
            if (android.support.v4.i.t.y(webView)) {
                a((View) webView, icVar, 10);
                return;
            }
            q();
            this.A = new sf(this, icVar);
            this.f3674a.getView().addOnAttachStateChangeListener(this.A);
        }
    }

    public final void h() {
        synchronized (this.f3675b) {
            this.p = true;
        }
        this.z++;
        r();
    }

    public final void i() {
        this.z--;
        r();
    }

    public final void j() {
        this.y = true;
        r();
    }

    public final void k() {
        if (this.w != null) {
            this.w.d();
            this.w = null;
        }
        q();
        super.k();
        synchronized (this.f3675b) {
            this.c = null;
            this.d = null;
            this.e = null;
            this.f = null;
            this.g = null;
            this.h = null;
            this.j = false;
            this.l = false;
            this.m = false;
            this.p = false;
            this.q = null;
            this.i = null;
            if (this.t != null) {
                this.t.a(true);
                this.t = null;
            }
        }
    }

    public final rz l() {
        return this.v;
    }

    public final ic m() {
        return this.w;
    }

    public final void n() {
        synchronized (this.f3675b) {
            this.j = false;
            this.l = true;
            nt.f3558a.execute(new sd(this));
        }
    }

    public final /* synthetic */ Object o() {
        return this.f3674a;
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ void p() {
        this.f3674a.F();
        com.google.android.gms.ads.internal.overlay.d r2 = this.f3674a.r();
        if (r2 != null) {
            r2.m();
        }
        if (this.i != null) {
            this.i.a();
            this.i = null;
        }
    }
}
