package com.google.android.gms.internal.ads;

import java.io.IOException;

public final class aoe extends afh<aoe> {

    /* renamed from: a reason: collision with root package name */
    private Integer f2797a;

    /* renamed from: b reason: collision with root package name */
    private aob f2798b;
    private anx c;

    public aoe() {
        this.f2797a = null;
        this.f2798b = null;
        this.c = null;
        this.Y = null;
        this.Z = -1;
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public final aoe a(afd afd) throws IOException {
        while (true) {
            int a2 = afd.a();
            switch (a2) {
                case 0:
                    break;
                case 8:
                    int j = afd.j();
                    try {
                        this.f2797a = Integer.valueOf(anf.a(afd.g()));
                        continue;
                    } catch (IllegalArgumentException e) {
                        afd.e(j);
                        a(afd, a2);
                        break;
                    }
                case 18:
                    if (this.f2798b == null) {
                        this.f2798b = new aob();
                    }
                    afd.a((afn) this.f2798b);
                    continue;
                case 26:
                    if (this.c == null) {
                        this.c = new anx();
                    }
                    afd.a((afn) this.c);
                    continue;
                default:
                    if (!super.a(afd, a2)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    /* access modifiers changed from: protected */
    public final int a() {
        int a2 = super.a();
        if (this.f2797a != null) {
            a2 += aff.b(1, this.f2797a.intValue());
        }
        if (this.f2798b != null) {
            a2 += aff.b(2, (afn) this.f2798b);
        }
        return this.c != null ? a2 + aff.b(3, (afn) this.c) : a2;
    }

    public final void a(aff aff) throws IOException {
        if (this.f2797a != null) {
            aff.a(1, this.f2797a.intValue());
        }
        if (this.f2798b != null) {
            aff.a(2, (afn) this.f2798b);
        }
        if (this.c != null) {
            aff.a(3, (afn) this.c);
        }
        super.a(aff);
    }
}
