package com.google.android.gms.internal.ads;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import com.google.android.gms.common.b.c;
import com.google.android.gms.common.internal.aa;

@cm
public final class art {

    /* renamed from: a reason: collision with root package name */
    private final Context f2862a;

    public art(Context context) {
        aa.a(context, (Object) "Context can not be null");
        this.f2862a = context;
    }

    private final boolean a(Intent intent) {
        aa.a(intent, (Object) "Intent can not be null");
        return !this.f2862a.getPackageManager().queryIntentActivities(intent, 0).isEmpty();
    }

    public final boolean a() {
        Intent intent = new Intent("android.intent.action.DIAL");
        intent.setData(Uri.parse("tel:"));
        return a(intent);
    }

    public final boolean b() {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setData(Uri.parse("sms:"));
        return a(intent);
    }

    public final boolean c() {
        return ((Boolean) ly.a(this.f2862a, new aru())).booleanValue() && c.b(this.f2862a).a("android.permission.WRITE_EXTERNAL_STORAGE") == 0;
    }

    @TargetApi(14)
    public final boolean d() {
        return a(new Intent("android.intent.action.INSERT").setType("vnd.android.cursor.dir/event"));
    }
}
