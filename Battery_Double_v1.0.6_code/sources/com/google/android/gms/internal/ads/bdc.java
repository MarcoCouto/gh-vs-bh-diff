package com.google.android.gms.internal.ads;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;

public final class bdc extends ajk implements bda {
    bdc(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.mediation.client.IMediationResponseMetadata");
    }

    public final int a() throws RemoteException {
        Parcel a2 = a(1, r_());
        int readInt = a2.readInt();
        a2.recycle();
        return readInt;
    }
}
