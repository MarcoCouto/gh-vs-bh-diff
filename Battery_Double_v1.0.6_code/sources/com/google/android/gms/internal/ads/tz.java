package com.google.android.gms.internal.ads;

import java.security.GeneralSecurityException;

public interface tz<P> {
    int a();

    P a(aah aah) throws GeneralSecurityException;

    P a(acw acw) throws GeneralSecurityException;

    acw b(aah aah) throws GeneralSecurityException;

    acw b(acw acw) throws GeneralSecurityException;

    xe c(aah aah) throws GeneralSecurityException;
}
