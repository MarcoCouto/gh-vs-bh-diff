package com.google.android.gms.internal.ads;

import java.io.IOException;

public final class anp extends afh<anp> {

    /* renamed from: a reason: collision with root package name */
    private Integer f2768a;

    /* renamed from: b reason: collision with root package name */
    private int[] f2769b;

    public anp() {
        this.f2768a = null;
        this.f2769b = afq.f2534a;
        this.Y = null;
        this.Z = -1;
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public final anp a(afd afd) throws IOException {
        while (true) {
            int a2 = afd.a();
            switch (a2) {
                case 0:
                    break;
                case 8:
                    int j = afd.j();
                    try {
                        this.f2768a = Integer.valueOf(anf.a(afd.g()));
                        continue;
                    } catch (IllegalArgumentException e) {
                        afd.e(j);
                        a(afd, a2);
                        break;
                    }
                case 16:
                    int a3 = afq.a(afd, 16);
                    int length = this.f2769b == null ? 0 : this.f2769b.length;
                    int[] iArr = new int[(a3 + length)];
                    if (length != 0) {
                        System.arraycopy(this.f2769b, 0, iArr, 0, length);
                    }
                    while (length < iArr.length - 1) {
                        iArr[length] = afd.g();
                        afd.a();
                        length++;
                    }
                    iArr[length] = afd.g();
                    this.f2769b = iArr;
                    continue;
                case 18:
                    int c = afd.c(afd.g());
                    int j2 = afd.j();
                    int i = 0;
                    while (afd.i() > 0) {
                        afd.g();
                        i++;
                    }
                    afd.e(j2);
                    int length2 = this.f2769b == null ? 0 : this.f2769b.length;
                    int[] iArr2 = new int[(i + length2)];
                    if (length2 != 0) {
                        System.arraycopy(this.f2769b, 0, iArr2, 0, length2);
                    }
                    while (length2 < iArr2.length) {
                        iArr2[length2] = afd.g();
                        length2++;
                    }
                    this.f2769b = iArr2;
                    afd.d(c);
                    continue;
                default:
                    if (!super.a(afd, a2)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    /* access modifiers changed from: protected */
    public final int a() {
        int a2 = super.a();
        if (this.f2768a != null) {
            a2 += aff.b(1, this.f2768a.intValue());
        }
        if (this.f2769b == null || this.f2769b.length <= 0) {
            return a2;
        }
        int i = 0;
        for (int a3 : this.f2769b) {
            i += aff.a(a3);
        }
        return a2 + i + (this.f2769b.length * 1);
    }

    public final void a(aff aff) throws IOException {
        if (this.f2768a != null) {
            aff.a(1, this.f2768a.intValue());
        }
        if (this.f2769b != null && this.f2769b.length > 0) {
            for (int a2 : this.f2769b) {
                aff.a(2, a2);
            }
        }
        super.a(aff);
    }
}
