package com.google.android.gms.internal.ads;

import android.provider.Settings.SettingNotFoundException;
import java.lang.reflect.InvocationTargetException;

public final class aim extends ajj {
    public aim(ahy ahy, String str, String str2, zz zzVar, int i, int i2) {
        super(ahy, str, str2, zzVar, i, 49);
    }

    /* access modifiers changed from: protected */
    public final void a() throws IllegalAccessException, InvocationTargetException {
        this.f2637b.F = Integer.valueOf(2);
        try {
            this.f2637b.F = Integer.valueOf(((Boolean) this.c.invoke(null, new Object[]{this.f2636a.a()})).booleanValue() ? 1 : 0);
        } catch (InvocationTargetException e) {
            if (!(e.getTargetException() instanceof SettingNotFoundException)) {
                throw e;
            }
        }
    }
}
