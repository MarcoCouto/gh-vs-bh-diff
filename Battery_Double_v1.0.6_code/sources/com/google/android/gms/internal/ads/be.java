package com.google.android.gms.internal.ads;

import java.util.List;

final class be implements my<List<atm>, ati> {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ String f3164a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ Integer f3165b;
    private final /* synthetic */ Integer c;
    private final /* synthetic */ int d;
    private final /* synthetic */ int e;
    private final /* synthetic */ int f;
    private final /* synthetic */ int g;
    private final /* synthetic */ boolean h;

    be(ay ayVar, String str, Integer num, Integer num2, int i, int i2, int i3, int i4, boolean z) {
        this.f3164a = str;
        this.f3165b = num;
        this.c = num2;
        this.d = i;
        this.e = i2;
        this.f = i3;
        this.g = i4;
        this.h = z;
    }

    public final /* synthetic */ Object a(Object obj) {
        Integer num = null;
        List list = (List) obj;
        if (list == null || list.isEmpty()) {
            return null;
        }
        String str = this.f3164a;
        Integer num2 = this.f3165b;
        Integer num3 = this.c;
        if (this.d > 0) {
            num = Integer.valueOf(this.d);
        }
        return new ati(str, list, num2, num3, num, this.e + this.f, this.g, this.h);
    }
}
