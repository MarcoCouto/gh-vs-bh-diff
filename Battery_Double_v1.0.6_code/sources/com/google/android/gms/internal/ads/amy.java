package com.google.android.gms.internal.ads;

import com.hmatalonga.greenhub.Config;

public final class amy {

    public static final class a extends abp<a, C0067a> implements acy {
        /* access modifiers changed from: private */
        public static final a zzakg = new a();
        private static volatile adi<a> zzakh;

        /* renamed from: com.google.android.gms.internal.ads.amy$a$a reason: collision with other inner class name */
        public static final class C0067a extends com.google.android.gms.internal.ads.abp.a<a, C0067a> implements acy {
            private C0067a() {
                super(a.zzakg);
            }

            /* synthetic */ C0067a(amz amz) {
                this();
            }
        }

        public enum b implements abs {
            UNKNOWN_EVENT_TYPE(0),
            AD_REQUEST(1),
            AD_LOADED(2),
            AD_FAILED_TO_LOAD(3),
            AD_FAILED_TO_LOAD_NO_FILL(4),
            AD_IMPRESSION(5),
            AD_FIRST_CLICK(6),
            AD_SUBSEQUENT_CLICK(7),
            REQUEST_WILL_START(8),
            REQUEST_DID_END(9),
            REQUEST_WILL_UPDATE_SIGNALS(10),
            REQUEST_DID_UPDATE_SIGNALS(11),
            REQUEST_WILL_BUILD_URL(12),
            REQUEST_DID_BUILD_URL(13),
            REQUEST_WILL_MAKE_NETWORK_REQUEST(14),
            REQUEST_DID_RECEIVE_NETWORK_RESPONSE(15),
            REQUEST_WILL_PROCESS_RESPONSE(16),
            REQUEST_DID_PROCESS_RESPONSE(17),
            REQUEST_WILL_RENDER(18),
            REQUEST_DID_RENDER(19),
            REQUEST_WILL_UPDATE_GMS_SIGNALS(1000),
            REQUEST_DID_UPDATE_GMS_SIGNALS(Config.NOTIFICATION_BATTERY_STATUS),
            REQUEST_FAILED_TO_UPDATE_GMS_SIGNALS(Config.NOTIFICATION_BATTERY_FULL),
            REQUEST_FAILED_TO_BUILD_URL(Config.NOTIFICATION_BATTERY_LOW),
            REQUEST_FAILED_TO_MAKE_NETWORK_REQUEST(Config.NOTIFICATION_TEMPERATURE_WARNING),
            REQUEST_FAILED_TO_PROCESS_RESPONSE(Config.NOTIFICATION_TEMPERATURE_HIGH),
            REQUEST_FAILED_TO_UPDATE_SIGNALS(Config.NOTIFICATION_MESSAGE_NEW),
            BANNER_SIZE_INVALID(10000),
            BANNER_SIZE_VALID(10001);
            
            private static final abt<b> D = null;
            private final int E;

            static {
                D = new ana();
            }

            private b(int i) {
                this.E = i;
            }

            public static b a(int i) {
                switch (i) {
                    case 0:
                        return UNKNOWN_EVENT_TYPE;
                    case 1:
                        return AD_REQUEST;
                    case 2:
                        return AD_LOADED;
                    case 3:
                        return AD_FAILED_TO_LOAD;
                    case 4:
                        return AD_FAILED_TO_LOAD_NO_FILL;
                    case 5:
                        return AD_IMPRESSION;
                    case 6:
                        return AD_FIRST_CLICK;
                    case 7:
                        return AD_SUBSEQUENT_CLICK;
                    case 8:
                        return REQUEST_WILL_START;
                    case 9:
                        return REQUEST_DID_END;
                    case 10:
                        return REQUEST_WILL_UPDATE_SIGNALS;
                    case 11:
                        return REQUEST_DID_UPDATE_SIGNALS;
                    case 12:
                        return REQUEST_WILL_BUILD_URL;
                    case 13:
                        return REQUEST_DID_BUILD_URL;
                    case 14:
                        return REQUEST_WILL_MAKE_NETWORK_REQUEST;
                    case 15:
                        return REQUEST_DID_RECEIVE_NETWORK_RESPONSE;
                    case 16:
                        return REQUEST_WILL_PROCESS_RESPONSE;
                    case 17:
                        return REQUEST_DID_PROCESS_RESPONSE;
                    case 18:
                        return REQUEST_WILL_RENDER;
                    case 19:
                        return REQUEST_DID_RENDER;
                    case 1000:
                        return REQUEST_WILL_UPDATE_GMS_SIGNALS;
                    case Config.NOTIFICATION_BATTERY_STATUS /*1001*/:
                        return REQUEST_DID_UPDATE_GMS_SIGNALS;
                    case Config.NOTIFICATION_BATTERY_FULL /*1002*/:
                        return REQUEST_FAILED_TO_UPDATE_GMS_SIGNALS;
                    case Config.NOTIFICATION_BATTERY_LOW /*1003*/:
                        return REQUEST_FAILED_TO_BUILD_URL;
                    case Config.NOTIFICATION_TEMPERATURE_WARNING /*1004*/:
                        return REQUEST_FAILED_TO_MAKE_NETWORK_REQUEST;
                    case Config.NOTIFICATION_TEMPERATURE_HIGH /*1005*/:
                        return REQUEST_FAILED_TO_PROCESS_RESPONSE;
                    case Config.NOTIFICATION_MESSAGE_NEW /*1006*/:
                        return REQUEST_FAILED_TO_UPDATE_SIGNALS;
                    case 10000:
                        return BANNER_SIZE_INVALID;
                    case 10001:
                        return BANNER_SIZE_VALID;
                    default:
                        return null;
                }
            }

            public final int a() {
                return this.E;
            }
        }

        static {
            abp.a(a.class, zzakg);
        }

        private a() {
        }

        /* JADX WARNING: type inference failed for: r0v7, types: [com.google.android.gms.internal.ads.adi<com.google.android.gms.internal.ads.amy$a>] */
        /* JADX WARNING: type inference failed for: r0v8, types: [java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v9, types: [com.google.android.gms.internal.ads.abp$b, com.google.android.gms.internal.ads.adi<com.google.android.gms.internal.ads.amy$a>] */
        /* JADX WARNING: type inference failed for: r0v16 */
        /* access modifiers changed from: protected */
        /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r0v9, types: [com.google.android.gms.internal.ads.abp$b, com.google.android.gms.internal.ads.adi<com.google.android.gms.internal.ads.amy$a>]
  assigns: [com.google.android.gms.internal.ads.abp$b]
  uses: [com.google.android.gms.internal.ads.adi<com.google.android.gms.internal.ads.amy$a>]
  mth insns count: 35
        	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
        	at java.util.ArrayList.forEach(Unknown Source)
        	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
        	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
        	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
        	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
        	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
        	at java.util.ArrayList.forEach(Unknown Source)
        	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
        	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$0(DepthTraversal.java:13)
        	at java.util.ArrayList.forEach(Unknown Source)
        	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:13)
        	at jadx.core.ProcessClass.process(ProcessClass.java:30)
        	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
        	at java.util.ArrayList.forEach(Unknown Source)
        	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
        	at jadx.core.ProcessClass.process(ProcessClass.java:35)
        	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
        	at jadx.api.JavaClass.decompile(JavaClass.java:62)
        	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
         */
        /* JADX WARNING: Unknown variable types count: 2 */
        public final Object a(int i, Object obj, Object obj2) {
            ? r0;
            switch (amz.f2743a[i - 1]) {
                case 1:
                    return new a();
                case 2:
                    return new C0067a(null);
                case 3:
                    return a((acw) zzakg, "\u0001\u0000", (Object[]) null);
                case 4:
                    return zzakg;
                case 5:
                    adi<a> adi = zzakh;
                    if (adi != null) {
                        return adi;
                    }
                    synchronized (a.class) {
                        r0 = zzakh;
                        if (r0 == 0) {
                            ? bVar = new com.google.android.gms.internal.ads.abp.b(zzakg);
                            zzakh = bVar;
                            r0 = bVar;
                        }
                    }
                    return r0;
                case 6:
                    return Byte.valueOf(1);
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }
    }
}
