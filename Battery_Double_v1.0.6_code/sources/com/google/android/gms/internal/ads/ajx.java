package com.google.android.gms.internal.ads;

import android.database.ContentObserver;
import android.os.Handler;

final class ajx extends ContentObserver {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ aju f2649a;

    public ajx(aju aju, Handler handler) {
        this.f2649a = aju;
        super(handler);
    }

    public final void onChange(boolean z) {
        super.onChange(z);
        this.f2649a.a();
    }
}
