package com.google.android.gms.internal.ads;

final /* synthetic */ class bai implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final bah f3067a;

    /* renamed from: b reason: collision with root package name */
    private final ahh f3068b;
    private final bay c;

    bai(bah bah, ahh ahh, bay bay) {
        this.f3067a = bah;
        this.f3068b = ahh;
        this.c = bay;
    }

    public final void run() {
        this.f3067a.a(this.f3068b, this.c);
    }
}
