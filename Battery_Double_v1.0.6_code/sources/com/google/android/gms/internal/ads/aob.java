package com.google.android.gms.internal.ads;

import java.io.IOException;

public final class aob extends afh<aob> {

    /* renamed from: a reason: collision with root package name */
    private Integer f2792a;

    public aob() {
        this.f2792a = null;
        this.Y = null;
        this.Z = -1;
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public final aob a(afd afd) throws IOException {
        while (true) {
            int a2 = afd.a();
            switch (a2) {
                case 0:
                    break;
                case 8:
                    int j = afd.j();
                    try {
                        int g = afd.g();
                        if (g < 0 || g > 3) {
                            throw new IllegalArgumentException(g + " is not a valid enum VideoErrorCode");
                        }
                        this.f2792a = Integer.valueOf(g);
                        continue;
                    } catch (IllegalArgumentException e) {
                        afd.e(j);
                        a(afd, a2);
                        break;
                    }
                default:
                    if (!super.a(afd, a2)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    /* access modifiers changed from: protected */
    public final int a() {
        int a2 = super.a();
        return this.f2792a != null ? a2 + aff.b(1, this.f2792a.intValue()) : a2;
    }

    public final void a(aff aff) throws IOException {
        if (this.f2792a != null) {
            aff.a(1, this.f2792a.intValue());
        }
        super.a(aff);
    }
}
