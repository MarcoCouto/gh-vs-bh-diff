package com.google.android.gms.internal.ads;

import android.app.KeyguardManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.graphics.Rect;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.PowerManager;
import android.provider.Settings.System;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.ViewTreeObserver.OnScrollChangedListener;
import android.view.WindowManager;
import com.google.android.gms.ads.internal.ax;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.UUID;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@cm
public final class aju implements OnGlobalLayoutListener, OnScrollChangedListener {

    /* renamed from: a reason: collision with root package name */
    protected final ajs f2645a;

    /* renamed from: b reason: collision with root package name */
    private final Object f2646b = new Object();
    private final WeakReference<ir> c;
    private WeakReference<ViewTreeObserver> d;
    private final alg e;
    private final Context f;
    private final WindowManager g;
    private final PowerManager h;
    private final KeyguardManager i;
    private final DisplayMetrics j;
    private akc k;
    private boolean l;
    private boolean m = false;
    private boolean n = false;
    private boolean o;
    private boolean p;
    private boolean q;
    private BroadcastReceiver r;
    private final HashSet<ajr> s = new HashSet<>();
    private lw t;
    private final HashSet<akq> u = new HashSet<>();
    private final Rect v = new Rect();
    private final ajx w;
    private float x;

    public aju(Context context, aot aot, ir irVar, mu muVar, alg alg) {
        this.c = new WeakReference<>(irVar);
        this.e = alg;
        this.d = new WeakReference<>(null);
        this.o = true;
        this.q = false;
        this.t = new lw(200);
        this.f2645a = new ajs(UUID.randomUUID().toString(), muVar, aot.f2814a, irVar.k, irVar.a(), aot.h);
        this.g = (WindowManager) context.getSystemService("window");
        this.h = (PowerManager) context.getApplicationContext().getSystemService("power");
        this.i = (KeyguardManager) context.getSystemService("keyguard");
        this.f = context;
        this.w = new ajx(this, new Handler());
        this.f.getContentResolver().registerContentObserver(System.CONTENT_URI, true, this.w);
        this.j = context.getResources().getDisplayMetrics();
        Display defaultDisplay = this.g.getDefaultDisplay();
        this.v.right = defaultDisplay.getWidth();
        this.v.bottom = defaultDisplay.getHeight();
        a();
    }

    private static int a(int i2, DisplayMetrics displayMetrics) {
        return (int) (((float) i2) / displayMetrics.density);
    }

    private final JSONObject a(View view, Boolean bool) throws JSONException {
        if (view == null) {
            return i().put("isAttachedToWindow", false).put("isScreenOn", j()).put("isVisible", false);
        }
        boolean a2 = ax.g().a(view);
        int[] iArr = new int[2];
        int[] iArr2 = new int[2];
        try {
            view.getLocationOnScreen(iArr);
            view.getLocationInWindow(iArr2);
        } catch (Exception e2) {
            jm.b("Failure getting view location.", e2);
        }
        Rect rect = new Rect();
        rect.left = iArr[0];
        rect.top = iArr[1];
        rect.right = rect.left + view.getWidth();
        rect.bottom = rect.top + view.getHeight();
        Rect rect2 = new Rect();
        boolean globalVisibleRect = view.getGlobalVisibleRect(rect2, null);
        Rect rect3 = new Rect();
        boolean localVisibleRect = view.getLocalVisibleRect(rect3);
        Rect rect4 = new Rect();
        view.getHitRect(rect4);
        JSONObject i2 = i();
        i2.put("windowVisibility", view.getWindowVisibility()).put("isAttachedToWindow", a2).put("viewBox", new JSONObject().put("top", a(this.v.top, this.j)).put("bottom", a(this.v.bottom, this.j)).put("left", a(this.v.left, this.j)).put("right", a(this.v.right, this.j))).put("adBox", new JSONObject().put("top", a(rect.top, this.j)).put("bottom", a(rect.bottom, this.j)).put("left", a(rect.left, this.j)).put("right", a(rect.right, this.j))).put("globalVisibleBox", new JSONObject().put("top", a(rect2.top, this.j)).put("bottom", a(rect2.bottom, this.j)).put("left", a(rect2.left, this.j)).put("right", a(rect2.right, this.j))).put("globalVisibleBoxVisible", globalVisibleRect).put("localVisibleBox", new JSONObject().put("top", a(rect3.top, this.j)).put("bottom", a(rect3.bottom, this.j)).put("left", a(rect3.left, this.j)).put("right", a(rect3.right, this.j))).put("localVisibleBoxVisible", localVisibleRect).put("hitBox", new JSONObject().put("top", a(rect4.top, this.j)).put("bottom", a(rect4.bottom, this.j)).put("left", a(rect4.left, this.j)).put("right", a(rect4.right, this.j))).put("screenDensity", (double) this.j.density);
        if (bool == null) {
            bool = Boolean.valueOf(ax.e().a(view, this.h, this.i));
        }
        i2.put("isVisible", bool.booleanValue());
        return i2;
    }

    private static JSONObject a(JSONObject jSONObject) throws JSONException {
        JSONArray jSONArray = new JSONArray();
        JSONObject jSONObject2 = new JSONObject();
        jSONArray.put(jSONObject);
        jSONObject2.put("units", jSONArray);
        return jSONObject2;
    }

    private final void a(JSONObject jSONObject, boolean z) {
        try {
            JSONObject a2 = a(jSONObject);
            ArrayList arrayList = new ArrayList(this.u);
            int size = arrayList.size();
            int i2 = 0;
            while (i2 < size) {
                Object obj = arrayList.get(i2);
                i2++;
                ((akq) obj).a(a2, z);
            }
        } catch (Throwable th) {
            jm.b("Skipping active view message.", th);
        }
    }

    private final void g() {
        if (this.k != null) {
            this.k.a(this);
        }
    }

    private final void h() {
        ViewTreeObserver viewTreeObserver = (ViewTreeObserver) this.d.get();
        if (viewTreeObserver != null && viewTreeObserver.isAlive()) {
            viewTreeObserver.removeOnScrollChangedListener(this);
            viewTreeObserver.removeGlobalOnLayoutListener(this);
        }
    }

    private final JSONObject i() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("afmaVersion", this.f2645a.b()).put("activeViewJSON", this.f2645a.c()).put("timestamp", ax.l().b()).put("adFormat", this.f2645a.a()).put("hashCode", this.f2645a.d()).put("isMraid", this.f2645a.e()).put("isStopped", this.n).put("isPaused", this.m).put("isNative", this.f2645a.f()).put("isScreenOn", j()).put("appMuted", ax.D().b()).put("appVolume", (double) ax.D().a()).put("deviceVolume", (double) this.x);
        return jSONObject;
    }

    private final boolean j() {
        return VERSION.SDK_INT >= 20 ? this.h.isInteractive() : this.h.isScreenOn();
    }

    public final void a() {
        this.x = kn.a(this.f);
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:68:?, code lost:
        return;
     */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    public final void a(int i2) {
        boolean z;
        synchronized (this.f2646b) {
            Iterator it = this.u.iterator();
            while (true) {
                if (it.hasNext()) {
                    if (((akq) it.next()).a()) {
                        z = true;
                        break;
                    }
                } else {
                    z = false;
                    break;
                }
            }
            if (z && this.o) {
                View a2 = this.e.a();
                boolean z2 = a2 != null && ax.e().a(a2, this.h, this.i);
                boolean z3 = a2 != null && z2 && a2.getGlobalVisibleRect(new Rect(), null);
                if (this.e.b()) {
                    b();
                    return;
                }
                if (i2 == 1) {
                    if (!this.t.a() && z3 == this.q) {
                        return;
                    }
                }
                if (z3 || this.q || i2 != 1) {
                    try {
                        a(a(a2, Boolean.valueOf(z2)), false);
                        this.q = z3;
                    } catch (RuntimeException | JSONException e2) {
                        jm.a("Active view update failed.", e2);
                    }
                    View a3 = this.e.c().a();
                    if (a3 != null) {
                        ViewTreeObserver viewTreeObserver = (ViewTreeObserver) this.d.get();
                        ViewTreeObserver viewTreeObserver2 = a3.getViewTreeObserver();
                        if (viewTreeObserver2 != viewTreeObserver) {
                            h();
                            if (!this.l || (viewTreeObserver != null && viewTreeObserver.isAlive())) {
                                this.l = true;
                                viewTreeObserver2.addOnScrollChangedListener(this);
                                viewTreeObserver2.addOnGlobalLayoutListener(this);
                            }
                            this.d = new WeakReference<>(viewTreeObserver2);
                        }
                    }
                    g();
                }
            }
        }
    }

    public final void a(akc akc) {
        synchronized (this.f2646b) {
            this.k = akc;
        }
    }

    public final void a(akq akq) {
        if (this.u.isEmpty()) {
            synchronized (this.f2646b) {
                if (this.r == null) {
                    IntentFilter intentFilter = new IntentFilter();
                    intentFilter.addAction("android.intent.action.SCREEN_ON");
                    intentFilter.addAction("android.intent.action.SCREEN_OFF");
                    this.r = new ajv(this);
                    ax.E().a(this.f, this.r, intentFilter);
                }
            }
            a(3);
        }
        this.u.add(akq);
        try {
            akq.a(a(a(this.e.a(), (Boolean) null)), false);
        } catch (JSONException e2) {
            jm.b("Skipping measurement update for new client.", e2);
        }
    }

    /* access modifiers changed from: 0000 */
    public final void a(akq akq, Map<String, String> map) {
        String str = "Received request to untrack: ";
        String valueOf = String.valueOf(this.f2645a.d());
        jm.b(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
        b(akq);
    }

    /* access modifiers changed from: 0000 */
    public final boolean a(Map<String, String> map) {
        if (map == null) {
            return false;
        }
        String str = (String) map.get("hashCode");
        return !TextUtils.isEmpty(str) && str.equals(this.f2645a.d());
    }

    public final void b() {
        synchronized (this.f2646b) {
            if (this.o) {
                this.p = true;
                try {
                    JSONObject i2 = i();
                    i2.put("doneReasonCode", "u");
                    a(i2, true);
                } catch (JSONException e2) {
                    jm.b("JSON failure while processing active view data.", e2);
                } catch (RuntimeException e3) {
                    jm.b("Failure while processing active view data.", e3);
                }
                String str = "Untracking ad unit: ";
                String valueOf = String.valueOf(this.f2645a.d());
                jm.b(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
            }
        }
    }

    public final void b(akq akq) {
        this.u.remove(akq);
        akq.b();
        if (this.u.isEmpty()) {
            synchronized (this.f2646b) {
                h();
                synchronized (this.f2646b) {
                    if (this.r != null) {
                        try {
                            ax.E().a(this.f, this.r);
                        } catch (IllegalStateException e2) {
                            jm.b("Failed trying to unregister the receiver", e2);
                        } catch (Exception e3) {
                            ax.i().a((Throwable) e3, "ActiveViewUnit.stopScreenStatusMonitoring");
                        }
                        this.r = null;
                    }
                }
                this.f.getContentResolver().unregisterContentObserver(this.w);
                this.o = false;
                g();
                ArrayList arrayList = new ArrayList(this.u);
                int size = arrayList.size();
                int i2 = 0;
                while (i2 < size) {
                    Object obj = arrayList.get(i2);
                    i2++;
                    b((akq) obj);
                }
            }
            return;
        }
        return;
    }

    /* access modifiers changed from: 0000 */
    public final void b(Map<String, String> map) {
        a(3);
    }

    /* access modifiers changed from: 0000 */
    public final void c(Map<String, String> map) {
        if (map.containsKey("isVisible")) {
            boolean z = "1".equals(map.get("isVisible")) || "true".equals(map.get("isVisible"));
            Iterator it = this.s.iterator();
            while (it.hasNext()) {
                ((ajr) it.next()).a(this, z);
            }
        }
    }

    public final boolean c() {
        boolean z;
        synchronized (this.f2646b) {
            z = this.o;
        }
        return z;
    }

    public final void d() {
        synchronized (this.f2646b) {
            this.n = true;
            a(3);
        }
    }

    public final void e() {
        synchronized (this.f2646b) {
            this.m = true;
            a(3);
        }
    }

    public final void f() {
        synchronized (this.f2646b) {
            this.m = false;
            a(3);
        }
    }

    public final void onGlobalLayout() {
        a(2);
    }

    public final void onScrollChanged() {
        a(1);
    }
}
