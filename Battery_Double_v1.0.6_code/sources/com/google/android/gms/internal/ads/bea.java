package com.google.android.gms.internal.ads;

import com.google.ads.a.C0043a;
import com.google.ads.a.b;

final /* synthetic */ class bea {

    /* renamed from: a reason: collision with root package name */
    static final /* synthetic */ int[] f3166a = new int[C0043a.values().length];

    /* renamed from: b reason: collision with root package name */
    private static final /* synthetic */ int[] f3167b = new int[b.values().length];

    static {
        try {
            f3166a[C0043a.INTERNAL_ERROR.ordinal()] = 1;
        } catch (NoSuchFieldError e) {
        }
        try {
            f3166a[C0043a.INVALID_REQUEST.ordinal()] = 2;
        } catch (NoSuchFieldError e2) {
        }
        try {
            f3166a[C0043a.NETWORK_ERROR.ordinal()] = 3;
        } catch (NoSuchFieldError e3) {
        }
        try {
            f3166a[C0043a.NO_FILL.ordinal()] = 4;
        } catch (NoSuchFieldError e4) {
        }
        try {
            f3167b[b.FEMALE.ordinal()] = 1;
        } catch (NoSuchFieldError e5) {
        }
        try {
            f3167b[b.MALE.ordinal()] = 2;
        } catch (NoSuchFieldError e6) {
        }
        try {
            f3167b[b.UNKNOWN.ordinal()] = 3;
        } catch (NoSuchFieldError e7) {
        }
    }
}
