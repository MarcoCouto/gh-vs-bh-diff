package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.ax;
import com.google.android.gms.ads.internal.m;

final class ayz {

    /* renamed from: a reason: collision with root package name */
    m f3018a;

    /* renamed from: b reason: collision with root package name */
    aop f3019b;
    axs c;
    long d;
    boolean e;
    boolean f;
    private final /* synthetic */ ayy g;

    ayz(ayy ayy, axr axr) {
        this.g = ayy;
        this.f3018a = axr.b(ayy.c);
        this.c = new axs();
        axs axs = this.c;
        m mVar = this.f3018a;
        mVar.a((apk) new axt(axs));
        mVar.a((aqe) new ayc(axs));
        mVar.a((atc) new aye(axs));
        mVar.a((aph) new ayg(axs));
        mVar.a((gn) new ayi(axs));
    }

    ayz(ayy ayy, axr axr, aop aop) {
        this(ayy, axr);
        this.f3019b = aop;
    }

    /* access modifiers changed from: 0000 */
    public final boolean a() {
        if (this.e) {
            return false;
        }
        this.f = this.f3018a.b(ayw.b(this.f3019b != null ? this.f3019b : this.g.f3017b));
        this.e = true;
        this.d = ax.l().a();
        return true;
    }
}
