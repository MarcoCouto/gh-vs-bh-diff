package com.google.android.gms.internal.ads;

import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import java.lang.ref.WeakReference;

final class bo implements OnGlobalLayoutListener {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ WeakReference f3197a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ bi f3198b;

    bo(bi biVar, WeakReference weakReference) {
        this.f3198b = biVar;
        this.f3197a = weakReference;
    }

    public final void onGlobalLayout() {
        this.f3198b.a(this.f3197a, false);
    }
}
