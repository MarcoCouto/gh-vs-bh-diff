package com.google.android.gms.internal.ads;

import java.io.IOException;
import java.util.List;
import java.util.Map;

final class aat implements ado {

    /* renamed from: a reason: collision with root package name */
    private final aaq f2402a;

    /* renamed from: b reason: collision with root package name */
    private int f2403b;
    private int c;
    private int d = 0;

    private aat(aaq aaq) {
        this.f2402a = (aaq) abr.a(aaq, "input");
        this.f2402a.c = this;
    }

    public static aat a(aaq aaq) {
        return aaq.c != null ? aaq.c : new aat(aaq);
    }

    private final Object a(aew aew, Class<?> cls, abc abc) throws IOException {
        switch (aau.f2404a[aew.ordinal()]) {
            case 1:
                return Boolean.valueOf(k());
            case 2:
                return n();
            case 3:
                return Double.valueOf(d());
            case 4:
                return Integer.valueOf(p());
            case 5:
                return Integer.valueOf(j());
            case 6:
                return Long.valueOf(i());
            case 7:
                return Float.valueOf(e());
            case 8:
                return Integer.valueOf(h());
            case 9:
                return Long.valueOf(g());
            case 10:
                a(2);
                return c(adj.a().a(cls), abc);
            case 11:
                return Integer.valueOf(q());
            case 12:
                return Long.valueOf(r());
            case 13:
                return Integer.valueOf(s());
            case 14:
                return Long.valueOf(t());
            case 15:
                return m();
            case 16:
                return Integer.valueOf(o());
            case 17:
                return Long.valueOf(f());
            default:
                throw new RuntimeException("unsupported field type.");
        }
    }

    private final void a(int i) throws IOException {
        if ((this.f2403b & 7) != i) {
            throw abv.f();
        }
    }

    private final void a(List<String> list, boolean z) throws IOException {
        int a2;
        int a3;
        if ((this.f2403b & 7) != 2) {
            throw abv.f();
        } else if (!(list instanceof acf) || z) {
            do {
                list.add(z ? m() : l());
                if (!this.f2402a.t()) {
                    a2 = this.f2402a.a();
                } else {
                    return;
                }
            } while (a2 == this.f2403b);
            this.d = a2;
        } else {
            acf acf = (acf) list;
            do {
                acf.a(n());
                if (!this.f2402a.t()) {
                    a3 = this.f2402a.a();
                } else {
                    return;
                }
            } while (a3 == this.f2403b);
            this.d = a3;
        }
    }

    private static void b(int i) throws IOException {
        if ((i & 7) != 0) {
            throw abv.g();
        }
    }

    private final <T> T c(adp<T> adp, abc abc) throws IOException {
        int m = this.f2402a.m();
        if (this.f2402a.f2400a >= this.f2402a.f2401b) {
            throw new abv("Protocol message had too many levels of nesting.  May be malicious.  Use CodedInputStream.setRecursionLimit() to increase the depth limit.");
        }
        int c2 = this.f2402a.c(m);
        T a2 = adp.a();
        this.f2402a.f2400a++;
        adp.a(a2, this, abc);
        adp.c(a2);
        this.f2402a.a(0);
        this.f2402a.f2400a--;
        this.f2402a.d(c2);
        return a2;
    }

    private static void c(int i) throws IOException {
        if ((i & 3) != 0) {
            throw abv.g();
        }
    }

    private final <T> T d(adp<T> adp, abc abc) throws IOException {
        int i = this.c;
        this.c = ((this.f2403b >>> 3) << 3) | 4;
        try {
            T a2 = adp.a();
            adp.a(a2, this, abc);
            adp.c(a2);
            if (this.f2403b == this.c) {
                return a2;
            }
            throw abv.g();
        } finally {
            this.c = i;
        }
    }

    private final void d(int i) throws IOException {
        if (this.f2402a.u() != i) {
            throw abv.a();
        }
    }

    public final int a() throws IOException {
        if (this.d != 0) {
            this.f2403b = this.d;
            this.d = 0;
        } else {
            this.f2403b = this.f2402a.a();
        }
        if (this.f2403b == 0 || this.f2403b == this.c) {
            return Integer.MAX_VALUE;
        }
        return this.f2403b >>> 3;
    }

    public final <T> T a(adp<T> adp, abc abc) throws IOException {
        a(2);
        return c(adp, abc);
    }

    public final void a(List<Double> list) throws IOException {
        int a2;
        int a3;
        if (list instanceof aay) {
            aay aay = (aay) list;
            switch (this.f2403b & 7) {
                case 1:
                    break;
                case 2:
                    int m = this.f2402a.m();
                    b(m);
                    int u = m + this.f2402a.u();
                    do {
                        aay.a(this.f2402a.b());
                    } while (this.f2402a.u() < u);
                    return;
                default:
                    throw abv.f();
            }
            do {
                aay.a(this.f2402a.b());
                if (!this.f2402a.t()) {
                    a3 = this.f2402a.a();
                } else {
                    return;
                }
            } while (a3 == this.f2403b);
            this.d = a3;
            return;
        }
        switch (this.f2403b & 7) {
            case 1:
                break;
            case 2:
                int m2 = this.f2402a.m();
                b(m2);
                int u2 = m2 + this.f2402a.u();
                do {
                    list.add(Double.valueOf(this.f2402a.b()));
                } while (this.f2402a.u() < u2);
                return;
            default:
                throw abv.f();
        }
        do {
            list.add(Double.valueOf(this.f2402a.b()));
            if (!this.f2402a.t()) {
                a2 = this.f2402a.a();
            } else {
                return;
            }
        } while (a2 == this.f2403b);
        this.d = a2;
    }

    public final <T> void a(List<T> list, adp<T> adp, abc abc) throws IOException {
        int a2;
        if ((this.f2403b & 7) != 2) {
            throw abv.f();
        }
        int i = this.f2403b;
        do {
            list.add(c(adp, abc));
            if (!this.f2402a.t() && this.d == 0) {
                a2 = this.f2402a.a();
            } else {
                return;
            }
        } while (a2 == i);
        this.d = a2;
    }

    public final <K, V> void a(Map<K, V> map, acp<K, V> acp, abc abc) throws IOException {
        a(2);
        int c2 = this.f2402a.c(this.f2402a.m());
        K k = acp.f2462b;
        V v = acp.d;
        while (true) {
            try {
                int a2 = a();
                if (a2 != Integer.MAX_VALUE && !this.f2402a.t()) {
                    switch (a2) {
                        case 1:
                            k = a(acp.f2461a, null, (abc) null);
                            break;
                        case 2:
                            v = a(acp.c, acp.d.getClass(), abc);
                            break;
                        default:
                            if (c()) {
                                break;
                            } else {
                                throw new abv("Unable to parse map entry.");
                            }
                    }
                } else {
                    map.put(k, v);
                    this.f2402a.d(c2);
                    return;
                }
            } catch (abw e) {
                if (!c()) {
                    throw new abv("Unable to parse map entry.");
                }
            } catch (Throwable th) {
                this.f2402a.d(c2);
                throw th;
            }
        }
    }

    public final int b() {
        return this.f2403b;
    }

    public final <T> T b(adp<T> adp, abc abc) throws IOException {
        a(3);
        return d(adp, abc);
    }

    public final void b(List<Float> list) throws IOException {
        int a2;
        int a3;
        if (list instanceof abn) {
            abn abn = (abn) list;
            switch (this.f2403b & 7) {
                case 2:
                    int m = this.f2402a.m();
                    c(m);
                    int u = m + this.f2402a.u();
                    do {
                        abn.a(this.f2402a.c());
                    } while (this.f2402a.u() < u);
                    return;
                case 5:
                    break;
                default:
                    throw abv.f();
            }
            do {
                abn.a(this.f2402a.c());
                if (!this.f2402a.t()) {
                    a3 = this.f2402a.a();
                } else {
                    return;
                }
            } while (a3 == this.f2403b);
            this.d = a3;
            return;
        }
        switch (this.f2403b & 7) {
            case 2:
                int m2 = this.f2402a.m();
                c(m2);
                int u2 = m2 + this.f2402a.u();
                do {
                    list.add(Float.valueOf(this.f2402a.c()));
                } while (this.f2402a.u() < u2);
                return;
            case 5:
                break;
            default:
                throw abv.f();
        }
        do {
            list.add(Float.valueOf(this.f2402a.c()));
            if (!this.f2402a.t()) {
                a2 = this.f2402a.a();
            } else {
                return;
            }
        } while (a2 == this.f2403b);
        this.d = a2;
    }

    public final <T> void b(List<T> list, adp<T> adp, abc abc) throws IOException {
        int a2;
        if ((this.f2403b & 7) != 3) {
            throw abv.f();
        }
        int i = this.f2403b;
        do {
            list.add(d(adp, abc));
            if (!this.f2402a.t() && this.d == 0) {
                a2 = this.f2402a.a();
            } else {
                return;
            }
        } while (a2 == i);
        this.d = a2;
    }

    public final void c(List<Long> list) throws IOException {
        int a2;
        int a3;
        if (list instanceof ack) {
            ack ack = (ack) list;
            switch (this.f2403b & 7) {
                case 0:
                    break;
                case 2:
                    int m = this.f2402a.m() + this.f2402a.u();
                    do {
                        ack.a(this.f2402a.d());
                    } while (this.f2402a.u() < m);
                    d(m);
                    return;
                default:
                    throw abv.f();
            }
            do {
                ack.a(this.f2402a.d());
                if (!this.f2402a.t()) {
                    a3 = this.f2402a.a();
                } else {
                    return;
                }
            } while (a3 == this.f2403b);
            this.d = a3;
            return;
        }
        switch (this.f2403b & 7) {
            case 0:
                break;
            case 2:
                int m2 = this.f2402a.m() + this.f2402a.u();
                do {
                    list.add(Long.valueOf(this.f2402a.d()));
                } while (this.f2402a.u() < m2);
                d(m2);
                return;
            default:
                throw abv.f();
        }
        do {
            list.add(Long.valueOf(this.f2402a.d()));
            if (!this.f2402a.t()) {
                a2 = this.f2402a.a();
            } else {
                return;
            }
        } while (a2 == this.f2403b);
        this.d = a2;
    }

    public final boolean c() throws IOException {
        if (this.f2402a.t() || this.f2403b == this.c) {
            return false;
        }
        return this.f2402a.b(this.f2403b);
    }

    public final double d() throws IOException {
        a(1);
        return this.f2402a.b();
    }

    public final void d(List<Long> list) throws IOException {
        int a2;
        int a3;
        if (list instanceof ack) {
            ack ack = (ack) list;
            switch (this.f2403b & 7) {
                case 0:
                    break;
                case 2:
                    int m = this.f2402a.m() + this.f2402a.u();
                    do {
                        ack.a(this.f2402a.e());
                    } while (this.f2402a.u() < m);
                    d(m);
                    return;
                default:
                    throw abv.f();
            }
            do {
                ack.a(this.f2402a.e());
                if (!this.f2402a.t()) {
                    a3 = this.f2402a.a();
                } else {
                    return;
                }
            } while (a3 == this.f2403b);
            this.d = a3;
            return;
        }
        switch (this.f2403b & 7) {
            case 0:
                break;
            case 2:
                int m2 = this.f2402a.m() + this.f2402a.u();
                do {
                    list.add(Long.valueOf(this.f2402a.e()));
                } while (this.f2402a.u() < m2);
                d(m2);
                return;
            default:
                throw abv.f();
        }
        do {
            list.add(Long.valueOf(this.f2402a.e()));
            if (!this.f2402a.t()) {
                a2 = this.f2402a.a();
            } else {
                return;
            }
        } while (a2 == this.f2403b);
        this.d = a2;
    }

    public final float e() throws IOException {
        a(5);
        return this.f2402a.c();
    }

    public final void e(List<Integer> list) throws IOException {
        int a2;
        int a3;
        if (list instanceof abq) {
            abq abq = (abq) list;
            switch (this.f2403b & 7) {
                case 0:
                    break;
                case 2:
                    int m = this.f2402a.m() + this.f2402a.u();
                    do {
                        abq.c(this.f2402a.f());
                    } while (this.f2402a.u() < m);
                    d(m);
                    return;
                default:
                    throw abv.f();
            }
            do {
                abq.c(this.f2402a.f());
                if (!this.f2402a.t()) {
                    a3 = this.f2402a.a();
                } else {
                    return;
                }
            } while (a3 == this.f2403b);
            this.d = a3;
            return;
        }
        switch (this.f2403b & 7) {
            case 0:
                break;
            case 2:
                int m2 = this.f2402a.m() + this.f2402a.u();
                do {
                    list.add(Integer.valueOf(this.f2402a.f()));
                } while (this.f2402a.u() < m2);
                d(m2);
                return;
            default:
                throw abv.f();
        }
        do {
            list.add(Integer.valueOf(this.f2402a.f()));
            if (!this.f2402a.t()) {
                a2 = this.f2402a.a();
            } else {
                return;
            }
        } while (a2 == this.f2403b);
        this.d = a2;
    }

    public final long f() throws IOException {
        a(0);
        return this.f2402a.d();
    }

    public final void f(List<Long> list) throws IOException {
        int a2;
        int a3;
        if (list instanceof ack) {
            ack ack = (ack) list;
            switch (this.f2403b & 7) {
                case 1:
                    break;
                case 2:
                    int m = this.f2402a.m();
                    b(m);
                    int u = m + this.f2402a.u();
                    do {
                        ack.a(this.f2402a.g());
                    } while (this.f2402a.u() < u);
                    return;
                default:
                    throw abv.f();
            }
            do {
                ack.a(this.f2402a.g());
                if (!this.f2402a.t()) {
                    a3 = this.f2402a.a();
                } else {
                    return;
                }
            } while (a3 == this.f2403b);
            this.d = a3;
            return;
        }
        switch (this.f2403b & 7) {
            case 1:
                break;
            case 2:
                int m2 = this.f2402a.m();
                b(m2);
                int u2 = m2 + this.f2402a.u();
                do {
                    list.add(Long.valueOf(this.f2402a.g()));
                } while (this.f2402a.u() < u2);
                return;
            default:
                throw abv.f();
        }
        do {
            list.add(Long.valueOf(this.f2402a.g()));
            if (!this.f2402a.t()) {
                a2 = this.f2402a.a();
            } else {
                return;
            }
        } while (a2 == this.f2403b);
        this.d = a2;
    }

    public final long g() throws IOException {
        a(0);
        return this.f2402a.e();
    }

    public final void g(List<Integer> list) throws IOException {
        int a2;
        int a3;
        if (list instanceof abq) {
            abq abq = (abq) list;
            switch (this.f2403b & 7) {
                case 2:
                    int m = this.f2402a.m();
                    c(m);
                    int u = m + this.f2402a.u();
                    do {
                        abq.c(this.f2402a.h());
                    } while (this.f2402a.u() < u);
                    return;
                case 5:
                    break;
                default:
                    throw abv.f();
            }
            do {
                abq.c(this.f2402a.h());
                if (!this.f2402a.t()) {
                    a3 = this.f2402a.a();
                } else {
                    return;
                }
            } while (a3 == this.f2403b);
            this.d = a3;
            return;
        }
        switch (this.f2403b & 7) {
            case 2:
                int m2 = this.f2402a.m();
                c(m2);
                int u2 = m2 + this.f2402a.u();
                do {
                    list.add(Integer.valueOf(this.f2402a.h()));
                } while (this.f2402a.u() < u2);
                return;
            case 5:
                break;
            default:
                throw abv.f();
        }
        do {
            list.add(Integer.valueOf(this.f2402a.h()));
            if (!this.f2402a.t()) {
                a2 = this.f2402a.a();
            } else {
                return;
            }
        } while (a2 == this.f2403b);
        this.d = a2;
    }

    public final int h() throws IOException {
        a(0);
        return this.f2402a.f();
    }

    public final void h(List<Boolean> list) throws IOException {
        int a2;
        int a3;
        if (list instanceof aaf) {
            aaf aaf = (aaf) list;
            switch (this.f2403b & 7) {
                case 0:
                    break;
                case 2:
                    int m = this.f2402a.m() + this.f2402a.u();
                    do {
                        aaf.a(this.f2402a.i());
                    } while (this.f2402a.u() < m);
                    d(m);
                    return;
                default:
                    throw abv.f();
            }
            do {
                aaf.a(this.f2402a.i());
                if (!this.f2402a.t()) {
                    a3 = this.f2402a.a();
                } else {
                    return;
                }
            } while (a3 == this.f2403b);
            this.d = a3;
            return;
        }
        switch (this.f2403b & 7) {
            case 0:
                break;
            case 2:
                int m2 = this.f2402a.m() + this.f2402a.u();
                do {
                    list.add(Boolean.valueOf(this.f2402a.i()));
                } while (this.f2402a.u() < m2);
                d(m2);
                return;
            default:
                throw abv.f();
        }
        do {
            list.add(Boolean.valueOf(this.f2402a.i()));
            if (!this.f2402a.t()) {
                a2 = this.f2402a.a();
            } else {
                return;
            }
        } while (a2 == this.f2403b);
        this.d = a2;
    }

    public final long i() throws IOException {
        a(1);
        return this.f2402a.g();
    }

    public final void i(List<String> list) throws IOException {
        a(list, false);
    }

    public final int j() throws IOException {
        a(5);
        return this.f2402a.h();
    }

    public final void j(List<String> list) throws IOException {
        a(list, true);
    }

    public final void k(List<aah> list) throws IOException {
        int a2;
        if ((this.f2403b & 7) != 2) {
            throw abv.f();
        }
        do {
            list.add(n());
            if (!this.f2402a.t()) {
                a2 = this.f2402a.a();
            } else {
                return;
            }
        } while (a2 == this.f2403b);
        this.d = a2;
    }

    public final boolean k() throws IOException {
        a(0);
        return this.f2402a.i();
    }

    public final String l() throws IOException {
        a(2);
        return this.f2402a.j();
    }

    public final void l(List<Integer> list) throws IOException {
        int a2;
        int a3;
        if (list instanceof abq) {
            abq abq = (abq) list;
            switch (this.f2403b & 7) {
                case 0:
                    break;
                case 2:
                    int m = this.f2402a.m() + this.f2402a.u();
                    do {
                        abq.c(this.f2402a.m());
                    } while (this.f2402a.u() < m);
                    d(m);
                    return;
                default:
                    throw abv.f();
            }
            do {
                abq.c(this.f2402a.m());
                if (!this.f2402a.t()) {
                    a3 = this.f2402a.a();
                } else {
                    return;
                }
            } while (a3 == this.f2403b);
            this.d = a3;
            return;
        }
        switch (this.f2403b & 7) {
            case 0:
                break;
            case 2:
                int m2 = this.f2402a.m() + this.f2402a.u();
                do {
                    list.add(Integer.valueOf(this.f2402a.m()));
                } while (this.f2402a.u() < m2);
                d(m2);
                return;
            default:
                throw abv.f();
        }
        do {
            list.add(Integer.valueOf(this.f2402a.m()));
            if (!this.f2402a.t()) {
                a2 = this.f2402a.a();
            } else {
                return;
            }
        } while (a2 == this.f2403b);
        this.d = a2;
    }

    public final String m() throws IOException {
        a(2);
        return this.f2402a.k();
    }

    public final void m(List<Integer> list) throws IOException {
        int a2;
        int a3;
        if (list instanceof abq) {
            abq abq = (abq) list;
            switch (this.f2403b & 7) {
                case 0:
                    break;
                case 2:
                    int m = this.f2402a.m() + this.f2402a.u();
                    do {
                        abq.c(this.f2402a.n());
                    } while (this.f2402a.u() < m);
                    d(m);
                    return;
                default:
                    throw abv.f();
            }
            do {
                abq.c(this.f2402a.n());
                if (!this.f2402a.t()) {
                    a3 = this.f2402a.a();
                } else {
                    return;
                }
            } while (a3 == this.f2403b);
            this.d = a3;
            return;
        }
        switch (this.f2403b & 7) {
            case 0:
                break;
            case 2:
                int m2 = this.f2402a.m() + this.f2402a.u();
                do {
                    list.add(Integer.valueOf(this.f2402a.n()));
                } while (this.f2402a.u() < m2);
                d(m2);
                return;
            default:
                throw abv.f();
        }
        do {
            list.add(Integer.valueOf(this.f2402a.n()));
            if (!this.f2402a.t()) {
                a2 = this.f2402a.a();
            } else {
                return;
            }
        } while (a2 == this.f2403b);
        this.d = a2;
    }

    public final aah n() throws IOException {
        a(2);
        return this.f2402a.l();
    }

    public final void n(List<Integer> list) throws IOException {
        int a2;
        int a3;
        if (list instanceof abq) {
            abq abq = (abq) list;
            switch (this.f2403b & 7) {
                case 2:
                    int m = this.f2402a.m();
                    c(m);
                    int u = m + this.f2402a.u();
                    do {
                        abq.c(this.f2402a.o());
                    } while (this.f2402a.u() < u);
                    return;
                case 5:
                    break;
                default:
                    throw abv.f();
            }
            do {
                abq.c(this.f2402a.o());
                if (!this.f2402a.t()) {
                    a3 = this.f2402a.a();
                } else {
                    return;
                }
            } while (a3 == this.f2403b);
            this.d = a3;
            return;
        }
        switch (this.f2403b & 7) {
            case 2:
                int m2 = this.f2402a.m();
                c(m2);
                int u2 = m2 + this.f2402a.u();
                do {
                    list.add(Integer.valueOf(this.f2402a.o()));
                } while (this.f2402a.u() < u2);
                return;
            case 5:
                break;
            default:
                throw abv.f();
        }
        do {
            list.add(Integer.valueOf(this.f2402a.o()));
            if (!this.f2402a.t()) {
                a2 = this.f2402a.a();
            } else {
                return;
            }
        } while (a2 == this.f2403b);
        this.d = a2;
    }

    public final int o() throws IOException {
        a(0);
        return this.f2402a.m();
    }

    public final void o(List<Long> list) throws IOException {
        int a2;
        int a3;
        if (list instanceof ack) {
            ack ack = (ack) list;
            switch (this.f2403b & 7) {
                case 1:
                    break;
                case 2:
                    int m = this.f2402a.m();
                    b(m);
                    int u = m + this.f2402a.u();
                    do {
                        ack.a(this.f2402a.p());
                    } while (this.f2402a.u() < u);
                    return;
                default:
                    throw abv.f();
            }
            do {
                ack.a(this.f2402a.p());
                if (!this.f2402a.t()) {
                    a3 = this.f2402a.a();
                } else {
                    return;
                }
            } while (a3 == this.f2403b);
            this.d = a3;
            return;
        }
        switch (this.f2403b & 7) {
            case 1:
                break;
            case 2:
                int m2 = this.f2402a.m();
                b(m2);
                int u2 = m2 + this.f2402a.u();
                do {
                    list.add(Long.valueOf(this.f2402a.p()));
                } while (this.f2402a.u() < u2);
                return;
            default:
                throw abv.f();
        }
        do {
            list.add(Long.valueOf(this.f2402a.p()));
            if (!this.f2402a.t()) {
                a2 = this.f2402a.a();
            } else {
                return;
            }
        } while (a2 == this.f2403b);
        this.d = a2;
    }

    public final int p() throws IOException {
        a(0);
        return this.f2402a.n();
    }

    public final void p(List<Integer> list) throws IOException {
        int a2;
        int a3;
        if (list instanceof abq) {
            abq abq = (abq) list;
            switch (this.f2403b & 7) {
                case 0:
                    break;
                case 2:
                    int m = this.f2402a.m() + this.f2402a.u();
                    do {
                        abq.c(this.f2402a.q());
                    } while (this.f2402a.u() < m);
                    d(m);
                    return;
                default:
                    throw abv.f();
            }
            do {
                abq.c(this.f2402a.q());
                if (!this.f2402a.t()) {
                    a3 = this.f2402a.a();
                } else {
                    return;
                }
            } while (a3 == this.f2403b);
            this.d = a3;
            return;
        }
        switch (this.f2403b & 7) {
            case 0:
                break;
            case 2:
                int m2 = this.f2402a.m() + this.f2402a.u();
                do {
                    list.add(Integer.valueOf(this.f2402a.q()));
                } while (this.f2402a.u() < m2);
                d(m2);
                return;
            default:
                throw abv.f();
        }
        do {
            list.add(Integer.valueOf(this.f2402a.q()));
            if (!this.f2402a.t()) {
                a2 = this.f2402a.a();
            } else {
                return;
            }
        } while (a2 == this.f2403b);
        this.d = a2;
    }

    public final int q() throws IOException {
        a(5);
        return this.f2402a.o();
    }

    public final void q(List<Long> list) throws IOException {
        int a2;
        int a3;
        if (list instanceof ack) {
            ack ack = (ack) list;
            switch (this.f2403b & 7) {
                case 0:
                    break;
                case 2:
                    int m = this.f2402a.m() + this.f2402a.u();
                    do {
                        ack.a(this.f2402a.r());
                    } while (this.f2402a.u() < m);
                    d(m);
                    return;
                default:
                    throw abv.f();
            }
            do {
                ack.a(this.f2402a.r());
                if (!this.f2402a.t()) {
                    a3 = this.f2402a.a();
                } else {
                    return;
                }
            } while (a3 == this.f2403b);
            this.d = a3;
            return;
        }
        switch (this.f2403b & 7) {
            case 0:
                break;
            case 2:
                int m2 = this.f2402a.m() + this.f2402a.u();
                do {
                    list.add(Long.valueOf(this.f2402a.r()));
                } while (this.f2402a.u() < m2);
                d(m2);
                return;
            default:
                throw abv.f();
        }
        do {
            list.add(Long.valueOf(this.f2402a.r()));
            if (!this.f2402a.t()) {
                a2 = this.f2402a.a();
            } else {
                return;
            }
        } while (a2 == this.f2403b);
        this.d = a2;
    }

    public final long r() throws IOException {
        a(1);
        return this.f2402a.p();
    }

    public final int s() throws IOException {
        a(0);
        return this.f2402a.q();
    }

    public final long t() throws IOException {
        a(0);
        return this.f2402a.r();
    }
}
