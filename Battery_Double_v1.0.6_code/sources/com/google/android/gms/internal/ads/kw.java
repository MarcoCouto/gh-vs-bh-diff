package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.net.Uri.Builder;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.ax;
import com.hmatalonga.greenhub.Config;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import org.json.JSONException;
import org.json.JSONObject;

@cm
public final class kw {

    /* renamed from: a reason: collision with root package name */
    private final Object f3463a = new Object();

    /* renamed from: b reason: collision with root package name */
    private String f3464b = "";
    private String c = "";
    private boolean d = false;
    private String e = "";

    private final String a(Context context) {
        String str;
        synchronized (this.f3463a) {
            if (TextUtils.isEmpty(this.f3464b)) {
                ax.e();
                this.f3464b = jv.c(context, "debug_signals_id.txt");
                if (TextUtils.isEmpty(this.f3464b)) {
                    ax.e();
                    this.f3464b = jv.a();
                    ax.e();
                    jv.b(context, "debug_signals_id.txt", this.f3464b);
                }
            }
            str = this.f3464b;
        }
        return str;
    }

    private final void a(Context context, String str, boolean z, boolean z2) {
        if (!(context instanceof Activity)) {
            jm.d("Can not create dialog without Activity Context");
        } else {
            jv.f3440a.post(new kx(this, context, str, z, z2));
        }
    }

    private final boolean b(Context context, String str, String str2) {
        String d2 = d(context, c(context, (String) ape.f().a(asi.cT), str, str2).toString(), str2);
        if (TextUtils.isEmpty(d2)) {
            jm.b("Not linked for in app preview.");
            return false;
        }
        try {
            JSONObject jSONObject = new JSONObject(d2.trim());
            String optString = jSONObject.optString("gct");
            this.e = jSONObject.optString("status");
            synchronized (this.f3463a) {
                this.c = optString;
            }
            return true;
        } catch (JSONException e2) {
            jm.c("Fail to get in app preview response json.", e2);
            return false;
        }
    }

    private final Uri c(Context context, String str, String str2, String str3) {
        Builder buildUpon = Uri.parse(str).buildUpon();
        buildUpon.appendQueryParameter("linkedDeviceId", a(context));
        buildUpon.appendQueryParameter("adSlotPath", str2);
        buildUpon.appendQueryParameter("afmaVersion", str3);
        return buildUpon.build();
    }

    private final boolean c(Context context, String str, String str2) {
        String d2 = d(context, c(context, (String) ape.f().a(asi.cU), str, str2).toString(), str2);
        if (TextUtils.isEmpty(d2)) {
            jm.b("Not linked for debug signals.");
            return false;
        }
        try {
            boolean equals = "1".equals(new JSONObject(d2.trim()).optString("debug_mode"));
            synchronized (this.f3463a) {
                this.d = equals;
            }
            return equals;
        } catch (JSONException e2) {
            jm.c("Fail to get debug mode response json.", e2);
            return false;
        }
    }

    private static String d(Context context, String str, String str2) {
        HashMap hashMap = new HashMap();
        hashMap.put("User-Agent", ax.e().b(context, str2));
        nn a2 = new lf(context).a(str, (Map<String, String>) hashMap);
        try {
            return (String) a2.get((long) ((Integer) ape.f().a(asi.cW)).intValue(), TimeUnit.MILLISECONDS);
        } catch (TimeoutException e2) {
            TimeoutException timeoutException = e2;
            String str3 = "Timeout while retriving a response from: ";
            String valueOf = String.valueOf(str);
            jm.b(valueOf.length() != 0 ? str3.concat(valueOf) : new String(str3), timeoutException);
            a2.cancel(true);
        } catch (InterruptedException e3) {
            InterruptedException interruptedException = e3;
            String str4 = "Interrupted while retriving a response from: ";
            String valueOf2 = String.valueOf(str);
            jm.b(valueOf2.length() != 0 ? str4.concat(valueOf2) : new String(str4), interruptedException);
            a2.cancel(true);
        } catch (Exception e4) {
            Exception exc = e4;
            String str5 = "Error retriving a response from: ";
            String valueOf3 = String.valueOf(str);
            jm.b(valueOf3.length() != 0 ? str5.concat(valueOf3) : new String(str5), exc);
        }
        return null;
    }

    private final void e(Context context, String str, String str2) {
        ax.e();
        jv.a(context, c(context, (String) ape.f().a(asi.cS), str, str2));
    }

    public final String a() {
        String str;
        synchronized (this.f3463a) {
            str = this.c;
        }
        return str;
    }

    public final void a(Context context, String str, String str2) {
        if (!b(context, str, str2)) {
            a(context, "In-app preview failed to load because of a system error. Please try again later.", true, true);
        } else if ("2".equals(this.e)) {
            jm.b("Creative is not pushed for this device.");
            a(context, "There was no creative pushed from DFP to the device.", false, false);
        } else if ("1".equals(this.e)) {
            jm.b("The app is not linked for creative preview.");
            e(context, str, str2);
        } else if (Config.NOTIFICATION_DEFAULT_PRIORITY.equals(this.e)) {
            jm.b("Device is linked for in app preview.");
            a(context, "The device is successfully linked for creative preview.", false, true);
        }
    }

    public final void a(Context context, String str, String str2, String str3) {
        boolean b2 = b();
        if (c(context, str, str2)) {
            if (!b2 && !TextUtils.isEmpty(str3)) {
                b(context, str2, str3, str);
            }
            jm.b("Device is linked for debug signals.");
            a(context, "The device is successfully linked for troubleshooting.", false, true);
            return;
        }
        e(context, str, str2);
    }

    public final void b(Context context, String str, String str2, String str3) {
        Builder buildUpon = c(context, (String) ape.f().a(asi.cV), str3, str).buildUpon();
        buildUpon.appendQueryParameter("debugData", str2);
        ax.e();
        jv.a(context, str, buildUpon.build().toString());
    }

    public final boolean b() {
        boolean z;
        synchronized (this.f3463a) {
            z = this.d;
        }
        return z;
    }
}
