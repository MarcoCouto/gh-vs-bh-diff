package com.google.android.gms.internal.ads;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.concurrent.ExecutionException;

public final class aio extends ajj {
    private static volatile agh d = null;
    private static final Object e = new Object();
    private wv f = null;

    public aio(ahy ahy, String str, String str2, zz zzVar, int i, int i2, wv wvVar) {
        super(ahy, str, str2, zzVar, i, 27);
        this.f = wvVar;
    }

    private final String c() {
        try {
            if (this.f2636a.l() != null) {
                this.f2636a.l().get();
            }
            zz k = this.f2636a.k();
            if (!(k == null || k.n == null)) {
                return k.n;
            }
        } catch (InterruptedException | ExecutionException e2) {
        }
        return null;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00fc, code lost:
        if (r0 != false) goto L_0x003b;
     */
    public final void a() throws IllegalAccessException, InvocationTargetException {
        boolean z;
        char c = 3;
        if (d == null || aig.b(d.f2560a) || d.f2560a.equals("E") || d.f2560a.equals("0000000000000000000000000000000000000000000000000000000000000000")) {
            synchronized (e) {
                wv wvVar = this.f;
                if (!aig.b(null)) {
                    c = 4;
                } else {
                    wv wvVar2 = this.f;
                    aig.b(null);
                    if (Boolean.valueOf(false).booleanValue()) {
                        if (this.f2636a.i()) {
                            if (((Boolean) ape.f().a(asi.bO)).booleanValue()) {
                                if (((Boolean) ape.f().a(asi.bP)).booleanValue()) {
                                    z = true;
                                }
                            }
                        }
                        z = false;
                    }
                    c = 2;
                }
                Method method = this.c;
                Object[] objArr = new Object[3];
                objArr[0] = this.f2636a.a();
                objArr[1] = Boolean.valueOf(c == 2);
                objArr[2] = ape.f().a(asi.bI);
                agh agh = new agh((String) method.invoke(null, objArr));
                d = agh;
                if (aig.b(agh.f2560a) || d.f2560a.equals("E")) {
                    switch (c) {
                        case 3:
                            String c2 = c();
                            if (!aig.b(c2)) {
                                d.f2560a = c2;
                                break;
                            }
                            break;
                        case 4:
                            d.f2560a = null.f3781a;
                            break;
                    }
                }
            }
        }
        synchronized (this.f2637b) {
            if (d != null) {
                this.f2637b.n = d.f2560a;
                this.f2637b.t = Long.valueOf(d.f2561b);
                this.f2637b.s = d.c;
                this.f2637b.C = d.d;
                this.f2637b.D = d.e;
            }
        }
    }
}
