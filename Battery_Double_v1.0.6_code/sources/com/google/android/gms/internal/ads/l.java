package com.google.android.gms.internal.ads;

public final class l {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public boolean f3469a;
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public boolean f3470b;
    /* access modifiers changed from: private */
    public boolean c;
    /* access modifiers changed from: private */
    public boolean d;
    /* access modifiers changed from: private */
    public boolean e;

    public final l a(boolean z) {
        this.f3469a = z;
        return this;
    }

    public final l b(boolean z) {
        this.f3470b = z;
        return this;
    }

    public final l c(boolean z) {
        this.c = z;
        return this;
    }

    public final l d(boolean z) {
        this.d = z;
        return this;
    }

    public final l e(boolean z) {
        this.e = true;
        return this;
    }
}
