package com.google.android.gms.internal.ads;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;

public final class aqm extends ajk implements aqk {
    aqm(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.client.ICorrelationIdProvider");
    }

    public final long a() throws RemoteException {
        Parcel a2 = a(1, r_());
        long readLong = a2.readLong();
        a2.recycle();
        return readLong;
    }
}
