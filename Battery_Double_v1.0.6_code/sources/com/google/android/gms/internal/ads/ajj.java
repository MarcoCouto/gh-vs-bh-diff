package com.google.android.gms.internal.ads;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.concurrent.Callable;

public abstract class ajj implements Callable {

    /* renamed from: a reason: collision with root package name */
    protected final ahy f2636a;

    /* renamed from: b reason: collision with root package name */
    protected final zz f2637b;
    protected Method c;
    private final String d = getClass().getSimpleName();
    private final String e;
    private final String f;
    private final int g;
    private final int h;

    public ajj(ahy ahy, String str, String str2, zz zzVar, int i, int i2) {
        this.f2636a = ahy;
        this.e = str;
        this.f = str2;
        this.f2637b = zzVar;
        this.g = i;
        this.h = i2;
    }

    /* access modifiers changed from: protected */
    public abstract void a() throws IllegalAccessException, InvocationTargetException;

    /* renamed from: b */
    public Void call() throws Exception {
        try {
            long nanoTime = System.nanoTime();
            this.c = this.f2636a.a(this.e, this.f);
            if (this.c != null) {
                a();
                ahb h2 = this.f2636a.h();
                if (!(h2 == null || this.g == Integer.MIN_VALUE)) {
                    h2.a(this.h, this.g, (System.nanoTime() - nanoTime) / 1000);
                }
            }
        } catch (IllegalAccessException | InvocationTargetException e2) {
        }
        return null;
    }
}
