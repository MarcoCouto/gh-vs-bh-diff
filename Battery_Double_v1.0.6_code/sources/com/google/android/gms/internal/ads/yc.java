package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.abp.b;
import com.google.android.gms.internal.ads.abp.e;
import java.util.List;

public final class yc extends abp<yc, a> implements acy {
    private static volatile adi<yc> zzakh;
    /* access modifiers changed from: private */
    public static final yc zzdmt = new yc();
    private int zzdlq;
    private String zzdmr = "";
    private abu<xl> zzdms = m();

    public static final class a extends com.google.android.gms.internal.ads.abp.a<yc, a> implements acy {
        private a() {
            super(yc.zzdmt);
        }

        /* synthetic */ a(yd ydVar) {
            this();
        }

        public final a a(xl xlVar) {
            b();
            ((yc) this.f2433a).a(xlVar);
            return this;
        }

        public final a a(String str) {
            b();
            ((yc) this.f2433a).a(str);
            return this;
        }
    }

    static {
        abp.a(yc.class, zzdmt);
    }

    private yc() {
    }

    /* access modifiers changed from: private */
    public final void a(xl xlVar) {
        if (xlVar == null) {
            throw new NullPointerException();
        }
        if (!this.zzdms.a()) {
            abu<xl> abu = this.zzdms;
            int size = abu.size();
            this.zzdms = abu.a(size == 0 ? 10 : size << 1);
        }
        this.zzdms.add(xlVar);
    }

    /* access modifiers changed from: private */
    public final void a(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.zzdmr = str;
    }

    public static a b() {
        return (a) ((com.google.android.gms.internal.ads.abp.a) zzdmt.a(e.e, (Object) null, (Object) null));
    }

    /* JADX WARNING: type inference failed for: r0v6, types: [com.google.android.gms.internal.ads.adi<com.google.android.gms.internal.ads.yc>] */
    /* JADX WARNING: type inference failed for: r0v7, types: [java.lang.Object] */
    /* JADX WARNING: type inference failed for: r0v8, types: [com.google.android.gms.internal.ads.abp$b, com.google.android.gms.internal.ads.adi<com.google.android.gms.internal.ads.yc>] */
    /* JADX WARNING: type inference failed for: r0v16 */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r0v8, types: [com.google.android.gms.internal.ads.abp$b, com.google.android.gms.internal.ads.adi<com.google.android.gms.internal.ads.yc>]
  assigns: [com.google.android.gms.internal.ads.abp$b]
  uses: [com.google.android.gms.internal.ads.adi<com.google.android.gms.internal.ads.yc>]
  mth insns count: 44
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
    	at jadx.core.ProcessClass.process(ProcessClass.java:35)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Unknown variable types count: 2 */
    public final Object a(int i, Object obj, Object obj2) {
        ? r0;
        switch (yd.f3787a[i - 1]) {
            case 1:
                return new yc();
            case 2:
                return new a(null);
            case 3:
                Object[] objArr = {"zzdlq", "zzdmr", "zzdms", xl.class};
                return a((acw) zzdmt, "\u0000\u0002\u0000\u0001\u0001\u0002\u0002\u0003\u0000\u0001\u0000\u0001Ȉ\u0002\u001b", objArr);
            case 4:
                return zzdmt;
            case 5:
                adi<yc> adi = zzakh;
                if (adi != null) {
                    return adi;
                }
                synchronized (yc.class) {
                    r0 = zzakh;
                    if (r0 == 0) {
                        ? bVar = new b(zzdmt);
                        zzakh = bVar;
                        r0 = bVar;
                    }
                }
                return r0;
            case 6:
                return Byte.valueOf(1);
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    public final List<xl> a() {
        return this.zzdms;
    }
}
