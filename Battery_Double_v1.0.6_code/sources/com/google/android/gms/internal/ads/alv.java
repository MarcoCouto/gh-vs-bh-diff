package com.google.android.gms.internal.ads;

import java.util.Comparator;

final class alv implements Comparator<amb> {
    alv(alu alu) {
    }

    public final /* synthetic */ int compare(Object obj, Object obj2) {
        amb amb = (amb) obj;
        amb amb2 = (amb) obj2;
        int i = amb.c - amb2.c;
        return i != 0 ? i : (int) (amb.f2721a - amb2.f2721a);
    }
}
