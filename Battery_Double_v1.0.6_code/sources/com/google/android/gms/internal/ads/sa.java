package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.MutableContextWrapper;

@cm
public final class sa extends MutableContextWrapper {

    /* renamed from: a reason: collision with root package name */
    private Activity f3670a;

    /* renamed from: b reason: collision with root package name */
    private Context f3671b;
    private Context c;

    public sa(Context context) {
        super(context);
        setBaseContext(context);
    }

    public final Activity a() {
        return this.f3670a;
    }

    public final Context b() {
        return this.c;
    }

    public final Object getSystemService(String str) {
        return this.c.getSystemService(str);
    }

    public final void setBaseContext(Context context) {
        this.f3671b = context.getApplicationContext();
        this.f3670a = context instanceof Activity ? (Activity) context : null;
        this.c = context;
        super.setBaseContext(this.f3671b);
    }

    public final void startActivity(Intent intent) {
        if (this.f3670a != null) {
            this.f3670a.startActivity(intent);
            return;
        }
        intent.setFlags(268435456);
        this.f3671b.startActivity(intent);
    }
}
