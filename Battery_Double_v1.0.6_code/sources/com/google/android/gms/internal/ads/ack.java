package com.google.android.gms.internal.ads;

import java.util.Arrays;
import java.util.Collection;
import java.util.RandomAccess;

final class ack extends aab<Long> implements abu<Long>, RandomAccess {

    /* renamed from: a reason: collision with root package name */
    private static final ack f2456a;

    /* renamed from: b reason: collision with root package name */
    private long[] f2457b;
    private int c;

    static {
        ack ack = new ack();
        f2456a = ack;
        ack.b();
    }

    ack() {
        this(new long[10], 0);
    }

    private ack(long[] jArr, int i) {
        this.f2457b = jArr;
        this.c = i;
    }

    private final void a(int i, long j) {
        c();
        if (i < 0 || i > this.c) {
            throw new IndexOutOfBoundsException(d(i));
        }
        if (this.c < this.f2457b.length) {
            System.arraycopy(this.f2457b, i, this.f2457b, i + 1, this.c - i);
        } else {
            long[] jArr = new long[(((this.c * 3) / 2) + 1)];
            System.arraycopy(this.f2457b, 0, jArr, 0, i);
            System.arraycopy(this.f2457b, i, jArr, i + 1, this.c - i);
            this.f2457b = jArr;
        }
        this.f2457b[i] = j;
        this.c++;
        this.modCount++;
    }

    private final void c(int i) {
        if (i < 0 || i >= this.c) {
            throw new IndexOutOfBoundsException(d(i));
        }
    }

    private final String d(int i) {
        return "Index:" + i + ", Size:" + this.c;
    }

    public final /* synthetic */ abu a(int i) {
        if (i >= this.c) {
            return new ack(Arrays.copyOf(this.f2457b, i), this.c);
        }
        throw new IllegalArgumentException();
    }

    public final void a(long j) {
        a(this.c, j);
    }

    public final /* synthetic */ void add(int i, Object obj) {
        a(i, ((Long) obj).longValue());
    }

    public final boolean addAll(Collection<? extends Long> collection) {
        c();
        abr.a(collection);
        if (!(collection instanceof ack)) {
            return super.addAll(collection);
        }
        ack ack = (ack) collection;
        if (ack.c == 0) {
            return false;
        }
        if (Integer.MAX_VALUE - this.c < ack.c) {
            throw new OutOfMemoryError();
        }
        int i = this.c + ack.c;
        if (i > this.f2457b.length) {
            this.f2457b = Arrays.copyOf(this.f2457b, i);
        }
        System.arraycopy(ack.f2457b, 0, this.f2457b, this.c, ack.c);
        this.c = i;
        this.modCount++;
        return true;
    }

    public final long b(int i) {
        c(i);
        return this.f2457b[i];
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ack)) {
            return super.equals(obj);
        }
        ack ack = (ack) obj;
        if (this.c != ack.c) {
            return false;
        }
        long[] jArr = ack.f2457b;
        for (int i = 0; i < this.c; i++) {
            if (this.f2457b[i] != jArr[i]) {
                return false;
            }
        }
        return true;
    }

    public final /* synthetic */ Object get(int i) {
        return Long.valueOf(b(i));
    }

    public final int hashCode() {
        int i = 1;
        for (int i2 = 0; i2 < this.c; i2++) {
            i = (i * 31) + abr.a(this.f2457b[i2]);
        }
        return i;
    }

    public final /* synthetic */ Object remove(int i) {
        c();
        c(i);
        long j = this.f2457b[i];
        if (i < this.c - 1) {
            System.arraycopy(this.f2457b, i + 1, this.f2457b, i, this.c - i);
        }
        this.c--;
        this.modCount++;
        return Long.valueOf(j);
    }

    public final boolean remove(Object obj) {
        c();
        for (int i = 0; i < this.c; i++) {
            if (obj.equals(Long.valueOf(this.f2457b[i]))) {
                System.arraycopy(this.f2457b, i + 1, this.f2457b, i, this.c - i);
                this.c--;
                this.modCount++;
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public final void removeRange(int i, int i2) {
        c();
        if (i2 < i) {
            throw new IndexOutOfBoundsException("toIndex < fromIndex");
        }
        System.arraycopy(this.f2457b, i2, this.f2457b, i, this.c - i2);
        this.c -= i2 - i;
        this.modCount++;
    }

    public final /* synthetic */ Object set(int i, Object obj) {
        long longValue = ((Long) obj).longValue();
        c();
        c(i);
        long j = this.f2457b[i];
        this.f2457b[i] = longValue;
        return Long.valueOf(j);
    }

    public final int size() {
        return this.c;
    }
}
