package com.google.android.gms.internal.ads;

final class pc implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ boolean f3593a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ oy f3594b;

    pc(oy oyVar, boolean z) {
        this.f3594b = oyVar;
        this.f3593a = z;
    }

    public final void run() {
        this.f3594b.a("windowVisibilityChanged", "isVisible", String.valueOf(this.f3593a));
    }
}
