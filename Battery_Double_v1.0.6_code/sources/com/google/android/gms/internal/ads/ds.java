package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.a.b;
import java.util.ArrayList;

public final class ds implements Creator<dr> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b2 = b.b(parcel);
        boolean z = false;
        ArrayList arrayList = null;
        while (parcel.dataPosition() < b2) {
            int a2 = b.a(parcel);
            switch (b.a(a2)) {
                case 2:
                    z = b.c(parcel, a2);
                    break;
                case 3:
                    arrayList = b.q(parcel, a2);
                    break;
                default:
                    b.b(parcel, a2);
                    break;
            }
        }
        b.r(parcel, b2);
        return new dr(z, arrayList);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new dr[i];
    }
}
