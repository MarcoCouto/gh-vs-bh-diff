package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.gmsg.ae;
import java.lang.ref.WeakReference;

final class auf {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public final WeakReference<qn> f2925a;
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public String f2926b;

    public auf(qn qnVar) {
        this.f2925a = new WeakReference<>(qnVar);
    }

    public final void a(bq bqVar) {
        bqVar.a("/loadHtml", (ae<? super T>) new aug<Object>(this, bqVar));
        bqVar.a("/showOverlay", (ae<? super T>) new aui<Object>(this, bqVar));
        bqVar.a("/hideOverlay", (ae<? super T>) new auj<Object>(this, bqVar));
        qn qnVar = (qn) this.f2925a.get();
        if (qnVar != null) {
            qnVar.a("/sendMessageToSdk", (ae<? super qn>) new auk<Object>(this, bqVar));
        }
    }
}
