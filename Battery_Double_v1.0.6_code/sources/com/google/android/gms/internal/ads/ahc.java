package com.google.android.gms.internal.ads;

final class ahc implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ ahb f2582a;

    ahc(ahb ahb) {
        this.f2582a = ahb;
    }

    public final void run() {
        if (this.f2582a.f2581b == null) {
            synchronized (ahb.d) {
                if (this.f2582a.f2581b == null) {
                    boolean booleanValue = ((Boolean) ape.f().a(asi.bC)).booleanValue();
                    if (booleanValue) {
                        try {
                            ahb.f2580a = new anb(this.f2582a.c.f2607a, "ADSHIELD", null);
                        } catch (Throwable th) {
                            booleanValue = false;
                        }
                    }
                    this.f2582a.f2581b = Boolean.valueOf(booleanValue);
                    ahb.d.open();
                }
            }
        }
    }
}
