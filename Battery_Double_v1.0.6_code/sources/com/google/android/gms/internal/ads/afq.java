package com.google.android.gms.internal.ads;

import java.io.IOException;

public final class afq {

    /* renamed from: a reason: collision with root package name */
    public static final int[] f2534a = new int[0];

    /* renamed from: b reason: collision with root package name */
    public static final long[] f2535b = new long[0];
    public static final String[] c = new String[0];
    public static final byte[][] d = new byte[0][];
    public static final byte[] e = new byte[0];
    private static final int f = 11;
    private static final int g = 12;
    private static final int h = 16;
    private static final int i = 26;
    private static final float[] j = new float[0];
    private static final double[] k = new double[0];
    private static final boolean[] l = new boolean[0];

    public static final int a(afd afd, int i2) throws IOException {
        int i3 = 1;
        int j2 = afd.j();
        afd.b(i2);
        while (afd.a() == i2) {
            afd.b(i2);
            i3++;
        }
        afd.b(j2, i2);
        return i3;
    }
}
