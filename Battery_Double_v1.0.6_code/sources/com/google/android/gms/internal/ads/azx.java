package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.ads.internal.ax;
import com.google.android.gms.ads.internal.bu;
import com.google.android.gms.ads.internal.gmsg.ae;
import com.google.android.gms.common.util.o;
import java.util.Map;
import org.json.JSONObject;

@cm
public final class azx implements azp, azv {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public final qn f3047a;

    /* renamed from: b reason: collision with root package name */
    private final Context f3048b;

    public azx(Context context, mu muVar, ahh ahh, bu buVar) throws qy {
        this.f3048b = context;
        ax.f();
        this.f3047a = qu.a(context, sb.a(), "", false, false, ahh, muVar, null, null, null, amw.a());
        this.f3047a.getView().setWillNotDraw(true);
    }

    private static void a(Runnable runnable) {
        ape.a();
        if (mh.b()) {
            runnable.run();
        } else {
            jv.f3440a.post(runnable);
        }
    }

    public final void a() {
        this.f3047a.destroy();
    }

    public final void a(azw azw) {
        rv v = this.f3047a.v();
        azw.getClass();
        v.a(baa.a(azw));
    }

    public final void a(String str) {
        a((Runnable) new bac(this, String.format("<!DOCTYPE html><html><head><script src=\"%s\"></script></head><body></body></html>", new Object[]{str})));
    }

    public final void a(String str, ae<? super bbe> aeVar) {
        this.f3047a.a(str, (ae<? super qn>) new baf<Object>(this, aeVar));
    }

    public final void a(String str, String str2) {
        azq.a((azp) this, str, str2);
    }

    public final void a(String str, Map map) {
        azq.a((azp) this, str, map);
    }

    public final void a(String str, JSONObject jSONObject) {
        azq.b(this, str, jSONObject);
    }

    public final bbf b() {
        return new bbg(this);
    }

    public final void b(String str) {
        a((Runnable) new azy(this, str));
    }

    public final void b(String str, ae<? super bbe> aeVar) {
        this.f3047a.a(str, (o<ae<? super qn>>) new azz<ae<? super qn>>(aeVar));
    }

    public final void b(String str, JSONObject jSONObject) {
        azq.a((azp) this, str, jSONObject);
    }

    public final void c(String str) {
        a((Runnable) new bad(this, str));
    }

    public final void d(String str) {
        a((Runnable) new bae(this, str));
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ void e(String str) {
        this.f3047a.b(str);
    }
}
