package com.google.android.gms.internal.ads;

import android.util.Base64OutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

final class alw {

    /* renamed from: a reason: collision with root package name */
    private ByteArrayOutputStream f2716a = new ByteArrayOutputStream(4096);

    /* renamed from: b reason: collision with root package name */
    private Base64OutputStream f2717b = new Base64OutputStream(this.f2716a, 10);

    public final void a(byte[] bArr) throws IOException {
        this.f2717b.write(bArr);
    }

    public final String toString() {
        String str;
        try {
            this.f2717b.close();
        } catch (IOException e) {
            jm.b("HashManager: Unable to convert to Base64.", e);
        }
        try {
            this.f2716a.close();
            str = this.f2716a.toString();
        } catch (IOException e2) {
            jm.b("HashManager: Unable to convert to Base64.", e2);
            str = "";
        } finally {
            this.f2716a = null;
            this.f2717b = null;
        }
        return str;
    }
}
