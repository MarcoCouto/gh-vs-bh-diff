package com.google.android.gms.internal.ads;

import java.util.Iterator;

final class adw {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public static final Iterator<Object> f2493a = new adx();

    /* renamed from: b reason: collision with root package name */
    private static final Iterable<Object> f2494b = new ady();

    static <T> Iterable<T> a() {
        return f2494b;
    }
}
