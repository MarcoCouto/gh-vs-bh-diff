package com.google.android.gms.internal.ads;

import java.util.Arrays;
import java.util.Collection;
import java.util.RandomAccess;

final class abq extends aab<Integer> implements abu<Integer>, RandomAccess {

    /* renamed from: a reason: collision with root package name */
    private static final abq f2438a;

    /* renamed from: b reason: collision with root package name */
    private int[] f2439b;
    private int c;

    static {
        abq abq = new abq();
        f2438a = abq;
        abq.b();
    }

    abq() {
        this(new int[10], 0);
    }

    private abq(int[] iArr, int i) {
        this.f2439b = iArr;
        this.c = i;
    }

    private final void a(int i, int i2) {
        c();
        if (i < 0 || i > this.c) {
            throw new IndexOutOfBoundsException(e(i));
        }
        if (this.c < this.f2439b.length) {
            System.arraycopy(this.f2439b, i, this.f2439b, i + 1, this.c - i);
        } else {
            int[] iArr = new int[(((this.c * 3) / 2) + 1)];
            System.arraycopy(this.f2439b, 0, iArr, 0, i);
            System.arraycopy(this.f2439b, i, iArr, i + 1, this.c - i);
            this.f2439b = iArr;
        }
        this.f2439b[i] = i2;
        this.c++;
        this.modCount++;
    }

    private final void d(int i) {
        if (i < 0 || i >= this.c) {
            throw new IndexOutOfBoundsException(e(i));
        }
    }

    private final String e(int i) {
        return "Index:" + i + ", Size:" + this.c;
    }

    public final /* synthetic */ abu a(int i) {
        if (i >= this.c) {
            return new abq(Arrays.copyOf(this.f2439b, i), this.c);
        }
        throw new IllegalArgumentException();
    }

    public final /* synthetic */ void add(int i, Object obj) {
        a(i, ((Integer) obj).intValue());
    }

    public final boolean addAll(Collection<? extends Integer> collection) {
        c();
        abr.a(collection);
        if (!(collection instanceof abq)) {
            return super.addAll(collection);
        }
        abq abq = (abq) collection;
        if (abq.c == 0) {
            return false;
        }
        if (Integer.MAX_VALUE - this.c < abq.c) {
            throw new OutOfMemoryError();
        }
        int i = this.c + abq.c;
        if (i > this.f2439b.length) {
            this.f2439b = Arrays.copyOf(this.f2439b, i);
        }
        System.arraycopy(abq.f2439b, 0, this.f2439b, this.c, abq.c);
        this.c = i;
        this.modCount++;
        return true;
    }

    public final int b(int i) {
        d(i);
        return this.f2439b[i];
    }

    public final void c(int i) {
        a(this.c, i);
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof abq)) {
            return super.equals(obj);
        }
        abq abq = (abq) obj;
        if (this.c != abq.c) {
            return false;
        }
        int[] iArr = abq.f2439b;
        for (int i = 0; i < this.c; i++) {
            if (this.f2439b[i] != iArr[i]) {
                return false;
            }
        }
        return true;
    }

    public final /* synthetic */ Object get(int i) {
        return Integer.valueOf(b(i));
    }

    public final int hashCode() {
        int i = 1;
        for (int i2 = 0; i2 < this.c; i2++) {
            i = (i * 31) + this.f2439b[i2];
        }
        return i;
    }

    public final /* synthetic */ Object remove(int i) {
        c();
        d(i);
        int i2 = this.f2439b[i];
        if (i < this.c - 1) {
            System.arraycopy(this.f2439b, i + 1, this.f2439b, i, this.c - i);
        }
        this.c--;
        this.modCount++;
        return Integer.valueOf(i2);
    }

    public final boolean remove(Object obj) {
        c();
        for (int i = 0; i < this.c; i++) {
            if (obj.equals(Integer.valueOf(this.f2439b[i]))) {
                System.arraycopy(this.f2439b, i + 1, this.f2439b, i, this.c - i);
                this.c--;
                this.modCount++;
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public final void removeRange(int i, int i2) {
        c();
        if (i2 < i) {
            throw new IndexOutOfBoundsException("toIndex < fromIndex");
        }
        System.arraycopy(this.f2439b, i2, this.f2439b, i, this.c - i2);
        this.c -= i2 - i;
        this.modCount++;
    }

    public final /* synthetic */ Object set(int i, Object obj) {
        int intValue = ((Integer) obj).intValue();
        c();
        d(i);
        int i2 = this.f2439b[i];
        this.f2439b[i] = intValue;
        return Integer.valueOf(i2);
    }

    public final int size() {
        return this.c;
    }
}
