package com.google.android.gms.internal.ads;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.b.a;
import com.google.android.gms.b.a.C0046a;
import java.util.List;

public abstract class avu extends ajl implements avt {
    public avu() {
        super("com.google.android.gms.ads.internal.formats.client.INativeCustomTemplateAd");
    }

    public static avt a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.ads.internal.formats.client.INativeCustomTemplateAd");
        return queryLocalInterface instanceof avt ? (avt) queryLocalInterface : new avv(iBinder);
    }

    /* access modifiers changed from: protected */
    public final boolean dispatchTransaction(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        switch (i) {
            case 1:
                String a2 = a(parcel.readString());
                parcel2.writeNoException();
                parcel2.writeString(a2);
                break;
            case 2:
                auw b2 = b(parcel.readString());
                parcel2.writeNoException();
                ajm.a(parcel2, (IInterface) b2);
                break;
            case 3:
                List a3 = a();
                parcel2.writeNoException();
                parcel2.writeStringList(a3);
                break;
            case 4:
                String l = l();
                parcel2.writeNoException();
                parcel2.writeString(l);
                break;
            case 5:
                c(parcel.readString());
                parcel2.writeNoException();
                break;
            case 6:
                d();
                parcel2.writeNoException();
                break;
            case 7:
                aqs c = c();
                parcel2.writeNoException();
                ajm.a(parcel2, (IInterface) c);
                break;
            case 8:
                f();
                parcel2.writeNoException();
                break;
            case 9:
                a e = e();
                parcel2.writeNoException();
                ajm.a(parcel2, (IInterface) e);
                break;
            case 10:
                boolean a4 = a(C0046a.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                ajm.a(parcel2, a4);
                break;
            case 11:
                a b3 = b();
                parcel2.writeNoException();
                ajm.a(parcel2, (IInterface) b3);
                break;
            default:
                return false;
        }
        return true;
    }
}
