package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.Bundle;
import android.os.RemoteException;
import com.google.android.gms.ads.l;
import com.google.android.gms.ads.mediation.m;
import com.google.android.gms.b.a;
import com.google.android.gms.b.b;
import java.util.Iterator;
import org.json.JSONException;
import org.json.JSONObject;

public final class bem extends beh {

    /* renamed from: a reason: collision with root package name */
    private final ta f3169a;

    public bem(ta taVar) {
        this.f3169a = taVar;
    }

    private static Bundle a(String str) throws RemoteException {
        String str2 = "Server parameters: ";
        String valueOf = String.valueOf(str);
        ms.e(valueOf.length() != 0 ? str2.concat(valueOf) : new String(str2));
        try {
            Bundle bundle = new Bundle();
            if (str == null) {
                return bundle;
            }
            JSONObject jSONObject = new JSONObject(str);
            Bundle bundle2 = new Bundle();
            Iterator keys = jSONObject.keys();
            while (keys.hasNext()) {
                String str3 = (String) keys.next();
                bundle2.putString(str3, jSONObject.getString(str3));
            }
            return bundle2;
        } catch (JSONException e) {
            ms.b("", e);
            throw new RemoteException();
        }
    }

    public final beq a() throws RemoteException {
        return beq.a(this.f3169a.b());
    }

    /* JADX WARNING: type inference failed for: r2v0, types: [com.google.android.gms.internal.ads.td, com.google.android.gms.internal.ads.bep] */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r2v0, types: [com.google.android.gms.internal.ads.td, com.google.android.gms.internal.ads.bep]
  assigns: [com.google.android.gms.internal.ads.bep]
  uses: [com.google.android.gms.internal.ads.td]
  mth insns count: 46
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
    	at jadx.core.ProcessClass.process(ProcessClass.java:35)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Unknown variable types count: 1 */
    public final void a(a aVar, String str, Bundle bundle, bej bej) throws RemoteException {
        int i;
        try {
            ? bep = new bep(this, bej);
            ta taVar = this.f3169a;
            Context context = (Context) b.a(aVar);
            char c = 65535;
            switch (str.hashCode()) {
                case -1396342996:
                    if (str.equals("banner")) {
                        c = 0;
                        break;
                    }
                    break;
                case -1052618729:
                    if (str.equals("native")) {
                        c = 3;
                        break;
                    }
                    break;
                case -239580146:
                    if (str.equals("rewarded")) {
                        c = 2;
                        break;
                    }
                    break;
                case 604727084:
                    if (str.equals("interstitial")) {
                        c = 1;
                        break;
                    }
                    break;
            }
            switch (c) {
                case 0:
                    i = tb.f3700a;
                    break;
                case 1:
                    i = tb.f3701b;
                    break;
                case 2:
                    i = tb.c;
                    break;
                case 3:
                    i = tb.d;
                    break;
                default:
                    throw new IllegalArgumentException("Internal Error");
            }
            taVar.a(new tc(context, i, bundle), bep);
        } catch (Throwable th) {
            ms.b("Error generating signals for RTB", th);
            throw new RemoteException();
        }
    }

    public final void a(byte[] bArr, String str, Bundle bundle, a aVar, bec bec, bcx bcx, aot aot) throws RemoteException {
        try {
            ben ben = new ben(this, bec, bcx);
            ta taVar = this.f3169a;
            new sz((Context) b.a(aVar), bArr, a(str), bundle);
            l.a(aot.e, aot.f2815b, aot.f2814a);
            ben.a(String.valueOf(taVar.getClass().getSimpleName()).concat(" does not support banner."));
        } catch (Throwable th) {
            ms.b("Adapter failed to render banner ad.", th);
            throw new RemoteException();
        }
    }

    public final void a(byte[] bArr, String str, Bundle bundle, a aVar, bee bee, bcx bcx) throws RemoteException {
        try {
            beo beo = new beo(this, bee, bcx);
            ta taVar = this.f3169a;
            new sz((Context) b.a(aVar), bArr, a(str), bundle);
            beo.a(String.valueOf(taVar.getClass().getSimpleName()).concat(" does not support interstitial."));
        } catch (Throwable th) {
            ms.b("Adapter failed to render interstitial ad.", th);
            throw new RemoteException();
        }
    }

    public final beq b() throws RemoteException {
        return beq.a(this.f3169a.a());
    }

    public final aqs c() {
        if (!(this.f3169a instanceof m)) {
            return null;
        }
        try {
            return ((m) this.f3169a).getVideoController();
        } catch (Throwable th) {
            ms.b("", th);
            return null;
        }
    }

    public final void d() throws RemoteException {
        sy syVar = null;
        try {
            syVar.a();
        } catch (Throwable th) {
            ms.b("", th);
            throw new RemoteException();
        }
    }
}
