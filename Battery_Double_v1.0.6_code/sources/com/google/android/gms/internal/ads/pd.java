package com.google.android.gms.internal.ads;

import android.content.Context;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import com.google.android.gms.common.internal.aa;

@cm
public final class pd {

    /* renamed from: a reason: collision with root package name */
    private final Context f3595a;

    /* renamed from: b reason: collision with root package name */
    private final pm f3596b;
    private final ViewGroup c;
    private oy d;

    private pd(Context context, ViewGroup viewGroup, pm pmVar, oy oyVar) {
        if (context.getApplicationContext() != null) {
            context = context.getApplicationContext();
        }
        this.f3595a = context;
        this.c = viewGroup;
        this.f3596b = pmVar;
        this.d = null;
    }

    public pd(Context context, ViewGroup viewGroup, qn qnVar) {
        this(context, viewGroup, qnVar, null);
    }

    public final oy a() {
        aa.b("getAdVideoUnderlay must be called from the UI thread.");
        return this.d;
    }

    public final void a(int i, int i2, int i3, int i4) {
        aa.b("The underlay may only be modified from the UI thread.");
        if (this.d != null) {
            this.d.a(i, i2, i3, i4);
        }
    }

    public final void a(int i, int i2, int i3, int i4, int i5, boolean z, pl plVar) {
        if (this.d == null) {
            aso.a(this.f3596b.j().a(), this.f3596b.c(), "vpr2");
            this.d = new oy(this.f3595a, this.f3596b, i5, z, this.f3596b.j().a(), plVar);
            this.c.addView(this.d, 0, new LayoutParams(-1, -1));
            this.d.a(i, i2, i3, i4);
            this.f3596b.a(false);
        }
    }

    public final void b() {
        aa.b("onPause must be called from the UI thread.");
        if (this.d != null) {
            this.d.i();
        }
    }

    public final void c() {
        aa.b("onDestroy must be called from the UI thread.");
        if (this.d != null) {
            this.d.n();
            this.c.removeView(this.d);
            this.d = null;
        }
    }
}
