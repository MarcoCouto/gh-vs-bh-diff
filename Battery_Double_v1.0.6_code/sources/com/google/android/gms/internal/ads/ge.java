package com.google.android.gms.internal.ads;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;

public final class ge extends ajk implements gc {
    ge(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.reward.client.IRewardItem");
    }

    public final String a() throws RemoteException {
        Parcel a2 = a(1, r_());
        String readString = a2.readString();
        a2.recycle();
        return readString;
    }

    public final int b() throws RemoteException {
        Parcel a2 = a(2, r_());
        int readInt = a2.readInt();
        a2.recycle();
        return readInt;
    }
}
