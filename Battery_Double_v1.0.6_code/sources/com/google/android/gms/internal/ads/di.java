package com.google.android.gms.internal.ads;

import android.content.Context;

@cm
public final class di extends de {

    /* renamed from: a reason: collision with root package name */
    private final Context f3257a;

    public di(Context context, oa<dl> oaVar, dc dcVar) {
        super(oaVar, dcVar);
        this.f3257a = context;
    }

    public final void a() {
    }

    public final dt d() {
        return eu.a(this.f3257a, et.a(this.f3257a));
    }
}
