package com.google.android.gms.internal.ads;

import android.view.View;
import android.view.View.OnAttachStateChangeListener;

final class qs implements OnAttachStateChangeListener {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ ic f3642a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ qo f3643b;

    qs(qo qoVar, ic icVar) {
        this.f3643b = qoVar;
        this.f3642a = icVar;
    }

    public final void onViewAttachedToWindow(View view) {
        this.f3643b.a(view, this.f3642a, 10);
    }

    public final void onViewDetachedFromWindow(View view) {
    }
}
