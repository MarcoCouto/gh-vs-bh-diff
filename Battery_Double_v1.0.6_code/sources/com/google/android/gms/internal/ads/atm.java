package com.google.android.gms.internal.ads;

import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.RemoteException;
import com.google.android.gms.b.a;
import com.google.android.gms.b.b;

@cm
public final class atm extends aux {

    /* renamed from: a reason: collision with root package name */
    private final Drawable f2904a;

    /* renamed from: b reason: collision with root package name */
    private final Uri f2905b;
    private final double c;

    public atm(Drawable drawable, Uri uri, double d) {
        this.f2904a = drawable;
        this.f2905b = uri;
        this.c = d;
    }

    public final a a() throws RemoteException {
        return b.a(this.f2904a);
    }

    public final Uri b() throws RemoteException {
        return this.f2905b;
    }

    public final double c() {
        return this.c;
    }
}
