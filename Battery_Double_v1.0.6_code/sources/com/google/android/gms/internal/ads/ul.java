package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.xe.b;
import java.security.GeneralSecurityException;
import java.util.logging.Logger;

class ul implements tz<tr> {

    /* renamed from: a reason: collision with root package name */
    private static final Logger f3726a = Logger.getLogger(ul.class.getName());

    ul() throws GeneralSecurityException {
        uh.a("type.googleapis.com/google.crypto.tink.AesCtrKey", (tz<P>) new um<P>());
    }

    /* access modifiers changed from: private */
    /* renamed from: d */
    public final tr a(aah aah) throws GeneralSecurityException {
        try {
            vi a2 = vi.a(aah);
            if (!(a2 instanceof vi)) {
                throw new GeneralSecurityException("expected AesCtrHmacAeadKey proto");
            }
            vi viVar = a2;
            zo.a(viVar.a(), 0);
            return new yu((zg) uh.b("type.googleapis.com/google.crypto.tink.AesCtrKey", viVar.b()), (ud) uh.b("type.googleapis.com/google.crypto.tink.HmacKey", viVar.c()), viVar.c().b().b());
        } catch (abv e) {
            throw new GeneralSecurityException("expected serialized AesCtrHmacAeadKey proto", e);
        }
    }

    public final int a() {
        return 0;
    }

    public final /* synthetic */ Object a(acw acw) throws GeneralSecurityException {
        if (!(acw instanceof vi)) {
            throw new GeneralSecurityException("expected AesCtrHmacAeadKey proto");
        }
        vi viVar = (vi) acw;
        zo.a(viVar.a(), 0);
        return new yu((zg) uh.b("type.googleapis.com/google.crypto.tink.AesCtrKey", viVar.b()), (ud) uh.b("type.googleapis.com/google.crypto.tink.HmacKey", viVar.c()), viVar.c().b().b());
    }

    public final acw b(aah aah) throws GeneralSecurityException {
        try {
            return b((acw) vk.a(aah));
        } catch (abv e) {
            throw new GeneralSecurityException("expected serialized AesCtrHmacAeadKeyFormat proto", e);
        }
    }

    public final acw b(acw acw) throws GeneralSecurityException {
        if (!(acw instanceof vk)) {
            throw new GeneralSecurityException("expected AesCtrHmacAeadKeyFormat proto");
        }
        vk vkVar = (vk) acw;
        return vi.d().a((vm) uh.a("type.googleapis.com/google.crypto.tink.AesCtrKey", (acw) vkVar.a())).a((wy) uh.a("type.googleapis.com/google.crypto.tink.HmacKey", (acw) vkVar.b())).a(0).c();
    }

    public final xe c(aah aah) throws GeneralSecurityException {
        return (xe) xe.d().a("type.googleapis.com/google.crypto.tink.AesCtrHmacAeadKey").a(((vi) b(aah)).h()).a(b.SYMMETRIC).c();
    }
}
