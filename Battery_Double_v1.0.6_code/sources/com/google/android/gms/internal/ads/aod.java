package com.google.android.gms.internal.ads;

import java.io.IOException;

public final class aod extends afh<aod> {

    /* renamed from: a reason: collision with root package name */
    private Integer f2795a;

    /* renamed from: b reason: collision with root package name */
    private aob f2796b;
    private Integer c;
    private Integer d;
    private Integer e;

    public aod() {
        this.f2795a = null;
        this.f2796b = null;
        this.c = null;
        this.d = null;
        this.e = null;
        this.Y = null;
        this.Z = -1;
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public final aod a(afd afd) throws IOException {
        while (true) {
            int a2 = afd.a();
            switch (a2) {
                case 0:
                    break;
                case 8:
                    int j = afd.j();
                    try {
                        this.f2795a = Integer.valueOf(anf.a(afd.g()));
                        continue;
                    } catch (IllegalArgumentException e2) {
                        afd.e(j);
                        a(afd, a2);
                        break;
                    }
                case 18:
                    if (this.f2796b == null) {
                        this.f2796b = new aob();
                    }
                    afd.a((afn) this.f2796b);
                    continue;
                case 24:
                    this.c = Integer.valueOf(afd.g());
                    continue;
                case 32:
                    this.d = Integer.valueOf(afd.g());
                    continue;
                case 40:
                    this.e = Integer.valueOf(afd.g());
                    continue;
                default:
                    if (!super.a(afd, a2)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    /* access modifiers changed from: protected */
    public final int a() {
        int a2 = super.a();
        if (this.f2795a != null) {
            a2 += aff.b(1, this.f2795a.intValue());
        }
        if (this.f2796b != null) {
            a2 += aff.b(2, (afn) this.f2796b);
        }
        if (this.c != null) {
            a2 += aff.b(3, this.c.intValue());
        }
        if (this.d != null) {
            a2 += aff.b(4, this.d.intValue());
        }
        return this.e != null ? a2 + aff.b(5, this.e.intValue()) : a2;
    }

    public final void a(aff aff) throws IOException {
        if (this.f2795a != null) {
            aff.a(1, this.f2795a.intValue());
        }
        if (this.f2796b != null) {
            aff.a(2, (afn) this.f2796b);
        }
        if (this.c != null) {
            aff.a(3, this.c.intValue());
        }
        if (this.d != null) {
            aff.a(4, this.d.intValue());
        }
        if (this.e != null) {
            aff.a(5, this.e.intValue());
        }
        super.a(aff);
    }
}
