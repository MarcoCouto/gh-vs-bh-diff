package com.google.android.gms.internal.ads;

import java.nio.ByteBuffer;
import java.security.GeneralSecurityException;
import java.security.interfaces.ECPublicKey;

public final class ym implements ty {

    /* renamed from: a reason: collision with root package name */
    private static final byte[] f3795a = new byte[0];

    /* renamed from: b reason: collision with root package name */
    private final yo f3796b;
    private final String c;
    private final byte[] d;
    private final yt e;
    private final yk f;

    public ym(ECPublicKey eCPublicKey, byte[] bArr, String str, yt ytVar, yk ykVar) throws GeneralSecurityException {
        yq.a(eCPublicKey.getW(), eCPublicKey.getParams().getCurve());
        this.f3796b = new yo(eCPublicKey);
        this.d = bArr;
        this.c = str;
        this.e = ytVar;
        this.f = ykVar;
    }

    public final byte[] a(byte[] bArr, byte[] bArr2) throws GeneralSecurityException {
        yp a2 = this.f3796b.a(this.c, this.d, bArr2, this.f.a(), this.e);
        byte[] a3 = this.f.a(a2.b()).a(bArr, f3795a);
        byte[] a4 = a2.a();
        return ByteBuffer.allocate(a4.length + a3.length).put(a4).put(a3).array();
    }
}
