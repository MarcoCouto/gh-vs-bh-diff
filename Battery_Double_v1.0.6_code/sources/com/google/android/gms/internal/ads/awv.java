package com.google.android.gms.internal.ads;

import android.os.IBinder;
import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.ads.b.c.a;
import com.google.android.gms.ads.b.c.b;
import com.google.android.gms.ads.b.k;
import com.google.android.gms.ads.i;
import java.util.ArrayList;
import java.util.List;

@cm
public final class awv extends k {

    /* renamed from: a reason: collision with root package name */
    private final aws f2965a;

    /* renamed from: b reason: collision with root package name */
    private final List<b> f2966b = new ArrayList();
    private final auz c;
    private final i d = new i();
    private final a e;

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0042 A[Catch:{ RemoteException -> 0x004d }] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0020 A[SYNTHETIC] */
    public awv(aws aws) {
        auz auz;
        auw auw;
        a aVar = null;
        this.f2965a = aws;
        try {
            List b2 = this.f2965a.b();
            if (b2 != null) {
                for (Object next : b2) {
                    if (next instanceof IBinder) {
                        IBinder iBinder = (IBinder) next;
                        if (iBinder != null) {
                            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.ads.internal.formats.client.INativeAdImage");
                            auw = queryLocalInterface instanceof auw ? (auw) queryLocalInterface : new auy(iBinder);
                            if (auw == null) {
                                this.f2966b.add(new auz(auw));
                            }
                        }
                    }
                    auw = null;
                    if (auw == null) {
                    }
                }
            }
        } catch (RemoteException e2) {
            ms.b("", e2);
        }
        try {
            auw d2 = this.f2965a.d();
            auz = d2 != null ? new auz(d2) : null;
        } catch (RemoteException e3) {
            ms.b("", e3);
            auz = null;
        }
        this.c = auz;
        try {
            if (this.f2965a.s() != null) {
                aVar = new auv(this.f2965a.s());
            }
        } catch (RemoteException e4) {
            ms.b("", e4);
        }
        this.e = aVar;
    }

    /* access modifiers changed from: private */
    /* renamed from: m */
    public final com.google.android.gms.b.a k() {
        try {
            return this.f2965a.n();
        } catch (RemoteException e2) {
            ms.b("", e2);
            return null;
        }
    }

    public final String a() {
        try {
            return this.f2965a.a();
        } catch (RemoteException e2) {
            ms.b("", e2);
            return null;
        }
    }

    public final List<b> b() {
        return this.f2966b;
    }

    public final String c() {
        try {
            return this.f2965a.c();
        } catch (RemoteException e2) {
            ms.b("", e2);
            return null;
        }
    }

    public final b d() {
        return this.c;
    }

    public final String e() {
        try {
            return this.f2965a.e();
        } catch (RemoteException e2) {
            ms.b("", e2);
            return null;
        }
    }

    public final String f() {
        try {
            return this.f2965a.f();
        } catch (RemoteException e2) {
            ms.b("", e2);
            return null;
        }
    }

    public final Double g() {
        try {
            double g = this.f2965a.g();
            if (g == -1.0d) {
                return null;
            }
            return Double.valueOf(g);
        } catch (RemoteException e2) {
            ms.b("", e2);
            return null;
        }
    }

    public final String h() {
        try {
            return this.f2965a.h();
        } catch (RemoteException e2) {
            ms.b("", e2);
            return null;
        }
    }

    public final String i() {
        try {
            return this.f2965a.i();
        } catch (RemoteException e2) {
            ms.b("", e2);
            return null;
        }
    }

    public final i j() {
        try {
            if (this.f2965a.j() != null) {
                this.d.a(this.f2965a.j());
            }
        } catch (RemoteException e2) {
            ms.b("Exception occurred while getting video controller", e2);
        }
        return this.d;
    }

    public final Object l() {
        try {
            com.google.android.gms.b.a p = this.f2965a.p();
            if (p != null) {
                return com.google.android.gms.b.b.a(p);
            }
        } catch (RemoteException e2) {
            ms.b("", e2);
        }
        return null;
    }
}
