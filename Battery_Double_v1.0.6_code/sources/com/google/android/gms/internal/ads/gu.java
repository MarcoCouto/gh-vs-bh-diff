package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.a.b;

public final class gu implements Creator<gt> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b2 = b.b(parcel);
        String str = null;
        aop aop = null;
        while (parcel.dataPosition() < b2) {
            int a2 = b.a(parcel);
            switch (b.a(a2)) {
                case 2:
                    aop = (aop) b.a(parcel, a2, aop.CREATOR);
                    break;
                case 3:
                    str = b.k(parcel, a2);
                    break;
                default:
                    b.b(parcel, a2);
                    break;
            }
        }
        b.r(parcel, b2);
        return new gt(aop, str);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new gt[i];
    }
}
