package com.google.android.gms.internal.ads;

import android.os.Handler;
import android.os.Looper;
import com.google.android.gms.ads.internal.ax;
import java.util.concurrent.Executor;

final class nu implements Executor {

    /* renamed from: a reason: collision with root package name */
    private final Handler f3560a = new jn(Looper.getMainLooper());

    nu() {
    }

    public final void execute(Runnable runnable) {
        if (Looper.getMainLooper().getThread() == Thread.currentThread()) {
            try {
                runnable.run();
            } catch (Throwable th) {
                ax.e();
                jv.a(ax.i().m(), th);
                throw th;
            }
        } else {
            this.f3560a.post(runnable);
        }
    }
}
