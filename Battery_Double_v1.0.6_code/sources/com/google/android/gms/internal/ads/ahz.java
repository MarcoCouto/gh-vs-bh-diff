package com.google.android.gms.internal.ads;

import android.os.Process;
import java.util.concurrent.BlockingQueue;

public final class ahz extends Thread {

    /* renamed from: a reason: collision with root package name */
    private static final boolean f2609a = eg.f3276a;

    /* renamed from: b reason: collision with root package name */
    private final BlockingQueue<awb<?>> f2610b;
    /* access modifiers changed from: private */
    public final BlockingQueue<awb<?>> c;
    private final zy d;
    /* access modifiers changed from: private */
    public final b e;
    private volatile boolean f = false;
    private final akb g;

    public ahz(BlockingQueue<awb<?>> blockingQueue, BlockingQueue<awb<?>> blockingQueue2, zy zyVar, b bVar) {
        this.f2610b = blockingQueue;
        this.c = blockingQueue2;
        this.d = zyVar;
        this.e = bVar;
        this.g = new akb(this);
    }

    private final void b() throws InterruptedException {
        awb awb = (awb) this.f2610b.take();
        awb.b("cache-queue-take");
        awb.g();
        agy a2 = this.d.a(awb.e());
        if (a2 == null) {
            awb.b("cache-miss");
            if (!this.g.b(awb)) {
                this.c.put(awb);
            }
        } else if (a2.a()) {
            awb.b("cache-hit-expired");
            awb.a(a2);
            if (!this.g.b(awb)) {
                this.c.put(awb);
            }
        } else {
            awb.b("cache-hit");
            bcd a3 = awb.a(new atz(a2.f2576a, a2.g));
            awb.b("cache-hit-parsed");
            if (a2.f < System.currentTimeMillis()) {
                awb.b("cache-hit-refresh-needed");
                awb.a(a2);
                a3.d = true;
                if (!this.g.b(awb)) {
                    this.e.a(awb, a3, new aja(this, awb));
                    return;
                }
            }
            this.e.a(awb, a3);
        }
    }

    public final void a() {
        this.f = true;
        interrupt();
    }

    public final void run() {
        if (f2609a) {
            eg.a("start new dispatcher", new Object[0]);
        }
        Process.setThreadPriority(10);
        this.d.a();
        while (true) {
            try {
                b();
            } catch (InterruptedException e2) {
                if (this.f) {
                    return;
                }
            }
        }
    }
}
