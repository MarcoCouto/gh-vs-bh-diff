package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.gmsg.h;
import org.json.JSONException;
import org.json.JSONObject;

final class bbw implements h {

    /* renamed from: a reason: collision with root package name */
    private final bau f3115a;

    /* renamed from: b reason: collision with root package name */
    private final ny f3116b;
    private final /* synthetic */ bbt c;

    public bbw(bbt bbt, bau bau, ny nyVar) {
        this.c = bbt;
        this.f3115a = bau;
        this.f3116b = nyVar;
    }

    public final void a(String str) {
        if (str == null) {
            try {
                this.f3116b.a(new bbh());
            } catch (IllegalStateException e) {
                this.f3115a.c();
                return;
            } catch (Throwable th) {
                this.f3115a.c();
                throw th;
            }
        } else {
            this.f3116b.a(new bbh(str));
        }
        this.f3115a.c();
    }

    public final void a(JSONObject jSONObject) {
        try {
            this.f3116b.b(this.c.f3109a.a(jSONObject));
        } catch (IllegalStateException e) {
        } catch (JSONException e2) {
            this.f3116b.b(e2);
        } finally {
            this.f3115a.c();
        }
    }
}
