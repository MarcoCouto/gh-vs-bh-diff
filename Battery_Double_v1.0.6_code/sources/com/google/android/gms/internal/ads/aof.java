package com.google.android.gms.internal.ads;

import android.os.Handler;
import java.util.concurrent.Executor;

final class aof implements Executor {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ Handler f2799a;

    aof(ane ane, Handler handler) {
        this.f2799a = handler;
    }

    public final void execute(Runnable runnable) {
        this.f2799a.post(runnable);
    }
}
