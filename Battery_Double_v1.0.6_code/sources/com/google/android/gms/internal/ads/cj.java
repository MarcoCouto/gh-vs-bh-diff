package com.google.android.gms.internal.ads;

final class cj implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ mt f3232a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ String f3233b;

    cj(cg cgVar, mt mtVar, String str) {
        this.f3232a = mtVar;
        this.f3233b = str;
    }

    public final void run() {
        this.f3232a.a(this.f3233b);
    }
}
