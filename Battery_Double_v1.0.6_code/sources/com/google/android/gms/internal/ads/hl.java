package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.b.a;

public interface hl extends IInterface {
    void a(Bundle bundle) throws RemoteException;

    void a(a aVar) throws RemoteException;

    void a(a aVar, int i) throws RemoteException;

    void a(a aVar, hp hpVar) throws RemoteException;

    void b(a aVar) throws RemoteException;

    void b(a aVar, int i) throws RemoteException;

    void c(a aVar) throws RemoteException;

    void d(a aVar) throws RemoteException;

    void e(a aVar) throws RemoteException;

    void f(a aVar) throws RemoteException;

    void g(a aVar) throws RemoteException;

    void h(a aVar) throws RemoteException;
}
