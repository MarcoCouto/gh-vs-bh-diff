package com.google.android.gms.internal.ads;

import java.util.concurrent.Callable;

final /* synthetic */ class ix implements Callable {

    /* renamed from: a reason: collision with root package name */
    private final iw f3407a;

    ix(iw iwVar) {
        this.f3407a = iwVar;
    }

    public final Object call() {
        return this.f3407a.o();
    }
}
