package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.gmsg.ae;
import java.util.Map;

final class bam implements ae<bbe> {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ bay f3074a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ azv f3075b;
    private final /* synthetic */ bah c;

    bam(bah bah, bay bay, azv azv) {
        this.c = bah;
        this.f3074a = bay;
        this.f3075b = azv;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        return;
     */
    public final /* synthetic */ void zza(Object obj, Map map) {
        synchronized (this.c.f3065a) {
            if (this.f3074a.b() != -1 && this.f3074a.b() != 1) {
                this.c.h = 0;
                this.c.e.a(this.f3075b);
                this.f3074a.a(this.f3075b);
                this.c.g = this.f3074a;
                jm.a("Successfully loaded JS Engine.");
            }
        }
    }
}
