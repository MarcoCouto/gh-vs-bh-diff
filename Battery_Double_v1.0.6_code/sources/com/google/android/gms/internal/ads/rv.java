package com.google.android.gms.internal.ads;

import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.ViewTreeObserver.OnScrollChangedListener;
import com.google.android.gms.ads.internal.bv;
import com.google.android.gms.ads.internal.gmsg.ai;
import com.google.android.gms.ads.internal.gmsg.k;
import com.google.android.gms.ads.internal.gmsg.m;
import com.google.android.gms.ads.internal.overlay.n;
import com.google.android.gms.ads.internal.overlay.t;

@cm
public interface rv {
    bv a();

    void a(int i, int i2);

    void a(int i, int i2, boolean z);

    void a(OnGlobalLayoutListener onGlobalLayoutListener, OnScrollChangedListener onScrollChangedListener);

    void a(aoj aoj, k kVar, n nVar, m mVar, t tVar, boolean z, ai aiVar, bv bvVar, o oVar, ic icVar);

    void a(rw rwVar);

    void a(rx rxVar);

    void a(ry ryVar);

    void a(rz rzVar);

    boolean b();

    boolean f();

    void g();

    void h();

    void i();

    void j();

    ic m();

    void n();
}
