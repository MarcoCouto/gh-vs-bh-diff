package com.google.android.gms.internal.ads;

import java.util.ArrayList;
import java.util.List;

final class adk<E> extends aab<E> {

    /* renamed from: a reason: collision with root package name */
    private static final adk<Object> f2478a;

    /* renamed from: b reason: collision with root package name */
    private final List<E> f2479b;

    static {
        adk<Object> adk = new adk<>();
        f2478a = adk;
        adk.b();
    }

    adk() {
        this(new ArrayList(10));
    }

    private adk(List<E> list) {
        this.f2479b = list;
    }

    public static <E> adk<E> d() {
        return f2478a;
    }

    public final /* synthetic */ abu a(int i) {
        if (i < size()) {
            throw new IllegalArgumentException();
        }
        ArrayList arrayList = new ArrayList(i);
        arrayList.addAll(this.f2479b);
        return new adk(arrayList);
    }

    public final void add(int i, E e) {
        c();
        this.f2479b.add(i, e);
        this.modCount++;
    }

    public final E get(int i) {
        return this.f2479b.get(i);
    }

    public final E remove(int i) {
        c();
        E remove = this.f2479b.remove(i);
        this.modCount++;
        return remove;
    }

    public final E set(int i, E e) {
        c();
        E e2 = this.f2479b.set(i, e);
        this.modCount++;
        return e2;
    }

    public final int size() {
        return this.f2479b.size();
    }
}
