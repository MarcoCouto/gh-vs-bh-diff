package com.google.android.gms.internal.ads;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.TextView;
import com.google.android.gms.ads.internal.ax;
import com.google.android.gms.common.internal.b;
import com.hmatalonga.greenhub.Config;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;

@cm
public final class oy extends FrameLayout implements ov {

    /* renamed from: a reason: collision with root package name */
    private final pm f3586a;

    /* renamed from: b reason: collision with root package name */
    private final FrameLayout f3587b;
    private final asv c;
    private final po d;
    private final long e;
    private ow f;
    private boolean g;
    private boolean h;
    private boolean i;
    private boolean j;
    private long k;
    private long l;
    private String m;
    private Bitmap n;
    private ImageView o;
    private boolean p;

    public oy(Context context, pm pmVar, int i2, boolean z, asv asv, pl plVar) {
        super(context);
        this.f3586a = pmVar;
        this.c = asv;
        this.f3587b = new FrameLayout(context);
        addView(this.f3587b, new LayoutParams(-1, -1));
        b.a(pmVar.e());
        this.f = pmVar.e().f2020b.a(context, pmVar, i2, z, asv, plVar);
        if (this.f != null) {
            this.f3587b.addView(this.f, new LayoutParams(-1, -1, 17));
            if (((Boolean) ape.f().a(asi.w)).booleanValue()) {
                m();
            }
        }
        this.o = new ImageView(context);
        this.e = ((Long) ape.f().a(asi.A)).longValue();
        this.j = ((Boolean) ape.f().a(asi.y)).booleanValue();
        if (this.c != null) {
            this.c.a("spinner_used", this.j ? "1" : Config.NOTIFICATION_DEFAULT_PRIORITY);
        }
        this.d = new po(this);
        if (this.f != null) {
            this.f.a((ov) this);
        }
        if (this.f == null) {
            a("AdVideoUnderlay Error", "Allocating player failed.");
        }
    }

    public static void a(pm pmVar) {
        HashMap hashMap = new HashMap();
        hashMap.put("event", "no_video_view");
        pmVar.a("onVideoEvent", (Map<String, ?>) hashMap);
    }

    public static void a(pm pmVar, String str) {
        HashMap hashMap = new HashMap();
        hashMap.put("event", "decoderProps");
        hashMap.put("error", str);
        pmVar.a("onVideoEvent", (Map<String, ?>) hashMap);
    }

    public static void a(pm pmVar, Map<String, List<Map<String, Object>>> map) {
        HashMap hashMap = new HashMap();
        hashMap.put("event", "decoderProps");
        hashMap.put("mimeTypes", map);
        pmVar.a("onVideoEvent", (Map<String, ?>) hashMap);
    }

    /* access modifiers changed from: private */
    public final void a(String str, String... strArr) {
        HashMap hashMap = new HashMap();
        hashMap.put("event", str);
        int length = strArr.length;
        int i2 = 0;
        String str2 = null;
        while (i2 < length) {
            String str3 = strArr[i2];
            if (str2 != null) {
                hashMap.put(str2, str3);
                str3 = null;
            }
            i2++;
            str2 = str3;
        }
        this.f3586a.a("onVideoEvent", (Map<String, ?>) hashMap);
    }

    private final boolean p() {
        return this.o.getParent() != null;
    }

    private final void q() {
        if (this.f3586a.d() != null && this.h && !this.i) {
            this.f3586a.d().getWindow().clearFlags(128);
            this.h = false;
        }
    }

    public final void a() {
        this.d.b();
        jv.f3440a.post(new pa(this));
    }

    public final void a(float f2, float f3) {
        if (this.f != null) {
            this.f.a(f2, f3);
        }
    }

    public final void a(int i2) {
        if (this.f != null) {
            this.f.a(i2);
        }
    }

    public final void a(int i2, int i3) {
        if (this.j) {
            int max = Math.max(i2 / ((Integer) ape.f().a(asi.z)).intValue(), 1);
            int max2 = Math.max(i3 / ((Integer) ape.f().a(asi.z)).intValue(), 1);
            if (this.n == null || this.n.getWidth() != max || this.n.getHeight() != max2) {
                this.n = Bitmap.createBitmap(max, max2, Bitmap.Config.ARGB_8888);
                this.p = false;
            }
        }
    }

    public final void a(int i2, int i3, int i4, int i5) {
        if (i4 != 0 && i5 != 0) {
            LayoutParams layoutParams = new LayoutParams(i4, i5);
            layoutParams.setMargins(i2, i3, 0, 0);
            this.f3587b.setLayoutParams(layoutParams);
            requestLayout();
        }
    }

    @TargetApi(14)
    public final void a(MotionEvent motionEvent) {
        if (this.f != null) {
            this.f.dispatchTouchEvent(motionEvent);
        }
    }

    public final void a(String str) {
        this.m = str;
    }

    public final void a(String str, String str2) {
        a("error", "what", str, "extra", str2);
    }

    public final void b() {
        if (this.f != null && this.l == 0) {
            a("canplaythrough", "duration", String.valueOf(((float) this.f.getDuration()) / 1000.0f), "videoWidth", String.valueOf(this.f.getVideoWidth()), "videoHeight", String.valueOf(this.f.getVideoHeight()));
        }
    }

    public final void c() {
        if (this.f3586a.d() != null && !this.h) {
            this.i = (this.f3586a.d().getWindow().getAttributes().flags & 128) != 0;
            if (!this.i) {
                this.f3586a.d().getWindow().addFlags(128);
                this.h = true;
            }
        }
        this.g = true;
    }

    public final void d() {
        a("pause", new String[0]);
        q();
        this.g = false;
    }

    public final void e() {
        a("ended", new String[0]);
        q();
    }

    public final void f() {
        if (this.p && this.n != null && !p()) {
            this.o.setImageBitmap(this.n);
            this.o.invalidate();
            this.f3587b.addView(this.o, new LayoutParams(-1, -1));
            this.f3587b.bringChildToFront(this.o);
        }
        this.d.a();
        this.l = this.k;
        jv.f3440a.post(new pb(this));
    }

    public final void finalize() throws Throwable {
        try {
            this.d.a();
            if (this.f != null) {
                ow owVar = this.f;
                Executor executor = nt.f3558a;
                owVar.getClass();
                executor.execute(oz.a(owVar));
            }
        } finally {
            super.finalize();
        }
    }

    public final void g() {
        if (this.g && p()) {
            this.f3587b.removeView(this.o);
        }
        if (this.n != null) {
            long b2 = ax.l().b();
            if (this.f.getBitmap(this.n) != null) {
                this.p = true;
            }
            long b3 = ax.l().b() - b2;
            if (jm.a()) {
                jm.a("Spinner frame grab took " + b3 + "ms");
            }
            if (b3 > this.e) {
                jm.e("Spinner frame grab crossed jank threshold! Suspending spinner.");
                this.j = false;
                this.n = null;
                if (this.c != null) {
                    this.c.a("spinner_jank", Long.toString(b3));
                }
            }
        }
    }

    public final void h() {
        if (this.f != null) {
            if (!TextUtils.isEmpty(this.m)) {
                this.f.setVideoPath(this.m);
            } else {
                a("no_src", new String[0]);
            }
        }
    }

    public final void i() {
        if (this.f != null) {
            this.f.d();
        }
    }

    public final void j() {
        if (this.f != null) {
            this.f.c();
        }
    }

    public final void k() {
        if (this.f != null) {
            ow owVar = this.f;
            owVar.f3585b.a(true);
            owVar.e();
        }
    }

    public final void l() {
        if (this.f != null) {
            ow owVar = this.f;
            owVar.f3585b.a(false);
            owVar.e();
        }
    }

    @TargetApi(14)
    public final void m() {
        if (this.f != null) {
            TextView textView = new TextView(this.f.getContext());
            String str = "AdMob - ";
            String valueOf = String.valueOf(this.f.a());
            textView.setText(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
            textView.setTextColor(-65536);
            textView.setBackgroundColor(-256);
            this.f3587b.addView(textView, new LayoutParams(-2, -2, 17));
            this.f3587b.bringChildToFront(textView);
        }
    }

    public final void n() {
        this.d.a();
        if (this.f != null) {
            this.f.b();
        }
        q();
    }

    /* access modifiers changed from: 0000 */
    public final void o() {
        if (this.f != null) {
            long currentPosition = (long) this.f.getCurrentPosition();
            if (this.k != currentPosition && currentPosition > 0) {
                a("timeupdate", "time", String.valueOf(((float) currentPosition) / 1000.0f));
                this.k = currentPosition;
            }
        }
    }

    public final void onWindowVisibilityChanged(int i2) {
        boolean z;
        if (i2 == 0) {
            this.d.b();
            z = true;
        } else {
            this.d.a();
            this.l = this.k;
            z = false;
        }
        jv.f3440a.post(new pc(this, z));
    }

    public final void setVolume(float f2) {
        if (this.f != null) {
            ow owVar = this.f;
            owVar.f3585b.a(f2);
            owVar.e();
        }
    }
}
