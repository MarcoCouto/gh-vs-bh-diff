package com.google.android.gms.internal.ads;

import java.util.AbstractList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.RandomAccess;

public final class ael extends AbstractList<String> implements acf, RandomAccess {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public final acf f2506a;

    public ael(acf acf) {
        this.f2506a = acf;
    }

    public final void a(aah aah) {
        throw new UnsupportedOperationException();
    }

    public final Object b(int i) {
        return this.f2506a.b(i);
    }

    public final List<?> d() {
        return this.f2506a.d();
    }

    public final acf e() {
        return this;
    }

    public final /* synthetic */ Object get(int i) {
        return (String) this.f2506a.get(i);
    }

    public final Iterator<String> iterator() {
        return new aen(this);
    }

    public final ListIterator<String> listIterator(int i) {
        return new aem(this, i);
    }

    public final int size() {
        return this.f2506a.size();
    }
}
