package com.google.android.gms.internal.ads;

final class act {

    /* renamed from: a reason: collision with root package name */
    private static final acr f2465a = c();

    /* renamed from: b reason: collision with root package name */
    private static final acr f2466b = new acs();

    static acr a() {
        return f2465a;
    }

    static acr b() {
        return f2466b;
    }

    private static acr c() {
        try {
            return (acr) Class.forName("com.google.protobuf.MapFieldSchemaFull").getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception e) {
            return null;
        }
    }
}
