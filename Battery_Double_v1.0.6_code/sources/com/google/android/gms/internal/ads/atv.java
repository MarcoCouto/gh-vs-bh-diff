package com.google.android.gms.internal.ads;

final class atv implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ atu f2918a;

    atv(atu atu) {
        this.f2918a = atu;
    }

    public final void run() {
        if (this.f2918a.q != null) {
            this.f2918a.q.i();
            this.f2918a.q.h();
            this.f2918a.q.k();
        }
        this.f2918a.q = null;
    }
}
