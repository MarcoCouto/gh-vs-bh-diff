package com.google.android.gms.internal.ads;

import android.os.RemoteException;

final class arj extends apo {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ arh f2854a;

    private arj(arh arh) {
        this.f2854a = arh;
    }

    public final String a() throws RemoteException {
        return null;
    }

    public final void a(aop aop) throws RemoteException {
        a(aop, 1);
    }

    public final void a(aop aop, int i) throws RemoteException {
        ms.c("This app is using a lightweight version of the Google Mobile Ads SDK that requires the latest Google Play services to be installed, but Google Play services is either missing or out of date.");
        mh.f3514a.post(new ark(this));
    }

    public final String b() throws RemoteException {
        return null;
    }

    public final boolean c() throws RemoteException {
        return false;
    }
}
