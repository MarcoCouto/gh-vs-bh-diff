package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.app.Application.ActivityLifecycleCallbacks;

final class ale implements alf {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ Activity f2691a;

    ale(akw akw, Activity activity) {
        this.f2691a = activity;
    }

    public final void a(ActivityLifecycleCallbacks activityLifecycleCallbacks) {
        activityLifecycleCallbacks.onActivityDestroyed(this.f2691a);
    }
}
