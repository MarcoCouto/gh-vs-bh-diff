package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.overlay.n;

final class bes implements n {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ zzzv f3177a;

    bes(zzzv zzzv) {
        this.f3177a = zzzv;
    }

    public final void d() {
        ms.b("AdMobCustomTabsAdapter overlay is paused.");
    }

    public final void f() {
        ms.b("AdMobCustomTabsAdapter overlay is resumed.");
    }

    public final void g() {
        ms.b("Opening AdMobCustomTabsAdapter overlay.");
        this.f3177a.f3833b.b(this.f3177a);
    }

    public final void p_() {
        ms.b("AdMobCustomTabsAdapter overlay is closed.");
        this.f3177a.f3833b.c(this.f3177a);
    }
}
