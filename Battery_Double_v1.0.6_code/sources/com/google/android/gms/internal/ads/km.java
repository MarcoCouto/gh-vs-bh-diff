package com.google.android.gms.internal.ads;

import android.annotation.TargetApi;
import android.content.Context;
import android.webkit.CookieManager;
import android.webkit.WebResourceResponse;
import com.google.android.gms.ads.internal.ax;
import java.io.InputStream;
import java.util.Map;

@TargetApi(21)
public final class km extends kk {
    public final WebResourceResponse a(String str, String str2, int i, String str3, Map<String, String> map, InputStream inputStream) {
        return new WebResourceResponse(str, str2, i, str3, map, inputStream);
    }

    public final qo a(qn qnVar, boolean z) {
        return new rn(qnVar, z);
    }

    public final CookieManager c(Context context) {
        CookieManager cookieManager = null;
        if (e()) {
            return cookieManager;
        }
        try {
            return CookieManager.getInstance();
        } catch (Throwable th) {
            jm.b("Failed to obtain CookieManager.", th);
            ax.i().a(th, "ApiLevelUtil.getCookieManager");
            return cookieManager;
        }
    }

    public final int f() {
        return 16974374;
    }
}
