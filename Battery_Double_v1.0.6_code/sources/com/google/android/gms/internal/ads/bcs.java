package com.google.android.gms.internal.ads;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public abstract class bcs extends ajl implements bcr {
    public bcs() {
        super("com.google.android.gms.ads.internal.mediation.client.IAdapterCreator");
    }

    public static bcr a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.ads.internal.mediation.client.IAdapterCreator");
        return queryLocalInterface instanceof bcr ? (bcr) queryLocalInterface : new bct(iBinder);
    }

    /* access modifiers changed from: protected */
    public final boolean dispatchTransaction(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        switch (i) {
            case 1:
                bcu a2 = a(parcel.readString());
                parcel2.writeNoException();
                ajm.a(parcel2, (IInterface) a2);
                break;
            case 2:
                boolean b2 = b(parcel.readString());
                parcel2.writeNoException();
                ajm.a(parcel2, b2);
                break;
            case 3:
                beg c = c(parcel.readString());
                parcel2.writeNoException();
                ajm.a(parcel2, (IInterface) c);
                break;
            default:
                return false;
        }
        return true;
    }
}
