package com.google.android.gms.internal.ads;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.ads.b.j;

public abstract class apr extends ajl implements apq {
    public apr() {
        super("com.google.android.gms.ads.internal.client.IAdLoaderBuilder");
    }

    /* JADX WARNING: type inference failed for: r0v0 */
    /* JADX WARNING: type inference failed for: r0v12, types: [com.google.android.gms.internal.ads.aqm] */
    /* JADX WARNING: type inference failed for: r0v13, types: [com.google.android.gms.internal.ads.aqk] */
    /* JADX WARNING: type inference failed for: r0v14, types: [com.google.android.gms.internal.ads.aqk] */
    /* JADX WARNING: type inference failed for: r0v25, types: [com.google.android.gms.internal.ads.apm] */
    /* JADX WARNING: type inference failed for: r0v26, types: [com.google.android.gms.internal.ads.apk] */
    /* JADX WARNING: type inference failed for: r0v27, types: [com.google.android.gms.internal.ads.apk] */
    /* JADX WARNING: type inference failed for: r0v30 */
    /* JADX WARNING: type inference failed for: r0v31 */
    /* JADX WARNING: type inference failed for: r0v32 */
    /* JADX WARNING: type inference failed for: r0v33 */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r0v0
  assigns: [?[int, float, boolean, short, byte, char, OBJECT, ARRAY], com.google.android.gms.internal.ads.apm, com.google.android.gms.internal.ads.aqm, com.google.android.gms.internal.ads.aqk, com.google.android.gms.internal.ads.apk]
  uses: [com.google.android.gms.internal.ads.aqk, com.google.android.gms.internal.ads.apk]
  mth insns count: 67
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
    	at jadx.core.ProcessClass.process(ProcessClass.java:35)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Unknown variable types count: 5 */
    public final boolean dispatchTransaction(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        ? r0 = 0;
        switch (i) {
            case 1:
                apn a2 = a();
                parcel2.writeNoException();
                ajm.a(parcel2, (IInterface) a2);
                break;
            case 2:
                IBinder readStrongBinder = parcel.readStrongBinder();
                if (readStrongBinder != null) {
                    IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.IAdListener");
                    r0 = queryLocalInterface instanceof apk ? (apk) queryLocalInterface : new apm(readStrongBinder);
                }
                a((apk) r0);
                parcel2.writeNoException();
                break;
            case 3:
                a(avy.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
            case 4:
                a(awc.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
            case 5:
                a(parcel.readString(), awi.a(parcel.readStrongBinder()), awf.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
            case 6:
                a((aul) ajm.a(parcel, aul.CREATOR));
                parcel2.writeNoException();
                break;
            case 7:
                IBinder readStrongBinder2 = parcel.readStrongBinder();
                if (readStrongBinder2 != null) {
                    IInterface queryLocalInterface2 = readStrongBinder2.queryLocalInterface("com.google.android.gms.ads.internal.client.ICorrelationIdProvider");
                    r0 = queryLocalInterface2 instanceof aqk ? (aqk) queryLocalInterface2 : new aqm(readStrongBinder2);
                }
                a((aqk) r0);
                parcel2.writeNoException();
                break;
            case 8:
                a(awl.a(parcel.readStrongBinder()), (aot) ajm.a(parcel, aot.CREATOR));
                parcel2.writeNoException();
                break;
            case 9:
                a((j) ajm.a(parcel, j.CREATOR));
                parcel2.writeNoException();
                break;
            case 10:
                a(awo.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
            default:
                return false;
        }
        return true;
    }
}
