package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.app.Application;
import android.app.Application.ActivityLifecycleCallbacks;
import android.app.KeyguardManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.PowerManager;
import android.os.SystemClock;
import android.view.View;
import android.view.View.OnAttachStateChangeListener;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.ViewTreeObserver.OnScrollChangedListener;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import java.lang.ref.WeakReference;

public final class aii implements ActivityLifecycleCallbacks, OnAttachStateChangeListener, OnGlobalLayoutListener, OnScrollChangedListener {

    /* renamed from: a reason: collision with root package name */
    private static final Handler f2624a = new Handler(Looper.getMainLooper());

    /* renamed from: b reason: collision with root package name */
    private final Context f2625b;
    private Application c;
    private final PowerManager d;
    private final KeyguardManager e;
    private final ahy f;
    private BroadcastReceiver g;
    private WeakReference<ViewTreeObserver> h;
    private WeakReference<View> i;
    private ahm j;
    private boolean k = false;
    private int l = -1;
    private long m = -3;

    public aii(ahy ahy, View view) {
        this.f = ahy;
        this.f2625b = ahy.f2607a;
        this.d = (PowerManager) this.f2625b.getSystemService("power");
        this.e = (KeyguardManager) this.f2625b.getSystemService("keyguard");
        if (this.f2625b instanceof Application) {
            this.c = (Application) this.f2625b;
            this.j = new ahm((Application) this.f2625b, this);
        }
        a(view);
    }

    private final void a(Activity activity, int i2) {
        if (this.i != null) {
            Window window = activity.getWindow();
            if (window != null) {
                View peekDecorView = window.peekDecorView();
                View view = (View) this.i.get();
                if (view != null && peekDecorView != null && view.getRootView() == peekDecorView.getRootView()) {
                    this.l = i2;
                }
            }
        }
    }

    private final void b() {
        f2624a.post(new aij(this));
    }

    private final void b(View view) {
        ViewTreeObserver viewTreeObserver = view.getViewTreeObserver();
        if (viewTreeObserver.isAlive()) {
            this.h = new WeakReference<>(viewTreeObserver);
            viewTreeObserver.addOnScrollChangedListener(this);
            viewTreeObserver.addOnGlobalLayoutListener(this);
        }
        if (this.g == null) {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.intent.action.SCREEN_ON");
            intentFilter.addAction("android.intent.action.SCREEN_OFF");
            intentFilter.addAction("android.intent.action.USER_PRESENT");
            this.g = new aik(this);
            this.f2625b.registerReceiver(this.g, intentFilter);
        }
        if (this.c != null) {
            try {
                this.c.registerActivityLifecycleCallbacks(this.j);
            } catch (Exception e2) {
            }
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0051, code lost:
        if (r1 == false) goto L_0x0092;
     */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x005d  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x007f  */
    /* JADX WARNING: Removed duplicated region for block: B:45:? A[RETURN, SYNTHETIC] */
    public final void c() {
        boolean z;
        boolean z2;
        boolean z3 = true;
        if (this.i != null) {
            View view = (View) this.i.get();
            if (view == null) {
                this.m = -3;
                this.k = false;
                return;
            }
            boolean globalVisibleRect = view.getGlobalVisibleRect(new Rect());
            boolean localVisibleRect = view.getLocalVisibleRect(new Rect());
            if (!this.f.j()) {
                if (this.e.inKeyguardRestrictedInputMode()) {
                    Activity a2 = aig.a(view);
                    if (a2 != null) {
                        Window window = a2.getWindow();
                        LayoutParams attributes = window == null ? null : window.getAttributes();
                        if (!(attributes == null || (attributes.flags & 524288) == 0)) {
                            z2 = true;
                        }
                    }
                    z2 = false;
                }
                z = false;
                int windowVisibility = view.getWindowVisibility();
                if (this.l != -1) {
                    windowVisibility = this.l;
                }
                if (view.getVisibility() != 0 || !view.isShown() || !this.d.isScreenOn() || !z || !localVisibleRect || !globalVisibleRect || windowVisibility != 0) {
                    z3 = false;
                }
                if (this.k == z3) {
                    this.m = z3 ? SystemClock.elapsedRealtime() : -2;
                    this.k = z3;
                    return;
                }
                return;
            }
            z = true;
            int windowVisibility2 = view.getWindowVisibility();
            if (this.l != -1) {
            }
            z3 = false;
            if (this.k == z3) {
            }
        }
    }

    private final void c(View view) {
        try {
            if (this.h != null) {
                ViewTreeObserver viewTreeObserver = (ViewTreeObserver) this.h.get();
                if (viewTreeObserver != null && viewTreeObserver.isAlive()) {
                    viewTreeObserver.removeOnScrollChangedListener(this);
                    viewTreeObserver.removeGlobalOnLayoutListener(this);
                }
                this.h = null;
            }
        } catch (Exception e2) {
        }
        try {
            ViewTreeObserver viewTreeObserver2 = view.getViewTreeObserver();
            if (viewTreeObserver2.isAlive()) {
                viewTreeObserver2.removeOnScrollChangedListener(this);
                viewTreeObserver2.removeGlobalOnLayoutListener(this);
            }
        } catch (Exception e3) {
        }
        if (this.g != null) {
            try {
                this.f2625b.unregisterReceiver(this.g);
            } catch (Exception e4) {
            }
            this.g = null;
        }
        if (this.c != null) {
            try {
                this.c.unregisterActivityLifecycleCallbacks(this.j);
            } catch (Exception e5) {
            }
        }
    }

    public final long a() {
        if (this.m == -2 && this.i.get() == null) {
            this.m = -3;
        }
        return this.m;
    }

    /* access modifiers changed from: 0000 */
    public final void a(View view) {
        View view2 = this.i != null ? (View) this.i.get() : null;
        if (view2 != null) {
            view2.removeOnAttachStateChangeListener(this);
            c(view2);
        }
        this.i = new WeakReference<>(view);
        if (view != null) {
            if ((view.getWindowToken() == null && view.getWindowVisibility() == 8) ? false : true) {
                b(view);
            }
            view.addOnAttachStateChangeListener(this);
            this.m = -2;
            return;
        }
        this.m = -3;
    }

    public final void onActivityCreated(Activity activity, Bundle bundle) {
        a(activity, 0);
        c();
    }

    public final void onActivityDestroyed(Activity activity) {
        c();
    }

    public final void onActivityPaused(Activity activity) {
        a(activity, 4);
        c();
    }

    public final void onActivityResumed(Activity activity) {
        a(activity, 0);
        c();
        b();
    }

    public final void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        c();
    }

    public final void onActivityStarted(Activity activity) {
        a(activity, 0);
        c();
    }

    public final void onActivityStopped(Activity activity) {
        c();
    }

    public final void onGlobalLayout() {
        c();
    }

    public final void onScrollChanged() {
        c();
    }

    public final void onViewAttachedToWindow(View view) {
        this.l = -1;
        b(view);
        c();
    }

    public final void onViewDetachedFromWindow(View view) {
        this.l = -1;
        c();
        b();
        c(view);
    }
}
