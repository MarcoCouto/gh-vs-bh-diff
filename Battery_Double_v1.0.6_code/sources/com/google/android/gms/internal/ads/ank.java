package com.google.android.gms.internal.ads;

import java.io.IOException;

public final class ank extends afh<ank> {

    /* renamed from: a reason: collision with root package name */
    private Integer f2758a;

    /* renamed from: b reason: collision with root package name */
    private anx f2759b;
    private anx c;
    private anx d;
    private anx[] e;
    private Integer f;

    public ank() {
        this.f2758a = null;
        this.f2759b = null;
        this.c = null;
        this.d = null;
        this.e = anx.b();
        this.f = null;
        this.Y = null;
        this.Z = -1;
    }

    /* access modifiers changed from: protected */
    public final int a() {
        int a2 = super.a();
        if (this.f2758a != null) {
            a2 += aff.b(1, this.f2758a.intValue());
        }
        if (this.f2759b != null) {
            a2 += aff.b(2, (afn) this.f2759b);
        }
        if (this.c != null) {
            a2 += aff.b(3, (afn) this.c);
        }
        if (this.d != null) {
            a2 += aff.b(4, (afn) this.d);
        }
        if (this.e != null && this.e.length > 0) {
            int i = a2;
            for (anx anx : this.e) {
                if (anx != null) {
                    i += aff.b(5, (afn) anx);
                }
            }
            a2 = i;
        }
        return this.f != null ? a2 + aff.b(6, this.f.intValue()) : a2;
    }

    public final /* synthetic */ afn a(afd afd) throws IOException {
        while (true) {
            int a2 = afd.a();
            switch (a2) {
                case 0:
                    break;
                case 8:
                    this.f2758a = Integer.valueOf(afd.g());
                    continue;
                case 18:
                    if (this.f2759b == null) {
                        this.f2759b = new anx();
                    }
                    afd.a((afn) this.f2759b);
                    continue;
                case 26:
                    if (this.c == null) {
                        this.c = new anx();
                    }
                    afd.a((afn) this.c);
                    continue;
                case 34:
                    if (this.d == null) {
                        this.d = new anx();
                    }
                    afd.a((afn) this.d);
                    continue;
                case 42:
                    int a3 = afq.a(afd, 42);
                    int length = this.e == null ? 0 : this.e.length;
                    anx[] anxArr = new anx[(a3 + length)];
                    if (length != 0) {
                        System.arraycopy(this.e, 0, anxArr, 0, length);
                    }
                    while (length < anxArr.length - 1) {
                        anxArr[length] = new anx();
                        afd.a((afn) anxArr[length]);
                        afd.a();
                        length++;
                    }
                    anxArr[length] = new anx();
                    afd.a((afn) anxArr[length]);
                    this.e = anxArr;
                    continue;
                case 48:
                    this.f = Integer.valueOf(afd.g());
                    continue;
                default:
                    if (!super.a(afd, a2)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    public final void a(aff aff) throws IOException {
        if (this.f2758a != null) {
            aff.a(1, this.f2758a.intValue());
        }
        if (this.f2759b != null) {
            aff.a(2, (afn) this.f2759b);
        }
        if (this.c != null) {
            aff.a(3, (afn) this.c);
        }
        if (this.d != null) {
            aff.a(4, (afn) this.d);
        }
        if (this.e != null && this.e.length > 0) {
            for (anx anx : this.e) {
                if (anx != null) {
                    aff.a(5, (afn) anx);
                }
            }
        }
        if (this.f != null) {
            aff.a(6, this.f.intValue());
        }
        super.a(aff);
    }
}
