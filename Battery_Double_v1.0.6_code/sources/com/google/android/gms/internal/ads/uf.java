package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.xn.b;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public final class uf<P> {

    /* renamed from: a reason: collision with root package name */
    private static final Charset f3717a = Charset.forName("UTF-8");

    /* renamed from: b reason: collision with root package name */
    private ConcurrentMap<String, List<ug<P>>> f3718b = new ConcurrentHashMap();
    private ug<P> c;

    public final ug<P> a() {
        return this.c;
    }

    /* access modifiers changed from: protected */
    public final ug<P> a(P p, b bVar) throws GeneralSecurityException {
        byte[] bArr;
        switch (bVar.e()) {
            case LEGACY:
            case CRUNCHY:
                bArr = ByteBuffer.allocate(5).put(0).putInt(bVar.d()).array();
                break;
            case TINK:
                bArr = ByteBuffer.allocate(5).put(1).putInt(bVar.d()).array();
                break;
            case RAW:
                bArr = tv.f3713a;
                break;
            default:
                throw new GeneralSecurityException("unknown output prefix type");
        }
        ug<P> ugVar = new ug<>(p, bArr, bVar.c(), bVar.e());
        ArrayList arrayList = new ArrayList();
        arrayList.add(ugVar);
        String str = new String(ugVar.b(), f3717a);
        List list = (List) this.f3718b.put(str, Collections.unmodifiableList(arrayList));
        if (list != null) {
            ArrayList arrayList2 = new ArrayList();
            arrayList2.addAll(list);
            arrayList2.add(ugVar);
            this.f3718b.put(str, Collections.unmodifiableList(arrayList2));
        }
        return ugVar;
    }

    /* access modifiers changed from: protected */
    public final void a(ug<P> ugVar) {
        this.c = ugVar;
    }
}
