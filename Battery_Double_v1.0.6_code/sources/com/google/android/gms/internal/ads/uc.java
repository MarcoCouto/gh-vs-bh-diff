package com.google.android.gms.internal.ads;

import java.security.GeneralSecurityException;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;

public final class uc {

    /* renamed from: a reason: collision with root package name */
    private static final CopyOnWriteArrayList<ub> f3716a = new CopyOnWriteArrayList<>();

    public static ub a(String str) throws GeneralSecurityException {
        Iterator it = f3716a.iterator();
        while (it.hasNext()) {
            ub ubVar = (ub) it.next();
            if (ubVar.a(str)) {
                return ubVar;
            }
        }
        String str2 = "No KMS client does support: ";
        String valueOf = String.valueOf(str);
        throw new GeneralSecurityException(valueOf.length() != 0 ? str2.concat(valueOf) : new String(str2));
    }
}
