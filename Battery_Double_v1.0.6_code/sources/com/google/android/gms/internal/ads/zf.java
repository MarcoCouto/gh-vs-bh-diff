package com.google.android.gms.internal.ads;

public final class zf {

    /* renamed from: a reason: collision with root package name */
    private final byte[] f3811a;

    private zf(byte[] bArr, int i, int i2) {
        this.f3811a = new byte[i2];
        System.arraycopy(bArr, 0, this.f3811a, 0, i2);
    }

    public static zf a(byte[] bArr) {
        if (bArr == null) {
            return null;
        }
        return new zf(bArr, 0, bArr.length);
    }

    public final byte[] a() {
        byte[] bArr = new byte[this.f3811a.length];
        System.arraycopy(this.f3811a, 0, bArr, 0, this.f3811a.length);
        return bArr;
    }
}
