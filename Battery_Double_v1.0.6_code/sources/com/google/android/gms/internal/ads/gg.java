package com.google.android.gms.internal.ads;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public final class gg extends ajk implements gf {
    gg(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.reward.client.IRewardedAdSkuListener");
    }

    public final void a(gc gcVar, String str) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (IInterface) gcVar);
        r_.writeString(str);
        b(1, r_);
    }
}
