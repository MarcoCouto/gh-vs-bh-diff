package com.google.android.gms.internal.ads;

import android.content.Context;
import android.net.Uri;
import android.net.Uri.Builder;
import android.os.Environment;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.ax;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

@cm
public final class asl {

    /* renamed from: a reason: collision with root package name */
    private BlockingQueue<asv> f2880a = new ArrayBlockingQueue(100);

    /* renamed from: b reason: collision with root package name */
    private ExecutorService f2881b;
    private LinkedHashMap<String, String> c = new LinkedHashMap<>();
    private Map<String, asp> d = new HashMap();
    private String e;
    private Context f;
    private String g;
    private AtomicBoolean h;
    private File i;

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00a0 A[SYNTHETIC, Splitter:B:30:0x00a0] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00b1 A[SYNTHETIC, Splitter:B:37:0x00b1] */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0000 A[SYNTHETIC] */
    public final void a() {
        FileOutputStream fileOutputStream;
        while (true) {
            try {
                asv asv = (asv) this.f2880a.take();
                String b2 = asv.b();
                if (!TextUtils.isEmpty(b2)) {
                    Map a2 = a(this.c, asv.c());
                    Builder buildUpon = Uri.parse(this.e).buildUpon();
                    for (Entry entry : a2.entrySet()) {
                        buildUpon.appendQueryParameter((String) entry.getKey(), (String) entry.getValue());
                    }
                    StringBuilder sb = new StringBuilder(buildUpon.build().toString());
                    sb.append("&it=").append(b2);
                    String sb2 = sb.toString();
                    if (this.h.get()) {
                        File file = this.i;
                        if (file != null) {
                            try {
                                fileOutputStream = new FileOutputStream(file, true);
                                try {
                                    fileOutputStream.write(sb2.getBytes());
                                    fileOutputStream.write(10);
                                    try {
                                        fileOutputStream.close();
                                    } catch (IOException e2) {
                                        jm.c("CsiReporter: Cannot close file: sdk_csi_data.txt.", e2);
                                    }
                                } catch (IOException e3) {
                                    e = e3;
                                    try {
                                        jm.c("CsiReporter: Cannot write to file: sdk_csi_data.txt.", e);
                                        if (fileOutputStream == null) {
                                        }
                                    } catch (Throwable th) {
                                        th = th;
                                        if (fileOutputStream != null) {
                                        }
                                        throw th;
                                    }
                                }
                            } catch (IOException e4) {
                                e = e4;
                                fileOutputStream = null;
                                jm.c("CsiReporter: Cannot write to file: sdk_csi_data.txt.", e);
                                if (fileOutputStream == null) {
                                    try {
                                        fileOutputStream.close();
                                    } catch (IOException e5) {
                                        jm.c("CsiReporter: Cannot close file: sdk_csi_data.txt.", e5);
                                    }
                                }
                            } catch (Throwable th2) {
                                th = th2;
                                fileOutputStream = null;
                                if (fileOutputStream != null) {
                                    try {
                                        fileOutputStream.close();
                                    } catch (IOException e6) {
                                        jm.c("CsiReporter: Cannot close file: sdk_csi_data.txt.", e6);
                                    }
                                }
                                throw th;
                            }
                        } else {
                            jm.e("CsiReporter: File doesn't exists. Cannot write CSI data to file.");
                        }
                    } else {
                        ax.e();
                        jv.a(this.f, this.g, sb2);
                    }
                }
            } catch (InterruptedException e7) {
                jm.c("CsiReporter:reporter interrupted", e7);
                return;
            }
        }
    }

    public final asp a(String str) {
        asp asp = (asp) this.d.get(str);
        return asp != null ? asp : asp.f2883a;
    }

    /* access modifiers changed from: 0000 */
    public final Map<String, String> a(Map<String, String> map, Map<String, String> map2) {
        LinkedHashMap linkedHashMap = new LinkedHashMap(map);
        if (map2 == null) {
            return linkedHashMap;
        }
        for (Entry entry : map2.entrySet()) {
            String str = (String) entry.getKey();
            String str2 = (String) linkedHashMap.get(str);
            linkedHashMap.put(str, a(str).a(str2, (String) entry.getValue()));
        }
        return linkedHashMap;
    }

    public final void a(Context context, String str, String str2, Map<String, String> map) {
        this.f = context;
        this.g = str;
        this.e = str2;
        this.h = new AtomicBoolean(false);
        this.h.set(((Boolean) ape.f().a(asi.P)).booleanValue());
        if (this.h.get()) {
            File externalStorageDirectory = Environment.getExternalStorageDirectory();
            if (externalStorageDirectory != null) {
                this.i = new File(externalStorageDirectory, "sdk_csi_data.txt");
            }
        }
        for (Entry entry : map.entrySet()) {
            this.c.put((String) entry.getKey(), (String) entry.getValue());
        }
        this.f2881b = Executors.newSingleThreadExecutor();
        this.f2881b.execute(new asm(this));
        this.d.put("action", asp.f2884b);
        this.d.put("ad_format", asp.f2884b);
        this.d.put("e", asp.c);
    }

    public final void a(List<String> list) {
        if (list != null && !list.isEmpty()) {
            this.c.put("e", TextUtils.join(",", list));
        }
    }

    public final boolean a(asv asv) {
        return this.f2880a.offer(asv);
    }
}
