package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.app.Application.ActivityLifecycleCallbacks;

final class alb implements alf {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ Activity f2688a;

    alb(akw akw, Activity activity) {
        this.f2688a = activity;
    }

    public final void a(ActivityLifecycleCallbacks activityLifecycleCallbacks) {
        activityLifecycleCallbacks.onActivityStopped(this.f2688a);
    }
}
