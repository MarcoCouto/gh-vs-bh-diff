package com.google.android.gms.internal.ads;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.b.a;

public final class ajq extends ajk implements ajo {
    ajq(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.clearcut.IClearcut");
    }

    public final void a() throws RemoteException {
        b(3, r_());
    }

    public final void a(int i) throws RemoteException {
        Parcel r_ = r_();
        r_.writeInt(i);
        b(6, r_);
    }

    public final void a(a aVar, String str) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (IInterface) aVar);
        r_.writeString(str);
        b(2, r_);
    }

    public final void a(a aVar, String str, String str2) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (IInterface) aVar);
        r_.writeString(str);
        r_.writeString(null);
        b(8, r_);
    }

    public final void a(byte[] bArr) throws RemoteException {
        Parcel r_ = r_();
        r_.writeByteArray(bArr);
        b(5, r_);
    }

    public final void a(int[] iArr) throws RemoteException {
        Parcel r_ = r_();
        r_.writeIntArray(null);
        b(4, r_);
    }

    public final void b(int i) throws RemoteException {
        Parcel r_ = r_();
        r_.writeInt(i);
        b(7, r_);
    }
}
