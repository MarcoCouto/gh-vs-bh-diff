package com.google.android.gms.internal.ads;

import java.lang.Comparable;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

class ads<K extends Comparable<K>, V> extends AbstractMap<K, V> {

    /* renamed from: a reason: collision with root package name */
    private final int f2488a;
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public List<adz> f2489b;
    /* access modifiers changed from: private */
    public Map<K, V> c;
    private boolean d;
    private volatile aeb e;
    /* access modifiers changed from: private */
    public Map<K, V> f;
    private volatile adv g;

    private ads(int i) {
        this.f2488a = i;
        this.f2489b = Collections.emptyList();
        this.c = Collections.emptyMap();
        this.f = Collections.emptyMap();
    }

    /* synthetic */ ads(int i, adt adt) {
        this(i);
    }

    private final int a(K k) {
        int size = this.f2489b.size() - 1;
        if (size >= 0) {
            int compareTo = k.compareTo((Comparable) ((adz) this.f2489b.get(size)).getKey());
            if (compareTo > 0) {
                return -(size + 2);
            }
            if (compareTo == 0) {
                return size;
            }
        }
        int i = 0;
        int i2 = size;
        while (i <= i2) {
            int i3 = (i + i2) / 2;
            int compareTo2 = k.compareTo((Comparable) ((adz) this.f2489b.get(i3)).getKey());
            if (compareTo2 < 0) {
                i2 = i3 - 1;
            } else if (compareTo2 <= 0) {
                return i3;
            } else {
                i = i3 + 1;
            }
        }
        return -(i + 1);
    }

    static <FieldDescriptorType extends abj<FieldDescriptorType>> ads<FieldDescriptorType, Object> a(int i) {
        return new adt(i);
    }

    /* access modifiers changed from: private */
    public final V c(int i) {
        f();
        V value = ((adz) this.f2489b.remove(i)).getValue();
        if (!this.c.isEmpty()) {
            Iterator it = g().entrySet().iterator();
            this.f2489b.add(new adz(this, (Entry) it.next()));
            it.remove();
        }
        return value;
    }

    /* access modifiers changed from: private */
    public final void f() {
        if (this.d) {
            throw new UnsupportedOperationException();
        }
    }

    private final SortedMap<K, V> g() {
        f();
        if (this.c.isEmpty() && !(this.c instanceof TreeMap)) {
            this.c = new TreeMap();
            this.f = ((TreeMap) this.c).descendingMap();
        }
        return (SortedMap) this.c;
    }

    /* renamed from: a */
    public final V put(K k, V v) {
        f();
        int a2 = a(k);
        if (a2 >= 0) {
            return ((adz) this.f2489b.get(a2)).setValue(v);
        }
        f();
        if (this.f2489b.isEmpty() && !(this.f2489b instanceof ArrayList)) {
            this.f2489b = new ArrayList(this.f2488a);
        }
        int i = -(a2 + 1);
        if (i >= this.f2488a) {
            return g().put(k, v);
        }
        if (this.f2489b.size() == this.f2488a) {
            adz adz = (adz) this.f2489b.remove(this.f2488a - 1);
            g().put((Comparable) adz.getKey(), adz.getValue());
        }
        this.f2489b.add(i, new adz(this, k, v));
        return null;
    }

    public void a() {
        if (!this.d) {
            this.c = this.c.isEmpty() ? Collections.emptyMap() : Collections.unmodifiableMap(this.c);
            this.f = this.f.isEmpty() ? Collections.emptyMap() : Collections.unmodifiableMap(this.f);
            this.d = true;
        }
    }

    public final Entry<K, V> b(int i) {
        return (Entry) this.f2489b.get(i);
    }

    public final boolean b() {
        return this.d;
    }

    public final int c() {
        return this.f2489b.size();
    }

    public void clear() {
        f();
        if (!this.f2489b.isEmpty()) {
            this.f2489b.clear();
        }
        if (!this.c.isEmpty()) {
            this.c.clear();
        }
    }

    public boolean containsKey(Object obj) {
        Comparable comparable = (Comparable) obj;
        return a((K) comparable) >= 0 || this.c.containsKey(comparable);
    }

    public final Iterable<Entry<K, V>> d() {
        return this.c.isEmpty() ? adw.a() : this.c.entrySet();
    }

    /* access modifiers changed from: 0000 */
    public final Set<Entry<K, V>> e() {
        if (this.g == null) {
            this.g = new adv(this, null);
        }
        return this.g;
    }

    public Set<Entry<K, V>> entrySet() {
        if (this.e == null) {
            this.e = new aeb(this, null);
        }
        return this.e;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ads)) {
            return super.equals(obj);
        }
        ads ads = (ads) obj;
        int size = size();
        if (size != ads.size()) {
            return false;
        }
        int c2 = c();
        if (c2 != ads.c()) {
            return entrySet().equals(ads.entrySet());
        }
        for (int i = 0; i < c2; i++) {
            if (!b(i).equals(ads.b(i))) {
                return false;
            }
        }
        if (c2 != size) {
            return this.c.equals(ads.c);
        }
        return true;
    }

    public V get(Object obj) {
        Comparable comparable = (Comparable) obj;
        int a2 = a((K) comparable);
        return a2 >= 0 ? ((adz) this.f2489b.get(a2)).getValue() : this.c.get(comparable);
    }

    public int hashCode() {
        int i = 0;
        for (int i2 = 0; i2 < c(); i2++) {
            i += ((adz) this.f2489b.get(i2)).hashCode();
        }
        return this.c.size() > 0 ? this.c.hashCode() + i : i;
    }

    public V remove(Object obj) {
        f();
        Comparable comparable = (Comparable) obj;
        int a2 = a((K) comparable);
        if (a2 >= 0) {
            return c(a2);
        }
        if (this.c.isEmpty()) {
            return null;
        }
        return this.c.remove(comparable);
    }

    public int size() {
        return this.f2489b.size() + this.c.size();
    }
}
