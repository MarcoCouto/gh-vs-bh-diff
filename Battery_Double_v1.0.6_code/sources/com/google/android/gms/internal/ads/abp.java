package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.abp;
import com.google.android.gms.internal.ads.abp.a;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public abstract class abp<MessageType extends abp<MessageType, BuilderType>, BuilderType extends a<MessageType, BuilderType>> extends zw<MessageType, BuilderType> {
    private static Map<Object, abp<?, ?>> zzdtv = new ConcurrentHashMap();
    protected aej zzdtt = aej.a();
    private int zzdtu = -1;

    public static abstract class a<MessageType extends abp<MessageType, BuilderType>, BuilderType extends a<MessageType, BuilderType>> extends zx<MessageType, BuilderType> {

        /* renamed from: a reason: collision with root package name */
        protected MessageType f2433a;

        /* renamed from: b reason: collision with root package name */
        private final MessageType f2434b;
        private boolean c = false;

        protected a(MessageType messagetype) {
            this.f2434b = messagetype;
            this.f2433a = (abp) messagetype.a(e.d, (Object) null, (Object) null);
        }

        private static void a(MessageType messagetype, MessageType messagetype2) {
            adj.a().a(messagetype).b(messagetype, messagetype2);
        }

        public final BuilderType a(MessageType messagetype) {
            b();
            a(this.f2433a, messagetype);
            return this;
        }

        public final /* synthetic */ zx a() {
            return (a) clone();
        }

        /* access modifiers changed from: protected */
        public final void b() {
            if (this.c) {
                MessageType messagetype = (abp) this.f2433a.a(e.d, (Object) null, (Object) null);
                a(messagetype, this.f2433a);
                this.f2433a = messagetype;
                this.c = false;
            }
        }

        public final MessageType c() {
            MessageType messagetype;
            boolean z;
            if (this.c) {
                messagetype = this.f2433a;
            } else {
                MessageType messagetype2 = this.f2433a;
                adj.a().a(messagetype2).c(messagetype2);
                this.c = true;
                messagetype = this.f2433a;
            }
            MessageType messagetype3 = (abp) messagetype;
            boolean booleanValue = Boolean.TRUE.booleanValue();
            byte byteValue = ((Byte) messagetype3.a(e.f2436a, (Object) null, (Object) null)).byteValue();
            if (byteValue == 1) {
                z = true;
            } else if (byteValue == 0) {
                z = false;
            } else {
                boolean d = adj.a().a(messagetype3).d(messagetype3);
                if (booleanValue) {
                    messagetype3.a(e.f2437b, (Object) d ? messagetype3 : null, (Object) null);
                }
                z = d;
            }
            if (z) {
                return messagetype3;
            }
            throw new aeh(messagetype3);
        }

        public /* synthetic */ Object clone() throws CloneNotSupportedException {
            MessageType messagetype;
            a aVar = (a) ((abp) this.f2434b).a(e.e, (Object) null, (Object) null);
            if (this.c) {
                messagetype = this.f2433a;
            } else {
                MessageType messagetype2 = this.f2433a;
                adj.a().a(messagetype2).c(messagetype2);
                this.c = true;
                messagetype = this.f2433a;
            }
            aVar.a((MessageType) (abp) messagetype);
            return aVar;
        }

        public final /* synthetic */ acw d() {
            if (this.c) {
                return this.f2433a;
            }
            MessageType messagetype = this.f2433a;
            adj.a().a(messagetype).c(messagetype);
            this.c = true;
            return this.f2433a;
        }

        public final /* synthetic */ acw e() {
            MessageType messagetype;
            boolean z;
            if (this.c) {
                messagetype = this.f2433a;
            } else {
                MessageType messagetype2 = this.f2433a;
                adj.a().a(messagetype2).c(messagetype2);
                this.c = true;
                messagetype = this.f2433a;
            }
            abp abp = (abp) messagetype;
            boolean booleanValue = Boolean.TRUE.booleanValue();
            byte byteValue = ((Byte) abp.a(e.f2436a, (Object) null, (Object) null)).byteValue();
            if (byteValue == 1) {
                z = true;
            } else if (byteValue == 0) {
                z = false;
            } else {
                boolean d = adj.a().a(abp).d(abp);
                if (booleanValue) {
                    abp.a(e.f2437b, (Object) d ? abp : null, (Object) null);
                }
                z = d;
            }
            if (z) {
                return abp;
            }
            throw new aeh(abp);
        }

        public final boolean k() {
            return abp.a(this.f2433a, false);
        }

        public final /* synthetic */ acw p() {
            return this.f2434b;
        }
    }

    public static class b<T extends abp<T, ?>> extends aaa<T> {

        /* renamed from: a reason: collision with root package name */
        private T f2435a;

        public b(T t) {
            this.f2435a = t;
        }
    }

    public static abstract class c<MessageType extends c<MessageType, BuilderType>, BuilderType> extends abp<MessageType, BuilderType> implements acy {
        protected abh<Object> zzdtz = abh.a();
    }

    public static class d<ContainingType extends acw, Type> extends aaz<ContainingType, Type> {
    }

    /* 'enum' access flag removed */
    public static final class e {

        /* renamed from: a reason: collision with root package name */
        public static final int f2436a = 1;

        /* renamed from: b reason: collision with root package name */
        public static final int f2437b = 2;
        public static final int c = 3;
        public static final int d = 4;
        public static final int e = 5;
        public static final int f = 6;
        public static final int g = 7;
        public static final int h = 1;
        public static final int i = 2;
        public static final int j = 1;
        public static final int k = 2;
        private static final /* synthetic */ int[] l = {f2436a, f2437b, c, d, e, f, g};
        private static final /* synthetic */ int[] m = {h, i};
        private static final /* synthetic */ int[] n = {j, k};

        public static int[] a() {
            return (int[]) l.clone();
        }
    }

    protected static <T extends abp<T, ?>> T a(T t, aah aah) throws abv {
        boolean z;
        boolean z2;
        T a2 = a(t, aah, abc.a());
        if (a2 != null) {
            boolean booleanValue = Boolean.TRUE.booleanValue();
            byte byteValue = ((Byte) a2.a(e.f2436a, (Object) null, (Object) null)).byteValue();
            if (byteValue == 1) {
                z2 = true;
            } else if (byteValue == 0) {
                z2 = false;
            } else {
                boolean d2 = adj.a().a(a2).d(a2);
                if (booleanValue) {
                    a2.a(e.f2437b, (Object) d2 ? a2 : null, (Object) null);
                }
                z2 = d2;
            }
            if (!z2) {
                throw new aeh(a2).a().a(a2);
            }
        }
        if (a2 != null) {
            boolean booleanValue2 = Boolean.TRUE.booleanValue();
            byte byteValue2 = ((Byte) a2.a(e.f2436a, (Object) null, (Object) null)).byteValue();
            if (byteValue2 == 1) {
                z = true;
            } else if (byteValue2 == 0) {
                z = false;
            } else {
                boolean d3 = adj.a().a(a2).d(a2);
                if (booleanValue2) {
                    a2.a(e.f2437b, (Object) d3 ? a2 : null, (Object) null);
                }
                z = d3;
            }
            if (!z) {
                throw new aeh(a2).a().a(a2);
            }
        }
        return a2;
    }

    private static <T extends abp<T, ?>> T a(T t, aah aah, abc abc) throws abv {
        T a2;
        try {
            aaq e2 = aah.e();
            a2 = a(t, e2, abc);
            e2.a(0);
            return a2;
        } catch (abv e3) {
            throw e3.a(a2);
        } catch (abv e4) {
            throw e4;
        }
    }

    private static <T extends abp<T, ?>> T a(T t, aaq aaq, abc abc) throws abv {
        T t2 = (abp) t.a(e.d, (Object) null, (Object) null);
        try {
            adj.a().a(t2).a(t2, aat.a(aaq), abc);
            adj.a().a(t2).c(t2);
            return t2;
        } catch (IOException e2) {
            if (e2.getCause() instanceof abv) {
                throw ((abv) e2.getCause());
            }
            throw new abv(e2.getMessage()).a(t2);
        } catch (RuntimeException e3) {
            if (e3.getCause() instanceof abv) {
                throw ((abv) e3.getCause());
            }
            throw e3;
        }
    }

    protected static <T extends abp<T, ?>> T a(T t, byte[] bArr) throws abv {
        boolean z;
        T b2 = b(t, bArr);
        if (b2 != null) {
            boolean booleanValue = Boolean.TRUE.booleanValue();
            byte byteValue = ((Byte) b2.a(e.f2436a, (Object) null, (Object) null)).byteValue();
            if (byteValue == 1) {
                z = true;
            } else if (byteValue == 0) {
                z = false;
            } else {
                boolean d2 = adj.a().a(b2).d(b2);
                if (booleanValue) {
                    b2.a(e.f2437b, (Object) d2 ? b2 : null, (Object) null);
                }
                z = d2;
            }
            if (!z) {
                throw new aeh(b2).a().a(b2);
            }
        }
        return b2;
    }

    static <T extends abp<?, ?>> T a(Class<T> cls) {
        T t = (abp) zzdtv.get(cls);
        if (t == null) {
            try {
                Class.forName(cls.getName(), true, cls.getClassLoader());
                t = (abp) zzdtv.get(cls);
            } catch (ClassNotFoundException e2) {
                throw new IllegalStateException("Class initialization cannot fail.", e2);
            }
        }
        if (t != null) {
            return t;
        }
        String str = "Unable to get default instance for: ";
        String valueOf = String.valueOf(cls.getName());
        throw new IllegalStateException(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
    }

    protected static Object a(acw acw, String str, Object[] objArr) {
        return new adl(acw, str, objArr);
    }

    static Object a(Method method, Object obj, Object... objArr) {
        try {
            return method.invoke(obj, objArr);
        } catch (IllegalAccessException e2) {
            throw new RuntimeException("Couldn't use Java reflection to implement protocol message reflection.", e2);
        } catch (InvocationTargetException e3) {
            Throwable cause = e3.getCause();
            if (cause instanceof RuntimeException) {
                throw ((RuntimeException) cause);
            } else if (cause instanceof Error) {
                throw ((Error) cause);
            } else {
                throw new RuntimeException("Unexpected exception thrown by generated accessor method.", cause);
            }
        }
    }

    protected static <T extends abp<?, ?>> void a(Class<T> cls, T t) {
        zzdtv.put(cls, t);
    }

    protected static final <T extends abp<T, ?>> boolean a(T t, boolean z) {
        byte byteValue = ((Byte) t.a(e.f2436a, (Object) null, (Object) null)).byteValue();
        if (byteValue == 1) {
            return true;
        }
        if (byteValue == 0) {
            return false;
        }
        return adj.a().a(t).d(t);
    }

    private static <T extends abp<T, ?>> T b(T t, byte[] bArr) throws abv {
        T t2 = (abp) t.a(e.d, (Object) null, (Object) null);
        try {
            adj.a().a(t2).a(t2, bArr, 0, bArr.length, new aae());
            adj.a().a(t2).c(t2);
            if (t2.zzdpf == 0) {
                return t2;
            }
            throw new RuntimeException();
        } catch (IOException e2) {
            if (e2.getCause() instanceof abv) {
                throw ((abv) e2.getCause());
            }
            throw new abv(e2.getMessage()).a(t2);
        } catch (IndexOutOfBoundsException e3) {
            throw abv.a().a(t2);
        }
    }

    protected static <E> abu<E> m() {
        return adk.d();
    }

    /* access modifiers changed from: protected */
    public abstract Object a(int i, Object obj, Object obj2);

    /* access modifiers changed from: 0000 */
    public final void a(int i) {
        this.zzdtu = i;
    }

    public final void a(aav aav) throws IOException {
        adj.a().a(getClass()).a(this, (afc) aax.a(aav));
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!((abp) a(e.f, (Object) null, (Object) null)).getClass().isInstance(obj)) {
            return false;
        }
        return adj.a().a(this).a(this, (abp) obj);
    }

    public int hashCode() {
        if (this.zzdpf != 0) {
            return this.zzdpf;
        }
        this.zzdpf = adj.a().a(this).a(this);
        return this.zzdpf;
    }

    /* access modifiers changed from: 0000 */
    public final int j() {
        return this.zzdtu;
    }

    public final boolean k() {
        boolean booleanValue = Boolean.TRUE.booleanValue();
        byte byteValue = ((Byte) a(e.f2436a, (Object) null, (Object) null)).byteValue();
        if (byteValue == 1) {
            return true;
        }
        if (byteValue == 0) {
            return false;
        }
        boolean d2 = adj.a().a(this).d(this);
        if (booleanValue) {
            a(e.f2437b, (Object) d2 ? this : null, (Object) null);
        }
        return d2;
    }

    public final int l() {
        if (this.zzdtu == -1) {
            this.zzdtu = adj.a().a(this).b(this);
        }
        return this.zzdtu;
    }

    public final /* synthetic */ acx n() {
        a aVar = (a) a(e.e, (Object) null, (Object) null);
        aVar.a(this);
        return aVar;
    }

    public final /* synthetic */ acx o() {
        return (a) a(e.e, (Object) null, (Object) null);
    }

    public final /* synthetic */ acw p() {
        return (abp) a(e.f, (Object) null, (Object) null);
    }

    public String toString() {
        return acz.a(this, super.toString());
    }
}
