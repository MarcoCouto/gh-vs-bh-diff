package com.google.android.gms.internal.ads;

import java.util.concurrent.AbstractExecutorService;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.RunnableFuture;

@cm
public abstract class mw extends AbstractExecutorService implements ns {
    /* renamed from: a */
    public final nn<?> submit(Runnable runnable) throws RejectedExecutionException {
        return (nn) super.submit(runnable);
    }

    /* renamed from: a */
    public final <T> nn<T> submit(Callable<T> callable) throws RejectedExecutionException {
        return (nn) super.submit(callable);
    }

    /* access modifiers changed from: protected */
    public final <T> RunnableFuture<T> newTaskFor(Runnable runnable, T t) {
        return new nr(runnable, t);
    }

    /* access modifiers changed from: protected */
    public final <T> RunnableFuture<T> newTaskFor(Callable<T> callable) {
        return new nr(callable);
    }

    public /* synthetic */ Future submit(Runnable runnable, Object obj) {
        return (nn) super.submit(runnable, obj);
    }
}
