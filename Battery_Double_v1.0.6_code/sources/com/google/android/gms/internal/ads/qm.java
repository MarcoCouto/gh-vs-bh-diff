package com.google.android.gms.internal.ads;

import android.webkit.ConsoleMessage.MessageLevel;

final /* synthetic */ class qm {

    /* renamed from: a reason: collision with root package name */
    static final /* synthetic */ int[] f3636a = new int[MessageLevel.values().length];

    static {
        try {
            f3636a[MessageLevel.ERROR.ordinal()] = 1;
        } catch (NoSuchFieldError e) {
        }
        try {
            f3636a[MessageLevel.WARNING.ordinal()] = 2;
        } catch (NoSuchFieldError e2) {
        }
        try {
            f3636a[MessageLevel.LOG.ordinal()] = 3;
        } catch (NoSuchFieldError e3) {
        }
        try {
            f3636a[MessageLevel.TIP.ordinal()] = 4;
        } catch (NoSuchFieldError e4) {
        }
        try {
            f3636a[MessageLevel.DEBUG.ordinal()] = 5;
        } catch (NoSuchFieldError e5) {
        }
    }
}
