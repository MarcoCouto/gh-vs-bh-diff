package com.google.android.gms.internal.ads;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

final class ju implements ThreadFactory {

    /* renamed from: a reason: collision with root package name */
    private final AtomicInteger f3438a = new AtomicInteger(1);

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ String f3439b;

    ju(String str) {
        this.f3439b = str;
    }

    public final Thread newThread(Runnable runnable) {
        String str = this.f3439b;
        return new Thread(runnable, new StringBuilder(String.valueOf(str).length() + 23).append("AdWorker(").append(str).append(") #").append(this.f3438a.getAndIncrement()).toString());
    }
}
