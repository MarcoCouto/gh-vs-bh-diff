package com.google.android.gms.internal.ads;

import android.os.IInterface;
import android.os.RemoteException;

public interface apn extends IInterface {
    String a() throws RemoteException;

    void a(aop aop) throws RemoteException;

    void a(aop aop, int i) throws RemoteException;

    String b() throws RemoteException;

    boolean c() throws RemoteException;
}
