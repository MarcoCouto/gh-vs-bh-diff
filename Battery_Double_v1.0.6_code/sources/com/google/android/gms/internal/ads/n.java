package com.google.android.gms.internal.ads;

import org.json.JSONException;
import org.json.JSONObject;

@cm
public class n {

    /* renamed from: a reason: collision with root package name */
    private final qn f3530a;

    /* renamed from: b reason: collision with root package name */
    private final String f3531b;

    public n(qn qnVar) {
        this(qnVar, "");
    }

    public n(qn qnVar, String str) {
        this.f3530a = qnVar;
        this.f3531b = str;
    }

    public final void a(int i, int i2, int i3, int i4) {
        try {
            this.f3530a.a("onSizeChanged", new JSONObject().put("x", i).put("y", i2).put("width", i3).put("height", i4));
        } catch (JSONException e) {
            jm.b("Error occured while dispatching size change.", e);
        }
    }

    public final void a(int i, int i2, int i3, int i4, float f, int i5) {
        try {
            this.f3530a.a("onScreenInfoChanged", new JSONObject().put("width", i).put("height", i2).put("maxSizeWidth", i3).put("maxSizeHeight", i4).put("density", (double) f).put("rotation", i5));
        } catch (JSONException e) {
            jm.b("Error occured while obtaining screen information.", e);
        }
    }

    public final void a(String str) {
        try {
            this.f3530a.a("onError", new JSONObject().put("message", str).put("action", this.f3531b));
        } catch (JSONException e) {
            jm.b("Error occurred while dispatching error event.", e);
        }
    }

    public final void b(int i, int i2, int i3, int i4) {
        try {
            this.f3530a.a("onDefaultPositionReceived", new JSONObject().put("x", i).put("y", i2).put("width", i3).put("height", i4));
        } catch (JSONException e) {
            jm.b("Error occured while dispatching default position.", e);
        }
    }

    public final void b(String str) {
        try {
            this.f3530a.a("onReadyEventReceived", new JSONObject().put("js", str));
        } catch (JSONException e) {
            jm.b("Error occured while dispatching ready Event.", e);
        }
    }

    public final void c(String str) {
        try {
            this.f3530a.a("onStateChanged", new JSONObject().put("state", str));
        } catch (JSONException e) {
            jm.b("Error occured while dispatching state change.", e);
        }
    }
}
