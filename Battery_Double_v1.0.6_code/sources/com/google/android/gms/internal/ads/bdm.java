package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.b.a;
import com.google.android.gms.b.a.C0046a;
import java.util.ArrayList;
import java.util.List;

public final class bdm extends ajk implements bdk {
    bdm(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.mediation.client.IUnifiedNativeAdMapper");
    }

    public final String a() throws RemoteException {
        Parcel a2 = a(2, r_());
        String readString = a2.readString();
        a2.recycle();
        return readString;
    }

    public final void a(a aVar) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (IInterface) aVar);
        b(20, r_);
    }

    public final void a(a aVar, a aVar2, a aVar3) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (IInterface) aVar);
        ajm.a(r_, (IInterface) aVar2);
        ajm.a(r_, (IInterface) aVar3);
        b(21, r_);
    }

    public final List b() throws RemoteException {
        Parcel a2 = a(3, r_());
        ArrayList b2 = ajm.b(a2);
        a2.recycle();
        return b2;
    }

    public final void b(a aVar) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (IInterface) aVar);
        b(22, r_);
    }

    public final String c() throws RemoteException {
        Parcel a2 = a(4, r_());
        String readString = a2.readString();
        a2.recycle();
        return readString;
    }

    public final auw d() throws RemoteException {
        Parcel a2 = a(5, r_());
        auw a3 = aux.a(a2.readStrongBinder());
        a2.recycle();
        return a3;
    }

    public final String e() throws RemoteException {
        Parcel a2 = a(6, r_());
        String readString = a2.readString();
        a2.recycle();
        return readString;
    }

    public final String f() throws RemoteException {
        Parcel a2 = a(7, r_());
        String readString = a2.readString();
        a2.recycle();
        return readString;
    }

    public final double g() throws RemoteException {
        Parcel a2 = a(8, r_());
        double readDouble = a2.readDouble();
        a2.recycle();
        return readDouble;
    }

    public final String h() throws RemoteException {
        Parcel a2 = a(9, r_());
        String readString = a2.readString();
        a2.recycle();
        return readString;
    }

    public final String i() throws RemoteException {
        Parcel a2 = a(10, r_());
        String readString = a2.readString();
        a2.recycle();
        return readString;
    }

    public final aqs j() throws RemoteException {
        Parcel a2 = a(11, r_());
        aqs a3 = aqt.a(a2.readStrongBinder());
        a2.recycle();
        return a3;
    }

    public final aus k() throws RemoteException {
        Parcel a2 = a(12, r_());
        aus a3 = aut.a(a2.readStrongBinder());
        a2.recycle();
        return a3;
    }

    public final a l() throws RemoteException {
        Parcel a2 = a(13, r_());
        a a3 = C0046a.a(a2.readStrongBinder());
        a2.recycle();
        return a3;
    }

    public final a m() throws RemoteException {
        Parcel a2 = a(14, r_());
        a a3 = C0046a.a(a2.readStrongBinder());
        a2.recycle();
        return a3;
    }

    public final a n() throws RemoteException {
        Parcel a2 = a(15, r_());
        a a3 = C0046a.a(a2.readStrongBinder());
        a2.recycle();
        return a3;
    }

    public final Bundle o() throws RemoteException {
        Parcel a2 = a(16, r_());
        Bundle bundle = (Bundle) ajm.a(a2, Bundle.CREATOR);
        a2.recycle();
        return bundle;
    }

    public final boolean p() throws RemoteException {
        Parcel a2 = a(17, r_());
        boolean a3 = ajm.a(a2);
        a2.recycle();
        return a3;
    }

    public final boolean q() throws RemoteException {
        Parcel a2 = a(18, r_());
        boolean a3 = ajm.a(a2);
        a2.recycle();
        return a3;
    }

    public final void r() throws RemoteException {
        b(19, r_());
    }
}
