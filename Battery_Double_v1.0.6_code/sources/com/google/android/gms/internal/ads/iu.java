package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.os.SystemClock;

@cm
final class iu {

    /* renamed from: a reason: collision with root package name */
    private long f3401a = -1;

    /* renamed from: b reason: collision with root package name */
    private long f3402b = -1;

    public final long a() {
        return this.f3402b;
    }

    public final void b() {
        this.f3402b = SystemClock.elapsedRealtime();
    }

    public final void c() {
        this.f3401a = SystemClock.elapsedRealtime();
    }

    public final Bundle d() {
        Bundle bundle = new Bundle();
        bundle.putLong("topen", this.f3401a);
        bundle.putLong("tclose", this.f3402b);
        return bundle;
    }
}
