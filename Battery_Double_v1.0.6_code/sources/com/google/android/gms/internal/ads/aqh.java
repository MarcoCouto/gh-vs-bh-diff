package com.google.android.gms.internal.ads;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.b.a;

public interface aqh extends IInterface {
    apq createAdLoaderBuilder(a aVar, String str, bcr bcr, int i) throws RemoteException;

    r createAdOverlay(a aVar) throws RemoteException;

    apv createBannerAdManager(a aVar, aot aot, String str, bcr bcr, int i) throws RemoteException;

    ab createInAppPurchaseManager(a aVar) throws RemoteException;

    apv createInterstitialAdManager(a aVar, aot aot, String str, bcr bcr, int i) throws RemoteException;

    avb createNativeAdViewDelegate(a aVar, a aVar2) throws RemoteException;

    avg createNativeAdViewHolderDelegate(a aVar, a aVar2, a aVar3) throws RemoteException;

    gh createRewardedVideoAd(a aVar, bcr bcr, int i) throws RemoteException;

    apv createSearchAdManager(a aVar, aot aot, String str, int i) throws RemoteException;

    aqn getMobileAdsSettingsManager(a aVar) throws RemoteException;

    aqn getMobileAdsSettingsManagerWithClientJarVersion(a aVar, int i) throws RemoteException;
}
