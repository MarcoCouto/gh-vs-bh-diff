package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.ax;

@cm
public final class lw {

    /* renamed from: a reason: collision with root package name */
    private long f3497a;

    /* renamed from: b reason: collision with root package name */
    private long f3498b = Long.MIN_VALUE;
    private Object c = new Object();

    public lw(long j) {
        this.f3497a = j;
    }

    public final boolean a() {
        boolean z;
        synchronized (this.c) {
            long b2 = ax.l().b();
            if (this.f3498b + this.f3497a > b2) {
                z = false;
            } else {
                this.f3498b = b2;
                z = true;
            }
        }
        return z;
    }
}
