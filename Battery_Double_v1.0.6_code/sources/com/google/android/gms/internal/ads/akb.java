package com.google.android.gms.internal.ads;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

final class akb implements axz {

    /* renamed from: a reason: collision with root package name */
    private final Map<String, List<awb<?>>> f2657a = new HashMap();

    /* renamed from: b reason: collision with root package name */
    private final ahz f2658b;

    akb(ahz ahz) {
        this.f2658b = ahz;
    }

    /* access modifiers changed from: private */
    public final synchronized boolean b(awb<?> awb) {
        boolean z = false;
        synchronized (this) {
            String e = awb.e();
            if (this.f2657a.containsKey(e)) {
                List list = (List) this.f2657a.get(e);
                if (list == null) {
                    list = new ArrayList();
                }
                awb.b("waiting-for-response");
                list.add(awb);
                this.f2657a.put(e, list);
                if (eg.f3276a) {
                    eg.b("Request for cacheKey=%s is in flight, putting on hold.", e);
                }
                z = true;
            } else {
                this.f2657a.put(e, null);
                awb.a((axz) this);
                if (eg.f3276a) {
                    eg.b("new request, sending to network %s", e);
                }
            }
        }
        return z;
    }

    public final synchronized void a(awb<?> awb) {
        String e = awb.e();
        List list = (List) this.f2657a.remove(e);
        if (list != null && !list.isEmpty()) {
            if (eg.f3276a) {
                eg.a("%d waiting requests for cacheKey=%s; resend to network", Integer.valueOf(list.size()), e);
            }
            awb awb2 = (awb) list.remove(0);
            this.f2657a.put(e, list);
            awb2.a((axz) this);
            try {
                this.f2658b.c.put(awb2);
            } catch (InterruptedException e2) {
                eg.c("Couldn't add request to queue. %s", e2.toString());
                Thread.currentThread().interrupt();
                this.f2658b.a();
            }
        }
        return;
    }

    public final void a(awb<?> awb, bcd<?> bcd) {
        List<awb> list;
        if (bcd.f3124b == null || bcd.f3124b.a()) {
            a(awb);
            return;
        }
        String e = awb.e();
        synchronized (this) {
            list = (List) this.f2657a.remove(e);
        }
        if (list != null) {
            if (eg.f3276a) {
                eg.a("Releasing %d waiting requests for cacheKey=%s.", Integer.valueOf(list.size()), e);
            }
            for (awb a2 : list) {
                this.f2658b.e.a(a2, bcd);
            }
        }
    }
}
