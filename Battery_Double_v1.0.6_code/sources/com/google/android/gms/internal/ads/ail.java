package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.view.View;
import java.lang.reflect.InvocationTargetException;

public final class ail extends ajj {
    private final Activity d;
    private final View e;

    public ail(ahy ahy, String str, String str2, zz zzVar, int i, int i2, View view, Activity activity) {
        super(ahy, str, str2, zzVar, i, 62);
        this.e = view;
        this.d = activity;
    }

    /* access modifiers changed from: protected */
    public final void a() throws IllegalAccessException, InvocationTargetException {
        if (this.e != null) {
            boolean booleanValue = ((Boolean) ape.f().a(asi.bF)).booleanValue();
            Object[] objArr = (Object[]) this.c.invoke(null, new Object[]{this.e, this.d, Boolean.valueOf(booleanValue)});
            synchronized (this.f2637b) {
                this.f2637b.Q = Long.valueOf(((Long) objArr[0]).longValue());
                this.f2637b.R = Long.valueOf(((Long) objArr[1]).longValue());
                if (booleanValue) {
                    this.f2637b.S = (String) objArr[2];
                }
            }
        }
    }
}
