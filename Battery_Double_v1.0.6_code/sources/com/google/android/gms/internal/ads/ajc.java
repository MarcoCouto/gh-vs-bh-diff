package com.google.android.gms.internal.ads;

import java.lang.reflect.InvocationTargetException;

public final class ajc extends ajj {
    public ajc(ahy ahy, String str, String str2, zz zzVar, int i, int i2) {
        super(ahy, str, str2, zzVar, i, 51);
    }

    /* access modifiers changed from: protected */
    public final void a() throws IllegalAccessException, InvocationTargetException {
        synchronized (this.f2637b) {
            ahx ahx = new ahx((String) this.c.invoke(null, new Object[0]));
            this.f2637b.G = ahx.f2604a;
            this.f2637b.H = ahx.f2605b;
        }
    }
}
