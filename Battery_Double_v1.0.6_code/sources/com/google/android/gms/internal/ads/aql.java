package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.RemoteException;

public abstract class aql extends ajl implements aqk {
    /* access modifiers changed from: protected */
    public final boolean dispatchTransaction(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i != 1) {
            return false;
        }
        long a2 = a();
        parcel2.writeNoException();
        parcel2.writeLong(a2);
        return true;
    }
}
