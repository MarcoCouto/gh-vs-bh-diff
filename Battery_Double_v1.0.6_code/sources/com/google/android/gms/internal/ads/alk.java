package com.google.android.gms.internal.ads;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application;
import android.app.Application.ActivityLifecycleCallbacks;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import com.google.android.gms.ads.internal.ax;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@TargetApi(14)
final class alk implements ActivityLifecycleCallbacks {

    /* renamed from: a reason: collision with root package name */
    private Activity f2698a;

    /* renamed from: b reason: collision with root package name */
    private Context f2699b;
    /* access modifiers changed from: private */
    public final Object c = new Object();
    /* access modifiers changed from: private */
    public boolean d = true;
    /* access modifiers changed from: private */
    public boolean e = false;
    /* access modifiers changed from: private */
    public final List<alm> f = new ArrayList();
    private final List<alz> g = new ArrayList();
    private Runnable h;
    private boolean i = false;
    private long j;

    alk() {
    }

    private final void a(Activity activity) {
        synchronized (this.c) {
            if (!activity.getClass().getName().startsWith("com.google.android.gms.ads")) {
                this.f2698a = activity;
            }
        }
    }

    public final Activity a() {
        return this.f2698a;
    }

    public final void a(Application application, Context context) {
        if (!this.i) {
            application.registerActivityLifecycleCallbacks(this);
            if (context instanceof Activity) {
                a((Activity) context);
            }
            this.f2699b = application;
            this.j = ((Long) ape.f().a(asi.aH)).longValue();
            this.i = true;
        }
    }

    public final void a(alm alm) {
        synchronized (this.c) {
            this.f.add(alm);
        }
    }

    public final Context b() {
        return this.f2699b;
    }

    public final void onActivityCreated(Activity activity, Bundle bundle) {
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    public final void onActivityDestroyed(Activity activity) {
        synchronized (this.c) {
            if (this.f2698a != null) {
                if (this.f2698a.equals(activity)) {
                    this.f2698a = null;
                }
                Iterator it = this.g.iterator();
                while (it.hasNext()) {
                    try {
                        if (((alz) it.next()).c(activity)) {
                            it.remove();
                        }
                    } catch (Exception e2) {
                        ax.i().a((Throwable) e2, "AppActivityTracker.ActivityListener.onActivityDestroyed");
                        ms.b("", e2);
                    }
                }
            }
        }
    }

    public final void onActivityPaused(Activity activity) {
        a(activity);
        synchronized (this.c) {
            for (alz b2 : this.g) {
                try {
                    b2.b(activity);
                } catch (Exception e2) {
                    ax.i().a((Throwable) e2, "AppActivityTracker.ActivityListener.onActivityPaused");
                    ms.b("", e2);
                }
            }
        }
        this.e = true;
        if (this.h != null) {
            jv.f3440a.removeCallbacks(this.h);
        }
        Handler handler = jv.f3440a;
        all all = new all(this);
        this.h = all;
        handler.postDelayed(all, this.j);
    }

    public final void onActivityResumed(Activity activity) {
        boolean z = false;
        a(activity);
        this.e = false;
        if (!this.d) {
            z = true;
        }
        this.d = true;
        if (this.h != null) {
            jv.f3440a.removeCallbacks(this.h);
        }
        synchronized (this.c) {
            for (alz a2 : this.g) {
                try {
                    a2.a(activity);
                } catch (Exception e2) {
                    ax.i().a((Throwable) e2, "AppActivityTracker.ActivityListener.onActivityResumed");
                    ms.b("", e2);
                }
            }
            if (z) {
                for (alm a3 : this.f) {
                    try {
                        a3.a(true);
                    } catch (Exception e3) {
                        ms.b("", e3);
                    }
                }
            } else {
                jm.b("App is still foreground.");
            }
        }
    }

    public final void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
    }

    public final void onActivityStarted(Activity activity) {
        a(activity);
    }

    public final void onActivityStopped(Activity activity) {
    }
}
