package com.google.android.gms.internal.ads;

import java.security.GeneralSecurityException;
import java.security.InvalidAlgorithmParameterException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import javax.crypto.Mac;

public final class zh implements ud {

    /* renamed from: a reason: collision with root package name */
    private Mac f3812a;

    /* renamed from: b reason: collision with root package name */
    private final int f3813b;
    private final String c;
    private final Key d;

    public zh(String str, Key key, int i) throws GeneralSecurityException {
        if (i < 10) {
            throw new InvalidAlgorithmParameterException("tag size too small, need at least 10 bytes");
        }
        char c2 = 65535;
        switch (str.hashCode()) {
            case -1823053428:
                if (str.equals("HMACSHA1")) {
                    c2 = 0;
                    break;
                }
                break;
            case 392315118:
                if (str.equals("HMACSHA256")) {
                    c2 = 1;
                    break;
                }
                break;
            case 392317873:
                if (str.equals("HMACSHA512")) {
                    c2 = 2;
                    break;
                }
                break;
        }
        switch (c2) {
            case 0:
                if (i > 20) {
                    throw new InvalidAlgorithmParameterException("tag size too big");
                }
                break;
            case 1:
                if (i > 32) {
                    throw new InvalidAlgorithmParameterException("tag size too big");
                }
                break;
            case 2:
                if (i > 64) {
                    throw new InvalidAlgorithmParameterException("tag size too big");
                }
                break;
            default:
                String str2 = "unknown Hmac algorithm: ";
                String valueOf = String.valueOf(str);
                throw new NoSuchAlgorithmException(valueOf.length() != 0 ? str2.concat(valueOf) : new String(str2));
        }
        this.c = str;
        this.f3813b = i;
        this.d = key;
        this.f3812a = (Mac) yv.f3810b.a(str);
        this.f3812a.init(key);
    }

    public final byte[] a(byte[] bArr) throws GeneralSecurityException {
        Mac mac;
        try {
            mac = (Mac) this.f3812a.clone();
        } catch (CloneNotSupportedException e) {
            mac = (Mac) yv.f3810b.a(this.c);
            mac.init(this.d);
        }
        mac.update(bArr);
        byte[] bArr2 = new byte[this.f3813b];
        System.arraycopy(mac.doFinal(), 0, bArr2, 0, this.f3813b);
        return bArr2;
    }
}
