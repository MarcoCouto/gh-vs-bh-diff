package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.b.a;
import java.util.List;

public interface bdh extends IInterface {
    String a() throws RemoteException;

    void a(a aVar) throws RemoteException;

    void a(a aVar, a aVar2, a aVar3) throws RemoteException;

    List b() throws RemoteException;

    void b(a aVar) throws RemoteException;

    String c() throws RemoteException;

    void c(a aVar) throws RemoteException;

    auw d() throws RemoteException;

    String e() throws RemoteException;

    String f() throws RemoteException;

    void g() throws RemoteException;

    boolean h() throws RemoteException;

    boolean i() throws RemoteException;

    Bundle j() throws RemoteException;

    a k() throws RemoteException;

    aqs l() throws RemoteException;

    aus m() throws RemoteException;

    a n() throws RemoteException;

    a o() throws RemoteException;
}
