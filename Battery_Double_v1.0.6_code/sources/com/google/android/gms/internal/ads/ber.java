package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.a.b;

public final class ber implements Creator<beq> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int i = 0;
        int b2 = b.b(parcel);
        int i2 = 0;
        int i3 = 0;
        while (parcel.dataPosition() < b2) {
            int a2 = b.a(parcel);
            switch (b.a(a2)) {
                case 1:
                    i3 = b.d(parcel, a2);
                    break;
                case 2:
                    i2 = b.d(parcel, a2);
                    break;
                case 3:
                    i = b.d(parcel, a2);
                    break;
                default:
                    b.b(parcel, a2);
                    break;
            }
        }
        b.r(parcel, b2);
        return new beq(i3, i2, i);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new beq[i];
    }
}
