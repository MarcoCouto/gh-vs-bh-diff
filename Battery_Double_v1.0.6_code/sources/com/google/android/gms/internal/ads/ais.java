package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.ads.identifier.AdvertisingIdClient.Info;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

public final class ais extends ajj {
    public ais(ahy ahy, String str, String str2, zz zzVar, int i, int i2) {
        super(ahy, str, str2, zzVar, i, 24);
    }

    private final void c() {
        AdvertisingIdClient m = this.f2636a.m();
        if (m != null) {
            try {
                Info info = m.getInfo();
                String a2 = aig.a(info.getId());
                if (a2 != null) {
                    synchronized (this.f2637b) {
                        this.f2637b.T = a2;
                        this.f2637b.V = Boolean.valueOf(info.isLimitAdTrackingEnabled());
                        this.f2637b.U = Integer.valueOf(5);
                    }
                }
            } catch (IOException e) {
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void a() throws IllegalAccessException, InvocationTargetException {
        if (this.f2636a.g()) {
            c();
            return;
        }
        synchronized (this.f2637b) {
            this.f2637b.T = (String) this.c.invoke(null, new Object[]{this.f2636a.a()});
        }
    }

    public final Void b() throws Exception {
        if (this.f2636a.b()) {
            return super.call();
        }
        if (this.f2636a.g()) {
            c();
        }
        return null;
    }

    public final /* synthetic */ Object call() throws Exception {
        return call();
    }
}
