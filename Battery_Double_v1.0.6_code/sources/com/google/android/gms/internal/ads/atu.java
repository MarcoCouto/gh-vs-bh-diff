package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.view.View;
import com.google.android.gms.b.a;
import com.google.android.gms.b.b;
import java.util.List;

@cm
public final class atu extends awt implements auc {

    /* renamed from: a reason: collision with root package name */
    private String f2916a;

    /* renamed from: b reason: collision with root package name */
    private List<atm> f2917b;
    private String c;
    private auw d;
    private String e;
    private String f;
    private double g;
    private String h;
    private String i;
    private ati j;
    private aqs k;
    private View l;
    private a m;
    private String n;
    private Bundle o;
    private Object p = new Object();
    /* access modifiers changed from: private */
    public aty q;

    public atu(String str, List<atm> list, String str2, auw auw, String str3, String str4, double d2, String str5, String str6, ati ati, aqs aqs, View view, a aVar, String str7, Bundle bundle) {
        this.f2916a = str;
        this.f2917b = list;
        this.c = str2;
        this.d = auw;
        this.e = str3;
        this.f = str4;
        this.g = d2;
        this.h = str5;
        this.i = str6;
        this.j = ati;
        this.k = aqs;
        this.l = view;
        this.m = aVar;
        this.n = str7;
        this.o = bundle;
    }

    public final String a() {
        return this.f2916a;
    }

    public final void a(Bundle bundle) {
        synchronized (this.p) {
            if (this.q == null) {
                jm.c("#001 Attempt to perform click before app native ad initialized.");
            } else {
                this.q.b(bundle);
            }
        }
    }

    public final void a(aty aty) {
        synchronized (this.p) {
            this.q = aty;
        }
    }

    public final void a(awq awq) {
        this.q.a(awq);
    }

    public final List b() {
        return this.f2917b;
    }

    public final boolean b(Bundle bundle) {
        boolean a2;
        synchronized (this.p) {
            if (this.q == null) {
                jm.c("#002 Attempt to record impression before native ad initialized.");
                a2 = false;
            } else {
                a2 = this.q.a(bundle);
            }
        }
        return a2;
    }

    public final String c() {
        return this.c;
    }

    public final void c(Bundle bundle) {
        synchronized (this.p) {
            if (this.q == null) {
                jm.c("#003 Attempt to report touch event before native ad initialized.");
            } else {
                this.q.c(bundle);
            }
        }
    }

    public final auw d() {
        return this.d;
    }

    public final String e() {
        return this.e;
    }

    public final String f() {
        return this.f;
    }

    public final double g() {
        return this.g;
    }

    public final String h() {
        return this.h;
    }

    public final String i() {
        return this.i;
    }

    public final aqs j() {
        return this.k;
    }

    public final String k() {
        return "6";
    }

    public final String l() {
        return "";
    }

    public final ati m() {
        return this.j;
    }

    public final a n() {
        return b.a(this.q);
    }

    public final View o() {
        return this.l;
    }

    public final a p() {
        return this.m;
    }

    public final String q() {
        return this.n;
    }

    public final Bundle r() {
        return this.o;
    }

    public final aus s() {
        return this.j;
    }

    public final void t() {
        jv.f3440a.post(new atv(this));
    }

    public final void u() {
        this.q.c();
    }
}
