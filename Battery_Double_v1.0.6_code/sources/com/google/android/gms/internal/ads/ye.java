package com.google.android.gms.internal.ads;

import java.security.GeneralSecurityException;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public final class ye implements zg {

    /* renamed from: a reason: collision with root package name */
    private final SecretKeySpec f3788a;

    /* renamed from: b reason: collision with root package name */
    private final int f3789b;
    private final int c = ((Cipher) yv.f3809a.a("AES/CTR/NoPadding")).getBlockSize();

    public ye(byte[] bArr, int i) throws GeneralSecurityException {
        this.f3788a = new SecretKeySpec(bArr, "AES");
        if (i < 12 || i > this.c) {
            throw new GeneralSecurityException("invalid IV size");
        }
        this.f3789b = i;
    }

    public final byte[] a(byte[] bArr) throws GeneralSecurityException {
        if (bArr.length > Integer.MAX_VALUE - this.f3789b) {
            throw new GeneralSecurityException("plaintext length can not exceed " + (Integer.MAX_VALUE - this.f3789b));
        }
        byte[] bArr2 = new byte[(this.f3789b + bArr.length)];
        byte[] a2 = zj.a(this.f3789b);
        System.arraycopy(a2, 0, bArr2, 0, this.f3789b);
        int length = bArr.length;
        int i = this.f3789b;
        Cipher cipher = (Cipher) yv.f3809a.a("AES/CTR/NoPadding");
        byte[] bArr3 = new byte[this.c];
        System.arraycopy(a2, 0, bArr3, 0, this.f3789b);
        cipher.init(1, this.f3788a, new IvParameterSpec(bArr3));
        if (cipher.doFinal(bArr, 0, length, bArr2, i) == length) {
            return bArr2;
        }
        throw new GeneralSecurityException("stored output's length does not match input's length");
    }
}
