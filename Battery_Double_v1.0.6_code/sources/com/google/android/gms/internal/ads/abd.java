package com.google.android.gms.internal.ads;

final class abd {

    /* renamed from: a reason: collision with root package name */
    private final Object f2416a;

    /* renamed from: b reason: collision with root package name */
    private final int f2417b;

    abd(Object obj, int i) {
        this.f2416a = obj;
        this.f2417b = i;
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof abd)) {
            return false;
        }
        abd abd = (abd) obj;
        return this.f2416a == abd.f2416a && this.f2417b == abd.f2417b;
    }

    public final int hashCode() {
        return (System.identityHashCode(this.f2416a) * 65535) + this.f2417b;
    }
}
