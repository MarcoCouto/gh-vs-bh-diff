package com.google.android.gms.internal.ads;

import android.content.Context;
import java.io.File;
import java.util.Map;
import java.util.concurrent.Executor;

@cm
public final class lf {

    /* renamed from: a reason: collision with root package name */
    private static bab f3477a;

    /* renamed from: b reason: collision with root package name */
    private static final Object f3478b = new Object();
    @Deprecated
    private static final ll<Void> c = new lg();

    public lf(Context context) {
        if (context.getApplicationContext() != null) {
            context = context.getApplicationContext();
        }
        a(context);
    }

    private static bab a(Context context) {
        bab bab;
        bab bab2;
        synchronized (f3478b) {
            if (f3477a == null) {
                asi.a(context);
                if (((Boolean) ape.f().a(asi.cI)).booleanValue()) {
                    bab2 = kz.a(context);
                } else {
                    bab2 = new bab(new lm(new File(context.getCacheDir(), "volley")), new ij((hi) new rs()));
                    bab2.a();
                }
                f3477a = bab2;
            }
            bab = f3477a;
        }
        return bab;
    }

    public final nn<String> a(int i, String str, Map<String, String> map, byte[] bArr) {
        ln lnVar = new ln(null);
        lj ljVar = new lj(this, str, lnVar);
        ml mlVar = new ml(null);
        lk lkVar = new lk(this, i, str, lnVar, ljVar, bArr, map, mlVar);
        if (ml.c()) {
            try {
                mlVar.a(str, "GET", lkVar.b(), lkVar.a());
            } catch (a e) {
                jm.e(e.getMessage());
            }
        }
        f3477a.a(lkVar);
        return lnVar;
    }

    @Deprecated
    public final <T> nn<T> a(String str, ll<T> llVar) {
        ny nyVar = new ny();
        f3477a.a(new lo(str, nyVar));
        return nc.a(nc.a((nn<A>) nyVar, (my<A, B>) new li<A,B>(this, llVar), (Executor) jt.f3436a), Throwable.class, (mx<? super X, ? extends V>) new lh<Object,Object>(this, llVar), nt.f3559b);
    }

    public final nn<String> a(String str, Map<String, String> map) {
        return a(0, str, map, null);
    }
}
