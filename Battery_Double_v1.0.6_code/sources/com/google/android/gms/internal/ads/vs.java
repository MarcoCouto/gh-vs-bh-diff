package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.abp.b;
import com.google.android.gms.internal.ads.abp.e;

public final class vs extends abp<vs, a> implements acy {
    private static volatile adi<vs> zzakh;
    /* access modifiers changed from: private */
    public static final vs zzdiw = new vs();
    private int zzdih;
    private aah zzdip = aah.f2393a;
    private vx zzdiv;

    public static final class a extends com.google.android.gms.internal.ads.abp.a<vs, a> implements acy {
        private a() {
            super(vs.zzdiw);
        }

        /* synthetic */ a(vt vtVar) {
            this();
        }

        public final a a(int i) {
            b();
            ((vs) this.f2433a).b(0);
            return this;
        }

        public final a a(aah aah) {
            b();
            ((vs) this.f2433a).b(aah);
            return this;
        }

        public final a a(vx vxVar) {
            b();
            ((vs) this.f2433a).a(vxVar);
            return this;
        }
    }

    static {
        abp.a(vs.class, zzdiw);
    }

    private vs() {
    }

    public static vs a(aah aah) throws abv {
        return (vs) abp.a(zzdiw, aah);
    }

    /* access modifiers changed from: private */
    public final void a(vx vxVar) {
        if (vxVar == null) {
            throw new NullPointerException();
        }
        this.zzdiv = vxVar;
    }

    /* access modifiers changed from: private */
    public final void b(int i) {
        this.zzdih = i;
    }

    /* access modifiers changed from: private */
    public final void b(aah aah) {
        if (aah == null) {
            throw new NullPointerException();
        }
        this.zzdip = aah;
    }

    public static a d() {
        return (a) ((com.google.android.gms.internal.ads.abp.a) zzdiw.a(e.e, (Object) null, (Object) null));
    }

    public final int a() {
        return this.zzdih;
    }

    /* JADX WARNING: type inference failed for: r0v6, types: [com.google.android.gms.internal.ads.adi<com.google.android.gms.internal.ads.vs>] */
    /* JADX WARNING: type inference failed for: r0v7, types: [java.lang.Object] */
    /* JADX WARNING: type inference failed for: r0v8, types: [com.google.android.gms.internal.ads.abp$b, com.google.android.gms.internal.ads.adi<com.google.android.gms.internal.ads.vs>] */
    /* JADX WARNING: type inference failed for: r0v16 */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r0v8, types: [com.google.android.gms.internal.ads.abp$b, com.google.android.gms.internal.ads.adi<com.google.android.gms.internal.ads.vs>]
  assigns: [com.google.android.gms.internal.ads.abp$b]
  uses: [com.google.android.gms.internal.ads.adi<com.google.android.gms.internal.ads.vs>]
  mth insns count: 42
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
    	at jadx.core.ProcessClass.process(ProcessClass.java:35)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Unknown variable types count: 2 */
    public final Object a(int i, Object obj, Object obj2) {
        ? r0;
        switch (vt.f3747a[i - 1]) {
            case 1:
                return new vs();
            case 2:
                return new a(null);
            case 3:
                Object[] objArr = {"zzdih", "zzdiv", "zzdip"};
                return a((acw) zzdiw, "\u0000\u0003\u0000\u0000\u0001\u0003\u0003\u0004\u0000\u0000\u0000\u0001\u000b\u0002\t\u0003\n", objArr);
            case 4:
                return zzdiw;
            case 5:
                adi<vs> adi = zzakh;
                if (adi != null) {
                    return adi;
                }
                synchronized (vs.class) {
                    r0 = zzakh;
                    if (r0 == 0) {
                        ? bVar = new b(zzdiw);
                        zzakh = bVar;
                        r0 = bVar;
                    }
                }
                return r0;
            case 6:
                return Byte.valueOf(1);
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    public final vx b() {
        return this.zzdiv == null ? vx.b() : this.zzdiv;
    }

    public final aah c() {
        return this.zzdip;
    }
}
