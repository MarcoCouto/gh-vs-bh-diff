package com.google.android.gms.internal.ads;

import java.util.HashMap;

public final class ahw extends agf<Integer, Object> {

    /* renamed from: a reason: collision with root package name */
    public Long f2602a;

    /* renamed from: b reason: collision with root package name */
    public Boolean f2603b;
    public Boolean c;

    public ahw() {
    }

    public ahw(String str) {
        a(str);
    }

    /* access modifiers changed from: protected */
    public final HashMap<Integer, Object> a() {
        HashMap<Integer, Object> hashMap = new HashMap<>();
        hashMap.put(Integer.valueOf(0), this.f2602a);
        hashMap.put(Integer.valueOf(1), this.f2603b);
        hashMap.put(Integer.valueOf(2), this.c);
        return hashMap;
    }

    /* access modifiers changed from: protected */
    public final void a(String str) {
        HashMap b2 = b(str);
        if (b2 != null) {
            this.f2602a = (Long) b2.get(Integer.valueOf(0));
            this.f2603b = (Boolean) b2.get(Integer.valueOf(1));
            this.c = (Boolean) b2.get(Integer.valueOf(2));
        }
    }
}
