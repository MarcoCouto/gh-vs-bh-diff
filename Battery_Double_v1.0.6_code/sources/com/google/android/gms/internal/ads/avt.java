package com.google.android.gms.internal.ads;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.b.a;
import java.util.List;

public interface avt extends IInterface {
    String a(String str) throws RemoteException;

    List<String> a() throws RemoteException;

    boolean a(a aVar) throws RemoteException;

    a b() throws RemoteException;

    auw b(String str) throws RemoteException;

    aqs c() throws RemoteException;

    void c(String str) throws RemoteException;

    void d() throws RemoteException;

    a e() throws RemoteException;

    void f() throws RemoteException;

    String l() throws RemoteException;
}
