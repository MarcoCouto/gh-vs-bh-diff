package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.common.l;
import com.google.android.gms.common.m;
import java.io.IOException;

final class ip implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ Context f3393a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ ny f3394b;

    ip(io ioVar, Context context, ny nyVar) {
        this.f3393a = context;
        this.f3394b = nyVar;
    }

    public final void run() {
        try {
            this.f3394b.b(AdvertisingIdClient.getAdvertisingIdInfo(this.f3393a));
        } catch (l | m | IOException | IllegalStateException e) {
            this.f3394b.a(e);
            ms.b("Exception while getting advertising Id info", e);
        }
    }
}
