package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.abp.c;
import java.io.IOException;
import java.util.Map.Entry;

final class abf extends abe<Object> {
    abf() {
    }

    /* access modifiers changed from: 0000 */
    public final int a(Entry<?, ?> entry) {
        entry.getKey();
        throw new NoSuchMethodError();
    }

    /* access modifiers changed from: 0000 */
    public final abh<Object> a(Object obj) {
        return ((c) obj).zzdtz;
    }

    /* access modifiers changed from: 0000 */
    public final Object a(abc abc, acw acw, int i) {
        return abc.a(acw, i);
    }

    /* access modifiers changed from: 0000 */
    public final <UT, UB> UB a(ado ado, Object obj, abc abc, abh<Object> abh, UB ub, aei<UT, UB> aei) throws IOException {
        throw new NoSuchMethodError();
    }

    /* access modifiers changed from: 0000 */
    public final void a(aah aah, Object obj, abc abc, abh<Object> abh) throws IOException {
        throw new NoSuchMethodError();
    }

    /* access modifiers changed from: 0000 */
    public final void a(ado ado, Object obj, abc abc, abh<Object> abh) throws IOException {
        throw new NoSuchMethodError();
    }

    /* access modifiers changed from: 0000 */
    public final void a(afc afc, Entry<?, ?> entry) throws IOException {
        entry.getKey();
        throw new NoSuchMethodError();
    }

    /* access modifiers changed from: 0000 */
    public final void a(Object obj, abh<Object> abh) {
        ((c) obj).zzdtz = abh;
    }

    /* access modifiers changed from: 0000 */
    public final boolean a(acw acw) {
        return acw instanceof c;
    }

    /* access modifiers changed from: 0000 */
    public final abh<Object> b(Object obj) {
        abh<Object> a2 = a(obj);
        if (!a2.d()) {
            return a2;
        }
        abh<Object> abh = (abh) a2.clone();
        a(obj, abh);
        return abh;
    }

    /* access modifiers changed from: 0000 */
    public final void c(Object obj) {
        a(obj).c();
    }
}
