package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.a;

@cm
public class apg extends a {

    /* renamed from: a reason: collision with root package name */
    private final Object f2837a = new Object();

    /* renamed from: b reason: collision with root package name */
    private a f2838b;

    public void a() {
        synchronized (this.f2837a) {
            if (this.f2838b != null) {
                this.f2838b.a();
            }
        }
    }

    public void a(int i) {
        synchronized (this.f2837a) {
            if (this.f2838b != null) {
                this.f2838b.a(i);
            }
        }
    }

    public final void a(a aVar) {
        synchronized (this.f2837a) {
            this.f2838b = aVar;
        }
    }

    public void b() {
        synchronized (this.f2837a) {
            if (this.f2838b != null) {
                this.f2838b.b();
            }
        }
    }

    public void c() {
        synchronized (this.f2837a) {
            if (this.f2838b != null) {
                this.f2838b.c();
            }
        }
    }

    public void d() {
        synchronized (this.f2837a) {
            if (this.f2838b != null) {
                this.f2838b.d();
            }
        }
    }
}
