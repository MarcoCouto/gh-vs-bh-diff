package com.google.android.gms.internal.ads;

import android.os.RemoteException;

final class arm implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ arl f2857a;

    arm(arl arl) {
        this.f2857a = arl;
    }

    public final void run() {
        if (this.f2857a.f2856a != null) {
            try {
                this.f2857a.f2856a.a(1);
            } catch (RemoteException e) {
                ms.c("Could not notify onAdFailedToLoad event.", e);
            }
        }
    }
}
