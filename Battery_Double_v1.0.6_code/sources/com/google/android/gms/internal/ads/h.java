package com.google.android.gms.internal.ads;

import android.app.DownloadManager;
import android.app.DownloadManager.Request;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.net.Uri;
import android.os.Environment;
import com.google.android.gms.ads.internal.ax;

final class h implements OnClickListener {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ String f3354a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ String f3355b;
    private final /* synthetic */ g c;

    h(g gVar, String str, String str2) {
        this.c = gVar;
        this.f3354a = str;
        this.f3355b = str2;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        DownloadManager downloadManager = (DownloadManager) this.c.f3337b.getSystemService("download");
        try {
            String str = this.f3354a;
            String str2 = this.f3355b;
            Request request = new Request(Uri.parse(str));
            request.setDestinationInExternalPublicDir(Environment.DIRECTORY_PICTURES, str2);
            ax.g().a(request);
            downloadManager.enqueue(request);
        } catch (IllegalStateException e) {
            this.c.a("Could not store picture.");
        }
    }
}
