package com.google.android.gms.internal.ads;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.PopupWindow;

@TargetApi(19)
@cm
public final class at extends aq {
    private Object d = new Object();
    private PopupWindow e;
    private boolean f = false;

    at(Context context, is isVar, qn qnVar, ap apVar) {
        super(context, isVar, qnVar, apVar);
    }

    private final void e() {
        synchronized (this.d) {
            this.f = true;
            if ((this.f2611a instanceof Activity) && ((Activity) this.f2611a).isDestroyed()) {
                this.e = null;
            }
            if (this.e != null) {
                if (this.e.isShowing()) {
                    this.e.dismiss();
                }
                this.e = null;
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void a(int i) {
        e();
        super.a(i);
    }

    public final void b() {
        e();
        super.b();
    }

    /* access modifiers changed from: protected */
    public final void d() {
        Window window = this.f2611a instanceof Activity ? ((Activity) this.f2611a).getWindow() : null;
        if (window != null && window.getDecorView() != null && !((Activity) this.f2611a).isDestroyed()) {
            FrameLayout frameLayout = new FrameLayout(this.f2611a);
            frameLayout.setLayoutParams(new LayoutParams(-1, -1));
            frameLayout.addView(this.f2612b.getView(), -1, -1);
            synchronized (this.d) {
                if (!this.f) {
                    this.e = new PopupWindow(frameLayout, 1, 1, false);
                    this.e.setOutsideTouchable(true);
                    this.e.setClippingEnabled(false);
                    jm.b("Displaying the 1x1 popup off the screen.");
                    try {
                        this.e.showAtLocation(window.getDecorView(), 0, -1, -1);
                    } catch (Exception e2) {
                        this.e = null;
                    }
                }
            }
        }
    }
}
