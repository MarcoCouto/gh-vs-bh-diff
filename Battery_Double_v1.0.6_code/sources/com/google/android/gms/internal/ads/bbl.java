package com.google.android.gms.internal.ads;

import org.json.JSONException;
import org.json.JSONObject;

public interface bbl<T> {
    JSONObject a(T t) throws JSONException;
}
