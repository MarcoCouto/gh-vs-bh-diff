package com.google.android.gms.internal.ads;

import android.os.Build.VERSION;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.google.android.gms.ads.internal.ax;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

@cm
public class sl extends WebView implements sq, ss, su, sv {

    /* renamed from: a reason: collision with root package name */
    protected final WebViewClient f3687a;

    /* renamed from: b reason: collision with root package name */
    private final List<sq> f3688b = new CopyOnWriteArrayList();
    private final List<sv> c = new CopyOnWriteArrayList();
    private final List<ss> d = new CopyOnWriteArrayList();
    private final List<su> e = new CopyOnWriteArrayList();
    private final sa f;

    public sl(sa saVar) {
        super(saVar);
        this.f = saVar;
        setBackgroundColor(0);
        WebSettings settings = getSettings();
        settings.setAllowFileAccess(false);
        settings.setSavePassword(false);
        settings.setSupportMultipleWindows(true);
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        if (VERSION.SDK_INT >= 21) {
            settings.setMixedContentMode(2);
        }
        ax.g().a(getContext(), settings);
        removeJavascriptInterface("accessibility");
        removeJavascriptInterface("accessibilityTraversal");
        try {
            getSettings().setJavaScriptEnabled(true);
        } catch (NullPointerException e2) {
            jm.b("Unable to enable Javascript.", e2);
        }
        setLayerType(1, null);
        this.f3687a = new sm(this, this, this, this);
        super.setWebViewClient(this.f3687a);
    }

    /* access modifiers changed from: protected */
    public final sa K() {
        return this.f;
    }

    public void a(sn snVar) {
        for (su a2 : this.e) {
            a2.a(snVar);
        }
    }

    public final void a(sq sqVar) {
        this.f3688b.add(sqVar);
    }

    public final void a(ss ssVar) {
        this.d.add(ssVar);
    }

    public final void a(su suVar) {
        this.e.add(suVar);
    }

    public final void a(sv svVar) {
        this.c.add(svVar);
    }

    public void addJavascriptInterface(Object obj, String str) {
        if (VERSION.SDK_INT >= 17) {
            super.addJavascriptInterface(obj, str);
        } else {
            jm.a("Ignore addJavascriptInterface due to low Android version.");
        }
    }

    public final void b(sn snVar) {
        for (ss b2 : this.d) {
            b2.b(snVar);
        }
    }

    public void b(String str) {
        sr.a(this, str);
    }

    public final boolean c(sn snVar) {
        for (sq c2 : this.f3688b) {
            if (c2.c(snVar)) {
                return true;
            }
        }
        return false;
    }

    public final WebResourceResponse d(sn snVar) {
        for (sv d2 : this.c) {
            WebResourceResponse d3 = d2.d(snVar);
            if (d3 != null) {
                return d3;
            }
        }
        return null;
    }

    public void loadUrl(String str) {
        try {
            super.loadUrl(str);
        } catch (Exception | IncompatibleClassChangeError | NoClassDefFoundError e2) {
            ax.i().a(e2, "CoreWebView.loadUrl");
            jm.d("#007 Could not call remote method.", e2);
        }
    }

    public void setWebViewClient(WebViewClient webViewClient) {
    }
}
