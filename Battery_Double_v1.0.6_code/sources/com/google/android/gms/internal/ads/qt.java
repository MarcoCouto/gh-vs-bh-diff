package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.overlay.n;

final class qt implements n {

    /* renamed from: a reason: collision with root package name */
    private qn f3644a;

    /* renamed from: b reason: collision with root package name */
    private n f3645b;

    public qt(qn qnVar, n nVar) {
        this.f3644a = qnVar;
        this.f3645b = nVar;
    }

    public final void d() {
    }

    public final void f() {
    }

    public final void g() {
        this.f3645b.g();
        this.f3644a.o();
    }

    public final void p_() {
        this.f3645b.p_();
        this.f3644a.n();
    }
}
