package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.app.Application.ActivityLifecycleCallbacks;

final class akz implements alf {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ Activity f2685a;

    akz(akw akw, Activity activity) {
        this.f2685a = activity;
    }

    public final void a(ActivityLifecycleCallbacks activityLifecycleCallbacks) {
        activityLifecycleCallbacks.onActivityResumed(this.f2685a);
    }
}
