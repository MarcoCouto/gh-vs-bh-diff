package com.google.android.gms.internal.ads;

import android.content.Context;
import android.location.Location;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.text.TextUtils;
import com.google.android.gms.ads.identifier.AdvertisingIdClient.Info;
import com.google.android.gms.ads.internal.ax;
import com.google.android.gms.common.util.l;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import org.json.JSONException;
import org.json.JSONObject;

@cm
public final class eu extends du {

    /* renamed from: a reason: collision with root package name */
    private static final Object f3294a = new Object();

    /* renamed from: b reason: collision with root package name */
    private static eu f3295b;
    private final Context c;
    private final et d;
    private final ScheduledExecutorService e = Executors.newSingleThreadScheduledExecutor();

    private eu(Context context, et etVar) {
        this.c = context;
        this.d = etVar;
    }

    private static dp a(Context context, et etVar, dl dlVar, ScheduledExecutorService scheduledExecutorService) {
        jm.b("Starting ad request from service using: google.afma.request.getAdDictionary");
        asv asv = new asv(((Boolean) ape.f().a(asi.N)).booleanValue(), "load_ad", dlVar.d.f2814a);
        if (dlVar.f3260a > 10 && dlVar.A != -1) {
            asv.a(asv.a(dlVar.A), "cts");
        }
        ast a2 = asv.a();
        nn a3 = nc.a(etVar.i.a(context), ((Long) ape.f().a(asi.cA)).longValue(), TimeUnit.MILLISECONDS, scheduledExecutorService);
        nn a4 = nc.a(etVar.h.a(context), ((Long) ape.f().a(asi.bv)).longValue(), TimeUnit.MILLISECONDS, scheduledExecutorService);
        nn a5 = etVar.c.a(dlVar.g.packageName);
        nn b2 = etVar.c.b(dlVar.g.packageName);
        nn a6 = etVar.j.a(dlVar.h, dlVar.g);
        Future a7 = ax.p().a(context);
        nn a8 = nc.a(null);
        Bundle bundle = dlVar.c.c;
        boolean z = (bundle == null || bundle.getString("_ad") == null) ? false : true;
        if (dlVar.G && !z) {
            a8 = etVar.f.a(dlVar.f);
        }
        nn a9 = nc.a(a8, ((Long) ape.f().a(asi.cr)).longValue(), TimeUnit.MILLISECONDS, scheduledExecutorService);
        Future future = ((Boolean) ape.f().a(asi.aJ)).booleanValue() ? nc.a(etVar.j.a(context), ((Long) ape.f().a(asi.aK)).longValue(), TimeUnit.MILLISECONDS, scheduledExecutorService) : nc.a(null);
        Bundle bundle2 = (dlVar.f3260a < 4 || dlVar.o == null) ? null : dlVar.o;
        ((Boolean) ape.f().a(asi.ad)).booleanValue();
        ax.e();
        if (jv.a(context, "android.permission.ACCESS_NETWORK_STATE")) {
            if (((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo() == null) {
                jm.b("Device is offline.");
            }
        }
        String uuid = dlVar.f3260a >= 7 ? dlVar.v : UUID.randomUUID().toString();
        new fa(context, uuid, dlVar.f.packageName);
        if (dlVar.c.c != null) {
            String string = dlVar.c.c.getString("_ad");
            if (string != null) {
                return ez.a(context, dlVar, string);
            }
        }
        List a10 = etVar.d.a(dlVar.w);
        Bundle bundle3 = (Bundle) nc.a((Future<T>) a3, null, ((Long) ape.f().a(asi.cA)).longValue(), TimeUnit.MILLISECONDS);
        fs fsVar = (fs) nc.a((Future<T>) a4, null);
        Location location = (Location) nc.a((Future<T>) a9, null);
        Info info = (Info) nc.a(future, null);
        String str = (String) nc.a((Future<T>) a6, null);
        String str2 = (String) nc.a((Future<T>) a5, null);
        String str3 = (String) nc.a((Future<T>) b2, null);
        fi fiVar = (fi) nc.a(a7, null);
        if (fiVar == null) {
            jm.e("Error fetching device info. This is not recoverable.");
            return new dp(0);
        }
        es esVar = new es();
        esVar.j = dlVar;
        esVar.k = fiVar;
        esVar.e = fsVar;
        esVar.d = location;
        esVar.f3291b = bundle3;
        esVar.h = str;
        esVar.i = info;
        if (a10 == null) {
            esVar.c.clear();
        }
        esVar.c = a10;
        esVar.f3290a = bundle2;
        esVar.f = str2;
        esVar.g = str3;
        esVar.l = etVar.f3293b.a(context);
        esVar.m = etVar.k;
        JSONObject a11 = ez.a(context, esVar);
        if (a11 == null) {
            return new dp(0);
        }
        if (dlVar.f3260a < 7) {
            try {
                a11.put("request_id", uuid);
            } catch (JSONException e2) {
            }
        }
        asv.a(a2, "arc");
        asv.a();
        nn a12 = nc.a(nc.a(etVar.l.a().b(a11), ev.f3296a, (Executor) scheduledExecutorService), 10, TimeUnit.SECONDS, scheduledExecutorService);
        nn a13 = etVar.e.a();
        if (a13 != null) {
            na.a(a13, "AdRequestServiceImpl.loadAd.flags");
        }
        fg fgVar = (fg) nc.a((Future<T>) a12, null);
        if (fgVar == null) {
            return new dp(0);
        }
        if (fgVar.a() != -2) {
            return new dp(fgVar.a());
        }
        asv.d();
        dp dpVar = null;
        if (!TextUtils.isEmpty(fgVar.i())) {
            dpVar = ez.a(context, dlVar, fgVar.i());
        }
        if (dpVar == null && !TextUtils.isEmpty(fgVar.e())) {
            dpVar = a(dlVar, context, dlVar.k.f3528a, fgVar.e(), str2, str3, fgVar, asv, etVar);
        }
        if (dpVar == null) {
            dpVar = new dp(0);
        }
        asv.a(a2, "tts");
        dpVar.w = asv.b();
        return dpVar;
    }

    /* JADX INFO: used method not loaded: com.google.android.gms.internal.ads.fe.a(long, boolean):null, types can be incorrect */
    /* JADX WARNING: Code restructure failed: missing block: B:115:0x0207, code lost:
        r3 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:116:0x0208, code lost:
        r4 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:127:?, code lost:
        return r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:129:?, code lost:
        return r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:130:?, code lost:
        return r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00ca, code lost:
        r7 = r8.toString();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:?, code lost:
        r5 = new java.io.InputStreamReader(r2.getInputStream());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:?, code lost:
        com.google.android.gms.ads.internal.ax.e();
        r6 = com.google.android.gms.internal.ads.jv.a(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:?, code lost:
        com.google.android.gms.common.util.l.a(r5);
        r12.a(r6);
        a(r7, r13, r6, r3);
        r9.a(r7, r13, r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00eb, code lost:
        if (r21 == null) goto L_0x00fa;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00ed, code lost:
        r21.a(r4, "ufe");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00fa, code lost:
        r3 = r9.a(r10, r20.j());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:?, code lost:
        r2.disconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x0105, code lost:
        if (r22 == null) goto L_0x010e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x0107, code lost:
        r22.g.b();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x014e, code lost:
        r3 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x014f, code lost:
        r4 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:?, code lost:
        com.google.android.gms.common.util.l.a(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x0153, code lost:
        throw r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x0170, code lost:
        com.google.android.gms.internal.ads.jm.e("No location header to follow redirect.");
        r3 = new com.google.android.gms.internal.ads.dp(0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:?, code lost:
        r2.disconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x017e, code lost:
        if (r22 == null) goto L_0x0187;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:0x0180, code lost:
        r22.g.b();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:0x01a2, code lost:
        com.google.android.gms.internal.ads.jm.e("Too many redirects.");
        r3 = new com.google.android.gms.internal.ads.dp(0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:94:?, code lost:
        r2.disconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:95:0x01b0, code lost:
        if (r22 == null) goto L_0x01b9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:0x01b2, code lost:
        r22.g.b();
     */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:74:0x0150=Splitter:B:74:0x0150, B:63:0x013c=Splitter:B:63:0x013c} */
    public static dp a(dl dlVar, Context context, String str, String str2, String str3, String str4, fg fgVar, asv asv, et etVar) {
        HttpURLConnection httpURLConnection;
        int responseCode;
        BufferedOutputStream bufferedOutputStream;
        ast ast = asv != null ? asv.a() : null;
        try {
            fe feVar = new fe(dlVar, fgVar.c());
            String str5 = "AdRequestServiceImpl: Sending request: ";
            String valueOf = String.valueOf(str2);
            jm.b(valueOf.length() != 0 ? str5.concat(valueOf) : new String(str5));
            URL url = new URL(str2);
            long b2 = ax.l().b();
            int i = 0;
            URL url2 = url;
            while (true) {
                if (etVar != null) {
                    etVar.g.a();
                }
                httpURLConnection = (HttpURLConnection) url2.openConnection();
                ax.e().a(context, str, false, httpURLConnection);
                if (fgVar.g()) {
                    if (!TextUtils.isEmpty(str3)) {
                        httpURLConnection.addRequestProperty("x-afma-drt-cookie", str3);
                    }
                    if (!TextUtils.isEmpty(str4)) {
                        httpURLConnection.addRequestProperty("x-afma-drt-v2-cookie", str4);
                    }
                }
                String str6 = dlVar.H;
                if (!TextUtils.isEmpty(str6)) {
                    jm.b("Sending webview cookie in ad request header.");
                    httpURLConnection.addRequestProperty("Cookie", str6);
                }
                byte[] bArr = null;
                if (fgVar != null && !TextUtils.isEmpty(fgVar.d())) {
                    httpURLConnection.setDoOutput(true);
                    bArr = fgVar.d().getBytes();
                    httpURLConnection.setFixedLengthStreamingMode(bArr.length);
                    try {
                        BufferedOutputStream bufferedOutputStream2 = new BufferedOutputStream(httpURLConnection.getOutputStream());
                        try {
                            bufferedOutputStream2.write(bArr);
                            l.a(bufferedOutputStream2);
                        } catch (Throwable th) {
                            th = th;
                            bufferedOutputStream = bufferedOutputStream2;
                            l.a(bufferedOutputStream);
                            throw th;
                        }
                    } catch (Throwable th2) {
                        th = th2;
                        bufferedOutputStream = null;
                        l.a(bufferedOutputStream);
                        throw th;
                    }
                }
                ml mlVar = new ml(dlVar.v);
                mlVar.a(httpURLConnection, bArr);
                responseCode = httpURLConnection.getResponseCode();
                Map headerFields = httpURLConnection.getHeaderFields();
                mlVar.a(httpURLConnection, responseCode);
                if (responseCode >= 200 && responseCode < 300) {
                    break;
                }
                a(url2.toString(), headerFields, (String) null, responseCode);
                if (responseCode >= 300 && responseCode < 400) {
                    String headerField = httpURLConnection.getHeaderField("Location");
                    if (TextUtils.isEmpty(headerField)) {
                        break;
                    }
                    URL url3 = new URL(headerField);
                    int i2 = i + 1;
                    if (i2 > ((Integer) ape.f().a(asi.df)).intValue()) {
                        break;
                    }
                    feVar.a(headerFields);
                    httpURLConnection.disconnect();
                    if (etVar != null) {
                        etVar.g.b();
                        i = i2;
                        url2 = url3;
                    } else {
                        i = i2;
                        url2 = url3;
                    }
                }
            }
            jm.e("Received error HTTP response code: " + responseCode);
            dp dpVar = new dp(0);
            httpURLConnection.disconnect();
            if (etVar != null) {
                etVar.g.b();
            }
            return dpVar;
        } catch (IOException e2) {
            String str7 = "Error while connecting to ad server: ";
            String valueOf2 = String.valueOf(e2.getMessage());
            jm.e(valueOf2.length() != 0 ? str7.concat(valueOf2) : new String(str7));
            return new dp(2);
        } catch (Throwable th3) {
            httpURLConnection.disconnect();
            if (etVar != null) {
                etVar.g.b();
            }
            throw th3;
        }
    }

    public static eu a(Context context, et etVar) {
        eu euVar;
        synchronized (f3294a) {
            if (f3295b == null) {
                if (context.getApplicationContext() != null) {
                    context = context.getApplicationContext();
                }
                asi.a(context);
                f3295b = new eu(context, etVar);
                if (context.getApplicationContext() != null) {
                    ax.e().c(context);
                }
                jj.a(context);
            }
            euVar = f3295b;
        }
        return euVar;
    }

    private static void a(String str, Map<String, List<String>> map, String str2, int i) {
        if (jm.a(2)) {
            jm.a(new StringBuilder(String.valueOf(str).length() + 39).append("Http Response: {\n  URL:\n    ").append(str).append("\n  Headers:").toString());
            if (map != null) {
                for (String str3 : map.keySet()) {
                    jm.a(new StringBuilder(String.valueOf(str3).length() + 5).append("    ").append(str3).append(":").toString());
                    for (String valueOf : (List) map.get(str3)) {
                        String str4 = "      ";
                        String valueOf2 = String.valueOf(valueOf);
                        jm.a(valueOf2.length() != 0 ? str4.concat(valueOf2) : new String(str4));
                    }
                }
            }
            jm.a("  Body:");
            if (str2 != null) {
                for (int i2 = 0; i2 < Math.min(str2.length(), 100000); i2 += 1000) {
                    jm.a(str2.substring(i2, Math.min(str2.length(), i2 + 1000)));
                }
            } else {
                jm.a("    null");
            }
            jm.a("  Response Code:\n    " + i + "\n}");
        }
    }

    public final dp a(dl dlVar) {
        return a(this.c, this.d, dlVar, this.e);
    }

    public final void a(dl dlVar, dw dwVar) {
        ax.i().a(this.c, dlVar.k);
        nn a2 = jt.a((Runnable) new ew(this, dlVar, dwVar));
        ax.t().a();
        ax.t().b().postDelayed(new ex(this, a2), 60000);
    }

    public final void a(ee eeVar, dz dzVar) {
        jm.a("Nonagon code path entered in octagon");
        throw new IllegalArgumentException();
    }

    public final void b(ee eeVar, dz dzVar) {
        jm.a("Nonagon code path entered in octagon");
        throw new IllegalArgumentException();
    }
}
