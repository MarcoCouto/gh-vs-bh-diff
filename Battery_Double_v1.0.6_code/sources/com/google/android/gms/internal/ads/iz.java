package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.ax;

final class iz {

    /* renamed from: a reason: collision with root package name */
    private final Object f3409a;

    /* renamed from: b reason: collision with root package name */
    private volatile int f3410b;
    private volatile long c;

    private iz() {
        this.f3409a = new Object();
        this.f3410b = ja.f3413a;
        this.c = 0;
    }

    /* synthetic */ iz(iy iyVar) {
        this();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        return;
     */
    private final void a(int i, int i2) {
        d();
        long a2 = ax.l().a();
        synchronized (this.f3409a) {
            if (this.f3410b == i) {
                this.f3410b = i2;
                if (this.f3410b == ja.c) {
                    this.c = a2;
                }
            }
        }
    }

    private final void d() {
        long a2 = ax.l().a();
        synchronized (this.f3409a) {
            if (this.f3410b == ja.c) {
                if (this.c + ((Long) ape.f().a(asi.di)).longValue() <= a2) {
                    this.f3410b = ja.f3413a;
                }
            }
        }
    }

    public final void a(boolean z) {
        if (z) {
            a(ja.f3413a, ja.f3414b);
        } else {
            a(ja.f3414b, ja.f3413a);
        }
    }

    public final boolean a() {
        d();
        return this.f3410b == ja.f3414b;
    }

    public final boolean b() {
        d();
        return this.f3410b == ja.c;
    }

    public final void c() {
        a(ja.f3414b, ja.c);
    }
}
