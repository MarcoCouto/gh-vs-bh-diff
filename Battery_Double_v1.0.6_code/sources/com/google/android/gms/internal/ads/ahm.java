package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.app.Application;
import android.app.Application.ActivityLifecycleCallbacks;
import android.os.Bundle;
import java.lang.ref.WeakReference;

final class ahm implements ActivityLifecycleCallbacks {

    /* renamed from: a reason: collision with root package name */
    private final Application f2591a;

    /* renamed from: b reason: collision with root package name */
    private final WeakReference<ActivityLifecycleCallbacks> f2592b;
    private boolean c = false;

    public ahm(Application application, ActivityLifecycleCallbacks activityLifecycleCallbacks) {
        this.f2592b = new WeakReference<>(activityLifecycleCallbacks);
        this.f2591a = application;
    }

    private final void a(ahu ahu) {
        try {
            ActivityLifecycleCallbacks activityLifecycleCallbacks = (ActivityLifecycleCallbacks) this.f2592b.get();
            if (activityLifecycleCallbacks != null) {
                ahu.a(activityLifecycleCallbacks);
            } else if (!this.c) {
                this.f2591a.unregisterActivityLifecycleCallbacks(this);
                this.c = true;
            }
        } catch (Exception e) {
        }
    }

    public final void onActivityCreated(Activity activity, Bundle bundle) {
        a(new ahn(this, activity, bundle));
    }

    public final void onActivityDestroyed(Activity activity) {
        a(new aht(this, activity));
    }

    public final void onActivityPaused(Activity activity) {
        a(new ahq(this, activity));
    }

    public final void onActivityResumed(Activity activity) {
        a(new ahp(this, activity));
    }

    public final void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        a(new ahs(this, activity, bundle));
    }

    public final void onActivityStarted(Activity activity) {
        a(new aho(this, activity));
    }

    public final void onActivityStopped(Activity activity) {
        a(new ahr(this, activity));
    }
}
