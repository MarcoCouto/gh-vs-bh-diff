package com.google.android.gms.internal.ads;

public final class ha {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public String f3356a;
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public String f3357b;
    /* access modifiers changed from: private */
    public int c;
    /* access modifiers changed from: private */
    public long d;

    public final gy a() {
        return new gy(this);
    }

    public final ha a(int i) {
        this.c = i;
        return this;
    }

    public final ha a(long j) {
        this.d = j;
        return this;
    }

    public final ha a(String str) {
        this.f3356a = str;
        return this;
    }

    public final ha b(String str) {
        this.f3357b = str;
        return this;
    }
}
