package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.gmsg.ae;
import java.util.Map;

final class ban implements ae<bbe> {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ ahh f3076a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ azv f3077b;
    private final /* synthetic */ lx c;
    private final /* synthetic */ bah d;

    ban(bah bah, ahh ahh, azv azv, lx lxVar) {
        this.d = bah;
        this.f3076a = ahh;
        this.f3077b = azv;
        this.c = lxVar;
    }

    public final /* synthetic */ void zza(Object obj, Map map) {
        synchronized (this.d.f3065a) {
            jm.d("JS Engine is requesting an update");
            if (this.d.h == 0) {
                jm.d("Starting reload.");
                this.d.h = 2;
                this.d.a(this.f3076a);
            }
            this.f3077b.b("/requestReload", (ae) this.c.a());
        }
    }
}
