package com.google.android.gms.internal.ads;

final /* synthetic */ class aev {

    /* renamed from: a reason: collision with root package name */
    private static final /* synthetic */ int[] f2515a = new int[aew.values().length];

    static {
        try {
            f2515a[aew.DOUBLE.ordinal()] = 1;
        } catch (NoSuchFieldError e) {
        }
        try {
            f2515a[aew.FLOAT.ordinal()] = 2;
        } catch (NoSuchFieldError e2) {
        }
        try {
            f2515a[aew.INT64.ordinal()] = 3;
        } catch (NoSuchFieldError e3) {
        }
        try {
            f2515a[aew.UINT64.ordinal()] = 4;
        } catch (NoSuchFieldError e4) {
        }
        try {
            f2515a[aew.INT32.ordinal()] = 5;
        } catch (NoSuchFieldError e5) {
        }
        try {
            f2515a[aew.FIXED64.ordinal()] = 6;
        } catch (NoSuchFieldError e6) {
        }
        try {
            f2515a[aew.FIXED32.ordinal()] = 7;
        } catch (NoSuchFieldError e7) {
        }
        try {
            f2515a[aew.BOOL.ordinal()] = 8;
        } catch (NoSuchFieldError e8) {
        }
        try {
            f2515a[aew.BYTES.ordinal()] = 9;
        } catch (NoSuchFieldError e9) {
        }
        try {
            f2515a[aew.UINT32.ordinal()] = 10;
        } catch (NoSuchFieldError e10) {
        }
        try {
            f2515a[aew.SFIXED32.ordinal()] = 11;
        } catch (NoSuchFieldError e11) {
        }
        try {
            f2515a[aew.SFIXED64.ordinal()] = 12;
        } catch (NoSuchFieldError e12) {
        }
        try {
            f2515a[aew.SINT32.ordinal()] = 13;
        } catch (NoSuchFieldError e13) {
        }
        try {
            f2515a[aew.SINT64.ordinal()] = 14;
        } catch (NoSuchFieldError e14) {
        }
        try {
            f2515a[aew.STRING.ordinal()] = 15;
        } catch (NoSuchFieldError e15) {
        }
        try {
            f2515a[aew.GROUP.ordinal()] = 16;
        } catch (NoSuchFieldError e16) {
        }
        try {
            f2515a[aew.MESSAGE.ordinal()] = 17;
        } catch (NoSuchFieldError e17) {
        }
        try {
            f2515a[aew.ENUM.ordinal()] = 18;
        } catch (NoSuchFieldError e18) {
        }
    }
}
