package com.google.android.gms.internal.ads;

final /* synthetic */ class rf implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final rd f3661a;

    /* renamed from: b reason: collision with root package name */
    private final int f3662b;
    private final int c;
    private final boolean d;
    private final boolean e;

    rf(rd rdVar, int i, int i2, boolean z, boolean z2) {
        this.f3661a = rdVar;
        this.f3662b = i;
        this.c = i2;
        this.d = z;
        this.e = z2;
    }

    public final void run() {
        this.f3661a.a(this.f3662b, this.c, this.d, this.e);
    }
}
