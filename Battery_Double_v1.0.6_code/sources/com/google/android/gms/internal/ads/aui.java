package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.gmsg.ae;
import java.util.Map;

final class aui implements ae<Object> {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ bq f2931a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ auf f2932b;

    aui(auf auf, bq bqVar) {
        this.f2932b = auf;
        this.f2931a = bqVar;
    }

    public final void zza(Object obj, Map<String, String> map) {
        qn qnVar = (qn) this.f2932b.f2925a.get();
        if (qnVar == null) {
            this.f2931a.b("/showOverlay", this);
        } else {
            qnVar.getView().setVisibility(0);
        }
    }
}
