package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.app.Application.ActivityLifecycleCallbacks;

final class aky implements alf {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ Activity f2684a;

    aky(akw akw, Activity activity) {
        this.f2684a = activity;
    }

    public final void a(ActivityLifecycleCallbacks activityLifecycleCallbacks) {
        activityLifecycleCallbacks.onActivityStarted(this.f2684a);
    }
}
