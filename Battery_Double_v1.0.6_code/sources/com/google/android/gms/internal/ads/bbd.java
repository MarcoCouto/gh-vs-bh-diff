package com.google.android.gms.internal.ads;

final /* synthetic */ class bbd implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final bbb f3098a;

    /* renamed from: b reason: collision with root package name */
    private final azv f3099b;

    bbd(bbb bbb, azv azv) {
        this.f3098a = bbb;
        this.f3099b = azv;
    }

    public final void run() {
        bbb bbb = this.f3098a;
        azv azv = this.f3099b;
        bbb.f3097a.f3092b.a(azv);
        azv.a();
    }
}
