package com.google.android.gms.internal.ads;

import com.google.android.gms.common.internal.aa;
import java.util.Iterator;
import java.util.LinkedList;

@cm
final class ayy {

    /* renamed from: a reason: collision with root package name */
    private final LinkedList<ayz> f3016a = new LinkedList<>();
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public aop f3017b;
    /* access modifiers changed from: private */
    public final String c;
    private final int d;
    private boolean e;

    ayy(aop aop, String str, int i) {
        aa.a(aop);
        aa.a(str);
        this.f3017b = aop;
        this.c = str;
        this.d = i;
    }

    /* access modifiers changed from: 0000 */
    public final aop a() {
        return this.f3017b;
    }

    /* access modifiers changed from: 0000 */
    public final ayz a(aop aop) {
        if (aop != null) {
            this.f3017b = aop;
        }
        return (ayz) this.f3016a.remove();
    }

    /* access modifiers changed from: 0000 */
    public final void a(axr axr, aop aop) {
        this.f3016a.add(new ayz(this, axr, aop));
    }

    /* access modifiers changed from: 0000 */
    public final boolean a(axr axr) {
        ayz ayz = new ayz(this, axr);
        this.f3016a.add(ayz);
        return ayz.a();
    }

    /* access modifiers changed from: 0000 */
    public final int b() {
        return this.d;
    }

    /* access modifiers changed from: 0000 */
    public final String c() {
        return this.c;
    }

    /* access modifiers changed from: 0000 */
    public final int d() {
        return this.f3016a.size();
    }

    /* access modifiers changed from: 0000 */
    public final int e() {
        int i = 0;
        Iterator it = this.f3016a.iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                return i2;
            }
            i = ((ayz) it.next()).e ? i2 + 1 : i2;
        }
    }

    /* access modifiers changed from: 0000 */
    public final int f() {
        int i = 0;
        Iterator it = this.f3016a.iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                return i2;
            }
            i = ((ayz) it.next()).a() ? i2 + 1 : i2;
        }
    }

    /* access modifiers changed from: 0000 */
    public final void g() {
        this.e = true;
    }

    /* access modifiers changed from: 0000 */
    public final boolean h() {
        return this.e;
    }
}
