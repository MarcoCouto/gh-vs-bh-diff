package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.ads.j;
import com.google.android.gms.common.internal.a.a;
import com.google.android.gms.common.internal.a.c;

@cm
public final class arr extends a {
    public static final Creator<arr> CREATOR = new ars();

    /* renamed from: a reason: collision with root package name */
    public final boolean f2860a;

    /* renamed from: b reason: collision with root package name */
    public final boolean f2861b;
    public final boolean c;

    public arr(j jVar) {
        this(jVar.a(), jVar.b(), jVar.c());
    }

    public arr(boolean z, boolean z2, boolean z3) {
        this.f2860a = z;
        this.f2861b = z2;
        this.c = z3;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = c.a(parcel);
        c.a(parcel, 2, this.f2860a);
        c.a(parcel, 3, this.f2861b);
        c.a(parcel, 4, this.c);
        c.a(parcel, a2);
    }
}
