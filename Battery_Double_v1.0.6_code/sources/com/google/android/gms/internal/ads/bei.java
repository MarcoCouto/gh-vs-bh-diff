package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.google.android.gms.b.a;

public final class bei extends ajk implements beg {
    bei(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.mediation.client.rtb.IRtbAdapter");
    }

    public final beq a() throws RemoteException {
        Parcel a2 = a(2, r_());
        beq beq = (beq) ajm.a(a2, beq.CREATOR);
        a2.recycle();
        return beq;
    }

    public final void a(a aVar, String str, Bundle bundle, bej bej) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (IInterface) aVar);
        r_.writeString(str);
        ajm.a(r_, (Parcelable) bundle);
        ajm.a(r_, (IInterface) bej);
        b(1, r_);
    }

    public final void a(byte[] bArr, String str, Bundle bundle, a aVar, bec bec, bcx bcx, aot aot) throws RemoteException {
        Parcel r_ = r_();
        r_.writeByteArray(bArr);
        r_.writeString(str);
        ajm.a(r_, (Parcelable) bundle);
        ajm.a(r_, (IInterface) aVar);
        ajm.a(r_, (IInterface) bec);
        ajm.a(r_, (IInterface) bcx);
        ajm.a(r_, (Parcelable) aot);
        b(4, r_);
    }

    public final void a(byte[] bArr, String str, Bundle bundle, a aVar, bee bee, bcx bcx) throws RemoteException {
        Parcel r_ = r_();
        r_.writeByteArray(bArr);
        r_.writeString(str);
        ajm.a(r_, (Parcelable) bundle);
        ajm.a(r_, (IInterface) aVar);
        ajm.a(r_, (IInterface) bee);
        ajm.a(r_, (IInterface) bcx);
        b(6, r_);
    }

    public final beq b() throws RemoteException {
        Parcel a2 = a(3, r_());
        beq beq = (beq) ajm.a(a2, beq.CREATOR);
        a2.recycle();
        return beq;
    }

    public final aqs c() throws RemoteException {
        Parcel a2 = a(5, r_());
        aqs a3 = aqt.a(a2.readStrongBinder());
        a2.recycle();
        return a3;
    }

    public final void d() throws RemoteException {
        b(7, r_());
    }
}
