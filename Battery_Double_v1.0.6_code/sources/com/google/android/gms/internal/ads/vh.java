package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.yc.a;
import java.security.GeneralSecurityException;

public final class vh {

    /* renamed from: a reason: collision with root package name */
    public static final yc f3740a = ((yc) yc.b().a("TINK_MAC_1_0_0").a(tu.a("TinkMac", "Mac", "HmacKey", 0, true)).c());

    /* renamed from: b reason: collision with root package name */
    private static final yc f3741b = ((yc) ((a) yc.b().a(f3740a)).a("TINK_MAC_1_1_0").c());

    static {
        try {
            a();
        } catch (GeneralSecurityException e) {
            throw new ExceptionInInitializerError(e);
        }
    }

    public static void a() throws GeneralSecurityException {
        uh.a("TinkMac", (tt<P>) new vg<P>());
    }
}
