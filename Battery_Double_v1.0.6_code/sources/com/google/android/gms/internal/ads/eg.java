package com.google.android.gms.internal.ads;

import android.os.SystemClock;
import android.util.Log;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class eg {

    /* renamed from: a reason: collision with root package name */
    public static boolean f3276a;

    /* renamed from: b reason: collision with root package name */
    private static String f3277b;

    static class a {

        /* renamed from: a reason: collision with root package name */
        public static final boolean f3278a = eg.f3276a;

        /* renamed from: b reason: collision with root package name */
        private final List<fh> f3279b = new ArrayList();
        private boolean c = false;

        a() {
        }

        public final synchronized void a(String str) {
            long j;
            this.c = true;
            if (this.f3279b.size() == 0) {
                j = 0;
            } else {
                j = ((fh) this.f3279b.get(this.f3279b.size() - 1)).c - ((fh) this.f3279b.get(0)).c;
            }
            if (j > 0) {
                long j2 = ((fh) this.f3279b.get(0)).c;
                eg.b("(%-4d ms) %s", Long.valueOf(j), str);
                long j3 = j2;
                for (fh fhVar : this.f3279b) {
                    long j4 = fhVar.c;
                    eg.b("(+%-4d) [%2d] %s", Long.valueOf(j4 - j3), Long.valueOf(fhVar.f3313b), fhVar.f3312a);
                    j3 = j4;
                }
            }
        }

        public final synchronized void a(String str, long j) {
            if (this.c) {
                throw new IllegalStateException("Marker added to finished log");
            }
            this.f3279b.add(new fh(str, j, SystemClock.elapsedRealtime()));
        }

        /* access modifiers changed from: protected */
        public final void finalize() throws Throwable {
            if (!this.c) {
                a("Request on the loose");
                eg.c("Marker log finalized without finish() - uncaught exit point for request", new Object[0]);
            }
        }
    }

    static {
        String str = "Volley";
        f3277b = str;
        f3276a = Log.isLoggable(str, 2);
    }

    public static void a(String str, Object... objArr) {
        if (f3276a) {
            Log.v(f3277b, d(str, objArr));
        }
    }

    public static void a(Throwable th, String str, Object... objArr) {
        Log.e(f3277b, d(str, objArr), th);
    }

    public static void b(String str, Object... objArr) {
        Log.d(f3277b, d(str, objArr));
    }

    public static void c(String str, Object... objArr) {
        Log.e(f3277b, d(str, objArr));
    }

    private static String d(String str, Object... objArr) {
        String str2;
        if (objArr != null) {
            str = String.format(Locale.US, str, objArr);
        }
        StackTraceElement[] stackTrace = new Throwable().fillInStackTrace().getStackTrace();
        String str3 = "<unknown>";
        int i = 2;
        while (true) {
            if (i >= stackTrace.length) {
                str2 = str3;
                break;
            } else if (!stackTrace[i].getClass().equals(eg.class)) {
                String className = stackTrace[i].getClassName();
                String substring = className.substring(className.lastIndexOf(46) + 1);
                String substring2 = substring.substring(substring.lastIndexOf(36) + 1);
                String methodName = stackTrace[i].getMethodName();
                str2 = new StringBuilder(String.valueOf(substring2).length() + 1 + String.valueOf(methodName).length()).append(substring2).append(".").append(methodName).toString();
                break;
            } else {
                i++;
            }
        }
        return String.format(Locale.US, "[%d] %s: %s", new Object[]{Long.valueOf(Thread.currentThread().getId()), str2, str});
    }
}
