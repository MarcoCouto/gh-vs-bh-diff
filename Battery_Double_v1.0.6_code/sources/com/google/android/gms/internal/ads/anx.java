package com.google.android.gms.internal.ads;

import java.io.IOException;

public final class anx extends afh<anx> {

    /* renamed from: a reason: collision with root package name */
    private static volatile anx[] f2784a;

    /* renamed from: b reason: collision with root package name */
    private Integer f2785b;
    private Integer c;

    public anx() {
        this.f2785b = null;
        this.c = null;
        this.Y = null;
        this.Z = -1;
    }

    public static anx[] b() {
        if (f2784a == null) {
            synchronized (afl.f2531b) {
                if (f2784a == null) {
                    f2784a = new anx[0];
                }
            }
        }
        return f2784a;
    }

    /* access modifiers changed from: protected */
    public final int a() {
        int a2 = super.a();
        if (this.f2785b != null) {
            a2 += aff.b(1, this.f2785b.intValue());
        }
        return this.c != null ? a2 + aff.b(2, this.c.intValue()) : a2;
    }

    public final /* synthetic */ afn a(afd afd) throws IOException {
        while (true) {
            int a2 = afd.a();
            switch (a2) {
                case 0:
                    break;
                case 8:
                    this.f2785b = Integer.valueOf(afd.g());
                    continue;
                case 16:
                    this.c = Integer.valueOf(afd.g());
                    continue;
                default:
                    if (!super.a(afd, a2)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    public final void a(aff aff) throws IOException {
        if (this.f2785b != null) {
            aff.a(1, this.f2785b.intValue());
        }
        if (this.c != null) {
            aff.a(2, this.c.intValue());
        }
        super.a(aff);
    }
}
