package com.google.android.gms.internal.ads;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.Charset;
import org.json.JSONException;
import org.json.JSONObject;

@cm
public final class bbn {

    /* renamed from: a reason: collision with root package name */
    public static final bbm<JSONObject> f3104a = new bbp();

    /* renamed from: b reason: collision with root package name */
    private static final Charset f3105b = Charset.forName("UTF-8");
    private static final bbk<InputStream> c = bbo.f3106a;

    static final /* synthetic */ InputStream a(JSONObject jSONObject) throws JSONException {
        return new ByteArrayInputStream(jSONObject.toString().getBytes(f3105b));
    }
}
