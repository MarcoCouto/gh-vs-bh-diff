package com.google.android.gms.internal.ads;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

@cm
public final class mt implements mk {

    /* renamed from: a reason: collision with root package name */
    private final String f3527a;

    public mt() {
        this(null);
    }

    public mt(String str) {
        this.f3527a = str;
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    public final void a(String str) {
        HttpURLConnection httpURLConnection;
        String str2 = "Pinging URL: ";
        try {
            String valueOf = String.valueOf(str);
            ms.b(valueOf.length() != 0 ? str2.concat(valueOf) : new String(str2));
            httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
            ape.a();
            mh.a(true, httpURLConnection, this.f3527a);
            ml mlVar = new ml();
            mlVar.a(httpURLConnection, (byte[]) null);
            int responseCode = httpURLConnection.getResponseCode();
            mlVar.a(httpURLConnection, responseCode);
            if (responseCode < 200 || responseCode >= 300) {
                ms.e(new StringBuilder(String.valueOf(str).length() + 65).append("Received non-success response code ").append(responseCode).append(" from pinging URL: ").append(str).toString());
            }
            httpURLConnection.disconnect();
        } catch (IndexOutOfBoundsException e) {
            String message = e.getMessage();
            ms.e(new StringBuilder(String.valueOf(str).length() + 32 + String.valueOf(message).length()).append("Error while parsing ping URL: ").append(str).append(". ").append(message).toString());
        } catch (IOException e2) {
            String message2 = e2.getMessage();
            ms.e(new StringBuilder(String.valueOf(str).length() + 27 + String.valueOf(message2).length()).append("Error while pinging URL: ").append(str).append(". ").append(message2).toString());
        } catch (RuntimeException e3) {
            String message3 = e3.getMessage();
            ms.e(new StringBuilder(String.valueOf(str).length() + 27 + String.valueOf(message3).length()).append("Error while pinging URL: ").append(str).append(". ").append(message3).toString());
        } catch (Throwable th) {
            httpURLConnection.disconnect();
            throw th;
        }
    }
}
