package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.RemoteException;
import com.google.android.gms.b.b;

final class aoz extends a<apv> {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ Context f2824a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ aot f2825b;
    private final /* synthetic */ String c;
    private final /* synthetic */ aox d;

    aoz(aox aox, Context context, aot aot, String str) {
        this.d = aox;
        this.f2824a = context;
        this.f2825b = aot;
        this.c = str;
        super();
    }

    public final /* synthetic */ Object a() throws RemoteException {
        apv a2 = this.d.c.a(this.f2824a, this.f2825b, this.c, null, 3);
        if (a2 != null) {
            return a2;
        }
        aox.a(this.f2824a, "search");
        return new arl();
    }

    public final /* synthetic */ Object a(aqh aqh) throws RemoteException {
        return aqh.createSearchAdManager(b.a(this.f2824a), this.f2825b, this.c, 12451000);
    }
}
