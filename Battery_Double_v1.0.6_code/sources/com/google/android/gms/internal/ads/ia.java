package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.a.b;
import java.util.ArrayList;
import java.util.List;

public final class ia implements Creator<hz> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        ArrayList arrayList = null;
        boolean z = false;
        int b2 = b.b(parcel);
        boolean z2 = false;
        List list = null;
        boolean z3 = false;
        boolean z4 = false;
        String str = null;
        String str2 = null;
        while (parcel.dataPosition() < b2) {
            int a2 = b.a(parcel);
            switch (b.a(a2)) {
                case 2:
                    str2 = b.k(parcel, a2);
                    break;
                case 3:
                    str = b.k(parcel, a2);
                    break;
                case 4:
                    z4 = b.c(parcel, a2);
                    break;
                case 5:
                    z3 = b.c(parcel, a2);
                    break;
                case 6:
                    list = b.q(parcel, a2);
                    break;
                case 7:
                    z2 = b.c(parcel, a2);
                    break;
                case 8:
                    z = b.c(parcel, a2);
                    break;
                case 9:
                    arrayList = b.q(parcel, a2);
                    break;
                default:
                    b.b(parcel, a2);
                    break;
            }
        }
        b.r(parcel, b2);
        return new hz(str2, str, z4, z3, list, z2, z, arrayList);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new hz[i];
    }
}
