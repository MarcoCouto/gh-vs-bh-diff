package com.google.android.gms.internal.ads;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public final class aqu extends ajk implements aqs {
    aqu(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.client.IVideoController");
    }

    public final void a() throws RemoteException {
        b(1, r_());
    }

    public final void a(aqv aqv) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (IInterface) aqv);
        b(8, r_);
    }

    public final void a(boolean z) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, z);
        b(3, r_);
    }

    public final void b() throws RemoteException {
        b(2, r_());
    }

    public final boolean c() throws RemoteException {
        Parcel a2 = a(4, r_());
        boolean a3 = ajm.a(a2);
        a2.recycle();
        return a3;
    }

    public final int d() throws RemoteException {
        Parcel a2 = a(5, r_());
        int readInt = a2.readInt();
        a2.recycle();
        return readInt;
    }

    public final float e() throws RemoteException {
        Parcel a2 = a(9, r_());
        float readFloat = a2.readFloat();
        a2.recycle();
        return readFloat;
    }

    public final float f() throws RemoteException {
        Parcel a2 = a(6, r_());
        float readFloat = a2.readFloat();
        a2.recycle();
        return readFloat;
    }

    public final float g() throws RemoteException {
        Parcel a2 = a(7, r_());
        float readFloat = a2.readFloat();
        a2.recycle();
        return readFloat;
    }

    public final aqv h() throws RemoteException {
        aqv aqx;
        Parcel a2 = a(11, r_());
        IBinder readStrongBinder = a2.readStrongBinder();
        if (readStrongBinder == null) {
            aqx = null;
        } else {
            IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.IVideoLifecycleCallbacks");
            aqx = queryLocalInterface instanceof aqv ? (aqv) queryLocalInterface : new aqx(readStrongBinder);
        }
        a2.recycle();
        return aqx;
    }

    public final boolean i() throws RemoteException {
        Parcel a2 = a(10, r_());
        boolean a3 = ajm.a(a2);
        a2.recycle();
        return a3;
    }

    public final boolean j() throws RemoteException {
        Parcel a2 = a(12, r_());
        boolean a3 = ajm.a(a2);
        a2.recycle();
        return a3;
    }
}
