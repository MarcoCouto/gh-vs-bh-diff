package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.abp.b;
import com.google.android.gms.internal.ads.abp.e;

public final class wn extends abp<wn, a> implements acy {
    private static volatile adi<wn> zzakh;
    /* access modifiers changed from: private */
    public static final wn zzdjq = new wn();
    private int zzdih;
    private aah zzdip = aah.f2393a;
    private wp zzdjp;

    public static final class a extends com.google.android.gms.internal.ads.abp.a<wn, a> implements acy {
        private a() {
            super(wn.zzdjq);
        }

        /* synthetic */ a(wo woVar) {
            this();
        }

        public final a a(int i) {
            b();
            ((wn) this.f2433a).b(0);
            return this;
        }

        public final a a(aah aah) {
            b();
            ((wn) this.f2433a).b(aah);
            return this;
        }

        public final a a(wp wpVar) {
            b();
            ((wn) this.f2433a).a(wpVar);
            return this;
        }
    }

    static {
        abp.a(wn.class, zzdjq);
    }

    private wn() {
    }

    public static wn a(aah aah) throws abv {
        return (wn) abp.a(zzdjq, aah);
    }

    /* access modifiers changed from: private */
    public final void a(wp wpVar) {
        if (wpVar == null) {
            throw new NullPointerException();
        }
        this.zzdjp = wpVar;
    }

    /* access modifiers changed from: private */
    public final void b(int i) {
        this.zzdih = i;
    }

    /* access modifiers changed from: private */
    public final void b(aah aah) {
        if (aah == null) {
            throw new NullPointerException();
        }
        this.zzdip = aah;
    }

    public static a d() {
        return (a) ((com.google.android.gms.internal.ads.abp.a) zzdjq.a(e.e, (Object) null, (Object) null));
    }

    public final int a() {
        return this.zzdih;
    }

    /* JADX WARNING: type inference failed for: r0v6, types: [com.google.android.gms.internal.ads.adi<com.google.android.gms.internal.ads.wn>] */
    /* JADX WARNING: type inference failed for: r0v7, types: [java.lang.Object] */
    /* JADX WARNING: type inference failed for: r0v8, types: [com.google.android.gms.internal.ads.adi<com.google.android.gms.internal.ads.wn>, com.google.android.gms.internal.ads.abp$b] */
    /* JADX WARNING: type inference failed for: r0v16 */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r0v8, types: [com.google.android.gms.internal.ads.adi<com.google.android.gms.internal.ads.wn>, com.google.android.gms.internal.ads.abp$b]
  assigns: [com.google.android.gms.internal.ads.abp$b]
  uses: [com.google.android.gms.internal.ads.adi<com.google.android.gms.internal.ads.wn>]
  mth insns count: 42
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
    	at jadx.core.ProcessClass.process(ProcessClass.java:35)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Unknown variable types count: 2 */
    public final Object a(int i, Object obj, Object obj2) {
        ? r0;
        switch (wo.f3760a[i - 1]) {
            case 1:
                return new wn();
            case 2:
                return new a(null);
            case 3:
                Object[] objArr = {"zzdih", "zzdjp", "zzdip"};
                return a((acw) zzdjq, "\u0000\u0003\u0000\u0000\u0001\u0003\u0003\u0004\u0000\u0000\u0000\u0001\u000b\u0002\t\u0003\n", objArr);
            case 4:
                return zzdjq;
            case 5:
                adi<wn> adi = zzakh;
                if (adi != null) {
                    return adi;
                }
                synchronized (wn.class) {
                    r0 = zzakh;
                    if (r0 == 0) {
                        ? bVar = new b(zzdjq);
                        zzakh = bVar;
                        r0 = bVar;
                    }
                }
                return r0;
            case 6:
                return Byte.valueOf(1);
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    public final wp b() {
        return this.zzdjp == null ? wp.f() : this.zzdjp;
    }

    public final aah c() {
        return this.zzdip;
    }
}
