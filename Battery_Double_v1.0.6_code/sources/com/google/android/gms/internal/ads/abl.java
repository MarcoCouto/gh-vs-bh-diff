package com.google.android.gms.internal.ads;

final /* synthetic */ class abl {

    /* renamed from: a reason: collision with root package name */
    static final /* synthetic */ int[] f2426a = new int[abm.values().length];

    /* renamed from: b reason: collision with root package name */
    static final /* synthetic */ int[] f2427b = new int[abx.values().length];

    static {
        try {
            f2427b[abx.BYTE_STRING.ordinal()] = 1;
        } catch (NoSuchFieldError e) {
        }
        try {
            f2427b[abx.MESSAGE.ordinal()] = 2;
        } catch (NoSuchFieldError e2) {
        }
        try {
            f2427b[abx.STRING.ordinal()] = 3;
        } catch (NoSuchFieldError e3) {
        }
        try {
            f2426a[abm.MAP.ordinal()] = 1;
        } catch (NoSuchFieldError e4) {
        }
        try {
            f2426a[abm.VECTOR.ordinal()] = 2;
        } catch (NoSuchFieldError e5) {
        }
        try {
            f2426a[abm.SCALAR.ordinal()] = 3;
        } catch (NoSuchFieldError e6) {
        }
    }
}
