package com.google.android.gms.internal.ads;

import java.lang.reflect.Type;

public enum abk {
    DOUBLE(0, abm.SCALAR, abx.DOUBLE),
    FLOAT(1, abm.SCALAR, abx.FLOAT),
    INT64(2, abm.SCALAR, abx.LONG),
    UINT64(3, abm.SCALAR, abx.LONG),
    INT32(4, abm.SCALAR, abx.INT),
    FIXED64(5, abm.SCALAR, abx.LONG),
    FIXED32(6, abm.SCALAR, abx.INT),
    BOOL(7, abm.SCALAR, abx.BOOLEAN),
    STRING(8, abm.SCALAR, abx.STRING),
    MESSAGE(9, abm.SCALAR, abx.MESSAGE),
    BYTES(10, abm.SCALAR, abx.BYTE_STRING),
    UINT32(11, abm.SCALAR, abx.INT),
    ENUM(12, abm.SCALAR, abx.ENUM),
    SFIXED32(13, abm.SCALAR, abx.INT),
    SFIXED64(14, abm.SCALAR, abx.LONG),
    SINT32(15, abm.SCALAR, abx.INT),
    SINT64(16, abm.SCALAR, abx.LONG),
    GROUP(17, abm.SCALAR, abx.MESSAGE),
    DOUBLE_LIST(18, abm.VECTOR, abx.DOUBLE),
    FLOAT_LIST(19, abm.VECTOR, abx.FLOAT),
    INT64_LIST(20, abm.VECTOR, abx.LONG),
    UINT64_LIST(21, abm.VECTOR, abx.LONG),
    INT32_LIST(22, abm.VECTOR, abx.INT),
    FIXED64_LIST(23, abm.VECTOR, abx.LONG),
    FIXED32_LIST(24, abm.VECTOR, abx.INT),
    BOOL_LIST(25, abm.VECTOR, abx.BOOLEAN),
    STRING_LIST(26, abm.VECTOR, abx.STRING),
    MESSAGE_LIST(27, abm.VECTOR, abx.MESSAGE),
    BYTES_LIST(28, abm.VECTOR, abx.BYTE_STRING),
    UINT32_LIST(29, abm.VECTOR, abx.INT),
    ENUM_LIST(30, abm.VECTOR, abx.ENUM),
    SFIXED32_LIST(31, abm.VECTOR, abx.INT),
    SFIXED64_LIST(32, abm.VECTOR, abx.LONG),
    SINT32_LIST(33, abm.VECTOR, abx.INT),
    SINT64_LIST(34, abm.VECTOR, abx.LONG),
    DOUBLE_LIST_PACKED(35, abm.PACKED_VECTOR, abx.DOUBLE),
    FLOAT_LIST_PACKED(36, abm.PACKED_VECTOR, abx.FLOAT),
    INT64_LIST_PACKED(37, abm.PACKED_VECTOR, abx.LONG),
    UINT64_LIST_PACKED(38, abm.PACKED_VECTOR, abx.LONG),
    INT32_LIST_PACKED(39, abm.PACKED_VECTOR, abx.INT),
    FIXED64_LIST_PACKED(40, abm.PACKED_VECTOR, abx.LONG),
    FIXED32_LIST_PACKED(41, abm.PACKED_VECTOR, abx.INT),
    BOOL_LIST_PACKED(42, abm.PACKED_VECTOR, abx.BOOLEAN),
    UINT32_LIST_PACKED(43, abm.PACKED_VECTOR, abx.INT),
    ENUM_LIST_PACKED(44, abm.PACKED_VECTOR, abx.ENUM),
    SFIXED32_LIST_PACKED(45, abm.PACKED_VECTOR, abx.INT),
    SFIXED64_LIST_PACKED(46, abm.PACKED_VECTOR, abx.LONG),
    SINT32_LIST_PACKED(47, abm.PACKED_VECTOR, abx.INT),
    SINT64_LIST_PACKED(48, abm.PACKED_VECTOR, abx.LONG),
    GROUP_LIST(49, abm.VECTOR, abx.MESSAGE),
    MAP(50, abm.MAP, abx.VOID);
    
    private static final abk[] ae = null;
    private static final Type[] af = null;
    private final abx Z;
    private final int aa;
    private final abm ab;
    private final Class<?> ac;
    private final boolean ad;

    static {
        int i;
        af = new Type[0];
        abk[] values = values();
        ae = new abk[values.length];
        for (abk abk : values) {
            ae[abk.aa] = abk;
        }
    }

    private abk(int i, abm abm, abx abx) {
        this.aa = i;
        this.ab = abm;
        this.Z = abx;
        switch (abl.f2426a[abm.ordinal()]) {
            case 1:
                this.ac = abx.a();
                break;
            case 2:
                this.ac = abx.a();
                break;
            default:
                this.ac = null;
                break;
        }
        boolean z = false;
        if (abm == abm.SCALAR) {
            switch (abl.f2427b[abx.ordinal()]) {
                case 1:
                case 2:
                case 3:
                    break;
                default:
                    z = true;
                    break;
            }
        }
        this.ad = z;
    }

    public final int a() {
        return this.aa;
    }
}
