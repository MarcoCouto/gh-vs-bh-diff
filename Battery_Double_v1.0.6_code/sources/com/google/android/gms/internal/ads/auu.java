package com.google.android.gms.internal.ads;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.List;

public final class auu extends ajk implements aus {
    auu(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.formats.client.IAttributionInfo");
    }

    public final String a() throws RemoteException {
        Parcel a2 = a(2, r_());
        String readString = a2.readString();
        a2.recycle();
        return readString;
    }

    public final List<auw> b() throws RemoteException {
        Parcel a2 = a(3, r_());
        ArrayList b2 = ajm.b(a2);
        a2.recycle();
        return b2;
    }
}
