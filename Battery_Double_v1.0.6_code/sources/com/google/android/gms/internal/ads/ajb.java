package com.google.android.gms.internal.ads;

import java.lang.reflect.InvocationTargetException;

public final class ajb extends ajj {
    private final StackTraceElement[] d;

    public ajb(ahy ahy, String str, String str2, zz zzVar, int i, int i2, StackTraceElement[] stackTraceElementArr) {
        super(ahy, str, str2, zzVar, i, 45);
        this.d = stackTraceElementArr;
    }

    /* access modifiers changed from: protected */
    public final void a() throws IllegalAccessException, InvocationTargetException {
        if (this.d != null) {
            ahw ahw = new ahw((String) this.c.invoke(null, new Object[]{this.d}));
            synchronized (this.f2637b) {
                this.f2637b.B = ahw.f2602a;
                if (ahw.f2603b.booleanValue()) {
                    this.f2637b.J = Integer.valueOf(ahw.c.booleanValue() ? 0 : 1);
                }
            }
        }
    }
}
