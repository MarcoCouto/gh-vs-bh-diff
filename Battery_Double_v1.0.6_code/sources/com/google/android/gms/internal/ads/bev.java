package com.google.android.gms.internal.ads;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import com.google.android.gms.ads.internal.ax;

final class bev implements OnClickListener {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ beu f3182a;

    bev(beu beu) {
        this.f3182a = beu;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        Intent b2 = this.f3182a.b();
        ax.e();
        jv.a(this.f3182a.f3181b, b2);
    }
}
