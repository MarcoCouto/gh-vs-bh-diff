package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.b.a;
import java.util.List;

public interface avp extends IInterface {
    String a() throws RemoteException;

    void a(Bundle bundle) throws RemoteException;

    List b() throws RemoteException;

    boolean b(Bundle bundle) throws RemoteException;

    a c() throws RemoteException;

    void c(Bundle bundle) throws RemoteException;

    String d() throws RemoteException;

    String e() throws RemoteException;

    auw f() throws RemoteException;

    String g() throws RemoteException;

    String h() throws RemoteException;

    aqs i() throws RemoteException;

    a j() throws RemoteException;

    Bundle n() throws RemoteException;

    aus p() throws RemoteException;

    void q() throws RemoteException;
}
