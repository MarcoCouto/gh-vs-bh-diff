package com.google.android.gms.internal.ads;

import java.lang.reflect.InvocationTargetException;

public final class air extends ajj {
    private long d;

    public air(ahy ahy, String str, String str2, zz zzVar, long j, int i, int i2) {
        super(ahy, str, str2, zzVar, i, 25);
        this.d = j;
    }

    /* access modifiers changed from: protected */
    public final void a() throws IllegalAccessException, InvocationTargetException {
        long longValue = ((Long) this.c.invoke(null, new Object[0])).longValue();
        synchronized (this.f2637b) {
            this.f2637b.W = Long.valueOf(longValue);
            if (this.d != 0) {
                this.f2637b.j = Long.valueOf(longValue - this.d);
                this.f2637b.m = Long.valueOf(this.d);
            }
        }
    }
}
