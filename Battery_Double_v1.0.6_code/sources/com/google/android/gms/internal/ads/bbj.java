package com.google.android.gms.internal.ads;

import android.content.Context;

@cm
public final class bbj {

    /* renamed from: a reason: collision with root package name */
    private final Object f3102a = new Object();

    /* renamed from: b reason: collision with root package name */
    private bbq f3103b;

    public final bbq a(Context context, mu muVar) {
        bbq bbq;
        synchronized (this.f3102a) {
            if (this.f3103b == null) {
                Context applicationContext = context.getApplicationContext();
                if (applicationContext != null) {
                    context = applicationContext;
                }
                this.f3103b = new bbq(context, muVar, (String) ape.f().a(asi.f2875a));
            }
            bbq = this.f3103b;
        }
        return bbq;
    }
}
