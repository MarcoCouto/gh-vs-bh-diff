package com.google.android.gms.internal.ads;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import com.google.android.gms.ads.internal.ax;
import com.google.android.gms.b.b;
import com.google.android.gms.common.internal.aa;
import java.util.List;

@cm
public final class atl extends RelativeLayout {

    /* renamed from: a reason: collision with root package name */
    private static final float[] f2902a = {5.0f, 5.0f, 5.0f, 5.0f, 5.0f, 5.0f, 5.0f, 5.0f};

    /* renamed from: b reason: collision with root package name */
    private AnimationDrawable f2903b;

    public atl(Context context, ati ati, LayoutParams layoutParams) {
        super(context);
        aa.a(ati);
        ShapeDrawable shapeDrawable = new ShapeDrawable(new RoundRectShape(f2902a, null, null));
        shapeDrawable.getPaint().setColor(ati.d());
        setLayoutParams(layoutParams);
        ax.g().a((View) this, (Drawable) shapeDrawable);
        LayoutParams layoutParams2 = new LayoutParams(-2, -2);
        if (!TextUtils.isEmpty(ati.a())) {
            LayoutParams layoutParams3 = new LayoutParams(-2, -2);
            TextView textView = new TextView(context);
            textView.setLayoutParams(layoutParams3);
            textView.setId(1195835393);
            textView.setTypeface(Typeface.DEFAULT);
            textView.setText(ati.a());
            textView.setTextColor(ati.e());
            textView.setTextSize((float) ati.f());
            ape.a();
            int a2 = mh.a(context, 4);
            ape.a();
            textView.setPadding(a2, 0, mh.a(context, 4), 0);
            addView(textView);
            layoutParams2.addRule(1, textView.getId());
        }
        ImageView imageView = new ImageView(context);
        imageView.setLayoutParams(layoutParams2);
        imageView.setId(1195835394);
        List<atm> c = ati.c();
        if (c != null && c.size() > 1) {
            this.f2903b = new AnimationDrawable();
            for (atm a3 : c) {
                try {
                    this.f2903b.addFrame((Drawable) b.a(a3.a()), ati.g());
                } catch (Exception e) {
                    jm.b("Error while getting drawable.", e);
                }
            }
            ax.g().a((View) imageView, (Drawable) this.f2903b);
        } else if (c.size() == 1) {
            try {
                imageView.setImageDrawable((Drawable) b.a(((atm) c.get(0)).a()));
            } catch (Exception e2) {
                jm.b("Error while getting drawable.", e2);
            }
        }
        addView(imageView);
    }

    public final void onAttachedToWindow() {
        if (this.f2903b != null) {
            this.f2903b.start();
        }
        super.onAttachedToWindow();
    }
}
