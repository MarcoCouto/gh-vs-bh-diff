package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.b.a.C0046a;

public abstract class ata extends ajl implements asz {
    public ata() {
        super("com.google.android.gms.ads.internal.customrenderedad.client.ICustomRenderedAd");
    }

    /* access modifiers changed from: protected */
    public final boolean dispatchTransaction(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        switch (i) {
            case 1:
                String a2 = a();
                parcel2.writeNoException();
                parcel2.writeString(a2);
                break;
            case 2:
                String b2 = b();
                parcel2.writeNoException();
                parcel2.writeString(b2);
                break;
            case 3:
                a(C0046a.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
            case 4:
                c();
                parcel2.writeNoException();
                break;
            case 5:
                d();
                parcel2.writeNoException();
                break;
            default:
                return false;
        }
        return true;
    }
}
