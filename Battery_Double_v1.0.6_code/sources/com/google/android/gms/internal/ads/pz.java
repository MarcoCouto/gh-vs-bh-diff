package com.google.android.gms.internal.ads;

import com.hmatalonga.greenhub.Config;
import java.util.HashMap;
import java.util.Map;

final class pz implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ String f3621a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ String f3622b;
    private final /* synthetic */ int c;
    private final /* synthetic */ int d;
    private final /* synthetic */ boolean e = false;
    private final /* synthetic */ py f;

    pz(py pyVar, String str, String str2, int i, int i2, boolean z) {
        this.f = pyVar;
        this.f3621a = str;
        this.f3622b = str2;
        this.c = i;
        this.d = i2;
    }

    public final void run() {
        HashMap hashMap = new HashMap();
        hashMap.put("event", "precacheProgress");
        hashMap.put("src", this.f3621a);
        hashMap.put("cachedSrc", this.f3622b);
        hashMap.put("bytesLoaded", Integer.toString(this.c));
        hashMap.put("totalBytes", Integer.toString(this.d));
        hashMap.put("cacheReady", this.e ? "1" : Config.NOTIFICATION_DEFAULT_PRIORITY);
        this.f.a("onPrecacheEvent", (Map<String, String>) hashMap);
    }
}
