package com.google.android.gms.internal.ads;

public class df extends Exception {

    /* renamed from: a reason: collision with root package name */
    private final atz f3252a;

    /* renamed from: b reason: collision with root package name */
    private long f3253b;

    public df() {
        this.f3252a = null;
    }

    public df(atz atz) {
        this.f3252a = atz;
    }

    public df(String str) {
        super(str);
        this.f3252a = null;
    }

    public df(Throwable th) {
        super(th);
        this.f3252a = null;
    }

    /* access modifiers changed from: 0000 */
    public final void a(long j) {
        this.f3253b = j;
    }
}
