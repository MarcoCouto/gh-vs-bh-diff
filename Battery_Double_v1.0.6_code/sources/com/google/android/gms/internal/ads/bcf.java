package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.Bundle;
import android.os.RemoteException;
import android.os.SystemClock;
import android.text.TextUtils;
import com.google.ads.mediation.AdUrlAdapter;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.b.d;
import com.google.android.gms.ads.b.d.a;
import com.google.android.gms.ads.internal.ax;
import com.google.android.gms.ads.mediation.b;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

@cm
public final class bcf implements bcj {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public final String f3127a;

    /* renamed from: b reason: collision with root package name */
    private final bcr f3128b;
    private final long c;
    private final bcb d;
    private final bca e;
    private aop f;
    private final aot g;
    private final Context h;
    /* access modifiers changed from: private */
    public final Object i = new Object();
    private final mu j;
    private final boolean k;
    private final aul l;
    private final List<String> m;
    private final List<String> n;
    private final List<String> o;
    private final boolean p;
    private final boolean q;
    /* access modifiers changed from: private */
    public bcu r;
    /* access modifiers changed from: private */
    public int s = -2;
    private bda t;

    public bcf(Context context, String str, bcr bcr, bcb bcb, bca bca, aop aop, aot aot, mu muVar, boolean z, boolean z2, aul aul, List<String> list, List<String> list2, List<String> list3, boolean z3) {
        this.h = context;
        this.f3128b = bcr;
        this.e = bca;
        if ("com.google.ads.mediation.customevent.CustomEventAdapter".equals(str)) {
            this.f3127a = b();
        } else {
            this.f3127a = str;
        }
        this.d = bcb;
        if (bca.t != -1) {
            this.c = bca.t;
        } else if (bcb.f3122b != -1) {
            this.c = bcb.f3122b;
        } else {
            this.c = 10000;
        }
        this.f = aop;
        this.g = aot;
        this.j = muVar;
        this.k = z;
        this.p = z2;
        this.l = aul;
        this.m = list;
        this.n = list2;
        this.o = list3;
        this.q = z3;
    }

    private static bcu a(b bVar) {
        return new bdp(bVar);
    }

    private final String a(String str) {
        if (str == null || !e() || b(2)) {
            return str;
        }
        try {
            JSONObject jSONObject = new JSONObject(str);
            jSONObject.remove("cpm_floor_cents");
            return jSONObject.toString();
        } catch (JSONException e2) {
            jm.e("Could not remove field. Returning the original value");
            return str;
        }
    }

    /* access modifiers changed from: private */
    public final void a(bce bce) {
        String a2 = a(this.e.k);
        try {
            if (this.j.c < 4100000) {
                if (this.g.d) {
                    this.r.a(com.google.android.gms.b.b.a(this.h), this.f, a2, bce);
                } else {
                    this.r.a(com.google.android.gms.b.b.a(this.h), this.g, this.f, a2, (bcx) bce);
                }
            } else if (this.k || this.e.b()) {
                ArrayList arrayList = new ArrayList(this.m);
                if (this.n != null) {
                    for (String str : this.n) {
                        String str2 = ":false";
                        if (this.o != null && this.o.contains(str)) {
                            str2 = ":true";
                        }
                        arrayList.add(new StringBuilder(String.valueOf(str).length() + 7 + String.valueOf(str2).length()).append("custom:").append(str).append(str2).toString());
                    }
                }
                this.r.a(com.google.android.gms.b.b.a(this.h), this.f, a2, this.e.f3119a, bce, this.l, arrayList);
            } else if (this.g.d) {
                this.r.a(com.google.android.gms.b.b.a(this.h), this.f, a2, this.e.f3119a, (bcx) bce);
            } else if (!this.p) {
                this.r.a(com.google.android.gms.b.b.a(this.h), this.g, this.f, a2, this.e.f3119a, bce);
            } else if (this.e.o != null) {
                this.r.a(com.google.android.gms.b.b.a(this.h), this.f, a2, this.e.f3119a, bce, new aul(b(this.e.s)), this.e.r);
            } else {
                this.r.a(com.google.android.gms.b.b.a(this.h), this.g, this.f, a2, this.e.f3119a, bce);
            }
        } catch (RemoteException e2) {
            jm.c("Could not request ad from mediation adapter.", e2);
            a(5);
        }
    }

    private static d b(String str) {
        int i2 = 0;
        a aVar = new a();
        if (str == null) {
            return aVar.a();
        }
        try {
            JSONObject jSONObject = new JSONObject(str);
            aVar.b(jSONObject.optBoolean("multiple_images", false));
            aVar.a(jSONObject.optBoolean("only_urls", false));
            String optString = jSONObject.optString("native_image_orientation", "any");
            if ("landscape".equals(optString)) {
                i2 = 2;
            } else if ("portrait".equals(optString)) {
                i2 = 1;
            } else if (!"any".equals(optString)) {
                i2 = -1;
            }
            aVar.a(i2);
        } catch (JSONException e2) {
            jm.c("Exception occurred when creating native ad options", e2);
        }
        return aVar.a();
    }

    private final String b() {
        try {
            if (!TextUtils.isEmpty(this.e.e)) {
                return this.f3128b.b(this.e.e) ? "com.google.android.gms.ads.mediation.customevent.CustomEventAdapter" : "com.google.ads.mediation.customevent.CustomEventAdapter";
            }
        } catch (RemoteException e2) {
            jm.e("Fail to determine the custom event's version, assuming the old one.");
        }
        return "com.google.ads.mediation.customevent.CustomEventAdapter";
    }

    /* access modifiers changed from: private */
    public final boolean b(int i2) {
        try {
            Bundle j2 = this.k ? this.r.l() : this.g.d ? this.r.k() : this.r.j();
            return j2 != null && (j2.getInt("capabilities", 0) & i2) == i2;
        } catch (RemoteException e2) {
            jm.e("Could not get adapter info. Returning false");
            return false;
        }
    }

    private final bda c() {
        if (this.s != 0 || !e()) {
            return null;
        }
        try {
            if (!(!b(4) || this.t == null || this.t.a() == 0)) {
                return this.t;
            }
        } catch (RemoteException e2) {
            jm.e("Could not get cpm value from MediationResponseMetadata");
        }
        return new bch(f());
    }

    /* access modifiers changed from: private */
    public final bcu d() {
        String str = "Instantiating mediation adapter: ";
        String valueOf = String.valueOf(this.f3127a);
        jm.d(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
        if (!this.k && !this.e.b()) {
            if (((Boolean) ape.f().a(asi.bw)).booleanValue() && "com.google.ads.mediation.admob.AdMobAdapter".equals(this.f3127a)) {
                return a((b) new AdMobAdapter());
            }
            if (((Boolean) ape.f().a(asi.bx)).booleanValue() && "com.google.ads.mediation.AdUrlAdapter".equals(this.f3127a)) {
                return a((b) new AdUrlAdapter());
            }
            if ("com.google.ads.mediation.admob.AdMobCustomTabsAdapter".equals(this.f3127a)) {
                return new bdp(new zzzv());
            }
        }
        try {
            return this.f3128b.a(this.f3127a);
        } catch (RemoteException e2) {
            RemoteException remoteException = e2;
            String str2 = "Could not instantiate mediation adapter: ";
            String valueOf2 = String.valueOf(this.f3127a);
            jm.a(valueOf2.length() != 0 ? str2.concat(valueOf2) : new String(str2), remoteException);
            return null;
        }
    }

    /* access modifiers changed from: private */
    public final boolean e() {
        return this.d.m != -1;
    }

    private final int f() {
        if (this.e.k == null) {
            return 0;
        }
        try {
            JSONObject jSONObject = new JSONObject(this.e.k);
            if ("com.google.ads.mediation.admob.AdMobAdapter".equals(this.f3127a)) {
                return jSONObject.optInt("cpm_cents", 0);
            }
            int i2 = b(2) ? jSONObject.optInt("cpm_floor_cents", 0) : 0;
            return i2 == 0 ? jSONObject.optInt("penalized_average_cpm_cents", 0) : i2;
        } catch (JSONException e2) {
            jm.e("Could not convert to json. Returning 0");
            return 0;
        }
    }

    public final bci a(long j2, long j3) {
        bci bci;
        synchronized (this.i) {
            long elapsedRealtime = SystemClock.elapsedRealtime();
            bce bce = new bce();
            jv.f3440a.post(new bcg(this, bce));
            long j4 = this.c;
            while (this.s == -2) {
                long elapsedRealtime2 = SystemClock.elapsedRealtime();
                long j5 = j4 - (elapsedRealtime2 - elapsedRealtime);
                long j6 = j3 - (elapsedRealtime2 - j2);
                if (j5 <= 0 || j6 <= 0) {
                    jm.d("Timed out waiting for adapter.");
                    this.s = 3;
                } else {
                    try {
                        this.i.wait(Math.min(j5, j6));
                    } catch (InterruptedException e2) {
                        this.s = 5;
                    }
                }
            }
            bci = new bci(this.e, this.r, this.f3127a, bce, this.s, c(), ax.l().b() - elapsedRealtime);
        }
        return bci;
    }

    public final void a() {
        synchronized (this.i) {
            try {
                if (this.r != null) {
                    this.r.c();
                }
            } catch (RemoteException e2) {
                jm.c("Could not destroy mediation adapter.", e2);
            }
            this.s = -1;
            this.i.notify();
        }
    }

    public final void a(int i2) {
        synchronized (this.i) {
            this.s = i2;
            this.i.notify();
        }
    }

    public final void a(int i2, bda bda) {
        synchronized (this.i) {
            this.s = 0;
            this.t = bda;
            this.i.notify();
        }
    }
}
