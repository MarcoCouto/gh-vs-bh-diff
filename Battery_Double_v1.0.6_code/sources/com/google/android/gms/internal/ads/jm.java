package com.google.android.gms.internal.ads;

import android.util.Log;

@cm
public final class jm extends ms {
    public static void a(String str) {
        if (a()) {
            Log.v("Ads", str);
        }
    }

    public static boolean a() {
        if (a(2)) {
            if (((Boolean) ape.f().a(asi.bl)).booleanValue()) {
                return true;
            }
        }
        return false;
    }
}
