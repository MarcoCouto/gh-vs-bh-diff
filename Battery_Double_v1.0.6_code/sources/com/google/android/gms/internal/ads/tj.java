package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.HandlerThread;
import com.google.android.gms.common.internal.e.a;
import com.google.android.gms.common.internal.e.b;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

final class tj implements a, b {

    /* renamed from: a reason: collision with root package name */
    private tk f3706a;

    /* renamed from: b reason: collision with root package name */
    private final String f3707b;
    private final String c;
    private final LinkedBlockingQueue<zz> d;
    private final HandlerThread e = new HandlerThread("GassClient");

    public tj(Context context, String str, String str2) {
        this.f3707b = str;
        this.c = str2;
        this.e.start();
        this.f3706a = new tk(context, this.e.getLooper(), this, this);
        this.d = new LinkedBlockingQueue<>();
        this.f3706a.o();
    }

    private final tp a() {
        try {
            return this.f3706a.A();
        } catch (DeadObjectException | IllegalStateException e2) {
            return null;
        }
    }

    private final void b() {
        if (this.f3706a == null) {
            return;
        }
        if (this.f3706a.b() || this.f3706a.c()) {
            this.f3706a.a();
        }
    }

    private static zz c() {
        zz zzVar = new zz();
        zzVar.k = Long.valueOf(32768);
        return zzVar;
    }

    public final void a(int i) {
        try {
            this.d.put(c());
        } catch (InterruptedException e2) {
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0039, code lost:
        b();
        r4.e.quit();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0041, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0038, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0038 A[ExcHandler:  FINALLY, Splitter:B:2:0x0006] */
    public final void a(Bundle bundle) {
        tp a2 = a();
        if (a2 != null) {
            try {
                this.d.put(a2.a(new tl(this.f3707b, this.c)).a());
            } catch (Throwable th) {
                this.d.put(c());
            } finally {
            }
        }
    }

    public final void a(com.google.android.gms.common.b bVar) {
        try {
            this.d.put(c());
        } catch (InterruptedException e2) {
        }
    }

    public final zz b(int i) {
        zz zzVar;
        try {
            zzVar = (zz) this.d.poll(5000, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e2) {
            zzVar = null;
        }
        return zzVar == null ? c() : zzVar;
    }
}
