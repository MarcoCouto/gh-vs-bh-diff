package com.google.android.gms.internal.ads;

import java.util.Collections;
import java.util.List;
import java.util.Map;

public final class agy {

    /* renamed from: a reason: collision with root package name */
    public byte[] f2576a;

    /* renamed from: b reason: collision with root package name */
    public String f2577b;
    public long c;
    public long d;
    public long e;
    public long f;
    public Map<String, String> g = Collections.emptyMap();
    public List<aqd> h;

    public final boolean a() {
        return this.e < System.currentTimeMillis();
    }
}
