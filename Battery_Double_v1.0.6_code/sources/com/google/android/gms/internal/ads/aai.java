package com.google.android.gms.internal.ads;

import java.util.Iterator;
import java.util.NoSuchElementException;

final class aai implements Iterator {

    /* renamed from: a reason: collision with root package name */
    private int f2395a = 0;

    /* renamed from: b reason: collision with root package name */
    private final int f2396b = this.c.a();
    private final /* synthetic */ aah c;

    aai(aah aah) {
        this.c = aah;
    }

    private final byte a() {
        try {
            aah aah = this.c;
            int i = this.f2395a;
            this.f2395a = i + 1;
            return aah.a(i);
        } catch (IndexOutOfBoundsException e) {
            throw new NoSuchElementException(e.getMessage());
        }
    }

    public final boolean hasNext() {
        return this.f2395a < this.f2396b;
    }

    public final /* synthetic */ Object next() {
        return Byte.valueOf(a());
    }

    public final void remove() {
        throw new UnsupportedOperationException();
    }
}
