package com.google.android.gms.internal.ads;

final class oo implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ String f3576a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ String f3577b;
    private final /* synthetic */ ok c;

    oo(ok okVar, String str, String str2) {
        this.c = okVar;
        this.f3576a = str;
        this.f3577b = str2;
    }

    public final void run() {
        if (this.c.r != null) {
            this.c.r.a(this.f3576a, this.f3577b);
        }
    }
}
