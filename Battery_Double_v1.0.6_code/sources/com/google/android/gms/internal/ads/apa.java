package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.RemoteException;
import com.google.android.gms.b.b;

final class apa extends a<apv> {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ Context f2826a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ aot f2827b;
    private final /* synthetic */ String c;
    private final /* synthetic */ bcr d;
    private final /* synthetic */ aox e;

    apa(aox aox, Context context, aot aot, String str, bcr bcr) {
        this.e = aox;
        this.f2826a = context;
        this.f2827b = aot;
        this.c = str;
        this.d = bcr;
        super();
    }

    public final /* synthetic */ Object a() throws RemoteException {
        apv a2 = this.e.c.a(this.f2826a, this.f2827b, this.c, this.d, 2);
        if (a2 != null) {
            return a2;
        }
        aox.a(this.f2826a, "interstitial");
        return new arl();
    }

    public final /* synthetic */ Object a(aqh aqh) throws RemoteException {
        return aqh.createInterstitialAdManager(b.a(this.f2826a), this.f2827b, this.c, this.d, 12451000);
    }
}
