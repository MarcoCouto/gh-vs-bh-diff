package com.google.android.gms.internal.ads;

import android.os.IBinder;

public final class aqr extends ajk implements aqq {
    aqr(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.client.IMobileAdsSettingManagerCreator");
    }
}
