package com.google.android.gms.internal.ads;

import android.view.View;
import java.lang.ref.WeakReference;

public final class ajw implements alg {

    /* renamed from: a reason: collision with root package name */
    private WeakReference<aty> f2648a;

    public ajw(aty aty) {
        this.f2648a = new WeakReference<>(aty);
    }

    public final View a() {
        aty aty = (aty) this.f2648a.get();
        if (aty != null) {
            return aty.l();
        }
        return null;
    }

    public final boolean b() {
        return this.f2648a.get() == null;
    }

    public final alg c() {
        return new ajy((aty) this.f2648a.get());
    }
}
