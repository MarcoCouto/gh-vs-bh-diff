package com.google.android.gms.internal.ads;

import java.io.IOException;

public final class afv extends afh<afv> {

    /* renamed from: a reason: collision with root package name */
    private byte[] f2543a;

    /* renamed from: b reason: collision with root package name */
    private byte[] f2544b;
    private byte[] c;

    public afv() {
        this.f2543a = null;
        this.f2544b = null;
        this.c = null;
        this.Y = null;
        this.Z = -1;
    }

    /* access modifiers changed from: protected */
    public final int a() {
        int a2 = super.a();
        if (this.f2543a != null) {
            a2 += aff.b(1, this.f2543a);
        }
        if (this.f2544b != null) {
            a2 += aff.b(2, this.f2544b);
        }
        return this.c != null ? a2 + aff.b(3, this.c) : a2;
    }

    public final /* synthetic */ afn a(afd afd) throws IOException {
        while (true) {
            int a2 = afd.a();
            switch (a2) {
                case 0:
                    break;
                case 10:
                    this.f2543a = afd.f();
                    continue;
                case 18:
                    this.f2544b = afd.f();
                    continue;
                case 26:
                    this.c = afd.f();
                    continue;
                default:
                    if (!super.a(afd, a2)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    public final void a(aff aff) throws IOException {
        if (this.f2543a != null) {
            aff.a(1, this.f2543a);
        }
        if (this.f2544b != null) {
            aff.a(2, this.f2544b);
        }
        if (this.c != null) {
            aff.a(3, this.c);
        }
        super.a(aff);
    }
}
