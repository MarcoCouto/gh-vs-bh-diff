package com.google.android.gms.internal.ads;

import java.util.Iterator;
import java.util.Map.Entry;

final class acc<K> implements Iterator<Entry<K, Object>> {

    /* renamed from: a reason: collision with root package name */
    private Iterator<Entry<K, Object>> f2448a;

    public acc(Iterator<Entry<K, Object>> it) {
        this.f2448a = it;
    }

    public final boolean hasNext() {
        return this.f2448a.hasNext();
    }

    public final /* synthetic */ Object next() {
        Entry entry = (Entry) this.f2448a.next();
        return entry.getValue() instanceof aby ? new aca(entry) : entry;
    }

    public final void remove() {
        this.f2448a.remove();
    }
}
