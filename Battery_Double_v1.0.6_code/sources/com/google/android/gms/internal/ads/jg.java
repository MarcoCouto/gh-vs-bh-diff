package com.google.android.gms.internal.ads;

import android.content.Context;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.ax;
import java.math.BigInteger;
import java.util.Locale;

@cm
public final class jg {

    /* renamed from: a reason: collision with root package name */
    private static final Object f3422a = new Object();

    /* renamed from: b reason: collision with root package name */
    private static String f3423b;

    public static String a() {
        String str;
        synchronized (f3422a) {
            str = f3423b;
        }
        return str;
    }

    public static String a(Context context, String str, String str2) {
        String str3;
        synchronized (f3422a) {
            if (f3423b == null && !TextUtils.isEmpty(str)) {
                try {
                    ClassLoader classLoader = context.createPackageContext(str2, 3).getClassLoader();
                    Class cls = Class.forName("com.google.ads.mediation.MediationAdapter", false, classLoader);
                    BigInteger bigInteger = new BigInteger(new byte[1]);
                    String[] split = str.split(",");
                    BigInteger bigInteger2 = bigInteger;
                    for (int i = 0; i < split.length; i++) {
                        ax.e();
                        if (jv.a(classLoader, cls, split[i])) {
                            bigInteger2 = bigInteger2.setBit(i);
                        }
                    }
                    f3423b = String.format(Locale.US, "%X", new Object[]{bigInteger2});
                } catch (Throwable th) {
                    f3423b = "err";
                }
            }
            str3 = f3423b;
        }
        return str3;
    }
}
