package com.google.android.gms.internal.ads;

import java.util.Collections;
import java.util.List;
import org.json.JSONObject;

@cm
public final class ir {
    public final String A;
    public final String B;
    public final aub C;
    public boolean D;
    public boolean E;
    public boolean F;
    public boolean G;
    public boolean H;
    public boolean I;
    public final List<String> J;
    public final amw K;
    public final boolean L;
    public final boolean M;
    public final boolean N;
    public final boolean O;
    private final dr P;
    private final long Q;
    private final long R;
    private final String S;

    /* renamed from: a reason: collision with root package name */
    public final aop f3395a;

    /* renamed from: b reason: collision with root package name */
    public final qn f3396b;
    public final List<String> c;
    public final int d;
    public final List<String> e;
    public final List<String> f;
    public final List<String> g;
    public final int h;
    public final long i;
    public final String j;
    public final JSONObject k;
    public final boolean l;
    public boolean m;
    public final boolean n;
    public final bca o;
    public final bcu p;
    public final String q;
    public final bcb r;
    public final bce s;
    public final String t;
    public final aot u;
    public final hp v;
    public final List<String> w;
    public final List<String> x;
    public final long y;
    public final long z;

    public ir(aop aop, qn qnVar, List<String> list, int i2, List<String> list2, List<String> list3, int i3, long j2, String str, boolean z2, bca bca, bcu bcu, String str2, bcb bcb, bce bce, long j3, aot aot, long j4, long j5, long j6, String str3, JSONObject jSONObject, aub aub, hp hpVar, List<String> list4, List<String> list5, boolean z3, dr drVar, String str4, List<String> list6, String str5, amw amw, boolean z4, boolean z5, boolean z6, List<String> list7, boolean z7, String str6) {
        this.D = false;
        this.E = false;
        this.F = false;
        this.G = false;
        this.H = false;
        this.I = false;
        this.f3395a = aop;
        this.f3396b = qnVar;
        this.c = a(list);
        this.d = i2;
        this.e = a(list2);
        this.g = a(list3);
        this.h = i3;
        this.i = j2;
        this.j = str;
        this.n = z2;
        this.o = bca;
        this.p = bcu;
        this.q = str2;
        this.r = bcb;
        this.s = bce;
        this.Q = j3;
        this.u = aot;
        this.R = j4;
        this.y = j5;
        this.z = j6;
        this.A = str3;
        this.k = jSONObject;
        this.C = aub;
        this.v = hpVar;
        this.w = a(list4);
        this.x = a(list5);
        this.l = z3;
        this.P = drVar;
        this.t = str4;
        this.J = a(list6);
        this.B = str5;
        this.K = amw;
        this.L = z4;
        this.M = z5;
        this.N = z6;
        this.f = a(list7);
        this.O = z7;
        this.S = str6;
    }

    public ir(is isVar, qn qnVar, bca bca, bcu bcu, String str, bce bce, aub aub, String str2) {
        this(isVar.f3397a.c, null, isVar.f3398b.c, isVar.e, isVar.f3398b.e, isVar.f3398b.i, isVar.f3398b.k, isVar.f3398b.j, isVar.f3397a.i, isVar.f3398b.g, null, null, null, isVar.c, null, isVar.f3398b.h, isVar.d, isVar.f3398b.f, isVar.f, isVar.g, isVar.f3398b.n, isVar.h, null, isVar.f3398b.A, isVar.f3398b.B, isVar.f3398b.B, isVar.f3398b.D, isVar.f3398b.E, null, isVar.f3398b.H, isVar.f3398b.L, isVar.i, isVar.f3398b.O, isVar.j, isVar.f3398b.Q, isVar.f3398b.R, isVar.f3398b.S, isVar.f3398b.T);
    }

    private static <T> List<T> a(List<T> list) {
        if (list == null) {
            return null;
        }
        return Collections.unmodifiableList(list);
    }

    public final boolean a() {
        if (this.f3396b == null || this.f3396b.v() == null) {
            return false;
        }
        return this.f3396b.v().b();
    }
}
