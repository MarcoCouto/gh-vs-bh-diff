package com.google.android.gms.internal.ads;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

public final class atz {

    /* renamed from: a reason: collision with root package name */
    public final int f2919a;

    /* renamed from: b reason: collision with root package name */
    public final byte[] f2920b;
    public final Map<String, String> c;
    public final List<aqd> d;
    public final boolean e;
    private final long f;

    private atz(int i, byte[] bArr, Map<String, String> map, List<aqd> list, boolean z, long j) {
        this.f2919a = i;
        this.f2920b = bArr;
        this.c = map;
        if (list == null) {
            this.d = null;
        } else {
            this.d = Collections.unmodifiableList(list);
        }
        this.e = z;
        this.f = j;
    }

    @Deprecated
    public atz(int i, byte[] bArr, Map<String, String> map, boolean z, long j) {
        List arrayList;
        if (map == null) {
            arrayList = null;
        } else if (map.isEmpty()) {
            arrayList = Collections.emptyList();
        } else {
            arrayList = new ArrayList(map.size());
            for (Entry entry : map.entrySet()) {
                arrayList.add(new aqd((String) entry.getKey(), (String) entry.getValue()));
            }
        }
        this(i, bArr, map, arrayList, z, j);
    }

    public atz(int i, byte[] bArr, boolean z, long j, List<aqd> list) {
        Map treeMap;
        if (list == null) {
            treeMap = null;
        } else if (list.isEmpty()) {
            treeMap = Collections.emptyMap();
        } else {
            treeMap = new TreeMap(String.CASE_INSENSITIVE_ORDER);
            for (aqd aqd : list) {
                treeMap.put(aqd.a(), aqd.b());
            }
        }
        this(i, bArr, treeMap, list, z, j);
    }

    @Deprecated
    public atz(byte[] bArr, Map<String, String> map) {
        this(200, bArr, map, false, 0);
    }
}
