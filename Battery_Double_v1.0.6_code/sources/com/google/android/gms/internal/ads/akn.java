package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.gmsg.ae;
import java.util.Map;

final class akn implements ae<bbe> {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ akh f2671a;

    akn(akh akh) {
        this.f2671a = akh;
    }

    public final /* synthetic */ void zza(Object obj, Map map) {
        if (this.f2671a.f2664a.a(map)) {
            this.f2671a.f2664a.a((akq) this.f2671a, map);
        }
    }
}
