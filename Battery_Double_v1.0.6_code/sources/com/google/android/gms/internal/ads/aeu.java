package com.google.android.gms.internal.ads;

final class aeu extends aer {
    aeu() {
    }

    private static int a(byte[] bArr, int i, long j, int i2) {
        switch (i2) {
            case 0:
                return aeq.b(i);
            case 1:
                return aeq.b(i, aeo.a(bArr, j));
            case 2:
                return aeq.b(i, (int) aeo.a(bArr, j), (int) aeo.a(bArr, 1 + j));
            default:
                throw new AssertionError();
        }
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Code restructure failed: missing block: B:87:?, code lost:
        return -1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:?, code lost:
        return -1;
     */
    public final int a(int i, byte[] bArr, int i2, int i3) {
        int i4;
        if ((i2 | i3 | (bArr.length - i3)) < 0) {
            throw new ArrayIndexOutOfBoundsException(String.format("Array length=%d, index=%d, limit=%d", new Object[]{Integer.valueOf(bArr.length), Integer.valueOf(i2), Integer.valueOf(i3)}));
        }
        long j = (long) i2;
        int i5 = (int) (((long) i3) - j);
        if (i5 >= 16) {
            i4 = 0;
            long j2 = j;
            while (true) {
                if (i4 >= i5) {
                    i4 = i5;
                    break;
                }
                long j3 = 1 + j2;
                if (aeo.a(bArr, j2) < 0) {
                    break;
                }
                i4++;
                j2 = j3;
            }
        } else {
            i4 = 0;
        }
        long j4 = ((long) i4) + j;
        int i6 = i5 - i4;
        while (true) {
            byte b2 = 0;
            long j5 = j4;
            while (true) {
                if (i6 <= 0) {
                    break;
                }
                long j6 = 1 + j5;
                b2 = aeo.a(bArr, j5);
                if (b2 < 0) {
                    j5 = j6;
                    break;
                }
                i6--;
                j5 = j6;
            }
            if (i6 != 0) {
                int i7 = i6 - 1;
                if (b2 >= -32) {
                    if (b2 >= -16) {
                        if (i7 >= 3) {
                            i6 = i7 - 3;
                            long j7 = 1 + j5;
                            byte a2 = aeo.a(bArr, j5);
                            if (a2 > -65 || (((b2 << 28) + (a2 + 112)) >> 30) != 0) {
                                break;
                            }
                            long j8 = 1 + j7;
                            if (aeo.a(bArr, j7) > -65) {
                                break;
                            }
                            j4 = 1 + j8;
                            if (aeo.a(bArr, j8) > -65) {
                                break;
                            }
                        } else {
                            return a(bArr, (int) b2, j5, i7);
                        }
                    } else if (i7 >= 2) {
                        i6 = i7 - 2;
                        long j9 = j5 + 1;
                        byte a3 = aeo.a(bArr, j5);
                        if (a3 > -65 || ((b2 == -32 && a3 < -96) || (b2 == -19 && a3 >= -96))) {
                            break;
                        }
                        j4 = 1 + j9;
                        if (aeo.a(bArr, j9) > -65) {
                            break;
                        }
                    } else {
                        return a(bArr, (int) b2, j5, i7);
                    }
                } else if (i7 != 0) {
                    i6 = i7 - 1;
                    if (b2 < -62) {
                        break;
                    }
                    j4 = 1 + j5;
                    if (aeo.a(bArr, j5) > -65) {
                        break;
                    }
                } else {
                    return b2;
                }
            } else {
                return 0;
            }
        }
        return -1;
    }

    /* access modifiers changed from: 0000 */
    public final int a(CharSequence charSequence, byte[] bArr, int i, int i2) {
        long j;
        long j2 = (long) i;
        long j3 = j2 + ((long) i2);
        int length = charSequence.length();
        if (length > i2 || bArr.length - i2 < i) {
            throw new ArrayIndexOutOfBoundsException("Failed writing " + charSequence.charAt(length - 1) + " at index " + (i + i2));
        }
        int i3 = 0;
        while (i3 < length) {
            char charAt = charSequence.charAt(i3);
            if (charAt >= 128) {
                break;
            }
            long j4 = 1 + j2;
            aeo.a(bArr, j2, (byte) charAt);
            i3++;
            j2 = j4;
        }
        if (i3 == length) {
            return (int) j2;
        }
        long j5 = j2;
        while (i3 < length) {
            char charAt2 = charSequence.charAt(i3);
            if (charAt2 < 128 && j5 < j3) {
                j = 1 + j5;
                aeo.a(bArr, j5, (byte) charAt2);
            } else if (charAt2 < 2048 && j5 <= j3 - 2) {
                long j6 = j5 + 1;
                aeo.a(bArr, j5, (byte) ((charAt2 >>> 6) | 960));
                j = 1 + j6;
                aeo.a(bArr, j6, (byte) ((charAt2 & '?') | 128));
            } else if ((charAt2 < 55296 || 57343 < charAt2) && j5 <= j3 - 3) {
                long j7 = 1 + j5;
                aeo.a(bArr, j5, (byte) ((charAt2 >>> 12) | 480));
                long j8 = 1 + j7;
                aeo.a(bArr, j7, (byte) (((charAt2 >>> 6) & 63) | 128));
                j = 1 + j8;
                aeo.a(bArr, j8, (byte) ((charAt2 & '?') | 128));
            } else if (j5 <= j3 - 4) {
                if (i3 + 1 != length) {
                    i3++;
                    char charAt3 = charSequence.charAt(i3);
                    if (Character.isSurrogatePair(charAt2, charAt3)) {
                        int codePoint = Character.toCodePoint(charAt2, charAt3);
                        long j9 = 1 + j5;
                        aeo.a(bArr, j5, (byte) ((codePoint >>> 18) | 240));
                        long j10 = 1 + j9;
                        aeo.a(bArr, j9, (byte) (((codePoint >>> 12) & 63) | 128));
                        long j11 = j10 + 1;
                        aeo.a(bArr, j10, (byte) (((codePoint >>> 6) & 63) | 128));
                        j = 1 + j11;
                        aeo.a(bArr, j11, (byte) ((codePoint & 63) | 128));
                    }
                }
                throw new aet(i3 - 1, length);
            } else if (55296 > charAt2 || charAt2 > 57343 || (i3 + 1 != length && Character.isSurrogatePair(charAt2, charSequence.charAt(i3 + 1)))) {
                throw new ArrayIndexOutOfBoundsException("Failed writing " + charAt2 + " at index " + j5);
            } else {
                throw new aet(i3, length);
            }
            i3++;
            j5 = j;
        }
        return (int) j5;
    }
}
