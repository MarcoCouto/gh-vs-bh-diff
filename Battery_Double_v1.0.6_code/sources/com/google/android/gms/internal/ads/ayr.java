package com.google.android.gms.internal.ads;

import android.os.RemoteException;

final class ayr implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ ays f3007a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ ayt f3008b;

    ayr(axs axs, ays ays, ayt ayt) {
        this.f3007a = ays;
        this.f3008b = ayt;
    }

    public final void run() {
        try {
            this.f3007a.a(this.f3008b);
        } catch (RemoteException e) {
            jm.c("Could not propagate interstitial ad event.", e);
        }
    }
}
