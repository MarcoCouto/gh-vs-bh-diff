package com.google.android.gms.internal.ads;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.app.KeyguardManager;
import android.content.Context;
import android.graphics.Rect;
import android.os.PowerManager;
import android.os.Process;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.TextView;
import com.google.android.gms.ads.internal.ax;
import com.google.android.gms.common.util.n;
import java.util.Iterator;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

@TargetApi(14)
@cm
public final class aln extends Thread {

    /* renamed from: a reason: collision with root package name */
    private boolean f2701a = false;

    /* renamed from: b reason: collision with root package name */
    private boolean f2702b = false;
    private boolean c = false;
    private final Object d;
    private final ali e;
    private final ck f;
    private final int g;
    private final int h;
    private final int i;
    private final int j;
    private final int k;
    private final int l;
    private final int m;
    private final int n;
    private final String o;
    private final boolean p;

    public aln(ali ali, ck ckVar) {
        this.e = ali;
        this.f = ckVar;
        this.d = new Object();
        this.h = ((Integer) ape.f().a(asi.R)).intValue();
        this.i = ((Integer) ape.f().a(asi.S)).intValue();
        this.j = ((Integer) ape.f().a(asi.T)).intValue();
        this.k = ((Integer) ape.f().a(asi.U)).intValue();
        this.l = ((Integer) ape.f().a(asi.X)).intValue();
        this.m = ((Integer) ape.f().a(asi.Z)).intValue();
        this.n = ((Integer) ape.f().a(asi.aa)).intValue();
        this.g = ((Integer) ape.f().a(asi.V)).intValue();
        this.o = (String) ape.f().a(asi.ac);
        this.p = ((Boolean) ape.f().a(asi.ae)).booleanValue();
        setName("ContentFetchTask");
    }

    private final alr a(View view, alh alh) {
        boolean z;
        int i2 = 0;
        if (view == null) {
            return new alr(this, 0, 0);
        }
        boolean globalVisibleRect = view.getGlobalVisibleRect(new Rect());
        if ((view instanceof TextView) && !(view instanceof EditText)) {
            CharSequence text = ((TextView) view).getText();
            if (TextUtils.isEmpty(text)) {
                return new alr(this, 0, 0);
            }
            alh.b(text.toString(), globalVisibleRect, view.getX(), view.getY(), (float) view.getWidth(), (float) view.getHeight());
            return new alr(this, 1, 0);
        } else if ((view instanceof WebView) && !(view instanceof qn)) {
            alh.g();
            WebView webView = (WebView) view;
            if (!n.g()) {
                z = false;
            } else {
                alh.g();
                webView.post(new alp(this, alh, webView, globalVisibleRect));
                z = true;
            }
            return z ? new alr(this, 0, 1) : new alr(this, 0, 0);
        } else if (!(view instanceof ViewGroup)) {
            return new alr(this, 0, 0);
        } else {
            ViewGroup viewGroup = (ViewGroup) view;
            int i3 = 0;
            for (int i4 = 0; i4 < viewGroup.getChildCount(); i4++) {
                alr a2 = a(viewGroup.getChildAt(i4), alh);
                i3 += a2.f2708a;
                i2 += a2.f2709b;
            }
            return new alr(this, i3, i2);
        }
    }

    private static boolean e() {
        try {
            Context b2 = ax.h().b();
            if (b2 == null) {
                return false;
            }
            ActivityManager activityManager = (ActivityManager) b2.getSystemService("activity");
            KeyguardManager keyguardManager = (KeyguardManager) b2.getSystemService("keyguard");
            if (activityManager == null || keyguardManager == null) {
                return false;
            }
            List runningAppProcesses = activityManager.getRunningAppProcesses();
            if (runningAppProcesses == null) {
                return false;
            }
            Iterator it = runningAppProcesses.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                RunningAppProcessInfo runningAppProcessInfo = (RunningAppProcessInfo) it.next();
                if (Process.myPid() == runningAppProcessInfo.pid) {
                    if (runningAppProcessInfo.importance == 100 && !keyguardManager.inKeyguardRestrictedInputMode()) {
                        PowerManager powerManager = (PowerManager) b2.getSystemService("power");
                        if (powerManager == null ? false : powerManager.isScreenOn()) {
                            return true;
                        }
                    }
                }
            }
            return false;
        } catch (Throwable th) {
            ax.i().a(th, "ContentFetchTask.isInForeground");
            return false;
        }
    }

    private final void f() {
        synchronized (this.d) {
            this.f2702b = true;
            jm.b("ContentFetchThread: paused, mPause = " + this.f2702b);
        }
    }

    public final void a() {
        synchronized (this.d) {
            if (this.f2701a) {
                jm.b("Content hash thread already started, quiting...");
                return;
            }
            this.f2701a = true;
            start();
        }
    }

    /* access modifiers changed from: 0000 */
    public final void a(View view) {
        try {
            alh alh = new alh(this.h, this.i, this.j, this.k, this.l, this.m, this.n);
            Context b2 = ax.h().b();
            if (b2 != null && !TextUtils.isEmpty(this.o)) {
                String str = (String) view.getTag(b2.getResources().getIdentifier((String) ape.f().a(asi.ab), "id", b2.getPackageName()));
                if (str != null && str.equals(this.o)) {
                    return;
                }
            }
            alr a2 = a(view, alh);
            alh.h();
            if (a2.f2708a != 0 || a2.f2709b != 0) {
                if (a2.f2709b != 0 || alh.j() != 0) {
                    if (a2.f2709b != 0 || !this.e.a(alh)) {
                        this.e.c(alh);
                    }
                }
            }
        } catch (Exception e2) {
            jm.b("Exception in fetchContentOnUIThread", e2);
            this.f.a(e2, "ContentFetchTask.fetchContent");
        }
    }

    /* access modifiers changed from: 0000 */
    public final void a(alh alh, WebView webView, String str, boolean z) {
        alh.f();
        try {
            if (!TextUtils.isEmpty(str)) {
                String optString = new JSONObject(str).optString("text");
                if (this.p || TextUtils.isEmpty(webView.getTitle())) {
                    alh.a(optString, z, webView.getX(), webView.getY(), (float) webView.getWidth(), (float) webView.getHeight());
                } else {
                    String title = webView.getTitle();
                    alh.a(new StringBuilder(String.valueOf(title).length() + 1 + String.valueOf(optString).length()).append(title).append("\n").append(optString).toString(), z, webView.getX(), webView.getY(), (float) webView.getWidth(), (float) webView.getHeight());
                }
            }
            if (alh.a()) {
                this.e.b(alh);
            }
        } catch (JSONException e2) {
            jm.b("Json string may be malformed.");
        } catch (Throwable th) {
            jm.a("Failed to get webview content.", th);
            this.f.a(th, "ContentFetchTask.processWebViewContent");
        }
    }

    public final alh b() {
        return this.e.a();
    }

    public final void c() {
        synchronized (this.d) {
            this.f2702b = false;
            this.d.notifyAll();
            jm.b("ContentFetchThread: wakeup");
        }
    }

    public final boolean d() {
        return this.f2702b;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0063, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0064, code lost:
        com.google.android.gms.internal.ads.jm.b("Error in ContentFetchTask", r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x006a, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:?, code lost:
        com.google.android.gms.ads.internal.ax.i().a((java.lang.Throwable) r1, "ContentFetchTask.extractContent");
        com.google.android.gms.internal.ads.jm.b("Failed getting root view of activity. Content not extracted.");
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0063 A[ExcHandler: InterruptedException (r0v1 'e' java.lang.InterruptedException A[CUSTOM_DECLARE]), Splitter:B:0:0x0000] */
    public final void run() {
        while (true) {
            try {
                if (e()) {
                    Activity a2 = ax.h().a();
                    if (a2 == null) {
                        jm.b("ContentFetchThread: no activity. Sleeping.");
                        f();
                    } else if (a2 != null) {
                        View view = null;
                        if (!(a2.getWindow() == null || a2.getWindow().getDecorView() == null)) {
                            view = a2.getWindow().getDecorView().findViewById(16908290);
                        }
                        if (!(view == null || view == null)) {
                            view.post(new alo(this, view));
                        }
                    }
                } else {
                    jm.b("ContentFetchTask: sleeping");
                    f();
                }
                Thread.sleep((long) (this.g * 1000));
            } catch (InterruptedException e2) {
            } catch (Exception e3) {
                jm.b("Error in ContentFetchTask", e3);
                this.f.a(e3, "ContentFetchTask.run");
            }
            synchronized (this.d) {
                while (this.f2702b) {
                    try {
                        jm.b("ContentFetchTask: waiting");
                        this.d.wait();
                    } catch (InterruptedException e4) {
                    }
                }
            }
        }
    }
}
