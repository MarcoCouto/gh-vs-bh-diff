package com.google.android.gms.internal.ads;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map.Entry;

final class add<T> implements adp<T> {

    /* renamed from: a reason: collision with root package name */
    private final acw f2472a;

    /* renamed from: b reason: collision with root package name */
    private final aei<?, ?> f2473b;
    private final boolean c;
    private final abe<?> d;

    private add(aei<?, ?> aei, abe<?> abe, acw acw) {
        this.f2473b = aei;
        this.c = abe.a(acw);
        this.d = abe;
        this.f2472a = acw;
    }

    static <T> add<T> a(aei<?, ?> aei, abe<?> abe, acw acw) {
        return new add<>(aei, abe, acw);
    }

    public final int a(T t) {
        int hashCode = this.f2473b.b(t).hashCode();
        return this.c ? (hashCode * 53) + this.d.a((Object) t).hashCode() : hashCode;
    }

    public final T a() {
        return this.f2472a.o().d();
    }

    public final void a(T t, ado ado, abc abc) throws IOException {
        boolean z;
        aei<?, ?> aei = this.f2473b;
        abe<?> abe = this.d;
        Object c2 = aei.c(t);
        abh b2 = abe.b(t);
        do {
            try {
                if (ado.a() == Integer.MAX_VALUE) {
                    aei.b((Object) t, c2);
                    return;
                }
                int b3 = ado.b();
                if (b3 == 11) {
                    aah aah = null;
                    int i = 0;
                    Object obj = null;
                    while (ado.a() != Integer.MAX_VALUE) {
                        int b4 = ado.b();
                        if (b4 == 16) {
                            i = ado.o();
                            obj = abe.a(abc, this.f2472a, i);
                        } else if (b4 == 26) {
                            if (obj != null) {
                                abe.a(ado, obj, abc, b2);
                            } else {
                                aah = ado.n();
                            }
                        } else if (!ado.c()) {
                            break;
                        }
                    }
                    if (ado.b() != 12) {
                        throw abv.e();
                    } else if (aah != null) {
                        if (obj != null) {
                            abe.a(aah, obj, abc, b2);
                        } else {
                            aei.a(c2, i, aah);
                        }
                    }
                } else if ((b3 & 7) == 2) {
                    Object a2 = abe.a(abc, this.f2472a, b3 >>> 3);
                    if (a2 != null) {
                        abe.a(ado, a2, abc, b2);
                    } else {
                        z = aei.a(c2, ado);
                        continue;
                    }
                } else {
                    z = ado.c();
                    continue;
                }
                z = true;
                continue;
            } finally {
                aei.b((Object) t, c2);
            }
        } while (z);
    }

    public final void a(T t, afc afc) throws IOException {
        Iterator e = this.d.a((Object) t).e();
        while (e.hasNext()) {
            Entry entry = (Entry) e.next();
            abj abj = (abj) entry.getKey();
            if (abj.c() != afb.MESSAGE || abj.d() || abj.e()) {
                throw new IllegalStateException("Found invalid MessageSet item.");
            } else if (entry instanceof aca) {
                afc.a(abj.a(), (Object) ((aca) entry).a().c());
            } else {
                afc.a(abj.a(), entry.getValue());
            }
        }
        aei<?, ?> aei = this.f2473b;
        aei.b(aei.b(t), afc);
    }

    public final void a(T t, byte[] bArr, int i, int i2, aae aae) throws IOException {
        aej aej = ((abp) t).zzdtt;
        if (aej == aej.a()) {
            aej = aej.b();
            ((abp) t).zzdtt = aej;
        }
        while (i < i2) {
            int a2 = aad.a(bArr, i, aae);
            int i3 = aae.f2389a;
            if (i3 != 11) {
                i = (i3 & 7) == 2 ? aad.a(i3, bArr, a2, i2, aej, aae) : aad.a(i3, bArr, a2, i2, aae);
            } else {
                int i4 = a2;
                int i5 = 0;
                Object obj = null;
                while (i4 < i2) {
                    i4 = aad.a(bArr, i4, aae);
                    int i6 = aae.f2389a;
                    int i7 = i6 & 7;
                    switch (i6 >>> 3) {
                        case 2:
                            if (i7 == 0) {
                                i4 = aad.a(bArr, i4, aae);
                                i5 = aae.f2389a;
                                break;
                            }
                        case 3:
                            if (i7 == 2) {
                                i4 = aad.e(bArr, i4, aae);
                                obj = (aah) aae.c;
                                break;
                            }
                        default:
                            if (i6 == 12) {
                                break;
                            } else {
                                i4 = aad.a(i6, bArr, i4, i2, aae);
                                break;
                            }
                    }
                }
                if (obj != null) {
                    aej.a((i5 << 3) | 2, obj);
                }
                i = i4;
            }
        }
        if (i != i2) {
            throw abv.g();
        }
    }

    public final boolean a(T t, T t2) {
        if (!this.f2473b.b(t).equals(this.f2473b.b(t2))) {
            return false;
        }
        if (this.c) {
            return this.d.a((Object) t).equals(this.d.a((Object) t2));
        }
        return true;
    }

    public final int b(T t) {
        aei<?, ?> aei = this.f2473b;
        int e = aei.e(aei.b(t)) + 0;
        return this.c ? e + this.d.a((Object) t).i() : e;
    }

    public final void b(T t, T t2) {
        adr.a(this.f2473b, t, t2);
        if (this.c) {
            adr.a(this.d, t, t2);
        }
    }

    public final void c(T t) {
        this.f2473b.d(t);
        this.d.c(t);
    }

    public final boolean d(T t) {
        return this.d.a((Object) t).g();
    }
}
