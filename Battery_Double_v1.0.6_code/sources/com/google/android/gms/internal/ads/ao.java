package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.ads.internal.a;
import com.google.android.gms.ads.internal.ae;
import com.google.android.gms.common.util.n;

@cm
public final class ao {
    public static ko a(Context context, a aVar, is isVar, ahh ahh, qn qnVar, bcr bcr, ap apVar, asv asv) {
        ko arVar;
        dp dpVar = isVar.f3398b;
        if (dpVar.g) {
            arVar = new au(context, isVar, bcr, apVar, asv, qnVar);
        } else if (dpVar.s || (aVar instanceof ae)) {
            arVar = (!dpVar.s || !(aVar instanceof ae)) ? new ar(isVar, apVar) : new aw(context, (ae) aVar, isVar, ahh, apVar, asv);
        } else {
            arVar = (!((Boolean) ape.f().a(asi.ah)).booleanValue() || !n.g() || n.i() || qnVar == null || !qnVar.t().d()) ? new aq(context, isVar, qnVar, apVar) : new at(context, isVar, qnVar, apVar);
        }
        String str = "AdRenderer: ";
        String valueOf = String.valueOf(arVar.getClass().getName());
        jm.b(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
        arVar.c();
        return arVar;
    }
}
