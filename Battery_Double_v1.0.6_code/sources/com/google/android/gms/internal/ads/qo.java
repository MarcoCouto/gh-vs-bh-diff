package com.google.android.gms.internal.ads;

import android.annotation.TargetApi;
import android.content.Context;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnAttachStateChangeListener;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.ViewTreeObserver.OnScrollChangedListener;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.google.android.gms.ads.internal.ax;
import com.google.android.gms.ads.internal.bv;
import com.google.android.gms.ads.internal.gmsg.a;
import com.google.android.gms.ads.internal.gmsg.ae;
import com.google.android.gms.ads.internal.gmsg.ah;
import com.google.android.gms.ads.internal.gmsg.ai;
import com.google.android.gms.ads.internal.gmsg.d;
import com.google.android.gms.ads.internal.gmsg.e;
import com.google.android.gms.ads.internal.gmsg.k;
import com.google.android.gms.ads.internal.gmsg.m;
import com.google.android.gms.ads.internal.gmsg.o;
import com.google.android.gms.ads.internal.overlay.AdOverlayInfoParcel;
import com.google.android.gms.ads.internal.overlay.c;
import com.google.android.gms.ads.internal.overlay.l;
import com.google.android.gms.ads.internal.overlay.n;
import com.google.android.gms.ads.internal.overlay.t;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.CopyOnWriteArrayList;

@cm
public class qo extends WebViewClient implements rv {

    /* renamed from: b reason: collision with root package name */
    private static final String[] f3637b = {"UNKNOWN", "HOST_LOOKUP", "UNSUPPORTED_AUTH_SCHEME", "AUTHENTICATION", "PROXY_AUTHENTICATION", "CONNECT", "IO", "TIMEOUT", "REDIRECT_LOOP", "UNSUPPORTED_SCHEME", "FAILED_SSL_HANDSHAKE", "BAD_URL", "FILE", "FILE_NOT_FOUND", "TOO_MANY_REQUESTS"};
    private static final String[] c = {"NOT_YET_VALID", "EXPIRED", "ID_MISMATCH", "UNTRUSTED", "DATE_INVALID", "INVALID"};
    private boolean A;
    private boolean B;
    private int C;
    private OnAttachStateChangeListener D;

    /* renamed from: a reason: collision with root package name */
    protected ic f3638a;
    private qn d;
    private final HashMap<String, List<ae<? super qn>>> e;
    private final Object f;
    private aoj g;
    private n h;
    private rw i;
    private rx j;
    private k k;
    private m l;
    private ry m;
    private boolean n;
    private ai o;
    private boolean p;
    private boolean q;
    private OnGlobalLayoutListener r;
    private OnScrollChangedListener s;
    private boolean t;
    private t u;
    private final m v;
    private bv w;
    private d x;
    private o y;
    private rz z;

    public qo(qn qnVar, boolean z2) {
        this(qnVar, z2, new m(qnVar, qnVar.q(), new art(qnVar.getContext())), null);
    }

    private qo(qn qnVar, boolean z2, m mVar, d dVar) {
        this.e = new HashMap<>();
        this.f = new Object();
        this.n = false;
        this.d = qnVar;
        this.p = z2;
        this.v = mVar;
        this.x = null;
    }

    private final void a(Context context, String str, String str2, String str3) {
        String str4;
        if (((Boolean) ape.f().a(asi.bs)).booleanValue()) {
            Bundle bundle = new Bundle();
            bundle.putString("err", str);
            bundle.putString("code", str2);
            String str5 = "host";
            if (!TextUtils.isEmpty(str3)) {
                Uri parse = Uri.parse(str3);
                if (parse.getHost() != null) {
                    str4 = parse.getHost();
                    bundle.putString(str5, str4);
                    ax.e().a(context, this.d.k().f3528a, "gmob-apps", bundle, true);
                }
            }
            str4 = "";
            bundle.putString(str5, str4);
            ax.e().a(context, this.d.k().f3528a, "gmob-apps", bundle, true);
        }
    }

    private final void a(Uri uri) {
        String path = uri.getPath();
        List<ae> list = (List) this.e.get(path);
        if (list != null) {
            ax.e();
            Map a2 = jv.a(uri);
            if (jm.a(2)) {
                String str = "Received GMSG: ";
                String valueOf = String.valueOf(path);
                jm.a(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
                for (String str2 : a2.keySet()) {
                    String str3 = (String) a2.get(str2);
                    jm.a(new StringBuilder(String.valueOf(str2).length() + 4 + String.valueOf(str3).length()).append("  ").append(str2).append(": ").append(str3).toString());
                }
            }
            for (ae zza : list) {
                zza.zza(this.d, a2);
            }
            return;
        }
        String valueOf2 = String.valueOf(uri);
        jm.a(new StringBuilder(String.valueOf(valueOf2).length() + 32).append("No GMSG handler found for GMSG: ").append(valueOf2).toString());
    }

    /* access modifiers changed from: private */
    public final void a(View view, ic icVar, int i2) {
        if (icVar.b() && i2 > 0) {
            icVar.a(view);
            if (icVar.b()) {
                jv.f3440a.postDelayed(new qq(this, view, icVar, i2), 100);
            }
        }
    }

    private final void a(AdOverlayInfoParcel adOverlayInfoParcel) {
        boolean z2 = false;
        boolean z3 = this.x != null ? this.x.a() : false;
        ax.c();
        Context context = this.d.getContext();
        if (!z3) {
            z2 = true;
        }
        l.a(context, adOverlayInfoParcel, z2);
        if (this.f3638a != null) {
            String str = adOverlayInfoParcel.l;
            if (str == null && adOverlayInfoParcel.f2076a != null) {
                str = adOverlayInfoParcel.f2076a.f2078a;
            }
            this.f3638a.a(str);
        }
    }

    private final WebResourceResponse b(String str, Map<String, String> map) throws IOException {
        HttpURLConnection httpURLConnection;
        URL url = new URL(str);
        int i2 = 0;
        while (true) {
            int i3 = i2 + 1;
            if (i3 <= 20) {
                URLConnection openConnection = url.openConnection();
                openConnection.setConnectTimeout(10000);
                openConnection.setReadTimeout(10000);
                for (Entry entry : map.entrySet()) {
                    openConnection.addRequestProperty((String) entry.getKey(), (String) entry.getValue());
                }
                if (!(openConnection instanceof HttpURLConnection)) {
                    throw new IOException("Invalid protocol.");
                }
                httpURLConnection = (HttpURLConnection) openConnection;
                ax.e().a(this.d.getContext(), this.d.k().f3528a, false, httpURLConnection);
                ml mlVar = new ml();
                mlVar.a(httpURLConnection, (byte[]) null);
                int responseCode = httpURLConnection.getResponseCode();
                mlVar.a(httpURLConnection, responseCode);
                if (responseCode < 300 || responseCode >= 400) {
                    ax.e();
                } else {
                    String headerField = httpURLConnection.getHeaderField("Location");
                    if (headerField == null) {
                        throw new IOException("Missing Location header in redirect");
                    }
                    URL url2 = new URL(url, headerField);
                    String protocol = url2.getProtocol();
                    if (protocol == null) {
                        jm.e("Protocol is null");
                        return null;
                    } else if (protocol.equals("http") || protocol.equals("https")) {
                        String str2 = "Redirecting to ";
                        String valueOf = String.valueOf(headerField);
                        jm.b(valueOf.length() != 0 ? str2.concat(valueOf) : new String(str2));
                        httpURLConnection.disconnect();
                        i2 = i3;
                        url = url2;
                    } else {
                        String str3 = "Unsupported scheme: ";
                        String valueOf2 = String.valueOf(protocol);
                        jm.e(valueOf2.length() != 0 ? str3.concat(valueOf2) : new String(str3));
                        return null;
                    }
                }
            } else {
                throw new IOException("Too many redirects (20)");
            }
        }
        ax.e();
        return jv.a(httpURLConnection);
    }

    private final void p() {
        if (this.D != null) {
            this.d.getView().removeOnAttachStateChangeListener(this.D);
        }
    }

    private final void q() {
        if (this.i != null && ((this.A && this.C <= 0) || this.B)) {
            this.i.a(!this.B);
            this.i = null;
        }
        this.d.G();
    }

    /* access modifiers changed from: protected */
    public final WebResourceResponse a(String str, Map<String, String> map) {
        try {
            String a2 = il.a(str, this.d.getContext());
            if (!a2.equals(str)) {
                return b(a2, map);
            }
            amp a3 = amp.a(str);
            if (a3 != null) {
                amm a4 = ax.k().a(a3);
                if (a4 != null && a4.a()) {
                    return new WebResourceResponse("", "", a4.b());
                }
            }
            if (ml.c()) {
                if (((Boolean) ape.f().a(asi.bi)).booleanValue()) {
                    return b(str, map);
                }
            }
            return null;
        } catch (Exception | NoClassDefFoundError e2) {
            ax.i().a(e2, "AdWebViewClient.interceptRequest");
            return null;
        }
    }

    public final bv a() {
        return this.w;
    }

    public final void a(int i2, int i3) {
        if (this.x != null) {
            this.x.a(i2, i3);
        }
    }

    public final void a(int i2, int i3, boolean z2) {
        this.v.a(i2, i3);
        if (this.x != null) {
            this.x.a(i2, i3, z2);
        }
    }

    public final void a(OnGlobalLayoutListener onGlobalLayoutListener, OnScrollChangedListener onScrollChangedListener) {
        synchronized (this.f) {
            this.q = true;
            this.d.F();
            this.r = onGlobalLayoutListener;
            this.s = onScrollChangedListener;
        }
    }

    public final void a(c cVar) {
        n nVar = null;
        boolean z2 = this.d.z();
        aoj aoj = (!z2 || this.d.t().d()) ? this.g : null;
        if (!z2) {
            nVar = this.h;
        }
        a(new AdOverlayInfoParcel(cVar, aoj, nVar, this.u, this.d.k()));
    }

    public final void a(aoj aoj, k kVar, n nVar, m mVar, t tVar, boolean z2, ai aiVar, bv bvVar, o oVar, ic icVar) {
        bv bvVar2 = bvVar == null ? new bv(this.d.getContext(), icVar, null) : bvVar;
        this.x = new d(this.d, oVar);
        this.f3638a = icVar;
        if (((Boolean) ape.f().a(asi.aF)).booleanValue()) {
            a("/adMetadata", (ae<? super qn>) new a<Object>(kVar));
        }
        a("/appEvent", (ae<? super qn>) new com.google.android.gms.ads.internal.gmsg.l<Object>(mVar));
        a("/backButton", o.j);
        a("/refresh", o.k);
        a("/canOpenURLs", o.f2056a);
        a("/canOpenIntents", o.f2057b);
        a("/click", o.c);
        a("/close", o.d);
        a("/customClose", o.e);
        a("/instrument", o.n);
        a("/delayPageLoaded", o.p);
        a("/delayPageClosed", o.q);
        a("/getLocationInfo", o.r);
        a("/httpTrack", o.f);
        a("/log", o.g);
        a("/mraid", (ae<? super qn>) new d<Object>(bvVar2, this.x, oVar));
        a("/mraidLoaded", (ae<? super qn>) this.v);
        a("/open", (ae<? super qn>) new e<Object>(this.d.getContext(), this.d.k(), this.d.y(), tVar, aoj, kVar, mVar, nVar, bvVar2, this.x));
        a("/precache", (ae<? super qn>) new qc<Object>());
        a("/touch", o.i);
        a("/video", o.l);
        a("/videoMeta", o.m);
        if (ax.B().a(this.d.getContext())) {
            a("/logScionEvent", (ae<? super qn>) new com.google.android.gms.ads.internal.gmsg.c<Object>(this.d.getContext()));
        }
        if (aiVar != null) {
            a("/setInterstitialProperties", (ae<? super qn>) new ah<Object>(aiVar));
        }
        this.g = aoj;
        this.h = nVar;
        this.k = kVar;
        this.l = mVar;
        this.u = tVar;
        this.w = bvVar2;
        this.y = oVar;
        this.o = aiVar;
        this.n = z2;
    }

    public final void a(rw rwVar) {
        this.i = rwVar;
    }

    public final void a(rx rxVar) {
        this.j = rxVar;
    }

    public final void a(ry ryVar) {
        this.m = ryVar;
    }

    public final void a(rz rzVar) {
        this.z = rzVar;
    }

    public final void a(String str, ae<? super qn> aeVar) {
        synchronized (this.f) {
            List list = (List) this.e.get(str);
            if (list == null) {
                list = new CopyOnWriteArrayList();
                this.e.put(str, list);
            }
            list.add(aeVar);
        }
    }

    public final void a(String str, com.google.android.gms.common.util.o<ae<? super qn>> oVar) {
        synchronized (this.f) {
            List<ae> list = (List) this.e.get(str);
            if (list != null) {
                ArrayList arrayList = new ArrayList();
                for (ae aeVar : list) {
                    if (oVar.a(aeVar)) {
                        arrayList.add(aeVar);
                    }
                }
                list.removeAll(arrayList);
            }
        }
    }

    public final void a(boolean z2) {
        this.n = z2;
    }

    public final void a(boolean z2, int i2) {
        a(new AdOverlayInfoParcel((!this.d.z() || this.d.t().d()) ? this.g : null, this.h, this.u, this.d, z2, i2, this.d.k()));
    }

    public final void a(boolean z2, int i2, String str) {
        qt qtVar = null;
        boolean z3 = this.d.z();
        aoj aoj = (!z3 || this.d.t().d()) ? this.g : null;
        if (!z3) {
            qtVar = new qt(this.d, this.h);
        }
        a(new AdOverlayInfoParcel(aoj, qtVar, this.k, this.l, this.u, this.d, z2, i2, str, this.d.k()));
    }

    public final void a(boolean z2, int i2, String str, String str2) {
        boolean z3 = this.d.z();
        a(new AdOverlayInfoParcel((!z3 || this.d.t().d()) ? this.g : null, z3 ? null : new qt(this.d, this.h), this.k, this.l, this.u, this.d, z2, i2, str, str2, this.d.k()));
    }

    public final void b(String str, ae<? super qn> aeVar) {
        synchronized (this.f) {
            List list = (List) this.e.get(str);
            if (list != null) {
                list.remove(aeVar);
            }
        }
    }

    public final boolean b() {
        boolean z2;
        synchronized (this.f) {
            z2 = this.p;
        }
        return z2;
    }

    public final boolean c() {
        boolean z2;
        synchronized (this.f) {
            z2 = this.q;
        }
        return z2;
    }

    public final OnGlobalLayoutListener d() {
        OnGlobalLayoutListener onGlobalLayoutListener;
        synchronized (this.f) {
            onGlobalLayoutListener = this.r;
        }
        return onGlobalLayoutListener;
    }

    public final OnScrollChangedListener e() {
        OnScrollChangedListener onScrollChangedListener;
        synchronized (this.f) {
            onScrollChangedListener = this.s;
        }
        return onScrollChangedListener;
    }

    public final boolean f() {
        boolean z2;
        synchronized (this.f) {
            z2 = this.t;
        }
        return z2;
    }

    public final void g() {
        ic icVar = this.f3638a;
        if (icVar != null) {
            WebView webView = this.d.getWebView();
            if (android.support.v4.i.t.y(webView)) {
                a((View) webView, icVar, 10);
                return;
            }
            p();
            this.D = new qs(this, icVar);
            this.d.getView().addOnAttachStateChangeListener(this.D);
        }
    }

    public final void h() {
        synchronized (this.f) {
            this.t = true;
        }
        this.C++;
        q();
    }

    public final void i() {
        this.C--;
        q();
    }

    public final void j() {
        this.B = true;
        q();
    }

    public final void k() {
        if (this.f3638a != null) {
            this.f3638a.d();
            this.f3638a = null;
        }
        p();
        synchronized (this.f) {
            this.e.clear();
            this.g = null;
            this.h = null;
            this.i = null;
            this.j = null;
            this.k = null;
            this.l = null;
            this.n = false;
            this.p = false;
            this.q = false;
            this.t = false;
            this.u = null;
            this.m = null;
            if (this.x != null) {
                this.x.a(true);
                this.x = null;
            }
        }
    }

    public final rz l() {
        return this.z;
    }

    public final ic m() {
        return this.f3638a;
    }

    public final void n() {
        synchronized (this.f) {
            this.n = false;
            this.p = true;
            nt.f3558a.execute(new qp(this));
        }
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ void o() {
        this.d.F();
        com.google.android.gms.ads.internal.overlay.d r2 = this.d.r();
        if (r2 != null) {
            r2.m();
        }
        if (this.m != null) {
            this.m.a();
            this.m = null;
        }
    }

    public final void onLoadResource(WebView webView, String str) {
        String str2 = "Loading resource: ";
        String valueOf = String.valueOf(str);
        jm.a(valueOf.length() != 0 ? str2.concat(valueOf) : new String(str2));
        Uri parse = Uri.parse(str);
        if ("gmsg".equalsIgnoreCase(parse.getScheme()) && "mobileads.google.com".equalsIgnoreCase(parse.getHost())) {
            a(parse);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x001f, code lost:
        r2.j.a();
        r2.j = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0027, code lost:
        q();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0018, code lost:
        r2.A = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001d, code lost:
        if (r2.j == null) goto L_0x0027;
     */
    public final void onPageFinished(WebView webView, String str) {
        synchronized (this.f) {
            if (this.d.A()) {
                jm.a("Blank page loaded, 1...");
                this.d.B();
            }
        }
    }

    public final void onReceivedError(WebView webView, int i2, String str, String str2) {
        a(this.d.getContext(), "http_err", (i2 >= 0 || (-i2) + -1 >= f3637b.length) ? String.valueOf(i2) : f3637b[(-i2) - 1], str2);
        super.onReceivedError(webView, i2, str, str2);
    }

    public final void onReceivedSslError(WebView webView, SslErrorHandler sslErrorHandler, SslError sslError) {
        if (sslError != null) {
            int primaryError = sslError.getPrimaryError();
            a(this.d.getContext(), "ssl_err", (primaryError < 0 || primaryError >= c.length) ? String.valueOf(primaryError) : c[primaryError], ax.g().a(sslError));
        }
        super.onReceivedSslError(webView, sslErrorHandler, sslError);
    }

    @TargetApi(11)
    public WebResourceResponse shouldInterceptRequest(WebView webView, String str) {
        return a(str, Collections.emptyMap());
    }

    public boolean shouldOverrideKeyEvent(WebView webView, KeyEvent keyEvent) {
        switch (keyEvent.getKeyCode()) {
            case 79:
            case 85:
            case 86:
            case 87:
            case 88:
            case 89:
            case 90:
            case 91:
            case 126:
            case 127:
            case 128:
            case 129:
            case 130:
            case 222:
                return true;
            default:
                return false;
        }
    }

    public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
        Uri uri;
        String str2 = "AdWebView shouldOverrideUrlLoading: ";
        String valueOf = String.valueOf(str);
        jm.a(valueOf.length() != 0 ? str2.concat(valueOf) : new String(str2));
        Uri parse = Uri.parse(str);
        if (!"gmsg".equalsIgnoreCase(parse.getScheme()) || !"mobileads.google.com".equalsIgnoreCase(parse.getHost())) {
            if (this.n && webView == this.d.getWebView()) {
                String scheme = parse.getScheme();
                if ("http".equalsIgnoreCase(scheme) || "https".equalsIgnoreCase(scheme)) {
                    if (this.g != null) {
                        if (((Boolean) ape.f().a(asi.aj)).booleanValue()) {
                            this.g.e();
                            if (this.f3638a != null) {
                                this.f3638a.a(str);
                            }
                            this.g = null;
                        }
                    }
                    return super.shouldOverrideUrlLoading(webView, str);
                }
            }
            if (!this.d.getWebView().willNotDraw()) {
                try {
                    ahh y2 = this.d.y();
                    if (y2 != null && y2.a(parse)) {
                        parse = y2.a(parse, this.d.getContext(), this.d.getView(), this.d.d());
                    }
                    uri = parse;
                } catch (ahi e2) {
                    String str3 = "Unable to append parameter to URL: ";
                    String valueOf2 = String.valueOf(str);
                    jm.e(valueOf2.length() != 0 ? str3.concat(valueOf2) : new String(str3));
                    uri = parse;
                }
                if (this.w == null || this.w.b()) {
                    a(new c("android.intent.action.VIEW", uri.toString(), null, null, null, null, null));
                } else {
                    this.w.a(str);
                }
            } else {
                String str4 = "AdWebView unable to handle URL: ";
                String valueOf3 = String.valueOf(str);
                jm.e(valueOf3.length() != 0 ? str4.concat(valueOf3) : new String(str4));
            }
        } else {
            a(parse);
        }
        return true;
    }
}
