package com.google.android.gms.internal.ads;

@cm
public final class ape {

    /* renamed from: a reason: collision with root package name */
    private static final Object f2834a = new Object();

    /* renamed from: b reason: collision with root package name */
    private static ape f2835b;
    private final mh c = new mh();
    private final aox d = new aox(new aon(), new aom(), new arg(), new aww(), new gr(), new q(), new awx());
    private final String e = mh.c();
    private final ase f = new ase();
    private final asf g = new asf();
    private final asg h = new asg();

    static {
        ape ape = new ape();
        synchronized (f2834a) {
            f2835b = ape;
        }
    }

    protected ape() {
    }

    public static mh a() {
        return g().c;
    }

    public static aox b() {
        return g().d;
    }

    public static String c() {
        return g().e;
    }

    public static asf d() {
        return g().g;
    }

    public static ase e() {
        return g().f;
    }

    public static asg f() {
        return g().h;
    }

    private static ape g() {
        ape ape;
        synchronized (f2834a) {
            ape = f2835b;
        }
        return ape;
    }
}
