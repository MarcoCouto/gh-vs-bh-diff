package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.os.RemoteException;
import com.google.android.gms.b.b;

final class apd extends a<r> {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ Activity f2832a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ aox f2833b;

    apd(aox aox, Activity activity) {
        this.f2833b = aox;
        this.f2832a = activity;
        super();
    }

    public final /* synthetic */ Object a() throws RemoteException {
        r a2 = this.f2833b.h.a(this.f2832a);
        if (a2 != null) {
            return a2;
        }
        aox.a(this.f2832a, "ad_overlay");
        return null;
    }

    public final /* synthetic */ Object a(aqh aqh) throws RemoteException {
        return aqh.createAdOverlay(b.a(this.f2832a));
    }
}
