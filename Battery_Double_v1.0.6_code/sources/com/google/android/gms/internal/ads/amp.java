package com.google.android.gms.internal.ads;

import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.ads.internal.ax;
import com.google.android.gms.common.internal.a.a;
import com.google.android.gms.common.internal.a.c;
import java.util.List;

@cm
public final class amp extends a {
    public static final Creator<amp> CREATOR = new amq();

    /* renamed from: a reason: collision with root package name */
    public final String f2737a;

    /* renamed from: b reason: collision with root package name */
    private final long f2738b;
    private final String c;
    private final String d;
    private final String e;
    private final Bundle f;
    private final boolean g;
    private long h;

    amp(String str, long j, String str2, String str3, String str4, Bundle bundle, boolean z, long j2) {
        this.f2737a = str;
        this.f2738b = j;
        if (str2 == null) {
            str2 = "";
        }
        this.c = str2;
        if (str3 == null) {
            str3 = "";
        }
        this.d = str3;
        if (str4 == null) {
            str4 = "";
        }
        this.e = str4;
        if (bundle == null) {
            bundle = new Bundle();
        }
        this.f = bundle;
        this.g = z;
        this.h = j2;
    }

    public static amp a(Uri uri) {
        long j = 0;
        try {
            if (!"gcache".equals(uri.getScheme())) {
                return null;
            }
            List pathSegments = uri.getPathSegments();
            if (pathSegments.size() != 2) {
                jm.e("Expected 2 path parts for namespace and id, found :" + pathSegments.size());
                return null;
            }
            String str = (String) pathSegments.get(0);
            String str2 = (String) pathSegments.get(1);
            String host = uri.getHost();
            String queryParameter = uri.getQueryParameter("url");
            boolean equals = "1".equals(uri.getQueryParameter("read_only"));
            String queryParameter2 = uri.getQueryParameter("expiration");
            if (queryParameter2 != null) {
                j = Long.parseLong(queryParameter2);
            }
            Bundle bundle = new Bundle();
            for (String str3 : ax.g().a(uri)) {
                if (str3.startsWith("tag.")) {
                    bundle.putString(str3.substring(4), uri.getQueryParameter(str3));
                }
            }
            return new amp(queryParameter, j, host, str, str2, bundle, equals, 0);
        } catch (NullPointerException | NumberFormatException e2) {
            jm.c("Unable to parse Uri into cache offering.", e2);
            return null;
        }
    }

    public static amp a(String str) {
        return a(Uri.parse(str));
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = c.a(parcel);
        c.a(parcel, 2, this.f2737a, false);
        c.a(parcel, 3, this.f2738b);
        c.a(parcel, 4, this.c, false);
        c.a(parcel, 5, this.d, false);
        c.a(parcel, 6, this.e, false);
        c.a(parcel, 7, this.f, false);
        c.a(parcel, 8, this.g);
        c.a(parcel, 9, this.h);
        c.a(parcel, a2);
    }
}
