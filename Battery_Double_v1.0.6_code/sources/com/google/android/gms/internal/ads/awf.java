package com.google.android.gms.internal.ads;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public abstract class awf extends ajl implements awe {
    public awf() {
        super("com.google.android.gms.ads.internal.formats.client.IOnCustomClickListener");
    }

    public static awe a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.ads.internal.formats.client.IOnCustomClickListener");
        return queryLocalInterface instanceof awe ? (awe) queryLocalInterface : new awg(iBinder);
    }

    /* access modifiers changed from: protected */
    public final boolean dispatchTransaction(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        avt avv;
        if (i != 1) {
            return false;
        }
        IBinder readStrongBinder = parcel.readStrongBinder();
        if (readStrongBinder == null) {
            avv = null;
        } else {
            IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.ads.internal.formats.client.INativeCustomTemplateAd");
            avv = queryLocalInterface instanceof avt ? (avt) queryLocalInterface : new avv(readStrongBinder);
        }
        a(avv, parcel.readString());
        parcel2.writeNoException();
        return true;
    }
}
