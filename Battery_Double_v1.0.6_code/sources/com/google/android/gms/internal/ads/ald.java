package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.app.Application.ActivityLifecycleCallbacks;
import android.os.Bundle;

final class ald implements alf {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ Activity f2689a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ Bundle f2690b;

    ald(akw akw, Activity activity, Bundle bundle) {
        this.f2689a = activity;
        this.f2690b = bundle;
    }

    public final void a(ActivityLifecycleCallbacks activityLifecycleCallbacks) {
        activityLifecycleCallbacks.onActivitySaveInstanceState(this.f2689a, this.f2690b);
    }
}
