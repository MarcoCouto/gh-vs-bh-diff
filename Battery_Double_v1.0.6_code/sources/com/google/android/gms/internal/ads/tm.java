package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.a.b;

public final class tm implements Creator<tl> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        String str = null;
        int b2 = b.b(parcel);
        int i = 0;
        String str2 = null;
        while (parcel.dataPosition() < b2) {
            int a2 = b.a(parcel);
            switch (b.a(a2)) {
                case 1:
                    i = b.d(parcel, a2);
                    break;
                case 2:
                    str2 = b.k(parcel, a2);
                    break;
                case 3:
                    str = b.k(parcel, a2);
                    break;
                default:
                    b.b(parcel, a2);
                    break;
            }
        }
        b.r(parcel, b2);
        return new tl(i, str2, str);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new tl[i];
    }
}
