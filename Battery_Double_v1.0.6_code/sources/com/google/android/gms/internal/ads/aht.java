package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.app.Application.ActivityLifecycleCallbacks;

final class aht implements ahu {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ Activity f2601a;

    aht(ahm ahm, Activity activity) {
        this.f2601a = activity;
    }

    public final void a(ActivityLifecycleCallbacks activityLifecycleCallbacks) {
        activityLifecycleCallbacks.onActivityDestroyed(this.f2601a);
    }
}
