package com.google.android.gms.internal.ads;

import android.os.RemoteException;

final class bcp implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ bci f3142a;

    bcp(bco bco, bci bci) {
        this.f3142a = bci;
    }

    public final void run() {
        try {
            this.f3142a.c.c();
        } catch (RemoteException e) {
            jm.c("Could not destroy mediation adapter.", e);
        }
    }
}
