package com.google.android.gms.internal.ads;

import android.os.Handler;
import android.os.Looper;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public final class bab {

    /* renamed from: a reason: collision with root package name */
    private final AtomicInteger f3055a;

    /* renamed from: b reason: collision with root package name */
    private final Set<awb<?>> f3056b;
    private final PriorityBlockingQueue<awb<?>> c;
    private final PriorityBlockingQueue<awb<?>> d;
    private final zy e;
    private final are f;
    private final b g;
    private final arx[] h;
    private ahz i;
    private final List<bbc> j;

    public bab(zy zyVar, are are) {
        this(zyVar, are, 4);
    }

    private bab(zy zyVar, are are, int i2) {
        this(zyVar, are, 4, new ane(new Handler(Looper.getMainLooper())));
    }

    private bab(zy zyVar, are are, int i2, b bVar) {
        this.f3055a = new AtomicInteger();
        this.f3056b = new HashSet();
        this.c = new PriorityBlockingQueue<>();
        this.d = new PriorityBlockingQueue<>();
        this.j = new ArrayList();
        this.e = zyVar;
        this.f = are;
        this.h = new arx[4];
        this.g = bVar;
    }

    public final <T> awb<T> a(awb<T> awb) {
        awb.a(this);
        synchronized (this.f3056b) {
            this.f3056b.add(awb);
        }
        awb.a(this.f3055a.incrementAndGet());
        awb.b("add-to-queue");
        if (!awb.h()) {
            this.d.add(awb);
        } else {
            this.c.add(awb);
        }
        return awb;
    }

    public final void a() {
        arx[] arxArr;
        if (this.i != null) {
            this.i.a();
        }
        for (arx arx : this.h) {
            if (arx != null) {
                arx.a();
            }
        }
        this.i = new ahz(this.c, this.d, this.e, this.g);
        this.i.start();
        for (int i2 = 0; i2 < this.h.length; i2++) {
            arx arx2 = new arx(this.d, this.f, this.e, this.g);
            this.h[i2] = arx2;
            arx2.start();
        }
    }

    /* access modifiers changed from: 0000 */
    public final <T> void b(awb<T> awb) {
        synchronized (this.f3056b) {
            this.f3056b.remove(awb);
        }
        synchronized (this.j) {
            for (bbc a2 : this.j) {
                a2.a(awb);
            }
        }
    }
}
