package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.ax;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@cm
public final class bcb {

    /* renamed from: a reason: collision with root package name */
    public final List<bca> f3121a;

    /* renamed from: b reason: collision with root package name */
    public final long f3122b;
    public final List<String> c;
    public final List<String> d;
    public final List<String> e;
    public final List<String> f;
    public final List<String> g;
    public final boolean h;
    public final String i;
    public final long j;
    public final String k;
    public final int l;
    public final int m;
    public final long n;
    public final boolean o;
    public final boolean p;
    public final boolean q;
    public int r;
    public int s;
    public boolean t;

    public bcb(String str) throws JSONException {
        this(new JSONObject(str));
    }

    public bcb(List<bca> list, long j2, List<String> list2, List<String> list3, List<String> list4, List<String> list5, List<String> list6, boolean z, String str, long j3, int i2, int i3, String str2, int i4, int i5, long j4, boolean z2) {
        this.f3121a = list;
        this.f3122b = j2;
        this.c = list2;
        this.d = list3;
        this.e = list4;
        this.f = list5;
        this.g = list6;
        this.h = z;
        this.i = str;
        this.j = -1;
        this.r = 0;
        this.s = 1;
        this.k = null;
        this.l = 0;
        this.m = -1;
        this.n = -1;
        this.o = false;
        this.p = false;
        this.q = false;
        this.t = false;
    }

    public bcb(JSONObject jSONObject) throws JSONException {
        boolean z;
        if (jm.a(2)) {
            String str = "Mediation Response JSON: ";
            String valueOf = String.valueOf(jSONObject.toString(2));
            jm.a(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
        }
        JSONArray jSONArray = jSONObject.getJSONArray("ad_networks");
        ArrayList arrayList = new ArrayList(jSONArray.length());
        int i2 = -1;
        for (int i3 = 0; i3 < jSONArray.length(); i3++) {
            bca bca = new bca(jSONArray.getJSONObject(i3));
            if (bca.a()) {
                this.t = true;
            }
            arrayList.add(bca);
            if (i2 < 0) {
                Iterator it = bca.c.iterator();
                while (true) {
                    if (it.hasNext()) {
                        if (((String) it.next()).equals("com.google.ads.mediation.admob.AdMobAdapter")) {
                            z = true;
                            break;
                        }
                    } else {
                        z = false;
                        break;
                    }
                }
                if (z) {
                    i2 = i3;
                }
            }
        }
        this.r = i2;
        this.s = jSONArray.length();
        this.f3121a = Collections.unmodifiableList(arrayList);
        this.i = jSONObject.optString("qdata");
        this.m = jSONObject.optInt("fs_model_type", -1);
        this.n = jSONObject.optLong("timeout_ms", -1);
        JSONObject optJSONObject = jSONObject.optJSONObject("settings");
        if (optJSONObject != null) {
            this.f3122b = optJSONObject.optLong("ad_network_timeout_millis", -1);
            ax.x();
            this.c = bck.a(optJSONObject, "click_urls");
            ax.x();
            this.d = bck.a(optJSONObject, "imp_urls");
            ax.x();
            this.e = bck.a(optJSONObject, "downloaded_imp_urls");
            ax.x();
            this.f = bck.a(optJSONObject, "nofill_urls");
            ax.x();
            this.g = bck.a(optJSONObject, "remote_ping_urls");
            this.h = optJSONObject.optBoolean("render_in_browser", false);
            long optLong = optJSONObject.optLong("refresh", -1);
            this.j = optLong > 0 ? optLong * 1000 : -1;
            hp a2 = hp.a(optJSONObject.optJSONArray("rewards"));
            if (a2 == null) {
                this.k = null;
                this.l = 0;
            } else {
                this.k = a2.f3369a;
                this.l = a2.f3370b;
            }
            this.o = optJSONObject.optBoolean("use_displayed_impression", false);
            this.p = optJSONObject.optBoolean("allow_pub_rendered_attribution", false);
            this.q = optJSONObject.optBoolean("allow_pub_owned_ad_view", false);
            return;
        }
        this.f3122b = -1;
        this.c = null;
        this.d = null;
        this.e = null;
        this.f = null;
        this.g = null;
        this.j = -1;
        this.k = null;
        this.l = 0;
        this.o = false;
        this.h = false;
        this.p = false;
        this.q = false;
    }
}
