package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.app.Application.ActivityLifecycleCallbacks;

final class aho implements ahu {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ Activity f2595a;

    aho(ahm ahm, Activity activity) {
        this.f2595a = activity;
    }

    public final void a(ActivityLifecycleCallbacks activityLifecycleCallbacks) {
        activityLifecycleCallbacks.onActivityStarted(this.f2595a);
    }
}
