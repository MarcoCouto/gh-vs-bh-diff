package com.google.android.gms.internal.ads;

import java.io.InputStream;
import java.util.Collections;
import java.util.List;

public final class pq {

    /* renamed from: a reason: collision with root package name */
    private final int f3612a;

    /* renamed from: b reason: collision with root package name */
    private final List<aqd> f3613b;
    private final int c;
    private final InputStream d;

    public pq(int i, List<aqd> list) {
        this(i, list, -1, null);
    }

    public pq(int i, List<aqd> list, int i2, InputStream inputStream) {
        this.f3612a = i;
        this.f3613b = list;
        this.c = i2;
        this.d = inputStream;
    }

    public final int a() {
        return this.f3612a;
    }

    public final List<aqd> b() {
        return Collections.unmodifiableList(this.f3613b);
    }

    public final int c() {
        return this.c;
    }

    public final InputStream d() {
        return this.d;
    }
}
