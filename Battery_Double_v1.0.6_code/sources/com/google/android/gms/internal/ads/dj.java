package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.Binder;
import android.os.Bundle;
import android.os.DeadObjectException;
import com.google.android.gms.ads.internal.ax;
import com.google.android.gms.common.internal.e.a;
import com.google.android.gms.common.internal.e.b;

@cm
public final class dj extends de implements a, b {

    /* renamed from: a reason: collision with root package name */
    private Context f3258a;

    /* renamed from: b reason: collision with root package name */
    private mu f3259b;
    private oa<dl> c;
    private final dc d;
    private final Object e = new Object();
    private dk f;

    public dj(Context context, mu muVar, oa<dl> oaVar, dc dcVar) {
        super(oaVar, dcVar);
        this.f3258a = context;
        this.f3259b = muVar;
        this.c = oaVar;
        this.d = dcVar;
        this.f = new dk(context, ((Boolean) ape.f().a(asi.G)).booleanValue() ? ax.t().a() : context.getMainLooper(), this, this);
        this.f.o();
    }

    public final void a() {
        synchronized (this.e) {
            if (this.f.b() || this.f.c()) {
                this.f.a();
            }
            Binder.flushPendingCommands();
        }
    }

    public final void a(int i) {
        jm.b("Disconnected from remote ad request service.");
    }

    public final void a(Bundle bundle) {
        c();
    }

    public final void a(com.google.android.gms.common.b bVar) {
        jm.b("Cannot connect to remote service, fallback to local instance.");
        new di(this.f3258a, this.c, this.d).c();
        Bundle bundle = new Bundle();
        bundle.putString("action", "gms_connection_failed_fallback_to_local");
        ax.e().b(this.f3258a, this.f3259b.f3528a, "gmob-apps", bundle, true);
    }

    public final dt d() {
        dt dtVar;
        synchronized (this.e) {
            try {
                dtVar = this.f.A();
            } catch (DeadObjectException | IllegalStateException e2) {
                dtVar = null;
            }
        }
        return dtVar;
    }
}
