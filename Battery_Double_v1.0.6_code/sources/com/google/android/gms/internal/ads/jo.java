package com.google.android.gms.internal.ads;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.security.NetworkSecurityPolicy;
import com.google.android.gms.ads.internal.ax;
import com.google.android.gms.common.util.n;
import java.util.Collections;
import java.util.Set;
import java.util.concurrent.CancellationException;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@cm
public final class jo {

    /* renamed from: a reason: collision with root package name */
    Editor f3430a;
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public final Object f3431b = new Object();
    private nn<?> c;
    /* access modifiers changed from: private */
    public CopyOnWriteArraySet<js> d = new CopyOnWriteArraySet<>();
    /* access modifiers changed from: private */
    public SharedPreferences e;
    /* access modifiers changed from: private */
    public boolean f = false;
    /* access modifiers changed from: private */
    public boolean g = true;
    /* access modifiers changed from: private */
    public String h;
    /* access modifiers changed from: private */
    public String i;
    /* access modifiers changed from: private */
    public boolean j = false;
    /* access modifiers changed from: private */
    public String k = "";
    /* access modifiers changed from: private */
    public long l = 0;
    /* access modifiers changed from: private */
    public long m = 0;
    /* access modifiers changed from: private */
    public long n = 0;
    /* access modifiers changed from: private */
    public int o = -1;
    /* access modifiers changed from: private */
    public int p = 0;
    /* access modifiers changed from: private */
    public Set<String> q = Collections.emptySet();
    /* access modifiers changed from: private */
    public JSONObject r = new JSONObject();
    /* access modifiers changed from: private */
    public boolean s = true;
    /* access modifiers changed from: private */
    public boolean t = true;

    /* access modifiers changed from: private */
    public final void a(Bundle bundle) {
        new jq(this, bundle).c();
    }

    /* access modifiers changed from: private */
    @TargetApi(23)
    public static boolean n() {
        return n.j() && !NetworkSecurityPolicy.getInstance().isCleartextTrafficPermitted();
    }

    private final void o() {
        if (this.c != null && !this.c.isDone()) {
            try {
                this.c.get(1, TimeUnit.SECONDS);
            } catch (InterruptedException e2) {
                Thread.currentThread().interrupt();
                jm.c("Interrupted while waiting for preferences loaded.", e2);
            } catch (CancellationException | ExecutionException | TimeoutException e3) {
                jm.b("Fail to initialize AdSharedPreferenceManager.", e3);
            }
        }
    }

    /* access modifiers changed from: private */
    public final Bundle p() {
        Bundle bundle = new Bundle();
        bundle.putBoolean("listener_registration_bundle", true);
        synchronized (this.f3431b) {
            bundle.putBoolean("use_https", this.g);
            bundle.putBoolean("content_url_opted_out", this.s);
            bundle.putBoolean("content_vertical_opted_out", this.t);
            bundle.putBoolean("auto_collect_location", this.j);
            bundle.putInt("version_code", this.p);
            bundle.putStringArray("never_pool_slots", (String[]) this.q.toArray(new String[this.q.size()]));
            bundle.putString("app_settings_json", this.k);
            bundle.putLong("app_settings_last_update_ms", this.l);
            bundle.putLong("app_last_background_time_ms", this.m);
            bundle.putInt("request_in_session_count", this.o);
            bundle.putLong("first_ad_req_time_ms", this.n);
            bundle.putString("native_advanced_settings", this.r.toString());
            if (this.h != null) {
                bundle.putString("content_url_hashes", this.h);
            }
            if (this.i != null) {
                bundle.putString("content_vertical_hashes", this.i);
            }
        }
        return bundle;
    }

    public final void a(int i2) {
        o();
        synchronized (this.f3431b) {
            if (this.p != i2) {
                this.p = i2;
                if (this.f3430a != null) {
                    this.f3430a.putInt("version_code", i2);
                    this.f3430a.apply();
                }
                Bundle bundle = new Bundle();
                bundle.putInt("version_code", i2);
                a(bundle);
            }
        }
    }

    public final void a(long j2) {
        o();
        synchronized (this.f3431b) {
            if (this.m != j2) {
                this.m = j2;
                if (this.f3430a != null) {
                    this.f3430a.putLong("app_last_background_time_ms", j2);
                    this.f3430a.apply();
                }
                Bundle bundle = new Bundle();
                bundle.putLong("app_last_background_time_ms", j2);
                a(bundle);
            }
        }
    }

    public final void a(Context context) {
        if (context.getApplicationContext() != null) {
            context = context.getApplicationContext();
        }
        this.c = (nn) new jp(this, context).c();
    }

    public final void a(js jsVar) {
        synchronized (this.f3431b) {
            if (this.c != null && this.c.isDone()) {
                jsVar.a(p());
            }
            this.d.add(jsVar);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        return;
     */
    public final void a(String str) {
        o();
        synchronized (this.f3431b) {
            if (str != null) {
                if (!str.equals(this.h)) {
                    this.h = str;
                    if (this.f3430a != null) {
                        this.f3430a.putString("content_url_hashes", str);
                        this.f3430a.apply();
                    }
                    Bundle bundle = new Bundle();
                    bundle.putString("content_url_hashes", str);
                    a(bundle);
                }
            }
        }
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    public final void a(String str, String str2, boolean z) {
        int i2 = 0;
        o();
        synchronized (this.f3431b) {
            JSONArray optJSONArray = this.r.optJSONArray(str);
            JSONArray jSONArray = optJSONArray == null ? new JSONArray() : optJSONArray;
            int length = jSONArray.length();
            while (true) {
                if (i2 >= jSONArray.length()) {
                    i2 = length;
                    break;
                }
                JSONObject optJSONObject = jSONArray.optJSONObject(i2);
                if (optJSONObject != null) {
                    if (!str2.equals(optJSONObject.optString("template_id"))) {
                        i2++;
                    } else if (z && optJSONObject.optBoolean("uses_media_view", false) == z) {
                        return;
                    }
                } else {
                    return;
                }
            }
            try {
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("template_id", str2);
                jSONObject.put("uses_media_view", z);
                jSONObject.put("timestamp_ms", ax.l().a());
                jSONArray.put(i2, jSONObject);
                this.r.put(str, jSONArray);
            } catch (JSONException e2) {
                jm.c("Could not update native advanced settings", e2);
            }
            if (this.f3430a != null) {
                this.f3430a.putString("native_advanced_settings", this.r.toString());
                this.f3430a.apply();
            }
            Bundle bundle = new Bundle();
            bundle.putString("native_advanced_settings", this.r.toString());
            a(bundle);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
        return;
     */
    public final void a(boolean z) {
        o();
        synchronized (this.f3431b) {
            if (this.g != z) {
                this.g = z;
                if (this.f3430a != null) {
                    this.f3430a.putBoolean("use_https", z);
                    this.f3430a.apply();
                }
                if (!this.f) {
                    Bundle bundle = new Bundle();
                    bundle.putBoolean("use_https", z);
                    a(bundle);
                }
            }
        }
    }

    public final boolean a() {
        boolean z;
        o();
        synchronized (this.f3431b) {
            z = this.g || this.f;
        }
        return z;
    }

    public final void b(int i2) {
        o();
        synchronized (this.f3431b) {
            if (this.o != i2) {
                this.o = i2;
                if (this.f3430a != null) {
                    this.f3430a.putInt("request_in_session_count", i2);
                    this.f3430a.apply();
                }
                Bundle bundle = new Bundle();
                bundle.putInt("request_in_session_count", i2);
                a(bundle);
            }
        }
    }

    public final void b(long j2) {
        o();
        synchronized (this.f3431b) {
            if (this.n != j2) {
                this.n = j2;
                if (this.f3430a != null) {
                    this.f3430a.putLong("first_ad_req_time_ms", j2);
                    this.f3430a.apply();
                }
                Bundle bundle = new Bundle();
                bundle.putLong("first_ad_req_time_ms", j2);
                a(bundle);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        return;
     */
    public final void b(String str) {
        o();
        synchronized (this.f3431b) {
            if (str != null) {
                if (!str.equals(this.i)) {
                    this.i = str;
                    if (this.f3430a != null) {
                        this.f3430a.putString("content_vertical_hashes", str);
                        this.f3430a.apply();
                    }
                    Bundle bundle = new Bundle();
                    bundle.putString("content_vertical_hashes", str);
                    a(bundle);
                }
            }
        }
    }

    public final void b(boolean z) {
        o();
        synchronized (this.f3431b) {
            if (this.s != z) {
                this.s = z;
                if (this.f3430a != null) {
                    this.f3430a.putBoolean("content_url_opted_out", z);
                    this.f3430a.apply();
                }
                Bundle bundle = new Bundle();
                bundle.putBoolean("content_url_opted_out", this.s);
                bundle.putBoolean("content_vertical_opted_out", this.t);
                a(bundle);
            }
        }
    }

    public final boolean b() {
        boolean z;
        o();
        synchronized (this.f3431b) {
            z = this.s;
        }
        return z;
    }

    public final String c() {
        String str;
        o();
        synchronized (this.f3431b) {
            str = this.h;
        }
        return str;
    }

    public final void c(String str) {
        o();
        synchronized (this.f3431b) {
            if (!this.q.contains(str)) {
                this.q.add(str);
                if (this.f3430a != null) {
                    this.f3430a.putStringSet("never_pool_slots", this.q);
                    this.f3430a.apply();
                }
                Bundle bundle = new Bundle();
                bundle.putStringArray("never_pool_slots", (String[]) this.q.toArray(new String[this.q.size()]));
                a(bundle);
            }
        }
    }

    public final void c(boolean z) {
        o();
        synchronized (this.f3431b) {
            if (this.t != z) {
                this.t = z;
                if (this.f3430a != null) {
                    this.f3430a.putBoolean("content_vertical_opted_out", z);
                    this.f3430a.apply();
                }
                Bundle bundle = new Bundle();
                bundle.putBoolean("content_url_opted_out", this.s);
                bundle.putBoolean("content_vertical_opted_out", this.t);
                a(bundle);
            }
        }
    }

    public final void d(String str) {
        o();
        synchronized (this.f3431b) {
            if (this.q.contains(str)) {
                this.q.remove(str);
                if (this.f3430a != null) {
                    this.f3430a.putStringSet("never_pool_slots", this.q);
                    this.f3430a.apply();
                }
                Bundle bundle = new Bundle();
                bundle.putStringArray("never_pool_slots", (String[]) this.q.toArray(new String[this.q.size()]));
                a(bundle);
            }
        }
    }

    public final void d(boolean z) {
        o();
        synchronized (this.f3431b) {
            if (this.j != z) {
                this.j = z;
                if (this.f3430a != null) {
                    this.f3430a.putBoolean("auto_collect_location", z);
                    this.f3430a.apply();
                }
                Bundle bundle = new Bundle();
                bundle.putBoolean("auto_collect_location", z);
                a(bundle);
            }
        }
    }

    public final boolean d() {
        boolean z;
        o();
        synchronized (this.f3431b) {
            z = this.t;
        }
        return z;
    }

    public final String e() {
        String str;
        o();
        synchronized (this.f3431b) {
            str = this.i;
        }
        return str;
    }

    public final boolean e(String str) {
        boolean contains;
        o();
        synchronized (this.f3431b) {
            contains = this.q.contains(str);
        }
        return contains;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
        return;
     */
    public final void f(String str) {
        o();
        synchronized (this.f3431b) {
            long a2 = ax.l().a();
            this.l = a2;
            if (str != null && !str.equals(this.k)) {
                this.k = str;
                if (this.f3430a != null) {
                    this.f3430a.putString("app_settings_json", str);
                    this.f3430a.putLong("app_settings_last_update_ms", a2);
                    this.f3430a.apply();
                }
                Bundle bundle = new Bundle();
                bundle.putString("app_settings_json", str);
                bundle.putLong("app_settings_last_update_ms", a2);
                a(bundle);
            }
        }
    }

    public final boolean f() {
        boolean z;
        o();
        synchronized (this.f3431b) {
            z = this.j;
        }
        return z;
    }

    public final int g() {
        int i2;
        o();
        synchronized (this.f3431b) {
            i2 = this.p;
        }
        return i2;
    }

    public final iv h() {
        iv ivVar;
        o();
        synchronized (this.f3431b) {
            ivVar = new iv(this.k, this.l);
        }
        return ivVar;
    }

    public final long i() {
        long j2;
        o();
        synchronized (this.f3431b) {
            j2 = this.m;
        }
        return j2;
    }

    public final int j() {
        int i2;
        o();
        synchronized (this.f3431b) {
            i2 = this.o;
        }
        return i2;
    }

    public final long k() {
        long j2;
        o();
        synchronized (this.f3431b) {
            j2 = this.n;
        }
        return j2;
    }

    public final JSONObject l() {
        JSONObject jSONObject;
        o();
        synchronized (this.f3431b) {
            jSONObject = this.r;
        }
        return jSONObject;
    }

    public final void m() {
        o();
        synchronized (this.f3431b) {
            this.r = new JSONObject();
            if (this.f3430a != null) {
                this.f3430a.remove("native_advanced_settings");
                this.f3430a.apply();
            }
            Bundle bundle = new Bundle();
            bundle.putString("native_advanced_settings", "{}");
            a(bundle);
        }
    }
}
