package com.google.android.gms.internal.ads;

import java.io.IOException;

final class aek extends aei<aej, aej> {
    aek() {
    }

    private static void a(Object obj, aej aej) {
        ((abp) obj).zzdtt = aej;
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ Object a() {
        return aej.b();
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ Object a(Object obj) {
        aej aej = (aej) obj;
        aej.c();
        return aej;
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ void a(Object obj, int i, int i2) {
        ((aej) obj).a((i << 3) | 5, (Object) Integer.valueOf(i2));
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ void a(Object obj, int i, long j) {
        ((aej) obj).a(i << 3, (Object) Long.valueOf(j));
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ void a(Object obj, int i, aah aah) {
        ((aej) obj).a((i << 3) | 2, (Object) aah);
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ void a(Object obj, int i, Object obj2) {
        ((aej) obj).a((i << 3) | 3, (Object) (aej) obj2);
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ void a(Object obj, afc afc) throws IOException {
        ((aej) obj).b(afc);
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ void a(Object obj, Object obj2) {
        a(obj, (aej) obj2);
    }

    /* access modifiers changed from: 0000 */
    public final boolean a(ado ado) {
        return false;
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ Object b(Object obj) {
        return ((abp) obj).zzdtt;
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ void b(Object obj, int i, long j) {
        ((aej) obj).a((i << 3) | 1, (Object) Long.valueOf(j));
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ void b(Object obj, afc afc) throws IOException {
        ((aej) obj).a(afc);
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ void b(Object obj, Object obj2) {
        a(obj, (aej) obj2);
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ Object c(Object obj) {
        aej aej = ((abp) obj).zzdtt;
        if (aej != aej.a()) {
            return aej;
        }
        aej b2 = aej.b();
        a(obj, b2);
        return b2;
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ Object c(Object obj, Object obj2) {
        aej aej = (aej) obj;
        aej aej2 = (aej) obj2;
        return aej2.equals(aej.a()) ? aej : aej.a(aej, aej2);
    }

    /* access modifiers changed from: 0000 */
    public final void d(Object obj) {
        ((abp) obj).zzdtt.c();
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ int e(Object obj) {
        return ((aej) obj).d();
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ int f(Object obj) {
        return ((aej) obj).e();
    }
}
