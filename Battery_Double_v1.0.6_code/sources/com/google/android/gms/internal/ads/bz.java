package com.google.android.gms.internal.ads;

import org.json.JSONObject;

final /* synthetic */ class bz implements mx {

    /* renamed from: a reason: collision with root package name */
    private final bu f3215a;

    /* renamed from: b reason: collision with root package name */
    private final JSONObject f3216b;

    bz(bu buVar, JSONObject jSONObject) {
        this.f3215a = buVar;
        this.f3216b = jSONObject;
    }

    public final nn a(Object obj) {
        return this.f3215a.a(this.f3216b, (qn) obj);
    }
}
