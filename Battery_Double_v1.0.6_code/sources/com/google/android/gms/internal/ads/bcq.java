package com.google.android.gms.internal.ads;

import android.os.RemoteException;
import com.google.ads.mediation.AdUrlAdapter;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.ads.mediation.b;
import com.google.ads.mediation.e;
import com.google.ads.mediation.f;
import com.google.android.gms.ads.mediation.customevent.CustomEventAdapter;
import com.google.android.gms.ads.mediation.customevent.a;
import com.google.android.gms.ads.mediation.customevent.c;
import java.util.Map;

@cm
public final class bcq extends bcs {

    /* renamed from: b reason: collision with root package name */
    private static final bel f3143b = new bel();

    /* renamed from: a reason: collision with root package name */
    private Map<Class<? extends Object>, Object> f3144a;

    private final <NetworkExtrasT extends f, ServerParametersT extends e> bcu d(String str) throws RemoteException {
        try {
            Class cls = Class.forName(str, false, bcq.class.getClassLoader());
            if (b.class.isAssignableFrom(cls)) {
                b bVar = (b) cls.getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
                return new bdu(bVar, (f) this.f3144a.get(bVar.getAdditionalParametersType()));
            } else if (com.google.android.gms.ads.mediation.b.class.isAssignableFrom(cls)) {
                return new bdp((com.google.android.gms.ads.mediation.b) cls.getDeclaredConstructor(new Class[0]).newInstance(new Object[0]));
            } else {
                ms.e(new StringBuilder(String.valueOf(str).length() + 64).append("Could not instantiate mediation adapter: ").append(str).append(" (not a valid adapter).").toString());
                throw new RemoteException();
            }
        } catch (Throwable th) {
            return e(str);
        }
    }

    private final bcu e(String str) throws RemoteException {
        try {
            ms.b("Reflection failed, retrying using direct instantiation");
            if ("com.google.ads.mediation.admob.AdMobAdapter".equals(str)) {
                return new bdp(new AdMobAdapter());
            }
            if ("com.google.ads.mediation.AdUrlAdapter".equals(str)) {
                return new bdp(new AdUrlAdapter());
            }
            if ("com.google.android.gms.ads.mediation.customevent.CustomEventAdapter".equals(str)) {
                return new bdp(new CustomEventAdapter());
            }
            if ("com.google.ads.mediation.customevent.CustomEventAdapter".equals(str)) {
                com.google.ads.mediation.customevent.CustomEventAdapter customEventAdapter = new com.google.ads.mediation.customevent.CustomEventAdapter();
                return new bdu(customEventAdapter, (c) this.f3144a.get(customEventAdapter.getAdditionalParametersType()));
            }
            throw new RemoteException();
        } catch (Throwable th) {
            ms.c(new StringBuilder(String.valueOf(str).length() + 43).append("Could not instantiate mediation adapter: ").append(str).append(". ").toString(), th);
        }
    }

    public final bcu a(String str) throws RemoteException {
        return d(str);
    }

    public final void a(Map<Class<? extends Object>, Object> map) {
        this.f3144a = map;
    }

    public final boolean b(String str) throws RemoteException {
        boolean z = false;
        try {
            return a.class.isAssignableFrom(Class.forName(str, false, bcq.class.getClassLoader()));
        } catch (Throwable th) {
            ms.e(new StringBuilder(String.valueOf(str).length() + 80).append("Could not load custom event implementation class: ").append(str).append(", assuming old implementation.").toString());
            return z;
        }
    }

    public final beg c(String str) throws RemoteException {
        return bel.a(str);
    }
}
