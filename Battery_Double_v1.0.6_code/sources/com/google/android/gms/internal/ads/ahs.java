package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.app.Application.ActivityLifecycleCallbacks;
import android.os.Bundle;

final class ahs implements ahu {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ Activity f2599a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ Bundle f2600b;

    ahs(ahm ahm, Activity activity, Bundle bundle) {
        this.f2599a = activity;
        this.f2600b = bundle;
    }

    public final void a(ActivityLifecycleCallbacks activityLifecycleCallbacks) {
        activityLifecycleCallbacks.onActivitySaveInstanceState(this.f2599a, this.f2600b);
    }
}
