package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.ax;

@cm
public abstract class de implements dc, ko<Void> {

    /* renamed from: a reason: collision with root package name */
    private final oa<dl> f3250a;

    /* renamed from: b reason: collision with root package name */
    private final dc f3251b;
    private final Object c = new Object();

    public de(oa<dl> oaVar, dc dcVar) {
        this.f3250a = oaVar;
        this.f3251b = dcVar;
    }

    public abstract void a();

    public final void a(dp dpVar) {
        synchronized (this.c) {
            this.f3251b.a(dpVar);
            a();
        }
    }

    /* access modifiers changed from: 0000 */
    public final boolean a(dt dtVar, dl dlVar) {
        try {
            dtVar.a(dlVar, (dw) new Cdo(this));
            return true;
        } catch (Throwable th) {
            jm.c("Could not fetch ad response from ad request service due to an Exception.", th);
            ax.i().a(th, "AdRequestClientTask.getAdResponseFromService");
            this.f3251b.a(new dp(0));
            return false;
        }
    }

    public final void b() {
        a();
    }

    public final /* synthetic */ Object c() {
        dt d = d();
        if (d == null) {
            this.f3251b.a(new dp(0));
            a();
        } else {
            this.f3250a.a(new dg(this, d), new dh(this));
        }
        return null;
    }

    public abstract dt d();
}
