package com.google.android.gms.internal.ads;

final /* synthetic */ class tw {

    /* renamed from: a reason: collision with root package name */
    static final /* synthetic */ int[] f3714a = new int[ya.values().length];

    static {
        try {
            f3714a[ya.LEGACY.ordinal()] = 1;
        } catch (NoSuchFieldError e) {
        }
        try {
            f3714a[ya.CRUNCHY.ordinal()] = 2;
        } catch (NoSuchFieldError e2) {
        }
        try {
            f3714a[ya.TINK.ordinal()] = 3;
        } catch (NoSuchFieldError e3) {
        }
        try {
            f3714a[ya.RAW.ordinal()] = 4;
        } catch (NoSuchFieldError e4) {
        }
    }
}
