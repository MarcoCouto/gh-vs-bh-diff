package com.google.android.gms.internal.ads;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Parcel;
import android.util.Base64;
import com.google.android.gms.ads.internal.ax;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@cm
public final class ayw {

    /* renamed from: a reason: collision with root package name */
    private final Map<ayx, ayy> f3013a = new HashMap();

    /* renamed from: b reason: collision with root package name */
    private final LinkedList<ayx> f3014b = new LinkedList<>();
    private axr c;

    static Set<String> a(aop aop) {
        HashSet hashSet = new HashSet();
        hashSet.addAll(aop.c.keySet());
        Bundle bundle = aop.m.getBundle("com.google.ads.mediation.admob.AdMobAdapter");
        if (bundle != null) {
            hashSet.addAll(bundle.keySet());
        }
        return hashSet;
    }

    private static void a(Bundle bundle, String str) {
        while (true) {
            String[] split = str.split("/", 2);
            if (split.length != 0) {
                String str2 = split[0];
                if (split.length == 1) {
                    bundle.remove(str2);
                    return;
                }
                bundle = bundle.getBundle(str2);
                if (bundle != null) {
                    str = split[1];
                } else {
                    return;
                }
            } else {
                return;
            }
        }
    }

    private static void a(String str, ayx ayx) {
        if (jm.a(2)) {
            jm.a(String.format(str, new Object[]{ayx}));
        }
    }

    private static String[] a(String str) {
        try {
            String[] split = str.split("\u0000");
            for (int i = 0; i < split.length; i++) {
                split[i] = new String(Base64.decode(split[i], 0), "UTF-8");
            }
            return split;
        } catch (UnsupportedEncodingException e) {
            return new String[0];
        }
    }

    static aop b(aop aop) {
        aop d = d(aop);
        String str = "_skipMediation";
        Bundle bundle = d.m.getBundle("com.google.ads.mediation.admob.AdMobAdapter");
        if (bundle != null) {
            bundle.putBoolean(str, true);
        }
        d.c.putBoolean(str, true);
        return d;
    }

    private final String b() {
        try {
            StringBuilder sb = new StringBuilder();
            Iterator it = this.f3014b.iterator();
            while (it.hasNext()) {
                sb.append(Base64.encodeToString(((ayx) it.next()).toString().getBytes("UTF-8"), 0));
                if (it.hasNext()) {
                    sb.append("\u0000");
                }
            }
            return sb.toString();
        } catch (UnsupportedEncodingException e) {
            return "";
        }
    }

    private static boolean b(String str) {
        try {
            return Pattern.matches((String) ape.f().a(asi.ba), str);
        } catch (RuntimeException e) {
            ax.i().a((Throwable) e, "InterstitialAdPool.isExcludedAdUnit");
            return false;
        }
    }

    private static aop c(aop aop) {
        String[] split;
        aop d = d(aop);
        for (String str : ((String) ape.f().a(asi.aW)).split(",")) {
            a(d.m, str);
            String str2 = "com.google.ads.mediation.admob.AdMobAdapter/";
            if (str.startsWith(str2)) {
                a(d.c, str.replaceFirst(str2, ""));
            }
        }
        return d;
    }

    private static String c(String str) {
        try {
            Matcher matcher = Pattern.compile("([^/]+/[0-9]+).*").matcher(str);
            return matcher.matches() ? matcher.group(1) : str;
        } catch (RuntimeException e) {
            return str;
        }
    }

    private static aop d(aop aop) {
        Parcel obtain = Parcel.obtain();
        aop.writeToParcel(obtain, 0);
        obtain.setDataPosition(0);
        aop aop2 = (aop) aop.CREATOR.createFromParcel(obtain);
        obtain.recycle();
        return ((Boolean) ape.f().a(asi.aN)).booleanValue() ? aop2.a() : aop2;
    }

    /* access modifiers changed from: 0000 */
    public final ayz a(aop aop, String str) {
        ayy ayy;
        if (b(str)) {
            return null;
        }
        int i = new fj(this.c.a()).a().n;
        aop c2 = c(aop);
        String c3 = c(str);
        ayx ayx = new ayx(c2, c3, i);
        ayy ayy2 = (ayy) this.f3013a.get(ayx);
        if (ayy2 == null) {
            a("Interstitial pool created at %s.", ayx);
            ayy ayy3 = new ayy(c2, c3, i);
            this.f3013a.put(ayx, ayy3);
            ayy = ayy3;
        } else {
            ayy = ayy2;
        }
        this.f3014b.remove(ayx);
        this.f3014b.add(ayx);
        ayy.g();
        while (true) {
            if (this.f3014b.size() <= ((Integer) ape.f().a(asi.aX)).intValue()) {
                break;
            }
            ayx ayx2 = (ayx) this.f3014b.remove();
            ayy ayy4 = (ayy) this.f3013a.get(ayx2);
            a("Evicting interstitial queue for %s.", ayx2);
            while (ayy4.d() > 0) {
                ayz a2 = ayy4.a((aop) null);
                if (a2.e) {
                    azb.a().c();
                }
                a2.f3018a.K();
            }
            this.f3013a.remove(ayx2);
        }
        while (ayy.d() > 0) {
            ayz a3 = ayy.a(c2);
            if (a3.e) {
                if (ax.l().a() - a3.d > 1000 * ((long) ((Integer) ape.f().a(asi.aZ)).intValue())) {
                    a("Expired interstitial at %s.", ayx);
                    azb.a().b();
                }
            }
            String str2 = a3.f3019b != null ? " (inline) " : " ";
            a(new StringBuilder(String.valueOf(str2).length() + 34).append("Pooled interstitial").append(str2).append("returned at %s.").toString(), ayx);
            return a3;
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    public final void a() {
        if (this.c != null) {
            for (Entry entry : this.f3013a.entrySet()) {
                ayx ayx = (ayx) entry.getKey();
                ayy ayy = (ayy) entry.getValue();
                if (jm.a(2)) {
                    int d = ayy.d();
                    int e = ayy.e();
                    if (e < d) {
                        jm.a(String.format("Loading %s/%s pooled interstitials for %s.", new Object[]{Integer.valueOf(d - e), Integer.valueOf(d), ayx}));
                    }
                }
                int f = ayy.f() + 0;
                while (true) {
                    if (ayy.d() >= ((Integer) ape.f().a(asi.aY)).intValue()) {
                        break;
                    }
                    a("Pooling and loading one new interstitial for %s.", ayx);
                    if (ayy.a(this.c)) {
                        f++;
                    }
                }
                azb.a().a(f);
            }
            if (this.c != null) {
                Editor edit = this.c.a().getSharedPreferences("com.google.android.gms.ads.internal.interstitial.InterstitialAdPool", 0).edit();
                edit.clear();
                for (Entry entry2 : this.f3013a.entrySet()) {
                    ayx ayx2 = (ayx) entry2.getKey();
                    ayy ayy2 = (ayy) entry2.getValue();
                    if (ayy2.h()) {
                        edit.putString(ayx2.toString(), new azd(ayy2).a());
                        a("Saved interstitial queue for %s.", ayx2);
                    }
                }
                edit.putString("PoolKeys", b());
                edit.apply();
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public final void a(axr axr) {
        if (this.c == null) {
            this.c = axr.b();
            if (this.c != null) {
                SharedPreferences sharedPreferences = this.c.a().getSharedPreferences("com.google.android.gms.ads.internal.interstitial.InterstitialAdPool", 0);
                while (this.f3014b.size() > 0) {
                    ayx ayx = (ayx) this.f3014b.remove();
                    ayy ayy = (ayy) this.f3013a.get(ayx);
                    a("Flushing interstitial queue for %s.", ayx);
                    while (ayy.d() > 0) {
                        ayy.a((aop) null).f3018a.K();
                    }
                    this.f3013a.remove(ayx);
                }
                try {
                    HashMap hashMap = new HashMap();
                    for (Entry entry : sharedPreferences.getAll().entrySet()) {
                        if (!((String) entry.getKey()).equals("PoolKeys")) {
                            azd a2 = azd.a((String) entry.getValue());
                            ayx ayx2 = new ayx(a2.f3028a, a2.f3029b, a2.c);
                            if (!this.f3013a.containsKey(ayx2)) {
                                this.f3013a.put(ayx2, new ayy(a2.f3028a, a2.f3029b, a2.c));
                                hashMap.put(ayx2.toString(), ayx2);
                                a("Restored interstitial queue for %s.", ayx2);
                            }
                        }
                    }
                    for (String str : a(sharedPreferences.getString("PoolKeys", ""))) {
                        ayx ayx3 = (ayx) hashMap.get(str);
                        if (this.f3013a.containsKey(ayx3)) {
                            this.f3014b.add(ayx3);
                        }
                    }
                } catch (IOException | RuntimeException e) {
                    ax.i().a(e, "InterstitialAdPool.restore");
                    jm.c("Malformed preferences value for InterstitialAdPool.", e);
                    this.f3013a.clear();
                    this.f3014b.clear();
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public final void b(aop aop, String str) {
        if (this.c != null) {
            int i = new fj(this.c.a()).a().n;
            aop c2 = c(aop);
            String c3 = c(str);
            ayx ayx = new ayx(c2, c3, i);
            ayy ayy = (ayy) this.f3013a.get(ayx);
            if (ayy == null) {
                a("Interstitial pool created at %s.", ayx);
                ayy = new ayy(c2, c3, i);
                this.f3013a.put(ayx, ayy);
            }
            ayy.a(this.c, aop);
            ayy.g();
            a("Inline entry added to the queue at %s.", ayx);
        }
    }
}
