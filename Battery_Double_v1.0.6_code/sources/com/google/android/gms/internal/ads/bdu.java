package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.os.Bundle;
import android.os.RemoteException;
import com.google.ads.mediation.MediationBannerAdapter;
import com.google.ads.mediation.MediationInterstitialAdapter;
import com.google.ads.mediation.b;
import com.google.ads.mediation.e;
import com.google.ads.mediation.f;
import com.google.android.gms.ads.l;
import com.google.android.gms.b.a;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import org.json.JSONObject;

@cm
public final class bdu<NETWORK_EXTRAS extends f, SERVER_PARAMETERS extends e> extends bcv {

    /* renamed from: a reason: collision with root package name */
    private final b<NETWORK_EXTRAS, SERVER_PARAMETERS> f3157a;

    /* renamed from: b reason: collision with root package name */
    private final NETWORK_EXTRAS f3158b;

    public bdu(b<NETWORK_EXTRAS, SERVER_PARAMETERS> bVar, NETWORK_EXTRAS network_extras) {
        this.f3157a = bVar;
        this.f3158b = network_extras;
    }

    private final SERVER_PARAMETERS a(String str, int i, String str2) throws RemoteException {
        HashMap hashMap;
        if (str != null) {
            try {
                JSONObject jSONObject = new JSONObject(str);
                hashMap = new HashMap(jSONObject.length());
                Iterator keys = jSONObject.keys();
                while (keys.hasNext()) {
                    String str3 = (String) keys.next();
                    hashMap.put(str3, jSONObject.getString(str3));
                }
            } catch (Throwable th) {
                ms.b("", th);
                throw new RemoteException();
            }
        } else {
            hashMap = new HashMap(0);
        }
        Class serverParametersType = this.f3157a.getServerParametersType();
        if (serverParametersType == null) {
            return null;
        }
        SERVER_PARAMETERS server_parameters = (e) serverParametersType.newInstance();
        server_parameters.a(hashMap);
        return server_parameters;
    }

    private static boolean a(aop aop) {
        if (!aop.f) {
            ape.a();
            if (!mh.a()) {
                return false;
            }
        }
        return true;
    }

    public final a a() throws RemoteException {
        if (!(this.f3157a instanceof MediationBannerAdapter)) {
            String str = "Not a MediationBannerAdapter: ";
            String valueOf = String.valueOf(this.f3157a.getClass().getCanonicalName());
            ms.e(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
            throw new RemoteException();
        }
        try {
            return com.google.android.gms.b.b.a(((MediationBannerAdapter) this.f3157a).getBannerView());
        } catch (Throwable th) {
            ms.b("", th);
            throw new RemoteException();
        }
    }

    public final void a(a aVar) throws RemoteException {
    }

    public final void a(a aVar, aop aop, String str, bcx bcx) throws RemoteException {
        a(aVar, aop, str, (String) null, bcx);
    }

    public final void a(a aVar, aop aop, String str, hl hlVar, String str2) throws RemoteException {
    }

    public final void a(a aVar, aop aop, String str, String str2, bcx bcx) throws RemoteException {
        if (!(this.f3157a instanceof MediationInterstitialAdapter)) {
            String str3 = "Not a MediationInterstitialAdapter: ";
            String valueOf = String.valueOf(this.f3157a.getClass().getCanonicalName());
            ms.e(valueOf.length() != 0 ? str3.concat(valueOf) : new String(str3));
            throw new RemoteException();
        }
        ms.b("Requesting interstitial ad from adapter.");
        try {
            ((MediationInterstitialAdapter) this.f3157a).requestInterstitialAd(new bdv(bcx), (Activity) com.google.android.gms.b.b.a(aVar), a(str, aop.g, str2), bdz.a(aop, a(aop)), this.f3158b);
        } catch (Throwable th) {
            ms.b("", th);
            throw new RemoteException();
        }
    }

    public final void a(a aVar, aop aop, String str, String str2, bcx bcx, aul aul, List<String> list) {
    }

    public final void a(a aVar, aot aot, aop aop, String str, bcx bcx) throws RemoteException {
        a(aVar, aot, aop, str, null, bcx);
    }

    public final void a(a aVar, aot aot, aop aop, String str, String str2, bcx bcx) throws RemoteException {
        com.google.ads.b bVar;
        int i = 0;
        if (!(this.f3157a instanceof MediationBannerAdapter)) {
            String str3 = "Not a MediationBannerAdapter: ";
            String valueOf = String.valueOf(this.f3157a.getClass().getCanonicalName());
            ms.e(valueOf.length() != 0 ? str3.concat(valueOf) : new String(str3));
            throw new RemoteException();
        }
        ms.b("Requesting banner ad from adapter.");
        try {
            MediationBannerAdapter mediationBannerAdapter = (MediationBannerAdapter) this.f3157a;
            bdv bdv = new bdv(bcx);
            Activity activity = (Activity) com.google.android.gms.b.b.a(aVar);
            e a2 = a(str, aop.g, str2);
            com.google.ads.b[] bVarArr = {com.google.ads.b.f1896a, com.google.ads.b.f1897b, com.google.ads.b.c, com.google.ads.b.d, com.google.ads.b.e, com.google.ads.b.f};
            while (true) {
                if (i < 6) {
                    if (bVarArr[i].a() == aot.e && bVarArr[i].b() == aot.f2815b) {
                        bVar = bVarArr[i];
                        break;
                    }
                    i++;
                } else {
                    bVar = new com.google.ads.b(l.a(aot.e, aot.f2815b, aot.f2814a));
                    break;
                }
            }
            mediationBannerAdapter.requestBannerAd(bdv, activity, a2, bVar, bdz.a(aop, a(aop)), this.f3158b);
        } catch (Throwable th) {
            ms.b("", th);
            throw new RemoteException();
        }
    }

    public final void a(a aVar, hl hlVar, List<String> list) {
    }

    public final void a(aop aop, String str) {
    }

    public final void a(aop aop, String str, String str2) {
    }

    public final void a(boolean z) {
    }

    public final void b() throws RemoteException {
        if (!(this.f3157a instanceof MediationInterstitialAdapter)) {
            String str = "Not a MediationInterstitialAdapter: ";
            String valueOf = String.valueOf(this.f3157a.getClass().getCanonicalName());
            ms.e(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
            throw new RemoteException();
        }
        ms.b("Showing interstitial from adapter.");
        try {
            ((MediationInterstitialAdapter) this.f3157a).showInterstitial();
        } catch (Throwable th) {
            ms.b("", th);
            throw new RemoteException();
        }
    }

    public final void c() throws RemoteException {
        try {
            this.f3157a.destroy();
        } catch (Throwable th) {
            ms.b("", th);
            throw new RemoteException();
        }
    }

    public final void d() throws RemoteException {
        throw new RemoteException();
    }

    public final void e() throws RemoteException {
        throw new RemoteException();
    }

    public final void f() {
    }

    public final boolean g() {
        return true;
    }

    public final bdd h() {
        return null;
    }

    public final bdh i() {
        return null;
    }

    public final Bundle j() {
        return new Bundle();
    }

    public final Bundle k() {
        return new Bundle();
    }

    public final Bundle l() {
        return new Bundle();
    }

    public final boolean m() {
        return false;
    }

    public final avt n() {
        return null;
    }

    public final aqs o() {
        return null;
    }

    public final bdk p() {
        return null;
    }
}
