package com.google.android.gms.internal.ads;

import java.util.concurrent.Future;

final /* synthetic */ class nk implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final nn f3545a;

    /* renamed from: b reason: collision with root package name */
    private final Future f3546b;

    nk(nn nnVar, Future future) {
        this.f3545a = nnVar;
        this.f3546b = future;
    }

    public final void run() {
        nn nnVar = this.f3545a;
        Future future = this.f3546b;
        if (nnVar.isCancelled()) {
            future.cancel(true);
        }
    }
}
