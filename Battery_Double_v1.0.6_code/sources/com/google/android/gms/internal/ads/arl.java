package com.google.android.gms.internal.ads;

import android.os.Bundle;
import com.google.android.gms.b.a;

public final class arl extends apw {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public apk f2856a;

    public final String D() {
        return null;
    }

    public final aqe E() {
        return null;
    }

    public final apk F() {
        return null;
    }

    public final void I() {
    }

    public final String a() {
        return null;
    }

    public final void a(af afVar, String str) {
    }

    public final void a(aot aot) {
    }

    public final void a(aph aph) {
    }

    public final void a(apk apk) {
        this.f2856a = apk;
    }

    public final void a(aqa aqa) {
    }

    public final void a(aqe aqe) {
    }

    public final void a(aqk aqk) {
    }

    public final void a(aqy aqy) {
    }

    public final void a(arr arr) {
    }

    public final void a(atc atc) {
    }

    public final void a(gn gnVar) {
    }

    public final void a(y yVar) {
    }

    public final void a(String str) {
    }

    public final void b(boolean z) {
    }

    public final boolean b(aop aop) {
        ms.c("This app is using a lightweight version of the Google Mobile Ads SDK that requires the latest Google Play services to be installed, but Google Play services is either missing or out of date.");
        mh.f3514a.post(new arm(this));
        return false;
    }

    public final void c(boolean z) {
    }

    public final void j() {
    }

    public final a k() {
        return null;
    }

    public final aot l() {
        return null;
    }

    public final boolean m() {
        return false;
    }

    public final void n() {
    }

    public final void o() {
    }

    public final void p() {
    }

    public final Bundle q() {
        return new Bundle();
    }

    public final String q_() {
        return null;
    }

    public final void r() {
    }

    public final boolean s() {
        return false;
    }

    public final aqs t() {
        return null;
    }
}
