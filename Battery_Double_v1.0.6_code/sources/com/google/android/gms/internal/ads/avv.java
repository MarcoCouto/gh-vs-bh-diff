package com.google.android.gms.internal.ads;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.b.a;
import com.google.android.gms.b.a.C0046a;
import java.util.ArrayList;
import java.util.List;

public final class avv extends ajk implements avt {
    avv(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.formats.client.INativeCustomTemplateAd");
    }

    public final String a(String str) throws RemoteException {
        Parcel r_ = r_();
        r_.writeString(str);
        Parcel a2 = a(1, r_);
        String readString = a2.readString();
        a2.recycle();
        return readString;
    }

    public final List<String> a() throws RemoteException {
        Parcel a2 = a(3, r_());
        ArrayList createStringArrayList = a2.createStringArrayList();
        a2.recycle();
        return createStringArrayList;
    }

    public final boolean a(a aVar) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (IInterface) aVar);
        Parcel a2 = a(10, r_);
        boolean a3 = ajm.a(a2);
        a2.recycle();
        return a3;
    }

    public final a b() throws RemoteException {
        Parcel a2 = a(11, r_());
        a a3 = C0046a.a(a2.readStrongBinder());
        a2.recycle();
        return a3;
    }

    public final auw b(String str) throws RemoteException {
        auw auy;
        Parcel r_ = r_();
        r_.writeString(str);
        Parcel a2 = a(2, r_);
        IBinder readStrongBinder = a2.readStrongBinder();
        if (readStrongBinder == null) {
            auy = null;
        } else {
            IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.ads.internal.formats.client.INativeAdImage");
            auy = queryLocalInterface instanceof auw ? (auw) queryLocalInterface : new auy(readStrongBinder);
        }
        a2.recycle();
        return auy;
    }

    public final aqs c() throws RemoteException {
        Parcel a2 = a(7, r_());
        aqs a3 = aqt.a(a2.readStrongBinder());
        a2.recycle();
        return a3;
    }

    public final void c(String str) throws RemoteException {
        Parcel r_ = r_();
        r_.writeString(str);
        b(5, r_);
    }

    public final void d() throws RemoteException {
        b(6, r_());
    }

    public final a e() throws RemoteException {
        Parcel a2 = a(9, r_());
        a a3 = C0046a.a(a2.readStrongBinder());
        a2.recycle();
        return a3;
    }

    public final void f() throws RemoteException {
        b(8, r_());
    }

    public final String l() throws RemoteException {
        Parcel a2 = a(4, r_());
        String readString = a2.readString();
        a2.recycle();
        return readString;
    }
}
