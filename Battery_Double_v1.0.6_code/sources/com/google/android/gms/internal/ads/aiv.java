package com.google.android.gms.internal.ads;

import java.lang.reflect.InvocationTargetException;

public final class aiv extends ajj {
    private long d = -1;

    public aiv(ahy ahy, String str, String str2, zz zzVar, int i, int i2) {
        super(ahy, str, str2, zzVar, i, 12);
    }

    /* access modifiers changed from: protected */
    public final void a() throws IllegalAccessException, InvocationTargetException {
        this.f2637b.f = Long.valueOf(-1);
        this.f2637b.f = (Long) this.c.invoke(null, new Object[]{this.f2636a.a()});
    }
}
