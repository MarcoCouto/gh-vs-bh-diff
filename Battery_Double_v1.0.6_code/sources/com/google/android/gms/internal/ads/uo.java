package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.xe.b;
import java.security.GeneralSecurityException;

final class uo implements tz<tr> {
    uo() {
    }

    /* access modifiers changed from: private */
    /* renamed from: d */
    public final tr a(aah aah) throws GeneralSecurityException {
        try {
            vz a2 = vz.a(aah);
            if (!(a2 instanceof vz)) {
                throw new GeneralSecurityException("expected AesGcmKey proto");
            }
            vz vzVar = a2;
            zo.a(vzVar.a(), 0);
            zo.a(vzVar.b().a());
            return new yg(vzVar.b().b());
        } catch (abv e) {
            throw new GeneralSecurityException("expected AesGcmKey proto");
        }
    }

    public final int a() {
        return 0;
    }

    public final /* synthetic */ Object a(acw acw) throws GeneralSecurityException {
        if (!(acw instanceof vz)) {
            throw new GeneralSecurityException("expected AesGcmKey proto");
        }
        vz vzVar = (vz) acw;
        zo.a(vzVar.a(), 0);
        zo.a(vzVar.b().a());
        return new yg(vzVar.b().b());
    }

    public final acw b(aah aah) throws GeneralSecurityException {
        try {
            return b((acw) wb.a(aah));
        } catch (abv e) {
            throw new GeneralSecurityException("expected serialized AesGcmKeyFormat proto", e);
        }
    }

    public final acw b(acw acw) throws GeneralSecurityException {
        if (!(acw instanceof wb)) {
            throw new GeneralSecurityException("expected AesGcmKeyFormat proto");
        }
        wb wbVar = (wb) acw;
        zo.a(wbVar.a());
        return vz.c().a(aah.a(zj.a(wbVar.a()))).a(0).c();
    }

    public final xe c(aah aah) throws GeneralSecurityException {
        return (xe) xe.d().a("type.googleapis.com/google.crypto.tink.AesGcmKey").a(((vz) b(aah)).h()).a(b.SYMMETRIC).c();
    }
}
