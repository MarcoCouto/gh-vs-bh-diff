package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.abj;

public interface abj<T extends abj<T>> extends Comparable<T> {
    int a();

    acx a(acx acx, acw acw);

    ade a(ade ade, ade ade2);

    aew b();

    afb c();

    boolean d();

    boolean e();
}
