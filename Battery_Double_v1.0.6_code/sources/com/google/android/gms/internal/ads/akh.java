package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.ads.internal.ax;
import com.google.android.gms.ads.internal.gmsg.ae;
import com.google.android.gms.ads.internal.gmsg.c;
import org.json.JSONObject;

@cm
public final class akh implements akq {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public final aju f2664a;

    /* renamed from: b reason: collision with root package name */
    private final Context f2665b;
    /* access modifiers changed from: private */
    public final c c;
    private bau d;
    /* access modifiers changed from: private */
    public boolean e;
    private final ae<bbe> f = new akm(this);
    private final ae<bbe> g = new akn(this);
    private final ae<bbe> h = new ako(this);
    private final ae<bbe> i = new akp(this);

    public akh(aju aju, bah bah, Context context) {
        this.f2664a = aju;
        this.f2665b = context;
        this.c = new c(this.f2665b);
        this.d = bah.b((ahh) null);
        this.d.a(new aki(this), new akj(this));
        String str = "Core JS tracking ad unit: ";
        String valueOf = String.valueOf(this.f2664a.f2645a.d());
        jm.b(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
    }

    /* access modifiers changed from: 0000 */
    public final void a(bbe bbe) {
        bbe.a("/updateActiveView", this.f);
        bbe.a("/untrackActiveViewUnit", this.g);
        bbe.a("/visibilityChanged", this.h);
        if (ax.B().a(this.f2665b)) {
            bbe.a("/logScionEvent", this.i);
        }
    }

    public final void a(JSONObject jSONObject, boolean z) {
        this.d.a(new akk(this, jSONObject), new oc());
    }

    public final boolean a() {
        return this.e;
    }

    public final void b() {
        this.d.a(new akl(this), new oc());
        this.d.c();
    }

    /* access modifiers changed from: 0000 */
    public final void b(bbe bbe) {
        bbe.b("/visibilityChanged", this.h);
        bbe.b("/untrackActiveViewUnit", this.g);
        bbe.b("/updateActiveView", this.f);
        if (ax.B().a(this.f2665b)) {
            bbe.b("/logScionEvent", this.i);
        }
    }
}
