package com.google.android.gms.internal.ads;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Locale;
import java.util.PriorityQueue;

@cm
public final class alu {

    /* renamed from: a reason: collision with root package name */
    private final int f2714a;

    /* renamed from: b reason: collision with root package name */
    private final int f2715b;
    private final int c;
    private final alt d = new aly();

    public alu(int i) {
        this.f2715b = i;
        this.f2714a = 6;
        this.c = 0;
    }

    private final String a(String str) {
        String[] split = str.split("\n");
        if (split.length == 0) {
            return "";
        }
        alw alw = new alw();
        PriorityQueue priorityQueue = new PriorityQueue(this.f2715b, new alv(this));
        for (String a2 : split) {
            String[] a3 = alx.a(a2, false);
            if (a3.length != 0) {
                ama.a(a3, this.f2715b, this.f2714a, priorityQueue);
            }
        }
        Iterator it = priorityQueue.iterator();
        while (it.hasNext()) {
            try {
                alw.a(this.d.a(((amb) it.next()).f2722b));
            } catch (IOException e) {
                jm.b("Error while writing hash to byteStream", e);
            }
        }
        return alw.toString();
    }

    public final String a(ArrayList<String> arrayList) {
        StringBuilder sb = new StringBuilder();
        ArrayList arrayList2 = arrayList;
        int size = arrayList2.size();
        int i = 0;
        while (i < size) {
            Object obj = arrayList2.get(i);
            i++;
            sb.append(((String) obj).toLowerCase(Locale.US));
            sb.append(10);
        }
        return a(sb.toString());
    }
}
