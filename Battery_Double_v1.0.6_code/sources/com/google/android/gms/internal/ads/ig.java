package com.google.android.gms.internal.ads;

import android.content.Context;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.ax;

@cm
public final class ig implements akv {

    /* renamed from: a reason: collision with root package name */
    private final Context f3384a;

    /* renamed from: b reason: collision with root package name */
    private final Object f3385b;
    private String c;
    private boolean d;

    public ig(Context context, String str) {
        if (context.getApplicationContext() != null) {
            context = context.getApplicationContext();
        }
        this.f3384a = context;
        this.c = str;
        this.d = false;
        this.f3385b = new Object();
    }

    public final void a(aku aku) {
        a(aku.f2678a);
    }

    public final void a(String str) {
        this.c = str;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
        return;
     */
    public final void a(boolean z) {
        if (ax.B().a(this.f3384a)) {
            synchronized (this.f3385b) {
                if (this.d != z) {
                    this.d = z;
                    if (!TextUtils.isEmpty(this.c)) {
                        if (this.d) {
                            ax.B().a(this.f3384a, this.c);
                        } else {
                            ax.B().b(this.f3384a, this.c);
                        }
                    }
                }
            }
        }
    }
}
