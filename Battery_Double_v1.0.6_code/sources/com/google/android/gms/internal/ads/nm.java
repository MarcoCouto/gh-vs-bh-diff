package com.google.android.gms.internal.ads;

import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;

@cm
final class nm<T> implements nn<T> {

    /* renamed from: a reason: collision with root package name */
    private final T f3549a;

    /* renamed from: b reason: collision with root package name */
    private final np f3550b = new np();

    nm(T t) {
        this.f3549a = t;
        this.f3550b.a();
    }

    public final void a(Runnable runnable, Executor executor) {
        this.f3550b.a(runnable, executor);
    }

    public final boolean cancel(boolean z) {
        return false;
    }

    public final T get() {
        return this.f3549a;
    }

    public final T get(long j, TimeUnit timeUnit) {
        return this.f3549a;
    }

    public final boolean isCancelled() {
        return false;
    }

    public final boolean isDone() {
        return true;
    }
}
