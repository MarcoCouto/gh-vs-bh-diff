package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.Bundle;
import android.os.RemoteException;
import com.google.android.gms.ads.a;
import com.google.android.gms.ads.a.e;
import com.google.android.gms.ads.f;
import com.google.android.gms.ads.reward.b;
import com.google.android.gms.ads.reward.c;

@cm
public final class arf {

    /* renamed from: a reason: collision with root package name */
    private final bcq f2851a;

    /* renamed from: b reason: collision with root package name */
    private final Context f2852b;
    private final aos c;
    private a d;
    private aoj e;
    private apv f;
    private String g;
    private c h;
    private com.google.android.gms.ads.a.a i;
    private com.google.android.gms.ads.a.c j;
    private f k;
    private b l;
    private boolean m;
    private boolean n;

    public arf(Context context) {
        this(context, aos.f2813a, null);
    }

    private arf(Context context, aos aos, e eVar) {
        this.f2851a = new bcq();
        this.f2852b = context;
        this.c = aos;
    }

    private final void b(String str) {
        if (this.f == null) {
            throw new IllegalStateException(new StringBuilder(String.valueOf(str).length() + 63).append("The ad unit ID must be set on InterstitialAd before ").append(str).append(" is called.").toString());
        }
    }

    public final void a(a aVar) {
        try {
            this.d = aVar;
            if (this.f != null) {
                this.f.a((apk) aVar != null ? new aol(aVar) : null);
            }
        } catch (RemoteException e2) {
            ms.d("#008 Must be called on the main UI thread.", e2);
        }
    }

    public final void a(b bVar) {
        try {
            this.l = bVar;
            if (this.f != null) {
                this.f.a((gn) bVar != null ? new gs(bVar) : null);
            }
        } catch (RemoteException e2) {
            ms.d("#008 Must be called on the main UI thread.", e2);
        }
    }

    public final void a(c cVar) {
        try {
            this.h = cVar;
            if (this.f != null) {
                this.f.a((aqa) cVar != null ? new aoo(cVar) : null);
            }
        } catch (RemoteException e2) {
            ms.d("#008 Must be called on the main UI thread.", e2);
        }
    }

    public final void a(aoj aoj) {
        try {
            this.e = aoj;
            if (this.f != null) {
                this.f.a((aph) aoj != null ? new aok(aoj) : null);
            }
        } catch (RemoteException e2) {
            ms.d("#008 Must be called on the main UI thread.", e2);
        }
    }

    public final void a(ara ara) {
        try {
            if (this.f == null) {
                String str = "loadAd";
                if (this.g == null) {
                    b(str);
                }
                aot aot = this.m ? aot.a() : new aot();
                aox b2 = ape.b();
                Context context = this.f2852b;
                this.f = (apv) aox.a(context, false, (a<T>) new apa<T>(b2, context, aot, this.g, this.f2851a));
                if (this.d != null) {
                    this.f.a((apk) new aol(this.d));
                }
                if (this.e != null) {
                    this.f.a((aph) new aok(this.e));
                }
                if (this.h != null) {
                    this.f.a((aqa) new aoo(this.h));
                }
                if (this.i != null) {
                    this.f.a((aqe) new aov(this.i));
                }
                if (this.j != null) {
                    this.f.a((atc) new atf(this.j));
                }
                if (this.k != null) {
                    this.f.a((aqk) this.k.a());
                }
                if (this.l != null) {
                    this.f.a((gn) new gs(this.l));
                }
                this.f.c(this.n);
            }
            if (this.f.b(aos.a(this.f2852b, ara))) {
                this.f2851a.a(ara.j());
            }
        } catch (RemoteException e2) {
            ms.d("#008 Must be called on the main UI thread.", e2);
        }
    }

    public final void a(String str) {
        if (this.g != null) {
            throw new IllegalStateException("The ad unit ID can only be set once on InterstitialAd.");
        }
        this.g = str;
    }

    public final void a(boolean z) {
        this.m = true;
    }

    public final boolean a() {
        try {
            if (this.f == null) {
                return false;
            }
            return this.f.m();
        } catch (RemoteException e2) {
            ms.d("#008 Must be called on the main UI thread.", e2);
            return false;
        }
    }

    public final Bundle b() {
        try {
            if (this.f != null) {
                return this.f.q();
            }
        } catch (RemoteException e2) {
            ms.d("#008 Must be called on the main UI thread.", e2);
        }
        return new Bundle();
    }

    public final void b(boolean z) {
        try {
            this.n = z;
            if (this.f != null) {
                this.f.c(z);
            }
        } catch (RemoteException e2) {
            ms.d("#008 Must be called on the main UI thread.", e2);
        }
    }

    public final void c() {
        try {
            b("show");
            this.f.I();
        } catch (RemoteException e2) {
            ms.d("#008 Must be called on the main UI thread.", e2);
        }
    }
}
