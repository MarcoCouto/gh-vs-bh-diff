package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.abj;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

final class abh<FieldDescriptorType extends abj<FieldDescriptorType>> {
    private static final abh d = new abh(true);

    /* renamed from: a reason: collision with root package name */
    private final ads<FieldDescriptorType, Object> f2420a = ads.a(16);

    /* renamed from: b reason: collision with root package name */
    private boolean f2421b;
    private boolean c = false;

    private abh() {
    }

    private abh(boolean z) {
        c();
    }

    static int a(aew aew, int i, Object obj) {
        int i2;
        int e = aav.e(i);
        if (aew == aew.GROUP) {
            abr.a((acw) obj);
            i2 = e << 1;
        } else {
            i2 = e;
        }
        return i2 + b(aew, obj);
    }

    public static <T extends abj<T>> abh<T> a() {
        return d;
    }

    private final Object a(FieldDescriptorType fielddescriptortype) {
        Object obj = this.f2420a.get(fielddescriptortype);
        return obj instanceof aby ? aby.a() : obj;
    }

    private static Object a(Object obj) {
        if (obj instanceof ade) {
            return ((ade) obj).a();
        }
        if (!(obj instanceof byte[])) {
            return obj;
        }
        byte[] bArr = (byte[]) obj;
        byte[] bArr2 = new byte[bArr.length];
        System.arraycopy(bArr, 0, bArr2, 0, bArr.length);
        return bArr2;
    }

    static void a(aav aav, aew aew, int i, Object obj) throws IOException {
        if (aew == aew.GROUP) {
            abr.a((acw) obj);
            acw acw = (acw) obj;
            aav.a(i, 3);
            acw.a(aav);
            aav.a(i, 4);
            return;
        }
        aav.a(i, aew.b());
        switch (abi.f2423b[aew.ordinal()]) {
            case 1:
                aav.a(((Double) obj).doubleValue());
                return;
            case 2:
                aav.a(((Float) obj).floatValue());
                return;
            case 3:
                aav.a(((Long) obj).longValue());
                return;
            case 4:
                aav.a(((Long) obj).longValue());
                return;
            case 5:
                aav.a(((Integer) obj).intValue());
                return;
            case 6:
                aav.c(((Long) obj).longValue());
                return;
            case 7:
                aav.d(((Integer) obj).intValue());
                return;
            case 8:
                aav.a(((Boolean) obj).booleanValue());
                return;
            case 9:
                ((acw) obj).a(aav);
                return;
            case 10:
                aav.a((acw) obj);
                return;
            case 11:
                if (obj instanceof aah) {
                    aav.a((aah) obj);
                    return;
                } else {
                    aav.a((String) obj);
                    return;
                }
            case 12:
                if (obj instanceof aah) {
                    aav.a((aah) obj);
                    return;
                }
                byte[] bArr = (byte[]) obj;
                aav.c(bArr, 0, bArr.length);
                return;
            case 13:
                aav.b(((Integer) obj).intValue());
                return;
            case 14:
                aav.d(((Integer) obj).intValue());
                return;
            case 15:
                aav.c(((Long) obj).longValue());
                return;
            case 16:
                aav.c(((Integer) obj).intValue());
                return;
            case 17:
                aav.b(((Long) obj).longValue());
                return;
            case 18:
                if (obj instanceof abs) {
                    aav.a(((abs) obj).a());
                    return;
                } else {
                    aav.a(((Integer) obj).intValue());
                    return;
                }
            default:
                return;
        }
    }

    /* JADX WARNING: type inference failed for: r1v0, types: [java.lang.Object] */
    /* JADX WARNING: type inference failed for: r1v1 */
    /* JADX WARNING: Multi-variable type inference failed */
    private final void a(FieldDescriptorType fielddescriptortype, Object obj) {
        ArrayList arrayList;
        if (!fielddescriptortype.d()) {
            a(fielddescriptortype.b(), obj);
            arrayList = obj;
        } else if (!(obj instanceof List)) {
            throw new IllegalArgumentException("Wrong object type used with protocol message reflection.");
        } else {
            arrayList = new ArrayList();
            arrayList.addAll((List) obj);
            ArrayList arrayList2 = arrayList;
            int size = arrayList2.size();
            int i = 0;
            while (i < size) {
                Object obj2 = arrayList2.get(i);
                i++;
                a(fielddescriptortype.b(), obj2);
            }
        }
        if (arrayList instanceof aby) {
            this.c = true;
        }
        this.f2420a.put(fielddescriptortype, arrayList);
    }

    private static void a(aew aew, Object obj) {
        boolean z = false;
        abr.a(obj);
        switch (abi.f2422a[aew.a().ordinal()]) {
            case 1:
                z = obj instanceof Integer;
                break;
            case 2:
                z = obj instanceof Long;
                break;
            case 3:
                z = obj instanceof Float;
                break;
            case 4:
                z = obj instanceof Double;
                break;
            case 5:
                z = obj instanceof Boolean;
                break;
            case 6:
                z = obj instanceof String;
                break;
            case 7:
                if ((obj instanceof aah) || (obj instanceof byte[])) {
                    z = true;
                    break;
                }
            case 8:
                if ((obj instanceof Integer) || (obj instanceof abs)) {
                    z = true;
                    break;
                }
            case 9:
                if ((obj instanceof acw) || (obj instanceof aby)) {
                    z = true;
                    break;
                }
        }
        if (!z) {
            throw new IllegalArgumentException("Wrong object type used with protocol message reflection.");
        }
    }

    private static boolean a(Entry<FieldDescriptorType, Object> entry) {
        abj abj = (abj) entry.getKey();
        if (abj.c() == afb.MESSAGE) {
            if (abj.d()) {
                for (acw k : (List) entry.getValue()) {
                    if (!k.k()) {
                        return false;
                    }
                }
            } else {
                Object value = entry.getValue();
                if (value instanceof acw) {
                    if (!((acw) value).k()) {
                        return false;
                    }
                } else if (value instanceof aby) {
                    return true;
                } else {
                    throw new IllegalArgumentException("Wrong object type used with protocol message reflection.");
                }
            }
        }
        return true;
    }

    private static int b(abj<?> abj, Object obj) {
        int i = 0;
        aew b2 = abj.b();
        int a2 = abj.a();
        if (!abj.d()) {
            return a(b2, a2, obj);
        }
        if (abj.e()) {
            for (Object b3 : (List) obj) {
                i += b(b2, b3);
            }
            return aav.l(i) + aav.e(a2) + i;
        }
        for (Object a3 : (List) obj) {
            i += a(b2, a2, a3);
        }
        return i;
    }

    private static int b(aew aew, Object obj) {
        switch (abi.f2423b[aew.ordinal()]) {
            case 1:
                return aav.b(((Double) obj).doubleValue());
            case 2:
                return aav.b(((Float) obj).floatValue());
            case 3:
                return aav.d(((Long) obj).longValue());
            case 4:
                return aav.e(((Long) obj).longValue());
            case 5:
                return aav.f(((Integer) obj).intValue());
            case 6:
                return aav.g(((Long) obj).longValue());
            case 7:
                return aav.i(((Integer) obj).intValue());
            case 8:
                return aav.b(((Boolean) obj).booleanValue());
            case 9:
                return aav.c((acw) obj);
            case 10:
                return obj instanceof aby ? aav.a((acd) (aby) obj) : aav.b((acw) obj);
            case 11:
                return obj instanceof aah ? aav.b((aah) obj) : aav.b((String) obj);
            case 12:
                return obj instanceof aah ? aav.b((aah) obj) : aav.b((byte[]) obj);
            case 13:
                return aav.g(((Integer) obj).intValue());
            case 14:
                return aav.j(((Integer) obj).intValue());
            case 15:
                return aav.h(((Long) obj).longValue());
            case 16:
                return aav.h(((Integer) obj).intValue());
            case 17:
                return aav.f(((Long) obj).longValue());
            case 18:
                return obj instanceof abs ? aav.k(((abs) obj).a()) : aav.k(((Integer) obj).intValue());
            default:
                throw new RuntimeException("There is no way to get here, but the compiler thinks otherwise.");
        }
    }

    private final void b(Entry<FieldDescriptorType, Object> entry) {
        abj abj = (abj) entry.getKey();
        Object value = entry.getValue();
        if (value instanceof aby) {
            value = aby.a();
        }
        if (abj.d()) {
            Object a2 = a((FieldDescriptorType) abj);
            if (a2 == null) {
                a2 = new ArrayList();
            }
            for (Object a3 : (List) value) {
                ((List) a2).add(a(a3));
            }
            this.f2420a.put(abj, a2);
        } else if (abj.c() == afb.MESSAGE) {
            Object a4 = a((FieldDescriptorType) abj);
            if (a4 == null) {
                this.f2420a.put(abj, a(value));
            } else {
                this.f2420a.put(abj, a4 instanceof ade ? abj.a((ade) a4, (ade) value) : abj.a(((acw) a4).n(), (acw) value).e());
            }
        } else {
            this.f2420a.put(abj, a(value));
        }
    }

    private static int c(Entry<FieldDescriptorType, Object> entry) {
        abj abj = (abj) entry.getKey();
        Object value = entry.getValue();
        return (abj.c() != afb.MESSAGE || abj.d() || abj.e()) ? b(abj, value) : value instanceof aby ? aav.b(((abj) entry.getKey()).a(), (acd) (aby) value) : aav.b(((abj) entry.getKey()).a(), (acw) value);
    }

    public final void a(abh<FieldDescriptorType> abh) {
        for (int i = 0; i < abh.f2420a.c(); i++) {
            b(abh.f2420a.b(i));
        }
        for (Entry b2 : abh.f2420a.d()) {
            b(b2);
        }
    }

    /* access modifiers changed from: 0000 */
    public final boolean b() {
        return this.f2420a.isEmpty();
    }

    public final void c() {
        if (!this.f2421b) {
            this.f2420a.a();
            this.f2421b = true;
        }
    }

    public final /* synthetic */ Object clone() throws CloneNotSupportedException {
        abh abh = new abh();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.f2420a.c()) {
                break;
            }
            Entry b2 = this.f2420a.b(i2);
            abh.a((FieldDescriptorType) (abj) b2.getKey(), b2.getValue());
            i = i2 + 1;
        }
        for (Entry entry : this.f2420a.d()) {
            abh.a((FieldDescriptorType) (abj) entry.getKey(), entry.getValue());
        }
        abh.c = this.c;
        return abh;
    }

    public final boolean d() {
        return this.f2421b;
    }

    public final Iterator<Entry<FieldDescriptorType, Object>> e() {
        return this.c ? new acc(this.f2420a.entrySet().iterator()) : this.f2420a.entrySet().iterator();
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof abh)) {
            return false;
        }
        return this.f2420a.equals(((abh) obj).f2420a);
    }

    /* access modifiers changed from: 0000 */
    public final Iterator<Entry<FieldDescriptorType, Object>> f() {
        return this.c ? new acc(this.f2420a.e().iterator()) : this.f2420a.e().iterator();
    }

    public final boolean g() {
        for (int i = 0; i < this.f2420a.c(); i++) {
            if (!a(this.f2420a.b(i))) {
                return false;
            }
        }
        for (Entry a2 : this.f2420a.d()) {
            if (!a(a2)) {
                return false;
            }
        }
        return true;
    }

    public final int h() {
        int i = 0;
        for (int i2 = 0; i2 < this.f2420a.c(); i2++) {
            Entry b2 = this.f2420a.b(i2);
            i += b((abj) b2.getKey(), b2.getValue());
        }
        for (Entry entry : this.f2420a.d()) {
            i += b((abj) entry.getKey(), entry.getValue());
        }
        return i;
    }

    public final int hashCode() {
        return this.f2420a.hashCode();
    }

    public final int i() {
        int i = 0;
        for (int i2 = 0; i2 < this.f2420a.c(); i2++) {
            i += c(this.f2420a.b(i2));
        }
        for (Entry c2 : this.f2420a.d()) {
            i += c(c2);
        }
        return i;
    }
}
