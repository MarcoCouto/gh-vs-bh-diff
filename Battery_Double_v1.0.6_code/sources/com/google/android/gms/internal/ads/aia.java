package com.google.android.gms.internal.ads;

import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

final class aia implements ThreadFactory {

    /* renamed from: a reason: collision with root package name */
    private final ThreadFactory f2613a = Executors.defaultThreadFactory();

    aia() {
    }

    public final Thread newThread(Runnable runnable) {
        Thread newThread = this.f2613a.newThread(runnable);
        newThread.setName(String.valueOf(newThread.getName()).concat(":"));
        return newThread;
    }
}
