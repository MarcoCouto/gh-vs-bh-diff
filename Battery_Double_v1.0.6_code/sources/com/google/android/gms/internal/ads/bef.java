package com.google.android.gms.internal.ads;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;

public final class bef extends ajk implements bee {
    bef(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.mediation.client.rtb.IInterstitialCallback");
    }

    public final void a(String str) throws RemoteException {
        Parcel r_ = r_();
        r_.writeString(str);
        b(3, r_);
    }
}
