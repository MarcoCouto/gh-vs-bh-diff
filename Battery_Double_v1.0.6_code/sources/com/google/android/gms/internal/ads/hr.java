package com.google.android.gms.internal.ads;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import com.google.android.gms.ads.internal.ax;
import com.google.android.gms.common.b.c;
import com.google.android.gms.common.h;
import com.google.android.gms.common.internal.aa;
import com.google.android.gms.common.util.n;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@cm
public final class hr implements ic {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public static List<Future<Void>> f3371a = Collections.synchronizedList(new ArrayList());

    /* renamed from: b reason: collision with root package name */
    private static ScheduledExecutorService f3372b = Executors.newSingleThreadScheduledExecutor();
    /* access modifiers changed from: private */
    public final afr c;
    private final LinkedHashMap<String, afz> d;
    private final List<String> e = new ArrayList();
    private final List<String> f = new ArrayList();
    private final Context g;
    private final ie h;
    private boolean i;
    private final hz j;
    private final Cif k;
    /* access modifiers changed from: private */
    public final Object l = new Object();
    private HashSet<String> m = new HashSet<>();
    private boolean n = false;
    private boolean o = false;
    private boolean p = false;

    public hr(Context context, mu muVar, hz hzVar, String str, ie ieVar) {
        aa.a(hzVar, (Object) "SafeBrowsing config is not present.");
        if (context.getApplicationContext() != null) {
            context = context.getApplicationContext();
        }
        this.g = context;
        this.d = new LinkedHashMap<>();
        this.h = ieVar;
        this.j = hzVar;
        for (String lowerCase : this.j.e) {
            this.m.add(lowerCase.toLowerCase(Locale.ENGLISH));
        }
        this.m.remove("cookie".toLowerCase(Locale.ENGLISH));
        afr afr = new afr();
        afr.f2536a = Integer.valueOf(8);
        afr.f2537b = str;
        afr.c = str;
        afr.d = new afs();
        afr.d.f2538a = this.j.f3379a;
        aga aga = new aga();
        aga.f2553a = muVar.f3528a;
        aga.c = Boolean.valueOf(c.b(this.g).a());
        long b2 = (long) h.b().b(this.g);
        if (b2 > 0) {
            aga.f2554b = Long.valueOf(b2);
        }
        afr.h = aga;
        this.c = afr;
        this.k = new Cif(this.g, this.j.h, this);
    }

    static final /* synthetic */ Void d(String str) {
        return null;
    }

    private final afz e(String str) {
        afz afz;
        synchronized (this.l) {
            afz = (afz) this.d.get(str);
        }
        return afz;
    }

    private final nn<Void> f() {
        nn<Void> a2;
        afz[] afzArr;
        boolean z = true;
        if ((!this.i || !this.j.g) && ((!this.p || !this.j.f) && (this.i || !this.j.d))) {
            z = false;
        }
        if (!z) {
            return nc.a(null);
        }
        synchronized (this.l) {
            this.c.e = new afz[this.d.size()];
            this.d.values().toArray(this.c.e);
            this.c.i = (String[]) this.e.toArray(new String[0]);
            this.c.j = (String[]) this.f.toArray(new String[0]);
            if (ib.a()) {
                String str = this.c.f2537b;
                String str2 = this.c.f;
                StringBuilder sb = new StringBuilder(new StringBuilder(String.valueOf(str).length() + 53 + String.valueOf(str2).length()).append("Sending SB report\n  url: ").append(str).append("\n  clickUrl: ").append(str2).append("\n  resources: \n").toString());
                for (afz afz : this.c.e) {
                    sb.append("    [");
                    sb.append(afz.e.length);
                    sb.append("] ");
                    sb.append(afz.f2552b);
                }
                ib.a(sb.toString());
            }
            nn a3 = new lf(this.g).a(1, this.j.f3380b, null, afn.a((afn) this.c));
            if (ib.a()) {
                a3.a(new hw(this), jt.f3436a);
            }
            a2 = nc.a(a3, ht.f3374a, nt.f3559b);
        }
        return a2;
    }

    public final hz a() {
        return this.j;
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ nn a(Map map) throws Exception {
        if (map != null) {
            for (String str : map.keySet()) {
                JSONArray optJSONArray = new JSONObject((String) map.get(str)).optJSONArray("matches");
                if (optJSONArray != null) {
                    synchronized (this.l) {
                        int length = optJSONArray.length();
                        afz e2 = e(str);
                        if (e2 == null) {
                            String str2 = "Cannot find the corresponding resource object for ";
                            String valueOf = String.valueOf(str);
                            ib.a(valueOf.length() != 0 ? str2.concat(valueOf) : new String(str2));
                        } else {
                            e2.e = new String[length];
                            for (int i2 = 0; i2 < length; i2++) {
                                e2.e[i2] = optJSONArray.getJSONObject(i2).getString("threat_type");
                            }
                            this.i = (length > 0) | this.i;
                        }
                    }
                }
            }
        }
        try {
            if (this.i) {
                synchronized (this.l) {
                    this.c.f2536a = Integer.valueOf(9);
                }
            }
            return f();
        } catch (JSONException e3) {
            JSONException jSONException = e3;
            String str3 = "Failed to get SafeBrowsing metadata";
            if (((Boolean) ape.f().a(asi.cB)).booleanValue()) {
                jm.a(str3, jSONException);
            }
            return nc.a((Throwable) new Exception("Safebrowsing report transmission failed."));
        }
    }

    public final void a(View view) {
        if (this.j.c && !this.o) {
            ax.e();
            Bitmap b2 = jv.b(view);
            if (b2 == null) {
                ib.a("Failed to capture the webview bitmap.");
                return;
            }
            this.o = true;
            jv.a((Runnable) new hu(this, b2));
        }
    }

    public final void a(String str) {
        synchronized (this.l) {
            this.c.f = str;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:45:?, code lost:
        return;
     */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    public final void a(String str, Map<String, String> map, int i2) {
        synchronized (this.l) {
            if (i2 == 3) {
                this.p = true;
            }
            if (!this.d.containsKey(str)) {
                afz afz = new afz();
                afz.d = Integer.valueOf(i2);
                afz.f2551a = Integer.valueOf(this.d.size());
                afz.f2552b = str;
                afz.c = new afu();
                if (this.m.size() > 0 && map != null) {
                    ArrayList arrayList = new ArrayList();
                    for (Entry entry : map.entrySet()) {
                        try {
                            String str2 = entry.getKey() != null ? (String) entry.getKey() : "";
                            String str3 = entry.getValue() != null ? (String) entry.getValue() : "";
                            if (this.m.contains(str2.toLowerCase(Locale.ENGLISH))) {
                                aft aft = new aft();
                                aft.f2539a = str2.getBytes("UTF-8");
                                aft.f2540b = str3.getBytes("UTF-8");
                                arrayList.add(aft);
                            }
                        } catch (UnsupportedEncodingException e2) {
                            ib.a("Cannot convert string to bytes, skip header.");
                        }
                    }
                    aft[] aftArr = new aft[arrayList.size()];
                    arrayList.toArray(aftArr);
                    afz.c.f2541a = aftArr;
                }
                this.d.put(str, afz);
            } else if (i2 == 3) {
                ((afz) this.d.get(str)).d = Integer.valueOf(i2);
            }
        }
    }

    public final String[] a(String[] strArr) {
        return (String[]) this.k.a(strArr).toArray(new String[0]);
    }

    /* access modifiers changed from: 0000 */
    public final void b(String str) {
        synchronized (this.l) {
            this.e.add(str);
        }
    }

    public final boolean b() {
        return n.g() && this.j.c && !this.o;
    }

    public final void c() {
        this.n = true;
    }

    /* access modifiers changed from: 0000 */
    public final void c(String str) {
        synchronized (this.l) {
            this.f.add(str);
        }
    }

    public final void d() {
        synchronized (this.l) {
            nn a2 = nc.a(this.h.a(this.g, this.d.keySet()), (mx<? super A, ? extends B>) new hs<Object,Object>(this), nt.f3559b);
            nn a3 = nc.a(a2, 10, TimeUnit.SECONDS, f3372b);
            nc.a(a2, (mz<V>) new hv<V>(this, a3), nt.f3559b);
            f3371a.add(a3);
        }
    }
}
