package com.google.android.gms.internal.ads;

import java.security.GeneralSecurityException;
import java.security.interfaces.ECPrivateKey;

public final class yl implements tx {

    /* renamed from: a reason: collision with root package name */
    private static final byte[] f3793a = new byte[0];

    /* renamed from: b reason: collision with root package name */
    private final ECPrivateKey f3794b;
    private final yn c;
    private final String d;
    private final byte[] e;
    private final yt f;
    private final yk g;

    public yl(ECPrivateKey eCPrivateKey, byte[] bArr, String str, yt ytVar, yk ykVar) throws GeneralSecurityException {
        this.f3794b = eCPrivateKey;
        this.c = new yn(eCPrivateKey);
        this.e = bArr;
        this.d = str;
        this.f = ytVar;
        this.g = ykVar;
    }
}
