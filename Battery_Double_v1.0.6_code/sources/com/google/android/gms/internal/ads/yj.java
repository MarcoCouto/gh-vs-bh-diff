package com.google.android.gms.internal.ads;

import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;

public final class yj extends zm {
    public yj(byte[] bArr) throws InvalidKeyException {
        super(bArr);
    }

    /* access modifiers changed from: 0000 */
    public final zl a(byte[] bArr, int i) throws InvalidKeyException {
        return new yi(bArr, i);
    }

    public final /* bridge */ /* synthetic */ byte[] a(byte[] bArr, byte[] bArr2) throws GeneralSecurityException {
        return super.a(bArr, bArr2);
    }
}
