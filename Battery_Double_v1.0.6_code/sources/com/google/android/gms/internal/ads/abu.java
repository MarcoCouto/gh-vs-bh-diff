package com.google.android.gms.internal.ads;

import java.util.List;
import java.util.RandomAccess;

public interface abu<E> extends List<E>, RandomAccess {
    abu<E> a(int i);

    boolean a();

    void b();
}
