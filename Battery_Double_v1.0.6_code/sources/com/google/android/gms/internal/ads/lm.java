package com.google.android.gms.internal.ads;

import android.os.SystemClock;
import android.text.TextUtils;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

public final class lm implements zy {

    /* renamed from: a reason: collision with root package name */
    private final Map<String, mn> f3485a;

    /* renamed from: b reason: collision with root package name */
    private long f3486b;
    private final File c;
    private final int d;

    public lm(File file) {
        this(file, 5242880);
    }

    private lm(File file, int i) {
        this.f3485a = new LinkedHashMap(16, 0.75f, true);
        this.f3486b = 0;
        this.c = file;
        this.d = 5242880;
    }

    static int a(InputStream inputStream) throws IOException {
        return c(inputStream) | 0 | (c(inputStream) << 8) | (c(inputStream) << 16) | (c(inputStream) << 24);
    }

    private static InputStream a(File file) throws FileNotFoundException {
        return new FileInputStream(file);
    }

    static String a(no noVar) throws IOException {
        return new String(a(noVar, b((InputStream) noVar)), "UTF-8");
    }

    static void a(OutputStream outputStream, int i) throws IOException {
        outputStream.write(i & 255);
        outputStream.write((i >> 8) & 255);
        outputStream.write((i >> 16) & 255);
        outputStream.write(i >>> 24);
    }

    static void a(OutputStream outputStream, long j) throws IOException {
        outputStream.write((byte) ((int) j));
        outputStream.write((byte) ((int) (j >>> 8)));
        outputStream.write((byte) ((int) (j >>> 16)));
        outputStream.write((byte) ((int) (j >>> 24)));
        outputStream.write((byte) ((int) (j >>> 32)));
        outputStream.write((byte) ((int) (j >>> 40)));
        outputStream.write((byte) ((int) (j >>> 48)));
        outputStream.write((byte) ((int) (j >>> 56)));
    }

    static void a(OutputStream outputStream, String str) throws IOException {
        byte[] bytes = str.getBytes("UTF-8");
        a(outputStream, (long) bytes.length);
        outputStream.write(bytes, 0, bytes.length);
    }

    private final void a(String str, mn mnVar) {
        if (!this.f3485a.containsKey(str)) {
            this.f3486b += mnVar.f3521a;
        } else {
            mn mnVar2 = (mn) this.f3485a.get(str);
            this.f3486b = (mnVar.f3521a - mnVar2.f3521a) + this.f3486b;
        }
        this.f3485a.put(str, mnVar);
    }

    private static byte[] a(no noVar, long j) throws IOException {
        long a2 = noVar.a();
        if (j < 0 || j > a2 || ((long) ((int) j)) != j) {
            throw new IOException("streamToBytes length=" + j + ", maxLength=" + a2);
        }
        byte[] bArr = new byte[((int) j)];
        new DataInputStream(noVar).readFully(bArr);
        return bArr;
    }

    static long b(InputStream inputStream) throws IOException {
        return 0 | (((long) c(inputStream)) & 255) | ((((long) c(inputStream)) & 255) << 8) | ((((long) c(inputStream)) & 255) << 16) | ((((long) c(inputStream)) & 255) << 24) | ((((long) c(inputStream)) & 255) << 32) | ((((long) c(inputStream)) & 255) << 40) | ((((long) c(inputStream)) & 255) << 48) | ((((long) c(inputStream)) & 255) << 56);
    }

    static List<aqd> b(no noVar) throws IOException {
        int a2 = a((InputStream) noVar);
        List<aqd> arrayList = a2 == 0 ? Collections.emptyList() : new ArrayList<>(a2);
        for (int i = 0; i < a2; i++) {
            arrayList.add(new aqd(a(noVar).intern(), a(noVar).intern()));
        }
        return arrayList;
    }

    private final synchronized void b(String str) {
        boolean delete = d(str).delete();
        e(str);
        if (!delete) {
            eg.b("Could not delete cache entry for key=%s, filename=%s", str, c(str));
        }
    }

    private static int c(InputStream inputStream) throws IOException {
        int read = inputStream.read();
        if (read != -1) {
            return read;
        }
        throw new EOFException();
    }

    private static String c(String str) {
        int length = str.length() / 2;
        String valueOf = String.valueOf(String.valueOf(str.substring(0, length).hashCode()));
        String valueOf2 = String.valueOf(String.valueOf(str.substring(length).hashCode()));
        return valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf);
    }

    private final File d(String str) {
        return new File(this.c, c(str));
    }

    private final void e(String str) {
        mn mnVar = (mn) this.f3485a.remove(str);
        if (mnVar != null) {
            this.f3486b -= mnVar.f3521a;
        }
    }

    public final synchronized agy a(String str) {
        agy agy;
        no noVar;
        mn mnVar = (mn) this.f3485a.get(str);
        if (mnVar == null) {
            agy = null;
        } else {
            File d2 = d(str);
            try {
                noVar = new no(new BufferedInputStream(a(d2)), d2.length());
                mn a2 = mn.a(noVar);
                if (!TextUtils.equals(str, a2.f3522b)) {
                    eg.b("%s: key=%s, found=%s", d2.getAbsolutePath(), str, a2.f3522b);
                    e(str);
                    noVar.close();
                    agy = null;
                } else {
                    byte[] a3 = a(noVar, noVar.a());
                    agy agy2 = new agy();
                    agy2.f2576a = a3;
                    agy2.f2577b = mnVar.c;
                    agy2.c = mnVar.d;
                    agy2.d = mnVar.e;
                    agy2.e = mnVar.f;
                    agy2.f = mnVar.g;
                    List<aqd> list = mnVar.h;
                    TreeMap treeMap = new TreeMap(String.CASE_INSENSITIVE_ORDER);
                    for (aqd aqd : list) {
                        treeMap.put(aqd.a(), aqd.b());
                    }
                    agy2.g = treeMap;
                    agy2.h = Collections.unmodifiableList(mnVar.h);
                    noVar.close();
                    agy = agy2;
                }
            } catch (IOException e) {
                eg.b("%s: %s", d2.getAbsolutePath(), e.toString());
                b(str);
                agy = null;
            } catch (Throwable th) {
                noVar.close();
                throw th;
            }
        }
        return agy;
    }

    public final synchronized void a() {
        no noVar;
        if (this.c.exists()) {
            File[] listFiles = this.c.listFiles();
            if (listFiles != null) {
                for (File file : listFiles) {
                    try {
                        long length = file.length();
                        noVar = new no(new BufferedInputStream(a(file)), length);
                        mn a2 = mn.a(noVar);
                        a2.f3521a = length;
                        a(a2.f3522b, a2);
                        noVar.close();
                    } catch (IOException e) {
                        file.delete();
                    } catch (Throwable th) {
                        noVar.close();
                        throw th;
                    }
                }
            }
        } else if (!this.c.mkdirs()) {
            eg.c("Unable to create cache dir %s", this.c.getAbsolutePath());
        }
    }

    public final synchronized void a(String str, agy agy) {
        int i;
        int i2 = 0;
        synchronized (this) {
            int length = agy.f2576a.length;
            if (this.f3486b + ((long) length) >= ((long) this.d)) {
                if (eg.f3276a) {
                    eg.a("Pruning old cache entries.", new Object[0]);
                }
                long j = this.f3486b;
                long elapsedRealtime = SystemClock.elapsedRealtime();
                Iterator it = this.f3485a.entrySet().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        i = i2;
                        break;
                    }
                    mn mnVar = (mn) ((Entry) it.next()).getValue();
                    if (d(mnVar.f3522b).delete()) {
                        this.f3486b -= mnVar.f3521a;
                    } else {
                        eg.b("Could not delete cache entry for key=%s, filename=%s", mnVar.f3522b, c(mnVar.f3522b));
                    }
                    it.remove();
                    i = i2 + 1;
                    if (((float) (this.f3486b + ((long) length))) < ((float) this.d) * 0.9f) {
                        break;
                    }
                    i2 = i;
                }
                if (eg.f3276a) {
                    eg.a("pruned %d files, %d bytes, %d ms", Integer.valueOf(i), Long.valueOf(this.f3486b - j), Long.valueOf(SystemClock.elapsedRealtime() - elapsedRealtime));
                }
            }
            File d2 = d(str);
            try {
                BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(d2));
                mn mnVar2 = new mn(str, agy);
                if (!mnVar2.a((OutputStream) bufferedOutputStream)) {
                    bufferedOutputStream.close();
                    eg.b("Failed to write header for %s", d2.getAbsolutePath());
                    throw new IOException();
                }
                bufferedOutputStream.write(agy.f2576a);
                bufferedOutputStream.close();
                a(str, mnVar2);
            } catch (IOException e) {
                if (!d2.delete()) {
                    eg.b("Could not clean up file %s", d2.getAbsolutePath());
                }
            }
        }
    }
}
