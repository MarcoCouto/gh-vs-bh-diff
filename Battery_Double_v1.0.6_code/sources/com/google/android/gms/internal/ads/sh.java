package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.ads.internal.aq;
import com.google.android.gms.ads.internal.ax;
import com.google.android.gms.ads.internal.bu;

@cm
public final class sh {
    public static qn a(Context context, sb sbVar, String str, boolean z, boolean z2, ahh ahh, mu muVar, asv asv, aq aqVar, bu buVar, amw amw) throws qy {
        try {
            return (qn) ly.a(new si(context, sbVar, str, z, z2, ahh, muVar, asv, aqVar, buVar, amw));
        } catch (Throwable th) {
            ax.i().a(th, "AdWebViewFactory.newAdWebView2");
            throw new qy("Webview initialization failed.", th);
        }
    }
}
