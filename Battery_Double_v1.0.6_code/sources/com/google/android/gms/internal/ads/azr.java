package com.google.android.gms.internal.ads;

import android.content.Context;

@cm
public final class azr {

    /* renamed from: a reason: collision with root package name */
    private Context f3043a;

    public final void a(Context context) {
        if (this.f3043a == null) {
            if (context.getApplicationContext() != null) {
                context = context.getApplicationContext();
            }
            this.f3043a = context;
        }
    }
}
