package com.google.android.gms.internal.ads;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;

final class c implements OnClickListener {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ beu f3217a;

    c(beu beu) {
        this.f3217a = beu;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f3217a.a("Operation denied by user.");
    }
}
