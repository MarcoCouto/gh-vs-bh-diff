package com.google.android.gms.internal.ads;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.os.Message;
import android.view.View;
import android.view.WindowManager.BadTokenException;
import android.webkit.ConsoleMessage;
import android.webkit.GeolocationPermissions.Callback;
import android.webkit.JsPromptResult;
import android.webkit.JsResult;
import android.webkit.PermissionRequest;
import android.webkit.WebChromeClient;
import android.webkit.WebChromeClient.CustomViewCallback;
import android.webkit.WebStorage.QuotaUpdater;
import android.webkit.WebView;
import android.webkit.WebView.WebViewTransport;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.google.android.gms.ads.internal.ax;
import com.google.android.gms.ads.internal.bv;
import com.google.android.gms.ads.internal.overlay.d;
import com.google.android.gms.common.util.n;

@TargetApi(11)
@cm
public final class qf extends WebChromeClient {

    /* renamed from: a reason: collision with root package name */
    private final qn f3628a;

    public qf(qn qnVar) {
        this.f3628a = qnVar;
    }

    private static Context a(WebView webView) {
        if (!(webView instanceof qn)) {
            return webView.getContext();
        }
        qn qnVar = (qn) webView;
        Activity d = qnVar.d();
        return d == null ? qnVar.getContext() : d;
    }

    private final boolean a(Context context, String str, String str2, String str3, String str4, JsResult jsResult, JsPromptResult jsPromptResult, boolean z) {
        try {
            if (!(this.f3628a == null || this.f3628a.v() == null || this.f3628a.v().a() == null)) {
                bv a2 = this.f3628a.v().a();
                if (a2 != null && !a2.b()) {
                    a2.a(new StringBuilder(String.valueOf(str).length() + 11 + String.valueOf(str3).length()).append("window.").append(str).append("('").append(str3).append("')").toString());
                    return false;
                }
            }
            Builder builder = new Builder(context);
            builder.setTitle(str2);
            if (z) {
                LinearLayout linearLayout = new LinearLayout(context);
                linearLayout.setOrientation(1);
                TextView textView = new TextView(context);
                textView.setText(str3);
                EditText editText = new EditText(context);
                editText.setText(str4);
                linearLayout.addView(textView);
                linearLayout.addView(editText);
                builder.setView(linearLayout).setPositiveButton(17039370, new ql(jsPromptResult, editText)).setNegativeButton(17039360, new qk(jsPromptResult)).setOnCancelListener(new qj(jsPromptResult)).create().show();
                return true;
            }
            builder.setMessage(str3).setPositiveButton(17039370, new qi(jsResult)).setNegativeButton(17039360, new qh(jsResult)).setOnCancelListener(new qg(jsResult)).create().show();
            return true;
        } catch (BadTokenException e) {
            jm.c("Fail to display Dialog.", e);
            return true;
        }
    }

    public final void onCloseWindow(WebView webView) {
        if (!(webView instanceof qn)) {
            jm.e("Tried to close a WebView that wasn't an AdWebView.");
            return;
        }
        d r = ((qn) webView).r();
        if (r == null) {
            jm.e("Tried to close an AdWebView not associated with an overlay.");
        } else {
            r.a();
        }
    }

    public final boolean onConsoleMessage(ConsoleMessage consoleMessage) {
        String message = consoleMessage.message();
        String sourceId = consoleMessage.sourceId();
        String sb = new StringBuilder(String.valueOf(message).length() + 19 + String.valueOf(sourceId).length()).append("JS: ").append(message).append(" (").append(sourceId).append(":").append(consoleMessage.lineNumber()).append(")").toString();
        if (sb.contains("Application Cache")) {
            return super.onConsoleMessage(consoleMessage);
        }
        switch (qm.f3636a[consoleMessage.messageLevel().ordinal()]) {
            case 1:
                jm.c(sb);
                break;
            case 2:
                jm.e(sb);
                break;
            case 3:
            case 4:
                jm.d(sb);
                break;
            case 5:
                jm.b(sb);
                break;
            default:
                jm.d(sb);
                break;
        }
        return super.onConsoleMessage(consoleMessage);
    }

    public final boolean onCreateWindow(WebView webView, boolean z, boolean z2, Message message) {
        WebViewTransport webViewTransport = (WebViewTransport) message.obj;
        WebView webView2 = new WebView(webView.getContext());
        if (this.f3628a.w() != null) {
            webView2.setWebViewClient(this.f3628a.w());
        }
        webViewTransport.setWebView(webView2);
        message.sendToTarget();
        return true;
    }

    public final void onExceededDatabaseQuota(String str, String str2, long j, long j2, long j3, QuotaUpdater quotaUpdater) {
        long j4 = 5242880 - j3;
        if (j4 <= 0) {
            quotaUpdater.updateQuota(j);
            return;
        }
        if (j != 0) {
            if (j2 == 0) {
                j = Math.min(Math.min(131072, j4) + j, 1048576);
            } else if (j2 <= Math.min(1048576 - j, j4)) {
                j += j2;
            }
            j2 = j;
        } else if (j2 > j4 || j2 > 1048576) {
            j2 = 0;
        }
        quotaUpdater.updateQuota(j2);
    }

    public final void onGeolocationPermissionsShowPrompt(String str, Callback callback) {
        boolean z;
        if (callback != null) {
            ax.e();
            if (!jv.a(this.f3628a.getContext(), "android.permission.ACCESS_FINE_LOCATION")) {
                ax.e();
                if (!jv.a(this.f3628a.getContext(), "android.permission.ACCESS_COARSE_LOCATION")) {
                    z = false;
                    callback.invoke(str, z, true);
                }
            }
            z = true;
            callback.invoke(str, z, true);
        }
    }

    public final void onHideCustomView() {
        d r = this.f3628a.r();
        if (r == null) {
            jm.e("Could not get ad overlay when hiding custom view.");
        } else {
            r.b();
        }
    }

    public final boolean onJsAlert(WebView webView, String str, String str2, JsResult jsResult) {
        return a(a(webView), "alert", str, str2, null, jsResult, null, false);
    }

    public final boolean onJsBeforeUnload(WebView webView, String str, String str2, JsResult jsResult) {
        return a(a(webView), "onBeforeUnload", str, str2, null, jsResult, null, false);
    }

    public final boolean onJsConfirm(WebView webView, String str, String str2, JsResult jsResult) {
        return a(a(webView), "confirm", str, str2, null, jsResult, null, false);
    }

    public final boolean onJsPrompt(WebView webView, String str, String str2, String str3, JsPromptResult jsPromptResult) {
        return a(a(webView), "prompt", str, str2, str3, null, jsPromptResult, true);
    }

    @TargetApi(21)
    public final void onPermissionRequest(PermissionRequest permissionRequest) {
        if (n.i()) {
            if (!((Boolean) ape.f().a(asi.aC)).booleanValue()) {
                if (this.f3628a == null || this.f3628a.v() == null || this.f3628a.v().m() == null) {
                    super.onPermissionRequest(permissionRequest);
                    return;
                }
                String[] a2 = this.f3628a.v().m().a(permissionRequest.getResources());
                if (a2.length > 0) {
                    permissionRequest.grant(a2);
                    return;
                } else {
                    permissionRequest.deny();
                    return;
                }
            }
        }
        super.onPermissionRequest(permissionRequest);
    }

    public final void onReachedMaxAppCacheSize(long j, long j2, QuotaUpdater quotaUpdater) {
        long j3 = 131072 + j;
        if (5242880 - j2 < j3) {
            quotaUpdater.updateQuota(0);
        } else {
            quotaUpdater.updateQuota(j3);
        }
    }

    @Deprecated
    public final void onShowCustomView(View view, int i, CustomViewCallback customViewCallback) {
        d r = this.f3628a.r();
        if (r == null) {
            jm.e("Could not get ad overlay when showing custom view.");
            customViewCallback.onCustomViewHidden();
            return;
        }
        r.a(view, customViewCallback);
        r.a(i);
    }

    public final void onShowCustomView(View view, CustomViewCallback customViewCallback) {
        onShowCustomView(view, -1, customViewCallback);
    }
}
