package com.google.android.gms.internal.ads;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public abstract class du extends ajl implements dt {
    public du() {
        super("com.google.android.gms.ads.internal.request.IAdRequestService");
    }

    /* JADX WARNING: type inference failed for: r1v0 */
    /* JADX WARNING: type inference failed for: r1v3, types: [com.google.android.gms.internal.ads.ea] */
    /* JADX WARNING: type inference failed for: r1v4, types: [com.google.android.gms.internal.ads.dz] */
    /* JADX WARNING: type inference failed for: r1v5, types: [com.google.android.gms.internal.ads.dz] */
    /* JADX WARNING: type inference failed for: r1v8, types: [com.google.android.gms.internal.ads.ea] */
    /* JADX WARNING: type inference failed for: r1v9, types: [com.google.android.gms.internal.ads.dz] */
    /* JADX WARNING: type inference failed for: r1v10, types: [com.google.android.gms.internal.ads.dz] */
    /* JADX WARNING: type inference failed for: r1v13, types: [com.google.android.gms.internal.ads.dy] */
    /* JADX WARNING: type inference failed for: r1v14, types: [com.google.android.gms.internal.ads.dw] */
    /* JADX WARNING: type inference failed for: r1v15, types: [com.google.android.gms.internal.ads.dw] */
    /* JADX WARNING: type inference failed for: r1v16 */
    /* JADX WARNING: type inference failed for: r1v17 */
    /* JADX WARNING: type inference failed for: r1v18 */
    /* JADX WARNING: type inference failed for: r1v19 */
    /* JADX WARNING: type inference failed for: r1v20 */
    /* JADX WARNING: type inference failed for: r1v21 */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r1v0
  assigns: [?[int, float, boolean, short, byte, char, OBJECT, ARRAY], com.google.android.gms.internal.ads.ea, com.google.android.gms.internal.ads.dz, com.google.android.gms.internal.ads.dy, com.google.android.gms.internal.ads.dw]
  uses: [com.google.android.gms.internal.ads.dz, com.google.android.gms.internal.ads.dw]
  mth insns count: 55
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Unknown variable types count: 7 */
    public final boolean dispatchTransaction(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        ? r1 = 0;
        switch (i) {
            case 1:
                dp a2 = a((dl) ajm.a(parcel, dl.CREATOR));
                parcel2.writeNoException();
                ajm.b(parcel2, a2);
                break;
            case 2:
                dl dlVar = (dl) ajm.a(parcel, dl.CREATOR);
                IBinder readStrongBinder = parcel.readStrongBinder();
                if (readStrongBinder != null) {
                    IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.ads.internal.request.IAdResponseListener");
                    r1 = queryLocalInterface instanceof dw ? (dw) queryLocalInterface : new dy(readStrongBinder);
                }
                a(dlVar, (dw) r1);
                parcel2.writeNoException();
                break;
            case 4:
                ee eeVar = (ee) ajm.a(parcel, ee.CREATOR);
                IBinder readStrongBinder2 = parcel.readStrongBinder();
                if (readStrongBinder2 != null) {
                    IInterface queryLocalInterface2 = readStrongBinder2.queryLocalInterface("com.google.android.gms.ads.internal.request.INonagonStreamingResponseListener");
                    r1 = queryLocalInterface2 instanceof dz ? (dz) queryLocalInterface2 : new ea(readStrongBinder2);
                }
                a(eeVar, (dz) r1);
                parcel2.writeNoException();
                break;
            case 5:
                ee eeVar2 = (ee) ajm.a(parcel, ee.CREATOR);
                IBinder readStrongBinder3 = parcel.readStrongBinder();
                if (readStrongBinder3 != null) {
                    IInterface queryLocalInterface3 = readStrongBinder3.queryLocalInterface("com.google.android.gms.ads.internal.request.INonagonStreamingResponseListener");
                    r1 = queryLocalInterface3 instanceof dz ? (dz) queryLocalInterface3 : new ea(readStrongBinder3);
                }
                b(eeVar2, r1);
                parcel2.writeNoException();
                break;
            default:
                return false;
        }
        return true;
    }
}
