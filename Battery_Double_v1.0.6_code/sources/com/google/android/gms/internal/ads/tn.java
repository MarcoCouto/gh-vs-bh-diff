package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.a.a;
import com.google.android.gms.common.internal.a.c;

public final class tn extends a {
    public static final Creator<tn> CREATOR = new to();

    /* renamed from: a reason: collision with root package name */
    private final int f3710a;

    /* renamed from: b reason: collision with root package name */
    private zz f3711b = null;
    private byte[] c;

    tn(int i, byte[] bArr) {
        this.f3710a = i;
        this.c = bArr;
        b();
    }

    private final void b() {
        if (this.f3711b == null && this.c != null) {
            return;
        }
        if (this.f3711b != null && this.c == null) {
            return;
        }
        if (this.f3711b != null && this.c != null) {
            throw new IllegalStateException("Invalid internal representation - full");
        } else if (this.f3711b == null && this.c == null) {
            throw new IllegalStateException("Invalid internal representation - empty");
        } else {
            throw new IllegalStateException("Impossible");
        }
    }

    public final zz a() {
        if (!(this.f3711b != null)) {
            try {
                this.f3711b = (zz) afn.a(new zz(), this.c);
                this.c = null;
            } catch (afm e) {
                throw new IllegalStateException(e);
            }
        }
        b();
        return this.f3711b;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = c.a(parcel);
        c.a(parcel, 1, this.f3710a);
        c.a(parcel, 2, this.c != null ? this.c : afn.a((afn) this.f3711b), false);
        c.a(parcel, a2);
    }
}
