package com.google.android.gms.internal.ads;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

public final class aiy extends ajj {
    private List<Long> d = null;

    public aiy(ahy ahy, String str, String str2, zz zzVar, int i, int i2) {
        super(ahy, str, str2, zzVar, i, 31);
    }

    /* access modifiers changed from: protected */
    public final void a() throws IllegalAccessException, InvocationTargetException {
        this.f2637b.p = Long.valueOf(-1);
        this.f2637b.q = Long.valueOf(-1);
        if (this.d == null) {
            this.d = (List) this.c.invoke(null, new Object[]{this.f2636a.a()});
        }
        if (this.d != null && this.d.size() == 2) {
            synchronized (this.f2637b) {
                this.f2637b.p = Long.valueOf(((Long) this.d.get(0)).longValue());
                this.f2637b.q = Long.valueOf(((Long) this.d.get(1)).longValue());
            }
        }
    }
}
