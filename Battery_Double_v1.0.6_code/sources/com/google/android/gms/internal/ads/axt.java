package com.google.android.gms.internal.ads;

import android.os.RemoteException;

final class axt extends apl {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ axs f2994a;

    axt(axs axs) {
        this.f2994a = axs;
    }

    public final void a() throws RemoteException {
        this.f2994a.f2993a.add(new axu(this));
    }

    public final void a(int i) throws RemoteException {
        this.f2994a.f2993a.add(new axv(this, i));
        jm.a("Pooled interstitial failed to load.");
    }

    public final void b() throws RemoteException {
        this.f2994a.f2993a.add(new axw(this));
    }

    public final void c() throws RemoteException {
        this.f2994a.f2993a.add(new axx(this));
        jm.a("Pooled interstitial loaded.");
    }

    public final void d() throws RemoteException {
        this.f2994a.f2993a.add(new axy(this));
    }

    public final void e() throws RemoteException {
        this.f2994a.f2993a.add(new ayb(this));
    }

    public final void f() throws RemoteException {
        this.f2994a.f2993a.add(new aya(this));
    }
}
