package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.ax;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public final /* synthetic */ class azq {
    public static void a(azp azp, String str, String str2) {
        azp.b(new StringBuilder(String.valueOf(str).length() + 3 + String.valueOf(str2).length()).append(str).append("(").append(str2).append(");").toString());
    }

    public static void a(azp azp, String str, Map map) {
        try {
            azp.a(str, ax.e().a(map));
        } catch (JSONException e) {
            jm.e("Could not convert parameters to JSON.");
        }
    }

    public static void a(azp azp, String str, JSONObject jSONObject) {
        if (jSONObject == null) {
            jSONObject = new JSONObject();
        }
        azp.a(str, jSONObject.toString());
    }

    public static void b(azp azp, String str, JSONObject jSONObject) {
        if (jSONObject == null) {
            jSONObject = new JSONObject();
        }
        String jSONObject2 = jSONObject.toString();
        StringBuilder sb = new StringBuilder();
        sb.append("(window.AFMA_ReceiveMessage || function() {})('");
        sb.append(str);
        sb.append("'");
        sb.append(",");
        sb.append(jSONObject2);
        sb.append(");");
        String str2 = "Dispatching AFMA event: ";
        String valueOf = String.valueOf(sb.toString());
        jm.b(valueOf.length() != 0 ? str2.concat(valueOf) : new String(str2));
        azp.b(sb.toString());
    }
}
