package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.a.a;
import com.google.android.gms.common.internal.a.c;

public final class tl extends a {
    public static final Creator<tl> CREATOR = new tm();

    /* renamed from: a reason: collision with root package name */
    private final int f3708a;

    /* renamed from: b reason: collision with root package name */
    private final String f3709b;
    private final String c;

    tl(int i, String str, String str2) {
        this.f3708a = i;
        this.f3709b = str;
        this.c = str2;
    }

    public tl(String str, String str2) {
        this(1, str, str2);
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = c.a(parcel);
        c.a(parcel, 1, this.f3708a);
        c.a(parcel, 2, this.f3709b, false);
        c.a(parcel, 3, this.c, false);
        c.a(parcel, a2);
    }
}
