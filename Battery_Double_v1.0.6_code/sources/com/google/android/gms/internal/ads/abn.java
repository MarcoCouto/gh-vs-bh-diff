package com.google.android.gms.internal.ads;

import java.util.Arrays;
import java.util.Collection;
import java.util.RandomAccess;

final class abn extends aab<Float> implements abu<Float>, RandomAccess {

    /* renamed from: a reason: collision with root package name */
    private static final abn f2430a;

    /* renamed from: b reason: collision with root package name */
    private float[] f2431b;
    private int c;

    static {
        abn abn = new abn();
        f2430a = abn;
        abn.b();
    }

    abn() {
        this(new float[10], 0);
    }

    private abn(float[] fArr, int i) {
        this.f2431b = fArr;
        this.c = i;
    }

    private final void a(int i, float f) {
        c();
        if (i < 0 || i > this.c) {
            throw new IndexOutOfBoundsException(c(i));
        }
        if (this.c < this.f2431b.length) {
            System.arraycopy(this.f2431b, i, this.f2431b, i + 1, this.c - i);
        } else {
            float[] fArr = new float[(((this.c * 3) / 2) + 1)];
            System.arraycopy(this.f2431b, 0, fArr, 0, i);
            System.arraycopy(this.f2431b, i, fArr, i + 1, this.c - i);
            this.f2431b = fArr;
        }
        this.f2431b[i] = f;
        this.c++;
        this.modCount++;
    }

    private final void b(int i) {
        if (i < 0 || i >= this.c) {
            throw new IndexOutOfBoundsException(c(i));
        }
    }

    private final String c(int i) {
        return "Index:" + i + ", Size:" + this.c;
    }

    public final /* synthetic */ abu a(int i) {
        if (i >= this.c) {
            return new abn(Arrays.copyOf(this.f2431b, i), this.c);
        }
        throw new IllegalArgumentException();
    }

    public final void a(float f) {
        a(this.c, f);
    }

    public final /* synthetic */ void add(int i, Object obj) {
        a(i, ((Float) obj).floatValue());
    }

    public final boolean addAll(Collection<? extends Float> collection) {
        c();
        abr.a(collection);
        if (!(collection instanceof abn)) {
            return super.addAll(collection);
        }
        abn abn = (abn) collection;
        if (abn.c == 0) {
            return false;
        }
        if (Integer.MAX_VALUE - this.c < abn.c) {
            throw new OutOfMemoryError();
        }
        int i = this.c + abn.c;
        if (i > this.f2431b.length) {
            this.f2431b = Arrays.copyOf(this.f2431b, i);
        }
        System.arraycopy(abn.f2431b, 0, this.f2431b, this.c, abn.c);
        this.c = i;
        this.modCount++;
        return true;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof abn)) {
            return super.equals(obj);
        }
        abn abn = (abn) obj;
        if (this.c != abn.c) {
            return false;
        }
        float[] fArr = abn.f2431b;
        for (int i = 0; i < this.c; i++) {
            if (this.f2431b[i] != fArr[i]) {
                return false;
            }
        }
        return true;
    }

    public final /* synthetic */ Object get(int i) {
        b(i);
        return Float.valueOf(this.f2431b[i]);
    }

    public final int hashCode() {
        int i = 1;
        for (int i2 = 0; i2 < this.c; i2++) {
            i = (i * 31) + Float.floatToIntBits(this.f2431b[i2]);
        }
        return i;
    }

    public final /* synthetic */ Object remove(int i) {
        c();
        b(i);
        float f = this.f2431b[i];
        if (i < this.c - 1) {
            System.arraycopy(this.f2431b, i + 1, this.f2431b, i, this.c - i);
        }
        this.c--;
        this.modCount++;
        return Float.valueOf(f);
    }

    public final boolean remove(Object obj) {
        c();
        for (int i = 0; i < this.c; i++) {
            if (obj.equals(Float.valueOf(this.f2431b[i]))) {
                System.arraycopy(this.f2431b, i + 1, this.f2431b, i, this.c - i);
                this.c--;
                this.modCount++;
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public final void removeRange(int i, int i2) {
        c();
        if (i2 < i) {
            throw new IndexOutOfBoundsException("toIndex < fromIndex");
        }
        System.arraycopy(this.f2431b, i2, this.f2431b, i, this.c - i2);
        this.c -= i2 - i;
        this.modCount++;
    }

    public final /* synthetic */ Object set(int i, Object obj) {
        float floatValue = ((Float) obj).floatValue();
        c();
        b(i);
        float f = this.f2431b[i];
        this.f2431b[i] = floatValue;
        return Float.valueOf(f);
    }

    public final int size() {
        return this.c;
    }
}
