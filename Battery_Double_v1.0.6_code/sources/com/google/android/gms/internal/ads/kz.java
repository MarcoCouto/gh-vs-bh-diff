package com.google.android.gms.internal.ads;

import android.content.Context;
import java.io.File;
import java.util.regex.Pattern;

@cm
public final class kz extends ij {

    /* renamed from: a reason: collision with root package name */
    private final Context f3468a;

    private kz(Context context, qr qrVar) {
        super(qrVar);
        this.f3468a = context;
    }

    public static bab a(Context context) {
        bab bab = new bab(new lm(new File(context.getCacheDir(), "admob_volley")), new kz(context, new rs()));
        bab.a();
        return bab;
    }

    public final atz a(awb<?> awb) throws df {
        if (awb.h() && awb.c() == 0) {
            if (Pattern.matches((String) ape.f().a(asi.cJ), awb.e())) {
                ape.a();
                if (mh.c(this.f3468a)) {
                    atz a2 = new axl(this.f3468a).a(awb);
                    if (a2 != null) {
                        String str = "Got gmscore asset response: ";
                        String valueOf = String.valueOf(awb.e());
                        jm.a(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
                        return a2;
                    }
                    String str2 = "Failed to get gmscore asset response: ";
                    String valueOf2 = String.valueOf(awb.e());
                    jm.a(valueOf2.length() != 0 ? str2.concat(valueOf2) : new String(str2));
                }
            }
        }
        return super.a(awb);
    }
}
