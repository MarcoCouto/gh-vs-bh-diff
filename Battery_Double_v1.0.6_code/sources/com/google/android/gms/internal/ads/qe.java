package com.google.android.gms.internal.ads;

import java.io.File;
import java.text.DecimalFormat;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@cm
public final class qe extends py {

    /* renamed from: b reason: collision with root package name */
    private static final Set<String> f3627b = Collections.synchronizedSet(new HashSet());
    private static final DecimalFormat c = new DecimalFormat("#,###");
    private File d;
    private boolean e;

    public qe(pm pmVar) {
        super(pmVar);
        File cacheDir = this.f3619a.getCacheDir();
        if (cacheDir == null) {
            jm.e("Context.getCacheDir() returned null");
            return;
        }
        this.d = new File(cacheDir, "admobVideoStreams");
        if (!this.d.isDirectory() && !this.d.mkdirs()) {
            String str = "Could not create preload cache directory at ";
            String valueOf = String.valueOf(this.d.getAbsolutePath());
            jm.e(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
            this.d = null;
        } else if (!this.d.setReadable(true, false) || !this.d.setExecutable(true, false)) {
            String str2 = "Could not set cache file permissions at ";
            String valueOf2 = String.valueOf(this.d.getAbsolutePath());
            jm.e(valueOf2.length() != 0 ? str2.concat(valueOf2) : new String(str2));
            this.d = null;
        }
    }

    private final File a(File file) {
        return new File(this.d, String.valueOf(file.getName()).concat(".done"));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:100:0x0246, code lost:
        r3 = null;
        r4 = r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:101:0x024a, code lost:
        r6 = new java.net.URL(r3, r8);
        r3 = r6.getProtocol();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:102:0x0253, code lost:
        if (r3 != null) goto L_0x025d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:104:0x025c, code lost:
        throw new java.io.IOException("Protocol is null");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:106:0x0263, code lost:
        if (r3.equals("http") != false) goto L_0x0289;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:108:0x026b, code lost:
        if (r3.equals("https") != false) goto L_0x0289;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:109:0x026d, code lost:
        r6 = "Unsupported scheme: ";
        r2 = java.lang.String.valueOf(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:110:0x0279, code lost:
        if (r2.length() == 0) goto L_0x0283;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:111:0x027b, code lost:
        r2 = r6.concat(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:113:0x0282, code lost:
        throw new java.io.IOException(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:114:0x0283, code lost:
        r2 = new java.lang.String(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:115:0x0289, code lost:
        r12 = "Redirecting to ";
        r3 = java.lang.String.valueOf(r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:116:0x0293, code lost:
        if (r3.length() == 0) goto L_0x02a3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:117:0x0295, code lost:
        r3 = r12.concat(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:118:0x0299, code lost:
        com.google.android.gms.internal.ads.jm.b(r3);
        r2.disconnect();
        r2 = r4;
        r3 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:119:0x02a3, code lost:
        r3 = new java.lang.String(r12);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:121:0x02b0, code lost:
        throw new java.io.IOException("Too many redirects (20)");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:123:0x02b3, code lost:
        if ((r2 instanceof java.net.HttpURLConnection) == false) goto L_0x0313;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:124:0x02b5, code lost:
        r6 = r2.getResponseCode();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:125:0x02bf, code lost:
        if (r6 < 400) goto L_0x0313;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:126:0x02c1, code lost:
        r4 = "badUrl";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:127:0x02c3, code lost:
        r2 = "HTTP request failed. Code: ";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:129:?, code lost:
        r3 = java.lang.String.valueOf(java.lang.Integer.toString(r6));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:130:0x02d1, code lost:
        if (r3.length() == 0) goto L_0x0309;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:131:0x02d3, code lost:
        r3 = r2.concat(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:134:0x0305, code lost:
        throw new java.io.IOException(new java.lang.StringBuilder(java.lang.String.valueOf(r28).length() + 32).append("HTTP status code ").append(r6).append(" at ").append(r28).toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:135:0x0306, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:137:?, code lost:
        r3 = new java.lang.String(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:138:0x030f, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:139:0x0310, code lost:
        r3 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:141:?, code lost:
        r7 = r2.getContentLength();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:142:0x0317, code lost:
        if (r7 >= 0) goto L_0x0348;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:143:0x0319, code lost:
        r3 = "Stream cache aborted, missing content-length header at ";
        r2 = java.lang.String.valueOf(r28);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:144:0x0323, code lost:
        if (r2.length() == 0) goto L_0x0342;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:145:0x0325, code lost:
        r2 = r3.concat(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:146:0x0329, code lost:
        com.google.android.gms.internal.ads.jm.e(r2);
        a(r28, r13.getAbsolutePath(), "contentLengthMissing", null);
        f3627b.remove(r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:147:0x0342, code lost:
        r2 = new java.lang.String(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:148:0x0348, code lost:
        r4 = c.format((long) r7);
        r15 = ((java.lang.Integer) com.google.android.gms.internal.ads.ape.f().a(com.google.android.gms.internal.ads.asi.o)).intValue();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:149:0x0363, code lost:
        if (r7 <= r15) goto L_0x03c5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:150:0x0365, code lost:
        com.google.android.gms.internal.ads.jm.e(new java.lang.StringBuilder((java.lang.String.valueOf(r4).length() + 33) + java.lang.String.valueOf(r28).length()).append("Content length ").append(r4).append(" exceeds limit at ").append(r28).toString());
        r3 = "File too big for full file cache. Size: ";
        r2 = java.lang.String.valueOf(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:151:0x03a4, code lost:
        if (r2.length() == 0) goto L_0x03bf;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:152:0x03a6, code lost:
        r2 = r3.concat(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:153:0x03aa, code lost:
        a(r28, r13.getAbsolutePath(), "sizeExceeded", r2);
        f3627b.remove(r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:154:0x03bf, code lost:
        r2 = new java.lang.String(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:155:0x03c5, code lost:
        com.google.android.gms.internal.ads.jm.b(new java.lang.StringBuilder((java.lang.String.valueOf(r4).length() + 20) + java.lang.String.valueOf(r28).length()).append("Caching ").append(r4).append(" bytes from ").append(r28).toString());
        r16 = java.nio.channels.Channels.newChannel(r2.getInputStream());
        r12 = new java.io.FileOutputStream(r13);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:157:?, code lost:
        r17 = r12.getChannel();
        r18 = java.nio.ByteBuffer.allocate(1048576);
        r19 = com.google.android.gms.ads.internal.ax.l();
        r6 = 0;
        r20 = r19.a();
        r0 = new com.google.android.gms.internal.ads.lw(((java.lang.Long) com.google.android.gms.internal.ads.ape.f().a(com.google.android.gms.internal.ads.asi.r)).longValue());
        r24 = ((java.lang.Long) com.google.android.gms.internal.ads.ape.f().a(com.google.android.gms.internal.ads.asi.q)).longValue();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:158:0x0441, code lost:
        r2 = r16.read(r18);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:159:0x0449, code lost:
        if (r2 < 0) goto L_0x0502;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:160:0x044b, code lost:
        r6 = r6 + r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:161:0x044c, code lost:
        if (r6 <= r15) goto L_0x047b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:162:0x044e, code lost:
        r4 = "sizeExceeded";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:163:0x0450, code lost:
        r2 = "File too big for full file cache. Size: ";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:165:?, code lost:
        r3 = java.lang.String.valueOf(java.lang.Integer.toString(r6));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:166:0x045e, code lost:
        if (r3.length() == 0) goto L_0x0470;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:167:0x0460, code lost:
        r3 = r2.concat(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:170:0x046b, code lost:
        throw new java.io.IOException("stream cache file size limit exceeded");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:171:0x046c, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:172:0x046d, code lost:
        r5 = r12;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:174:?, code lost:
        r3 = new java.lang.String(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:175:0x0476, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:176:0x0477, code lost:
        r3 = null;
        r5 = r12;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:178:?, code lost:
        r18.flip();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:180:0x0482, code lost:
        if (r17.write(r18) > 0) goto L_0x047e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:181:0x0484, code lost:
        r18.clear();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:182:0x0493, code lost:
        if ((r19.a() - r20) <= (1000 * r24)) goto L_0x04ca;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:183:0x0495, code lost:
        r4 = "downloadTimeout";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:185:?, code lost:
        r2 = java.lang.Long.toString(r24);
        r3 = new java.lang.StringBuilder(java.lang.String.valueOf(r2).length() + 29).append("Timeout exceeded. Limit: ").append(r2).append(" sec").toString();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:188:0x04c5, code lost:
        throw new java.io.IOException("stream cache time limit exceeded");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:189:0x04c6, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:190:0x04c7, code lost:
        r5 = r12;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:194:0x04ce, code lost:
        if (r27.e == false) goto L_0x04df;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:195:0x04d0, code lost:
        r4 = "externalAbort";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:198:0x04d9, code lost:
        throw new java.io.IOException("abort requested");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:199:0x04da, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:200:0x04db, code lost:
        r3 = null;
        r5 = r12;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:203:0x04e3, code lost:
        if (r0.a() == false) goto L_0x0441;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:204:0x04e5, code lost:
        com.google.android.gms.internal.ads.mh.f3514a.post(new com.google.android.gms.internal.ads.pz(r27, r28, r13.getAbsolutePath(), r6, r7, false));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:205:0x04fc, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:206:0x04fd, code lost:
        r3 = null;
        r4 = r11;
        r5 = r12;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:207:0x0502, code lost:
        r12.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:208:0x050a, code lost:
        if (com.google.android.gms.internal.ads.jm.a(3) == false) goto L_0x0548;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:209:0x050c, code lost:
        r2 = c.format((long) r6);
        com.google.android.gms.internal.ads.jm.b(new java.lang.StringBuilder((java.lang.String.valueOf(r2).length() + 22) + java.lang.String.valueOf(r28).length()).append("Preloaded ").append(r2).append(" bytes from ").append(r28).toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:210:0x0548, code lost:
        r13.setReadable(true, false);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:211:0x0551, code lost:
        if (r14.isFile() == false) goto L_0x056d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:212:0x0553, code lost:
        r14.setLastModified(java.lang.System.currentTimeMillis());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:216:?, code lost:
        r14.createNewFile();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:218:0x0573, code lost:
        com.google.android.gms.internal.ads.jm.c(new java.lang.StringBuilder(java.lang.String.valueOf(r28).length() + 25).append("Preload failed for URL \"").append(r28).append("\"").toString(), r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:219:0x059d, code lost:
        r2 = new java.lang.String(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:221:0x05aa, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:222:0x05ab, code lost:
        r3 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:223:0x05b1, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:224:0x05b2, code lost:
        r3 = null;
        r4 = r11;
        r5 = r12;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:258:?, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:259:?, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:260:?, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x0168, code lost:
        r5 = null;
        r11 = "error";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:?, code lost:
        com.google.android.gms.ads.internal.ax.q();
        r7 = ((java.lang.Integer) com.google.android.gms.internal.ads.ape.f().a(com.google.android.gms.internal.ads.asi.s)).intValue();
        r3 = new java.net.URL(r28);
        r2 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x0187, code lost:
        r4 = r2 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x018b, code lost:
        if (r4 > 20) goto L_0x02a9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x018d, code lost:
        r2 = r3.openConnection();
        r2.setConnectTimeout(r7);
        r2.setReadTimeout(r7);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x0199, code lost:
        if ((r2 instanceof java.net.HttpURLConnection) != false) goto L_0x021a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x01a2, code lost:
        throw new java.io.IOException("Invalid protocol.");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x01a3, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x01a4, code lost:
        r3 = null;
        r4 = r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x01a8, code lost:
        if ((r2 instanceof java.lang.RuntimeException) != false) goto L_0x01aa;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x01aa, code lost:
        com.google.android.gms.ads.internal.ax.i().a(r2, "VideoStreamFullFileCache.preload");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:?, code lost:
        r5.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x01ba, code lost:
        if (r27.e != false) goto L_0x01bc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x01bc, code lost:
        com.google.android.gms.internal.ads.jm.d(new java.lang.StringBuilder(java.lang.String.valueOf(r28).length() + 26).append("Preload aborted for URL \"").append(r28).append("\"").toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:0x01f0, code lost:
        r5 = "Could not delete partial cache file at ";
        r2 = java.lang.String.valueOf(r13.getAbsolutePath());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:0x01fe, code lost:
        if (r2.length() != 0) goto L_0x0200;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:0x0200, code lost:
        r2 = r5.concat(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x0204, code lost:
        com.google.android.gms.internal.ads.jm.e(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:0x0207, code lost:
        a(r28, r13.getAbsolutePath(), r4, r3);
        f3627b.remove(r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:?, code lost:
        r2 = (java.net.HttpURLConnection) r2;
        r6 = new com.google.android.gms.internal.ads.ml();
        r6.a(r2, (byte[]) null);
        r2.setInstanceFollowRedirects(false);
        r8 = r2.getResponseCode();
        r6.a(r2, r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:94:0x0233, code lost:
        if ((r8 / 100) != 3) goto L_0x02b1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:95:0x0235, code lost:
        r8 = r2.getHeaderField("Location");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:0x023b, code lost:
        if (r8 != null) goto L_0x024a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x0244, code lost:
        throw new java.io.IOException("Missing Location header in redirect");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:99:0x0245, code lost:
        r2 = e;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:218:0x0573  */
    /* JADX WARNING: Removed duplicated region for block: B:219:0x059d  */
    /* JADX WARNING: Removed duplicated region for block: B:223:0x05b1 A[ExcHandler: RuntimeException (e java.lang.RuntimeException), Splitter:B:156:0x0407] */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x01aa  */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x01bc  */
    /* JADX WARNING: Removed duplicated region for block: B:89:0x0200  */
    public final boolean a(String str) {
        int i;
        File file;
        String str2;
        int i2;
        boolean z;
        long j;
        File file2;
        if (this.d == null) {
            a(str, null, "noCacheDir", null);
            return false;
        }
        do {
            if (this.d == null) {
                i = 0;
            } else {
                int i3 = 0;
                for (File name : this.d.listFiles()) {
                    if (!name.getName().endsWith(".done")) {
                        i3++;
                    }
                }
                i = i3;
            }
            if (i <= ((Integer) ape.f().a(asi.n)).intValue()) {
                ape.a();
                file = new File(this.d, mh.a(str));
                File a2 = a(file);
                if (!file.isFile() || !a2.isFile()) {
                    String valueOf = String.valueOf(this.d.getAbsolutePath());
                    String valueOf2 = String.valueOf(str);
                    str2 = valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf);
                    synchronized (f3627b) {
                        if (f3627b.contains(str2)) {
                            String str3 = "Stream cache already in progress at ";
                            String valueOf3 = String.valueOf(str);
                            jm.e(valueOf3.length() != 0 ? str3.concat(valueOf3) : new String(str3));
                            a(str, file.getAbsolutePath(), "inProgress", null);
                            return false;
                        }
                        f3627b.add(str2);
                    }
                } else {
                    int length = (int) file.length();
                    String str4 = "Stream cache hit at ";
                    String valueOf4 = String.valueOf(str);
                    jm.b(valueOf4.length() != 0 ? str4.concat(valueOf4) : new String(str4));
                    a(str, file.getAbsolutePath(), length);
                    return true;
                }
            } else if (this.d == null) {
                z = false;
                continue;
            } else {
                File file3 = null;
                long j2 = Long.MAX_VALUE;
                File[] listFiles = this.d.listFiles();
                int length2 = listFiles.length;
                int i4 = 0;
                while (i4 < length2) {
                    File file4 = listFiles[i4];
                    if (!file4.getName().endsWith(".done")) {
                        j = file4.lastModified();
                        if (j < j2) {
                            file2 = file4;
                            i4++;
                            file3 = file2;
                            j2 = j;
                        }
                    }
                    j = j2;
                    file2 = file3;
                    i4++;
                    file3 = file2;
                    j2 = j;
                }
                z = false;
                if (file3 != null) {
                    z = file3.delete();
                    File a3 = a(file3);
                    if (a3.isFile()) {
                        z &= a3.delete();
                        continue;
                    } else {
                        continue;
                    }
                } else {
                    continue;
                }
            }
        } while (z);
        jm.e("Unable to expire stream cache");
        a(str, null, "expireFailed", null);
        return false;
        a(str, file.getAbsolutePath(), i2);
        f3627b.remove(str2);
        return true;
    }

    public final void b() {
        this.e = true;
    }
}
