package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.Bundle;

public final class tc {

    /* renamed from: a reason: collision with root package name */
    private final Context f3702a;

    /* renamed from: b reason: collision with root package name */
    private final int f3703b;
    private final Bundle c;

    public tc(Context context, int i, Bundle bundle) {
        this.f3702a = context;
        this.f3703b = i;
        this.c = bundle;
    }
}
