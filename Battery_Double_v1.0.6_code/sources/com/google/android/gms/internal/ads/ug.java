package com.google.android.gms.internal.ads;

import java.util.Arrays;

public final class ug<P> {

    /* renamed from: a reason: collision with root package name */
    private final P f3719a;

    /* renamed from: b reason: collision with root package name */
    private final byte[] f3720b;
    private final xh c;
    private final ya d;

    public ug(P p, byte[] bArr, xh xhVar, ya yaVar) {
        this.f3719a = p;
        this.f3720b = Arrays.copyOf(bArr, bArr.length);
        this.c = xhVar;
        this.d = yaVar;
    }

    public final P a() {
        return this.f3719a;
    }

    public final byte[] b() {
        if (this.f3720b == null) {
            return null;
        }
        return Arrays.copyOf(this.f3720b, this.f3720b.length);
    }
}
