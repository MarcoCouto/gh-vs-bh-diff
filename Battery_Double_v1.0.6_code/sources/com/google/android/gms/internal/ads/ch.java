package com.google.android.gms.internal.ads;

import java.lang.Thread.UncaughtExceptionHandler;

final class ch implements UncaughtExceptionHandler {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ UncaughtExceptionHandler f3228a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ cg f3229b;

    ch(cg cgVar, UncaughtExceptionHandler uncaughtExceptionHandler) {
        this.f3229b = cgVar;
        this.f3228a = uncaughtExceptionHandler;
    }

    public final void uncaughtException(Thread thread, Throwable th) {
        try {
            this.f3229b.a(thread, th);
            if (this.f3228a != null) {
                this.f3228a.uncaughtException(thread, th);
            }
        } catch (Throwable th2) {
            if (this.f3228a != null) {
                this.f3228a.uncaughtException(thread, th);
            }
            throw th2;
        }
    }
}
