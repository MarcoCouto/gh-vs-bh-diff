package com.google.android.gms.internal.ads;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

final class ka extends BroadcastReceiver {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ jv f3448a;

    private ka(jv jvVar) {
        this.f3448a = jvVar;
    }

    /* synthetic */ ka(jv jvVar, jw jwVar) {
        this(jvVar);
    }

    public final void onReceive(Context context, Intent intent) {
        if ("android.intent.action.USER_PRESENT".equals(intent.getAction())) {
            this.f3448a.c = true;
        } else if ("android.intent.action.SCREEN_OFF".equals(intent.getAction())) {
            this.f3448a.c = false;
        }
    }
}
