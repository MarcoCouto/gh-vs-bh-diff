package com.google.android.gms.internal.ads;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;

abstract class zm implements tr {

    /* renamed from: a reason: collision with root package name */
    private final byte[] f3817a;

    /* renamed from: b reason: collision with root package name */
    private final zl f3818b;
    private final zl c;

    zm(byte[] bArr) throws InvalidKeyException {
        this.f3817a = (byte[]) bArr.clone();
        this.f3818b = a(bArr, 1);
        this.c = a(bArr, 0);
    }

    /* access modifiers changed from: 0000 */
    public abstract zl a(byte[] bArr, int i) throws InvalidKeyException;

    public byte[] a(byte[] bArr, byte[] bArr2) throws GeneralSecurityException {
        int length = bArr.length;
        this.f3818b.a();
        if (length > 2147483619) {
            throw new GeneralSecurityException("plaintext too long");
        }
        ByteBuffer allocate = ByteBuffer.allocate(bArr.length + this.f3818b.a() + 16);
        if (allocate.remaining() < bArr.length + this.f3818b.a() + 16) {
            throw new IllegalArgumentException("Given ByteBuffer output is too small");
        }
        int position = allocate.position();
        this.f3818b.a(allocate, bArr);
        allocate.position(position);
        byte[] bArr3 = new byte[this.f3818b.a()];
        allocate.get(bArr3);
        allocate.limit(allocate.limit() - 16);
        if (bArr2 == null) {
            bArr2 = new byte[0];
        }
        byte[] bArr4 = new byte[32];
        this.c.a(bArr3, 0).get(bArr4);
        int length2 = bArr2.length % 16 == 0 ? bArr2.length : (bArr2.length + 16) - (bArr2.length % 16);
        int remaining = allocate.remaining();
        int i = remaining % 16 == 0 ? remaining : (remaining + 16) - (remaining % 16);
        ByteBuffer order = ByteBuffer.allocate(length2 + i + 16).order(ByteOrder.LITTLE_ENDIAN);
        order.put(bArr2);
        order.position(length2);
        order.put(allocate);
        order.position(length2 + i);
        order.putLong((long) bArr2.length);
        order.putLong((long) remaining);
        byte[] a2 = zi.a(bArr4, order.array());
        allocate.limit(allocate.limit() + 16);
        allocate.put(a2);
        return allocate.array();
    }
}
