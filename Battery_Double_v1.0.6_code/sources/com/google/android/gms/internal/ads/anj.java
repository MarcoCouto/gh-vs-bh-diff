package com.google.android.gms.internal.ads;

import java.io.IOException;

public final class anj extends afh<anj> {

    /* renamed from: a reason: collision with root package name */
    private String f2756a;

    /* renamed from: b reason: collision with root package name */
    private anh[] f2757b;
    private Integer c;
    private Integer d;
    private Integer e;

    public anj() {
        this.f2756a = null;
        this.f2757b = anh.b();
        this.c = null;
        this.d = null;
        this.e = null;
        this.Y = null;
        this.Z = -1;
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public final anj a(afd afd) throws IOException {
        while (true) {
            int a2 = afd.a();
            switch (a2) {
                case 0:
                    break;
                case 10:
                    this.f2756a = afd.e();
                    continue;
                case 18:
                    int a3 = afq.a(afd, 18);
                    int length = this.f2757b == null ? 0 : this.f2757b.length;
                    anh[] anhArr = new anh[(a3 + length)];
                    if (length != 0) {
                        System.arraycopy(this.f2757b, 0, anhArr, 0, length);
                    }
                    while (length < anhArr.length - 1) {
                        anhArr[length] = new anh();
                        afd.a((afn) anhArr[length]);
                        afd.a();
                        length++;
                    }
                    anhArr[length] = new anh();
                    afd.a((afn) anhArr[length]);
                    this.f2757b = anhArr;
                    continue;
                case 24:
                    int j = afd.j();
                    try {
                        this.c = Integer.valueOf(anf.a(afd.g()));
                        continue;
                    } catch (IllegalArgumentException e2) {
                        afd.e(j);
                        a(afd, a2);
                        break;
                    }
                case 32:
                    int j2 = afd.j();
                    try {
                        this.d = Integer.valueOf(anf.a(afd.g()));
                        continue;
                    } catch (IllegalArgumentException e3) {
                        afd.e(j2);
                        a(afd, a2);
                        break;
                    }
                case 40:
                    int j3 = afd.j();
                    try {
                        this.e = Integer.valueOf(anf.a(afd.g()));
                        continue;
                    } catch (IllegalArgumentException e4) {
                        afd.e(j3);
                        a(afd, a2);
                        break;
                    }
                default:
                    if (!super.a(afd, a2)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    /* access modifiers changed from: protected */
    public final int a() {
        int a2 = super.a();
        if (this.f2756a != null) {
            a2 += aff.b(1, this.f2756a);
        }
        if (this.f2757b != null && this.f2757b.length > 0) {
            int i = a2;
            for (anh anh : this.f2757b) {
                if (anh != null) {
                    i += aff.b(2, (afn) anh);
                }
            }
            a2 = i;
        }
        if (this.c != null) {
            a2 += aff.b(3, this.c.intValue());
        }
        if (this.d != null) {
            a2 += aff.b(4, this.d.intValue());
        }
        return this.e != null ? a2 + aff.b(5, this.e.intValue()) : a2;
    }

    public final void a(aff aff) throws IOException {
        if (this.f2756a != null) {
            aff.a(1, this.f2756a);
        }
        if (this.f2757b != null && this.f2757b.length > 0) {
            for (anh anh : this.f2757b) {
                if (anh != null) {
                    aff.a(2, (afn) anh);
                }
            }
        }
        if (this.c != null) {
            aff.a(3, this.c.intValue());
        }
        if (this.d != null) {
            aff.a(4, this.d.intValue());
        }
        if (this.e != null) {
            aff.a(5, this.e.intValue());
        }
        super.a(aff);
    }
}
