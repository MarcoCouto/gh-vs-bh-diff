package com.google.android.gms.internal.ads;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.b.a;

public interface ajo extends IInterface {
    void a() throws RemoteException;

    void a(int i) throws RemoteException;

    void a(a aVar, String str) throws RemoteException;

    void a(a aVar, String str, String str2) throws RemoteException;

    void a(byte[] bArr) throws RemoteException;

    void a(int[] iArr) throws RemoteException;

    void b(int i) throws RemoteException;
}
