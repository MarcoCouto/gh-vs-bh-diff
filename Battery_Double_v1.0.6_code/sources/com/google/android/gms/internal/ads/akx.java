package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.app.Application.ActivityLifecycleCallbacks;
import android.os.Bundle;

final class akx implements alf {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ Activity f2682a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ Bundle f2683b;

    akx(akw akw, Activity activity, Bundle bundle) {
        this.f2682a = activity;
        this.f2683b = bundle;
    }

    public final void a(ActivityLifecycleCallbacks activityLifecycleCallbacks) {
        activityLifecycleCallbacks.onActivityCreated(this.f2682a, this.f2683b);
    }
}
