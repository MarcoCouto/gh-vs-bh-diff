package com.google.android.gms.internal.ads;

import java.util.Collections;
import java.util.List;

@cm
public final class arv implements arw {
    public final List<String> a(List<String> list) {
        return list == null ? Collections.emptyList() : list;
    }
}
