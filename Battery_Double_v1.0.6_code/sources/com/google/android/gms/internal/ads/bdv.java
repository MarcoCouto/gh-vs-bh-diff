package com.google.android.gms.internal.ads;

import android.os.RemoteException;
import com.google.ads.a.C0043a;
import com.google.ads.mediation.MediationBannerAdapter;
import com.google.ads.mediation.MediationInterstitialAdapter;
import com.google.ads.mediation.c;
import com.google.ads.mediation.d;
import com.google.ads.mediation.e;
import com.google.ads.mediation.f;

@cm
public final class bdv<NETWORK_EXTRAS extends f, SERVER_PARAMETERS extends e> implements c, d {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public final bcx f3159a;

    public bdv(bcx bcx) {
        this.f3159a = bcx;
    }

    public final void a(MediationBannerAdapter<?, ?> mediationBannerAdapter, C0043a aVar) {
        String valueOf = String.valueOf(aVar);
        ms.b(new StringBuilder(String.valueOf(valueOf).length() + 47).append("Adapter called onFailedToReceiveAd with error. ").append(valueOf).toString());
        ape.a();
        if (!mh.b()) {
            ms.d("#008 Must be called on the main UI thread.", null);
            mh.f3514a.post(new bdw(this, aVar));
            return;
        }
        try {
            this.f3159a.a(bdz.a(aVar));
        } catch (RemoteException e) {
            ms.d("#007 Could not call remote method.", e);
        }
    }

    public final void a(MediationInterstitialAdapter<?, ?> mediationInterstitialAdapter, C0043a aVar) {
        String valueOf = String.valueOf(aVar);
        ms.b(new StringBuilder(String.valueOf(valueOf).length() + 47).append("Adapter called onFailedToReceiveAd with error ").append(valueOf).append(".").toString());
        ape.a();
        if (!mh.b()) {
            ms.d("#008 Must be called on the main UI thread.", null);
            mh.f3514a.post(new bdy(this, aVar));
            return;
        }
        try {
            this.f3159a.a(bdz.a(aVar));
        } catch (RemoteException e) {
            ms.d("#007 Could not call remote method.", e);
        }
    }
}
