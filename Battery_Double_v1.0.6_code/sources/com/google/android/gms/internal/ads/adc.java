package com.google.android.gms.internal.ads;

import java.io.IOException;

public final class adc extends afh<adc> {

    /* renamed from: a reason: collision with root package name */
    private Long f2470a;

    /* renamed from: b reason: collision with root package name */
    private Integer f2471b;
    private Boolean c;
    private int[] d;
    private Long e;

    public adc() {
        this.f2470a = null;
        this.f2471b = null;
        this.c = null;
        this.d = afq.f2534a;
        this.e = null;
        this.Z = -1;
    }

    /* access modifiers changed from: protected */
    public final int a() {
        int a2 = super.a();
        if (this.f2470a != null) {
            a2 += aff.d(1, this.f2470a.longValue());
        }
        if (this.f2471b != null) {
            a2 += aff.b(2, this.f2471b.intValue());
        }
        if (this.c != null) {
            this.c.booleanValue();
            a2 += aff.b(3) + 1;
        }
        if (this.d != null && this.d.length > 0) {
            int i = 0;
            for (int a3 : this.d) {
                i += aff.a(a3);
            }
            a2 = a2 + i + (this.d.length * 1);
        }
        return this.e != null ? a2 + aff.c(5, this.e.longValue()) : a2;
    }

    public final /* synthetic */ afn a(afd afd) throws IOException {
        while (true) {
            int a2 = afd.a();
            switch (a2) {
                case 0:
                    break;
                case 8:
                    this.f2470a = Long.valueOf(afd.h());
                    continue;
                case 16:
                    this.f2471b = Integer.valueOf(afd.g());
                    continue;
                case 24:
                    this.c = Boolean.valueOf(afd.d());
                    continue;
                case 32:
                    int a3 = afq.a(afd, 32);
                    int length = this.d == null ? 0 : this.d.length;
                    int[] iArr = new int[(a3 + length)];
                    if (length != 0) {
                        System.arraycopy(this.d, 0, iArr, 0, length);
                    }
                    while (length < iArr.length - 1) {
                        iArr[length] = afd.g();
                        afd.a();
                        length++;
                    }
                    iArr[length] = afd.g();
                    this.d = iArr;
                    continue;
                case 34:
                    int c2 = afd.c(afd.g());
                    int j = afd.j();
                    int i = 0;
                    while (afd.i() > 0) {
                        afd.g();
                        i++;
                    }
                    afd.e(j);
                    int length2 = this.d == null ? 0 : this.d.length;
                    int[] iArr2 = new int[(i + length2)];
                    if (length2 != 0) {
                        System.arraycopy(this.d, 0, iArr2, 0, length2);
                    }
                    while (length2 < iArr2.length) {
                        iArr2[length2] = afd.g();
                        length2++;
                    }
                    this.d = iArr2;
                    afd.d(c2);
                    continue;
                case 40:
                    this.e = Long.valueOf(afd.h());
                    continue;
                default:
                    if (!super.a(afd, a2)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    public final void a(aff aff) throws IOException {
        if (this.f2470a != null) {
            aff.b(1, this.f2470a.longValue());
        }
        if (this.f2471b != null) {
            aff.a(2, this.f2471b.intValue());
        }
        if (this.c != null) {
            aff.a(3, this.c.booleanValue());
        }
        if (this.d != null && this.d.length > 0) {
            for (int a2 : this.d) {
                aff.a(4, a2);
            }
        }
        if (this.e != null) {
            aff.a(5, this.e.longValue());
        }
        super.a(aff);
    }
}
