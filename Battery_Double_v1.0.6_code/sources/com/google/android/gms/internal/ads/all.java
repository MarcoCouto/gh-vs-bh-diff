package com.google.android.gms.internal.ads;

final class all implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ alk f2700a;

    all(alk alk) {
        this.f2700a = alk;
    }

    public final void run() {
        synchronized (this.f2700a.c) {
            if (!this.f2700a.d || !this.f2700a.e) {
                jm.b("App is still foreground");
            } else {
                this.f2700a.d = false;
                jm.b("App went background");
                for (alm a2 : this.f2700a.f) {
                    try {
                        a2.a(false);
                    } catch (Exception e) {
                        ms.b("", e);
                    }
                }
            }
        }
    }
}
