package com.google.android.gms.internal.ads;

import java.util.Map;

final /* synthetic */ class re implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final rd f3659a;

    /* renamed from: b reason: collision with root package name */
    private final Map f3660b;

    re(rd rdVar, Map map) {
        this.f3659a = rdVar;
        this.f3660b = map;
    }

    public final void run() {
        this.f3659a.a(this.f3660b);
    }
}
