package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.google.android.gms.b.a;
import com.google.android.gms.b.a.C0046a;
import java.util.ArrayList;
import java.util.List;

public final class avr extends ajk implements avp {
    avr(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.formats.client.INativeContentAd");
    }

    public final String a() throws RemoteException {
        Parcel a2 = a(3, r_());
        String readString = a2.readString();
        a2.recycle();
        return readString;
    }

    public final void a(Bundle bundle) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (Parcelable) bundle);
        b(12, r_);
    }

    public final List b() throws RemoteException {
        Parcel a2 = a(4, r_());
        ArrayList b2 = ajm.b(a2);
        a2.recycle();
        return b2;
    }

    public final boolean b(Bundle bundle) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (Parcelable) bundle);
        Parcel a2 = a(13, r_);
        boolean a3 = ajm.a(a2);
        a2.recycle();
        return a3;
    }

    public final a c() throws RemoteException {
        Parcel a2 = a(16, r_());
        a a3 = C0046a.a(a2.readStrongBinder());
        a2.recycle();
        return a3;
    }

    public final void c(Bundle bundle) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (Parcelable) bundle);
        b(14, r_);
    }

    public final String d() throws RemoteException {
        Parcel a2 = a(17, r_());
        String readString = a2.readString();
        a2.recycle();
        return readString;
    }

    public final String e() throws RemoteException {
        Parcel a2 = a(5, r_());
        String readString = a2.readString();
        a2.recycle();
        return readString;
    }

    public final auw f() throws RemoteException {
        auw auy;
        Parcel a2 = a(6, r_());
        IBinder readStrongBinder = a2.readStrongBinder();
        if (readStrongBinder == null) {
            auy = null;
        } else {
            IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.ads.internal.formats.client.INativeAdImage");
            auy = queryLocalInterface instanceof auw ? (auw) queryLocalInterface : new auy(readStrongBinder);
        }
        a2.recycle();
        return auy;
    }

    public final String g() throws RemoteException {
        Parcel a2 = a(7, r_());
        String readString = a2.readString();
        a2.recycle();
        return readString;
    }

    public final String h() throws RemoteException {
        Parcel a2 = a(8, r_());
        String readString = a2.readString();
        a2.recycle();
        return readString;
    }

    public final aqs i() throws RemoteException {
        Parcel a2 = a(11, r_());
        aqs a3 = aqt.a(a2.readStrongBinder());
        a2.recycle();
        return a3;
    }

    public final a j() throws RemoteException {
        Parcel a2 = a(2, r_());
        a a3 = C0046a.a(a2.readStrongBinder());
        a2.recycle();
        return a3;
    }

    public final Bundle n() throws RemoteException {
        Parcel a2 = a(9, r_());
        Bundle bundle = (Bundle) ajm.a(a2, Bundle.CREATOR);
        a2.recycle();
        return bundle;
    }

    public final aus p() throws RemoteException {
        aus auu;
        Parcel a2 = a(15, r_());
        IBinder readStrongBinder = a2.readStrongBinder();
        if (readStrongBinder == null) {
            auu = null;
        } else {
            IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.ads.internal.formats.client.IAttributionInfo");
            auu = queryLocalInterface instanceof aus ? (aus) queryLocalInterface : new auu(readStrongBinder);
        }
        a2.recycle();
        return auu;
    }

    public final void q() throws RemoteException {
        b(10, r_());
    }
}
