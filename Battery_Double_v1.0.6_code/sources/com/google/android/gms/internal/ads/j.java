package com.google.android.gms.internal.ads;

import org.json.JSONException;
import org.json.JSONObject;

@cm
public final class j {

    /* renamed from: a reason: collision with root package name */
    private final boolean f3411a;

    /* renamed from: b reason: collision with root package name */
    private final boolean f3412b;
    private final boolean c;
    private final boolean d;
    private final boolean e;

    private j(l lVar) {
        this.f3411a = lVar.f3469a;
        this.f3412b = lVar.f3470b;
        this.c = lVar.c;
        this.d = lVar.d;
        this.e = lVar.e;
    }

    public final JSONObject a() {
        try {
            return new JSONObject().put("sms", this.f3411a).put("tel", this.f3412b).put("calendar", this.c).put("storePicture", this.d).put("inlineVideo", this.e);
        } catch (JSONException e2) {
            jm.b("Error occured while obtaining the MRAID capabilities.", e2);
            return null;
        }
    }
}
