package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.b.a;

public interface beg extends IInterface {
    beq a() throws RemoteException;

    void a(a aVar, String str, Bundle bundle, bej bej) throws RemoteException;

    void a(byte[] bArr, String str, Bundle bundle, a aVar, bec bec, bcx bcx, aot aot) throws RemoteException;

    void a(byte[] bArr, String str, Bundle bundle, a aVar, bee bee, bcx bcx) throws RemoteException;

    beq b() throws RemoteException;

    aqs c() throws RemoteException;

    void d() throws RemoteException;
}
