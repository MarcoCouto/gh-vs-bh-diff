package com.google.android.gms.internal.ads;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.webkit.JsPromptResult;
import android.widget.EditText;

final class ql implements OnClickListener {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ JsPromptResult f3634a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ EditText f3635b;

    ql(JsPromptResult jsPromptResult, EditText editText) {
        this.f3634a = jsPromptResult;
        this.f3635b = editText;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f3634a.confirm(this.f3635b.getText().toString());
    }
}
