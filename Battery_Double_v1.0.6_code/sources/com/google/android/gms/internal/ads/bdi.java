package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.b.a;
import com.google.android.gms.b.a.C0046a;
import java.util.List;

public abstract class bdi extends ajl implements bdh {
    public bdi() {
        super("com.google.android.gms.ads.internal.mediation.client.INativeContentAdMapper");
    }

    /* access modifiers changed from: protected */
    public final boolean dispatchTransaction(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        switch (i) {
            case 2:
                String a2 = a();
                parcel2.writeNoException();
                parcel2.writeString(a2);
                break;
            case 3:
                List b2 = b();
                parcel2.writeNoException();
                parcel2.writeList(b2);
                break;
            case 4:
                String c = c();
                parcel2.writeNoException();
                parcel2.writeString(c);
                break;
            case 5:
                auw d = d();
                parcel2.writeNoException();
                ajm.a(parcel2, (IInterface) d);
                break;
            case 6:
                String e = e();
                parcel2.writeNoException();
                parcel2.writeString(e);
                break;
            case 7:
                String f = f();
                parcel2.writeNoException();
                parcel2.writeString(f);
                break;
            case 8:
                g();
                parcel2.writeNoException();
                break;
            case 9:
                a(C0046a.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
            case 10:
                b(C0046a.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
            case 11:
                boolean h = h();
                parcel2.writeNoException();
                ajm.a(parcel2, h);
                break;
            case 12:
                boolean i3 = i();
                parcel2.writeNoException();
                ajm.a(parcel2, i3);
                break;
            case 13:
                Bundle j = j();
                parcel2.writeNoException();
                ajm.b(parcel2, j);
                break;
            case 14:
                c(C0046a.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
            case 15:
                a k = k();
                parcel2.writeNoException();
                ajm.a(parcel2, (IInterface) k);
                break;
            case 16:
                aqs l = l();
                parcel2.writeNoException();
                ajm.a(parcel2, (IInterface) l);
                break;
            case 19:
                aus m = m();
                parcel2.writeNoException();
                ajm.a(parcel2, (IInterface) m);
                break;
            case 20:
                a n = n();
                parcel2.writeNoException();
                ajm.a(parcel2, (IInterface) n);
                break;
            case 21:
                a o = o();
                parcel2.writeNoException();
                ajm.a(parcel2, (IInterface) o);
                break;
            case 22:
                a(C0046a.a(parcel.readStrongBinder()), C0046a.a(parcel.readStrongBinder()), C0046a.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
            default:
                return false;
        }
        return true;
    }
}
