package com.google.android.gms.internal.ads;

import android.os.Build.VERSION;
import android.os.ConditionVariable;
import java.io.IOException;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class ahb {

    /* renamed from: a reason: collision with root package name */
    protected static volatile anb f2580a = null;
    /* access modifiers changed from: private */
    public static final ConditionVariable d = new ConditionVariable();
    private static volatile Random e = null;

    /* renamed from: b reason: collision with root package name */
    protected volatile Boolean f2581b;
    /* access modifiers changed from: private */
    public ahy c;

    public ahb(ahy ahy) {
        this.c = ahy;
        ahy.c().execute(new ahc(this));
    }

    public static int a() {
        try {
            return VERSION.SDK_INT >= 21 ? ThreadLocalRandom.current().nextInt() : c().nextInt();
        } catch (RuntimeException e2) {
            return c().nextInt();
        }
    }

    private static Random c() {
        if (e == null) {
            synchronized (ahb.class) {
                if (e == null) {
                    e = new Random();
                }
            }
        }
        return e;
    }

    public final void a(int i, int i2, long j) throws IOException {
        try {
            d.block();
            if (this.f2581b.booleanValue() && f2580a != null) {
                vu vuVar = new vu();
                vuVar.f3748a = this.c.f2607a.getPackageName();
                vuVar.f3749b = Long.valueOf(j);
                and a2 = f2580a.a(afn.a((afn) vuVar));
                a2.a(i2);
                a2.b(i);
                a2.a();
            }
        } catch (Exception e2) {
        }
    }
}
