package com.google.android.gms.internal.ads;

import android.content.Context;

@cm
public final class bbq {

    /* renamed from: a reason: collision with root package name */
    private static final la<azv> f3107a = new bbr();

    /* renamed from: b reason: collision with root package name */
    private static final la<azv> f3108b = new bbs();
    private final bah c;

    public bbq(Context context, mu muVar, String str) {
        this.c = new bah(context, muVar, str, f3107a, f3108b);
    }

    public final <I, O> bbi<I, O> a(String str, bbl<I> bbl, bbk<O> bbk) {
        return new bbt(this.c, str, bbl, bbk);
    }
}
