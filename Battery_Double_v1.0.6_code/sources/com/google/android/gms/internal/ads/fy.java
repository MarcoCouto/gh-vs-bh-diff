package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.Bundle;
import com.google.android.gms.ads.internal.bu;
import com.google.android.gms.b.a;
import com.google.android.gms.b.b;

@cm
public final class fy extends gj {

    /* renamed from: a reason: collision with root package name */
    private final Context f3334a;

    /* renamed from: b reason: collision with root package name */
    private final Object f3335b;
    private final mu c;
    private final fz d;

    public fy(Context context, bu buVar, bcr bcr, mu muVar) {
        this(context, muVar, new fz(context, buVar, aot.a(), bcr, muVar));
    }

    private fy(Context context, mu muVar, fz fzVar) {
        this.f3335b = new Object();
        this.f3334a = context;
        this.c = muVar;
        this.d = fzVar;
    }

    public final void a() {
        synchronized (this.f3335b) {
            this.d.K();
        }
    }

    public final void a(a aVar) {
        synchronized (this.f3335b) {
            this.d.o();
        }
    }

    public final void a(aqa aqa) {
        if (((Boolean) ape.f().a(asi.aF)).booleanValue()) {
            synchronized (this.f3335b) {
                this.d.a(aqa);
            }
        }
    }

    public final void a(gf gfVar) {
        synchronized (this.f3335b) {
            this.d.a(gfVar);
        }
    }

    public final void a(gn gnVar) {
        synchronized (this.f3335b) {
            this.d.a(gnVar);
        }
    }

    public final void a(gt gtVar) {
        synchronized (this.f3335b) {
            this.d.a(gtVar);
        }
    }

    public final void a(String str) {
        synchronized (this.f3335b) {
            this.d.a(str);
        }
    }

    public final void a(boolean z) {
        synchronized (this.f3335b) {
            this.d.c(z);
        }
    }

    public final Bundle b() {
        Bundle q;
        if (!((Boolean) ape.f().a(asi.aF)).booleanValue()) {
            return new Bundle();
        }
        synchronized (this.f3335b) {
            q = this.d.q();
        }
        return q;
    }

    public final void b(a aVar) {
        synchronized (this.f3335b) {
            Context context = aVar == null ? null : (Context) b.a(aVar);
            if (context != null) {
                try {
                    this.d.a(context);
                } catch (Exception e) {
                    jm.c("Unable to extract updated context.", e);
                }
            }
            this.d.p();
        }
    }

    public final void c(a aVar) {
        synchronized (this.f3335b) {
            this.d.j();
        }
    }

    public final boolean c() {
        boolean L;
        synchronized (this.f3335b) {
            L = this.d.L();
        }
        return L;
    }

    public final void d() {
        a((a) null);
    }

    public final void e() {
        b(null);
    }

    public final void f() {
        c(null);
    }

    public final String g() {
        String a2;
        synchronized (this.f3335b) {
            a2 = this.d.a();
        }
        return a2;
    }
}
