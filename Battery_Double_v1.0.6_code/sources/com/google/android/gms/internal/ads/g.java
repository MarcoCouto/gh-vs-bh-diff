package com.google.android.gms.internal.ads;

import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.res.Resources;
import android.net.Uri;
import android.text.TextUtils;
import android.webkit.URLUtil;
import com.google.android.gms.ads.c.a.C0045a;
import com.google.android.gms.ads.internal.ax;
import java.util.Map;

@cm
public final class g extends n {

    /* renamed from: a reason: collision with root package name */
    private final Map<String, String> f3336a;
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public final Context f3337b;

    public g(qn qnVar, Map<String, String> map) {
        super(qnVar, "storePicture");
        this.f3336a = map;
        this.f3337b = qnVar.d();
    }

    public final void a() {
        if (this.f3337b == null) {
            a("Activity context is not available");
            return;
        }
        ax.e();
        if (!jv.f(this.f3337b).c()) {
            a("Feature is not supported by the device.");
            return;
        }
        String str = (String) this.f3336a.get("iurl");
        if (TextUtils.isEmpty(str)) {
            a("Image url cannot be empty.");
        } else if (!URLUtil.isValidUrl(str)) {
            String str2 = "Invalid image url: ";
            String valueOf = String.valueOf(str);
            a(valueOf.length() != 0 ? str2.concat(valueOf) : new String(str2));
        } else {
            String lastPathSegment = Uri.parse(str).getLastPathSegment();
            ax.e();
            if (!jv.c(lastPathSegment)) {
                String str3 = "Image type not recognized: ";
                String valueOf2 = String.valueOf(lastPathSegment);
                a(valueOf2.length() != 0 ? str3.concat(valueOf2) : new String(str3));
                return;
            }
            Resources h = ax.i().h();
            ax.e();
            Builder e = jv.e(this.f3337b);
            e.setTitle(h != null ? h.getString(C0045a.s1) : "Save image");
            e.setMessage(h != null ? h.getString(C0045a.s2) : "Allow Ad to store image in Picture gallery?");
            e.setPositiveButton(h != null ? h.getString(C0045a.s3) : "Accept", new h(this, str, lastPathSegment));
            e.setNegativeButton(h != null ? h.getString(C0045a.s4) : "Decline", new i(this));
            e.create().show();
        }
    }
}
