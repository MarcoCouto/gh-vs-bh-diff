package com.google.android.gms.internal.ads;

import android.os.IInterface;
import android.os.RemoteException;

public interface aqs extends IInterface {
    void a() throws RemoteException;

    void a(aqv aqv) throws RemoteException;

    void a(boolean z) throws RemoteException;

    void b() throws RemoteException;

    boolean c() throws RemoteException;

    int d() throws RemoteException;

    float e() throws RemoteException;

    float f() throws RemoteException;

    float g() throws RemoteException;

    aqv h() throws RemoteException;

    boolean i() throws RemoteException;

    boolean j() throws RemoteException;
}
