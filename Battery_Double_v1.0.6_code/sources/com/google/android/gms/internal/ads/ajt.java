package com.google.android.gms.internal.ads;

import android.content.Context;
import android.view.View;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.WeakHashMap;

@cm
public final class ajt implements akc {

    /* renamed from: a reason: collision with root package name */
    private final Object f2643a = new Object();

    /* renamed from: b reason: collision with root package name */
    private final WeakHashMap<ir, aju> f2644b = new WeakHashMap<>();
    private final ArrayList<aju> c = new ArrayList<>();
    private final Context d;
    private final mu e;
    private final bah f;

    public ajt(Context context, mu muVar) {
        this.d = context.getApplicationContext();
        this.e = muVar;
        this.f = new bah(context.getApplicationContext(), muVar, (String) ape.f().a(asi.f2875a));
    }

    private final boolean e(ir irVar) {
        boolean z;
        synchronized (this.f2643a) {
            aju aju = (aju) this.f2644b.get(irVar);
            z = aju != null && aju.c();
        }
        return z;
    }

    public final void a(aju aju) {
        synchronized (this.f2643a) {
            if (!aju.c()) {
                this.c.remove(aju);
                Iterator it = this.f2644b.entrySet().iterator();
                while (it.hasNext()) {
                    if (((Entry) it.next()).getValue() == aju) {
                        it.remove();
                    }
                }
            }
        }
    }

    public final void a(aot aot, ir irVar) {
        a(aot, irVar, irVar.f3396b.getView());
    }

    public final void a(aot aot, ir irVar, View view) {
        a(aot, irVar, (alg) new aka(view, irVar), (qn) null);
    }

    public final void a(aot aot, ir irVar, View view, qn qnVar) {
        a(aot, irVar, (alg) new aka(view, irVar), qnVar);
    }

    public final void a(aot aot, ir irVar, alg alg, qn qnVar) {
        aju aju;
        synchronized (this.f2643a) {
            if (e(irVar)) {
                aju = (aju) this.f2644b.get(irVar);
            } else {
                aju = new aju(this.d, aot, irVar, this.e, alg);
                aju.a((akc) this);
                this.f2644b.put(irVar, aju);
                this.c.add(aju);
            }
            if (qnVar != null) {
                aju.a((akq) new akd(aju, qnVar));
            } else {
                aju.a((akq) new akh(aju, this.f, this.d));
            }
        }
    }

    public final void a(ir irVar) {
        synchronized (this.f2643a) {
            aju aju = (aju) this.f2644b.get(irVar);
            if (aju != null) {
                aju.b();
            }
        }
    }

    public final void b(ir irVar) {
        synchronized (this.f2643a) {
            aju aju = (aju) this.f2644b.get(irVar);
            if (aju != null) {
                aju.d();
            }
        }
    }

    public final void c(ir irVar) {
        synchronized (this.f2643a) {
            aju aju = (aju) this.f2644b.get(irVar);
            if (aju != null) {
                aju.e();
            }
        }
    }

    public final void d(ir irVar) {
        synchronized (this.f2643a) {
            aju aju = (aju) this.f2644b.get(irVar);
            if (aju != null) {
                aju.f();
            }
        }
    }
}
