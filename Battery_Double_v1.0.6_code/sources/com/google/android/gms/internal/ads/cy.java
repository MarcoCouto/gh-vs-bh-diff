package com.google.android.gms.internal.ads;

final class cy implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ oa f3244a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ cp f3245b;

    cy(cp cpVar, oa oaVar) {
        this.f3245b = cpVar;
        this.f3244a = oaVar;
    }

    public final void run() {
        synchronized (this.f3245b.d) {
            this.f3245b.f3234a = this.f3245b.a(this.f3245b.c.j, this.f3244a);
            if (this.f3245b.f3234a == null) {
                this.f3245b.a(0, "Could not start the ad request service.");
                jv.f3440a.removeCallbacks(this.f3245b.i);
            }
        }
    }
}
