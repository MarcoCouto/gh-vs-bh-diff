package com.google.android.gms.internal.ads;

import java.lang.reflect.InvocationTargetException;

public final class aiw extends ajj {
    private static volatile String d = null;
    private static final Object e = new Object();

    public aiw(ahy ahy, String str, String str2, zz zzVar, int i, int i2) {
        super(ahy, str, str2, zzVar, i, 1);
    }

    /* access modifiers changed from: protected */
    public final void a() throws IllegalAccessException, InvocationTargetException {
        this.f2637b.f3830a = "E";
        if (d == null) {
            synchronized (e) {
                if (d == null) {
                    d = (String) this.c.invoke(null, new Object[0]);
                }
            }
        }
        synchronized (this.f2637b) {
            this.f2637b.f3830a = d;
        }
    }
}
