package com.google.android.gms.internal.ads;

import android.os.Bundle;
import com.google.android.gms.ads.internal.ax;

@cm
public final class je {

    /* renamed from: a reason: collision with root package name */
    private final Object f3418a;

    /* renamed from: b reason: collision with root package name */
    private int f3419b;
    private int c;
    private final jf d;
    private final String e;

    private je(jf jfVar, String str) {
        this.f3418a = new Object();
        this.d = jfVar;
        this.e = str;
    }

    public je(String str) {
        this(ax.j(), str);
    }

    public final String a() {
        return this.e;
    }

    public final void a(int i, int i2) {
        synchronized (this.f3418a) {
            this.f3419b = i;
            this.c = i2;
            this.d.a(this);
        }
    }

    public final Bundle b() {
        Bundle bundle;
        synchronized (this.f3418a) {
            bundle = new Bundle();
            bundle.putInt("pmnli", this.f3419b);
            bundle.putInt("pmnll", this.c);
        }
        return bundle;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        je jeVar = (je) obj;
        return this.e != null ? this.e.equals(jeVar.e) : jeVar.e == null;
    }

    public final int hashCode() {
        if (this.e != null) {
            return this.e.hashCode();
        }
        return 0;
    }
}
