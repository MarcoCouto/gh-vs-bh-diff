package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.Bundle;
import android.os.RemoteException;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.b.i;
import com.google.android.gms.ads.l;
import com.google.android.gms.ads.mediation.MediationBannerAdapter;
import com.google.android.gms.ads.mediation.MediationInterstitialAdapter;
import com.google.android.gms.ads.mediation.MediationNativeAdapter;
import com.google.android.gms.ads.mediation.b;
import com.google.android.gms.ads.mediation.f;
import com.google.android.gms.ads.mediation.g;
import com.google.android.gms.ads.mediation.h;
import com.google.android.gms.ads.mediation.j;
import com.google.android.gms.ads.mediation.k;
import com.google.android.gms.ads.mediation.m;
import com.google.android.gms.ads.reward.mediation.InitializableMediationRewardedVideoAdAdapter;
import com.google.android.gms.ads.reward.mediation.MediationRewardedVideoAdAdapter;
import com.google.android.gms.b.a;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import org.json.JSONObject;

@cm
public final class bdp extends bcv {

    /* renamed from: a reason: collision with root package name */
    private final b f3149a;

    /* renamed from: b reason: collision with root package name */
    private bdq f3150b;

    public bdp(b bVar) {
        this.f3149a = bVar;
    }

    private final Bundle a(String str, aop aop, String str2) throws RemoteException {
        String str3 = "Server parameters: ";
        String valueOf = String.valueOf(str);
        ms.e(valueOf.length() != 0 ? str3.concat(valueOf) : new String(str3));
        try {
            Bundle bundle = new Bundle();
            if (str != null) {
                JSONObject jSONObject = new JSONObject(str);
                Bundle bundle2 = new Bundle();
                Iterator keys = jSONObject.keys();
                while (keys.hasNext()) {
                    String str4 = (String) keys.next();
                    bundle2.putString(str4, jSONObject.getString(str4));
                }
                bundle = bundle2;
            }
            if (this.f3149a instanceof AdMobAdapter) {
                bundle.putString("adJson", str2);
                if (aop != null) {
                    bundle.putInt("tagForChildDirectedTreatment", aop.g);
                }
            }
            return bundle;
        } catch (Throwable th) {
            ms.b("", th);
            throw new RemoteException();
        }
    }

    private static boolean a(aop aop) {
        if (!aop.f) {
            ape.a();
            if (!mh.a()) {
                return false;
            }
        }
        return true;
    }

    public final a a() throws RemoteException {
        if (!(this.f3149a instanceof MediationBannerAdapter)) {
            String str = "Not a MediationBannerAdapter: ";
            String valueOf = String.valueOf(this.f3149a.getClass().getCanonicalName());
            ms.e(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
            throw new RemoteException();
        }
        try {
            return com.google.android.gms.b.b.a(((MediationBannerAdapter) this.f3149a).getBannerView());
        } catch (Throwable th) {
            ms.b("", th);
            throw new RemoteException();
        }
    }

    public final void a(a aVar) throws RemoteException {
        try {
            ((j) this.f3149a).a((Context) com.google.android.gms.b.b.a(aVar));
        } catch (Throwable th) {
            ms.c("Failed", th);
        }
    }

    public final void a(a aVar, aop aop, String str, bcx bcx) throws RemoteException {
        a(aVar, aop, str, (String) null, bcx);
    }

    public final void a(a aVar, aop aop, String str, hl hlVar, String str2) throws RemoteException {
        Bundle bundle;
        com.google.android.gms.ads.mediation.a aVar2;
        if (!(this.f3149a instanceof MediationRewardedVideoAdAdapter)) {
            String str3 = "Not a MediationRewardedVideoAdAdapter: ";
            String valueOf = String.valueOf(this.f3149a.getClass().getCanonicalName());
            ms.e(valueOf.length() != 0 ? str3.concat(valueOf) : new String(str3));
            throw new RemoteException();
        }
        ms.b("Initialize rewarded video adapter.");
        try {
            MediationRewardedVideoAdAdapter mediationRewardedVideoAdAdapter = (MediationRewardedVideoAdAdapter) this.f3149a;
            Bundle a2 = a(str2, aop, (String) null);
            if (aop != null) {
                bdo bdo = new bdo(aop.f2810b == -1 ? null : new Date(aop.f2810b), aop.d, aop.e != null ? new HashSet(aop.e) : null, aop.k, a(aop), aop.g, aop.r);
                if (aop.m != null) {
                    bundle = aop.m.getBundle(mediationRewardedVideoAdAdapter.getClass().getName());
                    aVar2 = bdo;
                } else {
                    bundle = null;
                    aVar2 = bdo;
                }
            } else {
                bundle = null;
                aVar2 = null;
            }
            mediationRewardedVideoAdAdapter.initialize((Context) com.google.android.gms.b.b.a(aVar), aVar2, str, new ho(hlVar), a2, bundle);
        } catch (Throwable th) {
            ms.b("", th);
            throw new RemoteException();
        }
    }

    public final void a(a aVar, aop aop, String str, String str2, bcx bcx) throws RemoteException {
        if (!(this.f3149a instanceof MediationInterstitialAdapter)) {
            String str3 = "Not a MediationInterstitialAdapter: ";
            String valueOf = String.valueOf(this.f3149a.getClass().getCanonicalName());
            ms.e(valueOf.length() != 0 ? str3.concat(valueOf) : new String(str3));
            throw new RemoteException();
        }
        ms.b("Requesting interstitial ad from adapter.");
        try {
            MediationInterstitialAdapter mediationInterstitialAdapter = (MediationInterstitialAdapter) this.f3149a;
            mediationInterstitialAdapter.requestInterstitialAd((Context) com.google.android.gms.b.b.a(aVar), new bdq(bcx), a(str, aop, str2), new bdo(aop.f2810b == -1 ? null : new Date(aop.f2810b), aop.d, aop.e != null ? new HashSet(aop.e) : null, aop.k, a(aop), aop.g, aop.r), aop.m != null ? aop.m.getBundle(mediationInterstitialAdapter.getClass().getName()) : null);
        } catch (Throwable th) {
            ms.b("", th);
            throw new RemoteException();
        }
    }

    public final void a(a aVar, aop aop, String str, String str2, bcx bcx, aul aul, List<String> list) throws RemoteException {
        if (!(this.f3149a instanceof MediationNativeAdapter)) {
            String str3 = "Not a MediationNativeAdapter: ";
            String valueOf = String.valueOf(this.f3149a.getClass().getCanonicalName());
            ms.e(valueOf.length() != 0 ? str3.concat(valueOf) : new String(str3));
            throw new RemoteException();
        }
        try {
            MediationNativeAdapter mediationNativeAdapter = (MediationNativeAdapter) this.f3149a;
            bdt bdt = new bdt(aop.f2810b == -1 ? null : new Date(aop.f2810b), aop.d, aop.e != null ? new HashSet(aop.e) : null, aop.k, a(aop), aop.g, aul, list, aop.r);
            Bundle bundle = aop.m != null ? aop.m.getBundle(mediationNativeAdapter.getClass().getName()) : null;
            this.f3150b = new bdq(bcx);
            mediationNativeAdapter.requestNativeAd((Context) com.google.android.gms.b.b.a(aVar), this.f3150b, a(str, aop, str2), bdt, bundle);
        } catch (Throwable th) {
            ms.b("", th);
            throw new RemoteException();
        }
    }

    public final void a(a aVar, aot aot, aop aop, String str, bcx bcx) throws RemoteException {
        a(aVar, aot, aop, str, null, bcx);
    }

    public final void a(a aVar, aot aot, aop aop, String str, String str2, bcx bcx) throws RemoteException {
        if (!(this.f3149a instanceof MediationBannerAdapter)) {
            String str3 = "Not a MediationBannerAdapter: ";
            String valueOf = String.valueOf(this.f3149a.getClass().getCanonicalName());
            ms.e(valueOf.length() != 0 ? str3.concat(valueOf) : new String(str3));
            throw new RemoteException();
        }
        ms.b("Requesting banner ad from adapter.");
        try {
            MediationBannerAdapter mediationBannerAdapter = (MediationBannerAdapter) this.f3149a;
            mediationBannerAdapter.requestBannerAd((Context) com.google.android.gms.b.b.a(aVar), new bdq(bcx), a(str, aop, str2), l.a(aot.e, aot.f2815b, aot.f2814a), new bdo(aop.f2810b == -1 ? null : new Date(aop.f2810b), aop.d, aop.e != null ? new HashSet(aop.e) : null, aop.k, a(aop), aop.g, aop.r), aop.m != null ? aop.m.getBundle(mediationBannerAdapter.getClass().getName()) : null);
        } catch (Throwable th) {
            ms.b("", th);
            throw new RemoteException();
        }
    }

    public final void a(a aVar, hl hlVar, List<String> list) throws RemoteException {
        if (!(this.f3149a instanceof InitializableMediationRewardedVideoAdAdapter)) {
            String str = "Not an InitializableMediationRewardedVideoAdAdapter: ";
            String valueOf = String.valueOf(this.f3149a.getClass().getCanonicalName());
            ms.e(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
            throw new RemoteException();
        }
        ms.b("Initialize rewarded video adapter.");
        try {
            InitializableMediationRewardedVideoAdAdapter initializableMediationRewardedVideoAdAdapter = (InitializableMediationRewardedVideoAdAdapter) this.f3149a;
            ArrayList arrayList = new ArrayList();
            for (String a2 : list) {
                arrayList.add(a(a2, (aop) null, (String) null));
            }
            initializableMediationRewardedVideoAdAdapter.initialize((Context) com.google.android.gms.b.b.a(aVar), new ho(hlVar), arrayList);
        } catch (Throwable th) {
            ms.c("Could not initialize rewarded video adapter.", th);
            throw new RemoteException();
        }
    }

    public final void a(aop aop, String str) throws RemoteException {
        a(aop, str, (String) null);
    }

    public final void a(aop aop, String str, String str2) throws RemoteException {
        if (!(this.f3149a instanceof MediationRewardedVideoAdAdapter)) {
            String str3 = "Not a MediationRewardedVideoAdAdapter: ";
            String valueOf = String.valueOf(this.f3149a.getClass().getCanonicalName());
            ms.e(valueOf.length() != 0 ? str3.concat(valueOf) : new String(str3));
            throw new RemoteException();
        }
        ms.b("Requesting rewarded video ad from adapter.");
        try {
            MediationRewardedVideoAdAdapter mediationRewardedVideoAdAdapter = (MediationRewardedVideoAdAdapter) this.f3149a;
            mediationRewardedVideoAdAdapter.loadAd(new bdo(aop.f2810b == -1 ? null : new Date(aop.f2810b), aop.d, aop.e != null ? new HashSet(aop.e) : null, aop.k, a(aop), aop.g, aop.r), a(str, aop, str2), aop.m != null ? aop.m.getBundle(mediationRewardedVideoAdAdapter.getClass().getName()) : null);
        } catch (Throwable th) {
            ms.b("", th);
            throw new RemoteException();
        }
    }

    public final void a(boolean z) throws RemoteException {
        if (!(this.f3149a instanceof k)) {
            String str = "Not an OnImmersiveModeUpdatedListener: ";
            String valueOf = String.valueOf(this.f3149a.getClass().getCanonicalName());
            ms.d(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
            return;
        }
        try {
            ((k) this.f3149a).onImmersiveModeUpdated(z);
        } catch (Throwable th) {
            ms.b("", th);
        }
    }

    public final void b() throws RemoteException {
        if (!(this.f3149a instanceof MediationInterstitialAdapter)) {
            String str = "Not a MediationInterstitialAdapter: ";
            String valueOf = String.valueOf(this.f3149a.getClass().getCanonicalName());
            ms.e(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
            throw new RemoteException();
        }
        ms.b("Showing interstitial from adapter.");
        try {
            ((MediationInterstitialAdapter) this.f3149a).showInterstitial();
        } catch (Throwable th) {
            ms.b("", th);
            throw new RemoteException();
        }
    }

    public final void c() throws RemoteException {
        try {
            this.f3149a.onDestroy();
        } catch (Throwable th) {
            ms.b("", th);
            throw new RemoteException();
        }
    }

    public final void d() throws RemoteException {
        try {
            this.f3149a.onPause();
        } catch (Throwable th) {
            ms.b("", th);
            throw new RemoteException();
        }
    }

    public final void e() throws RemoteException {
        try {
            this.f3149a.onResume();
        } catch (Throwable th) {
            ms.b("", th);
            throw new RemoteException();
        }
    }

    public final void f() throws RemoteException {
        if (!(this.f3149a instanceof MediationRewardedVideoAdAdapter)) {
            String str = "Not a MediationRewardedVideoAdAdapter: ";
            String valueOf = String.valueOf(this.f3149a.getClass().getCanonicalName());
            ms.e(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
            throw new RemoteException();
        }
        ms.b("Show rewarded video ad from adapter.");
        try {
            ((MediationRewardedVideoAdAdapter) this.f3149a).showVideo();
        } catch (Throwable th) {
            ms.b("", th);
            throw new RemoteException();
        }
    }

    public final boolean g() throws RemoteException {
        if (!(this.f3149a instanceof MediationRewardedVideoAdAdapter)) {
            String str = "Not a MediationRewardedVideoAdAdapter: ";
            String valueOf = String.valueOf(this.f3149a.getClass().getCanonicalName());
            ms.e(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
            throw new RemoteException();
        }
        ms.b("Check if adapter is initialized.");
        try {
            return ((MediationRewardedVideoAdAdapter) this.f3149a).isInitialized();
        } catch (Throwable th) {
            ms.b("", th);
            throw new RemoteException();
        }
    }

    public final bdd h() {
        f a2 = this.f3150b.a();
        if (a2 instanceof g) {
            return new bdr((g) a2);
        }
        return null;
    }

    public final bdh i() {
        f a2 = this.f3150b.a();
        if (a2 instanceof h) {
            return new bds((h) a2);
        }
        return null;
    }

    public final Bundle j() {
        if (this.f3149a instanceof zzatl) {
            return ((zzatl) this.f3149a).zzmq();
        }
        String str = "Not a v2 MediationBannerAdapter: ";
        String valueOf = String.valueOf(this.f3149a.getClass().getCanonicalName());
        ms.e(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
        return new Bundle();
    }

    public final Bundle k() {
        if (this.f3149a instanceof zzatm) {
            return ((zzatm) this.f3149a).getInterstitialAdapterInfo();
        }
        String str = "Not a v2 MediationInterstitialAdapter: ";
        String valueOf = String.valueOf(this.f3149a.getClass().getCanonicalName());
        ms.e(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
        return new Bundle();
    }

    public final Bundle l() {
        return new Bundle();
    }

    public final boolean m() {
        return this.f3149a instanceof InitializableMediationRewardedVideoAdAdapter;
    }

    public final avt n() {
        i c = this.f3150b.c();
        if (c instanceof avw) {
            return ((avw) c).b();
        }
        return null;
    }

    public final aqs o() {
        if (!(this.f3149a instanceof m)) {
            return null;
        }
        try {
            return ((m) this.f3149a).getVideoController();
        } catch (Throwable th) {
            ms.b("", th);
            return null;
        }
    }

    public final bdk p() {
        com.google.android.gms.ads.mediation.l b2 = this.f3150b.b();
        if (b2 != null) {
            return new beb(b2);
        }
        return null;
    }
}
