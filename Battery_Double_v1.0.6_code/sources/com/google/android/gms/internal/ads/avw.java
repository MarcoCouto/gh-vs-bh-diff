package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.IBinder;
import android.os.RemoteException;
import com.google.android.gms.ads.b.b;
import com.google.android.gms.ads.b.i;
import java.util.WeakHashMap;

@cm
public final class avw implements i {

    /* renamed from: a reason: collision with root package name */
    private static WeakHashMap<IBinder, avw> f2959a = new WeakHashMap<>();

    /* renamed from: b reason: collision with root package name */
    private final avt f2960b;
    private final b c;
    private final com.google.android.gms.ads.i d = new com.google.android.gms.ads.i();

    private avw(avt avt) {
        Context context;
        b bVar = null;
        this.f2960b = avt;
        try {
            context = (Context) com.google.android.gms.b.b.a(avt.e());
        } catch (RemoteException | NullPointerException e) {
            ms.b("", e);
            context = null;
        }
        if (context != null) {
            b bVar2 = new b(context);
            try {
                if (!this.f2960b.a(com.google.android.gms.b.b.a(bVar2))) {
                    bVar2 = null;
                }
                bVar = bVar2;
            } catch (RemoteException e2) {
                ms.b("", e2);
            }
        }
        this.c = bVar;
    }

    public static avw a(avt avt) {
        avw avw;
        synchronized (f2959a) {
            avw = (avw) f2959a.get(avt.asBinder());
            if (avw == null) {
                avw = new avw(avt);
                f2959a.put(avt.asBinder(), avw);
            }
        }
        return avw;
    }

    public final String a() {
        try {
            return this.f2960b.l();
        } catch (RemoteException e) {
            ms.b("", e);
            return null;
        }
    }

    public final avt b() {
        return this.f2960b;
    }
}
