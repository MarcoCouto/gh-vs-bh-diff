package com.google.android.gms.internal.ads;

@cm
public final class hk {

    /* renamed from: a reason: collision with root package name */
    private final bcu f3366a;

    /* renamed from: b reason: collision with root package name */
    private final hd f3367b;

    public hk(bcu bcu, hc hcVar) {
        this.f3366a = bcu;
        this.f3367b = new hd(hcVar);
    }

    public final bcu a() {
        return this.f3366a;
    }

    public final hd b() {
        return this.f3367b;
    }
}
