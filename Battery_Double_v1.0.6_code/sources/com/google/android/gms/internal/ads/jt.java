package com.google.android.gms.internal.ads;

import java.util.concurrent.Callable;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@cm
public final class jt {

    /* renamed from: a reason: collision with root package name */
    public static final ns f3436a = nt.a(new ThreadPoolExecutor(2, Integer.MAX_VALUE, 10, TimeUnit.SECONDS, new SynchronousQueue(), a("Default")));

    /* renamed from: b reason: collision with root package name */
    private static final ns f3437b;

    static {
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(5, 5, 10, TimeUnit.SECONDS, new LinkedBlockingQueue(), a("Loader"));
        threadPoolExecutor.allowCoreThreadTimeOut(true);
        f3437b = nt.a(threadPoolExecutor);
    }

    public static nn<?> a(Runnable runnable) {
        return f3436a.a(runnable);
    }

    public static <T> nn<T> a(Callable<T> callable) {
        return f3436a.a(callable);
    }

    private static ThreadFactory a(String str) {
        return new ju(str);
    }

    public static nn<?> b(Runnable runnable) {
        return f3437b.a(runnable);
    }
}
