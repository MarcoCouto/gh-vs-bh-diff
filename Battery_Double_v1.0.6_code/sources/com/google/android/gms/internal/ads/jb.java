package com.google.android.gms.internal.ads;

import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import com.google.android.gms.ads.internal.ax;

@cm
public final class jb {

    /* renamed from: a reason: collision with root package name */
    int f3415a = -1;

    /* renamed from: b reason: collision with root package name */
    private long f3416b = -1;
    private long c = -1;
    private int d = -1;
    private long e = 0;
    private final Object f = new Object();
    private final String g;
    private int h = 0;
    private int i = 0;

    public jb(String str) {
        this.g = str;
    }

    private static boolean a(Context context) {
        int identifier = context.getResources().getIdentifier("Theme.Translucent", "style", "android");
        if (identifier == 0) {
            jm.d("Please set theme of AdActivity to @android:style/Theme.Translucent to enable transparent background interstitial ad.");
            return false;
        }
        try {
            if (identifier == context.getPackageManager().getActivityInfo(new ComponentName(context.getPackageName(), "com.google.android.gms.ads.AdActivity"), 0).theme) {
                return true;
            }
            jm.d("Please set theme of AdActivity to @android:style/Theme.Translucent to enable transparent background interstitial ad.");
            return false;
        } catch (NameNotFoundException e2) {
            jm.e("Fail to fetch AdActivity theme");
            jm.d("Please set theme of AdActivity to @android:style/Theme.Translucent to enable transparent background interstitial ad.");
            return false;
        }
    }

    public final Bundle a(Context context, String str) {
        Bundle bundle;
        synchronized (this.f) {
            bundle = new Bundle();
            bundle.putString("session_id", this.g);
            bundle.putLong("basets", this.c);
            bundle.putLong("currts", this.f3416b);
            bundle.putString("seq_num", str);
            bundle.putInt("preqs", this.d);
            bundle.putInt("preqs_in_session", this.f3415a);
            bundle.putLong("time_in_session", this.e);
            bundle.putInt("pclick", this.h);
            bundle.putInt("pimp", this.i);
            bundle.putBoolean("support_transparent_background", a(context));
        }
        return bundle;
    }

    public final void a() {
        synchronized (this.f) {
            this.h++;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:28:?, code lost:
        return;
     */
    public final void a(aop aop, long j) {
        synchronized (this.f) {
            long i2 = ax.i().l().i();
            long a2 = ax.l().a();
            if (this.c == -1) {
                if (a2 - i2 > ((Long) ape.f().a(asi.aI)).longValue()) {
                    this.f3415a = -1;
                } else {
                    this.f3415a = ax.i().l().j();
                }
                this.c = j;
                this.f3416b = this.c;
            } else {
                this.f3416b = j;
            }
            if (aop == null || aop.c == null || aop.c.getInt("gw", 2) != 1) {
                this.d++;
                this.f3415a++;
                if (this.f3415a == 0) {
                    this.e = 0;
                    ax.i().l().b(a2);
                } else {
                    this.e = a2 - ax.i().l().k();
                }
            }
        }
    }

    public final void b() {
        synchronized (this.f) {
            this.i++;
        }
    }
}
