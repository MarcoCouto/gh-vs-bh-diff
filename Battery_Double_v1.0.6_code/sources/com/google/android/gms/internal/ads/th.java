package com.google.android.gms.internal.ads;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.b.a;
import com.google.android.gms.b.a.C0046a;

public final class th extends ajk implements tf {
    th(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.omid.IOmid");
    }

    public final a a(String str, a aVar, String str2, String str3, String str4) throws RemoteException {
        Parcel r_ = r_();
        r_.writeString(str);
        ajm.a(r_, (IInterface) aVar);
        r_.writeString(str2);
        r_.writeString(str3);
        r_.writeString(str4);
        Parcel a2 = a(3, r_);
        a a3 = C0046a.a(a2.readStrongBinder());
        a2.recycle();
        return a3;
    }

    public final String a() throws RemoteException {
        Parcel a2 = a(6, r_());
        String readString = a2.readString();
        a2.recycle();
        return readString;
    }

    public final void a(a aVar, a aVar2) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (IInterface) aVar);
        ajm.a(r_, (IInterface) aVar2);
        b(5, r_);
    }

    public final boolean a(a aVar) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (IInterface) aVar);
        Parcel a2 = a(2, r_);
        boolean a3 = ajm.a(a2);
        a2.recycle();
        return a3;
    }

    public final void b(a aVar) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (IInterface) aVar);
        b(4, r_);
    }

    public final void c(a aVar) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (IInterface) aVar);
        b(7, r_);
    }
}
