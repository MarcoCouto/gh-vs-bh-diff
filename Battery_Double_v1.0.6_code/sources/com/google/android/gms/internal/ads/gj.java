package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.b.a.C0046a;

public abstract class gj extends ajl implements gh {
    public gj() {
        super("com.google.android.gms.ads.internal.reward.client.IRewardedVideoAd");
    }

    public static gh a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.ads.internal.reward.client.IRewardedVideoAd");
        return queryLocalInterface instanceof gh ? (gh) queryLocalInterface : new gk(iBinder);
    }

    /* JADX WARNING: type inference failed for: r0v0 */
    /* JADX WARNING: type inference failed for: r0v4, types: [com.google.android.gms.internal.ads.gg] */
    /* JADX WARNING: type inference failed for: r0v5, types: [com.google.android.gms.internal.ads.gf] */
    /* JADX WARNING: type inference failed for: r0v6, types: [com.google.android.gms.internal.ads.gf] */
    /* JADX WARNING: type inference failed for: r0v21, types: [com.google.android.gms.internal.ads.gp] */
    /* JADX WARNING: type inference failed for: r0v22, types: [com.google.android.gms.internal.ads.gn] */
    /* JADX WARNING: type inference failed for: r0v23, types: [com.google.android.gms.internal.ads.gn] */
    /* JADX WARNING: type inference failed for: r0v28 */
    /* JADX WARNING: type inference failed for: r0v29 */
    /* JADX WARNING: type inference failed for: r0v30 */
    /* JADX WARNING: type inference failed for: r0v31 */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r0v0
  assigns: [?[int, float, boolean, short, byte, char, OBJECT, ARRAY], com.google.android.gms.internal.ads.gp, com.google.android.gms.internal.ads.gg, com.google.android.gms.internal.ads.gf, com.google.android.gms.internal.ads.gn]
  uses: [com.google.android.gms.internal.ads.gf, com.google.android.gms.internal.ads.gn]
  mth insns count: 72
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
    	at jadx.core.ProcessClass.process(ProcessClass.java:35)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Unknown variable types count: 5 */
    public final boolean dispatchTransaction(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        ? r0 = 0;
        switch (i) {
            case 1:
                a((gt) ajm.a(parcel, gt.CREATOR));
                parcel2.writeNoException();
                break;
            case 2:
                a();
                parcel2.writeNoException();
                break;
            case 3:
                IBinder readStrongBinder = parcel.readStrongBinder();
                if (readStrongBinder != null) {
                    IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.ads.internal.reward.client.IRewardedVideoAdListener");
                    r0 = queryLocalInterface instanceof gn ? (gn) queryLocalInterface : new gp(readStrongBinder);
                }
                a((gn) r0);
                parcel2.writeNoException();
                break;
            case 5:
                boolean c = c();
                parcel2.writeNoException();
                ajm.a(parcel2, c);
                break;
            case 6:
                d();
                parcel2.writeNoException();
                break;
            case 7:
                e();
                parcel2.writeNoException();
                break;
            case 8:
                f();
                parcel2.writeNoException();
                break;
            case 9:
                a(C0046a.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
            case 10:
                b(C0046a.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
            case 11:
                c(C0046a.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
            case 12:
                String g = g();
                parcel2.writeNoException();
                parcel2.writeString(g);
                break;
            case 13:
                a(parcel.readString());
                parcel2.writeNoException();
                break;
            case 14:
                a(aqb.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
            case 15:
                Bundle b2 = b();
                parcel2.writeNoException();
                ajm.b(parcel2, b2);
                break;
            case 16:
                IBinder readStrongBinder2 = parcel.readStrongBinder();
                if (readStrongBinder2 != null) {
                    IInterface queryLocalInterface2 = readStrongBinder2.queryLocalInterface("com.google.android.gms.ads.internal.reward.client.IRewardedAdSkuListener");
                    r0 = queryLocalInterface2 instanceof gf ? (gf) queryLocalInterface2 : new gg(readStrongBinder2);
                }
                a((gf) r0);
                parcel2.writeNoException();
                break;
            case 34:
                a(ajm.a(parcel));
                parcel2.writeNoException();
                break;
            default:
                return false;
        }
        return true;
    }
}
