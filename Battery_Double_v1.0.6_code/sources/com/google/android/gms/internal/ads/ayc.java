package com.google.android.gms.internal.ads;

import android.os.RemoteException;

final class ayc extends aqf {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ axs f2998a;

    ayc(axs axs) {
        this.f2998a = axs;
    }

    public final void a(String str, String str2) throws RemoteException {
        this.f2998a.f2993a.add(new ayd(this, str, str2));
    }
}
