package com.google.android.gms.internal.ads;

import android.os.RemoteException;
import com.google.android.gms.ads.internal.ax;
import java.util.Random;

final class ayu extends apl {

    /* renamed from: a reason: collision with root package name */
    private final apk f3011a;

    ayu(apk apk) {
        this.f3011a = apk;
    }

    public final void a() throws RemoteException {
        if (aze.a()) {
            int intValue = ((Integer) ape.f().a(asi.bb)).intValue();
            int intValue2 = ((Integer) ape.f().a(asi.bc)).intValue();
            if (intValue <= 0 || intValue2 < 0) {
                ax.r().a();
            } else {
                jv.f3440a.postDelayed(ayv.f3012a, (long) (new Random().nextInt(intValue2 + 1) + intValue));
            }
        }
        this.f3011a.a();
    }

    public final void a(int i) throws RemoteException {
        this.f3011a.a(i);
    }

    public final void b() throws RemoteException {
        this.f3011a.b();
    }

    public final void c() throws RemoteException {
        this.f3011a.c();
    }

    public final void d() throws RemoteException {
        this.f3011a.d();
    }

    public final void e() throws RemoteException {
        this.f3011a.e();
    }

    public final void f() throws RemoteException {
        this.f3011a.f();
    }
}
