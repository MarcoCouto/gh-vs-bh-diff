package com.google.android.gms.internal.ads;

import java.io.IOException;

public final class afs extends afh<afs> {

    /* renamed from: a reason: collision with root package name */
    public String f2538a;

    public afs() {
        this.f2538a = null;
        this.Y = null;
        this.Z = -1;
    }

    /* access modifiers changed from: protected */
    public final int a() {
        int a2 = super.a();
        return this.f2538a != null ? a2 + aff.b(1, this.f2538a) : a2;
    }

    public final /* synthetic */ afn a(afd afd) throws IOException {
        while (true) {
            int a2 = afd.a();
            switch (a2) {
                case 0:
                    break;
                case 10:
                    this.f2538a = afd.e();
                    continue;
                default:
                    if (!super.a(afd, a2)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    public final void a(aff aff) throws IOException {
        if (this.f2538a != null) {
            aff.a(1, this.f2538a);
        }
        super.a(aff);
    }
}
