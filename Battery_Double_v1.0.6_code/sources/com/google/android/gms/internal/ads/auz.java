package com.google.android.gms.internal.ads;

import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.RemoteException;
import com.google.android.gms.ads.b.c.b;
import com.google.android.gms.b.a;

@cm
public final class auz extends b {

    /* renamed from: a reason: collision with root package name */
    private final auw f2951a;

    /* renamed from: b reason: collision with root package name */
    private final Drawable f2952b;
    private final Uri c;
    private final double d;

    /* JADX WARNING: type inference failed for: r1v0 */
    /* JADX WARNING: type inference failed for: r0v0 */
    /* JADX WARNING: type inference failed for: r0v2, types: [android.graphics.drawable.Drawable] */
    /* JADX WARNING: type inference failed for: r0v12, types: [android.graphics.drawable.Drawable] */
    /* JADX WARNING: type inference failed for: r0v13 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 3 */
    public auz(auw auw) {
        ? r0;
        Uri uri;
        ? r1 = 0;
        this.f2951a = auw;
        try {
            a a2 = this.f2951a.a();
            if (a2 != null) {
                r0 = (Drawable) com.google.android.gms.b.b.a(a2);
                this.f2952b = r0;
                uri = this.f2951a.b();
                this.c = uri;
                double d2 = 1.0d;
                d2 = this.f2951a.c();
                this.d = d2;
            }
        } catch (RemoteException e) {
            ms.b("", e);
        }
        r0 = r1;
        this.f2952b = r0;
        try {
            uri = this.f2951a.b();
        } catch (RemoteException e2) {
            ms.b("", e2);
            uri = r1;
        }
        this.c = uri;
        double d22 = 1.0d;
        try {
            d22 = this.f2951a.c();
        } catch (RemoteException e3) {
            ms.b("", e3);
        }
        this.d = d22;
    }

    public final Drawable a() {
        return this.f2952b;
    }

    public final Uri b() {
        return this.c;
    }

    public final double c() {
        return this.d;
    }
}
