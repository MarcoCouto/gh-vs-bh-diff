package com.google.android.gms.internal.ads;

final class apc implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final awb f2830a;

    /* renamed from: b reason: collision with root package name */
    private final bcd f2831b;
    private final Runnable c;

    public apc(ane ane, awb awb, bcd bcd, Runnable runnable) {
        this.f2830a = awb;
        this.f2831b = bcd;
        this.c = runnable;
    }

    public final void run() {
        this.f2830a.g();
        if (this.f2831b.c == null) {
            this.f2830a.a(this.f2831b.f3123a);
        } else {
            this.f2830a.a(this.f2831b.c);
        }
        if (this.f2831b.d) {
            this.f2830a.b("intermediate-response");
        } else {
            this.f2830a.c("done");
        }
        if (this.c != null) {
            this.c.run();
        }
    }
}
