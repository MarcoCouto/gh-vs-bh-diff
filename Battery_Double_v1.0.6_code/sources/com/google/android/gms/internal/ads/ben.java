package com.google.android.gms.internal.ads;

import android.os.RemoteException;

final class ben implements sx<Object, Object> {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ bec f3170a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ bcx f3171b;

    ben(bem bem, bec bec, bcx bcx) {
        this.f3170a = bec;
        this.f3171b = bcx;
    }

    public final void a(String str) {
        try {
            this.f3170a.a(str);
        } catch (RemoteException e) {
            ms.b("", e);
        }
    }
}
