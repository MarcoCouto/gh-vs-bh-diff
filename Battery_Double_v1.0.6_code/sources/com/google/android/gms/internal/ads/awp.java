package com.google.android.gms.internal.ads;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public final class awp extends ajk implements awn {
    awp(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.formats.client.IOnUnifiedNativeAdLoadedListener");
    }

    public final void a(aws aws) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (IInterface) aws);
        b(1, r_);
    }
}
