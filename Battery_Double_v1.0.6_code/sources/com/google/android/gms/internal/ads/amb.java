package com.google.android.gms.internal.ads;

public final class amb {

    /* renamed from: a reason: collision with root package name */
    final long f2721a;

    /* renamed from: b reason: collision with root package name */
    final String f2722b;
    final int c;

    amb(long j, String str, int i) {
        this.f2721a = j;
        this.f2722b = str;
        this.c = i;
    }

    public final boolean equals(Object obj) {
        if (obj == null || !(obj instanceof amb)) {
            return false;
        }
        return ((amb) obj).f2721a == this.f2721a && ((amb) obj).c == this.c;
    }

    public final int hashCode() {
        return (int) this.f2721a;
    }
}
