package com.google.android.gms.internal.ads;

import android.os.Bundle;
import com.google.android.gms.common.internal.e.a;

final class axn implements a {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ axl f2983a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ ny f2984b;
    private final /* synthetic */ axf c;

    axn(axl axl, ny nyVar, axf axf) {
        this.f2983a = axl;
        this.f2984b = nyVar;
        this.c = axf;
    }

    public final void a(int i) {
    }

    public final void a(Bundle bundle) {
        synchronized (this.f2983a.d) {
            if (!this.f2983a.f2981b) {
                this.f2983a.f2981b = true;
                axe d = this.f2983a.f2980a;
                if (d != null) {
                    this.f2984b.a(new axp(this.f2984b, jt.a((Runnable) new axo(this, d, this.f2984b, this.c))), nt.f3559b);
                }
            }
        }
    }
}
