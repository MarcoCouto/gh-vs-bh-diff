package com.google.android.gms.internal.ads;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

@cm
public class oe<T> implements oa<T> {

    /* renamed from: a reason: collision with root package name */
    private final Object f3565a = new Object();

    /* renamed from: b reason: collision with root package name */
    private int f3566b = 0;
    private final BlockingQueue<of> c = new LinkedBlockingQueue();
    private T d;

    public final void a() {
        synchronized (this.f3565a) {
            if (this.f3566b != 0) {
                throw new UnsupportedOperationException();
            }
            this.f3566b = -1;
            for (of ofVar : this.c) {
                ofVar.f3568b.a();
            }
            this.c.clear();
        }
    }

    public final void a(od<T> odVar, ob obVar) {
        synchronized (this.f3565a) {
            if (this.f3566b == 1) {
                odVar.a(this.d);
            } else if (this.f3566b == -1) {
                obVar.a();
            } else if (this.f3566b == 0) {
                this.c.add(new of(this, odVar, obVar));
            }
        }
    }

    public final void a(T t) {
        synchronized (this.f3565a) {
            if (this.f3566b != 0) {
                throw new UnsupportedOperationException();
            }
            this.d = t;
            this.f3566b = 1;
            for (of ofVar : this.c) {
                ofVar.f3567a.a(t);
            }
            this.c.clear();
        }
    }

    public final int b() {
        return this.f3566b;
    }
}
