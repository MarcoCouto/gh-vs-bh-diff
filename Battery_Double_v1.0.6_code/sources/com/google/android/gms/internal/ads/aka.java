package com.google.android.gms.internal.ads;

import android.view.View;
import java.lang.ref.WeakReference;

public final class aka implements alg {

    /* renamed from: a reason: collision with root package name */
    private final WeakReference<View> f2655a;

    /* renamed from: b reason: collision with root package name */
    private final WeakReference<ir> f2656b;

    public aka(View view, ir irVar) {
        this.f2655a = new WeakReference<>(view);
        this.f2656b = new WeakReference<>(irVar);
    }

    public final View a() {
        return (View) this.f2655a.get();
    }

    public final boolean b() {
        return this.f2655a.get() == null || this.f2656b.get() == null;
    }

    public final alg c() {
        return new ajz((View) this.f2655a.get(), (ir) this.f2656b.get());
    }
}
