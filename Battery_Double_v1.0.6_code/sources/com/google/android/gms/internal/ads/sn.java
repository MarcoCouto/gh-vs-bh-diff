package com.google.android.gms.internal.ads;

import android.annotation.TargetApi;
import android.net.Uri;
import android.webkit.WebResourceRequest;
import java.util.Collections;
import java.util.Map;

@cm
public final class sn {

    /* renamed from: a reason: collision with root package name */
    public final String f3691a;

    /* renamed from: b reason: collision with root package name */
    public final Uri f3692b;
    public final Map<String, String> c;
    private final String d;

    @TargetApi(21)
    public sn(WebResourceRequest webResourceRequest) {
        this(webResourceRequest.getUrl().toString(), webResourceRequest.getUrl(), webResourceRequest.getMethod(), webResourceRequest.getRequestHeaders());
    }

    public sn(String str) {
        this(str, Uri.parse(str), null, null);
    }

    private sn(String str, Uri uri, String str2, Map<String, String> map) {
        this.f3691a = str;
        this.f3692b = uri;
        if (str2 == null) {
            str2 = "GET";
        }
        this.d = str2;
        if (map == null) {
            map = Collections.emptyMap();
        }
        this.c = map;
    }
}
