package com.google.android.gms.internal.ads;

import android.text.TextUtils;

public final class aqd {

    /* renamed from: a reason: collision with root package name */
    private final String f2839a;

    /* renamed from: b reason: collision with root package name */
    private final String f2840b;

    public aqd(String str, String str2) {
        this.f2839a = str;
        this.f2840b = str2;
    }

    public final String a() {
        return this.f2839a;
    }

    public final String b() {
        return this.f2840b;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        aqd aqd = (aqd) obj;
        return TextUtils.equals(this.f2839a, aqd.f2839a) && TextUtils.equals(this.f2840b, aqd.f2840b);
    }

    public final int hashCode() {
        return (this.f2839a.hashCode() * 31) + this.f2840b.hashCode();
    }

    public final String toString() {
        String str = this.f2839a;
        String str2 = this.f2840b;
        return new StringBuilder(String.valueOf(str).length() + 20 + String.valueOf(str2).length()).append("Header[name=").append(str).append(",value=").append(str2).append("]").toString();
    }
}
