package com.google.android.gms.internal.ads;

import java.util.HashMap;

public final class ahx extends agf<Integer, Long> {

    /* renamed from: a reason: collision with root package name */
    public Long f2604a;

    /* renamed from: b reason: collision with root package name */
    public Long f2605b;

    public ahx() {
    }

    public ahx(String str) {
        a(str);
    }

    /* access modifiers changed from: protected */
    public final HashMap<Integer, Long> a() {
        HashMap<Integer, Long> hashMap = new HashMap<>();
        hashMap.put(Integer.valueOf(0), this.f2604a);
        hashMap.put(Integer.valueOf(1), this.f2605b);
        return hashMap;
    }

    /* access modifiers changed from: protected */
    public final void a(String str) {
        HashMap b2 = b(str);
        if (b2 != null) {
            this.f2604a = (Long) b2.get(Integer.valueOf(0));
            this.f2605b = (Long) b2.get(Integer.valueOf(1));
        }
    }
}
