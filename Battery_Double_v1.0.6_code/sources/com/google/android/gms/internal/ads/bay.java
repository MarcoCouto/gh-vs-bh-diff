package com.google.android.gms.internal.ads;

import com.google.android.gms.common.internal.aa;

public final class bay extends oe<azv> {

    /* renamed from: a reason: collision with root package name */
    private final Object f3091a = new Object();
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public la<azv> f3092b;
    private boolean c;
    private int d;

    public bay(la<azv> laVar) {
        this.f3092b = laVar;
        this.c = false;
        this.d = 0;
    }

    private final void f() {
        synchronized (this.f3091a) {
            aa.a(this.d >= 0);
            if (!this.c || this.d != 0) {
                jm.a("There are still references to the engine. Not destroying.");
            } else {
                jm.a("No reference is left (including root). Cleaning up engine.");
                a(new bbb(this), new oc());
            }
        }
    }

    public final bau c() {
        bau bau = new bau(this);
        synchronized (this.f3091a) {
            a(new baz(this, bau), new bba(this, bau));
            aa.a(this.d >= 0);
            this.d++;
        }
        return bau;
    }

    /* access modifiers changed from: protected */
    public final void d() {
        synchronized (this.f3091a) {
            aa.a(this.d > 0);
            jm.a("Releasing 1 reference for JS Engine");
            this.d--;
            f();
        }
    }

    public final void e() {
        boolean z = true;
        synchronized (this.f3091a) {
            if (this.d < 0) {
                z = false;
            }
            aa.a(z);
            jm.a("Releasing root reference. JS Engine will be destroyed once other references are released.");
            this.c = true;
            f();
        }
    }
}
