package com.google.android.gms.internal.ads;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import com.google.android.gms.ads.d.a;
import com.google.android.gms.ads.mediation.b;
import java.util.Collections;
import java.util.Date;
import java.util.Map;
import java.util.Set;

@cm
public final class ara {

    /* renamed from: a reason: collision with root package name */
    private final Date f2844a;

    /* renamed from: b reason: collision with root package name */
    private final String f2845b;
    private final int c;
    private final Set<String> d;
    private final Location e;
    private final boolean f;
    private final Bundle g;
    private final Map<Class<? extends Object>, Object> h;
    private final String i;
    private final String j;
    private final a k;
    private final int l;
    private final Set<String> m;
    private final Bundle n;
    private final Set<String> o;
    private final boolean p;

    public ara(arb arb) {
        this(arb, null);
    }

    public ara(arb arb, a aVar) {
        this.f2844a = arb.g;
        this.f2845b = arb.h;
        this.c = arb.i;
        this.d = Collections.unmodifiableSet(arb.f2846a);
        this.e = arb.j;
        this.f = arb.k;
        this.g = arb.f2847b;
        this.h = Collections.unmodifiableMap(arb.c);
        this.i = arb.l;
        this.j = arb.m;
        this.k = aVar;
        this.l = arb.n;
        this.m = Collections.unmodifiableSet(arb.d);
        this.n = arb.e;
        this.o = Collections.unmodifiableSet(arb.f);
        this.p = arb.o;
    }

    public final Bundle a(Class<? extends b> cls) {
        return this.g.getBundle(cls.getName());
    }

    public final Date a() {
        return this.f2844a;
    }

    public final boolean a(Context context) {
        Set<String> set = this.m;
        ape.a();
        return set.contains(mh.a(context));
    }

    public final String b() {
        return this.f2845b;
    }

    public final int c() {
        return this.c;
    }

    public final Set<String> d() {
        return this.d;
    }

    public final Location e() {
        return this.e;
    }

    public final boolean f() {
        return this.f;
    }

    public final String g() {
        return this.i;
    }

    public final String h() {
        return this.j;
    }

    public final a i() {
        return this.k;
    }

    public final Map<Class<? extends Object>, Object> j() {
        return this.h;
    }

    public final Bundle k() {
        return this.g;
    }

    public final int l() {
        return this.l;
    }

    public final Bundle m() {
        return this.n;
    }

    public final Set<String> n() {
        return this.o;
    }

    public final boolean o() {
        return this.p;
    }
}
