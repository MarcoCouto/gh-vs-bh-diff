package com.google.android.gms.internal.ads;

import android.util.JsonWriter;
import java.util.Map;

final /* synthetic */ class mo implements mr {

    /* renamed from: a reason: collision with root package name */
    private final int f3523a;

    /* renamed from: b reason: collision with root package name */
    private final Map f3524b;

    mo(int i, Map map) {
        this.f3523a = i;
        this.f3524b = map;
    }

    public final void a(JsonWriter jsonWriter) {
        ml.a(this.f3523a, this.f3524b, jsonWriter);
    }
}
