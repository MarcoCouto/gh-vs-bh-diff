package com.google.android.gms.internal.ads;

import android.annotation.TargetApi;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Looper;
import android.os.SystemClock;
import com.google.android.gms.common.util.n;
import java.io.InputStream;

final class bf implements ll<atm> {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ boolean f3183a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ double f3184b;
    private final /* synthetic */ boolean c;
    private final /* synthetic */ String d;
    private final /* synthetic */ ay e;

    bf(ay ayVar, boolean z, double d2, boolean z2, String str) {
        this.e = ayVar;
        this.f3183a = z;
        this.f3184b = d2;
        this.c = z2;
        this.d = str;
    }

    /* access modifiers changed from: private */
    @TargetApi(19)
    /* renamed from: b */
    public final atm a(InputStream inputStream) {
        Bitmap bitmap;
        Options options = new Options();
        options.inDensity = (int) (160.0d * this.f3184b);
        if (!this.c) {
            options.inPreferredConfig = Config.RGB_565;
        }
        long uptimeMillis = SystemClock.uptimeMillis();
        try {
            bitmap = BitmapFactory.decodeStream(inputStream, null, options);
        } catch (Exception e2) {
            jm.b("Error grabbing image.", e2);
            bitmap = null;
        }
        if (bitmap == null) {
            this.e.a(2, this.f3183a);
            return null;
        }
        long uptimeMillis2 = SystemClock.uptimeMillis();
        if (n.g() && jm.a()) {
            int width = bitmap.getWidth();
            int height = bitmap.getHeight();
            jm.a("Decoded image w: " + width + " h:" + height + " bytes: " + bitmap.getAllocationByteCount() + " time: " + (uptimeMillis2 - uptimeMillis) + " on ui thread: " + (Looper.getMainLooper().getThread() == Thread.currentThread()));
        }
        return new atm(new BitmapDrawable(Resources.getSystem(), bitmap), Uri.parse(this.d), this.f3184b);
    }

    public final /* synthetic */ Object a() {
        this.e.a(2, this.f3183a);
        return null;
    }
}
