package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.Build.VERSION;
import com.google.android.gms.ads.internal.ax;
import com.hmatalonga.greenhub.Config;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.Future;

@cm
public final class ask {

    /* renamed from: a reason: collision with root package name */
    private String f2878a;

    /* renamed from: b reason: collision with root package name */
    private Map<String, String> f2879b;
    private Context c = null;
    private String d = null;

    public ask(Context context, String str) {
        this.c = context;
        this.d = str;
        this.f2878a = (String) ape.f().a(asi.O);
        this.f2879b = new LinkedHashMap();
        this.f2879b.put("s", "gmob_sdk");
        this.f2879b.put("v", "3");
        this.f2879b.put("os", VERSION.RELEASE);
        this.f2879b.put("sdk", VERSION.SDK);
        ax.e();
        this.f2879b.put("device", jv.b());
        this.f2879b.put("app", context.getApplicationContext() != null ? context.getApplicationContext().getPackageName() : context.getPackageName());
        Map<String, String> map = this.f2879b;
        String str2 = "is_lite_sdk";
        ax.e();
        map.put(str2, jv.k(context) ? "1" : Config.NOTIFICATION_DEFAULT_PRIORITY);
        Future a2 = ax.p().a(this.c);
        try {
            a2.get();
            this.f2879b.put("network_coarse", Integer.toString(((fi) a2.get()).n));
            this.f2879b.put("network_fine", Integer.toString(((fi) a2.get()).o));
        } catch (Exception e) {
            ax.i().a((Throwable) e, "CsiConfiguration.CsiConfiguration");
        }
    }

    /* access modifiers changed from: 0000 */
    public final String a() {
        return this.f2878a;
    }

    /* access modifiers changed from: 0000 */
    public final Context b() {
        return this.c;
    }

    /* access modifiers changed from: 0000 */
    public final String c() {
        return this.d;
    }

    /* access modifiers changed from: 0000 */
    public final Map<String, String> d() {
        return this.f2879b;
    }
}
