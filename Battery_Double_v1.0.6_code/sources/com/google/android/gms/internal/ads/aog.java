package com.google.android.gms.internal.ads;

import java.io.IOException;

public final class aog extends afh<aog> {

    /* renamed from: a reason: collision with root package name */
    private Integer f2800a;

    /* renamed from: b reason: collision with root package name */
    private aob f2801b;
    private Integer c;
    private Integer d;
    private Integer e;
    private Long f;

    public aog() {
        this.f2800a = null;
        this.f2801b = null;
        this.c = null;
        this.d = null;
        this.e = null;
        this.f = null;
        this.Y = null;
        this.Z = -1;
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public final aog a(afd afd) throws IOException {
        while (true) {
            int a2 = afd.a();
            switch (a2) {
                case 0:
                    break;
                case 8:
                    int j = afd.j();
                    try {
                        this.f2800a = Integer.valueOf(anf.a(afd.g()));
                        continue;
                    } catch (IllegalArgumentException e2) {
                        afd.e(j);
                        a(afd, a2);
                        break;
                    }
                case 18:
                    if (this.f2801b == null) {
                        this.f2801b = new aob();
                    }
                    afd.a((afn) this.f2801b);
                    continue;
                case 24:
                    this.c = Integer.valueOf(afd.g());
                    continue;
                case 32:
                    this.d = Integer.valueOf(afd.g());
                    continue;
                case 40:
                    this.e = Integer.valueOf(afd.g());
                    continue;
                case 48:
                    this.f = Long.valueOf(afd.h());
                    continue;
                default:
                    if (!super.a(afd, a2)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    /* access modifiers changed from: protected */
    public final int a() {
        int a2 = super.a();
        if (this.f2800a != null) {
            a2 += aff.b(1, this.f2800a.intValue());
        }
        if (this.f2801b != null) {
            a2 += aff.b(2, (afn) this.f2801b);
        }
        if (this.c != null) {
            a2 += aff.b(3, this.c.intValue());
        }
        if (this.d != null) {
            a2 += aff.b(4, this.d.intValue());
        }
        if (this.e != null) {
            a2 += aff.b(5, this.e.intValue());
        }
        return this.f != null ? a2 + aff.c(6, this.f.longValue()) : a2;
    }

    public final void a(aff aff) throws IOException {
        if (this.f2800a != null) {
            aff.a(1, this.f2800a.intValue());
        }
        if (this.f2801b != null) {
            aff.a(2, (afn) this.f2801b);
        }
        if (this.c != null) {
            aff.a(3, this.c.intValue());
        }
        if (this.d != null) {
            aff.a(4, this.d.intValue());
        }
        if (this.e != null) {
            aff.a(5, this.e.intValue());
        }
        if (this.f != null) {
            aff.a(6, this.f.longValue());
        }
        super.a(aff);
    }
}
