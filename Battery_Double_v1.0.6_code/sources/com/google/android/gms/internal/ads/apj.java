package com.google.android.gms.internal.ads;

import android.os.IBinder;
import android.os.RemoteException;

public final class apj extends ajk implements aph {
    apj(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.client.IAdClickListener");
    }

    public final void a() throws RemoteException {
        b(1, r_());
    }
}
