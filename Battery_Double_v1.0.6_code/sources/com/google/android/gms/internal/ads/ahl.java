package com.google.android.gms.internal.ads;

import java.util.HashMap;

public final class ahl extends agf<Integer, Long> {

    /* renamed from: a reason: collision with root package name */
    public long f2589a;

    /* renamed from: b reason: collision with root package name */
    public long f2590b;

    public ahl() {
        this.f2589a = -1;
        this.f2590b = -1;
    }

    public ahl(String str) {
        this();
        a(str);
    }

    /* access modifiers changed from: protected */
    public final HashMap<Integer, Long> a() {
        HashMap<Integer, Long> hashMap = new HashMap<>();
        hashMap.put(Integer.valueOf(0), Long.valueOf(this.f2589a));
        hashMap.put(Integer.valueOf(1), Long.valueOf(this.f2590b));
        return hashMap;
    }

    /* access modifiers changed from: protected */
    public final void a(String str) {
        HashMap b2 = b(str);
        if (b2 != null) {
            this.f2589a = ((Long) b2.get(Integer.valueOf(0))).longValue();
            this.f2590b = ((Long) b2.get(Integer.valueOf(1))).longValue();
        }
    }
}
