package com.google.android.gms.internal.ads;

import android.annotation.TargetApi;
import android.webkit.WebView;
import com.google.android.gms.common.util.n;

@cm
final class sr {

    /* renamed from: a reason: collision with root package name */
    private static Boolean f3695a;

    private sr() {
    }

    @TargetApi(19)
    static void a(WebView webView, String str) {
        if (!n.g() || !a(webView)) {
            String str2 = "javascript:";
            String valueOf = String.valueOf(str);
            webView.loadUrl(valueOf.length() != 0 ? str2.concat(valueOf) : new String(str2));
            return;
        }
        webView.evaluateJavascript(str, null);
    }

    @TargetApi(19)
    private static boolean a(WebView webView) {
        boolean booleanValue;
        synchronized (sr.class) {
            if (f3695a == null) {
                try {
                    webView.evaluateJavascript("(function(){})()", null);
                    f3695a = Boolean.valueOf(true);
                } catch (IllegalStateException e) {
                    f3695a = Boolean.valueOf(false);
                }
            }
            booleanValue = f3695a.booleanValue();
        }
        return booleanValue;
    }
}
