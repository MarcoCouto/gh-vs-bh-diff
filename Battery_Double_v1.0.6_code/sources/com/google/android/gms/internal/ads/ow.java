package com.google.android.gms.internal.ads;

import android.annotation.TargetApi;
import android.content.Context;
import android.view.TextureView;

@TargetApi(14)
@cm
public abstract class ow extends TextureView implements pr {

    /* renamed from: a reason: collision with root package name */
    protected final pf f3584a = new pf();

    /* renamed from: b reason: collision with root package name */
    protected final pp f3585b;

    public ow(Context context) {
        super(context);
        this.f3585b = new pp(context, this);
    }

    public abstract String a();

    public abstract void a(float f, float f2);

    public abstract void a(int i);

    public abstract void a(ov ovVar);

    public abstract void b();

    public abstract void c();

    public abstract void d();

    public abstract void e();

    public abstract int getCurrentPosition();

    public abstract int getDuration();

    public abstract int getVideoHeight();

    public abstract int getVideoWidth();

    public abstract void setVideoPath(String str);
}
