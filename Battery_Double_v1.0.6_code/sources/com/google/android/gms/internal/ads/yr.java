package com.google.android.gms.internal.ads;

final /* synthetic */ class yr {

    /* renamed from: a reason: collision with root package name */
    static final /* synthetic */ int[] f3801a = new int[yt.values().length];

    /* renamed from: b reason: collision with root package name */
    static final /* synthetic */ int[] f3802b = new int[ys.values().length];

    static {
        try {
            f3802b[ys.NIST_P256.ordinal()] = 1;
        } catch (NoSuchFieldError e) {
        }
        try {
            f3802b[ys.NIST_P384.ordinal()] = 2;
        } catch (NoSuchFieldError e2) {
        }
        try {
            f3802b[ys.NIST_P521.ordinal()] = 3;
        } catch (NoSuchFieldError e3) {
        }
        try {
            f3801a[yt.UNCOMPRESSED.ordinal()] = 1;
        } catch (NoSuchFieldError e4) {
        }
        try {
            f3801a[yt.COMPRESSED.ordinal()] = 2;
        } catch (NoSuchFieldError e5) {
        }
    }
}
