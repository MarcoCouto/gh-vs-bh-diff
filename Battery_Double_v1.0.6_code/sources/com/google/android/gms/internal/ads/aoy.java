package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.RemoteException;
import com.google.android.gms.b.b;

final class aoy extends a<apv> {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ Context f2822a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ aot f2823b;
    private final /* synthetic */ String c;
    private final /* synthetic */ bcr d;
    private final /* synthetic */ aox e;

    aoy(aox aox, Context context, aot aot, String str, bcr bcr) {
        this.e = aox;
        this.f2822a = context;
        this.f2823b = aot;
        this.c = str;
        this.d = bcr;
        super();
    }

    public final /* synthetic */ Object a() throws RemoteException {
        apv a2 = this.e.c.a(this.f2822a, this.f2823b, this.c, this.d, 1);
        if (a2 != null) {
            return a2;
        }
        aox.a(this.f2822a, "banner");
        return new arl();
    }

    public final /* synthetic */ Object a(aqh aqh) throws RemoteException {
        return aqh.createBannerAdManager(b.a(this.f2822a), this.f2823b, this.c, this.d, 12451000);
    }
}
