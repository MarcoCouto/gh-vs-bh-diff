package com.google.android.gms.internal.ads;

import java.io.IOException;
import java.text.Normalizer;
import java.text.Normalizer.Form;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;

@cm
public final class ame {

    /* renamed from: a reason: collision with root package name */
    private final alt f2726a;

    /* renamed from: b reason: collision with root package name */
    private final int f2727b;
    private String c;
    private String d;
    private final boolean e = false;
    private final int f;
    private final int g;

    public ame(int i, int i2, int i3) {
        this.f2727b = i;
        if (i2 > 64 || i2 < 0) {
            this.f = 64;
        } else {
            this.f = i2;
        }
        if (i3 <= 0) {
            this.g = 1;
        } else {
            this.g = i3;
        }
        this.f2726a = new amc(this.f);
    }

    /* JADX WARNING: Removed duplicated region for block: B:35:0x00af  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x00d0 A[LOOP:0: B:1:0x0012->B:49:0x00d0, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x0115 A[EDGE_INSN: B:77:0x0115->B:64:0x0115 ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x010f A[SYNTHETIC] */
    public final String a(ArrayList<String> arrayList, ArrayList<als> arrayList2) {
        boolean z;
        String str;
        String[] a2;
        boolean z2;
        Collections.sort(arrayList2, new amf(this));
        HashSet hashSet = new HashSet();
        int i = 0;
        while (i < arrayList2.size()) {
            String[] split = Normalizer.normalize((CharSequence) arrayList.get(((als) arrayList2.get(i)).e()), Form.NFKC).toLowerCase(Locale.US).split("\n");
            if (split.length != 0) {
                int i2 = 0;
                while (true) {
                    if (i2 >= split.length) {
                        break;
                    }
                    String str2 = split[i2];
                    if (str2.indexOf("'") != -1) {
                        StringBuilder sb = new StringBuilder(str2);
                        int i3 = 1;
                        boolean z3 = false;
                        while (i3 + 2 <= sb.length()) {
                            if (sb.charAt(i3) == '\'') {
                                if (sb.charAt(i3 - 1) == ' ' || !((sb.charAt(i3 + 1) == 's' || sb.charAt(i3 + 1) == 'S') && (i3 + 2 == sb.length() || sb.charAt(i3 + 2) == ' '))) {
                                    sb.setCharAt(i3, ' ');
                                } else {
                                    sb.insert(i3, ' ');
                                    i3 += 2;
                                }
                                z3 = true;
                            }
                            i3++;
                        }
                        str = z3 ? sb.toString() : null;
                        if (str != null) {
                            this.d = str;
                            a2 = alx.a(str, true);
                            if (a2.length < this.g) {
                                int i4 = 0;
                                while (true) {
                                    if (i4 >= a2.length) {
                                        break;
                                    }
                                    String str3 = "";
                                    int i5 = 0;
                                    while (true) {
                                        if (i5 >= this.g) {
                                            z2 = true;
                                            break;
                                        } else if (i4 + i5 >= a2.length) {
                                            z2 = false;
                                            break;
                                        } else {
                                            if (i5 > 0) {
                                                str3 = String.valueOf(str3).concat(" ");
                                            }
                                            String valueOf = String.valueOf(str3);
                                            String valueOf2 = String.valueOf(a2[i4 + i5]);
                                            str3 = valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf);
                                            i5++;
                                        }
                                    }
                                    if (!z2) {
                                        break;
                                    }
                                    hashSet.add(str3);
                                    if (hashSet.size() >= this.f2727b) {
                                        z = false;
                                        break;
                                    }
                                    i4++;
                                }
                                if (hashSet.size() >= this.f2727b) {
                                    z = false;
                                    break;
                                }
                            }
                            i2++;
                        }
                    }
                    str = str2;
                    a2 = alx.a(str, true);
                    if (a2.length < this.g) {
                    }
                    i2++;
                }
                if (z) {
                    break;
                }
                i++;
            }
            z = true;
            if (z) {
            }
        }
        alw alw = new alw();
        this.c = "";
        Iterator it = hashSet.iterator();
        while (it.hasNext()) {
            try {
                alw.a(this.f2726a.a((String) it.next()));
            } catch (IOException e2) {
                jm.b("Error while writing hash to byteStream", e2);
            }
        }
        return alw.toString();
    }
}
