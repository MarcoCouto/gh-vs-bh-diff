package com.google.android.gms.internal.ads;

import android.view.MotionEvent;
import android.view.View;

final class auo implements atw {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ View f2941a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ aun f2942b;

    auo(aun aun, View view) {
        this.f2942b = aun;
        this.f2941a = view;
    }

    public final void a() {
        this.f2942b.onClick(this.f2941a);
    }

    public final void a(MotionEvent motionEvent) {
        this.f2942b.onTouch(null, motionEvent);
    }
}
