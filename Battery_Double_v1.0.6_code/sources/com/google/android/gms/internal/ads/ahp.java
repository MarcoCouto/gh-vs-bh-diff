package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.app.Application.ActivityLifecycleCallbacks;

final class ahp implements ahu {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ Activity f2596a;

    ahp(ahm ahm, Activity activity) {
        this.f2596a = activity;
    }

    public final void a(ActivityLifecycleCallbacks activityLifecycleCallbacks) {
        activityLifecycleCallbacks.onActivityResumed(this.f2596a);
    }
}
