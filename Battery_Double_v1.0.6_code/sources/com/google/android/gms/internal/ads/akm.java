package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.gmsg.ae;
import java.util.Map;

final class akm implements ae<bbe> {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ akh f2670a;

    akm(akh akh) {
        this.f2670a = akh;
    }

    public final /* synthetic */ void zza(Object obj, Map map) {
        if (this.f2670a.f2664a.a(map)) {
            this.f2670a.f2664a.b(map);
        }
    }
}
