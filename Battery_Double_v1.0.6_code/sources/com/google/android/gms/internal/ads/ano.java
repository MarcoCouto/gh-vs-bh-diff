package com.google.android.gms.internal.ads;

import java.io.IOException;

public final class ano extends afh<ano> {

    /* renamed from: a reason: collision with root package name */
    private String f2766a;

    /* renamed from: b reason: collision with root package name */
    private Integer f2767b;
    private int[] c;
    private anx d;

    public ano() {
        this.f2766a = null;
        this.f2767b = null;
        this.c = afq.f2534a;
        this.d = null;
        this.Y = null;
        this.Z = -1;
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public final ano a(afd afd) throws IOException {
        while (true) {
            int a2 = afd.a();
            switch (a2) {
                case 0:
                    break;
                case 10:
                    this.f2766a = afd.e();
                    continue;
                case 16:
                    int j = afd.j();
                    try {
                        this.f2767b = Integer.valueOf(anf.a(afd.g()));
                        continue;
                    } catch (IllegalArgumentException e) {
                        afd.e(j);
                        a(afd, a2);
                        break;
                    }
                case 24:
                    int a3 = afq.a(afd, 24);
                    int length = this.c == null ? 0 : this.c.length;
                    int[] iArr = new int[(a3 + length)];
                    if (length != 0) {
                        System.arraycopy(this.c, 0, iArr, 0, length);
                    }
                    while (length < iArr.length - 1) {
                        iArr[length] = afd.g();
                        afd.a();
                        length++;
                    }
                    iArr[length] = afd.g();
                    this.c = iArr;
                    continue;
                case 26:
                    int c2 = afd.c(afd.g());
                    int j2 = afd.j();
                    int i = 0;
                    while (afd.i() > 0) {
                        afd.g();
                        i++;
                    }
                    afd.e(j2);
                    int length2 = this.c == null ? 0 : this.c.length;
                    int[] iArr2 = new int[(i + length2)];
                    if (length2 != 0) {
                        System.arraycopy(this.c, 0, iArr2, 0, length2);
                    }
                    while (length2 < iArr2.length) {
                        iArr2[length2] = afd.g();
                        length2++;
                    }
                    this.c = iArr2;
                    afd.d(c2);
                    continue;
                case 34:
                    if (this.d == null) {
                        this.d = new anx();
                    }
                    afd.a((afn) this.d);
                    continue;
                default:
                    if (!super.a(afd, a2)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    /* access modifiers changed from: protected */
    public final int a() {
        int a2 = super.a();
        if (this.f2766a != null) {
            a2 += aff.b(1, this.f2766a);
        }
        if (this.f2767b != null) {
            a2 += aff.b(2, this.f2767b.intValue());
        }
        if (this.c != null && this.c.length > 0) {
            int i = 0;
            for (int a3 : this.c) {
                i += aff.a(a3);
            }
            a2 = a2 + i + (this.c.length * 1);
        }
        return this.d != null ? a2 + aff.b(4, (afn) this.d) : a2;
    }

    public final void a(aff aff) throws IOException {
        if (this.f2766a != null) {
            aff.a(1, this.f2766a);
        }
        if (this.f2767b != null) {
            aff.a(2, this.f2767b.intValue());
        }
        if (this.c != null && this.c.length > 0) {
            for (int a2 : this.c) {
                aff.a(3, a2);
            }
        }
        if (this.d != null) {
            aff.a(4, (afn) this.d);
        }
        super.a(aff);
    }
}
