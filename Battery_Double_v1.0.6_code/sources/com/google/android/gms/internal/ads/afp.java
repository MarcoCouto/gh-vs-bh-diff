package com.google.android.gms.internal.ads;

import java.util.Arrays;

final class afp {

    /* renamed from: a reason: collision with root package name */
    final int f2532a;

    /* renamed from: b reason: collision with root package name */
    final byte[] f2533b;

    afp(int i, byte[] bArr) {
        this.f2532a = i;
        this.f2533b = bArr;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof afp)) {
            return false;
        }
        afp afp = (afp) obj;
        return this.f2532a == afp.f2532a && Arrays.equals(this.f2533b, afp.f2533b);
    }

    public final int hashCode() {
        return ((this.f2532a + 527) * 31) + Arrays.hashCode(this.f2533b);
    }
}
