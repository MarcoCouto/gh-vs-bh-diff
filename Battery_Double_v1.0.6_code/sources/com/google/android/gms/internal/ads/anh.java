package com.google.android.gms.internal.ads;

import java.io.IOException;

public final class anh extends afh<anh> {

    /* renamed from: a reason: collision with root package name */
    private static volatile anh[] f2752a;

    /* renamed from: b reason: collision with root package name */
    private Integer f2753b;
    private anv c;

    public anh() {
        this.f2753b = null;
        this.c = null;
        this.Y = null;
        this.Z = -1;
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public final anh a(afd afd) throws IOException {
        while (true) {
            int a2 = afd.a();
            switch (a2) {
                case 0:
                    break;
                case 8:
                    int j = afd.j();
                    try {
                        int g = afd.g();
                        if (g < 0 || g > 10) {
                            throw new IllegalArgumentException(g + " is not a valid enum AdFormatType");
                        }
                        this.f2753b = Integer.valueOf(g);
                        continue;
                    } catch (IllegalArgumentException e) {
                        afd.e(j);
                        a(afd, a2);
                        break;
                    }
                case 18:
                    if (this.c == null) {
                        this.c = new anv();
                    }
                    afd.a((afn) this.c);
                    continue;
                default:
                    if (!super.a(afd, a2)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    public static anh[] b() {
        if (f2752a == null) {
            synchronized (afl.f2531b) {
                if (f2752a == null) {
                    f2752a = new anh[0];
                }
            }
        }
        return f2752a;
    }

    /* access modifiers changed from: protected */
    public final int a() {
        int a2 = super.a();
        if (this.f2753b != null) {
            a2 += aff.b(1, this.f2753b.intValue());
        }
        return this.c != null ? a2 + aff.b(2, (afn) this.c) : a2;
    }

    public final void a(aff aff) throws IOException {
        if (this.f2753b != null) {
            aff.a(1, this.f2753b.intValue());
        }
        if (this.c != null) {
            aff.a(2, (afn) this.c);
        }
        super.a(aff);
    }
}
