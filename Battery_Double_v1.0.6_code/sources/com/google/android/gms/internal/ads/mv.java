package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.a.b;

public final class mv implements Creator<mu> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        boolean z = false;
        int b2 = b.b(parcel);
        String str = null;
        boolean z2 = false;
        int i = 0;
        int i2 = 0;
        while (parcel.dataPosition() < b2) {
            int a2 = b.a(parcel);
            switch (b.a(a2)) {
                case 2:
                    str = b.k(parcel, a2);
                    break;
                case 3:
                    i2 = b.d(parcel, a2);
                    break;
                case 4:
                    i = b.d(parcel, a2);
                    break;
                case 5:
                    z2 = b.c(parcel, a2);
                    break;
                case 6:
                    z = b.c(parcel, a2);
                    break;
                default:
                    b.b(parcel, a2);
                    break;
            }
        }
        b.r(parcel, b2);
        return new mu(str, i2, i, z2, z);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new mu[i];
    }
}
