package com.google.android.gms.internal.ads;

import android.location.Location;
import android.os.Bundle;
import com.google.android.gms.ads.identifier.AdvertisingIdClient.Info;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONObject;

@cm
public final class es {

    /* renamed from: a reason: collision with root package name */
    public Bundle f3290a;

    /* renamed from: b reason: collision with root package name */
    public Bundle f3291b;
    public List<String> c = new ArrayList();
    public Location d;
    public fs e;
    public String f;
    public String g;
    public String h;
    public Info i;
    public dl j;
    public fi k;
    public JSONObject l = new JSONObject();
    public boolean m;
}
