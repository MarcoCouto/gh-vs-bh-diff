package com.google.android.gms.internal.ads;

import java.util.concurrent.Future;

final /* synthetic */ class nh implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final Future f3540a;

    nh(Future future) {
        this.f3540a = future;
    }

    public final void run() {
        Future future = this.f3540a;
        if (!future.isDone()) {
            future.cancel(true);
        }
    }
}
