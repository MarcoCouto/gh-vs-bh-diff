package com.google.android.gms.internal.ads;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

final class aci extends acg {

    /* renamed from: a reason: collision with root package name */
    private static final Class<?> f2455a = Collections.unmodifiableList(Collections.emptyList()).getClass();

    private aci() {
        super();
    }

    private static <L> List<L> a(Object obj, long j, int i) {
        List<L> c = c(obj, j);
        if (c.isEmpty()) {
            List<L> arrayList = c instanceof acf ? new ace<>(i) : new ArrayList<>(i);
            aeo.a(obj, j, (Object) arrayList);
            return arrayList;
        } else if (f2455a.isAssignableFrom(c.getClass())) {
            ArrayList arrayList2 = new ArrayList(c.size() + i);
            arrayList2.addAll(c);
            aeo.a(obj, j, (Object) arrayList2);
            return arrayList2;
        } else if (!(c instanceof ael)) {
            return c;
        } else {
            ace ace = new ace(c.size() + i);
            ace.addAll((ael) c);
            aeo.a(obj, j, (Object) ace);
            return ace;
        }
    }

    private static <E> List<E> c(Object obj, long j) {
        return (List) aeo.f(obj, j);
    }

    /* access modifiers changed from: 0000 */
    public final <L> List<L> a(Object obj, long j) {
        return a(obj, j, 10);
    }

    /* access modifiers changed from: 0000 */
    public final <E> void a(Object obj, Object obj2, long j) {
        List c = c(obj2, j);
        List a2 = a(obj, j, c.size());
        int size = a2.size();
        int size2 = c.size();
        if (size > 0 && size2 > 0) {
            a2.addAll(c);
        }
        if (size <= 0) {
            a2 = c;
        }
        aeo.a(obj, j, (Object) a2);
    }

    /* access modifiers changed from: 0000 */
    public final void b(Object obj, long j) {
        Object unmodifiableList;
        List list = (List) aeo.f(obj, j);
        if (list instanceof acf) {
            unmodifiableList = ((acf) list).e();
        } else if (!f2455a.isAssignableFrom(list.getClass())) {
            unmodifiableList = Collections.unmodifiableList(list);
        } else {
            return;
        }
        aeo.a(obj, j, unmodifiableList);
    }
}
