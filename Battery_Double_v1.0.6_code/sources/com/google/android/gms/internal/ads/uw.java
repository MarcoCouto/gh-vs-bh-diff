package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.yc.a;
import java.security.GeneralSecurityException;

public final class uw {

    /* renamed from: a reason: collision with root package name */
    public static final yc f3731a = ((yc) ((a) yc.b().a(uk.f3724a)).a(tu.a("TinkHybridDecrypt", "HybridDecrypt", "EciesAeadHkdfPrivateKey", 0, true)).a(tu.a("TinkHybridEncrypt", "HybridEncrypt", "EciesAeadHkdfPublicKey", 0, true)).a("TINK_HYBRID_1_0_0").c());

    /* renamed from: b reason: collision with root package name */
    private static final yc f3732b = ((yc) ((a) yc.b().a(f3731a)).a("TINK_HYBRID_1_1_0").c());

    static {
        try {
            uh.a("TinkHybridEncrypt", (tt<P>) new uy<P>());
            uh.a("TinkHybridDecrypt", (tt<P>) new ux<P>());
            uk.a();
        } catch (GeneralSecurityException e) {
            throw new ExceptionInInitializerError(e);
        }
    }
}
