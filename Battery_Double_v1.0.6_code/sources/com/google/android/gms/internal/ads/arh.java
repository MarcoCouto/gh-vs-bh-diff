package com.google.android.gms.internal.ads;

import android.os.RemoteException;
import com.google.android.gms.ads.b.j;

public final class arh extends apr {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public apk f2853a;

    public final apn a() throws RemoteException {
        return new arj(this);
    }

    public final void a(j jVar) throws RemoteException {
    }

    public final void a(apk apk) throws RemoteException {
        this.f2853a = apk;
    }

    public final void a(aqk aqk) throws RemoteException {
    }

    public final void a(aul aul) throws RemoteException {
    }

    public final void a(avx avx) throws RemoteException {
    }

    public final void a(awa awa) throws RemoteException {
    }

    public final void a(awk awk, aot aot) throws RemoteException {
    }

    public final void a(awn awn) throws RemoteException {
    }

    public final void a(String str, awh awh, awe awe) throws RemoteException {
    }
}
