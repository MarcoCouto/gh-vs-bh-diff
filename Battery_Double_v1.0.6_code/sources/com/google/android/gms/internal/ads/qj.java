package com.google.android.gms.internal.ads;

import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.webkit.JsPromptResult;

final class qj implements OnCancelListener {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ JsPromptResult f3632a;

    qj(JsPromptResult jsPromptResult) {
        this.f3632a = jsPromptResult;
    }

    public final void onCancel(DialogInterface dialogInterface) {
        this.f3632a.cancel();
    }
}
