package com.google.android.gms.internal.ads;

import android.os.Handler;
import java.util.ArrayList;
import java.util.List;

@cm
final class axs {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public final List<ays> f2993a = new ArrayList();

    axs() {
    }

    /* access modifiers changed from: 0000 */
    public final void a(ayt ayt) {
        Handler handler = jv.f3440a;
        for (ays ayr : this.f2993a) {
            handler.post(new ayr(this, ayr, ayt));
        }
        this.f2993a.clear();
    }
}
