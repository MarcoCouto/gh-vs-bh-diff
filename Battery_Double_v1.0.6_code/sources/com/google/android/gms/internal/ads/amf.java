package com.google.android.gms.internal.ads;

import java.util.Comparator;

public final class amf implements Comparator<als> {
    public amf(ame ame) {
    }

    public final /* synthetic */ int compare(Object obj, Object obj2) {
        als als = (als) obj;
        als als2 = (als) obj2;
        if (als.b() < als2.b()) {
            return -1;
        }
        if (als.b() > als2.b()) {
            return 1;
        }
        if (als.a() < als2.a()) {
            return -1;
        }
        if (als.a() > als2.a()) {
            return 1;
        }
        float d = (als.d() - als.b()) * (als.c() - als.a());
        float d2 = (als2.d() - als2.b()) * (als2.c() - als2.a());
        if (d <= d2) {
            return d < d2 ? 1 : 0;
        }
        return -1;
    }
}
