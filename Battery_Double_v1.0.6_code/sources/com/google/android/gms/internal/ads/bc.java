package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.gmsg.ae;
import java.util.Map;

final class bc implements ae<Object> {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ atr f3117a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ ay f3118b;

    bc(ay ayVar, atr atr) {
        this.f3118b = ayVar;
        this.f3117a = atr;
    }

    public final void zza(Object obj, Map<String, String> map) {
        this.f3118b.a((avt) this.f3117a, (String) map.get("asset"));
    }
}
