package com.google.android.gms.internal.ads;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.ax;
import com.hmatalonga.greenhub.Config;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@cm
public final class bck {
    private static String a(String str, String str2, String str3) {
        if (TextUtils.isEmpty(str3)) {
            str3 = "";
        }
        return str.replaceAll(str2, str3);
    }

    public static List<String> a(JSONObject jSONObject, String str) throws JSONException {
        JSONArray optJSONArray = jSONObject.optJSONArray(str);
        if (optJSONArray == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList(optJSONArray.length());
        for (int i = 0; i < optJSONArray.length(); i++) {
            arrayList.add(optJSONArray.getString(i));
        }
        return Collections.unmodifiableList(arrayList);
    }

    public static void a(Context context, String str, ir irVar, String str2, boolean z, List<String> list) {
        if (list != null && !list.isEmpty()) {
            String str3 = z ? "1" : Config.NOTIFICATION_DEFAULT_PRIORITY;
            for (String a2 : list) {
                String a3 = a(a(a(a(a(a(a(a2, "@gw_adlocid@", str2), "@gw_adnetrefresh@", str3), "@gw_qdata@", irVar.r.i), "@gw_sdkver@", str), "@gw_sessid@", ape.c()), "@gw_seqnum@", irVar.j), "@gw_adnetstatus@", irVar.t);
                if (irVar.o != null) {
                    a3 = a(a(a3, "@gw_adnetid@", irVar.o.f3120b), "@gw_allocid@", irVar.o.d);
                }
                String a4 = il.a(a3, context);
                ax.e();
                jv.a(context, str, a4);
            }
        }
    }

    public static void a(Context context, String str, List<String> list, String str2, hp hpVar) {
        if (list != null && !list.isEmpty()) {
            if (!TextUtils.isEmpty(str2) && ml.c()) {
                str2 = "fakeUserForAdDebugLog";
            }
            long a2 = ax.l().a();
            for (String a3 : list) {
                String a4 = a(a(a3, "@gw_rwd_userid@", Uri.encode(str2)), "@gw_tmstmp@", Long.toString(a2));
                if (hpVar != null) {
                    a4 = a(a(a4, "@gw_rwd_itm@", Uri.encode(hpVar.f3369a)), "@gw_rwd_amt@", Integer.toString(hpVar.f3370b));
                }
                ax.e();
                jv.a(context, str, a4);
            }
        }
    }

    public static boolean a(String str, int[] iArr) {
        if (TextUtils.isEmpty(str) || iArr.length != 2) {
            return false;
        }
        String[] split = str.split("x");
        if (split.length != 2) {
            return false;
        }
        try {
            iArr[0] = Integer.parseInt(split[0]);
            iArr[1] = Integer.parseInt(split[1]);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}
