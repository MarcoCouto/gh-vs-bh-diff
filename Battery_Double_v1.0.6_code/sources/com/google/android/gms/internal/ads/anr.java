package com.google.android.gms.internal.ads;

import java.io.IOException;

public final class anr extends afh<anr> {

    /* renamed from: a reason: collision with root package name */
    private Integer f2772a;

    /* renamed from: b reason: collision with root package name */
    private Integer f2773b;

    public anr() {
        this.f2772a = null;
        this.f2773b = null;
        this.Y = null;
        this.Z = -1;
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public final anr a(afd afd) throws IOException {
        while (true) {
            int a2 = afd.a();
            switch (a2) {
                case 0:
                    break;
                case 8:
                    int j = afd.j();
                    try {
                        int g = afd.g();
                        if (g < 0 || g > 2) {
                            throw new IllegalArgumentException(g + " is not a valid enum NetworkType");
                        }
                        this.f2772a = Integer.valueOf(g);
                        continue;
                    } catch (IllegalArgumentException e) {
                        afd.e(j);
                        a(afd, a2);
                        break;
                    }
                case 16:
                    int j2 = afd.j();
                    try {
                        int g2 = afd.g();
                        if ((g2 < 0 || g2 > 2) && (g2 < 4 || g2 > 4)) {
                            throw new IllegalArgumentException(g2 + " is not a valid enum CellularNetworkType");
                        }
                        this.f2773b = Integer.valueOf(g2);
                        continue;
                    } catch (IllegalArgumentException e2) {
                        afd.e(j2);
                        a(afd, a2);
                        break;
                    }
                default:
                    if (!super.a(afd, a2)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    /* access modifiers changed from: protected */
    public final int a() {
        int a2 = super.a();
        if (this.f2772a != null) {
            a2 += aff.b(1, this.f2772a.intValue());
        }
        return this.f2773b != null ? a2 + aff.b(2, this.f2773b.intValue()) : a2;
    }

    public final void a(aff aff) throws IOException {
        if (this.f2772a != null) {
            aff.a(1, this.f2772a.intValue());
        }
        if (this.f2773b != null) {
            aff.a(2, this.f2773b.intValue());
        }
        super.a(aff);
    }
}
