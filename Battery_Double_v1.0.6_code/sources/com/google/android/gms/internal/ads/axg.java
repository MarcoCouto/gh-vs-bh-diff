package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.a.b;

public final class axg implements Creator<axf> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        String[] strArr = null;
        int b2 = b.b(parcel);
        String[] strArr2 = null;
        String str = null;
        while (parcel.dataPosition() < b2) {
            int a2 = b.a(parcel);
            switch (b.a(a2)) {
                case 1:
                    str = b.k(parcel, a2);
                    break;
                case 2:
                    strArr2 = b.o(parcel, a2);
                    break;
                case 3:
                    strArr = b.o(parcel, a2);
                    break;
                default:
                    b.b(parcel, a2);
                    break;
            }
        }
        b.r(parcel, b2);
        return new axf(str, strArr2, strArr);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new axf[i];
    }
}
