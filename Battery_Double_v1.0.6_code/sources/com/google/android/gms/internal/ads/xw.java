package com.google.android.gms.internal.ads;

import java.io.IOException;

public final class xw extends afh<xw> {

    /* renamed from: a reason: collision with root package name */
    public String f3781a;

    /* renamed from: b reason: collision with root package name */
    private String f3782b;
    private String c;
    private String d;
    private String e;

    /* access modifiers changed from: protected */
    public final int a() {
        int a2 = super.a();
        if (this.f3781a != null) {
            a2 += aff.b(1, this.f3781a);
        }
        if (this.f3782b != null) {
            a2 += aff.b(2, this.f3782b);
        }
        if (this.c != null) {
            a2 += aff.b(3, this.c);
        }
        if (this.d != null) {
            a2 += aff.b(4, this.d);
        }
        return this.e != null ? a2 + aff.b(5, this.e) : a2;
    }

    public final /* synthetic */ afn a(afd afd) throws IOException {
        while (true) {
            int a2 = afd.a();
            switch (a2) {
                case 0:
                    break;
                case 10:
                    this.f3781a = afd.e();
                    continue;
                case 18:
                    this.f3782b = afd.e();
                    continue;
                case 26:
                    this.c = afd.e();
                    continue;
                case 34:
                    this.d = afd.e();
                    continue;
                case 42:
                    this.e = afd.e();
                    continue;
                default:
                    if (!super.a(afd, a2)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    public final void a(aff aff) throws IOException {
        if (this.f3781a != null) {
            aff.a(1, this.f3781a);
        }
        if (this.f3782b != null) {
            aff.a(2, this.f3782b);
        }
        if (this.c != null) {
            aff.a(3, this.c);
        }
        if (this.d != null) {
            aff.a(4, this.d);
        }
        if (this.e != null) {
            aff.a(5, this.e);
        }
        super.a(aff);
    }
}
