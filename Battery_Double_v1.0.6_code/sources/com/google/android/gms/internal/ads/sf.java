package com.google.android.gms.internal.ads;

import android.view.View;
import android.view.View.OnAttachStateChangeListener;

final class sf implements OnAttachStateChangeListener {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ ic f3679a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ sc f3680b;

    sf(sc scVar, ic icVar) {
        this.f3680b = scVar;
        this.f3679a = icVar;
    }

    public final void onViewAttachedToWindow(View view) {
        this.f3680b.a(view, this.f3679a, 10);
    }

    public final void onViewDetachedFromWindow(View view) {
    }
}
