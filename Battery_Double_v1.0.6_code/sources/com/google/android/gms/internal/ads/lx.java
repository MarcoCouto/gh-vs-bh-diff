package com.google.android.gms.internal.ads;

@cm
public final class lx<T> {

    /* renamed from: a reason: collision with root package name */
    private T f3499a;

    public final T a() {
        return this.f3499a;
    }

    public final void a(T t) {
        this.f3499a = t;
    }
}
