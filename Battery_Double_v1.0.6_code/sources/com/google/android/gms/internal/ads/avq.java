package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.b.a;
import java.util.List;

public abstract class avq extends ajl implements avp {
    public avq() {
        super("com.google.android.gms.ads.internal.formats.client.INativeContentAd");
    }

    /* access modifiers changed from: protected */
    public final boolean dispatchTransaction(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        switch (i) {
            case 2:
                a j = j();
                parcel2.writeNoException();
                ajm.a(parcel2, (IInterface) j);
                break;
            case 3:
                String a2 = a();
                parcel2.writeNoException();
                parcel2.writeString(a2);
                break;
            case 4:
                List b2 = b();
                parcel2.writeNoException();
                parcel2.writeList(b2);
                break;
            case 5:
                String e = e();
                parcel2.writeNoException();
                parcel2.writeString(e);
                break;
            case 6:
                auw f = f();
                parcel2.writeNoException();
                ajm.a(parcel2, (IInterface) f);
                break;
            case 7:
                String g = g();
                parcel2.writeNoException();
                parcel2.writeString(g);
                break;
            case 8:
                String h = h();
                parcel2.writeNoException();
                parcel2.writeString(h);
                break;
            case 9:
                Bundle n = n();
                parcel2.writeNoException();
                ajm.b(parcel2, n);
                break;
            case 10:
                q();
                parcel2.writeNoException();
                break;
            case 11:
                aqs i3 = i();
                parcel2.writeNoException();
                ajm.a(parcel2, (IInterface) i3);
                break;
            case 12:
                a((Bundle) ajm.a(parcel, Bundle.CREATOR));
                parcel2.writeNoException();
                break;
            case 13:
                boolean b3 = b((Bundle) ajm.a(parcel, Bundle.CREATOR));
                parcel2.writeNoException();
                ajm.a(parcel2, b3);
                break;
            case 14:
                c((Bundle) ajm.a(parcel, Bundle.CREATOR));
                parcel2.writeNoException();
                break;
            case 15:
                aus p = p();
                parcel2.writeNoException();
                ajm.a(parcel2, (IInterface) p);
                break;
            case 16:
                a c = c();
                parcel2.writeNoException();
                ajm.a(parcel2, (IInterface) c);
                break;
            case 17:
                String d = d();
                parcel2.writeNoException();
                parcel2.writeString(d);
                break;
            default:
                return false;
        }
        return true;
    }
}
