package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.content.Context;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;
import com.google.android.gms.ads.internal.ax;
import com.google.android.gms.ads.internal.gmsg.ae;
import java.util.Map;

@cm
public final class m extends n implements ae<qn> {

    /* renamed from: a reason: collision with root package name */
    private final qn f3500a;

    /* renamed from: b reason: collision with root package name */
    private final Context f3501b;
    private final WindowManager c;
    private final art d;
    private DisplayMetrics e;
    private float f;
    private int g = -1;
    private int h = -1;
    private int i;
    private int j = -1;
    private int k = -1;
    private int l = -1;
    private int m = -1;

    public m(qn qnVar, Context context, art art) {
        super(qnVar);
        this.f3500a = qnVar;
        this.f3501b = context;
        this.d = art;
        this.c = (WindowManager) context.getSystemService("window");
    }

    public final void a(int i2, int i3) {
        int i4 = this.f3501b instanceof Activity ? ax.e().c((Activity) this.f3501b)[0] : 0;
        if (this.f3500a.t() == null || !this.f3500a.t().d()) {
            ape.a();
            this.l = mh.b(this.f3501b, this.f3500a.getWidth());
            ape.a();
            this.m = mh.b(this.f3501b, this.f3500a.getHeight());
        }
        b(i2, i3 - i4, this.l, this.m);
        this.f3500a.v().a(i2, i3);
    }

    public final /* synthetic */ void zza(Object obj, Map map) {
        this.e = new DisplayMetrics();
        Display defaultDisplay = this.c.getDefaultDisplay();
        defaultDisplay.getMetrics(this.e);
        this.f = this.e.density;
        this.i = defaultDisplay.getRotation();
        ape.a();
        this.g = mh.b(this.e, this.e.widthPixels);
        ape.a();
        this.h = mh.b(this.e, this.e.heightPixels);
        Activity d2 = this.f3500a.d();
        if (d2 == null || d2.getWindow() == null) {
            this.j = this.g;
            this.k = this.h;
        } else {
            ax.e();
            int[] a2 = jv.a(d2);
            ape.a();
            this.j = mh.b(this.e, a2[0]);
            ape.a();
            this.k = mh.b(this.e, a2[1]);
        }
        if (this.f3500a.t().d()) {
            this.l = this.g;
            this.m = this.h;
        } else {
            this.f3500a.measure(0, 0);
        }
        a(this.g, this.h, this.j, this.k, this.f, this.i);
        this.f3500a.a("onDeviceFeaturesReceived", new j(new l().b(this.d.a()).a(this.d.b()).c(this.d.d()).d(this.d.c()).e(true)).a());
        int[] iArr = new int[2];
        this.f3500a.getLocationOnScreen(iArr);
        ape.a();
        int b2 = mh.b(this.f3501b, iArr[0]);
        ape.a();
        a(b2, mh.b(this.f3501b, iArr[1]));
        if (jm.a(2)) {
            jm.d("Dispatching Ready Event.");
        }
        b(this.f3500a.k().f3528a);
    }
}
