package com.google.android.gms.internal.ads;

import java.lang.reflect.InvocationTargetException;

public final class aix extends ajj {
    public aix(ahy ahy, String str, String str2, zz zzVar, int i, int i2) {
        super(ahy, str, str2, zzVar, i, 3);
    }

    /* access modifiers changed from: protected */
    public final void a() throws IllegalAccessException, InvocationTargetException {
        synchronized (this.f2637b) {
            ahl ahl = new ahl((String) this.c.invoke(null, new Object[]{this.f2636a.a()}));
            synchronized (this.f2637b) {
                this.f2637b.c = Long.valueOf(ahl.f2589a);
                this.f2637b.O = Long.valueOf(ahl.f2590b);
            }
        }
    }
}
