package com.google.android.gms.internal.ads;

import java.util.Map;

public final class lo extends awb<atz> {

    /* renamed from: a reason: collision with root package name */
    private final ny<atz> f3487a;

    /* renamed from: b reason: collision with root package name */
    private final Map<String, String> f3488b;
    private final ml c;

    public lo(String str, ny<atz> nyVar) {
        this(str, null, nyVar);
    }

    private lo(String str, Map<String, String> map, ny<atz> nyVar) {
        super(0, str, new lp(nyVar));
        this.f3488b = null;
        this.f3487a = nyVar;
        this.c = new ml();
        this.c.a(str, "GET", null, null);
    }

    /* access modifiers changed from: protected */
    public final bcd<atz> a(atz atz) {
        return bcd.a(atz, op.a(atz));
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void a(Object obj) {
        atz atz = (atz) obj;
        this.c.a(atz.c, atz.f2919a);
        ml mlVar = this.c;
        byte[] bArr = atz.f2920b;
        if (ml.c() && bArr != null) {
            mlVar.a(bArr);
        }
        this.f3487a.b(atz);
    }
}
