package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.app.Application.ActivityLifecycleCallbacks;

final class ahq implements ahu {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ Activity f2597a;

    ahq(ahm ahm, Activity activity) {
        this.f2597a = activity;
    }

    public final void a(ActivityLifecycleCallbacks activityLifecycleCallbacks) {
        activityLifecycleCallbacks.onActivityPaused(this.f2597a);
    }
}
