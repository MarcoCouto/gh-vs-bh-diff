package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.ax;

@cm
public final class pv extends jh {

    /* renamed from: a reason: collision with root package name */
    final pm f3615a;

    /* renamed from: b reason: collision with root package name */
    final py f3616b;
    private final String c;

    pv(pm pmVar, py pyVar, String str) {
        this.f3615a = pmVar;
        this.f3616b = pyVar;
        this.c = str;
        ax.z().a(this);
    }

    public final void a() {
        try {
            this.f3616b.a(this.c);
        } finally {
            jv.f3440a.post(new pw(this));
        }
    }

    public final void c_() {
        this.f3616b.b();
    }
}
