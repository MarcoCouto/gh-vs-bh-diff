package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.abp.b;

public final class vk extends abp<vk, a> implements acy {
    private static volatile adi<vk> zzakh;
    /* access modifiers changed from: private */
    public static final vk zzdin = new vk();
    private vo zzdil;
    private xa zzdim;

    public static final class a extends com.google.android.gms.internal.ads.abp.a<vk, a> implements acy {
        private a() {
            super(vk.zzdin);
        }

        /* synthetic */ a(vl vlVar) {
            this();
        }
    }

    static {
        abp.a(vk.class, zzdin);
    }

    private vk() {
    }

    public static vk a(aah aah) throws abv {
        return (vk) abp.a(zzdin, aah);
    }

    public final vo a() {
        return this.zzdil == null ? vo.c() : this.zzdil;
    }

    /* JADX WARNING: type inference failed for: r0v6, types: [com.google.android.gms.internal.ads.adi<com.google.android.gms.internal.ads.vk>] */
    /* JADX WARNING: type inference failed for: r0v7, types: [java.lang.Object] */
    /* JADX WARNING: type inference failed for: r0v8, types: [com.google.android.gms.internal.ads.adi<com.google.android.gms.internal.ads.vk>, com.google.android.gms.internal.ads.abp$b] */
    /* JADX WARNING: type inference failed for: r0v16 */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r0v8, types: [com.google.android.gms.internal.ads.adi<com.google.android.gms.internal.ads.vk>, com.google.android.gms.internal.ads.abp$b]
  assigns: [com.google.android.gms.internal.ads.abp$b]
  uses: [com.google.android.gms.internal.ads.adi<com.google.android.gms.internal.ads.vk>]
  mth insns count: 40
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
    	at jadx.core.ProcessClass.process(ProcessClass.java:35)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Unknown variable types count: 2 */
    public final Object a(int i, Object obj, Object obj2) {
        ? r0;
        switch (vl.f3743a[i - 1]) {
            case 1:
                return new vk();
            case 2:
                return new a(null);
            case 3:
                Object[] objArr = {"zzdil", "zzdim"};
                return a((acw) zzdin, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0003\u0000\u0000\u0000\u0001\t\u0002\t", objArr);
            case 4:
                return zzdin;
            case 5:
                adi<vk> adi = zzakh;
                if (adi != null) {
                    return adi;
                }
                synchronized (vk.class) {
                    r0 = zzakh;
                    if (r0 == 0) {
                        ? bVar = new b(zzdin);
                        zzakh = bVar;
                        r0 = bVar;
                    }
                }
                return r0;
            case 6:
                return Byte.valueOf(1);
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    public final xa b() {
        return this.zzdim == null ? xa.c() : this.zzdim;
    }
}
