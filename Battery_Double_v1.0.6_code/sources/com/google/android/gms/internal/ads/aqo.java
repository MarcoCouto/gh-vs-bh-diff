package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.b.a.C0046a;

public abstract class aqo extends ajl implements aqn {
    public aqo() {
        super("com.google.android.gms.ads.internal.client.IMobileAdsSettingManager");
    }

    /* access modifiers changed from: protected */
    public final boolean dispatchTransaction(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        switch (i) {
            case 1:
                a();
                parcel2.writeNoException();
                break;
            case 2:
                a(parcel.readFloat());
                parcel2.writeNoException();
                break;
            case 3:
                a(parcel.readString());
                parcel2.writeNoException();
                break;
            case 4:
                a(ajm.a(parcel));
                parcel2.writeNoException();
                break;
            case 5:
                a(C0046a.a(parcel.readStrongBinder()), parcel.readString());
                parcel2.writeNoException();
                break;
            case 6:
                a(parcel.readString(), C0046a.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
            case 7:
                float b2 = b();
                parcel2.writeNoException();
                parcel2.writeFloat(b2);
                break;
            case 8:
                boolean c = c();
                parcel2.writeNoException();
                ajm.a(parcel2, c);
                break;
            default:
                return false;
        }
        return true;
    }
}
