package com.google.android.gms.internal.ads;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

final class ik implements ThreadFactory {

    /* renamed from: a reason: collision with root package name */
    private final AtomicInteger f3392a = new AtomicInteger(1);

    ik(ih ihVar) {
    }

    public final Thread newThread(Runnable runnable) {
        return new Thread(runnable, "AdWorker(SCION_TASK_EXECUTOR) #" + this.f3392a.getAndIncrement());
    }
}
