package com.google.android.gms.internal.ads;

import java.io.IOException;

public final class afu extends afh<afu> {

    /* renamed from: a reason: collision with root package name */
    public aft[] f2541a;

    /* renamed from: b reason: collision with root package name */
    private afv f2542b;
    private byte[] c;
    private byte[] d;
    private Integer e;

    public afu() {
        this.f2542b = null;
        this.f2541a = aft.b();
        this.c = null;
        this.d = null;
        this.e = null;
        this.Y = null;
        this.Z = -1;
    }

    /* access modifiers changed from: protected */
    public final int a() {
        int a2 = super.a();
        if (this.f2542b != null) {
            a2 += aff.b(1, (afn) this.f2542b);
        }
        if (this.f2541a != null && this.f2541a.length > 0) {
            int i = a2;
            for (aft aft : this.f2541a) {
                if (aft != null) {
                    i += aff.b(2, (afn) aft);
                }
            }
            a2 = i;
        }
        if (this.c != null) {
            a2 += aff.b(3, this.c);
        }
        if (this.d != null) {
            a2 += aff.b(4, this.d);
        }
        return this.e != null ? a2 + aff.b(5, this.e.intValue()) : a2;
    }

    public final /* synthetic */ afn a(afd afd) throws IOException {
        while (true) {
            int a2 = afd.a();
            switch (a2) {
                case 0:
                    break;
                case 10:
                    if (this.f2542b == null) {
                        this.f2542b = new afv();
                    }
                    afd.a((afn) this.f2542b);
                    continue;
                case 18:
                    int a3 = afq.a(afd, 18);
                    int length = this.f2541a == null ? 0 : this.f2541a.length;
                    aft[] aftArr = new aft[(a3 + length)];
                    if (length != 0) {
                        System.arraycopy(this.f2541a, 0, aftArr, 0, length);
                    }
                    while (length < aftArr.length - 1) {
                        aftArr[length] = new aft();
                        afd.a((afn) aftArr[length]);
                        afd.a();
                        length++;
                    }
                    aftArr[length] = new aft();
                    afd.a((afn) aftArr[length]);
                    this.f2541a = aftArr;
                    continue;
                case 26:
                    this.c = afd.f();
                    continue;
                case 34:
                    this.d = afd.f();
                    continue;
                case 40:
                    this.e = Integer.valueOf(afd.c());
                    continue;
                default:
                    if (!super.a(afd, a2)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    public final void a(aff aff) throws IOException {
        if (this.f2542b != null) {
            aff.a(1, (afn) this.f2542b);
        }
        if (this.f2541a != null && this.f2541a.length > 0) {
            for (aft aft : this.f2541a) {
                if (aft != null) {
                    aff.a(2, (afn) aft);
                }
            }
        }
        if (this.c != null) {
            aff.a(3, this.c);
        }
        if (this.d != null) {
            aff.a(4, this.d);
        }
        if (this.e != null) {
            aff.a(5, this.e.intValue());
        }
        super.a(aff);
    }
}
