package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.abp.e;
import java.io.IOException;
import java.util.Arrays;

public final class aej {

    /* renamed from: a reason: collision with root package name */
    private static final aej f2504a = new aej(0, new int[0], new Object[0], false);

    /* renamed from: b reason: collision with root package name */
    private int f2505b;
    private int[] c;
    private Object[] d;
    private int e;
    private boolean f;

    private aej() {
        this(0, new int[8], new Object[8], true);
    }

    private aej(int i, int[] iArr, Object[] objArr, boolean z) {
        this.e = -1;
        this.f2505b = i;
        this.c = iArr;
        this.d = objArr;
        this.f = z;
    }

    public static aej a() {
        return f2504a;
    }

    static aej a(aej aej, aej aej2) {
        int i = aej.f2505b + aej2.f2505b;
        int[] copyOf = Arrays.copyOf(aej.c, i);
        System.arraycopy(aej2.c, 0, copyOf, aej.f2505b, aej2.f2505b);
        Object[] copyOf2 = Arrays.copyOf(aej.d, i);
        System.arraycopy(aej2.d, 0, copyOf2, aej.f2505b, aej2.f2505b);
        return new aej(i, copyOf, copyOf2, true);
    }

    private static void a(int i, Object obj, afc afc) throws IOException {
        int i2 = i >>> 3;
        switch (i & 7) {
            case 0:
                afc.a(i2, ((Long) obj).longValue());
                return;
            case 1:
                afc.d(i2, ((Long) obj).longValue());
                return;
            case 2:
                afc.a(i2, (aah) obj);
                return;
            case 3:
                if (afc.a() == e.j) {
                    afc.a(i2);
                    ((aej) obj).b(afc);
                    afc.b(i2);
                    return;
                }
                afc.b(i2);
                ((aej) obj).b(afc);
                afc.a(i2);
                return;
            case 5:
                afc.d(i2, ((Integer) obj).intValue());
                return;
            default:
                throw new RuntimeException(abv.f());
        }
    }

    static aej b() {
        return new aej();
    }

    /* access modifiers changed from: 0000 */
    public final void a(int i, Object obj) {
        if (!this.f) {
            throw new UnsupportedOperationException();
        }
        if (this.f2505b == this.c.length) {
            int i2 = (this.f2505b < 4 ? 8 : this.f2505b >> 1) + this.f2505b;
            this.c = Arrays.copyOf(this.c, i2);
            this.d = Arrays.copyOf(this.d, i2);
        }
        this.c[this.f2505b] = i;
        this.d[this.f2505b] = obj;
        this.f2505b++;
    }

    /* access modifiers changed from: 0000 */
    public final void a(afc afc) throws IOException {
        if (afc.a() == e.k) {
            for (int i = this.f2505b - 1; i >= 0; i--) {
                afc.a(this.c[i] >>> 3, this.d[i]);
            }
            return;
        }
        for (int i2 = 0; i2 < this.f2505b; i2++) {
            afc.a(this.c[i2] >>> 3, this.d[i2]);
        }
    }

    /* access modifiers changed from: 0000 */
    public final void a(StringBuilder sb, int i) {
        for (int i2 = 0; i2 < this.f2505b; i2++) {
            acz.a(sb, i, String.valueOf(this.c[i2] >>> 3), this.d[i2]);
        }
    }

    public final void b(afc afc) throws IOException {
        if (this.f2505b != 0) {
            if (afc.a() == e.j) {
                for (int i = 0; i < this.f2505b; i++) {
                    a(this.c[i], this.d[i], afc);
                }
                return;
            }
            for (int i2 = this.f2505b - 1; i2 >= 0; i2--) {
                a(this.c[i2], this.d[i2], afc);
            }
        }
    }

    public final void c() {
        this.f = false;
    }

    public final int d() {
        int i = this.e;
        if (i == -1) {
            i = 0;
            for (int i2 = 0; i2 < this.f2505b; i2++) {
                i += aav.d(this.c[i2] >>> 3, (aah) this.d[i2]);
            }
            this.e = i;
        }
        return i;
    }

    public final int e() {
        int e2;
        int i = this.e;
        if (i == -1) {
            i = 0;
            for (int i2 = 0; i2 < this.f2505b; i2++) {
                int i3 = this.c[i2];
                int i4 = i3 >>> 3;
                switch (i3 & 7) {
                    case 0:
                        e2 = aav.e(i4, ((Long) this.d[i2]).longValue());
                        break;
                    case 1:
                        e2 = aav.g(i4, ((Long) this.d[i2]).longValue());
                        break;
                    case 2:
                        e2 = aav.c(i4, (aah) this.d[i2]);
                        break;
                    case 3:
                        e2 = ((aej) this.d[i2]).e() + (aav.e(i4) << 1);
                        break;
                    case 5:
                        e2 = aav.i(i4, ((Integer) this.d[i2]).intValue());
                        break;
                    default:
                        throw new IllegalStateException(abv.f());
                }
                i += e2;
            }
            this.e = i;
        }
        return i;
    }

    public final boolean equals(Object obj) {
        boolean z;
        boolean z2;
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof aej)) {
            return false;
        }
        aej aej = (aej) obj;
        if (this.f2505b == aej.f2505b) {
            int[] iArr = this.c;
            int[] iArr2 = aej.c;
            int i = this.f2505b;
            int i2 = 0;
            while (true) {
                if (i2 >= i) {
                    z = true;
                    break;
                } else if (iArr[i2] != iArr2[i2]) {
                    z = false;
                    break;
                } else {
                    i2++;
                }
            }
            if (z) {
                Object[] objArr = this.d;
                Object[] objArr2 = aej.d;
                int i3 = this.f2505b;
                int i4 = 0;
                while (true) {
                    if (i4 >= i3) {
                        z2 = true;
                        break;
                    } else if (!objArr[i4].equals(objArr2[i4])) {
                        z2 = false;
                        break;
                    } else {
                        i4++;
                    }
                }
                if (z2) {
                    return true;
                }
            }
        }
        return false;
    }

    public final int hashCode() {
        int i = 17;
        int i2 = (this.f2505b + 527) * 31;
        int[] iArr = this.c;
        int i3 = 17;
        for (int i4 = 0; i4 < this.f2505b; i4++) {
            i3 = (i3 * 31) + iArr[i4];
        }
        int i5 = (i2 + i3) * 31;
        Object[] objArr = this.d;
        for (int i6 = 0; i6 < this.f2505b; i6++) {
            i = (i * 31) + objArr[i6].hashCode();
        }
        return i5 + i;
    }
}
