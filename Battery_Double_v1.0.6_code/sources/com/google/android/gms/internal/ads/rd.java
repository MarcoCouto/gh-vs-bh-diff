package com.google.android.gms.internal.ads;

import android.os.RemoteException;
import com.google.android.gms.common.util.f;
import com.hmatalonga.greenhub.Config;
import java.util.HashMap;
import java.util.Map;

@cm
public final class rd extends aqt {

    /* renamed from: a reason: collision with root package name */
    private final pm f3657a;

    /* renamed from: b reason: collision with root package name */
    private final Object f3658b = new Object();
    private final boolean c;
    private final boolean d;
    private final float e;
    private int f;
    private aqv g;
    private boolean h;
    private boolean i = true;
    private float j;
    private float k;
    private boolean l = true;
    private boolean m;
    private boolean n;

    public rd(pm pmVar, float f2, boolean z, boolean z2) {
        this.f3657a = pmVar;
        this.e = f2;
        this.c = z;
        this.d = z2;
    }

    private final void a(String str, Map<String, String> map) {
        HashMap hashMap = map == null ? new HashMap() : new HashMap(map);
        hashMap.put("action", str);
        nt.f3558a.execute(new re(this, hashMap));
    }

    public final void a() {
        a("play", null);
    }

    public final void a(float f2, int i2, boolean z, float f3) {
        boolean z2;
        int i3;
        synchronized (this.f3658b) {
            this.j = f2;
            z2 = this.i;
            this.i = z;
            i3 = this.f;
            this.f = i2;
            float f4 = this.k;
            this.k = f3;
            if (Math.abs(this.k - f4) > 1.0E-4f) {
                this.f3657a.getView().invalidate();
            }
        }
        nt.f3558a.execute(new rf(this, i3, i2, z2, z));
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ void a(int i2, int i3, boolean z, boolean z2) {
        boolean z3 = false;
        synchronized (this.f3658b) {
            boolean z4 = i2 != i3;
            boolean z5 = !this.h && i3 == 1;
            boolean z6 = z4 && i3 == 1;
            boolean z7 = z4 && i3 == 2;
            boolean z8 = z4 && i3 == 3;
            boolean z9 = z != z2;
            if (this.h || z5) {
                z3 = true;
            }
            this.h = z3;
            if (this.g != null) {
                if (z5) {
                    try {
                        this.g.a();
                    } catch (RemoteException e2) {
                        jm.c("Unable to call onVideoStart()", e2);
                    }
                }
                if (z6) {
                    try {
                        this.g.b();
                    } catch (RemoteException e3) {
                        jm.c("Unable to call onVideoPlay()", e3);
                    }
                }
                if (z7) {
                    try {
                        this.g.c();
                    } catch (RemoteException e4) {
                        jm.c("Unable to call onVideoPause()", e4);
                    }
                }
                if (z8) {
                    try {
                        this.g.d();
                    } catch (RemoteException e5) {
                        jm.c("Unable to call onVideoEnd()", e5);
                    }
                }
                if (z9) {
                    try {
                        this.g.a(z2);
                    } catch (RemoteException e6) {
                        jm.c("Unable to call onVideoMute()", e6);
                    }
                }
            }
        }
    }

    public final void a(aqv aqv) {
        synchronized (this.f3658b) {
            this.g = aqv;
        }
    }

    public final void a(arr arr) {
        synchronized (this.f3658b) {
            this.l = arr.f2860a;
            this.m = arr.f2861b;
            this.n = arr.c;
        }
        a("initialState", f.a("muteStart", arr.f2860a ? "1" : Config.NOTIFICATION_DEFAULT_PRIORITY, "customControlsRequested", arr.f2861b ? "1" : Config.NOTIFICATION_DEFAULT_PRIORITY, "clickToExpandRequested", arr.c ? "1" : Config.NOTIFICATION_DEFAULT_PRIORITY));
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ void a(Map map) {
        this.f3657a.a("pubVideoCmd", map);
    }

    public final void a(boolean z) {
        a(z ? "mute" : "unmute", null);
    }

    public final void b() {
        a("pause", null);
    }

    public final boolean c() {
        boolean z;
        synchronized (this.f3658b) {
            z = this.i;
        }
        return z;
    }

    public final int d() {
        int i2;
        synchronized (this.f3658b) {
            i2 = this.f;
        }
        return i2;
    }

    public final float e() {
        float f2;
        synchronized (this.f3658b) {
            f2 = this.k;
        }
        return f2;
    }

    public final float f() {
        return this.e;
    }

    public final float g() {
        float f2;
        synchronized (this.f3658b) {
            f2 = this.j;
        }
        return f2;
    }

    public final aqv h() throws RemoteException {
        aqv aqv;
        synchronized (this.f3658b) {
            aqv = this.g;
        }
        return aqv;
    }

    public final boolean i() {
        boolean z;
        synchronized (this.f3658b) {
            z = this.c && this.m;
        }
        return z;
    }

    public final boolean j() {
        boolean z;
        boolean i2 = i();
        synchronized (this.f3658b) {
            if (!i2) {
                if (this.n && this.d) {
                    z = true;
                }
            }
            z = false;
        }
        return z;
    }
}
