package com.google.android.gms.internal.ads;

import android.annotation.TargetApi;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import com.google.android.gms.ads.internal.ax;
import java.io.File;
import java.util.Collections;
import java.util.Map;

@TargetApi(11)
@cm
public class rm extends qo {
    public rm(qn qnVar, boolean z) {
        super(qnVar, z);
    }

    /* access modifiers changed from: protected */
    public final WebResourceResponse a(WebView webView, String str, Map<String, String> map) {
        String str2;
        if (!(webView instanceof qn)) {
            jm.e("Tried to intercept request from a WebView that wasn't an AdWebView.");
            return null;
        }
        qn qnVar = (qn) webView;
        if (this.f3638a != null) {
            this.f3638a.a(str, map, 1);
        }
        if (!"mraid.js".equalsIgnoreCase(new File(str).getName())) {
            if (map == null) {
                map = Collections.emptyMap();
            }
            return super.a(str, map);
        }
        if (qnVar.v() != null) {
            qnVar.v().n();
        }
        if (qnVar.t().d()) {
            str2 = (String) ape.f().a(asi.K);
        } else if (qnVar.z()) {
            str2 = (String) ape.f().a(asi.J);
        } else {
            str2 = (String) ape.f().a(asi.I);
        }
        ax.e();
        return jv.c(qnVar.getContext(), qnVar.k().f3528a, str2);
    }
}
