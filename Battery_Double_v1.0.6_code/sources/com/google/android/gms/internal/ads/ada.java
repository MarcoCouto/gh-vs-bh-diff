package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.abp.e;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import sun.misc.Unsafe;

final class ada<T> implements adp<T> {

    /* renamed from: a reason: collision with root package name */
    private static final Unsafe f2467a = aeo.c();

    /* renamed from: b reason: collision with root package name */
    private final int[] f2468b;
    private final Object[] c;
    private final int d;
    private final int e;
    private final int f;
    private final acw g;
    private final boolean h;
    private final boolean i;
    private final boolean j;
    private final boolean k;
    private final int[] l;
    private final int[] m;
    private final int[] n;
    private final adf o;
    private final acg p;
    private final aei<?, ?> q;
    private final abe<?> r;
    private final acr s;

    private ada(int[] iArr, Object[] objArr, int i2, int i3, int i4, acw acw, boolean z, boolean z2, int[] iArr2, int[] iArr3, int[] iArr4, adf adf, acg acg, aei<?, ?> aei, abe<?> abe, acr acr) {
        this.f2468b = iArr;
        this.c = objArr;
        this.d = i2;
        this.e = i3;
        this.f = i4;
        this.i = acw instanceof abp;
        this.j = z;
        this.h = abe != null && abe.a(acw);
        this.k = false;
        this.l = iArr2;
        this.m = iArr3;
        this.n = iArr4;
        this.o = adf;
        this.p = acg;
        this.q = aei;
        this.r = abe;
        this.g = acw;
        this.s = acr;
    }

    private static int a(int i2, byte[] bArr, int i3, int i4, Object obj, aae aae) throws IOException {
        return aad.a(i2, bArr, i3, i4, e(obj), aae);
    }

    private static int a(adp<?> adp, int i2, byte[] bArr, int i3, int i4, abu<?> abu, aae aae) throws IOException {
        int a2 = a((adp) adp, bArr, i3, i4, aae);
        abu.add(aae.c);
        while (a2 < i4) {
            int a3 = aad.a(bArr, a2, aae);
            if (i2 != aae.f2389a) {
                break;
            }
            a2 = a((adp) adp, bArr, a3, i4, aae);
            abu.add(aae.c);
        }
        return a2;
    }

    private static int a(adp adp, byte[] bArr, int i2, int i3, int i4, aae aae) throws IOException {
        ada ada = (ada) adp;
        Object a2 = ada.a();
        int a3 = ada.a((T) a2, bArr, i2, i3, i4, aae);
        ada.c((T) a2);
        aae.c = a2;
        return a3;
    }

    private static int a(adp adp, byte[] bArr, int i2, int i3, aae aae) throws IOException {
        int i4;
        int i5 = i2 + 1;
        byte b2 = bArr[i2];
        if (b2 < 0) {
            i5 = aad.a((int) b2, bArr, i5, aae);
            i4 = aae.f2389a;
        } else {
            i4 = b2;
        }
        if (i4 < 0 || i4 > i3 - i5) {
            throw abv.a();
        }
        Object a2 = adp.a();
        adp.a(a2, bArr, i5, i5 + i4, aae);
        adp.c(a2);
        aae.c = a2;
        return i5 + i4;
    }

    private static <UT, UB> int a(aei<UT, UB> aei, T t) {
        return aei.f(aei.b(t));
    }

    private final int a(T t, byte[] bArr, int i2, int i3, int i4, int i5, int i6, int i7, int i8, long j2, int i9, aae aae) throws IOException {
        int a2;
        Unsafe unsafe = f2467a;
        long j3 = (long) (this.f2468b[i9 + 2] & 1048575);
        switch (i8) {
            case 51:
                if (i6 == 1) {
                    unsafe.putObject(t, j2, Double.valueOf(aad.c(bArr, i2)));
                    a2 = i2 + 8;
                    break;
                } else {
                    return i2;
                }
            case 52:
                if (i6 == 5) {
                    unsafe.putObject(t, j2, Float.valueOf(aad.d(bArr, i2)));
                    a2 = i2 + 4;
                    break;
                } else {
                    return i2;
                }
            case 53:
            case 54:
                if (i6 == 0) {
                    a2 = aad.b(bArr, i2, aae);
                    unsafe.putObject(t, j2, Long.valueOf(aae.f2390b));
                    break;
                } else {
                    return i2;
                }
            case 55:
            case 62:
                if (i6 == 0) {
                    a2 = aad.a(bArr, i2, aae);
                    unsafe.putObject(t, j2, Integer.valueOf(aae.f2389a));
                    break;
                } else {
                    return i2;
                }
            case 56:
            case 65:
                if (i6 == 1) {
                    unsafe.putObject(t, j2, Long.valueOf(aad.b(bArr, i2)));
                    a2 = i2 + 8;
                    break;
                } else {
                    return i2;
                }
            case 57:
            case 64:
                if (i6 == 5) {
                    unsafe.putObject(t, j2, Integer.valueOf(aad.a(bArr, i2)));
                    a2 = i2 + 4;
                    break;
                } else {
                    return i2;
                }
            case 58:
                if (i6 == 0) {
                    int b2 = aad.b(bArr, i2, aae);
                    unsafe.putObject(t, j2, Boolean.valueOf(aae.f2390b != 0));
                    a2 = b2;
                    break;
                } else {
                    return i2;
                }
            case 59:
                if (i6 != 2) {
                    return i2;
                }
                int a3 = aad.a(bArr, i2, aae);
                int i10 = aae.f2389a;
                if (i10 == 0) {
                    unsafe.putObject(t, j2, "");
                } else if ((536870912 & i7) == 0 || aeq.a(bArr, a3, a3 + i10)) {
                    unsafe.putObject(t, j2, new String(bArr, a3, i10, abr.f2440a));
                    a3 += i10;
                } else {
                    throw abv.h();
                }
                unsafe.putInt(t, j3, i5);
                return a3;
            case 60:
                if (i6 != 2) {
                    return i2;
                }
                int a4 = a(a(i9), bArr, i2, i3, aae);
                Object obj = unsafe.getInt(t, j3) == i5 ? unsafe.getObject(t, j2) : null;
                if (obj == null) {
                    unsafe.putObject(t, j2, aae.c);
                } else {
                    unsafe.putObject(t, j2, abr.a(obj, aae.c));
                }
                unsafe.putInt(t, j3, i5);
                return a4;
            case 61:
                if (i6 != 2) {
                    return i2;
                }
                int a5 = aad.a(bArr, i2, aae);
                int i11 = aae.f2389a;
                if (i11 == 0) {
                    unsafe.putObject(t, j2, aah.f2393a);
                } else {
                    unsafe.putObject(t, j2, aah.a(bArr, a5, i11));
                    a5 += i11;
                }
                unsafe.putInt(t, j3, i5);
                return a5;
            case 63:
                if (i6 != 0) {
                    return i2;
                }
                a2 = aad.a(bArr, i2, aae);
                int i12 = aae.f2389a;
                abt c2 = c(i9);
                if (c2 == null || c2.a(i12) != null) {
                    unsafe.putObject(t, j2, Integer.valueOf(i12));
                    break;
                } else {
                    e((Object) t).a(i4, (Object) Long.valueOf((long) i12));
                    return a2;
                }
            case 66:
                if (i6 == 0) {
                    a2 = aad.a(bArr, i2, aae);
                    unsafe.putObject(t, j2, Integer.valueOf(aaq.f(aae.f2389a)));
                    break;
                } else {
                    return i2;
                }
            case 67:
                if (i6 == 0) {
                    a2 = aad.b(bArr, i2, aae);
                    unsafe.putObject(t, j2, Long.valueOf(aaq.a(aae.f2390b)));
                    break;
                } else {
                    return i2;
                }
            case 68:
                if (i6 == 3) {
                    a2 = a(a(i9), bArr, i2, i3, (i4 & -8) | 4, aae);
                    Object obj2 = unsafe.getInt(t, j3) == i5 ? unsafe.getObject(t, j2) : null;
                    if (obj2 != null) {
                        unsafe.putObject(t, j2, abr.a(obj2, aae.c));
                        break;
                    } else {
                        unsafe.putObject(t, j2, aae.c);
                        break;
                    }
                } else {
                    return i2;
                }
            default:
                return i2;
        }
        unsafe.putInt(t, j3, i5);
        return a2;
    }

    private final int a(T t, byte[] bArr, int i2, int i3, int i4, int i5, int i6, int i7, long j2, int i8, long j3, aae aae) throws IOException {
        abu abu;
        int a2;
        int i9;
        int i10;
        int i11;
        abu abu2 = (abu) f2467a.getObject(t, j3);
        if (!abu2.a()) {
            int size = abu2.size();
            abu = abu2.a(size == 0 ? 10 : size << 1);
            f2467a.putObject(t, j3, abu);
        } else {
            abu = abu2;
        }
        switch (i8) {
            case 18:
            case 35:
                if (i6 == 2) {
                    aay aay = (aay) abu;
                    int a3 = aad.a(bArr, i2, aae);
                    int i12 = aae.f2389a + a3;
                    while (a3 < i12) {
                        aay.a(aad.c(bArr, a3));
                        a3 += 8;
                    }
                    if (a3 == i12) {
                        return a3;
                    }
                    throw abv.a();
                } else if (i6 != 1) {
                    return i2;
                } else {
                    aay aay2 = (aay) abu;
                    aay2.a(aad.c(bArr, i2));
                    int i13 = i2 + 8;
                    while (i13 < i3) {
                        int a4 = aad.a(bArr, i13, aae);
                        if (i4 != aae.f2389a) {
                            return i13;
                        }
                        aay2.a(aad.c(bArr, a4));
                        i13 = a4 + 8;
                    }
                    return i13;
                }
            case 19:
            case 36:
                if (i6 == 2) {
                    abn abn = (abn) abu;
                    int a5 = aad.a(bArr, i2, aae);
                    int i14 = aae.f2389a + a5;
                    while (a5 < i14) {
                        abn.a(aad.d(bArr, a5));
                        a5 += 4;
                    }
                    if (a5 == i14) {
                        return a5;
                    }
                    throw abv.a();
                } else if (i6 != 5) {
                    return i2;
                } else {
                    abn abn2 = (abn) abu;
                    abn2.a(aad.d(bArr, i2));
                    int i15 = i2 + 4;
                    while (i15 < i3) {
                        int a6 = aad.a(bArr, i15, aae);
                        if (i4 != aae.f2389a) {
                            return i15;
                        }
                        abn2.a(aad.d(bArr, a6));
                        i15 = a6 + 4;
                    }
                    return i15;
                }
            case 20:
            case 21:
            case 37:
            case 38:
                if (i6 == 2) {
                    ack ack = (ack) abu;
                    int a7 = aad.a(bArr, i2, aae);
                    int i16 = aae.f2389a + a7;
                    while (a7 < i16) {
                        a7 = aad.b(bArr, a7, aae);
                        ack.a(aae.f2390b);
                    }
                    if (a7 == i16) {
                        return a7;
                    }
                    throw abv.a();
                } else if (i6 != 0) {
                    return i2;
                } else {
                    ack ack2 = (ack) abu;
                    int b2 = aad.b(bArr, i2, aae);
                    ack2.a(aae.f2390b);
                    while (b2 < i3) {
                        int a8 = aad.a(bArr, b2, aae);
                        if (i4 != aae.f2389a) {
                            return b2;
                        }
                        b2 = aad.b(bArr, a8, aae);
                        ack2.a(aae.f2390b);
                    }
                    return b2;
                }
            case 22:
            case 29:
            case 39:
            case 43:
                return i6 == 2 ? aad.a(bArr, i2, abu, aae) : i6 == 0 ? aad.a(i4, bArr, i2, i3, abu, aae) : i2;
            case 23:
            case 32:
            case 40:
            case 46:
                if (i6 == 2) {
                    ack ack3 = (ack) abu;
                    int a9 = aad.a(bArr, i2, aae);
                    int i17 = aae.f2389a + a9;
                    while (a9 < i17) {
                        ack3.a(aad.b(bArr, a9));
                        a9 += 8;
                    }
                    if (a9 == i17) {
                        return a9;
                    }
                    throw abv.a();
                } else if (i6 != 1) {
                    return i2;
                } else {
                    ack ack4 = (ack) abu;
                    ack4.a(aad.b(bArr, i2));
                    int i18 = i2 + 8;
                    while (i18 < i3) {
                        int a10 = aad.a(bArr, i18, aae);
                        if (i4 != aae.f2389a) {
                            return i18;
                        }
                        ack4.a(aad.b(bArr, a10));
                        i18 = a10 + 8;
                    }
                    return i18;
                }
            case 24:
            case 31:
            case 41:
            case 45:
                if (i6 == 2) {
                    abq abq = (abq) abu;
                    int a11 = aad.a(bArr, i2, aae);
                    int i19 = aae.f2389a + a11;
                    while (a11 < i19) {
                        abq.c(aad.a(bArr, a11));
                        a11 += 4;
                    }
                    if (a11 == i19) {
                        return a11;
                    }
                    throw abv.a();
                } else if (i6 != 5) {
                    return i2;
                } else {
                    abq abq2 = (abq) abu;
                    abq2.c(aad.a(bArr, i2));
                    int i20 = i2 + 4;
                    while (i20 < i3) {
                        int a12 = aad.a(bArr, i20, aae);
                        if (i4 != aae.f2389a) {
                            return i20;
                        }
                        abq2.c(aad.a(bArr, a12));
                        i20 = a12 + 4;
                    }
                    return i20;
                }
            case 25:
            case 42:
                if (i6 == 2) {
                    aaf aaf = (aaf) abu;
                    int a13 = aad.a(bArr, i2, aae);
                    int i21 = a13 + aae.f2389a;
                    while (a13 < i21) {
                        a13 = aad.b(bArr, a13, aae);
                        aaf.a(aae.f2390b != 0);
                    }
                    if (a13 == i21) {
                        return a13;
                    }
                    throw abv.a();
                } else if (i6 != 0) {
                    return i2;
                } else {
                    aaf aaf2 = (aaf) abu;
                    int b3 = aad.b(bArr, i2, aae);
                    aaf2.a(aae.f2390b != 0);
                    while (b3 < i3) {
                        int a14 = aad.a(bArr, b3, aae);
                        if (i4 != aae.f2389a) {
                            return b3;
                        }
                        b3 = aad.b(bArr, a14, aae);
                        aaf2.a(aae.f2390b != 0);
                    }
                    return b3;
                }
            case 26:
                if (i6 != 2) {
                    return i2;
                }
                if ((536870912 & j2) == 0) {
                    int a15 = aad.a(bArr, i2, aae);
                    int i22 = aae.f2389a;
                    if (i22 == 0) {
                        abu.add("");
                    } else {
                        abu.add(new String(bArr, a15, i22, abr.f2440a));
                        a15 += i22;
                    }
                    while (i11 < i3) {
                        int a16 = aad.a(bArr, i11, aae);
                        if (i4 != aae.f2389a) {
                            return i11;
                        }
                        i11 = aad.a(bArr, a16, aae);
                        int i23 = aae.f2389a;
                        if (i23 == 0) {
                            abu.add("");
                        } else {
                            abu.add(new String(bArr, i11, i23, abr.f2440a));
                            i11 += i23;
                        }
                    }
                    return i11;
                }
                int a17 = aad.a(bArr, i2, aae);
                int i24 = aae.f2389a;
                if (i24 == 0) {
                    abu.add("");
                } else {
                    if (!aeq.a(bArr, a17, a17 + i24)) {
                        throw abv.h();
                    }
                    abu.add(new String(bArr, a17, i24, abr.f2440a));
                    a17 += i24;
                }
                while (i10 < i3) {
                    int a18 = aad.a(bArr, i10, aae);
                    if (i4 != aae.f2389a) {
                        return i10;
                    }
                    i10 = aad.a(bArr, a18, aae);
                    int i25 = aae.f2389a;
                    if (i25 == 0) {
                        abu.add("");
                    } else {
                        if (!aeq.a(bArr, i10, i10 + i25)) {
                            throw abv.h();
                        }
                        abu.add(new String(bArr, i10, i25, abr.f2440a));
                        i10 += i25;
                    }
                }
                return i10;
            case 27:
                return i6 == 2 ? a(a(i7), i4, bArr, i2, i3, abu, aae) : i2;
            case 28:
                if (i6 != 2) {
                    return i2;
                }
                int a19 = aad.a(bArr, i2, aae);
                int i26 = aae.f2389a;
                if (i26 == 0) {
                    abu.add(aah.f2393a);
                } else {
                    abu.add(aah.a(bArr, a19, i26));
                    a19 += i26;
                }
                while (i9 < i3) {
                    int a20 = aad.a(bArr, i9, aae);
                    if (i4 != aae.f2389a) {
                        return i9;
                    }
                    i9 = aad.a(bArr, a20, aae);
                    int i27 = aae.f2389a;
                    if (i27 == 0) {
                        abu.add(aah.f2393a);
                    } else {
                        abu.add(aah.a(bArr, i9, i27));
                        i9 += i27;
                    }
                }
                return i9;
            case 30:
            case 44:
                if (i6 == 2) {
                    a2 = aad.a(bArr, i2, abu, aae);
                } else if (i6 != 0) {
                    return i2;
                } else {
                    a2 = aad.a(i4, bArr, i2, i3, abu, aae);
                }
                aej aej = ((abp) t).zzdtt;
                if (aej == aej.a()) {
                    aej = null;
                }
                aej aej2 = (aej) adr.a(i5, abu, c(i7), aej, this.q);
                if (aej2 == null) {
                    return a2;
                }
                ((abp) t).zzdtt = aej2;
                return a2;
            case 33:
            case 47:
                if (i6 == 2) {
                    abq abq3 = (abq) abu;
                    int a21 = aad.a(bArr, i2, aae);
                    int i28 = aae.f2389a + a21;
                    while (a21 < i28) {
                        a21 = aad.a(bArr, a21, aae);
                        abq3.c(aaq.f(aae.f2389a));
                    }
                    if (a21 == i28) {
                        return a21;
                    }
                    throw abv.a();
                } else if (i6 != 0) {
                    return i2;
                } else {
                    abq abq4 = (abq) abu;
                    int a22 = aad.a(bArr, i2, aae);
                    abq4.c(aaq.f(aae.f2389a));
                    while (a22 < i3) {
                        int a23 = aad.a(bArr, a22, aae);
                        if (i4 != aae.f2389a) {
                            return a22;
                        }
                        a22 = aad.a(bArr, a23, aae);
                        abq4.c(aaq.f(aae.f2389a));
                    }
                    return a22;
                }
            case 34:
            case 48:
                if (i6 == 2) {
                    ack ack5 = (ack) abu;
                    int a24 = aad.a(bArr, i2, aae);
                    int i29 = aae.f2389a + a24;
                    while (a24 < i29) {
                        a24 = aad.b(bArr, a24, aae);
                        ack5.a(aaq.a(aae.f2390b));
                    }
                    if (a24 == i29) {
                        return a24;
                    }
                    throw abv.a();
                } else if (i6 != 0) {
                    return i2;
                } else {
                    ack ack6 = (ack) abu;
                    int b4 = aad.b(bArr, i2, aae);
                    ack6.a(aaq.a(aae.f2390b));
                    while (b4 < i3) {
                        int a25 = aad.a(bArr, b4, aae);
                        if (i4 != aae.f2389a) {
                            return b4;
                        }
                        b4 = aad.b(bArr, a25, aae);
                        ack6.a(aaq.a(aae.f2390b));
                    }
                    return b4;
                }
            case 49:
                if (i6 != 3) {
                    return i2;
                }
                adp a26 = a(i7);
                int i30 = (i4 & -8) | 4;
                int a27 = a(a26, bArr, i2, i3, i30, aae);
                abu.add(aae.c);
                while (a27 < i3) {
                    int a28 = aad.a(bArr, a27, aae);
                    if (i4 != aae.f2389a) {
                        return a27;
                    }
                    a27 = a(a26, bArr, a28, i3, i30, aae);
                    abu.add(aae.c);
                }
                return a27;
            default:
                return i2;
        }
    }

    /* JADX WARNING: type inference failed for: r2v11, types: [int] */
    /* JADX WARNING: type inference failed for: r2v24 */
    /* JADX WARNING: Multi-variable type inference failed */
    private final <K, V> int a(T t, byte[] bArr, int i2, int i3, int i4, int i5, long j2, aae aae) throws IOException {
        Object obj;
        Unsafe unsafe = f2467a;
        Object b2 = b(i4);
        Object object = unsafe.getObject(t, j2);
        if (this.s.c(object)) {
            obj = this.s.e(b2);
            this.s.a(obj, object);
            unsafe.putObject(t, j2, obj);
        } else {
            obj = object;
        }
        acp f2 = this.s.f(b2);
        Map a2 = this.s.a(obj);
        int a3 = aad.a(bArr, i2, aae);
        int i6 = aae.f2389a;
        if (i6 < 0 || i6 > i3 - a3) {
            throw abv.a();
        }
        int i7 = a3 + i6;
        K k2 = f2.f2462b;
        V v = f2.d;
        K k3 = k2;
        int i8 = a3;
        while (i8 < i7) {
            int i9 = i8 + 1;
            byte b3 = bArr[i8];
            if (b3 < 0) {
                i9 = aad.a((int) b3, bArr, i9, aae);
                b3 = aae.f2389a;
            }
            int i10 = b3 & 7;
            switch (b3 >>> 3) {
                case 1:
                    if (i10 == f2.f2461a.b()) {
                        int a4 = a(bArr, i9, i3, f2.f2461a, null, aae);
                        k3 = aae.c;
                        i8 = a4;
                        break;
                    }
                case 2:
                    if (i10 == f2.c.b()) {
                        int a5 = a(bArr, i9, i3, f2.c, f2.d.getClass(), aae);
                        v = aae.c;
                        i8 = a5;
                        break;
                    }
                default:
                    i8 = aad.a(b3, bArr, i9, i3, aae);
                    break;
            }
        }
        if (i8 != i7) {
            throw abv.g();
        }
        a2.put(k3, v);
        return i7;
    }

    /* JADX WARNING: type inference failed for: r17v4, types: [int] */
    /* JADX WARNING: type inference failed for: r0v13 */
    /* JADX WARNING: type inference failed for: r29v0, types: [int] */
    /* JADX WARNING: type inference failed for: r8v2, types: [int] */
    /* JADX WARNING: type inference failed for: r0v47, types: [int] */
    /* JADX WARNING: type inference failed for: r0v90, types: [int] */
    /* JADX WARNING: type inference failed for: r17v5 */
    /* JADX WARNING: type inference failed for: r17v7 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x008d A[ADDED_TO_REGION] */
    /* JADX WARNING: Unknown variable types count: 5 */
    private final int a(T t, byte[] bArr, int i2, int i3, int i4, aae aae) throws IOException {
        int i5;
        int i6;
        int i7;
        abu abu;
        Unsafe unsafe = f2467a;
        int i8 = -1;
        int i9 = 0;
        int i10 = 0;
        int i11 = i2;
        while (true) {
            if (i11 < i3) {
                int i12 = i11 + 1;
                byte b2 = bArr[i11];
                if (b2 < 0) {
                    i12 = aad.a((int) b2, bArr, i12, aae);
                    b2 = aae.f2389a;
                }
                int i13 = i10 >>> 3;
                int i14 = i10 & 7;
                int g2 = g(i13);
                if (g2 != -1) {
                    int i15 = this.f2468b[g2 + 1];
                    int i16 = (267386880 & i15) >>> 20;
                    long j2 = (long) (1048575 & i15);
                    if (i16 <= 17) {
                        int i17 = this.f2468b[g2 + 2];
                        int i18 = 1 << (i17 >>> 20);
                        int i19 = i17 & 1048575;
                        if (i19 != i8) {
                            if (i8 != -1) {
                                unsafe.putInt(t, (long) i8, i9);
                            }
                            i9 = unsafe.getInt(t, (long) i19);
                            i8 = i19;
                        }
                        switch (i16) {
                            case 0:
                                if (i14 == 1) {
                                    aeo.a((Object) t, j2, aad.c(bArr, i12));
                                    i11 = i12 + 8;
                                    i9 |= i18;
                                    continue;
                                }
                                break;
                            case 1:
                                if (i14 == 5) {
                                    aeo.a((Object) t, j2, aad.d(bArr, i12));
                                    i11 = i12 + 4;
                                    i9 |= i18;
                                    continue;
                                }
                                break;
                            case 2:
                            case 3:
                                if (i14 == 0) {
                                    i11 = aad.b(bArr, i12, aae);
                                    unsafe.putLong(t, j2, aae.f2390b);
                                    i9 |= i18;
                                    continue;
                                }
                                break;
                            case 4:
                            case 11:
                                if (i14 == 0) {
                                    i11 = aad.a(bArr, i12, aae);
                                    unsafe.putInt(t, j2, aae.f2389a);
                                    i9 |= i18;
                                    continue;
                                }
                                break;
                            case 5:
                            case 14:
                                if (i14 == 1) {
                                    unsafe.putLong(t, j2, aad.b(bArr, i12));
                                    i11 = i12 + 8;
                                    i9 |= i18;
                                    continue;
                                }
                                break;
                            case 6:
                            case 13:
                                if (i14 == 5) {
                                    unsafe.putInt(t, j2, aad.a(bArr, i12));
                                    i11 = i12 + 4;
                                    i9 |= i18;
                                    continue;
                                }
                                break;
                            case 7:
                                if (i14 == 0) {
                                    i11 = aad.b(bArr, i12, aae);
                                    aeo.a((Object) t, j2, aae.f2390b != 0);
                                    i9 |= i18;
                                    continue;
                                }
                                break;
                            case 8:
                                if (i14 == 2) {
                                    int d2 = (536870912 & i15) == 0 ? aad.c(bArr, i12, aae) : aad.d(bArr, i12, aae);
                                    unsafe.putObject(t, j2, aae.c);
                                    i9 |= i18;
                                    i11 = d2;
                                    continue;
                                }
                                break;
                            case 9:
                                if (i14 == 2) {
                                    i11 = a(a(g2), bArr, i12, i3, aae);
                                    if ((i9 & i18) == 0) {
                                        unsafe.putObject(t, j2, aae.c);
                                    } else {
                                        unsafe.putObject(t, j2, abr.a(unsafe.getObject(t, j2), aae.c));
                                    }
                                    i9 |= i18;
                                    continue;
                                }
                                break;
                            case 10:
                                if (i14 == 2) {
                                    i11 = aad.e(bArr, i12, aae);
                                    unsafe.putObject(t, j2, aae.c);
                                    i9 |= i18;
                                    continue;
                                }
                                break;
                            case 12:
                                if (i14 == 0) {
                                    i11 = aad.a(bArr, i12, aae);
                                    int i20 = aae.f2389a;
                                    abt c2 = c(g2);
                                    if (c2 != null && c2.a(i20) == null) {
                                        e((Object) t).a((int) i10, (Object) Long.valueOf((long) i20));
                                        break;
                                    } else {
                                        unsafe.putInt(t, j2, i20);
                                        i9 |= i18;
                                        continue;
                                    }
                                }
                                break;
                            case 15:
                                if (i14 == 0) {
                                    i11 = aad.a(bArr, i12, aae);
                                    unsafe.putInt(t, j2, aaq.f(aae.f2389a));
                                    i9 |= i18;
                                    continue;
                                }
                                break;
                            case 16:
                                if (i14 == 0) {
                                    i11 = aad.b(bArr, i12, aae);
                                    unsafe.putLong(t, j2, aaq.a(aae.f2390b));
                                    i9 |= i18;
                                    continue;
                                }
                                break;
                            case 17:
                                if (i14 == 3) {
                                    i11 = a(a(g2), bArr, i12, i3, (i13 << 3) | 4, aae);
                                    if ((i9 & i18) == 0) {
                                        unsafe.putObject(t, j2, aae.c);
                                    } else {
                                        unsafe.putObject(t, j2, abr.a(unsafe.getObject(t, j2), aae.c));
                                    }
                                    i9 |= i18;
                                    continue;
                                }
                                break;
                        }
                        i6 = i9;
                        i7 = i8;
                        i11 = i12;
                        if (i10 == i4 || i4 == 0) {
                            i11 = a((int) i10, bArr, i11, i3, (Object) t, aae);
                            i9 = i6;
                            i8 = i7;
                            i10 = i10;
                        } else {
                            i5 = i10;
                        }
                    } else if (i16 == 27) {
                        if (i14 == 2) {
                            abu abu2 = (abu) unsafe.getObject(t, j2);
                            if (!abu2.a()) {
                                int size = abu2.size();
                                abu = abu2.a(size == 0 ? 10 : size << 1);
                                unsafe.putObject(t, j2, abu);
                            } else {
                                abu = abu2;
                            }
                            i11 = a(a(g2), (int) i10, bArr, i12, i3, abu, aae);
                            i10 = i10;
                        }
                    } else if (i16 <= 49) {
                        i11 = a(t, bArr, i12, i3, (int) i10, i13, i14, g2, (long) i15, i16, j2, aae);
                        if (i11 == i12) {
                            i6 = i9;
                            i7 = i8;
                            if (i10 == i4) {
                            }
                            i11 = a((int) i10, bArr, i11, i3, (Object) t, aae);
                            i9 = i6;
                            i8 = i7;
                            i10 = i10;
                        } else {
                            continue;
                            i10 = i10;
                        }
                    } else if (i16 != 50) {
                        i11 = a(t, bArr, i12, i3, (int) i10, i13, i14, i15, i16, j2, g2, aae);
                        if (i11 == i12) {
                            i6 = i9;
                            i7 = i8;
                            if (i10 == i4) {
                            }
                            i11 = a((int) i10, bArr, i11, i3, (Object) t, aae);
                            i9 = i6;
                            i8 = i7;
                            i10 = i10;
                        } else {
                            continue;
                            i10 = i10;
                        }
                    } else if (i14 == 2) {
                        i11 = a(t, bArr, i12, i3, g2, i13, j2, aae);
                        if (i11 == i12) {
                            i6 = i9;
                            i7 = i8;
                            if (i10 == i4) {
                            }
                            i11 = a((int) i10, bArr, i11, i3, (Object) t, aae);
                            i9 = i6;
                            i8 = i7;
                            i10 = i10;
                        } else {
                            continue;
                            i10 = i10;
                        }
                    }
                }
                i6 = i9;
                i7 = i8;
                i11 = i12;
                if (i10 == i4) {
                }
                i11 = a((int) i10, bArr, i11, i3, (Object) t, aae);
                i9 = i6;
                i8 = i7;
                i10 = i10;
            } else {
                i6 = i9;
                i7 = i8;
            }
        }
        if (i7 != -1) {
            unsafe.putInt(t, (long) i7, i6);
        }
        if (this.m != null) {
            aej aej = null;
            for (int a2 : this.m) {
                aej = (aej) a((Object) t, a2, (UB) aej, this.q);
            }
            if (aej != null) {
                this.q.b((Object) t, aej);
            }
        }
        if (i4 == 0) {
            if (i11 != i3) {
                throw abv.g();
            }
        } else if (i11 > i3 || i5 != i4) {
            throw abv.g();
        }
        return i11;
    }

    private static int a(byte[] bArr, int i2, int i3, aew aew, Class<?> cls, aae aae) throws IOException {
        switch (aew) {
            case BOOL:
                int b2 = aad.b(bArr, i2, aae);
                aae.c = Boolean.valueOf(aae.f2390b != 0);
                return b2;
            case BYTES:
                return aad.e(bArr, i2, aae);
            case DOUBLE:
                aae.c = Double.valueOf(aad.c(bArr, i2));
                return i2 + 8;
            case FIXED32:
            case SFIXED32:
                aae.c = Integer.valueOf(aad.a(bArr, i2));
                return i2 + 4;
            case FIXED64:
            case SFIXED64:
                aae.c = Long.valueOf(aad.b(bArr, i2));
                return i2 + 8;
            case FLOAT:
                aae.c = Float.valueOf(aad.d(bArr, i2));
                return i2 + 4;
            case ENUM:
            case INT32:
            case UINT32:
                int a2 = aad.a(bArr, i2, aae);
                aae.c = Integer.valueOf(aae.f2389a);
                return a2;
            case INT64:
            case UINT64:
                int b3 = aad.b(bArr, i2, aae);
                aae.c = Long.valueOf(aae.f2390b);
                return b3;
            case MESSAGE:
                return a(adj.a().a(cls), bArr, i2, i3, aae);
            case SINT32:
                int a3 = aad.a(bArr, i2, aae);
                aae.c = Integer.valueOf(aaq.f(aae.f2389a));
                return a3;
            case SINT64:
                int b4 = aad.b(bArr, i2, aae);
                aae.c = Long.valueOf(aaq.a(aae.f2390b));
                return b4;
            case STRING:
                return aad.d(bArr, i2, aae);
            default:
                throw new RuntimeException("unsupported field type.");
        }
    }

    static <T> ada<T> a(Class<T> cls, acu acu, adf adf, acg acg, aei<?, ?> aei, abe<?> abe, acr acr) {
        int e2;
        int f2;
        int k2;
        int a2;
        int i2;
        int i3;
        if (acu instanceof adl) {
            adl adl = (adl) acu;
            boolean z = adl.a() == e.i;
            if (adl.g() == 0) {
                e2 = 0;
                f2 = 0;
                k2 = 0;
            } else {
                e2 = adl.e();
                f2 = adl.f();
                k2 = adl.k();
            }
            int[] iArr = new int[(k2 << 2)];
            Object[] objArr = new Object[(k2 << 1)];
            int[] iArr2 = adl.h() > 0 ? new int[adl.h()] : null;
            int[] iArr3 = adl.i() > 0 ? new int[adl.i()] : null;
            int i4 = 0;
            int i5 = 0;
            adm d2 = adl.d();
            if (d2.a()) {
                int b2 = d2.b();
                int i6 = 0;
                while (true) {
                    if (b2 >= adl.l() || i6 >= ((b2 - e2) << 2)) {
                        if (d2.d()) {
                            a2 = (int) aeo.a(d2.e());
                            i2 = (int) aeo.a(d2.f());
                            i3 = 0;
                        } else {
                            a2 = (int) aeo.a(d2.g());
                            if (d2.h()) {
                                i2 = (int) aeo.a(d2.i());
                                i3 = d2.j();
                            } else {
                                i2 = 0;
                                i3 = 0;
                            }
                        }
                        iArr[i6] = d2.b();
                        iArr[i6 + 1] = a2 | (d2.l() ? 536870912 : 0) | (d2.k() ? 268435456 : 0) | (d2.c() << 20);
                        iArr[i6 + 2] = i2 | (i3 << 20);
                        if (d2.o() != null) {
                            objArr[(i6 / 4) << 1] = d2.o();
                            if (d2.m() != null) {
                                objArr[((i6 / 4) << 1) + 1] = d2.m();
                            } else if (d2.n() != null) {
                                objArr[((i6 / 4) << 1) + 1] = d2.n();
                            }
                        } else if (d2.m() != null) {
                            objArr[((i6 / 4) << 1) + 1] = d2.m();
                        } else if (d2.n() != null) {
                            objArr[((i6 / 4) << 1) + 1] = d2.n();
                        }
                        int c2 = d2.c();
                        if (c2 == abk.MAP.ordinal()) {
                            int i7 = i4 + 1;
                            iArr2[i4] = i6;
                            i4 = i7;
                        } else if (c2 >= 18 && c2 <= 49) {
                            int i8 = i5 + 1;
                            iArr3[i5] = iArr[i6 + 1] & 1048575;
                            i5 = i8;
                        }
                        if (!d2.a()) {
                            break;
                        }
                        b2 = d2.b();
                    } else {
                        for (int i9 = 0; i9 < 4; i9++) {
                            iArr[i6 + i9] = -1;
                        }
                    }
                    i6 += 4;
                }
            }
            return new ada<>(iArr, objArr, e2, f2, adl.l(), adl.c(), z, false, adl.j(), iArr2, iArr3, adf, acg, aei, abe, acr);
        }
        ((aec) acu).a();
        throw new NoSuchMethodError();
    }

    private final adp a(int i2) {
        int i3 = (i2 / 4) << 1;
        adp adp = (adp) this.c[i3];
        if (adp != null) {
            return adp;
        }
        adp a2 = adj.a().a((Class) this.c[i3 + 1]);
        this.c[i3] = a2;
        return a2;
    }

    private final <K, V, UT, UB> UB a(int i2, int i3, Map<K, V> map, abt<?> abt, UB ub, aei<UT, UB> aei) {
        acp f2 = this.s.f(b(i2));
        Iterator it = map.entrySet().iterator();
        while (it.hasNext()) {
            Entry entry = (Entry) it.next();
            if (abt.a(((Integer) entry.getValue()).intValue()) == null) {
                if (ub == null) {
                    ub = aei.a();
                }
                aam b2 = aah.b(aco.a(f2, entry.getKey(), entry.getValue()));
                try {
                    aco.a(b2.b(), f2, entry.getKey(), entry.getValue());
                    aei.a(ub, i3, b2.a());
                    it.remove();
                } catch (IOException e2) {
                    throw new RuntimeException(e2);
                }
            }
        }
        return ub;
    }

    private final <UT, UB> UB a(Object obj, int i2, UB ub, aei<UT, UB> aei) {
        int i3 = this.f2468b[i2];
        Object f2 = aeo.f(obj, (long) (d(i2) & 1048575));
        if (f2 == null) {
            return ub;
        }
        abt c2 = c(i2);
        if (c2 == null) {
            return ub;
        }
        return a(i2, i3, this.s.a(f2), c2, ub, aei);
    }

    private static <E> List<E> a(Object obj, long j2) {
        return (List) aeo.f(obj, j2);
    }

    private static void a(int i2, Object obj, afc afc) throws IOException {
        if (obj instanceof String) {
            afc.a(i2, (String) obj);
        } else {
            afc.a(i2, (aah) obj);
        }
    }

    private static <UT, UB> void a(aei<UT, UB> aei, T t, afc afc) throws IOException {
        aei.a(aei.b(t), afc);
    }

    private final <K, V> void a(afc afc, int i2, Object obj, int i3) throws IOException {
        if (obj != null) {
            afc.a(i2, this.s.f(b(i3)), this.s.b(obj));
        }
    }

    private final void a(Object obj, int i2, ado ado) throws IOException {
        if (f(i2)) {
            aeo.a(obj, (long) (i2 & 1048575), (Object) ado.m());
        } else if (this.i) {
            aeo.a(obj, (long) (i2 & 1048575), (Object) ado.l());
        } else {
            aeo.a(obj, (long) (i2 & 1048575), (Object) ado.n());
        }
    }

    private final void a(T t, T t2, int i2) {
        long d2 = (long) (d(i2) & 1048575);
        if (a(t2, i2)) {
            Object f2 = aeo.f(t, d2);
            Object f3 = aeo.f(t2, d2);
            if (f2 != null && f3 != null) {
                aeo.a((Object) t, d2, abr.a(f2, f3));
                b(t, i2);
            } else if (f3 != null) {
                aeo.a((Object) t, d2, f3);
                b(t, i2);
            }
        }
    }

    private final boolean a(T t, int i2) {
        if (this.j) {
            int d2 = d(i2);
            long j2 = (long) (d2 & 1048575);
            switch ((d2 & 267386880) >>> 20) {
                case 0:
                    return aeo.e(t, j2) != 0.0d;
                case 1:
                    return aeo.d(t, j2) != 0.0f;
                case 2:
                    return aeo.b(t, j2) != 0;
                case 3:
                    return aeo.b(t, j2) != 0;
                case 4:
                    return aeo.a((Object) t, j2) != 0;
                case 5:
                    return aeo.b(t, j2) != 0;
                case 6:
                    return aeo.a((Object) t, j2) != 0;
                case 7:
                    return aeo.c(t, j2);
                case 8:
                    Object f2 = aeo.f(t, j2);
                    if (f2 instanceof String) {
                        return !((String) f2).isEmpty();
                    }
                    if (f2 instanceof aah) {
                        return !aah.f2393a.equals(f2);
                    }
                    throw new IllegalArgumentException();
                case 9:
                    return aeo.f(t, j2) != null;
                case 10:
                    return !aah.f2393a.equals(aeo.f(t, j2));
                case 11:
                    return aeo.a((Object) t, j2) != 0;
                case 12:
                    return aeo.a((Object) t, j2) != 0;
                case 13:
                    return aeo.a((Object) t, j2) != 0;
                case 14:
                    return aeo.b(t, j2) != 0;
                case 15:
                    return aeo.a((Object) t, j2) != 0;
                case 16:
                    return aeo.b(t, j2) != 0;
                case 17:
                    return aeo.f(t, j2) != null;
                default:
                    throw new IllegalArgumentException();
            }
        } else {
            int e2 = e(i2);
            return (aeo.a((Object) t, (long) (e2 & 1048575)) & (1 << (e2 >>> 20))) != 0;
        }
    }

    private final boolean a(T t, int i2, int i3) {
        return aeo.a((Object) t, (long) (e(i3) & 1048575)) == i2;
    }

    private final boolean a(T t, int i2, int i3, int i4) {
        return this.j ? a(t, i2) : (i3 & i4) != 0;
    }

    private static boolean a(Object obj, int i2, adp adp) {
        return adp.d(aeo.f(obj, (long) (1048575 & i2)));
    }

    private static <T> double b(T t, long j2) {
        return ((Double) aeo.f(t, j2)).doubleValue();
    }

    private final Object b(int i2) {
        return this.c[(i2 / 4) << 1];
    }

    private final void b(T t, int i2) {
        if (!this.j) {
            int e2 = e(i2);
            long j2 = (long) (e2 & 1048575);
            aeo.a((Object) t, j2, aeo.a((Object) t, j2) | (1 << (e2 >>> 20)));
        }
    }

    private final void b(T t, int i2, int i3) {
        aeo.a((Object) t, (long) (e(i3) & 1048575), i2);
    }

    /* JADX WARNING: CFG modification limit reached, blocks count: 387 */
    private final void b(T t, afc afc) throws IOException {
        Entry entry;
        int i2;
        int i3;
        Iterator it = null;
        Entry entry2 = null;
        if (this.h) {
            abh a2 = this.r.a((Object) t);
            if (!a2.b()) {
                it = a2.e();
                entry2 = (Entry) it.next();
            }
        }
        int length = this.f2468b.length;
        Unsafe unsafe = f2467a;
        int i4 = 0;
        int i5 = -1;
        Entry entry3 = entry2;
        int i6 = 0;
        while (i4 < length) {
            int d2 = d(i4);
            int i7 = this.f2468b[i4];
            int i8 = (267386880 & d2) >>> 20;
            int i9 = 0;
            if (this.j || i8 > 17) {
                entry = entry3;
                i2 = i5;
                i3 = i6;
            } else {
                int i10 = this.f2468b[i4 + 2];
                int i11 = 1048575 & i10;
                if (i11 != i5) {
                    i6 = unsafe.getInt(t, (long) i11);
                } else {
                    i11 = i5;
                }
                int i12 = 1 << (i10 >>> 20);
                entry = entry3;
                i2 = i11;
                i9 = i12;
                i3 = i6;
            }
            while (entry != null && this.r.a(entry) <= i7) {
                this.r.a(afc, entry);
                entry = it.hasNext() ? (Entry) it.next() : null;
            }
            long j2 = (long) (1048575 & d2);
            switch (i8) {
                case 0:
                    if ((i3 & i9) == 0) {
                        break;
                    } else {
                        afc.a(i7, aeo.e(t, j2));
                        break;
                    }
                case 1:
                    if ((i3 & i9) == 0) {
                        break;
                    } else {
                        afc.a(i7, aeo.d(t, j2));
                        break;
                    }
                case 2:
                    if ((i3 & i9) == 0) {
                        break;
                    } else {
                        afc.a(i7, unsafe.getLong(t, j2));
                        break;
                    }
                case 3:
                    if ((i3 & i9) == 0) {
                        break;
                    } else {
                        afc.c(i7, unsafe.getLong(t, j2));
                        break;
                    }
                case 4:
                    if ((i3 & i9) == 0) {
                        break;
                    } else {
                        afc.c(i7, unsafe.getInt(t, j2));
                        break;
                    }
                case 5:
                    if ((i3 & i9) == 0) {
                        break;
                    } else {
                        afc.d(i7, unsafe.getLong(t, j2));
                        break;
                    }
                case 6:
                    if ((i3 & i9) == 0) {
                        break;
                    } else {
                        afc.d(i7, unsafe.getInt(t, j2));
                        break;
                    }
                case 7:
                    if ((i3 & i9) == 0) {
                        break;
                    } else {
                        afc.a(i7, aeo.c(t, j2));
                        break;
                    }
                case 8:
                    if ((i3 & i9) == 0) {
                        break;
                    } else {
                        a(i7, unsafe.getObject(t, j2), afc);
                        break;
                    }
                case 9:
                    if ((i3 & i9) == 0) {
                        break;
                    } else {
                        afc.a(i7, unsafe.getObject(t, j2), a(i4));
                        break;
                    }
                case 10:
                    if ((i3 & i9) == 0) {
                        break;
                    } else {
                        afc.a(i7, (aah) unsafe.getObject(t, j2));
                        break;
                    }
                case 11:
                    if ((i3 & i9) == 0) {
                        break;
                    } else {
                        afc.e(i7, unsafe.getInt(t, j2));
                        break;
                    }
                case 12:
                    if ((i3 & i9) == 0) {
                        break;
                    } else {
                        afc.b(i7, unsafe.getInt(t, j2));
                        break;
                    }
                case 13:
                    if ((i3 & i9) == 0) {
                        break;
                    } else {
                        afc.a(i7, unsafe.getInt(t, j2));
                        break;
                    }
                case 14:
                    if ((i3 & i9) == 0) {
                        break;
                    } else {
                        afc.b(i7, unsafe.getLong(t, j2));
                        break;
                    }
                case 15:
                    if ((i3 & i9) == 0) {
                        break;
                    } else {
                        afc.f(i7, unsafe.getInt(t, j2));
                        break;
                    }
                case 16:
                    if ((i3 & i9) == 0) {
                        break;
                    } else {
                        afc.e(i7, unsafe.getLong(t, j2));
                        break;
                    }
                case 17:
                    if ((i3 & i9) == 0) {
                        break;
                    } else {
                        afc.b(i7, unsafe.getObject(t, j2), a(i4));
                        break;
                    }
                case 18:
                    adr.a(this.f2468b[i4], (List) unsafe.getObject(t, j2), afc, false);
                    break;
                case 19:
                    adr.b(this.f2468b[i4], (List) unsafe.getObject(t, j2), afc, false);
                    break;
                case 20:
                    adr.c(this.f2468b[i4], (List) unsafe.getObject(t, j2), afc, false);
                    break;
                case 21:
                    adr.d(this.f2468b[i4], (List) unsafe.getObject(t, j2), afc, false);
                    break;
                case 22:
                    adr.h(this.f2468b[i4], (List) unsafe.getObject(t, j2), afc, false);
                    break;
                case 23:
                    adr.f(this.f2468b[i4], (List) unsafe.getObject(t, j2), afc, false);
                    break;
                case 24:
                    adr.k(this.f2468b[i4], (List) unsafe.getObject(t, j2), afc, false);
                    break;
                case 25:
                    adr.n(this.f2468b[i4], (List) unsafe.getObject(t, j2), afc, false);
                    break;
                case 26:
                    adr.a(this.f2468b[i4], (List) unsafe.getObject(t, j2), afc);
                    break;
                case 27:
                    adr.a(this.f2468b[i4], (List) unsafe.getObject(t, j2), afc, a(i4));
                    break;
                case 28:
                    adr.b(this.f2468b[i4], (List) unsafe.getObject(t, j2), afc);
                    break;
                case 29:
                    adr.i(this.f2468b[i4], (List) unsafe.getObject(t, j2), afc, false);
                    break;
                case 30:
                    adr.m(this.f2468b[i4], (List) unsafe.getObject(t, j2), afc, false);
                    break;
                case 31:
                    adr.l(this.f2468b[i4], (List) unsafe.getObject(t, j2), afc, false);
                    break;
                case 32:
                    adr.g(this.f2468b[i4], (List) unsafe.getObject(t, j2), afc, false);
                    break;
                case 33:
                    adr.j(this.f2468b[i4], (List) unsafe.getObject(t, j2), afc, false);
                    break;
                case 34:
                    adr.e(this.f2468b[i4], (List) unsafe.getObject(t, j2), afc, false);
                    break;
                case 35:
                    adr.a(this.f2468b[i4], (List) unsafe.getObject(t, j2), afc, true);
                    break;
                case 36:
                    adr.b(this.f2468b[i4], (List) unsafe.getObject(t, j2), afc, true);
                    break;
                case 37:
                    adr.c(this.f2468b[i4], (List) unsafe.getObject(t, j2), afc, true);
                    break;
                case 38:
                    adr.d(this.f2468b[i4], (List) unsafe.getObject(t, j2), afc, true);
                    break;
                case 39:
                    adr.h(this.f2468b[i4], (List) unsafe.getObject(t, j2), afc, true);
                    break;
                case 40:
                    adr.f(this.f2468b[i4], (List) unsafe.getObject(t, j2), afc, true);
                    break;
                case 41:
                    adr.k(this.f2468b[i4], (List) unsafe.getObject(t, j2), afc, true);
                    break;
                case 42:
                    adr.n(this.f2468b[i4], (List) unsafe.getObject(t, j2), afc, true);
                    break;
                case 43:
                    adr.i(this.f2468b[i4], (List) unsafe.getObject(t, j2), afc, true);
                    break;
                case 44:
                    adr.m(this.f2468b[i4], (List) unsafe.getObject(t, j2), afc, true);
                    break;
                case 45:
                    adr.l(this.f2468b[i4], (List) unsafe.getObject(t, j2), afc, true);
                    break;
                case 46:
                    adr.g(this.f2468b[i4], (List) unsafe.getObject(t, j2), afc, true);
                    break;
                case 47:
                    adr.j(this.f2468b[i4], (List) unsafe.getObject(t, j2), afc, true);
                    break;
                case 48:
                    adr.e(this.f2468b[i4], (List) unsafe.getObject(t, j2), afc, true);
                    break;
                case 49:
                    adr.b(this.f2468b[i4], (List) unsafe.getObject(t, j2), afc, a(i4));
                    break;
                case 50:
                    a(afc, i7, unsafe.getObject(t, j2), i4);
                    break;
                case 51:
                    if (!a(t, i7, i4)) {
                        break;
                    } else {
                        afc.a(i7, b(t, j2));
                        break;
                    }
                case 52:
                    if (!a(t, i7, i4)) {
                        break;
                    } else {
                        afc.a(i7, c(t, j2));
                        break;
                    }
                case 53:
                    if (!a(t, i7, i4)) {
                        break;
                    } else {
                        afc.a(i7, e(t, j2));
                        break;
                    }
                case 54:
                    if (!a(t, i7, i4)) {
                        break;
                    } else {
                        afc.c(i7, e(t, j2));
                        break;
                    }
                case 55:
                    if (!a(t, i7, i4)) {
                        break;
                    } else {
                        afc.c(i7, d(t, j2));
                        break;
                    }
                case 56:
                    if (!a(t, i7, i4)) {
                        break;
                    } else {
                        afc.d(i7, e(t, j2));
                        break;
                    }
                case 57:
                    if (!a(t, i7, i4)) {
                        break;
                    } else {
                        afc.d(i7, d(t, j2));
                        break;
                    }
                case 58:
                    if (!a(t, i7, i4)) {
                        break;
                    } else {
                        afc.a(i7, f(t, j2));
                        break;
                    }
                case 59:
                    if (!a(t, i7, i4)) {
                        break;
                    } else {
                        a(i7, unsafe.getObject(t, j2), afc);
                        break;
                    }
                case 60:
                    if (!a(t, i7, i4)) {
                        break;
                    } else {
                        afc.a(i7, unsafe.getObject(t, j2), a(i4));
                        break;
                    }
                case 61:
                    if (!a(t, i7, i4)) {
                        break;
                    } else {
                        afc.a(i7, (aah) unsafe.getObject(t, j2));
                        break;
                    }
                case 62:
                    if (!a(t, i7, i4)) {
                        break;
                    } else {
                        afc.e(i7, d(t, j2));
                        break;
                    }
                case 63:
                    if (!a(t, i7, i4)) {
                        break;
                    } else {
                        afc.b(i7, d(t, j2));
                        break;
                    }
                case 64:
                    if (!a(t, i7, i4)) {
                        break;
                    } else {
                        afc.a(i7, d(t, j2));
                        break;
                    }
                case 65:
                    if (!a(t, i7, i4)) {
                        break;
                    } else {
                        afc.b(i7, e(t, j2));
                        break;
                    }
                case 66:
                    if (!a(t, i7, i4)) {
                        break;
                    } else {
                        afc.f(i7, d(t, j2));
                        break;
                    }
                case 67:
                    if (!a(t, i7, i4)) {
                        break;
                    } else {
                        afc.e(i7, e(t, j2));
                        break;
                    }
                case 68:
                    if (!a(t, i7, i4)) {
                        break;
                    } else {
                        afc.b(i7, unsafe.getObject(t, j2), a(i4));
                        break;
                    }
            }
            i4 += 4;
            i6 = i3;
            i5 = i2;
            entry3 = entry;
        }
        Entry entry4 = entry3;
        while (entry4 != null) {
            this.r.a(afc, entry4);
            entry4 = it.hasNext() ? (Entry) it.next() : null;
        }
        a(this.q, t, afc);
    }

    private final void b(T t, T t2, int i2) {
        int d2 = d(i2);
        int i3 = this.f2468b[i2];
        long j2 = (long) (d2 & 1048575);
        if (a(t2, i3, i2)) {
            Object f2 = aeo.f(t, j2);
            Object f3 = aeo.f(t2, j2);
            if (f2 != null && f3 != null) {
                aeo.a((Object) t, j2, abr.a(f2, f3));
                b(t, i3, i2);
            } else if (f3 != null) {
                aeo.a((Object) t, j2, f3);
                b(t, i3, i2);
            }
        }
    }

    private static <T> float c(T t, long j2) {
        return ((Float) aeo.f(t, j2)).floatValue();
    }

    private final abt<?> c(int i2) {
        return (abt) this.c[((i2 / 4) << 1) + 1];
    }

    private final boolean c(T t, T t2, int i2) {
        return a(t, i2) == a(t2, i2);
    }

    private final int d(int i2) {
        return this.f2468b[i2 + 1];
    }

    private static <T> int d(T t, long j2) {
        return ((Integer) aeo.f(t, j2)).intValue();
    }

    private final int e(int i2) {
        return this.f2468b[i2 + 2];
    }

    private static <T> long e(T t, long j2) {
        return ((Long) aeo.f(t, j2)).longValue();
    }

    private static aej e(Object obj) {
        aej aej = ((abp) obj).zzdtt;
        if (aej != aej.a()) {
            return aej;
        }
        aej b2 = aej.b();
        ((abp) obj).zzdtt = b2;
        return b2;
    }

    private static boolean f(int i2) {
        return (536870912 & i2) != 0;
    }

    private static <T> boolean f(T t, long j2) {
        return ((Boolean) aeo.f(t, j2)).booleanValue();
    }

    private final int g(int i2) {
        if (i2 >= this.d) {
            if (i2 < this.f) {
                int i3 = (i2 - this.d) << 2;
                if (this.f2468b[i3] == i2) {
                    return i3;
                }
                return -1;
            } else if (i2 <= this.e) {
                int i4 = this.f - this.d;
                int length = (this.f2468b.length / 4) - 1;
                while (i4 <= length) {
                    int i5 = (length + i4) >>> 1;
                    int i6 = i5 << 2;
                    int i7 = this.f2468b[i6];
                    if (i2 == i7) {
                        return i6;
                    }
                    if (i2 < i7) {
                        length = i5 - 1;
                    } else {
                        i4 = i5 + 1;
                    }
                }
                return -1;
            }
        }
        return -1;
    }

    public final int a(T t) {
        int hashCode;
        int length = this.f2468b.length;
        int i2 = 0;
        int i3 = 0;
        while (i2 < length) {
            int d2 = d(i2);
            int i4 = this.f2468b[i2];
            long j2 = (long) (1048575 & d2);
            switch ((d2 & 267386880) >>> 20) {
                case 0:
                    hashCode = (i3 * 53) + abr.a(Double.doubleToLongBits(aeo.e(t, j2)));
                    break;
                case 1:
                    hashCode = (i3 * 53) + Float.floatToIntBits(aeo.d(t, j2));
                    break;
                case 2:
                    hashCode = (i3 * 53) + abr.a(aeo.b(t, j2));
                    break;
                case 3:
                    hashCode = (i3 * 53) + abr.a(aeo.b(t, j2));
                    break;
                case 4:
                    hashCode = (i3 * 53) + aeo.a((Object) t, j2);
                    break;
                case 5:
                    hashCode = (i3 * 53) + abr.a(aeo.b(t, j2));
                    break;
                case 6:
                    hashCode = (i3 * 53) + aeo.a((Object) t, j2);
                    break;
                case 7:
                    hashCode = (i3 * 53) + abr.a(aeo.c(t, j2));
                    break;
                case 8:
                    hashCode = ((String) aeo.f(t, j2)).hashCode() + (i3 * 53);
                    break;
                case 9:
                    Object f2 = aeo.f(t, j2);
                    hashCode = (f2 != null ? f2.hashCode() : 37) + (i3 * 53);
                    break;
                case 10:
                    hashCode = (i3 * 53) + aeo.f(t, j2).hashCode();
                    break;
                case 11:
                    hashCode = (i3 * 53) + aeo.a((Object) t, j2);
                    break;
                case 12:
                    hashCode = (i3 * 53) + aeo.a((Object) t, j2);
                    break;
                case 13:
                    hashCode = (i3 * 53) + aeo.a((Object) t, j2);
                    break;
                case 14:
                    hashCode = (i3 * 53) + abr.a(aeo.b(t, j2));
                    break;
                case 15:
                    hashCode = (i3 * 53) + aeo.a((Object) t, j2);
                    break;
                case 16:
                    hashCode = (i3 * 53) + abr.a(aeo.b(t, j2));
                    break;
                case 17:
                    Object f3 = aeo.f(t, j2);
                    hashCode = (f3 != null ? f3.hashCode() : 37) + (i3 * 53);
                    break;
                case 18:
                case 19:
                case 20:
                case 21:
                case 22:
                case 23:
                case 24:
                case 25:
                case 26:
                case 27:
                case 28:
                case 29:
                case 30:
                case 31:
                case 32:
                case 33:
                case 34:
                case 35:
                case 36:
                case 37:
                case 38:
                case 39:
                case 40:
                case 41:
                case 42:
                case 43:
                case 44:
                case 45:
                case 46:
                case 47:
                case 48:
                case 49:
                    hashCode = (i3 * 53) + aeo.f(t, j2).hashCode();
                    break;
                case 50:
                    hashCode = (i3 * 53) + aeo.f(t, j2).hashCode();
                    break;
                case 51:
                    if (a(t, i4, i2)) {
                        hashCode = (i3 * 53) + abr.a(Double.doubleToLongBits(b(t, j2)));
                        break;
                    }
                case 52:
                    if (a(t, i4, i2)) {
                        hashCode = (i3 * 53) + Float.floatToIntBits(c(t, j2));
                        break;
                    }
                case 53:
                    if (a(t, i4, i2)) {
                        hashCode = (i3 * 53) + abr.a(e(t, j2));
                        break;
                    }
                case 54:
                    if (a(t, i4, i2)) {
                        hashCode = (i3 * 53) + abr.a(e(t, j2));
                        break;
                    }
                case 55:
                    if (a(t, i4, i2)) {
                        hashCode = (i3 * 53) + d(t, j2);
                        break;
                    }
                case 56:
                    if (a(t, i4, i2)) {
                        hashCode = (i3 * 53) + abr.a(e(t, j2));
                        break;
                    }
                case 57:
                    if (a(t, i4, i2)) {
                        hashCode = (i3 * 53) + d(t, j2);
                        break;
                    }
                case 58:
                    if (a(t, i4, i2)) {
                        hashCode = (i3 * 53) + abr.a(f(t, j2));
                        break;
                    }
                case 59:
                    if (a(t, i4, i2)) {
                        hashCode = ((String) aeo.f(t, j2)).hashCode() + (i3 * 53);
                        break;
                    }
                case 60:
                    if (a(t, i4, i2)) {
                        hashCode = aeo.f(t, j2).hashCode() + (i3 * 53);
                        break;
                    }
                case 61:
                    if (a(t, i4, i2)) {
                        hashCode = (i3 * 53) + aeo.f(t, j2).hashCode();
                        break;
                    }
                case 62:
                    if (a(t, i4, i2)) {
                        hashCode = (i3 * 53) + d(t, j2);
                        break;
                    }
                case 63:
                    if (a(t, i4, i2)) {
                        hashCode = (i3 * 53) + d(t, j2);
                        break;
                    }
                case 64:
                    if (a(t, i4, i2)) {
                        hashCode = (i3 * 53) + d(t, j2);
                        break;
                    }
                case 65:
                    if (a(t, i4, i2)) {
                        hashCode = (i3 * 53) + abr.a(e(t, j2));
                        break;
                    }
                case 66:
                    if (a(t, i4, i2)) {
                        hashCode = (i3 * 53) + d(t, j2);
                        break;
                    }
                case 67:
                    if (a(t, i4, i2)) {
                        hashCode = (i3 * 53) + abr.a(e(t, j2));
                        break;
                    }
                case 68:
                    if (a(t, i4, i2)) {
                        hashCode = aeo.f(t, j2).hashCode() + (i3 * 53);
                        break;
                    }
                default:
                    hashCode = i3;
                    break;
            }
            i2 += 4;
            i3 = hashCode;
        }
        int hashCode2 = (i3 * 53) + this.q.b(t).hashCode();
        return this.h ? (hashCode2 * 53) + this.r.a((Object) t).hashCode() : hashCode2;
    }

    public final T a() {
        return this.o.a(this.g);
    }

    public final void a(T t, ado ado, abc abc) throws IOException {
        Object obj;
        if (abc == null) {
            throw new NullPointerException();
        }
        aei<?, ?> aei = this.q;
        abe<?> abe = this.r;
        Object obj2 = null;
        abh abh = null;
        while (true) {
            try {
                int a2 = ado.a();
                int g2 = g(a2);
                if (g2 >= 0) {
                    int d2 = d(g2);
                    switch ((267386880 & d2) >>> 20) {
                        case 0:
                            aeo.a((Object) t, (long) (1048575 & d2), ado.d());
                            b(t, g2);
                            break;
                        case 1:
                            aeo.a((Object) t, (long) (1048575 & d2), ado.e());
                            b(t, g2);
                            break;
                        case 2:
                            aeo.a((Object) t, (long) (1048575 & d2), ado.g());
                            b(t, g2);
                            break;
                        case 3:
                            aeo.a((Object) t, (long) (1048575 & d2), ado.f());
                            b(t, g2);
                            break;
                        case 4:
                            aeo.a((Object) t, (long) (1048575 & d2), ado.h());
                            b(t, g2);
                            break;
                        case 5:
                            aeo.a((Object) t, (long) (1048575 & d2), ado.i());
                            b(t, g2);
                            break;
                        case 6:
                            aeo.a((Object) t, (long) (1048575 & d2), ado.j());
                            b(t, g2);
                            break;
                        case 7:
                            aeo.a((Object) t, (long) (1048575 & d2), ado.k());
                            b(t, g2);
                            break;
                        case 8:
                            a((Object) t, d2, ado);
                            b(t, g2);
                            break;
                        case 9:
                            if (!a(t, g2)) {
                                aeo.a((Object) t, (long) (1048575 & d2), ado.a(a(g2), abc));
                                b(t, g2);
                                break;
                            } else {
                                aeo.a((Object) t, (long) (1048575 & d2), abr.a(aeo.f(t, (long) (1048575 & d2)), ado.a(a(g2), abc)));
                                break;
                            }
                        case 10:
                            aeo.a((Object) t, (long) (1048575 & d2), (Object) ado.n());
                            b(t, g2);
                            break;
                        case 11:
                            aeo.a((Object) t, (long) (1048575 & d2), ado.o());
                            b(t, g2);
                            break;
                        case 12:
                            int p2 = ado.p();
                            abt c2 = c(g2);
                            if (c2 != null && c2.a(p2) == null) {
                                obj2 = adr.a(a2, p2, obj2, aei);
                                break;
                            } else {
                                aeo.a((Object) t, (long) (1048575 & d2), p2);
                                b(t, g2);
                                break;
                            }
                        case 13:
                            aeo.a((Object) t, (long) (1048575 & d2), ado.q());
                            b(t, g2);
                            break;
                        case 14:
                            aeo.a((Object) t, (long) (1048575 & d2), ado.r());
                            b(t, g2);
                            break;
                        case 15:
                            aeo.a((Object) t, (long) (1048575 & d2), ado.s());
                            b(t, g2);
                            break;
                        case 16:
                            aeo.a((Object) t, (long) (1048575 & d2), ado.t());
                            b(t, g2);
                            break;
                        case 17:
                            if (!a(t, g2)) {
                                aeo.a((Object) t, (long) (1048575 & d2), ado.b(a(g2), abc));
                                b(t, g2);
                                break;
                            } else {
                                aeo.a((Object) t, (long) (1048575 & d2), abr.a(aeo.f(t, (long) (1048575 & d2)), ado.b(a(g2), abc)));
                                break;
                            }
                        case 18:
                            ado.a(this.p.a(t, (long) (1048575 & d2)));
                            break;
                        case 19:
                            ado.b(this.p.a(t, (long) (1048575 & d2)));
                            break;
                        case 20:
                            ado.d(this.p.a(t, (long) (1048575 & d2)));
                            break;
                        case 21:
                            ado.c(this.p.a(t, (long) (1048575 & d2)));
                            break;
                        case 22:
                            ado.e(this.p.a(t, (long) (1048575 & d2)));
                            break;
                        case 23:
                            ado.f(this.p.a(t, (long) (1048575 & d2)));
                            break;
                        case 24:
                            ado.g(this.p.a(t, (long) (1048575 & d2)));
                            break;
                        case 25:
                            ado.h(this.p.a(t, (long) (1048575 & d2)));
                            break;
                        case 26:
                            if (!f(d2)) {
                                ado.i(this.p.a(t, (long) (1048575 & d2)));
                                break;
                            } else {
                                ado.j(this.p.a(t, (long) (1048575 & d2)));
                                break;
                            }
                        case 27:
                            ado.a(this.p.a(t, (long) (1048575 & d2)), a(g2), abc);
                            break;
                        case 28:
                            ado.k(this.p.a(t, (long) (1048575 & d2)));
                            break;
                        case 29:
                            ado.l(this.p.a(t, (long) (1048575 & d2)));
                            break;
                        case 30:
                            List a3 = this.p.a(t, (long) (d2 & 1048575));
                            ado.m(a3);
                            obj2 = adr.a(a2, a3, c(g2), obj2, aei);
                            break;
                        case 31:
                            ado.n(this.p.a(t, (long) (1048575 & d2)));
                            break;
                        case 32:
                            ado.o(this.p.a(t, (long) (1048575 & d2)));
                            break;
                        case 33:
                            ado.p(this.p.a(t, (long) (1048575 & d2)));
                            break;
                        case 34:
                            ado.q(this.p.a(t, (long) (1048575 & d2)));
                            break;
                        case 35:
                            ado.a(this.p.a(t, (long) (1048575 & d2)));
                            break;
                        case 36:
                            ado.b(this.p.a(t, (long) (1048575 & d2)));
                            break;
                        case 37:
                            ado.d(this.p.a(t, (long) (1048575 & d2)));
                            break;
                        case 38:
                            ado.c(this.p.a(t, (long) (1048575 & d2)));
                            break;
                        case 39:
                            ado.e(this.p.a(t, (long) (1048575 & d2)));
                            break;
                        case 40:
                            ado.f(this.p.a(t, (long) (1048575 & d2)));
                            break;
                        case 41:
                            ado.g(this.p.a(t, (long) (1048575 & d2)));
                            break;
                        case 42:
                            ado.h(this.p.a(t, (long) (1048575 & d2)));
                            break;
                        case 43:
                            ado.l(this.p.a(t, (long) (1048575 & d2)));
                            break;
                        case 44:
                            List a4 = this.p.a(t, (long) (d2 & 1048575));
                            ado.m(a4);
                            obj2 = adr.a(a2, a4, c(g2), obj2, aei);
                            break;
                        case 45:
                            ado.n(this.p.a(t, (long) (1048575 & d2)));
                            break;
                        case 46:
                            ado.o(this.p.a(t, (long) (1048575 & d2)));
                            break;
                        case 47:
                            ado.p(this.p.a(t, (long) (1048575 & d2)));
                            break;
                        case 48:
                            ado.q(this.p.a(t, (long) (1048575 & d2)));
                            break;
                        case 49:
                            ado.b(this.p.a(t, (long) (1048575 & d2)), a(g2), abc);
                            break;
                        case 50:
                            Object b2 = b(g2);
                            long d3 = (long) (d(g2) & 1048575);
                            Object f2 = aeo.f(t, d3);
                            if (f2 == null) {
                                obj = this.s.e(b2);
                                aeo.a((Object) t, d3, obj);
                            } else if (this.s.c(f2)) {
                                obj = this.s.e(b2);
                                this.s.a(obj, f2);
                                aeo.a((Object) t, d3, obj);
                            } else {
                                obj = f2;
                            }
                            ado.a(this.s.a(obj), this.s.f(b2), abc);
                            break;
                        case 51:
                            aeo.a((Object) t, (long) (d2 & 1048575), (Object) Double.valueOf(ado.d()));
                            b(t, a2, g2);
                            break;
                        case 52:
                            aeo.a((Object) t, (long) (d2 & 1048575), (Object) Float.valueOf(ado.e()));
                            b(t, a2, g2);
                            break;
                        case 53:
                            aeo.a((Object) t, (long) (d2 & 1048575), (Object) Long.valueOf(ado.g()));
                            b(t, a2, g2);
                            break;
                        case 54:
                            aeo.a((Object) t, (long) (d2 & 1048575), (Object) Long.valueOf(ado.f()));
                            b(t, a2, g2);
                            break;
                        case 55:
                            aeo.a((Object) t, (long) (d2 & 1048575), (Object) Integer.valueOf(ado.h()));
                            b(t, a2, g2);
                            break;
                        case 56:
                            aeo.a((Object) t, (long) (d2 & 1048575), (Object) Long.valueOf(ado.i()));
                            b(t, a2, g2);
                            break;
                        case 57:
                            aeo.a((Object) t, (long) (d2 & 1048575), (Object) Integer.valueOf(ado.j()));
                            b(t, a2, g2);
                            break;
                        case 58:
                            aeo.a((Object) t, (long) (d2 & 1048575), (Object) Boolean.valueOf(ado.k()));
                            b(t, a2, g2);
                            break;
                        case 59:
                            a((Object) t, d2, ado);
                            b(t, a2, g2);
                            break;
                        case 60:
                            if (a(t, a2, g2)) {
                                aeo.a((Object) t, (long) (d2 & 1048575), abr.a(aeo.f(t, (long) (1048575 & d2)), ado.a(a(g2), abc)));
                            } else {
                                aeo.a((Object) t, (long) (d2 & 1048575), ado.a(a(g2), abc));
                                b(t, g2);
                            }
                            b(t, a2, g2);
                            break;
                        case 61:
                            aeo.a((Object) t, (long) (d2 & 1048575), (Object) ado.n());
                            b(t, a2, g2);
                            break;
                        case 62:
                            aeo.a((Object) t, (long) (d2 & 1048575), (Object) Integer.valueOf(ado.o()));
                            b(t, a2, g2);
                            break;
                        case 63:
                            int p3 = ado.p();
                            abt c3 = c(g2);
                            if (c3 != null && c3.a(p3) == null) {
                                obj2 = adr.a(a2, p3, obj2, aei);
                                break;
                            } else {
                                aeo.a((Object) t, (long) (d2 & 1048575), (Object) Integer.valueOf(p3));
                                b(t, a2, g2);
                                break;
                            }
                            break;
                        case 64:
                            aeo.a((Object) t, (long) (d2 & 1048575), (Object) Integer.valueOf(ado.q()));
                            b(t, a2, g2);
                            break;
                        case 65:
                            aeo.a((Object) t, (long) (d2 & 1048575), (Object) Long.valueOf(ado.r()));
                            b(t, a2, g2);
                            break;
                        case 66:
                            aeo.a((Object) t, (long) (d2 & 1048575), (Object) Integer.valueOf(ado.s()));
                            b(t, a2, g2);
                            break;
                        case 67:
                            aeo.a((Object) t, (long) (d2 & 1048575), (Object) Long.valueOf(ado.t()));
                            b(t, a2, g2);
                            break;
                        case 68:
                            aeo.a((Object) t, (long) (d2 & 1048575), ado.b(a(g2), abc));
                            b(t, a2, g2);
                            break;
                        default:
                            if (obj2 == null) {
                                obj2 = aei.a();
                            }
                            if (aei.a(obj2, ado)) {
                                break;
                            } else {
                                if (this.m != null) {
                                    for (int a5 : this.m) {
                                        obj2 = a((Object) t, a5, (UB) obj2, aei);
                                    }
                                }
                                if (obj2 != null) {
                                    aei.b((Object) t, obj2);
                                    return;
                                }
                                return;
                            }
                    }
                } else if (a2 == Integer.MAX_VALUE) {
                    if (this.m != null) {
                        for (int a6 : this.m) {
                            obj2 = a((Object) t, a6, (UB) obj2, aei);
                        }
                    }
                    if (obj2 != null) {
                        aei.b((Object) t, obj2);
                        return;
                    }
                    return;
                } else {
                    Object a7 = !this.h ? null : abe.a(abc, this.g, a2);
                    if (a7 != null) {
                        if (abh == null) {
                            abh = abe.b(t);
                        }
                        obj2 = abe.a(ado, a7, abc, abh, obj2, aei);
                    } else {
                        aei.a(ado);
                        if (obj2 == null) {
                            obj2 = aei.c(t);
                        }
                        if (!aei.a(obj2, ado)) {
                            if (this.m != null) {
                                for (int a8 : this.m) {
                                    obj2 = a((Object) t, a8, (UB) obj2, aei);
                                }
                            }
                            if (obj2 != null) {
                                aei.b((Object) t, obj2);
                                return;
                            }
                            return;
                        }
                    }
                }
            } catch (abw e2) {
                aei.a(ado);
                if (obj2 == null) {
                    obj2 = aei.c(t);
                }
                if (!aei.a(obj2, ado)) {
                    if (this.m != null) {
                        for (int a9 : this.m) {
                            obj2 = a((Object) t, a9, (UB) obj2, aei);
                        }
                    }
                    if (obj2 != null) {
                        aei.b((Object) t, obj2);
                        return;
                    }
                    return;
                }
            } catch (Throwable th) {
                if (this.m != null) {
                    for (int a10 : this.m) {
                        obj2 = a((Object) t, a10, (UB) obj2, aei);
                    }
                }
                if (obj2 != null) {
                    aei.b((Object) t, obj2);
                }
                throw th;
            }
        }
    }

    /* JADX WARNING: CFG modification limit reached, blocks count: 547 */
    public final void a(T t, afc afc) throws IOException {
        if (afc.a() == e.k) {
            a(this.q, t, afc);
            Iterator it = null;
            Entry entry = null;
            if (this.h) {
                abh a2 = this.r.a((Object) t);
                if (!a2.b()) {
                    it = a2.f();
                    entry = (Entry) it.next();
                }
            }
            int length = this.f2468b.length - 4;
            while (length >= 0) {
                int d2 = d(length);
                int i2 = this.f2468b[length];
                Entry entry2 = entry;
                while (entry2 != null && this.r.a(entry2) > i2) {
                    this.r.a(afc, entry2);
                    entry2 = it.hasNext() ? (Entry) it.next() : null;
                }
                switch ((267386880 & d2) >>> 20) {
                    case 0:
                        if (!a(t, length)) {
                            break;
                        } else {
                            afc.a(i2, aeo.e(t, (long) (1048575 & d2)));
                            break;
                        }
                    case 1:
                        if (!a(t, length)) {
                            break;
                        } else {
                            afc.a(i2, aeo.d(t, (long) (1048575 & d2)));
                            break;
                        }
                    case 2:
                        if (!a(t, length)) {
                            break;
                        } else {
                            afc.a(i2, aeo.b(t, (long) (1048575 & d2)));
                            break;
                        }
                    case 3:
                        if (!a(t, length)) {
                            break;
                        } else {
                            afc.c(i2, aeo.b(t, (long) (1048575 & d2)));
                            break;
                        }
                    case 4:
                        if (!a(t, length)) {
                            break;
                        } else {
                            afc.c(i2, aeo.a((Object) t, (long) (1048575 & d2)));
                            break;
                        }
                    case 5:
                        if (!a(t, length)) {
                            break;
                        } else {
                            afc.d(i2, aeo.b(t, (long) (1048575 & d2)));
                            break;
                        }
                    case 6:
                        if (!a(t, length)) {
                            break;
                        } else {
                            afc.d(i2, aeo.a((Object) t, (long) (1048575 & d2)));
                            break;
                        }
                    case 7:
                        if (!a(t, length)) {
                            break;
                        } else {
                            afc.a(i2, aeo.c(t, (long) (1048575 & d2)));
                            break;
                        }
                    case 8:
                        if (!a(t, length)) {
                            break;
                        } else {
                            a(i2, aeo.f(t, (long) (1048575 & d2)), afc);
                            break;
                        }
                    case 9:
                        if (!a(t, length)) {
                            break;
                        } else {
                            afc.a(i2, aeo.f(t, (long) (1048575 & d2)), a(length));
                            break;
                        }
                    case 10:
                        if (!a(t, length)) {
                            break;
                        } else {
                            afc.a(i2, (aah) aeo.f(t, (long) (1048575 & d2)));
                            break;
                        }
                    case 11:
                        if (!a(t, length)) {
                            break;
                        } else {
                            afc.e(i2, aeo.a((Object) t, (long) (1048575 & d2)));
                            break;
                        }
                    case 12:
                        if (!a(t, length)) {
                            break;
                        } else {
                            afc.b(i2, aeo.a((Object) t, (long) (1048575 & d2)));
                            break;
                        }
                    case 13:
                        if (!a(t, length)) {
                            break;
                        } else {
                            afc.a(i2, aeo.a((Object) t, (long) (1048575 & d2)));
                            break;
                        }
                    case 14:
                        if (!a(t, length)) {
                            break;
                        } else {
                            afc.b(i2, aeo.b(t, (long) (1048575 & d2)));
                            break;
                        }
                    case 15:
                        if (!a(t, length)) {
                            break;
                        } else {
                            afc.f(i2, aeo.a((Object) t, (long) (1048575 & d2)));
                            break;
                        }
                    case 16:
                        if (!a(t, length)) {
                            break;
                        } else {
                            afc.e(i2, aeo.b(t, (long) (1048575 & d2)));
                            break;
                        }
                    case 17:
                        if (!a(t, length)) {
                            break;
                        } else {
                            afc.b(i2, aeo.f(t, (long) (1048575 & d2)), a(length));
                            break;
                        }
                    case 18:
                        adr.a(this.f2468b[length], (List) aeo.f(t, (long) (1048575 & d2)), afc, false);
                        break;
                    case 19:
                        adr.b(this.f2468b[length], (List) aeo.f(t, (long) (1048575 & d2)), afc, false);
                        break;
                    case 20:
                        adr.c(this.f2468b[length], (List) aeo.f(t, (long) (1048575 & d2)), afc, false);
                        break;
                    case 21:
                        adr.d(this.f2468b[length], (List) aeo.f(t, (long) (1048575 & d2)), afc, false);
                        break;
                    case 22:
                        adr.h(this.f2468b[length], (List) aeo.f(t, (long) (1048575 & d2)), afc, false);
                        break;
                    case 23:
                        adr.f(this.f2468b[length], (List) aeo.f(t, (long) (1048575 & d2)), afc, false);
                        break;
                    case 24:
                        adr.k(this.f2468b[length], (List) aeo.f(t, (long) (1048575 & d2)), afc, false);
                        break;
                    case 25:
                        adr.n(this.f2468b[length], (List) aeo.f(t, (long) (1048575 & d2)), afc, false);
                        break;
                    case 26:
                        adr.a(this.f2468b[length], (List) aeo.f(t, (long) (1048575 & d2)), afc);
                        break;
                    case 27:
                        adr.a(this.f2468b[length], (List) aeo.f(t, (long) (1048575 & d2)), afc, a(length));
                        break;
                    case 28:
                        adr.b(this.f2468b[length], (List) aeo.f(t, (long) (1048575 & d2)), afc);
                        break;
                    case 29:
                        adr.i(this.f2468b[length], (List) aeo.f(t, (long) (1048575 & d2)), afc, false);
                        break;
                    case 30:
                        adr.m(this.f2468b[length], (List) aeo.f(t, (long) (1048575 & d2)), afc, false);
                        break;
                    case 31:
                        adr.l(this.f2468b[length], (List) aeo.f(t, (long) (1048575 & d2)), afc, false);
                        break;
                    case 32:
                        adr.g(this.f2468b[length], (List) aeo.f(t, (long) (1048575 & d2)), afc, false);
                        break;
                    case 33:
                        adr.j(this.f2468b[length], (List) aeo.f(t, (long) (1048575 & d2)), afc, false);
                        break;
                    case 34:
                        adr.e(this.f2468b[length], (List) aeo.f(t, (long) (1048575 & d2)), afc, false);
                        break;
                    case 35:
                        adr.a(this.f2468b[length], (List) aeo.f(t, (long) (1048575 & d2)), afc, true);
                        break;
                    case 36:
                        adr.b(this.f2468b[length], (List) aeo.f(t, (long) (1048575 & d2)), afc, true);
                        break;
                    case 37:
                        adr.c(this.f2468b[length], (List) aeo.f(t, (long) (1048575 & d2)), afc, true);
                        break;
                    case 38:
                        adr.d(this.f2468b[length], (List) aeo.f(t, (long) (1048575 & d2)), afc, true);
                        break;
                    case 39:
                        adr.h(this.f2468b[length], (List) aeo.f(t, (long) (1048575 & d2)), afc, true);
                        break;
                    case 40:
                        adr.f(this.f2468b[length], (List) aeo.f(t, (long) (1048575 & d2)), afc, true);
                        break;
                    case 41:
                        adr.k(this.f2468b[length], (List) aeo.f(t, (long) (1048575 & d2)), afc, true);
                        break;
                    case 42:
                        adr.n(this.f2468b[length], (List) aeo.f(t, (long) (1048575 & d2)), afc, true);
                        break;
                    case 43:
                        adr.i(this.f2468b[length], (List) aeo.f(t, (long) (1048575 & d2)), afc, true);
                        break;
                    case 44:
                        adr.m(this.f2468b[length], (List) aeo.f(t, (long) (1048575 & d2)), afc, true);
                        break;
                    case 45:
                        adr.l(this.f2468b[length], (List) aeo.f(t, (long) (1048575 & d2)), afc, true);
                        break;
                    case 46:
                        adr.g(this.f2468b[length], (List) aeo.f(t, (long) (1048575 & d2)), afc, true);
                        break;
                    case 47:
                        adr.j(this.f2468b[length], (List) aeo.f(t, (long) (1048575 & d2)), afc, true);
                        break;
                    case 48:
                        adr.e(this.f2468b[length], (List) aeo.f(t, (long) (1048575 & d2)), afc, true);
                        break;
                    case 49:
                        adr.b(this.f2468b[length], (List) aeo.f(t, (long) (1048575 & d2)), afc, a(length));
                        break;
                    case 50:
                        a(afc, i2, aeo.f(t, (long) (1048575 & d2)), length);
                        break;
                    case 51:
                        if (!a(t, i2, length)) {
                            break;
                        } else {
                            afc.a(i2, b(t, (long) (1048575 & d2)));
                            break;
                        }
                    case 52:
                        if (!a(t, i2, length)) {
                            break;
                        } else {
                            afc.a(i2, c(t, (long) (1048575 & d2)));
                            break;
                        }
                    case 53:
                        if (!a(t, i2, length)) {
                            break;
                        } else {
                            afc.a(i2, e(t, (long) (1048575 & d2)));
                            break;
                        }
                    case 54:
                        if (!a(t, i2, length)) {
                            break;
                        } else {
                            afc.c(i2, e(t, (long) (1048575 & d2)));
                            break;
                        }
                    case 55:
                        if (!a(t, i2, length)) {
                            break;
                        } else {
                            afc.c(i2, d(t, (long) (1048575 & d2)));
                            break;
                        }
                    case 56:
                        if (!a(t, i2, length)) {
                            break;
                        } else {
                            afc.d(i2, e(t, (long) (1048575 & d2)));
                            break;
                        }
                    case 57:
                        if (!a(t, i2, length)) {
                            break;
                        } else {
                            afc.d(i2, d(t, (long) (1048575 & d2)));
                            break;
                        }
                    case 58:
                        if (!a(t, i2, length)) {
                            break;
                        } else {
                            afc.a(i2, f(t, (long) (1048575 & d2)));
                            break;
                        }
                    case 59:
                        if (!a(t, i2, length)) {
                            break;
                        } else {
                            a(i2, aeo.f(t, (long) (1048575 & d2)), afc);
                            break;
                        }
                    case 60:
                        if (!a(t, i2, length)) {
                            break;
                        } else {
                            afc.a(i2, aeo.f(t, (long) (1048575 & d2)), a(length));
                            break;
                        }
                    case 61:
                        if (!a(t, i2, length)) {
                            break;
                        } else {
                            afc.a(i2, (aah) aeo.f(t, (long) (1048575 & d2)));
                            break;
                        }
                    case 62:
                        if (!a(t, i2, length)) {
                            break;
                        } else {
                            afc.e(i2, d(t, (long) (1048575 & d2)));
                            break;
                        }
                    case 63:
                        if (!a(t, i2, length)) {
                            break;
                        } else {
                            afc.b(i2, d(t, (long) (1048575 & d2)));
                            break;
                        }
                    case 64:
                        if (!a(t, i2, length)) {
                            break;
                        } else {
                            afc.a(i2, d(t, (long) (1048575 & d2)));
                            break;
                        }
                    case 65:
                        if (!a(t, i2, length)) {
                            break;
                        } else {
                            afc.b(i2, e(t, (long) (1048575 & d2)));
                            break;
                        }
                    case 66:
                        if (!a(t, i2, length)) {
                            break;
                        } else {
                            afc.f(i2, d(t, (long) (1048575 & d2)));
                            break;
                        }
                    case 67:
                        if (!a(t, i2, length)) {
                            break;
                        } else {
                            afc.e(i2, e(t, (long) (1048575 & d2)));
                            break;
                        }
                    case 68:
                        if (!a(t, i2, length)) {
                            break;
                        } else {
                            afc.b(i2, aeo.f(t, (long) (1048575 & d2)), a(length));
                            break;
                        }
                }
                length -= 4;
                entry = entry2;
            }
            while (entry != null) {
                this.r.a(afc, entry);
                entry = it.hasNext() ? (Entry) it.next() : null;
            }
        } else if (this.j) {
            Iterator it2 = null;
            Entry entry3 = null;
            if (this.h) {
                abh a3 = this.r.a((Object) t);
                if (!a3.b()) {
                    it2 = a3.e();
                    entry3 = (Entry) it2.next();
                }
            }
            int length2 = this.f2468b.length;
            int i3 = 0;
            while (i3 < length2) {
                int d3 = d(i3);
                int i4 = this.f2468b[i3];
                Entry entry4 = entry3;
                while (entry4 != null && this.r.a(entry4) <= i4) {
                    this.r.a(afc, entry4);
                    entry4 = it2.hasNext() ? (Entry) it2.next() : null;
                }
                switch ((267386880 & d3) >>> 20) {
                    case 0:
                        if (a(t, i3)) {
                            afc.a(i4, aeo.e(t, (long) (1048575 & d3)));
                            break;
                        }
                        break;
                    case 1:
                        if (a(t, i3)) {
                            afc.a(i4, aeo.d(t, (long) (1048575 & d3)));
                            break;
                        }
                        break;
                    case 2:
                        if (a(t, i3)) {
                            afc.a(i4, aeo.b(t, (long) (1048575 & d3)));
                            break;
                        }
                        break;
                    case 3:
                        if (a(t, i3)) {
                            afc.c(i4, aeo.b(t, (long) (1048575 & d3)));
                            break;
                        }
                        break;
                    case 4:
                        if (a(t, i3)) {
                            afc.c(i4, aeo.a((Object) t, (long) (1048575 & d3)));
                            break;
                        }
                        break;
                    case 5:
                        if (a(t, i3)) {
                            afc.d(i4, aeo.b(t, (long) (1048575 & d3)));
                            break;
                        }
                        break;
                    case 6:
                        if (a(t, i3)) {
                            afc.d(i4, aeo.a((Object) t, (long) (1048575 & d3)));
                            break;
                        }
                        break;
                    case 7:
                        if (a(t, i3)) {
                            afc.a(i4, aeo.c(t, (long) (1048575 & d3)));
                            break;
                        }
                        break;
                    case 8:
                        if (a(t, i3)) {
                            a(i4, aeo.f(t, (long) (1048575 & d3)), afc);
                            break;
                        }
                        break;
                    case 9:
                        if (a(t, i3)) {
                            afc.a(i4, aeo.f(t, (long) (1048575 & d3)), a(i3));
                            break;
                        }
                        break;
                    case 10:
                        if (a(t, i3)) {
                            afc.a(i4, (aah) aeo.f(t, (long) (1048575 & d3)));
                            break;
                        }
                        break;
                    case 11:
                        if (a(t, i3)) {
                            afc.e(i4, aeo.a((Object) t, (long) (1048575 & d3)));
                            break;
                        }
                        break;
                    case 12:
                        if (a(t, i3)) {
                            afc.b(i4, aeo.a((Object) t, (long) (1048575 & d3)));
                            break;
                        }
                        break;
                    case 13:
                        if (a(t, i3)) {
                            afc.a(i4, aeo.a((Object) t, (long) (1048575 & d3)));
                            break;
                        }
                        break;
                    case 14:
                        if (a(t, i3)) {
                            afc.b(i4, aeo.b(t, (long) (1048575 & d3)));
                            break;
                        }
                        break;
                    case 15:
                        if (a(t, i3)) {
                            afc.f(i4, aeo.a((Object) t, (long) (1048575 & d3)));
                            break;
                        }
                        break;
                    case 16:
                        if (a(t, i3)) {
                            afc.e(i4, aeo.b(t, (long) (1048575 & d3)));
                            break;
                        }
                        break;
                    case 17:
                        if (a(t, i3)) {
                            afc.b(i4, aeo.f(t, (long) (1048575 & d3)), a(i3));
                            break;
                        }
                        break;
                    case 18:
                        adr.a(this.f2468b[i3], (List) aeo.f(t, (long) (1048575 & d3)), afc, false);
                        break;
                    case 19:
                        adr.b(this.f2468b[i3], (List) aeo.f(t, (long) (1048575 & d3)), afc, false);
                        break;
                    case 20:
                        adr.c(this.f2468b[i3], (List) aeo.f(t, (long) (1048575 & d3)), afc, false);
                        break;
                    case 21:
                        adr.d(this.f2468b[i3], (List) aeo.f(t, (long) (1048575 & d3)), afc, false);
                        break;
                    case 22:
                        adr.h(this.f2468b[i3], (List) aeo.f(t, (long) (1048575 & d3)), afc, false);
                        break;
                    case 23:
                        adr.f(this.f2468b[i3], (List) aeo.f(t, (long) (1048575 & d3)), afc, false);
                        break;
                    case 24:
                        adr.k(this.f2468b[i3], (List) aeo.f(t, (long) (1048575 & d3)), afc, false);
                        break;
                    case 25:
                        adr.n(this.f2468b[i3], (List) aeo.f(t, (long) (1048575 & d3)), afc, false);
                        break;
                    case 26:
                        adr.a(this.f2468b[i3], (List) aeo.f(t, (long) (1048575 & d3)), afc);
                        break;
                    case 27:
                        adr.a(this.f2468b[i3], (List) aeo.f(t, (long) (1048575 & d3)), afc, a(i3));
                        break;
                    case 28:
                        adr.b(this.f2468b[i3], (List) aeo.f(t, (long) (1048575 & d3)), afc);
                        break;
                    case 29:
                        adr.i(this.f2468b[i3], (List) aeo.f(t, (long) (1048575 & d3)), afc, false);
                        break;
                    case 30:
                        adr.m(this.f2468b[i3], (List) aeo.f(t, (long) (1048575 & d3)), afc, false);
                        break;
                    case 31:
                        adr.l(this.f2468b[i3], (List) aeo.f(t, (long) (1048575 & d3)), afc, false);
                        break;
                    case 32:
                        adr.g(this.f2468b[i3], (List) aeo.f(t, (long) (1048575 & d3)), afc, false);
                        break;
                    case 33:
                        adr.j(this.f2468b[i3], (List) aeo.f(t, (long) (1048575 & d3)), afc, false);
                        break;
                    case 34:
                        adr.e(this.f2468b[i3], (List) aeo.f(t, (long) (1048575 & d3)), afc, false);
                        break;
                    case 35:
                        adr.a(this.f2468b[i3], (List) aeo.f(t, (long) (1048575 & d3)), afc, true);
                        break;
                    case 36:
                        adr.b(this.f2468b[i3], (List) aeo.f(t, (long) (1048575 & d3)), afc, true);
                        break;
                    case 37:
                        adr.c(this.f2468b[i3], (List) aeo.f(t, (long) (1048575 & d3)), afc, true);
                        break;
                    case 38:
                        adr.d(this.f2468b[i3], (List) aeo.f(t, (long) (1048575 & d3)), afc, true);
                        break;
                    case 39:
                        adr.h(this.f2468b[i3], (List) aeo.f(t, (long) (1048575 & d3)), afc, true);
                        break;
                    case 40:
                        adr.f(this.f2468b[i3], (List) aeo.f(t, (long) (1048575 & d3)), afc, true);
                        break;
                    case 41:
                        adr.k(this.f2468b[i3], (List) aeo.f(t, (long) (1048575 & d3)), afc, true);
                        break;
                    case 42:
                        adr.n(this.f2468b[i3], (List) aeo.f(t, (long) (1048575 & d3)), afc, true);
                        break;
                    case 43:
                        adr.i(this.f2468b[i3], (List) aeo.f(t, (long) (1048575 & d3)), afc, true);
                        break;
                    case 44:
                        adr.m(this.f2468b[i3], (List) aeo.f(t, (long) (1048575 & d3)), afc, true);
                        break;
                    case 45:
                        adr.l(this.f2468b[i3], (List) aeo.f(t, (long) (1048575 & d3)), afc, true);
                        break;
                    case 46:
                        adr.g(this.f2468b[i3], (List) aeo.f(t, (long) (1048575 & d3)), afc, true);
                        break;
                    case 47:
                        adr.j(this.f2468b[i3], (List) aeo.f(t, (long) (1048575 & d3)), afc, true);
                        break;
                    case 48:
                        adr.e(this.f2468b[i3], (List) aeo.f(t, (long) (1048575 & d3)), afc, true);
                        break;
                    case 49:
                        adr.b(this.f2468b[i3], (List) aeo.f(t, (long) (1048575 & d3)), afc, a(i3));
                        break;
                    case 50:
                        a(afc, i4, aeo.f(t, (long) (1048575 & d3)), i3);
                        break;
                    case 51:
                        if (a(t, i4, i3)) {
                            afc.a(i4, b(t, (long) (1048575 & d3)));
                            break;
                        }
                        break;
                    case 52:
                        if (a(t, i4, i3)) {
                            afc.a(i4, c(t, (long) (1048575 & d3)));
                            break;
                        }
                        break;
                    case 53:
                        if (a(t, i4, i3)) {
                            afc.a(i4, e(t, (long) (1048575 & d3)));
                            break;
                        }
                        break;
                    case 54:
                        if (a(t, i4, i3)) {
                            afc.c(i4, e(t, (long) (1048575 & d3)));
                            break;
                        }
                        break;
                    case 55:
                        if (a(t, i4, i3)) {
                            afc.c(i4, d(t, (long) (1048575 & d3)));
                            break;
                        }
                        break;
                    case 56:
                        if (a(t, i4, i3)) {
                            afc.d(i4, e(t, (long) (1048575 & d3)));
                            break;
                        }
                        break;
                    case 57:
                        if (a(t, i4, i3)) {
                            afc.d(i4, d(t, (long) (1048575 & d3)));
                            break;
                        }
                        break;
                    case 58:
                        if (a(t, i4, i3)) {
                            afc.a(i4, f(t, (long) (1048575 & d3)));
                            break;
                        }
                        break;
                    case 59:
                        if (a(t, i4, i3)) {
                            a(i4, aeo.f(t, (long) (1048575 & d3)), afc);
                            break;
                        }
                        break;
                    case 60:
                        if (a(t, i4, i3)) {
                            afc.a(i4, aeo.f(t, (long) (1048575 & d3)), a(i3));
                            break;
                        }
                        break;
                    case 61:
                        if (a(t, i4, i3)) {
                            afc.a(i4, (aah) aeo.f(t, (long) (1048575 & d3)));
                            break;
                        }
                        break;
                    case 62:
                        if (a(t, i4, i3)) {
                            afc.e(i4, d(t, (long) (1048575 & d3)));
                            break;
                        }
                        break;
                    case 63:
                        if (a(t, i4, i3)) {
                            afc.b(i4, d(t, (long) (1048575 & d3)));
                            break;
                        }
                        break;
                    case 64:
                        if (a(t, i4, i3)) {
                            afc.a(i4, d(t, (long) (1048575 & d3)));
                            break;
                        }
                        break;
                    case 65:
                        if (a(t, i4, i3)) {
                            afc.b(i4, e(t, (long) (1048575 & d3)));
                            break;
                        }
                        break;
                    case 66:
                        if (a(t, i4, i3)) {
                            afc.f(i4, d(t, (long) (1048575 & d3)));
                            break;
                        }
                        break;
                    case 67:
                        if (a(t, i4, i3)) {
                            afc.e(i4, e(t, (long) (1048575 & d3)));
                            break;
                        }
                        break;
                    case 68:
                        if (a(t, i4, i3)) {
                            afc.b(i4, aeo.f(t, (long) (1048575 & d3)), a(i3));
                            break;
                        }
                        break;
                }
                i3 += 4;
                entry3 = entry4;
            }
            while (entry3 != null) {
                this.r.a(afc, entry3);
                entry3 = it2.hasNext() ? (Entry) it2.next() : null;
            }
            a(this.q, t, afc);
        } else {
            b(t, afc);
        }
    }

    /* JADX WARNING: type inference failed for: r13v1, types: [int] */
    /* JADX WARNING: type inference failed for: r25v0, types: [int] */
    /* JADX WARNING: type inference failed for: r6v3, types: [int] */
    /* JADX WARNING: type inference failed for: r13v3 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 2 */
    public final void a(T t, byte[] bArr, int i2, int i3, aae aae) throws IOException {
        abu abu;
        if (this.j) {
            Unsafe unsafe = f2467a;
            int i4 = i2;
            while (i4 < i3) {
                int i5 = i4 + 1;
                byte b2 = bArr[i4];
                if (b2 < 0) {
                    i5 = aad.a((int) b2, bArr, i5, aae);
                    b2 = aae.f2389a;
                }
                int i6 = b2 >>> 3;
                int i7 = b2 & 7;
                int g2 = g(i6);
                if (g2 >= 0) {
                    int i8 = this.f2468b[g2 + 1];
                    int i9 = (267386880 & i8) >>> 20;
                    long j2 = (long) (1048575 & i8);
                    if (i9 <= 17) {
                        switch (i9) {
                            case 0:
                                if (i7 == 1) {
                                    aeo.a((Object) t, j2, aad.c(bArr, i5));
                                    i4 = i5 + 8;
                                    continue;
                                }
                                break;
                            case 1:
                                if (i7 == 5) {
                                    aeo.a((Object) t, j2, aad.d(bArr, i5));
                                    i4 = i5 + 4;
                                    continue;
                                }
                                break;
                            case 2:
                            case 3:
                                if (i7 == 0) {
                                    i4 = aad.b(bArr, i5, aae);
                                    unsafe.putLong(t, j2, aae.f2390b);
                                    continue;
                                }
                                break;
                            case 4:
                            case 11:
                                if (i7 == 0) {
                                    i4 = aad.a(bArr, i5, aae);
                                    unsafe.putInt(t, j2, aae.f2389a);
                                    continue;
                                }
                                break;
                            case 5:
                            case 14:
                                if (i7 == 1) {
                                    unsafe.putLong(t, j2, aad.b(bArr, i5));
                                    i4 = i5 + 8;
                                    continue;
                                }
                                break;
                            case 6:
                            case 13:
                                if (i7 == 5) {
                                    unsafe.putInt(t, j2, aad.a(bArr, i5));
                                    i4 = i5 + 4;
                                    continue;
                                }
                                break;
                            case 7:
                                if (i7 == 0) {
                                    i4 = aad.b(bArr, i5, aae);
                                    aeo.a((Object) t, j2, aae.f2390b != 0);
                                    continue;
                                }
                                break;
                            case 8:
                                if (i7 == 2) {
                                    int d2 = (536870912 & i8) == 0 ? aad.c(bArr, i5, aae) : aad.d(bArr, i5, aae);
                                    unsafe.putObject(t, j2, aae.c);
                                    i4 = d2;
                                    continue;
                                }
                                break;
                            case 9:
                                if (i7 == 2) {
                                    i4 = a(a(g2), bArr, i5, i3, aae);
                                    Object object = unsafe.getObject(t, j2);
                                    if (object != null) {
                                        unsafe.putObject(t, j2, abr.a(object, aae.c));
                                        break;
                                    } else {
                                        unsafe.putObject(t, j2, aae.c);
                                        continue;
                                    }
                                }
                                break;
                            case 10:
                                if (i7 == 2) {
                                    i4 = aad.e(bArr, i5, aae);
                                    unsafe.putObject(t, j2, aae.c);
                                    continue;
                                }
                                break;
                            case 12:
                                if (i7 == 0) {
                                    i4 = aad.a(bArr, i5, aae);
                                    unsafe.putInt(t, j2, aae.f2389a);
                                    continue;
                                }
                                break;
                            case 15:
                                if (i7 == 0) {
                                    i4 = aad.a(bArr, i5, aae);
                                    unsafe.putInt(t, j2, aaq.f(aae.f2389a));
                                    continue;
                                }
                                break;
                            case 16:
                                if (i7 == 0) {
                                    i4 = aad.b(bArr, i5, aae);
                                    unsafe.putLong(t, j2, aaq.a(aae.f2390b));
                                    continue;
                                }
                                break;
                            default:
                                i4 = i5;
                                break;
                        }
                    } else if (i9 == 27) {
                        if (i7 == 2) {
                            abu abu2 = (abu) unsafe.getObject(t, j2);
                            if (!abu2.a()) {
                                int size = abu2.size();
                                abu = abu2.a(size == 0 ? 10 : size << 1);
                                unsafe.putObject(t, j2, abu);
                            } else {
                                abu = abu2;
                            }
                            i4 = a(a(g2), (int) b2, bArr, i5, i3, abu, aae);
                        }
                    } else if (i9 <= 49) {
                        i4 = a(t, bArr, i5, i3, (int) b2, i6, i7, g2, (long) i8, i9, j2, aae);
                        if (i4 != i5) {
                        }
                        i4 = a((int) b2, bArr, i4, i3, (Object) t, aae);
                    } else if (i9 != 50) {
                        i4 = a(t, bArr, i5, i3, (int) b2, i6, i7, i8, i9, j2, g2, aae);
                        if (i4 != i5) {
                        }
                        i4 = a((int) b2, bArr, i4, i3, (Object) t, aae);
                    } else if (i7 == 2) {
                        i4 = a(t, bArr, i5, i3, g2, i6, j2, aae);
                        if (i4 != i5) {
                        }
                        i4 = a((int) b2, bArr, i4, i3, (Object) t, aae);
                    }
                }
                i4 = i5;
                i4 = a((int) b2, bArr, i4, i3, (Object) t, aae);
            }
            if (i4 != i3) {
                throw abv.g();
            }
            return;
        }
        a(t, bArr, i2, i3, 0, aae);
    }

    public final boolean a(T t, T t2) {
        boolean z;
        int length = this.f2468b.length;
        for (int i2 = 0; i2 < length; i2 += 4) {
            int d2 = d(i2);
            long j2 = (long) (d2 & 1048575);
            switch ((d2 & 267386880) >>> 20) {
                case 0:
                    if (!c(t, t2, i2) || aeo.b(t, j2) != aeo.b(t2, j2)) {
                        z = false;
                        break;
                    }
                case 1:
                    if (!c(t, t2, i2) || aeo.a((Object) t, j2) != aeo.a((Object) t2, j2)) {
                        z = false;
                        break;
                    }
                case 2:
                    if (!c(t, t2, i2) || aeo.b(t, j2) != aeo.b(t2, j2)) {
                        z = false;
                        break;
                    }
                case 3:
                    if (!c(t, t2, i2) || aeo.b(t, j2) != aeo.b(t2, j2)) {
                        z = false;
                        break;
                    }
                case 4:
                    if (!c(t, t2, i2) || aeo.a((Object) t, j2) != aeo.a((Object) t2, j2)) {
                        z = false;
                        break;
                    }
                case 5:
                    if (!c(t, t2, i2) || aeo.b(t, j2) != aeo.b(t2, j2)) {
                        z = false;
                        break;
                    }
                case 6:
                    if (!c(t, t2, i2) || aeo.a((Object) t, j2) != aeo.a((Object) t2, j2)) {
                        z = false;
                        break;
                    }
                case 7:
                    if (!c(t, t2, i2) || aeo.c(t, j2) != aeo.c(t2, j2)) {
                        z = false;
                        break;
                    }
                case 8:
                    if (!c(t, t2, i2) || !adr.a(aeo.f(t, j2), aeo.f(t2, j2))) {
                        z = false;
                        break;
                    }
                case 9:
                    if (!c(t, t2, i2) || !adr.a(aeo.f(t, j2), aeo.f(t2, j2))) {
                        z = false;
                        break;
                    }
                case 10:
                    if (!c(t, t2, i2) || !adr.a(aeo.f(t, j2), aeo.f(t2, j2))) {
                        z = false;
                        break;
                    }
                case 11:
                    if (!c(t, t2, i2) || aeo.a((Object) t, j2) != aeo.a((Object) t2, j2)) {
                        z = false;
                        break;
                    }
                case 12:
                    if (!c(t, t2, i2) || aeo.a((Object) t, j2) != aeo.a((Object) t2, j2)) {
                        z = false;
                        break;
                    }
                case 13:
                    if (!c(t, t2, i2) || aeo.a((Object) t, j2) != aeo.a((Object) t2, j2)) {
                        z = false;
                        break;
                    }
                case 14:
                    if (!c(t, t2, i2) || aeo.b(t, j2) != aeo.b(t2, j2)) {
                        z = false;
                        break;
                    }
                case 15:
                    if (!c(t, t2, i2) || aeo.a((Object) t, j2) != aeo.a((Object) t2, j2)) {
                        z = false;
                        break;
                    }
                case 16:
                    if (!c(t, t2, i2) || aeo.b(t, j2) != aeo.b(t2, j2)) {
                        z = false;
                        break;
                    }
                case 17:
                    if (!c(t, t2, i2) || !adr.a(aeo.f(t, j2), aeo.f(t2, j2))) {
                        z = false;
                        break;
                    }
                case 18:
                case 19:
                case 20:
                case 21:
                case 22:
                case 23:
                case 24:
                case 25:
                case 26:
                case 27:
                case 28:
                case 29:
                case 30:
                case 31:
                case 32:
                case 33:
                case 34:
                case 35:
                case 36:
                case 37:
                case 38:
                case 39:
                case 40:
                case 41:
                case 42:
                case 43:
                case 44:
                case 45:
                case 46:
                case 47:
                case 48:
                case 49:
                    z = adr.a(aeo.f(t, j2), aeo.f(t2, j2));
                    break;
                case 50:
                    z = adr.a(aeo.f(t, j2), aeo.f(t2, j2));
                    break;
                case 51:
                case 52:
                case 53:
                case 54:
                case 55:
                case 56:
                case 57:
                case 58:
                case 59:
                case 60:
                case 61:
                case 62:
                case 63:
                case 64:
                case 65:
                case 66:
                case 67:
                case 68:
                    int e2 = e(i2);
                    if (aeo.a((Object) t, (long) (e2 & 1048575)) != aeo.a((Object) t2, (long) (e2 & 1048575)) || !adr.a(aeo.f(t, j2), aeo.f(t2, j2))) {
                        z = false;
                        break;
                    }
                default:
                    z = true;
                    break;
            }
            if (!z) {
                return false;
            }
        }
        if (!this.q.b(t).equals(this.q.b(t2))) {
            return false;
        }
        if (this.h) {
            return this.r.a((Object) t).equals(this.r.a((Object) t2));
        }
        return true;
    }

    public final int b(T t) {
        int i2;
        int i3;
        int i4;
        if (this.j) {
            Unsafe unsafe = f2467a;
            int i5 = 0;
            int i6 = 0;
            while (true) {
                int i7 = i5;
                if (i7 >= this.f2468b.length) {
                    return a(this.q, t) + i6;
                }
                int d2 = d(i7);
                int i8 = (267386880 & d2) >>> 20;
                int i9 = this.f2468b[i7];
                long j2 = (long) (d2 & 1048575);
                int i10 = (i8 < abk.DOUBLE_LIST_PACKED.a() || i8 > abk.SINT64_LIST_PACKED.a()) ? 0 : this.f2468b[i7 + 2] & 1048575;
                switch (i8) {
                    case 0:
                        if (!a(t, i7)) {
                            break;
                        } else {
                            i6 += aav.b(i9, 0.0d);
                            break;
                        }
                    case 1:
                        if (!a(t, i7)) {
                            break;
                        } else {
                            i6 += aav.b(i9, 0.0f);
                            break;
                        }
                    case 2:
                        if (!a(t, i7)) {
                            break;
                        } else {
                            i6 += aav.d(i9, aeo.b(t, j2));
                            break;
                        }
                    case 3:
                        if (!a(t, i7)) {
                            break;
                        } else {
                            i6 += aav.e(i9, aeo.b(t, j2));
                            break;
                        }
                    case 4:
                        if (!a(t, i7)) {
                            break;
                        } else {
                            i6 += aav.f(i9, aeo.a((Object) t, j2));
                            break;
                        }
                    case 5:
                        if (!a(t, i7)) {
                            break;
                        } else {
                            i6 += aav.g(i9, 0);
                            break;
                        }
                    case 6:
                        if (!a(t, i7)) {
                            break;
                        } else {
                            i6 += aav.i(i9, 0);
                            break;
                        }
                    case 7:
                        if (!a(t, i7)) {
                            break;
                        } else {
                            i6 += aav.b(i9, true);
                            break;
                        }
                    case 8:
                        if (!a(t, i7)) {
                            break;
                        } else {
                            Object f2 = aeo.f(t, j2);
                            if (!(f2 instanceof aah)) {
                                i6 += aav.b(i9, (String) f2);
                                break;
                            } else {
                                i6 += aav.c(i9, (aah) f2);
                                break;
                            }
                        }
                    case 9:
                        if (!a(t, i7)) {
                            break;
                        } else {
                            i6 += adr.a(i9, aeo.f(t, j2), a(i7));
                            break;
                        }
                    case 10:
                        if (!a(t, i7)) {
                            break;
                        } else {
                            i6 += aav.c(i9, (aah) aeo.f(t, j2));
                            break;
                        }
                    case 11:
                        if (!a(t, i7)) {
                            break;
                        } else {
                            i6 += aav.g(i9, aeo.a((Object) t, j2));
                            break;
                        }
                    case 12:
                        if (!a(t, i7)) {
                            break;
                        } else {
                            i6 += aav.k(i9, aeo.a((Object) t, j2));
                            break;
                        }
                    case 13:
                        if (!a(t, i7)) {
                            break;
                        } else {
                            i6 += aav.j(i9, 0);
                            break;
                        }
                    case 14:
                        if (!a(t, i7)) {
                            break;
                        } else {
                            i6 += aav.h(i9, 0);
                            break;
                        }
                    case 15:
                        if (!a(t, i7)) {
                            break;
                        } else {
                            i6 += aav.h(i9, aeo.a((Object) t, j2));
                            break;
                        }
                    case 16:
                        if (!a(t, i7)) {
                            break;
                        } else {
                            i6 += aav.f(i9, aeo.b(t, j2));
                            break;
                        }
                    case 17:
                        if (!a(t, i7)) {
                            break;
                        } else {
                            i6 += aav.c(i9, (acw) aeo.f(t, j2), a(i7));
                            break;
                        }
                    case 18:
                        i6 += adr.i(i9, a((Object) t, j2), false);
                        break;
                    case 19:
                        i6 += adr.h(i9, a((Object) t, j2), false);
                        break;
                    case 20:
                        i6 += adr.a(i9, a((Object) t, j2), false);
                        break;
                    case 21:
                        i6 += adr.b(i9, a((Object) t, j2), false);
                        break;
                    case 22:
                        i6 += adr.e(i9, a((Object) t, j2), false);
                        break;
                    case 23:
                        i6 += adr.i(i9, a((Object) t, j2), false);
                        break;
                    case 24:
                        i6 += adr.h(i9, a((Object) t, j2), false);
                        break;
                    case 25:
                        i6 += adr.j(i9, a((Object) t, j2), false);
                        break;
                    case 26:
                        i6 += adr.a(i9, a((Object) t, j2));
                        break;
                    case 27:
                        i6 += adr.a(i9, a((Object) t, j2), a(i7));
                        break;
                    case 28:
                        i6 += adr.b(i9, a((Object) t, j2));
                        break;
                    case 29:
                        i6 += adr.f(i9, a((Object) t, j2), false);
                        break;
                    case 30:
                        i6 += adr.d(i9, a((Object) t, j2), false);
                        break;
                    case 31:
                        i6 += adr.h(i9, a((Object) t, j2), false);
                        break;
                    case 32:
                        i6 += adr.i(i9, a((Object) t, j2), false);
                        break;
                    case 33:
                        i6 += adr.g(i9, a((Object) t, j2), false);
                        break;
                    case 34:
                        i6 += adr.c(i9, a((Object) t, j2), false);
                        break;
                    case 35:
                        int i11 = adr.i((List) unsafe.getObject(t, j2));
                        if (i11 > 0) {
                            if (this.k) {
                                unsafe.putInt(t, (long) i10, i11);
                            }
                            i6 += i11 + aav.e(i9) + aav.g(i11);
                            break;
                        } else {
                            break;
                        }
                    case 36:
                        int h2 = adr.h((List) unsafe.getObject(t, j2));
                        if (h2 > 0) {
                            if (this.k) {
                                unsafe.putInt(t, (long) i10, h2);
                            }
                            i6 += h2 + aav.e(i9) + aav.g(h2);
                            break;
                        } else {
                            break;
                        }
                    case 37:
                        int a2 = adr.a((List) unsafe.getObject(t, j2));
                        if (a2 > 0) {
                            if (this.k) {
                                unsafe.putInt(t, (long) i10, a2);
                            }
                            i6 += a2 + aav.e(i9) + aav.g(a2);
                            break;
                        } else {
                            break;
                        }
                    case 38:
                        int b2 = adr.b((List) unsafe.getObject(t, j2));
                        if (b2 > 0) {
                            if (this.k) {
                                unsafe.putInt(t, (long) i10, b2);
                            }
                            i6 += b2 + aav.e(i9) + aav.g(b2);
                            break;
                        } else {
                            break;
                        }
                    case 39:
                        int e2 = adr.e((List) unsafe.getObject(t, j2));
                        if (e2 > 0) {
                            if (this.k) {
                                unsafe.putInt(t, (long) i10, e2);
                            }
                            i6 += e2 + aav.e(i9) + aav.g(e2);
                            break;
                        } else {
                            break;
                        }
                    case 40:
                        int i12 = adr.i((List) unsafe.getObject(t, j2));
                        if (i12 > 0) {
                            if (this.k) {
                                unsafe.putInt(t, (long) i10, i12);
                            }
                            i6 += i12 + aav.e(i9) + aav.g(i12);
                            break;
                        } else {
                            break;
                        }
                    case 41:
                        int h3 = adr.h((List) unsafe.getObject(t, j2));
                        if (h3 > 0) {
                            if (this.k) {
                                unsafe.putInt(t, (long) i10, h3);
                            }
                            i6 += h3 + aav.e(i9) + aav.g(h3);
                            break;
                        } else {
                            break;
                        }
                    case 42:
                        int j3 = adr.j((List) unsafe.getObject(t, j2));
                        if (j3 > 0) {
                            if (this.k) {
                                unsafe.putInt(t, (long) i10, j3);
                            }
                            i6 += j3 + aav.e(i9) + aav.g(j3);
                            break;
                        } else {
                            break;
                        }
                    case 43:
                        int f3 = adr.f((List) unsafe.getObject(t, j2));
                        if (f3 > 0) {
                            if (this.k) {
                                unsafe.putInt(t, (long) i10, f3);
                            }
                            i6 += f3 + aav.e(i9) + aav.g(f3);
                            break;
                        } else {
                            break;
                        }
                    case 44:
                        int d3 = adr.d((List) unsafe.getObject(t, j2));
                        if (d3 > 0) {
                            if (this.k) {
                                unsafe.putInt(t, (long) i10, d3);
                            }
                            i6 += d3 + aav.e(i9) + aav.g(d3);
                            break;
                        } else {
                            break;
                        }
                    case 45:
                        int h4 = adr.h((List) unsafe.getObject(t, j2));
                        if (h4 > 0) {
                            if (this.k) {
                                unsafe.putInt(t, (long) i10, h4);
                            }
                            i6 += h4 + aav.e(i9) + aav.g(h4);
                            break;
                        } else {
                            break;
                        }
                    case 46:
                        int i13 = adr.i((List) unsafe.getObject(t, j2));
                        if (i13 > 0) {
                            if (this.k) {
                                unsafe.putInt(t, (long) i10, i13);
                            }
                            i6 += i13 + aav.e(i9) + aav.g(i13);
                            break;
                        } else {
                            break;
                        }
                    case 47:
                        int g2 = adr.g((List) unsafe.getObject(t, j2));
                        if (g2 > 0) {
                            if (this.k) {
                                unsafe.putInt(t, (long) i10, g2);
                            }
                            i6 += g2 + aav.e(i9) + aav.g(g2);
                            break;
                        } else {
                            break;
                        }
                    case 48:
                        int c2 = adr.c((List) unsafe.getObject(t, j2));
                        if (c2 > 0) {
                            if (this.k) {
                                unsafe.putInt(t, (long) i10, c2);
                            }
                            i6 += c2 + aav.e(i9) + aav.g(c2);
                            break;
                        } else {
                            break;
                        }
                    case 49:
                        i6 += adr.b(i9, a((Object) t, j2), a(i7));
                        break;
                    case 50:
                        i6 += this.s.a(i9, aeo.f(t, j2), b(i7));
                        break;
                    case 51:
                        if (!a(t, i9, i7)) {
                            break;
                        } else {
                            i6 += aav.b(i9, 0.0d);
                            break;
                        }
                    case 52:
                        if (!a(t, i9, i7)) {
                            break;
                        } else {
                            i6 += aav.b(i9, 0.0f);
                            break;
                        }
                    case 53:
                        if (!a(t, i9, i7)) {
                            break;
                        } else {
                            i6 += aav.d(i9, e(t, j2));
                            break;
                        }
                    case 54:
                        if (!a(t, i9, i7)) {
                            break;
                        } else {
                            i6 += aav.e(i9, e(t, j2));
                            break;
                        }
                    case 55:
                        if (!a(t, i9, i7)) {
                            break;
                        } else {
                            i6 += aav.f(i9, d(t, j2));
                            break;
                        }
                    case 56:
                        if (!a(t, i9, i7)) {
                            break;
                        } else {
                            i6 += aav.g(i9, 0);
                            break;
                        }
                    case 57:
                        if (!a(t, i9, i7)) {
                            break;
                        } else {
                            i6 += aav.i(i9, 0);
                            break;
                        }
                    case 58:
                        if (!a(t, i9, i7)) {
                            break;
                        } else {
                            i6 += aav.b(i9, true);
                            break;
                        }
                    case 59:
                        if (!a(t, i9, i7)) {
                            break;
                        } else {
                            Object f4 = aeo.f(t, j2);
                            if (!(f4 instanceof aah)) {
                                i6 += aav.b(i9, (String) f4);
                                break;
                            } else {
                                i6 += aav.c(i9, (aah) f4);
                                break;
                            }
                        }
                    case 60:
                        if (!a(t, i9, i7)) {
                            break;
                        } else {
                            i6 += adr.a(i9, aeo.f(t, j2), a(i7));
                            break;
                        }
                    case 61:
                        if (!a(t, i9, i7)) {
                            break;
                        } else {
                            i6 += aav.c(i9, (aah) aeo.f(t, j2));
                            break;
                        }
                    case 62:
                        if (!a(t, i9, i7)) {
                            break;
                        } else {
                            i6 += aav.g(i9, d(t, j2));
                            break;
                        }
                    case 63:
                        if (!a(t, i9, i7)) {
                            break;
                        } else {
                            i6 += aav.k(i9, d(t, j2));
                            break;
                        }
                    case 64:
                        if (!a(t, i9, i7)) {
                            break;
                        } else {
                            i6 += aav.j(i9, 0);
                            break;
                        }
                    case 65:
                        if (!a(t, i9, i7)) {
                            break;
                        } else {
                            i6 += aav.h(i9, 0);
                            break;
                        }
                    case 66:
                        if (!a(t, i9, i7)) {
                            break;
                        } else {
                            i6 += aav.h(i9, d(t, j2));
                            break;
                        }
                    case 67:
                        if (!a(t, i9, i7)) {
                            break;
                        } else {
                            i6 += aav.f(i9, e(t, j2));
                            break;
                        }
                    case 68:
                        if (!a(t, i9, i7)) {
                            break;
                        } else {
                            i6 += aav.c(i9, (acw) aeo.f(t, j2), a(i7));
                            break;
                        }
                }
                i5 = i7 + 4;
            }
        } else {
            Unsafe unsafe2 = f2467a;
            int i14 = -1;
            int i15 = 0;
            int i16 = 0;
            int i17 = 0;
            while (i16 < this.f2468b.length) {
                int d4 = d(i16);
                int i18 = this.f2468b[i16];
                int i19 = (267386880 & d4) >>> 20;
                int i20 = 0;
                if (i19 <= 17) {
                    int i21 = this.f2468b[i16 + 2];
                    int i22 = 1048575 & i21;
                    int i23 = 1 << (i21 >>> 20);
                    if (i22 != i14) {
                        i17 = unsafe2.getInt(t, (long) i22);
                        i14 = i22;
                    }
                    i2 = i14;
                    i3 = i17;
                    i4 = i23;
                    i20 = i21;
                } else if (!this.k || i19 < abk.DOUBLE_LIST_PACKED.a() || i19 > abk.SINT64_LIST_PACKED.a()) {
                    i2 = i14;
                    i3 = i17;
                    i4 = 0;
                } else {
                    i20 = this.f2468b[i16 + 2] & 1048575;
                    i2 = i14;
                    i3 = i17;
                    i4 = 0;
                }
                long j4 = (long) (1048575 & d4);
                switch (i19) {
                    case 0:
                        if ((i4 & i3) == 0) {
                            break;
                        } else {
                            i15 += aav.b(i18, 0.0d);
                            break;
                        }
                    case 1:
                        if ((i4 & i3) == 0) {
                            break;
                        } else {
                            i15 += aav.b(i18, 0.0f);
                            break;
                        }
                    case 2:
                        if ((i4 & i3) == 0) {
                            break;
                        } else {
                            i15 += aav.d(i18, unsafe2.getLong(t, j4));
                            break;
                        }
                    case 3:
                        if ((i4 & i3) == 0) {
                            break;
                        } else {
                            i15 += aav.e(i18, unsafe2.getLong(t, j4));
                            break;
                        }
                    case 4:
                        if ((i4 & i3) == 0) {
                            break;
                        } else {
                            i15 += aav.f(i18, unsafe2.getInt(t, j4));
                            break;
                        }
                    case 5:
                        if ((i4 & i3) == 0) {
                            break;
                        } else {
                            i15 += aav.g(i18, 0);
                            break;
                        }
                    case 6:
                        if ((i4 & i3) == 0) {
                            break;
                        } else {
                            i15 += aav.i(i18, 0);
                            break;
                        }
                    case 7:
                        if ((i4 & i3) == 0) {
                            break;
                        } else {
                            i15 += aav.b(i18, true);
                            break;
                        }
                    case 8:
                        if ((i4 & i3) == 0) {
                            break;
                        } else {
                            Object object = unsafe2.getObject(t, j4);
                            if (!(object instanceof aah)) {
                                i15 += aav.b(i18, (String) object);
                                break;
                            } else {
                                i15 += aav.c(i18, (aah) object);
                                break;
                            }
                        }
                    case 9:
                        if ((i4 & i3) == 0) {
                            break;
                        } else {
                            i15 += adr.a(i18, unsafe2.getObject(t, j4), a(i16));
                            break;
                        }
                    case 10:
                        if ((i4 & i3) == 0) {
                            break;
                        } else {
                            i15 += aav.c(i18, (aah) unsafe2.getObject(t, j4));
                            break;
                        }
                    case 11:
                        if ((i4 & i3) == 0) {
                            break;
                        } else {
                            i15 += aav.g(i18, unsafe2.getInt(t, j4));
                            break;
                        }
                    case 12:
                        if ((i4 & i3) == 0) {
                            break;
                        } else {
                            i15 += aav.k(i18, unsafe2.getInt(t, j4));
                            break;
                        }
                    case 13:
                        if ((i4 & i3) == 0) {
                            break;
                        } else {
                            i15 += aav.j(i18, 0);
                            break;
                        }
                    case 14:
                        if ((i4 & i3) == 0) {
                            break;
                        } else {
                            i15 += aav.h(i18, 0);
                            break;
                        }
                    case 15:
                        if ((i4 & i3) == 0) {
                            break;
                        } else {
                            i15 += aav.h(i18, unsafe2.getInt(t, j4));
                            break;
                        }
                    case 16:
                        if ((i4 & i3) == 0) {
                            break;
                        } else {
                            i15 += aav.f(i18, unsafe2.getLong(t, j4));
                            break;
                        }
                    case 17:
                        if ((i4 & i3) == 0) {
                            break;
                        } else {
                            i15 += aav.c(i18, (acw) unsafe2.getObject(t, j4), a(i16));
                            break;
                        }
                    case 18:
                        i15 += adr.i(i18, (List) unsafe2.getObject(t, j4), false);
                        break;
                    case 19:
                        i15 += adr.h(i18, (List) unsafe2.getObject(t, j4), false);
                        break;
                    case 20:
                        i15 += adr.a(i18, (List) unsafe2.getObject(t, j4), false);
                        break;
                    case 21:
                        i15 += adr.b(i18, (List) unsafe2.getObject(t, j4), false);
                        break;
                    case 22:
                        i15 += adr.e(i18, (List) unsafe2.getObject(t, j4), false);
                        break;
                    case 23:
                        i15 += adr.i(i18, (List) unsafe2.getObject(t, j4), false);
                        break;
                    case 24:
                        i15 += adr.h(i18, (List) unsafe2.getObject(t, j4), false);
                        break;
                    case 25:
                        i15 += adr.j(i18, (List) unsafe2.getObject(t, j4), false);
                        break;
                    case 26:
                        i15 += adr.a(i18, (List) unsafe2.getObject(t, j4));
                        break;
                    case 27:
                        i15 += adr.a(i18, (List) unsafe2.getObject(t, j4), a(i16));
                        break;
                    case 28:
                        i15 += adr.b(i18, (List) unsafe2.getObject(t, j4));
                        break;
                    case 29:
                        i15 += adr.f(i18, (List) unsafe2.getObject(t, j4), false);
                        break;
                    case 30:
                        i15 += adr.d(i18, (List) unsafe2.getObject(t, j4), false);
                        break;
                    case 31:
                        i15 += adr.h(i18, (List) unsafe2.getObject(t, j4), false);
                        break;
                    case 32:
                        i15 += adr.i(i18, (List) unsafe2.getObject(t, j4), false);
                        break;
                    case 33:
                        i15 += adr.g(i18, (List) unsafe2.getObject(t, j4), false);
                        break;
                    case 34:
                        i15 += adr.c(i18, (List) unsafe2.getObject(t, j4), false);
                        break;
                    case 35:
                        int i24 = adr.i((List) unsafe2.getObject(t, j4));
                        if (i24 > 0) {
                            if (this.k) {
                                unsafe2.putInt(t, (long) i20, i24);
                            }
                            i15 += i24 + aav.e(i18) + aav.g(i24);
                            break;
                        } else {
                            break;
                        }
                    case 36:
                        int h5 = adr.h((List) unsafe2.getObject(t, j4));
                        if (h5 > 0) {
                            if (this.k) {
                                unsafe2.putInt(t, (long) i20, h5);
                            }
                            i15 += h5 + aav.e(i18) + aav.g(h5);
                            break;
                        } else {
                            break;
                        }
                    case 37:
                        int a3 = adr.a((List) unsafe2.getObject(t, j4));
                        if (a3 > 0) {
                            if (this.k) {
                                unsafe2.putInt(t, (long) i20, a3);
                            }
                            i15 += a3 + aav.e(i18) + aav.g(a3);
                            break;
                        } else {
                            break;
                        }
                    case 38:
                        int b3 = adr.b((List) unsafe2.getObject(t, j4));
                        if (b3 > 0) {
                            if (this.k) {
                                unsafe2.putInt(t, (long) i20, b3);
                            }
                            i15 += b3 + aav.e(i18) + aav.g(b3);
                            break;
                        } else {
                            break;
                        }
                    case 39:
                        int e3 = adr.e((List) unsafe2.getObject(t, j4));
                        if (e3 > 0) {
                            if (this.k) {
                                unsafe2.putInt(t, (long) i20, e3);
                            }
                            i15 += e3 + aav.e(i18) + aav.g(e3);
                            break;
                        } else {
                            break;
                        }
                    case 40:
                        int i25 = adr.i((List) unsafe2.getObject(t, j4));
                        if (i25 > 0) {
                            if (this.k) {
                                unsafe2.putInt(t, (long) i20, i25);
                            }
                            i15 += i25 + aav.e(i18) + aav.g(i25);
                            break;
                        } else {
                            break;
                        }
                    case 41:
                        int h6 = adr.h((List) unsafe2.getObject(t, j4));
                        if (h6 > 0) {
                            if (this.k) {
                                unsafe2.putInt(t, (long) i20, h6);
                            }
                            i15 += h6 + aav.e(i18) + aav.g(h6);
                            break;
                        } else {
                            break;
                        }
                    case 42:
                        int j5 = adr.j((List) unsafe2.getObject(t, j4));
                        if (j5 > 0) {
                            if (this.k) {
                                unsafe2.putInt(t, (long) i20, j5);
                            }
                            i15 += j5 + aav.e(i18) + aav.g(j5);
                            break;
                        } else {
                            break;
                        }
                    case 43:
                        int f5 = adr.f((List) unsafe2.getObject(t, j4));
                        if (f5 > 0) {
                            if (this.k) {
                                unsafe2.putInt(t, (long) i20, f5);
                            }
                            i15 += f5 + aav.e(i18) + aav.g(f5);
                            break;
                        } else {
                            break;
                        }
                    case 44:
                        int d5 = adr.d((List) unsafe2.getObject(t, j4));
                        if (d5 > 0) {
                            if (this.k) {
                                unsafe2.putInt(t, (long) i20, d5);
                            }
                            i15 += d5 + aav.e(i18) + aav.g(d5);
                            break;
                        } else {
                            break;
                        }
                    case 45:
                        int h7 = adr.h((List) unsafe2.getObject(t, j4));
                        if (h7 > 0) {
                            if (this.k) {
                                unsafe2.putInt(t, (long) i20, h7);
                            }
                            i15 += h7 + aav.e(i18) + aav.g(h7);
                            break;
                        } else {
                            break;
                        }
                    case 46:
                        int i26 = adr.i((List) unsafe2.getObject(t, j4));
                        if (i26 > 0) {
                            if (this.k) {
                                unsafe2.putInt(t, (long) i20, i26);
                            }
                            i15 += i26 + aav.e(i18) + aav.g(i26);
                            break;
                        } else {
                            break;
                        }
                    case 47:
                        int g3 = adr.g((List) unsafe2.getObject(t, j4));
                        if (g3 > 0) {
                            if (this.k) {
                                unsafe2.putInt(t, (long) i20, g3);
                            }
                            i15 += g3 + aav.e(i18) + aav.g(g3);
                            break;
                        } else {
                            break;
                        }
                    case 48:
                        int c3 = adr.c((List) unsafe2.getObject(t, j4));
                        if (c3 > 0) {
                            if (this.k) {
                                unsafe2.putInt(t, (long) i20, c3);
                            }
                            i15 += c3 + aav.e(i18) + aav.g(c3);
                            break;
                        } else {
                            break;
                        }
                    case 49:
                        i15 += adr.b(i18, (List) unsafe2.getObject(t, j4), a(i16));
                        break;
                    case 50:
                        i15 += this.s.a(i18, unsafe2.getObject(t, j4), b(i16));
                        break;
                    case 51:
                        if (!a(t, i18, i16)) {
                            break;
                        } else {
                            i15 += aav.b(i18, 0.0d);
                            break;
                        }
                    case 52:
                        if (!a(t, i18, i16)) {
                            break;
                        } else {
                            i15 += aav.b(i18, 0.0f);
                            break;
                        }
                    case 53:
                        if (!a(t, i18, i16)) {
                            break;
                        } else {
                            i15 += aav.d(i18, e(t, j4));
                            break;
                        }
                    case 54:
                        if (!a(t, i18, i16)) {
                            break;
                        } else {
                            i15 += aav.e(i18, e(t, j4));
                            break;
                        }
                    case 55:
                        if (!a(t, i18, i16)) {
                            break;
                        } else {
                            i15 += aav.f(i18, d(t, j4));
                            break;
                        }
                    case 56:
                        if (!a(t, i18, i16)) {
                            break;
                        } else {
                            i15 += aav.g(i18, 0);
                            break;
                        }
                    case 57:
                        if (!a(t, i18, i16)) {
                            break;
                        } else {
                            i15 += aav.i(i18, 0);
                            break;
                        }
                    case 58:
                        if (!a(t, i18, i16)) {
                            break;
                        } else {
                            i15 += aav.b(i18, true);
                            break;
                        }
                    case 59:
                        if (!a(t, i18, i16)) {
                            break;
                        } else {
                            Object object2 = unsafe2.getObject(t, j4);
                            if (!(object2 instanceof aah)) {
                                i15 += aav.b(i18, (String) object2);
                                break;
                            } else {
                                i15 += aav.c(i18, (aah) object2);
                                break;
                            }
                        }
                    case 60:
                        if (!a(t, i18, i16)) {
                            break;
                        } else {
                            i15 += adr.a(i18, unsafe2.getObject(t, j4), a(i16));
                            break;
                        }
                    case 61:
                        if (!a(t, i18, i16)) {
                            break;
                        } else {
                            i15 += aav.c(i18, (aah) unsafe2.getObject(t, j4));
                            break;
                        }
                    case 62:
                        if (!a(t, i18, i16)) {
                            break;
                        } else {
                            i15 += aav.g(i18, d(t, j4));
                            break;
                        }
                    case 63:
                        if (!a(t, i18, i16)) {
                            break;
                        } else {
                            i15 += aav.k(i18, d(t, j4));
                            break;
                        }
                    case 64:
                        if (!a(t, i18, i16)) {
                            break;
                        } else {
                            i15 += aav.j(i18, 0);
                            break;
                        }
                    case 65:
                        if (!a(t, i18, i16)) {
                            break;
                        } else {
                            i15 += aav.h(i18, 0);
                            break;
                        }
                    case 66:
                        if (!a(t, i18, i16)) {
                            break;
                        } else {
                            i15 += aav.h(i18, d(t, j4));
                            break;
                        }
                    case 67:
                        if (!a(t, i18, i16)) {
                            break;
                        } else {
                            i15 += aav.f(i18, e(t, j4));
                            break;
                        }
                    case 68:
                        if (!a(t, i18, i16)) {
                            break;
                        } else {
                            i15 += aav.c(i18, (acw) unsafe2.getObject(t, j4), a(i16));
                            break;
                        }
                }
                i16 += 4;
                i17 = i3;
                i14 = i2;
            }
            int a4 = a(this.q, t) + i15;
            return this.h ? a4 + this.r.a((Object) t).h() : a4;
        }
    }

    public final void b(T t, T t2) {
        if (t2 == null) {
            throw new NullPointerException();
        }
        for (int i2 = 0; i2 < this.f2468b.length; i2 += 4) {
            int d2 = d(i2);
            long j2 = (long) (1048575 & d2);
            int i3 = this.f2468b[i2];
            switch ((d2 & 267386880) >>> 20) {
                case 0:
                    if (!a(t2, i2)) {
                        break;
                    } else {
                        aeo.a((Object) t, j2, aeo.e(t2, j2));
                        b(t, i2);
                        break;
                    }
                case 1:
                    if (!a(t2, i2)) {
                        break;
                    } else {
                        aeo.a((Object) t, j2, aeo.d(t2, j2));
                        b(t, i2);
                        break;
                    }
                case 2:
                    if (!a(t2, i2)) {
                        break;
                    } else {
                        aeo.a((Object) t, j2, aeo.b(t2, j2));
                        b(t, i2);
                        break;
                    }
                case 3:
                    if (!a(t2, i2)) {
                        break;
                    } else {
                        aeo.a((Object) t, j2, aeo.b(t2, j2));
                        b(t, i2);
                        break;
                    }
                case 4:
                    if (!a(t2, i2)) {
                        break;
                    } else {
                        aeo.a((Object) t, j2, aeo.a((Object) t2, j2));
                        b(t, i2);
                        break;
                    }
                case 5:
                    if (!a(t2, i2)) {
                        break;
                    } else {
                        aeo.a((Object) t, j2, aeo.b(t2, j2));
                        b(t, i2);
                        break;
                    }
                case 6:
                    if (!a(t2, i2)) {
                        break;
                    } else {
                        aeo.a((Object) t, j2, aeo.a((Object) t2, j2));
                        b(t, i2);
                        break;
                    }
                case 7:
                    if (!a(t2, i2)) {
                        break;
                    } else {
                        aeo.a((Object) t, j2, aeo.c(t2, j2));
                        b(t, i2);
                        break;
                    }
                case 8:
                    if (!a(t2, i2)) {
                        break;
                    } else {
                        aeo.a((Object) t, j2, aeo.f(t2, j2));
                        b(t, i2);
                        break;
                    }
                case 9:
                    a(t, t2, i2);
                    break;
                case 10:
                    if (!a(t2, i2)) {
                        break;
                    } else {
                        aeo.a((Object) t, j2, aeo.f(t2, j2));
                        b(t, i2);
                        break;
                    }
                case 11:
                    if (!a(t2, i2)) {
                        break;
                    } else {
                        aeo.a((Object) t, j2, aeo.a((Object) t2, j2));
                        b(t, i2);
                        break;
                    }
                case 12:
                    if (!a(t2, i2)) {
                        break;
                    } else {
                        aeo.a((Object) t, j2, aeo.a((Object) t2, j2));
                        b(t, i2);
                        break;
                    }
                case 13:
                    if (!a(t2, i2)) {
                        break;
                    } else {
                        aeo.a((Object) t, j2, aeo.a((Object) t2, j2));
                        b(t, i2);
                        break;
                    }
                case 14:
                    if (!a(t2, i2)) {
                        break;
                    } else {
                        aeo.a((Object) t, j2, aeo.b(t2, j2));
                        b(t, i2);
                        break;
                    }
                case 15:
                    if (!a(t2, i2)) {
                        break;
                    } else {
                        aeo.a((Object) t, j2, aeo.a((Object) t2, j2));
                        b(t, i2);
                        break;
                    }
                case 16:
                    if (!a(t2, i2)) {
                        break;
                    } else {
                        aeo.a((Object) t, j2, aeo.b(t2, j2));
                        b(t, i2);
                        break;
                    }
                case 17:
                    a(t, t2, i2);
                    break;
                case 18:
                case 19:
                case 20:
                case 21:
                case 22:
                case 23:
                case 24:
                case 25:
                case 26:
                case 27:
                case 28:
                case 29:
                case 30:
                case 31:
                case 32:
                case 33:
                case 34:
                case 35:
                case 36:
                case 37:
                case 38:
                case 39:
                case 40:
                case 41:
                case 42:
                case 43:
                case 44:
                case 45:
                case 46:
                case 47:
                case 48:
                case 49:
                    this.p.a(t, t2, j2);
                    break;
                case 50:
                    adr.a(this.s, t, t2, j2);
                    break;
                case 51:
                case 52:
                case 53:
                case 54:
                case 55:
                case 56:
                case 57:
                case 58:
                case 59:
                    if (!a(t2, i3, i2)) {
                        break;
                    } else {
                        aeo.a((Object) t, j2, aeo.f(t2, j2));
                        b(t, i3, i2);
                        break;
                    }
                case 60:
                    b(t, t2, i2);
                    break;
                case 61:
                case 62:
                case 63:
                case 64:
                case 65:
                case 66:
                case 67:
                    if (!a(t2, i3, i2)) {
                        break;
                    } else {
                        aeo.a((Object) t, j2, aeo.f(t2, j2));
                        b(t, i3, i2);
                        break;
                    }
                case 68:
                    b(t, t2, i2);
                    break;
            }
        }
        if (!this.j) {
            adr.a(this.q, t, t2);
            if (this.h) {
                adr.a(this.r, t, t2);
            }
        }
    }

    public final void c(T t) {
        if (this.m != null) {
            for (int d2 : this.m) {
                long d3 = (long) (d(d2) & 1048575);
                Object f2 = aeo.f(t, d3);
                if (f2 != null) {
                    aeo.a((Object) t, d3, this.s.d(f2));
                }
            }
        }
        if (this.n != null) {
            for (int i2 : this.n) {
                this.p.b(t, (long) i2);
            }
        }
        this.q.d(t);
        if (this.h) {
            this.r.c(t);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0043  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x004e  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x005d  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x006f  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00a0  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x00b3  */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x0058 A[SYNTHETIC] */
    public final boolean d(T t) {
        int i2;
        int i3;
        boolean z;
        boolean z2;
        if (this.l == null || this.l.length == 0) {
            return true;
        }
        int i4 = -1;
        int i5 = 0;
        int[] iArr = this.l;
        int length = iArr.length;
        int i6 = 0;
        while (i6 < length) {
            int i7 = iArr[i6];
            int g2 = g(i7);
            int d2 = d(g2);
            int i8 = 0;
            if (!this.j) {
                int i9 = this.f2468b[g2 + 2];
                int i10 = 1048575 & i9;
                i8 = 1 << (i9 >>> 20);
                if (i10 != i4) {
                    i4 = i10;
                    i2 = f2467a.getInt(t, (long) i10);
                    i3 = i8;
                    if (!((268435456 & d2) == 0) && !a(t, g2, i2, i3)) {
                        return false;
                    }
                    switch ((267386880 & d2) >>> 20) {
                        case 9:
                        case 17:
                            if (a(t, g2, i2, i3) && !a((Object) t, d2, a(g2))) {
                                return false;
                            }
                        case 27:
                        case 49:
                            List list = (List) aeo.f(t, (long) (1048575 & d2));
                            if (!list.isEmpty()) {
                                adp a2 = a(g2);
                                int i11 = 0;
                                while (true) {
                                    if (i11 < list.size()) {
                                        if (!a2.d(list.get(i11))) {
                                            z2 = false;
                                        } else {
                                            i11++;
                                        }
                                    }
                                }
                            }
                            z2 = true;
                            if (z2) {
                                break;
                            } else {
                                return false;
                            }
                        case 50:
                            Map b2 = this.s.b(aeo.f(t, (long) (1048575 & d2)));
                            if (!b2.isEmpty()) {
                                if (this.s.f(b(g2)).c.a() == afb.MESSAGE) {
                                    adp adp = null;
                                    Iterator it = b2.values().iterator();
                                    while (true) {
                                        if (it.hasNext()) {
                                            Object next = it.next();
                                            if (adp == null) {
                                                adp = adj.a().a(next.getClass());
                                            }
                                            if (!adp.d(next)) {
                                                z = false;
                                            }
                                        }
                                    }
                                }
                            }
                            z = true;
                            if (z) {
                                break;
                            } else {
                                return false;
                            }
                        case 60:
                        case 68:
                            if (a(t, i7, g2) && !a((Object) t, d2, a(g2))) {
                                return false;
                            }
                    }
                    i6++;
                    i5 = i2;
                }
            }
            i2 = i5;
            i3 = i8;
            if (!((268435456 & d2) == 0)) {
            }
            switch ((267386880 & d2) >>> 20) {
                case 9:
                case 17:
                    break;
                case 27:
                case 49:
                    break;
                case 50:
                    break;
                case 60:
                case 68:
                    break;
            }
            i6++;
            i5 = i2;
        }
        return !this.h || this.r.a((Object) t).g();
    }
}
