package com.google.android.gms.internal.ads;

import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

final class zr {

    /* renamed from: a reason: collision with root package name */
    private final ConcurrentHashMap<zs, List<Throwable>> f3824a = new ConcurrentHashMap<>(16, 0.75f, 10);

    /* renamed from: b reason: collision with root package name */
    private final ReferenceQueue<Throwable> f3825b = new ReferenceQueue<>();

    zr() {
    }

    public final List<Throwable> a(Throwable th, boolean z) {
        Reference poll = this.f3825b.poll();
        while (poll != null) {
            this.f3824a.remove(poll);
            poll = this.f3825b.poll();
        }
        return (List) this.f3824a.get(new zs(th, null));
    }
}
