package com.google.android.gms.internal.ads;

import android.location.Location;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.a.b;
import java.util.ArrayList;

public final class aor implements Creator<aop> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b2 = b.b(parcel);
        int i = 0;
        long j = 0;
        Bundle bundle = null;
        int i2 = 0;
        ArrayList arrayList = null;
        boolean z = false;
        int i3 = 0;
        boolean z2 = false;
        String str = null;
        arn arn = null;
        Location location = null;
        String str2 = null;
        Bundle bundle2 = null;
        Bundle bundle3 = null;
        ArrayList arrayList2 = null;
        String str3 = null;
        String str4 = null;
        boolean z3 = false;
        while (parcel.dataPosition() < b2) {
            int a2 = b.a(parcel);
            switch (b.a(a2)) {
                case 1:
                    i = b.d(parcel, a2);
                    break;
                case 2:
                    j = b.f(parcel, a2);
                    break;
                case 3:
                    bundle = b.m(parcel, a2);
                    break;
                case 4:
                    i2 = b.d(parcel, a2);
                    break;
                case 5:
                    arrayList = b.q(parcel, a2);
                    break;
                case 6:
                    z = b.c(parcel, a2);
                    break;
                case 7:
                    i3 = b.d(parcel, a2);
                    break;
                case 8:
                    z2 = b.c(parcel, a2);
                    break;
                case 9:
                    str = b.k(parcel, a2);
                    break;
                case 10:
                    arn = (arn) b.a(parcel, a2, arn.CREATOR);
                    break;
                case 11:
                    location = (Location) b.a(parcel, a2, Location.CREATOR);
                    break;
                case 12:
                    str2 = b.k(parcel, a2);
                    break;
                case 13:
                    bundle2 = b.m(parcel, a2);
                    break;
                case 14:
                    bundle3 = b.m(parcel, a2);
                    break;
                case 15:
                    arrayList2 = b.q(parcel, a2);
                    break;
                case 16:
                    str3 = b.k(parcel, a2);
                    break;
                case 17:
                    str4 = b.k(parcel, a2);
                    break;
                case 18:
                    z3 = b.c(parcel, a2);
                    break;
                default:
                    b.b(parcel, a2);
                    break;
            }
        }
        b.r(parcel, b2);
        return new aop(i, j, bundle, i2, arrayList, z, i3, z2, str, arn, location, str2, bundle2, bundle3, arrayList2, str3, str4, z3);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new aop[i];
    }
}
