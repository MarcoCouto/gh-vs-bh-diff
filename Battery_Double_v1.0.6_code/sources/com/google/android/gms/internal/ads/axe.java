package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.DeadObjectException;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.google.android.gms.common.internal.e;
import com.google.android.gms.common.internal.e.a;
import com.google.android.gms.common.internal.e.b;

@cm
public final class axe extends e<axj> {
    axe(Context context, Looper looper, a aVar, b bVar) {
        super(context, looper, 166, aVar, bVar, null);
    }

    public final axj A() throws DeadObjectException {
        return (axj) super.x();
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ IInterface a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.ads.internal.httpcache.IHttpAssetsCacheService");
        return queryLocalInterface instanceof axj ? (axj) queryLocalInterface : new axk(iBinder);
    }

    /* access modifiers changed from: protected */
    public final String i() {
        return "com.google.android.gms.ads.service.HTTP";
    }

    /* access modifiers changed from: protected */
    public final String l() {
        return "com.google.android.gms.ads.internal.httpcache.IHttpAssetsCacheService";
    }
}
