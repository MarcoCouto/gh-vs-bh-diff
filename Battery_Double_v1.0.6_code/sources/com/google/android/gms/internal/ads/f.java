package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.ax;
import java.util.Map;

@cm
public final class f {

    /* renamed from: a reason: collision with root package name */
    private final qn f3301a;

    /* renamed from: b reason: collision with root package name */
    private final boolean f3302b;
    private final String c;

    public f(qn qnVar, Map<String, String> map) {
        this.f3301a = qnVar;
        this.c = (String) map.get("forceOrientation");
        if (map.containsKey("allowOrientationChange")) {
            this.f3302b = Boolean.parseBoolean((String) map.get("allowOrientationChange"));
        } else {
            this.f3302b = true;
        }
    }

    public final void a() {
        if (this.f3301a == null) {
            jm.e("AdWebView is null");
            return;
        }
        int c2 = "portrait".equalsIgnoreCase(this.c) ? ax.g().b() : "landscape".equalsIgnoreCase(this.c) ? ax.g().a() : this.f3302b ? -1 : ax.g().c();
        this.f3301a.setRequestedOrientation(c2);
    }
}
