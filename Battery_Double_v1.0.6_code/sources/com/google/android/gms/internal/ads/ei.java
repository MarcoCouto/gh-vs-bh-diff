package com.google.android.gms.internal.ads;

final class ei implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ is f3282a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ eh f3283b;

    ei(eh ehVar, is isVar) {
        this.f3283b = ehVar;
        this.f3282a = isVar;
    }

    public final void run() {
        this.f3283b.h.a(this.f3282a);
        if (this.f3283b.l != null) {
            this.f3283b.l.c();
            this.f3283b.l = null;
        }
    }
}
