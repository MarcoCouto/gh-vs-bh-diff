package com.google.android.gms.internal.ads;

import java.lang.reflect.InvocationTargetException;

public final class ajd extends ajj {
    private final aii d;
    private long e;

    public ajd(ahy ahy, String str, String str2, zz zzVar, int i, int i2, aii aii) {
        super(ahy, str, str2, zzVar, i, 53);
        this.d = aii;
        if (aii != null) {
            this.e = aii.a();
        }
    }

    /* access modifiers changed from: protected */
    public final void a() throws IllegalAccessException, InvocationTargetException {
        if (this.d != null) {
            this.f2637b.I = (Long) this.c.invoke(null, new Object[]{Long.valueOf(this.e)});
        }
    }
}
