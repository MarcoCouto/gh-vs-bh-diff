package com.google.android.gms.internal.ads;

import android.os.IInterface;
import android.os.RemoteException;

public interface gn extends IInterface {
    void a() throws RemoteException;

    void a(int i) throws RemoteException;

    void a(gc gcVar) throws RemoteException;

    void b() throws RemoteException;

    void c() throws RemoteException;

    void d() throws RemoteException;

    void e() throws RemoteException;

    void f() throws RemoteException;
}
