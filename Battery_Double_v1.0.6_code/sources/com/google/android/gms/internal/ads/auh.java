package com.google.android.gms.internal.ads;

import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

final /* synthetic */ class auh implements rw {

    /* renamed from: a reason: collision with root package name */
    private final aug f2929a;

    /* renamed from: b reason: collision with root package name */
    private final Map f2930b;
    private final bq c;

    auh(aug aug, Map map, bq bqVar) {
        this.f2929a = aug;
        this.f2930b = map;
        this.c = bqVar;
    }

    public final void a(boolean z) {
        aug aug = this.f2929a;
        Map map = this.f2930b;
        bq bqVar = this.c;
        aug.f2927a.f2926b = (String) map.get("id");
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("messageType", "htmlLoaded");
            jSONObject.put("id", aug.f2927a.f2926b);
            bqVar.a("sendMessageToNativeJs", jSONObject);
        } catch (JSONException e) {
            jm.b("Unable to dispatch sendMessageToNativeJs event", e);
        }
    }
}
