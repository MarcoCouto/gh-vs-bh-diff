package com.google.android.gms.internal.ads;

import java.util.List;

public final class xn extends abp<xn, a> implements acy {
    private static volatile adi<xn> zzakh;
    /* access modifiers changed from: private */
    public static final xn zzdlt = new xn();
    private int zzdlq;
    private int zzdlr;
    private abu<b> zzdls = m();

    public static final class a extends com.google.android.gms.internal.ads.abp.a<xn, a> implements acy {
        private a() {
            super(xn.zzdlt);
        }

        /* synthetic */ a(xo xoVar) {
            this();
        }
    }

    public static final class b extends abp<b, a> implements acy {
        private static volatile adi<b> zzakh;
        /* access modifiers changed from: private */
        public static final b zzdlx = new b();
        private int zzdlj;
        private xe zzdlu;
        private int zzdlv;
        private int zzdlw;

        public static final class a extends com.google.android.gms.internal.ads.abp.a<b, a> implements acy {
            private a() {
                super(b.zzdlx);
            }

            /* synthetic */ a(xo xoVar) {
                this();
            }
        }

        static {
            abp.a(b.class, zzdlx);
        }

        private b() {
        }

        /* JADX WARNING: type inference failed for: r0v6, types: [com.google.android.gms.internal.ads.adi<com.google.android.gms.internal.ads.xn$b>] */
        /* JADX WARNING: type inference failed for: r0v7, types: [java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v8, types: [com.google.android.gms.internal.ads.adi<com.google.android.gms.internal.ads.xn$b>, com.google.android.gms.internal.ads.abp$b] */
        /* JADX WARNING: type inference failed for: r0v16 */
        /* access modifiers changed from: protected */
        /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r0v8, types: [com.google.android.gms.internal.ads.adi<com.google.android.gms.internal.ads.xn$b>, com.google.android.gms.internal.ads.abp$b]
  assigns: [com.google.android.gms.internal.ads.abp$b]
  uses: [com.google.android.gms.internal.ads.adi<com.google.android.gms.internal.ads.xn$b>]
  mth insns count: 44
        	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
        	at java.util.ArrayList.forEach(Unknown Source)
        	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
        	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
        	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
        	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
        	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
        	at java.util.ArrayList.forEach(Unknown Source)
        	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
        	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$0(DepthTraversal.java:13)
        	at java.util.ArrayList.forEach(Unknown Source)
        	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:13)
        	at jadx.core.ProcessClass.process(ProcessClass.java:30)
        	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
        	at java.util.ArrayList.forEach(Unknown Source)
        	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
        	at jadx.core.ProcessClass.process(ProcessClass.java:35)
        	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
        	at jadx.api.JavaClass.decompile(JavaClass.java:62)
        	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
         */
        /* JADX WARNING: Unknown variable types count: 2 */
        public final Object a(int i, Object obj, Object obj2) {
            ? r0;
            switch (xo.f3777a[i - 1]) {
                case 1:
                    return new b();
                case 2:
                    return new a(null);
                case 3:
                    Object[] objArr = {"zzdlu", "zzdlv", "zzdlw", "zzdlj"};
                    return a((acw) zzdlx, "\u0000\u0004\u0000\u0000\u0001\u0004\u0004\u0005\u0000\u0000\u0000\u0001\t\u0002\f\u0003\u000b\u0004\f", objArr);
                case 4:
                    return zzdlx;
                case 5:
                    adi<b> adi = zzakh;
                    if (adi != null) {
                        return adi;
                    }
                    synchronized (b.class) {
                        r0 = zzakh;
                        if (r0 == 0) {
                            ? bVar = new com.google.android.gms.internal.ads.abp.b(zzdlx);
                            zzakh = bVar;
                            r0 = bVar;
                        }
                    }
                    return r0;
                case 6:
                    return Byte.valueOf(1);
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        public final boolean a() {
            return this.zzdlu != null;
        }

        public final xe b() {
            return this.zzdlu == null ? xe.e() : this.zzdlu;
        }

        public final xh c() {
            xh a2 = xh.a(this.zzdlv);
            return a2 == null ? xh.UNRECOGNIZED : a2;
        }

        public final int d() {
            return this.zzdlw;
        }

        public final ya e() {
            ya a2 = ya.a(this.zzdlj);
            return a2 == null ? ya.UNRECOGNIZED : a2;
        }
    }

    static {
        abp.a(xn.class, zzdlt);
    }

    private xn() {
    }

    public static xn a(byte[] bArr) throws abv {
        return (xn) abp.a(zzdlt, bArr);
    }

    public final int a() {
        return this.zzdlr;
    }

    /* JADX WARNING: type inference failed for: r0v6, types: [com.google.android.gms.internal.ads.adi<com.google.android.gms.internal.ads.xn>] */
    /* JADX WARNING: type inference failed for: r0v7, types: [java.lang.Object] */
    /* JADX WARNING: type inference failed for: r0v8, types: [com.google.android.gms.internal.ads.adi<com.google.android.gms.internal.ads.xn>, com.google.android.gms.internal.ads.abp$b] */
    /* JADX WARNING: type inference failed for: r0v16 */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r0v8, types: [com.google.android.gms.internal.ads.adi<com.google.android.gms.internal.ads.xn>, com.google.android.gms.internal.ads.abp$b]
  assigns: [com.google.android.gms.internal.ads.abp$b]
  uses: [com.google.android.gms.internal.ads.adi<com.google.android.gms.internal.ads.xn>]
  mth insns count: 44
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
    	at jadx.core.ProcessClass.process(ProcessClass.java:35)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Unknown variable types count: 2 */
    public final Object a(int i, Object obj, Object obj2) {
        ? r0;
        switch (xo.f3777a[i - 1]) {
            case 1:
                return new xn();
            case 2:
                return new a(null);
            case 3:
                Object[] objArr = {"zzdlq", "zzdlr", "zzdls", b.class};
                return a((acw) zzdlt, "\u0000\u0002\u0000\u0001\u0001\u0002\u0002\u0003\u0000\u0001\u0000\u0001\u000b\u0002\u001b", objArr);
            case 4:
                return zzdlt;
            case 5:
                adi<xn> adi = zzakh;
                if (adi != null) {
                    return adi;
                }
                synchronized (xn.class) {
                    r0 = zzakh;
                    if (r0 == 0) {
                        ? bVar = new com.google.android.gms.internal.ads.abp.b(zzdlt);
                        zzakh = bVar;
                        r0 = bVar;
                    }
                }
                return r0;
            case 6:
                return Byte.valueOf(1);
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    public final List<b> b() {
        return this.zzdls;
    }

    public final int c() {
        return this.zzdls.size();
    }
}
