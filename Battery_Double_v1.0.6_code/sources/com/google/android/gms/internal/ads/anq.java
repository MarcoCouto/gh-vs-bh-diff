package com.google.android.gms.internal.ads;

import java.io.IOException;

public final class anq extends afh<anq> {

    /* renamed from: a reason: collision with root package name */
    private ano f2770a;

    /* renamed from: b reason: collision with root package name */
    private anw[] f2771b;
    private Integer c;
    private anx d;

    public anq() {
        this.f2770a = null;
        this.f2771b = anw.b();
        this.c = null;
        this.d = null;
        this.Y = null;
        this.Z = -1;
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public final anq a(afd afd) throws IOException {
        while (true) {
            int a2 = afd.a();
            switch (a2) {
                case 0:
                    break;
                case 10:
                    if (this.f2770a == null) {
                        this.f2770a = new ano();
                    }
                    afd.a((afn) this.f2770a);
                    continue;
                case 18:
                    int a3 = afq.a(afd, 18);
                    int length = this.f2771b == null ? 0 : this.f2771b.length;
                    anw[] anwArr = new anw[(a3 + length)];
                    if (length != 0) {
                        System.arraycopy(this.f2771b, 0, anwArr, 0, length);
                    }
                    while (length < anwArr.length - 1) {
                        anwArr[length] = new anw();
                        afd.a((afn) anwArr[length]);
                        afd.a();
                        length++;
                    }
                    anwArr[length] = new anw();
                    afd.a((afn) anwArr[length]);
                    this.f2771b = anwArr;
                    continue;
                case 24:
                    int j = afd.j();
                    try {
                        this.c = Integer.valueOf(anf.a(afd.g()));
                        continue;
                    } catch (IllegalArgumentException e) {
                        afd.e(j);
                        a(afd, a2);
                        break;
                    }
                case 34:
                    if (this.d == null) {
                        this.d = new anx();
                    }
                    afd.a((afn) this.d);
                    continue;
                default:
                    if (!super.a(afd, a2)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    /* access modifiers changed from: protected */
    public final int a() {
        int a2 = super.a();
        if (this.f2770a != null) {
            a2 += aff.b(1, (afn) this.f2770a);
        }
        if (this.f2771b != null && this.f2771b.length > 0) {
            int i = a2;
            for (anw anw : this.f2771b) {
                if (anw != null) {
                    i += aff.b(2, (afn) anw);
                }
            }
            a2 = i;
        }
        if (this.c != null) {
            a2 += aff.b(3, this.c.intValue());
        }
        return this.d != null ? a2 + aff.b(4, (afn) this.d) : a2;
    }

    public final void a(aff aff) throws IOException {
        if (this.f2770a != null) {
            aff.a(1, (afn) this.f2770a);
        }
        if (this.f2771b != null && this.f2771b.length > 0) {
            for (anw anw : this.f2771b) {
                if (anw != null) {
                    aff.a(2, (afn) anw);
                }
            }
        }
        if (this.c != null) {
            aff.a(3, this.c.intValue());
        }
        if (this.d != null) {
            aff.a(4, (afn) this.d);
        }
        super.a(aff);
    }
}
