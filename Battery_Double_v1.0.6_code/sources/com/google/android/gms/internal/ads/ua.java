package com.google.android.gms.internal.ads;

import java.security.GeneralSecurityException;

public final class ua {

    /* renamed from: a reason: collision with root package name */
    private xn f3715a;

    private ua(xn xnVar) {
        this.f3715a = xnVar;
    }

    static final ua a(xn xnVar) throws GeneralSecurityException {
        if (xnVar != null && xnVar.c() > 0) {
            return new ua(xnVar);
        }
        throw new GeneralSecurityException("empty keyset");
    }

    /* access modifiers changed from: 0000 */
    public final xn a() {
        return this.f3715a;
    }

    public final String toString() {
        return ui.a(this.f3715a).toString();
    }
}
