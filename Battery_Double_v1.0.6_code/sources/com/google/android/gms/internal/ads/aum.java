package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.a.b;

public final class aum implements Creator<aul> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int i = 0;
        int b2 = b.b(parcel);
        arr arr = null;
        boolean z = false;
        int i2 = 0;
        boolean z2 = false;
        int i3 = 0;
        while (parcel.dataPosition() < b2) {
            int a2 = b.a(parcel);
            switch (b.a(a2)) {
                case 1:
                    i3 = b.d(parcel, a2);
                    break;
                case 2:
                    z2 = b.c(parcel, a2);
                    break;
                case 3:
                    i2 = b.d(parcel, a2);
                    break;
                case 4:
                    z = b.c(parcel, a2);
                    break;
                case 5:
                    i = b.d(parcel, a2);
                    break;
                case 6:
                    arr = (arr) b.a(parcel, a2, arr.CREATOR);
                    break;
                default:
                    b.b(parcel, a2);
                    break;
            }
        }
        b.r(parcel, b2);
        return new aul(i3, z2, i2, z, i, arr);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new aul[i];
    }
}
