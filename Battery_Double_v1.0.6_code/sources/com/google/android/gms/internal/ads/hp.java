package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import com.google.android.gms.common.internal.a.a;
import com.google.android.gms.common.internal.a.c;
import com.google.android.gms.common.internal.z;
import org.json.JSONArray;
import org.json.JSONException;

@cm
public final class hp extends a {
    public static final Creator<hp> CREATOR = new hq();

    /* renamed from: a reason: collision with root package name */
    public final String f3369a;

    /* renamed from: b reason: collision with root package name */
    public final int f3370b;

    public hp(com.google.android.gms.ads.reward.a aVar) {
        this(aVar.a(), aVar.b());
    }

    public hp(String str, int i) {
        this.f3369a = str;
        this.f3370b = i;
    }

    public static hp a(String str) {
        hp hpVar = null;
        if (TextUtils.isEmpty(str)) {
            return hpVar;
        }
        try {
            return a(new JSONArray(str));
        } catch (JSONException e) {
            return hpVar;
        }
    }

    public static hp a(JSONArray jSONArray) throws JSONException {
        if (jSONArray == null || jSONArray.length() == 0) {
            return null;
        }
        return new hp(jSONArray.getJSONObject(0).optString("rb_type"), jSONArray.getJSONObject(0).optInt("rb_amount"));
    }

    public final boolean equals(Object obj) {
        if (obj == null || !(obj instanceof hp)) {
            return false;
        }
        hp hpVar = (hp) obj;
        return z.a(this.f3369a, hpVar.f3369a) && z.a(Integer.valueOf(this.f3370b), Integer.valueOf(hpVar.f3370b));
    }

    public final int hashCode() {
        return z.a(this.f3369a, Integer.valueOf(this.f3370b));
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = c.a(parcel);
        c.a(parcel, 2, this.f3369a, false);
        c.a(parcel, 3, this.f3370b);
        c.a(parcel, a2);
    }
}
