package com.google.android.gms.internal.ads;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.security.GeneralSecurityException;

final class aie {

    /* renamed from: a reason: collision with root package name */
    static ty f2618a;

    static boolean a(ahy ahy) throws IllegalAccessException, InvocationTargetException {
        if (f2618a != null) {
            return true;
        }
        String str = (String) ape.f().a(asi.bK);
        if (str == null || str.length() == 0) {
            if (ahy == null) {
                str = null;
            } else {
                Method a2 = ahy.a("4o7tecxtkw7XaNt5hPj+0H1LvOi0SgxCIJTY9VcbazM/HSl/sFlxBFwnc8glnvoB", "RgSY6YxU2k1vLXOV3vapBnQwJDzYDlmX50wbm2tDcnw=");
                str = a2 == null ? null : (String) a2.invoke(null, new Object[0]);
            }
            if (str == null) {
                return false;
            }
        }
        try {
            try {
                ua a3 = ue.a(agg.a(str, true));
                for (xl xlVar : uw.f3731a.a()) {
                    if (xlVar.b().isEmpty()) {
                        throw new GeneralSecurityException("Missing type_url.");
                    } else if (xlVar.a().isEmpty()) {
                        throw new GeneralSecurityException("Missing primitive_name.");
                    } else if (xlVar.e().isEmpty()) {
                        throw new GeneralSecurityException("Missing catalogue_name.");
                    } else {
                        uh.a(xlVar.b(), uh.a(xlVar.e()).a(xlVar.b(), xlVar.a(), xlVar.c()), xlVar.d());
                    }
                }
                f2618a = uz.a(a3);
                return f2618a != null;
            } catch (GeneralSecurityException e) {
                return false;
            }
        } catch (IllegalArgumentException e2) {
            return false;
        }
    }
}
