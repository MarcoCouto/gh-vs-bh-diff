package com.google.android.gms.internal.ads;

import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.google.android.gms.b.a;

public final class t extends ajk implements r {
    t(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.overlay.client.IAdOverlay");
    }

    public final void a(int i, int i2, Intent intent) throws RemoteException {
        Parcel r_ = r_();
        r_.writeInt(i);
        r_.writeInt(i2);
        ajm.a(r_, (Parcelable) intent);
        b(12, r_);
    }

    public final void a(Bundle bundle) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (Parcelable) bundle);
        b(1, r_);
    }

    public final void a(a aVar) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (IInterface) aVar);
        b(13, r_);
    }

    public final void b(Bundle bundle) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (Parcelable) bundle);
        Parcel a2 = a(6, r_);
        if (a2.readInt() != 0) {
            bundle.readFromParcel(a2);
        }
        a2.recycle();
    }

    public final void d() throws RemoteException {
        b(10, r_());
    }

    public final boolean e() throws RemoteException {
        Parcel a2 = a(11, r_());
        boolean a3 = ajm.a(a2);
        a2.recycle();
        return a3;
    }

    public final void f() throws RemoteException {
        b(2, r_());
    }

    public final void g() throws RemoteException {
        b(3, r_());
    }

    public final void h() throws RemoteException {
        b(4, r_());
    }

    public final void i() throws RemoteException {
        b(5, r_());
    }

    public final void j() throws RemoteException {
        b(7, r_());
    }

    public final void k() throws RemoteException {
        b(8, r_());
    }

    public final void l() throws RemoteException {
        b(9, r_());
    }
}
