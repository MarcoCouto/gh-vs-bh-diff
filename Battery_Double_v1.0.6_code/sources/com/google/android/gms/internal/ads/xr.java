package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.abp.b;
import com.google.android.gms.internal.ads.abp.e;

public final class xr extends abp<xr, a> implements acy {
    private static volatile adi<xr> zzakh;
    /* access modifiers changed from: private */
    public static final xr zzdmc = new xr();
    private int zzdih;
    private xt zzdmb;

    public static final class a extends com.google.android.gms.internal.ads.abp.a<xr, a> implements acy {
        private a() {
            super(xr.zzdmc);
        }

        /* synthetic */ a(xs xsVar) {
            this();
        }

        public final a a(int i) {
            b();
            ((xr) this.f2433a).b(0);
            return this;
        }

        public final a a(xt xtVar) {
            b();
            ((xr) this.f2433a).a(xtVar);
            return this;
        }
    }

    static {
        abp.a(xr.class, zzdmc);
    }

    private xr() {
    }

    public static xr a(aah aah) throws abv {
        return (xr) abp.a(zzdmc, aah);
    }

    /* access modifiers changed from: private */
    public final void a(xt xtVar) {
        if (xtVar == null) {
            throw new NullPointerException();
        }
        this.zzdmb = xtVar;
    }

    /* access modifiers changed from: private */
    public final void b(int i) {
        this.zzdih = i;
    }

    public static a c() {
        return (a) ((com.google.android.gms.internal.ads.abp.a) zzdmc.a(e.e, (Object) null, (Object) null));
    }

    public final int a() {
        return this.zzdih;
    }

    /* JADX WARNING: type inference failed for: r0v6, types: [com.google.android.gms.internal.ads.adi<com.google.android.gms.internal.ads.xr>] */
    /* JADX WARNING: type inference failed for: r0v7, types: [java.lang.Object] */
    /* JADX WARNING: type inference failed for: r0v8, types: [com.google.android.gms.internal.ads.abp$b, com.google.android.gms.internal.ads.adi<com.google.android.gms.internal.ads.xr>] */
    /* JADX WARNING: type inference failed for: r0v16 */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r0v8, types: [com.google.android.gms.internal.ads.abp$b, com.google.android.gms.internal.ads.adi<com.google.android.gms.internal.ads.xr>]
  assigns: [com.google.android.gms.internal.ads.abp$b]
  uses: [com.google.android.gms.internal.ads.adi<com.google.android.gms.internal.ads.xr>]
  mth insns count: 40
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
    	at jadx.core.ProcessClass.process(ProcessClass.java:35)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Unknown variable types count: 2 */
    public final Object a(int i, Object obj, Object obj2) {
        ? r0;
        switch (xs.f3779a[i - 1]) {
            case 1:
                return new xr();
            case 2:
                return new a(null);
            case 3:
                Object[] objArr = {"zzdih", "zzdmb"};
                return a((acw) zzdmc, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0003\u0000\u0000\u0000\u0001\u000b\u0002\t", objArr);
            case 4:
                return zzdmc;
            case 5:
                adi<xr> adi = zzakh;
                if (adi != null) {
                    return adi;
                }
                synchronized (xr.class) {
                    r0 = zzakh;
                    if (r0 == 0) {
                        ? bVar = new b(zzdmc);
                        zzakh = bVar;
                        r0 = bVar;
                    }
                }
                return r0;
            case 6:
                return Byte.valueOf(1);
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    public final xt b() {
        return this.zzdmb == null ? xt.b() : this.zzdmb;
    }
}
