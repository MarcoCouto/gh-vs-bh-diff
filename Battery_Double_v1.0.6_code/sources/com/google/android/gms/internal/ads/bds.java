package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.view.View;
import com.google.android.gms.ads.b.c;
import com.google.android.gms.ads.mediation.h;
import com.google.android.gms.b.a;
import com.google.android.gms.b.b;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@cm
public final class bds extends bdi {

    /* renamed from: a reason: collision with root package name */
    private final h f3154a;

    public bds(h hVar) {
        this.f3154a = hVar;
    }

    public final String a() {
        return this.f3154a.i();
    }

    public final void a(a aVar) {
        this.f3154a.c((View) b.a(aVar));
    }

    public final void a(a aVar, a aVar2, a aVar3) {
        this.f3154a.a((View) b.a(aVar), (HashMap) b.a(aVar2), (HashMap) b.a(aVar3));
    }

    public final List b() {
        List<c.b> j = this.f3154a.j();
        if (j == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        for (c.b bVar : j) {
            arrayList.add(new atm(bVar.a(), bVar.b(), bVar.c()));
        }
        return arrayList;
    }

    public final void b(a aVar) {
        this.f3154a.a((View) b.a(aVar));
    }

    public final String c() {
        return this.f3154a.k();
    }

    public final void c(a aVar) {
        this.f3154a.b((View) b.a(aVar));
    }

    public final auw d() {
        c.b l = this.f3154a.l();
        if (l != null) {
            return new atm(l.a(), l.b(), l.c());
        }
        return null;
    }

    public final String e() {
        return this.f3154a.m();
    }

    public final String f() {
        return this.f3154a.n();
    }

    public final void g() {
        this.f3154a.e();
    }

    public final boolean h() {
        return this.f3154a.a();
    }

    public final boolean i() {
        return this.f3154a.b();
    }

    public final Bundle j() {
        return this.f3154a.c();
    }

    public final a k() {
        View d = this.f3154a.d();
        if (d == null) {
            return null;
        }
        return b.a(d);
    }

    public final aqs l() {
        if (this.f3154a.g() != null) {
            return this.f3154a.g().a();
        }
        return null;
    }

    public final aus m() {
        return null;
    }

    public final a n() {
        View f = this.f3154a.f();
        if (f == null) {
            return null;
        }
        return b.a(f);
    }

    public final a o() {
        return null;
    }
}
