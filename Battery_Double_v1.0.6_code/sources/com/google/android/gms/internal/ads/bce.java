package com.google.android.gms.internal.ads;

@cm
public final class bce extends bcy {

    /* renamed from: a reason: collision with root package name */
    private final Object f3125a = new Object();

    /* renamed from: b reason: collision with root package name */
    private bcj f3126b;
    private bcc c;

    public final void a() {
        synchronized (this.f3125a) {
            if (this.c != null) {
                this.c.U();
            }
        }
    }

    public final void a(int i) {
        synchronized (this.f3125a) {
            if (this.f3126b != null) {
                this.f3126b.a(i == 3 ? 1 : 2);
                this.f3126b = null;
            }
        }
    }

    public final void a(avt avt, String str) {
        synchronized (this.f3125a) {
            if (this.c != null) {
                this.c.a(avt, str);
            }
        }
    }

    public final void a(bcc bcc) {
        synchronized (this.f3125a) {
            this.c = bcc;
        }
    }

    public final void a(bcj bcj) {
        synchronized (this.f3125a) {
            this.f3126b = bcj;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        return;
     */
    public final void a(bda bda) {
        synchronized (this.f3125a) {
            if (this.f3126b != null) {
                this.f3126b.a(0, bda);
                this.f3126b = null;
            } else if (this.c != null) {
                this.c.ab();
            }
        }
    }

    public final void a(String str) {
    }

    public final void a(String str, String str2) {
        synchronized (this.f3125a) {
            if (this.c != null) {
                this.c.b(str, str2);
            }
        }
    }

    public final void b() {
        synchronized (this.f3125a) {
            if (this.c != null) {
                this.c.Y();
            }
        }
    }

    public final void c() {
        synchronized (this.f3125a) {
            if (this.c != null) {
                this.c.Z();
            }
        }
    }

    public final void d() {
        synchronized (this.f3125a) {
            if (this.c != null) {
                this.c.aa();
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        return;
     */
    public final void e() {
        synchronized (this.f3125a) {
            if (this.f3126b != null) {
                this.f3126b.a(0);
                this.f3126b = null;
            } else if (this.c != null) {
                this.c.ab();
            }
        }
    }

    public final void f() {
        synchronized (this.f3125a) {
            if (this.c != null) {
                this.c.V();
            }
        }
    }

    public final void g() {
        synchronized (this.f3125a) {
            if (this.c != null) {
                this.c.W();
            }
        }
    }
}
