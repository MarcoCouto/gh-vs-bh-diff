package com.google.android.gms.internal.ads;

import java.io.IOException;

public final class afm extends IOException {
    public afm(String str) {
        super(str);
    }

    static afm a() {
        return new afm("While parsing a protocol message, the input ended unexpectedly in the middle of a field.  This could mean either than the input has been truncated or that an embedded message misreported its own length.");
    }

    static afm b() {
        return new afm("CodedInputStream encountered an embedded string or message which claimed to have negative size.");
    }

    static afm c() {
        return new afm("CodedInputStream encountered a malformed varint.");
    }
}
