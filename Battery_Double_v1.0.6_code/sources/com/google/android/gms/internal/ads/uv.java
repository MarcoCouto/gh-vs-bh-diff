package com.google.android.gms.internal.ads;

import java.security.GeneralSecurityException;

final class uv implements tz<ty> {
    uv() {
    }

    /* access modifiers changed from: private */
    /* renamed from: d */
    public final ty a(aah aah) throws GeneralSecurityException {
        try {
            wp a2 = wp.a(aah);
            if (!(a2 instanceof wp)) {
                throw new GeneralSecurityException("expected EciesAeadHkdfPublicKey proto");
            }
            wp wpVar = a2;
            zo.a(wpVar.a(), 0);
            vb.a(wpVar.b());
            wl b2 = wpVar.b();
            wr a3 = b2.a();
            return new ym(yq.a(vb.a(a3.a()), wpVar.c().b(), wpVar.d().b()), a3.c().b(), vb.a(a3.b()), vb.a(b2.c()), new vd(b2.b().a()));
        } catch (abv e) {
            throw new GeneralSecurityException("expected serialized EciesAeadHkdfPublicKey proto", e);
        }
    }

    public final int a() {
        return 0;
    }

    public final /* synthetic */ Object a(acw acw) throws GeneralSecurityException {
        if (!(acw instanceof wp)) {
            throw new GeneralSecurityException("expected EciesAeadHkdfPublicKey proto");
        }
        wp wpVar = (wp) acw;
        zo.a(wpVar.a(), 0);
        vb.a(wpVar.b());
        wl b2 = wpVar.b();
        wr a2 = b2.a();
        return new ym(yq.a(vb.a(a2.a()), wpVar.c().b(), wpVar.d().b()), a2.c().b(), vb.a(a2.b()), vb.a(b2.c()), new vd(b2.b().a()));
    }

    public final acw b(aah aah) throws GeneralSecurityException {
        throw new GeneralSecurityException("Not implemented.");
    }

    public final acw b(acw acw) throws GeneralSecurityException {
        throw new GeneralSecurityException("Not implemented.");
    }

    public final xe c(aah aah) throws GeneralSecurityException {
        throw new GeneralSecurityException("Not implemented.");
    }
}
