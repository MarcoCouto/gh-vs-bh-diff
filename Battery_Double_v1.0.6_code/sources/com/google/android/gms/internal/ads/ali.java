package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.ax;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

@cm
public final class ali {

    /* renamed from: a reason: collision with root package name */
    private final Object f2694a = new Object();

    /* renamed from: b reason: collision with root package name */
    private int f2695b;
    private List<alh> c = new LinkedList();

    public final alh a() {
        int i;
        alh alh;
        int i2;
        alh alh2 = null;
        int i3 = 0;
        synchronized (this.f2694a) {
            if (this.c.size() == 0) {
                jm.b("Queue empty");
                return null;
            } else if (this.c.size() >= 2) {
                int i4 = Integer.MIN_VALUE;
                int i5 = 0;
                for (alh alh3 : this.c) {
                    int i6 = alh3.i();
                    if (i6 > i4) {
                        i2 = i6;
                        alh = alh3;
                        i = i5;
                    } else {
                        i = i3;
                        alh = alh2;
                        i2 = i4;
                    }
                    i5++;
                    i4 = i2;
                    alh2 = alh;
                    i3 = i;
                }
                this.c.remove(i3);
                return alh2;
            } else {
                alh alh4 = (alh) this.c.get(0);
                alh4.e();
                return alh4;
            }
        }
    }

    public final boolean a(alh alh) {
        boolean z;
        synchronized (this.f2694a) {
            z = this.c.contains(alh);
        }
        return z;
    }

    public final boolean b(alh alh) {
        synchronized (this.f2694a) {
            Iterator it = this.c.iterator();
            while (it.hasNext()) {
                alh alh2 = (alh) it.next();
                if (!((Boolean) ape.f().a(asi.W)).booleanValue() || ax.i().l().b()) {
                    if (((Boolean) ape.f().a(asi.Y)).booleanValue() && !ax.i().l().d() && alh != alh2 && alh2.d().equals(alh.d())) {
                        it.remove();
                        return true;
                    }
                } else if (alh != alh2 && alh2.b().equals(alh.b())) {
                    it.remove();
                    return true;
                }
            }
            return false;
        }
    }

    public final void c(alh alh) {
        synchronized (this.f2694a) {
            if (this.c.size() >= 10) {
                jm.b("Queue is full, current size = " + this.c.size());
                this.c.remove(0);
            }
            int i = this.f2695b;
            this.f2695b = i + 1;
            alh.a(i);
            this.c.add(alh);
        }
    }
}
