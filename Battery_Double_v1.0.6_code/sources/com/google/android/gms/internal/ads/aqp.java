package com.google.android.gms.internal.ads;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.b.a;

public final class aqp extends ajk implements aqn {
    aqp(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.client.IMobileAdsSettingManager");
    }

    public final void a() throws RemoteException {
        b(1, r_());
    }

    public final void a(float f) throws RemoteException {
        Parcel r_ = r_();
        r_.writeFloat(f);
        b(2, r_);
    }

    public final void a(a aVar, String str) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (IInterface) aVar);
        r_.writeString(str);
        b(5, r_);
    }

    public final void a(String str) throws RemoteException {
        Parcel r_ = r_();
        r_.writeString(str);
        b(3, r_);
    }

    public final void a(String str, a aVar) throws RemoteException {
        Parcel r_ = r_();
        r_.writeString(str);
        ajm.a(r_, (IInterface) aVar);
        b(6, r_);
    }

    public final void a(boolean z) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, z);
        b(4, r_);
    }

    public final float b() throws RemoteException {
        Parcel a2 = a(7, r_());
        float readFloat = a2.readFloat();
        a2.recycle();
        return readFloat;
    }

    public final boolean c() throws RemoteException {
        Parcel a2 = a(8, r_());
        boolean a3 = ajm.a(a2);
        a2.recycle();
        return a3;
    }
}
