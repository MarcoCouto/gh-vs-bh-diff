package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.bn;
import java.util.Arrays;

@cm
final class ayx {

    /* renamed from: a reason: collision with root package name */
    private final Object[] f3015a;

    ayx(aop aop, String str, int i) {
        this.f3015a = bn.a((String) ape.f().a(asi.aV), aop, str, i, null);
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof ayx)) {
            return false;
        }
        return Arrays.equals(this.f3015a, ((ayx) obj).f3015a);
    }

    public final int hashCode() {
        return Arrays.hashCode(this.f3015a);
    }

    public final String toString() {
        String arrays = Arrays.toString(this.f3015a);
        return new StringBuilder(String.valueOf(arrays).length() + 24).append("[InterstitialAdPoolKey ").append(arrays).append("]").toString();
    }
}
