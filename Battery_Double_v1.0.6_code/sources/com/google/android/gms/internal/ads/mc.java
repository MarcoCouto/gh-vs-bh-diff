package com.google.android.gms.internal.ads;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.webkit.WebSettings;
import java.util.concurrent.Callable;

final class mc implements Callable<String> {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ Context f3505a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ Context f3506b;

    mc(ma maVar, Context context, Context context2) {
        this.f3505a = context;
        this.f3506b = context2;
    }

    public final /* synthetic */ Object call() throws Exception {
        SharedPreferences sharedPreferences;
        boolean z = false;
        if (this.f3505a != null) {
            jm.a("Attempting to read user agent from Google Play Services.");
            sharedPreferences = this.f3505a.getSharedPreferences("admob_user_agent", 0);
        } else {
            jm.a("Attempting to read user agent from local cache.");
            sharedPreferences = this.f3506b.getSharedPreferences("admob_user_agent", 0);
            z = true;
        }
        String string = sharedPreferences.getString("user_agent", "");
        if (TextUtils.isEmpty(string)) {
            jm.a("Reading user agent from WebSettings");
            string = WebSettings.getDefaultUserAgent(this.f3506b);
            if (z) {
                sharedPreferences.edit().putString("user_agent", string).apply();
                jm.a("Persisting user agent.");
            }
        }
        return string;
    }
}
