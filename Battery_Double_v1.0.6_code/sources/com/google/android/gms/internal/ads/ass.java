package com.google.android.gms.internal.ads;

import android.text.TextUtils;

final class ass extends asp {
    ass() {
    }

    private static String a(String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        int i = 0;
        int length = str.length();
        while (i < str.length() && str.charAt(i) == ',') {
            i++;
        }
        while (length > 0 && str.charAt(length - 1) == ',') {
            length--;
        }
        if (length < i) {
            return null;
        }
        return (i == 0 && length == str.length()) ? str : str.substring(i, length);
    }

    public final String a(String str, String str2) {
        String a2 = a(str);
        String a3 = a(str2);
        return TextUtils.isEmpty(a2) ? a3 : TextUtils.isEmpty(a3) ? a2 : new StringBuilder(String.valueOf(a2).length() + 1 + String.valueOf(a3).length()).append(a2).append(",").append(a3).toString();
    }
}
