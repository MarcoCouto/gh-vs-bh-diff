package com.google.android.gms.internal.ads;

final /* synthetic */ class hh implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final hf f3364a;

    /* renamed from: b reason: collision with root package name */
    private final ir f3365b;

    hh(hf hfVar, ir irVar) {
        this.f3364a = hfVar;
        this.f3365b = irVar;
    }

    public final void run() {
        this.f3364a.a(this.f3365b);
    }
}
