package com.google.android.gms.internal.ads;

import android.net.Uri;
import com.google.android.gms.ads.internal.ax;
import com.google.android.gms.ads.internal.gmsg.ae;
import com.google.android.gms.common.util.o;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

@cm
public abstract class azt<ReferenceT> {

    /* renamed from: a reason: collision with root package name */
    private final Map<String, CopyOnWriteArrayList<ae<? super ReferenceT>>> f3044a = new HashMap();

    private final synchronized void a(String str, Map<String, String> map) {
        if (jm.a(2)) {
            String str2 = "Received GMSG: ";
            String valueOf = String.valueOf(str);
            jm.a(valueOf.length() != 0 ? str2.concat(valueOf) : new String(str2));
            for (String str3 : map.keySet()) {
                String str4 = (String) map.get(str3);
                jm.a(new StringBuilder(String.valueOf(str3).length() + 4 + String.valueOf(str4).length()).append("  ").append(str3).append(": ").append(str4).toString());
            }
        }
        CopyOnWriteArrayList copyOnWriteArrayList = (CopyOnWriteArrayList) this.f3044a.get(str);
        if (copyOnWriteArrayList != null) {
            Iterator it = copyOnWriteArrayList.iterator();
            while (it.hasNext()) {
                nt.f3558a.execute(new azu(this, (ae) it.next(), map));
            }
        }
    }

    public final synchronized void a(String str, ae<? super ReferenceT> aeVar) {
        CopyOnWriteArrayList copyOnWriteArrayList = (CopyOnWriteArrayList) this.f3044a.get(str);
        if (copyOnWriteArrayList == null) {
            copyOnWriteArrayList = new CopyOnWriteArrayList();
            this.f3044a.put(str, copyOnWriteArrayList);
        }
        copyOnWriteArrayList.add(aeVar);
    }

    public final synchronized void a(String str, o<ae<? super ReferenceT>> oVar) {
        CopyOnWriteArrayList copyOnWriteArrayList = (CopyOnWriteArrayList) this.f3044a.get(str);
        if (copyOnWriteArrayList != null) {
            ArrayList arrayList = new ArrayList();
            Iterator it = copyOnWriteArrayList.iterator();
            while (it.hasNext()) {
                ae aeVar = (ae) it.next();
                if (oVar.a(aeVar)) {
                    arrayList.add(aeVar);
                }
            }
            copyOnWriteArrayList.removeAll(arrayList);
        }
    }

    public final boolean a(Uri uri) {
        if (!"gmsg".equalsIgnoreCase(uri.getScheme()) || !"mobileads.google.com".equalsIgnoreCase(uri.getHost())) {
            return false;
        }
        String path = uri.getPath();
        ax.e();
        a(path, jv.a(uri));
        return true;
    }

    public final synchronized void b(String str, ae<? super ReferenceT> aeVar) {
        CopyOnWriteArrayList copyOnWriteArrayList = (CopyOnWriteArrayList) this.f3044a.get(str);
        if (copyOnWriteArrayList != null) {
            copyOnWriteArrayList.remove(aeVar);
        }
    }

    public synchronized void k() {
        this.f3044a.clear();
    }

    public abstract ReferenceT o();
}
