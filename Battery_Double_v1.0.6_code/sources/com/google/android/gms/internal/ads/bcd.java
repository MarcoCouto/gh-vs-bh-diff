package com.google.android.gms.internal.ads;

public final class bcd<T> {

    /* renamed from: a reason: collision with root package name */
    public final T f3123a;

    /* renamed from: b reason: collision with root package name */
    public final agy f3124b;
    public final df c;
    public boolean d;

    private bcd(df dfVar) {
        this.d = false;
        this.f3123a = null;
        this.f3124b = null;
        this.c = dfVar;
    }

    private bcd(T t, agy agy) {
        this.d = false;
        this.f3123a = t;
        this.f3124b = agy;
        this.c = null;
    }

    public static <T> bcd<T> a(df dfVar) {
        return new bcd<>(dfVar);
    }

    public static <T> bcd<T> a(T t, agy agy) {
        return new bcd<>(t, agy);
    }
}
