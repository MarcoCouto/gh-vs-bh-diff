package com.google.android.gms.internal.ads;

import java.io.IOException;

public final class zz extends afh<zz> {
    public Long A;
    public Long B;
    public String C;
    public String D;
    public Integer E;
    public Integer F;
    public Long G;
    public Long H;
    public Long I;
    public Integer J;
    public aba K;
    public aba[] L;
    public acb M;
    public Long N;
    public Long O;
    public Long P;
    public Long Q;
    public Long R;
    public String S;
    public String T;
    public Integer U;
    public Boolean V;
    public Long W;
    public afe X;

    /* renamed from: a reason: collision with root package name */
    public String f3830a;
    private Long aa;
    private Long ab;
    private Long ac;
    private Long ad;
    private Long ae;
    private Long af;
    private String ag;
    private Long ah;
    private Long ai;
    private adc aj;
    private Long ak;
    private Long al;
    private Long am;
    private Long an;
    private Integer ao;
    private Integer ap;
    private Integer aq;
    private Long ar;
    private String as;

    /* renamed from: b reason: collision with root package name */
    public String f3831b;
    public Long c;
    public Long d;
    public Long e;
    public Long f;
    public Long g;
    public Long h;
    public Long i;
    public Long j;
    public Long k;
    public Long l;
    public Long m;
    public String n;
    public String o;
    public Long p;
    public Long q;
    public Long r;
    public String s;
    public Long t;
    public Long u;
    public Long v;
    public Long w;
    public Long x;
    public Long y;
    public Long z;

    public zz() {
        this.f3830a = null;
        this.f3831b = null;
        this.c = null;
        this.aa = null;
        this.d = null;
        this.e = null;
        this.ab = null;
        this.ac = null;
        this.ad = null;
        this.ae = null;
        this.af = null;
        this.f = null;
        this.ag = null;
        this.g = null;
        this.h = null;
        this.i = null;
        this.j = null;
        this.ah = null;
        this.ai = null;
        this.k = null;
        this.l = null;
        this.m = null;
        this.n = null;
        this.o = null;
        this.p = null;
        this.q = null;
        this.r = null;
        this.s = null;
        this.t = null;
        this.u = null;
        this.v = null;
        this.aj = null;
        this.w = null;
        this.x = null;
        this.y = null;
        this.z = null;
        this.A = null;
        this.B = null;
        this.C = null;
        this.D = null;
        this.G = null;
        this.H = null;
        this.I = null;
        this.ak = null;
        this.al = null;
        this.K = null;
        this.L = aba.b();
        this.M = null;
        this.am = null;
        this.N = null;
        this.O = null;
        this.P = null;
        this.Q = null;
        this.R = null;
        this.S = null;
        this.an = null;
        this.ar = null;
        this.T = null;
        this.V = null;
        this.as = null;
        this.W = null;
        this.X = null;
        this.Z = -1;
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public final zz a(afd afd) throws IOException {
        while (true) {
            int a2 = afd.a();
            switch (a2) {
                case 0:
                    break;
                case 10:
                    this.f3830a = afd.e();
                    continue;
                case 18:
                    this.f3831b = afd.e();
                    continue;
                case 24:
                    this.c = Long.valueOf(afd.h());
                    continue;
                case 32:
                    this.aa = Long.valueOf(afd.h());
                    continue;
                case 40:
                    this.d = Long.valueOf(afd.h());
                    continue;
                case 48:
                    this.e = Long.valueOf(afd.h());
                    continue;
                case 56:
                    this.ab = Long.valueOf(afd.h());
                    continue;
                case 64:
                    this.ac = Long.valueOf(afd.h());
                    continue;
                case 72:
                    this.ad = Long.valueOf(afd.h());
                    continue;
                case 80:
                    this.ae = Long.valueOf(afd.h());
                    continue;
                case 88:
                    this.af = Long.valueOf(afd.h());
                    continue;
                case 96:
                    this.f = Long.valueOf(afd.h());
                    continue;
                case 106:
                    this.ag = afd.e();
                    continue;
                case 112:
                    this.g = Long.valueOf(afd.h());
                    continue;
                case 120:
                    this.h = Long.valueOf(afd.h());
                    continue;
                case 128:
                    this.i = Long.valueOf(afd.h());
                    continue;
                case 136:
                    this.j = Long.valueOf(afd.h());
                    continue;
                case 144:
                    this.ah = Long.valueOf(afd.h());
                    continue;
                case 152:
                    this.ai = Long.valueOf(afd.h());
                    continue;
                case 160:
                    this.k = Long.valueOf(afd.h());
                    continue;
                case 168:
                    this.ar = Long.valueOf(afd.h());
                    continue;
                case 176:
                    this.l = Long.valueOf(afd.h());
                    continue;
                case 184:
                    this.m = Long.valueOf(afd.h());
                    continue;
                case 194:
                    this.T = afd.e();
                    continue;
                case 200:
                    this.W = Long.valueOf(afd.h());
                    continue;
                case 208:
                    int j2 = afd.j();
                    try {
                        int g2 = afd.g();
                        if (g2 < 0 || g2 > 6) {
                            throw new IllegalArgumentException(g2 + " is not a valid enum DeviceIdType");
                        }
                        this.U = Integer.valueOf(g2);
                        continue;
                    } catch (IllegalArgumentException e2) {
                        afd.e(j2);
                        a(afd, a2);
                        break;
                    }
                case 218:
                    this.n = afd.e();
                    continue;
                case 224:
                    this.V = Boolean.valueOf(afd.d());
                    continue;
                case 234:
                    this.o = afd.e();
                    continue;
                case 242:
                    this.as = afd.e();
                    continue;
                case 248:
                    this.p = Long.valueOf(afd.h());
                    continue;
                case 256:
                    this.q = Long.valueOf(afd.h());
                    continue;
                case 264:
                    this.r = Long.valueOf(afd.h());
                    continue;
                case 274:
                    this.s = afd.e();
                    continue;
                case 280:
                    this.t = Long.valueOf(afd.h());
                    continue;
                case 288:
                    this.u = Long.valueOf(afd.h());
                    continue;
                case 296:
                    this.v = Long.valueOf(afd.h());
                    continue;
                case 306:
                    if (this.aj == null) {
                        this.aj = new adc();
                    }
                    afd.a((afn) this.aj);
                    continue;
                case 312:
                    this.w = Long.valueOf(afd.h());
                    continue;
                case 320:
                    this.x = Long.valueOf(afd.h());
                    continue;
                case 328:
                    this.y = Long.valueOf(afd.h());
                    continue;
                case 336:
                    this.z = Long.valueOf(afd.h());
                    continue;
                case 346:
                    int a3 = afq.a(afd, 346);
                    int length = this.L == null ? 0 : this.L.length;
                    aba[] abaArr = new aba[(a3 + length)];
                    if (length != 0) {
                        System.arraycopy(this.L, 0, abaArr, 0, length);
                    }
                    while (length < abaArr.length - 1) {
                        abaArr[length] = new aba();
                        afd.a((afn) abaArr[length]);
                        afd.a();
                        length++;
                    }
                    abaArr[length] = new aba();
                    afd.a((afn) abaArr[length]);
                    this.L = abaArr;
                    continue;
                case 352:
                    this.A = Long.valueOf(afd.h());
                    continue;
                case 360:
                    this.B = Long.valueOf(afd.h());
                    continue;
                case 370:
                    this.C = afd.e();
                    continue;
                case 378:
                    this.D = afd.e();
                    continue;
                case 384:
                    int j3 = afd.j();
                    try {
                        this.E = Integer.valueOf(yx.a(afd.g()));
                        continue;
                    } catch (IllegalArgumentException e3) {
                        afd.e(j3);
                        a(afd, a2);
                        break;
                    }
                case 392:
                    int j4 = afd.j();
                    try {
                        this.F = Integer.valueOf(yx.a(afd.g()));
                        continue;
                    } catch (IllegalArgumentException e4) {
                        afd.e(j4);
                        a(afd, a2);
                        break;
                    }
                case 402:
                    if (this.K == null) {
                        this.K = new aba();
                    }
                    afd.a((afn) this.K);
                    continue;
                case 408:
                    this.G = Long.valueOf(afd.h());
                    continue;
                case 416:
                    this.H = Long.valueOf(afd.h());
                    continue;
                case 424:
                    this.I = Long.valueOf(afd.h());
                    continue;
                case 432:
                    this.ak = Long.valueOf(afd.h());
                    continue;
                case 440:
                    this.al = Long.valueOf(afd.h());
                    continue;
                case 448:
                    int j5 = afd.j();
                    try {
                        this.J = Integer.valueOf(yx.a(afd.g()));
                        continue;
                    } catch (IllegalArgumentException e5) {
                        afd.e(j5);
                        a(afd, a2);
                        break;
                    }
                case 458:
                    if (this.M == null) {
                        this.M = new acb();
                    }
                    afd.a((afn) this.M);
                    continue;
                case 464:
                    this.am = Long.valueOf(afd.h());
                    continue;
                case 472:
                    this.N = Long.valueOf(afd.h());
                    continue;
                case 480:
                    this.O = Long.valueOf(afd.h());
                    continue;
                case 488:
                    this.P = Long.valueOf(afd.h());
                    continue;
                case 496:
                    this.Q = Long.valueOf(afd.h());
                    continue;
                case 504:
                    this.R = Long.valueOf(afd.h());
                    continue;
                case 512:
                    this.an = Long.valueOf(afd.h());
                    continue;
                case 520:
                    int j6 = afd.j();
                    try {
                        this.ao = Integer.valueOf(yx.c(afd.g()));
                        continue;
                    } catch (IllegalArgumentException e6) {
                        afd.e(j6);
                        a(afd, a2);
                        break;
                    }
                case 528:
                    int j7 = afd.j();
                    try {
                        this.ap = Integer.valueOf(yx.b(afd.g()));
                        continue;
                    } catch (IllegalArgumentException e7) {
                        afd.e(j7);
                        a(afd, a2);
                        break;
                    }
                case 538:
                    this.S = afd.e();
                    continue;
                case 544:
                    int j8 = afd.j();
                    try {
                        int g3 = afd.g();
                        if (g3 < 0 || g3 > 3) {
                            throw new IllegalArgumentException(g3 + " is not a valid enum DebuggerState");
                        }
                        this.aq = Integer.valueOf(g3);
                        continue;
                    } catch (IllegalArgumentException e8) {
                        afd.e(j8);
                        a(afd, a2);
                        break;
                    }
                case 1610:
                    if (this.X == null) {
                        this.X = new afe();
                    }
                    afd.a((afn) this.X);
                    continue;
                default:
                    if (!super.a(afd, a2)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    /* access modifiers changed from: protected */
    public final int a() {
        int a2 = super.a();
        if (this.f3830a != null) {
            a2 += aff.b(1, this.f3830a);
        }
        if (this.f3831b != null) {
            a2 += aff.b(2, this.f3831b);
        }
        if (this.c != null) {
            a2 += aff.d(3, this.c.longValue());
        }
        if (this.aa != null) {
            a2 += aff.d(4, this.aa.longValue());
        }
        if (this.d != null) {
            a2 += aff.d(5, this.d.longValue());
        }
        if (this.e != null) {
            a2 += aff.d(6, this.e.longValue());
        }
        if (this.ab != null) {
            a2 += aff.d(7, this.ab.longValue());
        }
        if (this.ac != null) {
            a2 += aff.d(8, this.ac.longValue());
        }
        if (this.ad != null) {
            a2 += aff.d(9, this.ad.longValue());
        }
        if (this.ae != null) {
            a2 += aff.d(10, this.ae.longValue());
        }
        if (this.af != null) {
            a2 += aff.d(11, this.af.longValue());
        }
        if (this.f != null) {
            a2 += aff.d(12, this.f.longValue());
        }
        if (this.ag != null) {
            a2 += aff.b(13, this.ag);
        }
        if (this.g != null) {
            a2 += aff.d(14, this.g.longValue());
        }
        if (this.h != null) {
            a2 += aff.d(15, this.h.longValue());
        }
        if (this.i != null) {
            a2 += aff.d(16, this.i.longValue());
        }
        if (this.j != null) {
            a2 += aff.d(17, this.j.longValue());
        }
        if (this.ah != null) {
            a2 += aff.d(18, this.ah.longValue());
        }
        if (this.ai != null) {
            a2 += aff.d(19, this.ai.longValue());
        }
        if (this.k != null) {
            a2 += aff.d(20, this.k.longValue());
        }
        if (this.ar != null) {
            a2 += aff.d(21, this.ar.longValue());
        }
        if (this.l != null) {
            a2 += aff.d(22, this.l.longValue());
        }
        if (this.m != null) {
            a2 += aff.d(23, this.m.longValue());
        }
        if (this.T != null) {
            a2 += aff.b(24, this.T);
        }
        if (this.W != null) {
            a2 += aff.d(25, this.W.longValue());
        }
        if (this.U != null) {
            a2 += aff.b(26, this.U.intValue());
        }
        if (this.n != null) {
            a2 += aff.b(27, this.n);
        }
        if (this.V != null) {
            this.V.booleanValue();
            a2 += aff.b(28) + 1;
        }
        if (this.o != null) {
            a2 += aff.b(29, this.o);
        }
        if (this.as != null) {
            a2 += aff.b(30, this.as);
        }
        if (this.p != null) {
            a2 += aff.d(31, this.p.longValue());
        }
        if (this.q != null) {
            a2 += aff.d(32, this.q.longValue());
        }
        if (this.r != null) {
            a2 += aff.d(33, this.r.longValue());
        }
        if (this.s != null) {
            a2 += aff.b(34, this.s);
        }
        if (this.t != null) {
            a2 += aff.d(35, this.t.longValue());
        }
        if (this.u != null) {
            a2 += aff.d(36, this.u.longValue());
        }
        if (this.v != null) {
            a2 += aff.d(37, this.v.longValue());
        }
        if (this.aj != null) {
            a2 += aff.b(38, (afn) this.aj);
        }
        if (this.w != null) {
            a2 += aff.d(39, this.w.longValue());
        }
        if (this.x != null) {
            a2 += aff.d(40, this.x.longValue());
        }
        if (this.y != null) {
            a2 += aff.d(41, this.y.longValue());
        }
        if (this.z != null) {
            a2 += aff.d(42, this.z.longValue());
        }
        if (this.L != null && this.L.length > 0) {
            int i2 = a2;
            for (aba aba : this.L) {
                if (aba != null) {
                    i2 += aff.b(43, (afn) aba);
                }
            }
            a2 = i2;
        }
        if (this.A != null) {
            a2 += aff.d(44, this.A.longValue());
        }
        if (this.B != null) {
            a2 += aff.d(45, this.B.longValue());
        }
        if (this.C != null) {
            a2 += aff.b(46, this.C);
        }
        if (this.D != null) {
            a2 += aff.b(47, this.D);
        }
        if (this.E != null) {
            a2 += aff.b(48, this.E.intValue());
        }
        if (this.F != null) {
            a2 += aff.b(49, this.F.intValue());
        }
        if (this.K != null) {
            a2 += aff.b(50, (afn) this.K);
        }
        if (this.G != null) {
            a2 += aff.d(51, this.G.longValue());
        }
        if (this.H != null) {
            a2 += aff.d(52, this.H.longValue());
        }
        if (this.I != null) {
            a2 += aff.d(53, this.I.longValue());
        }
        if (this.ak != null) {
            a2 += aff.d(54, this.ak.longValue());
        }
        if (this.al != null) {
            a2 += aff.d(55, this.al.longValue());
        }
        if (this.J != null) {
            a2 += aff.b(56, this.J.intValue());
        }
        if (this.M != null) {
            a2 += aff.b(57, (afn) this.M);
        }
        if (this.am != null) {
            a2 += aff.d(58, this.am.longValue());
        }
        if (this.N != null) {
            a2 += aff.d(59, this.N.longValue());
        }
        if (this.O != null) {
            a2 += aff.d(60, this.O.longValue());
        }
        if (this.P != null) {
            a2 += aff.d(61, this.P.longValue());
        }
        if (this.Q != null) {
            a2 += aff.d(62, this.Q.longValue());
        }
        if (this.R != null) {
            a2 += aff.d(63, this.R.longValue());
        }
        if (this.an != null) {
            a2 += aff.d(64, this.an.longValue());
        }
        if (this.ao != null) {
            a2 += aff.b(65, this.ao.intValue());
        }
        if (this.ap != null) {
            a2 += aff.b(66, this.ap.intValue());
        }
        if (this.S != null) {
            a2 += aff.b(67, this.S);
        }
        if (this.aq != null) {
            a2 += aff.b(68, this.aq.intValue());
        }
        return this.X != null ? a2 + aff.b(201, (afn) this.X) : a2;
    }

    public final void a(aff aff) throws IOException {
        if (this.f3830a != null) {
            aff.a(1, this.f3830a);
        }
        if (this.f3831b != null) {
            aff.a(2, this.f3831b);
        }
        if (this.c != null) {
            aff.b(3, this.c.longValue());
        }
        if (this.aa != null) {
            aff.b(4, this.aa.longValue());
        }
        if (this.d != null) {
            aff.b(5, this.d.longValue());
        }
        if (this.e != null) {
            aff.b(6, this.e.longValue());
        }
        if (this.ab != null) {
            aff.b(7, this.ab.longValue());
        }
        if (this.ac != null) {
            aff.b(8, this.ac.longValue());
        }
        if (this.ad != null) {
            aff.b(9, this.ad.longValue());
        }
        if (this.ae != null) {
            aff.b(10, this.ae.longValue());
        }
        if (this.af != null) {
            aff.b(11, this.af.longValue());
        }
        if (this.f != null) {
            aff.b(12, this.f.longValue());
        }
        if (this.ag != null) {
            aff.a(13, this.ag);
        }
        if (this.g != null) {
            aff.b(14, this.g.longValue());
        }
        if (this.h != null) {
            aff.b(15, this.h.longValue());
        }
        if (this.i != null) {
            aff.b(16, this.i.longValue());
        }
        if (this.j != null) {
            aff.b(17, this.j.longValue());
        }
        if (this.ah != null) {
            aff.b(18, this.ah.longValue());
        }
        if (this.ai != null) {
            aff.b(19, this.ai.longValue());
        }
        if (this.k != null) {
            aff.b(20, this.k.longValue());
        }
        if (this.ar != null) {
            aff.b(21, this.ar.longValue());
        }
        if (this.l != null) {
            aff.b(22, this.l.longValue());
        }
        if (this.m != null) {
            aff.b(23, this.m.longValue());
        }
        if (this.T != null) {
            aff.a(24, this.T);
        }
        if (this.W != null) {
            aff.b(25, this.W.longValue());
        }
        if (this.U != null) {
            aff.a(26, this.U.intValue());
        }
        if (this.n != null) {
            aff.a(27, this.n);
        }
        if (this.V != null) {
            aff.a(28, this.V.booleanValue());
        }
        if (this.o != null) {
            aff.a(29, this.o);
        }
        if (this.as != null) {
            aff.a(30, this.as);
        }
        if (this.p != null) {
            aff.b(31, this.p.longValue());
        }
        if (this.q != null) {
            aff.b(32, this.q.longValue());
        }
        if (this.r != null) {
            aff.b(33, this.r.longValue());
        }
        if (this.s != null) {
            aff.a(34, this.s);
        }
        if (this.t != null) {
            aff.b(35, this.t.longValue());
        }
        if (this.u != null) {
            aff.b(36, this.u.longValue());
        }
        if (this.v != null) {
            aff.b(37, this.v.longValue());
        }
        if (this.aj != null) {
            aff.a(38, (afn) this.aj);
        }
        if (this.w != null) {
            aff.b(39, this.w.longValue());
        }
        if (this.x != null) {
            aff.b(40, this.x.longValue());
        }
        if (this.y != null) {
            aff.b(41, this.y.longValue());
        }
        if (this.z != null) {
            aff.b(42, this.z.longValue());
        }
        if (this.L != null && this.L.length > 0) {
            for (aba aba : this.L) {
                if (aba != null) {
                    aff.a(43, (afn) aba);
                }
            }
        }
        if (this.A != null) {
            aff.b(44, this.A.longValue());
        }
        if (this.B != null) {
            aff.b(45, this.B.longValue());
        }
        if (this.C != null) {
            aff.a(46, this.C);
        }
        if (this.D != null) {
            aff.a(47, this.D);
        }
        if (this.E != null) {
            aff.a(48, this.E.intValue());
        }
        if (this.F != null) {
            aff.a(49, this.F.intValue());
        }
        if (this.K != null) {
            aff.a(50, (afn) this.K);
        }
        if (this.G != null) {
            aff.b(51, this.G.longValue());
        }
        if (this.H != null) {
            aff.b(52, this.H.longValue());
        }
        if (this.I != null) {
            aff.b(53, this.I.longValue());
        }
        if (this.ak != null) {
            aff.b(54, this.ak.longValue());
        }
        if (this.al != null) {
            aff.b(55, this.al.longValue());
        }
        if (this.J != null) {
            aff.a(56, this.J.intValue());
        }
        if (this.M != null) {
            aff.a(57, (afn) this.M);
        }
        if (this.am != null) {
            aff.b(58, this.am.longValue());
        }
        if (this.N != null) {
            aff.b(59, this.N.longValue());
        }
        if (this.O != null) {
            aff.b(60, this.O.longValue());
        }
        if (this.P != null) {
            aff.b(61, this.P.longValue());
        }
        if (this.Q != null) {
            aff.b(62, this.Q.longValue());
        }
        if (this.R != null) {
            aff.b(63, this.R.longValue());
        }
        if (this.an != null) {
            aff.b(64, this.an.longValue());
        }
        if (this.ao != null) {
            aff.a(65, this.ao.intValue());
        }
        if (this.ap != null) {
            aff.a(66, this.ap.intValue());
        }
        if (this.S != null) {
            aff.a(67, this.S);
        }
        if (this.aq != null) {
            aff.a(68, this.aq.intValue());
        }
        if (this.X != null) {
            aff.a(201, (afn) this.X);
        }
        super.a(aff);
    }
}
