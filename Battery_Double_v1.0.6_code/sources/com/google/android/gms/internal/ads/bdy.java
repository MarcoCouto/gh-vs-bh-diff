package com.google.android.gms.internal.ads;

import android.os.RemoteException;
import com.google.ads.a.C0043a;

final class bdy implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ C0043a f3162a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ bdv f3163b;

    bdy(bdv bdv, C0043a aVar) {
        this.f3163b = bdv;
        this.f3162a = aVar;
    }

    public final void run() {
        try {
            this.f3163b.f3159a.a(bdz.a(this.f3162a));
        } catch (RemoteException e) {
            ms.d("#007 Could not call remote method.", e);
        }
    }
}
