package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.xe.b;
import java.security.GeneralSecurityException;
import javax.crypto.spec.SecretKeySpec;

final class ve implements tz<ud> {
    ve() {
    }

    private static void a(xc xcVar) throws GeneralSecurityException {
        if (xcVar.b() < 10) {
            throw new GeneralSecurityException("tag size too small");
        }
        switch (vf.f3739a[xcVar.a().ordinal()]) {
            case 1:
                if (xcVar.b() > 20) {
                    throw new GeneralSecurityException("tag size too big");
                }
                return;
            case 2:
                if (xcVar.b() > 32) {
                    throw new GeneralSecurityException("tag size too big");
                }
                return;
            case 3:
                if (xcVar.b() > 64) {
                    throw new GeneralSecurityException("tag size too big");
                }
                return;
            default:
                throw new GeneralSecurityException("unknown hash type");
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: d */
    public final ud a(aah aah) throws GeneralSecurityException {
        zh zhVar;
        try {
            wy a2 = wy.a(aah);
            if (!(a2 instanceof wy)) {
                throw new GeneralSecurityException("expected HmacKey proto");
            }
            wy wyVar = a2;
            zo.a(wyVar.a(), 0);
            if (wyVar.c().a() < 16) {
                throw new GeneralSecurityException("key too short");
            }
            a(wyVar.b());
            ww a3 = wyVar.b().a();
            SecretKeySpec secretKeySpec = new SecretKeySpec(wyVar.c().b(), "HMAC");
            int b2 = wyVar.b().b();
            switch (vf.f3739a[a3.ordinal()]) {
                case 1:
                    zhVar = new zh("HMACSHA1", secretKeySpec, b2);
                    break;
                case 2:
                    zhVar = new zh("HMACSHA256", secretKeySpec, b2);
                    break;
                case 3:
                    zhVar = new zh("HMACSHA512", secretKeySpec, b2);
                    break;
                default:
                    throw new GeneralSecurityException("unknown hash");
            }
            return zhVar;
        } catch (abv e) {
            throw new GeneralSecurityException("expected serialized HmacKey proto", e);
        }
    }

    public final int a() {
        return 0;
    }

    public final /* synthetic */ Object a(acw acw) throws GeneralSecurityException {
        if (!(acw instanceof wy)) {
            throw new GeneralSecurityException("expected HmacKey proto");
        }
        wy wyVar = (wy) acw;
        zo.a(wyVar.a(), 0);
        if (wyVar.c().a() < 16) {
            throw new GeneralSecurityException("key too short");
        }
        a(wyVar.b());
        ww a2 = wyVar.b().a();
        SecretKeySpec secretKeySpec = new SecretKeySpec(wyVar.c().b(), "HMAC");
        int b2 = wyVar.b().b();
        switch (vf.f3739a[a2.ordinal()]) {
            case 1:
                return new zh("HMACSHA1", secretKeySpec, b2);
            case 2:
                return new zh("HMACSHA256", secretKeySpec, b2);
            case 3:
                return new zh("HMACSHA512", secretKeySpec, b2);
            default:
                throw new GeneralSecurityException("unknown hash");
        }
    }

    public final acw b(aah aah) throws GeneralSecurityException {
        try {
            return b((acw) xa.a(aah));
        } catch (abv e) {
            throw new GeneralSecurityException("expected serialized HmacKeyFormat proto", e);
        }
    }

    public final acw b(acw acw) throws GeneralSecurityException {
        if (!(acw instanceof xa)) {
            throw new GeneralSecurityException("expected HmacKeyFormat proto");
        }
        xa xaVar = (xa) acw;
        if (xaVar.b() < 16) {
            throw new GeneralSecurityException("key too short");
        }
        a(xaVar.a());
        return wy.d().a(0).a(xaVar.a()).a(aah.a(zj.a(xaVar.b()))).c();
    }

    public final xe c(aah aah) throws GeneralSecurityException {
        return (xe) xe.d().a("type.googleapis.com/google.crypto.tink.HmacKey").a(((wy) b(aah)).h()).a(b.SYMMETRIC).c();
    }
}
