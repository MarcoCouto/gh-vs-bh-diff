package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.yw;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.Provider;
import java.security.Security;
import java.security.Signature;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.Cipher;
import javax.crypto.KeyAgreement;
import javax.crypto.Mac;

public final class yv<T_WRAPPER extends yw<T_ENGINE>, T_ENGINE> {

    /* renamed from: a reason: collision with root package name */
    public static final yv<yy, Cipher> f3809a = new yv<>(new yy());

    /* renamed from: b reason: collision with root package name */
    public static final yv<zc, Mac> f3810b = new yv<>(new zc());
    public static final yv<yz, KeyAgreement> c = new yv<>(new yz());
    public static final yv<zb, KeyPairGenerator> d = new yv<>(new zb());
    public static final yv<za, KeyFactory> e = new yv<>(new za());
    private static final Logger f = Logger.getLogger(yv.class.getName());
    private static final List<Provider> g;
    private static final yv<ze, Signature> h = new yv<>(new ze());
    private static final yv<zd, MessageDigest> i = new yv<>(new zd());
    private T_WRAPPER j;
    private List<Provider> k = g;
    private boolean l = true;

    static {
        if (zn.a()) {
            String[] strArr = {"GmsCore_OpenSSL", "AndroidOpenSSL"};
            ArrayList arrayList = new ArrayList();
            for (int i2 = 0; i2 < 2; i2++) {
                String str = strArr[i2];
                Provider provider = Security.getProvider(str);
                if (provider != null) {
                    arrayList.add(provider);
                } else {
                    f.logp(Level.INFO, "com.google.crypto.tink.subtle.EngineFactory", "toProviderList", String.format("Provider %s not available", new Object[]{str}));
                }
            }
            g = arrayList;
        } else {
            g = new ArrayList();
        }
    }

    private yv(T_WRAPPER t_wrapper) {
        this.j = t_wrapper;
    }

    private final boolean a(String str, Provider provider) {
        try {
            this.j.a(str, provider);
            return true;
        } catch (Exception e2) {
            return false;
        }
    }

    public final T_ENGINE a(String str) throws GeneralSecurityException {
        for (Provider provider : this.k) {
            if (a(str, provider)) {
                return this.j.a(str, provider);
            }
        }
        if (this.l) {
            return this.j.a(str, null);
        }
        throw new GeneralSecurityException("No good Provider found.");
    }
}
