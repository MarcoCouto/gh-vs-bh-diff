package com.google.android.gms.internal.ads;

import java.io.IOException;

public final class afx extends afh<afx> {

    /* renamed from: a reason: collision with root package name */
    private Integer f2547a;

    /* renamed from: b reason: collision with root package name */
    private byte[] f2548b;
    private byte[] c;

    public afx() {
        this.f2547a = null;
        this.f2548b = null;
        this.c = null;
        this.Y = null;
        this.Z = -1;
    }

    /* access modifiers changed from: protected */
    public final int a() {
        int a2 = super.a();
        if (this.f2547a != null) {
            a2 += aff.b(1, this.f2547a.intValue());
        }
        if (this.f2548b != null) {
            a2 += aff.b(2, this.f2548b);
        }
        return this.c != null ? a2 + aff.b(3, this.c) : a2;
    }

    public final /* synthetic */ afn a(afd afd) throws IOException {
        while (true) {
            int a2 = afd.a();
            switch (a2) {
                case 0:
                    break;
                case 8:
                    this.f2547a = Integer.valueOf(afd.c());
                    continue;
                case 18:
                    this.f2548b = afd.f();
                    continue;
                case 26:
                    this.c = afd.f();
                    continue;
                default:
                    if (!super.a(afd, a2)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    public final void a(aff aff) throws IOException {
        if (this.f2547a != null) {
            aff.a(1, this.f2547a.intValue());
        }
        if (this.f2548b != null) {
            aff.a(2, this.f2548b);
        }
        if (this.c != null) {
            aff.a(3, this.c);
        }
        super.a(aff);
    }
}
