package com.google.android.gms.internal.ads;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import org.json.JSONObject;

@cm
public abstract class ary<T> {

    /* renamed from: a reason: collision with root package name */
    private final int f2865a;

    /* renamed from: b reason: collision with root package name */
    private final String f2866b;
    private final T c;

    private ary(int i, String str, T t) {
        this.f2865a = i;
        this.f2866b = str;
        this.c = t;
        ape.e().a(this);
    }

    /* synthetic */ ary(int i, String str, Object obj, arz arz) {
        this(i, str, obj);
    }

    public static ary<String> a(int i, String str) {
        ary<String> a2 = a(i, str, (String) null);
        ape.e().b(a2);
        return a2;
    }

    public static ary<Float> a(int i, String str, float f) {
        return new asc(i, str, Float.valueOf(f));
    }

    public static ary<Integer> a(int i, String str, int i2) {
        return new asa(i, str, Integer.valueOf(i2));
    }

    public static ary<Long> a(int i, String str, long j) {
        return new asb(i, str, Long.valueOf(j));
    }

    public static ary<Boolean> a(int i, String str, Boolean bool) {
        return new arz(i, str, bool);
    }

    public static ary<String> a(int i, String str, String str2) {
        return new asd(i, str, str2);
    }

    public static ary<String> b(int i, String str) {
        ary<String> a2 = a(i, str, (String) null);
        ape.e().c(a2);
        return a2;
    }

    /* access modifiers changed from: protected */
    public abstract T a(SharedPreferences sharedPreferences);

    /* access modifiers changed from: protected */
    public abstract T a(JSONObject jSONObject);

    public final String a() {
        return this.f2866b;
    }

    public abstract void a(Editor editor, T t);

    public final T b() {
        return this.c;
    }

    public final int c() {
        return this.f2865a;
    }
}
