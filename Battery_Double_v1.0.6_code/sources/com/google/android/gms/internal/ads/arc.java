package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.RemoteException;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import com.google.android.gms.ads.a;
import com.google.android.gms.ads.a.c;
import com.google.android.gms.ads.d;
import com.google.android.gms.ads.f;
import com.google.android.gms.ads.i;
import com.google.android.gms.ads.j;
import com.google.android.gms.b.b;
import java.util.concurrent.atomic.AtomicBoolean;

@cm
public final class arc {

    /* renamed from: a reason: collision with root package name */
    private final bcq f2848a;

    /* renamed from: b reason: collision with root package name */
    private final aos f2849b;
    private final AtomicBoolean c;
    /* access modifiers changed from: private */
    public final i d;
    private final apg e;
    private aoj f;
    private a g;
    private d[] h;
    private com.google.android.gms.ads.a.a i;
    private f j;
    private apv k;
    private c l;
    private j m;
    private String n;
    private ViewGroup o;
    private int p;
    private boolean q;

    public arc(ViewGroup viewGroup, int i2) {
        this(viewGroup, null, false, aos.f2813a, i2);
    }

    public arc(ViewGroup viewGroup, AttributeSet attributeSet, boolean z, int i2) {
        this(viewGroup, attributeSet, false, aos.f2813a, i2);
    }

    private arc(ViewGroup viewGroup, AttributeSet attributeSet, boolean z, aos aos, int i2) {
        this(viewGroup, attributeSet, z, aos, null, i2);
    }

    private arc(ViewGroup viewGroup, AttributeSet attributeSet, boolean z, aos aos, apv apv, int i2) {
        this.f2848a = new bcq();
        this.d = new i();
        this.e = new ard(this);
        this.o = viewGroup;
        this.f2849b = aos;
        this.k = null;
        this.c = new AtomicBoolean(false);
        this.p = i2;
        if (attributeSet != null) {
            Context context = viewGroup.getContext();
            try {
                aow aow = new aow(context, attributeSet);
                this.h = aow.a(z);
                this.n = aow.a();
                if (viewGroup.isInEditMode()) {
                    mh a2 = ape.a();
                    d dVar = this.h[0];
                    int i3 = this.p;
                    aot aot = new aot(context, dVar);
                    aot.j = a(i3);
                    a2.a(viewGroup, aot, "Ads by Google");
                }
            } catch (IllegalArgumentException e2) {
                ape.a().a(viewGroup, new aot(context, d.f1937a), e2.getMessage(), e2.getMessage());
            }
        }
    }

    private static aot a(Context context, d[] dVarArr, int i2) {
        aot aot = new aot(context, dVarArr);
        aot.j = a(i2);
        return aot;
    }

    private static boolean a(int i2) {
        return i2 == 1;
    }

    public final void a() {
        try {
            if (this.k != null) {
                this.k.j();
            }
        } catch (RemoteException e2) {
            ms.d("#007 Could not call remote method.", e2);
        }
    }

    public final void a(com.google.android.gms.ads.a.a aVar) {
        try {
            this.i = aVar;
            if (this.k != null) {
                this.k.a((aqe) aVar != null ? new aov(aVar) : null);
            }
        } catch (RemoteException e2) {
            ms.d("#007 Could not call remote method.", e2);
        }
    }

    public final void a(c cVar) {
        this.l = cVar;
        try {
            if (this.k != null) {
                this.k.a((atc) cVar != null ? new atf(cVar) : null);
            }
        } catch (RemoteException e2) {
            ms.d("#007 Could not call remote method.", e2);
        }
    }

    public final void a(a aVar) {
        this.g = aVar;
        this.e.a(aVar);
    }

    public final void a(f fVar) {
        this.j = fVar;
        try {
            if (this.k != null) {
                this.k.a((aqk) this.j == null ? null : this.j.a());
            }
        } catch (RemoteException e2) {
            ms.d("#007 Could not call remote method.", e2);
        }
    }

    public final void a(j jVar) {
        this.m = jVar;
        try {
            if (this.k != null) {
                this.k.a(jVar == null ? null : new arr(jVar));
            }
        } catch (RemoteException e2) {
            ms.d("#007 Could not call remote method.", e2);
        }
    }

    public final void a(aoj aoj) {
        try {
            this.f = aoj;
            if (this.k != null) {
                this.k.a((aph) aoj != null ? new aok(aoj) : null);
            }
        } catch (RemoteException e2) {
            ms.d("#007 Could not call remote method.", e2);
        }
    }

    public final void a(ara ara) {
        try {
            if (this.k == null) {
                if ((this.h == null || this.n == null) && this.k == null) {
                    throw new IllegalStateException("The ad size and ad unit ID must be set before loadAd is called.");
                }
                Context context = this.o.getContext();
                aot a2 = a(context, this.h, this.p);
                this.k = "search_v2".equals(a2.f2814a) ? (apv) aox.a(context, false, (a<T>) new aoz<T>(ape.b(), context, a2, this.n)) : (apv) aox.a(context, false, (a<T>) new aoy<T>(ape.b(), context, a2, this.n, this.f2848a));
                this.k.a((apk) new aol(this.e));
                if (this.f != null) {
                    this.k.a((aph) new aok(this.f));
                }
                if (this.i != null) {
                    this.k.a((aqe) new aov(this.i));
                }
                if (this.l != null) {
                    this.k.a((atc) new atf(this.l));
                }
                if (this.j != null) {
                    this.k.a((aqk) this.j.a());
                }
                if (this.m != null) {
                    this.k.a(new arr(this.m));
                }
                this.k.b(this.q);
                try {
                    com.google.android.gms.b.a k2 = this.k.k();
                    if (k2 != null) {
                        this.o.addView((View) b.a(k2));
                    }
                } catch (RemoteException e2) {
                    ms.d("#007 Could not call remote method.", e2);
                }
            }
            if (this.k.b(aos.a(this.o.getContext(), ara))) {
                this.f2848a.a(ara.j());
            }
        } catch (RemoteException e3) {
            ms.d("#007 Could not call remote method.", e3);
        }
    }

    public final void a(String str) {
        if (this.n != null) {
            throw new IllegalStateException("The ad unit ID can only be set once on AdView.");
        }
        this.n = str;
    }

    public final void a(boolean z) {
        this.q = z;
        try {
            if (this.k != null) {
                this.k.b(this.q);
            }
        } catch (RemoteException e2) {
            ms.d("#007 Could not call remote method.", e2);
        }
    }

    public final void a(d... dVarArr) {
        if (this.h != null) {
            throw new IllegalStateException("The ad size can only be set once on AdView.");
        }
        b(dVarArr);
    }

    public final a b() {
        return this.g;
    }

    public final void b(d... dVarArr) {
        this.h = dVarArr;
        try {
            if (this.k != null) {
                this.k.a(a(this.o.getContext(), this.h, this.p));
            }
        } catch (RemoteException e2) {
            ms.d("#007 Could not call remote method.", e2);
        }
        this.o.requestLayout();
    }

    public final d c() {
        try {
            if (this.k != null) {
                aot l2 = this.k.l();
                if (l2 != null) {
                    return l2.b();
                }
            }
        } catch (RemoteException e2) {
            ms.d("#007 Could not call remote method.", e2);
        }
        if (this.h != null) {
            return this.h[0];
        }
        return null;
    }

    public final d[] d() {
        return this.h;
    }

    public final String e() {
        if (this.n == null && this.k != null) {
            try {
                this.n = this.k.D();
            } catch (RemoteException e2) {
                ms.d("#007 Could not call remote method.", e2);
            }
        }
        return this.n;
    }

    public final com.google.android.gms.ads.a.a f() {
        return this.i;
    }

    public final c g() {
        return this.l;
    }

    public final void h() {
        try {
            if (this.k != null) {
                this.k.o();
            }
        } catch (RemoteException e2) {
            ms.d("#007 Could not call remote method.", e2);
        }
    }

    public final void i() {
        try {
            if (this.k != null) {
                this.k.p();
            }
        } catch (RemoteException e2) {
            ms.d("#007 Could not call remote method.", e2);
        }
    }

    public final String j() {
        try {
            if (this.k != null) {
                return this.k.q_();
            }
        } catch (RemoteException e2) {
            ms.d("#007 Could not call remote method.", e2);
        }
        return null;
    }

    public final i k() {
        return this.d;
    }

    public final aqs l() {
        aqs aqs = null;
        if (this.k == null) {
            return aqs;
        }
        try {
            return this.k.t();
        } catch (RemoteException e2) {
            ms.d("#007 Could not call remote method.", e2);
            return aqs;
        }
    }

    public final j m() {
        return this.m;
    }
}
