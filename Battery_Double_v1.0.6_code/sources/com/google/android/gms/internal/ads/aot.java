package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.util.DisplayMetrics;
import com.google.android.gms.ads.d;
import com.google.android.gms.ads.l;
import com.google.android.gms.common.internal.a.a;
import com.google.android.gms.common.internal.a.c;

@cm
public class aot extends a {
    public static final Creator<aot> CREATOR = new aou();

    /* renamed from: a reason: collision with root package name */
    public final String f2814a;

    /* renamed from: b reason: collision with root package name */
    public final int f2815b;
    public final int c;
    public final boolean d;
    public final int e;
    public final int f;
    public final aot[] g;
    public final boolean h;
    public final boolean i;
    public boolean j;

    public aot() {
        this("interstitial_mb", 0, 0, true, 0, 0, null, false, false, false);
    }

    public aot(Context context, d dVar) {
        this(context, new d[]{dVar});
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x006b  */
    public aot(Context context, d[] dVarArr) {
        int i2;
        double d2;
        d dVar = dVarArr[0];
        this.d = false;
        this.i = dVar.c();
        if (this.i) {
            this.e = d.f1937a.b();
            this.f2815b = d.f1937a.a();
        } else {
            this.e = dVar.b();
            this.f2815b = dVar.a();
        }
        boolean z = this.e == -1;
        boolean z2 = this.f2815b == -2;
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        if (z) {
            ape.a();
            if (mh.g(context)) {
                ape.a();
                if (mh.h(context)) {
                    int i3 = displayMetrics.widthPixels;
                    ape.a();
                    this.f = i3 - mh.i(context);
                    d2 = (double) (((float) this.f) / displayMetrics.density);
                    int i4 = (int) d2;
                    if (d2 - ((double) ((int) d2)) >= 0.01d) {
                        i4++;
                    }
                    i2 = i4;
                }
            }
            this.f = displayMetrics.widthPixels;
            d2 = (double) (((float) this.f) / displayMetrics.density);
            int i42 = (int) d2;
            if (d2 - ((double) ((int) d2)) >= 0.01d) {
            }
            i2 = i42;
        } else {
            int i5 = this.e;
            ape.a();
            this.f = mh.a(displayMetrics, this.e);
            i2 = i5;
        }
        int i6 = z2 ? c(displayMetrics) : this.f2815b;
        ape.a();
        this.c = mh.a(displayMetrics, i6);
        if (z || z2) {
            this.f2814a = i2 + "x" + i6 + "_as";
        } else if (this.i) {
            this.f2814a = "320x50_mb";
        } else {
            this.f2814a = dVar.toString();
        }
        if (dVarArr.length > 1) {
            this.g = new aot[dVarArr.length];
            for (int i7 = 0; i7 < dVarArr.length; i7++) {
                this.g[i7] = new aot(context, dVarArr[i7]);
            }
        } else {
            this.g = null;
        }
        this.h = false;
        this.j = false;
    }

    public aot(aot aot, aot[] aotArr) {
        this(aot.f2814a, aot.f2815b, aot.c, aot.d, aot.e, aot.f, aotArr, aot.h, aot.i, aot.j);
    }

    aot(String str, int i2, int i3, boolean z, int i4, int i5, aot[] aotArr, boolean z2, boolean z3, boolean z4) {
        this.f2814a = str;
        this.f2815b = i2;
        this.c = i3;
        this.d = z;
        this.e = i4;
        this.f = i5;
        this.g = aotArr;
        this.h = z2;
        this.i = z3;
        this.j = z4;
    }

    public static int a(DisplayMetrics displayMetrics) {
        return displayMetrics.widthPixels;
    }

    public static aot a() {
        return new aot("reward_mb", 0, 0, true, 0, 0, null, false, false, false);
    }

    public static aot a(Context context) {
        return new aot("320x50_mb", 0, 0, false, 0, 0, null, true, false, false);
    }

    public static int b(DisplayMetrics displayMetrics) {
        return (int) (((float) c(displayMetrics)) * displayMetrics.density);
    }

    private static int c(DisplayMetrics displayMetrics) {
        int i2 = (int) (((float) displayMetrics.heightPixels) / displayMetrics.density);
        if (i2 <= 400) {
            return 32;
        }
        return i2 <= 720 ? 50 : 90;
    }

    public final d b() {
        return l.a(this.e, this.f2815b, this.f2814a);
    }

    public void writeToParcel(Parcel parcel, int i2) {
        int a2 = c.a(parcel);
        c.a(parcel, 2, this.f2814a, false);
        c.a(parcel, 3, this.f2815b);
        c.a(parcel, 4, this.c);
        c.a(parcel, 5, this.d);
        c.a(parcel, 6, this.e);
        c.a(parcel, 7, this.f);
        c.a(parcel, 8, (T[]) this.g, i2, false);
        c.a(parcel, 9, this.h);
        c.a(parcel, 10, this.i);
        c.a(parcel, 11, this.j);
        c.a(parcel, a2);
    }
}
