package com.google.android.gms.internal.ads;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.RandomAccess;

final class adr {

    /* renamed from: a reason: collision with root package name */
    private static final Class<?> f2486a = d();

    /* renamed from: b reason: collision with root package name */
    private static final aei<?, ?> f2487b = a(false);
    private static final aei<?, ?> c = a(true);
    private static final aei<?, ?> d = new aek();

    static int a(int i, Object obj, adp adp) {
        return obj instanceof acd ? aav.a(i, (acd) obj) : aav.b(i, (acw) obj, adp);
    }

    static int a(int i, List<?> list) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        int e = aav.e(i) * size;
        if (list instanceof acf) {
            acf acf = (acf) list;
            int i2 = 0;
            while (i2 < size) {
                Object b2 = acf.b(i2);
                i2++;
                e = (b2 instanceof aah ? aav.b((aah) b2) : aav.b((String) b2)) + e;
            }
            return e;
        }
        int i3 = 0;
        while (i3 < size) {
            Object obj = list.get(i3);
            i3++;
            e = (obj instanceof aah ? aav.b((aah) obj) : aav.b((String) obj)) + e;
        }
        return e;
    }

    static int a(int i, List<?> list, adp adp) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        int e = aav.e(i) * size;
        int i2 = 0;
        while (i2 < size) {
            Object obj = list.get(i2);
            i2++;
            e = (obj instanceof acd ? aav.a((acd) obj) : aav.a((acw) obj, adp)) + e;
        }
        return e;
    }

    static int a(int i, List<Long> list, boolean z) {
        if (list.size() == 0) {
            return 0;
        }
        return a(list) + (list.size() * aav.e(i));
    }

    static int a(List<Long> list) {
        int i = 0;
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        if (list instanceof ack) {
            ack ack = (ack) list;
            int i2 = 0;
            while (i2 < size) {
                int d2 = aav.d(ack.b(i2)) + i;
                i2++;
                i = d2;
            }
            return i;
        }
        int i3 = 0;
        for (int i4 = 0; i4 < size; i4++) {
            i3 += aav.d(((Long) list.get(i4)).longValue());
        }
        return i3;
    }

    public static aei<?, ?> a() {
        return f2487b;
    }

    private static aei<?, ?> a(boolean z) {
        try {
            Class e = e();
            if (e == null) {
                return null;
            }
            return (aei) e.getConstructor(new Class[]{Boolean.TYPE}).newInstance(new Object[]{Boolean.valueOf(z)});
        } catch (Throwable th) {
            return null;
        }
    }

    static <UT, UB> UB a(int i, int i2, UB ub, aei<UT, UB> aei) {
        if (ub == null) {
            ub = aei.a();
        }
        aei.a(ub, i, (long) i2);
        return ub;
    }

    static <UT, UB> UB a(int i, List<Integer> list, abt<?> abt, UB ub, aei<UT, UB> aei) {
        UB ub2;
        UB a2;
        int i2;
        if (abt == null) {
            return ub;
        }
        if (list instanceof RandomAccess) {
            int size = list.size();
            int i3 = 0;
            int i4 = 0;
            ub2 = ub;
            while (i3 < size) {
                int intValue = ((Integer) list.get(i3)).intValue();
                if (abt.a(intValue) != null) {
                    if (i3 != i4) {
                        list.set(i4, Integer.valueOf(intValue));
                    }
                    i2 = i4 + 1;
                    a2 = ub2;
                } else {
                    int i5 = i4;
                    a2 = a(i, intValue, ub2, aei);
                    i2 = i5;
                }
                i3++;
                ub2 = a2;
                i4 = i2;
            }
            if (i4 != size) {
                list.subList(i4, size).clear();
            }
        } else {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                int intValue2 = ((Integer) it.next()).intValue();
                if (abt.a(intValue2) == null) {
                    ub = a(i, intValue2, ub, aei);
                    it.remove();
                }
            }
            ub2 = ub;
        }
        return ub2;
    }

    public static void a(int i, List<String> list, afc afc) throws IOException {
        if (list != null && !list.isEmpty()) {
            afc.a(i, list);
        }
    }

    public static void a(int i, List<?> list, afc afc, adp adp) throws IOException {
        if (list != null && !list.isEmpty()) {
            afc.a(i, list, adp);
        }
    }

    public static void a(int i, List<Double> list, afc afc, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            afc.g(i, list, z);
        }
    }

    static <T, FT extends abj<FT>> void a(abe<FT> abe, T t, T t2) {
        abh a2 = abe.a((Object) t2);
        if (!a2.b()) {
            abe.b(t).a(a2);
        }
    }

    static <T> void a(acr acr, T t, T t2, long j) {
        aeo.a((Object) t, j, acr.a(aeo.f(t, j), aeo.f(t2, j)));
    }

    static <T, UT, UB> void a(aei<UT, UB> aei, T t, T t2) {
        aei.a((Object) t, aei.c(aei.b(t), aei.b(t2)));
    }

    public static void a(Class<?> cls) {
        if (!abp.class.isAssignableFrom(cls) && f2486a != null && !f2486a.isAssignableFrom(cls)) {
            throw new IllegalArgumentException("Message classes must extend GeneratedMessage or GeneratedMessageLite");
        }
    }

    public static boolean a(int i, int i2, int i3) {
        return i2 < 40 || ((((long) i2) - ((long) i)) + 1) + 9 <= ((2 * ((long) i3)) + 3) + ((((long) i3) + 3) * 3);
    }

    static boolean a(Object obj, Object obj2) {
        return obj == obj2 || (obj != null && obj.equals(obj2));
    }

    static int b(int i, List<aah> list) {
        int i2 = 0;
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        int e = size * aav.e(i);
        while (true) {
            int i3 = i2;
            if (i3 >= list.size()) {
                return e;
            }
            e += aav.b((aah) list.get(i3));
            i2 = i3 + 1;
        }
    }

    static int b(int i, List<acw> list, adp adp) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        int i2 = 0;
        for (int i3 = 0; i3 < size; i3++) {
            i2 += aav.c(i, (acw) list.get(i3), adp);
        }
        return i2;
    }

    static int b(int i, List<Long> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return (size * aav.e(i)) + b(list);
    }

    static int b(List<Long> list) {
        int i = 0;
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        if (list instanceof ack) {
            ack ack = (ack) list;
            int i2 = 0;
            while (i2 < size) {
                int e = aav.e(ack.b(i2)) + i;
                i2++;
                i = e;
            }
            return i;
        }
        int i3 = 0;
        for (int i4 = 0; i4 < size; i4++) {
            i3 += aav.e(((Long) list.get(i4)).longValue());
        }
        return i3;
    }

    public static aei<?, ?> b() {
        return c;
    }

    public static void b(int i, List<aah> list, afc afc) throws IOException {
        if (list != null && !list.isEmpty()) {
            afc.b(i, list);
        }
    }

    public static void b(int i, List<?> list, afc afc, adp adp) throws IOException {
        if (list != null && !list.isEmpty()) {
            afc.b(i, list, adp);
        }
    }

    public static void b(int i, List<Float> list, afc afc, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            afc.f(i, list, z);
        }
    }

    static int c(int i, List<Long> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return (size * aav.e(i)) + c(list);
    }

    static int c(List<Long> list) {
        int i = 0;
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        if (list instanceof ack) {
            ack ack = (ack) list;
            int i2 = 0;
            while (i2 < size) {
                int f = aav.f(ack.b(i2)) + i;
                i2++;
                i = f;
            }
            return i;
        }
        int i3 = 0;
        for (int i4 = 0; i4 < size; i4++) {
            i3 += aav.f(((Long) list.get(i4)).longValue());
        }
        return i3;
    }

    public static aei<?, ?> c() {
        return d;
    }

    public static void c(int i, List<Long> list, afc afc, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            afc.c(i, list, z);
        }
    }

    static int d(int i, List<Integer> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return (size * aav.e(i)) + d(list);
    }

    static int d(List<Integer> list) {
        int i = 0;
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        if (list instanceof abq) {
            abq abq = (abq) list;
            int i2 = 0;
            while (i2 < size) {
                int k = aav.k(abq.b(i2)) + i;
                i2++;
                i = k;
            }
            return i;
        }
        int i3 = 0;
        for (int i4 = 0; i4 < size; i4++) {
            i3 += aav.k(((Integer) list.get(i4)).intValue());
        }
        return i3;
    }

    private static Class<?> d() {
        try {
            return Class.forName("com.google.protobuf.GeneratedMessage");
        } catch (Throwable th) {
            return null;
        }
    }

    public static void d(int i, List<Long> list, afc afc, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            afc.d(i, list, z);
        }
    }

    static int e(int i, List<Integer> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return (size * aav.e(i)) + e(list);
    }

    static int e(List<Integer> list) {
        int i = 0;
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        if (list instanceof abq) {
            abq abq = (abq) list;
            int i2 = 0;
            while (i2 < size) {
                int f = aav.f(abq.b(i2)) + i;
                i2++;
                i = f;
            }
            return i;
        }
        int i3 = 0;
        for (int i4 = 0; i4 < size; i4++) {
            i3 += aav.f(((Integer) list.get(i4)).intValue());
        }
        return i3;
    }

    private static Class<?> e() {
        try {
            return Class.forName("com.google.protobuf.UnknownFieldSetSchema");
        } catch (Throwable th) {
            return null;
        }
    }

    public static void e(int i, List<Long> list, afc afc, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            afc.n(i, list, z);
        }
    }

    static int f(int i, List<Integer> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return (size * aav.e(i)) + f(list);
    }

    static int f(List<Integer> list) {
        int i = 0;
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        if (list instanceof abq) {
            abq abq = (abq) list;
            int i2 = 0;
            while (i2 < size) {
                int g = aav.g(abq.b(i2)) + i;
                i2++;
                i = g;
            }
            return i;
        }
        int i3 = 0;
        for (int i4 = 0; i4 < size; i4++) {
            i3 += aav.g(((Integer) list.get(i4)).intValue());
        }
        return i3;
    }

    public static void f(int i, List<Long> list, afc afc, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            afc.e(i, list, z);
        }
    }

    static int g(int i, List<Integer> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return (size * aav.e(i)) + g(list);
    }

    static int g(List<Integer> list) {
        int i = 0;
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        if (list instanceof abq) {
            abq abq = (abq) list;
            int i2 = 0;
            while (i2 < size) {
                int h = aav.h(abq.b(i2)) + i;
                i2++;
                i = h;
            }
            return i;
        }
        int i3 = 0;
        for (int i4 = 0; i4 < size; i4++) {
            i3 += aav.h(((Integer) list.get(i4)).intValue());
        }
        return i3;
    }

    public static void g(int i, List<Long> list, afc afc, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            afc.l(i, list, z);
        }
    }

    static int h(int i, List<?> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return aav.i(i, 0) * size;
    }

    static int h(List<?> list) {
        return list.size() << 2;
    }

    public static void h(int i, List<Integer> list, afc afc, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            afc.a(i, list, z);
        }
    }

    static int i(int i, List<?> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return size * aav.g(i, 0);
    }

    static int i(List<?> list) {
        return list.size() << 3;
    }

    public static void i(int i, List<Integer> list, afc afc, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            afc.j(i, list, z);
        }
    }

    static int j(int i, List<?> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return size * aav.b(i, true);
    }

    static int j(List<?> list) {
        return list.size();
    }

    public static void j(int i, List<Integer> list, afc afc, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            afc.m(i, list, z);
        }
    }

    public static void k(int i, List<Integer> list, afc afc, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            afc.b(i, list, z);
        }
    }

    public static void l(int i, List<Integer> list, afc afc, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            afc.k(i, list, z);
        }
    }

    public static void m(int i, List<Integer> list, afc afc, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            afc.h(i, list, z);
        }
    }

    public static void n(int i, List<Boolean> list, afc afc, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            afc.i(i, list, z);
        }
    }
}
