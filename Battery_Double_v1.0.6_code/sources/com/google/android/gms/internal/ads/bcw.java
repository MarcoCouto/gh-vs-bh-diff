package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.google.android.gms.b.a;
import com.google.android.gms.b.a.C0046a;
import java.util.List;

public final class bcw extends ajk implements bcu {
    bcw(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.mediation.client.IMediationAdapter");
    }

    public final a a() throws RemoteException {
        Parcel a2 = a(2, r_());
        a a3 = C0046a.a(a2.readStrongBinder());
        a2.recycle();
        return a3;
    }

    public final void a(a aVar) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (IInterface) aVar);
        b(21, r_);
    }

    public final void a(a aVar, aop aop, String str, bcx bcx) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (IInterface) aVar);
        ajm.a(r_, (Parcelable) aop);
        r_.writeString(str);
        ajm.a(r_, (IInterface) bcx);
        b(3, r_);
    }

    public final void a(a aVar, aop aop, String str, hl hlVar, String str2) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (IInterface) aVar);
        ajm.a(r_, (Parcelable) aop);
        r_.writeString(str);
        ajm.a(r_, (IInterface) hlVar);
        r_.writeString(str2);
        b(10, r_);
    }

    public final void a(a aVar, aop aop, String str, String str2, bcx bcx) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (IInterface) aVar);
        ajm.a(r_, (Parcelable) aop);
        r_.writeString(str);
        r_.writeString(str2);
        ajm.a(r_, (IInterface) bcx);
        b(7, r_);
    }

    public final void a(a aVar, aop aop, String str, String str2, bcx bcx, aul aul, List<String> list) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (IInterface) aVar);
        ajm.a(r_, (Parcelable) aop);
        r_.writeString(str);
        r_.writeString(str2);
        ajm.a(r_, (IInterface) bcx);
        ajm.a(r_, (Parcelable) aul);
        r_.writeStringList(list);
        b(14, r_);
    }

    public final void a(a aVar, aot aot, aop aop, String str, bcx bcx) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (IInterface) aVar);
        ajm.a(r_, (Parcelable) aot);
        ajm.a(r_, (Parcelable) aop);
        r_.writeString(str);
        ajm.a(r_, (IInterface) bcx);
        b(1, r_);
    }

    public final void a(a aVar, aot aot, aop aop, String str, String str2, bcx bcx) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (IInterface) aVar);
        ajm.a(r_, (Parcelable) aot);
        ajm.a(r_, (Parcelable) aop);
        r_.writeString(str);
        r_.writeString(str2);
        ajm.a(r_, (IInterface) bcx);
        b(6, r_);
    }

    public final void a(a aVar, hl hlVar, List<String> list) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (IInterface) aVar);
        ajm.a(r_, (IInterface) hlVar);
        r_.writeStringList(list);
        b(23, r_);
    }

    public final void a(aop aop, String str) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (Parcelable) aop);
        r_.writeString(str);
        b(11, r_);
    }

    public final void a(aop aop, String str, String str2) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (Parcelable) aop);
        r_.writeString(str);
        r_.writeString(str2);
        b(20, r_);
    }

    public final void a(boolean z) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, z);
        b(25, r_);
    }

    public final void b() throws RemoteException {
        b(4, r_());
    }

    public final void c() throws RemoteException {
        b(5, r_());
    }

    public final void d() throws RemoteException {
        b(8, r_());
    }

    public final void e() throws RemoteException {
        b(9, r_());
    }

    public final void f() throws RemoteException {
        b(12, r_());
    }

    public final boolean g() throws RemoteException {
        Parcel a2 = a(13, r_());
        boolean a3 = ajm.a(a2);
        a2.recycle();
        return a3;
    }

    public final bdd h() throws RemoteException {
        bdd bdg;
        Parcel a2 = a(15, r_());
        IBinder readStrongBinder = a2.readStrongBinder();
        if (readStrongBinder == null) {
            bdg = null;
        } else {
            IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.ads.internal.mediation.client.INativeAppInstallAdMapper");
            bdg = queryLocalInterface instanceof bdd ? (bdd) queryLocalInterface : new bdg(readStrongBinder);
        }
        a2.recycle();
        return bdg;
    }

    public final bdh i() throws RemoteException {
        bdh bdj;
        Parcel a2 = a(16, r_());
        IBinder readStrongBinder = a2.readStrongBinder();
        if (readStrongBinder == null) {
            bdj = null;
        } else {
            IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.ads.internal.mediation.client.INativeContentAdMapper");
            bdj = queryLocalInterface instanceof bdh ? (bdh) queryLocalInterface : new bdj(readStrongBinder);
        }
        a2.recycle();
        return bdj;
    }

    public final Bundle j() throws RemoteException {
        Parcel a2 = a(17, r_());
        Bundle bundle = (Bundle) ajm.a(a2, Bundle.CREATOR);
        a2.recycle();
        return bundle;
    }

    public final Bundle k() throws RemoteException {
        Parcel a2 = a(18, r_());
        Bundle bundle = (Bundle) ajm.a(a2, Bundle.CREATOR);
        a2.recycle();
        return bundle;
    }

    public final Bundle l() throws RemoteException {
        Parcel a2 = a(19, r_());
        Bundle bundle = (Bundle) ajm.a(a2, Bundle.CREATOR);
        a2.recycle();
        return bundle;
    }

    public final boolean m() throws RemoteException {
        Parcel a2 = a(22, r_());
        boolean a3 = ajm.a(a2);
        a2.recycle();
        return a3;
    }

    public final avt n() throws RemoteException {
        Parcel a2 = a(24, r_());
        avt a3 = avu.a(a2.readStrongBinder());
        a2.recycle();
        return a3;
    }

    public final aqs o() throws RemoteException {
        Parcel a2 = a(26, r_());
        aqs a3 = aqt.a(a2.readStrongBinder());
        a2.recycle();
        return a3;
    }

    public final bdk p() throws RemoteException {
        bdk bdm;
        Parcel a2 = a(27, r_());
        IBinder readStrongBinder = a2.readStrongBinder();
        if (readStrongBinder == null) {
            bdm = null;
        } else {
            IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.ads.internal.mediation.client.IUnifiedNativeAdMapper");
            bdm = queryLocalInterface instanceof bdk ? (bdk) queryLocalInterface : new bdm(readStrongBinder);
        }
        a2.recycle();
        return bdm;
    }
}
