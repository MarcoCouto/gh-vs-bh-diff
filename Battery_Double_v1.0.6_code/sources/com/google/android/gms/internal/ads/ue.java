package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.xn.b;
import java.security.GeneralSecurityException;

public final class ue {
    @Deprecated
    public static final ua a(byte[] bArr) throws GeneralSecurityException {
        try {
            xn a2 = xn.a(bArr);
            for (b bVar : a2.b()) {
                if (bVar.b().c() == xe.b.UNKNOWN_KEYMATERIAL || bVar.b().c() == xe.b.SYMMETRIC) {
                    throw new GeneralSecurityException("keyset contains secret key material");
                } else if (bVar.b().c() == xe.b.ASYMMETRIC_PRIVATE) {
                    throw new GeneralSecurityException("keyset contains secret key material");
                }
            }
            return ua.a(a2);
        } catch (abv e) {
            throw new GeneralSecurityException("invalid keyset");
        }
    }
}
