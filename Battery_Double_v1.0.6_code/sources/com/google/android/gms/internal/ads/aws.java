package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.b.a;
import java.util.List;

public interface aws extends IInterface {
    String a() throws RemoteException;

    void a(Bundle bundle) throws RemoteException;

    void a(awq awq) throws RemoteException;

    List b() throws RemoteException;

    boolean b(Bundle bundle) throws RemoteException;

    String c() throws RemoteException;

    void c(Bundle bundle) throws RemoteException;

    auw d() throws RemoteException;

    String e() throws RemoteException;

    String f() throws RemoteException;

    double g() throws RemoteException;

    String h() throws RemoteException;

    String i() throws RemoteException;

    aqs j() throws RemoteException;

    a n() throws RemoteException;

    a p() throws RemoteException;

    String q() throws RemoteException;

    Bundle r() throws RemoteException;

    aus s() throws RemoteException;

    void t() throws RemoteException;

    void u() throws RemoteException;
}
