package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.abp.e;

public final class xe extends abp<xe, a> implements acy {
    private static volatile adi<xe> zzakh;
    /* access modifiers changed from: private */
    public static final xe zzdkv = new xe();
    private String zzdks = "";
    private aah zzdkt = aah.f2393a;
    private int zzdku;

    public static final class a extends com.google.android.gms.internal.ads.abp.a<xe, a> implements acy {
        private a() {
            super(xe.zzdkv);
        }

        /* synthetic */ a(xf xfVar) {
            this();
        }

        public final a a(aah aah) {
            b();
            ((xe) this.f2433a).a(aah);
            return this;
        }

        public final a a(b bVar) {
            b();
            ((xe) this.f2433a).a(bVar);
            return this;
        }

        public final a a(String str) {
            b();
            ((xe) this.f2433a).a(str);
            return this;
        }
    }

    public enum b implements abs {
        UNKNOWN_KEYMATERIAL(0),
        SYMMETRIC(1),
        ASYMMETRIC_PRIVATE(2),
        ASYMMETRIC_PUBLIC(3),
        REMOTE(4),
        UNRECOGNIZED(-1);
        
        private static final abt<b> g = null;
        private final int h;

        static {
            g = new xg();
        }

        private b(int i2) {
            this.h = i2;
        }

        public static b a(int i2) {
            switch (i2) {
                case 0:
                    return UNKNOWN_KEYMATERIAL;
                case 1:
                    return SYMMETRIC;
                case 2:
                    return ASYMMETRIC_PRIVATE;
                case 3:
                    return ASYMMETRIC_PUBLIC;
                case 4:
                    return REMOTE;
                default:
                    return null;
            }
        }

        public final int a() {
            if (this != UNRECOGNIZED) {
                return this.h;
            }
            throw new IllegalArgumentException("Can't get the number of an unknown enum value.");
        }
    }

    static {
        abp.a(xe.class, zzdkv);
    }

    private xe() {
    }

    /* access modifiers changed from: private */
    public final void a(aah aah) {
        if (aah == null) {
            throw new NullPointerException();
        }
        this.zzdkt = aah;
    }

    /* access modifiers changed from: private */
    public final void a(b bVar) {
        if (bVar == null) {
            throw new NullPointerException();
        }
        this.zzdku = bVar.a();
    }

    /* access modifiers changed from: private */
    public final void a(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.zzdks = str;
    }

    public static a d() {
        return (a) ((com.google.android.gms.internal.ads.abp.a) zzdkv.a(e.e, (Object) null, (Object) null));
    }

    public static xe e() {
        return zzdkv;
    }

    /* JADX WARNING: type inference failed for: r0v6, types: [com.google.android.gms.internal.ads.adi<com.google.android.gms.internal.ads.xe>] */
    /* JADX WARNING: type inference failed for: r0v7, types: [java.lang.Object] */
    /* JADX WARNING: type inference failed for: r0v8, types: [com.google.android.gms.internal.ads.abp$b, com.google.android.gms.internal.ads.adi<com.google.android.gms.internal.ads.xe>] */
    /* JADX WARNING: type inference failed for: r0v16 */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r0v8, types: [com.google.android.gms.internal.ads.abp$b, com.google.android.gms.internal.ads.adi<com.google.android.gms.internal.ads.xe>]
  assigns: [com.google.android.gms.internal.ads.abp$b]
  uses: [com.google.android.gms.internal.ads.adi<com.google.android.gms.internal.ads.xe>]
  mth insns count: 42
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
    	at jadx.core.ProcessClass.process(ProcessClass.java:35)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Unknown variable types count: 2 */
    public final Object a(int i, Object obj, Object obj2) {
        ? r0;
        switch (xf.f3772a[i - 1]) {
            case 1:
                return new xe();
            case 2:
                return new a(null);
            case 3:
                Object[] objArr = {"zzdks", "zzdkt", "zzdku"};
                return a((acw) zzdkv, "\u0000\u0003\u0000\u0000\u0001\u0003\u0003\u0004\u0000\u0000\u0000\u0001Ȉ\u0002\n\u0003\f", objArr);
            case 4:
                return zzdkv;
            case 5:
                adi<xe> adi = zzakh;
                if (adi != null) {
                    return adi;
                }
                synchronized (xe.class) {
                    r0 = zzakh;
                    if (r0 == 0) {
                        ? bVar = new com.google.android.gms.internal.ads.abp.b(zzdkv);
                        zzakh = bVar;
                        r0 = bVar;
                    }
                }
                return r0;
            case 6:
                return Byte.valueOf(1);
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    public final String a() {
        return this.zzdks;
    }

    public final aah b() {
        return this.zzdkt;
    }

    public final b c() {
        b a2 = b.a(this.zzdku);
        return a2 == null ? b.UNRECOGNIZED : a2;
    }
}
