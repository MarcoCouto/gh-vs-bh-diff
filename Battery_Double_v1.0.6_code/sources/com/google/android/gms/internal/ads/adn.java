package com.google.android.gms.internal.ads;

final class adn {

    /* renamed from: a reason: collision with root package name */
    private final String f2484a;

    /* renamed from: b reason: collision with root package name */
    private int f2485b = 0;

    adn(String str) {
        this.f2484a = str;
    }

    /* access modifiers changed from: 0000 */
    public final boolean a() {
        return this.f2485b < this.f2484a.length();
    }

    /* access modifiers changed from: 0000 */
    public final int b() {
        String str = this.f2484a;
        int i = this.f2485b;
        this.f2485b = i + 1;
        char charAt = str.charAt(i);
        if (charAt < 55296) {
            return charAt;
        }
        char c = charAt & 8191;
        int i2 = 13;
        while (true) {
            String str2 = this.f2484a;
            int i3 = this.f2485b;
            this.f2485b = i3 + 1;
            char charAt2 = str2.charAt(i3);
            if (charAt2 < 55296) {
                return (charAt2 << i2) | c;
            }
            c |= (charAt2 & 8191) << i2;
            i2 += 13;
        }
    }
}
