package com.google.android.gms.internal.ads;

import java.util.HashMap;

public final class aih extends agf<Integer, Long> {

    /* renamed from: a reason: collision with root package name */
    public Long f2622a;

    /* renamed from: b reason: collision with root package name */
    public Long f2623b;
    public Long c;

    public aih() {
    }

    public aih(String str) {
        a(str);
    }

    /* access modifiers changed from: protected */
    public final HashMap<Integer, Long> a() {
        HashMap<Integer, Long> hashMap = new HashMap<>();
        hashMap.put(Integer.valueOf(0), this.f2622a);
        hashMap.put(Integer.valueOf(1), this.f2623b);
        hashMap.put(Integer.valueOf(2), this.c);
        return hashMap;
    }

    /* access modifiers changed from: protected */
    public final void a(String str) {
        HashMap b2 = b(str);
        if (b2 != null) {
            this.f2622a = (Long) b2.get(Integer.valueOf(0));
            this.f2623b = (Long) b2.get(Integer.valueOf(1));
            this.c = (Long) b2.get(Integer.valueOf(2));
        }
    }
}
