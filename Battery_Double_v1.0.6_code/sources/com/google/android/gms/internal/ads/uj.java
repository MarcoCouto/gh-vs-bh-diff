package com.google.android.gms.internal.ads;

import java.security.GeneralSecurityException;

final class uj implements tt<tr> {
    public final tz<tr> a(String str, String str2, int i) throws GeneralSecurityException {
        boolean z;
        tz<tr> usVar;
        char c = 65535;
        String lowerCase = str2.toLowerCase();
        switch (lowerCase.hashCode()) {
            case 2989895:
                if (lowerCase.equals("aead")) {
                    z = false;
                    break;
                }
            default:
                z = true;
                break;
        }
        switch (z) {
            case false:
                switch (str.hashCode()) {
                    case 360753376:
                        if (str.equals("type.googleapis.com/google.crypto.tink.ChaCha20Poly1305Key")) {
                            c = 3;
                            break;
                        }
                        break;
                    case 1215885937:
                        if (str.equals("type.googleapis.com/google.crypto.tink.AesCtrHmacAeadKey")) {
                            c = 0;
                            break;
                        }
                        break;
                    case 1469984853:
                        if (str.equals("type.googleapis.com/google.crypto.tink.KmsAeadKey")) {
                            c = 4;
                            break;
                        }
                        break;
                    case 1797113348:
                        if (str.equals("type.googleapis.com/google.crypto.tink.AesEaxKey")) {
                            c = 1;
                            break;
                        }
                        break;
                    case 1855890991:
                        if (str.equals("type.googleapis.com/google.crypto.tink.AesGcmKey")) {
                            c = 2;
                            break;
                        }
                        break;
                    case 2079211877:
                        if (str.equals("type.googleapis.com/google.crypto.tink.KmsEnvelopeAeadKey")) {
                            c = 5;
                            break;
                        }
                        break;
                }
                switch (c) {
                    case 0:
                        usVar = new ul<>();
                        break;
                    case 1:
                        usVar = new un<>();
                        break;
                    case 2:
                        usVar = new uo<>();
                        break;
                    case 3:
                        usVar = new up<>();
                        break;
                    case 4:
                        usVar = new uq<>();
                        break;
                    case 5:
                        usVar = new us<>();
                        break;
                    default:
                        throw new GeneralSecurityException(String.format("No support for primitive 'Aead' with key type '%s'.", new Object[]{str}));
                }
                if (usVar.a() >= i) {
                    return usVar;
                }
                throw new GeneralSecurityException(String.format("No key manager for key type '%s' with version at least %d.", new Object[]{str, Integer.valueOf(i)}));
            default:
                throw new GeneralSecurityException(String.format("No support for primitive '%s'.", new Object[]{str2}));
        }
    }
}
