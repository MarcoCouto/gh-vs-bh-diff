package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.yc.a;
import java.security.GeneralSecurityException;

public final class uk {

    /* renamed from: a reason: collision with root package name */
    public static final yc f3724a = ((yc) ((a) yc.b().a(vh.f3740a)).a(tu.a("TinkAead", "Aead", "AesCtrHmacAeadKey", 0, true)).a(tu.a("TinkAead", "Aead", "AesEaxKey", 0, true)).a(tu.a("TinkAead", "Aead", "AesGcmKey", 0, true)).a(tu.a("TinkAead", "Aead", "ChaCha20Poly1305Key", 0, true)).a(tu.a("TinkAead", "Aead", "KmsAeadKey", 0, true)).a(tu.a("TinkAead", "Aead", "KmsEnvelopeAeadKey", 0, true)).a("TINK_AEAD_1_0_0").c());

    /* renamed from: b reason: collision with root package name */
    private static final yc f3725b = ((yc) ((a) yc.b().a(f3724a)).a("TINK_AEAD_1_1_0").c());

    static {
        try {
            a();
        } catch (GeneralSecurityException e) {
            throw new ExceptionInInitializerError(e);
        }
    }

    public static void a() throws GeneralSecurityException {
        uh.a("TinkAead", (tt<P>) new uj<P>());
        vh.a();
    }
}
