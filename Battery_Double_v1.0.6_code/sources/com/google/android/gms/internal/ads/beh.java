package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.b.a;
import com.google.android.gms.b.a.C0046a;

public abstract class beh extends ajl implements beg {
    public beh() {
        super("com.google.android.gms.ads.internal.mediation.client.rtb.IRtbAdapter");
    }

    public static beg a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.ads.internal.mediation.client.rtb.IRtbAdapter");
        return queryLocalInterface instanceof beg ? (beg) queryLocalInterface : new bei(iBinder);
    }

    /* JADX WARNING: type inference failed for: r5v0 */
    /* JADX WARNING: type inference failed for: r5v2, types: [com.google.android.gms.internal.ads.bef] */
    /* JADX WARNING: type inference failed for: r0v4, types: [com.google.android.gms.internal.ads.bee] */
    /* JADX WARNING: type inference failed for: r5v3 */
    /* JADX WARNING: type inference failed for: r5v4, types: [com.google.android.gms.internal.ads.bee] */
    /* JADX WARNING: type inference failed for: r5v6, types: [com.google.android.gms.internal.ads.bed] */
    /* JADX WARNING: type inference failed for: r0v12, types: [com.google.android.gms.internal.ads.bec] */
    /* JADX WARNING: type inference failed for: r5v7 */
    /* JADX WARNING: type inference failed for: r5v8, types: [com.google.android.gms.internal.ads.bec] */
    /* JADX WARNING: type inference failed for: r5v10 */
    /* JADX WARNING: type inference failed for: r5v11 */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r5v0
  assigns: [?[int, float, boolean, short, byte, char, OBJECT, ARRAY], ?[OBJECT, ARRAY], com.google.android.gms.internal.ads.bed, com.google.android.gms.internal.ads.bef]
  uses: [com.google.android.gms.internal.ads.bee, com.google.android.gms.internal.ads.bec]
  mth insns count: 79
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
    	at jadx.core.ProcessClass.process(ProcessClass.java:35)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Unknown variable types count: 5 */
    public final boolean dispatchTransaction(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        bej bek;
        ? r5 = 0;
        switch (i) {
            case 1:
                a a2 = C0046a.a(parcel.readStrongBinder());
                String readString = parcel.readString();
                Bundle bundle = (Bundle) ajm.a(parcel, Bundle.CREATOR);
                IBinder readStrongBinder = parcel.readStrongBinder();
                if (readStrongBinder == null) {
                    bek = null;
                } else {
                    IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.ads.internal.mediation.client.rtb.ISignalsCallback");
                    bek = queryLocalInterface instanceof bej ? (bej) queryLocalInterface : new bek(readStrongBinder);
                }
                a(a2, readString, bundle, bek);
                parcel2.writeNoException();
                break;
            case 2:
                beq a3 = a();
                parcel2.writeNoException();
                ajm.b(parcel2, a3);
                break;
            case 3:
                beq b2 = b();
                parcel2.writeNoException();
                ajm.b(parcel2, b2);
                break;
            case 4:
                byte[] createByteArray = parcel.createByteArray();
                String readString2 = parcel.readString();
                Bundle bundle2 = (Bundle) ajm.a(parcel, Bundle.CREATOR);
                a a4 = C0046a.a(parcel.readStrongBinder());
                IBinder readStrongBinder2 = parcel.readStrongBinder();
                if (readStrongBinder2 != null) {
                    IInterface queryLocalInterface2 = readStrongBinder2.queryLocalInterface("com.google.android.gms.ads.internal.mediation.client.rtb.IBannerCallback");
                    r5 = queryLocalInterface2 instanceof bec ? (bec) queryLocalInterface2 : new bed(readStrongBinder2);
                }
                a(createByteArray, readString2, bundle2, a4, r5, bcy.a(parcel.readStrongBinder()), (aot) ajm.a(parcel, aot.CREATOR));
                parcel2.writeNoException();
                break;
            case 5:
                aqs c = c();
                parcel2.writeNoException();
                ajm.a(parcel2, (IInterface) c);
                break;
            case 6:
                byte[] createByteArray2 = parcel.createByteArray();
                String readString3 = parcel.readString();
                Bundle bundle3 = (Bundle) ajm.a(parcel, Bundle.CREATOR);
                a a5 = C0046a.a(parcel.readStrongBinder());
                IBinder readStrongBinder3 = parcel.readStrongBinder();
                if (readStrongBinder3 != null) {
                    IInterface queryLocalInterface3 = readStrongBinder3.queryLocalInterface("com.google.android.gms.ads.internal.mediation.client.rtb.IInterstitialCallback");
                    r5 = queryLocalInterface3 instanceof bee ? (bee) queryLocalInterface3 : new bef(readStrongBinder3);
                }
                a(createByteArray2, readString3, bundle3, a5, r5, bcy.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
            case 7:
                d();
                parcel2.writeNoException();
                break;
            default:
                return false;
        }
        return true;
    }
}
