package com.google.android.gms.internal.ads;

final class aes extends aer {
    aes() {
    }

    /* JADX WARNING: type inference failed for: r0v3 */
    /* JADX WARNING: type inference failed for: r0v5 */
    /* JADX WARNING: type inference failed for: r0v12 */
    /* JADX WARNING: type inference failed for: r0v15 */
    /* JADX WARNING: type inference failed for: r0v19 */
    /* JADX WARNING: type inference failed for: r0v21 */
    /* JADX WARNING: type inference failed for: r0v22 */
    /* access modifiers changed from: 0000 */
    /* JADX WARNING: CFG modification limit reached, blocks count: 151 */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0021, code lost:
        if (r0 >= -32) goto L_0x0031;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0023, code lost:
        if (r3 >= r13) goto L_0x0015;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0027, code lost:
        if (r0 < -62) goto L_0x002f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0029, code lost:
        r0 = r3 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x002d, code lost:
        if (r11[r3] <= -65) goto L_0x0017;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x002f, code lost:
        r0 = -1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0033, code lost:
        if (r0 >= -16) goto L_0x0056;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0037, code lost:
        if (r3 < (r13 - 1)) goto L_0x003e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0039, code lost:
        r0 = com.google.android.gms.internal.ads.aeq.c(r11, r3, r13);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x003e, code lost:
        r4 = r3 + 1;
        r3 = r11[r3];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0042, code lost:
        if (r3 > -65) goto L_0x0054;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0044, code lost:
        if (r0 != -32) goto L_0x0048;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0046, code lost:
        if (r3 < -96) goto L_0x0054;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x004a, code lost:
        if (r0 != -19) goto L_0x004e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x004c, code lost:
        if (r3 >= -96) goto L_0x0054;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x004e, code lost:
        r0 = r4 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0052, code lost:
        if (r11[r4] <= -65) goto L_0x0017;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0054, code lost:
        r0 = -1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0058, code lost:
        if (r3 < (r13 - 2)) goto L_0x005f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x005a, code lost:
        r0 = com.google.android.gms.internal.ads.aeq.c(r11, r3, r13);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x005f, code lost:
        r4 = r3 + 1;
        r3 = r11[r3];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0063, code lost:
        if (r3 > -65) goto L_0x007a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x006c, code lost:
        if ((((r0 << 28) + (r3 + 112)) >> 30) != 0) goto L_0x007a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x006e, code lost:
        r3 = r4 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0072, code lost:
        if (r11[r4] > -65) goto L_0x007a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0074, code lost:
        r0 = r3 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x0078, code lost:
        if (r11[r3] <= -65) goto L_0x0017;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x007a, code lost:
        r0 = -1;
     */
    /* JADX WARNING: Incorrect type for immutable var: ssa=byte, code=int, for r0v4, types: [byte, int] */
    /* JADX WARNING: Multi-variable type inference failed */
    public final int a(int i, byte[] bArr, int i2, int i3) {
        int i4;
        int i5 = i2;
        while (i5 < i3 && bArr[i5] >= 0) {
            i5++;
        }
        if (i5 >= i3) {
            i4 = 0;
        } else {
            loop1:
            while (true) {
                if (i5 >= i3) {
                    i4 = 0;
                    break loop1;
                }
                int i6 = i5 + 1;
                i4 = bArr[i5];
                if (i4 < 0) {
                    break;
                }
                i5 = i6;
            }
        }
        return i4;
    }

    /* access modifiers changed from: 0000 */
    public final int a(CharSequence charSequence, byte[] bArr, int i, int i2) {
        int i3;
        int length = charSequence.length();
        int i4 = 0;
        int i5 = i + i2;
        while (i4 < length && i4 + i < i5) {
            char charAt = charSequence.charAt(i4);
            if (charAt >= 128) {
                break;
            }
            bArr[i + i4] = (byte) charAt;
            i4++;
        }
        if (i4 == length) {
            return i + length;
        }
        int i6 = i + i4;
        while (i4 < length) {
            char charAt2 = charSequence.charAt(i4);
            if (charAt2 < 128 && i6 < i5) {
                i3 = i6 + 1;
                bArr[i6] = (byte) charAt2;
            } else if (charAt2 < 2048 && i6 <= i5 - 2) {
                int i7 = i6 + 1;
                bArr[i6] = (byte) ((charAt2 >>> 6) | 960);
                i3 = i7 + 1;
                bArr[i7] = (byte) ((charAt2 & '?') | 128);
            } else if ((charAt2 < 55296 || 57343 < charAt2) && i6 <= i5 - 3) {
                int i8 = i6 + 1;
                bArr[i6] = (byte) ((charAt2 >>> 12) | 480);
                int i9 = i8 + 1;
                bArr[i8] = (byte) (((charAt2 >>> 6) & 63) | 128);
                i3 = i9 + 1;
                bArr[i9] = (byte) ((charAt2 & '?') | 128);
            } else if (i6 <= i5 - 4) {
                if (i4 + 1 != charSequence.length()) {
                    i4++;
                    char charAt3 = charSequence.charAt(i4);
                    if (Character.isSurrogatePair(charAt2, charAt3)) {
                        int codePoint = Character.toCodePoint(charAt2, charAt3);
                        int i10 = i6 + 1;
                        bArr[i6] = (byte) ((codePoint >>> 18) | 240);
                        int i11 = i10 + 1;
                        bArr[i10] = (byte) (((codePoint >>> 12) & 63) | 128);
                        int i12 = i11 + 1;
                        bArr[i11] = (byte) (((codePoint >>> 6) & 63) | 128);
                        i3 = i12 + 1;
                        bArr[i12] = (byte) ((codePoint & 63) | 128);
                    }
                }
                throw new aet(i4 - 1, length);
            } else if (55296 > charAt2 || charAt2 > 57343 || (i4 + 1 != charSequence.length() && Character.isSurrogatePair(charAt2, charSequence.charAt(i4 + 1)))) {
                throw new ArrayIndexOutOfBoundsException("Failed writing " + charAt2 + " at index " + i6);
            } else {
                throw new aet(i4, length);
            }
            i4++;
            i6 = i3;
        }
        return i6;
    }
}
