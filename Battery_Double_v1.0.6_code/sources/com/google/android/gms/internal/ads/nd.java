package com.google.android.gms.internal.ads;

import java.util.concurrent.ExecutionException;

final /* synthetic */ class nd implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final mz f3533a;

    /* renamed from: b reason: collision with root package name */
    private final nn f3534b;

    nd(mz mzVar, nn nnVar) {
        this.f3533a = mzVar;
        this.f3534b = nnVar;
    }

    public final void run() {
        mz mzVar = this.f3533a;
        try {
            mzVar.a(this.f3534b.get());
            return;
        } catch (ExecutionException e) {
            e = e.getCause();
        } catch (InterruptedException e2) {
            e = e2;
            Thread.currentThread().interrupt();
        } catch (Exception e3) {
            e = e3;
        }
        mzVar.a(e);
    }
}
