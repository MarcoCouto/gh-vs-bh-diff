package com.google.android.gms.internal.ads;

import java.util.PriorityQueue;

@cm
public final class ama {
    private static long a(long j, int i) {
        if (i == 0) {
            return 1;
        }
        return i != 1 ? i % 2 == 0 ? a((j * j) % 1073807359, i / 2) % 1073807359 : ((a((j * j) % 1073807359, i / 2) % 1073807359) * j) % 1073807359 : j;
    }

    private static String a(String[] strArr, int i, int i2) {
        if (strArr.length < i + i2) {
            jm.c("Unable to construct shingle");
            return "";
        }
        StringBuilder sb = new StringBuilder();
        for (int i3 = i; i3 < (i + i2) - 1; i3++) {
            sb.append(strArr[i3]);
            sb.append(' ');
        }
        sb.append(strArr[(i + i2) - 1]);
        return sb.toString();
    }

    private static void a(int i, long j, String str, int i2, PriorityQueue<amb> priorityQueue) {
        amb amb = new amb(j, str, i2);
        if ((priorityQueue.size() != i || (((amb) priorityQueue.peek()).c <= amb.c && ((amb) priorityQueue.peek()).f2721a <= amb.f2721a)) && !priorityQueue.contains(amb)) {
            priorityQueue.add(amb);
            if (priorityQueue.size() > i) {
                priorityQueue.poll();
            }
        }
    }

    public static void a(String[] strArr, int i, int i2, PriorityQueue<amb> priorityQueue) {
        if (strArr.length < i2) {
            a(i, b(strArr, 0, strArr.length), a(strArr, 0, strArr.length), strArr.length, priorityQueue);
            return;
        }
        long b2 = b(strArr, 0, i2);
        a(i, b2, a(strArr, 0, i2), i2, priorityQueue);
        long a2 = a(16785407, i2 - 1);
        for (int i3 = 1; i3 < (strArr.length - i2) + 1; i3++) {
            long j = b2 + 1073807359;
            b2 = (((((j - ((((((long) alx.a(strArr[i3 - 1])) + 2147483647L) % 1073807359) * a2) % 1073807359)) % 1073807359) * 16785407) % 1073807359) + ((((long) alx.a(strArr[(i3 + i2) - 1])) + 2147483647L) % 1073807359)) % 1073807359;
            a(i, b2, a(strArr, i3, i2), strArr.length, priorityQueue);
        }
    }

    private static long b(String[] strArr, int i, int i2) {
        long a2 = (((long) alx.a(strArr[0])) + 2147483647L) % 1073807359;
        for (int i3 = 1; i3 < i2; i3++) {
            a2 = (((a2 * 16785407) % 1073807359) + ((((long) alx.a(strArr[i3])) + 2147483647L) % 1073807359)) % 1073807359;
        }
        return a2;
    }
}
