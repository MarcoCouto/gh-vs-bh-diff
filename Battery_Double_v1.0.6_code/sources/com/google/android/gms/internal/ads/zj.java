package com.google.android.gms.internal.ads;

import java.security.SecureRandom;

public final class zj {

    /* renamed from: a reason: collision with root package name */
    private static final ThreadLocal<SecureRandom> f3814a = new zk();

    public static byte[] a(int i) {
        byte[] bArr = new byte[i];
        ((SecureRandom) f3814a.get()).nextBytes(bArr);
        return bArr;
    }

    /* access modifiers changed from: private */
    public static SecureRandom b() {
        SecureRandom secureRandom = new SecureRandom();
        secureRandom.nextLong();
        return secureRandom;
    }
}
