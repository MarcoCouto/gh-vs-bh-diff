package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewParent;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import com.google.android.gms.ads.internal.ax;
import com.google.android.gms.common.util.f;
import java.util.Map;
import java.util.Set;

@cm
public final class d extends n {

    /* renamed from: a reason: collision with root package name */
    private static final Set<String> f3247a = f.b((T[]) new String[]{"top-left", "top-right", "top-center", "center", "bottom-left", "bottom-right", "bottom-center"});

    /* renamed from: b reason: collision with root package name */
    private String f3248b = "top-right";
    private boolean c = true;
    private int d = 0;
    private int e = 0;
    private int f = -1;
    private int g = 0;
    private int h = 0;
    private int i = -1;
    private final Object j = new Object();
    private final qn k;
    private final Activity l;
    private sb m;
    private ImageView n;
    private LinearLayout o;
    private o p;
    private PopupWindow q;
    private RelativeLayout r;
    private ViewGroup s;

    public d(qn qnVar, o oVar) {
        super(qnVar, "resize");
        this.k = qnVar;
        this.l = qnVar.d();
        this.p = oVar;
    }

    private final void b(int i2, int i3) {
        a(i2, i3 - ax.e().c(this.l)[0], this.i, this.f);
    }

    private final int[] b() {
        boolean z;
        int i2;
        int i3;
        int[] b2 = ax.e().b(this.l);
        int[] c2 = ax.e().c(this.l);
        int i4 = b2[0];
        int i5 = b2[1];
        if (this.i < 50 || this.i > i4) {
            jm.e("Width is too small or too large.");
            z = false;
        } else if (this.f < 50 || this.f > i5) {
            jm.e("Height is too small or too large.");
            z = false;
        } else if (this.f == i5 && this.i == i4) {
            jm.e("Cannot resize to a full-screen ad.");
            z = false;
        } else {
            if (this.c) {
                String str = this.f3248b;
                char c3 = 65535;
                switch (str.hashCode()) {
                    case -1364013995:
                        if (str.equals("center")) {
                            c3 = 2;
                            break;
                        }
                        break;
                    case -1012429441:
                        if (str.equals("top-left")) {
                            c3 = 0;
                            break;
                        }
                        break;
                    case -655373719:
                        if (str.equals("bottom-left")) {
                            c3 = 3;
                            break;
                        }
                        break;
                    case 1163912186:
                        if (str.equals("bottom-right")) {
                            c3 = 5;
                            break;
                        }
                        break;
                    case 1288627767:
                        if (str.equals("bottom-center")) {
                            c3 = 4;
                            break;
                        }
                        break;
                    case 1755462605:
                        if (str.equals("top-center")) {
                            c3 = 1;
                            break;
                        }
                        break;
                }
                switch (c3) {
                    case 0:
                        i2 = this.g + this.d;
                        i3 = this.e + this.h;
                        break;
                    case 1:
                        i2 = ((this.d + this.g) + (this.i / 2)) - 25;
                        i3 = this.e + this.h;
                        break;
                    case 2:
                        i2 = ((this.d + this.g) + (this.i / 2)) - 25;
                        i3 = ((this.e + this.h) + (this.f / 2)) - 25;
                        break;
                    case 3:
                        i2 = this.g + this.d;
                        i3 = ((this.e + this.h) + this.f) - 50;
                        break;
                    case 4:
                        i2 = ((this.d + this.g) + (this.i / 2)) - 25;
                        i3 = ((this.e + this.h) + this.f) - 50;
                        break;
                    case 5:
                        i2 = ((this.d + this.g) + this.i) - 50;
                        i3 = ((this.e + this.h) + this.f) - 50;
                        break;
                    default:
                        i2 = ((this.d + this.g) + this.i) - 50;
                        i3 = this.e + this.h;
                        break;
                }
                if (i2 < 0 || i2 + 50 > i4 || i3 < c2[0] || i3 + 50 > c2[1]) {
                    z = false;
                }
            }
            z = true;
        }
        if (!z) {
            return null;
        }
        if (this.c) {
            return new int[]{this.d + this.g, this.e + this.h};
        }
        int[] b3 = ax.e().b(this.l);
        int[] c4 = ax.e().c(this.l);
        int i6 = b3[0];
        int i7 = this.d + this.g;
        int i8 = this.e + this.h;
        if (i7 < 0) {
            i7 = 0;
        } else if (this.i + i7 > i6) {
            i7 = i6 - this.i;
        }
        if (i8 < c4[0]) {
            i8 = c4[0];
        } else if (this.f + i8 > c4[1]) {
            i8 = c4[1] - this.f;
        }
        return new int[]{i7, i8};
    }

    public final void a(int i2, int i3) {
        this.d = i2;
        this.e = i3;
    }

    public final void a(int i2, int i3, boolean z) {
        synchronized (this.j) {
            this.d = i2;
            this.e = i3;
            if (this.q != null && z) {
                int[] b2 = b();
                if (b2 != null) {
                    PopupWindow popupWindow = this.q;
                    ape.a();
                    int a2 = mh.a((Context) this.l, b2[0]);
                    ape.a();
                    popupWindow.update(a2, mh.a((Context) this.l, b2[1]), this.q.getWidth(), this.q.getHeight());
                    b(b2[0], b2[1]);
                } else {
                    a(true);
                }
            }
        }
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    public final void a(Map<String, String> map) {
        char c2;
        synchronized (this.j) {
            if (this.l == null) {
                a("Not an activity context. Cannot resize.");
            } else if (this.k.t() == null) {
                a("Webview is not yet available, size is not set.");
            } else if (this.k.t().d()) {
                a("Is interstitial. Cannot resize an interstitial.");
            } else if (this.k.z()) {
                a("Cannot resize an expanded banner.");
            } else {
                if (!TextUtils.isEmpty((CharSequence) map.get("width"))) {
                    ax.e();
                    this.i = jv.b((String) map.get("width"));
                }
                if (!TextUtils.isEmpty((CharSequence) map.get("height"))) {
                    ax.e();
                    this.f = jv.b((String) map.get("height"));
                }
                if (!TextUtils.isEmpty((CharSequence) map.get("offsetX"))) {
                    ax.e();
                    this.g = jv.b((String) map.get("offsetX"));
                }
                if (!TextUtils.isEmpty((CharSequence) map.get("offsetY"))) {
                    ax.e();
                    this.h = jv.b((String) map.get("offsetY"));
                }
                if (!TextUtils.isEmpty((CharSequence) map.get("allowOffscreen"))) {
                    this.c = Boolean.parseBoolean((String) map.get("allowOffscreen"));
                }
                String str = (String) map.get("customClosePosition");
                if (!TextUtils.isEmpty(str)) {
                    this.f3248b = str;
                }
                if (!(this.i >= 0 && this.f >= 0)) {
                    a("Invalid width and height options. Cannot resize.");
                    return;
                }
                Window window = this.l.getWindow();
                if (window == null || window.getDecorView() == null) {
                    a("Activity context is not ready, cannot get window or decor view.");
                    return;
                }
                int[] b2 = b();
                if (b2 == null) {
                    a("Resize location out of screen or close button is not visible.");
                    return;
                }
                ape.a();
                int a2 = mh.a((Context) this.l, this.i);
                ape.a();
                int a3 = mh.a((Context) this.l, this.f);
                ViewParent parent = this.k.getView().getParent();
                if (parent == null || !(parent instanceof ViewGroup)) {
                    a("Webview is detached, probably in the middle of a resize or expand.");
                    return;
                }
                ((ViewGroup) parent).removeView(this.k.getView());
                if (this.q == null) {
                    this.s = (ViewGroup) parent;
                    ax.e();
                    Bitmap a4 = jv.a(this.k.getView());
                    this.n = new ImageView(this.l);
                    this.n.setImageBitmap(a4);
                    this.m = this.k.t();
                    this.s.addView(this.n);
                } else {
                    this.q.dismiss();
                }
                this.r = new RelativeLayout(this.l);
                this.r.setBackgroundColor(0);
                this.r.setLayoutParams(new LayoutParams(a2, a3));
                ax.e();
                this.q = jv.a((View) this.r, a2, a3, false);
                this.q.setOutsideTouchable(true);
                this.q.setTouchable(true);
                this.q.setClippingEnabled(!this.c);
                this.r.addView(this.k.getView(), -1, -1);
                this.o = new LinearLayout(this.l);
                ape.a();
                int a5 = mh.a((Context) this.l, 50);
                ape.a();
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(a5, mh.a((Context) this.l, 50));
                String str2 = this.f3248b;
                switch (str2.hashCode()) {
                    case -1364013995:
                        if (str2.equals("center")) {
                            c2 = 2;
                            break;
                        }
                    case -1012429441:
                        if (str2.equals("top-left")) {
                            c2 = 0;
                            break;
                        }
                    case -655373719:
                        if (str2.equals("bottom-left")) {
                            c2 = 3;
                            break;
                        }
                    case 1163912186:
                        if (str2.equals("bottom-right")) {
                            c2 = 5;
                            break;
                        }
                    case 1288627767:
                        if (str2.equals("bottom-center")) {
                            c2 = 4;
                            break;
                        }
                    case 1755462605:
                        if (str2.equals("top-center")) {
                            c2 = 1;
                            break;
                        }
                    default:
                        c2 = 65535;
                        break;
                }
                switch (c2) {
                    case 0:
                        layoutParams.addRule(10);
                        layoutParams.addRule(9);
                        break;
                    case 1:
                        layoutParams.addRule(10);
                        layoutParams.addRule(14);
                        break;
                    case 2:
                        layoutParams.addRule(13);
                        break;
                    case 3:
                        layoutParams.addRule(12);
                        layoutParams.addRule(9);
                        break;
                    case 4:
                        layoutParams.addRule(12);
                        layoutParams.addRule(14);
                        break;
                    case 5:
                        layoutParams.addRule(12);
                        layoutParams.addRule(11);
                        break;
                    default:
                        layoutParams.addRule(10);
                        layoutParams.addRule(11);
                        break;
                }
                this.o.setOnClickListener(new e(this));
                this.o.setContentDescription("Close button");
                this.r.addView(this.o, layoutParams);
                try {
                    PopupWindow popupWindow = this.q;
                    View decorView = window.getDecorView();
                    ape.a();
                    int a6 = mh.a((Context) this.l, b2[0]);
                    ape.a();
                    popupWindow.showAtLocation(decorView, 0, a6, mh.a((Context) this.l, b2[1]));
                    int i2 = b2[0];
                    int i3 = b2[1];
                    if (this.p != null) {
                        this.p.a(i2, i3, this.i, this.f);
                    }
                    this.k.a(sb.a(a2, a3));
                    b(b2[0], b2[1]);
                    c("resized");
                } catch (RuntimeException e2) {
                    String str3 = "Cannot show popup window: ";
                    String valueOf = String.valueOf(e2.getMessage());
                    a(valueOf.length() != 0 ? str3.concat(valueOf) : new String(str3));
                    this.r.removeView(this.k.getView());
                    if (this.s != null) {
                        this.s.removeView(this.n);
                        this.s.addView(this.k.getView());
                        this.k.a(this.m);
                    }
                }
            }
        }
    }

    public final void a(boolean z) {
        synchronized (this.j) {
            if (this.q != null) {
                this.q.dismiss();
                this.r.removeView(this.k.getView());
                if (this.s != null) {
                    this.s.removeView(this.n);
                    this.s.addView(this.k.getView());
                    this.k.a(this.m);
                }
                if (z) {
                    c("default");
                    if (this.p != null) {
                        this.p.N();
                    }
                }
                this.q = null;
                this.r = null;
                this.s = null;
                this.o = null;
            }
        }
    }

    public final boolean a() {
        boolean z;
        synchronized (this.j) {
            z = this.q != null;
        }
        return z;
    }
}
