package com.google.android.gms.internal.ads;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.os.Bundle;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;

@cm
public final class dm {
    public final float A;
    public final boolean B;
    public final int C;
    public final int D;
    public final boolean E;
    public final boolean F;
    public final Future<String> G;
    public final String H;
    public final boolean I;
    public final int J;
    public final Bundle K;
    public final String L;
    public final aqy M;
    public final boolean N;
    public final Bundle O;
    public final boolean P;
    public final Future<String> Q;
    public final List<Integer> R;
    public final String S;
    public final int T;
    public final boolean U;
    public final boolean V;
    public final boolean W;
    public final ArrayList<String> X;

    /* renamed from: a reason: collision with root package name */
    public final Bundle f3262a;

    /* renamed from: b reason: collision with root package name */
    public final aop f3263b;
    public final aot c;
    public final String d;
    public final ApplicationInfo e;
    public final PackageInfo f;
    public final String g;
    public final String h;
    public final Bundle i;
    public final mu j;
    public final int k;
    public final List<String> l;
    public final List<String> m;
    public final List<String> n;
    public final Bundle o;
    public final boolean p;
    public final int q;
    public final int r;
    public final float s;
    public final String t;
    public final long u;
    public final String v;
    public final List<String> w;
    public final String x;
    public final aul y;
    public final String z;

    public dm(Bundle bundle, aop aop, aot aot, String str, ApplicationInfo applicationInfo, PackageInfo packageInfo, String str2, String str3, mu muVar, Bundle bundle2, List<String> list, List<String> list2, Bundle bundle3, boolean z2, int i2, int i3, float f2, String str4, long j2, String str5, List<String> list3, String str6, aul aul, String str7, float f3, boolean z3, int i4, int i5, boolean z4, boolean z5, Future<String> future, String str8, boolean z6, int i6, Bundle bundle4, String str9, aqy aqy, boolean z7, Bundle bundle5, boolean z8, Future<String> future2, List<Integer> list4, String str10, List<String> list5, int i7, boolean z9, boolean z10, boolean z11, ArrayList<String> arrayList) {
        this.f3262a = bundle;
        this.f3263b = aop;
        this.c = aot;
        this.d = str;
        this.e = applicationInfo;
        this.f = packageInfo;
        this.g = str2;
        this.h = str3;
        this.j = muVar;
        this.i = bundle2;
        this.p = z2;
        this.q = i2;
        this.r = i3;
        this.s = f2;
        if (list == null || list.size() <= 0) {
            this.k = 0;
            this.l = null;
            this.m = null;
        } else {
            this.k = 3;
            this.l = list;
            this.m = list2;
        }
        this.o = bundle3;
        this.t = str4;
        this.u = j2;
        this.v = str5;
        this.w = list3;
        this.x = str6;
        this.y = aul;
        this.z = str7;
        this.A = f3;
        this.B = z3;
        this.C = i4;
        this.D = i5;
        this.E = z4;
        this.F = z5;
        this.G = future;
        this.H = str8;
        this.I = z6;
        this.J = i6;
        this.K = bundle4;
        this.L = str9;
        this.M = aqy;
        this.N = z7;
        this.O = bundle5;
        this.P = z8;
        this.Q = future2;
        this.R = list4;
        this.S = str10;
        this.n = list5;
        this.T = i7;
        this.U = z9;
        this.V = z10;
        this.W = z11;
        this.X = arrayList;
    }
}
