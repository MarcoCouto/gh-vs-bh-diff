package com.google.android.gms.internal.ads;

import android.net.TrafficStats;
import android.os.Process;
import android.os.SystemClock;
import java.util.concurrent.BlockingQueue;

public final class arx extends Thread {

    /* renamed from: a reason: collision with root package name */
    private final BlockingQueue<awb<?>> f2863a;

    /* renamed from: b reason: collision with root package name */
    private final are f2864b;
    private final zy c;
    private final b d;
    private volatile boolean e = false;

    public arx(BlockingQueue<awb<?>> blockingQueue, are are, zy zyVar, b bVar) {
        this.f2863a = blockingQueue;
        this.f2864b = are;
        this.c = zyVar;
        this.d = bVar;
    }

    private final void b() throws InterruptedException {
        long elapsedRealtime = SystemClock.elapsedRealtime();
        awb awb = (awb) this.f2863a.take();
        try {
            awb.b("network-queue-take");
            awb.g();
            TrafficStats.setThreadStatsTag(awb.d());
            atz a2 = this.f2864b.a(awb);
            awb.b("network-http-complete");
            if (!a2.e || !awb.l()) {
                bcd a3 = awb.a(a2);
                awb.b("network-parse-complete");
                if (awb.h() && a3.f3124b != null) {
                    this.c.a(awb.e(), a3.f3124b);
                    awb.b("network-cache-written");
                }
                awb.k();
                this.d.a(awb, a3);
                awb.a(a3);
                return;
            }
            awb.c("not-modified");
            awb.m();
        } catch (df e2) {
            e2.a(SystemClock.elapsedRealtime() - elapsedRealtime);
            this.d.a(awb, e2);
            awb.m();
        } catch (Exception e3) {
            eg.a(e3, "Unhandled exception %s", e3.toString());
            df dfVar = new df((Throwable) e3);
            dfVar.a(SystemClock.elapsedRealtime() - elapsedRealtime);
            this.d.a(awb, dfVar);
            awb.m();
        }
    }

    public final void a() {
        this.e = true;
        interrupt();
    }

    public final void run() {
        Process.setThreadPriority(10);
        while (true) {
            try {
                b();
            } catch (InterruptedException e2) {
                if (this.e) {
                    return;
                }
            }
        }
    }
}
