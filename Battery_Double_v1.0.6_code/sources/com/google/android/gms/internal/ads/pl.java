package com.google.android.gms.internal.ads;

import org.json.JSONException;
import org.json.JSONObject;

@cm
public final class pl {

    /* renamed from: a reason: collision with root package name */
    private final boolean f3604a;

    /* renamed from: b reason: collision with root package name */
    private final int f3605b;
    private final int c;
    private final int d;
    private final String e;
    private final int f;
    private final int g;
    private final int h;
    private final boolean i;

    public pl(String str) {
        JSONObject jSONObject;
        JSONObject jSONObject2 = null;
        if (str != null) {
            try {
                jSONObject = new JSONObject(str);
            } catch (JSONException e2) {
            }
        } else {
            jSONObject = null;
        }
        jSONObject2 = jSONObject;
        this.f3604a = a(jSONObject2, "aggressive_media_codec_release", asi.B);
        this.f3605b = b(jSONObject2, "byte_buffer_precache_limit", asi.m);
        this.c = b(jSONObject2, "exo_cache_buffer_size", asi.p);
        this.d = b(jSONObject2, "exo_connect_timeout_millis", asi.i);
        this.e = c(jSONObject2, "exo_player_version", asi.h);
        this.f = b(jSONObject2, "exo_read_timeout_millis", asi.j);
        this.g = b(jSONObject2, "load_check_interval_bytes", asi.k);
        this.h = b(jSONObject2, "player_precache_limit", asi.l);
        this.i = a(jSONObject2, "use_cache_data_source", asi.cH);
    }

    private static boolean a(JSONObject jSONObject, String str, ary<Boolean> ary) {
        if (jSONObject != null) {
            try {
                return jSONObject.getBoolean(str);
            } catch (JSONException e2) {
            }
        }
        return ((Boolean) ape.f().a(ary)).booleanValue();
    }

    private static int b(JSONObject jSONObject, String str, ary<Integer> ary) {
        if (jSONObject != null) {
            try {
                return jSONObject.getInt(str);
            } catch (JSONException e2) {
            }
        }
        return ((Integer) ape.f().a(ary)).intValue();
    }

    private static String c(JSONObject jSONObject, String str, ary<String> ary) {
        if (jSONObject != null) {
            try {
                return jSONObject.getString(str);
            } catch (JSONException e2) {
            }
        }
        return (String) ape.f().a(ary);
    }
}
