package com.google.android.gms.internal.ads;

import android.content.Context;
import android.content.pm.PackageInfo;
import com.google.android.gms.ads.identifier.AdvertisingIdClient.Info;

@cm
public final class io implements iq {
    public final nn<Info> a(Context context) {
        ny nyVar = new ny();
        ape.a();
        if (mh.f(context)) {
            jt.a((Runnable) new ip(this, context, nyVar));
        }
        return nyVar;
    }

    public final nn<String> a(String str, PackageInfo packageInfo) {
        return nc.a(str);
    }
}
