package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.ads.internal.bu;
import com.google.android.gms.ads.internal.m;

@cm
public final class axr {

    /* renamed from: a reason: collision with root package name */
    private final Context f2991a;

    /* renamed from: b reason: collision with root package name */
    private final bcr f2992b;
    private final mu c;
    private final bu d;

    axr(Context context, bcr bcr, mu muVar, bu buVar) {
        this.f2991a = context;
        this.f2992b = bcr;
        this.c = muVar;
        this.d = buVar;
    }

    public final Context a() {
        return this.f2991a.getApplicationContext();
    }

    public final m a(String str) {
        return new m(this.f2991a, new aot(), str, this.f2992b, this.c, this.d);
    }

    public final m b(String str) {
        return new m(this.f2991a.getApplicationContext(), new aot(), str, this.f2992b, this.c, this.d);
    }

    public final axr b() {
        return new axr(this.f2991a.getApplicationContext(), this.f2992b, this.c, this.d);
    }
}
