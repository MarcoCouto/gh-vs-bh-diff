package com.google.android.gms.internal.ads;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.Build.VERSION;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.ViewTreeObserver.OnScrollChangedListener;
import android.view.WindowManager;
import android.webkit.DownloadListener;
import android.webkit.ValueCallback;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.google.android.gms.ads.internal.aq;
import com.google.android.gms.ads.internal.ax;
import com.google.android.gms.ads.internal.bu;
import com.google.android.gms.ads.internal.gmsg.ae;
import com.google.android.gms.ads.internal.overlay.c;
import com.google.android.gms.ads.internal.overlay.d;
import com.google.android.gms.common.util.n;
import com.google.android.gms.common.util.o;
import com.google.android.gms.internal.ads.amy.a.b;
import com.hmatalonga.greenhub.Config;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

@cm
final class ra extends WebView implements OnGlobalLayoutListener, DownloadListener, qn {
    private int A;
    /* access modifiers changed from: private */
    public int B;
    private ast C;
    private ast D;
    private ast E;
    private asu F;
    private WeakReference<OnClickListener> G;
    private d H;
    private boolean I;
    private mg J;
    private int K = -1;
    private int L = -1;
    private int M = -1;
    private int N = -1;
    private Map<String, py> O;
    private final WindowManager P;
    private final amw Q;

    /* renamed from: a reason: collision with root package name */
    private final sa f3653a;

    /* renamed from: b reason: collision with root package name */
    private final ahh f3654b;
    private final mu c;
    private final aq d;
    private final bu e;
    private final DisplayMetrics f;
    private final float g;
    private boolean h = false;
    private boolean i = false;
    private qo j;
    private d k;
    private sb l;
    private String m;
    private boolean n;
    private boolean o;
    private boolean p;
    private boolean q;
    private Boolean r;
    private int s;
    private boolean t = true;
    private boolean u = false;
    private String v = "";
    private rd w;
    private boolean x;
    private boolean y;
    private atw z;

    private ra(sa saVar, sb sbVar, String str, boolean z2, boolean z3, ahh ahh, mu muVar, asv asv, aq aqVar, bu buVar, amw amw) {
        super(saVar);
        this.f3653a = saVar;
        this.l = sbVar;
        this.m = str;
        this.p = z2;
        this.s = -1;
        this.f3654b = ahh;
        this.c = muVar;
        this.d = aqVar;
        this.e = buVar;
        this.P = (WindowManager) getContext().getSystemService("window");
        ax.e();
        this.f = jv.a(this.P);
        this.g = this.f.density;
        this.Q = amw;
        setBackgroundColor(0);
        WebSettings settings = getSettings();
        settings.setAllowFileAccess(false);
        try {
            settings.setJavaScriptEnabled(true);
        } catch (NullPointerException e2) {
            jm.b("Unable to enable Javascript.", e2);
        }
        settings.setSavePassword(false);
        settings.setSupportMultipleWindows(true);
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        if (VERSION.SDK_INT >= 21) {
            settings.setMixedContentMode(2);
        }
        ax.e().a((Context) saVar, muVar.f3528a, settings);
        ax.g().a(getContext(), settings);
        setDownloadListener(this);
        O();
        if (n.e()) {
            addJavascriptInterface(rg.a(this), "googleAdsJsInterface");
        }
        if (n.a()) {
            removeJavascriptInterface("accessibility");
            removeJavascriptInterface("accessibilityTraversal");
        }
        this.J = new mg(this.f3653a.a(), this, this, null);
        T();
        this.F = new asu(new asv(true, "make_wv", this.m));
        this.F.a().a(asv);
        this.D = aso.a(this.F.a());
        this.F.a("native:view_create", this.D);
        this.E = null;
        this.C = null;
        ax.g().b((Context) saVar);
        ax.i().i();
    }

    private final boolean K() {
        int i2;
        int i3;
        if (!this.j.b() && !this.j.c()) {
            return false;
        }
        ape.a();
        int b2 = mh.b(this.f, this.f.widthPixels);
        ape.a();
        int b3 = mh.b(this.f, this.f.heightPixels);
        Activity a2 = this.f3653a.a();
        if (a2 == null || a2.getWindow() == null) {
            i2 = b3;
            i3 = b2;
        } else {
            ax.e();
            int[] a3 = jv.a(a2);
            ape.a();
            i3 = mh.b(this.f, a3[0]);
            ape.a();
            i2 = mh.b(this.f, a3[1]);
        }
        if (this.L == b2 && this.K == b3 && this.M == i3 && this.N == i2) {
            return false;
        }
        boolean z2 = (this.L == b2 && this.K == b3) ? false : true;
        this.L = b2;
        this.K = b3;
        this.M = i3;
        this.N = i2;
        new n(this).a(b2, b3, i3, i2, this.f.density, this.P.getDefaultDisplay().getRotation());
        return z2;
    }

    private final synchronized void L() {
        this.r = ax.i().c();
        if (this.r == null) {
            try {
                evaluateJavascript("(function(){})()", null);
                a(Boolean.valueOf(true));
            } catch (IllegalStateException e2) {
                a(Boolean.valueOf(false));
            }
        }
        return;
    }

    private final synchronized Boolean M() {
        return this.r;
    }

    private final void N() {
        aso.a(this.F.a(), this.D, "aeh2");
    }

    private final synchronized void O() {
        if (this.p || this.l.d()) {
            jm.b("Enabling hardware acceleration on an overlay.");
            Q();
        } else if (VERSION.SDK_INT < 18) {
            jm.b("Disabling hardware acceleration on an AdView.");
            P();
        } else {
            jm.b("Enabling hardware acceleration on an AdView.");
            Q();
        }
    }

    private final synchronized void P() {
        if (!this.q) {
            ax.g().c((View) this);
        }
        this.q = true;
    }

    private final synchronized void Q() {
        if (this.q) {
            ax.g().b((View) this);
        }
        this.q = false;
    }

    private final synchronized void R() {
        if (!this.I) {
            this.I = true;
            ax.i().j();
        }
    }

    private final synchronized void S() {
        this.O = null;
    }

    private final void T() {
        if (this.F != null) {
            asv a2 = this.F.a();
            if (a2 != null && ax.i().b() != null) {
                ax.i().b().a(a2);
            }
        }
    }

    static ra a(Context context, sb sbVar, String str, boolean z2, boolean z3, ahh ahh, mu muVar, asv asv, aq aqVar, bu buVar, amw amw) {
        return new ra(new sa(context), sbVar, str, z2, z3, ahh, muVar, asv, aqVar, buVar, amw);
    }

    private final void a(Boolean bool) {
        synchronized (this) {
            this.r = bool;
        }
        ax.i().a(bool);
    }

    @TargetApi(19)
    private final synchronized void a(String str, ValueCallback<String> valueCallback) {
        if (!A()) {
            evaluateJavascript(str, null);
        } else {
            jm.e("#004 The webview is destroyed. Ignoring action.");
        }
    }

    private final synchronized void c(String str) {
        if (!A()) {
            loadUrl(str);
        } else {
            jm.e("#004 The webview is destroyed. Ignoring action.");
        }
    }

    private final synchronized void d(String str) {
        try {
            super.loadUrl(str);
        } catch (Exception | IncompatibleClassChangeError | NoClassDefFoundError | UnsatisfiedLinkError e2) {
            ax.i().a(e2, "AdWebViewImpl.loadUrlUnsafe");
            jm.c("Could not call loadUrl. ", e2);
        }
        return;
    }

    private final void e(String str) {
        if (n.g()) {
            if (M() == null) {
                L();
            }
            if (M().booleanValue()) {
                a(str, null);
                return;
            }
            String str2 = "javascript:";
            String valueOf = String.valueOf(str);
            c(valueOf.length() != 0 ? str2.concat(valueOf) : new String(str2));
            return;
        }
        String str3 = "javascript:";
        String valueOf2 = String.valueOf(str);
        c(valueOf2.length() != 0 ? str3.concat(valueOf2) : new String(str3));
    }

    private final void f(boolean z2) {
        HashMap hashMap = new HashMap();
        hashMap.put("isVisible", z2 ? "1" : Config.NOTIFICATION_DEFAULT_PRIORITY);
        a("onAdVisibilityChanged", (Map<String, ?>) hashMap);
    }

    public final synchronized boolean A() {
        return this.o;
    }

    public final synchronized void B() {
        jm.a("Destroying WebView!");
        R();
        jv.f3440a.post(new rc(this));
    }

    public final synchronized boolean C() {
        return this.t;
    }

    public final synchronized boolean D() {
        return this.u;
    }

    public final synchronized boolean E() {
        return this.A > 0;
    }

    public final void F() {
        this.J.a();
    }

    public final void G() {
        if (this.E == null) {
            this.E = aso.a(this.F.a());
            this.F.a("native:view_load", this.E);
        }
    }

    public final synchronized atw H() {
        return this.z;
    }

    public final void I() {
        setBackgroundColor(0);
    }

    public final void J() {
        jm.a("Cannot add text view to inner AdWebView");
    }

    public final pd a() {
        return null;
    }

    public final void a(int i2) {
        if (i2 == 0) {
            aso.a(this.F.a(), this.D, "aebb2");
        }
        N();
        if (this.F.a() != null) {
            this.F.a().a("close_type", String.valueOf(i2));
        }
        HashMap hashMap = new HashMap(2);
        hashMap.put("closetype", String.valueOf(i2));
        hashMap.put("version", this.c.f3528a);
        a("onhide", (Map<String, ?>) hashMap);
    }

    public final void a(Context context) {
        this.f3653a.setBaseContext(context);
        this.J.a(this.f3653a.a());
    }

    public final void a(c cVar) {
        this.j.a(cVar);
    }

    public final synchronized void a(d dVar) {
        this.k = dVar;
    }

    public final void a(aku aku) {
        synchronized (this) {
            this.x = aku.f2678a;
        }
        f(aku.f2678a);
    }

    public final synchronized void a(atw atw) {
        this.z = atw;
    }

    public final synchronized void a(rd rdVar) {
        if (this.w != null) {
            jm.c("Attempt to create multiple AdWebViewVideoControllers.");
        } else {
            this.w = rdVar;
        }
    }

    public final synchronized void a(sb sbVar) {
        this.l = sbVar;
        requestLayout();
    }

    public final synchronized void a(String str) {
        if (str == null) {
            str = "";
        }
        this.v = str;
    }

    public final void a(String str, ae<? super qn> aeVar) {
        if (this.j != null) {
            this.j.a(str, aeVar);
        }
    }

    public final void a(String str, o<ae<? super qn>> oVar) {
        if (this.j != null) {
            this.j.a(str, oVar);
        }
    }

    public final synchronized void a(String str, String str2, String str3) {
        String str4;
        if (!A()) {
            if (((Boolean) ape.f().a(asi.aB)).booleanValue()) {
                str4 = rp.a(str2, rp.a());
            } else {
                str4 = str2;
            }
            super.loadDataWithBaseURL(str, str4, "text/html", "UTF-8", str3);
        } else {
            jm.e("#004 The webview is destroyed. Ignoring action.");
        }
    }

    public final void a(String str, Map<String, ?> map) {
        try {
            a(str, ax.e().a(map));
        } catch (JSONException e2) {
            jm.e("Could not convert parameters to JSON.");
        }
    }

    public final void a(String str, JSONObject jSONObject) {
        if (jSONObject == null) {
            jSONObject = new JSONObject();
        }
        String jSONObject2 = jSONObject.toString();
        StringBuilder sb = new StringBuilder();
        sb.append("(window.AFMA_ReceiveMessage || function() {})('");
        sb.append(str);
        sb.append("'");
        sb.append(",");
        sb.append(jSONObject2);
        sb.append(");");
        String str2 = "Dispatching AFMA event: ";
        String valueOf = String.valueOf(sb.toString());
        jm.b(valueOf.length() != 0 ? str2.concat(valueOf) : new String(str2));
        e(sb.toString());
    }

    public final void a(boolean z2) {
        this.j.a(z2);
    }

    public final void a(boolean z2, int i2) {
        this.j.a(z2, i2);
    }

    public final void a(boolean z2, int i2, String str) {
        this.j.a(z2, i2, str);
    }

    public final void a(boolean z2, int i2, String str, String str2) {
        this.j.a(z2, i2, str, str2);
    }

    public final synchronized rd b() {
        return this.w;
    }

    public final synchronized void b(d dVar) {
        this.H = dVar;
    }

    public final void b(String str) {
        e(str);
    }

    public final void b(String str, ae<? super qn> aeVar) {
        if (this.j != null) {
            this.j.b(str, aeVar);
        }
    }

    public final void b(String str, JSONObject jSONObject) {
        if (jSONObject == null) {
            jSONObject = new JSONObject();
        }
        String jSONObject2 = jSONObject.toString();
        e(new StringBuilder(String.valueOf(str).length() + 3 + String.valueOf(jSONObject2).length()).append(str).append("(").append(jSONObject2).append(");").toString());
    }

    public final synchronized void b(boolean z2) {
        boolean z3 = z2 != this.p;
        this.p = z2;
        O();
        if (z3) {
            new n(this).c(z2 ? "expanded" : "default");
        }
    }

    public final ast c() {
        return this.D;
    }

    public final synchronized void c(boolean z2) {
        if (this.k != null) {
            this.k.a(this.j.b(), z2);
        } else {
            this.n = z2;
        }
    }

    public final Activity d() {
        return this.f3653a.a();
    }

    public final synchronized void d(boolean z2) {
        this.t = z2;
    }

    public final synchronized void destroy() {
        T();
        this.J.b();
        if (this.k != null) {
            this.k.a();
            this.k.k();
            this.k = null;
        }
        this.j.k();
        if (!this.o) {
            ax.z();
            px.a((pm) this);
            S();
            this.o = true;
            jm.a("Initiating WebView self destruct sequence in 3...");
            jm.a("Loading blank page in WebView, 2...");
            d("about:blank");
        }
    }

    public final bu e() {
        return this.e;
    }

    public final synchronized void e(boolean z2) {
        this.A = (z2 ? 1 : -1) + this.A;
        if (this.A <= 0 && this.k != null) {
            this.k.q();
        }
    }

    @TargetApi(19)
    public final synchronized void evaluateJavascript(String str, ValueCallback<String> valueCallback) {
        if (A()) {
            jm.f("#004 The webview is destroyed. Ignoring action.");
            if (valueCallback != null) {
                valueCallback.onReceiveValue(null);
            }
        } else {
            super.evaluateJavascript(str, valueCallback);
        }
    }

    public final void f() {
        d r2 = r();
        if (r2 != null) {
            r2.p();
        }
    }

    /* access modifiers changed from: protected */
    public final void finalize() throws Throwable {
        try {
            synchronized (this) {
                if (!this.o) {
                    this.j.k();
                    ax.z();
                    px.a((pm) this);
                    S();
                    R();
                }
            }
        } finally {
            super.finalize();
        }
    }

    public final synchronized String g() {
        return this.v;
    }

    public final OnClickListener getOnClickListener() {
        return (OnClickListener) this.G.get();
    }

    public final synchronized int getRequestedOrientation() {
        return this.s;
    }

    public final View getView() {
        return this;
    }

    public final WebView getWebView() {
        return this;
    }

    public final synchronized void h_() {
        this.u = true;
        if (this.d != null) {
            this.d.h_();
        }
    }

    public final synchronized void i_() {
        this.u = false;
        if (this.d != null) {
            this.d.i_();
        }
    }

    public final asu j() {
        return this.F;
    }

    public final mu k() {
        return this.c;
    }

    public final int l() {
        return getMeasuredHeight();
    }

    public final synchronized void loadData(String str, String str2, String str3) {
        if (!A()) {
            super.loadData(str, str2, str3);
        } else {
            jm.e("#004 The webview is destroyed. Ignoring action.");
        }
    }

    public final synchronized void loadDataWithBaseURL(String str, String str2, String str3, String str4, String str5) {
        if (!A()) {
            super.loadDataWithBaseURL(str, str2, str3, str4, str5);
        } else {
            jm.e("#004 The webview is destroyed. Ignoring action.");
        }
    }

    public final synchronized void loadUrl(String str) {
        if (!A()) {
            try {
                super.loadUrl(str);
            } catch (Exception | IncompatibleClassChangeError | NoClassDefFoundError e2) {
                ax.i().a(e2, "AdWebViewImpl.loadUrl");
                jm.c("Could not call loadUrl. ", e2);
            }
        } else {
            jm.e("#004 The webview is destroyed. Ignoring action.");
        }
        return;
    }

    public final int m() {
        return getMeasuredWidth();
    }

    public final void n() {
        N();
        HashMap hashMap = new HashMap(1);
        hashMap.put("version", this.c.f3528a);
        a("onhide", (Map<String, ?>) hashMap);
    }

    public final void o() {
        if (this.C == null) {
            aso.a(this.F.a(), this.D, "aes2");
            this.C = aso.a(this.F.a());
            this.F.a("native:view_show", this.C);
        }
        HashMap hashMap = new HashMap(1);
        hashMap.put("version", this.c.f3528a);
        a("onshow", (Map<String, ?>) hashMap);
    }

    /* access modifiers changed from: protected */
    public final synchronized void onAttachedToWindow() {
        boolean z2;
        super.onAttachedToWindow();
        if (!A()) {
            this.J.c();
        }
        boolean z3 = this.x;
        if (this.j == null || !this.j.c()) {
            z2 = z3;
        } else {
            if (!this.y) {
                OnGlobalLayoutListener d2 = this.j.d();
                if (d2 != null) {
                    ax.A();
                    if (this == null) {
                        throw null;
                    }
                    og.a((View) this, d2);
                }
                OnScrollChangedListener e2 = this.j.e();
                if (e2 != null) {
                    ax.A();
                    if (this == null) {
                        throw null;
                    }
                    og.a((View) this, e2);
                }
                this.y = true;
            }
            K();
            z2 = true;
        }
        f(z2);
    }

    /* access modifiers changed from: protected */
    public final void onDetachedFromWindow() {
        synchronized (this) {
            if (!A()) {
                this.J.d();
            }
            super.onDetachedFromWindow();
            if (this.y && this.j != null && this.j.c() && getViewTreeObserver() != null && getViewTreeObserver().isAlive()) {
                OnGlobalLayoutListener d2 = this.j.d();
                if (d2 != null) {
                    ax.g().a(getViewTreeObserver(), d2);
                }
                OnScrollChangedListener e2 = this.j.e();
                if (e2 != null) {
                    getViewTreeObserver().removeOnScrollChangedListener(e2);
                }
                this.y = false;
            }
        }
        f(false);
    }

    public final void onDownloadStart(String str, String str2, String str3, String str4, long j2) {
        try {
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setDataAndType(Uri.parse(str), str4);
            ax.e();
            jv.a(getContext(), intent);
        } catch (ActivityNotFoundException e2) {
            jm.b(new StringBuilder(String.valueOf(str).length() + 51 + String.valueOf(str4).length()).append("Couldn't find an Activity to view url/mimetype: ").append(str).append(" / ").append(str4).toString());
        }
    }

    /* access modifiers changed from: protected */
    @TargetApi(21)
    public final void onDraw(Canvas canvas) {
        if (!A()) {
            if (VERSION.SDK_INT != 21 || !canvas.isHardwareAccelerated() || isAttachedToWindow()) {
                super.onDraw(canvas);
                if (this.j != null && this.j.l() != null) {
                    this.j.l().a();
                }
            }
        }
    }

    public final boolean onGenericMotionEvent(MotionEvent motionEvent) {
        if (((Boolean) ape.f().a(asi.ay)).booleanValue()) {
            float axisValue = motionEvent.getAxisValue(9);
            float axisValue2 = motionEvent.getAxisValue(10);
            if (motionEvent.getActionMasked() == 8 && ((axisValue > 0.0f && !canScrollVertically(-1)) || ((axisValue < 0.0f && !canScrollVertically(1)) || ((axisValue2 > 0.0f && !canScrollHorizontally(-1)) || (axisValue2 < 0.0f && !canScrollHorizontally(1)))))) {
                return false;
            }
        }
        return super.onGenericMotionEvent(motionEvent);
    }

    public final void onGlobalLayout() {
        boolean K2 = K();
        d r2 = r();
        if (r2 != null && K2) {
            r2.o();
        }
    }

    /* access modifiers changed from: protected */
    @SuppressLint({"DrawAllocation"})
    public final synchronized void onMeasure(int i2, int i3) {
        boolean z2;
        int size;
        if (A()) {
            setMeasuredDimension(0, 0);
        } else if (isInEditMode() || this.p || this.l.e()) {
            super.onMeasure(i2, i3);
        } else if (this.l.f()) {
            rd b2 = b();
            float f2 = b2 != null ? b2.e() : 0.0f;
            if (f2 == 0.0f) {
                super.onMeasure(i2, i3);
            } else {
                int size2 = MeasureSpec.getSize(i2);
                int size3 = MeasureSpec.getSize(i3);
                int i4 = (int) (((float) size3) * f2);
                int i5 = (int) (((float) size2) / f2);
                if (size3 == 0 && i5 != 0) {
                    i4 = (int) (((float) i5) * f2);
                    size3 = i5;
                } else if (size2 == 0 && i4 != 0) {
                    i5 = (int) (((float) i4) / f2);
                    size2 = i4;
                }
                setMeasuredDimension(Math.min(i4, size2), Math.min(i5, size3));
            }
        } else if (this.l.c()) {
            if (((Boolean) ape.f().a(asi.cm)).booleanValue() || !n.e()) {
                super.onMeasure(i2, i3);
            } else {
                a("/contentHeight", (ae<? super qn>) new rb<Object>(this));
                e("(function() {  var height = -1;  if (document.body) {    height = document.body.offsetHeight;  } else if (document.documentElement) {    height = document.documentElement.offsetHeight;  }  var url = 'gmsg://mobileads.google.com/contentHeight?';  url += 'height=' + height;  try {    window.googleAdsJsInterface.notify(url);  } catch (e) {    var frame = document.getElementById('afma-notify-fluid');    if (!frame) {      frame = document.createElement('IFRAME');      frame.id = 'afma-notify-fluid';      frame.style.display = 'none';      var body = document.body || document.documentElement;      body.appendChild(frame);    }    frame.src = url;  }})();");
                float f3 = this.f.density;
                int size4 = MeasureSpec.getSize(i2);
                switch (this.B) {
                    case -1:
                        size = MeasureSpec.getSize(i3);
                        break;
                    default:
                        size = (int) (f3 * ((float) this.B));
                        break;
                }
                setMeasuredDimension(size4, size);
            }
        } else if (this.l.d()) {
            setMeasuredDimension(this.f.widthPixels, this.f.heightPixels);
        } else {
            int mode = MeasureSpec.getMode(i2);
            int size5 = MeasureSpec.getSize(i2);
            int mode2 = MeasureSpec.getMode(i3);
            int size6 = MeasureSpec.getSize(i3);
            int i6 = (mode == Integer.MIN_VALUE || mode == 1073741824) ? size5 : Integer.MAX_VALUE;
            int i7 = (mode2 == Integer.MIN_VALUE || mode2 == 1073741824) ? size6 : Integer.MAX_VALUE;
            boolean z3 = this.l.f3673b > i6 || this.l.f3672a > i7;
            if (((Boolean) ape.f().a(asi.dh)).booleanValue()) {
                z2 = ((float) this.l.f3673b) / this.g <= ((float) i6) / this.g && ((float) this.l.f3672a) / this.g <= ((float) i7) / this.g;
                if (!z3) {
                    z2 = z3;
                }
            } else {
                z2 = z3;
            }
            if (z2) {
                jm.e("Not enough space to show ad. Needs " + ((int) (((float) this.l.f3673b) / this.g)) + "x" + ((int) (((float) this.l.f3672a) / this.g)) + " dp, but only has " + ((int) (((float) size5) / this.g)) + "x" + ((int) (((float) size6) / this.g)) + " dp.");
                if (getVisibility() != 8) {
                    setVisibility(4);
                }
                setMeasuredDimension(0, 0);
                if (!this.h) {
                    this.Q.a(b.BANNER_SIZE_INVALID);
                    this.h = true;
                }
            } else {
                if (getVisibility() != 8) {
                    setVisibility(0);
                }
                if (!this.i) {
                    this.Q.a(b.BANNER_SIZE_VALID);
                    this.i = true;
                }
                setMeasuredDimension(this.l.f3673b, this.l.f3672a);
            }
        }
    }

    public final void onPause() {
        if (!A()) {
            try {
                if (n.a()) {
                    super.onPause();
                }
            } catch (Exception e2) {
                jm.b("Could not pause webview.", e2);
            }
        }
    }

    public final void onResume() {
        if (!A()) {
            try {
                if (n.a()) {
                    super.onResume();
                }
            } catch (Exception e2) {
                jm.b("Could not resume webview.", e2);
            }
        }
    }

    public final boolean onTouchEvent(MotionEvent motionEvent) {
        if (this.j.c()) {
            synchronized (this) {
                if (this.z != null) {
                    this.z.a(motionEvent);
                }
            }
        } else if (this.f3654b != null) {
            this.f3654b.a(motionEvent);
        }
        if (A()) {
            return false;
        }
        return super.onTouchEvent(motionEvent);
    }

    public final void p() {
        HashMap hashMap = new HashMap(3);
        hashMap.put("app_muted", String.valueOf(ax.D().b()));
        hashMap.put("app_volume", String.valueOf(ax.D().a()));
        hashMap.put("device_volume", String.valueOf(kn.a(getContext())));
        a("volume", (Map<String, ?>) hashMap);
    }

    public final Context q() {
        return this.f3653a.b();
    }

    public final synchronized d r() {
        return this.k;
    }

    public final synchronized d s() {
        return this.H;
    }

    public final void setOnClickListener(OnClickListener onClickListener) {
        this.G = new WeakReference<>(onClickListener);
        super.setOnClickListener(onClickListener);
    }

    public final synchronized void setRequestedOrientation(int i2) {
        this.s = i2;
        if (this.k != null) {
            this.k.a(this.s);
        }
    }

    public final void setWebViewClient(WebViewClient webViewClient) {
        super.setWebViewClient(webViewClient);
        if (webViewClient instanceof qo) {
            this.j = (qo) webViewClient;
        }
    }

    public final void stopLoading() {
        if (!A()) {
            try {
                super.stopLoading();
            } catch (Exception e2) {
                jm.b("Could not stop loading webview.", e2);
            }
        }
    }

    public final synchronized sb t() {
        return this.l;
    }

    public final synchronized String u() {
        return this.m;
    }

    public final /* synthetic */ rv v() {
        return this.j;
    }

    public final WebViewClient w() {
        return this.j;
    }

    public final synchronized boolean x() {
        return this.n;
    }

    public final ahh y() {
        return this.f3654b;
    }

    public final synchronized boolean z() {
        return this.p;
    }
}
