package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.xn.b;
import java.security.GeneralSecurityException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class uh {

    /* renamed from: a reason: collision with root package name */
    private static final Logger f3721a = Logger.getLogger(uh.class.getName());

    /* renamed from: b reason: collision with root package name */
    private static final ConcurrentMap<String, tz> f3722b = new ConcurrentHashMap();
    private static final ConcurrentMap<String, Boolean> c = new ConcurrentHashMap();
    private static final ConcurrentMap<String, tt> d = new ConcurrentHashMap();

    public static <P> acw a(String str, acw acw) throws GeneralSecurityException {
        tz b2 = b(str);
        if (((Boolean) c.get(str)).booleanValue()) {
            return b2.b(acw);
        }
        String str2 = "newKey-operation not permitted for key type ";
        String valueOf = String.valueOf(str);
        throw new GeneralSecurityException(valueOf.length() != 0 ? str2.concat(valueOf) : new String(str2));
    }

    public static <P> tt<P> a(String str) throws GeneralSecurityException {
        if (str == null) {
            throw new IllegalArgumentException("catalogueName must be non-null.");
        }
        tt<P> ttVar = (tt) d.get(str.toLowerCase());
        if (ttVar != null) {
            return ttVar;
        }
        String format = String.format("no catalogue found for %s. ", new Object[]{str});
        if (str.toLowerCase().startsWith("tinkaead")) {
            format = String.valueOf(format).concat("Maybe call AeadConfig.init().");
        }
        if (str.toLowerCase().startsWith("tinkdeterministicaead")) {
            format = String.valueOf(format).concat("Maybe call DeterministicAeadConfig.init().");
        } else if (str.toLowerCase().startsWith("tinkstreamingaead")) {
            format = String.valueOf(format).concat("Maybe call StreamingAeadConfig.init().");
        } else if (str.toLowerCase().startsWith("tinkhybriddecrypt") || str.toLowerCase().startsWith("tinkhybridencrypt")) {
            format = String.valueOf(format).concat("Maybe call HybridConfig.init().");
        } else if (str.toLowerCase().startsWith("tinkmac")) {
            format = String.valueOf(format).concat("Maybe call MacConfig.init().");
        } else if (str.toLowerCase().startsWith("tinkpublickeysign") || str.toLowerCase().startsWith("tinkpublickeyverify")) {
            format = String.valueOf(format).concat("Maybe call SignatureConfig.init().");
        } else if (str.toLowerCase().startsWith("tink")) {
            format = String.valueOf(format).concat("Maybe call TinkConfig.init().");
        }
        throw new GeneralSecurityException(format);
    }

    public static <P> uf<P> a(ua uaVar, tz<P> tzVar) throws GeneralSecurityException {
        ui.b(uaVar.a());
        uf<P> ufVar = new uf<>();
        for (b bVar : uaVar.a().b()) {
            if (bVar.c() == xh.ENABLED) {
                ug a2 = ufVar.a(a(bVar.b().a(), bVar.b().b()), bVar);
                if (bVar.d() == uaVar.a().a()) {
                    ufVar.a(a2);
                }
            }
        }
        return ufVar;
    }

    public static <P> xe a(xj xjVar) throws GeneralSecurityException {
        tz b2 = b(xjVar.a());
        if (((Boolean) c.get(xjVar.a())).booleanValue()) {
            return b2.c(xjVar.b());
        }
        String str = "newKey-operation not permitted for key type ";
        String valueOf = String.valueOf(xjVar.a());
        throw new GeneralSecurityException(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
    }

    private static <P> P a(String str, aah aah) throws GeneralSecurityException {
        return b(str).a(aah);
    }

    public static <P> P a(String str, byte[] bArr) throws GeneralSecurityException {
        return a(str, aah.a(bArr));
    }

    public static synchronized <P> void a(String str, tt<P> ttVar) throws GeneralSecurityException {
        synchronized (uh.class) {
            if (d.containsKey(str.toLowerCase())) {
                if (!ttVar.getClass().equals(((tt) d.get(str.toLowerCase())).getClass())) {
                    Logger logger = f3721a;
                    Level level = Level.WARNING;
                    String str2 = "com.google.crypto.tink.Registry";
                    String str3 = "addCatalogue";
                    String str4 = "Attempted overwrite of a catalogueName catalogue for name ";
                    String valueOf = String.valueOf(str);
                    logger.logp(level, str2, str3, valueOf.length() != 0 ? str4.concat(valueOf) : new String(str4));
                    throw new GeneralSecurityException(new StringBuilder(String.valueOf(str).length() + 47).append("catalogue for name ").append(str).append(" has been already registered").toString());
                }
            }
            d.put(str.toLowerCase(), ttVar);
        }
    }

    public static <P> void a(String str, tz<P> tzVar) throws GeneralSecurityException {
        a(str, tzVar, true);
    }

    public static synchronized <P> void a(String str, tz<P> tzVar, boolean z) throws GeneralSecurityException {
        synchronized (uh.class) {
            if (tzVar == null) {
                throw new IllegalArgumentException("key manager must be non-null.");
            }
            if (f3722b.containsKey(str)) {
                tz b2 = b(str);
                boolean booleanValue = ((Boolean) c.get(str)).booleanValue();
                if (!tzVar.getClass().equals(b2.getClass()) || (!booleanValue && z)) {
                    Logger logger = f3721a;
                    Level level = Level.WARNING;
                    String str2 = "com.google.crypto.tink.Registry";
                    String str3 = "registerKeyManager";
                    String str4 = "Attempted overwrite of a registered key manager for key type ";
                    String valueOf = String.valueOf(str);
                    logger.logp(level, str2, str3, valueOf.length() != 0 ? str4.concat(valueOf) : new String(str4));
                    throw new GeneralSecurityException(String.format("typeUrl (%s) is already registered with %s, cannot be re-registered with %s", new Object[]{str, b2.getClass().getName(), tzVar.getClass().getName()}));
                }
            }
            f3722b.put(str, tzVar);
            c.put(str, Boolean.valueOf(z));
        }
    }

    public static <P> acw b(xj xjVar) throws GeneralSecurityException {
        tz b2 = b(xjVar.a());
        if (((Boolean) c.get(xjVar.a())).booleanValue()) {
            return b2.b(xjVar.b());
        }
        String str = "newKey-operation not permitted for key type ";
        String valueOf = String.valueOf(xjVar.a());
        throw new GeneralSecurityException(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
    }

    private static <P> tz<P> b(String str) throws GeneralSecurityException {
        tz<P> tzVar = (tz) f3722b.get(str);
        if (tzVar != null) {
            return tzVar;
        }
        throw new GeneralSecurityException(new StringBuilder(String.valueOf(str).length() + 78).append("No key manager found for key type: ").append(str).append(".  Check the configuration of the registry.").toString());
    }

    public static <P> P b(String str, acw acw) throws GeneralSecurityException {
        return b(str).a(acw);
    }
}
