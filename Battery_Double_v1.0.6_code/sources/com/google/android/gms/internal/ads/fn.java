package com.google.android.gms.internal.ads;

import android.content.Context;
import android.content.SharedPreferences;
import com.google.android.gms.ads.internal.ax;
import com.google.android.gms.dynamite.descriptors.com.google.android.gms.ads.dynamite.ModuleDescriptor;
import org.json.JSONException;
import org.json.JSONObject;

@cm
public final class fn extends fp {

    /* renamed from: a reason: collision with root package name */
    private final Object f3323a = new Object();

    /* renamed from: b reason: collision with root package name */
    private final Context f3324b;
    private SharedPreferences c;
    private final bbi<JSONObject, JSONObject> d;

    public fn(Context context, bbi<JSONObject, JSONObject> bbi) {
        this.f3324b = context.getApplicationContext();
        this.d = bbi;
    }

    public final nn<Void> a() {
        synchronized (this.f3323a) {
            if (this.c == null) {
                this.c = this.f3324b.getSharedPreferences("google_ads_flags_meta", 0);
            }
        }
        if (ax.l().a() - this.c.getLong("js_last_update", 0) < ((Long) ape.f().a(asi.bU)).longValue()) {
            return nc.a(null);
        }
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("js", mu.a().f3528a);
            jSONObject.put("mf", ape.f().a(asi.bV));
            jSONObject.put("cl", "193400285");
            jSONObject.put("rapid_rc", "dev");
            jSONObject.put("rapid_rollup", "HEAD");
            jSONObject.put("dynamite_version", ModuleDescriptor.MODULE_VERSION);
            return nc.a(this.d.b(jSONObject), (my<A, B>) new fo<A,B>(this), nt.f3559b);
        } catch (JSONException e) {
            jm.b("Unable to populate SDK Core Constants parameters.", e);
            return nc.a(null);
        }
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ Void a(JSONObject jSONObject) {
        asi.a(this.f3324b, 1, jSONObject);
        this.c.edit().putLong("js_last_update", ax.l().a()).apply();
        return null;
    }
}
