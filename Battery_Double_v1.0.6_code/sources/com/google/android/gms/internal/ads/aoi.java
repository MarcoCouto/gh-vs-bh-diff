package com.google.android.gms.internal.ads;

import java.io.IOException;

public final class aoi extends afh<aoi> {

    /* renamed from: a reason: collision with root package name */
    private Integer f2804a;

    /* renamed from: b reason: collision with root package name */
    private aob f2805b;

    public aoi() {
        this.f2804a = null;
        this.f2805b = null;
        this.Y = null;
        this.Z = -1;
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public final aoi a(afd afd) throws IOException {
        while (true) {
            int a2 = afd.a();
            switch (a2) {
                case 0:
                    break;
                case 8:
                    int j = afd.j();
                    try {
                        this.f2804a = Integer.valueOf(anf.a(afd.g()));
                        continue;
                    } catch (IllegalArgumentException e) {
                        afd.e(j);
                        a(afd, a2);
                        break;
                    }
                case 18:
                    if (this.f2805b == null) {
                        this.f2805b = new aob();
                    }
                    afd.a((afn) this.f2805b);
                    continue;
                default:
                    if (!super.a(afd, a2)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    /* access modifiers changed from: protected */
    public final int a() {
        int a2 = super.a();
        if (this.f2804a != null) {
            a2 += aff.b(1, this.f2804a.intValue());
        }
        return this.f2805b != null ? a2 + aff.b(2, (afn) this.f2805b) : a2;
    }

    public final void a(aff aff) throws IOException {
        if (this.f2804a != null) {
            aff.a(1, this.f2804a.intValue());
        }
        if (this.f2805b != null) {
            aff.a(2, (afn) this.f2805b);
        }
        super.a(aff);
    }
}
