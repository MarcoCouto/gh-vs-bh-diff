package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import com.google.android.gms.common.util.n;

@cm
public final class alj {

    /* renamed from: a reason: collision with root package name */
    private final Object f2696a = new Object();

    /* renamed from: b reason: collision with root package name */
    private alk f2697b = null;
    private boolean c = false;

    public final Activity a() {
        Activity activity = null;
        synchronized (this.f2696a) {
            if (n.b()) {
                if (this.f2697b != null) {
                    activity = this.f2697b.a();
                }
            }
        }
        return activity;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:36:?, code lost:
        return;
     */
    public final void a(Context context) {
        synchronized (this.f2696a) {
            if (!this.c) {
                if (n.b()) {
                    if (((Boolean) ape.f().a(asi.aG)).booleanValue()) {
                        Context applicationContext = context.getApplicationContext();
                        if (applicationContext == null) {
                            applicationContext = context;
                        }
                        Application application = applicationContext instanceof Application ? (Application) applicationContext : null;
                        if (application == null) {
                            jm.e("Can not cast Context to Application");
                            return;
                        }
                        if (this.f2697b == null) {
                            this.f2697b = new alk();
                        }
                        this.f2697b.a(application, context);
                        this.c = true;
                    }
                }
            }
        }
    }

    public final void a(alm alm) {
        synchronized (this.f2696a) {
            if (n.b()) {
                if (((Boolean) ape.f().a(asi.aG)).booleanValue()) {
                    if (this.f2697b == null) {
                        this.f2697b = new alk();
                    }
                    this.f2697b.a(alm);
                }
            }
        }
    }

    public final Context b() {
        Context context = null;
        synchronized (this.f2696a) {
            if (n.b()) {
                if (this.f2697b != null) {
                    context = this.f2697b.b();
                }
            }
        }
        return context;
    }
}
