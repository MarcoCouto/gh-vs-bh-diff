package com.google.android.gms.internal.ads;

import java.io.IOException;

public final class aba extends afh<aba> {
    private static volatile aba[] t;

    /* renamed from: a reason: collision with root package name */
    public Long f2411a;

    /* renamed from: b reason: collision with root package name */
    public Long f2412b;
    public Long c;
    public Long d;
    public Long e;
    public Long f;
    public Integer g;
    public Long h;
    public Long i;
    public Long j;
    public Integer k;
    public Long l;
    public Long m;
    public Long n;
    public Long o;
    public Long p;
    public Long q;
    public Long r;
    public Long s;
    private Long u;
    private Long v;

    public aba() {
        this.f2411a = null;
        this.f2412b = null;
        this.c = null;
        this.d = null;
        this.e = null;
        this.f = null;
        this.h = null;
        this.i = null;
        this.j = null;
        this.l = null;
        this.m = null;
        this.n = null;
        this.o = null;
        this.p = null;
        this.q = null;
        this.r = null;
        this.s = null;
        this.u = null;
        this.v = null;
        this.Z = -1;
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public final aba a(afd afd) throws IOException {
        while (true) {
            int a2 = afd.a();
            switch (a2) {
                case 0:
                    break;
                case 8:
                    this.f2411a = Long.valueOf(afd.h());
                    continue;
                case 16:
                    this.f2412b = Long.valueOf(afd.h());
                    continue;
                case 24:
                    this.c = Long.valueOf(afd.h());
                    continue;
                case 32:
                    this.d = Long.valueOf(afd.h());
                    continue;
                case 40:
                    this.e = Long.valueOf(afd.h());
                    continue;
                case 48:
                    this.f = Long.valueOf(afd.h());
                    continue;
                case 56:
                    int j2 = afd.j();
                    try {
                        this.g = Integer.valueOf(yx.a(afd.g()));
                        continue;
                    } catch (IllegalArgumentException e2) {
                        afd.e(j2);
                        a(afd, a2);
                        break;
                    }
                case 64:
                    this.h = Long.valueOf(afd.h());
                    continue;
                case 72:
                    this.i = Long.valueOf(afd.h());
                    continue;
                case 80:
                    this.j = Long.valueOf(afd.h());
                    continue;
                case 88:
                    int j3 = afd.j();
                    try {
                        this.k = Integer.valueOf(yx.a(afd.g()));
                        continue;
                    } catch (IllegalArgumentException e3) {
                        afd.e(j3);
                        a(afd, a2);
                        break;
                    }
                case 96:
                    this.l = Long.valueOf(afd.h());
                    continue;
                case 104:
                    this.m = Long.valueOf(afd.h());
                    continue;
                case 112:
                    this.n = Long.valueOf(afd.h());
                    continue;
                case 120:
                    this.o = Long.valueOf(afd.h());
                    continue;
                case 128:
                    this.p = Long.valueOf(afd.h());
                    continue;
                case 136:
                    this.q = Long.valueOf(afd.h());
                    continue;
                case 144:
                    this.r = Long.valueOf(afd.h());
                    continue;
                case 152:
                    this.s = Long.valueOf(afd.h());
                    continue;
                case 160:
                    this.u = Long.valueOf(afd.h());
                    continue;
                case 168:
                    this.v = Long.valueOf(afd.h());
                    continue;
                default:
                    if (!super.a(afd, a2)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    public static aba[] b() {
        if (t == null) {
            synchronized (afl.f2531b) {
                if (t == null) {
                    t = new aba[0];
                }
            }
        }
        return t;
    }

    /* access modifiers changed from: protected */
    public final int a() {
        int a2 = super.a();
        if (this.f2411a != null) {
            a2 += aff.d(1, this.f2411a.longValue());
        }
        if (this.f2412b != null) {
            a2 += aff.d(2, this.f2412b.longValue());
        }
        if (this.c != null) {
            a2 += aff.d(3, this.c.longValue());
        }
        if (this.d != null) {
            a2 += aff.d(4, this.d.longValue());
        }
        if (this.e != null) {
            a2 += aff.d(5, this.e.longValue());
        }
        if (this.f != null) {
            a2 += aff.d(6, this.f.longValue());
        }
        if (this.g != null) {
            a2 += aff.b(7, this.g.intValue());
        }
        if (this.h != null) {
            a2 += aff.d(8, this.h.longValue());
        }
        if (this.i != null) {
            a2 += aff.d(9, this.i.longValue());
        }
        if (this.j != null) {
            a2 += aff.d(10, this.j.longValue());
        }
        if (this.k != null) {
            a2 += aff.b(11, this.k.intValue());
        }
        if (this.l != null) {
            a2 += aff.d(12, this.l.longValue());
        }
        if (this.m != null) {
            a2 += aff.d(13, this.m.longValue());
        }
        if (this.n != null) {
            a2 += aff.d(14, this.n.longValue());
        }
        if (this.o != null) {
            a2 += aff.d(15, this.o.longValue());
        }
        if (this.p != null) {
            a2 += aff.d(16, this.p.longValue());
        }
        if (this.q != null) {
            a2 += aff.d(17, this.q.longValue());
        }
        if (this.r != null) {
            a2 += aff.d(18, this.r.longValue());
        }
        if (this.s != null) {
            a2 += aff.d(19, this.s.longValue());
        }
        if (this.u != null) {
            a2 += aff.d(20, this.u.longValue());
        }
        return this.v != null ? a2 + aff.d(21, this.v.longValue()) : a2;
    }

    public final void a(aff aff) throws IOException {
        if (this.f2411a != null) {
            aff.b(1, this.f2411a.longValue());
        }
        if (this.f2412b != null) {
            aff.b(2, this.f2412b.longValue());
        }
        if (this.c != null) {
            aff.b(3, this.c.longValue());
        }
        if (this.d != null) {
            aff.b(4, this.d.longValue());
        }
        if (this.e != null) {
            aff.b(5, this.e.longValue());
        }
        if (this.f != null) {
            aff.b(6, this.f.longValue());
        }
        if (this.g != null) {
            aff.a(7, this.g.intValue());
        }
        if (this.h != null) {
            aff.b(8, this.h.longValue());
        }
        if (this.i != null) {
            aff.b(9, this.i.longValue());
        }
        if (this.j != null) {
            aff.b(10, this.j.longValue());
        }
        if (this.k != null) {
            aff.a(11, this.k.intValue());
        }
        if (this.l != null) {
            aff.b(12, this.l.longValue());
        }
        if (this.m != null) {
            aff.b(13, this.m.longValue());
        }
        if (this.n != null) {
            aff.b(14, this.n.longValue());
        }
        if (this.o != null) {
            aff.b(15, this.o.longValue());
        }
        if (this.p != null) {
            aff.b(16, this.p.longValue());
        }
        if (this.q != null) {
            aff.b(17, this.q.longValue());
        }
        if (this.r != null) {
            aff.b(18, this.r.longValue());
        }
        if (this.s != null) {
            aff.b(19, this.s.longValue());
        }
        if (this.u != null) {
            aff.b(20, this.u.longValue());
        }
        if (this.v != null) {
            aff.b(21, this.v.longValue());
        }
        super.a(aff);
    }
}
