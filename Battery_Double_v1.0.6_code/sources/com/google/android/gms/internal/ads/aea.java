package com.google.android.gms.internal.ads;

import java.util.Iterator;
import java.util.Map.Entry;

final class aea implements Iterator<Entry<K, V>> {

    /* renamed from: a reason: collision with root package name */
    private int f2497a;

    /* renamed from: b reason: collision with root package name */
    private boolean f2498b;
    private Iterator<Entry<K, V>> c;
    private final /* synthetic */ ads d;

    private aea(ads ads) {
        this.d = ads;
        this.f2497a = -1;
    }

    /* synthetic */ aea(ads ads, adt adt) {
        this(ads);
    }

    private final Iterator<Entry<K, V>> a() {
        if (this.c == null) {
            this.c = this.d.c.entrySet().iterator();
        }
        return this.c;
    }

    public final boolean hasNext() {
        return this.f2497a + 1 < this.d.f2489b.size() || (!this.d.c.isEmpty() && a().hasNext());
    }

    public final /* synthetic */ Object next() {
        this.f2498b = true;
        int i = this.f2497a + 1;
        this.f2497a = i;
        return i < this.d.f2489b.size() ? (Entry) this.d.f2489b.get(this.f2497a) : (Entry) a().next();
    }

    public final void remove() {
        if (!this.f2498b) {
            throw new IllegalStateException("remove() was called before next()");
        }
        this.f2498b = false;
        this.d.f();
        if (this.f2497a < this.d.f2489b.size()) {
            ads ads = this.d;
            int i = this.f2497a;
            this.f2497a = i - 1;
            ads.c(i);
            return;
        }
        a().remove();
    }
}
