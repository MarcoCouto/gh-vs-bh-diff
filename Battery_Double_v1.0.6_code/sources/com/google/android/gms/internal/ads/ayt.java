package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.m;

@cm
final class ayt {

    /* renamed from: a reason: collision with root package name */
    apk f3009a;

    /* renamed from: b reason: collision with root package name */
    aqa f3010b;
    aqe c;
    atc d;
    aph e;
    gn f;

    ayt() {
    }

    /* access modifiers changed from: 0000 */
    public final void a(m mVar) {
        if (this.f3009a != null) {
            mVar.a((apk) new ayu(this.f3009a));
        }
        if (this.f3010b != null) {
            mVar.a(this.f3010b);
        }
        if (this.c != null) {
            mVar.a(this.c);
        }
        if (this.d != null) {
            mVar.a(this.d);
        }
        if (this.e != null) {
            mVar.a(this.e);
        }
        if (this.f != null) {
            mVar.a(this.f);
        }
    }
}
