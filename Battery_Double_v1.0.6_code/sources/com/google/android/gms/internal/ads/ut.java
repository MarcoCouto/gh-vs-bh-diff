package com.google.android.gms.internal.ads;

import java.io.UnsupportedEncodingException;

public class ut extends awb<String> {

    /* renamed from: a reason: collision with root package name */
    private final Object f3729a = new Object();

    /* renamed from: b reason: collision with root package name */
    private bdx<String> f3730b;

    public ut(int i, String str, bdx<String> bdx, bde bde) {
        super(i, str, bde);
        this.f3730b = bdx;
    }

    /* access modifiers changed from: protected */
    public final bcd<String> a(atz atz) {
        String str;
        String str2;
        try {
            byte[] bArr = atz.f2920b;
            String str3 = "ISO-8859-1";
            String str4 = (String) atz.c.get("Content-Type");
            if (str4 != null) {
                String[] split = str4.split(";");
                int i = 1;
                while (true) {
                    if (i >= split.length) {
                        break;
                    }
                    String[] split2 = split[i].trim().split("=");
                    if (split2.length == 2 && split2[0].equals("charset")) {
                        str2 = split2[1];
                        break;
                    }
                    i++;
                }
            }
            str2 = str3;
            str = new String(bArr, str2);
        } catch (UnsupportedEncodingException e) {
            str = new String(atz.f2920b);
        }
        return bcd.a(str, op.a(atz));
    }

    /* access modifiers changed from: protected */
    public void a(String str) {
        bdx<String> bdx;
        synchronized (this.f3729a) {
            bdx = this.f3730b;
        }
        if (bdx != null) {
            bdx.a(str);
        }
    }
}
