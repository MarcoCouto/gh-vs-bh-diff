package com.google.android.gms.internal.ads;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import java.util.Map;
import java.util.Map.Entry;
import java.util.WeakHashMap;

@cm
public final class md {

    /* renamed from: a reason: collision with root package name */
    private final BroadcastReceiver f3507a = new me(this);

    /* renamed from: b reason: collision with root package name */
    private final Map<BroadcastReceiver, IntentFilter> f3508b = new WeakHashMap();
    private boolean c = false;
    private boolean d;
    private Context e;

    /* access modifiers changed from: private */
    public final synchronized void a(Context context, Intent intent) {
        for (Entry entry : this.f3508b.entrySet()) {
            if (((IntentFilter) entry.getValue()).hasAction(intent.getAction())) {
                ((BroadcastReceiver) entry.getKey()).onReceive(context, intent);
            }
        }
    }

    public final synchronized void a(Context context) {
        if (!this.c) {
            this.e = context.getApplicationContext();
            if (this.e == null) {
                this.e = context;
            }
            asi.a(this.e);
            this.d = ((Boolean) ape.f().a(asi.ch)).booleanValue();
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.intent.action.SCREEN_ON");
            intentFilter.addAction("android.intent.action.SCREEN_OFF");
            intentFilter.addAction("android.intent.action.USER_PRESENT");
            this.e.registerReceiver(this.f3507a, intentFilter);
            this.c = true;
        }
    }

    public final synchronized void a(Context context, BroadcastReceiver broadcastReceiver) {
        if (this.d) {
            this.f3508b.remove(broadcastReceiver);
        } else {
            context.unregisterReceiver(broadcastReceiver);
        }
    }

    public final synchronized void a(Context context, BroadcastReceiver broadcastReceiver, IntentFilter intentFilter) {
        if (this.d) {
            this.f3508b.put(broadcastReceiver, intentFilter);
        } else {
            context.registerReceiver(broadcastReceiver, intentFilter);
        }
    }
}
