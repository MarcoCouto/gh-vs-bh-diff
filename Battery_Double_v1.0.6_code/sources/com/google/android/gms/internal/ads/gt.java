package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.a.a;
import com.google.android.gms.common.internal.a.c;

@cm
public final class gt extends a {
    public static final Creator<gt> CREATOR = new gu();

    /* renamed from: a reason: collision with root package name */
    public final aop f3344a;

    /* renamed from: b reason: collision with root package name */
    public final String f3345b;

    public gt(aop aop, String str) {
        this.f3344a = aop;
        this.f3345b = str;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = c.a(parcel);
        c.a(parcel, 2, (Parcelable) this.f3344a, i, false);
        c.a(parcel, 3, this.f3345b, false);
        c.a(parcel, a2);
    }
}
