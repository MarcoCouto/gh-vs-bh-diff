package com.google.android.gms.internal.ads;

import java.nio.charset.Charset;
import java.security.MessageDigest;

@cm
public final class amc extends alt {

    /* renamed from: b reason: collision with root package name */
    private MessageDigest f2723b;
    private final int c;
    private final int d;

    public amc(int i) {
        int i2 = i / 8;
        if (i % 8 > 0) {
            i2++;
        }
        this.c = i2;
        this.d = i;
    }

    public final byte[] a(String str) {
        byte[] bArr;
        synchronized (this.f2713a) {
            this.f2723b = a();
            if (this.f2723b == null) {
                bArr = new byte[0];
            } else {
                this.f2723b.reset();
                this.f2723b.update(str.getBytes(Charset.forName("UTF-8")));
                byte[] digest = this.f2723b.digest();
                bArr = new byte[(digest.length > this.c ? this.c : digest.length)];
                System.arraycopy(digest, 0, bArr, 0, bArr.length);
                if (this.d % 8 > 0) {
                    long j = 0;
                    for (int i = 0; i < bArr.length; i++) {
                        if (i > 0) {
                            j <<= 8;
                        }
                        j += (long) (bArr[i] & 255);
                    }
                    long j2 = j >>> (8 - (this.d % 8));
                    for (int i2 = this.c - 1; i2 >= 0; i2--) {
                        bArr[i2] = (byte) ((int) (255 & j2));
                        j2 >>>= 8;
                    }
                }
            }
        }
        return bArr;
    }
}
