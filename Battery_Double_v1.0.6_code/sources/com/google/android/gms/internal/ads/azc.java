package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.Bundle;
import android.os.RemoteException;
import com.google.android.gms.ads.internal.ax;
import com.google.android.gms.ads.internal.bu;
import com.google.android.gms.ads.internal.m;
import com.google.android.gms.b.a;

@cm
public final class azc extends apw {

    /* renamed from: a reason: collision with root package name */
    private final String f3026a;

    /* renamed from: b reason: collision with root package name */
    private boolean f3027b;
    private final axr c;
    private m d;
    private final ayt e;

    public azc(Context context, String str, bcr bcr, mu muVar, bu buVar) {
        this(str, new axr(context, bcr, muVar, buVar));
    }

    private azc(String str, axr axr) {
        this.f3026a = str;
        this.c = axr;
        this.e = new ayt();
        ax.r().a(axr);
    }

    private final void c() {
        if (this.d == null) {
            this.d = this.c.a(this.f3026a);
            this.e.a(this.d);
        }
    }

    public final String D() {
        throw new IllegalStateException("getAdUnitId not implemented");
    }

    public final aqe E() {
        throw new IllegalStateException("getIAppEventListener not implemented");
    }

    public final apk F() {
        throw new IllegalStateException("getIAdListener not implemented");
    }

    public final void I() throws RemoteException {
        if (this.d != null) {
            this.d.c(this.f3027b);
            this.d.I();
            return;
        }
        jm.e("Interstitial ad must be loaded before showInterstitial().");
    }

    public final String a() throws RemoteException {
        if (this.d != null) {
            return this.d.a();
        }
        return null;
    }

    public final void a(af afVar, String str) throws RemoteException {
        jm.e("setPlayStorePurchaseParams is deprecated and should not be called.");
    }

    public final void a(aot aot) throws RemoteException {
        if (this.d != null) {
            this.d.a(aot);
        }
    }

    public final void a(aph aph) throws RemoteException {
        this.e.e = aph;
        if (this.d != null) {
            this.e.a(this.d);
        }
    }

    public final void a(apk apk) throws RemoteException {
        this.e.f3009a = apk;
        if (this.d != null) {
            this.e.a(this.d);
        }
    }

    public final void a(aqa aqa) throws RemoteException {
        this.e.f3010b = aqa;
        if (this.d != null) {
            this.e.a(this.d);
        }
    }

    public final void a(aqe aqe) throws RemoteException {
        this.e.c = aqe;
        if (this.d != null) {
            this.e.a(this.d);
        }
    }

    public final void a(aqk aqk) throws RemoteException {
        c();
        if (this.d != null) {
            this.d.a(aqk);
        }
    }

    public final void a(aqy aqy) {
        throw new IllegalStateException("Unused method");
    }

    public final void a(arr arr) {
        throw new IllegalStateException("getVideoController not implemented for interstitials");
    }

    public final void a(atc atc) throws RemoteException {
        this.e.d = atc;
        if (this.d != null) {
            this.e.a(this.d);
        }
    }

    public final void a(gn gnVar) {
        this.e.f = gnVar;
        if (this.d != null) {
            this.e.a(this.d);
        }
    }

    public final void a(y yVar) throws RemoteException {
        jm.e("setInAppPurchaseListener is deprecated and should not be called.");
    }

    public final void a(String str) {
    }

    public final void b(boolean z) throws RemoteException {
        c();
        if (this.d != null) {
            this.d.b(z);
        }
    }

    public final boolean b(aop aop) throws RemoteException {
        if (!ayw.a(aop).contains("gw")) {
            c();
        }
        if (ayw.a(aop).contains("_skipMediation")) {
            c();
        }
        if (aop.j != null) {
            c();
        }
        if (this.d != null) {
            return this.d.b(aop);
        }
        ayw r = ax.r();
        if (ayw.a(aop).contains("_ad")) {
            r.b(aop, this.f3026a);
        }
        ayz a2 = r.a(aop, this.f3026a);
        if (a2 != null) {
            if (!a2.e) {
                a2.a();
                azb.a().e();
            } else {
                azb.a().d();
            }
            this.d = a2.f3018a;
            a2.c.a(this.e);
            this.e.a(this.d);
            return a2.f;
        }
        c();
        azb.a().e();
        return this.d.b(aop);
    }

    public final void c(boolean z) {
        this.f3027b = z;
    }

    public final void j() throws RemoteException {
        if (this.d != null) {
            this.d.j();
        }
    }

    public final a k() throws RemoteException {
        if (this.d != null) {
            return this.d.k();
        }
        return null;
    }

    public final aot l() throws RemoteException {
        if (this.d != null) {
            return this.d.l();
        }
        return null;
    }

    public final boolean m() throws RemoteException {
        return this.d != null && this.d.m();
    }

    public final void n() throws RemoteException {
        if (this.d != null) {
            this.d.n();
        } else {
            jm.e("Interstitial ad must be loaded before pingManualTrackingUrl().");
        }
    }

    public final void o() throws RemoteException {
        if (this.d != null) {
            this.d.o();
        }
    }

    public final void p() throws RemoteException {
        if (this.d != null) {
            this.d.p();
        }
    }

    public final Bundle q() throws RemoteException {
        return this.d != null ? this.d.q() : new Bundle();
    }

    public final String q_() throws RemoteException {
        if (this.d != null) {
            return this.d.q_();
        }
        return null;
    }

    public final void r() throws RemoteException {
        if (this.d != null) {
            this.d.r();
        }
    }

    public final boolean s() throws RemoteException {
        return this.d != null && this.d.s();
    }

    public final aqs t() {
        throw new IllegalStateException("getVideoController not implemented for interstitials");
    }
}
