package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.overlay.n;

final class sg implements n {

    /* renamed from: a reason: collision with root package name */
    private qn f3681a;

    /* renamed from: b reason: collision with root package name */
    private n f3682b;

    public sg(qn qnVar, n nVar) {
        this.f3681a = qnVar;
        this.f3682b = nVar;
    }

    public final void d() {
    }

    public final void f() {
    }

    public final void g() {
        this.f3682b.g();
        this.f3681a.o();
    }

    public final void p_() {
        this.f3682b.p_();
        this.f3681a.n();
    }
}
