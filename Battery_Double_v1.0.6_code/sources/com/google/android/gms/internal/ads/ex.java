package com.google.android.gms.internal.ads;

import java.util.concurrent.Future;

final class ex implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ Future f3299a;

    ex(eu euVar, Future future) {
        this.f3299a = future;
    }

    public final void run() {
        if (!this.f3299a.isDone()) {
            this.f3299a.cancel(true);
        }
    }
}
