package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.ads.internal.ax;

final class jy implements mk {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ Context f3446a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ String f3447b;

    jy(jv jvVar, Context context, String str) {
        this.f3446a = context;
        this.f3447b = str;
    }

    public final void a(String str) {
        ax.e();
        jv.a(this.f3446a, this.f3447b, str);
    }
}
