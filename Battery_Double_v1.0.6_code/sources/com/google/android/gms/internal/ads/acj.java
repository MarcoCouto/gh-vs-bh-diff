package com.google.android.gms.internal.ads;

import java.util.List;

final class acj extends acg {
    private acj() {
        super();
    }

    private static <E> abu<E> c(Object obj, long j) {
        return (abu) aeo.f(obj, j);
    }

    /* access modifiers changed from: 0000 */
    public final <L> List<L> a(Object obj, long j) {
        abu c = c(obj, j);
        if (c.a()) {
            return c;
        }
        int size = c.size();
        abu a2 = c.a(size == 0 ? 10 : size << 1);
        aeo.a(obj, j, (Object) a2);
        return a2;
    }

    /* access modifiers changed from: 0000 */
    public final <E> void a(Object obj, Object obj2, long j) {
        abu c = c(obj, j);
        abu c2 = c(obj2, j);
        int size = c.size();
        int size2 = c2.size();
        if (size > 0 && size2 > 0) {
            if (!c.a()) {
                c = c.a(size2 + size);
            }
            c.addAll(c2);
        }
        if (size <= 0) {
            c = c2;
        }
        aeo.a(obj, j, (Object) c);
    }

    /* access modifiers changed from: 0000 */
    public final void b(Object obj, long j) {
        c(obj, j).b();
    }
}
