package com.google.android.gms.internal.ads;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.google.android.gms.b.a;

public final class aqj extends ajk implements aqh {
    aqj(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.client.IClientApi");
    }

    public final apq createAdLoaderBuilder(a aVar, String str, bcr bcr, int i) throws RemoteException {
        apq aps;
        Parcel r_ = r_();
        ajm.a(r_, (IInterface) aVar);
        r_.writeString(str);
        ajm.a(r_, (IInterface) bcr);
        r_.writeInt(i);
        Parcel a2 = a(3, r_);
        IBinder readStrongBinder = a2.readStrongBinder();
        if (readStrongBinder == null) {
            aps = null;
        } else {
            IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.IAdLoaderBuilder");
            aps = queryLocalInterface instanceof apq ? (apq) queryLocalInterface : new aps(readStrongBinder);
        }
        a2.recycle();
        return aps;
    }

    public final r createAdOverlay(a aVar) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (IInterface) aVar);
        Parcel a2 = a(8, r_);
        r a3 = s.a(a2.readStrongBinder());
        a2.recycle();
        return a3;
    }

    public final apv createBannerAdManager(a aVar, aot aot, String str, bcr bcr, int i) throws RemoteException {
        apv apx;
        Parcel r_ = r_();
        ajm.a(r_, (IInterface) aVar);
        ajm.a(r_, (Parcelable) aot);
        r_.writeString(str);
        ajm.a(r_, (IInterface) bcr);
        r_.writeInt(i);
        Parcel a2 = a(1, r_);
        IBinder readStrongBinder = a2.readStrongBinder();
        if (readStrongBinder == null) {
            apx = null;
        } else {
            IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.IAdManager");
            apx = queryLocalInterface instanceof apv ? (apv) queryLocalInterface : new apx(readStrongBinder);
        }
        a2.recycle();
        return apx;
    }

    public final ab createInAppPurchaseManager(a aVar) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (IInterface) aVar);
        Parcel a2 = a(7, r_);
        ab a3 = ad.a(a2.readStrongBinder());
        a2.recycle();
        return a3;
    }

    public final apv createInterstitialAdManager(a aVar, aot aot, String str, bcr bcr, int i) throws RemoteException {
        apv apx;
        Parcel r_ = r_();
        ajm.a(r_, (IInterface) aVar);
        ajm.a(r_, (Parcelable) aot);
        r_.writeString(str);
        ajm.a(r_, (IInterface) bcr);
        r_.writeInt(i);
        Parcel a2 = a(2, r_);
        IBinder readStrongBinder = a2.readStrongBinder();
        if (readStrongBinder == null) {
            apx = null;
        } else {
            IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.IAdManager");
            apx = queryLocalInterface instanceof apv ? (apv) queryLocalInterface : new apx(readStrongBinder);
        }
        a2.recycle();
        return apx;
    }

    public final avb createNativeAdViewDelegate(a aVar, a aVar2) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (IInterface) aVar);
        ajm.a(r_, (IInterface) aVar2);
        Parcel a2 = a(5, r_);
        avb a3 = avc.a(a2.readStrongBinder());
        a2.recycle();
        return a3;
    }

    public final avg createNativeAdViewHolderDelegate(a aVar, a aVar2, a aVar3) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (IInterface) aVar);
        ajm.a(r_, (IInterface) aVar2);
        ajm.a(r_, (IInterface) aVar3);
        Parcel a2 = a(11, r_);
        avg a3 = avh.a(a2.readStrongBinder());
        a2.recycle();
        return a3;
    }

    public final gh createRewardedVideoAd(a aVar, bcr bcr, int i) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (IInterface) aVar);
        ajm.a(r_, (IInterface) bcr);
        r_.writeInt(i);
        Parcel a2 = a(6, r_);
        gh a3 = gj.a(a2.readStrongBinder());
        a2.recycle();
        return a3;
    }

    public final apv createSearchAdManager(a aVar, aot aot, String str, int i) throws RemoteException {
        apv apx;
        Parcel r_ = r_();
        ajm.a(r_, (IInterface) aVar);
        ajm.a(r_, (Parcelable) aot);
        r_.writeString(str);
        r_.writeInt(i);
        Parcel a2 = a(10, r_);
        IBinder readStrongBinder = a2.readStrongBinder();
        if (readStrongBinder == null) {
            apx = null;
        } else {
            IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.IAdManager");
            apx = queryLocalInterface instanceof apv ? (apv) queryLocalInterface : new apx(readStrongBinder);
        }
        a2.recycle();
        return apx;
    }

    public final aqn getMobileAdsSettingsManager(a aVar) throws RemoteException {
        aqn aqp;
        Parcel r_ = r_();
        ajm.a(r_, (IInterface) aVar);
        Parcel a2 = a(4, r_);
        IBinder readStrongBinder = a2.readStrongBinder();
        if (readStrongBinder == null) {
            aqp = null;
        } else {
            IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.IMobileAdsSettingManager");
            aqp = queryLocalInterface instanceof aqn ? (aqn) queryLocalInterface : new aqp(readStrongBinder);
        }
        a2.recycle();
        return aqp;
    }

    public final aqn getMobileAdsSettingsManagerWithClientJarVersion(a aVar, int i) throws RemoteException {
        aqn aqp;
        Parcel r_ = r_();
        ajm.a(r_, (IInterface) aVar);
        r_.writeInt(i);
        Parcel a2 = a(9, r_);
        IBinder readStrongBinder = a2.readStrongBinder();
        if (readStrongBinder == null) {
            aqp = null;
        } else {
            IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.IMobileAdsSettingManager");
            aqp = queryLocalInterface instanceof aqn ? (aqn) queryLocalInterface : new aqp(readStrongBinder);
        }
        a2.recycle();
        return aqp;
    }
}
