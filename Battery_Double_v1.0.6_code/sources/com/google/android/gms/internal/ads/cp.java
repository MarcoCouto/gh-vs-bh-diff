package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.webkit.CookieManager;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.internal.ax;
import com.google.android.gms.internal.ads.amy.a.b;
import org.json.JSONException;
import org.json.JSONObject;

@cm
public final class cp extends jh implements dc {

    /* renamed from: a reason: collision with root package name */
    ko f3234a;

    /* renamed from: b reason: collision with root package name */
    private final co f3235b;
    /* access modifiers changed from: private */
    public final dm c;
    /* access modifiers changed from: private */
    public final Object d = new Object();
    private final Context e;
    private final amw f;
    private final anb g;
    private dl h;
    /* access modifiers changed from: private */
    public Runnable i;
    private dp j;
    private bcb k;

    public cp(Context context, dm dmVar, co coVar, anb anb) {
        this.f3235b = coVar;
        this.e = context;
        this.c = dmVar;
        this.g = anb;
        this.f = new amw(this.g);
        this.f.a((amx) new cq(this));
        any any = new any();
        any.f2786a = Integer.valueOf(this.c.j.f3529b);
        any.f2787b = Integer.valueOf(this.c.j.c);
        any.c = Integer.valueOf(this.c.j.d ? 0 : 2);
        this.f.a((amx) new cr(any));
        if (this.c.f != null) {
            this.f.a((amx) new cs(this));
        }
        aot aot = this.c.c;
        if (aot.d && "interstitial_mb".equals(aot.f2814a)) {
            this.f.a(ct.f3239a);
        } else if (aot.d && "reward_mb".equals(aot.f2814a)) {
            this.f.a(cu.f3240a);
        } else if (aot.h || aot.d) {
            this.f.a(cw.f3242a);
        } else {
            this.f.a(cv.f3241a);
        }
        this.f.a(b.AD_REQUEST);
    }

    private final aot a(dl dlVar) throws cz {
        aot[] aotArr;
        boolean z = true;
        if (this.h == null || this.h.V == null || this.h.V.size() <= 1) {
            z = false;
        }
        if (z && this.k != null && !this.k.t) {
            return null;
        }
        if (this.j.y) {
            for (aot aot : dlVar.d.g) {
                if (aot.i) {
                    return new aot(aot, dlVar.d.g);
                }
            }
        }
        if (this.j.l == null) {
            throw new cz("The ad response must specify one of the supported ad sizes.", 0);
        }
        String[] split = this.j.l.split("x");
        if (split.length != 2) {
            String str = "Invalid ad size format from the ad response: ";
            String valueOf = String.valueOf(this.j.l);
            throw new cz(valueOf.length() != 0 ? str.concat(valueOf) : new String(str), 0);
        }
        try {
            int parseInt = Integer.parseInt(split[0]);
            int parseInt2 = Integer.parseInt(split[1]);
            aot[] aotArr2 = dlVar.d.g;
            int length = aotArr2.length;
            for (int i2 = 0; i2 < length; i2++) {
                aot aot2 = aotArr2[i2];
                float f2 = this.e.getResources().getDisplayMetrics().density;
                int i3 = aot2.e == -1 ? (int) (((float) aot2.f) / f2) : aot2.e;
                int i4 = aot2.f2815b == -2 ? (int) (((float) aot2.c) / f2) : aot2.f2815b;
                if (parseInt == i3 && parseInt2 == i4 && !aot2.i) {
                    return new aot(aot2, dlVar.d.g);
                }
            }
            String str2 = "The ad size from the ad response was not one of the requested sizes: ";
            String valueOf2 = String.valueOf(this.j.l);
            throw new cz(valueOf2.length() != 0 ? str2.concat(valueOf2) : new String(str2), 0);
        } catch (NumberFormatException e2) {
            String str3 = "Invalid ad size number from the ad response: ";
            String valueOf3 = String.valueOf(this.j.l);
            throw new cz(valueOf3.length() != 0 ? str3.concat(valueOf3) : new String(str3), 0);
        }
    }

    /* access modifiers changed from: private */
    public final void a(int i2, String str) {
        if (i2 == 3 || i2 == -1) {
            jm.d(str);
        } else {
            jm.e(str);
        }
        if (this.j == null) {
            this.j = new dp(i2);
        } else {
            this.j = new dp(i2, this.j.j);
        }
        this.f3235b.a(new is(this.h != null ? this.h : new dl(this.c, -1, null, null, null), this.j, this.k, null, i2, -1, this.j.m, null, this.f, null));
    }

    /* access modifiers changed from: 0000 */
    public final ko a(mu muVar, oa<dl> oaVar) {
        Context context = this.e;
        if (new db(context).a(muVar)) {
            jm.b("Fetching ad response from local ad request service.");
            di diVar = new di(context, oaVar, this);
            diVar.c();
            return diVar;
        }
        jm.b("Fetching ad response from remote ad request service.");
        ape.a();
        if (mh.c(context)) {
            return new dj(context, muVar, oaVar, this);
        }
        jm.e("Failed to connect to remote ad request service.");
        return null;
    }

    public final void a() {
        jm.b("AdLoaderBackgroundTask started.");
        this.i = new cx(this);
        jv.f3440a.postDelayed(this.i, ((Long) ape.f().a(asi.bA)).longValue());
        long b2 = ax.l().b();
        if (((Boolean) ape.f().a(asi.by)).booleanValue() && this.c.f3263b.c != null) {
            String string = this.c.f3263b.c.getString("_ad");
            if (string != null) {
                this.h = new dl(this.c, b2, null, null, null);
                a(ez.a(this.e, this.h, string));
                return;
            }
        }
        oe oeVar = new oe();
        jt.a((Runnable) new cy(this, oeVar));
        String h2 = ax.B().h(this.e);
        String i2 = ax.B().i(this.e);
        String j2 = ax.B().j(this.e);
        ax.B().f(this.e, j2);
        this.h = new dl(this.c, b2, h2, i2, j2);
        oeVar.a(this.h);
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ void a(ann ann) {
        ann.c.f2760a = this.c.f.packageName;
    }

    /* JADX WARNING: Removed duplicated region for block: B:53:0x0167  */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x0197  */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x01a1  */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x0221  */
    public final void a(dp dpVar) {
        JSONObject jSONObject;
        Bundle bundle;
        Boolean bool = null;
        jm.b("Received ad response.");
        this.j = dpVar;
        long b2 = ax.l().b();
        synchronized (this.d) {
            this.f3234a = null;
        }
        ax.i().l().d(this.j.F);
        if (((Boolean) ape.f().a(asi.aT)).booleanValue()) {
            if (this.j.N) {
                ax.i().l().c(this.h.e);
            } else {
                ax.i().l().d(this.h.e);
            }
        }
        try {
            if (this.j.d == -2 || this.j.d == -3) {
                if (this.j.d != -3) {
                    if (TextUtils.isEmpty(this.j.f3266b)) {
                        throw new cz("No fill from ad server.", 3);
                    }
                    ax.i().l().a(this.j.t);
                    if (this.j.g) {
                        this.k = new bcb(this.j.f3266b);
                        ax.i().a(this.k.h);
                    } else {
                        ax.i().a(this.j.I);
                    }
                    if (!TextUtils.isEmpty(this.j.G)) {
                        if (((Boolean) ape.f().a(asi.cC)).booleanValue()) {
                            jm.b("Received cookie from server. Setting webview cookie in CookieManager.");
                            CookieManager c2 = ax.g().c(this.e);
                            if (c2 != null) {
                                c2.setCookie("googleads.g.doubleclick.net", this.j.G);
                            }
                        }
                    }
                }
                aot aot = this.h.d.g != null ? a(this.h) : null;
                ax.i().l().b(this.j.u);
                ax.i().l().c(this.j.M);
                if (!TextUtils.isEmpty(this.j.q)) {
                    try {
                        jSONObject = new JSONObject(this.j.q);
                    } catch (Exception e2) {
                        jm.b("Error parsing the JSON for Active View.", e2);
                    }
                    if (this.j.P == 2) {
                        Boolean valueOf = Boolean.valueOf(true);
                        aop aop = this.h.c;
                        Bundle bundle2 = aop.m != null ? aop.m : new Bundle();
                        if (bundle2.getBundle(AdMobAdapter.class.getName()) != null) {
                            bundle = bundle2.getBundle(AdMobAdapter.class.getName());
                        } else {
                            Bundle bundle3 = new Bundle();
                            bundle2.putBundle(AdMobAdapter.class.getName(), bundle3);
                            bundle = bundle3;
                        }
                        bundle.putBoolean("render_test_label", true);
                        bool = valueOf;
                    }
                    if (this.j.P == 1) {
                        bool = Boolean.valueOf(false);
                    }
                    this.f3235b.a(new is(this.h, this.j, this.k, aot, -2, b2, this.j.m, jSONObject, this.f, this.j.P != 0 ? Boolean.valueOf(lz.a(this.h.c)) : bool));
                    jv.f3440a.removeCallbacks(this.i);
                    return;
                }
                jSONObject = null;
                if (this.j.P == 2) {
                }
                if (this.j.P == 1) {
                }
                this.f3235b.a(new is(this.h, this.j, this.k, aot, -2, b2, this.j.m, jSONObject, this.f, this.j.P != 0 ? Boolean.valueOf(lz.a(this.h.c)) : bool));
                jv.f3440a.removeCallbacks(this.i);
                return;
            }
            throw new cz("There was a problem getting an ad response. ErrorCode: " + this.j.d, this.j.d);
        } catch (JSONException e3) {
            jm.b("Could not parse mediation config.", e3);
            String str = "Could not parse mediation config: ";
            String valueOf2 = String.valueOf(this.j.f3266b);
            throw new cz(valueOf2.length() != 0 ? str.concat(valueOf2) : new String(str), 0);
        } catch (cz e4) {
            a(e4.a(), e4.getMessage());
            jv.f3440a.removeCallbacks(this.i);
        }
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ void b(ann ann) {
        ann.f2764a = this.c.v;
    }

    public final void c_() {
        synchronized (this.d) {
            if (this.f3234a != null) {
                this.f3234a.b();
            }
        }
    }
}
