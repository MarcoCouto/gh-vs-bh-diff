package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.Bundle;
import android.os.RemoteException;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import com.google.android.gms.b.a;
import com.google.android.gms.b.b;
import com.google.android.gms.common.internal.aa;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

@cm
public final class atx extends aud {
    private bdd c;
    private bdh d;
    private bdk e;
    private final aua f;
    private aty g;
    private boolean h;
    private Object i;

    private atx(Context context, aua aua, ahh ahh, aub aub) {
        super(context, aua, null, ahh, null, aub, null, null);
        this.h = false;
        this.i = new Object();
        this.f = aua;
    }

    public atx(Context context, aua aua, ahh ahh, bdd bdd, aub aub) {
        this(context, aua, ahh, aub);
        this.c = bdd;
    }

    public atx(Context context, aua aua, ahh ahh, bdh bdh, aub aub) {
        this(context, aua, ahh, aub);
        this.d = bdh;
    }

    public atx(Context context, aua aua, ahh ahh, bdk bdk, aub aub) {
        this(context, aua, ahh, aub);
        this.e = bdk;
    }

    private static HashMap<String, View> b(Map<String, WeakReference<View>> map) {
        HashMap hashMap = new HashMap();
        if (map == null) {
            return hashMap;
        }
        synchronized (map) {
            for (Entry entry : map.entrySet()) {
                View view = (View) ((WeakReference) entry.getValue()).get();
                if (view != null) {
                    hashMap.put((String) entry.getKey(), view);
                }
            }
        }
        return hashMap;
    }

    public final View a(OnClickListener onClickListener, boolean z) {
        a aVar;
        synchronized (this.i) {
            if (this.g != null) {
                View a2 = this.g.a(onClickListener, z);
                return a2;
            }
            try {
                if (this.e != null) {
                    aVar = this.e.l();
                } else if (this.c != null) {
                    aVar = this.c.n();
                } else {
                    if (this.d != null) {
                        aVar = this.d.k();
                    }
                    aVar = null;
                }
            } catch (RemoteException e2) {
                jm.c("Failed to call getAdChoicesContent", e2);
            }
            if (aVar == null) {
                return null;
            }
            View view = (View) b.a(aVar);
            return view;
        }
    }

    public final void a(View view) {
        synchronized (this.i) {
            if (this.g != null) {
                this.g.a(view);
            }
        }
    }

    public final void a(View view, Map<String, WeakReference<View>> map) {
        aa.b("recordImpression must be called on the main UI thread.");
        synchronized (this.i) {
            this.f2921a = true;
            if (this.g != null) {
                this.g.a(view, map);
                this.f.ac();
            } else {
                try {
                    if (this.e != null && !this.e.p()) {
                        this.e.r();
                        this.f.ac();
                    } else if (this.c != null && !this.c.j()) {
                        this.c.i();
                        this.f.ac();
                    } else if (this.d != null && !this.d.h()) {
                        this.d.g();
                        this.f.ac();
                    }
                } catch (RemoteException e2) {
                    jm.c("Failed to call recordImpression", e2);
                }
            }
        }
    }

    public final void a(View view, Map<String, WeakReference<View>> map, Bundle bundle, View view2) {
        aa.b("performClick must be called on the main UI thread.");
        synchronized (this.i) {
            if (this.g != null) {
                this.g.a(view, map, bundle, view2);
                this.f.e();
            } else {
                try {
                    if (this.e != null && !this.e.q()) {
                        this.e.a(b.a(view));
                        this.f.e();
                    } else if (this.c != null && !this.c.k()) {
                        this.c.a(b.a(view));
                        this.f.e();
                    } else if (this.d != null && !this.d.i()) {
                        this.d.a(b.a(view));
                        this.f.e();
                    }
                } catch (RemoteException e2) {
                    jm.c("Failed to call performClick", e2);
                }
            }
        }
    }

    public final void a(View view, Map<String, WeakReference<View>> map, Map<String, WeakReference<View>> map2, OnTouchListener onTouchListener, OnClickListener onClickListener) {
        synchronized (this.i) {
            this.h = true;
            HashMap b2 = b(map);
            HashMap b3 = b(map2);
            try {
                if (this.e != null) {
                    this.e.a(b.a(view), b.a(b2), b.a(b3));
                } else if (this.c != null) {
                    this.c.a(b.a(view), b.a(b2), b.a(b3));
                    this.c.b(b.a(view));
                } else if (this.d != null) {
                    this.d.a(b.a(view), b.a(b2), b.a(b3));
                    this.d.b(b.a(view));
                }
            } catch (RemoteException e2) {
                jm.c("Failed to call prepareAd", e2);
            }
            this.h = false;
        }
    }

    public final void a(aty aty) {
        synchronized (this.i) {
            this.g = aty;
        }
    }

    public final void a(awq awq) {
        synchronized (this.i) {
            if (this.g != null) {
                this.g.a(awq);
            }
        }
    }

    public final boolean a() {
        boolean S;
        synchronized (this.i) {
            S = this.g != null ? this.g.a() : this.f.S();
        }
        return S;
    }

    public final void b(View view, Map<String, WeakReference<View>> map) {
        synchronized (this.i) {
            try {
                if (this.e != null) {
                    this.e.b(b.a(view));
                } else if (this.c != null) {
                    this.c.c(b.a(view));
                } else if (this.d != null) {
                    this.d.c(b.a(view));
                }
            } catch (RemoteException e2) {
                jm.c("Failed to call untrackView", e2);
            }
        }
    }

    public final boolean b() {
        boolean T;
        synchronized (this.i) {
            T = this.g != null ? this.g.b() : this.f.T();
        }
        return T;
    }

    public final void c() {
        synchronized (this.i) {
            if (this.g != null) {
                this.g.c();
            }
        }
    }

    public final void d() {
        aa.b("recordDownloadedImpression must be called on main UI thread.");
        synchronized (this.i) {
            this.f2922b = true;
            if (this.g != null) {
                this.g.d();
            }
        }
    }

    public final boolean e() {
        boolean z;
        synchronized (this.i) {
            z = this.h;
        }
        return z;
    }

    public final aty f() {
        aty aty;
        synchronized (this.i) {
            aty = this.g;
        }
        return aty;
    }

    public final qn g() {
        return null;
    }

    public final void h() {
    }

    public final void i() {
    }

    public final void j() {
        if (this.g != null) {
            this.g.j();
        }
    }

    public final void k() {
        if (this.g != null) {
            this.g.k();
        }
    }
}
