package com.google.android.gms.internal.ads;

import android.view.View;

public final class ajy implements alg {

    /* renamed from: a reason: collision with root package name */
    private aty f2650a;

    public ajy(aty aty) {
        this.f2650a = aty;
    }

    public final View a() {
        if (this.f2650a != null) {
            return this.f2650a.l();
        }
        return null;
    }

    public final boolean b() {
        return this.f2650a == null;
    }

    public final alg c() {
        return this;
    }
}
