package com.google.android.gms.internal.ads;

import java.io.IOException;

public final class aft extends afh<aft> {
    private static volatile aft[] c;

    /* renamed from: a reason: collision with root package name */
    public byte[] f2539a;

    /* renamed from: b reason: collision with root package name */
    public byte[] f2540b;

    public aft() {
        this.f2539a = null;
        this.f2540b = null;
        this.Y = null;
        this.Z = -1;
    }

    public static aft[] b() {
        if (c == null) {
            synchronized (afl.f2531b) {
                if (c == null) {
                    c = new aft[0];
                }
            }
        }
        return c;
    }

    /* access modifiers changed from: protected */
    public final int a() {
        int a2 = super.a() + aff.b(1, this.f2539a);
        return this.f2540b != null ? a2 + aff.b(2, this.f2540b) : a2;
    }

    public final /* synthetic */ afn a(afd afd) throws IOException {
        while (true) {
            int a2 = afd.a();
            switch (a2) {
                case 0:
                    break;
                case 10:
                    this.f2539a = afd.f();
                    continue;
                case 18:
                    this.f2540b = afd.f();
                    continue;
                default:
                    if (!super.a(afd, a2)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    public final void a(aff aff) throws IOException {
        aff.a(1, this.f2539a);
        if (this.f2540b != null) {
            aff.a(2, this.f2540b);
        }
        super.a(aff);
    }
}
