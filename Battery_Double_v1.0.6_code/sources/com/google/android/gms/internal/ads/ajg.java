package com.google.android.gms.internal.ads;

import android.util.DisplayMetrics;
import android.view.View;
import java.lang.reflect.InvocationTargetException;

public final class ajg extends ajj {
    private final View d;

    public ajg(ahy ahy, String str, String str2, zz zzVar, int i, int i2, View view) {
        super(ahy, str, str2, zzVar, i, 57);
        this.d = view;
    }

    /* access modifiers changed from: protected */
    public final void a() throws IllegalAccessException, InvocationTargetException {
        if (this.d != null) {
            DisplayMetrics displayMetrics = this.f2636a.a().getResources().getDisplayMetrics();
            aih aih = new aih((String) this.c.invoke(null, new Object[]{this.d, displayMetrics}));
            acb acb = new acb();
            acb.f2446a = aih.f2622a;
            acb.f2447b = aih.f2623b;
            acb.c = aih.c;
            this.f2637b.M = acb;
        }
    }
}
