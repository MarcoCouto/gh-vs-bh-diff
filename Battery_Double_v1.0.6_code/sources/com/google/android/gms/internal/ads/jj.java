package com.google.android.gms.internal.ads;

import android.content.Context;

@cm
public final class jj {
    public static void a(Context context) {
        if (ml.a(context) && !ml.b()) {
            nn nnVar = (nn) new jl(context).c();
            jm.d("Updating ad debug logging enablement.");
            na.a(nnVar, "AdDebugLogUpdater.updateEnablement");
        }
    }
}
