package com.google.android.gms.internal.ads;

import android.content.Context;

@cm
public final class hx implements id {

    /* renamed from: a reason: collision with root package name */
    private ie f3378a;

    public hx(ie ieVar) {
        this.f3378a = ieVar;
    }

    public final ic a(Context context, mu muVar, dp dpVar) {
        if (dpVar.K == null) {
            return null;
        }
        return new hr(context, muVar, dpVar.K, dpVar.f3265a, this.f3378a);
    }
}
