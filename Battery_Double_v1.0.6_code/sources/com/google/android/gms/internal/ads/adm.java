package com.google.android.gms.internal.ads;

import java.lang.reflect.Field;
import java.util.Arrays;

final class adm {
    private int A;
    private int B;
    private Field C;
    private Object D;
    private Object E;
    private Object F;

    /* renamed from: a reason: collision with root package name */
    private final adn f2482a;

    /* renamed from: b reason: collision with root package name */
    private final Object[] f2483b;
    private Class<?> c;
    /* access modifiers changed from: private */
    public final int d;
    /* access modifiers changed from: private */
    public final int e;
    private final int f;
    private final int g;
    /* access modifiers changed from: private */
    public final int h;
    /* access modifiers changed from: private */
    public final int i;
    /* access modifiers changed from: private */
    public final int j;
    /* access modifiers changed from: private */
    public final int k;
    /* access modifiers changed from: private */
    public final int l;
    /* access modifiers changed from: private */
    public final int m;
    /* access modifiers changed from: private */
    public final int[] n;
    private int o;
    private int p;
    private int q = Integer.MAX_VALUE;
    private int r = Integer.MIN_VALUE;
    private int s = 0;
    private int t = 0;
    private int u = 0;
    private int v = 0;
    private int w = 0;
    private int x;
    private int y;
    private int z;

    adm(Class<?> cls, String str, Object[] objArr) {
        int[] iArr = null;
        this.c = cls;
        this.f2482a = new adn(str);
        this.f2483b = objArr;
        this.d = this.f2482a.b();
        this.e = this.f2482a.b();
        if (this.e == 0) {
            this.f = 0;
            this.g = 0;
            this.h = 0;
            this.i = 0;
            this.j = 0;
            this.l = 0;
            this.k = 0;
            this.m = 0;
            this.n = null;
            return;
        }
        this.f = this.f2482a.b();
        this.g = this.f2482a.b();
        this.h = this.f2482a.b();
        this.i = this.f2482a.b();
        this.l = this.f2482a.b();
        this.k = this.f2482a.b();
        this.j = this.f2482a.b();
        this.m = this.f2482a.b();
        int b2 = this.f2482a.b();
        if (b2 != 0) {
            iArr = new int[b2];
        }
        this.n = iArr;
        this.o = (this.f << 1) + this.g;
    }

    private static Field a(Class<?> cls, String str) {
        try {
            return cls.getDeclaredField(str);
        } catch (NoSuchFieldException e2) {
            Field[] declaredFields = cls.getDeclaredFields();
            for (Field field : declaredFields) {
                if (str.equals(field.getName())) {
                    return field;
                }
            }
            String name = cls.getName();
            String arrays = Arrays.toString(declaredFields);
            throw new RuntimeException(new StringBuilder(String.valueOf(str).length() + 40 + String.valueOf(name).length() + String.valueOf(arrays).length()).append("Field ").append(str).append(" for ").append(name).append(" not found. Known fields are ").append(arrays).toString());
        }
    }

    private final Object p() {
        Object[] objArr = this.f2483b;
        int i2 = this.o;
        this.o = i2 + 1;
        return objArr[i2];
    }

    private final boolean q() {
        return (this.d & 1) == 1;
    }

    /* access modifiers changed from: 0000 */
    public final boolean a() {
        boolean z2 = false;
        if (!this.f2482a.a()) {
            return false;
        }
        this.x = this.f2482a.b();
        this.y = this.f2482a.b();
        this.z = this.y & 255;
        if (this.x < this.q) {
            this.q = this.x;
        }
        if (this.x > this.r) {
            this.r = this.x;
        }
        if (this.z == abk.MAP.a()) {
            this.s++;
        } else if (this.z >= abk.DOUBLE_LIST.a() && this.z <= abk.GROUP_LIST.a()) {
            this.t++;
        }
        this.w++;
        if (adr.a(this.q, this.x, this.w)) {
            this.v = this.x + 1;
            this.u = this.v - this.q;
        } else {
            this.u++;
        }
        if ((this.y & 1024) != 0) {
            int[] iArr = this.n;
            int i2 = this.p;
            this.p = i2 + 1;
            iArr[i2] = this.x;
        }
        this.D = null;
        this.E = null;
        this.F = null;
        if (d()) {
            this.A = this.f2482a.b();
            if (this.z == abk.MESSAGE.a() + 51 || this.z == abk.GROUP.a() + 51) {
                this.D = p();
            } else if (this.z == abk.ENUM.a() + 51 && q()) {
                this.E = p();
            }
        } else {
            this.C = a(this.c, (String) p());
            if (h()) {
                this.B = this.f2482a.b();
            }
            if (this.z == abk.MESSAGE.a() || this.z == abk.GROUP.a()) {
                this.D = this.C.getType();
            } else if (this.z == abk.MESSAGE_LIST.a() || this.z == abk.GROUP_LIST.a()) {
                this.D = p();
            } else if (this.z == abk.ENUM.a() || this.z == abk.ENUM_LIST.a() || this.z == abk.ENUM_LIST_PACKED.a()) {
                if (q()) {
                    this.E = p();
                }
            } else if (this.z == abk.MAP.a()) {
                this.F = p();
                if ((this.y & 2048) != 0) {
                    z2 = true;
                }
                if (z2) {
                    this.E = p();
                }
            }
        }
        return true;
    }

    /* access modifiers changed from: 0000 */
    public final int b() {
        return this.x;
    }

    /* access modifiers changed from: 0000 */
    public final int c() {
        return this.z;
    }

    /* access modifiers changed from: 0000 */
    public final boolean d() {
        return this.z > abk.MAP.a();
    }

    /* access modifiers changed from: 0000 */
    public final Field e() {
        int i2 = this.A << 1;
        Object obj = this.f2483b[i2];
        if (obj instanceof Field) {
            return (Field) obj;
        }
        Field a2 = a(this.c, (String) obj);
        this.f2483b[i2] = a2;
        return a2;
    }

    /* access modifiers changed from: 0000 */
    public final Field f() {
        int i2 = (this.A << 1) + 1;
        Object obj = this.f2483b[i2];
        if (obj instanceof Field) {
            return (Field) obj;
        }
        Field a2 = a(this.c, (String) obj);
        this.f2483b[i2] = a2;
        return a2;
    }

    /* access modifiers changed from: 0000 */
    public final Field g() {
        return this.C;
    }

    /* access modifiers changed from: 0000 */
    public final boolean h() {
        return q() && this.z <= abk.GROUP.a();
    }

    /* access modifiers changed from: 0000 */
    public final Field i() {
        int i2 = (this.B / 32) + (this.f << 1);
        Object obj = this.f2483b[i2];
        if (obj instanceof Field) {
            return (Field) obj;
        }
        Field a2 = a(this.c, (String) obj);
        this.f2483b[i2] = a2;
        return a2;
    }

    /* access modifiers changed from: 0000 */
    public final int j() {
        return this.B % 32;
    }

    /* access modifiers changed from: 0000 */
    public final boolean k() {
        return (this.y & 256) != 0;
    }

    /* access modifiers changed from: 0000 */
    public final boolean l() {
        return (this.y & 512) != 0;
    }

    /* access modifiers changed from: 0000 */
    public final Object m() {
        return this.D;
    }

    /* access modifiers changed from: 0000 */
    public final Object n() {
        return this.E;
    }

    /* access modifiers changed from: 0000 */
    public final Object o() {
        return this.F;
    }
}
