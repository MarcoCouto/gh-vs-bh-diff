package com.google.android.gms.internal.ads;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;

public final class tq extends ajk implements tp {
    tq(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.gass.internal.IGassService");
    }

    public final tn a(tl tlVar) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (Parcelable) tlVar);
        Parcel a2 = a(1, r_);
        tn tnVar = (tn) ajm.a(a2, tn.CREATOR);
        a2.recycle();
        return tnVar;
    }
}
