package com.google.android.gms.internal.ads;

import android.text.TextUtils;
import com.google.android.gms.ads.internal.gmsg.ae;
import java.util.Map;

final class aug implements ae<Object> {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ auf f2927a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ bq f2928b;

    aug(auf auf, bq bqVar) {
        this.f2927a = auf;
        this.f2928b = bqVar;
    }

    public final void zza(Object obj, Map<String, String> map) {
        qn qnVar = (qn) this.f2927a.f2925a.get();
        if (qnVar == null) {
            this.f2928b.b("/loadHtml", this);
            return;
        }
        qnVar.v().a((rw) new auh(this, map, this.f2928b));
        String str = (String) map.get("overlayHtml");
        String str2 = (String) map.get("baseUrl");
        if (TextUtils.isEmpty(str2)) {
            qnVar.loadData(str, "text/html", "UTF-8");
        } else {
            qnVar.loadDataWithBaseURL(str2, str, "text/html", "UTF-8", null);
        }
    }
}
