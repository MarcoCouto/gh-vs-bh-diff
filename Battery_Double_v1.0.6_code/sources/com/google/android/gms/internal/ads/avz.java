package com.google.android.gms.internal.ads;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public final class avz extends ajk implements avx {
    avz(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.formats.client.IOnAppInstallAdLoadedListener");
    }

    public final void a(avl avl) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (IInterface) avl);
        b(1, r_);
    }
}
