package com.google.android.gms.internal.ads;

import android.os.RemoteException;

final class ayp implements ays {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ int f3006a;

    ayp(ayi ayi, int i) {
        this.f3006a = i;
    }

    public final void a(ayt ayt) throws RemoteException {
        if (ayt.f != null) {
            ayt.f.a(this.f3006a);
        }
    }
}
