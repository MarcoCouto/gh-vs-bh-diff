package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.abp.e;

final class acl implements adq {

    /* renamed from: b reason: collision with root package name */
    private static final acv f2458b = new acm();

    /* renamed from: a reason: collision with root package name */
    private final acv f2459a;

    public acl() {
        this(new acn(abo.a(), a()));
    }

    private acl(acv acv) {
        this.f2459a = (acv) abr.a(acv, "messageInfoFactory");
    }

    private static acv a() {
        try {
            return (acv) Class.forName("com.google.protobuf.DescriptorMessageInfoFactory").getDeclaredMethod("getInstance", new Class[0]).invoke(null, new Object[0]);
        } catch (Exception e) {
            return f2458b;
        }
    }

    private static boolean a(acu acu) {
        return acu.a() == e.h;
    }

    public final <T> adp<T> a(Class<T> cls) {
        adr.a(cls);
        acu b2 = this.f2459a.b(cls);
        if (b2.b()) {
            return abp.class.isAssignableFrom(cls) ? add.a(adr.c(), abg.a(), b2.c()) : add.a(adr.a(), abg.b(), b2.c());
        }
        if (abp.class.isAssignableFrom(cls)) {
            if (a(b2)) {
                return ada.a(cls, b2, adh.b(), acg.b(), adr.c(), abg.a(), act.b());
            }
            return ada.a(cls, b2, adh.b(), acg.b(), adr.c(), null, act.b());
        } else if (a(b2)) {
            return ada.a(cls, b2, adh.a(), acg.a(), adr.a(), abg.b(), act.a());
        } else {
            return ada.a(cls, b2, adh.a(), acg.a(), adr.b(), null, act.a());
        }
    }
}
