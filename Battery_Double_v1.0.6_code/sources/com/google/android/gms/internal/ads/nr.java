package com.google.android.gms.internal.ads;

import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.FutureTask;

@cm
final class nr<V> extends FutureTask<V> implements nn<V> {

    /* renamed from: a reason: collision with root package name */
    private final np f3557a = new np();

    nr(Runnable runnable, V v) {
        super(runnable, v);
    }

    nr(Callable<V> callable) {
        super(callable);
    }

    public final void a(Runnable runnable, Executor executor) {
        this.f3557a.a(runnable, executor);
    }

    /* access modifiers changed from: protected */
    public final void done() {
        this.f3557a.a();
    }
}
