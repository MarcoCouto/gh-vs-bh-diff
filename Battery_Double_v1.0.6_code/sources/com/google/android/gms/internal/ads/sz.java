package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.Bundle;

public final class sz {

    /* renamed from: a reason: collision with root package name */
    private final byte[] f3698a;

    /* renamed from: b reason: collision with root package name */
    private final Bundle f3699b;
    private final Bundle c;
    private final Context d;

    public sz(Context context, byte[] bArr, Bundle bundle, Bundle bundle2) {
        this.f3698a = bArr;
        this.f3699b = bundle;
        this.c = bundle2;
        this.d = context;
    }
}
