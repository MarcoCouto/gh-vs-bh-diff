package com.google.android.gms.internal.ads;

import android.graphics.Color;
import java.util.ArrayList;
import java.util.List;

@cm
public final class ati extends aut {

    /* renamed from: a reason: collision with root package name */
    private static final int f2897a = Color.rgb(12, 174, 206);

    /* renamed from: b reason: collision with root package name */
    private static final int f2898b;
    private static final int c;
    private static final int d = f2897a;
    private final String e;
    private final List<atm> f = new ArrayList();
    private final List<auw> g = new ArrayList();
    private final int h;
    private final int i;
    private final int j;
    private final int k;
    private final int l;
    private final boolean m;

    static {
        int rgb = Color.rgb(204, 204, 204);
        f2898b = rgb;
        c = rgb;
    }

    public ati(String str, List<atm> list, Integer num, Integer num2, Integer num3, int i2, int i3, boolean z) {
        this.e = str;
        if (list != null) {
            int i4 = 0;
            while (true) {
                int i5 = i4;
                if (i5 >= list.size()) {
                    break;
                }
                atm atm = (atm) list.get(i5);
                this.f.add(atm);
                this.g.add(atm);
                i4 = i5 + 1;
            }
        }
        this.h = num != null ? num.intValue() : c;
        this.i = num2 != null ? num2.intValue() : d;
        this.j = num3 != null ? num3.intValue() : 12;
        this.k = i2;
        this.l = i3;
        this.m = z;
    }

    public final String a() {
        return this.e;
    }

    public final List<auw> b() {
        return this.g;
    }

    public final List<atm> c() {
        return this.f;
    }

    public final int d() {
        return this.h;
    }

    public final int e() {
        return this.i;
    }

    public final int f() {
        return this.j;
    }

    public final int g() {
        return this.k;
    }

    public final int h() {
        return this.l;
    }

    public final boolean i() {
        return this.m;
    }
}
