package com.google.android.gms.internal.ads;

import android.view.ViewTreeObserver.OnScrollChangedListener;
import java.lang.ref.WeakReference;

final class bp implements OnScrollChangedListener {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ WeakReference f3199a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ bi f3200b;

    bp(bi biVar, WeakReference weakReference) {
        this.f3200b = biVar;
        this.f3199a = weakReference;
    }

    public final void onScrollChanged() {
        this.f3200b.a(this.f3199a, true);
    }
}
