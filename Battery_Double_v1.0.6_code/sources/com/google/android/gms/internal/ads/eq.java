package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.a.a;
import com.google.android.gms.common.internal.a.c;

@cm
public final class eq extends a {
    public static final Creator<eq> CREATOR = new er();

    /* renamed from: a reason: collision with root package name */
    String f3289a;

    public eq(String str) {
        this.f3289a = str;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = c.a(parcel);
        c.a(parcel, 2, this.f3289a, false);
        c.a(parcel, a2);
    }
}
