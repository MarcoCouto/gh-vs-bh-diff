package com.google.android.gms.internal.ads;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@cm
public abstract class alt {

    /* renamed from: b reason: collision with root package name */
    private static MessageDigest f2712b = null;

    /* renamed from: a reason: collision with root package name */
    protected Object f2713a = new Object();

    /* access modifiers changed from: protected */
    public final MessageDigest a() {
        MessageDigest messageDigest;
        synchronized (this.f2713a) {
            if (f2712b != null) {
                messageDigest = f2712b;
            } else {
                for (int i = 0; i < 2; i++) {
                    try {
                        f2712b = MessageDigest.getInstance("MD5");
                    } catch (NoSuchAlgorithmException e) {
                    }
                }
                messageDigest = f2712b;
            }
        }
        return messageDigest;
    }

    /* access modifiers changed from: 0000 */
    public abstract byte[] a(String str);
}
