package com.google.android.gms.internal.ads;

import android.view.View;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.ViewTreeObserver.OnScrollChangedListener;

@cm
public final class og {
    public static void a(View view, OnGlobalLayoutListener onGlobalLayoutListener) {
        new oh(view, onGlobalLayoutListener).a();
    }

    public static void a(View view, OnScrollChangedListener onScrollChangedListener) {
        new oi(view, onScrollChangedListener).a();
    }
}
