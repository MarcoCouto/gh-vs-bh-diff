package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.ax;
import com.google.android.gms.ads.internal.gmsg.o;
import org.json.JSONObject;

@cm
public final class bbt<I, O> implements bbi<I, O> {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public final bbk<O> f3109a;

    /* renamed from: b reason: collision with root package name */
    private final bbl<I> f3110b;
    private final bah c;
    private final String d;

    bbt(bah bah, String str, bbl<I> bbl, bbk<O> bbk) {
        this.c = bah;
        this.d = str;
        this.f3110b = bbl;
        this.f3109a = bbk;
    }

    /* access modifiers changed from: private */
    public final void a(bau bau, bbe bbe, I i, ny<O> nyVar) {
        try {
            ax.e();
            String a2 = jv.a();
            o.o.a(a2, new bbw(this, bau, nyVar));
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("id", a2);
            jSONObject.put("args", this.f3110b.a(i));
            bbe.b(this.d, jSONObject);
        } catch (Exception e) {
            nyVar.a(e);
            jm.b("Unable to invokeJavaScript", e);
            bau.c();
        } catch (Throwable th) {
            bau.c();
            throw th;
        }
    }

    public final nn<O> a(I i) throws Exception {
        return b(i);
    }

    public final nn<O> b(I i) {
        ny nyVar = new ny();
        bau b2 = this.c.b((ahh) null);
        b2.a(new bbu(this, b2, i, nyVar), new bbv(this, nyVar, b2));
        return nyVar;
    }
}
