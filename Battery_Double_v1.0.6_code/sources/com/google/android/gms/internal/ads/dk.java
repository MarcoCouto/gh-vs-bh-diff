package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.DeadObjectException;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.google.android.gms.common.internal.e;
import com.google.android.gms.common.internal.e.a;
import com.google.android.gms.common.internal.e.b;

@cm
public final class dk extends e<dt> {
    public dk(Context context, Looper looper, a aVar, b bVar) {
        super(context.getApplicationContext() != null ? context.getApplicationContext() : context, looper, 8, aVar, bVar, null);
    }

    public final dt A() throws DeadObjectException {
        return (dt) super.x();
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ IInterface a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.ads.internal.request.IAdRequestService");
        return queryLocalInterface instanceof dt ? (dt) queryLocalInterface : new dv(iBinder);
    }

    /* access modifiers changed from: protected */
    public final String i() {
        return "com.google.android.gms.ads.service.START";
    }

    /* access modifiers changed from: protected */
    public final String l() {
        return "com.google.android.gms.ads.internal.request.IAdRequestService";
    }
}
