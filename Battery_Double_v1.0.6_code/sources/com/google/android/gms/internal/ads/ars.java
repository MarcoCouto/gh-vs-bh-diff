package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.a.b;

public final class ars implements Creator<arr> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        boolean z = false;
        int b2 = b.b(parcel);
        boolean z2 = false;
        boolean z3 = false;
        while (parcel.dataPosition() < b2) {
            int a2 = b.a(parcel);
            switch (b.a(a2)) {
                case 2:
                    z3 = b.c(parcel, a2);
                    break;
                case 3:
                    z2 = b.c(parcel, a2);
                    break;
                case 4:
                    z = b.c(parcel, a2);
                    break;
                default:
                    b.b(parcel, a2);
                    break;
            }
        }
        b.r(parcel, b2);
        return new arr(z3, z2, z);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new arr[i];
    }
}
