package com.google.android.gms.internal.ads;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import com.google.android.gms.common.util.n;

@cm
public final class pe extends ox {
    public final ow a(Context context, pm pmVar, int i, boolean z, asv asv, pl plVar) {
        ApplicationInfo applicationInfo = context.getApplicationInfo();
        if (!(n.b() && (applicationInfo == null || applicationInfo.targetSdkVersion >= 11))) {
            return null;
        }
        ok okVar = new ok(context, z, pmVar.t().d(), plVar, new pn(context, pmVar.k(), pmVar.g(), asv, pmVar.c()));
        return okVar;
    }
}
