package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.common.util.i;

final class db implements dd {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ Context f3249a;

    db(Context context) {
        this.f3249a = context;
    }

    public final boolean a(mu muVar) {
        ape.a();
        boolean c = mh.c(this.f3249a);
        boolean z = ((Boolean) ape.f().a(asi.dd)).booleanValue() && muVar.d;
        if (da.b(this.f3249a, muVar.d) && c && !z) {
            if (!i.c(this.f3249a)) {
                return false;
            }
            if (((Boolean) ape.f().a(asi.H)).booleanValue()) {
                return false;
            }
        }
        return true;
    }
}
