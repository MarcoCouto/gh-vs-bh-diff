package com.google.android.gms.internal.ads;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public class ajk implements IInterface {

    /* renamed from: a reason: collision with root package name */
    private final IBinder f2638a;

    /* renamed from: b reason: collision with root package name */
    private final String f2639b;

    protected ajk(IBinder iBinder, String str) {
        this.f2638a = iBinder;
        this.f2639b = str;
    }

    /* access modifiers changed from: protected */
    public final Parcel a(int i, Parcel parcel) throws RemoteException {
        parcel = Parcel.obtain();
        try {
            this.f2638a.transact(i, parcel, parcel, 0);
            parcel.readException();
            return parcel;
        } catch (RuntimeException e) {
            throw e;
        } finally {
            parcel.recycle();
        }
    }

    public IBinder asBinder() {
        return this.f2638a;
    }

    /* access modifiers changed from: protected */
    public final void b(int i, Parcel parcel) throws RemoteException {
        Parcel obtain = Parcel.obtain();
        try {
            this.f2638a.transact(i, parcel, obtain, 0);
            obtain.readException();
        } finally {
            parcel.recycle();
            obtain.recycle();
        }
    }

    /* access modifiers changed from: protected */
    public final Parcel r_() {
        Parcel obtain = Parcel.obtain();
        obtain.writeInterfaceToken(this.f2639b);
        return obtain;
    }
}
