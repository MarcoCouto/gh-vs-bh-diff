package com.google.android.gms.internal.ads;

import java.lang.Thread.UncaughtExceptionHandler;

final class ci implements UncaughtExceptionHandler {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ UncaughtExceptionHandler f3230a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ cg f3231b;

    ci(cg cgVar, UncaughtExceptionHandler uncaughtExceptionHandler) {
        this.f3231b = cgVar;
        this.f3230a = uncaughtExceptionHandler;
    }

    public final void uncaughtException(Thread thread, Throwable th) {
        try {
            this.f3231b.a(thread, th);
            if (this.f3230a != null) {
                this.f3230a.uncaughtException(thread, th);
            }
        } catch (Throwable th2) {
            if (this.f3230a != null) {
                this.f3230a.uncaughtException(thread, th);
            }
            throw th2;
        }
    }
}
