package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.xe.b;
import java.security.GeneralSecurityException;

final class uq implements tz<tr> {
    uq() {
    }

    private static tr c(acw acw) throws GeneralSecurityException {
        if (!(acw instanceof xr)) {
            throw new GeneralSecurityException("expected KmsAeadKey proto");
        }
        xr xrVar = (xr) acw;
        zo.a(xrVar.a(), 0);
        String a2 = xrVar.b().a();
        return uc.a(a2).b(a2);
    }

    private static tr d(aah aah) throws GeneralSecurityException {
        try {
            return c((acw) xr.a(aah));
        } catch (abv e) {
            throw new GeneralSecurityException("expected KmsAeadKey proto", e);
        }
    }

    public final int a() {
        return 0;
    }

    public final /* synthetic */ Object a(aah aah) throws GeneralSecurityException {
        return d(aah);
    }

    public final /* synthetic */ Object a(acw acw) throws GeneralSecurityException {
        return c(acw);
    }

    public final acw b(aah aah) throws GeneralSecurityException {
        try {
            return b((acw) xt.a(aah));
        } catch (abv e) {
            throw new GeneralSecurityException("expected serialized KmsAeadKeyFormat proto", e);
        }
    }

    public final acw b(acw acw) throws GeneralSecurityException {
        if (!(acw instanceof xt)) {
            throw new GeneralSecurityException("expected KmsAeadKeyFormat proto");
        }
        return xr.c().a((xt) acw).a(0).c();
    }

    public final xe c(aah aah) throws GeneralSecurityException {
        return (xe) xe.d().a("type.googleapis.com/google.crypto.tink.KmsAeadKey").a(((xr) b(aah)).h()).a(b.REMOTE).c();
    }
}
