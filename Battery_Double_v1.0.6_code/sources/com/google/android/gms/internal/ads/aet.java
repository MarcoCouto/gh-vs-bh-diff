package com.google.android.gms.internal.ads;

final class aet extends IllegalArgumentException {
    aet(int i, int i2) {
        super("Unpaired surrogate at index " + i + " of " + i2);
    }
}
