package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zw;
import com.google.android.gms.internal.ads.zx;

public abstract class zx<MessageType extends zw<MessageType, BuilderType>, BuilderType extends zx<MessageType, BuilderType>> implements acx {
    public final /* synthetic */ acx a(acw acw) {
        if (p().getClass().isInstance(acw)) {
            return a((MessageType) (zw) acw);
        }
        throw new IllegalArgumentException("mergeFrom(MessageLite) can only merge messages of the same type.");
    }

    /* renamed from: a */
    public abstract BuilderType clone();

    /* access modifiers changed from: protected */
    public abstract BuilderType a(MessageType messagetype);
}
