package com.google.android.gms.internal.ads;

import android.os.RemoteException;

final class ayi extends go {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ axs f3004a;

    ayi(axs axs) {
        this.f3004a = axs;
    }

    public final void a() throws RemoteException {
        this.f3004a.f2993a.add(new ayj(this));
    }

    public final void a(int i) throws RemoteException {
        this.f3004a.f2993a.add(new ayp(this, i));
    }

    public final void a(gc gcVar) throws RemoteException {
        this.f3004a.f2993a.add(new ayn(this, gcVar));
    }

    public final void b() throws RemoteException {
        this.f3004a.f2993a.add(new ayk(this));
    }

    public final void c() throws RemoteException {
        this.f3004a.f2993a.add(new ayl(this));
    }

    public final void d() throws RemoteException {
        this.f3004a.f2993a.add(new aym(this));
    }

    public final void e() throws RemoteException {
        this.f3004a.f2993a.add(new ayo(this));
    }

    public final void f() throws RemoteException {
        this.f3004a.f2993a.add(new ayq(this));
    }
}
