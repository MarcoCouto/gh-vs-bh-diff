package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.b.a;
import java.util.List;

public interface bcu extends IInterface {
    a a() throws RemoteException;

    void a(a aVar) throws RemoteException;

    void a(a aVar, aop aop, String str, bcx bcx) throws RemoteException;

    void a(a aVar, aop aop, String str, hl hlVar, String str2) throws RemoteException;

    void a(a aVar, aop aop, String str, String str2, bcx bcx) throws RemoteException;

    void a(a aVar, aop aop, String str, String str2, bcx bcx, aul aul, List<String> list) throws RemoteException;

    void a(a aVar, aot aot, aop aop, String str, bcx bcx) throws RemoteException;

    void a(a aVar, aot aot, aop aop, String str, String str2, bcx bcx) throws RemoteException;

    void a(a aVar, hl hlVar, List<String> list) throws RemoteException;

    void a(aop aop, String str) throws RemoteException;

    void a(aop aop, String str, String str2) throws RemoteException;

    void a(boolean z) throws RemoteException;

    void b() throws RemoteException;

    void c() throws RemoteException;

    void d() throws RemoteException;

    void e() throws RemoteException;

    void f() throws RemoteException;

    boolean g() throws RemoteException;

    bdd h() throws RemoteException;

    bdh i() throws RemoteException;

    Bundle j() throws RemoteException;

    Bundle k() throws RemoteException;

    Bundle l() throws RemoteException;

    boolean m() throws RemoteException;

    avt n() throws RemoteException;

    aqs o() throws RemoteException;

    bdk p() throws RemoteException;
}
