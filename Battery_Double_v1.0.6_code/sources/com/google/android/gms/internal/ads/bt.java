package com.google.android.gms.internal.ads;

import android.support.v4.h.m;
import android.view.View;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@cm
public final class bt implements bh<atr> {

    /* renamed from: a reason: collision with root package name */
    private final boolean f3205a;

    public bt(boolean z) {
        this.f3205a = z;
    }

    public final /* synthetic */ aub a(ay ayVar, JSONObject jSONObject) throws JSONException, InterruptedException, ExecutionException {
        View view = null;
        int i = 0;
        m mVar = new m();
        m mVar2 = new m();
        nn a2 = ayVar.a(jSONObject);
        nn a3 = ayVar.a(jSONObject, "video");
        JSONArray jSONArray = jSONObject.getJSONArray("custom_assets");
        for (int i2 = 0; i2 < jSONArray.length(); i2++) {
            JSONObject jSONObject2 = jSONArray.getJSONObject(i2);
            String string = jSONObject2.getString("type");
            if ("string".equals(string)) {
                mVar2.put(jSONObject2.getString("name"), jSONObject2.getString("string_value"));
            } else if ("image".equals(string)) {
                mVar.put(jSONObject2.getString("name"), ayVar.a(jSONObject2, "image_value", this.f3205a));
            } else {
                String str = "Unknown custom asset type: ";
                String valueOf = String.valueOf(string);
                jm.e(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
            }
        }
        qn a4 = ay.a(a3);
        String string2 = jSONObject.getString("custom_template_id");
        m mVar3 = new m();
        while (true) {
            int i3 = i;
            if (i3 >= mVar.size()) {
                break;
            }
            mVar3.put(mVar.b(i3), ((Future) mVar.c(i3)).get());
            i = i3 + 1;
        }
        ati ati = (ati) a2.get();
        rd rdVar = a4 != null ? a4.b() : null;
        if (a4 != null) {
            view = a4.getView();
        }
        return new atr(string2, mVar3, mVar2, ati, rdVar, view);
    }
}
