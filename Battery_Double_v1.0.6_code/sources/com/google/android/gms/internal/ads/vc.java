package com.google.android.gms.internal.ads;

final /* synthetic */ class vc {

    /* renamed from: a reason: collision with root package name */
    static final /* synthetic */ int[] f3735a = new int[ww.values().length];

    /* renamed from: b reason: collision with root package name */
    static final /* synthetic */ int[] f3736b = new int[wt.values().length];
    static final /* synthetic */ int[] c = new int[wf.values().length];

    static {
        try {
            c[wf.UNCOMPRESSED.ordinal()] = 1;
        } catch (NoSuchFieldError e) {
        }
        try {
            c[wf.COMPRESSED.ordinal()] = 2;
        } catch (NoSuchFieldError e2) {
        }
        try {
            f3736b[wt.NIST_P256.ordinal()] = 1;
        } catch (NoSuchFieldError e3) {
        }
        try {
            f3736b[wt.NIST_P384.ordinal()] = 2;
        } catch (NoSuchFieldError e4) {
        }
        try {
            f3736b[wt.NIST_P521.ordinal()] = 3;
        } catch (NoSuchFieldError e5) {
        }
        try {
            f3735a[ww.SHA1.ordinal()] = 1;
        } catch (NoSuchFieldError e6) {
        }
        try {
            f3735a[ww.SHA256.ordinal()] = 2;
        } catch (NoSuchFieldError e7) {
        }
        try {
            f3735a[ww.SHA512.ordinal()] = 3;
        } catch (NoSuchFieldError e8) {
        }
    }
}
