package com.google.android.gms.internal.ads;

final /* synthetic */ class azj implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final azg f3035a;

    /* renamed from: b reason: collision with root package name */
    private final String f3036b;

    azj(azg azg, String str) {
        this.f3035a = azg;
        this.f3036b = str;
    }

    public final void run() {
        this.f3035a.g(this.f3036b);
    }
}
