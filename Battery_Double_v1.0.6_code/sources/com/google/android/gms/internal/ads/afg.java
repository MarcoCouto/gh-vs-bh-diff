package com.google.android.gms.internal.ads;

import java.io.IOException;

public final class afg extends IOException {
    afg(int i, int i2) {
        super("CodedOutputStream was writing to a flat byte array and ran out of space (pos " + i + " limit " + i2 + ").");
    }
}
