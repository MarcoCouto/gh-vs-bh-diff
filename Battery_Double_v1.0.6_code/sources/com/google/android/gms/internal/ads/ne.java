package com.google.android.gms.internal.ads;

import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;

final /* synthetic */ class ne implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final ny f3535a;

    /* renamed from: b reason: collision with root package name */
    private final my f3536b;
    private final nn c;

    ne(ny nyVar, my myVar, nn nnVar) {
        this.f3535a = nyVar;
        this.f3536b = myVar;
        this.c = nnVar;
    }

    /* JADX WARNING: type inference failed for: r1v2, types: [java.lang.Throwable] */
    /* JADX WARNING: type inference failed for: r0v5, types: [java.lang.Throwable] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    public final void run() {
        ny nyVar = this.f3535a;
        try {
            nyVar.b(this.f3536b.a(this.c.get()));
        } catch (CancellationException e) {
            nyVar.cancel(true);
        } catch (ExecutionException e2) {
            e = e2;
            ? cause = e.getCause();
            if (cause != 0) {
                e = cause;
            }
            nyVar.a(e);
        } catch (InterruptedException e3) {
            Thread.currentThread().interrupt();
            nyVar.a(e3);
        } catch (Exception e4) {
            nyVar.a(e4);
        }
    }
}
