package com.google.android.gms.internal.ads;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.RemoteException;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.ae;
import com.google.android.gms.ads.internal.ax;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@cm
public final class ay implements Callable<ir> {

    /* renamed from: a reason: collision with root package name */
    private static long f2996a = 10;

    /* renamed from: b reason: collision with root package name */
    private final Context f2997b;
    private final lf c;
    /* access modifiers changed from: private */
    public final ae d;
    private final ahh e;
    private final bq f;
    private final Object g = new Object();
    private final is h;
    private final asv i;
    private boolean j;
    private int k;
    private List<String> l;
    private JSONObject m;
    private String n;
    private String o;

    public ay(Context context, ae aeVar, lf lfVar, ahh ahh, is isVar, asv asv) {
        this.f2997b = context;
        this.d = aeVar;
        this.c = lfVar;
        this.h = isVar;
        this.e = ahh;
        this.i = asv;
        this.f = aeVar.J();
        this.j = false;
        this.k = -2;
        this.l = null;
        this.n = null;
        this.o = null;
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x003d, code lost:
        if (r2.length() != 0) goto L_0x0043;
     */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0075 A[Catch:{ InterruptedException | CancellationException | JSONException -> 0x018d, ExecutionException -> 0x01a5, TimeoutException -> 0x020b, Exception -> 0x0212 }] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0093 A[Catch:{ InterruptedException | CancellationException | JSONException -> 0x018d, ExecutionException -> 0x01a5, TimeoutException -> 0x020b, Exception -> 0x0212 }] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00b5 A[Catch:{ InterruptedException | CancellationException | JSONException -> 0x018d, ExecutionException -> 0x01a5, TimeoutException -> 0x020b, Exception -> 0x0212 }] */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x0197  */
    /* renamed from: a */
    public final ir call() {
        JSONObject jSONObject;
        boolean optBoolean;
        nn nnVar;
        bh bhVar;
        aub aub;
        String[] strArr;
        try {
            String j_ = this.d.j_();
            if (!b()) {
                JSONObject jSONObject2 = new JSONObject(this.h.f3398b.f3266b);
                JSONObject jSONObject3 = new JSONObject(this.h.f3398b.f3266b);
                if (jSONObject3.length() != 0) {
                    JSONArray optJSONArray = jSONObject3.optJSONArray("ads");
                    JSONObject jSONObject4 = optJSONArray != null ? optJSONArray.optJSONObject(0) : null;
                    if (jSONObject4 != null) {
                    }
                }
                a(3);
                JSONObject jSONObject5 = (JSONObject) this.f.a(jSONObject2).get(f2996a, TimeUnit.SECONDS);
                if (jSONObject5.optBoolean("success", false)) {
                    jSONObject = jSONObject5.getJSONObject("json").optJSONArray("ads").getJSONObject(0);
                    optBoolean = jSONObject.optBoolean("enable_omid");
                    if (optBoolean) {
                        nnVar = nc.a(null);
                    } else {
                        JSONObject optJSONObject = jSONObject.optJSONObject("omid_settings");
                        if (optJSONObject == null) {
                            nnVar = nc.a(null);
                        } else {
                            String optString = optJSONObject.optString("omid_html");
                            if (TextUtils.isEmpty(optString)) {
                                nnVar = nc.a(null);
                            } else {
                                ny nyVar = new ny();
                                nt.f3558a.execute(new az(this, nyVar, optString));
                                nnVar = nyVar;
                            }
                        }
                    }
                    if (!b() || jSONObject == null) {
                        bhVar = null;
                    } else {
                        String string = jSONObject.getString("template_id");
                        boolean z = this.h.f3397a.y != null ? this.h.f3397a.y.f2938b : false;
                        boolean z2 = this.h.f3397a.y != null ? this.h.f3397a.y.d : false;
                        if ("2".equals(string)) {
                            bhVar = new br(z, z2, this.h.j);
                        } else if ("1".equals(string)) {
                            bhVar = new bs(z, z2, this.h.j);
                        } else {
                            if ("3".equals(string)) {
                                String string2 = jSONObject.getString("custom_template_id");
                                ny nyVar2 = new ny();
                                jv.f3440a.post(new bb(this, nyVar2, string2));
                                if (nyVar2.get(f2996a, TimeUnit.SECONDS) != null) {
                                    bhVar = new bt(z);
                                } else {
                                    String str = "No handler for custom template: ";
                                    String valueOf = String.valueOf(jSONObject.getString("custom_template_id"));
                                    jm.c(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
                                }
                            } else {
                                a(0);
                            }
                            bhVar = null;
                        }
                    }
                    if (!b() || bhVar == null || jSONObject == null) {
                        aub = null;
                    } else {
                        JSONObject jSONObject6 = jSONObject.getJSONObject("tracking_urls_and_actions");
                        JSONArray optJSONArray2 = jSONObject6.optJSONArray("impression_tracking_urls");
                        if (optJSONArray2 == null) {
                            strArr = null;
                        } else {
                            strArr = new String[optJSONArray2.length()];
                            for (int i2 = 0; i2 < optJSONArray2.length(); i2++) {
                                strArr[i2] = optJSONArray2.getString(i2);
                            }
                        }
                        this.l = strArr == null ? null : Arrays.asList(strArr);
                        this.m = jSONObject6.optJSONObject("active_view");
                        this.n = jSONObject.optString("debug_signals");
                        this.o = jSONObject.optString("omid_settings");
                        aub a2 = bhVar.a(this, jSONObject);
                        a2.a(new aud(this.f2997b, this.d, this.f, this.e, jSONObject, a2, this.h.f3397a.k, j_));
                        aub = a2;
                    }
                    if (aub instanceof atr) {
                        this.f.a("/nativeAdCustomClick", (com.google.android.gms.ads.internal.gmsg.ae<? super T>) new bc<Object>(this, (atr) aub));
                    }
                    ir a3 = a(aub, optBoolean);
                    this.d.b(b(nnVar));
                    return a3;
                }
            }
            jSONObject = null;
            optBoolean = jSONObject.optBoolean("enable_omid");
            if (optBoolean) {
            }
            if (!b()) {
            }
            bhVar = null;
            if (!b()) {
            }
            aub = null;
            if (aub instanceof atr) {
            }
            ir a32 = a(aub, optBoolean);
            this.d.b(b(nnVar));
            return a32;
        } catch (InterruptedException | CancellationException | JSONException e2) {
            e = e2;
            jm.c("Malformed native JSON response.", e);
            if (!this.j) {
                a(0);
            }
            return a((aub) null, false);
        } catch (ExecutionException e3) {
            e = e3;
            jm.c("Malformed native JSON response.", e);
            if (!this.j) {
            }
            return a((aub) null, false);
        } catch (TimeoutException e4) {
            jm.c("Timeout when loading native ad.", e4);
            if (!this.j) {
            }
            return a((aub) null, false);
        } catch (Exception e5) {
            jm.c("Error occured while doing native ads initialization.", e5);
            if (!this.j) {
            }
            return a((aub) null, false);
        }
    }

    private final ir a(aub aub, boolean z) {
        int i2;
        synchronized (this.g) {
            i2 = this.k;
            if (aub == null && this.k == -2) {
                i2 = 0;
            }
        }
        return new ir(this.h.f3397a.c, null, this.h.f3398b.c, i2, this.h.f3398b.e, this.l, this.h.f3398b.k, this.h.f3398b.j, this.h.f3397a.i, false, null, null, null, null, null, 0, this.h.d, this.h.f3398b.f, this.h.f, this.h.g, this.h.f3398b.n, this.m, i2 != -2 ? null : aub, null, null, null, this.h.f3398b.D, this.h.f3398b.E, null, this.h.f3398b.H, this.n, this.h.i, this.h.f3398b.O, this.h.j, z, this.h.f3398b.R, this.h.f3398b.S, this.o);
    }

    private final nn<atm> a(JSONObject jSONObject, boolean z, boolean z2) throws JSONException {
        String optString = z ? jSONObject.getString("url") : jSONObject.optString("url");
        double optDouble = jSONObject.optDouble("scale", 1.0d);
        boolean optBoolean = jSONObject.optBoolean("is_transparent", true);
        if (!TextUtils.isEmpty(optString)) {
            return z2 ? nc.a(new atm(null, Uri.parse(optString), optDouble)) : this.c.a(optString, (ll<T>) new bf<T>(this, z, optDouble, optBoolean, optString));
        }
        a(0, z);
        return nc.a(null);
    }

    static qn a(nn<qn> nnVar) {
        try {
            return (qn) nnVar.get((long) ((Integer) ape.f().a(asi.cc)).intValue(), TimeUnit.SECONDS);
        } catch (InterruptedException e2) {
            jm.c("InterruptedException occurred while waiting for video to load", e2);
            Thread.currentThread().interrupt();
        } catch (CancellationException | ExecutionException | TimeoutException e3) {
            jm.c("Exception occurred while waiting for video to load", e3);
        }
        return null;
    }

    private final void a(int i2) {
        synchronized (this.g) {
            this.j = true;
            this.k = i2;
        }
    }

    /* access modifiers changed from: private */
    public final void a(avt avt, String str) {
        try {
            awe b2 = this.d.b(avt.l());
            if (b2 != null) {
                b2.a(avt, str);
            }
        } catch (RemoteException e2) {
            jm.c(new StringBuilder(String.valueOf(str).length() + 40).append("Failed to call onCustomClick for asset ").append(str).append(".").toString(), e2);
        }
    }

    private static qn b(nn<qn> nnVar) {
        try {
            return (qn) nnVar.get((long) ((Integer) ape.f().a(asi.cd)).intValue(), TimeUnit.MILLISECONDS);
        } catch (InterruptedException e2) {
            ms.c("", e2);
            Thread.currentThread().interrupt();
        } catch (CancellationException | ExecutionException | TimeoutException e3) {
            ms.c("", e3);
        }
        return null;
    }

    private static Integer b(JSONObject jSONObject, String str) {
        try {
            JSONObject jSONObject2 = jSONObject.getJSONObject(str);
            return Integer.valueOf(Color.rgb(jSONObject2.getInt("r"), jSONObject2.getInt("g"), jSONObject2.getInt("b")));
        } catch (JSONException e2) {
            return null;
        }
    }

    /* access modifiers changed from: private */
    public static <V> List<V> b(List<nn<V>> list) throws ExecutionException, InterruptedException {
        ArrayList arrayList = new ArrayList();
        for (nn nnVar : list) {
            Object obj = nnVar.get();
            if (obj != null) {
                arrayList.add(obj);
            }
        }
        return arrayList;
    }

    private final boolean b() {
        boolean z;
        synchronized (this.g) {
            z = this.j;
        }
        return z;
    }

    public final nn<ati> a(JSONObject jSONObject) throws JSONException {
        List<nn> list;
        JSONObject optJSONObject = jSONObject.optJSONObject("attribution");
        if (optJSONObject == null) {
            return nc.a(null);
        }
        String optString = optJSONObject.optString("text");
        int optInt = optJSONObject.optInt("text_size", -1);
        Integer b2 = b(optJSONObject, "text_color");
        Integer b3 = b(optJSONObject, "bg_color");
        int optInt2 = optJSONObject.optInt("animation_ms", 1000);
        int optInt3 = optJSONObject.optInt("presentation_ms", 4000);
        int i2 = (this.h.f3397a.y == null || this.h.f3397a.y.f2937a < 2) ? 1 : this.h.f3397a.y.e;
        boolean optBoolean = optJSONObject.optBoolean("allow_pub_rendering");
        List arrayList = new ArrayList();
        if (optJSONObject.optJSONArray("images") != null) {
            list = a(optJSONObject, "images", false, false, true);
        } else {
            arrayList.add(a(optJSONObject, "image", false, false));
            list = arrayList;
        }
        ny nyVar = new ny();
        int size = list.size();
        AtomicInteger atomicInteger = new AtomicInteger(0);
        for (nn a2 : list) {
            a2.a(new bg(atomicInteger, size, nyVar, list), jt.f3436a);
        }
        return nc.a((nn<A>) nyVar, (my<A, B>) new be<A,B>(this, optString, b3, b2, optInt, optInt3, optInt2, i2, optBoolean), (Executor) jt.f3436a);
    }

    public final nn<qn> a(JSONObject jSONObject, String str) throws JSONException {
        JSONObject optJSONObject = jSONObject.optJSONObject(str);
        if (optJSONObject == null) {
            return nc.a(null);
        }
        if (TextUtils.isEmpty(optJSONObject.optString("vast_xml"))) {
            jm.e("Required field 'vast_xml' is missing");
            return nc.a(null);
        }
        bi biVar = new bi(this.f2997b, this.e, this.h, this.i, this.d);
        ny nyVar = new ny();
        nt.f3558a.execute(new bj(biVar, optJSONObject, nyVar));
        return nyVar;
    }

    public final nn<atm> a(JSONObject jSONObject, String str, boolean z, boolean z2) throws JSONException {
        JSONObject optJSONObject = z ? jSONObject.getJSONObject(str) : jSONObject.optJSONObject(str);
        if (optJSONObject == null) {
            optJSONObject = new JSONObject();
        }
        return a(optJSONObject, z, z2);
    }

    public final List<nn<atm>> a(JSONObject jSONObject, String str, boolean z, boolean z2, boolean z3) throws JSONException {
        JSONArray optJSONArray = jSONObject.optJSONArray(str);
        ArrayList arrayList = new ArrayList();
        if (optJSONArray == null || optJSONArray.length() == 0) {
            a(0, false);
            return arrayList;
        }
        int i2 = z3 ? optJSONArray.length() : 1;
        for (int i3 = 0; i3 < i2; i3++) {
            JSONObject jSONObject2 = optJSONArray.getJSONObject(i3);
            if (jSONObject2 == null) {
                jSONObject2 = new JSONObject();
            }
            arrayList.add(a(jSONObject2, false, z2));
        }
        return arrayList;
    }

    public final Future<atm> a(JSONObject jSONObject, String str, boolean z) throws JSONException {
        JSONObject jSONObject2 = jSONObject.getJSONObject(str);
        boolean optBoolean = jSONObject2.optBoolean("require", true);
        if (jSONObject2 == null) {
            jSONObject2 = new JSONObject();
        }
        return a(jSONObject2, optBoolean, z);
    }

    public final void a(int i2, boolean z) {
        if (z) {
            a(i2);
        }
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ void a(ny nyVar, String str) {
        try {
            ax.f();
            qn a2 = qu.a(this.f2997b, sb.a(), "native-omid", false, false, this.e, this.h.f3397a.k, this.i, null, this.d.i(), this.h.i);
            a2.v().a((rw) new ba(nyVar, a2));
            a2.loadData(str, "text/html", "UTF-8");
        } catch (Exception e2) {
            nyVar.b(null);
            ms.c("", e2);
        }
    }
}
