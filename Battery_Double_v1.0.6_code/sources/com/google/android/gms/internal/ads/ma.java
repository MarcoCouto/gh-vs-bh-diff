package com.google.android.gms.internal.ads;

import android.annotation.TargetApi;

@TargetApi(17)
@cm
public final class ma {

    /* renamed from: b reason: collision with root package name */
    private static ma f3502b = null;

    /* renamed from: a reason: collision with root package name */
    String f3503a;

    private ma() {
    }

    public static ma a() {
        if (f3502b == null) {
            f3502b = new ma();
        }
        return f3502b;
    }
}
