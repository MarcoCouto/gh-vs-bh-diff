package com.google.android.gms.internal.ads;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

final class no extends FilterInputStream {

    /* renamed from: a reason: collision with root package name */
    private final long f3551a;

    /* renamed from: b reason: collision with root package name */
    private long f3552b;

    no(InputStream inputStream, long j) {
        super(inputStream);
        this.f3551a = j;
    }

    /* access modifiers changed from: 0000 */
    public final long a() {
        return this.f3551a - this.f3552b;
    }

    public final int read() throws IOException {
        int read = super.read();
        if (read != -1) {
            this.f3552b++;
        }
        return read;
    }

    public final int read(byte[] bArr, int i, int i2) throws IOException {
        int read = super.read(bArr, i, i2);
        if (read != -1) {
            this.f3552b += (long) read;
        }
        return read;
    }
}
