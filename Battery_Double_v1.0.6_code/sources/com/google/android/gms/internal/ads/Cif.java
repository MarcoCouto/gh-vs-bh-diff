package com.google.android.gms.internal.ads;

import android.annotation.TargetApi;
import android.content.Context;
import com.google.android.gms.ads.internal.ax;
import com.google.android.gms.common.util.n;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@TargetApi(21)
@cm
/* renamed from: com.google.android.gms.internal.ads.if reason: invalid class name */
final class Cif {

    /* renamed from: a reason: collision with root package name */
    private static final Map<String, String> f3382a;

    /* renamed from: b reason: collision with root package name */
    private final Context f3383b;
    private final List<String> c;
    private final hr d;

    static {
        HashMap hashMap = new HashMap();
        if (n.i()) {
            hashMap.put("android.webkit.resource.AUDIO_CAPTURE", "android.permission.RECORD_AUDIO");
            hashMap.put("android.webkit.resource.VIDEO_CAPTURE", "android.permission.CAMERA");
        }
        f3382a = hashMap;
    }

    Cif(Context context, List<String> list, hr hrVar) {
        this.f3383b = context;
        this.c = list;
        this.d = hrVar;
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0046  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0073  */
    public final List<String> a(String[] strArr) {
        boolean z;
        boolean z2;
        ArrayList arrayList = new ArrayList();
        for (String str : strArr) {
            Iterator it = this.c.iterator();
            while (true) {
                if (!it.hasNext()) {
                    z = false;
                    break;
                }
                String str2 = (String) it.next();
                if (!str2.equals(str)) {
                    String valueOf = String.valueOf("android.webkit.resource.");
                    String valueOf2 = String.valueOf(str2);
                    if ((valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf)).equals(str)) {
                        z = true;
                        break;
                    }
                } else {
                    z = true;
                    break;
                }
            }
            if (z) {
                if (f3382a.containsKey(str)) {
                    ax.e();
                    if (!jv.a(this.f3383b, (String) f3382a.get(str))) {
                        z2 = false;
                        if (!z2) {
                            arrayList.add(str);
                        } else {
                            this.d.c(str);
                        }
                    }
                }
                z2 = true;
                if (!z2) {
                }
            } else {
                this.d.b(str);
            }
        }
        return arrayList;
    }
}
