package com.google.android.gms.internal.ads;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.b.a;

public interface avg extends IInterface {
    void a() throws RemoteException;

    void a(a aVar) throws RemoteException;

    void b(a aVar) throws RemoteException;
}
