package com.google.android.gms.internal.ads;

final /* synthetic */ class nf implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final ny f3537a;

    /* renamed from: b reason: collision with root package name */
    private final mx f3538b;
    private final nn c;

    nf(ny nyVar, mx mxVar, nn nnVar) {
        this.f3537a = nyVar;
        this.f3538b = mxVar;
        this.c = nnVar;
    }

    public final void run() {
        nc.a(this.f3537a, this.f3538b, this.c);
    }
}
