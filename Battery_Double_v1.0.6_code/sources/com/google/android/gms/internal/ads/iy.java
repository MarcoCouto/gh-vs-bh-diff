package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.ax;

final class iy extends jh {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ iw f3408a;

    iy(iw iwVar) {
        this.f3408a = iwVar;
    }

    public final void a() {
        ask ask = new ask(this.f3408a.f, this.f3408a.g.f3528a);
        synchronized (this.f3408a.f3405a) {
            try {
                ax.n();
                asn.a(this.f3408a.h, ask);
            } catch (IllegalArgumentException e) {
                jm.c("Cannot config CSI reporter.", e);
            }
        }
    }

    public final void c_() {
    }
}
