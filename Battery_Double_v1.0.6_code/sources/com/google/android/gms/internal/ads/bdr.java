package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.view.View;
import com.google.android.gms.ads.b.c;
import com.google.android.gms.ads.mediation.g;
import com.google.android.gms.b.a;
import com.google.android.gms.b.b;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@cm
public final class bdr extends bdf {

    /* renamed from: a reason: collision with root package name */
    private final g f3153a;

    public bdr(g gVar) {
        this.f3153a = gVar;
    }

    public final String a() {
        return this.f3153a.i();
    }

    public final void a(a aVar) {
        this.f3153a.c((View) b.a(aVar));
    }

    public final void a(a aVar, a aVar2, a aVar3) {
        this.f3153a.a((View) b.a(aVar), (HashMap) b.a(aVar2), (HashMap) b.a(aVar3));
    }

    public final List b() {
        List<c.b> j = this.f3153a.j();
        if (j == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        for (c.b bVar : j) {
            arrayList.add(new atm(bVar.a(), bVar.b(), bVar.c()));
        }
        return arrayList;
    }

    public final void b(a aVar) {
        this.f3153a.a((View) b.a(aVar));
    }

    public final String c() {
        return this.f3153a.k();
    }

    public final void c(a aVar) {
        this.f3153a.b((View) b.a(aVar));
    }

    public final auw d() {
        c.b l = this.f3153a.l();
        if (l != null) {
            return new atm(l.a(), l.b(), l.c());
        }
        return null;
    }

    public final String e() {
        return this.f3153a.m();
    }

    public final double f() {
        return this.f3153a.n();
    }

    public final String g() {
        return this.f3153a.o();
    }

    public final String h() {
        return this.f3153a.p();
    }

    public final void i() {
        this.f3153a.e();
    }

    public final boolean j() {
        return this.f3153a.a();
    }

    public final boolean k() {
        return this.f3153a.b();
    }

    public final Bundle l() {
        return this.f3153a.c();
    }

    public final aqs m() {
        if (this.f3153a.g() != null) {
            return this.f3153a.g().a();
        }
        return null;
    }

    public final a n() {
        View d = this.f3153a.d();
        if (d == null) {
            return null;
        }
        return b.a(d);
    }

    public final aus o() {
        return null;
    }

    public final a p() {
        View f = this.f3153a.f();
        if (f == null) {
            return null;
        }
        return b.a(f);
    }

    public final a q() {
        return null;
    }
}
