package com.google.android.gms.internal.ads;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

final class acs implements acr {
    acs() {
    }

    public final int a(int i, Object obj, Object obj2) {
        acq acq = (acq) obj;
        if (!acq.isEmpty()) {
            Iterator it = acq.entrySet().iterator();
            if (it.hasNext()) {
                Entry entry = (Entry) it.next();
                entry.getKey();
                entry.getValue();
                throw new NoSuchMethodError();
            }
        }
        return 0;
    }

    public final Object a(Object obj, Object obj2) {
        acq acq = (acq) obj;
        acq acq2 = (acq) obj2;
        if (!acq2.isEmpty()) {
            if (!acq.d()) {
                acq = acq.b();
            }
            acq.a(acq2);
        }
        return acq;
    }

    public final Map<?, ?> a(Object obj) {
        return (acq) obj;
    }

    public final Map<?, ?> b(Object obj) {
        return (acq) obj;
    }

    public final boolean c(Object obj) {
        return !((acq) obj).d();
    }

    public final Object d(Object obj) {
        ((acq) obj).c();
        return obj;
    }

    public final Object e(Object obj) {
        return acq.a().b();
    }

    public final acp<?, ?> f(Object obj) {
        throw new NoSuchMethodError();
    }
}
