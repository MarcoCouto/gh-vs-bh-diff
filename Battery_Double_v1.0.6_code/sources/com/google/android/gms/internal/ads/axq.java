package com.google.android.gms.internal.ads;

import com.google.android.gms.common.internal.e.b;

final class axq implements b {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ ny f2989a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ axl f2990b;

    axq(axl axl, ny nyVar) {
        this.f2990b = axl;
        this.f2989a = nyVar;
    }

    public final void a(com.google.android.gms.common.b bVar) {
        synchronized (this.f2990b.d) {
            this.f2989a.a(new RuntimeException("Connection failed."));
        }
    }
}
