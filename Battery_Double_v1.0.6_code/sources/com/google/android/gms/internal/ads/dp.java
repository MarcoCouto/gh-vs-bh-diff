package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import com.google.android.gms.common.internal.a.a;
import com.google.android.gms.common.internal.a.c;
import com.google.android.gms.common.internal.a.d;
import java.util.Collections;
import java.util.List;

@cm
public final class dp extends a {
    public static final Creator<dp> CREATOR = new dq();
    public final hp A;
    public final List<String> B;
    public final List<String> C;
    public final boolean D;
    public final dr E;
    public final boolean F;
    public String G;
    public final List<String> H;
    public final boolean I;
    public final String J;
    public final hz K;
    public final String L;
    public final boolean M;
    public final boolean N;
    public final boolean O;
    public final int P;
    public final boolean Q;
    public final List<String> R;
    public final boolean S;
    public final String T;
    private dl U;
    private final int V;
    private eb W;
    private Bundle X;

    /* renamed from: a reason: collision with root package name */
    public final String f3265a;

    /* renamed from: b reason: collision with root package name */
    public String f3266b;
    public final List<String> c;
    public final int d;
    public final List<String> e;
    public final long f;
    public final boolean g;
    public final long h;
    public final List<String> i;
    public final long j;
    public final int k;
    public final String l;
    public final long m;
    public final String n;
    public final boolean o;
    public final String p;
    public final String q;
    public final boolean r;
    public final boolean s;
    public final boolean t;
    public final boolean u;
    public final boolean v;
    public String w;
    public final String x;
    public final boolean y;
    public final boolean z;

    public dp(int i2) {
        this(19, null, null, null, i2, null, -1, false, -1, null, -1, -1, null, -1, null, false, null, null, false, false, false, true, false, null, null, null, false, false, null, null, null, false, null, false, null, null, false, null, null, null, true, false, null, false, 0, false, null, false, null);
    }

    public dp(int i2, long j2) {
        this(19, null, null, null, i2, null, -1, false, -1, null, j2, -1, null, -1, null, false, null, null, false, false, false, true, false, null, null, null, false, false, null, null, null, false, null, false, null, null, false, null, null, null, true, false, null, false, 0, false, null, false, null);
    }

    dp(int i2, String str, String str2, List<String> list, int i3, List<String> list2, long j2, boolean z2, long j3, List<String> list3, long j4, int i4, String str3, long j5, String str4, boolean z3, String str5, String str6, boolean z4, boolean z5, boolean z6, boolean z7, boolean z8, eb ebVar, String str7, String str8, boolean z9, boolean z10, hp hpVar, List<String> list4, List<String> list5, boolean z11, dr drVar, boolean z12, String str9, List<String> list6, boolean z13, String str10, hz hzVar, String str11, boolean z14, boolean z15, Bundle bundle, boolean z16, int i5, boolean z17, List<String> list7, boolean z18, String str12) {
        this.V = i2;
        this.f3265a = str;
        this.f3266b = str2;
        this.c = list != null ? Collections.unmodifiableList(list) : null;
        this.d = i3;
        this.e = list2 != null ? Collections.unmodifiableList(list2) : null;
        this.f = j2;
        this.g = z2;
        this.h = j3;
        this.i = list3 != null ? Collections.unmodifiableList(list3) : null;
        this.j = j4;
        this.k = i4;
        this.l = str3;
        this.m = j5;
        this.n = str4;
        this.o = z3;
        this.p = str5;
        this.q = str6;
        this.r = z4;
        this.s = z5;
        this.t = z6;
        this.u = z7;
        this.M = z14;
        this.v = z8;
        this.W = ebVar;
        this.w = str7;
        this.x = str8;
        if (this.f3266b == null && this.W != null) {
            eq eqVar = (eq) this.W.a(eq.CREATOR);
            if (eqVar != null && !TextUtils.isEmpty(eqVar.f3289a)) {
                this.f3266b = eqVar.f3289a;
            }
        }
        this.y = z9;
        this.z = z10;
        this.A = hpVar;
        this.B = list4;
        this.C = list5;
        this.D = z11;
        this.E = drVar;
        this.F = z12;
        this.G = str9;
        this.H = list6;
        this.I = z13;
        this.J = str10;
        this.K = hzVar;
        this.L = str11;
        this.N = z15;
        this.X = bundle;
        this.O = z16;
        this.P = i5;
        this.Q = z17;
        this.R = list7 != null ? Collections.unmodifiableList(list7) : null;
        this.S = z18;
        this.T = str12;
    }

    public dp(dl dlVar, String str, String str2, List<String> list, List<String> list2, long j2, boolean z2, long j3, List<String> list3, long j4, int i2, String str3, long j5, String str4, String str5, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7, String str6, boolean z8, boolean z9, hp hpVar, List<String> list4, List<String> list5, boolean z10, dr drVar, boolean z11, String str7, List<String> list6, boolean z12, String str8, hz hzVar, String str9, boolean z13, boolean z14, boolean z15, int i3, boolean z16, List<String> list7, boolean z17, String str10) {
        this(19, str, str2, list, -2, list2, j2, z2, -1, list3, j4, i2, str3, j5, str4, false, null, str5, z3, z4, z5, z6, false, null, null, str6, z8, z9, hpVar, list4, list5, z10, drVar, z11, str7, list6, z12, str8, hzVar, str9, z13, z14, null, z15, i3, z16, list7, z17, str10);
        this.U = dlVar;
    }

    public dp(dl dlVar, String str, String str2, List<String> list, List<String> list2, long j2, boolean z2, long j3, List<String> list3, long j4, int i2, String str3, long j5, String str4, boolean z3, String str5, String str6, boolean z4, boolean z5, boolean z6, boolean z7, boolean z8, String str7, boolean z9, boolean z10, hp hpVar, List<String> list4, List<String> list5, boolean z11, dr drVar, boolean z12, String str8, List<String> list6, boolean z13, String str9, hz hzVar, String str10, boolean z14, boolean z15, boolean z16, int i3, boolean z17, List<String> list7, boolean z18, String str11) {
        this(19, str, str2, list, -2, list2, j2, z2, j3, list3, j4, i2, str3, j5, str4, z3, str5, str6, z4, z5, z6, z7, z8, null, null, str7, z9, z10, hpVar, list4, list5, z11, drVar, z12, str8, list6, z13, str9, hzVar, str10, z14, z15, null, z16, 0, z17, list7, z18, str11);
        this.U = dlVar;
    }

    public final void writeToParcel(Parcel parcel, int i2) {
        if (this.U != null && this.U.f3260a >= 9 && !TextUtils.isEmpty(this.f3266b)) {
            this.W = new eb((d) new eq(this.f3266b));
            this.f3266b = null;
        }
        int a2 = c.a(parcel);
        c.a(parcel, 1, this.V);
        c.a(parcel, 2, this.f3265a, false);
        c.a(parcel, 3, this.f3266b, false);
        c.b(parcel, 4, this.c, false);
        c.a(parcel, 5, this.d);
        c.b(parcel, 6, this.e, false);
        c.a(parcel, 7, this.f);
        c.a(parcel, 8, this.g);
        c.a(parcel, 9, this.h);
        c.b(parcel, 10, this.i, false);
        c.a(parcel, 11, this.j);
        c.a(parcel, 12, this.k);
        c.a(parcel, 13, this.l, false);
        c.a(parcel, 14, this.m);
        c.a(parcel, 15, this.n, false);
        c.a(parcel, 18, this.o);
        c.a(parcel, 19, this.p, false);
        c.a(parcel, 21, this.q, false);
        c.a(parcel, 22, this.r);
        c.a(parcel, 23, this.s);
        c.a(parcel, 24, this.t);
        c.a(parcel, 25, this.u);
        c.a(parcel, 26, this.v);
        c.a(parcel, 28, (Parcelable) this.W, i2, false);
        c.a(parcel, 29, this.w, false);
        c.a(parcel, 30, this.x, false);
        c.a(parcel, 31, this.y);
        c.a(parcel, 32, this.z);
        c.a(parcel, 33, (Parcelable) this.A, i2, false);
        c.b(parcel, 34, this.B, false);
        c.b(parcel, 35, this.C, false);
        c.a(parcel, 36, this.D);
        c.a(parcel, 37, (Parcelable) this.E, i2, false);
        c.a(parcel, 38, this.F);
        c.a(parcel, 39, this.G, false);
        c.b(parcel, 40, this.H, false);
        c.a(parcel, 42, this.I);
        c.a(parcel, 43, this.J, false);
        c.a(parcel, 44, (Parcelable) this.K, i2, false);
        c.a(parcel, 45, this.L, false);
        c.a(parcel, 46, this.M);
        c.a(parcel, 47, this.N);
        c.a(parcel, 48, this.X, false);
        c.a(parcel, 49, this.O);
        c.a(parcel, 50, this.P);
        c.a(parcel, 51, this.Q);
        c.b(parcel, 52, this.R, false);
        c.a(parcel, 53, this.S);
        c.a(parcel, 54, this.T, false);
        c.a(parcel, a2);
    }
}
