package com.google.android.gms.internal.ads;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

public final class jk {
    private static final Comparator<byte[]> e = new kl();

    /* renamed from: a reason: collision with root package name */
    private final List<byte[]> f3427a = new LinkedList();

    /* renamed from: b reason: collision with root package name */
    private final List<byte[]> f3428b = new ArrayList(64);
    private int c = 0;
    private final int d = 4096;

    public jk(int i) {
    }

    private final synchronized void a() {
        while (this.c > this.d) {
            byte[] bArr = (byte[]) this.f3427a.remove(0);
            this.f3428b.remove(bArr);
            this.c -= bArr.length;
        }
    }

    public final synchronized void a(byte[] bArr) {
        if (bArr != null) {
            if (bArr.length <= this.d) {
                this.f3427a.add(bArr);
                int binarySearch = Collections.binarySearch(this.f3428b, bArr, e);
                if (binarySearch < 0) {
                    binarySearch = (-binarySearch) - 1;
                }
                this.f3428b.add(binarySearch, bArr);
                this.c += bArr.length;
                a();
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:?, code lost:
        r0 = new byte[r5];
     */
    public final synchronized byte[] a(int i) {
        byte[] bArr;
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= this.f3428b.size()) {
                break;
            }
            bArr = (byte[]) this.f3428b.get(i3);
            if (bArr.length >= i) {
                this.c -= bArr.length;
                this.f3428b.remove(i3);
                this.f3427a.remove(bArr);
                break;
            }
            i2 = i3 + 1;
        }
        return bArr;
    }
}
