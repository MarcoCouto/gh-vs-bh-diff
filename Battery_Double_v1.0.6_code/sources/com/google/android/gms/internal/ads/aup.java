package com.google.android.gms.internal.ads;

import android.graphics.Point;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.ViewTreeObserver.OnScrollChangedListener;
import android.widget.FrameLayout;
import com.google.android.gms.ads.internal.ax;
import com.google.android.gms.b.a;
import com.google.android.gms.b.b;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

@cm
public final class aup extends avh implements OnClickListener, OnTouchListener, OnGlobalLayoutListener, OnScrollChangedListener {

    /* renamed from: a reason: collision with root package name */
    static final String[] f2943a = {"2011", "1009", "3010"};

    /* renamed from: b reason: collision with root package name */
    private final Object f2944b = new Object();
    private final WeakReference<View> c;
    private final Map<String, WeakReference<View>> d = new HashMap();
    private final Map<String, WeakReference<View>> e = new HashMap();
    private final Map<String, WeakReference<View>> f = new HashMap();
    private aty g;
    private View h;
    private Point i = new Point();
    private Point j = new Point();
    private WeakReference<akr> k = new WeakReference<>(null);

    public aup(View view, HashMap<String, View> hashMap, HashMap<String, View> hashMap2) {
        ax.A();
        og.a(view, (OnGlobalLayoutListener) this);
        ax.A();
        og.a(view, (OnScrollChangedListener) this);
        view.setOnTouchListener(this);
        view.setOnClickListener(this);
        this.c = new WeakReference<>(view);
        for (Entry entry : hashMap.entrySet()) {
            String str = (String) entry.getKey();
            View view2 = (View) entry.getValue();
            if (view2 != null) {
                this.d.put(str, new WeakReference(view2));
                if (!"1098".equals(str) && !"3011".equals(str)) {
                    view2.setOnTouchListener(this);
                    view2.setClickable(true);
                    view2.setOnClickListener(this);
                }
            }
        }
        this.f.putAll(this.d);
        for (Entry entry2 : hashMap2.entrySet()) {
            View view3 = (View) entry2.getValue();
            if (view3 != null) {
                this.e.put((String) entry2.getKey(), new WeakReference(view3));
                view3.setOnTouchListener(this);
            }
        }
        this.f.putAll(this.e);
        asi.a(view.getContext());
    }

    private final int a(int i2) {
        int b2;
        synchronized (this.f2944b) {
            ape.a();
            b2 = mh.b(this.g.m(), i2);
        }
        return b2;
    }

    private final void a(View view) {
        synchronized (this.f2944b) {
            if (this.g != null) {
                aty aty = this.g instanceof atx ? ((atx) this.g).f() : this.g;
                if (aty != null) {
                    aty.c(view);
                }
            }
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:27:?, code lost:
        return;
     */
    public final void a(aud aud) {
        View view;
        synchronized (this.f2944b) {
            String[] strArr = f2943a;
            int length = strArr.length;
            int i2 = 0;
            while (true) {
                if (i2 >= length) {
                    view = null;
                    break;
                }
                WeakReference weakReference = (WeakReference) this.f.get(strArr[i2]);
                if (weakReference != null) {
                    view = (View) weakReference.get();
                    break;
                }
                i2++;
            }
            if (!(view instanceof FrameLayout)) {
                aud.i();
                return;
            }
            aur aur = new aur(this, view);
            if (aud instanceof atx) {
                aud.b(view, (atw) aur);
            } else {
                aud.a(view, (atw) aur);
            }
        }
    }

    /* access modifiers changed from: private */
    public final boolean a(String[] strArr) {
        for (String str : strArr) {
            if (this.d.get(str) != null) {
                return true;
            }
        }
        for (String str2 : strArr) {
            if (this.e.get(str2) != null) {
                return false;
            }
        }
        return false;
    }

    public final void a() {
        synchronized (this.f2944b) {
            this.h = null;
            this.g = null;
            this.i = null;
            this.j = null;
        }
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    public final void a(a aVar) {
        View view;
        synchronized (this.f2944b) {
            a((View) null);
            Object a2 = b.a(aVar);
            if (!(a2 instanceof aud)) {
                jm.e("Not an instance of native engine. This is most likely a transient error");
                return;
            }
            aud aud = (aud) a2;
            if (!aud.b()) {
                jm.c("Your account must be enabled to use this feature. Talk to your account manager to request this feature for your account.");
                return;
            }
            View view2 = (View) this.c.get();
            if (!(this.g == null || view2 == null)) {
                if (((Boolean) ape.f().a(asi.bZ)).booleanValue()) {
                    this.g.b(view2, this.f);
                }
            }
            synchronized (this.f2944b) {
                if (this.g instanceof aud) {
                    aud aud2 = (aud) this.g;
                    View view3 = (View) this.c.get();
                    if (!(aud2 == null || aud2.m() == null || view3 == null || !ax.B().c(view3.getContext()))) {
                        ig n = aud2.n();
                        if (n != null) {
                            n.a(false);
                        }
                        akr akr = (akr) this.k.get();
                        if (!(akr == null || n == null)) {
                            akr.b((akv) n);
                        }
                    }
                }
            }
            if (!(this.g instanceof atx) || !((atx) this.g).e()) {
                this.g = aud;
                if (aud instanceof atx) {
                    ((atx) aud).a((aty) null);
                }
            } else {
                ((atx) this.g).a((aty) aud);
            }
            String[] strArr = {"1098", "3011"};
            int i2 = 0;
            while (true) {
                if (i2 >= 2) {
                    view = null;
                    break;
                }
                WeakReference weakReference = (WeakReference) this.f.get(strArr[i2]);
                if (weakReference != null) {
                    view = (View) weakReference.get();
                    break;
                }
                i2++;
            }
            if (view == null) {
                jm.e("Ad choices asset view is not provided.");
            } else {
                ViewGroup viewGroup = view instanceof ViewGroup ? (ViewGroup) view : null;
                if (viewGroup != null) {
                    this.h = aud.a((OnClickListener) this, true);
                    if (this.h != null) {
                        this.f.put("1007", new WeakReference(this.h));
                        this.d.put("1007", new WeakReference(this.h));
                        viewGroup.removeAllViews();
                        viewGroup.addView(this.h);
                    }
                }
            }
            aud.a(view2, this.d, this.e, (OnTouchListener) this, (OnClickListener) this);
            jv.f3440a.post(new auq(this, aud));
            a(view2);
            this.g.b(view2);
            synchronized (this.f2944b) {
                if (this.g instanceof aud) {
                    aud aud3 = (aud) this.g;
                    View view4 = (View) this.c.get();
                    if (!(aud3 == null || aud3.m() == null || view4 == null || !ax.B().c(view4.getContext()))) {
                        akr akr2 = (akr) this.k.get();
                        if (akr2 == null) {
                            akr2 = new akr(view4.getContext(), view4);
                            this.k = new WeakReference<>(akr2);
                        }
                        akr2.a((akv) aud3.n());
                    }
                }
            }
        }
    }

    public final void b(a aVar) {
        synchronized (this.f2944b) {
            this.g.a((View) b.a(aVar));
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:29:?, code lost:
        return;
     */
    public final void onClick(View view) {
        synchronized (this.f2944b) {
            if (this.g != null) {
                View view2 = (View) this.c.get();
                if (view2 != null) {
                    Bundle bundle = new Bundle();
                    bundle.putFloat("x", (float) a(this.i.x));
                    bundle.putFloat("y", (float) a(this.i.y));
                    bundle.putFloat("start_x", (float) a(this.j.x));
                    bundle.putFloat("start_y", (float) a(this.j.y));
                    if (this.h == null || !this.h.equals(view)) {
                        this.g.a(view, this.f, bundle, view2);
                    } else if (!(this.g instanceof atx)) {
                        this.g.a(view, "1007", bundle, this.f, view2);
                    } else if (((atx) this.g).f() != null) {
                        ((atx) this.g).f().a(view, "1007", bundle, this.f, view2);
                    }
                }
            }
        }
    }

    public final void onGlobalLayout() {
        synchronized (this.f2944b) {
            if (this.g != null) {
                View view = (View) this.c.get();
                if (view != null) {
                    this.g.c(view, this.f);
                }
            }
        }
    }

    public final void onScrollChanged() {
        synchronized (this.f2944b) {
            if (this.g != null) {
                View view = (View) this.c.get();
                if (view != null) {
                    this.g.c(view, this.f);
                }
            }
        }
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        synchronized (this.f2944b) {
            if (this.g != null) {
                View view2 = (View) this.c.get();
                if (view2 != null) {
                    int[] iArr = new int[2];
                    view2.getLocationOnScreen(iArr);
                    Point point = new Point((int) (motionEvent.getRawX() - ((float) iArr[0])), (int) (motionEvent.getRawY() - ((float) iArr[1])));
                    this.i = point;
                    if (motionEvent.getAction() == 0) {
                        this.j = point;
                    }
                    MotionEvent obtain = MotionEvent.obtain(motionEvent);
                    obtain.setLocation((float) point.x, (float) point.y);
                    this.g.a(obtain);
                    obtain.recycle();
                }
            }
        }
        return false;
    }
}
