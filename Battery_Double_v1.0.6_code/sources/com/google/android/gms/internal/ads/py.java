package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.ads.internal.ax;
import com.google.android.gms.common.api.h;
import java.lang.ref.WeakReference;
import java.util.Map;

@cm
public abstract class py implements h {

    /* renamed from: a reason: collision with root package name */
    protected Context f3619a;

    /* renamed from: b reason: collision with root package name */
    private String f3620b;
    private WeakReference<pm> c;

    public py(pm pmVar) {
        this.f3619a = pmVar.getContext();
        this.f3620b = ax.e().b(this.f3619a, pmVar.k().f3528a);
        this.c = new WeakReference<>(pmVar);
    }

    /* access modifiers changed from: private */
    public final void a(String str, Map<String, String> map) {
        pm pmVar = (pm) this.c.get();
        if (pmVar != null) {
            pmVar.a(str, map);
        }
    }

    /* access modifiers changed from: private */
    public static String b(String str) {
        String str2 = "internal";
        char c2 = 65535;
        switch (str.hashCode()) {
            case -1947652542:
                if (str.equals("interrupted")) {
                    c2 = 3;
                    break;
                }
                break;
            case -1396664534:
                if (str.equals("badUrl")) {
                    c2 = 8;
                    break;
                }
                break;
            case -1347010958:
                if (str.equals("inProgress")) {
                    c2 = 2;
                    break;
                }
                break;
            case -918817863:
                if (str.equals("downloadTimeout")) {
                    c2 = 9;
                    break;
                }
                break;
            case -659376217:
                if (str.equals("contentLengthMissing")) {
                    c2 = 0;
                    break;
                }
                break;
            case -642208130:
                if (str.equals("playerFailed")) {
                    c2 = 5;
                    break;
                }
                break;
            case -354048396:
                if (str.equals("sizeExceeded")) {
                    c2 = 11;
                    break;
                }
                break;
            case -32082395:
                if (str.equals("externalAbort")) {
                    c2 = 10;
                    break;
                }
                break;
            case 3387234:
                if (str.equals("noop")) {
                    c2 = 4;
                    break;
                }
                break;
            case 96784904:
                if (str.equals("error")) {
                    c2 = 1;
                    break;
                }
                break;
            case 580119100:
                if (str.equals("expireFailed")) {
                    c2 = 6;
                    break;
                }
                break;
            case 725497484:
                if (str.equals("noCacheDir")) {
                    c2 = 7;
                    break;
                }
                break;
        }
        switch (c2) {
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
                return "internal";
            case 6:
            case 7:
                return "io";
            case 8:
            case 9:
                return "network";
            case 10:
            case 11:
                return "policy";
            default:
                return str2;
        }
    }

    public void a() {
    }

    /* access modifiers changed from: protected */
    public final void a(String str, String str2, int i) {
        mh.f3514a.post(new qa(this, str, str2, i));
    }

    public final void a(String str, String str2, String str3, String str4) {
        mh.f3514a.post(new qb(this, str, str2, str3, str4));
    }

    public abstract boolean a(String str);

    public abstract void b();
}
