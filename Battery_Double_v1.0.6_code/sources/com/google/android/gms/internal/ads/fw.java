package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.RemoteException;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.ax;
import com.google.android.gms.ads.internal.ay;
import com.google.android.gms.ads.internal.gmsg.k;
import com.google.android.gms.b.b;
import com.google.android.gms.common.internal.aa;
import java.util.HashMap;
import java.util.Map;

@cm
public final class fw {

    /* renamed from: a reason: collision with root package name */
    private static final bcq f3330a = new bcq();

    /* renamed from: b reason: collision with root package name */
    private final bcr f3331b;
    private final ay c;
    private final Map<String, hk> d = new HashMap();
    private final hc e;
    private final k f;
    private final ap g;

    public fw(ay ayVar, bcr bcr, hc hcVar, k kVar, ap apVar) {
        this.c = ayVar;
        this.f3331b = bcr;
        this.e = hcVar;
        this.f = kVar;
        this.g = apVar;
    }

    public static boolean a(ir irVar, ir irVar2) {
        return true;
    }

    public final k a() {
        return this.f;
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0038  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0041  */
    public final hk a(String str) {
        Throwable th;
        hk hkVar;
        hk hkVar2 = (hk) this.d.get(str);
        if (hkVar2 != null) {
            return hkVar2;
        }
        try {
            hkVar = new hk(("com.google.ads.mediation.admob.AdMobAdapter".equals(str) ? f3330a : this.f3331b).a(str), this.e);
            try {
                this.d.put(str, hkVar);
                return hkVar;
            } catch (Exception e2) {
                th = e2;
                String str2 = "Fail to instantiate adapter ";
                String valueOf = String.valueOf(str);
                jm.c(valueOf.length() == 0 ? str2.concat(valueOf) : new String(str2), th);
                return hkVar;
            }
        } catch (Exception e3) {
            th = e3;
            hkVar = hkVar2;
            String str22 = "Fail to instantiate adapter ";
            String valueOf2 = String.valueOf(str);
            jm.c(valueOf2.length() == 0 ? str22.concat(valueOf2) : new String(str22), th);
            return hkVar;
        }
    }

    public final hp a(hp hpVar) {
        if (!(this.c.j == null || this.c.j.r == null || TextUtils.isEmpty(this.c.j.r.k))) {
            hpVar = new hp(this.c.j.r.k, this.c.j.r.l);
        }
        if (!(this.c.j == null || this.c.j.o == null)) {
            ax.x();
            bck.a(this.c.c, this.c.e.f3528a, this.c.j.o.m, this.c.E, hpVar);
        }
        return hpVar;
    }

    public final void a(Context context) {
        for (hk a2 : this.d.values()) {
            try {
                a2.a().a(b.a(context));
            } catch (RemoteException e2) {
                jm.b("Unable to call Adapter.onContextChanged.", e2);
            }
        }
    }

    public final void a(boolean z) {
        hk a2 = a(this.c.j.q);
        if (a2 != null && a2.a() != null) {
            try {
                a2.a().a(z);
                a2.a().f();
            } catch (RemoteException e2) {
                jm.d("#007 Could not call remote method.", e2);
            }
        }
    }

    public final ap b() {
        return this.g;
    }

    public final void c() {
        this.c.I = 0;
        ay ayVar = this.c;
        ax.d();
        hf hfVar = new hf(this.c.c, this.c.k, this);
        String str = "AdRenderer: ";
        String valueOf = String.valueOf(hfVar.getClass().getName());
        jm.b(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
        hfVar.c();
        ayVar.h = hfVar;
    }

    public final void d() {
        aa.b("pause must be called on the main UI thread.");
        for (String str : this.d.keySet()) {
            try {
                hk hkVar = (hk) this.d.get(str);
                if (!(hkVar == null || hkVar.a() == null)) {
                    hkVar.a().d();
                }
            } catch (RemoteException e2) {
                jm.d("#007 Could not call remote method.", e2);
            }
        }
    }

    public final void e() {
        aa.b("resume must be called on the main UI thread.");
        for (String str : this.d.keySet()) {
            try {
                hk hkVar = (hk) this.d.get(str);
                if (!(hkVar == null || hkVar.a() == null)) {
                    hkVar.a().e();
                }
            } catch (RemoteException e2) {
                jm.d("#007 Could not call remote method.", e2);
            }
        }
    }

    public final void f() {
        aa.b("destroy must be called on the main UI thread.");
        for (String str : this.d.keySet()) {
            try {
                hk hkVar = (hk) this.d.get(str);
                if (!(hkVar == null || hkVar.a() == null)) {
                    hkVar.a().c();
                }
            } catch (RemoteException e2) {
                jm.d("#007 Could not call remote method.", e2);
            }
        }
    }

    public final void g() {
        if (this.c.j != null && this.c.j.o != null) {
            ax.x();
            bck.a(this.c.c, this.c.e.f3528a, this.c.j, this.c.f1990b, false, this.c.j.o.l);
        }
    }

    public final void h() {
        if (this.c.j != null && this.c.j.o != null) {
            ax.x();
            bck.a(this.c.c, this.c.e.f3528a, this.c.j, this.c.f1990b, false, this.c.j.o.n);
        }
    }
}
