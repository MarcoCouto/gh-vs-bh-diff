package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.b.c;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.ax;
import com.google.android.gms.ads.internal.overlay.AdOverlayInfoParcel;
import com.google.android.gms.ads.mediation.MediationInterstitialAdapter;
import com.google.android.gms.ads.mediation.a;
import com.google.android.gms.ads.mediation.d;
import com.google.android.gms.common.util.n;

@cm
public final class zzzv implements MediationInterstitialAdapter {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public Activity f3832a;
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public d f3833b;
    private Uri c;

    public final void onDestroy() {
        ms.b("Destroying AdMobCustomTabsAdapter adapter.");
    }

    public final void onPause() {
        ms.b("Pausing AdMobCustomTabsAdapter adapter.");
    }

    public final void onResume() {
        ms.b("Resuming AdMobCustomTabsAdapter adapter.");
    }

    public final void requestInterstitialAd(Context context, d dVar, Bundle bundle, a aVar, Bundle bundle2) {
        this.f3833b = dVar;
        if (this.f3833b == null) {
            ms.e("Listener not set for mediation. Returning.");
        } else if (!(context instanceof Activity)) {
            ms.e("AdMobCustomTabs can only work with Activity context. Bailing out.");
            this.f3833b.a(this, 0);
        } else {
            if (!(n.c() && atg.a(context))) {
                ms.e("Default browser does not support custom tabs. Bailing out.");
                this.f3833b.a(this, 0);
                return;
            }
            String string = bundle.getString("tab_url");
            if (TextUtils.isEmpty(string)) {
                ms.e("The tab_url retrieved from mediation metadata is empty. Bailing out.");
                this.f3833b.a(this, 0);
                return;
            }
            this.f3832a = (Activity) context;
            this.c = Uri.parse(string);
            this.f3833b.a(this);
        }
    }

    public final void showInterstitial() {
        c a2 = new c.a().a();
        a2.f259a.setData(this.c);
        jv.f3440a.post(new bet(this, new AdOverlayInfoParcel(new com.google.android.gms.ads.internal.overlay.c(a2.f259a), null, new bes(this), null, new mu(0, 0, false))));
        ax.i().f();
    }
}
