package com.google.android.gms.internal.ads;

import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.util.Iterator;

public abstract class aah implements Serializable, Iterable<Byte> {

    /* renamed from: a reason: collision with root package name */
    public static final aah f2393a = new aao(abr.f2441b);

    /* renamed from: b reason: collision with root package name */
    private static final aal f2394b = (aac.a() ? new aap(null) : new aaj(null));
    private int c = 0;

    aah() {
    }

    public static aah a(String str) {
        return new aao(str.getBytes(abr.f2440a));
    }

    public static aah a(byte[] bArr) {
        return a(bArr, 0, bArr.length);
    }

    public static aah a(byte[] bArr, int i, int i2) {
        return new aao(f2394b.a(bArr, i, i2));
    }

    static int b(int i, int i2, int i3) {
        int i4 = i2 - i;
        if ((i | i2 | i4 | (i3 - i2)) >= 0) {
            return i4;
        }
        if (i < 0) {
            throw new IndexOutOfBoundsException("Beginning index: " + i + " < 0");
        } else if (i2 < i) {
            throw new IndexOutOfBoundsException("Beginning index larger than ending index: " + i + ", " + i2);
        } else {
            throw new IndexOutOfBoundsException("End index: " + i2 + " >= " + i3);
        }
    }

    static aah b(byte[] bArr) {
        return new aao(bArr);
    }

    static aam b(int i) {
        return new aam(i, null);
    }

    public abstract byte a(int i);

    public abstract int a();

    /* access modifiers changed from: protected */
    public abstract int a(int i, int i2, int i3);

    public abstract aah a(int i, int i2);

    /* access modifiers changed from: protected */
    public abstract String a(Charset charset);

    /* access modifiers changed from: 0000 */
    public abstract void a(aag aag) throws IOException;

    /* access modifiers changed from: protected */
    public abstract void a(byte[] bArr, int i, int i2, int i3);

    public final byte[] b() {
        int a2 = a();
        if (a2 == 0) {
            return abr.f2441b;
        }
        byte[] bArr = new byte[a2];
        a(bArr, 0, 0, a2);
        return bArr;
    }

    public final String c() {
        return a() == 0 ? "" : a(abr.f2440a);
    }

    public abstract boolean d();

    public abstract aaq e();

    public abstract boolean equals(Object obj);

    /* access modifiers changed from: protected */
    public final int f() {
        return this.c;
    }

    public final int hashCode() {
        int i = this.c;
        if (i == 0) {
            int a2 = a();
            i = a(a2, 0, a2);
            if (i == 0) {
                i = 1;
            }
            this.c = i;
        }
        return i;
    }

    public /* synthetic */ Iterator iterator() {
        return new aai(this);
    }

    public final String toString() {
        return String.format("<ByteString@%s size=%d>", new Object[]{Integer.toHexString(System.identityHashCode(this)), Integer.valueOf(a())});
    }
}
