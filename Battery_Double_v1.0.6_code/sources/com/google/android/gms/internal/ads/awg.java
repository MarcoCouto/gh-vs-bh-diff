package com.google.android.gms.internal.ads;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public final class awg extends ajk implements awe {
    awg(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.formats.client.IOnCustomClickListener");
    }

    public final void a(avt avt, String str) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (IInterface) avt);
        r_.writeString(str);
        b(1, r_);
    }
}
