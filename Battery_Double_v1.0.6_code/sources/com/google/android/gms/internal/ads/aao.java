package com.google.android.gms.internal.ads;

import java.io.IOException;
import java.nio.charset.Charset;

class aao extends aan {

    /* renamed from: b reason: collision with root package name */
    protected final byte[] f2399b;

    aao(byte[] bArr) {
        this.f2399b = bArr;
    }

    public byte a(int i) {
        return this.f2399b[i];
    }

    public int a() {
        return this.f2399b.length;
    }

    /* access modifiers changed from: protected */
    public final int a(int i, int i2, int i3) {
        return abr.a(i, this.f2399b, g(), i3);
    }

    public final aah a(int i, int i2) {
        int b2 = b(0, i2, a());
        return b2 == 0 ? aah.f2393a : new aak(this.f2399b, g(), b2);
    }

    /* access modifiers changed from: protected */
    public final String a(Charset charset) {
        return new String(this.f2399b, g(), a(), charset);
    }

    /* access modifiers changed from: 0000 */
    public final void a(aag aag) throws IOException {
        aag.a(this.f2399b, g(), a());
    }

    /* access modifiers changed from: protected */
    public void a(byte[] bArr, int i, int i2, int i3) {
        System.arraycopy(this.f2399b, 0, bArr, 0, i3);
    }

    /* access modifiers changed from: 0000 */
    public final boolean a(aah aah, int i, int i2) {
        if (i2 > aah.a()) {
            throw new IllegalArgumentException("Length too large: " + i2 + a());
        } else if (i2 > aah.a()) {
            throw new IllegalArgumentException("Ran off end of other: 0, " + i2 + ", " + aah.a());
        } else if (!(aah instanceof aao)) {
            return aah.a(0, i2).equals(a(0, i2));
        } else {
            aao aao = (aao) aah;
            byte[] bArr = this.f2399b;
            byte[] bArr2 = aao.f2399b;
            int g = g() + i2;
            int g2 = g();
            int g3 = aao.g();
            while (g2 < g) {
                if (bArr[g2] != bArr2[g3]) {
                    return false;
                }
                g2++;
                g3++;
            }
            return true;
        }
    }

    public final boolean d() {
        int g = g();
        return aeq.a(this.f2399b, g, a() + g);
    }

    public final aaq e() {
        return aaq.a(this.f2399b, g(), a(), true);
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof aah)) {
            return false;
        }
        if (a() != ((aah) obj).a()) {
            return false;
        }
        if (a() == 0) {
            return true;
        }
        if (!(obj instanceof aao)) {
            return obj.equals(this);
        }
        aao aao = (aao) obj;
        int f = f();
        int f2 = aao.f();
        if (f == 0 || f2 == 0 || f == f2) {
            return a((aao) obj, 0, a());
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public int g() {
        return 0;
    }
}
