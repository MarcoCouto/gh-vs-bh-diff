package com.google.android.gms.internal.ads;

import android.location.Location;
import android.os.Bundle;
import java.util.ArrayList;
import java.util.List;

@cm
public final class aoq {

    /* renamed from: a reason: collision with root package name */
    private long f2811a;

    /* renamed from: b reason: collision with root package name */
    private Bundle f2812b;
    private int c;
    private List<String> d;
    private boolean e;
    private int f;
    private boolean g;
    private String h;
    private arn i;
    private Location j;
    private String k;
    private Bundle l;
    private Bundle m;
    private List<String> n;
    private String o;
    private String p;
    private boolean q;

    public aoq() {
        this.f2811a = -1;
        this.f2812b = new Bundle();
        this.c = -1;
        this.d = new ArrayList();
        this.e = false;
        this.f = -1;
        this.g = false;
        this.h = null;
        this.i = null;
        this.j = null;
        this.k = null;
        this.l = new Bundle();
        this.m = new Bundle();
        this.n = new ArrayList();
        this.o = null;
        this.p = null;
        this.q = false;
    }

    public aoq(aop aop) {
        this.f2811a = aop.f2810b;
        this.f2812b = aop.c;
        this.c = aop.d;
        this.d = aop.e;
        this.e = aop.f;
        this.f = aop.g;
        this.g = aop.h;
        this.h = aop.i;
        this.i = aop.j;
        this.j = aop.k;
        this.k = aop.l;
        this.l = aop.m;
        this.m = aop.n;
        this.n = aop.o;
        this.o = aop.p;
        this.p = aop.q;
    }

    public final aop a() {
        return new aop(7, this.f2811a, this.f2812b, this.c, this.d, this.e, this.f, this.g, this.h, this.i, this.j, this.k, this.l, this.m, this.n, this.o, this.p, false);
    }

    public final aoq a(Location location) {
        this.j = null;
        return this;
    }
}
