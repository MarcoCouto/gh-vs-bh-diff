package com.google.android.gms.internal.ads;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;

public final class amt extends ajk implements ams {
    amt(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.cache.ICacheService");
    }

    public final amm a(amp amp) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (Parcelable) amp);
        Parcel a2 = a(1, r_);
        amm amm = (amm) ajm.a(a2, amm.CREATOR);
        a2.recycle();
        return amm;
    }
}
