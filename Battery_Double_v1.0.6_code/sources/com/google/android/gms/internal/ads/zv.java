package com.google.android.gms.internal.ads;

public final class zv {

    /* renamed from: a reason: collision with root package name */
    private final byte[] f3828a = new byte[256];

    /* renamed from: b reason: collision with root package name */
    private int f3829b;
    private int c;

    public zv(byte[] bArr) {
        for (int i = 0; i < 256; i++) {
            this.f3828a[i] = (byte) i;
        }
        byte b2 = 0;
        for (int i2 = 0; i2 < 256; i2++) {
            b2 = (b2 + this.f3828a[i2] + bArr[i2 % bArr.length]) & 255;
            byte b3 = this.f3828a[i2];
            this.f3828a[i2] = this.f3828a[b2];
            this.f3828a[b2] = b3;
        }
        this.f3829b = 0;
        this.c = 0;
    }

    public final void a(byte[] bArr) {
        int i = this.f3829b;
        int i2 = this.c;
        for (int i3 = 0; i3 < bArr.length; i3++) {
            i = (i + 1) & 255;
            i2 = (i2 + this.f3828a[i]) & 255;
            byte b2 = this.f3828a[i];
            this.f3828a[i] = this.f3828a[i2];
            this.f3828a[i2] = b2;
            bArr[i3] = (byte) (bArr[i3] ^ this.f3828a[(this.f3828a[i] + this.f3828a[i2]) & 255]);
        }
        this.f3829b = i;
        this.c = i2;
    }
}
