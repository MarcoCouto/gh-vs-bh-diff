package com.google.android.gms.internal.ads;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.ViewTreeObserver.OnScrollChangedListener;
import android.view.WindowManager;
import com.google.android.gms.ads.internal.ae;
import com.google.android.gms.ads.internal.ax;
import com.google.android.gms.ads.internal.gmsg.o;
import java.lang.ref.WeakReference;
import org.json.JSONObject;

@cm
public final class bi {

    /* renamed from: a reason: collision with root package name */
    private final Object f3187a = new Object();

    /* renamed from: b reason: collision with root package name */
    private final Context f3188b;
    private final ahh c;
    private final is d;
    private final asv e;
    /* access modifiers changed from: private */
    public final ae f;
    private OnGlobalLayoutListener g;
    private OnScrollChangedListener h;
    private final DisplayMetrics i;
    private lw j;
    private int k = -1;
    private int l = -1;

    public bi(Context context, ahh ahh, is isVar, asv asv, ae aeVar) {
        this.f3188b = context;
        this.c = ahh;
        this.d = isVar;
        this.e = asv;
        this.f = aeVar;
        this.j = new lw(200);
        ax.e();
        this.i = jv.a((WindowManager) context.getSystemService("window"));
    }

    /* access modifiers changed from: private */
    public final void a(WeakReference<qn> weakReference, boolean z) {
        if (weakReference != null) {
            qn qnVar = (qn) weakReference.get();
            if (qnVar != null && qnVar.getView() != null) {
                if (!z || this.j.a()) {
                    int[] iArr = new int[2];
                    qnVar.getView().getLocationOnScreen(iArr);
                    ape.a();
                    int b2 = mh.b(this.i, iArr[0]);
                    ape.a();
                    int b3 = mh.b(this.i, iArr[1]);
                    synchronized (this.f3187a) {
                        if (!(this.k == b2 && this.l == b3)) {
                            this.k = b2;
                            this.l = b3;
                            qnVar.v().a(this.k, this.l, !z);
                        }
                    }
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ void a(ny nyVar, qn qnVar, boolean z) {
        this.f.R();
        nyVar.b(qnVar);
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ void a(JSONObject jSONObject, ny nyVar) {
        try {
            ax.f();
            qn a2 = qu.a(this.f3188b, sb.a(), "native-video", false, false, this.c, this.d.f3397a.k, this.e, null, this.f.i(), this.d.i);
            a2.a(sb.b());
            this.f.a(a2);
            WeakReference weakReference = new WeakReference(a2);
            rv v = a2.v();
            if (this.g == null) {
                this.g = new bo(this, weakReference);
            }
            OnGlobalLayoutListener onGlobalLayoutListener = this.g;
            if (this.h == null) {
                this.h = new bp(this, weakReference);
            }
            v.a(onGlobalLayoutListener, this.h);
            a2.a("/video", o.l);
            a2.a("/videoMeta", o.m);
            a2.a("/precache", (com.google.android.gms.ads.internal.gmsg.ae<? super qn>) new qc<Object>());
            a2.a("/delayPageLoaded", o.p);
            a2.a("/instrument", o.n);
            a2.a("/log", o.g);
            a2.a("/videoClicked", o.h);
            a2.a("/trackActiveViewUnit", (com.google.android.gms.ads.internal.gmsg.ae<? super qn>) new bm<Object>(this));
            a2.a("/untrackActiveViewUnit", (com.google.android.gms.ads.internal.gmsg.ae<? super qn>) new bn<Object>(this));
            a2.v().a((rx) new bk(a2, jSONObject));
            a2.v().a((rw) new bl(this, nyVar, a2));
            a2.loadUrl((String) ape.f().a(asi.bY));
        } catch (Exception e2) {
            jm.c("Exception occurred while getting video view", e2);
            nyVar.b(null);
        }
    }
}
