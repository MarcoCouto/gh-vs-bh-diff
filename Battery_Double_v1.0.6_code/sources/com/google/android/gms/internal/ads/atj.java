package com.google.android.gms.internal.ads;

import android.os.RemoteException;
import android.view.View;
import android.view.View.OnClickListener;
import com.google.android.gms.ads.internal.ax;
import com.google.android.gms.ads.internal.gmsg.ae;
import java.lang.ref.WeakReference;
import org.json.JSONException;
import org.json.JSONObject;

@cm
public final class atj implements OnClickListener {

    /* renamed from: a reason: collision with root package name */
    String f2899a;

    /* renamed from: b reason: collision with root package name */
    Long f2900b;
    WeakReference<View> c;
    private final bq d;
    /* access modifiers changed from: private */
    public awq e;
    private ae f;

    public atj(bq bqVar) {
        this.d = bqVar;
    }

    private final void c() {
        this.f2899a = null;
        this.f2900b = null;
        if (this.c != null) {
            View view = (View) this.c.get();
            this.c = null;
            if (view != null) {
                view.setClickable(false);
                view.setOnClickListener(null);
            }
        }
    }

    public final awq a() {
        return this.e;
    }

    public final void a(awq awq) {
        this.e = awq;
        if (this.f != null) {
            this.d.b("/unconfirmedClick", this.f);
        }
        this.f = new atk(this);
        this.d.a("/unconfirmedClick", this.f);
    }

    public final void b() {
        if (this.e != null && this.f2900b != null) {
            c();
            try {
                this.e.a();
            } catch (RemoteException e2) {
                ms.d("#007 Could not call remote method.", e2);
            }
        }
    }

    public final void onClick(View view) {
        if (this.c != null && this.c.get() == view) {
            if (!(this.f2899a == null || this.f2900b == null)) {
                JSONObject jSONObject = new JSONObject();
                try {
                    jSONObject.put("id", this.f2899a);
                    jSONObject.put("time_interval", ax.l().a() - this.f2900b.longValue());
                    jSONObject.put("messageType", "onePointFiveClick");
                    this.d.a("sendMessageToNativeJs", jSONObject);
                } catch (JSONException e2) {
                    jm.b("Unable to dispatch sendMessageToNativeJs event", e2);
                }
            }
            c();
        }
    }
}
