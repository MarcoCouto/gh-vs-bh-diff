package com.google.android.gms.internal.ads;

import android.text.TextUtils;
import com.google.android.gms.ads.internal.gmsg.ae;
import java.util.Map;

final class sk implements ae<qn> {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ sj f3686a;

    sk(sj sjVar) {
        this.f3686a = sjVar;
    }

    public final /* synthetic */ void zza(Object obj, Map map) {
        if (map != null) {
            String str = (String) map.get("height");
            if (!TextUtils.isEmpty(str)) {
                try {
                    int parseInt = Integer.parseInt(str);
                    synchronized (this.f3686a) {
                        if (this.f3686a.v != parseInt) {
                            this.f3686a.v = parseInt;
                            this.f3686a.requestLayout();
                        }
                    }
                } catch (Exception e) {
                    jm.c("Exception occurred while getting webview content height", e);
                }
            }
        }
    }
}
