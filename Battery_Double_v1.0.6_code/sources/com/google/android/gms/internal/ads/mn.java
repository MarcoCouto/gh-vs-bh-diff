package com.google.android.gms.internal.ads;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

final class mn {

    /* renamed from: a reason: collision with root package name */
    long f3521a;

    /* renamed from: b reason: collision with root package name */
    final String f3522b;
    final String c;
    final long d;
    final long e;
    final long f;
    final long g;
    final List<aqd> h;

    mn(String str, agy agy) {
        List arrayList;
        String str2 = agy.f2577b;
        long j = agy.c;
        long j2 = agy.d;
        long j3 = agy.e;
        long j4 = agy.f;
        if (agy.h != null) {
            arrayList = agy.h;
        } else {
            Map<String, String> map = agy.g;
            arrayList = new ArrayList(map.size());
            for (Entry entry : map.entrySet()) {
                arrayList.add(new aqd((String) entry.getKey(), (String) entry.getValue()));
            }
        }
        this(str, str2, j, j2, j3, j4, arrayList);
        this.f3521a = (long) agy.f2576a.length;
    }

    private mn(String str, String str2, long j, long j2, long j3, long j4, List<aqd> list) {
        this.f3522b = str;
        if ("".equals(str2)) {
            str2 = null;
        }
        this.c = str2;
        this.d = j;
        this.e = j2;
        this.f = j3;
        this.g = j4;
        this.h = list;
    }

    static mn a(no noVar) throws IOException {
        if (lm.a((InputStream) noVar) == 538247942) {
            return new mn(lm.a(noVar), lm.a(noVar), lm.b((InputStream) noVar), lm.b((InputStream) noVar), lm.b((InputStream) noVar), lm.b((InputStream) noVar), lm.b(noVar));
        }
        throw new IOException();
    }

    /* access modifiers changed from: 0000 */
    public final boolean a(OutputStream outputStream) {
        try {
            lm.a(outputStream, 538247942);
            lm.a(outputStream, this.f3522b);
            lm.a(outputStream, this.c == null ? "" : this.c);
            lm.a(outputStream, this.d);
            lm.a(outputStream, this.e);
            lm.a(outputStream, this.f);
            lm.a(outputStream, this.g);
            List<aqd> list = this.h;
            if (list != null) {
                lm.a(outputStream, list.size());
                for (aqd aqd : list) {
                    lm.a(outputStream, aqd.a());
                    lm.a(outputStream, aqd.b());
                }
            } else {
                lm.a(outputStream, 0);
            }
            outputStream.flush();
            return true;
        } catch (IOException e2) {
            eg.b("%s", e2.toString());
            return false;
        }
    }
}
