package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.Binder;
import android.os.ParcelFileDescriptor;
import com.google.android.gms.ads.internal.ax;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

@cm
public final class axl implements are {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public axe f2980a;
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public boolean f2981b;
    private final Context c;
    /* access modifiers changed from: private */
    public final Object d = new Object();

    public axl(Context context) {
        this.c = context;
    }

    private final Future<ParcelFileDescriptor> a(axf axf) {
        axm axm = new axm(this);
        axn axn = new axn(this, axm, axf);
        axq axq = new axq(this, axm);
        synchronized (this.d) {
            this.f2980a = new axe(this.c, ax.t().a(), axn, axq);
            this.f2980a.o();
        }
        return axm;
    }

    /* access modifiers changed from: private */
    public final void a() {
        synchronized (this.d) {
            if (this.f2980a != null) {
                this.f2980a.a();
                this.f2980a = null;
                Binder.flushPendingCommands();
            }
        }
    }

    /* JADX INFO: finally extract failed */
    public final atz a(awb<?> awb) throws df {
        atz atz;
        axf a2 = axf.a(awb);
        long intValue = (long) ((Integer) ape.f().a(asi.cK)).intValue();
        long b2 = ax.l().b();
        try {
            axh axh = (axh) new eb((ParcelFileDescriptor) a(a2).get(intValue, TimeUnit.MILLISECONDS)).a(axh.CREATOR);
            if (axh.f2978a) {
                throw new df(axh.f2979b);
            }
            if (axh.e.length != axh.f.length) {
                atz = null;
            } else {
                HashMap hashMap = new HashMap();
                for (int i = 0; i < axh.e.length; i++) {
                    hashMap.put(axh.e[i], axh.f[i]);
                }
                atz = new atz(axh.c, axh.d, (Map<String, String>) hashMap, axh.g, axh.h);
            }
            jm.a("Http assets remote cache took " + (ax.l().b() - b2) + "ms");
            return atz;
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            jm.a("Http assets remote cache took " + (ax.l().b() - b2) + "ms");
            return null;
        } catch (Throwable th) {
            jm.a("Http assets remote cache took " + (ax.l().b() - b2) + "ms");
            throw th;
        }
    }
}
