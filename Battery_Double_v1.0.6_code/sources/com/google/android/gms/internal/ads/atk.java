package com.google.android.gms.internal.ads;

import android.os.RemoteException;
import com.google.android.gms.ads.internal.gmsg.ae;
import java.util.Map;

final class atk implements ae<Object> {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ atj f2901a;

    atk(atj atj) {
        this.f2901a = atj;
    }

    public final void zza(Object obj, Map<String, String> map) {
        try {
            this.f2901a.f2900b = Long.valueOf(Long.parseLong((String) map.get("timestamp")));
        } catch (NumberFormatException e) {
            jm.c("Failed to call parse unconfirmedClickTimestamp.");
        }
        this.f2901a.f2899a = (String) map.get("id");
        String str = (String) map.get("asset_id");
        if (this.f2901a.e == null) {
            jm.b("Received unconfirmed click but UnconfirmedClickListener is null.");
            return;
        }
        try {
            this.f2901a.e.a(str);
        } catch (RemoteException e2) {
            ms.d("#007 Could not call remote method.", e2);
        }
    }
}
