package com.google.android.gms.internal.ads;

import android.webkit.ValueCallback;

final class alq implements ValueCallback<String> {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ alp f2707a;

    alq(alp alp) {
        this.f2707a = alp;
    }

    public final /* synthetic */ void onReceiveValue(Object obj) {
        this.f2707a.d.a(this.f2707a.f2705a, this.f2707a.f2706b, (String) obj, this.f2707a.c);
    }
}
