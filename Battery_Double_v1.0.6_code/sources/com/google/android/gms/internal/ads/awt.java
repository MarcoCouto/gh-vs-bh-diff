package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.b.a;
import java.util.List;

public abstract class awt extends ajl implements aws {
    public awt() {
        super("com.google.android.gms.ads.internal.formats.client.IUnifiedNativeAd");
    }

    /* access modifiers changed from: protected */
    public final boolean dispatchTransaction(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        awq awr;
        switch (i) {
            case 2:
                String a2 = a();
                parcel2.writeNoException();
                parcel2.writeString(a2);
                break;
            case 3:
                List b2 = b();
                parcel2.writeNoException();
                parcel2.writeList(b2);
                break;
            case 4:
                String c = c();
                parcel2.writeNoException();
                parcel2.writeString(c);
                break;
            case 5:
                auw d = d();
                parcel2.writeNoException();
                ajm.a(parcel2, (IInterface) d);
                break;
            case 6:
                String e = e();
                parcel2.writeNoException();
                parcel2.writeString(e);
                break;
            case 7:
                String f = f();
                parcel2.writeNoException();
                parcel2.writeString(f);
                break;
            case 8:
                double g = g();
                parcel2.writeNoException();
                parcel2.writeDouble(g);
                break;
            case 9:
                String h = h();
                parcel2.writeNoException();
                parcel2.writeString(h);
                break;
            case 10:
                String i3 = i();
                parcel2.writeNoException();
                parcel2.writeString(i3);
                break;
            case 11:
                aqs j = j();
                parcel2.writeNoException();
                ajm.a(parcel2, (IInterface) j);
                break;
            case 12:
                String q = q();
                parcel2.writeNoException();
                parcel2.writeString(q);
                break;
            case 13:
                t();
                parcel2.writeNoException();
                break;
            case 14:
                aus s = s();
                parcel2.writeNoException();
                ajm.a(parcel2, (IInterface) s);
                break;
            case 15:
                a((Bundle) ajm.a(parcel, Bundle.CREATOR));
                parcel2.writeNoException();
                break;
            case 16:
                boolean b3 = b((Bundle) ajm.a(parcel, Bundle.CREATOR));
                parcel2.writeNoException();
                ajm.a(parcel2, b3);
                break;
            case 17:
                c((Bundle) ajm.a(parcel, Bundle.CREATOR));
                parcel2.writeNoException();
                break;
            case 18:
                a n = n();
                parcel2.writeNoException();
                ajm.a(parcel2, (IInterface) n);
                break;
            case 19:
                a p = p();
                parcel2.writeNoException();
                ajm.a(parcel2, (IInterface) p);
                break;
            case 20:
                Bundle r = r();
                parcel2.writeNoException();
                ajm.b(parcel2, r);
                break;
            case 21:
                IBinder readStrongBinder = parcel.readStrongBinder();
                if (readStrongBinder == null) {
                    awr = null;
                } else {
                    IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.ads.internal.formats.client.IUnconfirmedClickListener");
                    awr = queryLocalInterface instanceof awq ? (awq) queryLocalInterface : new awr(readStrongBinder);
                }
                a(awr);
                parcel2.writeNoException();
                break;
            case 22:
                u();
                parcel2.writeNoException();
                break;
            default:
                return false;
        }
        return true;
    }
}
