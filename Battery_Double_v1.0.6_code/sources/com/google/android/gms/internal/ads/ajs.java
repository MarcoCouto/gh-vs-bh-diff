package com.google.android.gms.internal.ads;

import org.json.JSONObject;

@cm
public final class ajs {

    /* renamed from: a reason: collision with root package name */
    private final String f2641a;

    /* renamed from: b reason: collision with root package name */
    private final JSONObject f2642b;
    private final String c;
    private final String d;
    private final boolean e;
    private final boolean f;

    public ajs(String str, mu muVar, String str2, JSONObject jSONObject, boolean z, boolean z2) {
        this.d = muVar.f3528a;
        this.f2642b = jSONObject;
        this.c = str;
        this.f2641a = str2;
        this.e = z;
        this.f = z2;
    }

    public final String a() {
        return this.f2641a;
    }

    public final String b() {
        return this.d;
    }

    public final JSONObject c() {
        return this.f2642b;
    }

    public final String d() {
        return this.c;
    }

    public final boolean e() {
        return this.e;
    }

    public final boolean f() {
        return this.f;
    }
}
