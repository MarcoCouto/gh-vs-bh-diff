package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.gmsg.ae;
import java.util.Map;

final class fc implements ae<Object> {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ fa f3306a;

    fc(fa faVar) {
        this.f3306a = faVar;
    }

    public final void zza(Object obj, Map<String, String> map) {
        synchronized (this.f3306a.f3304b) {
            if (!this.f3306a.e.isDone()) {
                fg fgVar = new fg(-2, map);
                if (this.f3306a.c.equals(fgVar.h())) {
                    String e = fgVar.e();
                    if (e == null) {
                        jm.e("URL missing in loadAdUrl GMSG.");
                        return;
                    }
                    if (e.contains("%40mediation_adapters%40")) {
                        String replaceAll = e.replaceAll("%40mediation_adapters%40", jg.a(this.f3306a.f3303a, (String) map.get("check_adapters"), this.f3306a.d));
                        fgVar.a(replaceAll);
                        String str = "Ad request URL modified to ";
                        String valueOf = String.valueOf(replaceAll);
                        jm.a(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
                    }
                    this.f3306a.e.b(fgVar);
                }
            }
        }
    }
}
