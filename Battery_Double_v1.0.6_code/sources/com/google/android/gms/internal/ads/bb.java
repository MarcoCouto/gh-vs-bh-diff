package com.google.android.gms.internal.ads;

final class bb implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ ny f3094a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ String f3095b;
    private final /* synthetic */ ay c;

    bb(ay ayVar, ny nyVar, String str) {
        this.c = ayVar;
        this.f3094a = nyVar;
        this.f3095b = str;
    }

    public final void run() {
        this.f3094a.b((awh) this.c.d.O().get(this.f3095b));
    }
}
