package com.google.android.gms.internal.ads;

import android.graphics.Bitmap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

@cm
public final class lu {

    /* renamed from: a reason: collision with root package name */
    private Map<Integer, Bitmap> f3493a = new ConcurrentHashMap();

    /* renamed from: b reason: collision with root package name */
    private AtomicInteger f3494b = new AtomicInteger(0);

    public final int a(Bitmap bitmap) {
        if (bitmap == null) {
            jm.b("Bitmap is null. Skipping putting into the Memory Map.");
            return -1;
        }
        int andIncrement = this.f3494b.getAndIncrement();
        this.f3493a.put(Integer.valueOf(andIncrement), bitmap);
        return andIncrement;
    }

    public final Bitmap a(Integer num) {
        return (Bitmap) this.f3493a.get(num);
    }

    public final void b(Integer num) {
        this.f3493a.remove(num);
    }
}
