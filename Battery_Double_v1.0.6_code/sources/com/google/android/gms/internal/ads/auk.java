package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.gmsg.ae;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

final class auk implements ae<Object> {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ bq f2935a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ auf f2936b;

    auk(auf auf, bq bqVar) {
        this.f2936b = auf;
        this.f2935a = bqVar;
    }

    public final void zza(Object obj, Map<String, String> map) {
        JSONObject jSONObject = new JSONObject();
        try {
            for (String str : map.keySet()) {
                jSONObject.put(str, map.get(str));
            }
            jSONObject.put("id", this.f2936b.f2926b);
            this.f2935a.a("sendMessageToNativeJs", jSONObject);
        } catch (JSONException e) {
            jm.b("Unable to dispatch sendMessageToNativeJs event", e);
        }
    }
}
