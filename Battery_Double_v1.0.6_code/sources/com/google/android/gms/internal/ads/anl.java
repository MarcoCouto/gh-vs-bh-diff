package com.google.android.gms.internal.ads;

import java.io.IOException;

public final class anl extends afh<anl> {

    /* renamed from: a reason: collision with root package name */
    public String f2760a;

    /* renamed from: b reason: collision with root package name */
    public any f2761b;
    private anx c;
    private Integer d;
    private Integer e;
    private Integer f;
    private Integer g;
    private Integer h;

    public anl() {
        this.f2760a = null;
        this.c = null;
        this.d = null;
        this.f2761b = null;
        this.e = null;
        this.f = null;
        this.g = null;
        this.h = null;
        this.Y = null;
        this.Z = -1;
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public final anl a(afd afd) throws IOException {
        while (true) {
            int a2 = afd.a();
            switch (a2) {
                case 0:
                    break;
                case 10:
                    this.f2760a = afd.e();
                    continue;
                case 18:
                    if (this.c == null) {
                        this.c = new anx();
                    }
                    afd.a((afn) this.c);
                    continue;
                case 24:
                    this.d = Integer.valueOf(afd.g());
                    continue;
                case 34:
                    if (this.f2761b == null) {
                        this.f2761b = new any();
                    }
                    afd.a((afn) this.f2761b);
                    continue;
                case 40:
                    this.e = Integer.valueOf(afd.g());
                    continue;
                case 48:
                    int j = afd.j();
                    try {
                        this.f = Integer.valueOf(anf.a(afd.g()));
                        continue;
                    } catch (IllegalArgumentException e2) {
                        afd.e(j);
                        a(afd, a2);
                        break;
                    }
                case 56:
                    int j2 = afd.j();
                    try {
                        this.g = Integer.valueOf(anf.a(afd.g()));
                        continue;
                    } catch (IllegalArgumentException e3) {
                        afd.e(j2);
                        a(afd, a2);
                        break;
                    }
                case 64:
                    int j3 = afd.j();
                    try {
                        this.h = Integer.valueOf(anf.a(afd.g()));
                        continue;
                    } catch (IllegalArgumentException e4) {
                        afd.e(j3);
                        a(afd, a2);
                        break;
                    }
                default:
                    if (!super.a(afd, a2)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    /* access modifiers changed from: protected */
    public final int a() {
        int a2 = super.a();
        if (this.f2760a != null) {
            a2 += aff.b(1, this.f2760a);
        }
        if (this.c != null) {
            a2 += aff.b(2, (afn) this.c);
        }
        if (this.d != null) {
            a2 += aff.b(3, this.d.intValue());
        }
        if (this.f2761b != null) {
            a2 += aff.b(4, (afn) this.f2761b);
        }
        if (this.e != null) {
            a2 += aff.b(5, this.e.intValue());
        }
        if (this.f != null) {
            a2 += aff.b(6, this.f.intValue());
        }
        if (this.g != null) {
            a2 += aff.b(7, this.g.intValue());
        }
        return this.h != null ? a2 + aff.b(8, this.h.intValue()) : a2;
    }

    public final void a(aff aff) throws IOException {
        if (this.f2760a != null) {
            aff.a(1, this.f2760a);
        }
        if (this.c != null) {
            aff.a(2, (afn) this.c);
        }
        if (this.d != null) {
            aff.a(3, this.d.intValue());
        }
        if (this.f2761b != null) {
            aff.a(4, (afn) this.f2761b);
        }
        if (this.e != null) {
            aff.a(5, this.e.intValue());
        }
        if (this.f != null) {
            aff.a(6, this.f.intValue());
        }
        if (this.g != null) {
            aff.a(7, this.g.intValue());
        }
        if (this.h != null) {
            aff.a(8, this.h.intValue());
        }
        super.a(aff);
    }
}
