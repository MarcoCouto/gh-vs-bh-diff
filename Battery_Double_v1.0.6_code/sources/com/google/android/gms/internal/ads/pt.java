package com.google.android.gms.internal.ads;

import android.content.Context;
import android.graphics.Color;
import android.os.Build.VERSION;
import android.os.SystemClock;
import android.text.TextUtils;
import android.view.MotionEvent;
import com.google.android.gms.ads.internal.gmsg.ae;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;

@cm
public final class pt implements ae<pm> {

    /* renamed from: a reason: collision with root package name */
    private boolean f3614a;

    private static int a(Context context, Map<String, String> map, String str, int i) {
        String str2 = (String) map.get(str);
        if (str2 == null) {
            return i;
        }
        try {
            ape.a();
            return mh.a(context, Integer.parseInt(str2));
        } catch (NumberFormatException e) {
            jm.e(new StringBuilder(String.valueOf(str).length() + 34 + String.valueOf(str2).length()).append("Could not parse ").append(str).append(" in a video GMSG: ").append(str2).toString());
            return i;
        }
    }

    private static void a(oy oyVar, Map<String, String> map) {
        String str = (String) map.get("minBufferMs");
        String str2 = (String) map.get("maxBufferMs");
        String str3 = (String) map.get("bufferForPlaybackMs");
        String str4 = (String) map.get("bufferForPlaybackAfterRebufferMs");
        if (str != null) {
            try {
                Integer.parseInt(str);
            } catch (NumberFormatException e) {
                jm.e(String.format("Could not parse buffer parameters in loadControl video GMSG: (%s, %s)", new Object[]{str, str2}));
                return;
            }
        }
        if (str2 != null) {
            Integer.parseInt(str2);
        }
        if (str3 != null) {
            Integer.parseInt(str3);
        }
        if (str4 != null) {
            Integer.parseInt(str4);
        }
    }

    public final /* synthetic */ void zza(Object obj, Map map) {
        int i;
        int i2;
        String[] split;
        pm pmVar = (pm) obj;
        String str = (String) map.get("action");
        if (str == null) {
            jm.e("Action missing from video GMSG.");
            return;
        }
        if (jm.a(3)) {
            JSONObject jSONObject = new JSONObject(map);
            jSONObject.remove("google.afma.Notify_dt");
            String jSONObject2 = jSONObject.toString();
            jm.b(new StringBuilder(String.valueOf(str).length() + 13 + String.valueOf(jSONObject2).length()).append("Video GMSG: ").append(str).append(" ").append(jSONObject2).toString());
        }
        if ("background".equals(str)) {
            String str2 = (String) map.get("color");
            if (TextUtils.isEmpty(str2)) {
                jm.e("Color parameter missing from color video GMSG.");
                return;
            }
            try {
                pmVar.setBackgroundColor(Color.parseColor(str2));
            } catch (IllegalArgumentException e) {
                jm.e("Invalid color parameter in video GMSG.");
            }
        } else if ("decoderProps".equals(str)) {
            String str3 = (String) map.get("mimeTypes");
            if (str3 == null) {
                jm.e("No MIME types specified for decoder properties inspection.");
                oy.a(pmVar, "missingMimeTypes");
            } else if (VERSION.SDK_INT < 16) {
                jm.e("Video decoder properties available on API versions >= 16.");
                oy.a(pmVar, "deficientApiVersion");
            } else {
                HashMap hashMap = new HashMap();
                for (String str4 : str3.split(",")) {
                    hashMap.put(str4, mf.a(str4.trim()));
                }
                oy.a(pmVar, (Map<String, List<Map<String, Object>>>) hashMap);
            }
        } else {
            pd a2 = pmVar.a();
            if (a2 == null) {
                jm.e("Could not get underlay container for a video GMSG.");
                return;
            }
            boolean equals = "new".equals(str);
            boolean equals2 = "position".equals(str);
            if (equals || equals2) {
                Context context = pmVar.getContext();
                int a3 = a(context, map, "x", 0);
                int a4 = a(context, map, "y", 0);
                int a5 = a(context, map, "w", -1);
                int a6 = a(context, map, "h", -1);
                if (((Boolean) ape.f().a(asi.cf)).booleanValue()) {
                    i = Math.min(a5, pmVar.m() - a3);
                    a6 = Math.min(a6, pmVar.l() - a4);
                } else {
                    i = a5;
                }
                try {
                    i2 = Integer.parseInt((String) map.get("player"));
                } catch (NumberFormatException e2) {
                    i2 = 0;
                }
                boolean parseBoolean = Boolean.parseBoolean((String) map.get("spherical"));
                if (!equals || a2.a() != null) {
                    a2.a(a3, a4, i, a6);
                    return;
                }
                a2.a(a3, a4, i, a6, i2, parseBoolean, new pl((String) map.get("flags")));
                oy a7 = a2.a();
                if (a7 != null) {
                    a(a7, map);
                    return;
                }
                return;
            }
            oy a8 = a2.a();
            if (a8 == null) {
                oy.a(pmVar);
            } else if ("click".equals(str)) {
                Context context2 = pmVar.getContext();
                int a9 = a(context2, map, "x", 0);
                int a10 = a(context2, map, "y", 0);
                long uptimeMillis = SystemClock.uptimeMillis();
                MotionEvent obtain = MotionEvent.obtain(uptimeMillis, uptimeMillis, 0, (float) a9, (float) a10, 0);
                a8.a(obtain);
                obtain.recycle();
            } else if ("currentTime".equals(str)) {
                String str5 = (String) map.get("time");
                if (str5 == null) {
                    jm.e("Time parameter missing from currentTime video GMSG.");
                    return;
                }
                try {
                    a8.a((int) (Float.parseFloat(str5) * 1000.0f));
                } catch (NumberFormatException e3) {
                    String str6 = "Could not parse time parameter from currentTime video GMSG: ";
                    String valueOf = String.valueOf(str5);
                    jm.e(valueOf.length() != 0 ? str6.concat(valueOf) : new String(str6));
                }
            } else if ("hide".equals(str)) {
                a8.setVisibility(4);
            } else if ("load".equals(str)) {
                a8.h();
            } else if ("loadControl".equals(str)) {
                a(a8, map);
            } else if ("muted".equals(str)) {
                if (Boolean.parseBoolean((String) map.get("muted"))) {
                    a8.k();
                } else {
                    a8.l();
                }
            } else if ("pause".equals(str)) {
                a8.i();
            } else if ("play".equals(str)) {
                a8.j();
            } else if ("show".equals(str)) {
                a8.setVisibility(0);
            } else if ("src".equals(str)) {
                a8.a((String) map.get("src"));
            } else if ("touchMove".equals(str)) {
                Context context3 = pmVar.getContext();
                a8.a((float) a(context3, map, "dx", 0), (float) a(context3, map, "dy", 0));
                if (!this.f3614a) {
                    pmVar.f();
                    this.f3614a = true;
                }
            } else if ("volume".equals(str)) {
                String str7 = (String) map.get("volume");
                if (str7 == null) {
                    jm.e("Level parameter missing from volume video GMSG.");
                    return;
                }
                try {
                    a8.setVolume(Float.parseFloat(str7));
                } catch (NumberFormatException e4) {
                    String str8 = "Could not parse volume parameter from volume video GMSG: ";
                    String valueOf2 = String.valueOf(str7);
                    jm.e(valueOf2.length() != 0 ? str8.concat(valueOf2) : new String(str8));
                }
            } else if ("watermark".equals(str)) {
                a8.m();
            } else {
                String str9 = "Unknown video action: ";
                String valueOf3 = String.valueOf(str);
                jm.e(valueOf3.length() != 0 ? str9.concat(valueOf3) : new String(str9));
            }
        }
    }
}
