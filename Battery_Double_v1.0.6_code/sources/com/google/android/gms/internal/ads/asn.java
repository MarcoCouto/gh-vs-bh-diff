package com.google.android.gms.internal.ads;

import android.text.TextUtils;

@cm
public final class asn {
    public static void a(asl asl, ask ask) {
        if (ask.b() == null) {
            throw new IllegalArgumentException("Context can't be null. Please set up context in CsiConfiguration.");
        } else if (TextUtils.isEmpty(ask.c())) {
            throw new IllegalArgumentException("AfmaVersion can't be null or empty. Please set up afmaVersion in CsiConfiguration.");
        } else {
            asl.a(ask.b(), ask.c(), ask.a(), ask.d());
        }
    }
}
