package com.google.android.gms.internal.ads;

import java.util.concurrent.Future;

final /* synthetic */ class axp implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final ny f2987a;

    /* renamed from: b reason: collision with root package name */
    private final Future f2988b;

    axp(ny nyVar, Future future) {
        this.f2987a = nyVar;
        this.f2988b = future;
    }

    public final void run() {
        ny nyVar = this.f2987a;
        Future future = this.f2988b;
        if (nyVar.isCancelled()) {
            future.cancel(true);
        }
    }
}
