package com.google.android.gms.internal.ads;

import android.os.Environment;
import com.google.android.gms.ads.internal.ax;
import com.google.android.gms.internal.ads.amy.a.b;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@cm
public final class amw {

    /* renamed from: a reason: collision with root package name */
    private final anb f2739a;

    /* renamed from: b reason: collision with root package name */
    private final ann f2740b;
    private final boolean c;

    private amw() {
        this.c = false;
        this.f2739a = new anb();
        this.f2740b = new ann();
        b();
    }

    public amw(anb anb) {
        this.f2739a = anb;
        this.c = ((Boolean) ape.f().a(asi.db)).booleanValue();
        this.f2740b = new ann();
        b();
    }

    public static amw a() {
        return new amw();
    }

    private final synchronized void b() {
        this.f2740b.d = new ang();
        this.f2740b.d.f2751b = new anj();
        this.f2740b.c = new anl();
    }

    private final synchronized void b(b bVar) {
        this.f2740b.f2765b = c();
        this.f2739a.a(afn.a((afn) this.f2740b)).b(bVar.a()).a();
        String str = "Logging Event with event code : ";
        String valueOf = String.valueOf(Integer.toString(bVar.a(), 10));
        jm.a(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
    }

    private final synchronized void c(b bVar) {
        File externalStorageDirectory = Environment.getExternalStorageDirectory();
        if (externalStorageDirectory != null) {
            try {
                FileOutputStream fileOutputStream = new FileOutputStream(new File(externalStorageDirectory, "clearcut_events.txt"), true);
                try {
                    fileOutputStream.write(d(bVar).getBytes());
                    fileOutputStream.write(10);
                    try {
                    } catch (IOException e) {
                        jm.a("Could not close Clearcut output stream.");
                    }
                } catch (IOException e2) {
                    jm.a("Could not write Clearcut to file.");
                    try {
                    } catch (IOException e3) {
                        jm.a("Could not close Clearcut output stream.");
                    }
                } finally {
                    try {
                        fileOutputStream.close();
                    } catch (IOException e4) {
                        jm.a("Could not close Clearcut output stream.");
                    }
                }
            } catch (FileNotFoundException e5) {
                jm.a("Could not find file for Clearcut");
            }
        }
        return;
    }

    private static long[] c() {
        int i = 0;
        List<String> b2 = asi.b();
        ArrayList arrayList = new ArrayList();
        for (String split : b2) {
            for (String valueOf : split.split(",")) {
                try {
                    arrayList.add(Long.valueOf(valueOf));
                } catch (NumberFormatException e) {
                    jm.a("Experiment ID is not a number");
                }
            }
        }
        long[] jArr = new long[arrayList.size()];
        ArrayList arrayList2 = arrayList;
        int size = arrayList2.size();
        int i2 = 0;
        while (i < size) {
            Object obj = arrayList2.get(i);
            i++;
            jArr[i2] = ((Long) obj).longValue();
            i2++;
        }
        return jArr;
    }

    private final synchronized String d(b bVar) {
        return String.format("id=%s,timestamp=%s,event=%s", new Object[]{this.f2740b.f2764a, Long.valueOf(ax.l().b()), Integer.valueOf(bVar.a())});
    }

    public final synchronized void a(amx amx) {
        if (this.c) {
            try {
                amx.a(this.f2740b);
            } catch (NullPointerException e) {
                ax.i().a((Throwable) e, "AdMobClearcutLogger.modify");
            }
        }
        return;
    }

    public final synchronized void a(b bVar) {
        if (this.c) {
            if (((Boolean) ape.f().a(asi.dc)).booleanValue()) {
                c(bVar);
            } else {
                b(bVar);
            }
        }
    }
}
