package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.os.RemoteException;
import com.google.android.gms.ads.reward.mediation.MediationRewardedVideoAdAdapter;
import com.google.android.gms.ads.reward.mediation.a;
import com.google.android.gms.b.b;
import com.google.android.gms.common.internal.aa;

@cm
public final class ho implements a {

    /* renamed from: a reason: collision with root package name */
    private final hl f3368a;

    public ho(hl hlVar) {
        this.f3368a = hlVar;
    }

    public final void a(Bundle bundle) {
        aa.b("#008 Must be called on the main UI thread.");
        ms.b("Adapter called onAdMetadataChanged.");
        try {
            this.f3368a.a(bundle);
        } catch (RemoteException e) {
            ms.d("#007 Could not call remote method.", e);
        }
    }

    public final void a(MediationRewardedVideoAdAdapter mediationRewardedVideoAdAdapter) {
        aa.b("#008 Must be called on the main UI thread.");
        ms.b("Adapter called onInitializationSucceeded.");
        try {
            this.f3368a.a(b.a(mediationRewardedVideoAdAdapter));
        } catch (RemoteException e) {
            ms.d("#007 Could not call remote method.", e);
        }
    }

    public final void a(MediationRewardedVideoAdAdapter mediationRewardedVideoAdAdapter, int i) {
        aa.b("#008 Must be called on the main UI thread.");
        ms.b("Adapter called onAdFailedToLoad.");
        try {
            this.f3368a.b(b.a(mediationRewardedVideoAdAdapter), i);
        } catch (RemoteException e) {
            ms.d("#007 Could not call remote method.", e);
        }
    }

    public final void a(MediationRewardedVideoAdAdapter mediationRewardedVideoAdAdapter, com.google.android.gms.ads.reward.a aVar) {
        aa.b("#008 Must be called on the main UI thread.");
        ms.b("Adapter called onRewarded.");
        if (aVar != null) {
            try {
                this.f3368a.a(b.a(mediationRewardedVideoAdAdapter), new hp(aVar));
            } catch (RemoteException e) {
                ms.d("#007 Could not call remote method.", e);
            }
        } else {
            this.f3368a.a(b.a(mediationRewardedVideoAdAdapter), new hp("", 1));
        }
    }

    public final void b(MediationRewardedVideoAdAdapter mediationRewardedVideoAdAdapter) {
        aa.b("#008 Must be called on the main UI thread.");
        ms.b("Adapter called onAdLoaded.");
        try {
            this.f3368a.b(b.a(mediationRewardedVideoAdAdapter));
        } catch (RemoteException e) {
            ms.d("#007 Could not call remote method.", e);
        }
    }

    public final void c(MediationRewardedVideoAdAdapter mediationRewardedVideoAdAdapter) {
        aa.b("#008 Must be called on the main UI thread.");
        ms.b("Adapter called onAdOpened.");
        try {
            this.f3368a.c(b.a(mediationRewardedVideoAdAdapter));
        } catch (RemoteException e) {
            ms.d("#007 Could not call remote method.", e);
        }
    }

    public final void d(MediationRewardedVideoAdAdapter mediationRewardedVideoAdAdapter) {
        aa.b("#008 Must be called on the main UI thread.");
        ms.b("Adapter called onVideoStarted.");
        try {
            this.f3368a.d(b.a(mediationRewardedVideoAdAdapter));
        } catch (RemoteException e) {
            ms.d("#007 Could not call remote method.", e);
        }
    }

    public final void e(MediationRewardedVideoAdAdapter mediationRewardedVideoAdAdapter) {
        aa.b("#008 Must be called on the main UI thread.");
        ms.b("Adapter called onAdClosed.");
        try {
            this.f3368a.e(b.a(mediationRewardedVideoAdAdapter));
        } catch (RemoteException e) {
            ms.d("#007 Could not call remote method.", e);
        }
    }

    public final void f(MediationRewardedVideoAdAdapter mediationRewardedVideoAdAdapter) {
        aa.b("#008 Must be called on the main UI thread.");
        ms.b("Adapter called onAdLeftApplication.");
        try {
            this.f3368a.g(b.a(mediationRewardedVideoAdAdapter));
        } catch (RemoteException e) {
            ms.d("#007 Could not call remote method.", e);
        }
    }

    public final void g(MediationRewardedVideoAdAdapter mediationRewardedVideoAdAdapter) {
        aa.b("#008 Must be called on the main UI thread.");
        ms.b("Adapter called onVideoCompleted.");
        try {
            this.f3368a.h(b.a(mediationRewardedVideoAdAdapter));
        } catch (RemoteException e) {
            ms.d("#007 Could not call remote method.", e);
        }
    }
}
