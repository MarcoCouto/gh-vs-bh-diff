package com.google.android.gms.internal.ads;

import android.os.RemoteException;

final class ayd implements ays {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ String f2999a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ String f3000b;

    ayd(ayc ayc, String str, String str2) {
        this.f2999a = str;
        this.f3000b = str2;
    }

    public final void a(ayt ayt) throws RemoteException {
        if (ayt.c != null) {
            ayt.c.a(this.f2999a, this.f3000b);
        }
    }
}
