package com.google.android.gms.internal.ads;

import android.os.Parcel;
import com.google.android.gms.common.internal.a.c;

@cm
public final class arp extends aot {
    public arp(aot aot) {
        super(aot.f2814a, aot.f2815b, aot.c, aot.d, aot.e, aot.f, aot.g, aot.h, aot.i, aot.j);
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = c.a(parcel);
        c.a(parcel, 2, this.f2814a, false);
        c.a(parcel, 3, this.f2815b);
        c.a(parcel, 6, this.e);
        c.a(parcel, a2);
    }
}
