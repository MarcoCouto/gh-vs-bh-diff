package com.google.android.gms.internal.ads;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;

final class i implements OnClickListener {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ g f3381a;

    i(g gVar) {
        this.f3381a = gVar;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f3381a.a("User canceled the download.");
    }
}
