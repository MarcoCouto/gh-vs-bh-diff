package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.app.Application.ActivityLifecycleCallbacks;
import android.os.Bundle;

final class ahn implements ahu {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ Activity f2593a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ Bundle f2594b;

    ahn(ahm ahm, Activity activity, Bundle bundle) {
        this.f2593a = activity;
        this.f2594b = bundle;
    }

    public final void a(ActivityLifecycleCallbacks activityLifecycleCallbacks) {
        activityLifecycleCallbacks.onActivityCreated(this.f2593a, this.f2594b);
    }
}
