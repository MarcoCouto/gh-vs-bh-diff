package com.google.android.gms.internal.ads;

import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

final class adu implements Iterator<Entry<K, V>> {

    /* renamed from: a reason: collision with root package name */
    private int f2490a;

    /* renamed from: b reason: collision with root package name */
    private Iterator<Entry<K, V>> f2491b;
    private final /* synthetic */ ads c;

    private adu(ads ads) {
        this.c = ads;
        this.f2490a = this.c.f2489b.size();
    }

    /* synthetic */ adu(ads ads, adt adt) {
        this(ads);
    }

    private final Iterator<Entry<K, V>> a() {
        if (this.f2491b == null) {
            this.f2491b = this.c.f.entrySet().iterator();
        }
        return this.f2491b;
    }

    public final boolean hasNext() {
        return (this.f2490a > 0 && this.f2490a <= this.c.f2489b.size()) || a().hasNext();
    }

    public final /* synthetic */ Object next() {
        if (a().hasNext()) {
            return (Entry) a().next();
        }
        List b2 = this.c.f2489b;
        int i = this.f2490a - 1;
        this.f2490a = i;
        return (Entry) b2.get(i);
    }

    public final void remove() {
        throw new UnsupportedOperationException();
    }
}
