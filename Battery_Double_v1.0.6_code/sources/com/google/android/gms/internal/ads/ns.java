package com.google.android.gms.internal.ads;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;

public interface ns extends ExecutorService {
    nn<?> a(Runnable runnable);

    <T> nn<T> a(Callable<T> callable);
}
