package com.google.android.gms.internal.ads;

final class bcn implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ nn f3138a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ bcl f3139b;

    bcn(bcl bcl, nn nnVar) {
        this.f3139b = bcl;
        this.f3138a = nnVar;
    }

    public final void run() {
        for (nn nnVar : this.f3139b.k.keySet()) {
            if (nnVar != this.f3138a) {
                ((bcf) this.f3139b.k.get(nnVar)).a();
            }
        }
    }
}
