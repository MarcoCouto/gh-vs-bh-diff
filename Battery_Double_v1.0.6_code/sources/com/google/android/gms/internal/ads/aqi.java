package com.google.android.gms.internal.ads;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.b.a.C0046a;

public abstract class aqi extends ajl implements aqh {
    public aqi() {
        super("com.google.android.gms.ads.internal.client.IClientApi");
    }

    public static aqh asInterface(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.IClientApi");
        return queryLocalInterface instanceof aqh ? (aqh) queryLocalInterface : new aqj(iBinder);
    }

    /* access modifiers changed from: protected */
    public final boolean dispatchTransaction(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        switch (i) {
            case 1:
                apv createBannerAdManager = createBannerAdManager(C0046a.a(parcel.readStrongBinder()), (aot) ajm.a(parcel, aot.CREATOR), parcel.readString(), bcs.a(parcel.readStrongBinder()), parcel.readInt());
                parcel2.writeNoException();
                ajm.a(parcel2, (IInterface) createBannerAdManager);
                break;
            case 2:
                apv createInterstitialAdManager = createInterstitialAdManager(C0046a.a(parcel.readStrongBinder()), (aot) ajm.a(parcel, aot.CREATOR), parcel.readString(), bcs.a(parcel.readStrongBinder()), parcel.readInt());
                parcel2.writeNoException();
                ajm.a(parcel2, (IInterface) createInterstitialAdManager);
                break;
            case 3:
                apq createAdLoaderBuilder = createAdLoaderBuilder(C0046a.a(parcel.readStrongBinder()), parcel.readString(), bcs.a(parcel.readStrongBinder()), parcel.readInt());
                parcel2.writeNoException();
                ajm.a(parcel2, (IInterface) createAdLoaderBuilder);
                break;
            case 4:
                aqn mobileAdsSettingsManager = getMobileAdsSettingsManager(C0046a.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                ajm.a(parcel2, (IInterface) mobileAdsSettingsManager);
                break;
            case 5:
                avb createNativeAdViewDelegate = createNativeAdViewDelegate(C0046a.a(parcel.readStrongBinder()), C0046a.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                ajm.a(parcel2, (IInterface) createNativeAdViewDelegate);
                break;
            case 6:
                gh createRewardedVideoAd = createRewardedVideoAd(C0046a.a(parcel.readStrongBinder()), bcs.a(parcel.readStrongBinder()), parcel.readInt());
                parcel2.writeNoException();
                ajm.a(parcel2, (IInterface) createRewardedVideoAd);
                break;
            case 7:
                ab createInAppPurchaseManager = createInAppPurchaseManager(C0046a.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                ajm.a(parcel2, (IInterface) createInAppPurchaseManager);
                break;
            case 8:
                r createAdOverlay = createAdOverlay(C0046a.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                ajm.a(parcel2, (IInterface) createAdOverlay);
                break;
            case 9:
                aqn mobileAdsSettingsManagerWithClientJarVersion = getMobileAdsSettingsManagerWithClientJarVersion(C0046a.a(parcel.readStrongBinder()), parcel.readInt());
                parcel2.writeNoException();
                ajm.a(parcel2, (IInterface) mobileAdsSettingsManagerWithClientJarVersion);
                break;
            case 10:
                apv createSearchAdManager = createSearchAdManager(C0046a.a(parcel.readStrongBinder()), (aot) ajm.a(parcel, aot.CREATOR), parcel.readString(), parcel.readInt());
                parcel2.writeNoException();
                ajm.a(parcel2, (IInterface) createSearchAdManager);
                break;
            case 11:
                avg createNativeAdViewHolderDelegate = createNativeAdViewHolderDelegate(C0046a.a(parcel.readStrongBinder()), C0046a.a(parcel.readStrongBinder()), C0046a.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                ajm.a(parcel2, (IInterface) createNativeAdViewHolderDelegate);
                break;
            default:
                return false;
        }
        return true;
    }
}
