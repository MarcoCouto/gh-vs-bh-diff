package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.ax;

final class lh implements mx<Throwable, T> {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ ll f3479a;

    lh(lf lfVar, ll llVar) {
        this.f3479a = llVar;
    }

    public final /* synthetic */ nn a(Object obj) throws Exception {
        Throwable th = (Throwable) obj;
        jm.b("Error occurred while dispatching http response in getter.", th);
        ax.i().a(th, "HttpGetter.deliverResponse.1");
        return nc.a(this.f3479a.a());
    }
}
