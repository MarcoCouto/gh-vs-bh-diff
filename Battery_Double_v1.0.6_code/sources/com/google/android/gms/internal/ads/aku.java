package com.google.android.gms.internal.ads;

import android.graphics.Rect;

public final class aku {

    /* renamed from: a reason: collision with root package name */
    public final boolean f2678a;

    /* renamed from: b reason: collision with root package name */
    private final long f2679b;
    private final boolean c;
    private final boolean d;
    private final int e;
    private final Rect f;
    private final Rect g;
    private final Rect h;
    private final boolean i;
    private final Rect j;
    private final boolean k;
    private final Rect l;
    private final float m;

    public aku(long j2, boolean z, boolean z2, int i2, Rect rect, Rect rect2, Rect rect3, boolean z3, Rect rect4, boolean z4, Rect rect5, float f2, boolean z5) {
        this.f2679b = j2;
        this.c = z;
        this.d = z2;
        this.e = i2;
        this.f = rect;
        this.g = rect2;
        this.h = rect3;
        this.i = z3;
        this.j = rect4;
        this.k = z4;
        this.l = rect5;
        this.m = f2;
        this.f2678a = z5;
    }
}
