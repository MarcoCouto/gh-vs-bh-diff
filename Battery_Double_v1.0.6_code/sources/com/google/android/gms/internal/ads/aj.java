package com.google.android.gms.internal.ads;

final class aj implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ ai f2630a;

    aj(ai aiVar) {
        this.f2630a = aiVar;
    }

    public final void run() {
        if (this.f2630a.h.get()) {
            jm.c("Timed out waiting for WebView to finish loading.");
            this.f2630a.b();
        }
    }
}
