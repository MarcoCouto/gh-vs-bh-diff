package com.google.android.gms.internal.ads;

final /* synthetic */ class aau {

    /* renamed from: a reason: collision with root package name */
    static final /* synthetic */ int[] f2404a = new int[aew.values().length];

    static {
        try {
            f2404a[aew.BOOL.ordinal()] = 1;
        } catch (NoSuchFieldError e) {
        }
        try {
            f2404a[aew.BYTES.ordinal()] = 2;
        } catch (NoSuchFieldError e2) {
        }
        try {
            f2404a[aew.DOUBLE.ordinal()] = 3;
        } catch (NoSuchFieldError e3) {
        }
        try {
            f2404a[aew.ENUM.ordinal()] = 4;
        } catch (NoSuchFieldError e4) {
        }
        try {
            f2404a[aew.FIXED32.ordinal()] = 5;
        } catch (NoSuchFieldError e5) {
        }
        try {
            f2404a[aew.FIXED64.ordinal()] = 6;
        } catch (NoSuchFieldError e6) {
        }
        try {
            f2404a[aew.FLOAT.ordinal()] = 7;
        } catch (NoSuchFieldError e7) {
        }
        try {
            f2404a[aew.INT32.ordinal()] = 8;
        } catch (NoSuchFieldError e8) {
        }
        try {
            f2404a[aew.INT64.ordinal()] = 9;
        } catch (NoSuchFieldError e9) {
        }
        try {
            f2404a[aew.MESSAGE.ordinal()] = 10;
        } catch (NoSuchFieldError e10) {
        }
        try {
            f2404a[aew.SFIXED32.ordinal()] = 11;
        } catch (NoSuchFieldError e11) {
        }
        try {
            f2404a[aew.SFIXED64.ordinal()] = 12;
        } catch (NoSuchFieldError e12) {
        }
        try {
            f2404a[aew.SINT32.ordinal()] = 13;
        } catch (NoSuchFieldError e13) {
        }
        try {
            f2404a[aew.SINT64.ordinal()] = 14;
        } catch (NoSuchFieldError e14) {
        }
        try {
            f2404a[aew.STRING.ordinal()] = 15;
        } catch (NoSuchFieldError e15) {
        }
        try {
            f2404a[aew.UINT32.ordinal()] = 16;
        } catch (NoSuchFieldError e16) {
        }
        try {
            f2404a[aew.UINT64.ordinal()] = 17;
        } catch (NoSuchFieldError e17) {
        }
    }
}
