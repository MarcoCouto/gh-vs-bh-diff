package com.google.android.gms.internal.ads;

import android.view.View;
import com.google.android.gms.ads.internal.g;
import com.google.android.gms.b.a;
import com.google.android.gms.b.b;

@cm
public final class asw extends ata {

    /* renamed from: a reason: collision with root package name */
    private final g f2891a;

    /* renamed from: b reason: collision with root package name */
    private final String f2892b;
    private final String c;

    public asw(g gVar, String str, String str2) {
        this.f2891a = gVar;
        this.f2892b = str;
        this.c = str2;
    }

    public final String a() {
        return this.f2892b;
    }

    public final void a(a aVar) {
        if (aVar != null) {
            this.f2891a.a_((View) b.a(aVar));
        }
    }

    public final String b() {
        return this.c;
    }

    public final void c() {
        this.f2891a.f_();
    }

    public final void d() {
        this.f2891a.g_();
    }
}
