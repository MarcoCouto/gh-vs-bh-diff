package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.abj;
import java.io.IOException;
import java.util.Map.Entry;

abstract class abe<T extends abj<T>> {
    abe() {
    }

    /* access modifiers changed from: 0000 */
    public abstract int a(Entry<?, ?> entry);

    /* access modifiers changed from: 0000 */
    public abstract abh<T> a(Object obj);

    /* access modifiers changed from: 0000 */
    public abstract Object a(abc abc, acw acw, int i);

    /* access modifiers changed from: 0000 */
    public abstract <UT, UB> UB a(ado ado, Object obj, abc abc, abh<T> abh, UB ub, aei<UT, UB> aei) throws IOException;

    /* access modifiers changed from: 0000 */
    public abstract void a(aah aah, Object obj, abc abc, abh<T> abh) throws IOException;

    /* access modifiers changed from: 0000 */
    public abstract void a(ado ado, Object obj, abc abc, abh<T> abh) throws IOException;

    /* access modifiers changed from: 0000 */
    public abstract void a(afc afc, Entry<?, ?> entry) throws IOException;

    /* access modifiers changed from: 0000 */
    public abstract void a(Object obj, abh<T> abh);

    /* access modifiers changed from: 0000 */
    public abstract boolean a(acw acw);

    /* access modifiers changed from: 0000 */
    public abstract abh<T> b(Object obj);

    /* access modifiers changed from: 0000 */
    public abstract void c(Object obj);
}
