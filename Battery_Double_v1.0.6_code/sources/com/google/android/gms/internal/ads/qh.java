package com.google.android.gms.internal.ads;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.webkit.JsResult;

final class qh implements OnClickListener {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ JsResult f3630a;

    qh(JsResult jsResult) {
        this.f3630a = jsResult;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f3630a.cancel();
    }
}
