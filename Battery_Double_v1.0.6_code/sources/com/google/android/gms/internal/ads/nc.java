package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.ax;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

@cm
public final class nc {
    public static <T> nl<T> a(Throwable th) {
        return new nl<>(th);
    }

    public static <T> nm<T> a(T t) {
        return new nm<>(t);
    }

    public static <V> nn<V> a(nn<V> nnVar, long j, TimeUnit timeUnit, ScheduledExecutorService scheduledExecutorService) {
        ny nyVar = new ny();
        a((nn<A>) nyVar, (Future<B>) nnVar);
        ScheduledFuture schedule = scheduledExecutorService.schedule(new ng(nyVar), j, timeUnit);
        a(nnVar, nyVar);
        nyVar.a(new nh(schedule), nt.f3559b);
        return nyVar;
    }

    public static <A, B> nn<B> a(nn<A> nnVar, mx<? super A, ? extends B> mxVar, Executor executor) {
        ny nyVar = new ny();
        nnVar.a(new nf(nyVar, mxVar, nnVar), executor);
        a((nn<A>) nyVar, (Future<B>) nnVar);
        return nyVar;
    }

    public static <A, B> nn<B> a(nn<A> nnVar, my<A, B> myVar, Executor executor) {
        ny nyVar = new ny();
        nnVar.a(new ne(nyVar, myVar, nnVar), executor);
        a((nn<A>) nyVar, (Future<B>) nnVar);
        return nyVar;
    }

    public static <V, X extends Throwable> nn<V> a(nn<? extends V> nnVar, Class<X> cls, mx<? super X, ? extends V> mxVar, Executor executor) {
        ny nyVar = new ny();
        a((nn<A>) nyVar, (Future<B>) nnVar);
        nnVar.a(new ni(nyVar, nnVar, cls, mxVar, executor), nt.f3559b);
        return nyVar;
    }

    public static <T> T a(Future<T> future, T t) {
        try {
            return future.get(((Long) ape.f().a(asi.bz)).longValue(), TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            future.cancel(true);
            jm.c("InterruptedException caught while resolving future.", e);
            Thread.currentThread().interrupt();
            ax.i().b(e, "Futures.resolveFuture");
            return t;
        } catch (Exception e2) {
            future.cancel(true);
            jm.b("Error waiting for future.", e2);
            ax.i().b(e2, "Futures.resolveFuture");
            return t;
        }
    }

    public static <T> T a(Future<T> future, T t, long j, TimeUnit timeUnit) {
        try {
            return future.get(j, timeUnit);
        } catch (InterruptedException e) {
            future.cancel(true);
            jm.c("InterruptedException caught while resolving future.", e);
            Thread.currentThread().interrupt();
            ax.i().a((Throwable) e, "Futures.resolveFuture");
            return t;
        } catch (Exception e2) {
            future.cancel(true);
            jm.b("Error waiting for future.", e2);
            ax.i().a((Throwable) e2, "Futures.resolveFuture");
            return t;
        }
    }

    public static <V> void a(nn<V> nnVar, mz<V> mzVar, Executor executor) {
        nnVar.a(new nd(mzVar, nnVar), executor);
    }

    private static <V> void a(nn<? extends V> nnVar, ny<V> nyVar) {
        a((nn<A>) nyVar, (Future<B>) nnVar);
        nnVar.a(new nj(nyVar, nnVar), nt.f3559b);
    }

    private static <A, B> void a(nn<A> nnVar, Future<B> future) {
        nnVar.a(new nk(nnVar, future), nt.f3559b);
    }

    static final /* synthetic */ void a(ny nyVar, mx mxVar, nn nnVar) {
        if (!nyVar.isCancelled()) {
            try {
                a(mxVar.a(nnVar.get()), nyVar);
            } catch (CancellationException e) {
                nyVar.cancel(true);
            } catch (ExecutionException e2) {
                nyVar.a(e2.getCause());
            } catch (InterruptedException e3) {
                Thread.currentThread().interrupt();
                nyVar.a(e3);
            } catch (Exception e4) {
                nyVar.a(e4);
            }
        }
    }

    static final /* synthetic */ void a(ny nyVar, nn nnVar, Class cls, mx mxVar, Executor executor) {
        try {
            nyVar.b(nnVar.get());
            return;
        } catch (ExecutionException e) {
            e = e.getCause();
        } catch (InterruptedException e2) {
            e = e2;
            Thread.currentThread().interrupt();
        } catch (Exception e3) {
            e = e3;
        }
        if (cls.isInstance(e)) {
            a(a((nn<A>) a((T) e), mxVar, executor), nyVar);
        } else {
            nyVar.a(e);
        }
    }
}
