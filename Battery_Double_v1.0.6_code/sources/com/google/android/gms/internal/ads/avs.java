package com.google.android.gms.internal.ads;

import android.os.IBinder;
import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.ads.b.c.a;
import com.google.android.gms.ads.b.c.b;
import com.google.android.gms.ads.b.h;
import com.google.android.gms.ads.i;
import java.util.ArrayList;
import java.util.List;

@cm
public final class avs extends h {

    /* renamed from: a reason: collision with root package name */
    private final avp f2957a;

    /* renamed from: b reason: collision with root package name */
    private final List<b> f2958b = new ArrayList();
    private final auz c;
    private final i d = new i();
    private final a e;

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0042 A[Catch:{ RemoteException -> 0x004d }] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0020 A[SYNTHETIC] */
    public avs(avp avp) {
        auz auz;
        auw auw;
        a aVar = null;
        this.f2957a = avp;
        try {
            List b2 = this.f2957a.b();
            if (b2 != null) {
                for (Object next : b2) {
                    if (next instanceof IBinder) {
                        IBinder iBinder = (IBinder) next;
                        if (iBinder != null) {
                            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.ads.internal.formats.client.INativeAdImage");
                            auw = queryLocalInterface instanceof auw ? (auw) queryLocalInterface : new auy(iBinder);
                            if (auw == null) {
                                this.f2958b.add(new auz(auw));
                            }
                        }
                    }
                    auw = null;
                    if (auw == null) {
                    }
                }
            }
        } catch (RemoteException e2) {
            ms.b("", e2);
        }
        try {
            auw f = this.f2957a.f();
            auz = f != null ? new auz(f) : null;
        } catch (RemoteException e3) {
            ms.b("", e3);
            auz = null;
        }
        this.c = auz;
        try {
            if (this.f2957a.p() != null) {
                aVar = new auv(this.f2957a.p());
            }
        } catch (RemoteException e4) {
            ms.b("", e4);
        }
        this.e = aVar;
    }

    /* access modifiers changed from: private */
    /* renamed from: i */
    public final com.google.android.gms.b.a a() {
        try {
            return this.f2957a.j();
        } catch (RemoteException e2) {
            ms.b("", e2);
            return null;
        }
    }

    public final CharSequence b() {
        try {
            return this.f2957a.a();
        } catch (RemoteException e2) {
            ms.b("", e2);
            return null;
        }
    }

    public final List<b> c() {
        return this.f2958b;
    }

    public final CharSequence d() {
        try {
            return this.f2957a.e();
        } catch (RemoteException e2) {
            ms.b("", e2);
            return null;
        }
    }

    public final b e() {
        return this.c;
    }

    public final CharSequence f() {
        try {
            return this.f2957a.g();
        } catch (RemoteException e2) {
            ms.b("", e2);
            return null;
        }
    }

    public final CharSequence g() {
        try {
            return this.f2957a.h();
        } catch (RemoteException e2) {
            ms.b("", e2);
            return null;
        }
    }

    public final i h() {
        try {
            if (this.f2957a.i() != null) {
                this.d.a(this.f2957a.i());
            }
        } catch (RemoteException e2) {
            ms.b("Exception occurred while getting video controller", e2);
        }
        return this.d;
    }
}
