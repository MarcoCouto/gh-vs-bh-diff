package com.google.android.gms.internal.ads;

final class acn implements acv {

    /* renamed from: a reason: collision with root package name */
    private acv[] f2460a;

    acn(acv... acvArr) {
        this.f2460a = acvArr;
    }

    public final boolean a(Class<?> cls) {
        for (acv a2 : this.f2460a) {
            if (a2.a(cls)) {
                return true;
            }
        }
        return false;
    }

    public final acu b(Class<?> cls) {
        acv[] acvArr;
        for (acv acv : this.f2460a) {
            if (acv.a(cls)) {
                return acv.b(cls);
            }
        }
        String str = "No factory is available for message type: ";
        String valueOf = String.valueOf(cls.getName());
        throw new UnsupportedOperationException(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
    }
}
