package com.google.android.gms.internal.ads;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import org.json.JSONException;
import org.json.JSONObject;

final class jp extends jr {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ Context f3432a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ jo f3433b;

    jp(jo joVar, Context context) {
        this.f3433b = joVar;
        this.f3432a = context;
        super(null);
    }

    public final void a() {
        SharedPreferences sharedPreferences = this.f3432a.getSharedPreferences("admob", 0);
        Editor edit = sharedPreferences.edit();
        synchronized (this.f3433b.f3431b) {
            this.f3433b.e = sharedPreferences;
            this.f3433b.f3430a = edit;
            this.f3433b.f = jo.n();
            this.f3433b.g = this.f3433b.e.getBoolean("use_https", this.f3433b.g);
            this.f3433b.s = this.f3433b.e.getBoolean("content_url_opted_out", this.f3433b.s);
            this.f3433b.h = this.f3433b.e.getString("content_url_hashes", this.f3433b.h);
            this.f3433b.j = this.f3433b.e.getBoolean("auto_collect_location", this.f3433b.j);
            this.f3433b.t = this.f3433b.e.getBoolean("content_vertical_opted_out", this.f3433b.t);
            this.f3433b.i = this.f3433b.e.getString("content_vertical_hashes", this.f3433b.i);
            this.f3433b.p = this.f3433b.e.getInt("version_code", this.f3433b.p);
            this.f3433b.k = this.f3433b.e.getString("app_settings_json", this.f3433b.k);
            this.f3433b.l = this.f3433b.e.getLong("app_settings_last_update_ms", this.f3433b.l);
            this.f3433b.m = this.f3433b.e.getLong("app_last_background_time_ms", this.f3433b.m);
            this.f3433b.o = this.f3433b.e.getInt("request_in_session_count", this.f3433b.o);
            this.f3433b.n = this.f3433b.e.getLong("first_ad_req_time_ms", this.f3433b.n);
            this.f3433b.q = this.f3433b.e.getStringSet("never_pool_slots", this.f3433b.q);
            try {
                this.f3433b.r = new JSONObject(this.f3433b.e.getString("native_advanced_settings", "{}"));
            } catch (JSONException e) {
                jm.c("Could not convert native advanced settings to json object", e);
            }
            this.f3433b.a(this.f3433b.p());
        }
    }
}
