package com.google.android.gms.internal.ads;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.d.a;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;

@cm
public final class aos {

    /* renamed from: a reason: collision with root package name */
    public static final aos f2813a = new aos();

    protected aos() {
    }

    public static aop a(Context context, ara ara) {
        Date a2 = ara.a();
        long j = a2 != null ? a2.getTime() : -1;
        String b2 = ara.b();
        int c = ara.c();
        Set d = ara.d();
        List list = !d.isEmpty() ? Collections.unmodifiableList(new ArrayList(d)) : null;
        boolean a3 = ara.a(context);
        int l = ara.l();
        Location e = ara.e();
        Bundle a4 = ara.a(AdMobAdapter.class);
        boolean f = ara.f();
        String g = ara.g();
        a i = ara.i();
        arn arn = i != null ? new arn(i) : null;
        String str = null;
        Context applicationContext = context.getApplicationContext();
        if (applicationContext != null) {
            String packageName = applicationContext.getPackageName();
            ape.a();
            str = mh.a(Thread.currentThread().getStackTrace(), packageName);
        }
        return new aop(7, j, a4, c, list, a3, l, f, g, arn, e, b2, ara.k(), ara.m(), Collections.unmodifiableList(new ArrayList(ara.n())), ara.h(), str, ara.o());
    }
}
