package com.google.android.gms.internal.ads;

import android.annotation.TargetApi;
import android.graphics.SurfaceTexture;
import java.util.concurrent.TimeUnit;

@TargetApi(14)
@cm
public final class pf {

    /* renamed from: a reason: collision with root package name */
    private final long f3597a = TimeUnit.MILLISECONDS.toNanos(((Long) ape.f().a(asi.x)).longValue());

    /* renamed from: b reason: collision with root package name */
    private long f3598b;
    private boolean c = true;

    pf() {
    }

    public final void a() {
        this.c = true;
    }

    public final void a(SurfaceTexture surfaceTexture, ov ovVar) {
        if (ovVar != null) {
            long timestamp = surfaceTexture.getTimestamp();
            if (this.c || Math.abs(timestamp - this.f3598b) >= this.f3597a) {
                this.c = false;
                this.f3598b = timestamp;
                jv.f3440a.post(new pg(this, ovVar));
            }
        }
    }
}
