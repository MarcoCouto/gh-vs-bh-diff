package com.google.android.gms.internal.ads;

import java.io.IOException;

public final class anw extends afh<anw> {

    /* renamed from: a reason: collision with root package name */
    private static volatile anw[] f2782a;

    /* renamed from: b reason: collision with root package name */
    private String f2783b;
    private Integer c;
    private anx d;

    public anw() {
        this.f2783b = null;
        this.c = null;
        this.d = null;
        this.Y = null;
        this.Z = -1;
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public final anw a(afd afd) throws IOException {
        while (true) {
            int a2 = afd.a();
            switch (a2) {
                case 0:
                    break;
                case 10:
                    this.f2783b = afd.e();
                    continue;
                case 16:
                    int j = afd.j();
                    try {
                        this.c = Integer.valueOf(anf.a(afd.g()));
                        continue;
                    } catch (IllegalArgumentException e) {
                        afd.e(j);
                        a(afd, a2);
                        break;
                    }
                case 26:
                    if (this.d == null) {
                        this.d = new anx();
                    }
                    afd.a((afn) this.d);
                    continue;
                default:
                    if (!super.a(afd, a2)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    public static anw[] b() {
        if (f2782a == null) {
            synchronized (afl.f2531b) {
                if (f2782a == null) {
                    f2782a = new anw[0];
                }
            }
        }
        return f2782a;
    }

    /* access modifiers changed from: protected */
    public final int a() {
        int a2 = super.a();
        if (this.f2783b != null) {
            a2 += aff.b(1, this.f2783b);
        }
        if (this.c != null) {
            a2 += aff.b(2, this.c.intValue());
        }
        return this.d != null ? a2 + aff.b(3, (afn) this.d) : a2;
    }

    public final void a(aff aff) throws IOException {
        if (this.f2783b != null) {
            aff.a(1, this.f2783b);
        }
        if (this.c != null) {
            aff.a(2, this.c.intValue());
        }
        if (this.d != null) {
            aff.a(3, (afn) this.d);
        }
        super.a(aff);
    }
}
