package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.ax;
import java.util.ArrayList;

@cm
public final class alh {

    /* renamed from: a reason: collision with root package name */
    private final int f2692a;

    /* renamed from: b reason: collision with root package name */
    private final int f2693b;
    private final int c;
    private final alu d;
    private final ame e;
    private final Object f = new Object();
    private ArrayList<String> g = new ArrayList<>();
    private ArrayList<String> h = new ArrayList<>();
    private ArrayList<als> i = new ArrayList<>();
    private int j = 0;
    private int k = 0;
    private int l = 0;
    private int m;
    private String n = "";
    private String o = "";
    private String p = "";

    public alh(int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        this.f2692a = i2;
        this.f2693b = i3;
        this.c = i4;
        this.d = new alu(i5);
        this.e = new ame(i6, i7, i8);
    }

    private static String a(ArrayList<String> arrayList, int i2) {
        if (arrayList.isEmpty()) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        ArrayList arrayList2 = arrayList;
        int size = arrayList2.size();
        int i3 = 0;
        while (i3 < size) {
            Object obj = arrayList2.get(i3);
            i3++;
            sb.append((String) obj);
            sb.append(' ');
            if (sb.length() > 100) {
                break;
            }
        }
        sb.deleteCharAt(sb.length() - 1);
        String sb2 = sb.toString();
        return sb2.length() >= 100 ? sb2.substring(0, 100) : sb2;
    }

    private final void c(String str, boolean z, float f2, float f3, float f4, float f5) {
        if (str != null && str.length() >= this.c) {
            synchronized (this.f) {
                this.g.add(str);
                this.j += str.length();
                if (z) {
                    this.h.add(str);
                    this.i.add(new als(f2, f3, f4, f5, this.h.size() - 1));
                }
            }
        }
    }

    public final void a(int i2) {
        this.k = i2;
    }

    public final void a(String str, boolean z, float f2, float f3, float f4, float f5) {
        c(str, z, f2, f3, f4, f5);
        synchronized (this.f) {
            if (this.l < 0) {
                jm.b("ActivityContent: negative number of WebViews.");
            }
            h();
        }
    }

    public final boolean a() {
        boolean z;
        synchronized (this.f) {
            z = this.l == 0;
        }
        return z;
    }

    public final String b() {
        return this.n;
    }

    public final void b(String str, boolean z, float f2, float f3, float f4, float f5) {
        c(str, z, f2, f3, f4, f5);
    }

    public final String c() {
        return this.o;
    }

    public final String d() {
        return this.p;
    }

    public final void e() {
        synchronized (this.f) {
            this.m -= 100;
        }
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof alh)) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        alh alh = (alh) obj;
        return alh.n != null && alh.n.equals(this.n);
    }

    public final void f() {
        synchronized (this.f) {
            this.l--;
        }
    }

    public final void g() {
        synchronized (this.f) {
            this.l++;
        }
    }

    public final void h() {
        synchronized (this.f) {
            int i2 = (this.j * this.f2692a) + (this.k * this.f2693b);
            if (i2 > this.m) {
                this.m = i2;
                if (((Boolean) ape.f().a(asi.W)).booleanValue() && !ax.i().l().b()) {
                    this.n = this.d.a(this.g);
                    this.o = this.d.a(this.h);
                }
                if (((Boolean) ape.f().a(asi.Y)).booleanValue() && !ax.i().l().d()) {
                    this.p = this.e.a(this.h, this.i);
                }
            }
        }
    }

    public final int hashCode() {
        return this.n.hashCode();
    }

    public final int i() {
        return this.m;
    }

    /* access modifiers changed from: 0000 */
    public final int j() {
        return this.j;
    }

    public final String toString() {
        int i2 = this.k;
        int i3 = this.m;
        int i4 = this.j;
        String a2 = a(this.g, 100);
        String a3 = a(this.h, 100);
        String str = this.n;
        String str2 = this.o;
        String str3 = this.p;
        return new StringBuilder(String.valueOf(a2).length() + 165 + String.valueOf(a3).length() + String.valueOf(str).length() + String.valueOf(str2).length() + String.valueOf(str3).length()).append("ActivityContent fetchId: ").append(i2).append(" score:").append(i3).append(" total_length:").append(i4).append("\n text: ").append(a2).append("\n viewableText").append(a3).append("\n signture: ").append(str).append("\n viewableSignture: ").append(str2).append("\n viewableSignatureForVertical: ").append(str3).toString();
    }
}
