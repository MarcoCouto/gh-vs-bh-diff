package com.google.android.gms.internal.ads;

import android.content.Context;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

public final class ahg extends ahf {
    private ahg(Context context, String str, boolean z) {
        super(context, str, z);
    }

    public static ahg a(String str, Context context, boolean z) {
        a(context, z);
        return new ahg(context, str, z);
    }

    /* access modifiers changed from: protected */
    public final List<Callable<Void>> a(ahy ahy, zz zzVar, wv wvVar) {
        if (ahy.c() == null || !this.r) {
            return super.a(ahy, zzVar, wvVar);
        }
        int n = ahy.n();
        ArrayList arrayList = new ArrayList();
        arrayList.addAll(super.a(ahy, zzVar, wvVar));
        arrayList.add(new ais(ahy, "1QeH3Cf7T53ayw17Ebbo9YTdhU+IFx0X5nCtC5gZQym4uicOVPXxYWmMK9k58i8n", "bHJRpFJ+2R5LAbYQUBDMyfYpLd1oiGixlpIqMJOBQPY=", zzVar, n, 24));
        return arrayList;
    }
}
