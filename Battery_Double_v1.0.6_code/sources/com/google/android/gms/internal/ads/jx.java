package com.google.android.gms.internal.ads;

import android.content.Context;

final class jx implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ Context f3444a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ jv f3445b;

    jx(jv jvVar, Context context) {
        this.f3445b = jvVar;
        this.f3444a = context;
    }

    public final void run() {
        synchronized (this.f3445b.f3441b) {
            this.f3445b.d = jv.d(this.f3444a);
            this.f3445b.f3441b.notifyAll();
        }
    }
}
