package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.DeadObjectException;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.google.android.gms.common.internal.e;
import com.google.android.gms.common.internal.e.a;
import com.google.android.gms.common.internal.e.b;

public final class tk extends e<tp> {
    public tk(Context context, Looper looper, a aVar, b bVar) {
        super(context, looper, 116, aVar, bVar, null);
    }

    public final tp A() throws DeadObjectException {
        return (tp) super.x();
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ IInterface a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.gass.internal.IGassService");
        return queryLocalInterface instanceof tp ? (tp) queryLocalInterface : new tq(iBinder);
    }

    /* access modifiers changed from: protected */
    public final String i() {
        return "com.google.android.gms.gass.START";
    }

    /* access modifiers changed from: protected */
    public final String l() {
        return "com.google.android.gms.gass.internal.IGassService";
    }
}
