package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.ads.internal.ax;
import java.util.concurrent.Executor;

@cm
public final class bah {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public final Object f3065a;

    /* renamed from: b reason: collision with root package name */
    private final Context f3066b;
    private final String c;
    private final mu d;
    /* access modifiers changed from: private */
    public la<azv> e;
    private la<azv> f;
    /* access modifiers changed from: private */
    public bay g;
    /* access modifiers changed from: private */
    public int h;

    public bah(Context context, mu muVar, String str) {
        this.f3065a = new Object();
        this.h = 1;
        this.c = str;
        this.f3066b = context.getApplicationContext();
        this.d = muVar;
        this.e = new bat();
        this.f = new bat();
    }

    public bah(Context context, mu muVar, String str, la<azv> laVar, la<azv> laVar2) {
        this(context, muVar, str);
        this.e = laVar;
        this.f = laVar2;
    }

    /* access modifiers changed from: protected */
    public final bay a(ahh ahh) {
        bay bay = new bay(this.f);
        nt.f3558a.execute(new bai(this, ahh, bay));
        bay.a(new baq(this, bay), new bar(this, bay));
        return bay;
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ void a(ahh ahh, bay bay) {
        try {
            Context context = this.f3066b;
            mu muVar = this.d;
            azv azx = ((Boolean) ape.f().a(asi.aA)).booleanValue() ? new azg(context, muVar) : new azx(context, muVar, ahh, null);
            azx.a((azw) new baj(this, bay, azx));
            azx.a("/jsLoaded", new bam(this, bay, azx));
            lx lxVar = new lx();
            ban ban = new ban(this, ahh, azx, lxVar);
            lxVar.a(ban);
            azx.a("/requestReload", ban);
            if (this.c.endsWith(".js")) {
                azx.a(this.c);
            } else if (this.c.startsWith("<html>")) {
                azx.c(this.c);
            } else {
                azx.d(this.c);
            }
            jv.f3440a.postDelayed(new bao(this, bay, azx), (long) bas.f3085a);
        } catch (Throwable th) {
            jm.b("Error creating webview.", th);
            ax.i().a(th, "SdkJavascriptFactory.loadJavascriptEngine");
            bay.a();
        }
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        return;
     */
    public final /* synthetic */ void a(bay bay, azv azv) {
        synchronized (this.f3065a) {
            if (bay.b() != -1 && bay.b() != 1) {
                bay.a();
                Executor executor = nt.f3558a;
                azv.getClass();
                executor.execute(bal.a(azv));
                jm.a("Could not receive loaded message in a timely manner. Rejecting.");
            }
        }
    }

    public final bau b(ahh ahh) {
        bau bau;
        synchronized (this.f3065a) {
            if (this.g == null || this.g.b() == -1) {
                this.h = 2;
                this.g = a((ahh) null);
                bau = this.g.c();
            } else if (this.h == 0) {
                bau = this.g.c();
            } else if (this.h == 1) {
                this.h = 2;
                a((ahh) null);
                bau = this.g.c();
            } else {
                bau = this.h == 2 ? this.g.c() : this.g.c();
            }
        }
        return bau;
    }
}
