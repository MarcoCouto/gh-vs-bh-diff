package com.google.android.gms.internal.ads;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;

public final class aqg extends ajk implements aqe {
    aqg(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.client.IAppEventListener");
    }

    public final void a(String str, String str2) throws RemoteException {
        Parcel r_ = r_();
        r_.writeString(str);
        r_.writeString(str2);
        b(1, r_);
    }
}
