package com.google.android.gms.internal.ads;

import java.util.concurrent.Callable;

final class ash implements Callable<T> {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ ary f2873a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ asg f2874b;

    ash(asg asg, ary ary) {
        this.f2874b = asg;
        this.f2873a = ary;
    }

    public final T call() {
        return this.f2873a.a(this.f2874b.d);
    }
}
