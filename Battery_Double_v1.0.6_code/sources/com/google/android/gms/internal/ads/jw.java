package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import java.util.List;

final class jw implements ath {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ List f3442a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ atg f3443b;
    private final /* synthetic */ Context c;

    jw(jv jvVar, List list, atg atg, Context context) {
        this.f3442a = list;
        this.f3443b = atg;
        this.c = context;
    }

    public final void a() {
        for (String str : this.f3442a) {
            String str2 = "Pinging url: ";
            String valueOf = String.valueOf(str);
            jm.d(valueOf.length() != 0 ? str2.concat(valueOf) : new String(str2));
            this.f3443b.a(Uri.parse(str), null, null);
        }
        this.f3443b.a((Activity) this.c);
    }

    public final void b() {
    }
}
