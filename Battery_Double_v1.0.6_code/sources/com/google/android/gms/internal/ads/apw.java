package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.b.a;

public abstract class apw extends ajl implements apv {
    public apw() {
        super("com.google.android.gms.ads.internal.client.IAdManager");
    }

    public static apv a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.IAdManager");
        return queryLocalInterface instanceof apv ? (apv) queryLocalInterface : new apx(iBinder);
    }

    /* JADX WARNING: type inference failed for: r0v0 */
    /* JADX WARNING: type inference failed for: r0v4, types: [com.google.android.gms.internal.ads.aqc] */
    /* JADX WARNING: type inference failed for: r0v5, types: [com.google.android.gms.internal.ads.aqa] */
    /* JADX WARNING: type inference failed for: r0v6, types: [com.google.android.gms.internal.ads.aqa] */
    /* JADX WARNING: type inference failed for: r0v26, types: [com.google.android.gms.internal.ads.aqm] */
    /* JADX WARNING: type inference failed for: r0v27, types: [com.google.android.gms.internal.ads.aqk] */
    /* JADX WARNING: type inference failed for: r0v28, types: [com.google.android.gms.internal.ads.aqk] */
    /* JADX WARNING: type inference failed for: r0v31, types: [com.google.android.gms.internal.ads.apj] */
    /* JADX WARNING: type inference failed for: r0v32, types: [com.google.android.gms.internal.ads.aph] */
    /* JADX WARNING: type inference failed for: r0v33, types: [com.google.android.gms.internal.ads.aph] */
    /* JADX WARNING: type inference failed for: r0v47, types: [com.google.android.gms.internal.ads.aqg] */
    /* JADX WARNING: type inference failed for: r0v48, types: [com.google.android.gms.internal.ads.aqe] */
    /* JADX WARNING: type inference failed for: r0v49, types: [com.google.android.gms.internal.ads.aqe] */
    /* JADX WARNING: type inference failed for: r0v52, types: [com.google.android.gms.internal.ads.apm] */
    /* JADX WARNING: type inference failed for: r0v53, types: [com.google.android.gms.internal.ads.apk] */
    /* JADX WARNING: type inference failed for: r0v54, types: [com.google.android.gms.internal.ads.apk] */
    /* JADX WARNING: type inference failed for: r0v62 */
    /* JADX WARNING: type inference failed for: r0v63 */
    /* JADX WARNING: type inference failed for: r0v64 */
    /* JADX WARNING: type inference failed for: r0v65 */
    /* JADX WARNING: type inference failed for: r0v66 */
    /* JADX WARNING: type inference failed for: r0v67 */
    /* JADX WARNING: type inference failed for: r0v68 */
    /* JADX WARNING: type inference failed for: r0v69 */
    /* JADX WARNING: type inference failed for: r0v70 */
    /* JADX WARNING: type inference failed for: r0v71 */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r0v0
  assigns: [?[int, float, boolean, short, byte, char, OBJECT, ARRAY], com.google.android.gms.internal.ads.aqm, com.google.android.gms.internal.ads.aqc, com.google.android.gms.internal.ads.aqa, com.google.android.gms.internal.ads.aqk, com.google.android.gms.internal.ads.apj, com.google.android.gms.internal.ads.aph, com.google.android.gms.internal.ads.aqg, com.google.android.gms.internal.ads.aqe, com.google.android.gms.internal.ads.apm, com.google.android.gms.internal.ads.apk]
  uses: [com.google.android.gms.internal.ads.aqa, com.google.android.gms.internal.ads.aqk, com.google.android.gms.internal.ads.aph, com.google.android.gms.internal.ads.aqe, com.google.android.gms.internal.ads.apk]
  mth insns count: 156
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
    	at jadx.core.ProcessClass.process(ProcessClass.java:35)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Unknown variable types count: 11 */
    public final boolean dispatchTransaction(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        ? r0 = 0;
        switch (i) {
            case 1:
                a k = k();
                parcel2.writeNoException();
                ajm.a(parcel2, (IInterface) k);
                break;
            case 2:
                j();
                parcel2.writeNoException();
                break;
            case 3:
                boolean m = m();
                parcel2.writeNoException();
                ajm.a(parcel2, m);
                break;
            case 4:
                boolean b2 = b((aop) ajm.a(parcel, aop.CREATOR));
                parcel2.writeNoException();
                ajm.a(parcel2, b2);
                break;
            case 5:
                o();
                parcel2.writeNoException();
                break;
            case 6:
                p();
                parcel2.writeNoException();
                break;
            case 7:
                IBinder readStrongBinder = parcel.readStrongBinder();
                if (readStrongBinder != null) {
                    IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.IAdListener");
                    r0 = queryLocalInterface instanceof apk ? (apk) queryLocalInterface : new apm(readStrongBinder);
                }
                a((apk) r0);
                parcel2.writeNoException();
                break;
            case 8:
                IBinder readStrongBinder2 = parcel.readStrongBinder();
                if (readStrongBinder2 != null) {
                    IInterface queryLocalInterface2 = readStrongBinder2.queryLocalInterface("com.google.android.gms.ads.internal.client.IAppEventListener");
                    r0 = queryLocalInterface2 instanceof aqe ? (aqe) queryLocalInterface2 : new aqg(readStrongBinder2);
                }
                a((aqe) r0);
                parcel2.writeNoException();
                break;
            case 9:
                I();
                parcel2.writeNoException();
                break;
            case 10:
                r();
                parcel2.writeNoException();
                break;
            case 11:
                n();
                parcel2.writeNoException();
                break;
            case 12:
                aot l = l();
                parcel2.writeNoException();
                ajm.b(parcel2, l);
                break;
            case 13:
                a((aot) ajm.a(parcel, aot.CREATOR));
                parcel2.writeNoException();
                break;
            case 14:
                a(z.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
            case 15:
                a(ag.a(parcel.readStrongBinder()), parcel.readString());
                parcel2.writeNoException();
                break;
            case 18:
                String a2 = a();
                parcel2.writeNoException();
                parcel2.writeString(a2);
                break;
            case 19:
                a(atd.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
            case 20:
                IBinder readStrongBinder3 = parcel.readStrongBinder();
                if (readStrongBinder3 != null) {
                    IInterface queryLocalInterface3 = readStrongBinder3.queryLocalInterface("com.google.android.gms.ads.internal.client.IAdClickListener");
                    r0 = queryLocalInterface3 instanceof aph ? (aph) queryLocalInterface3 : new apj(readStrongBinder3);
                }
                a((aph) r0);
                parcel2.writeNoException();
                break;
            case 21:
                IBinder readStrongBinder4 = parcel.readStrongBinder();
                if (readStrongBinder4 != null) {
                    IInterface queryLocalInterface4 = readStrongBinder4.queryLocalInterface("com.google.android.gms.ads.internal.client.ICorrelationIdProvider");
                    r0 = queryLocalInterface4 instanceof aqk ? (aqk) queryLocalInterface4 : new aqm(readStrongBinder4);
                }
                a((aqk) r0);
                parcel2.writeNoException();
                break;
            case 22:
                b(ajm.a(parcel));
                parcel2.writeNoException();
                break;
            case 23:
                boolean s = s();
                parcel2.writeNoException();
                ajm.a(parcel2, s);
                break;
            case 24:
                a(go.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
            case 25:
                a(parcel.readString());
                parcel2.writeNoException();
                break;
            case 26:
                aqs t = t();
                parcel2.writeNoException();
                ajm.a(parcel2, (IInterface) t);
                break;
            case 29:
                a((arr) ajm.a(parcel, arr.CREATOR));
                parcel2.writeNoException();
                break;
            case 30:
                a((aqy) ajm.a(parcel, aqy.CREATOR));
                parcel2.writeNoException();
                break;
            case 31:
                String D = D();
                parcel2.writeNoException();
                parcel2.writeString(D);
                break;
            case 32:
                aqe E = E();
                parcel2.writeNoException();
                ajm.a(parcel2, (IInterface) E);
                break;
            case 33:
                apk F = F();
                parcel2.writeNoException();
                ajm.a(parcel2, (IInterface) F);
                break;
            case 34:
                c(ajm.a(parcel));
                parcel2.writeNoException();
                break;
            case 35:
                String q_ = q_();
                parcel2.writeNoException();
                parcel2.writeString(q_);
                break;
            case 36:
                IBinder readStrongBinder5 = parcel.readStrongBinder();
                if (readStrongBinder5 != null) {
                    IInterface queryLocalInterface5 = readStrongBinder5.queryLocalInterface("com.google.android.gms.ads.internal.client.IAdMetadataListener");
                    r0 = queryLocalInterface5 instanceof aqa ? (aqa) queryLocalInterface5 : new aqc(readStrongBinder5);
                }
                a((aqa) r0);
                parcel2.writeNoException();
                break;
            case 37:
                Bundle q = q();
                parcel2.writeNoException();
                ajm.b(parcel2, q);
                break;
            default:
                return false;
        }
        return true;
    }
}
