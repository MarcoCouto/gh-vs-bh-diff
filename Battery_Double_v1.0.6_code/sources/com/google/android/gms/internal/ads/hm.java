package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.b.a.C0046a;

public abstract class hm extends ajl implements hl {
    public hm() {
        super("com.google.android.gms.ads.internal.reward.mediation.client.IMediationRewardedVideoAdListener");
    }

    public static hl a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.ads.internal.reward.mediation.client.IMediationRewardedVideoAdListener");
        return queryLocalInterface instanceof hl ? (hl) queryLocalInterface : new hn(iBinder);
    }

    /* access modifiers changed from: protected */
    public final boolean dispatchTransaction(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        switch (i) {
            case 1:
                a(C0046a.a(parcel.readStrongBinder()));
                break;
            case 2:
                a(C0046a.a(parcel.readStrongBinder()), parcel.readInt());
                break;
            case 3:
                b(C0046a.a(parcel.readStrongBinder()));
                break;
            case 4:
                c(C0046a.a(parcel.readStrongBinder()));
                break;
            case 5:
                d(C0046a.a(parcel.readStrongBinder()));
                break;
            case 6:
                e(C0046a.a(parcel.readStrongBinder()));
                break;
            case 7:
                a(C0046a.a(parcel.readStrongBinder()), (hp) ajm.a(parcel, hp.CREATOR));
                break;
            case 8:
                f(C0046a.a(parcel.readStrongBinder()));
                break;
            case 9:
                b(C0046a.a(parcel.readStrongBinder()), parcel.readInt());
                break;
            case 10:
                g(C0046a.a(parcel.readStrongBinder()));
                break;
            case 11:
                h(C0046a.a(parcel.readStrongBinder()));
                break;
            case 12:
                a((Bundle) ajm.a(parcel, Bundle.CREATOR));
                break;
            default:
                return false;
        }
        parcel2.writeNoException();
        return true;
    }
}
