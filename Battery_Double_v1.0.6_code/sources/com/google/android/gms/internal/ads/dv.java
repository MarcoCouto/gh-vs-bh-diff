package com.google.android.gms.internal.ads;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;

public final class dv extends ajk implements dt {
    dv(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.request.IAdRequestService");
    }

    public final dp a(dl dlVar) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (Parcelable) dlVar);
        Parcel a2 = a(1, r_);
        dp dpVar = (dp) ajm.a(a2, dp.CREATOR);
        a2.recycle();
        return dpVar;
    }

    public final void a(dl dlVar, dw dwVar) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (Parcelable) dlVar);
        ajm.a(r_, (IInterface) dwVar);
        b(2, r_);
    }

    public final void a(ee eeVar, dz dzVar) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (Parcelable) eeVar);
        ajm.a(r_, (IInterface) dzVar);
        b(4, r_);
    }

    public final void b(ee eeVar, dz dzVar) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (Parcelable) eeVar);
        ajm.a(r_, (IInterface) dzVar);
        b(5, r_);
    }
}
