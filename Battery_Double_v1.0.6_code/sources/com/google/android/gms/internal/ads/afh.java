package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.afh;
import java.io.IOException;

public abstract class afh<M extends afh<M>> extends afn {
    protected afj Y;

    /* access modifiers changed from: protected */
    public int a() {
        if (this.Y == null) {
            return 0;
        }
        int i = 0;
        for (int i2 = 0; i2 < this.Y.a(); i2++) {
            i += this.Y.b(i2).a();
        }
        return i;
    }

    public void a(aff aff) throws IOException {
        if (this.Y != null) {
            for (int i = 0; i < this.Y.a(); i++) {
                this.Y.b(i).a(aff);
            }
        }
    }

    /* access modifiers changed from: protected */
    public final boolean a(afd afd, int i) throws IOException {
        int j = afd.j();
        if (!afd.b(i)) {
            return false;
        }
        int i2 = i >>> 3;
        afp afp = new afp(i, afd.a(j, afd.j() - j));
        afk afk = null;
        if (this.Y == null) {
            this.Y = new afj();
        } else {
            afk = this.Y.a(i2);
        }
        if (afk == null) {
            afk = new afk();
            this.Y.a(i2, afk);
        }
        afk.a(afp);
        return true;
    }

    public final /* synthetic */ afn c() throws CloneNotSupportedException {
        return (afh) clone();
    }

    public /* synthetic */ Object clone() throws CloneNotSupportedException {
        afh afh = (afh) super.clone();
        afl.a(this, afh);
        return afh;
    }
}
