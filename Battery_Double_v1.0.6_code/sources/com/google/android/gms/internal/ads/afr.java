package com.google.android.gms.internal.ads;

import java.io.IOException;

public final class afr extends afh<afr> {

    /* renamed from: a reason: collision with root package name */
    public Integer f2536a;

    /* renamed from: b reason: collision with root package name */
    public String f2537b;
    public String c;
    public afs d;
    public afz[] e;
    public String f;
    public afy g;
    public aga h;
    public String[] i;
    public String[] j;
    private Integer k;
    private String l;
    private Boolean m;
    private String[] n;
    private String o;
    private Boolean p;
    private Boolean q;
    private byte[] r;

    public afr() {
        this.f2536a = null;
        this.k = null;
        this.f2537b = null;
        this.c = null;
        this.l = null;
        this.d = null;
        this.e = afz.b();
        this.f = null;
        this.g = null;
        this.m = null;
        this.n = afq.c;
        this.o = null;
        this.p = null;
        this.q = null;
        this.r = null;
        this.h = null;
        this.i = afq.c;
        this.j = afq.c;
        this.Y = null;
        this.Z = -1;
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public final afr a(afd afd) throws IOException {
        while (true) {
            int a2 = afd.a();
            switch (a2) {
                case 0:
                    break;
                case 10:
                    this.f2537b = afd.e();
                    continue;
                case 18:
                    this.c = afd.e();
                    continue;
                case 26:
                    this.l = afd.e();
                    continue;
                case 34:
                    int a3 = afq.a(afd, 34);
                    int length = this.e == null ? 0 : this.e.length;
                    afz[] afzArr = new afz[(a3 + length)];
                    if (length != 0) {
                        System.arraycopy(this.e, 0, afzArr, 0, length);
                    }
                    while (length < afzArr.length - 1) {
                        afzArr[length] = new afz();
                        afd.a((afn) afzArr[length]);
                        afd.a();
                        length++;
                    }
                    afzArr[length] = new afz();
                    afd.a((afn) afzArr[length]);
                    this.e = afzArr;
                    continue;
                case 40:
                    this.m = Boolean.valueOf(afd.d());
                    continue;
                case 50:
                    int a4 = afq.a(afd, 50);
                    int length2 = this.n == null ? 0 : this.n.length;
                    String[] strArr = new String[(a4 + length2)];
                    if (length2 != 0) {
                        System.arraycopy(this.n, 0, strArr, 0, length2);
                    }
                    while (length2 < strArr.length - 1) {
                        strArr[length2] = afd.e();
                        afd.a();
                        length2++;
                    }
                    strArr[length2] = afd.e();
                    this.n = strArr;
                    continue;
                case 58:
                    this.o = afd.e();
                    continue;
                case 64:
                    this.p = Boolean.valueOf(afd.d());
                    continue;
                case 72:
                    this.q = Boolean.valueOf(afd.d());
                    continue;
                case 80:
                    int j2 = afd.j();
                    try {
                        int c2 = afd.c();
                        if (c2 < 0 || c2 > 9) {
                            throw new IllegalArgumentException(c2 + " is not a valid enum ReportType");
                        }
                        this.f2536a = Integer.valueOf(c2);
                        continue;
                    } catch (IllegalArgumentException e2) {
                        afd.e(j2);
                        a(afd, a2);
                        break;
                    }
                case 88:
                    int j3 = afd.j();
                    try {
                        int c3 = afd.c();
                        if (c3 < 0 || c3 > 4) {
                            throw new IllegalArgumentException(c3 + " is not a valid enum Verdict");
                        }
                        this.k = Integer.valueOf(c3);
                        continue;
                    } catch (IllegalArgumentException e3) {
                        afd.e(j3);
                        a(afd, a2);
                        break;
                    }
                case 98:
                    if (this.d == null) {
                        this.d = new afs();
                    }
                    afd.a((afn) this.d);
                    continue;
                case 106:
                    this.f = afd.e();
                    continue;
                case 114:
                    if (this.g == null) {
                        this.g = new afy();
                    }
                    afd.a((afn) this.g);
                    continue;
                case 122:
                    this.r = afd.f();
                    continue;
                case 138:
                    if (this.h == null) {
                        this.h = new aga();
                    }
                    afd.a((afn) this.h);
                    continue;
                case 162:
                    int a5 = afq.a(afd, 162);
                    int length3 = this.i == null ? 0 : this.i.length;
                    String[] strArr2 = new String[(a5 + length3)];
                    if (length3 != 0) {
                        System.arraycopy(this.i, 0, strArr2, 0, length3);
                    }
                    while (length3 < strArr2.length - 1) {
                        strArr2[length3] = afd.e();
                        afd.a();
                        length3++;
                    }
                    strArr2[length3] = afd.e();
                    this.i = strArr2;
                    continue;
                case 170:
                    int a6 = afq.a(afd, 170);
                    int length4 = this.j == null ? 0 : this.j.length;
                    String[] strArr3 = new String[(a6 + length4)];
                    if (length4 != 0) {
                        System.arraycopy(this.j, 0, strArr3, 0, length4);
                    }
                    while (length4 < strArr3.length - 1) {
                        strArr3[length4] = afd.e();
                        afd.a();
                        length4++;
                    }
                    strArr3[length4] = afd.e();
                    this.j = strArr3;
                    continue;
                default:
                    if (!super.a(afd, a2)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    /* access modifiers changed from: protected */
    public final int a() {
        int a2 = super.a();
        if (this.f2537b != null) {
            a2 += aff.b(1, this.f2537b);
        }
        if (this.c != null) {
            a2 += aff.b(2, this.c);
        }
        if (this.l != null) {
            a2 += aff.b(3, this.l);
        }
        if (this.e != null && this.e.length > 0) {
            int i2 = a2;
            for (afz afz : this.e) {
                if (afz != null) {
                    i2 += aff.b(4, (afn) afz);
                }
            }
            a2 = i2;
        }
        if (this.m != null) {
            this.m.booleanValue();
            a2 += aff.b(5) + 1;
        }
        if (this.n != null && this.n.length > 0) {
            int i3 = 0;
            int i4 = 0;
            for (String str : this.n) {
                if (str != null) {
                    i4++;
                    i3 += aff.a(str);
                }
            }
            a2 = a2 + i3 + (i4 * 1);
        }
        if (this.o != null) {
            a2 += aff.b(7, this.o);
        }
        if (this.p != null) {
            this.p.booleanValue();
            a2 += aff.b(8) + 1;
        }
        if (this.q != null) {
            this.q.booleanValue();
            a2 += aff.b(9) + 1;
        }
        if (this.f2536a != null) {
            a2 += aff.b(10, this.f2536a.intValue());
        }
        if (this.k != null) {
            a2 += aff.b(11, this.k.intValue());
        }
        if (this.d != null) {
            a2 += aff.b(12, (afn) this.d);
        }
        if (this.f != null) {
            a2 += aff.b(13, this.f);
        }
        if (this.g != null) {
            a2 += aff.b(14, (afn) this.g);
        }
        if (this.r != null) {
            a2 += aff.b(15, this.r);
        }
        if (this.h != null) {
            a2 += aff.b(17, (afn) this.h);
        }
        if (this.i != null && this.i.length > 0) {
            int i5 = 0;
            int i6 = 0;
            for (String str2 : this.i) {
                if (str2 != null) {
                    i6++;
                    i5 += aff.a(str2);
                }
            }
            a2 = a2 + i5 + (i6 * 2);
        }
        if (this.j == null || this.j.length <= 0) {
            return a2;
        }
        int i7 = 0;
        int i8 = 0;
        for (String str3 : this.j) {
            if (str3 != null) {
                i8++;
                i7 += aff.a(str3);
            }
        }
        return a2 + i7 + (i8 * 2);
    }

    public final void a(aff aff) throws IOException {
        if (this.f2537b != null) {
            aff.a(1, this.f2537b);
        }
        if (this.c != null) {
            aff.a(2, this.c);
        }
        if (this.l != null) {
            aff.a(3, this.l);
        }
        if (this.e != null && this.e.length > 0) {
            for (afz afz : this.e) {
                if (afz != null) {
                    aff.a(4, (afn) afz);
                }
            }
        }
        if (this.m != null) {
            aff.a(5, this.m.booleanValue());
        }
        if (this.n != null && this.n.length > 0) {
            for (String str : this.n) {
                if (str != null) {
                    aff.a(6, str);
                }
            }
        }
        if (this.o != null) {
            aff.a(7, this.o);
        }
        if (this.p != null) {
            aff.a(8, this.p.booleanValue());
        }
        if (this.q != null) {
            aff.a(9, this.q.booleanValue());
        }
        if (this.f2536a != null) {
            aff.a(10, this.f2536a.intValue());
        }
        if (this.k != null) {
            aff.a(11, this.k.intValue());
        }
        if (this.d != null) {
            aff.a(12, (afn) this.d);
        }
        if (this.f != null) {
            aff.a(13, this.f);
        }
        if (this.g != null) {
            aff.a(14, (afn) this.g);
        }
        if (this.r != null) {
            aff.a(15, this.r);
        }
        if (this.h != null) {
            aff.a(17, (afn) this.h);
        }
        if (this.i != null && this.i.length > 0) {
            for (String str2 : this.i) {
                if (str2 != null) {
                    aff.a(20, str2);
                }
            }
        }
        if (this.j != null && this.j.length > 0) {
            for (String str3 : this.j) {
                if (str3 != null) {
                    aff.a(21, str3);
                }
            }
        }
        super.a(aff);
    }
}
