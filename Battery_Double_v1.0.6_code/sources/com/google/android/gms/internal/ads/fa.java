package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.ads.internal.gmsg.ae;

@cm
public final class fa {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public final Context f3303a;
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public final Object f3304b = new Object();
    /* access modifiers changed from: private */
    public String c;
    /* access modifiers changed from: private */
    public String d;
    /* access modifiers changed from: private */
    public ny<fg> e = new ny<>();
    private final ae<Object> f = new fb(this);
    private final ae<Object> g = new fc(this);
    private final ae<Object> h = new fd(this);

    public fa(Context context, String str, String str2) {
        this.f3303a = context;
        this.d = str2;
        this.c = str;
    }
}
