package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.xe.b;
import java.security.GeneralSecurityException;

final class um implements tz<zg> {
    um() {
    }

    private static void a(vq vqVar) throws GeneralSecurityException {
        if (vqVar.a() < 12 || vqVar.a() > 16) {
            throw new GeneralSecurityException("invalid IV size");
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: d */
    public final ye a(aah aah) throws GeneralSecurityException {
        try {
            vm a2 = vm.a(aah);
            if (!(a2 instanceof vm)) {
                throw new GeneralSecurityException("expected AesCtrKey proto");
            }
            vm vmVar = a2;
            zo.a(vmVar.a(), 0);
            zo.a(vmVar.c().a());
            a(vmVar.b());
            return new ye(vmVar.c().b(), vmVar.b().a());
        } catch (abv e) {
            throw new GeneralSecurityException("expected serialized AesCtrKey proto", e);
        }
    }

    public final int a() {
        return 0;
    }

    public final /* synthetic */ Object a(acw acw) throws GeneralSecurityException {
        if (!(acw instanceof vm)) {
            throw new GeneralSecurityException("expected AesCtrKey proto");
        }
        vm vmVar = (vm) acw;
        zo.a(vmVar.a(), 0);
        zo.a(vmVar.c().a());
        a(vmVar.b());
        return new ye(vmVar.c().b(), vmVar.b().a());
    }

    public final acw b(aah aah) throws GeneralSecurityException {
        try {
            return b((acw) vo.a(aah));
        } catch (abv e) {
            throw new GeneralSecurityException("expected serialized AesCtrKeyFormat proto", e);
        }
    }

    public final acw b(acw acw) throws GeneralSecurityException {
        if (!(acw instanceof vo)) {
            throw new GeneralSecurityException("expected AesCtrKeyFormat proto");
        }
        vo voVar = (vo) acw;
        zo.a(voVar.b());
        a(voVar.a());
        return vm.d().a(voVar.a()).a(aah.a(zj.a(voVar.b()))).a(0).c();
    }

    public final xe c(aah aah) throws GeneralSecurityException {
        return (xe) xe.d().a("type.googleapis.com/google.crypto.tink.AesCtrKey").a(((vm) b(aah)).h()).a(b.SYMMETRIC).c();
    }
}
