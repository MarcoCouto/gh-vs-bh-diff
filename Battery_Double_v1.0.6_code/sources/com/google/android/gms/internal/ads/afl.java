package com.google.android.gms.internal.ads;

import java.nio.charset.Charset;

public final class afl {

    /* renamed from: a reason: collision with root package name */
    protected static final Charset f2530a = Charset.forName("UTF-8");

    /* renamed from: b reason: collision with root package name */
    public static final Object f2531b = new Object();
    private static final Charset c = Charset.forName("ISO-8859-1");

    public static void a(afh afh, afh afh2) {
        if (afh.Y != null) {
            afh2.Y = (afj) afh.Y.clone();
        }
    }
}
