package com.google.android.gms.internal.ads;

public enum aew {
    DOUBLE(afb.DOUBLE, 1),
    FLOAT(afb.FLOAT, 5),
    INT64(afb.LONG, 0),
    UINT64(afb.LONG, 0),
    INT32(afb.INT, 0),
    FIXED64(afb.LONG, 1),
    FIXED32(afb.INT, 5),
    BOOL(afb.BOOLEAN, 0),
    STRING(afb.STRING, 2),
    GROUP(afb.MESSAGE, 3),
    MESSAGE(afb.MESSAGE, 2),
    BYTES(afb.BYTE_STRING, 2),
    UINT32(afb.INT, 0),
    ENUM(afb.ENUM, 0),
    SFIXED32(afb.INT, 5),
    SFIXED64(afb.LONG, 1),
    SINT32(afb.INT, 0),
    SINT64(afb.LONG, 0);
    
    private final afb s;
    private final int t;

    private aew(afb afb, int i) {
        this.s = afb;
        this.t = i;
    }

    public final afb a() {
        return this.s;
    }

    public final int b() {
        return this.t;
    }
}
