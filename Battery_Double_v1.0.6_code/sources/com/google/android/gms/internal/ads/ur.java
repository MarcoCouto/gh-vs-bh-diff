package com.google.android.gms.internal.ads;

import java.nio.ByteBuffer;
import java.security.GeneralSecurityException;

public final class ur implements tr {

    /* renamed from: a reason: collision with root package name */
    private static final byte[] f3727a = new byte[0];

    /* renamed from: b reason: collision with root package name */
    private final xj f3728b;
    private final tr c;

    public ur(xj xjVar, tr trVar) {
        this.f3728b = xjVar;
        this.c = trVar;
    }

    public final byte[] a(byte[] bArr, byte[] bArr2) throws GeneralSecurityException {
        byte[] i = uh.b(this.f3728b).i();
        byte[] a2 = this.c.a(i, f3727a);
        byte[] a3 = ((tr) uh.a(this.f3728b.a(), i)).a(bArr, bArr2);
        return ByteBuffer.allocate(a2.length + 4 + a3.length).putInt(a2.length).put(a2).put(a3).array();
    }
}
