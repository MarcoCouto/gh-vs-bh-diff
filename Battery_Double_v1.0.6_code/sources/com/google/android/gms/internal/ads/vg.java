package com.google.android.gms.internal.ads;

import java.security.GeneralSecurityException;

final class vg implements tt<ud> {
    public final tz<ud> a(String str, String str2, int i) throws GeneralSecurityException {
        boolean z;
        char c = 65535;
        String lowerCase = str2.toLowerCase();
        switch (lowerCase.hashCode()) {
            case 107855:
                if (lowerCase.equals("mac")) {
                    z = false;
                    break;
                }
            default:
                z = true;
                break;
        }
        switch (z) {
            case false:
                switch (str.hashCode()) {
                    case 836622442:
                        if (str.equals("type.googleapis.com/google.crypto.tink.HmacKey")) {
                            c = 0;
                            break;
                        }
                        break;
                }
                switch (c) {
                    case 0:
                        ve veVar = new ve();
                        if (i <= 0) {
                            return veVar;
                        }
                        throw new GeneralSecurityException(String.format("No key manager for key type '%s' with version at least %d.", new Object[]{str, Integer.valueOf(i)}));
                    default:
                        throw new GeneralSecurityException(String.format("No support for primitive 'Mac' with key type '%s'.", new Object[]{str}));
                }
            default:
                throw new GeneralSecurityException(String.format("No support for primitive '%s'.", new Object[]{str2}));
        }
    }
}
