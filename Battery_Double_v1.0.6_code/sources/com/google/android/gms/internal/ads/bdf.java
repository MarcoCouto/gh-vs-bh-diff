package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.b.a;
import com.google.android.gms.b.a.C0046a;
import java.util.List;

public abstract class bdf extends ajl implements bdd {
    public bdf() {
        super("com.google.android.gms.ads.internal.mediation.client.INativeAppInstallAdMapper");
    }

    /* access modifiers changed from: protected */
    public final boolean dispatchTransaction(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        switch (i) {
            case 2:
                String a2 = a();
                parcel2.writeNoException();
                parcel2.writeString(a2);
                break;
            case 3:
                List b2 = b();
                parcel2.writeNoException();
                parcel2.writeList(b2);
                break;
            case 4:
                String c = c();
                parcel2.writeNoException();
                parcel2.writeString(c);
                break;
            case 5:
                auw d = d();
                parcel2.writeNoException();
                ajm.a(parcel2, (IInterface) d);
                break;
            case 6:
                String e = e();
                parcel2.writeNoException();
                parcel2.writeString(e);
                break;
            case 7:
                double f = f();
                parcel2.writeNoException();
                parcel2.writeDouble(f);
                break;
            case 8:
                String g = g();
                parcel2.writeNoException();
                parcel2.writeString(g);
                break;
            case 9:
                String h = h();
                parcel2.writeNoException();
                parcel2.writeString(h);
                break;
            case 10:
                i();
                parcel2.writeNoException();
                break;
            case 11:
                a(C0046a.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
            case 12:
                b(C0046a.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
            case 13:
                boolean j = j();
                parcel2.writeNoException();
                ajm.a(parcel2, j);
                break;
            case 14:
                boolean k = k();
                parcel2.writeNoException();
                ajm.a(parcel2, k);
                break;
            case 15:
                Bundle l = l();
                parcel2.writeNoException();
                ajm.b(parcel2, l);
                break;
            case 16:
                c(C0046a.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
            case 17:
                aqs m = m();
                parcel2.writeNoException();
                ajm.a(parcel2, (IInterface) m);
                break;
            case 18:
                a n = n();
                parcel2.writeNoException();
                ajm.a(parcel2, (IInterface) n);
                break;
            case 19:
                aus o = o();
                parcel2.writeNoException();
                ajm.a(parcel2, (IInterface) o);
                break;
            case 20:
                a p = p();
                parcel2.writeNoException();
                ajm.a(parcel2, (IInterface) p);
                break;
            case 21:
                a q = q();
                parcel2.writeNoException();
                ajm.a(parcel2, (IInterface) q);
                break;
            case 22:
                a(C0046a.a(parcel.readStrongBinder()), C0046a.a(parcel.readStrongBinder()), C0046a.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
            default:
                return false;
        }
        return true;
    }
}
