package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.abp.b;
import com.google.android.gms.internal.ads.abp.e;

public final class xl extends abp<xl, a> implements acy {
    private static volatile adi<xl> zzakh;
    /* access modifiers changed from: private */
    public static final xl zzdlp = new xl();
    private String zzdks = "";
    private String zzdll = "";
    private int zzdlm;
    private boolean zzdln;
    private String zzdlo = "";

    public static final class a extends com.google.android.gms.internal.ads.abp.a<xl, a> implements acy {
        private a() {
            super(xl.zzdlp);
        }

        /* synthetic */ a(xm xmVar) {
            this();
        }

        public final a a(int i) {
            b();
            ((xl) this.f2433a).b(0);
            return this;
        }

        public final a a(String str) {
            b();
            ((xl) this.f2433a).a(str);
            return this;
        }

        public final a a(boolean z) {
            b();
            ((xl) this.f2433a).a(true);
            return this;
        }

        public final a b(String str) {
            b();
            ((xl) this.f2433a).b(str);
            return this;
        }

        public final a c(String str) {
            b();
            ((xl) this.f2433a).c(str);
            return this;
        }
    }

    static {
        abp.a(xl.class, zzdlp);
    }

    private xl() {
    }

    /* access modifiers changed from: private */
    public final void a(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.zzdll = str;
    }

    /* access modifiers changed from: private */
    public final void a(boolean z) {
        this.zzdln = z;
    }

    /* access modifiers changed from: private */
    public final void b(int i) {
        this.zzdlm = i;
    }

    /* access modifiers changed from: private */
    public final void b(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.zzdks = str;
    }

    /* access modifiers changed from: private */
    public final void c(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.zzdlo = str;
    }

    public static a f() {
        return (a) ((com.google.android.gms.internal.ads.abp.a) zzdlp.a(e.e, (Object) null, (Object) null));
    }

    /* JADX WARNING: type inference failed for: r0v6, types: [com.google.android.gms.internal.ads.adi<com.google.android.gms.internal.ads.xl>] */
    /* JADX WARNING: type inference failed for: r0v7, types: [java.lang.Object] */
    /* JADX WARNING: type inference failed for: r0v8, types: [com.google.android.gms.internal.ads.adi<com.google.android.gms.internal.ads.xl>, com.google.android.gms.internal.ads.abp$b] */
    /* JADX WARNING: type inference failed for: r0v16 */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r0v8, types: [com.google.android.gms.internal.ads.adi<com.google.android.gms.internal.ads.xl>, com.google.android.gms.internal.ads.abp$b]
  assigns: [com.google.android.gms.internal.ads.abp$b]
  uses: [com.google.android.gms.internal.ads.adi<com.google.android.gms.internal.ads.xl>]
  mth insns count: 46
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
    	at jadx.core.ProcessClass.process(ProcessClass.java:35)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Unknown variable types count: 2 */
    public final Object a(int i, Object obj, Object obj2) {
        ? r0;
        switch (xm.f3776a[i - 1]) {
            case 1:
                return new xl();
            case 2:
                return new a(null);
            case 3:
                Object[] objArr = {"zzdll", "zzdks", "zzdlm", "zzdln", "zzdlo"};
                return a((acw) zzdlp, "\u0000\u0005\u0000\u0000\u0001\u0005\u0005\u0006\u0000\u0000\u0000\u0001Ȉ\u0002Ȉ\u0003\u000b\u0004\u0007\u0005Ȉ", objArr);
            case 4:
                return zzdlp;
            case 5:
                adi<xl> adi = zzakh;
                if (adi != null) {
                    return adi;
                }
                synchronized (xl.class) {
                    r0 = zzakh;
                    if (r0 == 0) {
                        ? bVar = new b(zzdlp);
                        zzakh = bVar;
                        r0 = bVar;
                    }
                }
                return r0;
            case 6:
                return Byte.valueOf(1);
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    public final String a() {
        return this.zzdll;
    }

    public final String b() {
        return this.zzdks;
    }

    public final int c() {
        return this.zzdlm;
    }

    public final boolean d() {
        return this.zzdln;
    }

    public final String e() {
        return this.zzdlo;
    }
}
