package com.google.android.gms.internal.ads;

import android.content.Context;
import java.util.WeakHashMap;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

@cm
public final class fk {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public WeakHashMap<Context, fm> f3318a = new WeakHashMap<>();

    public final Future<fi> a(Context context) {
        return jt.a((Callable<T>) new fl<T>(this, context));
    }
}
