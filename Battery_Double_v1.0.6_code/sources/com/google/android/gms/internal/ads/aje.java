package com.google.android.gms.internal.ads;

import java.lang.reflect.InvocationTargetException;

public final class aje extends ajj {
    private static volatile Long d = null;
    private static final Object e = new Object();

    public aje(ahy ahy, String str, String str2, zz zzVar, int i, int i2) {
        super(ahy, str, str2, zzVar, i, 33);
    }

    /* access modifiers changed from: protected */
    public final void a() throws IllegalAccessException, InvocationTargetException {
        if (d == null) {
            synchronized (e) {
                if (d == null) {
                    d = (Long) this.c.invoke(null, new Object[0]);
                }
            }
        }
        synchronized (this.f2637b) {
            this.f2637b.r = d;
        }
    }
}
