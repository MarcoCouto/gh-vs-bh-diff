package com.google.android.gms.internal.ads;

import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.b.a.C0046a;

public abstract class s extends ajl implements r {
    public s() {
        super("com.google.android.gms.ads.internal.overlay.client.IAdOverlay");
    }

    public static r a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.ads.internal.overlay.client.IAdOverlay");
        return queryLocalInterface instanceof r ? (r) queryLocalInterface : new t(iBinder);
    }

    /* access modifiers changed from: protected */
    public final boolean dispatchTransaction(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        switch (i) {
            case 1:
                a((Bundle) ajm.a(parcel, Bundle.CREATOR));
                parcel2.writeNoException();
                break;
            case 2:
                f();
                parcel2.writeNoException();
                break;
            case 3:
                g();
                parcel2.writeNoException();
                break;
            case 4:
                h();
                parcel2.writeNoException();
                break;
            case 5:
                i();
                parcel2.writeNoException();
                break;
            case 6:
                Bundle bundle = (Bundle) ajm.a(parcel, Bundle.CREATOR);
                b(bundle);
                parcel2.writeNoException();
                ajm.b(parcel2, bundle);
                break;
            case 7:
                j();
                parcel2.writeNoException();
                break;
            case 8:
                k();
                parcel2.writeNoException();
                break;
            case 9:
                l();
                parcel2.writeNoException();
                break;
            case 10:
                d();
                parcel2.writeNoException();
                break;
            case 11:
                boolean e = e();
                parcel2.writeNoException();
                ajm.a(parcel2, e);
                break;
            case 12:
                a(parcel.readInt(), parcel.readInt(), (Intent) ajm.a(parcel, Intent.CREATOR));
                parcel2.writeNoException();
                break;
            case 13:
                a(C0046a.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
            default:
                return false;
        }
        return true;
    }
}
