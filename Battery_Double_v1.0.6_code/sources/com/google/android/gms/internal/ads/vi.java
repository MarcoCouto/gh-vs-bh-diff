package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.abp.b;
import com.google.android.gms.internal.ads.abp.e;

public final class vi extends abp<vi, a> implements acy {
    private static volatile adi<vi> zzakh;
    /* access modifiers changed from: private */
    public static final vi zzdik = new vi();
    private int zzdih;
    private vm zzdii;
    private wy zzdij;

    public static final class a extends com.google.android.gms.internal.ads.abp.a<vi, a> implements acy {
        private a() {
            super(vi.zzdik);
        }

        /* synthetic */ a(vj vjVar) {
            this();
        }

        public final a a(int i) {
            b();
            ((vi) this.f2433a).b(i);
            return this;
        }

        public final a a(vm vmVar) {
            b();
            ((vi) this.f2433a).a(vmVar);
            return this;
        }

        public final a a(wy wyVar) {
            b();
            ((vi) this.f2433a).a(wyVar);
            return this;
        }
    }

    static {
        abp.a(vi.class, zzdik);
    }

    private vi() {
    }

    public static vi a(aah aah) throws abv {
        return (vi) abp.a(zzdik, aah);
    }

    /* access modifiers changed from: private */
    public final void a(vm vmVar) {
        if (vmVar == null) {
            throw new NullPointerException();
        }
        this.zzdii = vmVar;
    }

    /* access modifiers changed from: private */
    public final void a(wy wyVar) {
        if (wyVar == null) {
            throw new NullPointerException();
        }
        this.zzdij = wyVar;
    }

    /* access modifiers changed from: private */
    public final void b(int i) {
        this.zzdih = i;
    }

    public static a d() {
        return (a) ((com.google.android.gms.internal.ads.abp.a) zzdik.a(e.e, (Object) null, (Object) null));
    }

    public final int a() {
        return this.zzdih;
    }

    /* JADX WARNING: type inference failed for: r0v6, types: [com.google.android.gms.internal.ads.adi<com.google.android.gms.internal.ads.vi>] */
    /* JADX WARNING: type inference failed for: r0v7, types: [java.lang.Object] */
    /* JADX WARNING: type inference failed for: r0v8, types: [com.google.android.gms.internal.ads.adi<com.google.android.gms.internal.ads.vi>, com.google.android.gms.internal.ads.abp$b] */
    /* JADX WARNING: type inference failed for: r0v16 */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r0v8, types: [com.google.android.gms.internal.ads.adi<com.google.android.gms.internal.ads.vi>, com.google.android.gms.internal.ads.abp$b]
  assigns: [com.google.android.gms.internal.ads.abp$b]
  uses: [com.google.android.gms.internal.ads.adi<com.google.android.gms.internal.ads.vi>]
  mth insns count: 42
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
    	at jadx.core.ProcessClass.process(ProcessClass.java:35)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Unknown variable types count: 2 */
    public final Object a(int i, Object obj, Object obj2) {
        ? r0;
        switch (vj.f3742a[i - 1]) {
            case 1:
                return new vi();
            case 2:
                return new a(null);
            case 3:
                Object[] objArr = {"zzdih", "zzdii", "zzdij"};
                return a((acw) zzdik, "\u0000\u0003\u0000\u0000\u0001\u0003\u0003\u0004\u0000\u0000\u0000\u0001\u000b\u0002\t\u0003\t", objArr);
            case 4:
                return zzdik;
            case 5:
                adi<vi> adi = zzakh;
                if (adi != null) {
                    return adi;
                }
                synchronized (vi.class) {
                    r0 = zzakh;
                    if (r0 == 0) {
                        ? bVar = new b(zzdik);
                        zzakh = bVar;
                        r0 = bVar;
                    }
                }
                return r0;
            case 6:
                return Byte.valueOf(1);
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    public final vm b() {
        return this.zzdii == null ? vm.e() : this.zzdii;
    }

    public final wy c() {
        return this.zzdij == null ? wy.e() : this.zzdij;
    }
}
