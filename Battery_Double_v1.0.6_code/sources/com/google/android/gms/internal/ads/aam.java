package com.google.android.gms.internal.ads;

final class aam {

    /* renamed from: a reason: collision with root package name */
    private final aav f2397a;

    /* renamed from: b reason: collision with root package name */
    private final byte[] f2398b;

    private aam(int i) {
        this.f2398b = new byte[i];
        this.f2397a = aav.a(this.f2398b);
    }

    /* synthetic */ aam(int i, aai aai) {
        this(i);
    }

    public final aah a() {
        this.f2397a.b();
        return new aao(this.f2398b);
    }

    public final aav b() {
        return this.f2397a;
    }
}
