package com.google.android.gms.internal.ads;

import android.content.Context;
import android.webkit.WebSettings;
import java.util.concurrent.Callable;

final class kf implements Callable<Boolean> {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ Context f3449a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ WebSettings f3450b;

    kf(ke keVar, Context context, WebSettings webSettings) {
        this.f3449a = context;
        this.f3450b = webSettings;
    }

    public final /* synthetic */ Object call() throws Exception {
        if (this.f3449a.getCacheDir() != null) {
            this.f3450b.setAppCachePath(this.f3449a.getCacheDir().getAbsolutePath());
            this.f3450b.setAppCacheMaxSize(0);
            this.f3450b.setAppCacheEnabled(true);
        }
        this.f3450b.setDatabasePath(this.f3449a.getDatabasePath("com.google.android.gms.ads.db").getAbsolutePath());
        this.f3450b.setDatabaseEnabled(true);
        this.f3450b.setDomStorageEnabled(true);
        this.f3450b.setDisplayZoomControls(false);
        this.f3450b.setBuiltInZoomControls(true);
        this.f3450b.setSupportZoom(true);
        this.f3450b.setAllowContentAccess(false);
        return Boolean.valueOf(true);
    }
}
