package com.google.android.gms.internal.ads;

import android.os.RemoteException;

final class beo implements sx<sy, Object> {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ bee f3172a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ bcx f3173b;
    private final /* synthetic */ bem c;

    beo(bem bem, bee bee, bcx bcx) {
        this.c = bem;
        this.f3172a = bee;
        this.f3173b = bcx;
    }

    public final void a(String str) {
        try {
            this.f3172a.a(str);
        } catch (RemoteException e) {
            ms.b("", e);
        }
    }
}
