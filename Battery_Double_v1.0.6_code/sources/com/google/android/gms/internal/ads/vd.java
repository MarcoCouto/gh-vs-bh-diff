package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.vz.a;
import java.security.GeneralSecurityException;
import java.util.Arrays;

final class vd implements yk {

    /* renamed from: a reason: collision with root package name */
    private final String f3737a;

    /* renamed from: b reason: collision with root package name */
    private final int f3738b;
    private vz c;
    private vi d;
    private int e;

    vd(xj xjVar) throws GeneralSecurityException {
        this.f3737a = xjVar.a();
        if (this.f3737a.equals("type.googleapis.com/google.crypto.tink.AesGcmKey")) {
            try {
                wb a2 = wb.a(xjVar.b());
                this.c = (vz) uh.b(xjVar);
                this.f3738b = a2.a();
            } catch (abv e2) {
                throw new GeneralSecurityException("invalid KeyFormat protobuf, expected AesGcmKeyFormat", e2);
            }
        } else if (this.f3737a.equals("type.googleapis.com/google.crypto.tink.AesCtrHmacAeadKey")) {
            try {
                vk a3 = vk.a(xjVar.b());
                this.d = (vi) uh.b(xjVar);
                this.e = a3.a().b();
                this.f3738b = a3.b().b() + this.e;
            } catch (abv e3) {
                throw new GeneralSecurityException("invalid KeyFormat protobuf, expected AesGcmKeyFormat", e3);
            }
        } else {
            String str = "unsupported AEAD DEM key type: ";
            String valueOf = String.valueOf(this.f3737a);
            throw new GeneralSecurityException(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
        }
    }

    public final int a() {
        return this.f3738b;
    }

    public final tr a(byte[] bArr) throws GeneralSecurityException {
        if (bArr.length != this.f3738b) {
            throw new GeneralSecurityException("Symmetric key has incorrect length");
        } else if (this.f3737a.equals("type.googleapis.com/google.crypto.tink.AesGcmKey")) {
            return (tr) uh.b(this.f3737a, (vz) ((a) vz.c().a(this.c)).a(aah.a(bArr, 0, this.f3738b)).c());
        } else if (this.f3737a.equals("type.googleapis.com/google.crypto.tink.AesCtrHmacAeadKey")) {
            byte[] copyOfRange = Arrays.copyOfRange(bArr, 0, this.e);
            vm vmVar = (vm) ((vm.a) vm.d().a(this.d.b())).a(aah.a(copyOfRange)).c();
            return (tr) uh.b(this.f3737a, (vi) vi.d().a(this.d.a()).a(vmVar).a((wy) ((wy.a) wy.d().a(this.d.c())).a(aah.a(Arrays.copyOfRange(bArr, this.e, this.f3738b))).c()).c());
        } else {
            throw new GeneralSecurityException("unknown DEM key type");
        }
    }
}
