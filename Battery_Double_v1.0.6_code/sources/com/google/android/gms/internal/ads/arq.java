package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.i.a;

public final class arq extends aqw {

    /* renamed from: a reason: collision with root package name */
    private final a f2859a;

    public arq(a aVar) {
        this.f2859a = aVar;
    }

    public final void a() {
        this.f2859a.a();
    }

    public final void a(boolean z) {
        this.f2859a.a(z);
    }

    public final void b() {
        this.f2859a.b();
    }

    public final void c() {
        this.f2859a.c();
    }

    public final void d() {
        this.f2859a.d();
    }
}
