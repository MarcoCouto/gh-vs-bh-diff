package com.google.android.gms.internal.ads;

import java.util.List;

abstract class acg {

    /* renamed from: a reason: collision with root package name */
    private static final acg f2453a = new aci();

    /* renamed from: b reason: collision with root package name */
    private static final acg f2454b = new acj();

    private acg() {
    }

    static acg a() {
        return f2453a;
    }

    static acg b() {
        return f2454b;
    }

    /* access modifiers changed from: 0000 */
    public abstract <L> List<L> a(Object obj, long j);

    /* access modifiers changed from: 0000 */
    public abstract <L> void a(Object obj, Object obj2, long j);

    /* access modifiers changed from: 0000 */
    public abstract void b(Object obj, long j);
}
