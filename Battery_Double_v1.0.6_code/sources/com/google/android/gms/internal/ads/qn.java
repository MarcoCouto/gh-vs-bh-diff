package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewParent;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.google.android.gms.ads.internal.aq;
import com.google.android.gms.ads.internal.bu;
import com.google.android.gms.ads.internal.gmsg.ae;
import com.google.android.gms.ads.internal.overlay.d;
import com.google.android.gms.common.util.o;

@cm
public interface qn extends aq, akv, azf, bag, pm, rj, rk, ro, rr, rt, ru {
    boolean A();

    void B();

    boolean C();

    boolean D();

    boolean E();

    void F();

    void G();

    atw H();

    void I();

    void J();

    void a(int i);

    void a(Context context);

    void a(d dVar);

    void a(atw atw);

    void a(rd rdVar);

    void a(sb sbVar);

    void a(String str);

    void a(String str, ae<? super qn> aeVar);

    void a(String str, o<ae<? super qn>> oVar);

    void a(String str, String str2, String str3);

    rd b();

    void b(d dVar);

    void b(String str, ae<? super qn> aeVar);

    void b(boolean z);

    void c(boolean z);

    Activity d();

    void d(boolean z);

    void destroy();

    bu e();

    void e(boolean z);

    Context getContext();

    int getHeight();

    LayoutParams getLayoutParams();

    void getLocationOnScreen(int[] iArr);

    OnClickListener getOnClickListener();

    ViewParent getParent();

    int getRequestedOrientation();

    View getView();

    WebView getWebView();

    int getWidth();

    asu j();

    mu k();

    void loadData(String str, String str2, String str3);

    void loadDataWithBaseURL(String str, String str2, String str3, String str4, String str5);

    void loadUrl(String str);

    void measure(int i, int i2);

    void n();

    void o();

    void onPause();

    void onResume();

    void p();

    Context q();

    d r();

    d s();

    void setBackgroundColor(int i);

    void setOnClickListener(OnClickListener onClickListener);

    void setOnTouchListener(OnTouchListener onTouchListener);

    void setRequestedOrientation(int i);

    void setWebChromeClient(WebChromeClient webChromeClient);

    void setWebViewClient(WebViewClient webViewClient);

    void stopLoading();

    sb t();

    String u();

    rv v();

    WebViewClient w();

    boolean x();

    ahh y();

    boolean z();
}
