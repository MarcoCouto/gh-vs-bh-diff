package com.google.android.gms.internal.ads;

@cm
public final class bci {

    /* renamed from: a reason: collision with root package name */
    public final int f3132a;

    /* renamed from: b reason: collision with root package name */
    public final bca f3133b;
    public final bcu c;
    public final String d;
    public final bce e;
    public final bda f;
    public final long g;

    public bci(int i) {
        this(null, null, null, null, i, null, 0);
    }

    public bci(bca bca, bcu bcu, String str, bce bce, int i, bda bda, long j) {
        this.f3133b = bca;
        this.c = bcu;
        this.d = str;
        this.e = bce;
        this.f3132a = i;
        this.f = bda;
        this.g = j;
    }
}
