package com.google.android.gms.internal.ads;

import android.os.IInterface;
import android.os.RemoteException;

public interface bcx extends IInterface {
    void a() throws RemoteException;

    void a(int i) throws RemoteException;

    void a(avt avt, String str) throws RemoteException;

    void a(bda bda) throws RemoteException;

    void a(String str) throws RemoteException;

    void a(String str, String str2) throws RemoteException;

    void b() throws RemoteException;

    void c() throws RemoteException;

    void d() throws RemoteException;

    void e() throws RemoteException;

    void f() throws RemoteException;

    void g() throws RemoteException;
}
