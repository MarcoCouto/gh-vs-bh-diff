package com.google.android.gms.internal.ads;

final class aja implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ awb f2631a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ ahz f2632b;

    aja(ahz ahz, awb awb) {
        this.f2632b = ahz;
        this.f2631a = awb;
    }

    public final void run() {
        try {
            this.f2632b.c.put(this.f2631a);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }
}
