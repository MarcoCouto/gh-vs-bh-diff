package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.abp.e;

final class adl implements acu {

    /* renamed from: a reason: collision with root package name */
    private final acw f2480a;

    /* renamed from: b reason: collision with root package name */
    private final String f2481b;
    private final adm c;

    adl(acw acw, String str, Object[] objArr) {
        this.f2480a = acw;
        this.f2481b = str;
        this.c = new adm(acw.getClass(), str, objArr);
    }

    public final int a() {
        return (this.c.d & 1) == 1 ? e.h : e.i;
    }

    public final boolean b() {
        return (this.c.d & 2) == 2;
    }

    public final acw c() {
        return this.f2480a;
    }

    /* access modifiers changed from: 0000 */
    public final adm d() {
        return this.c;
    }

    public final int e() {
        return this.c.h;
    }

    public final int f() {
        return this.c.i;
    }

    public final int g() {
        return this.c.e;
    }

    public final int h() {
        return this.c.j;
    }

    public final int i() {
        return this.c.m;
    }

    /* access modifiers changed from: 0000 */
    public final int[] j() {
        return this.c.n;
    }

    public final int k() {
        return this.c.l;
    }

    public final int l() {
        return this.c.k;
    }
}
