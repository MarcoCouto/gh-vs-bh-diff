package com.google.android.gms.internal.ads;

final class aee {
    static String a(aah aah) {
        aef aef = new aef(aah);
        StringBuilder sb = new StringBuilder(aef.a());
        for (int i = 0; i < aef.a(); i++) {
            byte a2 = aef.a(i);
            switch (a2) {
                case 7:
                    sb.append("\\a");
                    break;
                case 8:
                    sb.append("\\b");
                    break;
                case 9:
                    sb.append("\\t");
                    break;
                case 10:
                    sb.append("\\n");
                    break;
                case 11:
                    sb.append("\\v");
                    break;
                case 12:
                    sb.append("\\f");
                    break;
                case 13:
                    sb.append("\\r");
                    break;
                case 34:
                    sb.append("\\\"");
                    break;
                case 39:
                    sb.append("\\'");
                    break;
                case 92:
                    sb.append("\\\\");
                    break;
                default:
                    if (a2 >= 32 && a2 <= 126) {
                        sb.append((char) a2);
                        break;
                    } else {
                        sb.append('\\');
                        sb.append((char) (((a2 >>> 6) & 3) + 48));
                        sb.append((char) (((a2 >>> 3) & 7) + 48));
                        sb.append((char) ((a2 & 7) + 48));
                        break;
                    }
            }
        }
        return sb.toString();
    }
}
