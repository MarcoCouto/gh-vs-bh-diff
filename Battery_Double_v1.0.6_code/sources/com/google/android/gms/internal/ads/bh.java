package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.aub;
import java.util.concurrent.ExecutionException;
import org.json.JSONException;
import org.json.JSONObject;

public interface bh<T extends aub> {
    T a(ay ayVar, JSONObject jSONObject) throws JSONException, InterruptedException, ExecutionException;
}
