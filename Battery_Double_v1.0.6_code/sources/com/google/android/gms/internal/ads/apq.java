package com.google.android.gms.internal.ads;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.ads.b.j;

public interface apq extends IInterface {
    apn a() throws RemoteException;

    void a(j jVar) throws RemoteException;

    void a(apk apk) throws RemoteException;

    void a(aqk aqk) throws RemoteException;

    void a(aul aul) throws RemoteException;

    void a(avx avx) throws RemoteException;

    void a(awa awa) throws RemoteException;

    void a(awk awk, aot aot) throws RemoteException;

    void a(awn awn) throws RemoteException;

    void a(String str, awh awh, awe awe) throws RemoteException;
}
