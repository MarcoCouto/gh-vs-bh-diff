package com.google.android.gms.internal.ads;

final class bar implements ob {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ bay f3083a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ bah f3084b;

    bar(bah bah, bay bay) {
        this.f3084b = bah;
        this.f3083a = bay;
    }

    public final void a() {
        synchronized (this.f3084b.f3065a) {
            this.f3084b.h = 1;
            jm.a("Failed loading new engine. Marking new engine destroyable.");
            this.f3083a.e();
        }
    }
}
