package com.google.android.gms.internal.ads;

import java.util.ArrayList;
import java.util.List;

@cm
public final class lb {

    /* renamed from: a reason: collision with root package name */
    private final String[] f3471a;

    /* renamed from: b reason: collision with root package name */
    private final double[] f3472b;
    private final double[] c;
    private final int[] d;
    private int e;

    private lb(le leVar) {
        int size = leVar.f3476b.size();
        this.f3471a = (String[]) leVar.f3475a.toArray(new String[size]);
        this.f3472b = a(leVar.f3476b);
        this.c = a(leVar.c);
        this.d = new int[size];
        this.e = 0;
    }

    private static double[] a(List<Double> list) {
        double[] dArr = new double[list.size()];
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= dArr.length) {
                return dArr;
            }
            dArr[i2] = ((Double) list.get(i2)).doubleValue();
            i = i2 + 1;
        }
    }

    public final List<ld> a() {
        ArrayList arrayList = new ArrayList(this.f3471a.length);
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.f3471a.length) {
                return arrayList;
            }
            arrayList.add(new ld(this.f3471a[i2], this.c[i2], this.f3472b[i2], ((double) this.d[i2]) / ((double) this.e), this.d[i2]));
            i = i2 + 1;
        }
    }

    public final void a(double d2) {
        this.e++;
        int i = 0;
        while (i < this.c.length) {
            if (this.c[i] <= d2 && d2 < this.f3472b[i]) {
                int[] iArr = this.d;
                iArr[i] = iArr[i] + 1;
            }
            if (d2 >= this.c[i]) {
                i++;
            } else {
                return;
            }
        }
    }
}
