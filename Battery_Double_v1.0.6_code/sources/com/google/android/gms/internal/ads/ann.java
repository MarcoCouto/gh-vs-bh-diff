package com.google.android.gms.internal.ads;

import java.io.IOException;

public final class ann extends afh<ann> {

    /* renamed from: a reason: collision with root package name */
    public String f2764a;

    /* renamed from: b reason: collision with root package name */
    public long[] f2765b;
    public anl c;
    public ang d;
    private Integer e;
    private Integer f;
    private Integer g;
    private any h;
    private anm i;
    private anr j;

    public ann() {
        this.e = null;
        this.f2764a = null;
        this.f = null;
        this.g = null;
        this.h = null;
        this.f2765b = afq.f2535b;
        this.c = null;
        this.i = null;
        this.j = null;
        this.d = null;
        this.Y = null;
        this.Z = -1;
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public final ann a(afd afd) throws IOException {
        while (true) {
            int a2 = afd.a();
            switch (a2) {
                case 0:
                    break;
                case 72:
                    this.e = Integer.valueOf(afd.g());
                    continue;
                case 82:
                    this.f2764a = afd.e();
                    continue;
                case 88:
                    this.f = Integer.valueOf(afd.g());
                    continue;
                case 96:
                    int j2 = afd.j();
                    try {
                        this.g = Integer.valueOf(anf.a(afd.g()));
                        continue;
                    } catch (IllegalArgumentException e2) {
                        afd.e(j2);
                        a(afd, a2);
                        break;
                    }
                case 106:
                    if (this.h == null) {
                        this.h = new any();
                    }
                    afd.a((afn) this.h);
                    continue;
                case 112:
                    int a3 = afq.a(afd, 112);
                    int length = this.f2765b == null ? 0 : this.f2765b.length;
                    long[] jArr = new long[(a3 + length)];
                    if (length != 0) {
                        System.arraycopy(this.f2765b, 0, jArr, 0, length);
                    }
                    while (length < jArr.length - 1) {
                        jArr[length] = afd.h();
                        afd.a();
                        length++;
                    }
                    jArr[length] = afd.h();
                    this.f2765b = jArr;
                    continue;
                case 114:
                    int c2 = afd.c(afd.g());
                    int j3 = afd.j();
                    int i2 = 0;
                    while (afd.i() > 0) {
                        afd.h();
                        i2++;
                    }
                    afd.e(j3);
                    int length2 = this.f2765b == null ? 0 : this.f2765b.length;
                    long[] jArr2 = new long[(i2 + length2)];
                    if (length2 != 0) {
                        System.arraycopy(this.f2765b, 0, jArr2, 0, length2);
                    }
                    while (length2 < jArr2.length) {
                        jArr2[length2] = afd.h();
                        length2++;
                    }
                    this.f2765b = jArr2;
                    afd.d(c2);
                    continue;
                case 122:
                    if (this.c == null) {
                        this.c = new anl();
                    }
                    afd.a((afn) this.c);
                    continue;
                case 130:
                    if (this.i == null) {
                        this.i = new anm();
                    }
                    afd.a((afn) this.i);
                    continue;
                case 138:
                    if (this.j == null) {
                        this.j = new anr();
                    }
                    afd.a((afn) this.j);
                    continue;
                case 146:
                    if (this.d == null) {
                        this.d = new ang();
                    }
                    afd.a((afn) this.d);
                    continue;
                default:
                    if (!super.a(afd, a2)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    /* access modifiers changed from: protected */
    public final int a() {
        int a2 = super.a();
        if (this.e != null) {
            a2 += aff.b(9, this.e.intValue());
        }
        if (this.f2764a != null) {
            a2 += aff.b(10, this.f2764a);
        }
        if (this.f != null) {
            a2 += aff.d(this.f.intValue()) + aff.b(11);
        }
        if (this.g != null) {
            a2 += aff.b(12, this.g.intValue());
        }
        if (this.h != null) {
            a2 += aff.b(13, (afn) this.h);
        }
        if (this.f2765b != null && this.f2765b.length > 0) {
            int i2 = 0;
            for (long a3 : this.f2765b) {
                i2 += aff.a(a3);
            }
            a2 = a2 + i2 + (this.f2765b.length * 1);
        }
        if (this.c != null) {
            a2 += aff.b(15, (afn) this.c);
        }
        if (this.i != null) {
            a2 += aff.b(16, (afn) this.i);
        }
        if (this.j != null) {
            a2 += aff.b(17, (afn) this.j);
        }
        return this.d != null ? a2 + aff.b(18, (afn) this.d) : a2;
    }

    public final void a(aff aff) throws IOException {
        if (this.e != null) {
            aff.a(9, this.e.intValue());
        }
        if (this.f2764a != null) {
            aff.a(10, this.f2764a);
        }
        if (this.f != null) {
            int intValue = this.f.intValue();
            aff.c(11, 0);
            aff.c(intValue);
        }
        if (this.g != null) {
            aff.a(12, this.g.intValue());
        }
        if (this.h != null) {
            aff.a(13, (afn) this.h);
        }
        if (this.f2765b != null && this.f2765b.length > 0) {
            for (long a2 : this.f2765b) {
                aff.a(14, a2);
            }
        }
        if (this.c != null) {
            aff.a(15, (afn) this.c);
        }
        if (this.i != null) {
            aff.a(16, (afn) this.i);
        }
        if (this.j != null) {
            aff.a(17, (afn) this.j);
        }
        if (this.d != null) {
            aff.a(18, (afn) this.d);
        }
        super.a(aff);
    }
}
