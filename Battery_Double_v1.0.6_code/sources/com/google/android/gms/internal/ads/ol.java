package com.google.android.gms.internal.ads;

final /* synthetic */ class ol implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final ok f3572a;

    /* renamed from: b reason: collision with root package name */
    private final int f3573b;

    ol(ok okVar, int i) {
        this.f3572a = okVar;
        this.f3573b = i;
    }

    public final void run() {
        this.f3572a.b(this.f3573b);
    }
}
