package com.google.android.gms.internal.ads;

import java.io.IOException;

public final class anm extends afh<anm> {

    /* renamed from: a reason: collision with root package name */
    private Integer f2762a;

    /* renamed from: b reason: collision with root package name */
    private any f2763b;
    private String c;
    private String d;

    public anm() {
        this.f2762a = null;
        this.f2763b = null;
        this.c = null;
        this.d = null;
        this.Y = null;
        this.Z = -1;
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public final anm a(afd afd) throws IOException {
        while (true) {
            int a2 = afd.a();
            switch (a2) {
                case 0:
                    break;
                case 40:
                    int j = afd.j();
                    try {
                        int g = afd.g();
                        if (g < 0 || g > 2) {
                            throw new IllegalArgumentException(g + " is not a valid enum Platform");
                        }
                        this.f2762a = Integer.valueOf(g);
                        continue;
                    } catch (IllegalArgumentException e) {
                        afd.e(j);
                        a(afd, a2);
                        break;
                    }
                case 50:
                    if (this.f2763b == null) {
                        this.f2763b = new any();
                    }
                    afd.a((afn) this.f2763b);
                    continue;
                case 58:
                    this.c = afd.e();
                    continue;
                case 66:
                    this.d = afd.e();
                    continue;
                default:
                    if (!super.a(afd, a2)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    /* access modifiers changed from: protected */
    public final int a() {
        int a2 = super.a();
        if (this.f2762a != null) {
            a2 += aff.b(5, this.f2762a.intValue());
        }
        if (this.f2763b != null) {
            a2 += aff.b(6, (afn) this.f2763b);
        }
        if (this.c != null) {
            a2 += aff.b(7, this.c);
        }
        return this.d != null ? a2 + aff.b(8, this.d) : a2;
    }

    public final void a(aff aff) throws IOException {
        if (this.f2762a != null) {
            aff.a(5, this.f2762a.intValue());
        }
        if (this.f2763b != null) {
            aff.a(6, (afn) this.f2763b);
        }
        if (this.c != null) {
            aff.a(7, this.c);
        }
        if (this.d != null) {
            aff.a(8, this.d);
        }
        super.a(aff);
    }
}
