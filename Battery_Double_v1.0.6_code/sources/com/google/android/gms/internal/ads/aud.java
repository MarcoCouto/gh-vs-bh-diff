package com.google.android.gms.internal.ads;

import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import com.google.android.gms.ads.internal.ax;
import com.google.android.gms.b.a;
import com.google.android.gms.b.b;
import com.google.android.gms.common.internal.aa;
import java.lang.ref.WeakReference;
import java.util.Map;
import java.util.Map.Entry;
import org.json.JSONException;
import org.json.JSONObject;

@cm
public class aud implements aty {

    /* renamed from: a reason: collision with root package name */
    boolean f2921a;

    /* renamed from: b reason: collision with root package name */
    boolean f2922b;
    private final Object c = new Object();
    private final aua d;
    private final Context e;
    private final atj f;
    private final JSONObject g;
    private final bq h;
    private final aub i;
    private final ahh j;
    private final mu k;
    private String l;
    private ig m;
    private WeakReference<View> n = null;

    public aud(Context context, aua aua, bq bqVar, ahh ahh, JSONObject jSONObject, aub aub, mu muVar, String str) {
        this.e = context;
        this.d = aua;
        this.h = bqVar;
        this.j = ahh;
        this.g = jSONObject;
        this.i = aub;
        this.k = muVar;
        this.l = str;
        this.f = new atj(this.h);
    }

    private final int a(int i2) {
        ape.a();
        return mh.b(this.e, i2);
    }

    private final JSONObject a(Rect rect) throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("width", a(rect.right - rect.left));
        jSONObject.put("height", a(rect.bottom - rect.top));
        jSONObject.put("x", a(rect.left));
        jSONObject.put("y", a(rect.top));
        jSONObject.put("relative_to", "self");
        return jSONObject;
    }

    private final JSONObject a(Map<String, WeakReference<View>> map, View view) {
        JSONObject jSONObject;
        JSONObject jSONObject2 = new JSONObject();
        if (map == null || view == null) {
            return jSONObject2;
        }
        int[] f2 = f(view);
        synchronized (map) {
            for (Entry entry : map.entrySet()) {
                View view2 = (View) ((WeakReference) entry.getValue()).get();
                if (view2 != null) {
                    int[] f3 = f(view2);
                    JSONObject jSONObject3 = new JSONObject();
                    JSONObject jSONObject4 = new JSONObject();
                    try {
                        jSONObject4.put("width", a(view2.getMeasuredWidth()));
                        jSONObject4.put("height", a(view2.getMeasuredHeight()));
                        jSONObject4.put("x", a(f3[0] - f2[0]));
                        jSONObject4.put("y", a(f3[1] - f2[1]));
                        jSONObject4.put("relative_to", "ad_view");
                        jSONObject3.put("frame", jSONObject4);
                        Rect rect = new Rect();
                        if (view2.getLocalVisibleRect(rect)) {
                            jSONObject = a(rect);
                        } else {
                            jSONObject = new JSONObject();
                            jSONObject.put("width", 0);
                            jSONObject.put("height", 0);
                            jSONObject.put("x", a(f3[0] - f2[0]));
                            jSONObject.put("y", a(f3[1] - f2[1]));
                            jSONObject.put("relative_to", "ad_view");
                        }
                        jSONObject3.put("visible_bounds", jSONObject);
                        if (view2 instanceof TextView) {
                            TextView textView = (TextView) view2;
                            jSONObject3.put("text_color", textView.getCurrentTextColor());
                            jSONObject3.put("font_size", (double) textView.getTextSize());
                            jSONObject3.put("text", textView.getText());
                        }
                        jSONObject2.put((String) entry.getKey(), jSONObject3);
                    } catch (JSONException e2) {
                        jm.e("Unable to get asset views information");
                    }
                }
            }
        }
        return jSONObject2;
    }

    private final void a(View view, JSONObject jSONObject, JSONObject jSONObject2, JSONObject jSONObject3, JSONObject jSONObject4, String str, JSONObject jSONObject5, JSONObject jSONObject6) {
        aa.b("Invalid call from a non-UI thread.");
        try {
            JSONObject jSONObject7 = new JSONObject();
            jSONObject7.put("ad", this.g);
            if (jSONObject2 != null) {
                jSONObject7.put("asset_view_signal", jSONObject2);
            }
            if (jSONObject != null) {
                jSONObject7.put("ad_view_signal", jSONObject);
            }
            if (jSONObject5 != null) {
                jSONObject7.put("click_signal", jSONObject5);
            }
            if (jSONObject3 != null) {
                jSONObject7.put("scroll_view_signal", jSONObject3);
            }
            if (jSONObject4 != null) {
                jSONObject7.put("lock_screen_signal", jSONObject4);
            }
            JSONObject jSONObject8 = new JSONObject();
            jSONObject8.put("asset_id", str);
            jSONObject8.put("template", this.i.k());
            ax.g();
            jSONObject8.put("is_privileged_process", kb.e());
            if (((Boolean) ape.f().a(asi.ck)).booleanValue() && this.f.a() != null && this.g.optBoolean("custom_one_point_five_click_enabled", false)) {
                jSONObject8.put("custom_one_point_five_click_eligible", true);
            }
            jSONObject8.put("timestamp", ax.l().a());
            jSONObject8.put("has_custom_click_handler", this.d.b(this.i.l()) != null);
            jSONObject7.put("has_custom_click_handler", this.d.b(this.i.l()) != null);
            try {
                JSONObject optJSONObject = this.g.optJSONObject("tracking_urls_and_actions");
                if (optJSONObject == null) {
                    optJSONObject = new JSONObject();
                }
                jSONObject8.put("click_signals", this.j.a().a(this.e, optJSONObject.optString("click_string"), view));
            } catch (Exception e2) {
                jm.b("Exception obtaining click signals", e2);
            }
            jSONObject7.put("click", jSONObject8);
            if (jSONObject6 != null) {
                jSONObject7.put("provided_signals", jSONObject6);
            }
            jSONObject7.put("ads_id", this.l);
            na.a(this.h.b(jSONObject7), "NativeAdEngineImpl.performClick");
        } catch (JSONException e3) {
            jm.b("Unable to create click JSON.", e3);
        }
    }

    private final boolean a(String str) {
        JSONObject optJSONObject = this.g == null ? null : this.g.optJSONObject("allow_pub_event_reporting");
        if (optJSONObject == null) {
            return false;
        }
        return optJSONObject.optBoolean(str, false);
    }

    private final boolean a(JSONObject jSONObject, JSONObject jSONObject2, JSONObject jSONObject3, JSONObject jSONObject4, JSONObject jSONObject5) {
        aa.b("Invalid call from a non-UI thread.");
        if (this.f2921a) {
            return true;
        }
        this.f2921a = true;
        try {
            JSONObject jSONObject6 = new JSONObject();
            jSONObject6.put("ad", this.g);
            jSONObject6.put("ads_id", this.l);
            if (jSONObject2 != null) {
                jSONObject6.put("asset_view_signal", jSONObject2);
            }
            if (jSONObject != null) {
                jSONObject6.put("ad_view_signal", jSONObject);
            }
            if (jSONObject3 != null) {
                jSONObject6.put("scroll_view_signal", jSONObject3);
            }
            if (jSONObject4 != null) {
                jSONObject6.put("lock_screen_signal", jSONObject4);
            }
            if (jSONObject5 != null) {
                jSONObject6.put("provided_signals", jSONObject5);
            }
            na.a(this.h.c(jSONObject6), "NativeAdEngineImpl.recordImpression");
            this.d.a((aty) this);
            this.d.C();
            j();
            return true;
        } catch (JSONException e2) {
            jm.b("Unable to create impression JSON.", e2);
            return false;
        }
    }

    private static boolean e(View view) {
        return view.isShown() && view.getGlobalVisibleRect(new Rect(), null);
    }

    private static int[] f(View view) {
        int[] iArr = new int[2];
        view.getLocationOnScreen(iArr);
        return iArr;
    }

    private final JSONObject g(View view) {
        JSONObject jSONObject;
        JSONObject jSONObject2 = new JSONObject();
        if (view != null) {
            try {
                int[] f2 = f(view);
                JSONObject jSONObject3 = new JSONObject();
                jSONObject3.put("width", a(view.getMeasuredWidth()));
                jSONObject3.put("height", a(view.getMeasuredHeight()));
                jSONObject3.put("x", a(f2[0]));
                jSONObject3.put("y", a(f2[1]));
                jSONObject3.put("relative_to", "window");
                jSONObject2.put("frame", jSONObject3);
                Rect rect = new Rect();
                if (view.getGlobalVisibleRect(rect)) {
                    jSONObject = a(rect);
                } else {
                    jSONObject = new JSONObject();
                    jSONObject.put("width", 0);
                    jSONObject.put("height", 0);
                    jSONObject.put("x", a(f2[0]));
                    jSONObject.put("y", a(f2[1]));
                    jSONObject.put("relative_to", "window");
                }
                jSONObject2.put("visible_bounds", jSONObject);
            } catch (Exception e2) {
                jm.e("Unable to get native ad view bounding box");
            }
        }
        return jSONObject2;
    }

    private static JSONObject h(View view) {
        JSONObject jSONObject = new JSONObject();
        if (view != null) {
            try {
                ax.e();
                jSONObject.put("contained_in_scroll_view", jv.d(view) != -1);
            } catch (Exception e2) {
            }
        }
        return jSONObject;
    }

    private final JSONObject i(View view) {
        JSONObject jSONObject = new JSONObject();
        if (view != null) {
            String str = "can_show_on_lock_screen";
            try {
                ax.e();
                jSONObject.put(str, jv.c(view));
                ax.e();
                jSONObject.put("is_keyguard_locked", jv.j(this.e));
            } catch (JSONException e2) {
                jm.e("Unable to get lock screen information");
            }
        }
        return jSONObject;
    }

    public View a(OnClickListener onClickListener, boolean z) {
        ati m2 = this.i.m();
        if (m2 == null) {
            return null;
        }
        LayoutParams layoutParams = new LayoutParams(-2, -2);
        if (!z) {
            switch (m2.h()) {
                case 0:
                    layoutParams.addRule(10);
                    layoutParams.addRule(9);
                    break;
                case 2:
                    layoutParams.addRule(12);
                    layoutParams.addRule(11);
                    break;
                case 3:
                    layoutParams.addRule(12);
                    layoutParams.addRule(9);
                    break;
                default:
                    layoutParams.addRule(10);
                    layoutParams.addRule(11);
                    break;
            }
        }
        atl atl = new atl(this.e, m2, layoutParams);
        atl.setOnClickListener(onClickListener);
        atl.setContentDescription((CharSequence) ape.f().a(asi.ce));
        return atl;
    }

    public final void a(MotionEvent motionEvent) {
        this.j.a(motionEvent);
    }

    public void a(View view) {
        if (((Boolean) ape.f().a(asi.ck)).booleanValue()) {
            if (!this.g.optBoolean("custom_one_point_five_click_enabled", false)) {
                jm.e("Your account need to be whitelisted to use this feature.\nContact your account manager for more information.");
                return;
            }
            atj atj = this.f;
            if (view != null) {
                view.setOnClickListener(atj);
                view.setClickable(true);
                atj.c = new WeakReference<>(view);
            }
        }
    }

    public final void a(View view, atw atw) {
        if (!b(view, atw)) {
            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-1, -1);
            ((FrameLayout) view).removeAllViews();
            if (this.i instanceof auc) {
                auc auc = (auc) this.i;
                if (auc.b() != null && auc.b().size() > 0) {
                    Object obj = auc.b().get(0);
                    auw auw = obj instanceof IBinder ? aux.a((IBinder) obj) : null;
                    if (auw != null) {
                        try {
                            a a2 = auw.a();
                            if (a2 != null) {
                                Drawable drawable = (Drawable) b.a(a2);
                                ImageView imageView = new ImageView(this.e);
                                imageView.setImageDrawable(drawable);
                                imageView.setScaleType(ScaleType.CENTER_INSIDE);
                                ((FrameLayout) view).addView(imageView, layoutParams);
                            }
                        } catch (RemoteException e2) {
                            jm.e("Could not get drawable from image");
                        }
                    }
                }
            }
        }
    }

    public final void a(View view, String str, Bundle bundle, Map<String, WeakReference<View>> map, View view2) {
        JSONObject jSONObject;
        JSONObject a2 = a(map, view2);
        JSONObject g2 = g(view2);
        JSONObject h2 = h(view2);
        JSONObject i2 = i(view2);
        try {
            JSONObject a3 = ax.e().a(bundle, (JSONObject) null);
            jSONObject = new JSONObject();
            try {
                jSONObject.put("click_point", a3);
                jSONObject.put("asset_id", str);
            } catch (Exception e2) {
                e = e2;
                jm.b("Error occurred while grabbing click signals.", e);
                a(view, g2, a2, h2, i2, str, jSONObject, null);
            }
        } catch (Exception e3) {
            e = e3;
            jSONObject = null;
            jm.b("Error occurred while grabbing click signals.", e);
            a(view, g2, a2, h2, i2, str, jSONObject, null);
        }
        a(view, g2, a2, h2, i2, str, jSONObject, null);
    }

    public void a(View view, Map<String, WeakReference<View>> map) {
        a(g(view), a(map, view), h(view), i(view), (JSONObject) null);
    }

    public void a(View view, Map<String, WeakReference<View>> map, Bundle bundle, View view2) {
        aa.b("Invalid call from a non-UI thread.");
        if (map != null) {
            synchronized (map) {
                for (Entry entry : map.entrySet()) {
                    if (view.equals((View) ((WeakReference) entry.getValue()).get())) {
                        a(view, (String) entry.getKey(), bundle, map, view2);
                        return;
                    }
                }
            }
        }
        if ("6".equals(this.i.k())) {
            a(view, "3099", bundle, map, view2);
        } else if ("2".equals(this.i.k())) {
            a(view, "2099", bundle, map, view2);
        } else if ("1".equals(this.i.k())) {
            a(view, "1099", bundle, map, view2);
        }
    }

    public void a(View view, Map<String, WeakReference<View>> map, Map<String, WeakReference<View>> map2, OnTouchListener onTouchListener, OnClickListener onClickListener) {
        if (((Boolean) ape.f().a(asi.cb)).booleanValue()) {
            view.setOnTouchListener(onTouchListener);
            view.setClickable(true);
            view.setOnClickListener(onClickListener);
            if (map != null) {
                synchronized (map) {
                    for (Entry value : map.entrySet()) {
                        View view2 = (View) ((WeakReference) value.getValue()).get();
                        if (view2 != null) {
                            view2.setOnTouchListener(onTouchListener);
                            view2.setClickable(true);
                            view2.setOnClickListener(onClickListener);
                        }
                    }
                }
            }
            if (map2 != null) {
                synchronized (map2) {
                    for (Entry value2 : map2.entrySet()) {
                        View view3 = (View) ((WeakReference) value2.getValue()).get();
                        if (view3 != null) {
                            view3.setOnTouchListener(onTouchListener);
                        }
                    }
                }
            }
        }
    }

    public void a(awq awq) {
        if (((Boolean) ape.f().a(asi.ck)).booleanValue()) {
            if (!this.g.optBoolean("custom_one_point_five_click_enabled", false)) {
                jm.e("Your account need to be whitelisted to use this feature.\nContact your account manager for more information.");
            } else {
                this.f.a(awq);
            }
        }
    }

    public final void a(Map<String, WeakReference<View>> map) {
        if (this.i.o() == null) {
            return;
        }
        if ("2".equals(this.i.k())) {
            ax.i().l().a(this.d.D(), this.i.k(), map.containsKey("2011"));
        } else if ("1".equals(this.i.k())) {
            ax.i().l().a(this.d.D(), this.i.k(), map.containsKey("1009"));
        }
    }

    public boolean a() {
        ati m2 = this.i.m();
        return m2 != null && m2.i();
    }

    public final boolean a(Bundle bundle) {
        if (!a("impression_reporting")) {
            jm.c("The ad slot cannot handle external impression events. You must be whitelisted to whitelisted to be able to report your impression events.");
            return false;
        }
        return a((JSONObject) null, (JSONObject) null, (JSONObject) null, (JSONObject) null, ax.e().a(bundle, (JSONObject) null));
    }

    public final void b(Bundle bundle) {
        if (bundle == null) {
            jm.b("Click data is null. No click is reported.");
        } else if (!a("click_reporting")) {
            jm.c("The ad slot cannot handle external click events. You must be whitelisted to be able to report your click events.");
        } else {
            a(null, null, null, null, null, bundle.getBundle("click_signal").getString("asset_id"), null, ax.e().a(bundle, (JSONObject) null));
        }
    }

    public final void b(View view) {
        if (((Boolean) ape.f().a(asi.bG)).booleanValue() && this.j != null) {
            ahd a2 = this.j.a();
            if (a2 != null) {
                a2.a(view);
            }
        }
    }

    public void b(View view, Map<String, WeakReference<View>> map) {
        if (!((Boolean) ape.f().a(asi.ca)).booleanValue()) {
            view.setOnTouchListener(null);
            view.setClickable(false);
            view.setOnClickListener(null);
            if (map != null) {
                synchronized (map) {
                    for (Entry value : map.entrySet()) {
                        View view2 = (View) ((WeakReference) value.getValue()).get();
                        if (view2 != null) {
                            view2.setOnTouchListener(null);
                            view2.setClickable(false);
                            view2.setOnClickListener(null);
                        }
                    }
                }
            }
        }
    }

    public boolean b() {
        return this.g != null && this.g.optBoolean("allow_pub_owned_ad_view", false);
    }

    public final boolean b(View view, atw atw) {
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-2, -2, 17);
        View o = this.i.o();
        if (o == null) {
            return false;
        }
        ViewParent parent = o.getParent();
        if (parent instanceof ViewGroup) {
            ((ViewGroup) parent).removeView(o);
        }
        ((FrameLayout) view).removeAllViews();
        ((FrameLayout) view).addView(o, layoutParams);
        this.d.a(atw);
        return true;
    }

    public void c() {
        if (((Boolean) ape.f().a(asi.ck)).booleanValue()) {
            if (!this.g.optBoolean("custom_one_point_five_click_enabled", false)) {
                jm.e("Your account need to be whitelisted to use this feature.\nContact your account manager for more information.");
            } else {
                this.f.b();
            }
        }
    }

    public final void c(Bundle bundle) {
        if (bundle == null) {
            jm.b("Touch event data is null. No touch event is reported.");
        } else if (!a("touch_reporting")) {
            jm.c("The ad slot cannot handle external touch events. You must be whitelisted to be able to report your touch events.");
        } else {
            this.j.a().a((int) bundle.getFloat("x"), (int) bundle.getFloat("y"), bundle.getInt("duration_ms"));
        }
    }

    public final void c(View view) {
        this.n = new WeakReference<>(view);
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    public final void c(View view, Map<String, WeakReference<View>> map) {
        synchronized (this.c) {
            if (!this.f2921a) {
                if (e(view)) {
                    a(view, map);
                    return;
                }
                if (((Boolean) ape.f().a(asi.cj)).booleanValue() && map != null) {
                    synchronized (map) {
                        for (Entry value : map.entrySet()) {
                            View view2 = (View) ((WeakReference) value.getValue()).get();
                            if (view2 != null && e(view2)) {
                                a(view, map);
                                return;
                            }
                        }
                    }
                }
            }
        }
    }

    public void d() {
        aa.b("Invalid call from a non-UI thread.");
        if (!this.f2922b) {
            this.f2922b = true;
            try {
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("ad", this.g);
                jSONObject.put("ads_id", this.l);
                na.a(this.h.d(jSONObject), "NativeAdEngineImpl.recordDownloadedImpression");
            } catch (JSONException e2) {
                ms.b("", e2);
            }
        }
    }

    public final void d(View view) {
        this.d.b(view);
    }

    public qn g() throws qy {
        qn qnVar = null;
        if (!(this.g == null || this.g.optJSONObject("overlay") == null)) {
            ax.f();
            Context context = this.e;
            aot a2 = aot.a(this.e);
            qnVar = qu.a(context, sb.a(a2), a2.f2814a, false, false, this.j, this.k, null, null, null, amw.a());
            if (qnVar != null) {
                qnVar.getView().setVisibility(8);
                new auf(qnVar).a(this.h);
            }
        }
        return qnVar;
    }

    public void h() {
        this.h.a();
    }

    public void i() {
        this.d.P();
    }

    public void j() {
        this.d.L();
    }

    public void k() {
        this.d.Q();
    }

    public final View l() {
        if (this.n != null) {
            return (View) this.n.get();
        }
        return null;
    }

    public final Context m() {
        return this.e;
    }

    public final ig n() {
        if (!ax.B().c(this.e)) {
            return null;
        }
        if (this.m == null) {
            this.m = new ig(this.e, this.d.D());
        }
        return this.m;
    }
}
