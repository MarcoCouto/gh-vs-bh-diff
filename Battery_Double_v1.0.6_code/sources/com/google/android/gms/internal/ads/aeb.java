package com.google.android.gms.internal.ads;

import java.util.AbstractSet;
import java.util.Iterator;
import java.util.Map.Entry;

class aeb extends AbstractSet<Entry<K, V>> {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ ads f2499a;

    private aeb(ads ads) {
        this.f2499a = ads;
    }

    /* synthetic */ aeb(ads ads, adt adt) {
        this(ads);
    }

    public /* synthetic */ boolean add(Object obj) {
        Entry entry = (Entry) obj;
        if (contains(entry)) {
            return false;
        }
        this.f2499a.put((Comparable) entry.getKey(), entry.getValue());
        return true;
    }

    public void clear() {
        this.f2499a.clear();
    }

    public boolean contains(Object obj) {
        Entry entry = (Entry) obj;
        Object obj2 = this.f2499a.get(entry.getKey());
        Object value = entry.getValue();
        return obj2 == value || (obj2 != null && obj2.equals(value));
    }

    public Iterator<Entry<K, V>> iterator() {
        return new aea(this.f2499a, null);
    }

    public boolean remove(Object obj) {
        Entry entry = (Entry) obj;
        if (!contains(entry)) {
            return false;
        }
        this.f2499a.remove(entry.getKey());
        return true;
    }

    public int size() {
        return this.f2499a.size();
    }
}
