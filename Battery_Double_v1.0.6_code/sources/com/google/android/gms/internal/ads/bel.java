package com.google.android.gms.internal.ads;

import android.os.RemoteException;

@cm
public class bel {
    public static beg a(String str) throws RemoteException {
        try {
            return new bem((ta) Class.forName(str, false, bel.class.getClassLoader()).getDeclaredConstructor(new Class[0]).newInstance(new Object[0]));
        } catch (Throwable th) {
            throw new RemoteException();
        }
    }
}
