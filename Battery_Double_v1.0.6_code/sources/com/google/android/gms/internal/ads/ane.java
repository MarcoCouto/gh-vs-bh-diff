package com.google.android.gms.internal.ads;

import android.os.Handler;
import java.util.concurrent.Executor;

public final class ane implements b {

    /* renamed from: a reason: collision with root package name */
    private final Executor f2749a;

    public ane(Handler handler) {
        this.f2749a = new aof(this, handler);
    }

    public final void a(awb<?> awb, bcd<?> bcd) {
        a(awb, bcd, null);
    }

    public final void a(awb<?> awb, bcd<?> bcd, Runnable runnable) {
        awb.k();
        awb.b("post-response");
        this.f2749a.execute(new apc(this, awb, bcd, runnable));
    }

    public final void a(awb<?> awb, df dfVar) {
        awb.b("post-error");
        this.f2749a.execute(new apc(this, awb, bcd.a(dfVar), null));
    }
}
