package com.google.android.gms.internal.ads;

final /* synthetic */ class az implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final ay f3020a;

    /* renamed from: b reason: collision with root package name */
    private final ny f3021b;
    private final String c;

    az(ay ayVar, ny nyVar, String str) {
        this.f3020a = ayVar;
        this.f3021b = nyVar;
        this.c = str;
    }

    public final void run() {
        this.f3020a.a(this.f3021b, this.c);
    }
}
