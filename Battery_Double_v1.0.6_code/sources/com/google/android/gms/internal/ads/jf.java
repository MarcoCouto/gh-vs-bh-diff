package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.Bundle;
import com.google.android.gms.ads.internal.ax;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

@cm
public final class jf implements alm {

    /* renamed from: a reason: collision with root package name */
    private final Object f3420a;

    /* renamed from: b reason: collision with root package name */
    private final jb f3421b;
    private final HashSet<it> c;
    private final HashSet<je> d;

    public jf() {
        this(ape.c());
    }

    private jf(String str) {
        this.f3420a = new Object();
        this.c = new HashSet<>();
        this.d = new HashSet<>();
        this.f3421b = new jb(str);
    }

    public final Bundle a(Context context, jc jcVar, String str) {
        Bundle bundle;
        synchronized (this.f3420a) {
            bundle = new Bundle();
            bundle.putBundle("app", this.f3421b.a(context, str));
            Bundle bundle2 = new Bundle();
            Iterator it = this.d.iterator();
            while (it.hasNext()) {
                je jeVar = (je) it.next();
                bundle2.putBundle(jeVar.a(), jeVar.b());
            }
            bundle.putBundle("slots", bundle2);
            ArrayList arrayList = new ArrayList();
            Iterator it2 = this.c.iterator();
            while (it2.hasNext()) {
                arrayList.add(((it) it2.next()).d());
            }
            bundle.putParcelableArrayList("ads", arrayList);
            jcVar.a(this.c);
            this.c.clear();
        }
        return bundle;
    }

    public final void a() {
        synchronized (this.f3420a) {
            this.f3421b.a();
        }
    }

    public final void a(aop aop, long j) {
        synchronized (this.f3420a) {
            this.f3421b.a(aop, j);
        }
    }

    public final void a(it itVar) {
        synchronized (this.f3420a) {
            this.c.add(itVar);
        }
    }

    public final void a(je jeVar) {
        synchronized (this.f3420a) {
            this.d.add(jeVar);
        }
    }

    public final void a(HashSet<it> hashSet) {
        synchronized (this.f3420a) {
            this.c.addAll(hashSet);
        }
    }

    public final void a(boolean z) {
        long a2 = ax.l().a();
        if (z) {
            if (a2 - ax.i().l().i() > ((Long) ape.f().a(asi.aI)).longValue()) {
                this.f3421b.f3415a = -1;
                return;
            }
            this.f3421b.f3415a = ax.i().l().j();
            return;
        }
        ax.i().l().a(a2);
        ax.i().l().b(this.f3421b.f3415a);
    }

    public final void b() {
        synchronized (this.f3420a) {
            this.f3421b.b();
        }
    }
}
