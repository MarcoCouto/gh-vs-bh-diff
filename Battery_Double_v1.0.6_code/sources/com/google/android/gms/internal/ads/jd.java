package com.google.android.gms.internal.ads;

import java.math.BigInteger;

@cm
public final class jd {

    /* renamed from: a reason: collision with root package name */
    private BigInteger f3417a = BigInteger.ONE;

    public final synchronized String a() {
        String bigInteger;
        bigInteger = this.f3417a.toString();
        this.f3417a = this.f3417a.add(BigInteger.ONE);
        return bigInteger;
    }
}
