package com.google.android.gms.internal.ads;

enum abm {
    SCALAR(false),
    VECTOR(true),
    PACKED_VECTOR(true),
    MAP(false);
    
    private final boolean e;

    private abm(boolean z) {
        this.e = z;
    }
}
