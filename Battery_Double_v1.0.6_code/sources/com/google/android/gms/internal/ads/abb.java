package com.google.android.gms.internal.ads;

final class abb {

    /* renamed from: a reason: collision with root package name */
    private static final Class<?> f2413a = b();

    public static abc a() {
        if (f2413a != null) {
            try {
                return (abc) f2413a.getDeclaredMethod("getEmptyRegistry", new Class[0]).invoke(null, new Object[0]);
            } catch (Exception e) {
            }
        }
        return abc.f2414a;
    }

    private static Class<?> b() {
        try {
            return Class.forName("com.google.protobuf.ExtensionRegistry");
        } catch (ClassNotFoundException e) {
            return null;
        }
    }
}
