package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.a.b;
import java.util.ArrayList;

public final class dq implements Creator<dp> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b2 = b.b(parcel);
        int i = 0;
        String str = null;
        String str2 = null;
        ArrayList arrayList = null;
        int i2 = 0;
        ArrayList arrayList2 = null;
        long j = 0;
        boolean z = false;
        long j2 = 0;
        ArrayList arrayList3 = null;
        long j3 = 0;
        int i3 = 0;
        String str3 = null;
        long j4 = 0;
        String str4 = null;
        boolean z2 = false;
        String str5 = null;
        String str6 = null;
        boolean z3 = false;
        boolean z4 = false;
        boolean z5 = false;
        boolean z6 = false;
        boolean z7 = false;
        eb ebVar = null;
        String str7 = null;
        String str8 = null;
        boolean z8 = false;
        boolean z9 = false;
        hp hpVar = null;
        ArrayList arrayList4 = null;
        ArrayList arrayList5 = null;
        boolean z10 = false;
        dr drVar = null;
        boolean z11 = false;
        String str9 = null;
        ArrayList arrayList6 = null;
        boolean z12 = false;
        String str10 = null;
        hz hzVar = null;
        String str11 = null;
        boolean z13 = false;
        boolean z14 = false;
        Bundle bundle = null;
        boolean z15 = false;
        int i4 = 0;
        boolean z16 = false;
        ArrayList arrayList7 = null;
        boolean z17 = false;
        String str12 = null;
        while (parcel.dataPosition() < b2) {
            int a2 = b.a(parcel);
            switch (b.a(a2)) {
                case 1:
                    i = b.d(parcel, a2);
                    break;
                case 2:
                    str = b.k(parcel, a2);
                    break;
                case 3:
                    str2 = b.k(parcel, a2);
                    break;
                case 4:
                    arrayList = b.q(parcel, a2);
                    break;
                case 5:
                    i2 = b.d(parcel, a2);
                    break;
                case 6:
                    arrayList2 = b.q(parcel, a2);
                    break;
                case 7:
                    j = b.f(parcel, a2);
                    break;
                case 8:
                    z = b.c(parcel, a2);
                    break;
                case 9:
                    j2 = b.f(parcel, a2);
                    break;
                case 10:
                    arrayList3 = b.q(parcel, a2);
                    break;
                case 11:
                    j3 = b.f(parcel, a2);
                    break;
                case 12:
                    i3 = b.d(parcel, a2);
                    break;
                case 13:
                    str3 = b.k(parcel, a2);
                    break;
                case 14:
                    j4 = b.f(parcel, a2);
                    break;
                case 15:
                    str4 = b.k(parcel, a2);
                    break;
                case 18:
                    z2 = b.c(parcel, a2);
                    break;
                case 19:
                    str5 = b.k(parcel, a2);
                    break;
                case 21:
                    str6 = b.k(parcel, a2);
                    break;
                case 22:
                    z3 = b.c(parcel, a2);
                    break;
                case 23:
                    z4 = b.c(parcel, a2);
                    break;
                case 24:
                    z5 = b.c(parcel, a2);
                    break;
                case 25:
                    z6 = b.c(parcel, a2);
                    break;
                case 26:
                    z7 = b.c(parcel, a2);
                    break;
                case 28:
                    ebVar = (eb) b.a(parcel, a2, eb.CREATOR);
                    break;
                case 29:
                    str7 = b.k(parcel, a2);
                    break;
                case 30:
                    str8 = b.k(parcel, a2);
                    break;
                case 31:
                    z8 = b.c(parcel, a2);
                    break;
                case 32:
                    z9 = b.c(parcel, a2);
                    break;
                case 33:
                    hpVar = (hp) b.a(parcel, a2, hp.CREATOR);
                    break;
                case 34:
                    arrayList4 = b.q(parcel, a2);
                    break;
                case 35:
                    arrayList5 = b.q(parcel, a2);
                    break;
                case 36:
                    z10 = b.c(parcel, a2);
                    break;
                case 37:
                    drVar = (dr) b.a(parcel, a2, dr.CREATOR);
                    break;
                case 38:
                    z11 = b.c(parcel, a2);
                    break;
                case 39:
                    str9 = b.k(parcel, a2);
                    break;
                case 40:
                    arrayList6 = b.q(parcel, a2);
                    break;
                case 42:
                    z12 = b.c(parcel, a2);
                    break;
                case 43:
                    str10 = b.k(parcel, a2);
                    break;
                case 44:
                    hzVar = (hz) b.a(parcel, a2, hz.CREATOR);
                    break;
                case 45:
                    str11 = b.k(parcel, a2);
                    break;
                case 46:
                    z13 = b.c(parcel, a2);
                    break;
                case 47:
                    z14 = b.c(parcel, a2);
                    break;
                case 48:
                    bundle = b.m(parcel, a2);
                    break;
                case 49:
                    z15 = b.c(parcel, a2);
                    break;
                case 50:
                    i4 = b.d(parcel, a2);
                    break;
                case 51:
                    z16 = b.c(parcel, a2);
                    break;
                case 52:
                    arrayList7 = b.q(parcel, a2);
                    break;
                case 53:
                    z17 = b.c(parcel, a2);
                    break;
                case 54:
                    str12 = b.k(parcel, a2);
                    break;
                default:
                    b.b(parcel, a2);
                    break;
            }
        }
        b.r(parcel, b2);
        return new dp(i, str, str2, arrayList, i2, arrayList2, j, z, j2, arrayList3, j3, i3, str3, j4, str4, z2, str5, str6, z3, z4, z5, z6, z7, ebVar, str7, str8, z8, z9, hpVar, arrayList4, arrayList5, z10, drVar, z11, str9, arrayList6, z12, str10, hzVar, str11, z13, z14, bundle, z15, i4, z16, arrayList7, z17, str12);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new dp[i];
    }
}
