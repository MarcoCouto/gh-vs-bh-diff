package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.SystemClock;

@cm
public abstract class ak extends jh {

    /* renamed from: a reason: collision with root package name */
    protected final ap f2653a;

    /* renamed from: b reason: collision with root package name */
    protected final Context f2654b;
    protected final Object c = new Object();
    protected final Object d = new Object();
    protected final is e;
    protected dp f;

    protected ak(Context context, is isVar, ap apVar) {
        super(true);
        this.f2654b = context;
        this.e = isVar;
        this.f = isVar.f3398b;
        this.f2653a = apVar;
    }

    /* access modifiers changed from: protected */
    public abstract ir a(int i);

    public final void a() {
        synchronized (this.c) {
            jm.b("AdRendererBackgroundTask started.");
            int i = this.e.e;
            try {
                a(SystemClock.elapsedRealtime());
            } catch (an e2) {
                int a2 = e2.a();
                if (a2 == 3 || a2 == -1) {
                    jm.d(e2.getMessage());
                } else {
                    jm.e(e2.getMessage());
                }
                if (this.f == null) {
                    this.f = new dp(a2);
                } else {
                    this.f = new dp(a2, this.f.j);
                }
                jv.f3440a.post(new al(this));
                i = a2;
            }
            jv.f3440a.post(new am(this, a(i)));
        }
    }

    /* access modifiers changed from: protected */
    public abstract void a(long j) throws an;

    public void c_() {
    }
}
