package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.b.b;
import com.google.android.gms.b.c;
import com.google.android.gms.b.c.a;

@cm
public final class q extends c<u> {
    public q() {
        super("com.google.android.gms.ads.AdOverlayCreatorImpl");
    }

    public final r a(Activity activity) {
        try {
            IBinder a2 = ((u) a((Context) activity)).a(b.a(activity));
            if (a2 == null) {
                return null;
            }
            IInterface queryLocalInterface = a2.queryLocalInterface("com.google.android.gms.ads.internal.overlay.client.IAdOverlay");
            return queryLocalInterface instanceof r ? (r) queryLocalInterface : new t(a2);
        } catch (RemoteException e) {
            ms.c("Could not create remote AdOverlay.", e);
            return null;
        } catch (a e2) {
            ms.c("Could not create remote AdOverlay.", e2);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.ads.internal.overlay.client.IAdOverlayCreator");
        return queryLocalInterface instanceof u ? (u) queryLocalInterface : new v(iBinder);
    }
}
