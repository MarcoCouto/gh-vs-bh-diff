package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.Bundle;
import android.os.RemoteException;
import com.google.android.gms.ads.internal.ax;
import com.google.android.gms.ads.internal.gmsg.k;
import java.util.concurrent.Future;

@cm
public final class gv extends jh implements hb, he, hj {

    /* renamed from: a reason: collision with root package name */
    public final String f3346a;

    /* renamed from: b reason: collision with root package name */
    private final is f3347b;
    /* access modifiers changed from: private */
    public final Context c;
    private final hk d;
    private final he e;
    private final Object f;
    /* access modifiers changed from: private */
    public final String g;
    private final bca h;
    private final long i;
    private int j = 0;
    private int k = 3;
    private gy l;
    private Future m;
    private volatile k n;

    public gv(Context context, String str, String str2, bca bca, is isVar, hk hkVar, he heVar, long j2) {
        this.c = context;
        this.f3346a = str;
        this.g = str2;
        this.h = bca;
        this.f3347b = isVar;
        this.d = hkVar;
        this.f = new Object();
        this.e = heVar;
        this.i = j2;
    }

    /* access modifiers changed from: private */
    public final void a(aop aop, bcu bcu) {
        this.d.b().a((he) this);
        try {
            if ("com.google.ads.mediation.admob.AdMobAdapter".equals(this.f3346a)) {
                bcu.a(aop, this.g, this.h.f3119a);
            } else {
                bcu.a(aop, this.g);
            }
        } catch (RemoteException e2) {
            jm.c("Fail to load ad from adapter.", e2);
            a(this.f3346a, 0);
        }
    }

    private final boolean a(long j2) {
        long b2 = this.i - (ax.l().b() - j2);
        if (b2 <= 0) {
            this.k = 4;
            return false;
        }
        try {
            this.f.wait(b2);
            return true;
        } catch (InterruptedException e2) {
            Thread.currentThread().interrupt();
            this.k = 5;
            return false;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0051, code lost:
        r2 = new com.google.android.gms.internal.ads.ha().a(com.google.android.gms.ads.internal.ax.l().b() - r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0066, code lost:
        if (1 != r10.j) goto L_0x00ac;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0068, code lost:
        r0 = 6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0069, code lost:
        r10.l = r2.a(r0).a(r10.f3346a).b(r10.h.d).a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:?, code lost:
        r0 = r10.k;
     */
    public final void a() {
        if (this.d != null && this.d.b() != null && this.d.a() != null) {
            hd b2 = this.d.b();
            b2.a((he) null);
            b2.a((hb) this);
            b2.a((hj) this);
            aop aop = this.f3347b.f3397a.c;
            bcu a2 = this.d.a();
            try {
                if (a2.g()) {
                    mh.f3514a.post(new gw(this, aop, a2));
                } else {
                    mh.f3514a.post(new gx(this, a2, aop, b2));
                }
            } catch (RemoteException e2) {
                jm.c("Fail to check if adapter is initialized.", e2);
                a(this.f3346a, 0);
            }
            long b3 = ax.l().b();
            while (true) {
                synchronized (this.f) {
                    if (this.j != 0) {
                        break;
                    } else if (!a(b3)) {
                        this.l = new ha().a(this.k).a(ax.l().b() - b3).a(this.f3346a).b(this.h.d).a();
                    }
                }
            }
            b2.a((he) null);
            b2.a((hb) null);
            if (this.j == 1) {
                this.e.a(this.f3346a);
            } else {
                this.e.a(this.f3346a, this.k);
            }
        }
    }

    public final void a(int i2) {
        a(this.f3346a, 0);
    }

    public final void a(Bundle bundle) {
        k kVar = this.n;
        if (kVar != null) {
            kVar.a("", bundle);
        }
    }

    public final void a(k kVar) {
        this.n = kVar;
    }

    public final void a(String str) {
        synchronized (this.f) {
            this.j = 1;
            this.f.notify();
        }
    }

    public final void a(String str, int i2) {
        synchronized (this.f) {
            this.j = 2;
            this.k = i2;
            this.f.notify();
        }
    }

    public final void c_() {
    }

    public final Future d() {
        if (this.m != null) {
            return this.m;
        }
        nn nnVar = (nn) c();
        this.m = nnVar;
        return nnVar;
    }

    public final gy e() {
        gy gyVar;
        synchronized (this.f) {
            gyVar = this.l;
        }
        return gyVar;
    }

    public final bca f() {
        return this.h;
    }

    public final void g() {
        a(this.f3347b.f3397a.c, this.d.a());
    }
}
