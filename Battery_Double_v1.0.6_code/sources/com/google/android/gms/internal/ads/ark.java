package com.google.android.gms.internal.ads;

import android.os.RemoteException;

final class ark implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ arj f2855a;

    ark(arj arj) {
        this.f2855a = arj;
    }

    public final void run() {
        if (this.f2855a.f2854a.f2853a != null) {
            try {
                this.f2855a.f2854a.f2853a.a(1);
            } catch (RemoteException e) {
                ms.c("Could not notify onAdFailedToLoad event.", e);
            }
        }
    }
}
