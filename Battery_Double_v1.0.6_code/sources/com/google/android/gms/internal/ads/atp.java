package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.view.View;
import com.google.android.gms.b.a;
import com.google.android.gms.b.b;
import java.util.List;

@cm
public final class atp extends avq implements auc {

    /* renamed from: a reason: collision with root package name */
    private String f2909a;

    /* renamed from: b reason: collision with root package name */
    private List<atm> f2910b;
    private String c;
    private auw d;
    private String e;
    private String f;
    private ati g;
    private Bundle h;
    private aqs i;
    private View j;
    private a k;
    private String l;
    private Object m = new Object();
    /* access modifiers changed from: private */
    public aty n;

    public atp(String str, List<atm> list, String str2, auw auw, String str3, String str4, ati ati, Bundle bundle, aqs aqs, View view, a aVar, String str5) {
        this.f2909a = str;
        this.f2910b = list;
        this.c = str2;
        this.d = auw;
        this.e = str3;
        this.f = str4;
        this.g = ati;
        this.h = bundle;
        this.i = aqs;
        this.j = view;
        this.k = aVar;
        this.l = str5;
    }

    public final String a() {
        return this.f2909a;
    }

    public final void a(Bundle bundle) {
        synchronized (this.m) {
            if (this.n == null) {
                jm.c("#001 Attempt to perform click before app native ad initialized.");
            } else {
                this.n.b(bundle);
            }
        }
    }

    public final void a(aty aty) {
        synchronized (this.m) {
            this.n = aty;
        }
    }

    public final List b() {
        return this.f2910b;
    }

    public final boolean b(Bundle bundle) {
        boolean a2;
        synchronized (this.m) {
            if (this.n == null) {
                jm.c("#002 Attempt to record impression before native ad initialized.");
                a2 = false;
            } else {
                a2 = this.n.a(bundle);
            }
        }
        return a2;
    }

    public final a c() {
        return this.k;
    }

    public final void c(Bundle bundle) {
        synchronized (this.m) {
            if (this.n == null) {
                jm.c("#003 Attempt to report touch event before native ad initialized.");
            } else {
                this.n.c(bundle);
            }
        }
    }

    public final String d() {
        return this.l;
    }

    public final String e() {
        return this.c;
    }

    public final auw f() {
        return this.d;
    }

    public final String g() {
        return this.e;
    }

    public final String h() {
        return this.f;
    }

    public final aqs i() {
        return this.i;
    }

    public final a j() {
        return b.a(this.n);
    }

    public final String k() {
        return "1";
    }

    public final String l() {
        return "";
    }

    public final ati m() {
        return this.g;
    }

    public final Bundle n() {
        return this.h;
    }

    public final View o() {
        return this.j;
    }

    public final aus p() {
        return this.g;
    }

    public final void q() {
        jv.f3440a.post(new atq(this));
        this.f2909a = null;
        this.f2910b = null;
        this.c = null;
        this.d = null;
        this.e = null;
        this.f = null;
        this.g = null;
        this.h = null;
        this.m = null;
        this.i = null;
        this.j = null;
    }
}
