package com.google.android.gms.internal.ads;

import android.annotation.TargetApi;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.provider.CalendarContract.Events;
import android.text.TextUtils;
import com.google.android.gms.ads.c.a.C0045a;
import com.google.android.gms.ads.internal.ax;
import java.util.Map;

@cm
public final class beu extends n {

    /* renamed from: a reason: collision with root package name */
    private final Map<String, String> f3180a;
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public final Context f3181b;
    private String c = d("description");
    private long d = e("start_ticks");
    private long e = e("end_ticks");
    private String f = d("summary");
    private String g = d("location");

    public beu(qn qnVar, Map<String, String> map) {
        super(qnVar, "createCalendarEvent");
        this.f3180a = map;
        this.f3181b = qnVar.d();
    }

    private final String d(String str) {
        return TextUtils.isEmpty((CharSequence) this.f3180a.get(str)) ? "" : (String) this.f3180a.get(str);
    }

    private final long e(String str) {
        String str2 = (String) this.f3180a.get(str);
        if (str2 == null) {
            return -1;
        }
        try {
            return Long.parseLong(str2);
        } catch (NumberFormatException e2) {
            return -1;
        }
    }

    public final void a() {
        if (this.f3181b == null) {
            a("Activity context is not available.");
            return;
        }
        ax.e();
        if (!jv.f(this.f3181b).d()) {
            a("This feature is not available on the device.");
            return;
        }
        ax.e();
        Builder e2 = jv.e(this.f3181b);
        Resources h = ax.i().h();
        e2.setTitle(h != null ? h.getString(C0045a.s5) : "Create calendar event");
        e2.setMessage(h != null ? h.getString(C0045a.s6) : "Allow Ad to create a calendar event?");
        e2.setPositiveButton(h != null ? h.getString(C0045a.s3) : "Accept", new bev(this));
        e2.setNegativeButton(h != null ? h.getString(C0045a.s4) : "Decline", new c(this));
        e2.create().show();
    }

    /* access modifiers changed from: 0000 */
    @TargetApi(14)
    public final Intent b() {
        Intent data = new Intent("android.intent.action.EDIT").setData(Events.CONTENT_URI);
        data.putExtra("title", this.c);
        data.putExtra("eventLocation", this.g);
        data.putExtra("description", this.f);
        if (this.d > -1) {
            data.putExtra("beginTime", this.d);
        }
        if (this.e > -1) {
            data.putExtra("endTime", this.e);
        }
        data.setFlags(268435456);
        return data;
    }
}
