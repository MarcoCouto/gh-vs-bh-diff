package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.b.a;
import com.google.android.gms.b.a.C0046a;
import java.util.ArrayList;
import java.util.List;

public final class bdg extends ajk implements bdd {
    bdg(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.mediation.client.INativeAppInstallAdMapper");
    }

    public final String a() throws RemoteException {
        Parcel a2 = a(2, r_());
        String readString = a2.readString();
        a2.recycle();
        return readString;
    }

    public final void a(a aVar) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (IInterface) aVar);
        b(11, r_);
    }

    public final void a(a aVar, a aVar2, a aVar3) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (IInterface) aVar);
        ajm.a(r_, (IInterface) aVar2);
        ajm.a(r_, (IInterface) aVar3);
        b(22, r_);
    }

    public final List b() throws RemoteException {
        Parcel a2 = a(3, r_());
        ArrayList b2 = ajm.b(a2);
        a2.recycle();
        return b2;
    }

    public final void b(a aVar) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (IInterface) aVar);
        b(12, r_);
    }

    public final String c() throws RemoteException {
        Parcel a2 = a(4, r_());
        String readString = a2.readString();
        a2.recycle();
        return readString;
    }

    public final void c(a aVar) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (IInterface) aVar);
        b(16, r_);
    }

    public final auw d() throws RemoteException {
        Parcel a2 = a(5, r_());
        auw a3 = aux.a(a2.readStrongBinder());
        a2.recycle();
        return a3;
    }

    public final String e() throws RemoteException {
        Parcel a2 = a(6, r_());
        String readString = a2.readString();
        a2.recycle();
        return readString;
    }

    public final double f() throws RemoteException {
        Parcel a2 = a(7, r_());
        double readDouble = a2.readDouble();
        a2.recycle();
        return readDouble;
    }

    public final String g() throws RemoteException {
        Parcel a2 = a(8, r_());
        String readString = a2.readString();
        a2.recycle();
        return readString;
    }

    public final String h() throws RemoteException {
        Parcel a2 = a(9, r_());
        String readString = a2.readString();
        a2.recycle();
        return readString;
    }

    public final void i() throws RemoteException {
        b(10, r_());
    }

    public final boolean j() throws RemoteException {
        Parcel a2 = a(13, r_());
        boolean a3 = ajm.a(a2);
        a2.recycle();
        return a3;
    }

    public final boolean k() throws RemoteException {
        Parcel a2 = a(14, r_());
        boolean a3 = ajm.a(a2);
        a2.recycle();
        return a3;
    }

    public final Bundle l() throws RemoteException {
        Parcel a2 = a(15, r_());
        Bundle bundle = (Bundle) ajm.a(a2, Bundle.CREATOR);
        a2.recycle();
        return bundle;
    }

    public final aqs m() throws RemoteException {
        Parcel a2 = a(17, r_());
        aqs a3 = aqt.a(a2.readStrongBinder());
        a2.recycle();
        return a3;
    }

    public final a n() throws RemoteException {
        Parcel a2 = a(18, r_());
        a a3 = C0046a.a(a2.readStrongBinder());
        a2.recycle();
        return a3;
    }

    public final aus o() throws RemoteException {
        Parcel a2 = a(19, r_());
        aus a3 = aut.a(a2.readStrongBinder());
        a2.recycle();
        return a3;
    }

    public final a p() throws RemoteException {
        Parcel a2 = a(20, r_());
        a a3 = C0046a.a(a2.readStrongBinder());
        a2.recycle();
        return a3;
    }

    public final a q() throws RemoteException {
        Parcel a2 = a(21, r_());
        a a3 = C0046a.a(a2.readStrongBinder());
        a2.recycle();
        return a3;
    }
}
