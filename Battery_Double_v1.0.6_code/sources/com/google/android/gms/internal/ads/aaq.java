package com.google.android.gms.internal.ads;

import java.io.IOException;

public abstract class aaq {
    private static volatile boolean f;

    /* renamed from: a reason: collision with root package name */
    int f2400a;

    /* renamed from: b reason: collision with root package name */
    int f2401b;
    aat c;
    private int d;
    private boolean e;

    static {
        f = false;
        f = true;
    }

    private aaq() {
        this.f2401b = 100;
        this.d = Integer.MAX_VALUE;
        this.e = false;
    }

    public static long a(long j) {
        return (j >>> 1) ^ (-(1 & j));
    }

    static aaq a(byte[] bArr, int i, int i2, boolean z) {
        aas aas = new aas(bArr, i, i2, z);
        try {
            aas.c(i2);
            return aas;
        } catch (abv e2) {
            throw new IllegalArgumentException(e2);
        }
    }

    public static int f(int i) {
        return (i >>> 1) ^ (-(i & 1));
    }

    public abstract int a() throws IOException;

    public abstract void a(int i) throws abv;

    public abstract double b() throws IOException;

    public abstract boolean b(int i) throws IOException;

    public abstract float c() throws IOException;

    public abstract int c(int i) throws abv;

    public abstract long d() throws IOException;

    public abstract void d(int i);

    public abstract long e() throws IOException;

    public abstract void e(int i) throws IOException;

    public abstract int f() throws IOException;

    public abstract long g() throws IOException;

    public abstract int h() throws IOException;

    public abstract boolean i() throws IOException;

    public abstract String j() throws IOException;

    public abstract String k() throws IOException;

    public abstract aah l() throws IOException;

    public abstract int m() throws IOException;

    public abstract int n() throws IOException;

    public abstract int o() throws IOException;

    public abstract long p() throws IOException;

    public abstract int q() throws IOException;

    public abstract long r() throws IOException;

    /* access modifiers changed from: 0000 */
    public abstract long s() throws IOException;

    public abstract boolean t() throws IOException;

    public abstract int u();
}
