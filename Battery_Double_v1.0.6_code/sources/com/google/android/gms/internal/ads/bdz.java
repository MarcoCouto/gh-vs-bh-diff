package com.google.android.gms.internal.ads;

import com.google.ads.a.C0043a;
import com.google.ads.a.b;
import com.google.ads.mediation.a;
import java.util.Date;
import java.util.HashSet;

@cm
public final class bdz {
    public static int a(C0043a aVar) {
        switch (bea.f3166a[aVar.ordinal()]) {
            case 2:
                return 1;
            case 3:
                return 2;
            case 4:
                return 3;
            default:
                return 0;
        }
    }

    public static a a(aop aop, boolean z) {
        b bVar;
        HashSet hashSet = aop.e != null ? new HashSet(aop.e) : null;
        Date date = new Date(aop.f2810b);
        switch (aop.d) {
            case 1:
                bVar = b.MALE;
                break;
            case 2:
                bVar = b.FEMALE;
                break;
            default:
                bVar = b.UNKNOWN;
                break;
        }
        return new a(date, bVar, hashSet, z, aop.k);
    }
}
