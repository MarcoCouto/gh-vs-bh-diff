package com.google.android.gms.internal.ads;

import java.io.PrintWriter;

abstract class zq {

    /* renamed from: a reason: collision with root package name */
    private static final Throwable[] f3823a = new Throwable[0];

    zq() {
    }

    public abstract void a(Throwable th, PrintWriter printWriter);
}
