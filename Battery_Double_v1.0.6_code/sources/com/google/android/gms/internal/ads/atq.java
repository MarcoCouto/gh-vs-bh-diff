package com.google.android.gms.internal.ads;

final class atq implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ atp f2911a;

    atq(atp atp) {
        this.f2911a = atp;
    }

    public final void run() {
        if (this.f2911a.n != null) {
            this.f2911a.n.i();
            this.f2911a.n.h();
            this.f2911a.n.k();
        }
        this.f2911a.n = null;
    }
}
