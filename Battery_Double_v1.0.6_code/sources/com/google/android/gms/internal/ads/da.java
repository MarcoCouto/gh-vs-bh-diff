package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.dynamite.DynamiteModule;
import com.google.android.gms.dynamite.descriptors.com.google.android.gms.ads.dynamite.ModuleDescriptor;

@cm
public final class da {
    /* access modifiers changed from: private */
    public static boolean b(Context context, boolean z) {
        if (!z) {
            return true;
        }
        int b2 = DynamiteModule.b(context, ModuleDescriptor.MODULE_ID);
        if (b2 == 0) {
            return false;
        }
        return b2 <= DynamiteModule.a(context, ModuleDescriptor.MODULE_ID);
    }
}
