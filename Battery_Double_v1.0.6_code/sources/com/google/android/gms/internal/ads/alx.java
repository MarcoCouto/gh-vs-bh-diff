package com.google.android.gms.internal.ads;

import com.google.android.gms.common.util.m;
import java.io.UnsupportedEncodingException;
import java.lang.Character.UnicodeBlock;
import java.util.ArrayList;

@cm
public final class alx {
    public static int a(String str) {
        byte[] bytes;
        try {
            bytes = str.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            bytes = str.getBytes();
        }
        return m.a(bytes, 0, bytes.length, 0);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:46:0x007a, code lost:
        if (((r9 >= 65382 && r9 <= 65437) || (r9 >= 65441 && r9 <= 65500)) != false) goto L_0x007c;
     */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x007f  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x00a1  */
    public static String[] a(String str, boolean z) {
        boolean z2;
        int i;
        boolean z3;
        if (str == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        char[] charArray = str.toCharArray();
        int length = str.length();
        boolean z4 = false;
        int i2 = 0;
        int i3 = 0;
        while (i3 < length) {
            int codePointAt = Character.codePointAt(charArray, i3);
            int charCount = Character.charCount(codePointAt);
            if (Character.isLetter(codePointAt)) {
                UnicodeBlock of = UnicodeBlock.of(codePointAt);
                if (!(of == UnicodeBlock.BOPOMOFO || of == UnicodeBlock.BOPOMOFO_EXTENDED || of == UnicodeBlock.CJK_COMPATIBILITY || of == UnicodeBlock.CJK_COMPATIBILITY_IDEOGRAPHS || of == UnicodeBlock.CJK_COMPATIBILITY_IDEOGRAPHS_SUPPLEMENT || of == UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS || of == UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A || of == UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_B || of == UnicodeBlock.ENCLOSED_CJK_LETTERS_AND_MONTHS || of == UnicodeBlock.HANGUL_JAMO || of == UnicodeBlock.HANGUL_SYLLABLES || of == UnicodeBlock.HIRAGANA || of == UnicodeBlock.KATAKANA || of == UnicodeBlock.KATAKANA_PHONETIC_EXTENSIONS)) {
                }
                z2 = true;
                if (!z2) {
                    if (z4) {
                        arrayList.add(new String(charArray, i2, i3 - i2));
                    }
                    arrayList.add(new String(charArray, i3, charCount));
                    i = i2;
                    z3 = false;
                } else if (Character.isLetterOrDigit(codePointAt) || Character.getType(codePointAt) == 6 || Character.getType(codePointAt) == 8) {
                    if (!z4) {
                        i2 = i3;
                    }
                    i = i2;
                    z3 = true;
                } else if (z && Character.charCount(codePointAt) == 1 && Character.toChars(codePointAt)[0] == '\'') {
                    if (!z4) {
                        i2 = i3;
                    }
                    i = i2;
                    z3 = true;
                } else if (z4) {
                    arrayList.add(new String(charArray, i2, i3 - i2));
                    i = i2;
                    z3 = false;
                } else {
                    boolean z5 = z4;
                    i = i2;
                    z3 = z5;
                }
                i3 += charCount;
                boolean z6 = z3;
                i2 = i;
                z4 = z6;
            }
            z2 = false;
            if (!z2) {
            }
            i3 += charCount;
            boolean z62 = z3;
            i2 = i;
            z4 = z62;
        }
        if (z4) {
            arrayList.add(new String(charArray, i2, i3 - i2));
        }
        return (String[]) arrayList.toArray(new String[arrayList.size()]);
    }
}
