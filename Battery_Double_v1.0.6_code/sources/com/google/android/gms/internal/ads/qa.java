package com.google.android.gms.internal.ads;

import java.util.HashMap;
import java.util.Map;

final class qa implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ String f3623a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ String f3624b;
    private final /* synthetic */ int c;
    private final /* synthetic */ py d;

    qa(py pyVar, String str, String str2, int i) {
        this.d = pyVar;
        this.f3623a = str;
        this.f3624b = str2;
        this.c = i;
    }

    public final void run() {
        HashMap hashMap = new HashMap();
        hashMap.put("event", "precacheComplete");
        hashMap.put("src", this.f3623a);
        hashMap.put("cachedSrc", this.f3624b);
        hashMap.put("totalBytes", Integer.toString(this.c));
        this.d.a("onPrecacheEvent", (Map<String, String>) hashMap);
    }
}
