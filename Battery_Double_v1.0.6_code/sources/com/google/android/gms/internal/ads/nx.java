package com.google.android.gms.internal.ads;

@cm
public final class nx<T> extends ny<T> {

    /* renamed from: a reason: collision with root package name */
    private final T f3562a;

    private nx(T t) {
        this.f3562a = t;
    }

    public static <T> nx<T> a(T t) {
        return new nx<>(t);
    }

    public final void a() {
        b(this.f3562a);
    }
}
