package com.google.android.gms.internal.ads;

import java.io.IOException;

public abstract class afn {
    protected volatile int Z = -1;

    public static final <T extends afn> T a(T t, byte[] bArr) throws afm {
        return a(t, bArr, 0, bArr.length);
    }

    private static final <T extends afn> T a(T t, byte[] bArr, int i, int i2) throws afm {
        try {
            afd a2 = afd.a(bArr, 0, i2);
            t.a(a2);
            a2.a(0);
            return t;
        } catch (afm e) {
            throw e;
        } catch (IOException e2) {
            throw new RuntimeException("Reading from a byte array threw an IOException (should never happen).", e2);
        }
    }

    public static final byte[] a(afn afn) {
        byte[] bArr = new byte[afn.d()];
        try {
            aff a2 = aff.a(bArr, 0, bArr.length);
            afn.a(a2);
            a2.a();
            return bArr;
        } catch (IOException e) {
            throw new RuntimeException("Serializing to a byte array threw an IOException (should never happen).", e);
        }
    }

    /* access modifiers changed from: protected */
    public int a() {
        return 0;
    }

    public abstract afn a(afd afd) throws IOException;

    public void a(aff aff) throws IOException {
    }

    /* renamed from: c */
    public afn clone() throws CloneNotSupportedException {
        return (afn) super.clone();
    }

    public final int d() {
        int a2 = a();
        this.Z = a2;
        return a2;
    }

    public String toString() {
        return afo.a(this);
    }
}
