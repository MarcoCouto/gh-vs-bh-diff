package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.xe.b;
import java.security.GeneralSecurityException;

final class up implements tz<tr> {
    up() {
    }

    private static wd b() throws GeneralSecurityException {
        return (wd) wd.c().a(0).a(aah.a(zj.a(32))).c();
    }

    /* access modifiers changed from: private */
    /* renamed from: d */
    public final tr a(aah aah) throws GeneralSecurityException {
        try {
            wd a2 = wd.a(aah);
            if (!(a2 instanceof wd)) {
                throw new GeneralSecurityException("expected ChaCha20Poly1305Key proto");
            }
            wd wdVar = a2;
            zo.a(wdVar.a(), 0);
            if (wdVar.b().a() == 32) {
                return new yj(wdVar.b().b());
            }
            throw new GeneralSecurityException("invalid ChaCha20Poly1305Key: incorrect key length");
        } catch (abv e) {
            throw new GeneralSecurityException("invalid ChaCha20Poly1305 key", e);
        }
    }

    public final int a() {
        return 0;
    }

    public final /* synthetic */ Object a(acw acw) throws GeneralSecurityException {
        if (!(acw instanceof wd)) {
            throw new GeneralSecurityException("expected ChaCha20Poly1305Key proto");
        }
        wd wdVar = (wd) acw;
        zo.a(wdVar.a(), 0);
        if (wdVar.b().a() == 32) {
            return new yj(wdVar.b().b());
        }
        throw new GeneralSecurityException("invalid ChaCha20Poly1305Key: incorrect key length");
    }

    public final acw b(aah aah) throws GeneralSecurityException {
        return b();
    }

    public final acw b(acw acw) throws GeneralSecurityException {
        return b();
    }

    public final xe c(aah aah) throws GeneralSecurityException {
        return (xe) xe.d().a("type.googleapis.com/google.crypto.tink.ChaCha20Poly1305Key").a(b().h()).a(b.SYMMETRIC).c();
    }
}
