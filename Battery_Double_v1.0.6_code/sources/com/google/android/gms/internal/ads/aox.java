package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;

@cm
public class aox {

    /* renamed from: a reason: collision with root package name */
    private aqh f2819a;

    /* renamed from: b reason: collision with root package name */
    private final Object f2820b = new Object();
    /* access modifiers changed from: private */
    public final aon c;
    /* access modifiers changed from: private */
    public final aom d;
    private final arg e;
    private final aww f;
    private final gr g;
    /* access modifiers changed from: private */
    public final q h;
    private final awx i;

    abstract class a<T> {
        a() {
        }

        /* access modifiers changed from: protected */
        public abstract T a() throws RemoteException;

        /* access modifiers changed from: protected */
        public abstract T a(aqh aqh) throws RemoteException;

        /* access modifiers changed from: protected */
        public final T b() {
            T t = null;
            aqh a2 = aox.this.b();
            if (a2 == null) {
                ms.e("ClientApi class cannot be loaded.");
                return t;
            }
            try {
                return a(a2);
            } catch (RemoteException e) {
                ms.c("Cannot invoke local loader using ClientApi class", e);
                return t;
            }
        }

        /* access modifiers changed from: protected */
        public final T c() {
            try {
                return a();
            } catch (RemoteException e) {
                ms.c("Cannot invoke remote loader", e);
                return null;
            }
        }
    }

    public aox(aon aon, aom aom, arg arg, aww aww, gr grVar, q qVar, awx awx) {
        this.c = aon;
        this.d = aom;
        this.e = arg;
        this.f = aww;
        this.g = grVar;
        this.h = qVar;
        this.i = awx;
    }

    private static aqh a() {
        try {
            Object newInstance = aox.class.getClassLoader().loadClass("com.google.android.gms.ads.internal.ClientApi").newInstance();
            if (newInstance instanceof IBinder) {
                return aqi.asInterface((IBinder) newInstance);
            }
            ms.e("ClientApi class is not an instance of IBinder");
            return null;
        } catch (Exception e2) {
            ms.c("Failed to instantiate ClientApi class.", e2);
            return null;
        }
    }

    static <T> T a(Context context, boolean z, a<T> aVar) {
        boolean z2 = true;
        boolean z3 = z;
        if (!z3) {
            ape.a();
            if (!mh.c(context)) {
                ms.b("Google Play Services is not available");
                z3 = true;
            }
        }
        ape.a();
        int e2 = mh.e(context);
        ape.a();
        if (e2 <= mh.d(context)) {
            z2 = z3;
        }
        asi.a(context);
        if (((Boolean) ape.f().a(asi.de)).booleanValue()) {
            z2 = false;
        }
        if (z2) {
            T b2 = aVar.b();
            return b2 == null ? aVar.c() : b2;
        }
        T c2 = aVar.c();
        return c2 == null ? aVar.b() : c2;
    }

    /* access modifiers changed from: private */
    public static void a(Context context, String str) {
        Bundle bundle = new Bundle();
        bundle.putString("action", "no_ads_fallback");
        bundle.putString("flow", str);
        ape.a().a(context, (String) null, "gmob-apps", bundle, true);
    }

    /* access modifiers changed from: private */
    public final aqh b() {
        aqh aqh;
        synchronized (this.f2820b) {
            if (this.f2819a == null) {
                this.f2819a = a();
            }
            aqh = this.f2819a;
        }
        return aqh;
    }

    public final apq a(Context context, String str, bcr bcr) {
        return (apq) a(context, false, (a<T>) new apb<T>(this, context, str, bcr));
    }

    public final r a(Activity activity) {
        boolean z = false;
        String str = "com.google.android.gms.ads.internal.overlay.useClientJar";
        Intent intent = activity.getIntent();
        if (!intent.hasExtra(str)) {
            ms.c("useClientJar flag not found in activity intent extras.");
        } else {
            z = intent.getBooleanExtra(str, false);
        }
        return (r) a((Context) activity, z, (a<T>) new apd<T>(this, activity));
    }
}
