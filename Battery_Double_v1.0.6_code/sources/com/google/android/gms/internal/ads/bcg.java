package com.google.android.gms.internal.ads;

final class bcg implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ bce f3129a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ bcf f3130b;

    bcg(bcf bcf, bce bce) {
        this.f3130b = bcf;
        this.f3129a = bce;
    }

    public final void run() {
        synchronized (this.f3130b.i) {
            if (this.f3130b.s == -2) {
                this.f3130b.r = this.f3130b.d();
                if (this.f3130b.r == null) {
                    this.f3130b.a(4);
                } else if (!this.f3130b.e() || this.f3130b.b(1)) {
                    this.f3129a.a((bcj) this.f3130b);
                    this.f3130b.a(this.f3129a);
                } else {
                    String f = this.f3130b.f3127a;
                    jm.e(new StringBuilder(String.valueOf(f).length() + 56).append("Ignoring adapter ").append(f).append(" as delayed impression is not supported").toString());
                    this.f3130b.a(2);
                }
            }
        }
    }
}
