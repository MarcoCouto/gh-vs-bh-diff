package com.google.android.gms.internal.ads;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;

@cm
final class np {

    /* renamed from: a reason: collision with root package name */
    private final Object f3553a = new Object();

    /* renamed from: b reason: collision with root package name */
    private final List<Runnable> f3554b = new ArrayList();
    private boolean c = false;

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0026, code lost:
        r1 = r0.get(r2);
        r2 = r2 + 1;
        ((java.lang.Runnable) r1).run();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x001c, code lost:
        r0 = r0;
        r3 = r0.size();
        r2 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0024, code lost:
        if (r2 >= r3) goto L_0x000d;
     */
    public final void a() {
        ArrayList arrayList = new ArrayList();
        synchronized (this.f3553a) {
            if (!this.c) {
                arrayList.addAll(this.f3554b);
                this.f3554b.clear();
                this.c = true;
            }
        }
    }

    public final void a(Runnable runnable, Executor executor) {
        synchronized (this.f3553a) {
            if (this.c) {
                executor.execute(runnable);
            } else {
                this.f3554b.add(new nq(executor, runnable));
            }
        }
    }
}
