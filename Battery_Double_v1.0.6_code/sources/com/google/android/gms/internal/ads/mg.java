package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.ViewTreeObserver.OnScrollChangedListener;
import android.view.Window;
import com.google.android.gms.ads.internal.ax;

@cm
public final class mg {

    /* renamed from: a reason: collision with root package name */
    private final View f3512a;

    /* renamed from: b reason: collision with root package name */
    private Activity f3513b;
    private boolean c;
    private boolean d;
    private boolean e;
    private OnGlobalLayoutListener f;
    private OnScrollChangedListener g;

    public mg(Activity activity, View view, OnGlobalLayoutListener onGlobalLayoutListener, OnScrollChangedListener onScrollChangedListener) {
        this.f3513b = activity;
        this.f3512a = view;
        this.f = onGlobalLayoutListener;
        this.g = onScrollChangedListener;
    }

    private static ViewTreeObserver b(Activity activity) {
        if (activity == null) {
            return null;
        }
        Window window = activity.getWindow();
        if (window == null) {
            return null;
        }
        View decorView = window.getDecorView();
        if (decorView != null) {
            return decorView.getViewTreeObserver();
        }
        return null;
    }

    private final void e() {
        if (!this.c) {
            if (this.f != null) {
                if (this.f3513b != null) {
                    Activity activity = this.f3513b;
                    OnGlobalLayoutListener onGlobalLayoutListener = this.f;
                    ViewTreeObserver b2 = b(activity);
                    if (b2 != null) {
                        b2.addOnGlobalLayoutListener(onGlobalLayoutListener);
                    }
                }
                ax.A();
                og.a(this.f3512a, this.f);
            }
            if (this.g != null) {
                if (this.f3513b != null) {
                    Activity activity2 = this.f3513b;
                    OnScrollChangedListener onScrollChangedListener = this.g;
                    ViewTreeObserver b3 = b(activity2);
                    if (b3 != null) {
                        b3.addOnScrollChangedListener(onScrollChangedListener);
                    }
                }
                ax.A();
                og.a(this.f3512a, this.g);
            }
            this.c = true;
        }
    }

    private final void f() {
        if (this.f3513b != null && this.c) {
            if (this.f != null) {
                Activity activity = this.f3513b;
                OnGlobalLayoutListener onGlobalLayoutListener = this.f;
                ViewTreeObserver b2 = b(activity);
                if (b2 != null) {
                    ax.g().a(b2, onGlobalLayoutListener);
                }
            }
            if (this.g != null) {
                Activity activity2 = this.f3513b;
                OnScrollChangedListener onScrollChangedListener = this.g;
                ViewTreeObserver b3 = b(activity2);
                if (b3 != null) {
                    b3.removeOnScrollChangedListener(onScrollChangedListener);
                }
            }
            this.c = false;
        }
    }

    public final void a() {
        this.e = true;
        if (this.d) {
            e();
        }
    }

    public final void a(Activity activity) {
        this.f3513b = activity;
    }

    public final void b() {
        this.e = false;
        f();
    }

    public final void c() {
        this.d = true;
        if (this.e) {
            e();
        }
    }

    public final void d() {
        this.d = false;
        f();
    }
}
