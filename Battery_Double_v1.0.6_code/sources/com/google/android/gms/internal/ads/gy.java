package com.google.android.gms.internal.ads;

@cm
public final class gy {

    /* renamed from: a reason: collision with root package name */
    public final String f3352a;

    /* renamed from: b reason: collision with root package name */
    public final int f3353b;
    public final long c;
    private final String d;

    private gy(ha haVar) {
        this.d = haVar.f3356a;
        this.f3352a = haVar.f3357b;
        this.f3353b = haVar.c;
        this.c = haVar.d;
    }
}
