package com.google.android.gms.internal.ads;

import java.lang.reflect.InvocationTargetException;

public final class aip extends ajj {
    public aip(ahy ahy, String str, String str2, zz zzVar, int i, int i2) {
        super(ahy, str, str2, zzVar, i, 5);
    }

    /* access modifiers changed from: protected */
    public final void a() throws IllegalAccessException, InvocationTargetException {
        this.f2637b.d = Long.valueOf(-1);
        this.f2637b.e = Long.valueOf(-1);
        int[] iArr = (int[]) this.c.invoke(null, new Object[]{this.f2636a.a()});
        synchronized (this.f2637b) {
            this.f2637b.d = Long.valueOf((long) iArr[0]);
            this.f2637b.e = Long.valueOf((long) iArr[1]);
            if (iArr[2] != Integer.MIN_VALUE) {
                this.f2637b.N = Long.valueOf((long) iArr[2]);
            }
        }
    }
}
