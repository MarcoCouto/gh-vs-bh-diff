package com.google.android.gms.internal.ads;

import java.util.concurrent.Executor;

final class bao implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ bay f3078a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ azv f3079b;
    private final /* synthetic */ bah c;

    bao(bah bah, bay bay, azv azv) {
        this.c = bah;
        this.f3078a = bay;
        this.f3079b = azv;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        return;
     */
    public final void run() {
        synchronized (this.c.f3065a) {
            if (this.f3078a.b() != -1 && this.f3078a.b() != 1) {
                this.f3078a.a();
                Executor executor = nt.f3558a;
                azv azv = this.f3079b;
                azv.getClass();
                executor.execute(bap.a(azv));
                jm.a("Could not receive loaded message in a timely manner. Rejecting.");
            }
        }
    }
}
