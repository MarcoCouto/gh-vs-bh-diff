package com.google.android.gms.internal.ads;

final class am implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ ir f2719a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ ak f2720b;

    am(ak akVar, ir irVar) {
        this.f2720b = akVar;
        this.f2719a = irVar;
    }

    public final void run() {
        synchronized (this.f2720b.c) {
            ak akVar = this.f2720b;
            akVar.f2653a.b(this.f2719a);
        }
    }
}
