package com.google.android.gms.internal.ads;

import java.io.IOException;

abstract class aei<T, B> {
    aei() {
    }

    /* access modifiers changed from: 0000 */
    public abstract B a();

    /* access modifiers changed from: 0000 */
    public abstract T a(B b2);

    /* access modifiers changed from: 0000 */
    public abstract void a(B b2, int i, int i2);

    /* access modifiers changed from: 0000 */
    public abstract void a(B b2, int i, long j);

    /* access modifiers changed from: 0000 */
    public abstract void a(B b2, int i, aah aah);

    /* access modifiers changed from: 0000 */
    public abstract void a(B b2, int i, T t);

    /* access modifiers changed from: 0000 */
    public abstract void a(T t, afc afc) throws IOException;

    /* access modifiers changed from: 0000 */
    public abstract void a(Object obj, T t);

    /* access modifiers changed from: 0000 */
    public abstract boolean a(ado ado);

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x004e  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0053  */
    public final boolean a(B b2, ado ado) throws IOException {
        int b3 = ado.b();
        int i = b3 >>> 3;
        switch (b3 & 7) {
            case 0:
                a(b2, i, ado.g());
                return true;
            case 1:
                b(b2, i, ado.i());
                return true;
            case 2:
                a(b2, i, ado.n());
                return true;
            case 3:
                Object a2 = a();
                int i2 = (i << 3) | 4;
                while (ado.a() != Integer.MAX_VALUE) {
                    if (!a((B) a2, ado)) {
                        if (i2 == ado.b()) {
                            throw abv.e();
                        }
                        a(b2, i, (T) a((B) a2));
                        return true;
                    }
                }
                if (i2 == ado.b()) {
                }
            case 4:
                return false;
            case 5:
                a(b2, i, ado.j());
                return true;
            default:
                throw abv.f();
        }
    }

    /* access modifiers changed from: 0000 */
    public abstract T b(Object obj);

    /* access modifiers changed from: 0000 */
    public abstract void b(B b2, int i, long j);

    /* access modifiers changed from: 0000 */
    public abstract void b(T t, afc afc) throws IOException;

    /* access modifiers changed from: 0000 */
    public abstract void b(Object obj, B b2);

    /* access modifiers changed from: 0000 */
    public abstract B c(Object obj);

    /* access modifiers changed from: 0000 */
    public abstract T c(T t, T t2);

    /* access modifiers changed from: 0000 */
    public abstract void d(Object obj);

    /* access modifiers changed from: 0000 */
    public abstract int e(T t);

    /* access modifiers changed from: 0000 */
    public abstract int f(T t);
}
