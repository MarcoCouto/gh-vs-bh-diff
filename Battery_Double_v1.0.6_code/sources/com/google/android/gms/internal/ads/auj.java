package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.gmsg.ae;
import java.util.Map;

final class auj implements ae<Object> {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ bq f2933a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ auf f2934b;

    auj(auf auf, bq bqVar) {
        this.f2934b = auf;
        this.f2933a = bqVar;
    }

    public final void zza(Object obj, Map<String, String> map) {
        qn qnVar = (qn) this.f2934b.f2925a.get();
        if (qnVar == null) {
            this.f2933a.b("/hideOverlay", this);
        } else {
            qnVar.getView().setVisibility(8);
        }
    }
}
