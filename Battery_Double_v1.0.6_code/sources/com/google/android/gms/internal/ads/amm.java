package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.ParcelFileDescriptor.AutoCloseInputStream;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.a.a;
import com.google.android.gms.common.internal.a.c;
import java.io.InputStream;

@cm
public final class amm extends a {
    public static final Creator<amm> CREATOR = new amn();

    /* renamed from: a reason: collision with root package name */
    private ParcelFileDescriptor f2736a;

    public amm() {
        this(null);
    }

    public amm(ParcelFileDescriptor parcelFileDescriptor) {
        this.f2736a = parcelFileDescriptor;
    }

    private final synchronized ParcelFileDescriptor c() {
        return this.f2736a;
    }

    public final synchronized boolean a() {
        return this.f2736a != null;
    }

    public final synchronized InputStream b() {
        AutoCloseInputStream autoCloseInputStream = null;
        synchronized (this) {
            if (this.f2736a != null) {
                autoCloseInputStream = new AutoCloseInputStream(this.f2736a);
                this.f2736a = null;
            }
        }
        return autoCloseInputStream;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = c.a(parcel);
        c.a(parcel, 2, (Parcelable) c(), i, false);
        c.a(parcel, a2);
    }
}
