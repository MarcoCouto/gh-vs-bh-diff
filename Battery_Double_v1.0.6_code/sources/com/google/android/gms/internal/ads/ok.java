package com.google.android.gms.internal.ads;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.SurfaceTexture;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnBufferingUpdateListener;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnInfoListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.MediaPlayer.OnVideoSizeChangedListener;
import android.net.Uri;
import android.os.Build.VERSION;
import android.view.Surface;
import android.view.TextureView.SurfaceTextureListener;
import android.view.View.MeasureSpec;
import com.google.android.gms.ads.internal.ax;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@TargetApi(14)
@cm
public final class ok extends ow implements OnBufferingUpdateListener, OnCompletionListener, OnErrorListener, OnInfoListener, OnPreparedListener, OnVideoSizeChangedListener, SurfaceTextureListener {
    private static final Map<Integer, String> c = new HashMap();
    private final pn d;
    private final boolean e;
    private int f = 0;
    private int g = 0;
    private MediaPlayer h;
    private Uri i;
    private int j;
    private int k;
    private int l;
    private int m;
    private int n;
    private pk o;
    private boolean p;
    private int q;
    /* access modifiers changed from: private */
    public ov r;

    static {
        if (VERSION.SDK_INT >= 17) {
            c.put(Integer.valueOf(-1004), "MEDIA_ERROR_IO");
            c.put(Integer.valueOf(-1007), "MEDIA_ERROR_MALFORMED");
            c.put(Integer.valueOf(-1010), "MEDIA_ERROR_UNSUPPORTED");
            c.put(Integer.valueOf(-110), "MEDIA_ERROR_TIMED_OUT");
            c.put(Integer.valueOf(3), "MEDIA_INFO_VIDEO_RENDERING_START");
        }
        c.put(Integer.valueOf(100), "MEDIA_ERROR_SERVER_DIED");
        c.put(Integer.valueOf(1), "MEDIA_ERROR_UNKNOWN");
        c.put(Integer.valueOf(1), "MEDIA_INFO_UNKNOWN");
        c.put(Integer.valueOf(700), "MEDIA_INFO_VIDEO_TRACK_LAGGING");
        c.put(Integer.valueOf(701), "MEDIA_INFO_BUFFERING_START");
        c.put(Integer.valueOf(702), "MEDIA_INFO_BUFFERING_END");
        c.put(Integer.valueOf(800), "MEDIA_INFO_BAD_INTERLEAVING");
        c.put(Integer.valueOf(801), "MEDIA_INFO_NOT_SEEKABLE");
        c.put(Integer.valueOf(802), "MEDIA_INFO_METADATA_UPDATE");
        if (VERSION.SDK_INT >= 19) {
            c.put(Integer.valueOf(901), "MEDIA_INFO_UNSUPPORTED_SUBTITLE");
            c.put(Integer.valueOf(902), "MEDIA_INFO_SUBTITLE_TIMED_OUT");
        }
    }

    public ok(Context context, boolean z, boolean z2, pl plVar, pn pnVar) {
        super(context);
        setSurfaceTextureListener(this);
        this.d = pnVar;
        this.p = z;
        this.e = z2;
        this.d.a(this);
    }

    private final void a(float f2) {
        if (this.h != null) {
            try {
                this.h.setVolume(f2, f2);
            } catch (IllegalStateException e2) {
            }
        } else {
            jm.e("AdMediaPlayerView setMediaPlayerVolume() called before onPrepared().");
        }
    }

    private final void a(boolean z) {
        jm.a("AdMediaPlayerView release");
        if (this.o != null) {
            this.o.b();
            this.o = null;
        }
        if (this.h != null) {
            this.h.reset();
            this.h.release();
            this.h = null;
            c(0);
            if (z) {
                this.g = 0;
                this.g = 0;
            }
        }
    }

    private final void c(int i2) {
        if (i2 == 3) {
            this.d.c();
            this.f3585b.b();
        } else if (this.f == 3) {
            this.d.d();
            this.f3585b.c();
        }
        this.f = i2;
    }

    private final void f() {
        SurfaceTexture surfaceTexture;
        jm.a("AdMediaPlayerView init MediaPlayer");
        SurfaceTexture surfaceTexture2 = getSurfaceTexture();
        if (this.i != null && surfaceTexture2 != null) {
            a(false);
            try {
                ax.v();
                this.h = new MediaPlayer();
                this.h.setOnBufferingUpdateListener(this);
                this.h.setOnCompletionListener(this);
                this.h.setOnErrorListener(this);
                this.h.setOnInfoListener(this);
                this.h.setOnPreparedListener(this);
                this.h.setOnVideoSizeChangedListener(this);
                this.l = 0;
                if (this.p) {
                    this.o = new pk(getContext());
                    this.o.a(surfaceTexture2, getWidth(), getHeight());
                    this.o.start();
                    surfaceTexture = this.o.c();
                    if (surfaceTexture == null) {
                        this.o.b();
                        this.o = null;
                    }
                    this.h.setDataSource(getContext(), this.i);
                    ax.w();
                    this.h.setSurface(new Surface(surfaceTexture));
                    this.h.setAudioStreamType(3);
                    this.h.setScreenOnWhilePlaying(true);
                    this.h.prepareAsync();
                    c(1);
                }
                surfaceTexture = surfaceTexture2;
                this.h.setDataSource(getContext(), this.i);
                ax.w();
                this.h.setSurface(new Surface(surfaceTexture));
                this.h.setAudioStreamType(3);
                this.h.setScreenOnWhilePlaying(true);
                this.h.prepareAsync();
                c(1);
            } catch (IOException | IllegalArgumentException | IllegalStateException e2) {
                String valueOf = String.valueOf(this.i);
                jm.c(new StringBuilder(String.valueOf(valueOf).length() + 36).append("Failed to initialize MediaPlayer at ").append(valueOf).toString(), e2);
                onError(this.h, 1, 0);
            }
        }
    }

    private final void g() {
        if (this.e && h() && this.h.getCurrentPosition() > 0 && this.g != 3) {
            jm.a("AdMediaPlayerView nudging MediaPlayer");
            a(0.0f);
            this.h.start();
            int currentPosition = this.h.getCurrentPosition();
            long a2 = ax.l().a();
            while (h() && this.h.getCurrentPosition() == currentPosition) {
                if (ax.l().a() - a2 > 250) {
                    break;
                }
            }
            this.h.pause();
            e();
        }
    }

    private final boolean h() {
        return (this.h == null || this.f == -1 || this.f == 0 || this.f == 1) ? false : true;
    }

    public final String a() {
        String str = "MediaPlayer";
        String valueOf = String.valueOf(this.p ? " spherical" : "");
        return valueOf.length() != 0 ? str.concat(valueOf) : new String(str);
    }

    public final void a(float f2, float f3) {
        if (this.o != null) {
            this.o.a(f2, f3);
        }
    }

    public final void a(int i2) {
        jm.a("AdMediaPlayerView seek " + i2);
        if (h()) {
            this.h.seekTo(i2);
            this.q = 0;
            return;
        }
        this.q = i2;
    }

    public final void a(ov ovVar) {
        this.r = ovVar;
    }

    public final void b() {
        jm.a("AdMediaPlayerView stop");
        if (this.h != null) {
            this.h.stop();
            this.h.release();
            this.h = null;
            c(0);
            this.g = 0;
        }
        this.d.b();
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ void b(int i2) {
        if (this.r != null) {
            this.r.onWindowVisibilityChanged(i2);
        }
    }

    public final void c() {
        jm.a("AdMediaPlayerView play");
        if (h()) {
            this.h.start();
            c(3);
            this.f3584a.a();
            jv.f3440a.post(new ot(this));
        }
        this.g = 3;
    }

    public final void d() {
        jm.a("AdMediaPlayerView pause");
        if (h() && this.h.isPlaying()) {
            this.h.pause();
            c(4);
            jv.f3440a.post(new ou(this));
        }
        this.g = 4;
    }

    public final void e() {
        a(this.f3585b.a());
    }

    public final int getCurrentPosition() {
        if (h()) {
            return this.h.getCurrentPosition();
        }
        return 0;
    }

    public final int getDuration() {
        if (h()) {
            return this.h.getDuration();
        }
        return -1;
    }

    public final int getVideoHeight() {
        if (this.h != null) {
            return this.h.getVideoHeight();
        }
        return 0;
    }

    public final int getVideoWidth() {
        if (this.h != null) {
            return this.h.getVideoWidth();
        }
        return 0;
    }

    public final void onBufferingUpdate(MediaPlayer mediaPlayer, int i2) {
        this.l = i2;
    }

    public final void onCompletion(MediaPlayer mediaPlayer) {
        jm.a("AdMediaPlayerView completion");
        c(5);
        this.g = 5;
        jv.f3440a.post(new on(this));
    }

    public final boolean onError(MediaPlayer mediaPlayer, int i2, int i3) {
        String str = (String) c.get(Integer.valueOf(i2));
        String str2 = (String) c.get(Integer.valueOf(i3));
        jm.e(new StringBuilder(String.valueOf(str).length() + 38 + String.valueOf(str2).length()).append("AdMediaPlayerView MediaPlayer error: ").append(str).append(":").append(str2).toString());
        c(-1);
        this.g = -1;
        jv.f3440a.post(new oo(this, str, str2));
        return true;
    }

    public final boolean onInfo(MediaPlayer mediaPlayer, int i2, int i3) {
        String str = (String) c.get(Integer.valueOf(i2));
        String str2 = (String) c.get(Integer.valueOf(i3));
        jm.a(new StringBuilder(String.valueOf(str).length() + 37 + String.valueOf(str2).length()).append("AdMediaPlayerView MediaPlayer info: ").append(str).append(":").append(str2).toString());
        return true;
    }

    /* access modifiers changed from: protected */
    public final void onMeasure(int i2, int i3) {
        int defaultSize = getDefaultSize(this.j, i2);
        int defaultSize2 = getDefaultSize(this.k, i3);
        if (this.j > 0 && this.k > 0 && this.o == null) {
            int mode = MeasureSpec.getMode(i2);
            int size = MeasureSpec.getSize(i2);
            int mode2 = MeasureSpec.getMode(i3);
            defaultSize2 = MeasureSpec.getSize(i3);
            if (mode == 1073741824 && mode2 == 1073741824) {
                if (this.j * defaultSize2 < this.k * size) {
                    defaultSize = (this.j * defaultSize2) / this.k;
                } else if (this.j * defaultSize2 > this.k * size) {
                    defaultSize2 = (this.k * size) / this.j;
                    defaultSize = size;
                } else {
                    defaultSize = size;
                }
            } else if (mode == 1073741824) {
                int i4 = (this.k * size) / this.j;
                if (mode2 != Integer.MIN_VALUE || i4 <= defaultSize2) {
                    defaultSize2 = i4;
                    defaultSize = size;
                } else {
                    defaultSize = size;
                }
            } else if (mode2 == 1073741824) {
                defaultSize = (this.j * defaultSize2) / this.k;
                if (mode == Integer.MIN_VALUE && defaultSize > size) {
                    defaultSize = size;
                }
            } else {
                int i5 = this.j;
                int i6 = this.k;
                if (mode2 != Integer.MIN_VALUE || i6 <= defaultSize2) {
                    defaultSize2 = i6;
                    defaultSize = i5;
                } else {
                    defaultSize = (this.j * defaultSize2) / this.k;
                }
                if (mode == Integer.MIN_VALUE && defaultSize > size) {
                    defaultSize2 = (this.k * size) / this.j;
                    defaultSize = size;
                }
            }
        }
        setMeasuredDimension(defaultSize, defaultSize2);
        if (this.o != null) {
            this.o.a(defaultSize, defaultSize2);
        }
        if (VERSION.SDK_INT == 16) {
            if ((this.m > 0 && this.m != defaultSize) || (this.n > 0 && this.n != defaultSize2)) {
                g();
            }
            this.m = defaultSize;
            this.n = defaultSize2;
        }
    }

    public final void onPrepared(MediaPlayer mediaPlayer) {
        jm.a("AdMediaPlayerView prepared");
        c(2);
        this.d.a();
        jv.f3440a.post(new om(this));
        this.j = mediaPlayer.getVideoWidth();
        this.k = mediaPlayer.getVideoHeight();
        if (this.q != 0) {
            a(this.q);
        }
        g();
        int i2 = this.j;
        jm.d("AdMediaPlayerView stream dimensions: " + i2 + " x " + this.k);
        if (this.g == 3) {
            c();
        }
        e();
    }

    public final void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i2, int i3) {
        jm.a("AdMediaPlayerView surface created");
        f();
        jv.f3440a.post(new oq(this));
    }

    public final boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
        jm.a("AdMediaPlayerView surface destroyed");
        if (this.h != null && this.q == 0) {
            this.q = this.h.getCurrentPosition();
        }
        if (this.o != null) {
            this.o.b();
        }
        jv.f3440a.post(new os(this));
        a(true);
        return true;
    }

    public final void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i2, int i3) {
        boolean z = true;
        jm.a("AdMediaPlayerView surface changed");
        boolean z2 = this.g == 3;
        if (!(this.j == i2 && this.k == i3)) {
            z = false;
        }
        if (this.h != null && z2 && z) {
            if (this.q != 0) {
                a(this.q);
            }
            c();
        }
        if (this.o != null) {
            this.o.a(i2, i3);
        }
        jv.f3440a.post(new or(this, i2, i3));
    }

    public final void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
        this.d.b(this);
        this.f3584a.a(surfaceTexture, this.r);
    }

    public final void onVideoSizeChanged(MediaPlayer mediaPlayer, int i2, int i3) {
        jm.a("AdMediaPlayerView size changed: " + i2 + " x " + i3);
        this.j = mediaPlayer.getVideoWidth();
        this.k = mediaPlayer.getVideoHeight();
        if (this.j != 0 && this.k != 0) {
            requestLayout();
        }
    }

    /* access modifiers changed from: protected */
    public final void onWindowVisibilityChanged(int i2) {
        jm.a("AdMediaPlayerView window visibility changed to " + i2);
        jv.f3440a.post(new ol(this, i2));
        super.onWindowVisibilityChanged(i2);
    }

    public final void setVideoPath(String str) {
        Uri parse = Uri.parse(str);
        amp a2 = amp.a(parse);
        if (a2 != null) {
            parse = Uri.parse(a2.f2737a);
        }
        this.i = parse;
        this.q = 0;
        f();
        requestLayout();
        invalidate();
    }

    public final String toString() {
        String name = getClass().getName();
        String hexString = Integer.toHexString(hashCode());
        return new StringBuilder(String.valueOf(name).length() + 1 + String.valueOf(hexString).length()).append(name).append("@").append(hexString).toString();
    }
}
