package com.google.android.gms.internal.ads;

import android.os.RemoteException;

final class axv implements ays {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ int f2995a;

    axv(axt axt, int i) {
        this.f2995a = i;
    }

    public final void a(ayt ayt) throws RemoteException {
        if (ayt.f3009a != null) {
            ayt.f3009a.a(this.f2995a);
        }
    }
}
