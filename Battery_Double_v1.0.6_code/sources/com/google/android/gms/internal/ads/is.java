package com.google.android.gms.internal.ads;

import org.json.JSONObject;

@cm
public final class is {

    /* renamed from: a reason: collision with root package name */
    public final dl f3397a;

    /* renamed from: b reason: collision with root package name */
    public final dp f3398b;
    public final bcb c;
    public final aot d;
    public final int e;
    public final long f;
    public final long g;
    public final JSONObject h;
    public final amw i;
    public final boolean j;

    public is(dl dlVar, dp dpVar, bcb bcb, aot aot, int i2, long j2, long j3, JSONObject jSONObject, amw amw, Boolean bool) {
        this.f3397a = dlVar;
        this.f3398b = dpVar;
        this.c = bcb;
        this.d = aot;
        this.e = i2;
        this.f = j2;
        this.g = j3;
        this.h = jSONObject;
        this.i = amw;
        if (bool != null) {
            this.j = bool.booleanValue();
        } else if (lz.a(dlVar.c)) {
            this.j = true;
        } else {
            this.j = false;
        }
    }

    public is(dl dlVar, dp dpVar, bcb bcb, aot aot, int i2, long j2, long j3, JSONObject jSONObject, anb anb) {
        this.f3397a = dlVar;
        this.f3398b = dpVar;
        this.c = null;
        this.d = null;
        this.e = i2;
        this.f = j2;
        this.g = j3;
        this.h = null;
        this.i = new amw(anb);
        this.j = false;
    }
}
