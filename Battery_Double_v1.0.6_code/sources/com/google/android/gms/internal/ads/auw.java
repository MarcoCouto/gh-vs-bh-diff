package com.google.android.gms.internal.ads;

import android.net.Uri;
import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.b.a;

public interface auw extends IInterface {
    a a() throws RemoteException;

    Uri b() throws RemoteException;

    double c() throws RemoteException;
}
