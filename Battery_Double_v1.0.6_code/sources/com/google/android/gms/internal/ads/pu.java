package com.google.android.gms.internal.ads;

import android.text.TextUtils;
import com.google.android.gms.ads.internal.ax;
import com.google.android.gms.ads.internal.gmsg.ae;
import java.util.Map;

@cm
public final class pu implements ae<pm> {
    public final /* synthetic */ void zza(Object obj, Map map) {
        rd rdVar;
        pm pmVar = (pm) obj;
        if (((Boolean) ape.f().a(asi.bu)).booleanValue()) {
            rd b2 = pmVar.b();
            if (b2 == null) {
                try {
                    rd rdVar2 = new rd(pmVar, Float.parseFloat((String) map.get("duration")), "1".equals(map.get("customControlsAllowed")), "1".equals(map.get("clickToExpandAllowed")));
                    pmVar.a(rdVar2);
                    rdVar = rdVar2;
                } catch (NullPointerException | NumberFormatException e) {
                    jm.b("Unable to parse videoMeta message.", e);
                    ax.i().a(e, "VideoMetaGmsgHandler.onGmsg");
                    return;
                }
            } else {
                rdVar = b2;
            }
            boolean equals = "1".equals(map.get("muted"));
            float parseFloat = Float.parseFloat((String) map.get("currentTime"));
            int parseInt = Integer.parseInt((String) map.get("playbackState"));
            int i = (parseInt < 0 || 3 < parseInt) ? 0 : parseInt;
            String str = (String) map.get("aspectRatio");
            float parseFloat2 = TextUtils.isEmpty(str) ? 0.0f : Float.parseFloat(str);
            if (jm.a(3)) {
                jm.b(new StringBuilder(String.valueOf(str).length() + 79).append("Video Meta GMSG: isMuted : ").append(equals).append(" , playbackState : ").append(i).append(" , aspectRatio : ").append(str).toString());
            }
            rdVar.a(parseFloat, i, equals, parseFloat2);
        }
    }
}
