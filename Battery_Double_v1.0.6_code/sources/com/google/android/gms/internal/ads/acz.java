package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.abp.c;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeSet;

final class acz {
    static String a(acw acw, String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("# ").append(str);
        a(acw, sb, 0);
        return sb.toString();
    }

    private static final String a(String str) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < str.length(); i++) {
            char charAt = str.charAt(i);
            if (Character.isUpperCase(charAt)) {
                sb.append("_");
            }
            sb.append(Character.toLowerCase(charAt));
        }
        return sb.toString();
    }

    private static void a(acw acw, StringBuilder sb, int i) {
        Method[] declaredMethods;
        boolean booleanValue;
        HashMap hashMap = new HashMap();
        HashMap hashMap2 = new HashMap();
        TreeSet<String> treeSet = new TreeSet<>();
        for (Method method : acw.getClass().getDeclaredMethods()) {
            hashMap2.put(method.getName(), method);
            if (method.getParameterTypes().length == 0) {
                hashMap.put(method.getName(), method);
                if (method.getName().startsWith("get")) {
                    treeSet.add(method.getName());
                }
            }
        }
        for (String str : treeSet) {
            String replaceFirst = str.replaceFirst("get", "");
            if (replaceFirst.endsWith("List") && !replaceFirst.endsWith("OrBuilderList") && !replaceFirst.equals("List")) {
                String valueOf = String.valueOf(replaceFirst.substring(0, 1).toLowerCase());
                String valueOf2 = String.valueOf(replaceFirst.substring(1, replaceFirst.length() - 4));
                String str2 = valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf);
                Method method2 = (Method) hashMap.get(str);
                if (method2 != null && method2.getReturnType().equals(List.class)) {
                    a(sb, i, a(str2), abp.a(method2, (Object) acw, new Object[0]));
                }
            }
            if (replaceFirst.endsWith("Map") && !replaceFirst.equals("Map")) {
                String valueOf3 = String.valueOf(replaceFirst.substring(0, 1).toLowerCase());
                String valueOf4 = String.valueOf(replaceFirst.substring(1, replaceFirst.length() - 3));
                String str3 = valueOf4.length() != 0 ? valueOf3.concat(valueOf4) : new String(valueOf3);
                Method method3 = (Method) hashMap.get(str);
                if (method3 != null && method3.getReturnType().equals(Map.class) && !method3.isAnnotationPresent(Deprecated.class) && Modifier.isPublic(method3.getModifiers())) {
                    a(sb, i, a(str3), abp.a(method3, (Object) acw, new Object[0]));
                }
            }
            String str4 = "set";
            String valueOf5 = String.valueOf(replaceFirst);
            if (((Method) hashMap2.get(valueOf5.length() != 0 ? str4.concat(valueOf5) : new String(str4))) != null) {
                if (replaceFirst.endsWith("Bytes")) {
                    String str5 = "get";
                    String valueOf6 = String.valueOf(replaceFirst.substring(0, replaceFirst.length() - 5));
                    if (hashMap.containsKey(valueOf6.length() != 0 ? str5.concat(valueOf6) : new String(str5))) {
                    }
                }
                String valueOf7 = String.valueOf(replaceFirst.substring(0, 1).toLowerCase());
                String valueOf8 = String.valueOf(replaceFirst.substring(1));
                String str6 = valueOf8.length() != 0 ? valueOf7.concat(valueOf8) : new String(valueOf7);
                String str7 = "get";
                String valueOf9 = String.valueOf(replaceFirst);
                Method method4 = (Method) hashMap.get(valueOf9.length() != 0 ? str7.concat(valueOf9) : new String(str7));
                String str8 = "has";
                String valueOf10 = String.valueOf(replaceFirst);
                Method method5 = (Method) hashMap.get(valueOf10.length() != 0 ? str8.concat(valueOf10) : new String(str8));
                if (method4 != null) {
                    Object a2 = abp.a(method4, (Object) acw, new Object[0]);
                    if (method5 == null) {
                        boolean z = a2 instanceof Boolean ? !((Boolean) a2).booleanValue() : a2 instanceof Integer ? ((Integer) a2).intValue() == 0 : a2 instanceof Float ? ((Float) a2).floatValue() == 0.0f : a2 instanceof Double ? ((Double) a2).doubleValue() == 0.0d : a2 instanceof String ? a2.equals("") : a2 instanceof aah ? a2.equals(aah.f2393a) : a2 instanceof acw ? a2 == ((acw) a2).p() : a2 instanceof Enum ? ((Enum) a2).ordinal() == 0 : false;
                        booleanValue = !z;
                    } else {
                        booleanValue = ((Boolean) abp.a(method5, (Object) acw, new Object[0])).booleanValue();
                    }
                    if (booleanValue) {
                        a(sb, i, a(str6), a2);
                    }
                }
            }
        }
        if (acw instanceof c) {
            Iterator e = ((c) acw).zzdtz.e();
            if (e.hasNext()) {
                ((Entry) e.next()).getKey();
                throw new NoSuchMethodError();
            }
        }
        if (((abp) acw).zzdtt != null) {
            ((abp) acw).zzdtt.a(sb, i);
        }
    }

    static final void a(StringBuilder sb, int i, String str, Object obj) {
        int i2 = 0;
        if (obj instanceof List) {
            for (Object a2 : (List) obj) {
                a(sb, i, str, a2);
            }
        } else if (obj instanceof Map) {
            for (Entry a3 : ((Map) obj).entrySet()) {
                a(sb, i, str, a3);
            }
        } else {
            sb.append(10);
            for (int i3 = 0; i3 < i; i3++) {
                sb.append(' ');
            }
            sb.append(str);
            if (obj instanceof String) {
                sb.append(": \"").append(aee.a(aah.a((String) obj))).append('\"');
            } else if (obj instanceof aah) {
                sb.append(": \"").append(aee.a((aah) obj)).append('\"');
            } else if (obj instanceof abp) {
                sb.append(" {");
                a((abp) obj, sb, i + 2);
                sb.append("\n");
                while (i2 < i) {
                    sb.append(' ');
                    i2++;
                }
                sb.append("}");
            } else if (obj instanceof Entry) {
                sb.append(" {");
                Entry entry = (Entry) obj;
                a(sb, i + 2, "key", entry.getKey());
                a(sb, i + 2, "value", entry.getValue());
                sb.append("\n");
                while (i2 < i) {
                    sb.append(' ');
                    i2++;
                }
                sb.append("}");
            } else {
                sb.append(": ").append(obj.toString());
            }
        }
    }
}
