package com.google.android.gms.internal.ads;

@cm
public final class als {

    /* renamed from: a reason: collision with root package name */
    private final float f2710a;

    /* renamed from: b reason: collision with root package name */
    private final float f2711b;
    private final float c;
    private final float d;
    private final int e;

    public als(float f, float f2, float f3, float f4, int i) {
        this.f2710a = f;
        this.f2711b = f2;
        this.c = f + f3;
        this.d = f2 + f4;
        this.e = i;
    }

    /* access modifiers changed from: 0000 */
    public final float a() {
        return this.f2710a;
    }

    /* access modifiers changed from: 0000 */
    public final float b() {
        return this.f2711b;
    }

    /* access modifiers changed from: 0000 */
    public final float c() {
        return this.c;
    }

    /* access modifiers changed from: 0000 */
    public final float d() {
        return this.d;
    }

    /* access modifiers changed from: 0000 */
    public final int e() {
        return this.e;
    }
}
