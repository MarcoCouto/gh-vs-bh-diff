package com.google.android.gms.internal.ads;

import java.util.ArrayList;
import java.util.List;

public final class le {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public final List<String> f3475a = new ArrayList();
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public final List<Double> f3476b = new ArrayList();
    /* access modifiers changed from: private */
    public final List<Double> c = new ArrayList();

    public final lb a() {
        return new lb(this);
    }

    public final le a(String str, double d, double d2) {
        int i;
        int i2 = 0;
        while (true) {
            i = i2;
            if (i >= this.f3475a.size()) {
                break;
            }
            double doubleValue = ((Double) this.c.get(i)).doubleValue();
            double doubleValue2 = ((Double) this.f3476b.get(i)).doubleValue();
            if (d < doubleValue || (doubleValue == d && d2 < doubleValue2)) {
                break;
            }
            i2 = i + 1;
        }
        this.f3475a.add(i, str);
        this.c.add(i, Double.valueOf(d));
        this.f3476b.add(i, Double.valueOf(d2));
        return this;
    }
}
