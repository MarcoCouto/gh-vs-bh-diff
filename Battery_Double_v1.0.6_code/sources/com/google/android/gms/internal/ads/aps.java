package com.google.android.gms.internal.ads;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.google.android.gms.ads.b.j;

public final class aps extends ajk implements apq {
    aps(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.client.IAdLoaderBuilder");
    }

    public final apn a() throws RemoteException {
        apn app;
        Parcel a2 = a(1, r_());
        IBinder readStrongBinder = a2.readStrongBinder();
        if (readStrongBinder == null) {
            app = null;
        } else {
            IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.IAdLoader");
            app = queryLocalInterface instanceof apn ? (apn) queryLocalInterface : new app(readStrongBinder);
        }
        a2.recycle();
        return app;
    }

    public final void a(j jVar) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (Parcelable) jVar);
        b(9, r_);
    }

    public final void a(apk apk) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (IInterface) apk);
        b(2, r_);
    }

    public final void a(aqk aqk) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (IInterface) aqk);
        b(7, r_);
    }

    public final void a(aul aul) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (Parcelable) aul);
        b(6, r_);
    }

    public final void a(avx avx) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (IInterface) avx);
        b(3, r_);
    }

    public final void a(awa awa) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (IInterface) awa);
        b(4, r_);
    }

    public final void a(awk awk, aot aot) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (IInterface) awk);
        ajm.a(r_, (Parcelable) aot);
        b(8, r_);
    }

    public final void a(awn awn) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (IInterface) awn);
        b(10, r_);
    }

    public final void a(String str, awh awh, awe awe) throws RemoteException {
        Parcel r_ = r_();
        r_.writeString(str);
        ajm.a(r_, (IInterface) awh);
        ajm.a(r_, (IInterface) awe);
        b(5, r_);
    }
}
