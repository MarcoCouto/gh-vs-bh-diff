package com.google.android.gms.internal.ads;

import android.os.IBinder;
import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.ads.b.c.a;
import com.google.android.gms.ads.b.c.b;
import java.util.ArrayList;
import java.util.List;

@cm
public final class auv extends a {

    /* renamed from: a reason: collision with root package name */
    private final aus f2949a;

    /* renamed from: b reason: collision with root package name */
    private final List<b> f2950b = new ArrayList();
    private String c;

    /* JADX WARNING: Removed duplicated region for block: B:15:0x003e A[Catch:{ RemoteException -> 0x0049 }] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x001c A[SYNTHETIC] */
    public auv(aus aus) {
        auw auw;
        this.f2949a = aus;
        try {
            this.c = this.f2949a.a();
        } catch (RemoteException e) {
            ms.b("", e);
            this.c = "";
        }
        try {
            for (Object next : aus.b()) {
                if (next instanceof IBinder) {
                    IBinder iBinder = (IBinder) next;
                    if (iBinder != null) {
                        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.ads.internal.formats.client.INativeAdImage");
                        auw = queryLocalInterface instanceof auw ? (auw) queryLocalInterface : new auy(iBinder);
                        if (auw == null) {
                            this.f2950b.add(new auz(auw));
                        }
                    }
                }
                auw = null;
                if (auw == null) {
                }
            }
        } catch (RemoteException e2) {
            ms.b("", e2);
        }
    }
}
