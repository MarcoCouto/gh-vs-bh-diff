package com.google.android.gms.internal.ads;

import java.security.GeneralSecurityException;

final class uy implements tt<ty> {
    public final tz<ty> a(String str, String str2, int i) throws GeneralSecurityException {
        boolean z;
        char c = 65535;
        String lowerCase = str2.toLowerCase();
        switch (lowerCase.hashCode()) {
            case 1420614889:
                if (lowerCase.equals("hybridencrypt")) {
                    z = false;
                    break;
                }
            default:
                z = true;
                break;
        }
        switch (z) {
            case false:
                switch (str.hashCode()) {
                    case 396454335:
                        if (str.equals("type.googleapis.com/google.crypto.tink.EciesAeadHkdfPublicKey")) {
                            c = 0;
                            break;
                        }
                        break;
                }
                switch (c) {
                    case 0:
                        uv uvVar = new uv();
                        if (i <= 0) {
                            return uvVar;
                        }
                        throw new GeneralSecurityException(String.format("No key manager for key type '%s' with version at least %d.", new Object[]{str, Integer.valueOf(i)}));
                    default:
                        throw new GeneralSecurityException(String.format("No support for primitive 'HybridEncrypt' with key type '%s'.", new Object[]{str}));
                }
            default:
                throw new GeneralSecurityException(String.format("No support for primitive '%s'.", new Object[]{str2}));
        }
    }
}
