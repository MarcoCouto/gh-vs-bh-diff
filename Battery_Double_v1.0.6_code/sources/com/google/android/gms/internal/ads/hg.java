package com.google.android.gms.internal.ads;

final /* synthetic */ class hg implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final hf f3362a;

    /* renamed from: b reason: collision with root package name */
    private final ir f3363b;

    hg(hf hfVar, ir irVar) {
        this.f3362a = hfVar;
        this.f3363b = irVar;
    }

    public final void run() {
        this.f3362a.b(this.f3363b);
    }
}
