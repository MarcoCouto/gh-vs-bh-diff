package com.google.android.gms.internal.ads;

import com.google.android.gms.common.internal.z;

@cm
public final class fx extends gd {

    /* renamed from: a reason: collision with root package name */
    private final String f3332a;

    /* renamed from: b reason: collision with root package name */
    private final int f3333b;

    public fx(String str, int i) {
        this.f3332a = str;
        this.f3333b = i;
    }

    public final String a() {
        return this.f3332a;
    }

    public final int b() {
        return this.f3333b;
    }

    public final boolean equals(Object obj) {
        if (obj == null || !(obj instanceof fx)) {
            return false;
        }
        fx fxVar = (fx) obj;
        return z.a(this.f3332a, fxVar.f3332a) && z.a(Integer.valueOf(this.f3333b), Integer.valueOf(fxVar.f3333b));
    }
}
