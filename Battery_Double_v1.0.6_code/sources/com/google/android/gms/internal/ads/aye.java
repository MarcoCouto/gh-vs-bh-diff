package com.google.android.gms.internal.ads;

import android.os.RemoteException;

final class aye extends atd {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ axs f3001a;

    aye(axs axs) {
        this.f3001a = axs;
    }

    public final void a(asz asz) throws RemoteException {
        this.f3001a.f2993a.add(new ayf(this, asz));
    }
}
