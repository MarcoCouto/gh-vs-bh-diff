package com.google.android.gms.internal.ads;

import java.util.Arrays;
import java.util.Collection;
import java.util.RandomAccess;

final class aay extends aab<Double> implements abu<Double>, RandomAccess {

    /* renamed from: a reason: collision with root package name */
    private static final aay f2409a;

    /* renamed from: b reason: collision with root package name */
    private double[] f2410b;
    private int c;

    static {
        aay aay = new aay();
        f2409a = aay;
        aay.b();
    }

    aay() {
        this(new double[10], 0);
    }

    private aay(double[] dArr, int i) {
        this.f2410b = dArr;
        this.c = i;
    }

    private final void a(int i, double d) {
        c();
        if (i < 0 || i > this.c) {
            throw new IndexOutOfBoundsException(c(i));
        }
        if (this.c < this.f2410b.length) {
            System.arraycopy(this.f2410b, i, this.f2410b, i + 1, this.c - i);
        } else {
            double[] dArr = new double[(((this.c * 3) / 2) + 1)];
            System.arraycopy(this.f2410b, 0, dArr, 0, i);
            System.arraycopy(this.f2410b, i, dArr, i + 1, this.c - i);
            this.f2410b = dArr;
        }
        this.f2410b[i] = d;
        this.c++;
        this.modCount++;
    }

    private final void b(int i) {
        if (i < 0 || i >= this.c) {
            throw new IndexOutOfBoundsException(c(i));
        }
    }

    private final String c(int i) {
        return "Index:" + i + ", Size:" + this.c;
    }

    public final /* synthetic */ abu a(int i) {
        if (i >= this.c) {
            return new aay(Arrays.copyOf(this.f2410b, i), this.c);
        }
        throw new IllegalArgumentException();
    }

    public final void a(double d) {
        a(this.c, d);
    }

    public final /* synthetic */ void add(int i, Object obj) {
        a(i, ((Double) obj).doubleValue());
    }

    public final boolean addAll(Collection<? extends Double> collection) {
        c();
        abr.a(collection);
        if (!(collection instanceof aay)) {
            return super.addAll(collection);
        }
        aay aay = (aay) collection;
        if (aay.c == 0) {
            return false;
        }
        if (Integer.MAX_VALUE - this.c < aay.c) {
            throw new OutOfMemoryError();
        }
        int i = this.c + aay.c;
        if (i > this.f2410b.length) {
            this.f2410b = Arrays.copyOf(this.f2410b, i);
        }
        System.arraycopy(aay.f2410b, 0, this.f2410b, this.c, aay.c);
        this.c = i;
        this.modCount++;
        return true;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof aay)) {
            return super.equals(obj);
        }
        aay aay = (aay) obj;
        if (this.c != aay.c) {
            return false;
        }
        double[] dArr = aay.f2410b;
        for (int i = 0; i < this.c; i++) {
            if (this.f2410b[i] != dArr[i]) {
                return false;
            }
        }
        return true;
    }

    public final /* synthetic */ Object get(int i) {
        b(i);
        return Double.valueOf(this.f2410b[i]);
    }

    public final int hashCode() {
        int i = 1;
        for (int i2 = 0; i2 < this.c; i2++) {
            i = (i * 31) + abr.a(Double.doubleToLongBits(this.f2410b[i2]));
        }
        return i;
    }

    public final /* synthetic */ Object remove(int i) {
        c();
        b(i);
        double d = this.f2410b[i];
        if (i < this.c - 1) {
            System.arraycopy(this.f2410b, i + 1, this.f2410b, i, this.c - i);
        }
        this.c--;
        this.modCount++;
        return Double.valueOf(d);
    }

    public final boolean remove(Object obj) {
        c();
        for (int i = 0; i < this.c; i++) {
            if (obj.equals(Double.valueOf(this.f2410b[i]))) {
                System.arraycopy(this.f2410b, i + 1, this.f2410b, i, this.c - i);
                this.c--;
                this.modCount++;
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public final void removeRange(int i, int i2) {
        c();
        if (i2 < i) {
            throw new IndexOutOfBoundsException("toIndex < fromIndex");
        }
        System.arraycopy(this.f2410b, i2, this.f2410b, i, this.c - i2);
        this.c -= i2 - i;
        this.modCount++;
    }

    public final /* synthetic */ Object set(int i, Object obj) {
        double doubleValue = ((Double) obj).doubleValue();
        c();
        b(i);
        double d = this.f2410b[i];
        this.f2410b[i] = doubleValue;
        return Double.valueOf(d);
    }

    public final int size() {
        return this.c;
    }
}
