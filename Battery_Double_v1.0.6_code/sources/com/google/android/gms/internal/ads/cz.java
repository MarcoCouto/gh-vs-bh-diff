package com.google.android.gms.internal.ads;

@cm
final class cz extends Exception {

    /* renamed from: a reason: collision with root package name */
    private final int f3246a;

    public cz(String str, int i) {
        super(str);
        this.f3246a = i;
    }

    public final int a() {
        return this.f3246a;
    }
}
