package com.google.android.gms.internal.ads;

import java.io.IOException;

public final class ani extends afh<ani> {

    /* renamed from: a reason: collision with root package name */
    private String f2754a;

    /* renamed from: b reason: collision with root package name */
    private anh[] f2755b;
    private Integer c;

    public ani() {
        this.f2754a = null;
        this.f2755b = anh.b();
        this.c = null;
        this.Y = null;
        this.Z = -1;
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public final ani a(afd afd) throws IOException {
        while (true) {
            int a2 = afd.a();
            switch (a2) {
                case 0:
                    break;
                case 10:
                    this.f2754a = afd.e();
                    continue;
                case 18:
                    int a3 = afq.a(afd, 18);
                    int length = this.f2755b == null ? 0 : this.f2755b.length;
                    anh[] anhArr = new anh[(a3 + length)];
                    if (length != 0) {
                        System.arraycopy(this.f2755b, 0, anhArr, 0, length);
                    }
                    while (length < anhArr.length - 1) {
                        anhArr[length] = new anh();
                        afd.a((afn) anhArr[length]);
                        afd.a();
                        length++;
                    }
                    anhArr[length] = new anh();
                    afd.a((afn) anhArr[length]);
                    this.f2755b = anhArr;
                    continue;
                case 24:
                    int j = afd.j();
                    try {
                        this.c = Integer.valueOf(anf.a(afd.g()));
                        continue;
                    } catch (IllegalArgumentException e) {
                        afd.e(j);
                        a(afd, a2);
                        break;
                    }
                default:
                    if (!super.a(afd, a2)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    /* access modifiers changed from: protected */
    public final int a() {
        int a2 = super.a();
        if (this.f2754a != null) {
            a2 += aff.b(1, this.f2754a);
        }
        if (this.f2755b != null && this.f2755b.length > 0) {
            int i = a2;
            for (anh anh : this.f2755b) {
                if (anh != null) {
                    i += aff.b(2, (afn) anh);
                }
            }
            a2 = i;
        }
        return this.c != null ? a2 + aff.b(3, this.c.intValue()) : a2;
    }

    public final void a(aff aff) throws IOException {
        if (this.f2754a != null) {
            aff.a(1, this.f2754a);
        }
        if (this.f2755b != null && this.f2755b.length > 0) {
            for (anh anh : this.f2755b) {
                if (anh != null) {
                    aff.a(2, (afn) anh);
                }
            }
        }
        if (this.c != null) {
            aff.a(3, this.c.intValue());
        }
        super.a(aff);
    }
}
