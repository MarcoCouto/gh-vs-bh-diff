package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.abp.b;
import com.google.android.gms.internal.ads.abp.e;

public final class xv extends abp<xv, a> implements acy {
    private static volatile adi<xv> zzakh;
    /* access modifiers changed from: private */
    public static final xv zzdmg = new xv();
    private int zzdih;
    private xy zzdmf;

    public static final class a extends com.google.android.gms.internal.ads.abp.a<xv, a> implements acy {
        private a() {
            super(xv.zzdmg);
        }

        /* synthetic */ a(xx xxVar) {
            this();
        }

        public final a a(int i) {
            b();
            ((xv) this.f2433a).b(0);
            return this;
        }

        public final a a(xy xyVar) {
            b();
            ((xv) this.f2433a).a(xyVar);
            return this;
        }
    }

    static {
        abp.a(xv.class, zzdmg);
    }

    private xv() {
    }

    public static xv a(aah aah) throws abv {
        return (xv) abp.a(zzdmg, aah);
    }

    /* access modifiers changed from: private */
    public final void a(xy xyVar) {
        if (xyVar == null) {
            throw new NullPointerException();
        }
        this.zzdmf = xyVar;
    }

    /* access modifiers changed from: private */
    public final void b(int i) {
        this.zzdih = i;
    }

    public static a c() {
        return (a) ((com.google.android.gms.internal.ads.abp.a) zzdmg.a(e.e, (Object) null, (Object) null));
    }

    public final int a() {
        return this.zzdih;
    }

    /* JADX WARNING: type inference failed for: r0v6, types: [com.google.android.gms.internal.ads.adi<com.google.android.gms.internal.ads.xv>] */
    /* JADX WARNING: type inference failed for: r0v7, types: [java.lang.Object] */
    /* JADX WARNING: type inference failed for: r0v8, types: [com.google.android.gms.internal.ads.abp$b, com.google.android.gms.internal.ads.adi<com.google.android.gms.internal.ads.xv>] */
    /* JADX WARNING: type inference failed for: r0v16 */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r0v8, types: [com.google.android.gms.internal.ads.abp$b, com.google.android.gms.internal.ads.adi<com.google.android.gms.internal.ads.xv>]
  assigns: [com.google.android.gms.internal.ads.abp$b]
  uses: [com.google.android.gms.internal.ads.adi<com.google.android.gms.internal.ads.xv>]
  mth insns count: 40
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
    	at jadx.core.ProcessClass.process(ProcessClass.java:35)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Unknown variable types count: 2 */
    public final Object a(int i, Object obj, Object obj2) {
        ? r0;
        switch (xx.f3783a[i - 1]) {
            case 1:
                return new xv();
            case 2:
                return new a(null);
            case 3:
                Object[] objArr = {"zzdih", "zzdmf"};
                return a((acw) zzdmg, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0003\u0000\u0000\u0000\u0001\u000b\u0002\t", objArr);
            case 4:
                return zzdmg;
            case 5:
                adi<xv> adi = zzakh;
                if (adi != null) {
                    return adi;
                }
                synchronized (xv.class) {
                    r0 = zzakh;
                    if (r0 == 0) {
                        ? bVar = new b(zzdmg);
                        zzakh = bVar;
                        r0 = bVar;
                    }
                }
                return r0;
            case 6:
                return Byte.valueOf(1);
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    public final xy b() {
        return this.zzdmf == null ? xy.c() : this.zzdmf;
    }
}
