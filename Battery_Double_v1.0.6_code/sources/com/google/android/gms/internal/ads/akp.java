package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.gmsg.ae;
import java.util.Map;

final class akp implements ae<bbe> {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ akh f2673a;

    akp(akh akh) {
        this.f2673a = akh;
    }

    public final /* synthetic */ void zza(Object obj, Map map) {
        bbe bbe = (bbe) obj;
        if (this.f2673a.f2664a.a(map)) {
            this.f2673a.c.zza(bbe, map);
        }
    }
}
