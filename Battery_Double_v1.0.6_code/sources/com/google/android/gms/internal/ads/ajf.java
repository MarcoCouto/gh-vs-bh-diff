package com.google.android.gms.internal.ads;

import java.lang.reflect.InvocationTargetException;

public final class ajf extends ajj {
    public ajf(ahy ahy, String str, String str2, zz zzVar, int i, int i2) {
        super(ahy, str, str2, zzVar, i, 48);
    }

    /* access modifiers changed from: protected */
    public final void a() throws IllegalAccessException, InvocationTargetException {
        this.f2637b.E = Integer.valueOf(2);
        boolean booleanValue = ((Boolean) this.c.invoke(null, new Object[]{this.f2636a.a()})).booleanValue();
        synchronized (this.f2637b) {
            if (booleanValue) {
                this.f2637b.E = Integer.valueOf(1);
            } else {
                this.f2637b.E = Integer.valueOf(0);
            }
        }
    }
}
