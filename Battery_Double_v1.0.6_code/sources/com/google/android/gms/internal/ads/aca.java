package com.google.android.gms.internal.ads;

import java.util.Map.Entry;

final class aca<K> implements Entry<K, Object> {

    /* renamed from: a reason: collision with root package name */
    private Entry<K, aby> f2445a;

    private aca(Entry<K, aby> entry) {
        this.f2445a = entry;
    }

    public final aby a() {
        return (aby) this.f2445a.getValue();
    }

    public final K getKey() {
        return this.f2445a.getKey();
    }

    public final Object getValue() {
        if (((aby) this.f2445a.getValue()) == null) {
            return null;
        }
        return aby.a();
    }

    public final Object setValue(Object obj) {
        if (obj instanceof acw) {
            return ((aby) this.f2445a.getValue()).a((acw) obj);
        }
        throw new IllegalArgumentException("LazyField now only used for MessageSet, and the value of MessageSet must be an instance of MessageLite");
    }
}
