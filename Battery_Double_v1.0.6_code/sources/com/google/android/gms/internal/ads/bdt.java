package com.google.android.gms.internal.ads;

import android.location.Location;
import com.google.android.gms.ads.b.d;
import com.google.android.gms.ads.b.d.a;
import com.google.android.gms.ads.j;
import com.google.android.gms.ads.mediation.i;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@cm
public final class bdt implements i {

    /* renamed from: a reason: collision with root package name */
    private final Date f3155a;

    /* renamed from: b reason: collision with root package name */
    private final int f3156b;
    private final Set<String> c;
    private final boolean d;
    private final Location e;
    private final int f;
    private final aul g;
    private final List<String> h = new ArrayList();
    private final boolean i;
    private final Map<String, Boolean> j = new HashMap();

    public bdt(Date date, int i2, Set<String> set, Location location, boolean z, int i3, aul aul, List<String> list, boolean z2) {
        this.f3155a = date;
        this.f3156b = i2;
        this.c = set;
        this.e = location;
        this.d = z;
        this.f = i3;
        this.g = aul;
        this.i = z2;
        String str = "custom:";
        if (list != null) {
            for (String str2 : list) {
                if (str2.startsWith(str)) {
                    String[] split = str2.split(":", 3);
                    if (split.length == 3) {
                        if ("true".equals(split[2])) {
                            this.j.put(split[1], Boolean.valueOf(true));
                        } else if ("false".equals(split[2])) {
                            this.j.put(split[1], Boolean.valueOf(false));
                        }
                    }
                } else {
                    this.h.add(str2);
                }
            }
        }
    }

    public final Date a() {
        return this.f3155a;
    }

    public final int b() {
        return this.f3156b;
    }

    public final Set<String> c() {
        return this.c;
    }

    public final Location d() {
        return this.e;
    }

    public final int e() {
        return this.f;
    }

    public final boolean f() {
        return this.d;
    }

    public final boolean g() {
        return this.i;
    }

    public final d h() {
        if (this.g == null) {
            return null;
        }
        a b2 = new a().a(this.g.f2938b).a(this.g.c).b(this.g.d);
        if (this.g.f2937a >= 2) {
            b2.b(this.g.e);
        }
        if (this.g.f2937a >= 3 && this.g.f != null) {
            b2.a(new j(this.g.f));
        }
        return b2.a();
    }

    public final boolean i() {
        return this.h != null && (this.h.contains("2") || this.h.contains("6"));
    }

    public final boolean j() {
        return this.h != null && this.h.contains("6");
    }

    public final boolean k() {
        return this.h != null && (this.h.contains("1") || this.h.contains("6"));
    }

    public final boolean l() {
        return this.h != null && this.h.contains("3");
    }

    public final Map<String, Boolean> m() {
        return this.j;
    }
}
