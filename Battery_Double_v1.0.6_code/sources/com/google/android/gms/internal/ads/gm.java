package com.google.android.gms.internal.ads;

import android.os.IBinder;

public final class gm extends ajk implements gl {
    gm(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.reward.client.IRewardedVideoAdCreator");
    }
}
