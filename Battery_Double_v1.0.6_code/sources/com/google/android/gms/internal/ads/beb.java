package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.view.View;
import com.google.android.gms.ads.b.c;
import com.google.android.gms.ads.mediation.l;
import com.google.android.gms.b.a;
import com.google.android.gms.b.b;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@cm
public final class beb extends bdl {

    /* renamed from: a reason: collision with root package name */
    private final l f3168a;

    public beb(l lVar) {
        this.f3168a = lVar;
    }

    public final String a() {
        return this.f3168a.a();
    }

    public final void a(a aVar) {
        this.f3168a.a((View) b.a(aVar));
    }

    public final void a(a aVar, a aVar2, a aVar3) {
        this.f3168a.a((View) b.a(aVar), (HashMap) b.a(aVar2), (HashMap) b.a(aVar3));
    }

    public final List b() {
        List<c.b> b2 = this.f3168a.b();
        ArrayList arrayList = new ArrayList();
        if (b2 != null) {
            for (c.b bVar : b2) {
                arrayList.add(new atm(bVar.a(), bVar.b(), bVar.c()));
            }
        }
        return arrayList;
    }

    public final void b(a aVar) {
        this.f3168a.b((View) b.a(aVar));
    }

    public final String c() {
        return this.f3168a.c();
    }

    public final auw d() {
        c.b d = this.f3168a.d();
        if (d != null) {
            return new atm(d.a(), d.b(), d.c());
        }
        return null;
    }

    public final String e() {
        return this.f3168a.e();
    }

    public final String f() {
        return this.f3168a.f();
    }

    public final double g() {
        if (this.f3168a.g() != null) {
            return this.f3168a.g().doubleValue();
        }
        return -1.0d;
    }

    public final String h() {
        return this.f3168a.h();
    }

    public final String i() {
        return this.f3168a.i();
    }

    public final aqs j() {
        if (this.f3168a.j() != null) {
            return this.f3168a.j().a();
        }
        return null;
    }

    public final aus k() {
        return null;
    }

    public final a l() {
        View l = this.f3168a.l();
        if (l == null) {
            return null;
        }
        return b.a(l);
    }

    public final a m() {
        View m = this.f3168a.m();
        if (m == null) {
            return null;
        }
        return b.a(m);
    }

    public final a n() {
        Object n = this.f3168a.n();
        if (n == null) {
            return null;
        }
        return b.a(n);
    }

    public final Bundle o() {
        return this.f3168a.o();
    }

    public final boolean p() {
        return this.f3168a.p();
    }

    public final boolean q() {
        return this.f3168a.q();
    }

    public final void r() {
        this.f3168a.r();
    }
}
