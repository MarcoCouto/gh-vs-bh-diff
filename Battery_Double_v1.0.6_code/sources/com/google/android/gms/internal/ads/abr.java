package com.google.android.gms.internal.ads;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;

public final class abr {

    /* renamed from: a reason: collision with root package name */
    static final Charset f2440a = Charset.forName("UTF-8");

    /* renamed from: b reason: collision with root package name */
    public static final byte[] f2441b;
    private static final Charset c = Charset.forName("ISO-8859-1");
    private static final ByteBuffer d;
    private static final aaq e;

    static {
        byte[] bArr = new byte[0];
        f2441b = bArr;
        d = ByteBuffer.wrap(bArr);
        byte[] bArr2 = f2441b;
        e = aaq.a(bArr2, 0, bArr2.length, false);
    }

    static int a(int i, byte[] bArr, int i2, int i3) {
        for (int i4 = i2; i4 < i2 + i3; i4++) {
            i = (i * 31) + bArr[i4];
        }
        return i;
    }

    public static int a(long j) {
        return (int) ((j >>> 32) ^ j);
    }

    public static int a(boolean z) {
        return z ? 1231 : 1237;
    }

    static <T> T a(T t) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException();
    }

    static Object a(Object obj, Object obj2) {
        return ((acw) obj).n().a((acw) obj2).d();
    }

    static <T> T a(T t, String str) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException(str);
    }

    static boolean a(acw acw) {
        return false;
    }

    public static boolean a(byte[] bArr) {
        return aeq.a(bArr);
    }

    public static String b(byte[] bArr) {
        return new String(bArr, f2440a);
    }

    public static int c(byte[] bArr) {
        int length = bArr.length;
        int a2 = a(length, bArr, 0, length);
        if (a2 == 0) {
            return 1;
        }
        return a2;
    }
}
