package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.gmsg.ae;
import java.util.Map;

final class baf implements ae<qn> {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public final ae<? super bbe> f3063a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ azx f3064b;

    public baf(azx azx, ae<? super bbe> aeVar) {
        this.f3064b = azx;
        this.f3063a = aeVar;
    }

    public final /* synthetic */ void zza(Object obj, Map map) {
        this.f3063a.zza(this.f3064b, map);
    }
}
