package com.google.android.gms.internal.ads;

final class cx implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ cp f3243a;

    cx(cp cpVar) {
        this.f3243a = cpVar;
    }

    public final void run() {
        synchronized (this.f3243a.d) {
            if (this.f3243a.f3234a != null) {
                this.f3243a.c_();
                this.f3243a.a(2, "Timed out waiting for ad response.");
            }
        }
    }
}
