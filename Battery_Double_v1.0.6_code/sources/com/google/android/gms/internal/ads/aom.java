package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.b.b;
import com.google.android.gms.b.c;
import com.google.android.gms.b.c.a;

@cm
public final class aom extends c<apt> {
    public aom() {
        super("com.google.android.gms.ads.AdLoaderBuilderCreatorImpl");
    }

    public final apq a(Context context, String str, bcr bcr) {
        try {
            IBinder a2 = ((apt) a(context)).a(b.a(context), str, bcr, 12451000);
            if (a2 == null) {
                return null;
            }
            IInterface queryLocalInterface = a2.queryLocalInterface("com.google.android.gms.ads.internal.client.IAdLoaderBuilder");
            return queryLocalInterface instanceof apq ? (apq) queryLocalInterface : new aps(a2);
        } catch (RemoteException | a e) {
            ms.c("Could not create remote builder for AdLoader.", e);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.IAdLoaderBuilderCreator");
        return queryLocalInterface instanceof apt ? (apt) queryLocalInterface : new apu(iBinder);
    }
}
