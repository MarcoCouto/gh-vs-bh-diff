package com.google.android.gms.internal.ads;

import android.os.Bundle;
import com.google.android.gms.b.a;
import com.google.android.gms.b.b;

@cm
public final class hd extends hm {

    /* renamed from: a reason: collision with root package name */
    private volatile hb f3358a;

    /* renamed from: b reason: collision with root package name */
    private volatile he f3359b;
    private volatile hc c;
    private volatile hj d;

    public hd(hc hcVar) {
        this.c = hcVar;
    }

    public final void a(Bundle bundle) {
        if (this.d != null) {
            this.d.a(bundle);
        }
    }

    public final void a(a aVar) {
        if (this.f3358a != null) {
            this.f3358a.g();
        }
    }

    public final void a(a aVar, int i) {
        if (this.f3358a != null) {
            this.f3358a.a(i);
        }
    }

    public final void a(a aVar, hp hpVar) {
        if (this.c != null) {
            this.c.a(hpVar);
        }
    }

    public final void a(hb hbVar) {
        this.f3358a = hbVar;
    }

    public final void a(he heVar) {
        this.f3359b = heVar;
    }

    public final void a(hj hjVar) {
        this.d = hjVar;
    }

    public final void b(a aVar) {
        if (this.f3359b != null) {
            this.f3359b.a(b.a(aVar).getClass().getName());
        }
    }

    public final void b(a aVar, int i) {
        if (this.f3359b != null) {
            this.f3359b.a(b.a(aVar).getClass().getName(), i);
        }
    }

    public final void c(a aVar) {
        if (this.c != null) {
            this.c.l_();
        }
    }

    public final void d(a aVar) {
        if (this.c != null) {
            this.c.b();
        }
    }

    public final void e(a aVar) {
        if (this.c != null) {
            this.c.c();
        }
    }

    public final void f(a aVar) {
        if (this.c != null) {
            this.c.n_();
        }
    }

    public final void g(a aVar) {
        if (this.c != null) {
            this.c.o_();
        }
    }

    public final void h(a aVar) {
        if (this.c != null) {
            this.c.m_();
        }
    }
}
