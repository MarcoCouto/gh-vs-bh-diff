package com.google.android.gms.internal.ads;

import com.google.android.gms.common.internal.e.b;

final class aml implements b {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ amh f2735a;

    aml(amh amh) {
        this.f2735a = amh;
    }

    public final void a(com.google.android.gms.common.b bVar) {
        synchronized (this.f2735a.f2731b) {
            this.f2735a.e = null;
            if (this.f2735a.c != null) {
                this.f2735a.c = null;
            }
            this.f2735a.f2731b.notifyAll();
        }
    }
}
