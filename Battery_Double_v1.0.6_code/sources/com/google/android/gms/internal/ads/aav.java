package com.google.android.gms.internal.ads;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class aav extends aag {

    /* renamed from: b reason: collision with root package name */
    private static final Logger f2405b = Logger.getLogger(aav.class.getName());
    /* access modifiers changed from: private */
    public static final boolean c = aeo.a();

    /* renamed from: a reason: collision with root package name */
    aax f2406a = this;

    static class a extends aav {

        /* renamed from: b reason: collision with root package name */
        private final byte[] f2407b;
        private final int c;
        private final int d;
        private int e;

        a(byte[] bArr, int i, int i2) {
            super();
            if (bArr == null) {
                throw new NullPointerException("buffer");
            } else if ((i2 | 0 | (bArr.length - (i2 + 0))) < 0) {
                throw new IllegalArgumentException(String.format("Array range is invalid. Buffer.length=%d, offset=%d, length=%d", new Object[]{Integer.valueOf(bArr.length), Integer.valueOf(0), Integer.valueOf(i2)}));
            } else {
                this.f2407b = bArr;
                this.c = 0;
                this.e = 0;
                this.d = i2 + 0;
            }
        }

        public final int a() {
            return this.d - this.e;
        }

        public final void a(byte b2) throws IOException {
            try {
                byte[] bArr = this.f2407b;
                int i = this.e;
                this.e = i + 1;
                bArr[i] = b2;
            } catch (IndexOutOfBoundsException e2) {
                throw new b(String.format("Pos: %d, limit: %d, len: %d", new Object[]{Integer.valueOf(this.e), Integer.valueOf(this.d), Integer.valueOf(1)}), e2);
            }
        }

        public final void a(int i) throws IOException {
            if (i >= 0) {
                b(i);
            } else {
                a((long) i);
            }
        }

        public final void a(int i, int i2) throws IOException {
            b((i << 3) | i2);
        }

        public final void a(int i, long j) throws IOException {
            a(i, 0);
            a(j);
        }

        public final void a(int i, aah aah) throws IOException {
            a(i, 2);
            a(aah);
        }

        public final void a(int i, acw acw) throws IOException {
            a(1, 3);
            c(2, i);
            a(3, 2);
            a(acw);
            a(1, 4);
        }

        /* access modifiers changed from: 0000 */
        public final void a(int i, acw acw, adp adp) throws IOException {
            a(i, 2);
            zw zwVar = (zw) acw;
            int j = zwVar.j();
            if (j == -1) {
                j = adp.b(zwVar);
                zwVar.a(j);
            }
            b(j);
            adp.a(acw, (afc) this.f2406a);
        }

        public final void a(int i, String str) throws IOException {
            a(i, 2);
            a(str);
        }

        public final void a(int i, boolean z) throws IOException {
            int i2 = 0;
            a(i, 0);
            if (z) {
                i2 = 1;
            }
            a((byte) i2);
        }

        public final void a(long j) throws IOException {
            if (!aav.c || a() < 10) {
                while ((j & -128) != 0) {
                    try {
                        byte[] bArr = this.f2407b;
                        int i = this.e;
                        this.e = i + 1;
                        bArr[i] = (byte) ((((int) j) & 127) | 128);
                        j >>>= 7;
                    } catch (IndexOutOfBoundsException e2) {
                        throw new b(String.format("Pos: %d, limit: %d, len: %d", new Object[]{Integer.valueOf(this.e), Integer.valueOf(this.d), Integer.valueOf(1)}), e2);
                    }
                }
                byte[] bArr2 = this.f2407b;
                int i2 = this.e;
                this.e = i2 + 1;
                bArr2[i2] = (byte) ((int) j);
                return;
            }
            while ((j & -128) != 0) {
                byte[] bArr3 = this.f2407b;
                int i3 = this.e;
                this.e = i3 + 1;
                aeo.a(bArr3, (long) i3, (byte) ((((int) j) & 127) | 128));
                j >>>= 7;
            }
            byte[] bArr4 = this.f2407b;
            int i4 = this.e;
            this.e = i4 + 1;
            aeo.a(bArr4, (long) i4, (byte) ((int) j));
        }

        public final void a(aah aah) throws IOException {
            b(aah.a());
            aah.a((aag) this);
        }

        public final void a(acw acw) throws IOException {
            b(acw.l());
            acw.a(this);
        }

        public final void a(String str) throws IOException {
            int i = this.e;
            try {
                int g = g(str.length() * 3);
                int g2 = g(str.length());
                if (g2 == g) {
                    this.e = i + g2;
                    int a2 = aeq.a(str, this.f2407b, this.e, a());
                    this.e = i;
                    b((a2 - i) - g2);
                    this.e = a2;
                    return;
                }
                b(aeq.a((CharSequence) str));
                this.e = aeq.a(str, this.f2407b, this.e, a());
            } catch (aet e2) {
                this.e = i;
                a(str, e2);
            } catch (IndexOutOfBoundsException e3) {
                throw new b(e3);
            }
        }

        public final void a(byte[] bArr, int i, int i2) throws IOException {
            b(bArr, i, i2);
        }

        public final void b(int i) throws IOException {
            if (!aav.c || a() < 10) {
                while ((i & -128) != 0) {
                    try {
                        byte[] bArr = this.f2407b;
                        int i2 = this.e;
                        this.e = i2 + 1;
                        bArr[i2] = (byte) ((i & 127) | 128);
                        i >>>= 7;
                    } catch (IndexOutOfBoundsException e2) {
                        throw new b(String.format("Pos: %d, limit: %d, len: %d", new Object[]{Integer.valueOf(this.e), Integer.valueOf(this.d), Integer.valueOf(1)}), e2);
                    }
                }
                byte[] bArr2 = this.f2407b;
                int i3 = this.e;
                this.e = i3 + 1;
                bArr2[i3] = (byte) i;
                return;
            }
            while ((i & -128) != 0) {
                byte[] bArr3 = this.f2407b;
                int i4 = this.e;
                this.e = i4 + 1;
                aeo.a(bArr3, (long) i4, (byte) ((i & 127) | 128));
                i >>>= 7;
            }
            byte[] bArr4 = this.f2407b;
            int i5 = this.e;
            this.e = i5 + 1;
            aeo.a(bArr4, (long) i5, (byte) i);
        }

        public final void b(int i, int i2) throws IOException {
            a(i, 0);
            a(i2);
        }

        public final void b(int i, aah aah) throws IOException {
            a(1, 3);
            c(2, i);
            a(3, aah);
            a(1, 4);
        }

        public final void b(byte[] bArr, int i, int i2) throws IOException {
            try {
                System.arraycopy(bArr, i, this.f2407b, this.e, i2);
                this.e += i2;
            } catch (IndexOutOfBoundsException e2) {
                throw new b(String.format("Pos: %d, limit: %d, len: %d", new Object[]{Integer.valueOf(this.e), Integer.valueOf(this.d), Integer.valueOf(i2)}), e2);
            }
        }

        public final void c(int i, int i2) throws IOException {
            a(i, 0);
            b(i2);
        }

        public final void c(int i, long j) throws IOException {
            a(i, 1);
            c(j);
        }

        public final void c(long j) throws IOException {
            try {
                byte[] bArr = this.f2407b;
                int i = this.e;
                this.e = i + 1;
                bArr[i] = (byte) ((int) j);
                byte[] bArr2 = this.f2407b;
                int i2 = this.e;
                this.e = i2 + 1;
                bArr2[i2] = (byte) ((int) (j >> 8));
                byte[] bArr3 = this.f2407b;
                int i3 = this.e;
                this.e = i3 + 1;
                bArr3[i3] = (byte) ((int) (j >> 16));
                byte[] bArr4 = this.f2407b;
                int i4 = this.e;
                this.e = i4 + 1;
                bArr4[i4] = (byte) ((int) (j >> 24));
                byte[] bArr5 = this.f2407b;
                int i5 = this.e;
                this.e = i5 + 1;
                bArr5[i5] = (byte) ((int) (j >> 32));
                byte[] bArr6 = this.f2407b;
                int i6 = this.e;
                this.e = i6 + 1;
                bArr6[i6] = (byte) ((int) (j >> 40));
                byte[] bArr7 = this.f2407b;
                int i7 = this.e;
                this.e = i7 + 1;
                bArr7[i7] = (byte) ((int) (j >> 48));
                byte[] bArr8 = this.f2407b;
                int i8 = this.e;
                this.e = i8 + 1;
                bArr8[i8] = (byte) ((int) (j >> 56));
            } catch (IndexOutOfBoundsException e2) {
                throw new b(String.format("Pos: %d, limit: %d, len: %d", new Object[]{Integer.valueOf(this.e), Integer.valueOf(this.d), Integer.valueOf(1)}), e2);
            }
        }

        public final void c(byte[] bArr, int i, int i2) throws IOException {
            b(i2);
            b(bArr, 0, i2);
        }

        /* JADX WARNING: type inference failed for: r0v4, types: [byte[]] */
        /* JADX WARNING: type inference failed for: r2v11, types: [byte, int] */
        /* JADX WARNING: Incorrect type for immutable var: ssa=byte[], code=null, for r0v4, types: [byte[]] */
        /* JADX WARNING: Incorrect type for immutable var: ssa=int, code=null, for r2v11, types: [byte, int] */
        /* JADX WARNING: Unknown variable types count: 2 */
        public final void d(int i) throws IOException {
            try {
                byte[] bArr = this.f2407b;
                int i2 = this.e;
                this.e = i2 + 1;
                bArr[i2] = (byte) i;
                byte[] bArr2 = this.f2407b;
                int i3 = this.e;
                this.e = i3 + 1;
                bArr2[i3] = (byte) (i >> 8);
                byte[] bArr3 = this.f2407b;
                int i4 = this.e;
                this.e = i4 + 1;
                bArr3[i4] = (byte) (i >> 16);
                ? r0 = this.f2407b;
                int i5 = this.e;
                this.e = i5 + 1;
                r0[i5] = i >> 24;
            } catch (IndexOutOfBoundsException e2) {
                throw new b(String.format("Pos: %d, limit: %d, len: %d", new Object[]{Integer.valueOf(this.e), Integer.valueOf(this.d), Integer.valueOf(1)}), e2);
            }
        }

        public final void e(int i, int i2) throws IOException {
            a(i, 5);
            d(i2);
        }
    }

    public static class b extends IOException {
        b() {
            super("CodedOutputStream was writing to a flat byte array and ran out of space.");
        }

        b(String str, Throwable th) {
            String valueOf = String.valueOf("CodedOutputStream was writing to a flat byte array and ran out of space.: ");
            String valueOf2 = String.valueOf(str);
            super(valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf), th);
        }

        b(Throwable th) {
            super("CodedOutputStream was writing to a flat byte array and ran out of space.", th);
        }
    }

    private aav() {
    }

    public static int a(int i, acd acd) {
        int e = e(i);
        int b2 = acd.b();
        return e + b2 + g(b2);
    }

    public static int a(acd acd) {
        int b2 = acd.b();
        return b2 + g(b2);
    }

    static int a(acw acw, adp adp) {
        zw zwVar = (zw) acw;
        int j = zwVar.j();
        if (j == -1) {
            j = adp.b(zwVar);
            zwVar.a(j);
        }
        return j + g(j);
    }

    public static aav a(byte[] bArr) {
        return new a(bArr, 0, bArr.length);
    }

    public static int b(double d) {
        return 8;
    }

    public static int b(float f) {
        return 4;
    }

    public static int b(int i, double d) {
        return e(i) + 8;
    }

    public static int b(int i, float f) {
        return e(i) + 4;
    }

    public static int b(int i, acd acd) {
        return (e(1) << 1) + g(2, i) + a(3, acd);
    }

    public static int b(int i, acw acw) {
        return (e(1) << 1) + g(2, i) + e(3) + b(acw);
    }

    static int b(int i, acw acw, adp adp) {
        return e(i) + a(acw, adp);
    }

    public static int b(int i, String str) {
        return e(i) + b(str);
    }

    public static int b(int i, boolean z) {
        return e(i) + 1;
    }

    public static int b(aah aah) {
        int a2 = aah.a();
        return a2 + g(a2);
    }

    public static int b(acw acw) {
        int l = acw.l();
        return l + g(l);
    }

    public static int b(String str) {
        int length;
        try {
            length = aeq.a((CharSequence) str);
        } catch (aet e) {
            length = str.getBytes(abr.f2440a).length;
        }
        return length + g(length);
    }

    public static int b(boolean z) {
        return 1;
    }

    public static int b(byte[] bArr) {
        int length = bArr.length;
        return length + g(length);
    }

    public static int c(int i, aah aah) {
        int e = e(i);
        int a2 = aah.a();
        return e + a2 + g(a2);
    }

    @Deprecated
    static int c(int i, acw acw, adp adp) {
        int e = e(i) << 1;
        zw zwVar = (zw) acw;
        int j = zwVar.j();
        if (j == -1) {
            j = adp.b(zwVar);
            zwVar.a(j);
        }
        return j + e;
    }

    @Deprecated
    public static int c(acw acw) {
        return acw.l();
    }

    public static int d(int i, long j) {
        return e(i) + e(j);
    }

    public static int d(int i, aah aah) {
        return (e(1) << 1) + g(2, i) + c(3, aah);
    }

    public static int d(long j) {
        return e(j);
    }

    public static int e(int i) {
        return g(i << 3);
    }

    public static int e(int i, long j) {
        return e(i) + e(j);
    }

    public static int e(long j) {
        long j2;
        if ((-128 & j) == 0) {
            return 1;
        }
        if (j < 0) {
            return 10;
        }
        int i = 2;
        if ((-34359738368L & j) != 0) {
            i = 6;
            j2 = j >>> 28;
        } else {
            j2 = j;
        }
        if ((-2097152 & j2) != 0) {
            i += 2;
            j2 >>>= 14;
        }
        return (j2 & -16384) != 0 ? i + 1 : i;
    }

    public static int f(int i) {
        if (i >= 0) {
            return g(i);
        }
        return 10;
    }

    public static int f(int i, int i2) {
        return e(i) + f(i2);
    }

    public static int f(int i, long j) {
        return e(i) + e(i(j));
    }

    public static int f(long j) {
        return e(i(j));
    }

    public static int g(int i) {
        if ((i & -128) == 0) {
            return 1;
        }
        if ((i & -16384) == 0) {
            return 2;
        }
        if ((-2097152 & i) == 0) {
            return 3;
        }
        return (-268435456 & i) == 0 ? 4 : 5;
    }

    public static int g(int i, int i2) {
        return e(i) + g(i2);
    }

    public static int g(int i, long j) {
        return e(i) + 8;
    }

    public static int g(long j) {
        return 8;
    }

    public static int h(int i) {
        return g(m(i));
    }

    public static int h(int i, int i2) {
        return e(i) + g(m(i2));
    }

    public static int h(int i, long j) {
        return e(i) + 8;
    }

    public static int h(long j) {
        return 8;
    }

    public static int i(int i) {
        return 4;
    }

    public static int i(int i, int i2) {
        return e(i) + 4;
    }

    private static long i(long j) {
        return (j << 1) ^ (j >> 63);
    }

    public static int j(int i) {
        return 4;
    }

    public static int j(int i, int i2) {
        return e(i) + 4;
    }

    public static int k(int i) {
        return f(i);
    }

    public static int k(int i, int i2) {
        return e(i) + f(i2);
    }

    @Deprecated
    public static int l(int i) {
        return g(i);
    }

    private static int m(int i) {
        return (i << 1) ^ (i >> 31);
    }

    public abstract int a();

    public abstract void a(byte b2) throws IOException;

    public final void a(double d) throws IOException {
        c(Double.doubleToRawLongBits(d));
    }

    public final void a(float f) throws IOException {
        d(Float.floatToRawIntBits(f));
    }

    public abstract void a(int i) throws IOException;

    public final void a(int i, double d) throws IOException {
        c(i, Double.doubleToRawLongBits(d));
    }

    public final void a(int i, float f) throws IOException {
        e(i, Float.floatToRawIntBits(f));
    }

    public abstract void a(int i, int i2) throws IOException;

    public abstract void a(int i, long j) throws IOException;

    public abstract void a(int i, aah aah) throws IOException;

    public abstract void a(int i, acw acw) throws IOException;

    /* access modifiers changed from: 0000 */
    public abstract void a(int i, acw acw, adp adp) throws IOException;

    public abstract void a(int i, String str) throws IOException;

    public abstract void a(int i, boolean z) throws IOException;

    public abstract void a(long j) throws IOException;

    public abstract void a(aah aah) throws IOException;

    public abstract void a(acw acw) throws IOException;

    public abstract void a(String str) throws IOException;

    /* access modifiers changed from: 0000 */
    public final void a(String str, aet aet) throws IOException {
        f2405b.logp(Level.WARNING, "com.google.protobuf.CodedOutputStream", "inefficientWriteStringNoTag", "Converting ill-formed UTF-16. Your Protocol Buffer will not round trip correctly!", aet);
        byte[] bytes = str.getBytes(abr.f2440a);
        try {
            b(bytes.length);
            a(bytes, 0, bytes.length);
        } catch (IndexOutOfBoundsException e) {
            throw new b(e);
        } catch (b e2) {
            throw e2;
        }
    }

    public final void a(boolean z) throws IOException {
        a((byte) (z ? 1 : 0));
    }

    public final void b() {
        if (a() != 0) {
            throw new IllegalStateException("Did not write as much data as expected.");
        }
    }

    public abstract void b(int i) throws IOException;

    public abstract void b(int i, int i2) throws IOException;

    public final void b(int i, long j) throws IOException {
        a(i, i(j));
    }

    public abstract void b(int i, aah aah) throws IOException;

    public final void b(long j) throws IOException {
        a(i(j));
    }

    public abstract void b(byte[] bArr, int i, int i2) throws IOException;

    public final void c(int i) throws IOException {
        b(m(i));
    }

    public abstract void c(int i, int i2) throws IOException;

    public abstract void c(int i, long j) throws IOException;

    public abstract void c(long j) throws IOException;

    /* access modifiers changed from: 0000 */
    public abstract void c(byte[] bArr, int i, int i2) throws IOException;

    public abstract void d(int i) throws IOException;

    public final void d(int i, int i2) throws IOException {
        c(i, m(i2));
    }

    public abstract void e(int i, int i2) throws IOException;
}
