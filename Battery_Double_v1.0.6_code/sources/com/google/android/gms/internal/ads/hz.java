package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.a.a;
import com.google.android.gms.common.internal.a.c;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

@cm
public final class hz extends a {
    public static final Creator<hz> CREATOR = new ia();

    /* renamed from: a reason: collision with root package name */
    public final String f3379a;

    /* renamed from: b reason: collision with root package name */
    public final String f3380b;
    public final boolean c;
    public final boolean d;
    public final List<String> e;
    public final boolean f;
    public final boolean g;
    public final List<String> h;

    public hz(String str, String str2, boolean z, boolean z2, List<String> list, boolean z3, boolean z4, List<String> list2) {
        this.f3379a = str;
        this.f3380b = str2;
        this.c = z;
        this.d = z2;
        this.e = list;
        this.f = z3;
        this.g = z4;
        if (list2 == null) {
            list2 = new ArrayList<>();
        }
        this.h = list2;
    }

    public static hz a(JSONObject jSONObject) throws JSONException {
        if (jSONObject == null) {
            return null;
        }
        return new hz(jSONObject.optString("click_string", ""), jSONObject.optString("report_url", ""), jSONObject.optBoolean("rendered_ad_enabled", false), jSONObject.optBoolean("non_malicious_reporting_enabled", false), lq.a(jSONObject.optJSONArray("allowed_headers"), null), jSONObject.optBoolean("protection_enabled", false), jSONObject.optBoolean("malicious_reporting_enabled", false), lq.a(jSONObject.optJSONArray("webview_permissions"), null));
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = c.a(parcel);
        c.a(parcel, 2, this.f3379a, false);
        c.a(parcel, 3, this.f3380b, false);
        c.a(parcel, 4, this.c);
        c.a(parcel, 5, this.d);
        c.b(parcel, 6, this.e, false);
        c.a(parcel, 7, this.f);
        c.a(parcel, 8, this.g);
        c.b(parcel, 9, this.h, false);
        c.a(parcel, a2);
    }
}
