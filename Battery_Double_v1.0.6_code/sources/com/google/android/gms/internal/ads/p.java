package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.RemoteException;
import android.view.View;
import android.webkit.WebView;
import com.google.android.gms.b.b;
import com.google.android.gms.dynamite.DynamiteModule;
import com.google.android.gms.dynamite.DynamiteModule.a;
import com.google.android.gms.dynamite.descriptors.com.google.android.gms.ads.dynamite.ModuleDescriptor;

@cm
public final class p {

    /* renamed from: a reason: collision with root package name */
    private static final Object f3589a = new Object();

    /* renamed from: b reason: collision with root package name */
    private static boolean f3590b = false;
    private static boolean c = false;
    private tf d;

    private final void c(Context context) {
        synchronized (f3589a) {
            if (((Boolean) ape.f().a(asi.dg)).booleanValue() && !c) {
                try {
                    c = true;
                    this.d = tg.a(DynamiteModule.a(context, DynamiteModule.f2374a, ModuleDescriptor.MODULE_ID).a("com.google.android.gms.ads.omid.DynamiteOmid"));
                } catch (a e) {
                    ms.d("#007 Could not call remote method.", e);
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        return null;
     */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    public final com.google.android.gms.b.a a(String str, WebView webView, String str2, String str3, String str4) {
        synchronized (f3589a) {
            if (((Boolean) ape.f().a(asi.dg)).booleanValue() && f3590b) {
                try {
                    return this.d.a(str, b.a(webView), str2, str3, str4);
                } catch (RemoteException | NullPointerException e) {
                    ms.d("#007 Could not call remote method.", e);
                    return null;
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        return;
     */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    public final void a(com.google.android.gms.b.a aVar) {
        synchronized (f3589a) {
            if (((Boolean) ape.f().a(asi.dg)).booleanValue() && f3590b) {
                try {
                    this.d.b(aVar);
                } catch (RemoteException | NullPointerException e) {
                    ms.d("#007 Could not call remote method.", e);
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        return;
     */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    public final void a(com.google.android.gms.b.a aVar, View view) {
        synchronized (f3589a) {
            if (((Boolean) ape.f().a(asi.dg)).booleanValue() && f3590b) {
                try {
                    this.d.a(aVar, b.a(view));
                } catch (RemoteException | NullPointerException e) {
                    ms.d("#007 Could not call remote method.", e);
                }
            }
        }
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    public final boolean a(Context context) {
        synchronized (f3589a) {
            if (!((Boolean) ape.f().a(asi.dg)).booleanValue()) {
                return false;
            }
            if (f3590b) {
                return true;
            }
            try {
                c(context);
                boolean a2 = this.d.a(b.a(context));
                f3590b = a2;
                return a2;
            } catch (RemoteException | NullPointerException e) {
                ms.d("#007 Could not call remote method.", e);
                return false;
            }
        }
    }

    public final String b(Context context) {
        if (!((Boolean) ape.f().a(asi.dg)).booleanValue()) {
            return null;
        }
        try {
            c(context);
            String str = "a.";
            String valueOf = String.valueOf(this.d.a());
            return valueOf.length() != 0 ? str.concat(valueOf) : new String(str);
        } catch (RemoteException | NullPointerException e) {
            ms.d("#007 Could not call remote method.", e);
            return null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        return;
     */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    public final void b(com.google.android.gms.b.a aVar) {
        synchronized (f3589a) {
            if (((Boolean) ape.f().a(asi.dg)).booleanValue() && f3590b) {
                try {
                    this.d.c(aVar);
                } catch (RemoteException | NullPointerException e) {
                    ms.d("#007 Could not call remote method.", e);
                }
            }
        }
    }
}
