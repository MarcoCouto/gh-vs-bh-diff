package com.google.android.gms.internal.ads;

import android.view.View;

public final class ajz implements alg {

    /* renamed from: a reason: collision with root package name */
    private final View f2651a;

    /* renamed from: b reason: collision with root package name */
    private final ir f2652b;

    public ajz(View view, ir irVar) {
        this.f2651a = view;
        this.f2652b = irVar;
    }

    public final View a() {
        return this.f2651a;
    }

    public final boolean b() {
        return this.f2652b == null || this.f2651a == null;
    }

    public final alg c() {
        return this;
    }
}
