package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.a.a;
import com.google.android.gms.common.internal.a.c;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

@cm
public final class axf extends a {
    public static final Creator<axf> CREATOR = new axg();

    /* renamed from: a reason: collision with root package name */
    private final String f2976a;

    /* renamed from: b reason: collision with root package name */
    private final String[] f2977b;
    private final String[] c;

    axf(String str, String[] strArr, String[] strArr2) {
        this.f2976a = str;
        this.f2977b = strArr;
        this.c = strArr2;
    }

    public static axf a(awb awb) throws a {
        Map b2 = awb.b();
        int size = b2.size();
        String[] strArr = new String[size];
        String[] strArr2 = new String[size];
        int i = 0;
        Iterator it = b2.entrySet().iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                return new axf(awb.e(), strArr, strArr2);
            }
            Entry entry = (Entry) it.next();
            strArr[i2] = (String) entry.getKey();
            strArr2[i2] = (String) entry.getValue();
            i = i2 + 1;
        }
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = c.a(parcel);
        c.a(parcel, 1, this.f2976a, false);
        c.a(parcel, 2, this.f2977b, false);
        c.a(parcel, 3, this.c, false);
        c.a(parcel, a2);
    }
}
