package com.google.android.gms.internal.ads;

@cm
final class po implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private oy f3608a;

    /* renamed from: b reason: collision with root package name */
    private boolean f3609b = false;

    po(oy oyVar) {
        this.f3608a = oyVar;
    }

    private final void c() {
        jv.f3440a.removeCallbacks(this);
        jv.f3440a.postDelayed(this, 250);
    }

    public final void a() {
        this.f3609b = true;
    }

    public final void b() {
        this.f3609b = false;
        c();
    }

    public final void run() {
        if (!this.f3609b) {
            this.f3608a.o();
            c();
        }
    }
}
