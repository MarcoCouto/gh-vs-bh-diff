package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.google.android.gms.b.a;

public final class gk extends ajk implements gh {
    gk(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.reward.client.IRewardedVideoAd");
    }

    public final void a() throws RemoteException {
        b(2, r_());
    }

    public final void a(a aVar) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (IInterface) aVar);
        b(9, r_);
    }

    public final void a(aqa aqa) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (IInterface) aqa);
        b(14, r_);
    }

    public final void a(gf gfVar) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (IInterface) gfVar);
        b(16, r_);
    }

    public final void a(gn gnVar) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (IInterface) gnVar);
        b(3, r_);
    }

    public final void a(gt gtVar) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (Parcelable) gtVar);
        b(1, r_);
    }

    public final void a(String str) throws RemoteException {
        Parcel r_ = r_();
        r_.writeString(str);
        b(13, r_);
    }

    public final void a(boolean z) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, z);
        b(34, r_);
    }

    public final Bundle b() throws RemoteException {
        Parcel a2 = a(15, r_());
        Bundle bundle = (Bundle) ajm.a(a2, Bundle.CREATOR);
        a2.recycle();
        return bundle;
    }

    public final void b(a aVar) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (IInterface) aVar);
        b(10, r_);
    }

    public final void c(a aVar) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (IInterface) aVar);
        b(11, r_);
    }

    public final boolean c() throws RemoteException {
        Parcel a2 = a(5, r_());
        boolean a3 = ajm.a(a2);
        a2.recycle();
        return a3;
    }

    public final void d() throws RemoteException {
        b(6, r_());
    }

    public final void e() throws RemoteException {
        b(7, r_());
    }

    public final void f() throws RemoteException {
        b(8, r_());
    }

    public final String g() throws RemoteException {
        Parcel a2 = a(12, r_());
        String readString = a2.readString();
        a2.recycle();
        return readString;
    }
}
