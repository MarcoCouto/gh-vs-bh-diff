package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.b.a;
import com.google.android.gms.b.a.C0046a;
import java.util.List;

public abstract class bdl extends ajl implements bdk {
    public bdl() {
        super("com.google.android.gms.ads.internal.mediation.client.IUnifiedNativeAdMapper");
    }

    /* access modifiers changed from: protected */
    public final boolean dispatchTransaction(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        switch (i) {
            case 2:
                String a2 = a();
                parcel2.writeNoException();
                parcel2.writeString(a2);
                break;
            case 3:
                List b2 = b();
                parcel2.writeNoException();
                parcel2.writeList(b2);
                break;
            case 4:
                String c = c();
                parcel2.writeNoException();
                parcel2.writeString(c);
                break;
            case 5:
                auw d = d();
                parcel2.writeNoException();
                ajm.a(parcel2, (IInterface) d);
                break;
            case 6:
                String e = e();
                parcel2.writeNoException();
                parcel2.writeString(e);
                break;
            case 7:
                String f = f();
                parcel2.writeNoException();
                parcel2.writeString(f);
                break;
            case 8:
                double g = g();
                parcel2.writeNoException();
                parcel2.writeDouble(g);
                break;
            case 9:
                String h = h();
                parcel2.writeNoException();
                parcel2.writeString(h);
                break;
            case 10:
                String i3 = i();
                parcel2.writeNoException();
                parcel2.writeString(i3);
                break;
            case 11:
                aqs j = j();
                parcel2.writeNoException();
                ajm.a(parcel2, (IInterface) j);
                break;
            case 12:
                aus k = k();
                parcel2.writeNoException();
                ajm.a(parcel2, (IInterface) k);
                break;
            case 13:
                a l = l();
                parcel2.writeNoException();
                ajm.a(parcel2, (IInterface) l);
                break;
            case 14:
                a m = m();
                parcel2.writeNoException();
                ajm.a(parcel2, (IInterface) m);
                break;
            case 15:
                a n = n();
                parcel2.writeNoException();
                ajm.a(parcel2, (IInterface) n);
                break;
            case 16:
                Bundle o = o();
                parcel2.writeNoException();
                ajm.b(parcel2, o);
                break;
            case 17:
                boolean p = p();
                parcel2.writeNoException();
                ajm.a(parcel2, p);
                break;
            case 18:
                boolean q = q();
                parcel2.writeNoException();
                ajm.a(parcel2, q);
                break;
            case 19:
                r();
                parcel2.writeNoException();
                break;
            case 20:
                a(C0046a.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
            case 21:
                a(C0046a.a(parcel.readStrongBinder()), C0046a.a(parcel.readStrongBinder()), C0046a.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
            case 22:
                b(C0046a.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
            default:
                return false;
        }
        return true;
    }
}
