package com.google.android.gms.internal.ads;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;

@cm
final class nl<T> implements nn<T> {

    /* renamed from: a reason: collision with root package name */
    private final Throwable f3547a;

    /* renamed from: b reason: collision with root package name */
    private final np f3548b = new np();

    nl(Throwable th) {
        this.f3547a = th;
        this.f3548b.a();
    }

    public final void a(Runnable runnable, Executor executor) {
        this.f3548b.a(runnable, executor);
    }

    public final boolean cancel(boolean z) {
        return false;
    }

    public final T get() throws ExecutionException {
        throw new ExecutionException(this.f3547a);
    }

    public final T get(long j, TimeUnit timeUnit) throws ExecutionException {
        throw new ExecutionException(this.f3547a);
    }

    public final boolean isCancelled() {
        return false;
    }

    public final boolean isDone() {
        return true;
    }
}
