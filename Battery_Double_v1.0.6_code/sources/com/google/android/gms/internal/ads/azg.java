package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.ads.internal.ax;
import java.util.Map;
import org.json.JSONObject;

@cm
public final class azg extends azt<bbe> implements azp, azv {

    /* renamed from: a reason: collision with root package name */
    private final so f3032a;

    /* JADX WARNING: type inference failed for: r3v0, types: [com.google.android.gms.internal.ads.azs, com.google.android.gms.internal.ads.azg] */
    public azg(Context context, mu muVar) throws qy {
        try {
            this.f3032a = new so(new sa(context));
            this.f3032a.setWillNotDraw(true);
            this.f3032a.a((sq) new azh(this));
            this.f3032a.a((ss) new azi(this));
            this.f3032a.addJavascriptInterface(new azo(this), "GoogleJsInterface");
            ax.e().a(context, muVar.f3528a, this.f3032a.getSettings());
        } catch (Throwable th) {
            throw new qy("Init failed.", th);
        }
    }

    public final void a() {
        this.f3032a.destroy();
    }

    public final void a(azw azw) {
        this.f3032a.a((su) new azl(azw));
    }

    public final void a(String str) {
        c(String.format("<!DOCTYPE html><html><head><script src=\"%s\"></script></head></html>", new Object[]{str}));
    }

    public final void a(String str, String str2) {
        azq.a((azp) this, str, str2);
    }

    public final void a(String str, Map map) {
        azq.a((azp) this, str, map);
    }

    public final void a(String str, JSONObject jSONObject) {
        azq.b(this, str, jSONObject);
    }

    public final bbf b() {
        return new bbg(this);
    }

    public final void b(String str) {
        nt.f3558a.execute(new azm(this, str));
    }

    public final void b(String str, JSONObject jSONObject) {
        azq.a((azp) this, str, jSONObject);
    }

    public final void c(String str) {
        nt.f3558a.execute(new azj(this, str));
    }

    public final void d(String str) {
        nt.f3558a.execute(new azk(this, str));
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ void e(String str) {
        this.f3032a.b(str);
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ void f(String str) {
        this.f3032a.loadUrl(str);
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ void g(String str) {
        this.f3032a.loadData(str, "text/html", "UTF-8");
    }

    public final /* bridge */ /* synthetic */ Object o() {
        if (this != null) {
            return this;
        }
        throw null;
    }
}
