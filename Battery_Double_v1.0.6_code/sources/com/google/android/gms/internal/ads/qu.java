package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.ads.internal.aq;
import com.google.android.gms.ads.internal.bu;

@cm
public final class qu {
    public static nn<qn> a(Context context, mu muVar, String str, ahh ahh, bu buVar) {
        return nc.a((nn<A>) nc.a(null), (mx<? super A, ? extends B>) new qv<Object,Object>(context, ahh, muVar, buVar, str), nt.f3558a);
    }

    public static qn a(Context context, sb sbVar, String str, boolean z, boolean z2, ahh ahh, mu muVar, asv asv, aq aqVar, bu buVar, amw amw) throws qy {
        asi.a(context);
        if (((Boolean) ape.f().a(asi.az)).booleanValue()) {
            return sh.a(context, sbVar, str, z2, z, ahh, muVar, asv, aqVar, buVar, amw);
        }
        try {
            return (qn) ly.a(new qw(context, sbVar, str, z, z2, ahh, muVar, asv, aqVar, buVar, amw));
        } catch (Throwable th) {
            throw new qy("Webview initialization failed.", th);
        }
    }
}
