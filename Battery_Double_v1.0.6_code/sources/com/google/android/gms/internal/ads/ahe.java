package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.content.Context;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.util.Iterator;
import java.util.LinkedList;

public abstract class ahe implements ahd {

    /* renamed from: a reason: collision with root package name */
    protected static volatile ahy f2583a = null;

    /* renamed from: b reason: collision with root package name */
    protected MotionEvent f2584b;
    protected LinkedList<MotionEvent> c = new LinkedList<>();
    protected long d = 0;
    protected long e = 0;
    protected long f = 0;
    protected long g = 0;
    protected long h = 0;
    protected long i = 0;
    protected long j = 0;
    protected double k;
    protected float l;
    protected float m;
    protected float n;
    protected float o;
    protected boolean p = false;
    protected DisplayMetrics q;
    private double r;
    private double s;
    private boolean t = false;

    protected ahe(Context context) {
        try {
            if (((Boolean) ape.f().a(asi.bL)).booleanValue()) {
                agi.a();
            } else {
                aie.a(f2583a);
            }
            this.q = context.getResources().getDisplayMetrics();
        } catch (Throwable th) {
        }
    }

    private final String a(Context context, String str, boolean z, View view, Activity activity, byte[] bArr) {
        zz a2;
        if (z) {
            try {
                a2 = a(context, view, activity);
                this.t = true;
            } catch (UnsupportedEncodingException | GeneralSecurityException e2) {
                return Integer.toString(7);
            } catch (Throwable th) {
                return Integer.toString(3);
            }
        } else {
            a2 = a(context, null);
        }
        return (a2 == null || a2.d() == 0) ? Integer.toString(5) : agi.a(a2, str);
    }

    /* access modifiers changed from: protected */
    public abstract long a(StackTraceElement[] stackTraceElementArr) throws ahv;

    /* access modifiers changed from: protected */
    public abstract zz a(Context context, View view, Activity activity);

    /* access modifiers changed from: protected */
    public abstract zz a(Context context, wv wvVar);

    public final String a(Context context) {
        if (aig.a()) {
            if (((Boolean) ape.f().a(asi.bN)).booleanValue()) {
                throw new IllegalStateException("The caller must not be called from the UI thread.");
            }
        }
        return a(context, null, false, null, null, null);
    }

    public final String a(Context context, String str, View view) {
        return a(context, str, view, null);
    }

    public final String a(Context context, String str, View view, Activity activity) {
        return a(context, str, true, view, activity, null);
    }

    public final void a(int i2, int i3, int i4) {
        if (this.f2584b != null) {
            this.f2584b.recycle();
        }
        if (this.q != null) {
            this.f2584b = MotionEvent.obtain(0, (long) i4, 1, ((float) i2) * this.q.density, ((float) i3) * this.q.density, 0.0f, 0.0f, 0, 0.0f, 0.0f, 0, 0);
        } else {
            this.f2584b = null;
        }
        this.p = false;
    }

    public final void a(MotionEvent motionEvent) {
        if (this.t) {
            this.g = 0;
            this.f = 0;
            this.e = 0;
            this.d = 0;
            this.h = 0;
            this.j = 0;
            this.i = 0;
            Iterator it = this.c.iterator();
            while (it.hasNext()) {
                ((MotionEvent) it.next()).recycle();
            }
            this.c.clear();
            this.f2584b = null;
            this.t = false;
        }
        switch (motionEvent.getAction()) {
            case 0:
                this.k = 0.0d;
                this.r = (double) motionEvent.getRawX();
                this.s = (double) motionEvent.getRawY();
                break;
            case 1:
            case 2:
                double rawX = (double) motionEvent.getRawX();
                double rawY = (double) motionEvent.getRawY();
                double d2 = rawX - this.r;
                double d3 = rawY - this.s;
                this.k = Math.sqrt((d2 * d2) + (d3 * d3)) + this.k;
                this.r = rawX;
                this.s = rawY;
                break;
        }
        switch (motionEvent.getAction()) {
            case 0:
                this.l = motionEvent.getX();
                this.m = motionEvent.getY();
                this.n = motionEvent.getRawX();
                this.o = motionEvent.getRawY();
                this.d++;
                break;
            case 1:
                this.f2584b = MotionEvent.obtain(motionEvent);
                this.c.add(this.f2584b);
                if (this.c.size() > 6) {
                    ((MotionEvent) this.c.remove()).recycle();
                }
                this.f++;
                try {
                    this.h = a(new Throwable().getStackTrace());
                    break;
                } catch (ahv e2) {
                    break;
                }
            case 2:
                this.e += (long) (motionEvent.getHistorySize() + 1);
                try {
                    aif b2 = b(motionEvent);
                    if ((b2 == null || b2.d == null || b2.g == null) ? false : true) {
                        this.i += b2.d.longValue() + b2.g.longValue();
                    }
                    if ((this.q == null || b2 == null || b2.e == null || b2.h == null) ? false : true) {
                        this.j = b2.h.longValue() + b2.e.longValue() + this.j;
                        break;
                    }
                } catch (ahv e3) {
                    break;
                }
                break;
            case 3:
                this.g++;
                break;
        }
        this.p = true;
    }

    public void a(View view) {
    }

    /* access modifiers changed from: protected */
    public abstract aif b(MotionEvent motionEvent) throws ahv;
}
