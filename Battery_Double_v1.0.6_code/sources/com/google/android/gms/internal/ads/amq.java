package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.a.b;

public final class amq implements Creator<amp> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        long j = 0;
        Bundle bundle = null;
        int b2 = b.b(parcel);
        boolean z = false;
        String str = null;
        String str2 = null;
        String str3 = null;
        long j2 = 0;
        String str4 = null;
        while (parcel.dataPosition() < b2) {
            int a2 = b.a(parcel);
            switch (b.a(a2)) {
                case 2:
                    str4 = b.k(parcel, a2);
                    break;
                case 3:
                    j2 = b.f(parcel, a2);
                    break;
                case 4:
                    str3 = b.k(parcel, a2);
                    break;
                case 5:
                    str2 = b.k(parcel, a2);
                    break;
                case 6:
                    str = b.k(parcel, a2);
                    break;
                case 7:
                    bundle = b.m(parcel, a2);
                    break;
                case 8:
                    z = b.c(parcel, a2);
                    break;
                case 9:
                    j = b.f(parcel, a2);
                    break;
                default:
                    b.b(parcel, a2);
                    break;
            }
        }
        b.r(parcel, b2);
        return new amp(str4, j2, str3, str2, str, bundle, z, j);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new amp[i];
    }
}
