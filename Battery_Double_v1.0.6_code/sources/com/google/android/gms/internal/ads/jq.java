package com.google.android.gms.internal.ads;

import android.os.Bundle;
import java.util.Iterator;

final class jq extends jr {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ Bundle f3434a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ jo f3435b;

    jq(jo joVar, Bundle bundle) {
        this.f3435b = joVar;
        this.f3434a = bundle;
        super(null);
    }

    public final void a() {
        Iterator it = this.f3435b.d.iterator();
        while (it.hasNext()) {
            ((js) it.next()).a(this.f3434a);
        }
    }
}
