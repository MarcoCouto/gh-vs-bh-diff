package com.google.android.gms.internal.ads;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;

final /* synthetic */ class kr implements OnClickListener {

    /* renamed from: a reason: collision with root package name */
    private final kp f3456a;

    /* renamed from: b reason: collision with root package name */
    private final int f3457b;
    private final int c;
    private final int d;

    kr(kp kpVar, int i, int i2, int i3) {
        this.f3456a = kpVar;
        this.f3457b = i;
        this.c = i2;
        this.d = i3;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f3456a.a(this.f3457b, this.c, this.d, dialogInterface, i);
    }
}
