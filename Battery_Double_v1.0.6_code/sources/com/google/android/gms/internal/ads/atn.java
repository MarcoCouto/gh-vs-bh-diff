package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.view.View;
import com.google.android.gms.b.a;
import com.google.android.gms.b.b;
import java.util.List;

@cm
public final class atn extends avm implements auc {

    /* renamed from: a reason: collision with root package name */
    private String f2906a;

    /* renamed from: b reason: collision with root package name */
    private List<atm> f2907b;
    private String c;
    private auw d;
    private String e;
    private double f;
    private String g;
    private String h;
    private ati i;
    private Bundle j;
    private aqs k;
    private View l;
    private a m;
    private String n;
    private Object o = new Object();
    /* access modifiers changed from: private */
    public aty p;

    public atn(String str, List<atm> list, String str2, auw auw, String str3, double d2, String str4, String str5, ati ati, Bundle bundle, aqs aqs, View view, a aVar, String str6) {
        this.f2906a = str;
        this.f2907b = list;
        this.c = str2;
        this.d = auw;
        this.e = str3;
        this.f = d2;
        this.g = str4;
        this.h = str5;
        this.i = ati;
        this.j = bundle;
        this.k = aqs;
        this.l = view;
        this.m = aVar;
        this.n = str6;
    }

    public final String a() {
        return this.f2906a;
    }

    public final void a(Bundle bundle) {
        synchronized (this.o) {
            if (this.p == null) {
                jm.c("#001 Attempt to perform click before app native ad initialized.");
            } else {
                this.p.b(bundle);
            }
        }
    }

    public final void a(aty aty) {
        synchronized (this.o) {
            this.p = aty;
        }
    }

    public final List b() {
        return this.f2907b;
    }

    public final boolean b(Bundle bundle) {
        boolean a2;
        synchronized (this.o) {
            if (this.p == null) {
                jm.c("#002 Attempt to record impression before native ad initialized.");
                a2 = false;
            } else {
                a2 = this.p.a(bundle);
            }
        }
        return a2;
    }

    public final String c() {
        return this.c;
    }

    public final void c(Bundle bundle) {
        synchronized (this.o) {
            if (this.p == null) {
                jm.c("#003 Attempt to report touch event before native ad initialized.");
            } else {
                this.p.c(bundle);
            }
        }
    }

    public final auw d() {
        return this.d;
    }

    public final String e() {
        return this.e;
    }

    public final double f() {
        return this.f;
    }

    public final String g() {
        return this.g;
    }

    public final String h() {
        return this.h;
    }

    public final aqs i() {
        return this.k;
    }

    public final a j() {
        return b.a(this.p);
    }

    public final String k() {
        return "2";
    }

    public final String l() {
        return "";
    }

    public final ati m() {
        return this.i;
    }

    public final Bundle n() {
        return this.j;
    }

    public final View o() {
        return this.l;
    }

    public final a p() {
        return this.m;
    }

    public final String q() {
        return this.n;
    }

    public final aus r() {
        return this.i;
    }

    public final void s() {
        jv.f3440a.post(new ato(this));
        this.f2906a = null;
        this.f2907b = null;
        this.c = null;
        this.d = null;
        this.e = null;
        this.f = 0.0d;
        this.g = null;
        this.h = null;
        this.i = null;
        this.j = null;
        this.o = null;
        this.k = null;
        this.l = null;
    }
}
