package com.google.android.gms.internal.ads;

import java.util.concurrent.Executor;

final /* synthetic */ class nq implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final Executor f3555a;

    /* renamed from: b reason: collision with root package name */
    private final Runnable f3556b;

    nq(Executor executor, Runnable runnable) {
        this.f3555a = executor;
        this.f3556b = runnable;
    }

    public final void run() {
        this.f3555a.execute(this.f3556b);
    }
}
