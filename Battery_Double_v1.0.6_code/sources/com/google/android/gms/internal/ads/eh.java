package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.ads.identifier.AdvertisingIdClient.Info;
import com.google.android.gms.ads.internal.ax;
import com.google.android.gms.ads.internal.gmsg.HttpClient;
import com.google.android.gms.ads.internal.gmsg.ae;
import com.google.android.gms.ads.internal.gmsg.b;
import com.google.android.gms.common.l;
import com.google.android.gms.common.m;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import org.json.JSONException;
import org.json.JSONObject;

@cm
public final class eh extends jh {

    /* renamed from: a reason: collision with root package name */
    private static final long f3280a = TimeUnit.SECONDS.toMillis(10);

    /* renamed from: b reason: collision with root package name */
    private static final Object f3281b = new Object();
    private static boolean c = false;
    /* access modifiers changed from: private */
    public static bah d = null;
    private static HttpClient e = null;
    /* access modifiers changed from: private */
    public static b f = null;
    private static ae<Object> g = null;
    /* access modifiers changed from: private */
    public final co h;
    private final dm i;
    private final Object j = new Object();
    private final Context k;
    /* access modifiers changed from: private */
    public bau l;
    private anb m;

    public eh(Context context, dm dmVar, co coVar, anb anb) {
        super(true);
        this.h = coVar;
        this.k = context;
        this.i = dmVar;
        this.m = anb;
        synchronized (f3281b) {
            if (!c) {
                f = new b();
                e = new HttpClient(context.getApplicationContext(), dmVar.j);
                g = new ep();
                d = new bah(this.k.getApplicationContext(), this.i.j, (String) ape.f().a(asi.f2875a), new eo(), new en());
                c = true;
            }
        }
    }

    private final dp a(dl dlVar) {
        ax.e();
        String a2 = jv.a();
        JSONObject a3 = a(dlVar, a2);
        if (a3 == null) {
            return new dp(0);
        }
        long b2 = ax.l().b();
        Future a4 = f.a(a2);
        mh.f3514a.post(new ej(this, a3, a2));
        try {
            JSONObject jSONObject = (JSONObject) a4.get(f3280a - (ax.l().b() - b2), TimeUnit.MILLISECONDS);
            if (jSONObject == null) {
                return new dp(-1);
            }
            dp a5 = ez.a(this.k, dlVar, jSONObject.toString());
            return (a5.d == -3 || !TextUtils.isEmpty(a5.f3266b)) ? a5 : new dp(3);
        } catch (InterruptedException | CancellationException e2) {
            return new dp(-1);
        } catch (TimeoutException e3) {
            return new dp(2);
        } catch (ExecutionException e4) {
            return new dp(0);
        }
    }

    private final JSONObject a(dl dlVar, String str) {
        fi fiVar;
        Info info;
        JSONObject jSONObject = 0;
        Bundle bundle = dlVar.c.c.getBundle("sdk_less_server_data");
        if (bundle == null) {
            return jSONObject;
        }
        try {
            fiVar = (fi) ax.p().a(this.k).get();
        } catch (Exception e2) {
            jm.c("Error grabbing device info: ", e2);
            fiVar = jSONObject;
        }
        Context context = this.k;
        es esVar = new es();
        esVar.j = dlVar;
        esVar.k = fiVar;
        JSONObject a2 = ez.a(context, esVar);
        if (a2 == null) {
            return jSONObject;
        }
        try {
            info = AdvertisingIdClient.getAdvertisingIdInfo(this.k);
        } catch (l | m | IOException | IllegalStateException e3) {
            jm.c("Cannot get advertising id info", e3);
            info = jSONObject;
        }
        HashMap hashMap = new HashMap();
        hashMap.put("request_id", str);
        hashMap.put("request_param", a2);
        hashMap.put("data", bundle);
        if (info != 0) {
            hashMap.put("adid", info.getId());
            hashMap.put("lat", Integer.valueOf(info.isLimitAdTrackingEnabled() ? 1 : 0));
        }
        try {
            return ax.e().a((Map<String, ?>) hashMap);
        } catch (JSONException e4) {
            return jSONObject;
        }
    }

    protected static void a(azv azv) {
        azv.a("/loadAd", f);
        azv.a("/fetchHttpRequest", e);
        azv.a("/invalidRequest", g);
    }

    protected static void b(azv azv) {
        azv.b("/loadAd", f);
        azv.b("/fetchHttpRequest", e);
        azv.b("/invalidRequest", g);
    }

    public final void a() {
        jm.b("SdkLessAdLoaderBackgroundTask started.");
        String j2 = ax.B().j(this.k);
        dl dlVar = new dl(this.i, -1, ax.B().h(this.k), ax.B().i(this.k), j2);
        ax.B().f(this.k, j2);
        dp a2 = a(dlVar);
        dl dlVar2 = dlVar;
        mh.f3514a.post(new ei(this, new is(dlVar2, a2, null, null, a2.d, ax.l().b(), a2.m, null, this.m)));
    }

    public final void c_() {
        synchronized (this.j) {
            mh.f3514a.post(new em(this));
        }
    }
}
