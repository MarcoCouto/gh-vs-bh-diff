package com.google.android.gms.internal.ads;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public final class awj extends ajk implements awh {
    awj(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.formats.client.IOnCustomTemplateAdLoadedListener");
    }

    public final void a(avt avt) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (IInterface) avt);
        b(1, r_);
    }
}
