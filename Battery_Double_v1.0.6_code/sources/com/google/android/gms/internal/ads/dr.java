package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.a.a;
import com.google.android.gms.common.internal.a.c;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@cm
public final class dr extends a {
    public static final Creator<dr> CREATOR = new ds();

    /* renamed from: a reason: collision with root package name */
    public final boolean f3267a;

    /* renamed from: b reason: collision with root package name */
    public final List<String> f3268b;

    public dr() {
        this(false, Collections.emptyList());
    }

    public dr(boolean z, List<String> list) {
        this.f3267a = z;
        this.f3268b = list;
    }

    public static dr a(JSONObject jSONObject) {
        if (jSONObject == null) {
            return new dr();
        }
        JSONArray optJSONArray = jSONObject.optJSONArray("reporting_urls");
        ArrayList arrayList = new ArrayList();
        if (optJSONArray != null) {
            for (int i = 0; i < optJSONArray.length(); i++) {
                try {
                    arrayList.add(optJSONArray.getString(i));
                } catch (JSONException e) {
                    jm.c("Error grabbing url from json.", e);
                }
            }
        }
        return new dr(jSONObject.optBoolean("enable_protection"), arrayList);
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = c.a(parcel);
        c.a(parcel, 2, this.f3267a);
        c.b(parcel, 3, this.f3268b, false);
        c.a(parcel, a2);
    }
}
