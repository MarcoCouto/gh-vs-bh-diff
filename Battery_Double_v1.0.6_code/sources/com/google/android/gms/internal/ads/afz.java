package com.google.android.gms.internal.ads;

import java.io.IOException;

public final class afz extends afh<afz> {
    private static volatile afz[] f;

    /* renamed from: a reason: collision with root package name */
    public Integer f2551a;

    /* renamed from: b reason: collision with root package name */
    public String f2552b;
    public afu c;
    public Integer d;
    public String[] e;
    private afw g;
    private Integer h;
    private int[] i;
    private String j;

    public afz() {
        this.f2551a = null;
        this.f2552b = null;
        this.c = null;
        this.g = null;
        this.h = null;
        this.i = afq.f2534a;
        this.j = null;
        this.d = null;
        this.e = afq.c;
        this.Y = null;
        this.Z = -1;
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public final afz a(afd afd) throws IOException {
        while (true) {
            int a2 = afd.a();
            switch (a2) {
                case 0:
                    break;
                case 8:
                    this.f2551a = Integer.valueOf(afd.c());
                    continue;
                case 18:
                    this.f2552b = afd.e();
                    continue;
                case 26:
                    if (this.c == null) {
                        this.c = new afu();
                    }
                    afd.a((afn) this.c);
                    continue;
                case 34:
                    if (this.g == null) {
                        this.g = new afw();
                    }
                    afd.a((afn) this.g);
                    continue;
                case 40:
                    this.h = Integer.valueOf(afd.c());
                    continue;
                case 48:
                    int a3 = afq.a(afd, 48);
                    int length = this.i == null ? 0 : this.i.length;
                    int[] iArr = new int[(a3 + length)];
                    if (length != 0) {
                        System.arraycopy(this.i, 0, iArr, 0, length);
                    }
                    while (length < iArr.length - 1) {
                        iArr[length] = afd.c();
                        afd.a();
                        length++;
                    }
                    iArr[length] = afd.c();
                    this.i = iArr;
                    continue;
                case 50:
                    int c2 = afd.c(afd.g());
                    int j2 = afd.j();
                    int i2 = 0;
                    while (afd.i() > 0) {
                        afd.c();
                        i2++;
                    }
                    afd.e(j2);
                    int length2 = this.i == null ? 0 : this.i.length;
                    int[] iArr2 = new int[(i2 + length2)];
                    if (length2 != 0) {
                        System.arraycopy(this.i, 0, iArr2, 0, length2);
                    }
                    while (length2 < iArr2.length) {
                        iArr2[length2] = afd.c();
                        length2++;
                    }
                    this.i = iArr2;
                    afd.d(c2);
                    continue;
                case 58:
                    this.j = afd.e();
                    continue;
                case 64:
                    int j3 = afd.j();
                    try {
                        int c3 = afd.c();
                        if (c3 < 0 || c3 > 3) {
                            throw new IllegalArgumentException(c3 + " is not a valid enum AdResourceType");
                        }
                        this.d = Integer.valueOf(c3);
                        continue;
                    } catch (IllegalArgumentException e2) {
                        afd.e(j3);
                        a(afd, a2);
                        break;
                    }
                case 74:
                    int a4 = afq.a(afd, 74);
                    int length3 = this.e == null ? 0 : this.e.length;
                    String[] strArr = new String[(a4 + length3)];
                    if (length3 != 0) {
                        System.arraycopy(this.e, 0, strArr, 0, length3);
                    }
                    while (length3 < strArr.length - 1) {
                        strArr[length3] = afd.e();
                        afd.a();
                        length3++;
                    }
                    strArr[length3] = afd.e();
                    this.e = strArr;
                    continue;
                default:
                    if (!super.a(afd, a2)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    public static afz[] b() {
        if (f == null) {
            synchronized (afl.f2531b) {
                if (f == null) {
                    f = new afz[0];
                }
            }
        }
        return f;
    }

    /* access modifiers changed from: protected */
    public final int a() {
        int a2 = super.a() + aff.b(1, this.f2551a.intValue());
        if (this.f2552b != null) {
            a2 += aff.b(2, this.f2552b);
        }
        if (this.c != null) {
            a2 += aff.b(3, (afn) this.c);
        }
        if (this.g != null) {
            a2 += aff.b(4, (afn) this.g);
        }
        if (this.h != null) {
            a2 += aff.b(5, this.h.intValue());
        }
        if (this.i != null && this.i.length > 0) {
            int i2 = 0;
            for (int a3 : this.i) {
                i2 += aff.a(a3);
            }
            a2 = a2 + i2 + (this.i.length * 1);
        }
        if (this.j != null) {
            a2 += aff.b(7, this.j);
        }
        if (this.d != null) {
            a2 += aff.b(8, this.d.intValue());
        }
        if (this.e == null || this.e.length <= 0) {
            return a2;
        }
        int i3 = 0;
        int i4 = 0;
        for (String str : this.e) {
            if (str != null) {
                i4++;
                i3 += aff.a(str);
            }
        }
        return a2 + i3 + (i4 * 1);
    }

    public final void a(aff aff) throws IOException {
        aff.a(1, this.f2551a.intValue());
        if (this.f2552b != null) {
            aff.a(2, this.f2552b);
        }
        if (this.c != null) {
            aff.a(3, (afn) this.c);
        }
        if (this.g != null) {
            aff.a(4, (afn) this.g);
        }
        if (this.h != null) {
            aff.a(5, this.h.intValue());
        }
        if (this.i != null && this.i.length > 0) {
            for (int a2 : this.i) {
                aff.a(6, a2);
            }
        }
        if (this.j != null) {
            aff.a(7, this.j);
        }
        if (this.d != null) {
            aff.a(8, this.d.intValue());
        }
        if (this.e != null && this.e.length > 0) {
            for (String str : this.e) {
                if (str != null) {
                    aff.a(9, str);
                }
            }
        }
        super.a(aff);
    }
}
