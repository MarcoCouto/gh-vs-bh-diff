package com.google.android.gms.internal.ads;

import android.os.IInterface;
import android.os.RemoteException;

public interface dt extends IInterface {
    dp a(dl dlVar) throws RemoteException;

    void a(dl dlVar, dw dwVar) throws RemoteException;

    void a(ee eeVar, dz dzVar) throws RemoteException;

    void b(ee eeVar, dz dzVar) throws RemoteException;
}
