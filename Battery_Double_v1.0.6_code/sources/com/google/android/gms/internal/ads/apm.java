package com.google.android.gms.internal.ads;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;

public final class apm extends ajk implements apk {
    apm(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.client.IAdListener");
    }

    public final void a() throws RemoteException {
        b(1, r_());
    }

    public final void a(int i) throws RemoteException {
        Parcel r_ = r_();
        r_.writeInt(i);
        b(2, r_);
    }

    public final void b() throws RemoteException {
        b(3, r_());
    }

    public final void c() throws RemoteException {
        b(4, r_());
    }

    public final void d() throws RemoteException {
        b(5, r_());
    }

    public final void e() throws RemoteException {
        b(6, r_());
    }

    public final void f() throws RemoteException {
        b(7, r_());
    }
}
