package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.app.Application.ActivityLifecycleCallbacks;

final class ahr implements ahu {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ Activity f2598a;

    ahr(ahm ahm, Activity activity) {
        this.f2598a = activity;
    }

    public final void a(ActivityLifecycleCallbacks activityLifecycleCallbacks) {
        activityLifecycleCallbacks.onActivityStopped(this.f2598a);
    }
}
