package com.google.android.gms.internal.ads;

import android.net.Uri;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.b.a;
import com.google.android.gms.b.a.C0046a;

public final class auy extends ajk implements auw {
    auy(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.formats.client.INativeAdImage");
    }

    public final a a() throws RemoteException {
        Parcel a2 = a(1, r_());
        a a3 = C0046a.a(a2.readStrongBinder());
        a2.recycle();
        return a3;
    }

    public final Uri b() throws RemoteException {
        Parcel a2 = a(2, r_());
        Uri uri = (Uri) ajm.a(a2, Uri.CREATOR);
        a2.recycle();
        return uri;
    }

    public final double c() throws RemoteException {
        Parcel a2 = a(3, r_());
        double readDouble = a2.readDouble();
        a2.recycle();
        return readDouble;
    }
}
