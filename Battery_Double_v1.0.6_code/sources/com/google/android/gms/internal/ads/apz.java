package com.google.android.gms.internal.ads;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.google.android.gms.b.a;

public final class apz extends ajk implements apy {
    apz(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.client.IAdManagerCreator");
    }

    public final IBinder a(a aVar, aot aot, String str, bcr bcr, int i, int i2) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (IInterface) aVar);
        ajm.a(r_, (Parcelable) aot);
        r_.writeString(str);
        ajm.a(r_, (IInterface) bcr);
        r_.writeInt(12451000);
        r_.writeInt(i2);
        Parcel a2 = a(2, r_);
        IBinder readStrongBinder = a2.readStrongBinder();
        a2.recycle();
        return readStrongBinder;
    }
}
