package com.google.android.gms.internal.ads;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.webkit.WebSettings;
import com.google.android.gms.common.util.q;
import java.util.concurrent.Callable;

final class mb implements Callable<String> {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ Context f3504a;

    mb(ma maVar, Context context) {
        this.f3504a = context;
    }

    public final /* synthetic */ Object call() throws Exception {
        SharedPreferences sharedPreferences = this.f3504a.getSharedPreferences("admob_user_agent", 0);
        String string = sharedPreferences.getString("user_agent", "");
        if (TextUtils.isEmpty(string)) {
            jm.a("User agent is not initialized on Google Play Services. Initializing.");
            String defaultUserAgent = WebSettings.getDefaultUserAgent(this.f3504a);
            q.a(this.f3504a, sharedPreferences.edit().putString("user_agent", defaultUserAgent), "admob_user_agent");
            return defaultUserAgent;
        }
        jm.a("User agent is already initialized on Google Play Services.");
        return string;
    }
}
