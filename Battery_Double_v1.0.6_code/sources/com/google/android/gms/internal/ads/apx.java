package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.google.android.gms.b.a;
import com.google.android.gms.b.a.C0046a;

public final class apx extends ajk implements apv {
    apx(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.client.IAdManager");
    }

    public final String D() throws RemoteException {
        Parcel a2 = a(31, r_());
        String readString = a2.readString();
        a2.recycle();
        return readString;
    }

    public final aqe E() throws RemoteException {
        aqe aqg;
        Parcel a2 = a(32, r_());
        IBinder readStrongBinder = a2.readStrongBinder();
        if (readStrongBinder == null) {
            aqg = null;
        } else {
            IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.IAppEventListener");
            aqg = queryLocalInterface instanceof aqe ? (aqe) queryLocalInterface : new aqg(readStrongBinder);
        }
        a2.recycle();
        return aqg;
    }

    public final apk F() throws RemoteException {
        apk apm;
        Parcel a2 = a(33, r_());
        IBinder readStrongBinder = a2.readStrongBinder();
        if (readStrongBinder == null) {
            apm = null;
        } else {
            IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.IAdListener");
            apm = queryLocalInterface instanceof apk ? (apk) queryLocalInterface : new apm(readStrongBinder);
        }
        a2.recycle();
        return apm;
    }

    public final void I() throws RemoteException {
        b(9, r_());
    }

    public final String a() throws RemoteException {
        Parcel a2 = a(18, r_());
        String readString = a2.readString();
        a2.recycle();
        return readString;
    }

    public final void a(af afVar, String str) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (IInterface) afVar);
        r_.writeString(str);
        b(15, r_);
    }

    public final void a(aot aot) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (Parcelable) aot);
        b(13, r_);
    }

    public final void a(aph aph) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (IInterface) aph);
        b(20, r_);
    }

    public final void a(apk apk) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (IInterface) apk);
        b(7, r_);
    }

    public final void a(aqa aqa) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (IInterface) aqa);
        b(36, r_);
    }

    public final void a(aqe aqe) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (IInterface) aqe);
        b(8, r_);
    }

    public final void a(aqk aqk) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (IInterface) aqk);
        b(21, r_);
    }

    public final void a(aqy aqy) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (Parcelable) aqy);
        b(30, r_);
    }

    public final void a(arr arr) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (Parcelable) arr);
        b(29, r_);
    }

    public final void a(atc atc) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (IInterface) atc);
        b(19, r_);
    }

    public final void a(gn gnVar) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (IInterface) gnVar);
        b(24, r_);
    }

    public final void a(y yVar) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (IInterface) yVar);
        b(14, r_);
    }

    public final void a(String str) throws RemoteException {
        Parcel r_ = r_();
        r_.writeString(str);
        b(25, r_);
    }

    public final void b(boolean z) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, z);
        b(22, r_);
    }

    public final boolean b(aop aop) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (Parcelable) aop);
        Parcel a2 = a(4, r_);
        boolean a3 = ajm.a(a2);
        a2.recycle();
        return a3;
    }

    public final void c(boolean z) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, z);
        b(34, r_);
    }

    public final void j() throws RemoteException {
        b(2, r_());
    }

    public final a k() throws RemoteException {
        Parcel a2 = a(1, r_());
        a a3 = C0046a.a(a2.readStrongBinder());
        a2.recycle();
        return a3;
    }

    public final aot l() throws RemoteException {
        Parcel a2 = a(12, r_());
        aot aot = (aot) ajm.a(a2, aot.CREATOR);
        a2.recycle();
        return aot;
    }

    public final boolean m() throws RemoteException {
        Parcel a2 = a(3, r_());
        boolean a3 = ajm.a(a2);
        a2.recycle();
        return a3;
    }

    public final void n() throws RemoteException {
        b(11, r_());
    }

    public final void o() throws RemoteException {
        b(5, r_());
    }

    public final void p() throws RemoteException {
        b(6, r_());
    }

    public final Bundle q() throws RemoteException {
        Parcel a2 = a(37, r_());
        Bundle bundle = (Bundle) ajm.a(a2, Bundle.CREATOR);
        a2.recycle();
        return bundle;
    }

    public final String q_() throws RemoteException {
        Parcel a2 = a(35, r_());
        String readString = a2.readString();
        a2.recycle();
        return readString;
    }

    public final void r() throws RemoteException {
        b(10, r_());
    }

    public final boolean s() throws RemoteException {
        Parcel a2 = a(23, r_());
        boolean a3 = ajm.a(a2);
        a2.recycle();
        return a3;
    }

    public final aqs t() throws RemoteException {
        aqs aqu;
        Parcel a2 = a(26, r_());
        IBinder readStrongBinder = a2.readStrongBinder();
        if (readStrongBinder == null) {
            aqu = null;
        } else {
            IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.IVideoController");
            aqu = queryLocalInterface instanceof aqs ? (aqs) queryLocalInterface : new aqu(readStrongBinder);
        }
        a2.recycle();
        return aqu;
    }
}
