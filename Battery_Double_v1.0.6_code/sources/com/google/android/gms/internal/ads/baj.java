package com.google.android.gms.internal.ads;

final /* synthetic */ class baj implements azw {

    /* renamed from: a reason: collision with root package name */
    private final bah f3069a;

    /* renamed from: b reason: collision with root package name */
    private final bay f3070b;
    private final azv c;

    baj(bah bah, bay bay, azv azv) {
        this.f3069a = bah;
        this.f3070b = bay;
        this.c = azv;
    }

    public final void a() {
        jv.f3440a.postDelayed(new bak(this.f3069a, this.f3070b, this.c), (long) bas.f3086b);
    }
}
