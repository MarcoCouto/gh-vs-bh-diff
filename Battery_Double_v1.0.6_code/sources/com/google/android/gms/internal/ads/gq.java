package com.google.android.gms.internal.ads;

import android.os.RemoteException;
import com.google.android.gms.ads.reward.a;

@cm
public final class gq implements a {

    /* renamed from: a reason: collision with root package name */
    private final gc f3342a;

    public gq(gc gcVar) {
        this.f3342a = gcVar;
    }

    public final String a() {
        String str = null;
        if (this.f3342a == null) {
            return str;
        }
        try {
            return this.f3342a.a();
        } catch (RemoteException e) {
            ms.c("Could not forward getType to RewardItem", e);
            return str;
        }
    }

    public final int b() {
        int i = 0;
        if (this.f3342a == null) {
            return i;
        }
        try {
            return this.f3342a.b();
        } catch (RemoteException e) {
            ms.c("Could not forward getAmount to RewardItem", e);
            return i;
        }
    }
}
