package com.google.android.gms.internal.ads;

final /* synthetic */ class bak implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final bah f3071a;

    /* renamed from: b reason: collision with root package name */
    private final bay f3072b;
    private final azv c;

    bak(bah bah, bay bay, azv azv) {
        this.f3071a = bah;
        this.f3072b = bay;
        this.c = azv;
    }

    public final void run() {
        this.f3071a.a(this.f3072b, this.c);
    }
}
