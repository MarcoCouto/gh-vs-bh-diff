package com.google.android.gms.internal.ads;

import android.text.TextUtils;
import com.google.android.gms.ads.internal.gmsg.ae;
import java.util.Map;
import org.json.JSONObject;

final class ca implements ae<qn> {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ qn f3218a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ ny f3219b;
    private final /* synthetic */ bu c;

    ca(bu buVar, qn qnVar, ny nyVar) {
        this.c = buVar;
        this.f3218a = qnVar;
        this.f3219b = nyVar;
    }

    public final /* synthetic */ void zza(Object obj, Map map) {
        JSONObject jSONObject;
        boolean z;
        try {
            String str = (String) map.get("success");
            String str2 = (String) map.get("failure");
            if (!TextUtils.isEmpty(str2)) {
                z = false;
                jSONObject = new JSONObject(str2);
            } else {
                jSONObject = new JSONObject(str);
                z = true;
            }
            if (this.c.h.equals(jSONObject.optString("ads_id", ""))) {
                this.f3218a.b("/nativeAdPreProcess", this);
                JSONObject jSONObject2 = new JSONObject();
                jSONObject2.put("success", z);
                jSONObject2.put("json", jSONObject);
                this.f3219b.b(jSONObject2);
            }
        } catch (Throwable th) {
            jm.b("Error while preprocessing json.", th);
            this.f3219b.a(th);
        }
    }
}
