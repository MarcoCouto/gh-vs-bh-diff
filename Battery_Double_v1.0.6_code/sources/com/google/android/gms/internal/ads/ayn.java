package com.google.android.gms.internal.ads;

import android.os.RemoteException;

final class ayn implements ays {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ gc f3005a;

    ayn(ayi ayi, gc gcVar) {
        this.f3005a = gcVar;
    }

    public final void a(ayt ayt) throws RemoteException {
        if (ayt.f != null) {
            ayt.f.a(this.f3005a);
        }
    }
}
