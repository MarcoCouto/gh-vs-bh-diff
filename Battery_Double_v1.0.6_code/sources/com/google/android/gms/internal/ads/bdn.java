package com.google.android.gms.internal.ads;

import android.os.RemoteException;

public final class bdn extends aqt {

    /* renamed from: a reason: collision with root package name */
    private final Object f3145a = new Object();

    /* renamed from: b reason: collision with root package name */
    private volatile aqv f3146b;

    public final void a() throws RemoteException {
        throw new RemoteException();
    }

    public final void a(aqv aqv) throws RemoteException {
        synchronized (this.f3145a) {
            this.f3146b = aqv;
        }
    }

    public final void a(boolean z) throws RemoteException {
        throw new RemoteException();
    }

    public final void b() throws RemoteException {
        throw new RemoteException();
    }

    public final boolean c() throws RemoteException {
        throw new RemoteException();
    }

    public final int d() throws RemoteException {
        throw new RemoteException();
    }

    public final float e() throws RemoteException {
        throw new RemoteException();
    }

    public final float f() throws RemoteException {
        throw new RemoteException();
    }

    public final float g() throws RemoteException {
        throw new RemoteException();
    }

    public final aqv h() throws RemoteException {
        aqv aqv;
        synchronized (this.f3145a) {
            aqv = this.f3146b;
        }
        return aqv;
    }

    public final boolean i() throws RemoteException {
        throw new RemoteException();
    }

    public final boolean j() throws RemoteException {
        throw new RemoteException();
    }
}
