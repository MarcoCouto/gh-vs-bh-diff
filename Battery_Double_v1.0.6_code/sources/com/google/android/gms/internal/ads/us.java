package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.xe.b;
import java.security.GeneralSecurityException;

final class us implements tz<tr> {
    us() {
    }

    /* access modifiers changed from: private */
    /* renamed from: d */
    public final tr a(aah aah) throws GeneralSecurityException {
        try {
            xv a2 = xv.a(aah);
            if (!(a2 instanceof xv)) {
                throw new GeneralSecurityException("expected KmsEnvelopeAeadKey proto");
            }
            xv xvVar = a2;
            zo.a(xvVar.a(), 0);
            String a3 = xvVar.b().a();
            return new ur(xvVar.b().b(), uc.a(a3).b(a3));
        } catch (abv e) {
            throw new GeneralSecurityException("expected serialized KmSEnvelopeAeadKey proto", e);
        }
    }

    public final int a() {
        return 0;
    }

    public final /* synthetic */ Object a(acw acw) throws GeneralSecurityException {
        if (!(acw instanceof xv)) {
            throw new GeneralSecurityException("expected KmsEnvelopeAeadKey proto");
        }
        xv xvVar = (xv) acw;
        zo.a(xvVar.a(), 0);
        String a2 = xvVar.b().a();
        return new ur(xvVar.b().b(), uc.a(a2).b(a2));
    }

    public final acw b(aah aah) throws GeneralSecurityException {
        try {
            return b((acw) xy.a(aah));
        } catch (abv e) {
            throw new GeneralSecurityException("expected serialized KmsEnvelopeAeadKeyFormat proto", e);
        }
    }

    public final acw b(acw acw) throws GeneralSecurityException {
        if (!(acw instanceof xy)) {
            throw new GeneralSecurityException("expected KmsEnvelopeAeadKeyFormat proto");
        }
        return xv.c().a((xy) acw).a(0).c();
    }

    public final xe c(aah aah) throws GeneralSecurityException {
        return (xe) xe.d().a("type.googleapis.com/google.crypto.tink.KmsEnvelopeAeadKey").a(((xv) b(aah)).h()).a(b.REMOTE).c();
    }
}
