package com.google.android.gms.internal.ads;

import android.annotation.TargetApi;
import android.graphics.Point;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.ViewTreeObserver.OnScrollChangedListener;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import com.google.android.gms.ads.internal.ax;
import com.google.android.gms.b.a;
import com.google.android.gms.b.b;
import com.google.android.gms.common.util.n;
import java.lang.ref.WeakReference;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@cm
public final class aun extends avc implements OnClickListener, OnTouchListener, OnGlobalLayoutListener, OnScrollChangedListener {

    /* renamed from: a reason: collision with root package name */
    private static final String[] f2939a = {"2011", "1009", "3010"};

    /* renamed from: b reason: collision with root package name */
    private final Object f2940b = new Object();
    private final FrameLayout c;
    private FrameLayout d;
    private View e;
    private final boolean f;
    private Map<String, WeakReference<View>> g = Collections.synchronizedMap(new HashMap());
    private View h;
    private aty i;
    private boolean j = false;
    private Point k = new Point();
    private Point l = new Point();
    private WeakReference<akr> m = new WeakReference<>(null);

    @TargetApi(21)
    public aun(FrameLayout frameLayout, FrameLayout frameLayout2) {
        this.c = frameLayout;
        this.d = frameLayout2;
        ax.A();
        og.a((View) this.c, (OnGlobalLayoutListener) this);
        ax.A();
        og.a((View) this.c, (OnScrollChangedListener) this);
        this.c.setOnTouchListener(this);
        this.c.setOnClickListener(this);
        if (frameLayout2 != null && n.i()) {
            frameLayout2.setElevation(Float.MAX_VALUE);
        }
        asi.a(this.c.getContext());
        this.f = ((Boolean) ape.f().a(asi.ci)).booleanValue();
    }

    private final int a(int i2) {
        ape.a();
        return mh.b(this.i.m(), i2);
    }

    private final void a(View view) {
        if (this.i != null) {
            aty aty = this.i instanceof atx ? ((atx) this.i).f() : this.i;
            if (aty != null) {
                aty.c(view);
            }
        }
    }

    private final void b() {
        synchronized (this.f2940b) {
            if (!this.f && this.j) {
                int measuredWidth = this.c.getMeasuredWidth();
                int measuredHeight = this.c.getMeasuredHeight();
                if (!(measuredWidth == 0 || measuredHeight == 0 || this.d == null)) {
                    this.d.setLayoutParams(new LayoutParams(measuredWidth, measuredHeight));
                    this.j = false;
                }
            }
        }
    }

    public final a a(String str) {
        Object obj = null;
        synchronized (this.f2940b) {
            if (this.g == null) {
                return null;
            }
            WeakReference weakReference = (WeakReference) this.g.get(str);
            if (weakReference != null) {
                obj = (View) weakReference.get();
            }
            a a2 = b.a(obj);
            return a2;
        }
    }

    public final void a() {
        synchronized (this.f2940b) {
            if (this.d != null) {
                this.d.removeAllViews();
            }
            this.d = null;
            this.g = null;
            this.h = null;
            this.i = null;
            this.k = null;
            this.l = null;
            this.m = null;
            this.e = null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:151:?, code lost:
        return;
     */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* JADX WARNING: Removed duplicated region for block: B:106:0x01cf A[Catch:{ Exception -> 0x0241 }] */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x0121 A[Catch:{ Exception -> 0x0241 }] */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x014b A[Catch:{ Exception -> 0x0241 }] */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x017f A[SYNTHETIC, Splitter:B:85:0x017f] */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x018f A[SYNTHETIC] */
    public final void a(a aVar) {
        ViewGroup viewGroup;
        qn qnVar;
        View view;
        View view2;
        synchronized (this.f2940b) {
            a((View) null);
            Object a2 = b.a(aVar);
            if (!(a2 instanceof aud)) {
                jm.e("Not an instance of native engine. This is most likely a transient error");
                return;
            }
            if (!this.f && this.d != null) {
                this.d.setLayoutParams(new LayoutParams(0, 0));
                this.c.requestLayout();
            }
            this.j = true;
            aud aud = (aud) a2;
            if (this.i != null) {
                if (((Boolean) ape.f().a(asi.bZ)).booleanValue()) {
                    this.i.b(this.c, this.g);
                }
            }
            if (this.i instanceof aud) {
                aud aud2 = (aud) this.i;
                if (!(aud2 == null || aud2.m() == null || !ax.B().c(this.c.getContext()))) {
                    ig n = aud2.n();
                    if (n != null) {
                        n.a(false);
                    }
                    akr akr = (akr) this.m.get();
                    if (!(akr == null || n == null)) {
                        akr.b((akv) n);
                    }
                }
            }
            if (!(this.i instanceof atx) || !((atx) this.i).e()) {
                this.i = aud;
                if (aud instanceof atx) {
                    ((atx) aud).a((aty) null);
                }
            } else {
                ((atx) this.i).a((aty) aud);
            }
            if (this.d != null) {
                if (((Boolean) ape.f().a(asi.bZ)).booleanValue()) {
                    this.d.setClickable(false);
                }
                this.d.removeAllViews();
                boolean a3 = aud.a();
                if (a3) {
                    if (this.g != null) {
                        String[] strArr = {"1098", "3011"};
                        int i2 = 0;
                        while (true) {
                            if (i2 >= 2) {
                                break;
                            }
                            WeakReference weakReference = (WeakReference) this.g.get(strArr[i2]);
                            if (weakReference != null) {
                                view2 = (View) weakReference.get();
                                break;
                            }
                            i2++;
                        }
                    }
                    view2 = null;
                    if (view2 instanceof ViewGroup) {
                        viewGroup = (ViewGroup) view2;
                        boolean z = !a3 && viewGroup != null;
                        this.h = aud.a((OnClickListener) this, z);
                        if (this.h != null) {
                            if (this.g != null) {
                                this.g.put("1007", new WeakReference(this.h));
                            }
                            if (z) {
                                viewGroup.removeAllViews();
                                viewGroup.addView(this.h);
                            } else {
                                com.google.android.gms.ads.b.a aVar2 = new com.google.android.gms.ads.b.a(aud.m());
                                aVar2.setLayoutParams(new LayoutParams(-1, -1));
                                aVar2.addView(this.h);
                                if (this.d != null) {
                                    this.d.addView(aVar2);
                                }
                            }
                        }
                        aud.a((View) this.c, this.g, null, (OnTouchListener) this, (OnClickListener) this);
                        if (this.f) {
                            if (this.e == null) {
                                this.e = new View(this.c.getContext());
                                this.e.setLayoutParams(new LayoutParams(-1, 0));
                            }
                            if (this.c != this.e.getParent()) {
                                this.c.addView(this.e);
                            }
                        }
                        qnVar = aud.g();
                        if (qnVar != null) {
                            if (this.d != null) {
                                this.d.addView(qnVar.getView());
                            }
                        }
                        synchronized (this.f2940b) {
                            aud.a(this.g);
                            if (this.g != null) {
                                String[] strArr2 = f2939a;
                                int length = strArr2.length;
                                int i3 = 0;
                                while (true) {
                                    if (i3 >= length) {
                                        break;
                                    }
                                    WeakReference weakReference2 = (WeakReference) this.g.get(strArr2[i3]);
                                    if (weakReference2 != null) {
                                        view = (View) weakReference2.get();
                                        break;
                                    }
                                    i3++;
                                }
                            }
                            view = null;
                            if (!(view instanceof FrameLayout)) {
                                aud.i();
                            } else {
                                auo auo = new auo(this, view);
                                if (aud instanceof atx) {
                                    aud.b(view, (atw) auo);
                                } else {
                                    aud.a(view, (atw) auo);
                                }
                            }
                        }
                        aud.d(this.c);
                        a((View) this.c);
                        this.i.b((View) this.c);
                        if (this.i instanceof aud) {
                            aud aud3 = (aud) this.i;
                            if (!(aud3 == null || aud3.m() == null || !ax.B().c(this.c.getContext()))) {
                                akr akr2 = (akr) this.m.get();
                                if (akr2 == null) {
                                    akr2 = new akr(this.c.getContext(), this.c);
                                    this.m = new WeakReference<>(akr2);
                                }
                                akr2.a((akv) aud3.n());
                            }
                        }
                    }
                }
                viewGroup = null;
                if (!a3) {
                }
                this.h = aud.a((OnClickListener) this, z);
                if (this.h != null) {
                }
                aud.a((View) this.c, this.g, null, (OnTouchListener) this, (OnClickListener) this);
                if (this.f) {
                }
                try {
                    qnVar = aud.g();
                } catch (Exception e2) {
                    ax.g();
                    if (kb.e()) {
                        jm.e("Privileged processes cannot create HTML overlays.");
                        qnVar = null;
                    } else {
                        jm.b("Error obtaining overlay.", e2);
                        qnVar = null;
                    }
                }
                if (qnVar != null) {
                }
                synchronized (this.f2940b) {
                }
                aud.d(this.c);
                a((View) this.c);
                this.i.b((View) this.c);
                if (this.i instanceof aud) {
                }
            }
        }
    }

    public final void a(a aVar, int i2) {
        if (ax.B().c(this.c.getContext()) && this.m != null) {
            akr akr = (akr) this.m.get();
            if (akr != null) {
                akr.a();
            }
        }
        b();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        return;
     */
    public final void a(String str, a aVar) {
        View view = (View) b.a(aVar);
        synchronized (this.f2940b) {
            if (this.g != null) {
                if (view == null) {
                    this.g.remove(str);
                } else {
                    this.g.put(str, new WeakReference(view));
                    if (!"1098".equals(str) && !"3011".equals(str)) {
                        view.setOnTouchListener(this);
                        view.setClickable(true);
                        view.setOnClickListener(this);
                    }
                }
            }
        }
    }

    public final void b(a aVar) {
        this.i.a((View) b.a(aVar));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        return;
     */
    public final void onClick(View view) {
        synchronized (this.f2940b) {
            if (this.i != null) {
                this.i.c();
                Bundle bundle = new Bundle();
                bundle.putFloat("x", (float) a(this.k.x));
                bundle.putFloat("y", (float) a(this.k.y));
                bundle.putFloat("start_x", (float) a(this.l.x));
                bundle.putFloat("start_y", (float) a(this.l.y));
                if (this.h == null || !this.h.equals(view)) {
                    this.i.a(view, this.g, bundle, this.c);
                } else if (!(this.i instanceof atx)) {
                    this.i.a(view, "1007", bundle, this.g, this.c);
                } else if (((atx) this.i).f() != null) {
                    ((atx) this.i).f().a(view, "1007", bundle, this.g, this.c);
                }
            }
        }
    }

    public final void onGlobalLayout() {
        synchronized (this.f2940b) {
            b();
            if (this.i != null) {
                this.i.c(this.c, this.g);
            }
        }
    }

    public final void onScrollChanged() {
        synchronized (this.f2940b) {
            if (this.i != null) {
                this.i.c(this.c, this.g);
            }
            b();
        }
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        synchronized (this.f2940b) {
            if (this.i != null) {
                int[] iArr = new int[2];
                this.c.getLocationOnScreen(iArr);
                Point point = new Point((int) (motionEvent.getRawX() - ((float) iArr[0])), (int) (motionEvent.getRawY() - ((float) iArr[1])));
                this.k = point;
                if (motionEvent.getAction() == 0) {
                    this.l = point;
                }
                MotionEvent obtain = MotionEvent.obtain(motionEvent);
                obtain.setLocation((float) point.x, (float) point.y);
                this.i.a(obtain);
                obtain.recycle();
            }
        }
        return false;
    }
}
