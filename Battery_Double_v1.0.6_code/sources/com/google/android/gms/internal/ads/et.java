package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.ads.internal.ax;

@cm
public final class et {

    /* renamed from: a reason: collision with root package name */
    public final ff f3292a = null;

    /* renamed from: b reason: collision with root package name */
    public final amr f3293b;
    public final im c;
    public final arw d;
    public final fp e;
    public final bbx f;
    public final fq g;
    public final fr h;
    public final x i;
    public final iq j;
    public final boolean k;
    public final ey l;

    private et(ff ffVar, amr amr, im imVar, arw arw, fp fpVar, bbx bbx, fq fqVar, fr frVar, x xVar, iq iqVar, boolean z, ey eyVar) {
        this.f3293b = amr;
        this.c = imVar;
        this.d = arw;
        this.e = fpVar;
        this.f = bbx;
        this.g = fqVar;
        this.h = frVar;
        this.i = xVar;
        this.j = iqVar;
        this.k = true;
        this.l = eyVar;
    }

    public static et a(Context context) {
        ax.C().a(context);
        fv fvVar = new fv(context);
        return new et(null, new amu(), new in(), new arv(), new fn(context, fvVar.b()), new bby(), new ft(), new fu(), new w(), new io(), true, fvVar);
    }
}
