package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.RemoteException;
import com.google.android.gms.b.b;
import com.google.android.gms.dynamite.DynamiteModule;
import com.google.android.gms.dynamite.DynamiteModule.a;
import com.google.android.gms.dynamite.descriptors.com.google.android.gms.ads.dynamite.ModuleDescriptor;

@cm
public final class anb {

    /* renamed from: a reason: collision with root package name */
    ajo f2745a;

    /* renamed from: b reason: collision with root package name */
    boolean f2746b;

    public anb() {
    }

    public anb(Context context) {
        asi.a(context);
        if (((Boolean) ape.f().a(asi.db)).booleanValue()) {
            try {
                this.f2745a = ajp.a(DynamiteModule.a(context, DynamiteModule.f2374a, ModuleDescriptor.MODULE_ID).a("com.google.android.gms.ads.clearcut.DynamiteClearcutLogger"));
                b.a(context);
                this.f2745a.a(b.a(context), "GMA_SDK");
                this.f2746b = true;
            } catch (RemoteException | a | NullPointerException e) {
                ms.b("Cannot dynamite load clearcut");
            }
        }
    }

    public anb(Context context, String str, String str2) {
        asi.a(context);
        try {
            this.f2745a = ajp.a(DynamiteModule.a(context, DynamiteModule.f2374a, ModuleDescriptor.MODULE_ID).a("com.google.android.gms.ads.clearcut.DynamiteClearcutLogger"));
            b.a(context);
            this.f2745a.a(b.a(context), str, null);
            this.f2746b = true;
        } catch (RemoteException | a | NullPointerException e) {
            ms.b("Cannot dynamite load clearcut");
        }
    }

    public final and a(byte[] bArr) {
        return new and(this, bArr);
    }
}
