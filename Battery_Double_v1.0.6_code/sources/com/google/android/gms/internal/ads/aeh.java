package com.google.android.gms.internal.ads;

import java.util.List;

public final class aeh extends RuntimeException {

    /* renamed from: a reason: collision with root package name */
    private final List<String> f2503a = null;

    public aeh(acw acw) {
        super("Message was missing required fields.  (Lite runtime could not determine which fields were missing).");
    }

    public final abv a() {
        return new abv(getMessage());
    }
}
