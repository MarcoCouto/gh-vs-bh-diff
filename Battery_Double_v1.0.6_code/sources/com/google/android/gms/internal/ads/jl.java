package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.common.l;
import com.google.android.gms.common.m;
import java.io.IOException;

final class jl extends jh {

    /* renamed from: a reason: collision with root package name */
    private Context f3429a;

    jl(Context context) {
        this.f3429a = context;
    }

    public final void a() {
        boolean z;
        try {
            z = AdvertisingIdClient.getIsAdIdFakeForDebugLogging(this.f3429a);
        } catch (l | m | IOException | IllegalStateException e) {
            jm.b("Fail to get isAdIdFakeForDebugLogging", e);
            z = false;
        }
        ml.a(z);
        jm.e("Update ad debug logging enablement as " + z);
    }

    public final void c_() {
    }
}
