package com.google.android.gms.internal.ads;

import java.util.Map.Entry;

final class adz implements Comparable<adz>, Entry<K, V> {

    /* renamed from: a reason: collision with root package name */
    private final K f2495a;

    /* renamed from: b reason: collision with root package name */
    private V f2496b;
    private final /* synthetic */ ads c;

    adz(ads ads, K k, V v) {
        this.c = ads;
        this.f2495a = k;
        this.f2496b = v;
    }

    adz(ads ads, Entry<K, V> entry) {
        this(ads, (Comparable) entry.getKey(), entry.getValue());
    }

    private static boolean a(Object obj, Object obj2) {
        return obj == null ? obj2 == null : obj.equals(obj2);
    }

    public final /* synthetic */ int compareTo(Object obj) {
        return ((Comparable) getKey()).compareTo((Comparable) ((adz) obj).getKey());
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Entry)) {
            return false;
        }
        Entry entry = (Entry) obj;
        return a(this.f2495a, entry.getKey()) && a(this.f2496b, entry.getValue());
    }

    public final /* synthetic */ Object getKey() {
        return this.f2495a;
    }

    public final V getValue() {
        return this.f2496b;
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = this.f2495a == null ? 0 : this.f2495a.hashCode();
        if (this.f2496b != null) {
            i = this.f2496b.hashCode();
        }
        return hashCode ^ i;
    }

    public final V setValue(V v) {
        this.c.f();
        V v2 = this.f2496b;
        this.f2496b = v;
        return v2;
    }

    public final String toString() {
        String valueOf = String.valueOf(this.f2495a);
        String valueOf2 = String.valueOf(this.f2496b);
        return new StringBuilder(String.valueOf(valueOf).length() + 1 + String.valueOf(valueOf2).length()).append(valueOf).append("=").append(valueOf2).toString();
    }
}
