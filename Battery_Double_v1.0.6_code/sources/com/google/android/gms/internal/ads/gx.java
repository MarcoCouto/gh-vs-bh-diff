package com.google.android.gms.internal.ads;

import android.os.RemoteException;
import com.google.android.gms.b.b;

final class gx implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ bcu f3350a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ aop f3351b;
    private final /* synthetic */ hd c;
    private final /* synthetic */ gv d;

    gx(gv gvVar, bcu bcu, aop aop, hd hdVar) {
        this.d = gvVar;
        this.f3350a = bcu;
        this.f3351b = aop;
        this.c = hdVar;
    }

    public final void run() {
        try {
            this.f3350a.a(b.a(this.d.c), this.f3351b, (String) null, (hl) this.c, this.d.g);
        } catch (RemoteException e) {
            RemoteException remoteException = e;
            String str = "Fail to initialize adapter ";
            String valueOf = String.valueOf(this.d.f3346a);
            jm.c(valueOf.length() != 0 ? str.concat(valueOf) : new String(str), remoteException);
            this.d.a(this.d.f3346a, 0);
        }
    }
}
