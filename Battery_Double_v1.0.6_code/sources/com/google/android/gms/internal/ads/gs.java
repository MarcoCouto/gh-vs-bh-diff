package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.reward.a;
import com.google.android.gms.ads.reward.b;

@cm
public final class gs extends go {

    /* renamed from: a reason: collision with root package name */
    private b f3343a;

    public gs(b bVar) {
        this.f3343a = bVar;
    }

    public final void a() {
        if (this.f3343a != null) {
            this.f3343a.a();
        }
    }

    public final void a(int i) {
        if (this.f3343a != null) {
            this.f3343a.a(i);
        }
    }

    public final void a(gc gcVar) {
        if (this.f3343a != null) {
            this.f3343a.a((a) new gq(gcVar));
        }
    }

    public final void b() {
        if (this.f3343a != null) {
            this.f3343a.b();
        }
    }

    public final void c() {
        if (this.f3343a != null) {
            this.f3343a.c();
        }
    }

    public final void d() {
        if (this.f3343a != null) {
            this.f3343a.d();
        }
    }

    public final void e() {
        if (this.f3343a != null) {
            this.f3343a.e();
        }
    }

    public final void f() {
        if (this.f3343a != null) {
            this.f3343a.f();
        }
    }
}
