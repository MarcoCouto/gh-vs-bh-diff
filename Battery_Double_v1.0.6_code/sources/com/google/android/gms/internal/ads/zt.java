package com.google.android.gms.internal.ads;

import java.io.PrintWriter;
import java.util.List;

final class zt extends zq {

    /* renamed from: a reason: collision with root package name */
    private final zr f3827a = new zr();

    zt() {
    }

    public final void a(Throwable th, PrintWriter printWriter) {
        th.printStackTrace(printWriter);
        List<Throwable> a2 = this.f3827a.a(th, false);
        if (a2 != null) {
            synchronized (a2) {
                for (Throwable th2 : a2) {
                    printWriter.print("Suppressed: ");
                    th2.printStackTrace(printWriter);
                }
            }
        }
    }
}
