package com.google.android.gms.internal.ads;

import android.text.TextUtils;
import com.google.android.gms.ads.internal.ax;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@cm
public final class asv {

    /* renamed from: a reason: collision with root package name */
    private boolean f2889a;

    /* renamed from: b reason: collision with root package name */
    private final List<ast> f2890b = new LinkedList();
    private final Map<String, String> c = new LinkedHashMap();
    private final Object d = new Object();
    private String e;
    private asv f;

    public asv(boolean z, String str, String str2) {
        this.f2889a = z;
        this.c.put("action", str);
        this.c.put("ad_format", str2);
    }

    public final ast a() {
        return a(ax.l().b());
    }

    public final ast a(long j) {
        if (!this.f2889a) {
            return null;
        }
        return new ast(j, null, null);
    }

    public final void a(asv asv) {
        synchronized (this.d) {
            this.f = asv;
        }
    }

    public final void a(String str) {
        if (this.f2889a) {
            synchronized (this.d) {
                this.e = str;
            }
        }
    }

    public final void a(String str, String str2) {
        if (this.f2889a && !TextUtils.isEmpty(str2)) {
            asl b2 = ax.i().b();
            if (b2 != null) {
                synchronized (this.d) {
                    asp a2 = b2.a(str);
                    Map<String, String> map = this.c;
                    map.put(str, a2.a((String) map.get(str), str2));
                }
            }
        }
    }

    public final boolean a(ast ast, long j, String... strArr) {
        synchronized (this.d) {
            for (String ast2 : strArr) {
                this.f2890b.add(new ast(j, ast2, ast));
            }
        }
        return true;
    }

    public final boolean a(ast ast, String... strArr) {
        if (!this.f2889a || ast == null) {
            return false;
        }
        return a(ast, ax.l().b(), strArr);
    }

    public final String b() {
        String sb;
        StringBuilder sb2 = new StringBuilder();
        synchronized (this.d) {
            for (ast ast : this.f2890b) {
                long a2 = ast.a();
                String b2 = ast.b();
                ast c2 = ast.c();
                if (c2 != null && a2 > 0) {
                    sb2.append(b2).append('.').append(a2 - c2.a()).append(',');
                }
            }
            this.f2890b.clear();
            if (!TextUtils.isEmpty(this.e)) {
                sb2.append(this.e);
            } else if (sb2.length() > 0) {
                sb2.setLength(sb2.length() - 1);
            }
            sb = sb2.toString();
        }
        return sb;
    }

    /* access modifiers changed from: 0000 */
    public final Map<String, String> c() {
        Map<String, String> a2;
        synchronized (this.d) {
            asl b2 = ax.i().b();
            a2 = (b2 == null || this.f == null) ? this.c : b2.a(this.c, this.f.c());
        }
        return a2;
    }

    public final ast d() {
        synchronized (this.d) {
        }
        return null;
    }
}
