package com.google.android.gms.internal.ads;

@cm
public final class ar extends jh {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public final ap f2842a;

    /* renamed from: b reason: collision with root package name */
    private final dp f2843b = this.c.f3398b;
    private final is c;

    public ar(is isVar, ap apVar) {
        this.c = isVar;
        this.f2842a = apVar;
    }

    public final void a() {
        jv.f3440a.post(new as(this, new ir(this.c.f3397a.c, null, null, 0, null, null, this.f2843b.k, this.f2843b.j, this.c.f3397a.i, false, null, null, null, null, null, this.f2843b.h, this.c.d, this.f2843b.f, this.c.f, this.f2843b.m, this.f2843b.n, this.c.h, null, null, null, null, this.c.f3398b.D, this.c.f3398b.E, null, null, null, this.c.i, this.c.f3398b.O, this.c.j, this.c.f3398b.Q, null, this.c.f3398b.S, this.c.f3398b.T)));
    }

    public final void c_() {
    }
}
