package com.google.android.gms.internal.ads;

import android.content.Context;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

@cm
public final class hf extends jh implements he {

    /* renamed from: a reason: collision with root package name */
    private final is f3360a;

    /* renamed from: b reason: collision with root package name */
    private final Context f3361b;
    private final ArrayList<gv> c;
    private final List<gy> d;
    private final HashSet<String> e;
    private final Object f;
    private final fw g;
    private final long h;

    public hf(Context context, is isVar, fw fwVar) {
        long longValue = ((Long) ape.f().a(asi.aE)).longValue();
        this(context, isVar, fwVar, longValue);
    }

    private hf(Context context, is isVar, fw fwVar, long j) {
        this.c = new ArrayList<>();
        this.d = new ArrayList();
        this.e = new HashSet<>();
        this.f = new Object();
        this.f3361b = context;
        this.f3360a = isVar;
        this.g = fwVar;
        this.h = j;
    }

    private final ir a(int i, String str, bca bca) {
        String substring;
        int i2;
        aop aop = this.f3360a.f3397a.c;
        List<String> list = this.f3360a.f3398b.c;
        List<String> list2 = this.f3360a.f3398b.e;
        List<String> list3 = this.f3360a.f3398b.i;
        int i3 = this.f3360a.f3398b.k;
        long j = this.f3360a.f3398b.j;
        String str2 = this.f3360a.f3397a.i;
        boolean z = this.f3360a.f3398b.g;
        bcb bcb = this.f3360a.c;
        long j2 = this.f3360a.f3398b.h;
        aot aot = this.f3360a.d;
        long j3 = this.f3360a.f3398b.f;
        long j4 = this.f3360a.f;
        long j5 = this.f3360a.f3398b.m;
        String str3 = this.f3360a.f3398b.n;
        JSONObject jSONObject = this.f3360a.h;
        hp hpVar = this.f3360a.f3398b.A;
        List<String> list4 = this.f3360a.f3398b.B;
        List<String> list5 = this.f3360a.f3398b.C;
        boolean z2 = this.f3360a.f3398b.D;
        dr drVar = this.f3360a.f3398b.E;
        StringBuilder sb = new StringBuilder("");
        if (this.d == null) {
            substring = sb.toString();
        } else {
            for (gy gyVar : this.d) {
                if (gyVar != null && !TextUtils.isEmpty(gyVar.f3352a)) {
                    String str4 = gyVar.f3352a;
                    switch (gyVar.f3353b) {
                        case 3:
                            i2 = 1;
                            break;
                        case 4:
                            i2 = 2;
                            break;
                        case 5:
                            i2 = 4;
                            break;
                        case 6:
                            i2 = 0;
                            break;
                        case 7:
                            i2 = 3;
                            break;
                        default:
                            i2 = 6;
                            break;
                    }
                    long j6 = gyVar.c;
                    StringBuilder sb2 = new StringBuilder(String.valueOf(str4).length() + 33);
                    sb.append(String.valueOf(sb2.append(str4).append(".").append(i2).append(".").append(j6).toString()).concat("_"));
                }
            }
            substring = sb.substring(0, Math.max(0, sb.length() - 1));
        }
        return new ir(aop, null, list, i, list2, list3, i3, j, str2, z, bca, null, str, bcb, null, j2, aot, j3, j4, j5, str3, jSONObject, null, hpVar, list4, list5, z2, drVar, substring, this.f3360a.f3398b.H, this.f3360a.f3398b.L, this.f3360a.i, this.f3360a.f3398b.O, this.f3360a.j, this.f3360a.f3398b.Q, this.f3360a.f3398b.R, this.f3360a.f3398b.S, this.f3360a.f3398b.T);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:105:0x0190, code lost:
        r2 = r3;
     */
    public final void a() {
        String str;
        for (bca bca : this.f3360a.c.f3121a) {
            String str2 = bca.k;
            Iterator it = bca.c.iterator();
            while (true) {
                if (it.hasNext()) {
                    String str3 = (String) it.next();
                    if ("com.google.android.gms.ads.mediation.customevent.CustomEventAdapter".equals(str3) || "com.google.ads.mediation.customevent.CustomEventAdapter".equals(str3)) {
                        try {
                            str = new JSONObject(str2).getString("class_name");
                        } catch (JSONException e2) {
                            jm.b("Unable to determine custom event class name, skipping...", e2);
                        }
                    } else {
                        str = str3;
                    }
                    synchronized (this.f) {
                        hk a2 = this.g.a(str);
                        if (a2 == null || a2.b() == null || a2.a() == null) {
                            this.d.add(new ha().b(bca.d).a(str).a(0).a(7).a());
                        } else {
                            gv gvVar = new gv(this.f3361b, str, str2, bca, this.f3360a, a2, this, this.h);
                            gvVar.a(this.g.a());
                            this.c.add(gvVar);
                        }
                    }
                }
            }
        }
        HashSet hashSet = new HashSet();
        ArrayList arrayList = this.c;
        int size = arrayList.size();
        int i = 0;
        while (i < size) {
            Object obj = arrayList.get(i);
            i++;
            gv gvVar2 = (gv) obj;
            if (hashSet.add(gvVar2.f3346a)) {
                gvVar2.d();
            }
        }
        ArrayList arrayList2 = this.c;
        int size2 = arrayList2.size();
        int i2 = 0;
        while (i2 < size2) {
            int i3 = i2 + 1;
            gv gvVar3 = (gv) arrayList2.get(i2);
            try {
                gvVar3.d().get();
                synchronized (this.f) {
                    if (!TextUtils.isEmpty(gvVar3.f3346a)) {
                        this.d.add(gvVar3.e());
                    }
                }
                synchronized (this.f) {
                    if (this.e.contains(gvVar3.f3346a)) {
                        mh.f3514a.post(new hg(this, a(-2, gvVar3.f3346a, gvVar3.f())));
                        return;
                    }
                }
            } catch (InterruptedException e3) {
                Thread.currentThread().interrupt();
                synchronized (this.f) {
                    if (!TextUtils.isEmpty(gvVar3.f3346a)) {
                        this.d.add(gvVar3.e());
                    }
                }
            } catch (Exception e4) {
                jm.c("Unable to resolve rewarded adapter.", e4);
                synchronized (this.f) {
                    if (!TextUtils.isEmpty(gvVar3.f3346a)) {
                        this.d.add(gvVar3.e());
                    }
                    i2 = i3;
                }
            } catch (Throwable th) {
                synchronized (this.f) {
                    if (!TextUtils.isEmpty(gvVar3.f3346a)) {
                        this.d.add(gvVar3.e());
                    }
                    throw th;
                }
            }
        }
        mh.f3514a.post(new hh(this, a(3, null, null)));
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ void a(ir irVar) {
        this.g.b().b(irVar);
    }

    public final void a(String str) {
        synchronized (this.f) {
            this.e.add(str);
        }
    }

    public final void a(String str, int i) {
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ void b(ir irVar) {
        this.g.b().b(irVar);
    }

    public final void c_() {
    }
}
