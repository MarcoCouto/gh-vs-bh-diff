package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.RemoteException;

public abstract class apo extends ajl implements apn {
    public apo() {
        super("com.google.android.gms.ads.internal.client.IAdLoader");
    }

    /* access modifiers changed from: protected */
    public final boolean dispatchTransaction(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        switch (i) {
            case 1:
                a((aop) ajm.a(parcel, aop.CREATOR));
                parcel2.writeNoException();
                break;
            case 2:
                String a2 = a();
                parcel2.writeNoException();
                parcel2.writeString(a2);
                break;
            case 3:
                boolean c = c();
                parcel2.writeNoException();
                ajm.a(parcel2, c);
                break;
            case 4:
                String b2 = b();
                parcel2.writeNoException();
                parcel2.writeString(b2);
                break;
            case 5:
                a((aop) ajm.a(parcel, aop.CREATOR), parcel.readInt());
                parcel2.writeNoException();
                break;
            default:
                return false;
        }
        return true;
    }
}
