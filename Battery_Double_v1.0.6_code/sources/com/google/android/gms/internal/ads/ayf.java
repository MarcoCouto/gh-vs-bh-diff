package com.google.android.gms.internal.ads;

import android.os.RemoteException;

final class ayf implements ays {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ asz f3002a;

    ayf(aye aye, asz asz) {
        this.f3002a = asz;
    }

    public final void a(ayt ayt) throws RemoteException {
        if (ayt.d != null) {
            ayt.d.a(this.f3002a);
        }
    }
}
