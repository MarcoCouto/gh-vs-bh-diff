package com.google.android.gms.internal.ads;

import java.lang.reflect.InvocationTargetException;

public final class aiz extends ajj {
    private final boolean d;

    public aiz(ahy ahy, String str, String str2, zz zzVar, int i, int i2) {
        super(ahy, str, str2, zzVar, i, 61);
        this.d = ahy.j();
    }

    /* access modifiers changed from: protected */
    public final void a() throws IllegalAccessException, InvocationTargetException {
        long longValue = ((Long) this.c.invoke(null, new Object[]{this.f2636a.a(), Boolean.valueOf(this.d)})).longValue();
        synchronized (this.f2637b) {
            this.f2637b.P = Long.valueOf(longValue);
        }
    }
}
