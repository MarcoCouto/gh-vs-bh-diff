package com.google.android.gms.internal.ads;

import java.io.IOException;

public final class acb extends afh<acb> {

    /* renamed from: a reason: collision with root package name */
    public Long f2446a;

    /* renamed from: b reason: collision with root package name */
    public Long f2447b;
    public Long c;
    private Long d;
    private Long e;

    public acb() {
        this.d = null;
        this.e = null;
        this.f2446a = null;
        this.f2447b = null;
        this.c = null;
        this.Z = -1;
    }

    /* access modifiers changed from: protected */
    public final int a() {
        int a2 = super.a();
        if (this.d != null) {
            a2 += aff.d(1, this.d.longValue());
        }
        if (this.e != null) {
            a2 += aff.d(2, this.e.longValue());
        }
        if (this.f2446a != null) {
            a2 += aff.d(3, this.f2446a.longValue());
        }
        if (this.f2447b != null) {
            a2 += aff.d(4, this.f2447b.longValue());
        }
        return this.c != null ? a2 + aff.d(5, this.c.longValue()) : a2;
    }

    public final /* synthetic */ afn a(afd afd) throws IOException {
        while (true) {
            int a2 = afd.a();
            switch (a2) {
                case 0:
                    break;
                case 8:
                    this.d = Long.valueOf(afd.h());
                    continue;
                case 16:
                    this.e = Long.valueOf(afd.h());
                    continue;
                case 24:
                    this.f2446a = Long.valueOf(afd.h());
                    continue;
                case 32:
                    this.f2447b = Long.valueOf(afd.h());
                    continue;
                case 40:
                    this.c = Long.valueOf(afd.h());
                    continue;
                default:
                    if (!super.a(afd, a2)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    public final void a(aff aff) throws IOException {
        if (this.d != null) {
            aff.b(1, this.d.longValue());
        }
        if (this.e != null) {
            aff.b(2, this.e.longValue());
        }
        if (this.f2446a != null) {
            aff.b(3, this.f2446a.longValue());
        }
        if (this.f2447b != null) {
            aff.b(4, this.f2447b.longValue());
        }
        if (this.c != null) {
            aff.b(5, this.c.longValue());
        }
        super.a(aff);
    }
}
