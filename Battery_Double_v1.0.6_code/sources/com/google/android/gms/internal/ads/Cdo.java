package com.google.android.gms.internal.ads;

import java.lang.ref.WeakReference;

@cm
/* renamed from: com.google.android.gms.internal.ads.do reason: invalid class name */
public final class Cdo extends dx {

    /* renamed from: a reason: collision with root package name */
    private final WeakReference<dc> f3264a;

    public Cdo(dc dcVar) {
        this.f3264a = new WeakReference<>(dcVar);
    }

    public final void a(dp dpVar) {
        dc dcVar = (dc) this.f3264a.get();
        if (dcVar != null) {
            dcVar.a(dpVar);
        }
    }
}
