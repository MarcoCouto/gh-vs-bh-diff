package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.a.a;
import com.google.android.gms.common.internal.a.c;

@cm
public final class arn extends a {
    public static final Creator<arn> CREATOR = new aro();

    /* renamed from: a reason: collision with root package name */
    public final String f2858a;

    public arn(com.google.android.gms.ads.d.a aVar) {
        this.f2858a = aVar.a();
    }

    arn(String str) {
        this.f2858a = str;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = c.a(parcel);
        c.a(parcel, 15, this.f2858a, false);
        c.a(parcel, a2);
    }
}
