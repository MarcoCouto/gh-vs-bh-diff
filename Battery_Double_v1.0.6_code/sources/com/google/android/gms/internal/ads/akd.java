package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.gmsg.ae;
import org.json.JSONObject;

@cm
public final class akd implements akq {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public final aju f2659a;

    /* renamed from: b reason: collision with root package name */
    private final qn f2660b;
    private final ae<qn> c = new ake(this);
    private final ae<qn> d = new akf(this);
    private final ae<qn> e = new akg(this);

    public akd(aju aju, qn qnVar) {
        this.f2659a = aju;
        this.f2660b = qnVar;
        qn qnVar2 = this.f2660b;
        qnVar2.a("/updateActiveView", this.c);
        qnVar2.a("/untrackActiveViewUnit", this.d);
        qnVar2.a("/visibilityChanged", this.e);
        String str = "Custom JS tracking ad unit: ";
        String valueOf = String.valueOf(this.f2659a.f2645a.d());
        jm.b(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
    }

    public final void a(JSONObject jSONObject, boolean z) {
        if (!z) {
            this.f2660b.b("AFMA_updateActiveView", jSONObject);
        } else {
            this.f2659a.b((akq) this);
        }
    }

    public final boolean a() {
        return true;
    }

    public final void b() {
        qn qnVar = this.f2660b;
        qnVar.b("/visibilityChanged", this.e);
        qnVar.b("/untrackActiveViewUnit", this.d);
        qnVar.b("/updateActiveView", this.c);
    }
}
