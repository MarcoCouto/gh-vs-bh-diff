package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.GradientDrawable;
import android.os.Build.VERSION;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.TextView;
import com.google.android.gms.ads.c.a.C0045a;
import com.google.android.gms.ads.internal.ax;
import com.google.android.gms.ads.internal.bu;
import com.google.android.gms.ads.internal.gmsg.ae;
import com.google.android.gms.ads.internal.overlay.c;
import com.google.android.gms.ads.internal.overlay.d;
import com.google.android.gms.common.util.o;
import java.util.Map;
import org.json.JSONObject;

@cm
public final class qz extends FrameLayout implements qn {

    /* renamed from: a reason: collision with root package name */
    private final qn f3651a;

    /* renamed from: b reason: collision with root package name */
    private final pd f3652b;

    public qz(qn qnVar) {
        super(qnVar.getContext());
        this.f3651a = qnVar;
        this.f3652b = new pd(qnVar.q(), this, this);
        addView(this.f3651a.getView());
    }

    public final boolean A() {
        return this.f3651a.A();
    }

    public final void B() {
        this.f3652b.c();
        this.f3651a.B();
    }

    public final boolean C() {
        return this.f3651a.C();
    }

    public final boolean D() {
        return this.f3651a.D();
    }

    public final boolean E() {
        return this.f3651a.E();
    }

    public final void F() {
        this.f3651a.F();
    }

    public final void G() {
        this.f3651a.G();
    }

    public final atw H() {
        return this.f3651a.H();
    }

    public final void I() {
        setBackgroundColor(0);
        this.f3651a.setBackgroundColor(0);
    }

    public final void J() {
        TextView textView = new TextView(getContext());
        Resources h = ax.i().h();
        textView.setText(h != null ? h.getString(C0045a.s7) : "Test Ad");
        textView.setTextSize(15.0f);
        textView.setTextColor(-1);
        textView.setPadding(5, 0, 5, 0);
        GradientDrawable gradientDrawable = new GradientDrawable();
        gradientDrawable.setShape(0);
        gradientDrawable.setColor(-12303292);
        gradientDrawable.setCornerRadius(8.0f);
        if (VERSION.SDK_INT >= 16) {
            textView.setBackground(gradientDrawable);
        } else {
            textView.setBackgroundDrawable(gradientDrawable);
        }
        addView(textView, new LayoutParams(-2, -2, 49));
        bringChildToFront(textView);
    }

    public final pd a() {
        return this.f3652b;
    }

    public final void a(int i) {
        this.f3651a.a(i);
    }

    public final void a(Context context) {
        this.f3651a.a(context);
    }

    public final void a(c cVar) {
        this.f3651a.a(cVar);
    }

    public final void a(d dVar) {
        this.f3651a.a(dVar);
    }

    public final void a(aku aku) {
        this.f3651a.a(aku);
    }

    public final void a(atw atw) {
        this.f3651a.a(atw);
    }

    public final void a(rd rdVar) {
        this.f3651a.a(rdVar);
    }

    public final void a(sb sbVar) {
        this.f3651a.a(sbVar);
    }

    public final void a(String str) {
        this.f3651a.a(str);
    }

    public final void a(String str, ae<? super qn> aeVar) {
        this.f3651a.a(str, aeVar);
    }

    public final void a(String str, o<ae<? super qn>> oVar) {
        this.f3651a.a(str, oVar);
    }

    public final void a(String str, String str2, String str3) {
        this.f3651a.a(str, str2, str3);
    }

    public final void a(String str, Map<String, ?> map) {
        this.f3651a.a(str, map);
    }

    public final void a(String str, JSONObject jSONObject) {
        this.f3651a.a(str, jSONObject);
    }

    public final void a(boolean z) {
        this.f3651a.a(z);
    }

    public final void a(boolean z, int i) {
        this.f3651a.a(z, i);
    }

    public final void a(boolean z, int i, String str) {
        this.f3651a.a(z, i, str);
    }

    public final void a(boolean z, int i, String str, String str2) {
        this.f3651a.a(z, i, str, str2);
    }

    public final rd b() {
        return this.f3651a.b();
    }

    public final void b(d dVar) {
        this.f3651a.b(dVar);
    }

    public final void b(String str) {
        this.f3651a.b(str);
    }

    public final void b(String str, ae<? super qn> aeVar) {
        this.f3651a.b(str, aeVar);
    }

    public final void b(String str, JSONObject jSONObject) {
        this.f3651a.b(str, jSONObject);
    }

    public final void b(boolean z) {
        this.f3651a.b(z);
    }

    public final ast c() {
        return this.f3651a.c();
    }

    public final void c(boolean z) {
        this.f3651a.c(z);
    }

    public final Activity d() {
        return this.f3651a.d();
    }

    public final void d(boolean z) {
        this.f3651a.d(z);
    }

    public final void destroy() {
        this.f3651a.destroy();
    }

    public final bu e() {
        return this.f3651a.e();
    }

    public final void e(boolean z) {
        this.f3651a.e(z);
    }

    public final void f() {
        this.f3651a.f();
    }

    public final String g() {
        return this.f3651a.g();
    }

    public final OnClickListener getOnClickListener() {
        return this.f3651a.getOnClickListener();
    }

    public final int getRequestedOrientation() {
        return this.f3651a.getRequestedOrientation();
    }

    public final View getView() {
        return this;
    }

    public final WebView getWebView() {
        return this.f3651a.getWebView();
    }

    public final void h_() {
        this.f3651a.h_();
    }

    public final void i_() {
        this.f3651a.i_();
    }

    public final asu j() {
        return this.f3651a.j();
    }

    public final mu k() {
        return this.f3651a.k();
    }

    public final int l() {
        return getMeasuredHeight();
    }

    public final void loadData(String str, String str2, String str3) {
        this.f3651a.loadData(str, str2, str3);
    }

    public final void loadDataWithBaseURL(String str, String str2, String str3, String str4, String str5) {
        this.f3651a.loadDataWithBaseURL(str, str2, str3, str4, str5);
    }

    public final void loadUrl(String str) {
        this.f3651a.loadUrl(str);
    }

    public final int m() {
        return getMeasuredWidth();
    }

    public final void n() {
        this.f3651a.n();
    }

    public final void o() {
        this.f3651a.o();
    }

    public final void onPause() {
        this.f3652b.b();
        this.f3651a.onPause();
    }

    public final void onResume() {
        this.f3651a.onResume();
    }

    public final void p() {
        this.f3651a.p();
    }

    public final Context q() {
        return this.f3651a.q();
    }

    public final d r() {
        return this.f3651a.r();
    }

    public final d s() {
        return this.f3651a.s();
    }

    public final void setOnClickListener(OnClickListener onClickListener) {
        this.f3651a.setOnClickListener(onClickListener);
    }

    public final void setOnTouchListener(OnTouchListener onTouchListener) {
        this.f3651a.setOnTouchListener(onTouchListener);
    }

    public final void setRequestedOrientation(int i) {
        this.f3651a.setRequestedOrientation(i);
    }

    public final void setWebChromeClient(WebChromeClient webChromeClient) {
        this.f3651a.setWebChromeClient(webChromeClient);
    }

    public final void setWebViewClient(WebViewClient webViewClient) {
        this.f3651a.setWebViewClient(webViewClient);
    }

    public final void stopLoading() {
        this.f3651a.stopLoading();
    }

    public final sb t() {
        return this.f3651a.t();
    }

    public final String u() {
        return this.f3651a.u();
    }

    public final rv v() {
        return this.f3651a.v();
    }

    public final WebViewClient w() {
        return this.f3651a.w();
    }

    public final boolean x() {
        return this.f3651a.x();
    }

    public final ahh y() {
        return this.f3651a.y();
    }

    public final boolean z() {
        return this.f3651a.z();
    }
}
