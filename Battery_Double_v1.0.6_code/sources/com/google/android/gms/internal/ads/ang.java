package com.google.android.gms.internal.ads;

import java.io.IOException;

public final class ang extends afh<ang> {

    /* renamed from: a reason: collision with root package name */
    public Integer f2750a;

    /* renamed from: b reason: collision with root package name */
    public anj f2751b;
    private Integer c;
    private ani d;
    private anh[] e;
    private ank f;
    private ant g;
    private ans h;
    private anp i;
    private anq j;
    private anz[] k;

    public ang() {
        this.f2750a = null;
        this.c = null;
        this.d = null;
        this.f2751b = null;
        this.e = anh.b();
        this.f = null;
        this.g = null;
        this.h = null;
        this.i = null;
        this.j = null;
        this.k = anz.b();
        this.Y = null;
        this.Z = -1;
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public final ang a(afd afd) throws IOException {
        while (true) {
            int a2 = afd.a();
            switch (a2) {
                case 0:
                    break;
                case 56:
                    int j2 = afd.j();
                    try {
                        int g2 = afd.g();
                        if (g2 < 0 || g2 > 9) {
                            throw new IllegalArgumentException(g2 + " is not a valid enum AdInitiater");
                        }
                        this.f2750a = Integer.valueOf(g2);
                        continue;
                    } catch (IllegalArgumentException e2) {
                        afd.e(j2);
                        a(afd, a2);
                        break;
                    }
                case 64:
                    int j3 = afd.j();
                    try {
                        this.c = Integer.valueOf(anf.a(afd.g()));
                        continue;
                    } catch (IllegalArgumentException e3) {
                        afd.e(j3);
                        a(afd, a2);
                        break;
                    }
                case 74:
                    if (this.d == null) {
                        this.d = new ani();
                    }
                    afd.a((afn) this.d);
                    continue;
                case 82:
                    if (this.f2751b == null) {
                        this.f2751b = new anj();
                    }
                    afd.a((afn) this.f2751b);
                    continue;
                case 90:
                    int a3 = afq.a(afd, 90);
                    int length = this.e == null ? 0 : this.e.length;
                    anh[] anhArr = new anh[(a3 + length)];
                    if (length != 0) {
                        System.arraycopy(this.e, 0, anhArr, 0, length);
                    }
                    while (length < anhArr.length - 1) {
                        anhArr[length] = new anh();
                        afd.a((afn) anhArr[length]);
                        afd.a();
                        length++;
                    }
                    anhArr[length] = new anh();
                    afd.a((afn) anhArr[length]);
                    this.e = anhArr;
                    continue;
                case 98:
                    if (this.f == null) {
                        this.f = new ank();
                    }
                    afd.a((afn) this.f);
                    continue;
                case 106:
                    if (this.g == null) {
                        this.g = new ant();
                    }
                    afd.a((afn) this.g);
                    continue;
                case 114:
                    if (this.h == null) {
                        this.h = new ans();
                    }
                    afd.a((afn) this.h);
                    continue;
                case 122:
                    if (this.i == null) {
                        this.i = new anp();
                    }
                    afd.a((afn) this.i);
                    continue;
                case 130:
                    if (this.j == null) {
                        this.j = new anq();
                    }
                    afd.a((afn) this.j);
                    continue;
                case 138:
                    int a4 = afq.a(afd, 138);
                    int length2 = this.k == null ? 0 : this.k.length;
                    anz[] anzArr = new anz[(a4 + length2)];
                    if (length2 != 0) {
                        System.arraycopy(this.k, 0, anzArr, 0, length2);
                    }
                    while (length2 < anzArr.length - 1) {
                        anzArr[length2] = new anz();
                        afd.a((afn) anzArr[length2]);
                        afd.a();
                        length2++;
                    }
                    anzArr[length2] = new anz();
                    afd.a((afn) anzArr[length2]);
                    this.k = anzArr;
                    continue;
                default:
                    if (!super.a(afd, a2)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    /* access modifiers changed from: protected */
    public final int a() {
        int a2 = super.a();
        if (this.f2750a != null) {
            a2 += aff.b(7, this.f2750a.intValue());
        }
        if (this.c != null) {
            a2 += aff.b(8, this.c.intValue());
        }
        if (this.d != null) {
            a2 += aff.b(9, (afn) this.d);
        }
        if (this.f2751b != null) {
            a2 += aff.b(10, (afn) this.f2751b);
        }
        if (this.e != null && this.e.length > 0) {
            int i2 = a2;
            for (anh anh : this.e) {
                if (anh != null) {
                    i2 += aff.b(11, (afn) anh);
                }
            }
            a2 = i2;
        }
        if (this.f != null) {
            a2 += aff.b(12, (afn) this.f);
        }
        if (this.g != null) {
            a2 += aff.b(13, (afn) this.g);
        }
        if (this.h != null) {
            a2 += aff.b(14, (afn) this.h);
        }
        if (this.i != null) {
            a2 += aff.b(15, (afn) this.i);
        }
        if (this.j != null) {
            a2 += aff.b(16, (afn) this.j);
        }
        if (this.k != null && this.k.length > 0) {
            for (anz anz : this.k) {
                if (anz != null) {
                    a2 += aff.b(17, (afn) anz);
                }
            }
        }
        return a2;
    }

    public final void a(aff aff) throws IOException {
        if (this.f2750a != null) {
            aff.a(7, this.f2750a.intValue());
        }
        if (this.c != null) {
            aff.a(8, this.c.intValue());
        }
        if (this.d != null) {
            aff.a(9, (afn) this.d);
        }
        if (this.f2751b != null) {
            aff.a(10, (afn) this.f2751b);
        }
        if (this.e != null && this.e.length > 0) {
            for (anh anh : this.e) {
                if (anh != null) {
                    aff.a(11, (afn) anh);
                }
            }
        }
        if (this.f != null) {
            aff.a(12, (afn) this.f);
        }
        if (this.g != null) {
            aff.a(13, (afn) this.g);
        }
        if (this.h != null) {
            aff.a(14, (afn) this.h);
        }
        if (this.i != null) {
            aff.a(15, (afn) this.i);
        }
        if (this.j != null) {
            aff.a(16, (afn) this.j);
        }
        if (this.k != null && this.k.length > 0) {
            for (anz anz : this.k) {
                if (anz != null) {
                    aff.a(17, (afn) anz);
                }
            }
        }
        super.a(aff);
    }
}
