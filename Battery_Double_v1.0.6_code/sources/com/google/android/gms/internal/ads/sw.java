package com.google.android.gms.internal.ads;

import android.net.Uri;
import android.net.http.SslError;
import android.os.Bundle;
import android.text.TextUtils;

@cm
final class sw {

    /* renamed from: a reason: collision with root package name */
    private static final String[] f3696a = {"UNKNOWN", "HOST_LOOKUP", "UNSUPPORTED_AUTH_SCHEME", "AUTHENTICATION", "PROXY_AUTHENTICATION", "CONNECT", "IO", "TIMEOUT", "REDIRECT_LOOP", "UNSUPPORTED_SCHEME", "FAILED_SSL_HANDSHAKE", "BAD_URL", "FILE", "FILE_NOT_FOUND", "TOO_MANY_REQUESTS"};

    /* renamed from: b reason: collision with root package name */
    private static final String[] f3697b = {"NOT_YET_VALID", "EXPIRED", "ID_MISMATCH", "UNTRUSTED", "DATE_INVALID", "INVALID"};

    sw() {
    }

    private static void a(String str, String str2, String str3) {
        String str4;
        if (((Boolean) ape.f().a(asi.bs)).booleanValue()) {
            Bundle bundle = new Bundle();
            bundle.putString("err", str);
            bundle.putString("code", str2);
            String str5 = "host";
            if (!TextUtils.isEmpty(str3)) {
                Uri parse = Uri.parse(str3);
                if (parse.getHost() != null) {
                    str4 = parse.getHost();
                    bundle.putString(str5, str4);
                }
            }
            str4 = "";
            bundle.putString(str5, str4);
        }
    }

    /* access modifiers changed from: 0000 */
    public final void a(int i, String str) {
        a("http_err", (i >= 0 || (-i) + -1 >= f3696a.length) ? String.valueOf(i) : f3696a[(-i) - 1], str);
    }

    /* access modifiers changed from: 0000 */
    public final void a(SslError sslError) {
        if (sslError != null) {
            int primaryError = sslError.getPrimaryError();
            a("ssl_err", (primaryError < 0 || primaryError >= f3697b.length) ? String.valueOf(primaryError) : f3697b[primaryError], sslError.getUrl());
        }
    }
}
