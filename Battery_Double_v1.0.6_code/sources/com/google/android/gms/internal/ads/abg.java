package com.google.android.gms.internal.ads;

final class abg {

    /* renamed from: a reason: collision with root package name */
    private static final abe<?> f2418a = new abf();

    /* renamed from: b reason: collision with root package name */
    private static final abe<?> f2419b = c();

    static abe<?> a() {
        return f2418a;
    }

    static abe<?> b() {
        if (f2419b != null) {
            return f2419b;
        }
        throw new IllegalStateException("Protobuf runtime is not correctly loaded.");
    }

    private static abe<?> c() {
        try {
            return (abe) Class.forName("com.google.protobuf.ExtensionSchemaFull").getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception e) {
            return null;
        }
    }
}
