package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.ax;
import com.google.android.gms.ads.internal.overlay.AdOverlayInfoParcel;
import com.google.android.gms.ads.internal.overlay.l;

final class bet implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ AdOverlayInfoParcel f3178a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ zzzv f3179b;

    bet(zzzv zzzv, AdOverlayInfoParcel adOverlayInfoParcel) {
        this.f3179b = zzzv;
        this.f3178a = adOverlayInfoParcel;
    }

    public final void run() {
        ax.c();
        l.a(this.f3179b.f3832a, this.f3178a, true);
    }
}
