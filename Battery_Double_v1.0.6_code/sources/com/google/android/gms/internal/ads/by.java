package com.google.android.gms.internal.ads;

import org.json.JSONObject;

final /* synthetic */ class by implements mx {

    /* renamed from: a reason: collision with root package name */
    private final bu f3213a;

    /* renamed from: b reason: collision with root package name */
    private final JSONObject f3214b;

    by(bu buVar, JSONObject jSONObject) {
        this.f3213a = buVar;
        this.f3214b = jSONObject;
    }

    public final nn a(Object obj) {
        return this.f3213a.b(this.f3214b, (qn) obj);
    }
}
