package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.abp.e;

final class abo implements acv {

    /* renamed from: a reason: collision with root package name */
    private static final abo f2432a = new abo();

    private abo() {
    }

    public static abo a() {
        return f2432a;
    }

    public final boolean a(Class<?> cls) {
        return abp.class.isAssignableFrom(cls);
    }

    public final acu b(Class<?> cls) {
        if (!abp.class.isAssignableFrom(cls)) {
            String str = "Unsupported message type: ";
            String valueOf = String.valueOf(cls.getName());
            throw new IllegalArgumentException(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
        }
        try {
            return (acu) abp.a(cls.asSubclass(abp.class)).a(e.c, (Object) null, (Object) null);
        } catch (Exception e) {
            Exception exc = e;
            String str2 = "Unable to get message info for ";
            String valueOf2 = String.valueOf(cls.getName());
            throw new RuntimeException(valueOf2.length() != 0 ? str2.concat(valueOf2) : new String(str2), exc);
        }
    }
}
