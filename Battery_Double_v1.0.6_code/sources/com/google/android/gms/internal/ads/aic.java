package com.google.android.gms.internal.ads;

final class aic implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ int f2615a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ boolean f2616b;
    private final /* synthetic */ ahy c;

    aic(ahy ahy, int i, boolean z) {
        this.c = ahy;
        this.f2615a = i;
        this.f2616b = z;
    }

    public final void run() {
        zz b2 = this.c.b(this.f2615a, this.f2616b);
        this.c.k = b2;
        if (ahy.b(this.f2615a, b2)) {
            this.c.a(this.f2615a + 1, this.f2616b);
        }
    }
}
