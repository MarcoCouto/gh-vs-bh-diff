package com.google.android.gms.internal.ads;

import android.content.Context;
import java.util.concurrent.Callable;

final /* synthetic */ class ii implements Callable {

    /* renamed from: a reason: collision with root package name */
    private final ih f3388a;

    /* renamed from: b reason: collision with root package name */
    private final Context f3389b;

    ii(ih ihVar, Context context) {
        this.f3388a = ihVar;
        this.f3389b = context;
    }

    public final Object call() {
        return this.f3388a.k(this.f3389b);
    }
}
