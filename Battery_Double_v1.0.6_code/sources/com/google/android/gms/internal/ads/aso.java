package com.google.android.gms.internal.ads;

@cm
public final class aso {
    public static ast a(asv asv) {
        if (asv == null) {
            return null;
        }
        return asv.a();
    }

    public static boolean a(asv asv, ast ast, String... strArr) {
        if (asv == null || ast == null) {
            return false;
        }
        return asv.a(ast, strArr);
    }
}
