package com.google.android.gms.internal.ads;

import android.content.Context;
import android.net.Uri.Builder;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Looper;
import android.text.TextUtils;
import com.google.android.gms.common.b.c;
import com.hmatalonga.greenhub.models.Network;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@cm
public final class cg implements ck {

    /* renamed from: a reason: collision with root package name */
    private static final Object f3226a = new Object();

    /* renamed from: b reason: collision with root package name */
    private static ck f3227b = null;
    private static ck c = null;
    private final Object d;
    private final Context e;
    private final WeakHashMap<Thread, Boolean> f;
    private final ExecutorService g;
    private final mu h;

    private cg(Context context) {
        this(context, mu.a());
    }

    private cg(Context context, mu muVar) {
        this.d = new Object();
        this.f = new WeakHashMap<>();
        this.g = Executors.newCachedThreadPool();
        if (context.getApplicationContext() != null) {
            context = context.getApplicationContext();
        }
        this.e = context;
        this.h = muVar;
    }

    private final Builder a(String str, String str2, String str3, int i) {
        boolean z = false;
        try {
            z = c.b(this.e).a();
        } catch (Throwable th) {
            ms.b("Error fetching instant app info", th);
        }
        String str4 = Network.TYPE_UNKNOWN;
        try {
            str4 = this.e.getPackageName();
        } catch (Throwable th2) {
            ms.e("Cannot obtain package name, proceeding.");
        }
        Builder appendQueryParameter = new Builder().scheme("https").path("//pagead2.googlesyndication.com/pagead/gen_204").appendQueryParameter("is_aia", Boolean.toString(z)).appendQueryParameter("id", "gmob-apps-report-exception").appendQueryParameter("os", VERSION.RELEASE).appendQueryParameter("api", String.valueOf(VERSION.SDK_INT));
        String str5 = "device";
        String str6 = Build.MANUFACTURER;
        String str7 = Build.MODEL;
        if (!str7.startsWith(str6)) {
            str7 = new StringBuilder(String.valueOf(str6).length() + 1 + String.valueOf(str7).length()).append(str6).append(" ").append(str7).toString();
        }
        return appendQueryParameter.appendQueryParameter(str5, str7).appendQueryParameter("js", this.h.f3528a).appendQueryParameter("appid", str4).appendQueryParameter("exceptiontype", str).appendQueryParameter("stacktrace", str2).appendQueryParameter("eids", TextUtils.join(",", asi.a())).appendQueryParameter("exceptionkey", str3).appendQueryParameter("cl", "193400285").appendQueryParameter("rc", "dev").appendQueryParameter("session_id", ape.c()).appendQueryParameter("sampling_rate", Integer.toString(i)).appendQueryParameter("pb_tm", String.valueOf(ape.f().a(asi.dj)));
    }

    public static ck a(Context context) {
        synchronized (f3226a) {
            if (f3227b == null) {
                if (((Boolean) ape.f().a(asi.f2876b)).booleanValue()) {
                    f3227b = new cg(context);
                } else {
                    f3227b = new cl();
                }
            }
        }
        return f3227b;
    }

    public static ck a(Context context, mu muVar) {
        synchronized (f3226a) {
            if (c == null) {
                if (((Boolean) ape.f().a(asi.f2876b)).booleanValue()) {
                    cg cgVar = new cg(context, muVar);
                    Thread thread = Looper.getMainLooper().getThread();
                    if (thread != null) {
                        synchronized (cgVar.d) {
                            cgVar.f.put(thread, Boolean.valueOf(true));
                        }
                        thread.setUncaughtExceptionHandler(new ci(cgVar, thread.getUncaughtExceptionHandler()));
                    }
                    Thread.setDefaultUncaughtExceptionHandler(new ch(cgVar, Thread.getDefaultUncaughtExceptionHandler()));
                    c = cgVar;
                } else {
                    c = new cl();
                }
            }
        }
        return c;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x003c, code lost:
        if (r2 == false) goto L_0x003e;
     */
    public final void a(Thread thread, Throwable th) {
        StackTraceElement[] stackTrace;
        boolean z = true;
        if (th != null) {
            boolean z2 = false;
            boolean z3 = false;
            for (Throwable th2 = th; th2 != null; th2 = th2.getCause()) {
                for (StackTraceElement stackTraceElement : th2.getStackTrace()) {
                    if (mh.b(stackTraceElement.getClassName())) {
                        z3 = true;
                    }
                    if (getClass().getName().equals(stackTraceElement.getClassName())) {
                        z2 = true;
                    }
                }
            }
            if (z3) {
            }
        }
        z = false;
        if (z) {
            a(th, "", 1.0f);
        }
    }

    public final void a(Throwable th, String str) {
        a(th, str, 1.0f);
    }

    public final void a(Throwable th, String str, float f2) {
        if (mh.a(th) != null) {
            String name = th.getClass().getName();
            StringWriter stringWriter = new StringWriter();
            zp.a(th, new PrintWriter(stringWriter));
            String stringWriter2 = stringWriter.toString();
            boolean z = Math.random() < ((double) f2);
            int i = f2 > 0.0f ? (int) (1.0f / f2) : 1;
            if (z) {
                ArrayList arrayList = new ArrayList();
                arrayList.add(a(name, stringWriter2, str, i).toString());
                ArrayList arrayList2 = arrayList;
                int size = arrayList2.size();
                int i2 = 0;
                while (i2 < size) {
                    Object obj = arrayList2.get(i2);
                    i2++;
                    this.g.submit(new cj(this, new mt(), (String) obj));
                }
            }
        }
    }
}
