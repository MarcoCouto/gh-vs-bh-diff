package com.google.android.gms.internal.ads;

final /* synthetic */ class vf {

    /* renamed from: a reason: collision with root package name */
    static final /* synthetic */ int[] f3739a = new int[ww.values().length];

    static {
        try {
            f3739a[ww.SHA1.ordinal()] = 1;
        } catch (NoSuchFieldError e) {
        }
        try {
            f3739a[ww.SHA256.ordinal()] = 2;
        } catch (NoSuchFieldError e2) {
        }
        try {
            f3739a[ww.SHA512.ordinal()] = 3;
        } catch (NoSuchFieldError e3) {
        }
    }
}
