package com.google.android.gms.internal.ads;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;

final /* synthetic */ class ks implements OnClickListener {

    /* renamed from: a reason: collision with root package name */
    private final kp f3458a;

    /* renamed from: b reason: collision with root package name */
    private final String f3459b;

    ks(kp kpVar, String str) {
        this.f3458a = kpVar;
        this.f3459b = str;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f3458a.a(this.f3459b, dialogInterface, i);
    }
}
