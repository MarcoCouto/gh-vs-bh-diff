package com.google.android.gms.internal.ads;

import android.os.RemoteException;
import com.google.android.gms.ads.internal.ax;

final class ew implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ dl f3297a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ dw f3298b;
    private final /* synthetic */ eu c;

    ew(eu euVar, dl dlVar, dw dwVar) {
        this.c = euVar;
        this.f3297a = dlVar;
        this.f3298b = dwVar;
    }

    public final void run() {
        dp dpVar;
        try {
            dpVar = this.c.a(this.f3297a);
        } catch (Exception e) {
            ax.i().a((Throwable) e, "AdRequestServiceImpl.loadAdAsync");
            jm.c("Could not fetch ad response due to an Exception.", e);
            dpVar = null;
        }
        if (dpVar == null) {
            dpVar = new dp(0);
        }
        try {
            this.f3298b.a(dpVar);
        } catch (RemoteException e2) {
            jm.c("Fail to forward ad response.", e2);
        }
    }
}
