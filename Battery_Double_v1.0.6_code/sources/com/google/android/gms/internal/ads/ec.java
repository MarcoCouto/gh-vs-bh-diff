package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.ax;
import com.google.android.gms.common.util.l;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;

final class ec implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ OutputStream f3272a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ byte[] f3273b;

    ec(eb ebVar, OutputStream outputStream, byte[] bArr) {
        this.f3272a = outputStream;
        this.f3273b = bArr;
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0029  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x002f  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0037  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x003d  */
    public final void run() {
        DataOutputStream dataOutputStream;
        try {
            dataOutputStream = new DataOutputStream(this.f3272a);
            try {
                dataOutputStream.writeInt(this.f3273b.length);
                dataOutputStream.write(this.f3273b);
                l.a(dataOutputStream);
            } catch (IOException e) {
                e = e;
                try {
                    jm.b("Error transporting the ad response", e);
                    ax.i().a((Throwable) e, "LargeParcelTeleporter.pipeData.1");
                    if (dataOutputStream != null) {
                        l.a(this.f3272a);
                    } else {
                        l.a(dataOutputStream);
                    }
                } catch (Throwable th) {
                    th = th;
                    if (dataOutputStream != null) {
                        l.a(this.f3272a);
                    } else {
                        l.a(dataOutputStream);
                    }
                    throw th;
                }
            }
        } catch (IOException e2) {
            e = e2;
            dataOutputStream = null;
            jm.b("Error transporting the ad response", e);
            ax.i().a((Throwable) e, "LargeParcelTeleporter.pipeData.1");
            if (dataOutputStream != null) {
            }
        } catch (Throwable th2) {
            th = th2;
            dataOutputStream = null;
            if (dataOutputStream != null) {
            }
            throw th;
        }
    }
}
