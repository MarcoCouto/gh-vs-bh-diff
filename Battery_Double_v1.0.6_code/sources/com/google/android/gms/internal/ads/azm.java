package com.google.android.gms.internal.ads;

final /* synthetic */ class azm implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final azg f3040a;

    /* renamed from: b reason: collision with root package name */
    private final String f3041b;

    azm(azg azg, String str) {
        this.f3040a = azg;
        this.f3041b = str;
    }

    public final void run() {
        this.f3040a.e(this.f3041b);
    }
}
