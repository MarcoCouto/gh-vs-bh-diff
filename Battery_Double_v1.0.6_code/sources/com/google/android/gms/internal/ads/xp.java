package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.abp.e;

public final class xp extends abp<xp, a> implements acy {
    private static volatile adi<xp> zzakh;
    /* access modifiers changed from: private */
    public static final xp zzdlz = new xp();
    private int zzdlq;
    private int zzdlr;
    private abu<b> zzdly = m();

    public static final class a extends com.google.android.gms.internal.ads.abp.a<xp, a> implements acy {
        private a() {
            super(xp.zzdlz);
        }

        /* synthetic */ a(xq xqVar) {
            this();
        }

        public final a a(int i) {
            b();
            ((xp) this.f2433a).b(i);
            return this;
        }

        public final a a(b bVar) {
            b();
            ((xp) this.f2433a).a(bVar);
            return this;
        }
    }

    public static final class b extends abp<b, a> implements acy {
        private static volatile adi<b> zzakh;
        /* access modifiers changed from: private */
        public static final b zzdma = new b();
        private String zzdks = "";
        private int zzdlj;
        private int zzdlv;
        private int zzdlw;

        public static final class a extends com.google.android.gms.internal.ads.abp.a<b, a> implements acy {
            private a() {
                super(b.zzdma);
            }

            /* synthetic */ a(xq xqVar) {
                this();
            }

            public final a a(int i) {
                b();
                ((b) this.f2433a).b(i);
                return this;
            }

            public final a a(xh xhVar) {
                b();
                ((b) this.f2433a).a(xhVar);
                return this;
            }

            public final a a(ya yaVar) {
                b();
                ((b) this.f2433a).a(yaVar);
                return this;
            }

            public final a a(String str) {
                b();
                ((b) this.f2433a).a(str);
                return this;
            }
        }

        static {
            abp.a(b.class, zzdma);
        }

        private b() {
        }

        public static a a() {
            return (a) ((com.google.android.gms.internal.ads.abp.a) zzdma.a(e.e, (Object) null, (Object) null));
        }

        /* access modifiers changed from: private */
        public final void a(xh xhVar) {
            if (xhVar == null) {
                throw new NullPointerException();
            }
            this.zzdlv = xhVar.a();
        }

        /* access modifiers changed from: private */
        public final void a(ya yaVar) {
            if (yaVar == null) {
                throw new NullPointerException();
            }
            this.zzdlj = yaVar.a();
        }

        /* access modifiers changed from: private */
        public final void a(String str) {
            if (str == null) {
                throw new NullPointerException();
            }
            this.zzdks = str;
        }

        /* access modifiers changed from: private */
        public final void b(int i) {
            this.zzdlw = i;
        }

        /* JADX WARNING: type inference failed for: r0v6, types: [com.google.android.gms.internal.ads.adi<com.google.android.gms.internal.ads.xp$b>] */
        /* JADX WARNING: type inference failed for: r0v7, types: [java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v8, types: [com.google.android.gms.internal.ads.adi<com.google.android.gms.internal.ads.xp$b>, com.google.android.gms.internal.ads.abp$b] */
        /* JADX WARNING: type inference failed for: r0v16 */
        /* access modifiers changed from: protected */
        /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r0v8, types: [com.google.android.gms.internal.ads.adi<com.google.android.gms.internal.ads.xp$b>, com.google.android.gms.internal.ads.abp$b]
  assigns: [com.google.android.gms.internal.ads.abp$b]
  uses: [com.google.android.gms.internal.ads.adi<com.google.android.gms.internal.ads.xp$b>]
  mth insns count: 44
        	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
        	at java.util.ArrayList.forEach(Unknown Source)
        	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
        	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
        	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
        	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
        	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
        	at java.util.ArrayList.forEach(Unknown Source)
        	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
        	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$0(DepthTraversal.java:13)
        	at java.util.ArrayList.forEach(Unknown Source)
        	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:13)
        	at jadx.core.ProcessClass.process(ProcessClass.java:30)
        	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
        	at java.util.ArrayList.forEach(Unknown Source)
        	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
        	at jadx.core.ProcessClass.process(ProcessClass.java:35)
        	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
        	at jadx.api.JavaClass.decompile(JavaClass.java:62)
        	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
         */
        /* JADX WARNING: Unknown variable types count: 2 */
        public final Object a(int i, Object obj, Object obj2) {
            ? r0;
            switch (xq.f3778a[i - 1]) {
                case 1:
                    return new b();
                case 2:
                    return new a(null);
                case 3:
                    Object[] objArr = {"zzdks", "zzdlv", "zzdlw", "zzdlj"};
                    return a((acw) zzdma, "\u0000\u0004\u0000\u0000\u0001\u0004\u0004\u0005\u0000\u0000\u0000\u0001Ȉ\u0002\f\u0003\u000b\u0004\f", objArr);
                case 4:
                    return zzdma;
                case 5:
                    adi<b> adi = zzakh;
                    if (adi != null) {
                        return adi;
                    }
                    synchronized (b.class) {
                        r0 = zzakh;
                        if (r0 == 0) {
                            ? bVar = new com.google.android.gms.internal.ads.abp.b(zzdma);
                            zzakh = bVar;
                            r0 = bVar;
                        }
                    }
                    return r0;
                case 6:
                    return Byte.valueOf(1);
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }
    }

    static {
        abp.a(xp.class, zzdlz);
    }

    private xp() {
    }

    public static a a() {
        return (a) ((com.google.android.gms.internal.ads.abp.a) zzdlz.a(e.e, (Object) null, (Object) null));
    }

    /* access modifiers changed from: private */
    public final void a(b bVar) {
        if (bVar == null) {
            throw new NullPointerException();
        }
        if (!this.zzdly.a()) {
            abu<b> abu = this.zzdly;
            int size = abu.size();
            this.zzdly = abu.a(size == 0 ? 10 : size << 1);
        }
        this.zzdly.add(bVar);
    }

    /* access modifiers changed from: private */
    public final void b(int i) {
        this.zzdlr = i;
    }

    /* JADX WARNING: type inference failed for: r0v6, types: [com.google.android.gms.internal.ads.adi<com.google.android.gms.internal.ads.xp>] */
    /* JADX WARNING: type inference failed for: r0v7, types: [java.lang.Object] */
    /* JADX WARNING: type inference failed for: r0v8, types: [com.google.android.gms.internal.ads.adi<com.google.android.gms.internal.ads.xp>, com.google.android.gms.internal.ads.abp$b] */
    /* JADX WARNING: type inference failed for: r0v16 */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r0v8, types: [com.google.android.gms.internal.ads.adi<com.google.android.gms.internal.ads.xp>, com.google.android.gms.internal.ads.abp$b]
  assigns: [com.google.android.gms.internal.ads.abp$b]
  uses: [com.google.android.gms.internal.ads.adi<com.google.android.gms.internal.ads.xp>]
  mth insns count: 44
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
    	at jadx.core.ProcessClass.process(ProcessClass.java:35)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Unknown variable types count: 2 */
    public final Object a(int i, Object obj, Object obj2) {
        ? r0;
        switch (xq.f3778a[i - 1]) {
            case 1:
                return new xp();
            case 2:
                return new a(null);
            case 3:
                Object[] objArr = {"zzdlq", "zzdlr", "zzdly", b.class};
                return a((acw) zzdlz, "\u0000\u0002\u0000\u0001\u0001\u0002\u0002\u0003\u0000\u0001\u0000\u0001\u000b\u0002\u001b", objArr);
            case 4:
                return zzdlz;
            case 5:
                adi<xp> adi = zzakh;
                if (adi != null) {
                    return adi;
                }
                synchronized (xp.class) {
                    r0 = zzakh;
                    if (r0 == 0) {
                        ? bVar = new com.google.android.gms.internal.ads.abp.b(zzdlz);
                        zzakh = bVar;
                        r0 = bVar;
                    }
                }
                return r0;
            case 6:
                return Byte.valueOf(1);
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
