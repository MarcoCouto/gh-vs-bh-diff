package com.google.android.gms.internal.ads;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.ConditionVariable;
import com.google.android.gms.common.n;

@cm
public final class asg {

    /* renamed from: a reason: collision with root package name */
    private final Object f2871a = new Object();

    /* renamed from: b reason: collision with root package name */
    private final ConditionVariable f2872b = new ConditionVariable();
    private volatile boolean c = false;
    /* access modifiers changed from: private */
    public SharedPreferences d = null;
    private Context e;

    public final <T> T a(ary<T> ary) {
        if (!this.f2872b.block(5000)) {
            throw new IllegalStateException("Flags.initialize() was not called!");
        }
        if (!this.c || this.d == null) {
            synchronized (this.f2871a) {
                if (!this.c || this.d == null) {
                    T b2 = ary.b();
                    return b2;
                }
            }
        }
        return ly.a(this.e, new ash(this, ary));
    }

    public final void a(Context context) {
        if (!this.c) {
            synchronized (this.f2871a) {
                if (!this.c) {
                    this.e = context.getApplicationContext() == null ? context : context.getApplicationContext();
                    try {
                        Context remoteContext = n.getRemoteContext(context);
                        if (remoteContext != null || context == null) {
                            context = remoteContext;
                        } else {
                            Context applicationContext = context.getApplicationContext();
                            if (applicationContext != null) {
                                context = applicationContext;
                            }
                        }
                        if (context != null) {
                            ape.d();
                            this.d = context.getSharedPreferences("google_ads_flags", 0);
                            this.c = true;
                            this.f2872b.open();
                        }
                    } finally {
                        this.f2872b.open();
                    }
                }
            }
        }
    }
}
