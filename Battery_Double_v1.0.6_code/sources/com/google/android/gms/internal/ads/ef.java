package com.google.android.gms.internal.ads;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.a.b;
import java.util.List;

public final class ef implements Creator<ee> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        String str = null;
        int b2 = b.b(parcel);
        boolean z = false;
        String str2 = null;
        PackageInfo packageInfo = null;
        List list = null;
        String str3 = null;
        ApplicationInfo applicationInfo = null;
        mu muVar = null;
        Bundle bundle = null;
        while (parcel.dataPosition() < b2) {
            int a2 = b.a(parcel);
            switch (b.a(a2)) {
                case 1:
                    bundle = b.m(parcel, a2);
                    break;
                case 2:
                    muVar = (mu) b.a(parcel, a2, mu.CREATOR);
                    break;
                case 3:
                    applicationInfo = (ApplicationInfo) b.a(parcel, a2, ApplicationInfo.CREATOR);
                    break;
                case 4:
                    str3 = b.k(parcel, a2);
                    break;
                case 5:
                    list = b.q(parcel, a2);
                    break;
                case 6:
                    packageInfo = (PackageInfo) b.a(parcel, a2, PackageInfo.CREATOR);
                    break;
                case 7:
                    str2 = b.k(parcel, a2);
                    break;
                case 8:
                    z = b.c(parcel, a2);
                    break;
                case 9:
                    str = b.k(parcel, a2);
                    break;
                default:
                    b.b(parcel, a2);
                    break;
            }
        }
        b.r(parcel, b2);
        return new ee(bundle, muVar, applicationInfo, str3, list, packageInfo, str2, z, str);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new ee[i];
    }
}
