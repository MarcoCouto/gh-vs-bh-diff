package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.b.a;

public interface gh extends IInterface {
    void a() throws RemoteException;

    void a(a aVar) throws RemoteException;

    void a(aqa aqa) throws RemoteException;

    void a(gf gfVar) throws RemoteException;

    void a(gn gnVar) throws RemoteException;

    void a(gt gtVar) throws RemoteException;

    void a(String str) throws RemoteException;

    void a(boolean z) throws RemoteException;

    Bundle b() throws RemoteException;

    void b(a aVar) throws RemoteException;

    void c(a aVar) throws RemoteException;

    boolean c() throws RemoteException;

    void d() throws RemoteException;

    void e() throws RemoteException;

    void f() throws RemoteException;

    String g() throws RemoteException;
}
