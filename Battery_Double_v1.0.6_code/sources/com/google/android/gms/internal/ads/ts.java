package com.google.android.gms.internal.ads;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public final class ts extends ByteArrayOutputStream {

    /* renamed from: a reason: collision with root package name */
    private final jk f3712a;

    public ts(jk jkVar, int i) {
        this.f3712a = jkVar;
        this.buf = this.f3712a.a(Math.max(i, 256));
    }

    private final void a(int i) {
        if (this.count + i > this.buf.length) {
            byte[] a2 = this.f3712a.a((this.count + i) << 1);
            System.arraycopy(this.buf, 0, a2, 0, this.count);
            this.f3712a.a(this.buf);
            this.buf = a2;
        }
    }

    public final void close() throws IOException {
        this.f3712a.a(this.buf);
        this.buf = null;
        super.close();
    }

    public final void finalize() {
        this.f3712a.a(this.buf);
    }

    public final synchronized void write(int i) {
        a(1);
        super.write(i);
    }

    public final synchronized void write(byte[] bArr, int i, int i2) {
        a(i2);
        super.write(bArr, i, i2);
    }
}
