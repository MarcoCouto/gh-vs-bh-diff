package com.google.android.gms.internal.ads;

import java.io.IOException;

public final class ans extends afh<ans> {

    /* renamed from: a reason: collision with root package name */
    private Integer f2774a;

    /* renamed from: b reason: collision with root package name */
    private anx f2775b;

    public ans() {
        this.f2774a = null;
        this.f2775b = null;
        this.Y = null;
        this.Z = -1;
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public final ans a(afd afd) throws IOException {
        while (true) {
            int a2 = afd.a();
            switch (a2) {
                case 0:
                    break;
                case 8:
                    int j = afd.j();
                    try {
                        this.f2774a = Integer.valueOf(anf.a(afd.g()));
                        continue;
                    } catch (IllegalArgumentException e) {
                        afd.e(j);
                        a(afd, a2);
                        break;
                    }
                case 18:
                    if (this.f2775b == null) {
                        this.f2775b = new anx();
                    }
                    afd.a((afn) this.f2775b);
                    continue;
                default:
                    if (!super.a(afd, a2)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    /* access modifiers changed from: protected */
    public final int a() {
        int a2 = super.a();
        if (this.f2774a != null) {
            a2 += aff.b(1, this.f2774a.intValue());
        }
        return this.f2775b != null ? a2 + aff.b(2, (afn) this.f2775b) : a2;
    }

    public final void a(aff aff) throws IOException {
        if (this.f2774a != null) {
            aff.a(1, this.f2774a.intValue());
        }
        if (this.f2775b != null) {
            aff.a(2, (afn) this.f2775b);
        }
        super.a(aff);
    }
}
