package com.google.android.gms.internal.ads;

import java.io.IOException;

public final class anz extends afh<anz> {

    /* renamed from: a reason: collision with root package name */
    private static volatile anz[] f2788a;

    /* renamed from: b reason: collision with root package name */
    private aod f2789b;
    private aog c;
    private aoh d;
    private aoi e;
    private aoa f;
    private aoe g;
    private aoc h;
    private Integer i;
    private Integer j;
    private anx k;
    private Integer l;
    private Integer m;
    private Integer n;
    private Integer o;
    private Integer p;
    private Long q;

    public anz() {
        this.f2789b = null;
        this.c = null;
        this.d = null;
        this.e = null;
        this.f = null;
        this.g = null;
        this.h = null;
        this.i = null;
        this.j = null;
        this.k = null;
        this.l = null;
        this.m = null;
        this.n = null;
        this.o = null;
        this.p = null;
        this.q = null;
        this.Y = null;
        this.Z = -1;
    }

    public static anz[] b() {
        if (f2788a == null) {
            synchronized (afl.f2531b) {
                if (f2788a == null) {
                    f2788a = new anz[0];
                }
            }
        }
        return f2788a;
    }

    /* access modifiers changed from: protected */
    public final int a() {
        int a2 = super.a();
        if (this.f2789b != null) {
            a2 += aff.b(5, (afn) this.f2789b);
        }
        if (this.c != null) {
            a2 += aff.b(6, (afn) this.c);
        }
        if (this.d != null) {
            a2 += aff.b(7, (afn) this.d);
        }
        if (this.e != null) {
            a2 += aff.b(8, (afn) this.e);
        }
        if (this.f != null) {
            a2 += aff.b(9, (afn) this.f);
        }
        if (this.g != null) {
            a2 += aff.b(10, (afn) this.g);
        }
        if (this.h != null) {
            a2 += aff.b(11, (afn) this.h);
        }
        if (this.i != null) {
            a2 += aff.b(12, this.i.intValue());
        }
        if (this.j != null) {
            a2 += aff.b(13, this.j.intValue());
        }
        if (this.k != null) {
            a2 += aff.b(14, (afn) this.k);
        }
        if (this.l != null) {
            a2 += aff.b(15, this.l.intValue());
        }
        if (this.m != null) {
            a2 += aff.b(16, this.m.intValue());
        }
        if (this.n != null) {
            a2 += aff.b(17, this.n.intValue());
        }
        if (this.o != null) {
            a2 += aff.b(18, this.o.intValue());
        }
        if (this.p != null) {
            a2 += aff.b(19, this.p.intValue());
        }
        return this.q != null ? a2 + aff.c(20, this.q.longValue()) : a2;
    }

    public final /* synthetic */ afn a(afd afd) throws IOException {
        while (true) {
            int a2 = afd.a();
            switch (a2) {
                case 0:
                    break;
                case 42:
                    if (this.f2789b == null) {
                        this.f2789b = new aod();
                    }
                    afd.a((afn) this.f2789b);
                    continue;
                case 50:
                    if (this.c == null) {
                        this.c = new aog();
                    }
                    afd.a((afn) this.c);
                    continue;
                case 58:
                    if (this.d == null) {
                        this.d = new aoh();
                    }
                    afd.a((afn) this.d);
                    continue;
                case 66:
                    if (this.e == null) {
                        this.e = new aoi();
                    }
                    afd.a((afn) this.e);
                    continue;
                case 74:
                    if (this.f == null) {
                        this.f = new aoa();
                    }
                    afd.a((afn) this.f);
                    continue;
                case 82:
                    if (this.g == null) {
                        this.g = new aoe();
                    }
                    afd.a((afn) this.g);
                    continue;
                case 90:
                    if (this.h == null) {
                        this.h = new aoc();
                    }
                    afd.a((afn) this.h);
                    continue;
                case 96:
                    this.i = Integer.valueOf(afd.g());
                    continue;
                case 104:
                    this.j = Integer.valueOf(afd.g());
                    continue;
                case 114:
                    if (this.k == null) {
                        this.k = new anx();
                    }
                    afd.a((afn) this.k);
                    continue;
                case 120:
                    this.l = Integer.valueOf(afd.g());
                    continue;
                case 128:
                    this.m = Integer.valueOf(afd.g());
                    continue;
                case 136:
                    this.n = Integer.valueOf(afd.g());
                    continue;
                case 144:
                    this.o = Integer.valueOf(afd.g());
                    continue;
                case 152:
                    this.p = Integer.valueOf(afd.g());
                    continue;
                case 160:
                    this.q = Long.valueOf(afd.h());
                    continue;
                default:
                    if (!super.a(afd, a2)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    public final void a(aff aff) throws IOException {
        if (this.f2789b != null) {
            aff.a(5, (afn) this.f2789b);
        }
        if (this.c != null) {
            aff.a(6, (afn) this.c);
        }
        if (this.d != null) {
            aff.a(7, (afn) this.d);
        }
        if (this.e != null) {
            aff.a(8, (afn) this.e);
        }
        if (this.f != null) {
            aff.a(9, (afn) this.f);
        }
        if (this.g != null) {
            aff.a(10, (afn) this.g);
        }
        if (this.h != null) {
            aff.a(11, (afn) this.h);
        }
        if (this.i != null) {
            aff.a(12, this.i.intValue());
        }
        if (this.j != null) {
            aff.a(13, this.j.intValue());
        }
        if (this.k != null) {
            aff.a(14, (afn) this.k);
        }
        if (this.l != null) {
            aff.a(15, this.l.intValue());
        }
        if (this.m != null) {
            aff.a(16, this.m.intValue());
        }
        if (this.n != null) {
            aff.a(17, this.n.intValue());
        }
        if (this.o != null) {
            aff.a(18, this.o.intValue());
        }
        if (this.p != null) {
            aff.a(19, this.p.intValue());
        }
        if (this.q != null) {
            aff.a(20, this.q.longValue());
        }
        super.a(aff);
    }
}
