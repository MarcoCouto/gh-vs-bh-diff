package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.abp.b;

public final class xc extends abp<xc, a> implements acy {
    private static volatile adi<xc> zzakh;
    /* access modifiers changed from: private */
    public static final xc zzdkr = new xc();
    private int zzdkp;
    private int zzdkq;

    public static final class a extends com.google.android.gms.internal.ads.abp.a<xc, a> implements acy {
        private a() {
            super(xc.zzdkr);
        }

        /* synthetic */ a(xd xdVar) {
            this();
        }
    }

    static {
        abp.a(xc.class, zzdkr);
    }

    private xc() {
    }

    public static xc c() {
        return zzdkr;
    }

    public final ww a() {
        ww a2 = ww.a(this.zzdkp);
        return a2 == null ? ww.UNRECOGNIZED : a2;
    }

    /* JADX WARNING: type inference failed for: r0v6, types: [com.google.android.gms.internal.ads.adi<com.google.android.gms.internal.ads.xc>] */
    /* JADX WARNING: type inference failed for: r0v7, types: [java.lang.Object] */
    /* JADX WARNING: type inference failed for: r0v8, types: [com.google.android.gms.internal.ads.abp$b, com.google.android.gms.internal.ads.adi<com.google.android.gms.internal.ads.xc>] */
    /* JADX WARNING: type inference failed for: r0v16 */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r0v8, types: [com.google.android.gms.internal.ads.abp$b, com.google.android.gms.internal.ads.adi<com.google.android.gms.internal.ads.xc>]
  assigns: [com.google.android.gms.internal.ads.abp$b]
  uses: [com.google.android.gms.internal.ads.adi<com.google.android.gms.internal.ads.xc>]
  mth insns count: 40
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
    	at jadx.core.ProcessClass.process(ProcessClass.java:35)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Unknown variable types count: 2 */
    public final Object a(int i, Object obj, Object obj2) {
        ? r0;
        switch (xd.f3769a[i - 1]) {
            case 1:
                return new xc();
            case 2:
                return new a(null);
            case 3:
                Object[] objArr = {"zzdkp", "zzdkq"};
                return a((acw) zzdkr, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0003\u0000\u0000\u0000\u0001\f\u0002\u000b", objArr);
            case 4:
                return zzdkr;
            case 5:
                adi<xc> adi = zzakh;
                if (adi != null) {
                    return adi;
                }
                synchronized (xc.class) {
                    r0 = zzakh;
                    if (r0 == 0) {
                        ? bVar = new b(zzdkr);
                        zzakh = bVar;
                        r0 = bVar;
                    }
                }
                return r0;
            case 6:
                return Byte.valueOf(1);
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    public final int b() {
        return this.zzdkq;
    }
}
