package com.google.android.gms.internal.ads;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public abstract class aqt extends ajl implements aqs {
    public aqt() {
        super("com.google.android.gms.ads.internal.client.IVideoController");
    }

    public static aqs a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.IVideoController");
        return queryLocalInterface instanceof aqs ? (aqs) queryLocalInterface : new aqu(iBinder);
    }

    /* access modifiers changed from: protected */
    public final boolean dispatchTransaction(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        aqv aqx;
        switch (i) {
            case 1:
                a();
                parcel2.writeNoException();
                break;
            case 2:
                b();
                parcel2.writeNoException();
                break;
            case 3:
                a(ajm.a(parcel));
                parcel2.writeNoException();
                break;
            case 4:
                boolean c = c();
                parcel2.writeNoException();
                ajm.a(parcel2, c);
                break;
            case 5:
                int d = d();
                parcel2.writeNoException();
                parcel2.writeInt(d);
                break;
            case 6:
                float f = f();
                parcel2.writeNoException();
                parcel2.writeFloat(f);
                break;
            case 7:
                float g = g();
                parcel2.writeNoException();
                parcel2.writeFloat(g);
                break;
            case 8:
                IBinder readStrongBinder = parcel.readStrongBinder();
                if (readStrongBinder == null) {
                    aqx = null;
                } else {
                    IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.IVideoLifecycleCallbacks");
                    aqx = queryLocalInterface instanceof aqv ? (aqv) queryLocalInterface : new aqx(readStrongBinder);
                }
                a(aqx);
                parcel2.writeNoException();
                break;
            case 9:
                float e = e();
                parcel2.writeNoException();
                parcel2.writeFloat(e);
                break;
            case 10:
                boolean i3 = i();
                parcel2.writeNoException();
                ajm.a(parcel2, i3);
                break;
            case 11:
                aqv h = h();
                parcel2.writeNoException();
                ajm.a(parcel2, (IInterface) h);
                break;
            case 12:
                boolean j = j();
                parcel2.writeNoException();
                ajm.a(parcel2, j);
                break;
            default:
                return false;
        }
        return true;
    }
}
