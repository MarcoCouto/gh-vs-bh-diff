package com.google.android.gms.internal.ads;

import java.io.IOException;

public class abv extends IOException {

    /* renamed from: a reason: collision with root package name */
    private acw f2442a = null;

    public abv(String str) {
        super(str);
    }

    static abv a() {
        return new abv("While parsing a protocol message, the input ended unexpectedly in the middle of a field.  This could mean either that the input has been truncated or that an embedded message misreported its own length.");
    }

    static abv b() {
        return new abv("CodedInputStream encountered an embedded string or message which claimed to have negative size.");
    }

    static abv c() {
        return new abv("CodedInputStream encountered a malformed varint.");
    }

    static abv d() {
        return new abv("Protocol message contained an invalid tag (zero).");
    }

    static abv e() {
        return new abv("Protocol message end-group tag did not match expected tag.");
    }

    static abw f() {
        return new abw("Protocol message tag had invalid wire type.");
    }

    static abv g() {
        return new abv("Failed to parse the message.");
    }

    static abv h() {
        return new abv("Protocol message had invalid UTF-8.");
    }

    public final abv a(acw acw) {
        this.f2442a = acw;
        return this;
    }
}
