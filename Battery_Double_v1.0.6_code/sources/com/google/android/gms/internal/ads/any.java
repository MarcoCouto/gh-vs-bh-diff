package com.google.android.gms.internal.ads;

import java.io.IOException;

public final class any extends afh<any> {

    /* renamed from: a reason: collision with root package name */
    public Integer f2786a;

    /* renamed from: b reason: collision with root package name */
    public Integer f2787b;
    public Integer c;

    public any() {
        this.f2786a = null;
        this.f2787b = null;
        this.c = null;
        this.Y = null;
        this.Z = -1;
    }

    /* access modifiers changed from: protected */
    public final int a() {
        int a2 = super.a();
        if (this.f2786a != null) {
            a2 += aff.b(1, this.f2786a.intValue());
        }
        if (this.f2787b != null) {
            a2 += aff.b(2, this.f2787b.intValue());
        }
        return this.c != null ? a2 + aff.b(3, this.c.intValue()) : a2;
    }

    public final /* synthetic */ afn a(afd afd) throws IOException {
        while (true) {
            int a2 = afd.a();
            switch (a2) {
                case 0:
                    break;
                case 8:
                    this.f2786a = Integer.valueOf(afd.g());
                    continue;
                case 16:
                    this.f2787b = Integer.valueOf(afd.g());
                    continue;
                case 24:
                    this.c = Integer.valueOf(afd.g());
                    continue;
                default:
                    if (!super.a(afd, a2)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    public final void a(aff aff) throws IOException {
        if (this.f2786a != null) {
            aff.a(1, this.f2786a.intValue());
        }
        if (this.f2787b != null) {
            aff.a(2, this.f2787b.intValue());
        }
        if (this.c != null) {
            aff.a(3, this.c.intValue());
        }
        super.a(aff);
    }
}
