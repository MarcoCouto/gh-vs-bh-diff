package com.google.android.gms.internal.ads;

import java.util.concurrent.Executor;

final /* synthetic */ class ni implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final ny f3541a;

    /* renamed from: b reason: collision with root package name */
    private final nn f3542b;
    private final Class c;
    private final mx d;
    private final Executor e;

    ni(ny nyVar, nn nnVar, Class cls, mx mxVar, Executor executor) {
        this.f3541a = nyVar;
        this.f3542b = nnVar;
        this.c = cls;
        this.d = mxVar;
        this.e = executor;
    }

    public final void run() {
        nc.a(this.f3541a, this.f3542b, this.c, this.d, this.e);
    }
}
