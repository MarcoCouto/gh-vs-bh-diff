package com.google.android.gms.internal.ads;

import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import java.util.Collections;
import java.util.Map;

public abstract class awb<T> implements Comparable<awb<T>> {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public final a f2963a;

    /* renamed from: b reason: collision with root package name */
    private final int f2964b;
    private final String c;
    private final int d;
    private final Object e;
    private bde f;
    private Integer g;
    private bab h;
    private boolean i;
    private boolean j;
    private boolean k;
    private boolean l;
    private ac m;
    private agy n;
    private axz o;

    public awb(int i2, String str, bde bde) {
        int i3;
        this.f2963a = a.f3278a ? new a() : null;
        this.e = new Object();
        this.i = true;
        this.j = false;
        this.k = false;
        this.l = false;
        this.n = null;
        this.f2964b = i2;
        this.c = str;
        this.f = bde;
        this.m = new amd();
        if (!TextUtils.isEmpty(str)) {
            Uri parse = Uri.parse(str);
            if (parse != null) {
                String host = parse.getHost();
                if (host != null) {
                    i3 = host.hashCode();
                    this.d = i3;
                }
            }
        }
        i3 = 0;
        this.d = i3;
    }

    public final awb<?> a(int i2) {
        this.g = Integer.valueOf(i2);
        return this;
    }

    public final awb<?> a(agy agy) {
        this.n = agy;
        return this;
    }

    public final awb<?> a(bab bab) {
        this.h = bab;
        return this;
    }

    /* access modifiers changed from: protected */
    public abstract bcd<T> a(atz atz);

    /* access modifiers changed from: 0000 */
    public final void a(axz axz) {
        synchronized (this.e) {
            this.o = axz;
        }
    }

    /* access modifiers changed from: 0000 */
    public final void a(bcd<?> bcd) {
        axz axz;
        synchronized (this.e) {
            axz = this.o;
        }
        if (axz != null) {
            axz.a(this, bcd);
        }
    }

    public final void a(df dfVar) {
        bde bde;
        synchronized (this.e) {
            bde = this.f;
        }
        if (bde != null) {
            bde.a(dfVar);
        }
    }

    /* access modifiers changed from: protected */
    public abstract void a(T t);

    public byte[] a() throws a {
        return null;
    }

    public Map<String, String> b() throws a {
        return Collections.emptyMap();
    }

    public final void b(String str) {
        if (a.f3278a) {
            this.f2963a.a(str, Thread.currentThread().getId());
        }
    }

    public final int c() {
        return this.f2964b;
    }

    /* access modifiers changed from: 0000 */
    public final void c(String str) {
        if (this.h != null) {
            this.h.b(this);
        }
        if (a.f3278a) {
            long id = Thread.currentThread().getId();
            if (Looper.myLooper() != Looper.getMainLooper()) {
                new Handler(Looper.getMainLooper()).post(new axb(this, str, id));
                return;
            }
            this.f2963a.a(str, id);
            this.f2963a.a(toString());
        }
    }

    public /* synthetic */ int compareTo(Object obj) {
        awb awb = (awb) obj;
        aza aza = aza.NORMAL;
        aza aza2 = aza.NORMAL;
        return aza == aza2 ? this.g.intValue() - awb.g.intValue() : aza2.ordinal() - aza.ordinal();
    }

    public final int d() {
        return this.d;
    }

    public final String e() {
        return this.c;
    }

    public final agy f() {
        return this.n;
    }

    public final boolean g() {
        synchronized (this.e) {
        }
        return false;
    }

    public final boolean h() {
        return this.i;
    }

    public final int i() {
        return this.m.a();
    }

    public final ac j() {
        return this.m;
    }

    public final void k() {
        synchronized (this.e) {
            this.k = true;
        }
    }

    public final boolean l() {
        boolean z;
        synchronized (this.e) {
            z = this.k;
        }
        return z;
    }

    /* access modifiers changed from: 0000 */
    public final void m() {
        axz axz;
        synchronized (this.e) {
            axz = this.o;
        }
        if (axz != null) {
            axz.a(this);
        }
    }

    public String toString() {
        String str = "0x";
        String valueOf = String.valueOf(Integer.toHexString(this.d));
        String str2 = valueOf.length() != 0 ? str.concat(valueOf) : new String(str);
        String str3 = "[ ] ";
        String str4 = this.c;
        String valueOf2 = String.valueOf(aza.NORMAL);
        String valueOf3 = String.valueOf(this.g);
        return new StringBuilder(String.valueOf(str3).length() + 3 + String.valueOf(str4).length() + String.valueOf(str2).length() + String.valueOf(valueOf2).length() + String.valueOf(valueOf3).length()).append(str3).append(str4).append(" ").append(str2).append(" ").append(valueOf2).append(" ").append(valueOf3).toString();
    }
}
