package com.google.android.gms.internal.ads;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.app.ActivityManager.RunningTaskInfo;
import android.app.AlertDialog.Builder;
import android.app.KeyguardManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Debug;
import android.os.Debug.MemoryInfo;
import android.os.Handler;
import android.os.Looper;
import android.os.PowerManager;
import android.os.Process;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.Window;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.PopupWindow;
import com.google.android.gms.ads.internal.ax;
import com.google.android.gms.common.b.c;
import com.google.android.gms.common.util.g;
import com.google.android.gms.common.util.l;
import com.google.android.gms.common.util.n;
import java.io.ByteArrayInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@cm
public final class jv {

    /* renamed from: a reason: collision with root package name */
    public static final Handler f3440a = new jn(Looper.getMainLooper());
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public final Object f3441b = new Object();
    /* access modifiers changed from: private */
    public boolean c = true;
    /* access modifiers changed from: private */
    public String d;
    private boolean e = false;
    private boolean f = false;
    private Pattern g;
    private Pattern h;

    public static Bitmap a(View view) {
        view.setDrawingCacheEnabled(true);
        Bitmap createBitmap = Bitmap.createBitmap(view.getDrawingCache());
        view.setDrawingCacheEnabled(false);
        return createBitmap;
    }

    public static Bundle a(aln aln) {
        String str;
        String c2;
        String e2;
        if (aln == null) {
            return null;
        }
        if (!((Boolean) ape.f().a(asi.W)).booleanValue()) {
            if (!((Boolean) ape.f().a(asi.Y)).booleanValue()) {
                return null;
            }
        }
        if (ax.i().l().b() && ax.i().l().d()) {
            return null;
        }
        if (aln.d()) {
            aln.c();
        }
        alh b2 = aln.b();
        if (b2 != null) {
            String b3 = b2.b();
            String c3 = b2.c();
            String d2 = b2.d();
            if (b3 != null) {
                ax.i().l().a(b3);
            }
            if (d2 != null) {
                ax.i().l().b(d2);
                c2 = b3;
                str = c3;
                e2 = d2;
            } else {
                c2 = b3;
                str = c3;
                e2 = d2;
            }
        } else {
            str = null;
            c2 = ax.i().l().c();
            e2 = ax.i().l().e();
        }
        Bundle bundle = new Bundle(1);
        if (e2 != null) {
            if (((Boolean) ape.f().a(asi.Y)).booleanValue() && !ax.i().l().d()) {
                bundle.putString("v_fp_vertical", e2);
            }
        }
        if (c2 != null) {
            if (((Boolean) ape.f().a(asi.W)).booleanValue() && !ax.i().l().b()) {
                bundle.putString("fingerprint", c2);
                if (!c2.equals(str)) {
                    bundle.putString("v_fp", str);
                }
            }
        }
        if (!bundle.isEmpty()) {
            return bundle;
        }
        return null;
    }

    public static DisplayMetrics a(WindowManager windowManager) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        windowManager.getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics;
    }

    public static WebResourceResponse a(HttpURLConnection httpURLConnection) throws IOException {
        String str;
        ax.e();
        String contentType = httpURLConnection.getContentType();
        String trim = TextUtils.isEmpty(contentType) ? "" : contentType.split(";")[0].trim();
        ax.e();
        String contentType2 = httpURLConnection.getContentType();
        if (!TextUtils.isEmpty(contentType2)) {
            String[] split = contentType2.split(";");
            if (split.length != 1) {
                int i = 1;
                while (true) {
                    if (i >= split.length) {
                        break;
                    }
                    if (split[i].trim().startsWith("charset")) {
                        String[] split2 = split[i].trim().split("=");
                        if (split2.length > 1) {
                            str = split2[1].trim();
                            break;
                        }
                    }
                    i++;
                }
            }
        }
        str = "";
        Map headerFields = httpURLConnection.getHeaderFields();
        HashMap hashMap = new HashMap(headerFields.size());
        for (Entry entry : headerFields.entrySet()) {
            if (!(entry.getKey() == null || entry.getValue() == null || ((List) entry.getValue()).size() <= 0)) {
                hashMap.put((String) entry.getKey(), (String) ((List) entry.getValue()).get(0));
            }
        }
        return ax.g().a(trim, str, httpURLConnection.getResponseCode(), httpURLConnection.getResponseMessage(), hashMap, httpURLConnection.getInputStream());
    }

    public static PopupWindow a(View view, int i, int i2, boolean z) {
        return new PopupWindow(view, i, i2, false);
    }

    public static String a() {
        return UUID.randomUUID().toString();
    }

    public static String a(Context context, View view, aot aot) {
        if (!((Boolean) ape.f().a(asi.ak)).booleanValue()) {
            return null;
        }
        try {
            JSONObject jSONObject = new JSONObject();
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("width", aot.e);
            jSONObject2.put("height", aot.f2815b);
            jSONObject.put("size", jSONObject2);
            jSONObject.put("activity", l(context));
            if (!aot.d) {
                JSONArray jSONArray = new JSONArray();
                while (view != null) {
                    ViewParent parent = view.getParent();
                    if (parent != null) {
                        int i = -1;
                        if (parent instanceof ViewGroup) {
                            i = ((ViewGroup) parent).indexOfChild(view);
                        }
                        JSONObject jSONObject3 = new JSONObject();
                        jSONObject3.put("type", parent.getClass().getName());
                        jSONObject3.put("index_of_child", i);
                        jSONArray.put(jSONObject3);
                    }
                    view = (parent == null || !(parent instanceof View)) ? null : (View) parent;
                }
                if (jSONArray.length() > 0) {
                    jSONObject.put("parents", jSONArray);
                }
            }
            return jSONObject.toString();
        } catch (JSONException e2) {
            jm.c("Fail to get view hierarchy json", e2);
            return null;
        }
    }

    public static String a(InputStreamReader inputStreamReader) throws IOException {
        StringBuilder sb = new StringBuilder(8192);
        char[] cArr = new char[2048];
        while (true) {
            int read = inputStreamReader.read(cArr);
            if (read == -1) {
                return sb.toString();
            }
            sb.append(cArr, 0, read);
        }
    }

    public static String a(String str) {
        return Uri.parse(str).buildUpon().query(null).build().toString();
    }

    public static Map<String, String> a(Uri uri) {
        if (uri == null) {
            return null;
        }
        HashMap hashMap = new HashMap();
        for (String str : ax.g().a(uri)) {
            hashMap.put(str, uri.getQueryParameter(str));
        }
        return hashMap;
    }

    private final JSONArray a(Collection<?> collection) throws JSONException {
        JSONArray jSONArray = new JSONArray();
        for (Object a2 : collection) {
            a(jSONArray, a2);
        }
        return jSONArray;
    }

    private final JSONObject a(Bundle bundle) throws JSONException {
        JSONObject jSONObject = new JSONObject();
        for (String str : bundle.keySet()) {
            a(jSONObject, str, bundle.get(str));
        }
        return jSONObject;
    }

    public static void a(Context context, Intent intent) {
        try {
            context.startActivity(intent);
        } catch (Throwable th) {
            intent.addFlags(268435456);
            context.startActivity(intent);
        }
    }

    @TargetApi(18)
    public static void a(Context context, Uri uri) {
        try {
            Intent intent = new Intent("android.intent.action.VIEW", uri);
            Bundle bundle = new Bundle();
            intent.putExtras(bundle);
            if (((Boolean) ape.f().a(asi.cL)).booleanValue()) {
                b(context, intent);
            }
            bundle.putString("com.android.browser.application_id", context.getPackageName());
            context.startActivity(intent);
            String uri2 = uri.toString();
            jm.b(new StringBuilder(String.valueOf(uri2).length() + 26).append("Opening ").append(uri2).append(" in a new browser.").toString());
        } catch (ActivityNotFoundException e2) {
            jm.b("No browser is found.", e2);
        }
    }

    public static void a(Context context, String str, String str2) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(str2);
        a(context, str, (List<String>) arrayList);
    }

    public static void a(Context context, String str, List<String> list) {
        for (String lvVar : list) {
            new lv(context, str, lvVar).c();
        }
    }

    public static void a(Context context, Throwable th) {
        boolean z;
        if (context != null) {
            try {
                z = ((Boolean) ape.f().a(asi.c)).booleanValue();
            } catch (IllegalStateException e2) {
                z = false;
            }
            if (z) {
                g.a(context, th);
            }
        }
    }

    public static void a(Runnable runnable) {
        if (Looper.getMainLooper().getThread() != Thread.currentThread()) {
            runnable.run();
        } else {
            jt.a(runnable);
        }
    }

    private final void a(JSONArray jSONArray, Object obj) throws JSONException {
        if (obj instanceof Bundle) {
            jSONArray.put(a((Bundle) obj));
        } else if (obj instanceof Map) {
            jSONArray.put(a((Map) obj));
        } else if (obj instanceof Collection) {
            jSONArray.put(a((Collection) obj));
        } else if (obj instanceof Object[]) {
            Object[] objArr = (Object[]) obj;
            JSONArray jSONArray2 = new JSONArray();
            for (Object a2 : objArr) {
                a(jSONArray2, a2);
            }
            jSONArray.put(jSONArray2);
        } else {
            jSONArray.put(obj);
        }
    }

    private final void a(JSONObject jSONObject, String str, Object obj) throws JSONException {
        if (obj instanceof Bundle) {
            jSONObject.put(str, a((Bundle) obj));
        } else if (obj instanceof Map) {
            jSONObject.put(str, a((Map) obj));
        } else if (obj instanceof Collection) {
            if (str == null) {
                str = "null";
            }
            jSONObject.put(str, a((Collection) obj));
        } else if (obj instanceof Object[]) {
            jSONObject.put(str, a((Collection<?>) Arrays.asList((Object[]) obj)));
        } else {
            jSONObject.put(str, obj);
        }
    }

    private static boolean a(int i, int i2, int i3) {
        return Math.abs(i - i2) <= i3;
    }

    @TargetApi(24)
    public static boolean a(Activity activity, Configuration configuration) {
        ape.a();
        int a2 = mh.a((Context) activity, configuration.screenHeightDp);
        int a3 = mh.a((Context) activity, configuration.screenWidthDp);
        DisplayMetrics a4 = a((WindowManager) activity.getApplicationContext().getSystemService("window"));
        int i = a4.heightPixels;
        int i2 = a4.widthPixels;
        int identifier = activity.getResources().getIdentifier("status_bar_height", "dimen", "android");
        int i3 = identifier > 0 ? activity.getResources().getDimensionPixelSize(identifier) : 0;
        int intValue = ((Integer) ape.f().a(asi.cX)).intValue() * ((int) Math.round(((double) activity.getResources().getDisplayMetrics().density) + 0.5d));
        return a(i, i3 + a2, intValue) && a(i2, a3, intValue);
    }

    public static boolean a(Context context) {
        boolean z;
        Intent intent = new Intent();
        intent.setClassName(context, "com.google.android.gms.ads.AdActivity");
        try {
            ResolveInfo resolveActivity = context.getPackageManager().resolveActivity(intent, 65536);
            if (resolveActivity == null || resolveActivity.activityInfo == null) {
                jm.e("Could not find com.google.android.gms.ads.AdActivity, please make sure it is declared in AndroidManifest.xml.");
                return false;
            }
            String str = "com.google.android.gms.ads.AdActivity requires the android:configChanges value to contain \"%s\".";
            if ((resolveActivity.activityInfo.configChanges & 16) == 0) {
                jm.e(String.format(str, new Object[]{"keyboard"}));
                z = false;
            } else {
                z = true;
            }
            if ((resolveActivity.activityInfo.configChanges & 32) == 0) {
                jm.e(String.format(str, new Object[]{"keyboardHidden"}));
                z = false;
            }
            if ((resolveActivity.activityInfo.configChanges & 128) == 0) {
                jm.e(String.format(str, new Object[]{"orientation"}));
                z = false;
            }
            if ((resolveActivity.activityInfo.configChanges & 256) == 0) {
                jm.e(String.format(str, new Object[]{"screenLayout"}));
                z = false;
            }
            if ((resolveActivity.activityInfo.configChanges & 512) == 0) {
                jm.e(String.format(str, new Object[]{"uiMode"}));
                z = false;
            }
            if ((resolveActivity.activityInfo.configChanges & 1024) == 0) {
                jm.e(String.format(str, new Object[]{"screenSize"}));
                z = false;
            }
            if ((resolveActivity.activityInfo.configChanges & 2048) != 0) {
                return z;
            }
            jm.e(String.format(str, new Object[]{"smallestScreenSize"}));
            return false;
        } catch (Exception e2) {
            jm.c("Could not verify that com.google.android.gms.ads.AdActivity is declared in AndroidManifest.xml", e2);
            ax.i().a((Throwable) e2, "AdUtil.hasAdActivity");
            return false;
        }
    }

    public static boolean a(Context context, String str) {
        return c.b(context).a(str, context.getPackageName()) == 0;
    }

    public static boolean a(ClassLoader classLoader, Class<?> cls, String str) {
        boolean z = false;
        try {
            return cls.isAssignableFrom(Class.forName(str, false, classLoader));
        } catch (Throwable th) {
            return z;
        }
    }

    public static int[] a(Activity activity) {
        Window window = activity.getWindow();
        if (window != null) {
            View findViewById = window.findViewById(16908290);
            if (findViewById != null) {
                return new int[]{findViewById.getWidth(), findViewById.getHeight()};
            }
        }
        return e();
    }

    public static int b(String str) {
        try {
            return Integer.parseInt(str);
        } catch (NumberFormatException e2) {
            String valueOf = String.valueOf(e2);
            jm.e(new StringBuilder(String.valueOf(valueOf).length() + 22).append("Could not parse value:").append(valueOf).toString());
            return 0;
        }
    }

    public static Bitmap b(View view) {
        if (view == null) {
            return null;
        }
        Bitmap f2 = f(view);
        return f2 == null ? e(view) : f2;
    }

    public static String b() {
        String str = Build.MANUFACTURER;
        String str2 = Build.MODEL;
        return str2.startsWith(str) ? str2 : new StringBuilder(String.valueOf(str).length() + 1 + String.valueOf(str2).length()).append(str).append(" ").append(str2).toString();
    }

    @TargetApi(18)
    public static void b(Context context, Intent intent) {
        if (intent != null && n.f()) {
            Bundle bundle = intent.getExtras() != null ? intent.getExtras() : new Bundle();
            bundle.putBinder("android.support.customtabs.extra.SESSION", null);
            bundle.putString("com.android.browser.application_id", context.getPackageName());
            intent.putExtras(bundle);
        }
    }

    public static void b(Context context, String str, String str2) {
        try {
            FileOutputStream openFileOutput = context.openFileOutput(str, 0);
            openFileOutput.write(str2.getBytes("UTF-8"));
            openFileOutput.close();
        } catch (Exception e2) {
            jm.b("Error writing to file in internal storage.", e2);
        }
    }

    public static Bundle c() {
        Bundle bundle = new Bundle();
        try {
            if (((Boolean) ape.f().a(asi.C)).booleanValue()) {
                MemoryInfo memoryInfo = new MemoryInfo();
                Debug.getMemoryInfo(memoryInfo);
                bundle.putParcelable("debug_memory_info", memoryInfo);
            }
            if (((Boolean) ape.f().a(asi.D)).booleanValue()) {
                Runtime runtime = Runtime.getRuntime();
                bundle.putLong("runtime_free_memory", runtime.freeMemory());
                bundle.putLong("runtime_max_memory", runtime.maxMemory());
                bundle.putLong("runtime_total_memory", runtime.totalMemory());
            }
            bundle.putInt("web_view_count", ax.i().k());
        } catch (Exception e2) {
            jm.c("Unable to gather memory stats", e2);
        }
        return bundle;
    }

    public static WebResourceResponse c(Context context, String str, String str2) {
        try {
            HashMap hashMap = new HashMap();
            hashMap.put("User-Agent", ax.e().b(context, str));
            hashMap.put("Cache-Control", "max-stale=3600");
            String str3 = (String) new lf(context).a(str2, (Map<String, String>) hashMap).get(60, TimeUnit.SECONDS);
            if (str3 != null) {
                return new WebResourceResponse("application/javascript", "UTF-8", new ByteArrayInputStream(str3.getBytes("UTF-8")));
            }
        } catch (IOException | InterruptedException | ExecutionException | TimeoutException e2) {
            jm.c("Could not fetch MRAID JS.", e2);
        }
        return null;
    }

    public static String c(Context context, String str) {
        try {
            return new String(l.a(context.openFileInput(str), true), "UTF-8");
        } catch (IOException e2) {
            jm.b("Error reading from internal storage.");
            return "";
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:6:0x0014  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0018  */
    public static boolean c(View view) {
        Activity activity;
        View rootView = view.getRootView();
        if (rootView != null) {
            Context context = rootView.getContext();
            if (context instanceof Activity) {
                activity = (Activity) context;
                if (activity != null) {
                    return false;
                }
                Window window = activity.getWindow();
                LayoutParams attributes = window == null ? null : window.getAttributes();
                return (attributes == null || (attributes.flags & 524288) == 0) ? false : true;
            }
        }
        activity = null;
        if (activity != null) {
        }
    }

    public static boolean c(String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        return str.matches("([^\\s]+(\\.(?i)(jpg|png|gif|bmp|webp))$)");
    }

    public static int d(View view) {
        if (view == null) {
            return -1;
        }
        ViewParent parent = view.getParent();
        while (parent != null && !(parent instanceof AdapterView)) {
            parent = parent.getParent();
        }
        if (parent == null) {
            return -1;
        }
        return ((AdapterView) parent).getPositionForView(view);
    }

    private static String d() {
        StringBuilder sb = new StringBuilder(256);
        sb.append("Mozilla/5.0 (Linux; U; Android");
        if (VERSION.RELEASE != null) {
            sb.append(" ").append(VERSION.RELEASE);
        }
        sb.append("; ").append(Locale.getDefault());
        if (Build.DEVICE != null) {
            sb.append("; ").append(Build.DEVICE);
            if (Build.DISPLAY != null) {
                sb.append(" Build/").append(Build.DISPLAY);
            }
        }
        sb.append(") AppleWebKit/533 Version/4.0 Safari/533");
        return sb.toString();
    }

    protected static String d(Context context) {
        try {
            return new WebView(context).getSettings().getUserAgentString();
        } catch (Throwable th) {
            return d();
        }
    }

    public static Builder e(Context context) {
        return new Builder(context);
    }

    private static Bitmap e(View view) {
        try {
            int width = view.getWidth();
            int height = view.getHeight();
            if (width == 0 || height == 0) {
                jm.e("Width or height of view is zero");
                return null;
            }
            Bitmap createBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Config.RGB_565);
            Canvas canvas = new Canvas(createBitmap);
            view.layout(0, 0, width, height);
            view.draw(canvas);
            return createBitmap;
        } catch (RuntimeException e2) {
            jm.b("Fail to capture the webview", e2);
            return null;
        }
    }

    private static int[] e() {
        return new int[]{0, 0};
    }

    private static Bitmap f(View view) {
        Throwable e2;
        Bitmap bitmap;
        try {
            boolean isDrawingCacheEnabled = view.isDrawingCacheEnabled();
            view.setDrawingCacheEnabled(true);
            Bitmap drawingCache = view.getDrawingCache();
            bitmap = drawingCache != null ? Bitmap.createBitmap(drawingCache) : null;
            try {
                view.setDrawingCacheEnabled(isDrawingCacheEnabled);
            } catch (RuntimeException e3) {
                e2 = e3;
                jm.b("Fail to capture the web view", e2);
                return bitmap;
            }
        } catch (RuntimeException e4) {
            Throwable th = e4;
            bitmap = null;
            e2 = th;
            jm.b("Fail to capture the web view", e2);
            return bitmap;
        }
        return bitmap;
    }

    public static art f(Context context) {
        return new art(context);
    }

    public static boolean g(Context context) {
        try {
            ActivityManager activityManager = (ActivityManager) context.getSystemService("activity");
            KeyguardManager keyguardManager = (KeyguardManager) context.getSystemService("keyguard");
            if (activityManager == null || keyguardManager == null) {
                return false;
            }
            List runningAppProcesses = activityManager.getRunningAppProcesses();
            if (runningAppProcesses == null) {
                return false;
            }
            Iterator it = runningAppProcesses.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                RunningAppProcessInfo runningAppProcessInfo = (RunningAppProcessInfo) it.next();
                if (Process.myPid() == runningAppProcessInfo.pid) {
                    if (runningAppProcessInfo.importance == 100 && !keyguardManager.inKeyguardRestrictedInputMode()) {
                        PowerManager powerManager = (PowerManager) context.getSystemService("power");
                        if (powerManager == null ? false : powerManager.isScreenOn()) {
                            return true;
                        }
                    }
                }
            }
            return false;
        } catch (Throwable th) {
            return false;
        }
    }

    public static Bitmap h(Context context) {
        Bitmap bitmap;
        if (!(context instanceof Activity)) {
            return null;
        }
        try {
            if (((Boolean) ape.f().a(asi.bS)).booleanValue()) {
                Window window = ((Activity) context).getWindow();
                if (window != null) {
                    bitmap = f(window.getDecorView().getRootView());
                }
                bitmap = null;
            } else {
                bitmap = e(((Activity) context).getWindow().getDecorView());
            }
        } catch (RuntimeException e2) {
            jm.b("Fail to capture screen shot", e2);
        }
        return bitmap;
    }

    public static int i(Context context) {
        ApplicationInfo applicationInfo = context.getApplicationInfo();
        if (applicationInfo == null) {
            return 0;
        }
        return applicationInfo.targetSdkVersion;
    }

    @TargetApi(16)
    public static boolean j(Context context) {
        if (context == null || !n.d()) {
            return false;
        }
        KeyguardManager m = m(context);
        return m != null && m.isKeyguardLocked();
    }

    public static boolean k(Context context) {
        try {
            context.getClassLoader().loadClass("com.google.android.gms.ads.internal.ClientApi");
            return false;
        } catch (ClassNotFoundException e2) {
            return true;
        } catch (Throwable th) {
            jm.b("Error loading class.", th);
            ax.i().a(th, "AdUtil.isLiteSdk");
            return false;
        }
    }

    private static String l(Context context) {
        try {
            ActivityManager activityManager = (ActivityManager) context.getSystemService("activity");
            if (activityManager == null) {
                return null;
            }
            List runningTasks = activityManager.getRunningTasks(1);
            if (runningTasks != null && !runningTasks.isEmpty()) {
                RunningTaskInfo runningTaskInfo = (RunningTaskInfo) runningTasks.get(0);
                if (!(runningTaskInfo == null || runningTaskInfo.topActivity == null)) {
                    return runningTaskInfo.topActivity.getClassName();
                }
            }
            return null;
        } catch (Exception e2) {
        }
    }

    private static KeyguardManager m(Context context) {
        Object systemService = context.getSystemService("keyguard");
        if (systemService == null || !(systemService instanceof KeyguardManager)) {
            return null;
        }
        return (KeyguardManager) systemService;
    }

    public final JSONObject a(Bundle bundle, JSONObject jSONObject) {
        JSONObject jSONObject2 = null;
        if (bundle == null) {
            return jSONObject2;
        }
        try {
            return a(bundle);
        } catch (JSONException e2) {
            jm.b("Error converting Bundle to JSON", e2);
            return jSONObject2;
        }
    }

    public final JSONObject a(Map<String, ?> map) throws JSONException {
        try {
            JSONObject jSONObject = new JSONObject();
            for (String str : map.keySet()) {
                a(jSONObject, str, map.get(str));
            }
            return jSONObject;
        } catch (ClassCastException e2) {
            String str2 = "Could not convert map to JSON: ";
            String valueOf = String.valueOf(e2.getMessage());
            throw new JSONException(valueOf.length() != 0 ? str2.concat(valueOf) : new String(str2));
        }
    }

    public final void a(Context context, String str, WebSettings webSettings) {
        webSettings.setUserAgentString(b(context, str));
    }

    public final void a(Context context, String str, String str2, Bundle bundle, boolean z) {
        if (z) {
            ax.e();
            bundle.putString("device", b());
            bundle.putString("eids", TextUtils.join(",", asi.a()));
        }
        ape.a();
        mh.a(context, str, str2, bundle, z, new jy(this, context, str));
    }

    public final void a(Context context, String str, boolean z, HttpURLConnection httpURLConnection) {
        httpURLConnection.setConnectTimeout(60000);
        httpURLConnection.setInstanceFollowRedirects(false);
        httpURLConnection.setReadTimeout(60000);
        httpURLConnection.setRequestProperty("User-Agent", b(context, str));
        httpURLConnection.setUseCaches(false);
    }

    public final void a(Context context, List<String> list) {
        if (!(context instanceof Activity) || TextUtils.isEmpty(agb.a((Activity) context))) {
            return;
        }
        if (list == null) {
            jm.a("Cannot ping urls: empty list.");
        } else if (!atg.a(context)) {
            jm.a("Cannot ping url because custom tabs is not supported");
        } else {
            atg atg = new atg();
            atg.a((ath) new jw(this, list, atg, context));
            atg.b((Activity) context);
        }
    }

    public final boolean a(View view, Context context) {
        PowerManager powerManager = null;
        Context applicationContext = context.getApplicationContext();
        if (applicationContext != null) {
            powerManager = (PowerManager) applicationContext.getSystemService("power");
        }
        return a(view, powerManager, m(context));
    }

    public final boolean a(View view, PowerManager powerManager, KeyguardManager keyguardManager) {
        boolean z;
        if (!ax.e().c) {
            if (keyguardManager == null ? false : keyguardManager.inKeyguardRestrictedInputMode()) {
                if (!((Boolean) ape.f().a(asi.bo)).booleanValue() || !c(view)) {
                    z = false;
                    if (view.getVisibility() == 0 && view.isShown()) {
                        if ((powerManager != null || powerManager.isScreenOn()) && z) {
                            if (!((Boolean) ape.f().a(asi.bm)).booleanValue() || view.getLocalVisibleRect(new Rect()) || view.getGlobalVisibleRect(new Rect())) {
                                return true;
                            }
                        }
                    }
                    return false;
                }
            }
        }
        z = true;
        return true;
    }

    public final String b(Context context, String str) {
        String str2;
        synchronized (this.f3441b) {
            if (this.d != null) {
                str2 = this.d;
            } else if (str == null) {
                str2 = d();
            } else {
                try {
                    this.d = ax.g().a(context);
                } catch (Exception e2) {
                }
                if (TextUtils.isEmpty(this.d)) {
                    ape.a();
                    if (!mh.b()) {
                        this.d = null;
                        f3440a.post(new jx(this, context));
                        while (this.d == null) {
                            try {
                                this.f3441b.wait();
                            } catch (InterruptedException e3) {
                                this.d = d();
                                String str3 = "Interrupted, use default user agent: ";
                                String valueOf = String.valueOf(this.d);
                                jm.e(valueOf.length() != 0 ? str3.concat(valueOf) : new String(str3));
                            }
                        }
                    } else {
                        this.d = d(context);
                    }
                }
                String valueOf2 = String.valueOf(this.d);
                this.d = new StringBuilder(String.valueOf(valueOf2).length() + 10 + String.valueOf(str).length()).append(valueOf2).append(" (Mobile; ").append(str).toString();
                try {
                    if (c.b(context).a()) {
                        this.d = String.valueOf(this.d).concat(";aia");
                    }
                } catch (Exception e4) {
                    ax.i().a((Throwable) e4, "AdUtil.getUserAgent");
                }
                this.d = String.valueOf(this.d).concat(")");
                str2 = this.d;
            }
        }
        return str2;
    }

    public final void b(Context context, String str, String str2, Bundle bundle, boolean z) {
        if (((Boolean) ape.f().a(asi.br)).booleanValue()) {
            a(context, str, str2, bundle, z);
        }
    }

    public final boolean b(Context context) {
        if (this.e) {
            return false;
        }
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.USER_PRESENT");
        intentFilter.addAction("android.intent.action.SCREEN_OFF");
        context.getApplicationContext().registerReceiver(new ka(this, null), intentFilter);
        this.e = true;
        return true;
    }

    public final int[] b(Activity activity) {
        int[] a2 = a(activity);
        ape.a();
        ape.a();
        return new int[]{mh.b((Context) activity, a2[0]), mh.b((Context) activity, a2[1])};
    }

    public final boolean c(Context context) {
        if (this.f) {
            return false;
        }
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.google.android.ads.intent.DEBUG_LOGGING_ENABLEMENT_CHANGED");
        context.getApplicationContext().registerReceiver(new jz(this, null), intentFilter);
        this.f = true;
        return true;
    }

    public final int[] c(Activity activity) {
        int[] e2;
        Window window = activity.getWindow();
        if (window != null) {
            View findViewById = window.findViewById(16908290);
            if (findViewById != null) {
                e2 = new int[]{findViewById.getTop(), findViewById.getBottom()};
                ape.a();
                ape.a();
                return new int[]{mh.b((Context) activity, e2[0]), mh.b((Context) activity, e2[1])};
            }
        }
        e2 = e();
        ape.a();
        ape.a();
        return new int[]{mh.b((Context) activity, e2[0]), mh.b((Context) activity, e2[1])};
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0024, code lost:
        if (((java.lang.String) com.google.android.gms.internal.ads.ape.f().a(com.google.android.gms.internal.ads.asi.aq)).equals(r3.g.pattern()) == false) goto L_0x0026;
     */
    public final boolean d(String str) {
        boolean matches;
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        try {
            synchronized (this) {
                if (this.g != null) {
                }
                this.g = Pattern.compile((String) ape.f().a(asi.aq));
                matches = this.g.matcher(str).matches();
            }
            return matches;
        } catch (PatternSyntaxException e2) {
            return false;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0024, code lost:
        if (((java.lang.String) com.google.android.gms.internal.ads.ape.f().a(com.google.android.gms.internal.ads.asi.ar)).equals(r3.h.pattern()) == false) goto L_0x0026;
     */
    public final boolean e(String str) {
        boolean matches;
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        try {
            synchronized (this) {
                if (this.h != null) {
                }
                this.h = Pattern.compile((String) ape.f().a(asi.ar));
                matches = this.h.matcher(str).matches();
            }
            return matches;
        } catch (PatternSyntaxException e2) {
            return false;
        }
    }
}
