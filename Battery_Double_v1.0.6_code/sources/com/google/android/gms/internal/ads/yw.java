package com.google.android.gms.internal.ads;

import java.security.GeneralSecurityException;
import java.security.Provider;

public interface yw<T> {
    T a(String str, Provider provider) throws GeneralSecurityException;
}
