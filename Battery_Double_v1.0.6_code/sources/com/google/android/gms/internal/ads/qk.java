package com.google.android.gms.internal.ads;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.webkit.JsPromptResult;

final class qk implements OnClickListener {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ JsPromptResult f3633a;

    qk(JsPromptResult jsPromptResult) {
        this.f3633a = jsPromptResult;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f3633a.cancel();
    }
}
