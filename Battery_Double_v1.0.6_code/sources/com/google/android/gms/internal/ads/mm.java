package com.google.android.gms.internal.ads;

import android.util.JsonWriter;
import java.util.Map;

final /* synthetic */ class mm implements mr {

    /* renamed from: a reason: collision with root package name */
    private final String f3519a;

    /* renamed from: b reason: collision with root package name */
    private final String f3520b;
    private final Map c;
    private final byte[] d;

    mm(String str, String str2, Map map, byte[] bArr) {
        this.f3519a = str;
        this.f3520b = str2;
        this.c = map;
        this.d = bArr;
    }

    public final void a(JsonWriter jsonWriter) {
        ml.a(this.f3519a, this.f3520b, this.c, this.d, jsonWriter);
    }
}
