package com.google.android.gms.internal.ads;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.os.HandlerThread;
import android.view.Display;
import android.view.WindowManager;

@cm
final class ph implements SensorEventListener {

    /* renamed from: a reason: collision with root package name */
    private final SensorManager f3600a;

    /* renamed from: b reason: collision with root package name */
    private final Object f3601b = new Object();
    private final Display c;
    private final float[] d = new float[9];
    private final float[] e = new float[9];
    private float[] f;
    private Handler g;
    private pj h;

    ph(Context context) {
        this.f3600a = (SensorManager) context.getSystemService("sensor");
        this.c = ((WindowManager) context.getSystemService("window")).getDefaultDisplay();
    }

    private final void a(int i, int i2) {
        float f2 = this.e[i];
        this.e[i] = this.e[i2];
        this.e[i2] = f2;
    }

    /* access modifiers changed from: 0000 */
    public final void a() {
        if (this.g == null) {
            Sensor defaultSensor = this.f3600a.getDefaultSensor(11);
            if (defaultSensor == null) {
                jm.c("No Sensor of TYPE_ROTATION_VECTOR");
                return;
            }
            HandlerThread handlerThread = new HandlerThread("OrientationMonitor");
            handlerThread.start();
            this.g = new Handler(handlerThread.getLooper());
            if (!this.f3600a.registerListener(this, defaultSensor, 0, this.g)) {
                jm.c("SensorManager.registerListener failed.");
                b();
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public final void a(pj pjVar) {
        this.h = pjVar;
    }

    /* access modifiers changed from: 0000 */
    public final boolean a(float[] fArr) {
        boolean z = false;
        synchronized (this.f3601b) {
            if (this.f != null) {
                System.arraycopy(this.f, 0, fArr, 0, this.f.length);
                z = true;
            }
        }
        return z;
    }

    /* access modifiers changed from: 0000 */
    public final void b() {
        if (this.g != null) {
            this.f3600a.unregisterListener(this);
            this.g.post(new pi(this));
            this.g = null;
        }
    }

    public final void onAccuracyChanged(Sensor sensor, int i) {
    }

    public final void onSensorChanged(SensorEvent sensorEvent) {
        float[] fArr = sensorEvent.values;
        if (fArr[0] != 0.0f || fArr[1] != 0.0f || fArr[2] != 0.0f) {
            synchronized (this.f3601b) {
                if (this.f == null) {
                    this.f = new float[9];
                }
            }
            SensorManager.getRotationMatrixFromVector(this.d, fArr);
            switch (this.c.getRotation()) {
                case 1:
                    SensorManager.remapCoordinateSystem(this.d, 2, 129, this.e);
                    break;
                case 2:
                    SensorManager.remapCoordinateSystem(this.d, 129, 130, this.e);
                    break;
                case 3:
                    SensorManager.remapCoordinateSystem(this.d, 130, 1, this.e);
                    break;
                default:
                    System.arraycopy(this.d, 0, this.e, 0, 9);
                    break;
            }
            a(1, 3);
            a(2, 6);
            a(5, 7);
            synchronized (this.f3601b) {
                System.arraycopy(this.e, 0, this.f, 0, 9);
            }
            if (this.h != null) {
                this.h.a();
            }
        }
    }
}
