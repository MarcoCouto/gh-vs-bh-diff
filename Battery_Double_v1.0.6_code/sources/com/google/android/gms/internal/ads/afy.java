package com.google.android.gms.internal.ads;

import java.io.IOException;

public final class afy extends afh<afy> {

    /* renamed from: a reason: collision with root package name */
    public Integer f2549a;

    /* renamed from: b reason: collision with root package name */
    public String f2550b;
    public byte[] c;

    public afy() {
        this.f2549a = null;
        this.f2550b = null;
        this.c = null;
        this.Y = null;
        this.Z = -1;
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public final afy a(afd afd) throws IOException {
        while (true) {
            int a2 = afd.a();
            switch (a2) {
                case 0:
                    break;
                case 8:
                    int j = afd.j();
                    try {
                        int c2 = afd.c();
                        if (c2 < 0 || c2 > 1) {
                            throw new IllegalArgumentException(c2 + " is not a valid enum Type");
                        }
                        this.f2549a = Integer.valueOf(c2);
                        continue;
                    } catch (IllegalArgumentException e) {
                        afd.e(j);
                        a(afd, a2);
                        break;
                    }
                case 18:
                    this.f2550b = afd.e();
                    continue;
                case 26:
                    this.c = afd.f();
                    continue;
                default:
                    if (!super.a(afd, a2)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    /* access modifiers changed from: protected */
    public final int a() {
        int a2 = super.a();
        if (this.f2549a != null) {
            a2 += aff.b(1, this.f2549a.intValue());
        }
        if (this.f2550b != null) {
            a2 += aff.b(2, this.f2550b);
        }
        return this.c != null ? a2 + aff.b(3, this.c) : a2;
    }

    public final void a(aff aff) throws IOException {
        if (this.f2549a != null) {
            aff.a(1, this.f2549a.intValue());
        }
        if (this.f2550b != null) {
            aff.a(2, this.f2550b);
        }
        if (this.c != null) {
            aff.a(3, this.c);
        }
        super.a(aff);
    }
}
