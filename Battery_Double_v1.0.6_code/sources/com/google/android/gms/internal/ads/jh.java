package com.google.android.gms.internal.ads;

@cm
public abstract class jh implements ko<nn> {

    /* renamed from: a reason: collision with root package name */
    private final Runnable f3424a = new ji(this);
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public volatile Thread f3425b;
    private boolean c = false;

    public jh() {
    }

    public jh(boolean z) {
    }

    public abstract void a();

    public final void b() {
        c_();
        if (this.f3425b != null) {
            this.f3425b.interrupt();
        }
    }

    public final /* synthetic */ Object c() {
        return this.c ? jt.b(this.f3424a) : jt.a(this.f3424a);
    }

    public abstract void c_();

    public final nn h() {
        return this.c ? jt.b(this.f3424a) : jt.a(this.f3424a);
    }
}
