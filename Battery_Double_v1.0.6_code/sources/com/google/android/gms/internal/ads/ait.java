package com.google.android.gms.internal.ads;

import java.util.concurrent.Callable;

public final class ait implements Callable {

    /* renamed from: a reason: collision with root package name */
    private final ahy f2628a;

    /* renamed from: b reason: collision with root package name */
    private final zz f2629b;

    public ait(ahy ahy, zz zzVar) {
        this.f2628a = ahy;
        this.f2629b = zzVar;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public final Void call() throws Exception {
        if (this.f2628a.l() != null) {
            this.f2628a.l().get();
        }
        zz k = this.f2628a.k();
        if (k != null) {
            try {
                synchronized (this.f2629b) {
                    afn.a(this.f2629b, afn.a((afn) k));
                }
            } catch (afm e) {
            }
        }
        return null;
    }
}
