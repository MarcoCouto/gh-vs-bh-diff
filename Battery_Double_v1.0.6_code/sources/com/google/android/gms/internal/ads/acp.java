package com.google.android.gms.internal.ads;

final class acp<K, V> {

    /* renamed from: a reason: collision with root package name */
    public final aew f2461a;

    /* renamed from: b reason: collision with root package name */
    public final K f2462b;
    public final aew c;
    public final V d;
}
