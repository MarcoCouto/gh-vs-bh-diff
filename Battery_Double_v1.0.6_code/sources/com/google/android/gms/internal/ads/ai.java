package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.ads.internal.ax;
import com.google.android.gms.common.internal.aa;
import java.util.concurrent.atomic.AtomicBoolean;

@cm
public abstract class ai implements ko<Void>, rw {

    /* renamed from: a reason: collision with root package name */
    protected final Context f2611a;

    /* renamed from: b reason: collision with root package name */
    protected final qn f2612b;
    protected dp c;
    private final ap d;
    private final is e;
    private Runnable f;
    private final Object g = new Object();
    /* access modifiers changed from: private */
    public AtomicBoolean h = new AtomicBoolean(true);

    protected ai(Context context, is isVar, qn qnVar, ap apVar) {
        this.f2611a = context;
        this.e = isVar;
        this.c = this.e.f3398b;
        this.f2612b = qnVar;
        this.d = apVar;
    }

    /* access modifiers changed from: protected */
    public abstract void a();

    /* access modifiers changed from: protected */
    public void a(int i) {
        if (i != -2) {
            this.c = new dp(i, this.c.j);
        }
        this.f2612b.p();
        ap apVar = this.d;
        dl dlVar = this.e.f3397a;
        ap apVar2 = apVar;
        apVar2.b(new ir(dlVar.c, this.f2612b, this.c.c, i, this.c.e, this.c.i, this.c.k, this.c.j, dlVar.i, this.c.g, null, null, null, null, null, this.c.h, this.e.d, this.c.f, this.e.f, this.c.m, this.c.n, this.e.h, null, this.c.A, this.c.B, this.c.C, this.c.D, this.c.E, null, this.c.H, this.c.L, this.e.i, this.e.f3398b.O, this.e.j, this.e.f3398b.Q, this.c.R, this.e.f3398b.S, this.e.f3398b.T));
    }

    public final void a(boolean z) {
        int i = 0;
        jm.b("WebView finished loading.");
        if (this.h.getAndSet(false)) {
            if (z) {
                i = -2;
            }
            a(i);
            jv.f3440a.removeCallbacks(this.f);
        }
    }

    public void b() {
        if (this.h.getAndSet(false)) {
            this.f2612b.stopLoading();
            ax.g();
            kb.a(this.f2612b);
            a(-1);
            jv.f3440a.removeCallbacks(this.f);
        }
    }

    public final /* synthetic */ Object c() {
        aa.b("Webview render task needs to be called on UI thread.");
        this.f = new aj(this);
        jv.f3440a.postDelayed(this.f, ((Long) ape.f().a(asi.bB)).longValue());
        a();
        return null;
    }
}
