package com.google.android.gms.internal.ads;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.a.a;
import com.google.android.gms.common.internal.a.c;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

@cm
public final class dl extends a {
    public static final Creator<dl> CREATOR = new dn();
    public final long A;
    public final String B;
    public final float C;
    public final int D;
    public final int E;
    public final boolean F;
    public final boolean G;
    public final String H;
    public final boolean I;
    public final String J;
    public final boolean K;
    public final int L;
    public final Bundle M;
    public final String N;
    public final aqy O;
    public final boolean P;
    public final Bundle Q;
    public final String R;
    public final String S;
    public final String T;
    public final boolean U;
    public final List<Integer> V;
    public final String W;
    public final List<String> X;
    public final int Y;
    public final boolean Z;

    /* renamed from: a reason: collision with root package name */
    public final int f3260a;
    public final boolean aa;
    public final boolean ab;
    public final ArrayList<String> ac;

    /* renamed from: b reason: collision with root package name */
    public final Bundle f3261b;
    public final aop c;
    public final aot d;
    public final String e;
    public final ApplicationInfo f;
    public final PackageInfo g;
    public final String h;
    public final String i;
    public final String j;
    public final mu k;
    public final Bundle l;
    public final int m;
    public final List<String> n;
    public final Bundle o;
    public final boolean p;
    public final int q;
    public final int r;
    public final float s;
    public final String t;
    public final long u;
    public final String v;
    public final List<String> w;
    public final String x;
    public final aul y;
    public final List<String> z;

    dl(int i2, Bundle bundle, aop aop, aot aot, String str, ApplicationInfo applicationInfo, PackageInfo packageInfo, String str2, String str3, String str4, mu muVar, Bundle bundle2, int i3, List<String> list, Bundle bundle3, boolean z2, int i4, int i5, float f2, String str5, long j2, String str6, List<String> list2, String str7, aul aul, List<String> list3, long j3, String str8, float f3, boolean z3, int i6, int i7, boolean z4, boolean z5, String str9, String str10, boolean z6, int i8, Bundle bundle4, String str11, aqy aqy, boolean z7, Bundle bundle5, String str12, String str13, String str14, boolean z8, List<Integer> list4, String str15, List<String> list5, int i9, boolean z9, boolean z10, boolean z11, ArrayList<String> arrayList) {
        this.f3260a = i2;
        this.f3261b = bundle;
        this.c = aop;
        this.d = aot;
        this.e = str;
        this.f = applicationInfo;
        this.g = packageInfo;
        this.h = str2;
        this.i = str3;
        this.j = str4;
        this.k = muVar;
        this.l = bundle2;
        this.m = i3;
        this.n = list;
        this.z = list3 == null ? Collections.emptyList() : Collections.unmodifiableList(list3);
        this.o = bundle3;
        this.p = z2;
        this.q = i4;
        this.r = i5;
        this.s = f2;
        this.t = str5;
        this.u = j2;
        this.v = str6;
        this.w = list2 == null ? Collections.emptyList() : Collections.unmodifiableList(list2);
        this.x = str7;
        this.y = aul;
        this.A = j3;
        this.B = str8;
        this.C = f3;
        this.I = z3;
        this.D = i6;
        this.E = i7;
        this.F = z4;
        this.G = z5;
        this.H = str9;
        this.J = str10;
        this.K = z6;
        this.L = i8;
        this.M = bundle4;
        this.N = str11;
        this.O = aqy;
        this.P = z7;
        this.Q = bundle5;
        this.R = str12;
        this.S = str13;
        this.T = str14;
        this.U = z8;
        this.V = list4;
        this.W = str15;
        this.X = list5;
        this.Y = i9;
        this.Z = z9;
        this.aa = z10;
        this.ab = z11;
        this.ac = arrayList;
    }

    private dl(Bundle bundle, aop aop, aot aot, String str, ApplicationInfo applicationInfo, PackageInfo packageInfo, String str2, String str3, String str4, mu muVar, Bundle bundle2, int i2, List<String> list, List<String> list2, Bundle bundle3, boolean z2, int i3, int i4, float f2, String str5, long j2, String str6, List<String> list3, String str7, aul aul, long j3, String str8, float f3, boolean z3, int i5, int i6, boolean z4, boolean z5, String str9, String str10, boolean z6, int i7, Bundle bundle4, String str11, aqy aqy, boolean z7, Bundle bundle5, String str12, String str13, String str14, boolean z8, List<Integer> list4, String str15, List<String> list5, int i8, boolean z9, boolean z10, boolean z11, ArrayList<String> arrayList) {
        this(24, bundle, aop, aot, str, applicationInfo, packageInfo, str2, str3, str4, muVar, bundle2, i2, list, bundle3, z2, i3, i4, f2, str5, j2, str6, list3, str7, aul, list2, j3, str8, f3, z3, i5, i6, z4, z5, str9, str10, z6, i7, bundle4, str11, aqy, z7, bundle5, str12, str13, str14, z8, list4, str15, list5, i8, z9, z10, z11, arrayList);
    }

    public dl(dm dmVar, long j2, String str, String str2, String str3) {
        this(dmVar.f3262a, dmVar.f3263b, dmVar.c, dmVar.d, dmVar.e, dmVar.f, (String) nc.a(dmVar.Q, ""), dmVar.g, dmVar.h, dmVar.j, dmVar.i, dmVar.k, dmVar.l, dmVar.m, dmVar.o, dmVar.p, dmVar.q, dmVar.r, dmVar.s, dmVar.t, dmVar.u, dmVar.v, dmVar.w, dmVar.x, dmVar.y, j2, dmVar.z, dmVar.A, dmVar.B, dmVar.C, dmVar.D, dmVar.E, dmVar.F, (String) nc.a(dmVar.G, "", 1, TimeUnit.SECONDS), dmVar.H, dmVar.I, dmVar.J, dmVar.K, dmVar.L, dmVar.M, dmVar.N, dmVar.O, str, str2, str3, dmVar.P, dmVar.R, dmVar.S, dmVar.n, dmVar.T, dmVar.U, dmVar.V, dmVar.W, dmVar.X);
    }

    public final void writeToParcel(Parcel parcel, int i2) {
        int a2 = c.a(parcel);
        c.a(parcel, 1, this.f3260a);
        c.a(parcel, 2, this.f3261b, false);
        c.a(parcel, 3, (Parcelable) this.c, i2, false);
        c.a(parcel, 4, (Parcelable) this.d, i2, false);
        c.a(parcel, 5, this.e, false);
        c.a(parcel, 6, (Parcelable) this.f, i2, false);
        c.a(parcel, 7, (Parcelable) this.g, i2, false);
        c.a(parcel, 8, this.h, false);
        c.a(parcel, 9, this.i, false);
        c.a(parcel, 10, this.j, false);
        c.a(parcel, 11, (Parcelable) this.k, i2, false);
        c.a(parcel, 12, this.l, false);
        c.a(parcel, 13, this.m);
        c.b(parcel, 14, this.n, false);
        c.a(parcel, 15, this.o, false);
        c.a(parcel, 16, this.p);
        c.a(parcel, 18, this.q);
        c.a(parcel, 19, this.r);
        c.a(parcel, 20, this.s);
        c.a(parcel, 21, this.t, false);
        c.a(parcel, 25, this.u);
        c.a(parcel, 26, this.v, false);
        c.b(parcel, 27, this.w, false);
        c.a(parcel, 28, this.x, false);
        c.a(parcel, 29, (Parcelable) this.y, i2, false);
        c.b(parcel, 30, this.z, false);
        c.a(parcel, 31, this.A);
        c.a(parcel, 33, this.B, false);
        c.a(parcel, 34, this.C);
        c.a(parcel, 35, this.D);
        c.a(parcel, 36, this.E);
        c.a(parcel, 37, this.F);
        c.a(parcel, 38, this.G);
        c.a(parcel, 39, this.H, false);
        c.a(parcel, 40, this.I);
        c.a(parcel, 41, this.J, false);
        c.a(parcel, 42, this.K);
        c.a(parcel, 43, this.L);
        c.a(parcel, 44, this.M, false);
        c.a(parcel, 45, this.N, false);
        c.a(parcel, 46, (Parcelable) this.O, i2, false);
        c.a(parcel, 47, this.P);
        c.a(parcel, 48, this.Q, false);
        c.a(parcel, 49, this.R, false);
        c.a(parcel, 50, this.S, false);
        c.a(parcel, 51, this.T, false);
        c.a(parcel, 52, this.U);
        c.a(parcel, 53, this.V, false);
        c.a(parcel, 54, this.W, false);
        c.b(parcel, 55, this.X, false);
        c.a(parcel, 56, this.Y);
        c.a(parcel, 57, this.Z);
        c.a(parcel, 58, this.aa);
        c.a(parcel, 59, this.ab);
        c.b(parcel, 60, this.ac, false);
        c.a(parcel, a2);
    }
}
