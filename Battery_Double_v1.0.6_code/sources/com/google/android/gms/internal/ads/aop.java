package com.google.android.gms.internal.ads;

import android.location.Location;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.a.a;
import com.google.android.gms.common.internal.a.c;
import com.google.android.gms.common.internal.z;
import java.util.List;

@cm
public final class aop extends a {
    public static final Creator<aop> CREATOR = new aor();

    /* renamed from: a reason: collision with root package name */
    public final int f2809a;

    /* renamed from: b reason: collision with root package name */
    public final long f2810b;
    public final Bundle c;
    public final int d;
    public final List<String> e;
    public final boolean f;
    public final int g;
    public final boolean h;
    public final String i;
    public final arn j;
    public final Location k;
    public final String l;
    public final Bundle m;
    public final Bundle n;
    public final List<String> o;
    public final String p;
    public final String q;
    public final boolean r;

    public aop(int i2, long j2, Bundle bundle, int i3, List<String> list, boolean z, int i4, boolean z2, String str, arn arn, Location location, String str2, Bundle bundle2, Bundle bundle3, List<String> list2, String str3, String str4, boolean z3) {
        this.f2809a = i2;
        this.f2810b = j2;
        if (bundle == null) {
            bundle = new Bundle();
        }
        this.c = bundle;
        this.d = i3;
        this.e = list;
        this.f = z;
        this.g = i4;
        this.h = z2;
        this.i = str;
        this.j = arn;
        this.k = location;
        this.l = str2;
        if (bundle2 == null) {
            bundle2 = new Bundle();
        }
        this.m = bundle2;
        this.n = bundle3;
        this.o = list2;
        this.p = str3;
        this.q = str4;
        this.r = z3;
    }

    public final aop a() {
        Bundle bundle = this.m.getBundle("com.google.ads.mediation.admob.AdMobAdapter");
        if (bundle == null) {
            bundle = this.c;
            this.m.putBundle("com.google.ads.mediation.admob.AdMobAdapter", this.c);
        }
        return new aop(this.f2809a, this.f2810b, bundle, this.d, this.e, this.f, this.g, this.h, this.i, this.j, this.k, this.l, this.m, this.n, this.o, this.p, this.q, this.r);
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof aop)) {
            return false;
        }
        aop aop = (aop) obj;
        return this.f2809a == aop.f2809a && this.f2810b == aop.f2810b && z.a(this.c, aop.c) && this.d == aop.d && z.a(this.e, aop.e) && this.f == aop.f && this.g == aop.g && this.h == aop.h && z.a(this.i, aop.i) && z.a(this.j, aop.j) && z.a(this.k, aop.k) && z.a(this.l, aop.l) && z.a(this.m, aop.m) && z.a(this.n, aop.n) && z.a(this.o, aop.o) && z.a(this.p, aop.p) && z.a(this.q, aop.q) && this.r == aop.r;
    }

    public final int hashCode() {
        return z.a(Integer.valueOf(this.f2809a), Long.valueOf(this.f2810b), this.c, Integer.valueOf(this.d), this.e, Boolean.valueOf(this.f), Integer.valueOf(this.g), Boolean.valueOf(this.h), this.i, this.j, this.k, this.l, this.m, this.n, this.o, this.p, this.q, Boolean.valueOf(this.r));
    }

    public final void writeToParcel(Parcel parcel, int i2) {
        int a2 = c.a(parcel);
        c.a(parcel, 1, this.f2809a);
        c.a(parcel, 2, this.f2810b);
        c.a(parcel, 3, this.c, false);
        c.a(parcel, 4, this.d);
        c.b(parcel, 5, this.e, false);
        c.a(parcel, 6, this.f);
        c.a(parcel, 7, this.g);
        c.a(parcel, 8, this.h);
        c.a(parcel, 9, this.i, false);
        c.a(parcel, 10, (Parcelable) this.j, i2, false);
        c.a(parcel, 11, (Parcelable) this.k, i2, false);
        c.a(parcel, 12, this.l, false);
        c.a(parcel, 13, this.m, false);
        c.a(parcel, 14, this.n, false);
        c.b(parcel, 15, this.o, false);
        c.a(parcel, 16, this.p, false);
        c.a(parcel, 17, this.q, false);
        c.a(parcel, 18, this.r);
        c.a(parcel, a2);
    }
}
