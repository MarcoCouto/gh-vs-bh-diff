package com.google.android.gms.internal.ads;

import java.util.Iterator;
import java.util.Map.Entry;

final class adv extends aeb {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ ads f2492a;

    private adv(ads ads) {
        this.f2492a = ads;
        super(ads, null);
    }

    /* synthetic */ adv(ads ads, adt adt) {
        this(ads);
    }

    public final Iterator<Entry<K, V>> iterator() {
        return new adu(this.f2492a, null);
    }
}
