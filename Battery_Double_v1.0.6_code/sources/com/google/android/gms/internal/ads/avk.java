package com.google.android.gms.internal.ads;

import android.os.IBinder;

public final class avk extends ajk implements avj {
    avk(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.formats.client.INativeAdViewHolderDelegateCreator");
    }
}
