package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.gmsg.ae;
import com.google.android.gms.common.util.o;

final /* synthetic */ class azz implements o {

    /* renamed from: a reason: collision with root package name */
    private final ae f3051a;

    azz(ae aeVar) {
        this.f3051a = aeVar;
    }

    public final boolean a(Object obj) {
        ae aeVar = (ae) obj;
        return (aeVar instanceof baf) && ((baf) aeVar).f3063a.equals(this.f3051a);
    }
}
