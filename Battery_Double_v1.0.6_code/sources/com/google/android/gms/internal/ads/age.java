package com.google.android.gms.internal.ads;

import java.io.IOException;

public final class age extends afh<age> {

    /* renamed from: a reason: collision with root package name */
    public byte[][] f2557a;

    /* renamed from: b reason: collision with root package name */
    public byte[] f2558b;
    public Integer c;
    private Integer d;

    public age() {
        this.f2557a = afq.d;
        this.f2558b = null;
        this.Z = -1;
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public final age a(afd afd) throws IOException {
        while (true) {
            int a2 = afd.a();
            switch (a2) {
                case 0:
                    break;
                case 10:
                    int a3 = afq.a(afd, 10);
                    int length = this.f2557a == null ? 0 : this.f2557a.length;
                    byte[][] bArr = new byte[(a3 + length)][];
                    if (length != 0) {
                        System.arraycopy(this.f2557a, 0, bArr, 0, length);
                    }
                    while (length < bArr.length - 1) {
                        bArr[length] = afd.f();
                        afd.a();
                        length++;
                    }
                    bArr[length] = afd.f();
                    this.f2557a = bArr;
                    continue;
                case 18:
                    this.f2558b = afd.f();
                    continue;
                case 24:
                    int j = afd.j();
                    try {
                        this.d = Integer.valueOf(yx.b(afd.g()));
                        continue;
                    } catch (IllegalArgumentException e) {
                        afd.e(j);
                        a(afd, a2);
                        break;
                    }
                case 32:
                    int j2 = afd.j();
                    try {
                        this.c = Integer.valueOf(yx.c(afd.g()));
                        continue;
                    } catch (IllegalArgumentException e2) {
                        afd.e(j2);
                        a(afd, a2);
                        break;
                    }
                default:
                    if (!super.a(afd, a2)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    /* access modifiers changed from: protected */
    public final int a() {
        int i;
        int a2 = super.a();
        if (this.f2557a == null || this.f2557a.length <= 0) {
            i = a2;
        } else {
            int i2 = 0;
            int i3 = 0;
            for (byte[] bArr : this.f2557a) {
                if (bArr != null) {
                    i3++;
                    i2 += aff.b(bArr);
                }
            }
            i = a2 + i2 + (i3 * 1);
        }
        if (this.f2558b != null) {
            i += aff.b(2, this.f2558b);
        }
        if (this.d != null) {
            i += aff.b(3, this.d.intValue());
        }
        return this.c != null ? i + aff.b(4, this.c.intValue()) : i;
    }

    public final void a(aff aff) throws IOException {
        if (this.f2557a != null && this.f2557a.length > 0) {
            for (byte[] bArr : this.f2557a) {
                if (bArr != null) {
                    aff.a(1, bArr);
                }
            }
        }
        if (this.f2558b != null) {
            aff.a(2, this.f2558b);
        }
        if (this.d != null) {
            aff.a(3, this.d.intValue());
        }
        if (this.c != null) {
            aff.a(4, this.c.intValue());
        }
        super.a(aff);
    }
}
