package com.google.android.gms.internal.ads;

import org.json.JSONObject;

final class ej implements Runnable {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ JSONObject f3284a;

    /* renamed from: b reason: collision with root package name */
    final /* synthetic */ String f3285b;
    private final /* synthetic */ eh c;

    ej(eh ehVar, JSONObject jSONObject, String str) {
        this.c = ehVar;
        this.f3284a = jSONObject;
        this.f3285b = str;
    }

    public final void run() {
        this.c.l = eh.d.b((ahh) null);
        this.c.l.a(new ek(this), new el(this));
    }
}
