package com.google.android.gms.internal.ads;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.security.GeneralSecurityException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Vector;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

final class agi {

    /* renamed from: a reason: collision with root package name */
    static CountDownLatch f2562a = new CountDownLatch(1);

    /* renamed from: b reason: collision with root package name */
    private static boolean f2563b = false;
    /* access modifiers changed from: private */
    public static MessageDigest c = null;
    private static final Object d = new Object();
    private static final Object e = new Object();

    private static zz a(long j) {
        zz zzVar = new zz();
        zzVar.k = Long.valueOf(4096);
        return zzVar;
    }

    static String a(zz zzVar, String str) throws GeneralSecurityException, UnsupportedEncodingException {
        byte[] a2;
        byte[] a3 = afn.a((afn) zzVar);
        if (((Boolean) ape.f().a(asi.bL)).booleanValue()) {
            Vector a4 = a(a3, 255);
            if (a4 == null || a4.size() == 0) {
                a2 = a(afn.a((afn) a(4096)), str, true);
            } else {
                age age = new age();
                age.f2557a = new byte[a4.size()][];
                Iterator it = a4.iterator();
                int i = 0;
                while (it.hasNext()) {
                    int i2 = i + 1;
                    age.f2557a[i] = a((byte[]) it.next(), str, false);
                    i = i2;
                }
                age.f2558b = a(a3);
                a2 = afn.a((afn) age);
            }
        } else if (aie.f2618a == null) {
            throw new GeneralSecurityException();
        } else {
            byte[] a5 = aie.f2618a.a(a3, str != null ? str.getBytes() : new byte[0]);
            age age2 = new age();
            age2.f2557a = new byte[][]{a5};
            age2.c = Integer.valueOf(2);
            a2 = afn.a((afn) age2);
        }
        return agg.a(a2, true);
    }

    private static Vector<byte[]> a(byte[] bArr, int i) {
        if (bArr == null || bArr.length <= 0) {
            return null;
        }
        int length = ((bArr.length + 255) - 1) / 255;
        Vector vector = new Vector();
        int i2 = 0;
        while (i2 < length) {
            int i3 = i2 * 255;
            try {
                vector.add(Arrays.copyOfRange(bArr, i3, bArr.length - i3 > 255 ? i3 + 255 : bArr.length));
                i2++;
            } catch (IndexOutOfBoundsException e2) {
                return null;
            }
        }
        return vector;
    }

    static void a() {
        synchronized (e) {
            if (!f2563b) {
                f2563b = true;
                new Thread(new agk()).start();
            }
        }
    }

    public static byte[] a(byte[] bArr) throws NoSuchAlgorithmException {
        byte[] digest;
        synchronized (d) {
            MessageDigest b2 = b();
            if (b2 == null) {
                throw new NoSuchAlgorithmException("Cannot compute hash");
            }
            b2.reset();
            b2.update(bArr);
            digest = c.digest();
        }
        return digest;
    }

    private static byte[] a(byte[] bArr, String str, boolean z) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        byte[] array;
        int i = z ? 239 : 255;
        if (bArr.length > i) {
            bArr = afn.a((afn) a(4096));
        }
        if (bArr.length < i) {
            byte[] bArr2 = new byte[(i - bArr.length)];
            new SecureRandom().nextBytes(bArr2);
            array = ByteBuffer.allocate(i + 1).put((byte) bArr.length).put(bArr).put(bArr2).array();
        } else {
            array = ByteBuffer.allocate(i + 1).put((byte) bArr.length).put(bArr).array();
        }
        if (z) {
            array = ByteBuffer.allocate(256).put(a(array)).put(array).array();
        }
        byte[] bArr3 = new byte[256];
        for (agn a2 : new agl().cN) {
            a2.a(array, bArr3);
        }
        if (str != null && str.length() > 0) {
            if (str.length() > 32) {
                str = str.substring(0, 32);
            }
            new zv(str.getBytes("UTF-8")).a(bArr3);
        }
        return bArr3;
    }

    private static MessageDigest b() {
        a();
        boolean z = false;
        try {
            z = f2562a.await(2, TimeUnit.SECONDS);
        } catch (InterruptedException e2) {
        }
        if (z && c != null) {
            return c;
        }
        return null;
    }
}
