package com.google.android.gms.internal.ads;

import android.os.IInterface;
import android.os.RemoteException;

public interface aqv extends IInterface {
    void a() throws RemoteException;

    void a(boolean z) throws RemoteException;

    void b() throws RemoteException;

    void c() throws RemoteException;

    void d() throws RemoteException;
}
