package com.google.android.gms.internal.ads;

import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import java.io.ByteArrayOutputStream;

final class hu implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ Bitmap f3375a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ hr f3376b;

    hu(hr hrVar, Bitmap bitmap) {
        this.f3376b = hrVar;
        this.f3375a = bitmap;
    }

    public final void run() {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        this.f3375a.compress(CompressFormat.PNG, 0, byteArrayOutputStream);
        synchronized (this.f3376b.l) {
            this.f3376b.c.g = new afy();
            this.f3376b.c.g.c = byteArrayOutputStream.toByteArray();
            this.f3376b.c.g.f2550b = "image/png";
            this.f3376b.c.g.f2549a = Integer.valueOf(1);
        }
    }
}
