package com.google.android.gms.internal.ads;

final class baq implements od<azv> {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ bay f3081a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ bah f3082b;

    baq(bah bah, bay bay) {
        this.f3082b = bah;
        this.f3081a = bay;
    }

    public final /* synthetic */ void a(Object obj) {
        synchronized (this.f3082b.f3065a) {
            this.f3082b.h = 0;
            if (!(this.f3082b.g == null || this.f3081a == this.f3082b.g)) {
                jm.a("New JS engine is loaded, marking previous one as destroyable.");
                this.f3082b.g.e();
            }
            this.f3082b.g = this.f3081a;
        }
    }
}
