package com.google.android.gms.internal.ads;

import java.util.concurrent.ExecutionException;

final /* synthetic */ class nj implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final ny f3543a;

    /* renamed from: b reason: collision with root package name */
    private final nn f3544b;

    nj(ny nyVar, nn nnVar) {
        this.f3543a = nyVar;
        this.f3544b = nnVar;
    }

    public final void run() {
        ny nyVar = this.f3543a;
        try {
            nyVar.b(this.f3544b.get());
        } catch (ExecutionException e) {
            nyVar.a(e.getCause());
        } catch (InterruptedException e2) {
            Thread.currentThread().interrupt();
            nyVar.a(e2);
        } catch (Exception e3) {
            nyVar.a(e3);
        }
    }
}
