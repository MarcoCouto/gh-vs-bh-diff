package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.a.a;
import com.google.android.gms.common.internal.a.c;
import com.hmatalonga.greenhub.Config;

@cm
public final class mu extends a {
    public static final Creator<mu> CREATOR = new mv();

    /* renamed from: a reason: collision with root package name */
    public String f3528a;

    /* renamed from: b reason: collision with root package name */
    public int f3529b;
    public int c;
    public boolean d;
    public boolean e;

    public mu(int i, int i2, boolean z) {
        this(i, i2, z, false, false);
    }

    public mu(int i, int i2, boolean z, boolean z2) {
        this(12451000, i2, true, false, z2);
    }

    private mu(int i, int i2, boolean z, boolean z2, boolean z3) {
        String str = z ? Config.NOTIFICATION_DEFAULT_PRIORITY : "1";
        String sb = new StringBuilder(String.valueOf(str).length() + 36).append("afma-sdk-a-v").append(i).append(".").append(i2).append(".").append(str).toString();
        this(sb, i, i2, z, z3);
    }

    mu(String str, int i, int i2, boolean z, boolean z2) {
        this.f3528a = str;
        this.f3529b = i;
        this.c = i2;
        this.d = z;
        this.e = z2;
    }

    public static mu a() {
        return new mu(12451009, 12451009, true);
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = c.a(parcel);
        c.a(parcel, 2, this.f3528a, false);
        c.a(parcel, 3, this.f3529b);
        c.a(parcel, 4, this.c);
        c.a(parcel, 5, this.d);
        c.a(parcel, 6, this.e);
        c.a(parcel, a2);
    }
}
