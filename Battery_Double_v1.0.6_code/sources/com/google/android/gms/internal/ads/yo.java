package com.google.android.gms.internal.ads;

import java.security.GeneralSecurityException;
import java.security.KeyPair;
import java.security.interfaces.ECPrivateKey;
import java.security.interfaces.ECPublicKey;
import java.security.spec.ECParameterSpec;
import java.security.spec.ECPoint;
import java.security.spec.EllipticCurve;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

public final class yo {

    /* renamed from: a reason: collision with root package name */
    private ECPublicKey f3798a;

    public yo(ECPublicKey eCPublicKey) {
        this.f3798a = eCPublicKey;
    }

    public final yp a(String str, byte[] bArr, byte[] bArr2, int i, yt ytVar) throws GeneralSecurityException {
        byte[] bArr3;
        KeyPair a2 = yq.a(this.f3798a.getParams());
        ECPublicKey eCPublicKey = (ECPublicKey) a2.getPublic();
        ECPrivateKey eCPrivateKey = (ECPrivateKey) a2.getPrivate();
        ECPublicKey eCPublicKey2 = this.f3798a;
        ECParameterSpec params = eCPublicKey2.getParams();
        ECParameterSpec params2 = eCPrivateKey.getParams();
        if (!params.getCurve().equals(params2.getCurve()) || !params.getGenerator().equals(params2.getGenerator()) || !params.getOrder().equals(params2.getOrder()) || params.getCofactor() != params2.getCofactor()) {
            throw new GeneralSecurityException("invalid public key spec");
        }
        byte[] a3 = yq.a(eCPrivateKey, eCPublicKey2.getW());
        EllipticCurve curve = eCPublicKey.getParams().getCurve();
        ECPoint w = eCPublicKey.getW();
        yq.a(w, curve);
        int a4 = yq.a(curve);
        switch (yr.f3801a[ytVar.ordinal()]) {
            case 1:
                byte[] bArr4 = new byte[((a4 * 2) + 1)];
                byte[] byteArray = w.getAffineX().toByteArray();
                byte[] byteArray2 = w.getAffineY().toByteArray();
                System.arraycopy(byteArray2, 0, bArr4, ((a4 * 2) + 1) - byteArray2.length, byteArray2.length);
                System.arraycopy(byteArray, 0, bArr4, (a4 + 1) - byteArray.length, byteArray.length);
                bArr4[0] = 4;
                bArr3 = bArr4;
                break;
            case 2:
                bArr3 = new byte[(a4 + 1)];
                byte[] byteArray3 = w.getAffineX().toByteArray();
                System.arraycopy(byteArray3, 0, bArr3, (a4 + 1) - byteArray3.length, byteArray3.length);
                bArr3[0] = (byte) (w.getAffineY().testBit(0) ? 3 : 2);
                break;
            default:
                String valueOf = String.valueOf(ytVar);
                throw new GeneralSecurityException(new StringBuilder(String.valueOf(valueOf).length() + 15).append("invalid format:").append(valueOf).toString());
        }
        byte[] a5 = yh.a(bArr3, a3);
        Mac mac = (Mac) yv.f3810b.a(str);
        if (i > mac.getMacLength() * 255) {
            throw new GeneralSecurityException("size too large");
        }
        if (bArr == null || bArr.length == 0) {
            mac.init(new SecretKeySpec(new byte[mac.getMacLength()], str));
        } else {
            mac.init(new SecretKeySpec(bArr, str));
        }
        byte[] bArr5 = new byte[i];
        mac.init(new SecretKeySpec(mac.doFinal(a5), str));
        byte[] bArr6 = new byte[0];
        int i2 = 1;
        int i3 = 0;
        while (true) {
            mac.update(bArr6);
            mac.update(bArr2);
            mac.update((byte) i2);
            bArr6 = mac.doFinal();
            if (bArr6.length + i3 < i) {
                System.arraycopy(bArr6, 0, bArr5, i3, bArr6.length);
                i3 += bArr6.length;
                i2++;
            } else {
                System.arraycopy(bArr6, 0, bArr5, i3, i - i3);
                return new yp(bArr3, bArr5);
            }
        }
    }
}
