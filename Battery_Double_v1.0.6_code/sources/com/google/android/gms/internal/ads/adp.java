package com.google.android.gms.internal.ads;

import java.io.IOException;

interface adp<T> {
    int a(T t);

    T a();

    void a(T t, ado ado, abc abc) throws IOException;

    void a(T t, afc afc) throws IOException;

    void a(T t, byte[] bArr, int i, int i2, aae aae) throws IOException;

    boolean a(T t, T t2);

    int b(T t);

    void b(T t, T t2);

    void c(T t);

    boolean d(T t);
}
