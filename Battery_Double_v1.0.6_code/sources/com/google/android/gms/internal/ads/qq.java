package com.google.android.gms.internal.ads;

import android.view.View;

final class qq implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ View f3640a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ ic f3641b;
    private final /* synthetic */ int c;
    private final /* synthetic */ qo d;

    qq(qo qoVar, View view, ic icVar, int i) {
        this.d = qoVar;
        this.f3640a = view;
        this.f3641b = icVar;
        this.c = i;
    }

    public final void run() {
        this.d.a(this.f3640a, this.f3641b, this.c - 1);
    }
}
