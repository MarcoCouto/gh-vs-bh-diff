package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zw;
import com.google.android.gms.internal.ads.zx;
import java.io.IOException;

public abstract class zw<MessageType extends zw<MessageType, BuilderType>, BuilderType extends zx<MessageType, BuilderType>> implements acw {
    private static boolean zzdpg = false;
    protected int zzdpf = 0;

    /* access modifiers changed from: 0000 */
    public void a(int i) {
        throw new UnsupportedOperationException();
    }

    public final aah h() {
        try {
            aam b2 = aah.b(l());
            a(b2.b());
            return b2.a();
        } catch (IOException e) {
            String str = "ByteString";
            String name = getClass().getName();
            throw new RuntimeException(new StringBuilder(String.valueOf(name).length() + 62 + String.valueOf(str).length()).append("Serializing ").append(name).append(" to a ").append(str).append(" threw an IOException (should never happen).").toString(), e);
        }
    }

    public final byte[] i() {
        try {
            byte[] bArr = new byte[l()];
            aav a2 = aav.a(bArr);
            a(a2);
            a2.b();
            return bArr;
        } catch (IOException e) {
            String str = "byte array";
            String name = getClass().getName();
            throw new RuntimeException(new StringBuilder(String.valueOf(name).length() + 62 + String.valueOf(str).length()).append("Serializing ").append(name).append(" to a ").append(str).append(" threw an IOException (should never happen).").toString(), e);
        }
    }

    /* access modifiers changed from: 0000 */
    public int j() {
        throw new UnsupportedOperationException();
    }
}
