package com.google.android.gms.internal.ads;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;

public final class app extends ajk implements apn {
    app(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.client.IAdLoader");
    }

    public final String a() throws RemoteException {
        Parcel a2 = a(2, r_());
        String readString = a2.readString();
        a2.recycle();
        return readString;
    }

    public final void a(aop aop) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (Parcelable) aop);
        b(1, r_);
    }

    public final void a(aop aop, int i) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (Parcelable) aop);
        r_.writeInt(i);
        b(5, r_);
    }

    public final String b() throws RemoteException {
        Parcel a2 = a(4, r_());
        String readString = a2.readString();
        a2.recycle();
        return readString;
    }

    public final boolean c() throws RemoteException {
        Parcel a2 = a(3, r_());
        boolean a3 = ajm.a(a2);
        a2.recycle();
        return a3;
    }
}
