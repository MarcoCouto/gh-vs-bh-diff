package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.abp.b;

public final class xt extends abp<xt, a> implements acy {
    private static volatile adi<xt> zzakh;
    /* access modifiers changed from: private */
    public static final xt zzdme = new xt();
    private String zzdmd = "";

    public static final class a extends com.google.android.gms.internal.ads.abp.a<xt, a> implements acy {
        private a() {
            super(xt.zzdme);
        }

        /* synthetic */ a(xu xuVar) {
            this();
        }
    }

    static {
        abp.a(xt.class, zzdme);
    }

    private xt() {
    }

    public static xt a(aah aah) throws abv {
        return (xt) abp.a(zzdme, aah);
    }

    public static xt b() {
        return zzdme;
    }

    /* JADX WARNING: type inference failed for: r0v6, types: [com.google.android.gms.internal.ads.adi<com.google.android.gms.internal.ads.xt>] */
    /* JADX WARNING: type inference failed for: r0v7, types: [java.lang.Object] */
    /* JADX WARNING: type inference failed for: r0v8, types: [com.google.android.gms.internal.ads.abp$b, com.google.android.gms.internal.ads.adi<com.google.android.gms.internal.ads.xt>] */
    /* JADX WARNING: type inference failed for: r0v15 */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r0v8, types: [com.google.android.gms.internal.ads.abp$b, com.google.android.gms.internal.ads.adi<com.google.android.gms.internal.ads.xt>]
  assigns: [com.google.android.gms.internal.ads.abp$b]
  uses: [com.google.android.gms.internal.ads.adi<com.google.android.gms.internal.ads.xt>]
  mth insns count: 38
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
    	at jadx.core.ProcessClass.process(ProcessClass.java:35)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Unknown variable types count: 2 */
    public final Object a(int i, Object obj, Object obj2) {
        ? r0;
        switch (xu.f3780a[i - 1]) {
            case 1:
                return new xt();
            case 2:
                return new a(null);
            case 3:
                Object[] objArr = {"zzdmd"};
                return a((acw) zzdme, "\u0000\u0001\u0000\u0000\u0001\u0001\u0001\u0002\u0000\u0000\u0000\u0001Ȉ", objArr);
            case 4:
                return zzdme;
            case 5:
                adi<xt> adi = zzakh;
                if (adi != null) {
                    return adi;
                }
                synchronized (xt.class) {
                    r0 = zzakh;
                    if (r0 == 0) {
                        ? bVar = new b(zzdme);
                        zzakh = bVar;
                        r0 = bVar;
                    }
                }
                return r0;
            case 6:
                return Byte.valueOf(1);
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    public final String a() {
        return this.zzdmd;
    }
}
