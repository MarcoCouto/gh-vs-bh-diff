package com.google.android.gms.internal.ads;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.a.b;
import java.util.ArrayList;

public final class dn implements Creator<dl> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b2 = b.b(parcel);
        int i = 0;
        Bundle bundle = null;
        aop aop = null;
        aot aot = null;
        String str = null;
        ApplicationInfo applicationInfo = null;
        PackageInfo packageInfo = null;
        String str2 = null;
        String str3 = null;
        String str4 = null;
        mu muVar = null;
        Bundle bundle2 = null;
        int i2 = 0;
        ArrayList arrayList = null;
        Bundle bundle3 = null;
        boolean z = false;
        int i3 = 0;
        int i4 = 0;
        float f = 0.0f;
        String str5 = null;
        long j = 0;
        String str6 = null;
        ArrayList arrayList2 = null;
        String str7 = null;
        aul aul = null;
        ArrayList arrayList3 = null;
        long j2 = 0;
        String str8 = null;
        float f2 = 0.0f;
        boolean z2 = false;
        int i5 = 0;
        int i6 = 0;
        boolean z3 = false;
        boolean z4 = false;
        String str9 = null;
        String str10 = null;
        boolean z5 = false;
        int i7 = 0;
        Bundle bundle4 = null;
        String str11 = null;
        aqy aqy = null;
        boolean z6 = false;
        Bundle bundle5 = null;
        String str12 = null;
        String str13 = null;
        String str14 = null;
        boolean z7 = false;
        ArrayList arrayList4 = null;
        String str15 = null;
        ArrayList arrayList5 = null;
        int i8 = 0;
        boolean z8 = false;
        boolean z9 = false;
        boolean z10 = false;
        ArrayList arrayList6 = null;
        while (parcel.dataPosition() < b2) {
            int a2 = b.a(parcel);
            switch (b.a(a2)) {
                case 1:
                    i = b.d(parcel, a2);
                    break;
                case 2:
                    bundle = b.m(parcel, a2);
                    break;
                case 3:
                    aop = (aop) b.a(parcel, a2, aop.CREATOR);
                    break;
                case 4:
                    aot = (aot) b.a(parcel, a2, aot.CREATOR);
                    break;
                case 5:
                    str = b.k(parcel, a2);
                    break;
                case 6:
                    applicationInfo = (ApplicationInfo) b.a(parcel, a2, ApplicationInfo.CREATOR);
                    break;
                case 7:
                    packageInfo = (PackageInfo) b.a(parcel, a2, PackageInfo.CREATOR);
                    break;
                case 8:
                    str2 = b.k(parcel, a2);
                    break;
                case 9:
                    str3 = b.k(parcel, a2);
                    break;
                case 10:
                    str4 = b.k(parcel, a2);
                    break;
                case 11:
                    muVar = (mu) b.a(parcel, a2, mu.CREATOR);
                    break;
                case 12:
                    bundle2 = b.m(parcel, a2);
                    break;
                case 13:
                    i2 = b.d(parcel, a2);
                    break;
                case 14:
                    arrayList = b.q(parcel, a2);
                    break;
                case 15:
                    bundle3 = b.m(parcel, a2);
                    break;
                case 16:
                    z = b.c(parcel, a2);
                    break;
                case 18:
                    i3 = b.d(parcel, a2);
                    break;
                case 19:
                    i4 = b.d(parcel, a2);
                    break;
                case 20:
                    f = b.h(parcel, a2);
                    break;
                case 21:
                    str5 = b.k(parcel, a2);
                    break;
                case 25:
                    j = b.f(parcel, a2);
                    break;
                case 26:
                    str6 = b.k(parcel, a2);
                    break;
                case 27:
                    arrayList2 = b.q(parcel, a2);
                    break;
                case 28:
                    str7 = b.k(parcel, a2);
                    break;
                case 29:
                    aul = (aul) b.a(parcel, a2, aul.CREATOR);
                    break;
                case 30:
                    arrayList3 = b.q(parcel, a2);
                    break;
                case 31:
                    j2 = b.f(parcel, a2);
                    break;
                case 33:
                    str8 = b.k(parcel, a2);
                    break;
                case 34:
                    f2 = b.h(parcel, a2);
                    break;
                case 35:
                    i5 = b.d(parcel, a2);
                    break;
                case 36:
                    i6 = b.d(parcel, a2);
                    break;
                case 37:
                    z3 = b.c(parcel, a2);
                    break;
                case 38:
                    z4 = b.c(parcel, a2);
                    break;
                case 39:
                    str9 = b.k(parcel, a2);
                    break;
                case 40:
                    z2 = b.c(parcel, a2);
                    break;
                case 41:
                    str10 = b.k(parcel, a2);
                    break;
                case 42:
                    z5 = b.c(parcel, a2);
                    break;
                case 43:
                    i7 = b.d(parcel, a2);
                    break;
                case 44:
                    bundle4 = b.m(parcel, a2);
                    break;
                case 45:
                    str11 = b.k(parcel, a2);
                    break;
                case 46:
                    aqy = (aqy) b.a(parcel, a2, aqy.CREATOR);
                    break;
                case 47:
                    z6 = b.c(parcel, a2);
                    break;
                case 48:
                    bundle5 = b.m(parcel, a2);
                    break;
                case 49:
                    str12 = b.k(parcel, a2);
                    break;
                case 50:
                    str13 = b.k(parcel, a2);
                    break;
                case 51:
                    str14 = b.k(parcel, a2);
                    break;
                case 52:
                    z7 = b.c(parcel, a2);
                    break;
                case 53:
                    arrayList4 = b.p(parcel, a2);
                    break;
                case 54:
                    str15 = b.k(parcel, a2);
                    break;
                case 55:
                    arrayList5 = b.q(parcel, a2);
                    break;
                case 56:
                    i8 = b.d(parcel, a2);
                    break;
                case 57:
                    z8 = b.c(parcel, a2);
                    break;
                case 58:
                    z9 = b.c(parcel, a2);
                    break;
                case 59:
                    z10 = b.c(parcel, a2);
                    break;
                case 60:
                    arrayList6 = b.q(parcel, a2);
                    break;
                default:
                    b.b(parcel, a2);
                    break;
            }
        }
        b.r(parcel, b2);
        return new dl(i, bundle, aop, aot, str, applicationInfo, packageInfo, str2, str3, str4, muVar, bundle2, i2, arrayList, bundle3, z, i3, i4, f, str5, j, str6, arrayList2, str7, aul, arrayList3, j2, str8, f2, z2, i5, i6, z3, z4, str9, str10, z5, i7, bundle4, str11, aqy, z6, bundle5, str12, str13, str14, z7, arrayList4, str15, arrayList5, i8, z8, z9, z10, arrayList6);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new dl[i];
    }
}
