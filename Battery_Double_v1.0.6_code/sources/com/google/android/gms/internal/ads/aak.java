package com.google.android.gms.internal.ads;

final class aak extends aao {
    private final int c;
    private final int d;

    aak(byte[] bArr, int i, int i2) {
        super(bArr);
        b(i, i + i2, bArr.length);
        this.c = i;
        this.d = i2;
    }

    public final byte a(int i) {
        int a2 = a();
        if (((a2 - (i + 1)) | i) >= 0) {
            return this.f2399b[this.c + i];
        }
        if (i < 0) {
            throw new ArrayIndexOutOfBoundsException("Index < 0: " + i);
        }
        throw new ArrayIndexOutOfBoundsException("Index > length: " + i + ", " + a2);
    }

    public final int a() {
        return this.d;
    }

    /* access modifiers changed from: protected */
    public final void a(byte[] bArr, int i, int i2, int i3) {
        System.arraycopy(this.f2399b, g(), bArr, 0, i3);
    }

    /* access modifiers changed from: protected */
    public final int g() {
        return this.c;
    }
}
