package com.google.android.gms.internal.ads;

public final class an extends Exception {

    /* renamed from: a reason: collision with root package name */
    private final int f2744a;

    public an(String str, int i) {
        super(str);
        this.f2744a = i;
    }

    public final int a() {
        return this.f2744a;
    }
}
