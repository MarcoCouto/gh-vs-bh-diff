package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.ads.internal.ax;
import com.google.android.gms.ads.internal.bu;

final /* synthetic */ class qv implements mx {

    /* renamed from: a reason: collision with root package name */
    private final Context f3646a;

    /* renamed from: b reason: collision with root package name */
    private final ahh f3647b;
    private final mu c;
    private final bu d;
    private final String e;

    qv(Context context, ahh ahh, mu muVar, bu buVar, String str) {
        this.f3646a = context;
        this.f3647b = ahh;
        this.c = muVar;
        this.d = buVar;
        this.e = str;
    }

    public final nn a(Object obj) {
        Context context = this.f3646a;
        ahh ahh = this.f3647b;
        mu muVar = this.c;
        bu buVar = this.d;
        String str = this.e;
        ax.f();
        qn a2 = qu.a(context, sb.a(), "", false, false, ahh, muVar, null, null, buVar, amw.a());
        nx a3 = nx.a(a2);
        a2.v().a((rw) new qx(a3));
        a2.loadUrl(str);
        return a3;
    }
}
