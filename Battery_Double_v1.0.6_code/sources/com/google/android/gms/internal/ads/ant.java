package com.google.android.gms.internal.ads;

import java.io.IOException;

public final class ant extends afh<ant> {

    /* renamed from: a reason: collision with root package name */
    private Integer f2776a;

    /* renamed from: b reason: collision with root package name */
    private Integer f2777b;
    private Integer c;
    private Integer d;
    private Integer e;
    private Integer f;
    private Integer g;
    private Integer h;
    private Integer i;
    private Integer j;
    private anu k;

    public ant() {
        this.f2776a = null;
        this.f2777b = null;
        this.c = null;
        this.d = null;
        this.e = null;
        this.f = null;
        this.g = null;
        this.h = null;
        this.i = null;
        this.j = null;
        this.k = null;
        this.Y = null;
        this.Z = -1;
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public final ant a(afd afd) throws IOException {
        while (true) {
            int a2 = afd.a();
            switch (a2) {
                case 0:
                    break;
                case 8:
                    int j2 = afd.j();
                    try {
                        this.f2776a = Integer.valueOf(anf.a(afd.g()));
                        continue;
                    } catch (IllegalArgumentException e2) {
                        afd.e(j2);
                        a(afd, a2);
                        break;
                    }
                case 16:
                    int j3 = afd.j();
                    try {
                        this.f2777b = Integer.valueOf(anf.a(afd.g()));
                        continue;
                    } catch (IllegalArgumentException e3) {
                        afd.e(j3);
                        a(afd, a2);
                        break;
                    }
                case 24:
                    this.c = Integer.valueOf(afd.g());
                    continue;
                case 32:
                    this.d = Integer.valueOf(afd.g());
                    continue;
                case 40:
                    this.e = Integer.valueOf(afd.g());
                    continue;
                case 48:
                    this.f = Integer.valueOf(afd.g());
                    continue;
                case 56:
                    this.g = Integer.valueOf(afd.g());
                    continue;
                case 64:
                    this.h = Integer.valueOf(afd.g());
                    continue;
                case 72:
                    this.i = Integer.valueOf(afd.g());
                    continue;
                case 80:
                    this.j = Integer.valueOf(afd.g());
                    continue;
                case 90:
                    if (this.k == null) {
                        this.k = new anu();
                    }
                    afd.a((afn) this.k);
                    continue;
                default:
                    if (!super.a(afd, a2)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    /* access modifiers changed from: protected */
    public final int a() {
        int a2 = super.a();
        if (this.f2776a != null) {
            a2 += aff.b(1, this.f2776a.intValue());
        }
        if (this.f2777b != null) {
            a2 += aff.b(2, this.f2777b.intValue());
        }
        if (this.c != null) {
            a2 += aff.b(3, this.c.intValue());
        }
        if (this.d != null) {
            a2 += aff.b(4, this.d.intValue());
        }
        if (this.e != null) {
            a2 += aff.b(5, this.e.intValue());
        }
        if (this.f != null) {
            a2 += aff.b(6, this.f.intValue());
        }
        if (this.g != null) {
            a2 += aff.b(7, this.g.intValue());
        }
        if (this.h != null) {
            a2 += aff.b(8, this.h.intValue());
        }
        if (this.i != null) {
            a2 += aff.b(9, this.i.intValue());
        }
        if (this.j != null) {
            a2 += aff.b(10, this.j.intValue());
        }
        return this.k != null ? a2 + aff.b(11, (afn) this.k) : a2;
    }

    public final void a(aff aff) throws IOException {
        if (this.f2776a != null) {
            aff.a(1, this.f2776a.intValue());
        }
        if (this.f2777b != null) {
            aff.a(2, this.f2777b.intValue());
        }
        if (this.c != null) {
            aff.a(3, this.c.intValue());
        }
        if (this.d != null) {
            aff.a(4, this.d.intValue());
        }
        if (this.e != null) {
            aff.a(5, this.e.intValue());
        }
        if (this.f != null) {
            aff.a(6, this.f.intValue());
        }
        if (this.g != null) {
            aff.a(7, this.g.intValue());
        }
        if (this.h != null) {
            aff.a(8, this.h.intValue());
        }
        if (this.i != null) {
            aff.a(9, this.i.intValue());
        }
        if (this.j != null) {
            aff.a(10, this.j.intValue());
        }
        if (this.k != null) {
            aff.a(11, (afn) this.k);
        }
        super.a(aff);
    }
}
