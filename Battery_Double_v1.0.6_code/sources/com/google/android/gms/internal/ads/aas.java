package com.google.android.gms.internal.ads;

import java.io.IOException;
import java.util.Arrays;

final class aas extends aaq {
    private final byte[] d;
    private final boolean e;
    private int f;
    private int g;
    private int h;
    private int i;
    private int j;
    private int k;

    private aas(byte[] bArr, int i2, int i3, boolean z) {
        super();
        this.k = Integer.MAX_VALUE;
        this.d = bArr;
        this.f = i2 + i3;
        this.h = i2;
        this.i = this.h;
        this.e = z;
    }

    private final byte A() throws IOException {
        if (this.h == this.f) {
            throw abv.a();
        }
        byte[] bArr = this.d;
        int i2 = this.h;
        this.h = i2 + 1;
        return bArr[i2];
    }

    /* JADX WARNING: Code restructure failed: missing block: B:28:0x006a, code lost:
        if (r3[r2] < 0) goto L_0x006c;
     */
    private final int v() throws IOException {
        byte b2;
        int i2 = this.h;
        if (this.f != i2) {
            byte[] bArr = this.d;
            int i3 = i2 + 1;
            byte b3 = bArr[i2];
            if (b3 >= 0) {
                this.h = i3;
                return b3;
            } else if (this.f - i3 >= 9) {
                int i4 = i3 + 1;
                byte b4 = b3 ^ (bArr[i3] << 7);
                if (b4 < 0) {
                    b2 = b4 ^ Byte.MIN_VALUE;
                } else {
                    int i5 = i4 + 1;
                    byte b5 = b4 ^ (bArr[i4] << 14);
                    if (b5 >= 0) {
                        b2 = b5 ^ 16256;
                        i4 = i5;
                    } else {
                        i4 = i5 + 1;
                        byte b6 = b5 ^ (bArr[i5] << 21);
                        if (b6 < 0) {
                            b2 = b6 ^ -2080896;
                        } else {
                            int i6 = i4 + 1;
                            byte b7 = bArr[i4];
                            b2 = (b6 ^ (b7 << 28)) ^ 266354560;
                            if (b7 < 0) {
                                i4 = i6 + 1;
                                if (bArr[i6] < 0) {
                                    i6 = i4 + 1;
                                    if (bArr[i4] < 0) {
                                        i4 = i6 + 1;
                                        if (bArr[i6] < 0) {
                                            i6 = i4 + 1;
                                            if (bArr[i4] < 0) {
                                                i4 = i6 + 1;
                                            }
                                        }
                                    }
                                }
                            }
                            i4 = i6;
                        }
                    }
                }
                this.h = i4;
                return b2;
            }
        }
        return (int) s();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00b2, code lost:
        if (((long) r4[r3]) < 0) goto L_0x00b4;
     */
    private final long w() throws IOException {
        long j2;
        int i2 = this.h;
        if (this.f != i2) {
            byte[] bArr = this.d;
            int i3 = i2 + 1;
            byte b2 = bArr[i2];
            if (b2 >= 0) {
                this.h = i3;
                return (long) b2;
            } else if (this.f - i3 >= 9) {
                int i4 = i3 + 1;
                byte b3 = b2 ^ (bArr[i3] << 7);
                if (b3 < 0) {
                    j2 = (long) (b3 ^ Byte.MIN_VALUE);
                } else {
                    int i5 = i4 + 1;
                    byte b4 = b3 ^ (bArr[i4] << 14);
                    if (b4 >= 0) {
                        j2 = (long) (b4 ^ 16256);
                        i4 = i5;
                    } else {
                        i4 = i5 + 1;
                        byte b5 = b4 ^ (bArr[i5] << 21);
                        if (b5 < 0) {
                            j2 = (long) (b5 ^ -2080896);
                        } else {
                            int i6 = i4 + 1;
                            long j3 = ((long) b5) ^ (((long) bArr[i4]) << 28);
                            if (j3 >= 0) {
                                j2 = j3 ^ 266354560;
                                i4 = i6;
                            } else {
                                i4 = i6 + 1;
                                long j4 = j3 ^ (((long) bArr[i6]) << 35);
                                if (j4 < 0) {
                                    j2 = j4 ^ -34093383808L;
                                } else {
                                    int i7 = i4 + 1;
                                    long j5 = j4 ^ (((long) bArr[i4]) << 42);
                                    if (j5 >= 0) {
                                        j2 = j5 ^ 4363953127296L;
                                        i4 = i7;
                                    } else {
                                        i4 = i7 + 1;
                                        long j6 = j5 ^ (((long) bArr[i7]) << 49);
                                        if (j6 < 0) {
                                            j2 = j6 ^ -558586000294016L;
                                        } else {
                                            int i8 = i4 + 1;
                                            j2 = (j6 ^ (((long) bArr[i4]) << 56)) ^ 71499008037633920L;
                                            i4 = j2 < 0 ? i8 + 1 : i8;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                this.h = i4;
                return j2;
            }
        }
        return s();
    }

    private final int x() throws IOException {
        int i2 = this.h;
        if (this.f - i2 < 4) {
            throw abv.a();
        }
        byte[] bArr = this.d;
        this.h = i2 + 4;
        return ((bArr[i2 + 3] & 255) << 24) | (bArr[i2] & 255) | ((bArr[i2 + 1] & 255) << 8) | ((bArr[i2 + 2] & 255) << 16);
    }

    private final long y() throws IOException {
        int i2 = this.h;
        if (this.f - i2 < 8) {
            throw abv.a();
        }
        byte[] bArr = this.d;
        this.h = i2 + 8;
        return ((((long) bArr[i2 + 7]) & 255) << 56) | (((long) bArr[i2]) & 255) | ((((long) bArr[i2 + 1]) & 255) << 8) | ((((long) bArr[i2 + 2]) & 255) << 16) | ((((long) bArr[i2 + 3]) & 255) << 24) | ((((long) bArr[i2 + 4]) & 255) << 32) | ((((long) bArr[i2 + 5]) & 255) << 40) | ((((long) bArr[i2 + 6]) & 255) << 48);
    }

    private final void z() {
        this.f += this.g;
        int i2 = this.f - this.i;
        if (i2 > this.k) {
            this.g = i2 - this.k;
            this.f -= this.g;
            return;
        }
        this.g = 0;
    }

    public final int a() throws IOException {
        if (t()) {
            this.j = 0;
            return 0;
        }
        this.j = v();
        if ((this.j >>> 3) != 0) {
            return this.j;
        }
        throw abv.d();
    }

    public final void a(int i2) throws abv {
        if (this.j != i2) {
            throw abv.e();
        }
    }

    public final double b() throws IOException {
        return Double.longBitsToDouble(y());
    }

    public final boolean b(int i2) throws IOException {
        int a2;
        int i3 = 0;
        switch (i2 & 7) {
            case 0:
                if (this.f - this.h >= 10) {
                    while (i3 < 10) {
                        byte[] bArr = this.d;
                        int i4 = this.h;
                        this.h = i4 + 1;
                        if (bArr[i4] >= 0) {
                            return true;
                        }
                        i3++;
                    }
                    throw abv.c();
                }
                while (i3 < 10) {
                    if (A() >= 0) {
                        return true;
                    }
                    i3++;
                }
                throw abv.c();
            case 1:
                e(8);
                return true;
            case 2:
                e(v());
                return true;
            case 3:
                break;
            case 4:
                return false;
            case 5:
                e(4);
                return true;
            default:
                throw abv.f();
        }
        do {
            a2 = a();
            if (a2 != 0) {
            }
            a(((i2 >>> 3) << 3) | 4);
            return true;
        } while (b(a2));
        a(((i2 >>> 3) << 3) | 4);
        return true;
    }

    public final float c() throws IOException {
        return Float.intBitsToFloat(x());
    }

    public final int c(int i2) throws abv {
        if (i2 < 0) {
            throw abv.b();
        }
        int u = u() + i2;
        int i3 = this.k;
        if (u > i3) {
            throw abv.a();
        }
        this.k = u;
        z();
        return i3;
    }

    public final long d() throws IOException {
        return w();
    }

    public final void d(int i2) {
        this.k = i2;
        z();
    }

    public final long e() throws IOException {
        return w();
    }

    public final void e(int i2) throws IOException {
        if (i2 >= 0 && i2 <= this.f - this.h) {
            this.h += i2;
        } else if (i2 < 0) {
            throw abv.b();
        } else {
            throw abv.a();
        }
    }

    public final int f() throws IOException {
        return v();
    }

    public final long g() throws IOException {
        return y();
    }

    public final int h() throws IOException {
        return x();
    }

    public final boolean i() throws IOException {
        return w() != 0;
    }

    public final String j() throws IOException {
        int v = v();
        if (v > 0 && v <= this.f - this.h) {
            String str = new String(this.d, this.h, v, abr.f2440a);
            this.h = v + this.h;
            return str;
        } else if (v == 0) {
            return "";
        } else {
            if (v < 0) {
                throw abv.b();
            }
            throw abv.a();
        }
    }

    public final String k() throws IOException {
        int v = v();
        if (v <= 0 || v > this.f - this.h) {
            if (v == 0) {
                return "";
            }
            if (v <= 0) {
                throw abv.b();
            }
            throw abv.a();
        } else if (!aeq.a(this.d, this.h, this.h + v)) {
            throw abv.h();
        } else {
            int i2 = this.h;
            this.h += v;
            return new String(this.d, i2, v, abr.f2440a);
        }
    }

    public final aah l() throws IOException {
        byte[] bArr;
        int v = v();
        if (v > 0 && v <= this.f - this.h) {
            aah a2 = aah.a(this.d, this.h, v);
            this.h = v + this.h;
            return a2;
        } else if (v == 0) {
            return aah.f2393a;
        } else {
            if (v > 0 && v <= this.f - this.h) {
                int i2 = this.h;
                this.h = v + this.h;
                bArr = Arrays.copyOfRange(this.d, i2, this.h);
            } else if (v > 0) {
                throw abv.a();
            } else if (v == 0) {
                bArr = abr.f2441b;
            } else {
                throw abv.b();
            }
            return aah.b(bArr);
        }
    }

    public final int m() throws IOException {
        return v();
    }

    public final int n() throws IOException {
        return v();
    }

    public final int o() throws IOException {
        return x();
    }

    public final long p() throws IOException {
        return y();
    }

    public final int q() throws IOException {
        return f(v());
    }

    public final long r() throws IOException {
        return a(w());
    }

    /* access modifiers changed from: 0000 */
    public final long s() throws IOException {
        long j2 = 0;
        for (int i2 = 0; i2 < 64; i2 += 7) {
            byte A = A();
            j2 |= ((long) (A & Byte.MAX_VALUE)) << i2;
            if ((A & 128) == 0) {
                return j2;
            }
        }
        throw abv.c();
    }

    public final boolean t() throws IOException {
        return this.h == this.f;
    }

    public final int u() {
        return this.h - this.i;
    }
}
