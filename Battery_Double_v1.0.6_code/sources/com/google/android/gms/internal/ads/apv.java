package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.b.a;

public interface apv extends IInterface {
    String D() throws RemoteException;

    aqe E() throws RemoteException;

    apk F() throws RemoteException;

    void I() throws RemoteException;

    String a() throws RemoteException;

    void a(af afVar, String str) throws RemoteException;

    void a(aot aot) throws RemoteException;

    void a(aph aph) throws RemoteException;

    void a(apk apk) throws RemoteException;

    void a(aqa aqa) throws RemoteException;

    void a(aqe aqe) throws RemoteException;

    void a(aqk aqk) throws RemoteException;

    void a(aqy aqy) throws RemoteException;

    void a(arr arr) throws RemoteException;

    void a(atc atc) throws RemoteException;

    void a(gn gnVar) throws RemoteException;

    void a(y yVar) throws RemoteException;

    void a(String str) throws RemoteException;

    void b(boolean z) throws RemoteException;

    boolean b(aop aop) throws RemoteException;

    void c(boolean z) throws RemoteException;

    void j() throws RemoteException;

    a k() throws RemoteException;

    aot l() throws RemoteException;

    boolean m() throws RemoteException;

    void n() throws RemoteException;

    void o() throws RemoteException;

    void p() throws RemoteException;

    Bundle q() throws RemoteException;

    String q_() throws RemoteException;

    void r() throws RemoteException;

    boolean s() throws RemoteException;

    aqs t() throws RemoteException;
}
