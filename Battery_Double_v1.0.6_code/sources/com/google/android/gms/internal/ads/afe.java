package com.google.android.gms.internal.ads;

import java.io.IOException;

public final class afe extends afh<afe> {

    /* renamed from: a reason: collision with root package name */
    public Long f2522a;

    /* renamed from: b reason: collision with root package name */
    private String f2523b;
    private byte[] c;

    public afe() {
        this.f2522a = null;
        this.f2523b = null;
        this.c = null;
        this.Z = -1;
    }

    /* access modifiers changed from: protected */
    public final int a() {
        int a2 = super.a();
        if (this.f2522a != null) {
            a2 += aff.d(1, this.f2522a.longValue());
        }
        if (this.f2523b != null) {
            a2 += aff.b(3, this.f2523b);
        }
        return this.c != null ? a2 + aff.b(4, this.c) : a2;
    }

    public final /* synthetic */ afn a(afd afd) throws IOException {
        while (true) {
            int a2 = afd.a();
            switch (a2) {
                case 0:
                    break;
                case 8:
                    this.f2522a = Long.valueOf(afd.h());
                    continue;
                case 26:
                    this.f2523b = afd.e();
                    continue;
                case 34:
                    this.c = afd.f();
                    continue;
                default:
                    if (!super.a(afd, a2)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    public final void a(aff aff) throws IOException {
        if (this.f2522a != null) {
            aff.b(1, this.f2522a.longValue());
        }
        if (this.f2523b != null) {
            aff.a(3, this.f2523b);
        }
        if (this.c != null) {
            aff.a(4, this.c);
        }
        super.a(aff);
    }
}
