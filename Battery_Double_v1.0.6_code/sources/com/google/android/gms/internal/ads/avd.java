package com.google.android.gms.internal.ads;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.b.a;
import com.google.android.gms.b.a.C0046a;

public final class avd extends ajk implements avb {
    avd(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.formats.client.INativeAdViewDelegate");
    }

    public final a a(String str) throws RemoteException {
        Parcel r_ = r_();
        r_.writeString(str);
        Parcel a2 = a(2, r_);
        a a3 = C0046a.a(a2.readStrongBinder());
        a2.recycle();
        return a3;
    }

    public final void a() throws RemoteException {
        b(4, r_());
    }

    public final void a(a aVar) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (IInterface) aVar);
        b(3, r_);
    }

    public final void a(a aVar, int i) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (IInterface) aVar);
        r_.writeInt(i);
        b(5, r_);
    }

    public final void a(String str, a aVar) throws RemoteException {
        Parcel r_ = r_();
        r_.writeString(str);
        ajm.a(r_, (IInterface) aVar);
        b(1, r_);
    }

    public final void b(a aVar) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (IInterface) aVar);
        b(6, r_);
    }
}
