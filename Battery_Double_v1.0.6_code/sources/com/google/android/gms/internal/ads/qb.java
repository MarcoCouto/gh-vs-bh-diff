package com.google.android.gms.internal.ads;

import android.text.TextUtils;
import java.util.HashMap;
import java.util.Map;

final class qb implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ String f3625a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ String f3626b;
    private final /* synthetic */ String c;
    private final /* synthetic */ String d;
    private final /* synthetic */ py e;

    qb(py pyVar, String str, String str2, String str3, String str4) {
        this.e = pyVar;
        this.f3625a = str;
        this.f3626b = str2;
        this.c = str3;
        this.d = str4;
    }

    public final void run() {
        HashMap hashMap = new HashMap();
        hashMap.put("event", "precacheCanceled");
        hashMap.put("src", this.f3625a);
        if (!TextUtils.isEmpty(this.f3626b)) {
            hashMap.put("cachedSrc", this.f3626b);
        }
        hashMap.put("type", py.b(this.c));
        hashMap.put("reason", this.c);
        if (!TextUtils.isEmpty(this.d)) {
            hashMap.put("message", this.d);
        }
        this.e.a("onPrecacheEvent", (Map<String, String>) hashMap);
    }
}
