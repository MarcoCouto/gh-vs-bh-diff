package com.google.android.gms.internal.ads;

final /* synthetic */ class azk implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final azg f3037a;

    /* renamed from: b reason: collision with root package name */
    private final String f3038b;

    azk(azg azg, String str) {
        this.f3037a = azg;
        this.f3038b = str;
    }

    public final void run() {
        this.f3037a.f(this.f3038b);
    }
}
