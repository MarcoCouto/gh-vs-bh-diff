package com.google.android.gms.internal.ads;

final class gw implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ aop f3348a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ bcu f3349b;
    private final /* synthetic */ gv c;

    gw(gv gvVar, aop aop, bcu bcu) {
        this.c = gvVar;
        this.f3348a = aop;
        this.f3349b = bcu;
    }

    public final void run() {
        this.c.a(this.f3348a, this.f3349b);
    }
}
