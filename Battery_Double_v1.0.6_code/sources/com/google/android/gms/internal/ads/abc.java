package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.abp.d;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public final class abc {

    /* renamed from: a reason: collision with root package name */
    static final abc f2414a = new abc(true);

    /* renamed from: b reason: collision with root package name */
    private static volatile boolean f2415b = false;
    private static final Class<?> c = b();
    private final Map<abd, d<?, ?>> d;

    abc() {
        this.d = new HashMap();
    }

    private abc(boolean z) {
        this.d = Collections.emptyMap();
    }

    public static abc a() {
        return abb.a();
    }

    private static Class<?> b() {
        try {
            return Class.forName("com.google.protobuf.Extension");
        } catch (ClassNotFoundException e) {
            return null;
        }
    }

    public final <ContainingType extends acw> d<ContainingType, ?> a(ContainingType containingtype, int i) {
        return (d) this.d.get(new abd(containingtype, i));
    }
}
