package com.google.android.gms.internal.ads;

import android.location.Location;
import com.google.android.gms.ads.mediation.a;
import java.util.Date;
import java.util.Set;

@cm
public final class bdo implements a {

    /* renamed from: a reason: collision with root package name */
    private final Date f3147a;

    /* renamed from: b reason: collision with root package name */
    private final int f3148b;
    private final Set<String> c;
    private final boolean d;
    private final Location e;
    private final int f;
    private final boolean g;

    public bdo(Date date, int i, Set<String> set, Location location, boolean z, int i2, boolean z2) {
        this.f3147a = date;
        this.f3148b = i;
        this.c = set;
        this.e = location;
        this.d = z;
        this.f = i2;
        this.g = z2;
    }

    public final Date a() {
        return this.f3147a;
    }

    public final int b() {
        return this.f3148b;
    }

    public final Set<String> c() {
        return this.c;
    }

    public final Location d() {
        return this.e;
    }

    public final int e() {
        return this.f;
    }

    public final boolean f() {
        return this.d;
    }

    public final boolean g() {
        return this.g;
    }
}
