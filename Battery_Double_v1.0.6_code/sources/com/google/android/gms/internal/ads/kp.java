package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.net.Uri.Builder;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ViewConfiguration;
import android.view.WindowManager.BadTokenException;
import com.google.android.gms.ads.internal.ax;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@cm
public final class kp {

    /* renamed from: a reason: collision with root package name */
    private final Context f3453a;

    /* renamed from: b reason: collision with root package name */
    private String f3454b;
    private String c;
    private String d;
    private String e;
    private final float f;
    private float g;
    private float h;
    private float i;
    private int j;
    private int k;
    private float l;
    private float m;
    private float n;
    private float o;
    private Handler p;
    private Runnable q;

    public kp(Context context) {
        this.j = 0;
        this.q = new kq(this);
        this.f3453a = context;
        this.f = context.getResources().getDisplayMetrics().density;
        this.k = ViewConfiguration.get(this.f3453a).getScaledTouchSlop();
        ax.t().a();
        this.p = ax.t().b();
    }

    public kp(Context context, String str) {
        this(context);
        this.f3454b = str;
    }

    private static int a(List<String> list, String str, boolean z) {
        if (!z) {
            return -1;
        }
        list.add(str);
        return list.size() - 1;
    }

    private final void a(int i2, float f2, float f3) {
        if (i2 == 0) {
            this.j = 0;
            this.g = f2;
            this.h = f3;
            this.i = f3;
        } else if (this.j == -1) {
        } else {
            if (i2 == 2) {
                if (f3 > this.h) {
                    this.h = f3;
                } else if (f3 < this.i) {
                    this.i = f3;
                }
                if (this.h - this.i > 30.0f * this.f) {
                    this.j = -1;
                    return;
                }
                if (this.j == 0 || this.j == 2) {
                    if (f2 - this.g >= 50.0f * this.f) {
                        this.g = f2;
                        this.j++;
                    }
                } else if ((this.j == 1 || this.j == 3) && f2 - this.g <= -50.0f * this.f) {
                    this.g = f2;
                    this.j++;
                }
                if (this.j == 1 || this.j == 3) {
                    if (f2 > this.g) {
                        this.g = f2;
                    }
                } else if (this.j == 2 && f2 < this.g) {
                    this.g = f2;
                }
            } else if (i2 == 1 && this.j == 4) {
                a();
            }
        }
    }

    private final boolean a(float f2, float f3, float f4, float f5) {
        return Math.abs(this.l - f2) < ((float) this.k) && Math.abs(this.m - f3) < ((float) this.k) && Math.abs(this.n - f4) < ((float) this.k) && Math.abs(this.o - f5) < ((float) this.k);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x006f, code lost:
        if (android.text.TextUtils.isEmpty(r0) == false) goto L_0x0071;
     */
    private final void e() {
        String str;
        if (!(this.f3453a instanceof Activity)) {
            jm.d("Can not create dialog without Activity Context");
            return;
        }
        String str2 = this.f3454b;
        if (!TextUtils.isEmpty(str2)) {
            Uri build = new Builder().encodedQuery(str2.replaceAll("\\+", "%20")).build();
            StringBuilder sb = new StringBuilder();
            ax.e();
            Map a2 = jv.a(build);
            for (String str3 : a2.keySet()) {
                sb.append(str3).append(" = ").append((String) a2.get(str3)).append("\n\n");
            }
            str = sb.toString().trim();
        }
        str = "No debug information";
        AlertDialog.Builder builder = new AlertDialog.Builder(this.f3453a);
        builder.setMessage(str);
        builder.setTitle("Ad Information");
        builder.setPositiveButton("Share", new ks(this, str));
        builder.setNegativeButton("Close", kt.f3460a);
        builder.create().show();
    }

    public final void a() {
        try {
            if (!((Boolean) ape.f().a(asi.cP)).booleanValue()) {
                if (!((Boolean) ape.f().a(asi.cO)).booleanValue()) {
                    e();
                    return;
                }
            }
            if (!(this.f3453a instanceof Activity)) {
                jm.d("Can not create dialog without Activity Context");
                return;
            }
            String str = !TextUtils.isEmpty(ax.o().a()) ? "Creative Preview (Enabled)" : "Creative Preview";
            String str2 = ax.o().b() ? "Troubleshooting (Enabled)" : "Troubleshooting";
            ArrayList arrayList = new ArrayList();
            int a2 = a((List<String>) arrayList, "Ad Information", true);
            int a3 = a((List<String>) arrayList, str, ((Boolean) ape.f().a(asi.cO)).booleanValue());
            int a4 = a((List<String>) arrayList, str2, ((Boolean) ape.f().a(asi.cP)).booleanValue());
            AlertDialog.Builder builder = new AlertDialog.Builder(this.f3453a, ax.g().f());
            builder.setTitle("Select a Debug Mode").setItems((CharSequence[]) arrayList.toArray(new String[0]), new kr(this, a2, a3, a4));
            builder.create().show();
        } catch (BadTokenException e2) {
            String str3 = "";
            if (jm.a()) {
                Log.v("Ads", str3, e2);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ void a(int i2, int i3, int i4, DialogInterface dialogInterface, int i5) {
        if (i5 == i2) {
            e();
            return;
        }
        if (i5 == i3) {
            if (((Boolean) ape.f().a(asi.cO)).booleanValue()) {
                jm.b("Debug mode [Creative Preview] selected.");
                jt.a((Runnable) new ku(this));
                return;
            }
        }
        if (i5 == i4) {
            if (((Boolean) ape.f().a(asi.cP)).booleanValue()) {
                jm.b("Debug mode [Troubleshooting] selected.");
                jt.a((Runnable) new kv(this));
            }
        }
    }

    public final void a(MotionEvent motionEvent) {
        boolean z = true;
        if (((Boolean) ape.f().a(asi.cQ)).booleanValue()) {
            int actionMasked = motionEvent.getActionMasked();
            int historySize = motionEvent.getHistorySize();
            int pointerCount = motionEvent.getPointerCount();
            if (actionMasked == 0) {
                this.j = 0;
                this.l = motionEvent.getX();
                this.m = motionEvent.getY();
            } else if (this.j == -1) {
            } else {
                if (this.j == 0 && actionMasked == 5) {
                    this.j = 5;
                    this.n = motionEvent.getX(1);
                    this.o = motionEvent.getY(1);
                    this.p.postDelayed(this.q, ((Long) ape.f().a(asi.cR)).longValue());
                } else if (this.j == 5) {
                    if (pointerCount == 2) {
                        if (actionMasked == 2) {
                            boolean z2 = false;
                            for (int i2 = 0; i2 < historySize; i2++) {
                                if (!a(motionEvent.getHistoricalX(0, i2), motionEvent.getHistoricalY(0, i2), motionEvent.getHistoricalX(1, i2), motionEvent.getHistoricalY(1, i2))) {
                                    z2 = true;
                                }
                            }
                            if (a(motionEvent.getX(), motionEvent.getY(), motionEvent.getX(1), motionEvent.getY(1))) {
                                z = z2;
                            }
                        } else {
                            z = false;
                        }
                    }
                    if (z) {
                        this.j = -1;
                        this.p.removeCallbacks(this.q);
                    }
                }
            }
        } else {
            int historySize2 = motionEvent.getHistorySize();
            for (int i3 = 0; i3 < historySize2; i3++) {
                a(motionEvent.getActionMasked(), motionEvent.getHistoricalX(0, i3), motionEvent.getHistoricalY(0, i3));
            }
            a(motionEvent.getActionMasked(), motionEvent.getX(), motionEvent.getY());
        }
    }

    public final void a(String str) {
        this.c = str;
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ void a(String str, DialogInterface dialogInterface, int i2) {
        ax.e();
        jv.a(this.f3453a, Intent.createChooser(new Intent("android.intent.action.SEND").setType("text/plain").putExtra("android.intent.extra.TEXT", str), "Share via"));
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ void b() {
        ax.o().a(this.f3453a, this.c, this.d, this.e);
    }

    public final void b(String str) {
        this.d = str;
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ void c() {
        ax.o().a(this.f3453a, this.c, this.d);
    }

    public final void c(String str) {
        this.f3454b = str;
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ void d() {
        this.j = 4;
        a();
    }

    public final void d(String str) {
        this.e = str;
    }
}
