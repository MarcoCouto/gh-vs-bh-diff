package com.google.android.gms.internal.ads;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.b.a;

public interface avb extends IInterface {
    a a(String str) throws RemoteException;

    void a() throws RemoteException;

    void a(a aVar) throws RemoteException;

    void a(a aVar, int i) throws RemoteException;

    void a(String str, a aVar) throws RemoteException;

    void b(a aVar) throws RemoteException;
}
