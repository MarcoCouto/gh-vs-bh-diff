package com.google.android.gms.internal.ads;

import java.util.HashMap;
import java.util.Map;

@cm
public final class asu {

    /* renamed from: a reason: collision with root package name */
    private final Map<String, ast> f2887a = new HashMap();

    /* renamed from: b reason: collision with root package name */
    private final asv f2888b;

    public asu(asv asv) {
        this.f2888b = asv;
    }

    public final asv a() {
        return this.f2888b;
    }

    public final void a(String str, ast ast) {
        this.f2887a.put(str, ast);
    }

    public final void a(String str, String str2, long j) {
        asv asv = this.f2888b;
        ast ast = (ast) this.f2887a.get(str2);
        String[] strArr = {str};
        if (!(asv == null || ast == null)) {
            asv.a(ast, j, strArr);
        }
        Map<String, ast> map = this.f2887a;
        asv asv2 = this.f2888b;
        map.put(str, asv2 == null ? null : asv2.a(j));
    }
}
