package com.google.android.gms.internal.ads;

final /* synthetic */ class azy implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final azx f3049a;

    /* renamed from: b reason: collision with root package name */
    private final String f3050b;

    azy(azx azx, String str) {
        this.f3049a = azx;
        this.f3050b = str;
    }

    public final void run() {
        this.f3049a.e(this.f3050b);
    }
}
