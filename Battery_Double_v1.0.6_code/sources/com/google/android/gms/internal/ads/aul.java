package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.ads.b.d;
import com.google.android.gms.common.internal.a.a;
import com.google.android.gms.common.internal.a.c;

@cm
public final class aul extends a {
    public static final Creator<aul> CREATOR = new aum();

    /* renamed from: a reason: collision with root package name */
    public final int f2937a;

    /* renamed from: b reason: collision with root package name */
    public final boolean f2938b;
    public final int c;
    public final boolean d;
    public final int e;
    public final arr f;

    public aul(int i, boolean z, int i2, boolean z2, int i3, arr arr) {
        this.f2937a = i;
        this.f2938b = z;
        this.c = i2;
        this.d = z2;
        this.e = i3;
        this.f = arr;
    }

    public aul(d dVar) {
        this(3, dVar.a(), dVar.b(), dVar.c(), dVar.d(), dVar.e() != null ? new arr(dVar.e()) : null);
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = c.a(parcel);
        c.a(parcel, 1, this.f2937a);
        c.a(parcel, 2, this.f2938b);
        c.a(parcel, 3, this.c);
        c.a(parcel, 4, this.d);
        c.a(parcel, 5, this.e);
        c.a(parcel, 6, (Parcelable) this.f, i, false);
        c.a(parcel, a2);
    }
}
