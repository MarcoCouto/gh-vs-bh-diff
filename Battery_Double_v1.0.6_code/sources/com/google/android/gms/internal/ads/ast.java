package com.google.android.gms.internal.ads;

@cm
public final class ast {

    /* renamed from: a reason: collision with root package name */
    private final long f2885a;

    /* renamed from: b reason: collision with root package name */
    private final String f2886b;
    private final ast c;

    public ast(long j, String str, ast ast) {
        this.f2885a = j;
        this.f2886b = str;
        this.c = ast;
    }

    public final long a() {
        return this.f2885a;
    }

    public final String b() {
        return this.f2886b;
    }

    public final ast c() {
        return this.c;
    }
}
