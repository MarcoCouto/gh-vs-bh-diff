package com.google.android.gms.internal.ads;

public final class amd implements ac {

    /* renamed from: a reason: collision with root package name */
    private int f2724a;

    /* renamed from: b reason: collision with root package name */
    private int f2725b;
    private final int c;
    private final float d;

    public amd() {
        this(2500, 1, 1.0f);
    }

    private amd(int i, int i2, float f) {
        this.f2724a = 2500;
        this.c = 1;
        this.d = 1.0f;
    }

    public final int a() {
        return this.f2724a;
    }

    public final void a(df dfVar) throws df {
        this.f2725b++;
        this.f2724a = (int) (((float) this.f2724a) + (((float) this.f2724a) * this.d));
        if (!(this.f2725b <= this.c)) {
            throw dfVar;
        }
    }

    public final int b() {
        return this.f2725b;
    }
}
