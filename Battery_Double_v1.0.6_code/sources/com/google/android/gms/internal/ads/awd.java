package com.google.android.gms.internal.ads;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public final class awd extends ajk implements awa {
    awd(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.formats.client.IOnContentAdLoadedListener");
    }

    public final void a(avp avp) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (IInterface) avp);
        b(1, r_);
    }
}
