package com.google.android.gms.internal.ads;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.a.a;
import com.google.android.gms.common.internal.a.c;
import java.util.List;

@cm
public final class ee extends a {
    public static final Creator<ee> CREATOR = new ef();

    /* renamed from: a reason: collision with root package name */
    private final Bundle f3274a;

    /* renamed from: b reason: collision with root package name */
    private final mu f3275b;
    private final ApplicationInfo c;
    private final String d;
    private final List<String> e;
    private final PackageInfo f;
    private final String g;
    private final boolean h;
    private final String i;

    public ee(Bundle bundle, mu muVar, ApplicationInfo applicationInfo, String str, List<String> list, PackageInfo packageInfo, String str2, boolean z, String str3) {
        this.f3274a = bundle;
        this.f3275b = muVar;
        this.d = str;
        this.c = applicationInfo;
        this.e = list;
        this.f = packageInfo;
        this.g = str2;
        this.h = z;
        this.i = str3;
    }

    public final void writeToParcel(Parcel parcel, int i2) {
        int a2 = c.a(parcel);
        c.a(parcel, 1, this.f3274a, false);
        c.a(parcel, 2, (Parcelable) this.f3275b, i2, false);
        c.a(parcel, 3, (Parcelable) this.c, i2, false);
        c.a(parcel, 4, this.d, false);
        c.b(parcel, 5, this.e, false);
        c.a(parcel, 6, (Parcelable) this.f, i2, false);
        c.a(parcel, 7, this.g, false);
        c.a(parcel, 8, this.h);
        c.a(parcel, 9, this.i, false);
        c.a(parcel, a2);
    }
}
