package com.google.android.gms.internal.ads;

import android.content.res.Resources;
import android.os.Bundle;
import com.google.android.gms.ads.c.a.C0045a;
import com.google.android.gms.ads.internal.ax;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import org.json.JSONException;
import org.json.JSONObject;

@cm
public final class bs implements bh<atp> {

    /* renamed from: a reason: collision with root package name */
    private final boolean f3203a;

    /* renamed from: b reason: collision with root package name */
    private final boolean f3204b;
    private final boolean c;

    public bs(boolean z, boolean z2, boolean z3) {
        this.f3203a = z;
        this.f3204b = z2;
        this.c = z3;
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x00c2  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x00c8  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00d6  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00d8  */
    public final /* synthetic */ aub a(ay ayVar, JSONObject jSONObject) throws JSONException, InterruptedException, ExecutionException {
        String str;
        List<nn> a2 = ayVar.a(jSONObject, "images", false, this.f3203a, this.f3204b);
        nn a3 = ayVar.a(jSONObject, "secondary_image", false, this.f3203a);
        nn a4 = ayVar.a(jSONObject);
        nn a5 = ayVar.a(jSONObject, "video");
        ArrayList arrayList = new ArrayList();
        for (nn nnVar : a2) {
            arrayList.add((atm) nnVar.get());
        }
        qn a6 = ay.a(a5);
        String string = jSONObject.getString("headline");
        if (this.c) {
            if (((Boolean) ape.f().a(asi.dm)).booleanValue()) {
                Resources h = ax.i().h();
                str = h != null ? h.getString(C0045a.s7) : "Test Ad";
                if (string != null) {
                    str = new StringBuilder(String.valueOf(str).length() + 3 + String.valueOf(string).length()).append(str).append(" : ").append(string).toString();
                }
                return new atp(str, arrayList, jSONObject.getString("body"), (auw) a3.get(), jSONObject.getString("call_to_action"), jSONObject.getString("advertiser"), (ati) a4.get(), new Bundle(), a6 == null ? a6.b() : null, a6 == null ? a6.getView() : null, null, null);
            }
        }
        str = string;
        return new atp(str, arrayList, jSONObject.getString("body"), (auw) a3.get(), jSONObject.getString("call_to_action"), jSONObject.getString("advertiser"), (ati) a4.get(), new Bundle(), a6 == null ? a6.b() : null, a6 == null ? a6.getView() : null, null, null);
    }
}
