package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.Binder;
import android.os.RemoteException;
import com.google.android.gms.ads.internal.ax;

@cm
public final class amh {

    /* renamed from: a reason: collision with root package name */
    private final Runnable f2730a = new ami(this);
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public final Object f2731b = new Object();
    /* access modifiers changed from: private */
    public amo c;
    private Context d;
    /* access modifiers changed from: private */
    public ams e;

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        return;
     */
    public final void b() {
        synchronized (this.f2731b) {
            if (this.d != null && this.c == null) {
                this.c = new amo(this.d, ax.t().a(), new amk(this), new aml(this));
                this.c.o();
            }
        }
    }

    /* access modifiers changed from: private */
    public final void c() {
        synchronized (this.f2731b) {
            if (this.c != null) {
                if (this.c.b() || this.c.c()) {
                    this.c.a();
                }
                this.c = null;
                this.e = null;
                Binder.flushPendingCommands();
            }
        }
    }

    public final amm a(amp amp) {
        amm amm;
        synchronized (this.f2731b) {
            if (this.e == null) {
                amm = new amm();
            } else {
                try {
                    amm = this.e.a(amp);
                } catch (RemoteException e2) {
                    jm.b("Unable to call into cache service.", e2);
                    amm = new amm();
                }
            }
        }
        return amm;
    }

    public final void a() {
        if (((Boolean) ape.f().a(asi.cF)).booleanValue()) {
            synchronized (this.f2731b) {
                b();
                ax.e();
                jv.f3440a.removeCallbacks(this.f2730a);
                ax.e();
                jv.f3440a.postDelayed(this.f2730a, ((Long) ape.f().a(asi.cG)).longValue());
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:?, code lost:
        return;
     */
    public final void a(Context context) {
        if (context != null) {
            synchronized (this.f2731b) {
                if (this.d == null) {
                    this.d = context.getApplicationContext();
                    if (((Boolean) ape.f().a(asi.cE)).booleanValue()) {
                        b();
                    } else {
                        if (((Boolean) ape.f().a(asi.cD)).booleanValue()) {
                            ax.h().a((alm) new amj(this));
                        }
                    }
                }
            }
        }
    }
}
