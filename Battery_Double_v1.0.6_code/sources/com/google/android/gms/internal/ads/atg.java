package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.b.a;
import android.support.b.b;
import android.support.b.d;
import android.support.b.e;
import java.util.List;

@cm
public final class atg implements agd {

    /* renamed from: a reason: collision with root package name */
    private e f2895a;

    /* renamed from: b reason: collision with root package name */
    private b f2896b;
    private d c;
    private ath d;

    public static boolean a(Context context) {
        PackageManager packageManager = context.getPackageManager();
        if (packageManager == null) {
            return false;
        }
        Intent intent = new Intent("android.intent.action.VIEW", Uri.parse("http://www.example.com"));
        ResolveInfo resolveActivity = packageManager.resolveActivity(intent, 0);
        List queryIntentActivities = packageManager.queryIntentActivities(intent, 65536);
        if (queryIntentActivities == null || resolveActivity == null) {
            return false;
        }
        for (int i = 0; i < queryIntentActivities.size(); i++) {
            if (resolveActivity.activityInfo.name.equals(((ResolveInfo) queryIntentActivities.get(i)).activityInfo.name)) {
                return resolveActivity.activityInfo.packageName.equals(agb.a(context));
            }
        }
        return false;
    }

    public final void a() {
        this.f2896b = null;
        this.f2895a = null;
        if (this.d != null) {
            this.d.b();
        }
    }

    public final void a(Activity activity) {
        if (this.c != null) {
            activity.unbindService(this.c);
            this.f2896b = null;
            this.f2895a = null;
            this.c = null;
        }
    }

    public final void a(b bVar) {
        this.f2896b = bVar;
        this.f2896b.a(0);
        if (this.d != null) {
            this.d.a();
        }
    }

    public final void a(ath ath) {
        this.d = ath;
    }

    public final boolean a(Uri uri, Bundle bundle, List<Bundle> list) {
        if (this.f2896b == null) {
            return false;
        }
        if (this.f2896b == null) {
            this.f2895a = null;
        } else if (this.f2895a == null) {
            this.f2895a = this.f2896b.a((a) null);
        }
        e eVar = this.f2895a;
        if (eVar != null) {
            return eVar.a(uri, null, null);
        }
        return false;
    }

    public final void b(Activity activity) {
        if (this.f2896b == null) {
            String a2 = agb.a(activity);
            if (a2 != null) {
                this.c = new agc(this);
                b.a(activity, a2, this.c);
            }
        }
    }
}
