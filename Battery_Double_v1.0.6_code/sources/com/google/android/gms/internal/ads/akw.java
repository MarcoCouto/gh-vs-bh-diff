package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.app.Application;
import android.app.Application.ActivityLifecycleCallbacks;
import android.os.Bundle;
import java.lang.ref.WeakReference;

final class akw implements ActivityLifecycleCallbacks {

    /* renamed from: a reason: collision with root package name */
    private final Application f2680a;

    /* renamed from: b reason: collision with root package name */
    private final WeakReference<ActivityLifecycleCallbacks> f2681b;
    private boolean c = false;

    public akw(Application application, ActivityLifecycleCallbacks activityLifecycleCallbacks) {
        this.f2681b = new WeakReference<>(activityLifecycleCallbacks);
        this.f2680a = application;
    }

    private final void a(alf alf) {
        try {
            ActivityLifecycleCallbacks activityLifecycleCallbacks = (ActivityLifecycleCallbacks) this.f2681b.get();
            if (activityLifecycleCallbacks != null) {
                alf.a(activityLifecycleCallbacks);
            } else if (!this.c) {
                this.f2680a.unregisterActivityLifecycleCallbacks(this);
                this.c = true;
            }
        } catch (Exception e) {
            jm.b("Error while dispatching lifecycle callback.", e);
        }
    }

    public final void onActivityCreated(Activity activity, Bundle bundle) {
        a(new akx(this, activity, bundle));
    }

    public final void onActivityDestroyed(Activity activity) {
        a(new ale(this, activity));
    }

    public final void onActivityPaused(Activity activity) {
        a(new ala(this, activity));
    }

    public final void onActivityResumed(Activity activity) {
        a(new akz(this, activity));
    }

    public final void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        a(new ald(this, activity, bundle));
    }

    public final void onActivityStarted(Activity activity) {
        a(new aky(this, activity));
    }

    public final void onActivityStopped(Activity activity) {
        a(new alb(this, activity));
    }
}
