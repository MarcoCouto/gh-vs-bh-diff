package com.google.android.gms.internal.ads;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Looper;
import com.google.android.gms.ads.internal.ax;
import com.google.android.gms.common.b.c;
import com.google.android.gms.common.util.n;
import com.google.android.gms.dynamite.DynamiteModule;
import com.google.android.gms.dynamite.DynamiteModule.a;
import com.google.android.gms.dynamite.descriptors.com.google.android.gms.ads.dynamite.ModuleDescriptor;
import java.util.ArrayList;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicInteger;

@cm
public final class iw implements js {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public final Object f3405a = new Object();

    /* renamed from: b reason: collision with root package name */
    private ajt f3406b;
    private final jd c = new jd();
    private final jo d = new jo();
    private boolean e = false;
    /* access modifiers changed from: private */
    public Context f;
    /* access modifiers changed from: private */
    public mu g;
    /* access modifiers changed from: private */
    public asl h = null;
    private aln i = null;
    private ali j = null;
    private Boolean k = null;
    private String l;
    private final AtomicInteger m = new AtomicInteger(0);
    private final iz n = new iz(null);
    private final Object o = new Object();
    private nn<ArrayList<String>> p;

    /* JADX WARNING: Code restructure failed: missing block: B:38:?, code lost:
        return null;
     */
    private final aln a(Context context, boolean z, boolean z2) {
        if (!((Boolean) ape.f().a(asi.Q)).booleanValue()) {
            return null;
        }
        if (!n.b()) {
            return null;
        }
        if (!((Boolean) ape.f().a(asi.Y)).booleanValue()) {
            if (!((Boolean) ape.f().a(asi.W)).booleanValue()) {
                return null;
            }
        }
        if (z && z2) {
            return null;
        }
        synchronized (this.f3405a) {
            if (Looper.getMainLooper() != null && context != null) {
                if (this.j == null) {
                    this.j = new ali();
                }
                if (this.i == null) {
                    this.i = new aln(this.j, cg.a(context, this.g));
                }
                this.i.a();
                jm.d("start fetching content...");
                aln aln = this.i;
                return aln;
            }
        }
    }

    @TargetApi(16)
    private static ArrayList<String> b(Context context) {
        ArrayList arrayList = new ArrayList();
        try {
            PackageInfo b2 = c.b(context).b(context.getApplicationInfo().packageName, 4096);
            if (b2.requestedPermissions == null || b2.requestedPermissionsFlags == null) {
                return arrayList;
            }
            for (int i2 = 0; i2 < b2.requestedPermissions.length; i2++) {
                if ((b2.requestedPermissionsFlags[i2] & 2) != 0) {
                    arrayList.add(b2.requestedPermissions[i2]);
                }
            }
            return arrayList;
        } catch (NameNotFoundException e2) {
            return arrayList;
        }
    }

    public final aln a(Context context) {
        return a(context, this.d.b(), this.d.d());
    }

    public final jd a() {
        return this.c;
    }

    @TargetApi(23)
    public final void a(Context context, mu muVar) {
        asl asl;
        synchronized (this.f3405a) {
            if (!this.e) {
                this.f = context.getApplicationContext();
                this.g = muVar;
                ax.h().a((alm) ax.j());
                this.d.a(this.f);
                this.d.a((js) this);
                cg.a(this.f, this.g);
                this.l = ax.e().b(context, muVar.f3528a);
                this.f3406b = new ajt(context.getApplicationContext(), this.g);
                ax.n();
                if (!((Boolean) ape.f().a(asi.N)).booleanValue()) {
                    jm.a("CsiReporterFactory: CSI is not enabled. No CSI reporter created.");
                    asl = null;
                } else {
                    asl = new asl();
                }
                this.h = asl;
                na.a((nn) new iy(this).c(), "AppState.registerCsiReporter");
                this.e = true;
                n();
            }
        }
    }

    public final void a(Bundle bundle) {
        if (bundle.containsKey("content_url_opted_out") && bundle.containsKey("content_vertical_opted_out")) {
            a(this.f, bundle.getBoolean("content_url_opted_out"), bundle.getBoolean("content_vertical_opted_out"));
        }
    }

    public final void a(Boolean bool) {
        synchronized (this.f3405a) {
            this.k = bool;
        }
    }

    public final void a(Throwable th, String str) {
        cg.a(this.f, this.g).a(th, str);
    }

    public final void a(boolean z) {
        this.n.a(z);
    }

    public final asl b() {
        asl asl;
        synchronized (this.f3405a) {
            asl = this.h;
        }
        return asl;
    }

    public final void b(Throwable th, String str) {
        cg.a(this.f, this.g).a(th, str, ((Float) ape.f().a(asi.f)).floatValue());
    }

    public final Boolean c() {
        Boolean bool;
        synchronized (this.f3405a) {
            bool = this.k;
        }
        return bool;
    }

    public final boolean d() {
        return this.n.a();
    }

    public final boolean e() {
        return this.n.b();
    }

    public final void f() {
        this.n.c();
    }

    public final ajt g() {
        return this.f3406b;
    }

    public final Resources h() {
        if (this.g.d) {
            return this.f.getResources();
        }
        try {
            DynamiteModule a2 = DynamiteModule.a(this.f, DynamiteModule.f2374a, ModuleDescriptor.MODULE_ID);
            if (a2 != null) {
                return a2.a().getResources();
            }
            return null;
        } catch (a e2) {
            jm.c("Cannot load resource from dynamite apk or local jar", e2);
            return null;
        }
    }

    public final void i() {
        this.m.incrementAndGet();
    }

    public final void j() {
        this.m.decrementAndGet();
    }

    public final int k() {
        return this.m.get();
    }

    public final jo l() {
        jo joVar;
        synchronized (this.f3405a) {
            joVar = this.d;
        }
        return joVar;
    }

    public final Context m() {
        return this.f;
    }

    public final nn<ArrayList<String>> n() {
        if (this.f != null && n.d()) {
            if (!((Boolean) ape.f().a(asi.bH)).booleanValue()) {
                synchronized (this.o) {
                    if (this.p != null) {
                        nn<ArrayList<String>> nnVar = this.p;
                        return nnVar;
                    }
                    nn<ArrayList<String>> a2 = jt.a((Callable<T>) new ix<T>(this));
                    this.p = a2;
                    return a2;
                }
            }
        }
        return nc.a(new ArrayList());
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ ArrayList o() throws Exception {
        return b(this.f);
    }
}
