package com.google.android.gms.internal.ads;

import android.content.SharedPreferences.Editor;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.json.JSONObject;

@cm
public final class ase {

    /* renamed from: a reason: collision with root package name */
    private final Collection<ary<?>> f2869a = new ArrayList();

    /* renamed from: b reason: collision with root package name */
    private final Collection<ary<String>> f2870b = new ArrayList();
    private final Collection<ary<String>> c = new ArrayList();

    public final List<String> a() {
        ArrayList arrayList = new ArrayList();
        for (ary a2 : this.f2870b) {
            String str = (String) ape.f().a(a2);
            if (str != null) {
                arrayList.add(str);
            }
        }
        return arrayList;
    }

    public final void a(Editor editor, int i, JSONObject jSONObject) {
        for (ary ary : this.f2869a) {
            if (ary.c() == 1) {
                ary.a(editor, ary.a(jSONObject));
            }
        }
    }

    public final void a(ary ary) {
        this.f2869a.add(ary);
    }

    public final List<String> b() {
        List<String> a2 = a();
        for (ary a3 : this.c) {
            String str = (String) ape.f().a(a3);
            if (str != null) {
                a2.add(str);
            }
        }
        return a2;
    }

    public final void b(ary<String> ary) {
        this.f2870b.add(ary);
    }

    public final void c(ary<String> ary) {
        this.c.add(ary);
    }
}
