package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.os.SystemClock;
import com.google.android.gms.ads.internal.ax;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

@cm
public final class it {

    /* renamed from: a reason: collision with root package name */
    private final jf f3399a;

    /* renamed from: b reason: collision with root package name */
    private final LinkedList<iu> f3400b;
    private final Object c;
    private final String d;
    private final String e;
    private long f;
    private long g;
    private boolean h;
    private long i;
    private long j;
    private long k;
    private long l;

    private it(jf jfVar, String str, String str2) {
        this.c = new Object();
        this.f = -1;
        this.g = -1;
        this.h = false;
        this.i = -1;
        this.j = 0;
        this.k = -1;
        this.l = -1;
        this.f3399a = jfVar;
        this.d = str;
        this.e = str2;
        this.f3400b = new LinkedList<>();
    }

    public it(String str, String str2) {
        this(ax.j(), str, str2);
    }

    public final void a() {
        synchronized (this.c) {
            if (this.l != -1 && this.g == -1) {
                this.g = SystemClock.elapsedRealtime();
                this.f3399a.a(this);
            }
            this.f3399a.b();
        }
    }

    public final void a(long j2) {
        synchronized (this.c) {
            this.l = j2;
            if (this.l != -1) {
                this.f3399a.a(this);
            }
        }
    }

    public final void a(aop aop) {
        synchronized (this.c) {
            this.k = SystemClock.elapsedRealtime();
            this.f3399a.a(aop, this.k);
        }
    }

    public final void a(boolean z) {
        synchronized (this.c) {
            if (this.l != -1) {
                this.i = SystemClock.elapsedRealtime();
                if (!z) {
                    this.g = this.i;
                    this.f3399a.a(this);
                }
            }
        }
    }

    public final void b() {
        synchronized (this.c) {
            if (this.l != -1) {
                iu iuVar = new iu();
                iuVar.c();
                this.f3400b.add(iuVar);
                this.j++;
                this.f3399a.a();
                this.f3399a.a(this);
            }
        }
    }

    public final void b(long j2) {
        synchronized (this.c) {
            if (this.l != -1) {
                this.f = j2;
                this.f3399a.a(this);
            }
        }
    }

    public final void b(boolean z) {
        synchronized (this.c) {
            if (this.l != -1) {
                this.h = z;
                this.f3399a.a(this);
            }
        }
    }

    public final void c() {
        synchronized (this.c) {
            if (this.l != -1 && !this.f3400b.isEmpty()) {
                iu iuVar = (iu) this.f3400b.getLast();
                if (iuVar.a() == -1) {
                    iuVar.b();
                    this.f3399a.a(this);
                }
            }
        }
    }

    public final Bundle d() {
        Bundle bundle;
        synchronized (this.c) {
            bundle = new Bundle();
            bundle.putString("seq_num", this.d);
            bundle.putString("slotid", this.e);
            bundle.putBoolean("ismediation", this.h);
            bundle.putLong("treq", this.k);
            bundle.putLong("tresponse", this.l);
            bundle.putLong("timp", this.g);
            bundle.putLong("tload", this.i);
            bundle.putLong("pcc", this.j);
            bundle.putLong("tfetch", this.f);
            ArrayList arrayList = new ArrayList();
            Iterator it = this.f3400b.iterator();
            while (it.hasNext()) {
                arrayList.add(((iu) it.next()).d());
            }
            bundle.putParcelableArrayList("tclick", arrayList);
        }
        return bundle;
    }
}
