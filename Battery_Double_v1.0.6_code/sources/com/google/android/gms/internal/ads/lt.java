package com.google.android.gms.internal.ads;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import com.google.android.gms.common.internal.aa;

@cm
public final class lt {

    /* renamed from: a reason: collision with root package name */
    private HandlerThread f3491a = null;

    /* renamed from: b reason: collision with root package name */
    private Handler f3492b = null;
    private int c = 0;
    private final Object d = new Object();

    public final Looper a() {
        Looper looper;
        synchronized (this.d) {
            if (this.c != 0) {
                aa.a(this.f3491a, (Object) "Invalid state: mHandlerThread should already been initialized.");
            } else if (this.f3491a == null) {
                jm.a("Starting the looper thread.");
                this.f3491a = new HandlerThread("LooperProvider");
                this.f3491a.start();
                this.f3492b = new Handler(this.f3491a.getLooper());
                jm.a("Looper thread started.");
            } else {
                jm.a("Resuming the looper thread");
                this.d.notifyAll();
            }
            this.c++;
            looper = this.f3491a.getLooper();
        }
        return looper;
    }

    public final Handler b() {
        return this.f3492b;
    }
}
