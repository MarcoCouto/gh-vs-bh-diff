package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.gmsg.ae;
import java.util.Map;

final /* synthetic */ class azu implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final azt f3045a;

    /* renamed from: b reason: collision with root package name */
    private final ae f3046b;
    private final Map c;

    azu(azt azt, ae aeVar, Map map) {
        this.f3045a = azt;
        this.f3046b = aeVar;
        this.c = map;
    }

    public final void run() {
        azt azt = this.f3045a;
        this.f3046b.zza(azt.o(), this.c);
    }
}
