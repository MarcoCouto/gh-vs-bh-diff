package com.google.android.gms.internal.ads;

import android.support.v4.h.m;
import android.view.View;
import android.widget.FrameLayout;
import com.google.android.gms.b.a;
import com.google.android.gms.b.b;
import java.util.Arrays;
import java.util.List;

@cm
public final class atr extends avu implements aub {

    /* renamed from: a reason: collision with root package name */
    private final ati f2912a;

    /* renamed from: b reason: collision with root package name */
    private final String f2913b;
    private final m<String, atm> c;
    private final m<String, String> d;
    private aqs e;
    private View f;
    private final Object g = new Object();
    /* access modifiers changed from: private */
    public aty h;

    public atr(String str, m<String, atm> mVar, m<String, String> mVar2, ati ati, aqs aqs, View view) {
        this.f2913b = str;
        this.c = mVar;
        this.d = mVar2;
        this.f2912a = ati;
        this.e = aqs;
        this.f = view;
    }

    public final String a(String str) {
        return (String) this.d.get(str);
    }

    public final List<String> a() {
        int i = 0;
        String[] strArr = new String[(this.c.size() + this.d.size())];
        int i2 = 0;
        for (int i3 = 0; i3 < this.c.size(); i3++) {
            strArr[i2] = (String) this.c.b(i3);
            i2++;
        }
        while (i < this.d.size()) {
            strArr[i2] = (String) this.d.b(i);
            i++;
            i2++;
        }
        return Arrays.asList(strArr);
    }

    public final void a(aty aty) {
        synchronized (this.g) {
            this.h = aty;
        }
    }

    public final boolean a(a aVar) {
        if (this.h == null) {
            ms.c("Attempt to call renderVideoInMediaView before ad initialized.");
            return false;
        } else if (this.f == null) {
            return false;
        } else {
            FrameLayout frameLayout = (FrameLayout) b.a(aVar);
            this.h.a((View) frameLayout, (atw) new ats(this));
            return true;
        }
    }

    public final a b() {
        return b.a(this.h);
    }

    public final auw b(String str) {
        return (auw) this.c.get(str);
    }

    public final aqs c() {
        return this.e;
    }

    public final void c(String str) {
        synchronized (this.g) {
            if (this.h == null) {
                ms.c("#001 Attempt to perform click before app native ad initialized.");
            } else {
                this.h.a(null, str, null, null, null);
            }
        }
    }

    public final void d() {
        synchronized (this.g) {
            if (this.h == null) {
                ms.c("#002 Attempt to record impression before native ad initialized.");
            } else {
                this.h.a((View) null, null);
            }
        }
    }

    public final a e() {
        return b.a(this.h.m().getApplicationContext());
    }

    public final void f() {
        jv.f3440a.post(new att(this));
        this.e = null;
        this.f = null;
    }

    public final String k() {
        return "3";
    }

    public final String l() {
        return this.f2913b;
    }

    public final ati m() {
        return this.f2912a;
    }

    public final View o() {
        return this.f;
    }
}
