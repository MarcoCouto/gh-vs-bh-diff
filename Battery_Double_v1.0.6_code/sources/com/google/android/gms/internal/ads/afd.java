package com.google.android.gms.internal.ads;

import java.io.IOException;

public final class afd {

    /* renamed from: a reason: collision with root package name */
    private final byte[] f2520a;

    /* renamed from: b reason: collision with root package name */
    private final int f2521b;
    private final int c;
    private int d;
    private int e;
    private int f;
    private int g;
    private int h = Integer.MAX_VALUE;
    private int i;
    private int j = 64;
    private int k = 67108864;

    private afd(byte[] bArr, int i2, int i3) {
        this.f2520a = bArr;
        this.f2521b = i2;
        int i4 = i2 + i3;
        this.d = i4;
        this.c = i4;
        this.f = i2;
    }

    public static afd a(byte[] bArr, int i2, int i3) {
        return new afd(bArr, 0, i3);
    }

    private final void f(int i2) throws IOException {
        if (i2 < 0) {
            throw afm.b();
        } else if (this.f + i2 > this.h) {
            f(this.h - this.f);
            throw afm.a();
        } else if (i2 <= this.d - this.f) {
            this.f += i2;
        } else {
            throw afm.a();
        }
    }

    private final void k() {
        this.d += this.e;
        int i2 = this.d;
        if (i2 > this.h) {
            this.e = i2 - this.h;
            this.d -= this.e;
            return;
        }
        this.e = 0;
    }

    private final byte l() throws IOException {
        if (this.f == this.d) {
            throw afm.a();
        }
        byte[] bArr = this.f2520a;
        int i2 = this.f;
        this.f = i2 + 1;
        return bArr[i2];
    }

    public final int a() throws IOException {
        if (this.f == this.d) {
            this.g = 0;
            return 0;
        }
        this.g = g();
        if (this.g != 0) {
            return this.g;
        }
        throw new afm("Protocol message contained an invalid tag (zero).");
    }

    public final void a(int i2) throws afm {
        if (this.g != i2) {
            throw new afm("Protocol message end-group tag did not match expected tag.");
        }
    }

    public final void a(afn afn) throws IOException {
        int g2 = g();
        if (this.i >= this.j) {
            throw new afm("Protocol message had too many levels of nesting.  May be malicious.  Use CodedInputStream.setRecursionLimit() to increase the depth limit.");
        }
        int c2 = c(g2);
        this.i++;
        afn.a(this);
        a(0);
        this.i--;
        d(c2);
    }

    public final byte[] a(int i2, int i3) {
        if (i3 == 0) {
            return afq.e;
        }
        byte[] bArr = new byte[i3];
        System.arraycopy(this.f2520a, this.f2521b + i2, bArr, 0, i3);
        return bArr;
    }

    public final long b() throws IOException {
        return h();
    }

    /* access modifiers changed from: 0000 */
    public final void b(int i2, int i3) {
        if (i2 > this.f - this.f2521b) {
            throw new IllegalArgumentException("Position " + i2 + " is beyond current " + (this.f - this.f2521b));
        } else if (i2 < 0) {
            throw new IllegalArgumentException("Bad position " + i2);
        } else {
            this.f = this.f2521b + i2;
            this.g = i3;
        }
    }

    public final boolean b(int i2) throws IOException {
        int a2;
        switch (i2 & 7) {
            case 0:
                g();
                return true;
            case 1:
                l();
                l();
                l();
                l();
                l();
                l();
                l();
                l();
                return true;
            case 2:
                f(g());
                return true;
            case 3:
                break;
            case 4:
                return false;
            case 5:
                l();
                l();
                l();
                l();
                return true;
            default:
                throw new afm("Protocol message tag had invalid wire type.");
        }
        do {
            a2 = a();
            if (a2 != 0) {
            }
            a(((i2 >>> 3) << 3) | 4);
            return true;
        } while (b(a2));
        a(((i2 >>> 3) << 3) | 4);
        return true;
    }

    public final int c() throws IOException {
        return g();
    }

    public final int c(int i2) throws afm {
        if (i2 < 0) {
            throw afm.b();
        }
        int i3 = this.f + i2;
        int i4 = this.h;
        if (i3 > i4) {
            throw afm.a();
        }
        this.h = i3;
        k();
        return i4;
    }

    public final void d(int i2) {
        this.h = i2;
        k();
    }

    public final boolean d() throws IOException {
        return g() != 0;
    }

    public final String e() throws IOException {
        int g2 = g();
        if (g2 < 0) {
            throw afm.b();
        } else if (g2 > this.d - this.f) {
            throw afm.a();
        } else {
            String str = new String(this.f2520a, this.f, g2, afl.f2530a);
            this.f = g2 + this.f;
            return str;
        }
    }

    public final void e(int i2) {
        b(i2, this.g);
    }

    public final byte[] f() throws IOException {
        int g2 = g();
        if (g2 < 0) {
            throw afm.b();
        } else if (g2 == 0) {
            return afq.e;
        } else {
            if (g2 > this.d - this.f) {
                throw afm.a();
            }
            byte[] bArr = new byte[g2];
            System.arraycopy(this.f2520a, this.f, bArr, 0, g2);
            this.f = g2 + this.f;
            return bArr;
        }
    }

    public final int g() throws IOException {
        byte l = l();
        if (l >= 0) {
            return l;
        }
        byte b2 = l & Byte.MAX_VALUE;
        byte l2 = l();
        if (l2 >= 0) {
            return b2 | (l2 << 7);
        }
        byte b3 = b2 | ((l2 & Byte.MAX_VALUE) << 7);
        byte l3 = l();
        if (l3 >= 0) {
            return b3 | (l3 << 14);
        }
        byte b4 = b3 | ((l3 & Byte.MAX_VALUE) << 14);
        byte l4 = l();
        if (l4 >= 0) {
            return b4 | (l4 << 21);
        }
        byte b5 = b4 | ((l4 & Byte.MAX_VALUE) << 21);
        byte l5 = l();
        byte b6 = b5 | (l5 << 28);
        if (l5 >= 0) {
            return b6;
        }
        for (int i2 = 0; i2 < 5; i2++) {
            if (l() >= 0) {
                return b6;
            }
        }
        throw afm.c();
    }

    public final long h() throws IOException {
        long j2 = 0;
        for (int i2 = 0; i2 < 64; i2 += 7) {
            byte l = l();
            j2 |= ((long) (l & Byte.MAX_VALUE)) << i2;
            if ((l & 128) == 0) {
                return j2;
            }
        }
        throw afm.c();
    }

    public final int i() {
        if (this.h == Integer.MAX_VALUE) {
            return -1;
        }
        return this.h - this.f;
    }

    public final int j() {
        return this.f - this.f2521b;
    }
}
