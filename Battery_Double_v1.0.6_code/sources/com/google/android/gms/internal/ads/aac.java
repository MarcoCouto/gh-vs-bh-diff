package com.google.android.gms.internal.ads;

final class aac {

    /* renamed from: a reason: collision with root package name */
    private static final Class<?> f2387a = a("libcore.io.Memory");

    /* renamed from: b reason: collision with root package name */
    private static final boolean f2388b = (a("org.robolectric.Robolectric") != null);

    private static <T> Class<T> a(String str) {
        try {
            return Class.forName(str);
        } catch (Throwable th) {
            return null;
        }
    }

    static boolean a() {
        return f2387a != null && !f2388b;
    }

    static Class<?> b() {
        return f2387a;
    }
}
