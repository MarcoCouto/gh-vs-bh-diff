package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.xe.b;
import java.security.GeneralSecurityException;

final class un implements tz<tr> {
    un() {
    }

    /* access modifiers changed from: private */
    /* renamed from: d */
    public final tr a(aah aah) throws GeneralSecurityException {
        try {
            vs a2 = vs.a(aah);
            if (!(a2 instanceof vs)) {
                throw new GeneralSecurityException("expected AesEaxKey proto");
            }
            vs vsVar = a2;
            zo.a(vsVar.a(), 0);
            zo.a(vsVar.c().a());
            if (vsVar.b().a() == 12 || vsVar.b().a() == 16) {
                return new yf(vsVar.c().b(), vsVar.b().a());
            }
            throw new GeneralSecurityException("invalid IV size; acceptable values have 12 or 16 bytes");
        } catch (abv e) {
            throw new GeneralSecurityException("expected serialized AesEaxKey proto", e);
        }
    }

    public final int a() {
        return 0;
    }

    public final /* synthetic */ Object a(acw acw) throws GeneralSecurityException {
        if (!(acw instanceof vs)) {
            throw new GeneralSecurityException("expected AesEaxKey proto");
        }
        vs vsVar = (vs) acw;
        zo.a(vsVar.a(), 0);
        zo.a(vsVar.c().a());
        if (vsVar.b().a() == 12 || vsVar.b().a() == 16) {
            return new yf(vsVar.c().b(), vsVar.b().a());
        }
        throw new GeneralSecurityException("invalid IV size; acceptable values have 12 or 16 bytes");
    }

    public final acw b(aah aah) throws GeneralSecurityException {
        try {
            return b((acw) vv.a(aah));
        } catch (abv e) {
            throw new GeneralSecurityException("expected serialized AesEaxKeyFormat proto", e);
        }
    }

    public final acw b(acw acw) throws GeneralSecurityException {
        if (!(acw instanceof vv)) {
            throw new GeneralSecurityException("expected AesEaxKeyFormat proto");
        }
        vv vvVar = (vv) acw;
        zo.a(vvVar.b());
        if (vvVar.a().a() == 12 || vvVar.a().a() == 16) {
            return vs.d().a(aah.a(zj.a(vvVar.b()))).a(vvVar.a()).a(0).c();
        }
        throw new GeneralSecurityException("invalid IV size; acceptable values have 12 or 16 bytes");
    }

    public final xe c(aah aah) throws GeneralSecurityException {
        return (xe) xe.d().a("type.googleapis.com/google.crypto.tink.AesEaxKey").a(((vs) b(aah)).h()).a(b.SYMMETRIC).c();
    }
}
