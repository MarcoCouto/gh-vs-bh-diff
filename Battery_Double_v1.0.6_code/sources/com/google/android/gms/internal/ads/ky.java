package com.google.android.gms.internal.ads;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.net.Uri;
import com.google.android.gms.ads.internal.ax;

final class ky implements OnClickListener {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ kx f3467a;

    ky(kx kxVar) {
        this.f3467a = kxVar;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        ax.e();
        jv.a(this.f3467a.f3465a, Uri.parse("https://support.google.com/dfp_premium/answer/7160685#push"));
    }
}
