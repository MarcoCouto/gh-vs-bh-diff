package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.ads.internal.ax;

@cm
public final class lv extends jh {

    /* renamed from: a reason: collision with root package name */
    private final mt f3495a;

    /* renamed from: b reason: collision with root package name */
    private final String f3496b;

    public lv(Context context, String str, String str2) {
        this(str2, ax.e().b(context, str));
    }

    private lv(String str, String str2) {
        this.f3495a = new mt(str2);
        this.f3496b = str;
    }

    public final void a() {
        this.f3495a.a(this.f3496b);
    }

    public final void c_() {
    }
}
