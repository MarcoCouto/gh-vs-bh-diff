package com.google.android.gms.internal.ads;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.b.a;

public interface aqn extends IInterface {
    void a() throws RemoteException;

    void a(float f) throws RemoteException;

    void a(a aVar, String str) throws RemoteException;

    void a(String str) throws RemoteException;

    void a(String str, a aVar) throws RemoteException;

    void a(boolean z) throws RemoteException;

    float b() throws RemoteException;

    boolean c() throws RemoteException;
}
