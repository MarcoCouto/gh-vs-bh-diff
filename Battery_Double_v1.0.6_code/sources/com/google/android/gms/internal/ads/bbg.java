package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.gmsg.ae;
import java.util.AbstractMap.SimpleEntry;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONObject;

@cm
public final class bbg implements azp, bbf {

    /* renamed from: a reason: collision with root package name */
    private final bbe f3100a;

    /* renamed from: b reason: collision with root package name */
    private final HashSet<SimpleEntry<String, ae<? super bbe>>> f3101b = new HashSet<>();

    public bbg(bbe bbe) {
        this.f3100a = bbe;
    }

    public final void a() {
        Iterator it = this.f3101b.iterator();
        while (it.hasNext()) {
            SimpleEntry simpleEntry = (SimpleEntry) it.next();
            String str = "Unregistering eventhandler: ";
            String valueOf = String.valueOf(((ae) simpleEntry.getValue()).toString());
            jm.a(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
            this.f3100a.b((String) simpleEntry.getKey(), (ae) simpleEntry.getValue());
        }
        this.f3101b.clear();
    }

    public final void a(String str, ae<? super bbe> aeVar) {
        this.f3100a.a(str, aeVar);
        this.f3101b.add(new SimpleEntry(str, aeVar));
    }

    public final void a(String str, String str2) {
        azq.a((azp) this, str, str2);
    }

    public final void a(String str, Map map) {
        azq.a((azp) this, str, map);
    }

    public final void a(String str, JSONObject jSONObject) {
        azq.b(this, str, jSONObject);
    }

    public final void b(String str) {
        this.f3100a.b(str);
    }

    public final void b(String str, ae<? super bbe> aeVar) {
        this.f3100a.b(str, aeVar);
        this.f3101b.remove(new SimpleEntry(str, aeVar));
    }

    public final void b(String str, JSONObject jSONObject) {
        azq.a((azp) this, str, jSONObject);
    }
}
