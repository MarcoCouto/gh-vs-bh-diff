package com.google.android.gms.internal.ads;

import java.nio.ByteBuffer;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public final class ahj {

    /* renamed from: b reason: collision with root package name */
    private static Cipher f2587b = null;
    private static final Object c = new Object();
    private static final Object d = new Object();

    /* renamed from: a reason: collision with root package name */
    private final SecureRandom f2588a = null;

    public ahj(SecureRandom secureRandom) {
    }

    private static Cipher a() throws NoSuchAlgorithmException, NoSuchPaddingException {
        Cipher cipher;
        synchronized (d) {
            if (f2587b == null) {
                f2587b = Cipher.getInstance("AES/CBC/PKCS5Padding");
            }
            cipher = f2587b;
        }
        return cipher;
    }

    public final String a(byte[] bArr, byte[] bArr2) throws ahk {
        byte[] doFinal;
        byte[] iv;
        if (bArr.length != 16) {
            throw new ahk(this);
        }
        try {
            SecretKeySpec secretKeySpec = new SecretKeySpec(bArr, "AES");
            synchronized (c) {
                a().init(1, secretKeySpec, null);
                doFinal = a().doFinal(bArr2);
                iv = a().getIV();
            }
            int length = doFinal.length + iv.length;
            ByteBuffer allocate = ByteBuffer.allocate(length);
            allocate.put(iv).put(doFinal);
            allocate.flip();
            byte[] bArr3 = new byte[length];
            allocate.get(bArr3);
            return agg.a(bArr3, false);
        } catch (NoSuchAlgorithmException e) {
            throw new ahk(this, e);
        } catch (InvalidKeyException e2) {
            throw new ahk(this, e2);
        } catch (IllegalBlockSizeException e3) {
            throw new ahk(this, e3);
        } catch (NoSuchPaddingException e4) {
            throw new ahk(this, e4);
        } catch (BadPaddingException e5) {
            throw new ahk(this, e5);
        }
    }

    public final byte[] a(String str) throws ahk {
        try {
            byte[] a2 = agg.a(str, false);
            if (a2.length != 32) {
                throw new ahk(this);
            }
            byte[] bArr = new byte[16];
            ByteBuffer.wrap(a2, 4, 16).get(bArr);
            for (int i = 0; i < 16; i++) {
                bArr[i] = (byte) (bArr[i] ^ 68);
            }
            return bArr;
        } catch (IllegalArgumentException e) {
            throw new ahk(this, e);
        }
    }

    public final byte[] a(byte[] bArr, String str) throws ahk {
        byte[] doFinal;
        if (bArr.length != 16) {
            throw new ahk(this);
        }
        try {
            byte[] a2 = agg.a(str, false);
            if (a2.length <= 16) {
                throw new ahk(this);
            }
            ByteBuffer allocate = ByteBuffer.allocate(a2.length);
            allocate.put(a2);
            allocate.flip();
            byte[] bArr2 = new byte[16];
            byte[] bArr3 = new byte[(a2.length - 16)];
            allocate.get(bArr2);
            allocate.get(bArr3);
            SecretKeySpec secretKeySpec = new SecretKeySpec(bArr, "AES");
            synchronized (c) {
                a().init(2, secretKeySpec, new IvParameterSpec(bArr2));
                doFinal = a().doFinal(bArr3);
            }
            return doFinal;
        } catch (NoSuchAlgorithmException e) {
            throw new ahk(this, e);
        } catch (InvalidKeyException e2) {
            throw new ahk(this, e2);
        } catch (IllegalBlockSizeException e3) {
            throw new ahk(this, e3);
        } catch (NoSuchPaddingException e4) {
            throw new ahk(this, e4);
        } catch (BadPaddingException e5) {
            throw new ahk(this, e5);
        } catch (InvalidAlgorithmParameterException e6) {
            throw new ahk(this, e6);
        } catch (IllegalArgumentException e7) {
            throw new ahk(this, e7);
        }
    }
}
