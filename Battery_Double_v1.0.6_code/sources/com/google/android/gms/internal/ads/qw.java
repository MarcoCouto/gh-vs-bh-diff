package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.ads.internal.aq;
import com.google.android.gms.ads.internal.ax;
import com.google.android.gms.ads.internal.bu;
import java.util.concurrent.Callable;

final /* synthetic */ class qw implements Callable {

    /* renamed from: a reason: collision with root package name */
    private final Context f3648a;

    /* renamed from: b reason: collision with root package name */
    private final sb f3649b;
    private final String c;
    private final boolean d;
    private final boolean e;
    private final ahh f;
    private final mu g;
    private final asv h;
    private final aq i;
    private final bu j;
    private final amw k;

    qw(Context context, sb sbVar, String str, boolean z, boolean z2, ahh ahh, mu muVar, asv asv, aq aqVar, bu buVar, amw amw) {
        this.f3648a = context;
        this.f3649b = sbVar;
        this.c = str;
        this.d = z;
        this.e = z2;
        this.f = ahh;
        this.g = muVar;
        this.h = asv;
        this.i = aqVar;
        this.j = buVar;
        this.k = amw;
    }

    public final Object call() {
        Context context = this.f3648a;
        sb sbVar = this.f3649b;
        String str = this.c;
        boolean z = this.d;
        boolean z2 = this.e;
        qz qzVar = new qz(ra.a(context, sbVar, str, z, z2, this.f, this.g, this.h, this.i, this.j, this.k));
        qzVar.setWebViewClient(ax.g().a((qn) qzVar, z2));
        qzVar.setWebChromeClient(new qf(qzVar));
        return qzVar;
    }
}
