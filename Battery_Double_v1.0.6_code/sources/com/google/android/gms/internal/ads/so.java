package com.google.android.gms.internal.ads;

import android.annotation.TargetApi;
import android.graphics.Canvas;
import android.view.MotionEvent;
import android.webkit.ValueCallback;
import com.google.android.gms.ads.internal.ax;

@cm
public class so extends sl {

    /* renamed from: b reason: collision with root package name */
    private boolean f3693b;
    private boolean c;

    public so(sa saVar) {
        super(saVar);
        ax.i().i();
    }

    private final synchronized void a() {
        if (!this.c) {
            this.c = true;
            ax.i().j();
        }
    }

    public final synchronized boolean A() {
        return this.f3693b;
    }

    public final synchronized void B() {
        jm.a("Destroying WebView!");
        a();
        nt.f3558a.execute(new sp(this));
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ void L() {
        super.destroy();
    }

    public final synchronized void a(sn snVar) {
        if (A()) {
            jm.a("Blank page loaded, 1...");
            B();
        } else {
            super.a(snVar);
        }
    }

    public synchronized void destroy() {
        if (!this.f3693b) {
            this.f3693b = true;
            f(false);
            jm.a("Initiating WebView self destruct sequence in 3...");
            jm.a("Loading blank page in WebView, 2...");
            try {
                super.loadUrl("about:blank");
            } catch (UnsatisfiedLinkError e) {
                ax.i().a((Throwable) e, "AdWebViewImpl.loadUrlUnsafe");
                jm.d("#007 Could not call remote method.", e);
            }
        }
        return;
    }

    @TargetApi(19)
    public synchronized void evaluateJavascript(String str, ValueCallback<String> valueCallback) {
        if (A()) {
            jm.e("#004 The webview is destroyed. Ignoring action.");
            if (valueCallback != null) {
                valueCallback.onReceiveValue(null);
            }
        } else {
            super.evaluateJavascript(str, valueCallback);
        }
    }

    /* access modifiers changed from: protected */
    public void f(boolean z) {
    }

    /* access modifiers changed from: protected */
    public void finalize() throws Throwable {
        try {
            synchronized (this) {
                if (!A()) {
                    f(true);
                }
                a();
            }
        } finally {
            super.finalize();
        }
    }

    public synchronized void loadData(String str, String str2, String str3) {
        if (!A()) {
            super.loadData(str, str2, str3);
        } else {
            jm.e("#004 The webview is destroyed. Ignoring action.");
        }
    }

    public synchronized void loadDataWithBaseURL(String str, String str2, String str3, String str4, String str5) {
        if (!A()) {
            super.loadDataWithBaseURL(str, str2, str3, str4, str5);
        } else {
            jm.e("#004 The webview is destroyed. Ignoring action.");
        }
    }

    public synchronized void loadUrl(String str) {
        if (!A()) {
            super.loadUrl(str);
        } else {
            jm.e("#004 The webview is destroyed. Ignoring action.");
        }
    }

    /* access modifiers changed from: protected */
    @TargetApi(21)
    public void onDraw(Canvas canvas) {
        if (!A()) {
            super.onDraw(canvas);
        }
    }

    public void onPause() {
        if (!A()) {
            super.onPause();
        }
    }

    public void onResume() {
        if (!A()) {
            super.onResume();
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        return !A() && super.onTouchEvent(motionEvent);
    }

    public void stopLoading() {
        if (!A()) {
            super.stopLoading();
        }
    }
}
