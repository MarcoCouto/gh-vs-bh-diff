package com.google.android.gms.internal.ads;

public class acd {

    /* renamed from: a reason: collision with root package name */
    private static final abc f2449a = abc.a();

    /* renamed from: b reason: collision with root package name */
    private aah f2450b;
    private volatile acw c;
    private volatile aah d;

    private final acw b(acw acw) {
        if (this.c == null) {
            synchronized (this) {
                if (this.c == null) {
                    try {
                        this.c = acw;
                        this.d = aah.f2393a;
                    } catch (abv e) {
                        this.c = acw;
                        this.d = aah.f2393a;
                    }
                }
            }
        }
        return this.c;
    }

    public final acw a(acw acw) {
        acw acw2 = this.c;
        this.f2450b = null;
        this.d = null;
        this.c = acw;
        return acw2;
    }

    public final int b() {
        if (this.d != null) {
            return this.d.a();
        }
        if (this.c != null) {
            return this.c.l();
        }
        return 0;
    }

    public final aah c() {
        if (this.d != null) {
            return this.d;
        }
        synchronized (this) {
            if (this.d != null) {
                aah aah = this.d;
                return aah;
            }
            if (this.c == null) {
                this.d = aah.f2393a;
            } else {
                this.d = this.c.h();
            }
            aah aah2 = this.d;
            return aah2;
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof acd)) {
            return false;
        }
        acd acd = (acd) obj;
        acw acw = this.c;
        acw acw2 = acd.c;
        return (acw == null && acw2 == null) ? c().equals(acd.c()) : (acw == null || acw2 == null) ? acw != null ? acw.equals(acd.b(acw.p())) : b(acw2.p()).equals(acw2) : acw.equals(acw2);
    }

    public int hashCode() {
        return 1;
    }
}
