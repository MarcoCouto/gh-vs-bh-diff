package com.google.android.gms.internal.ads;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicInteger;

final class bg implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ AtomicInteger f3185a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ int f3186b;
    private final /* synthetic */ ny c;
    private final /* synthetic */ List d;

    bg(AtomicInteger atomicInteger, int i, ny nyVar, List list) {
        this.f3185a = atomicInteger;
        this.f3186b = i;
        this.c = nyVar;
        this.d = list;
    }

    public final void run() {
        if (this.f3185a.incrementAndGet() >= this.f3186b) {
            try {
                this.c.b(ay.b(this.d));
            } catch (InterruptedException | ExecutionException e) {
                jm.c("Unable to convert list of futures to a future of list", e);
            }
        }
    }
}
