package com.google.android.gms.internal.ads;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public final class bct extends ajk implements bcr {
    bct(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.mediation.client.IAdapterCreator");
    }

    public final bcu a(String str) throws RemoteException {
        bcu bcw;
        Parcel r_ = r_();
        r_.writeString(str);
        Parcel a2 = a(1, r_);
        IBinder readStrongBinder = a2.readStrongBinder();
        if (readStrongBinder == null) {
            bcw = null;
        } else {
            IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.ads.internal.mediation.client.IMediationAdapter");
            bcw = queryLocalInterface instanceof bcu ? (bcu) queryLocalInterface : new bcw(readStrongBinder);
        }
        a2.recycle();
        return bcw;
    }

    public final boolean b(String str) throws RemoteException {
        Parcel r_ = r_();
        r_.writeString(str);
        Parcel a2 = a(2, r_);
        boolean a3 = ajm.a(a2);
        a2.recycle();
        return a3;
    }

    public final beg c(String str) throws RemoteException {
        Parcel r_ = r_();
        r_.writeString(str);
        Parcel a2 = a(3, r_);
        beg a3 = beh.a(a2.readStrongBinder());
        a2.recycle();
        return a3;
    }
}
