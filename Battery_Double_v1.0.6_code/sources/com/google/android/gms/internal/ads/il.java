package com.google.android.gms.internal.ads;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.ax;

@cm
public final class il {
    public static Uri a(Uri uri, Context context) {
        if (!ax.B().f(context) || !TextUtils.isEmpty(uri.getQueryParameter("fbs_aeid"))) {
            return uri;
        }
        String j = ax.B().j(context);
        Uri a2 = a(uri.toString(), "fbs_aeid", j);
        ax.B().d(context, j);
        return a2;
    }

    private static Uri a(String str, String str2, String str3) {
        int indexOf = str.indexOf("&adurl");
        if (indexOf == -1) {
            indexOf = str.indexOf("?adurl");
        }
        return indexOf != -1 ? Uri.parse(new StringBuilder(str.substring(0, indexOf + 1)).append(str2).append("=").append(str3).append("&").append(str.substring(indexOf + 1)).toString()) : Uri.parse(str).buildUpon().appendQueryParameter(str2, str3).build();
    }

    public static String a(String str, Context context) {
        if (!ax.B().a(context) || TextUtils.isEmpty(str)) {
            return str;
        }
        String j = ax.B().j(context);
        if (j == null) {
            return str;
        }
        if (((Boolean) ape.f().a(asi.at)).booleanValue()) {
            String str2 = (String) ape.f().a(asi.au);
            if (!str.contains(str2)) {
                return str;
            }
            if (ax.e().d(str)) {
                ax.B().d(context, j);
                return str.replace(str2, j);
            } else if (!ax.e().e(str)) {
                return str;
            } else {
                ax.B().e(context, j);
                return str.replace(str2, j);
            }
        } else if (str.contains("fbs_aeid")) {
            return str;
        } else {
            if (ax.e().d(str)) {
                ax.B().d(context, j);
                return a(str, "fbs_aeid", j).toString();
            } else if (!ax.e().e(str)) {
                return str;
            } else {
                ax.B().e(context, j);
                return a(str, "fbs_aeid", j).toString();
            }
        }
    }

    public static String b(String str, Context context) {
        if (!ax.B().a(context) || TextUtils.isEmpty(str)) {
            return str;
        }
        String j = ax.B().j(context);
        if (j == null || !ax.e().e(str)) {
            return str;
        }
        if (!((Boolean) ape.f().a(asi.at)).booleanValue()) {
            return !str.contains("fbs_aeid") ? a(str, "fbs_aeid", j).toString() : str;
        }
        String str2 = (String) ape.f().a(asi.au);
        return str.contains(str2) ? str.replace(str2, j) : str;
    }
}
