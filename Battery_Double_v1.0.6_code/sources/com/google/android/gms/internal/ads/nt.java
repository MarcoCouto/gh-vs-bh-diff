package com.google.android.gms.internal.ads;

import java.util.concurrent.Executor;

@cm
public final class nt {

    /* renamed from: a reason: collision with root package name */
    public static final Executor f3558a = new nu();

    /* renamed from: b reason: collision with root package name */
    public static final Executor f3559b = new nv();
    private static final ns c = a(f3558a);
    private static final ns d = a(f3559b);

    public static ns a(Executor executor) {
        return new nw(executor, null);
    }
}
