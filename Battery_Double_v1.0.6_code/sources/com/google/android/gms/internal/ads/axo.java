package com.google.android.gms.internal.ads;

import android.os.RemoteException;

final /* synthetic */ class axo implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final axn f2985a;

    /* renamed from: b reason: collision with root package name */
    private final axe f2986b;
    private final ny c;
    private final axf d;

    axo(axn axn, axe axe, ny nyVar, axf axf) {
        this.f2985a = axn;
        this.f2986b = axe;
        this.c = nyVar;
        this.d = axf;
    }

    public final void run() {
        axn axn = this.f2985a;
        axe axe = this.f2986b;
        ny nyVar = this.c;
        try {
            nyVar.b(axe.A().a(this.d));
        } catch (RemoteException e) {
            jm.b("Unable to obtain a cache service instance.", e);
            nyVar.a(e);
            axn.f2983a.a();
        }
    }
}
