package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.os.DeadObjectException;
import com.google.android.gms.common.internal.e.a;

final class amk implements a {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ amh f2734a;

    amk(amh amh) {
        this.f2734a = amh;
    }

    public final void a(int i) {
        synchronized (this.f2734a.f2731b) {
            this.f2734a.e = null;
            this.f2734a.f2731b.notifyAll();
        }
    }

    public final void a(Bundle bundle) {
        synchronized (this.f2734a.f2731b) {
            try {
                if (this.f2734a.c != null) {
                    this.f2734a.e = this.f2734a.c.A();
                }
            } catch (DeadObjectException e) {
                jm.b("Unable to obtain a cache service instance.", e);
                this.f2734a.c();
            }
            this.f2734a.f2731b.notifyAll();
        }
    }
}
