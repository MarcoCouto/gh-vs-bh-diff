package com.google.android.gms.internal.ads;

import android.app.AlertDialog.Builder;
import android.content.Context;

final class kx implements Runnable {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ Context f3465a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ String f3466b;
    private final /* synthetic */ boolean c;
    private final /* synthetic */ boolean d;

    kx(kw kwVar, Context context, String str, boolean z, boolean z2) {
        this.f3465a = context;
        this.f3466b = str;
        this.c = z;
        this.d = z2;
    }

    public final void run() {
        Builder builder = new Builder(this.f3465a);
        builder.setMessage(this.f3466b);
        if (this.c) {
            builder.setTitle("Error");
        } else {
            builder.setTitle("Info");
        }
        if (this.d) {
            builder.setNeutralButton("Dismiss", null);
        } else {
            builder.setPositiveButton("Learn More", new ky(this));
            builder.setNegativeButton("Dismiss", null);
        }
        builder.create().show();
    }
}
