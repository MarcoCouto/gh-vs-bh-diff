package com.google.android.gms.internal.ads;

import java.util.concurrent.Callable;

final class bcm implements Callable<bci> {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ bcf f3136a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ bcl f3137b;

    bcm(bcl bcl, bcf bcf) {
        this.f3137b = bcl;
        this.f3136a = bcf;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public final bci call() throws Exception {
        synchronized (this.f3137b.i) {
            if (this.f3137b.j) {
                return null;
            }
            return this.f3136a.a(this.f3137b.f, this.f3137b.g);
        }
    }
}
