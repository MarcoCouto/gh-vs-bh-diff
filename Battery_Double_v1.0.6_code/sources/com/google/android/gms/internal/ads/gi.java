package com.google.android.gms.internal.ads;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Map;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.conn.ConnectTimeoutException;

final class gi extends hi {

    /* renamed from: a reason: collision with root package name */
    private final qr f3341a;

    gi(qr qrVar) {
        this.f3341a = qrVar;
    }

    public final pq a(awb<?> awb, Map<String, String> map) throws IOException, a {
        try {
            HttpResponse b2 = this.f3341a.b(awb, map);
            int statusCode = b2.getStatusLine().getStatusCode();
            Header[] allHeaders = b2.getAllHeaders();
            ArrayList arrayList = new ArrayList(allHeaders.length);
            for (Header header : allHeaders) {
                arrayList.add(new aqd(header.getName(), header.getValue()));
            }
            if (b2.getEntity() == null) {
                return new pq(statusCode, arrayList);
            }
            long contentLength = b2.getEntity().getContentLength();
            if (((long) ((int) contentLength)) == contentLength) {
                return new pq(statusCode, arrayList, (int) b2.getEntity().getContentLength(), b2.getEntity().getContent());
            }
            throw new IOException("Response too large: " + contentLength);
        } catch (ConnectTimeoutException e) {
            throw new SocketTimeoutException(e.getMessage());
        }
    }
}
