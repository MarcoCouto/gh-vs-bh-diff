package com.google.android.gms.internal.ads;

import java.io.IOException;

final class aad {
    static int a(int i, byte[] bArr, int i2, int i3, aae aae) throws abv {
        int i4;
        if ((i >>> 3) == 0) {
            throw abv.d();
        }
        switch (i & 7) {
            case 0:
                return b(bArr, i2, aae);
            case 1:
                return i2 + 8;
            case 2:
                return a(bArr, i2, aae) + aae.f2389a;
            case 3:
                int i5 = (i & -8) | 4;
                int i6 = 0;
                int i7 = i2;
                while (i7 < i3) {
                    i7 = a(bArr, i7, aae);
                    i6 = aae.f2389a;
                    if (i6 != i5) {
                        i7 = a(i6, bArr, i7, i3, aae);
                    } else {
                        int i8 = i6;
                        i4 = i7;
                        int i9 = i8;
                        if (i4 > i3 && i9 == i5) {
                            return i4;
                        }
                        throw abv.g();
                    }
                }
                int i82 = i6;
                i4 = i7;
                int i92 = i82;
                if (i4 > i3) {
                }
                throw abv.g();
            case 5:
                return i2 + 4;
            default:
                throw abv.d();
        }
    }

    static int a(int i, byte[] bArr, int i2, int i3, abu<?> abu, aae aae) {
        abq abq = (abq) abu;
        int a2 = a(bArr, i2, aae);
        abq.c(aae.f2389a);
        while (a2 < i3) {
            int a3 = a(bArr, a2, aae);
            if (i != aae.f2389a) {
                break;
            }
            a2 = a(bArr, a3, aae);
            abq.c(aae.f2389a);
        }
        return a2;
    }

    static int a(int i, byte[] bArr, int i2, int i3, aej aej, aae aae) throws IOException {
        int i4;
        if ((i >>> 3) == 0) {
            throw abv.d();
        }
        switch (i & 7) {
            case 0:
                int b2 = b(bArr, i2, aae);
                aej.a(i, (Object) Long.valueOf(aae.f2390b));
                return b2;
            case 1:
                aej.a(i, (Object) Long.valueOf(b(bArr, i2)));
                return i2 + 8;
            case 2:
                int a2 = a(bArr, i2, aae);
                int i5 = aae.f2389a;
                if (i5 == 0) {
                    aej.a(i, (Object) aah.f2393a);
                } else {
                    aej.a(i, (Object) aah.a(bArr, a2, i5));
                }
                return a2 + i5;
            case 3:
                aej b3 = aej.b();
                int i6 = (i & -8) | 4;
                int i7 = 0;
                int i8 = i2;
                while (i8 < i3) {
                    i8 = a(bArr, i8, aae);
                    i7 = aae.f2389a;
                    if (i7 != i6) {
                        i8 = a(i7, bArr, i8, i3, b3, aae);
                    } else {
                        int i9 = i7;
                        i4 = i8;
                        if (i4 <= i3 || i9 != i6) {
                            throw abv.g();
                        }
                        aej.a(i, (Object) b3);
                        return i4;
                    }
                }
                int i92 = i7;
                i4 = i8;
                if (i4 <= i3) {
                }
                throw abv.g();
            case 5:
                aej.a(i, (Object) Integer.valueOf(a(bArr, i2)));
                return i2 + 4;
            default:
                throw abv.d();
        }
    }

    static int a(int i, byte[] bArr, int i2, aae aae) {
        int i3 = i & 127;
        int i4 = i2 + 1;
        byte b2 = bArr[i2];
        if (b2 >= 0) {
            aae.f2389a = i3 | (b2 << 7);
            return i4;
        }
        int i5 = ((b2 & Byte.MAX_VALUE) << 7) | i3;
        int i6 = i4 + 1;
        byte b3 = bArr[i4];
        if (b3 >= 0) {
            aae.f2389a = (b3 << 14) | i5;
            return i6;
        }
        int i7 = ((b3 & Byte.MAX_VALUE) << 14) | i5;
        int i8 = i6 + 1;
        byte b4 = bArr[i6];
        if (b4 >= 0) {
            aae.f2389a = i7 | (b4 << 21);
            return i8;
        }
        int i9 = ((b4 & Byte.MAX_VALUE) << 21) | i7;
        int i10 = i8 + 1;
        byte b5 = bArr[i8];
        if (b5 >= 0) {
            aae.f2389a = i9 | (b5 << 28);
            return i10;
        }
        int i11 = ((b5 & Byte.MAX_VALUE) << 28) | i9;
        while (true) {
            int i12 = i10 + 1;
            if (bArr[i10] >= 0) {
                aae.f2389a = i11;
                return i12;
            }
            i10 = i12;
        }
    }

    static int a(byte[] bArr, int i) {
        return (bArr[i] & 255) | ((bArr[i + 1] & 255) << 8) | ((bArr[i + 2] & 255) << 16) | ((bArr[i + 3] & 255) << 24);
    }

    static int a(byte[] bArr, int i, aae aae) {
        int i2 = i + 1;
        byte b2 = bArr[i];
        if (b2 < 0) {
            return a((int) b2, bArr, i2, aae);
        }
        aae.f2389a = b2;
        return i2;
    }

    static int a(byte[] bArr, int i, abu<?> abu, aae aae) throws IOException {
        abq abq = (abq) abu;
        int a2 = a(bArr, i, aae);
        int i2 = aae.f2389a + a2;
        while (a2 < i2) {
            a2 = a(bArr, a2, aae);
            abq.c(aae.f2389a);
        }
        if (a2 == i2) {
            return a2;
        }
        throw abv.a();
    }

    static int b(byte[] bArr, int i, aae aae) {
        int i2 = 7;
        int i3 = i + 1;
        long j = (long) bArr[i];
        if (j >= 0) {
            aae.f2390b = j;
        } else {
            long j2 = j & 127;
            int i4 = i3 + 1;
            byte b2 = bArr[i3];
            byte b3 = b2;
            i3 = i4;
            long j3 = j2 | (((long) (b2 & Byte.MAX_VALUE)) << 7);
            byte b4 = b3;
            while (b4 < 0) {
                int i5 = i3 + 1;
                b4 = bArr[i3];
                int i6 = i2 + 7;
                j3 |= ((long) (b4 & Byte.MAX_VALUE)) << i6;
                i2 = i6;
                i3 = i5;
            }
            aae.f2390b = j3;
        }
        return i3;
    }

    static long b(byte[] bArr, int i) {
        return (((long) bArr[i]) & 255) | ((((long) bArr[i + 1]) & 255) << 8) | ((((long) bArr[i + 2]) & 255) << 16) | ((((long) bArr[i + 3]) & 255) << 24) | ((((long) bArr[i + 4]) & 255) << 32) | ((((long) bArr[i + 5]) & 255) << 40) | ((((long) bArr[i + 6]) & 255) << 48) | ((((long) bArr[i + 7]) & 255) << 56);
    }

    static double c(byte[] bArr, int i) {
        return Double.longBitsToDouble(b(bArr, i));
    }

    static int c(byte[] bArr, int i, aae aae) {
        int a2 = a(bArr, i, aae);
        int i2 = aae.f2389a;
        if (i2 == 0) {
            aae.c = "";
            return a2;
        }
        aae.c = new String(bArr, a2, i2, abr.f2440a);
        return a2 + i2;
    }

    static float d(byte[] bArr, int i) {
        return Float.intBitsToFloat(a(bArr, i));
    }

    static int d(byte[] bArr, int i, aae aae) throws IOException {
        int a2 = a(bArr, i, aae);
        int i2 = aae.f2389a;
        if (i2 == 0) {
            aae.c = "";
            return a2;
        } else if (!aeq.a(bArr, a2, a2 + i2)) {
            throw abv.h();
        } else {
            aae.c = new String(bArr, a2, i2, abr.f2440a);
            return a2 + i2;
        }
    }

    static int e(byte[] bArr, int i, aae aae) {
        int a2 = a(bArr, i, aae);
        int i2 = aae.f2389a;
        if (i2 == 0) {
            aae.c = aah.f2393a;
            return a2;
        }
        aae.c = aah.a(bArr, a2, i2);
        return a2 + i2;
    }
}
