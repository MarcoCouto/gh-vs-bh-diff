package com.google.android.gms.internal.ads;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;

public final class bed extends ajk implements bec {
    bed(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.mediation.client.rtb.IBannerCallback");
    }

    public final void a(String str) throws RemoteException {
        Parcel r_ = r_();
        r_.writeString(str);
        b(2, r_);
    }
}
