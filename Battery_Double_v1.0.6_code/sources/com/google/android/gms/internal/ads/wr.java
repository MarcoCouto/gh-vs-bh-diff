package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.abp.b;

public final class wr extends abp<wr, a> implements acy {
    private static volatile adi<wr> zzakh;
    /* access modifiers changed from: private */
    public static final wr zzdjx = new wr();
    private int zzdju;
    private int zzdjv;
    private aah zzdjw = aah.f2393a;

    public static final class a extends com.google.android.gms.internal.ads.abp.a<wr, a> implements acy {
        private a() {
            super(wr.zzdjx);
        }

        /* synthetic */ a(ws wsVar) {
            this();
        }
    }

    static {
        abp.a(wr.class, zzdjx);
    }

    private wr() {
    }

    public static wr d() {
        return zzdjx;
    }

    public final wt a() {
        wt a2 = wt.a(this.zzdju);
        return a2 == null ? wt.UNRECOGNIZED : a2;
    }

    /* JADX WARNING: type inference failed for: r0v6, types: [com.google.android.gms.internal.ads.adi<com.google.android.gms.internal.ads.wr>] */
    /* JADX WARNING: type inference failed for: r0v7, types: [java.lang.Object] */
    /* JADX WARNING: type inference failed for: r0v8, types: [com.google.android.gms.internal.ads.abp$b, com.google.android.gms.internal.ads.adi<com.google.android.gms.internal.ads.wr>] */
    /* JADX WARNING: type inference failed for: r0v16 */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r0v8, types: [com.google.android.gms.internal.ads.abp$b, com.google.android.gms.internal.ads.adi<com.google.android.gms.internal.ads.wr>]
  assigns: [com.google.android.gms.internal.ads.abp$b]
  uses: [com.google.android.gms.internal.ads.adi<com.google.android.gms.internal.ads.wr>]
  mth insns count: 42
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
    	at jadx.core.ProcessClass.process(ProcessClass.java:35)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Unknown variable types count: 2 */
    public final Object a(int i, Object obj, Object obj2) {
        ? r0;
        switch (ws.f3762a[i - 1]) {
            case 1:
                return new wr();
            case 2:
                return new a(null);
            case 3:
                Object[] objArr = {"zzdju", "zzdjv", "zzdjw"};
                return a((acw) zzdjx, "\u0000\u0003\u0000\u0000\u0001\u000b\u000b\f\u0000\u0000\u0000\u0001\f\u0002\f\u000b\n", objArr);
            case 4:
                return zzdjx;
            case 5:
                adi<wr> adi = zzakh;
                if (adi != null) {
                    return adi;
                }
                synchronized (wr.class) {
                    r0 = zzakh;
                    if (r0 == 0) {
                        ? bVar = new b(zzdjx);
                        zzakh = bVar;
                        r0 = bVar;
                    }
                }
                return r0;
            case 6:
                return Byte.valueOf(1);
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    public final ww b() {
        ww a2 = ww.a(this.zzdjv);
        return a2 == null ? ww.UNRECOGNIZED : a2;
    }

    public final aah c() {
        return this.zzdjw;
    }
}
