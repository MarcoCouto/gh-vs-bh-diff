package com.google.android.gms.internal.ads;

import android.webkit.ValueCallback;
import android.webkit.WebView;

final class alp implements Runnable {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ alh f2705a;

    /* renamed from: b reason: collision with root package name */
    final /* synthetic */ WebView f2706b;
    final /* synthetic */ boolean c;
    final /* synthetic */ aln d;
    private ValueCallback<String> e = new alq(this);

    alp(aln aln, alh alh, WebView webView, boolean z) {
        this.d = aln;
        this.f2705a = alh;
        this.f2706b = webView;
        this.c = z;
    }

    public final void run() {
        if (this.f2706b.getSettings().getJavaScriptEnabled()) {
            try {
                this.f2706b.evaluateJavascript("(function() { return  {text:document.body.innerText}})();", this.e);
            } catch (Throwable th) {
                this.e.onReceiveValue("");
            }
        }
    }
}
