package com.google.android.gms.internal.ads;

@cm
public final class sb {

    /* renamed from: a reason: collision with root package name */
    public final int f3672a;

    /* renamed from: b reason: collision with root package name */
    public final int f3673b;
    private final int c;

    private sb(int i, int i2, int i3) {
        this.c = i;
        this.f3673b = i2;
        this.f3672a = i3;
    }

    public static sb a() {
        return new sb(0, 0, 0);
    }

    public static sb a(int i, int i2) {
        return new sb(1, i, i2);
    }

    public static sb a(aot aot) {
        return aot.d ? new sb(3, 0, 0) : aot.i ? new sb(2, 0, 0) : aot.h ? a() : a(aot.f, aot.c);
    }

    public static sb b() {
        return new sb(4, 0, 0);
    }

    public final boolean c() {
        return this.c == 2;
    }

    public final boolean d() {
        return this.c == 3;
    }

    public final boolean e() {
        return this.c == 0;
    }

    public final boolean f() {
        return this.c == 4;
    }
}
