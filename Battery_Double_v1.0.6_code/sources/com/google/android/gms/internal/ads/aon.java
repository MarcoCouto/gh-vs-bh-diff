package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.b.b;
import com.google.android.gms.b.c;
import com.google.android.gms.b.c.a;

@cm
public final class aon extends c<apy> {
    public aon() {
        super("com.google.android.gms.ads.AdManagerCreatorImpl");
    }

    public final apv a(Context context, aot aot, String str, bcr bcr, int i) {
        try {
            IBinder a2 = ((apy) a(context)).a(b.a(context), aot, str, bcr, 12451000, i);
            if (a2 == null) {
                return null;
            }
            IInterface queryLocalInterface = a2.queryLocalInterface("com.google.android.gms.ads.internal.client.IAdManager");
            return queryLocalInterface instanceof apv ? (apv) queryLocalInterface : new apx(a2);
        } catch (RemoteException | a e) {
            ms.a("Could not create remote AdManager.", e);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.IAdManagerCreator");
        return queryLocalInterface instanceof apy ? (apy) queryLocalInterface : new apz(iBinder);
    }
}
