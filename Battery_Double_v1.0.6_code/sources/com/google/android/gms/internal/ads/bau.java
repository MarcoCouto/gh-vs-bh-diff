package com.google.android.gms.internal.ads;

public final class bau extends oe<bbe> {

    /* renamed from: a reason: collision with root package name */
    private final Object f3087a = new Object();
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public final bay f3088b;
    private boolean c;

    public bau(bay bay) {
        this.f3088b = bay;
    }

    public final void c() {
        synchronized (this.f3087a) {
            if (!this.c) {
                this.c = true;
                a(new bav(this), new oc());
                a(new baw(this), new bax(this));
            }
        }
    }
}
