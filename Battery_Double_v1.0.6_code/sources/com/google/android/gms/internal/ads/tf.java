package com.google.android.gms.internal.ads;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.b.a;

public interface tf extends IInterface {
    a a(String str, a aVar, String str2, String str3, String str4) throws RemoteException;

    String a() throws RemoteException;

    void a(a aVar, a aVar2) throws RemoteException;

    boolean a(a aVar) throws RemoteException;

    void b(a aVar) throws RemoteException;

    void c(a aVar) throws RemoteException;
}
