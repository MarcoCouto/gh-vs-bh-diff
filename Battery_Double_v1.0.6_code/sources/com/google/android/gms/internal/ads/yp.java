package com.google.android.gms.internal.ads;

public final class yp {

    /* renamed from: a reason: collision with root package name */
    private final zf f3799a;

    /* renamed from: b reason: collision with root package name */
    private final zf f3800b;

    public yp(byte[] bArr, byte[] bArr2) {
        this.f3799a = zf.a(bArr);
        this.f3800b = zf.a(bArr2);
    }

    public final byte[] a() {
        if (this.f3799a == null) {
            return null;
        }
        return this.f3799a.a();
    }

    public final byte[] b() {
        if (this.f3800b == null) {
            return null;
        }
        return this.f3800b.a();
    }
}
