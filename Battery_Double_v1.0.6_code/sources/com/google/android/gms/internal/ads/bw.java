package com.google.android.gms.internal.ads;

import org.json.JSONObject;

final /* synthetic */ class bw implements mx {

    /* renamed from: a reason: collision with root package name */
    private final bu f3209a;

    /* renamed from: b reason: collision with root package name */
    private final JSONObject f3210b;

    bw(bu buVar, JSONObject jSONObject) {
        this.f3209a = buVar;
        this.f3210b = jSONObject;
    }

    public final nn a(Object obj) {
        return this.f3209a.d(this.f3210b, (qn) obj);
    }
}
