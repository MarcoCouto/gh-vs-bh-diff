package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.t;
import java.util.concurrent.CountDownLatch;

final class av implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ CountDownLatch f2953a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ au f2954b;

    av(au auVar, CountDownLatch countDownLatch) {
        this.f2954b = auVar;
        this.f2953a = countDownLatch;
    }

    public final void run() {
        synchronized (this.f2954b.d) {
            this.f2954b.m = t.a(this.f2954b.l, this.f2954b.g, this.f2953a);
        }
    }
}
