package com.google.android.gms.internal.ads;

import java.io.IOException;

public final class aga extends afh<aga> {

    /* renamed from: a reason: collision with root package name */
    public String f2553a;

    /* renamed from: b reason: collision with root package name */
    public Long f2554b;
    public Boolean c;

    public aga() {
        this.f2553a = null;
        this.f2554b = null;
        this.c = null;
        this.Y = null;
        this.Z = -1;
    }

    /* access modifiers changed from: protected */
    public final int a() {
        int a2 = super.a();
        if (this.f2553a != null) {
            a2 += aff.b(1, this.f2553a);
        }
        if (this.f2554b != null) {
            a2 += aff.d(2, this.f2554b.longValue());
        }
        if (this.c == null) {
            return a2;
        }
        this.c.booleanValue();
        return a2 + aff.b(3) + 1;
    }

    public final /* synthetic */ afn a(afd afd) throws IOException {
        while (true) {
            int a2 = afd.a();
            switch (a2) {
                case 0:
                    break;
                case 10:
                    this.f2553a = afd.e();
                    continue;
                case 16:
                    this.f2554b = Long.valueOf(afd.b());
                    continue;
                case 24:
                    this.c = Boolean.valueOf(afd.d());
                    continue;
                default:
                    if (!super.a(afd, a2)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    public final void a(aff aff) throws IOException {
        if (this.f2553a != null) {
            aff.a(1, this.f2553a);
        }
        if (this.f2554b != null) {
            aff.b(2, this.f2554b.longValue());
        }
        if (this.c != null) {
            aff.a(3, this.c.booleanValue());
        }
        super.a(aff);
    }
}
