package com.google.android.gms.internal.ads;

import android.text.TextUtils;
import com.google.android.gms.ads.internal.gmsg.ae;
import java.util.Map;

final class rb implements ae<qn> {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ ra f3655a;

    rb(ra raVar) {
        this.f3655a = raVar;
    }

    public final /* synthetic */ void zza(Object obj, Map map) {
        if (map != null) {
            String str = (String) map.get("height");
            if (!TextUtils.isEmpty(str)) {
                try {
                    int parseInt = Integer.parseInt(str);
                    synchronized (this.f3655a) {
                        if (this.f3655a.B != parseInt) {
                            this.f3655a.B = parseInt;
                            this.f3655a.requestLayout();
                        }
                    }
                } catch (Exception e) {
                    jm.c("Exception occurred while getting webview content height", e);
                }
            }
        }
    }
}
