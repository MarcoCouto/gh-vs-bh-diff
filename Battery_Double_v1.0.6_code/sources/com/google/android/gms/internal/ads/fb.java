package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.gmsg.ae;
import java.util.Map;

final class fb implements ae<Object> {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ fa f3305a;

    fb(fa faVar) {
        this.f3305a = faVar;
    }

    public final void zza(Object obj, Map<String, String> map) {
        synchronized (this.f3305a.f3304b) {
            if (!this.f3305a.e.isDone()) {
                if (this.f3305a.c.equals(map.get("request_id"))) {
                    fg fgVar = new fg(1, map);
                    String f = fgVar.f();
                    String valueOf = String.valueOf(fgVar.b());
                    jm.e(new StringBuilder(String.valueOf(f).length() + 24 + String.valueOf(valueOf).length()).append("Invalid ").append(f).append(" request error: ").append(valueOf).toString());
                    this.f3305a.e.b(fgVar);
                }
            }
        }
    }
}
