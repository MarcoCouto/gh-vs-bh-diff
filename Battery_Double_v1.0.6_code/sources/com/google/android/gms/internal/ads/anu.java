package com.google.android.gms.internal.ads;

import java.io.IOException;

public final class anu extends afh<anu> {

    /* renamed from: a reason: collision with root package name */
    private Integer f2778a;

    /* renamed from: b reason: collision with root package name */
    private Integer f2779b;

    public anu() {
        this.f2778a = null;
        this.f2779b = null;
        this.Y = null;
        this.Z = -1;
    }

    /* access modifiers changed from: protected */
    public final int a() {
        int a2 = super.a();
        if (this.f2778a != null) {
            a2 += aff.b(1, this.f2778a.intValue());
        }
        return this.f2779b != null ? a2 + aff.b(2, this.f2779b.intValue()) : a2;
    }

    public final /* synthetic */ afn a(afd afd) throws IOException {
        while (true) {
            int a2 = afd.a();
            switch (a2) {
                case 0:
                    break;
                case 8:
                    this.f2778a = Integer.valueOf(afd.g());
                    continue;
                case 16:
                    this.f2779b = Integer.valueOf(afd.g());
                    continue;
                default:
                    if (!super.a(afd, a2)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    public final void a(aff aff) throws IOException {
        if (this.f2778a != null) {
            aff.a(1, this.f2778a.intValue());
        }
        if (this.f2779b != null) {
            aff.a(2, this.f2779b.intValue());
        }
        super.a(aff);
    }
}
