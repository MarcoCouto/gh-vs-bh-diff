package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.ads.internal.ae;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

@cm
public final class aw extends jh {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public final ap f2961a;

    /* renamed from: b reason: collision with root package name */
    private final dp f2962b;
    private final is c;
    private final ay d;
    private final Object e;
    private Future<ir> f;

    public aw(Context context, ae aeVar, is isVar, ahh ahh, ap apVar, asv asv) {
        this(isVar, apVar, new ay(context, aeVar, new lf(context), ahh, isVar, asv));
    }

    private aw(is isVar, ap apVar, ay ayVar) {
        this.e = new Object();
        this.c = isVar;
        this.f2962b = isVar.f3398b;
        this.f2961a = apVar;
        this.d = ayVar;
    }

    public final void a() {
        ir irVar;
        int i = -2;
        try {
            synchronized (this.e) {
                this.f = jt.a((Callable<T>) this.d);
            }
            irVar = (ir) this.f.get(60000, TimeUnit.MILLISECONDS);
        } catch (TimeoutException e2) {
            jm.e("Timed out waiting for native ad.");
            i = 2;
            this.f.cancel(true);
            irVar = null;
        } catch (ExecutionException e3) {
            i = 0;
            irVar = null;
        } catch (InterruptedException e4) {
            i = 0;
            irVar = null;
        } catch (CancellationException e5) {
            i = 0;
            irVar = null;
        }
        if (irVar == null) {
            irVar = new ir(this.c.f3397a.c, null, null, i, null, null, this.f2962b.k, this.f2962b.j, this.c.f3397a.i, false, null, null, null, null, null, this.f2962b.h, this.c.d, this.f2962b.f, this.c.f, this.f2962b.m, this.f2962b.n, this.c.h, null, null, null, null, this.c.f3398b.D, this.c.f3398b.E, null, null, this.f2962b.L, this.c.i, this.c.f3398b.O, false, this.c.f3398b.Q, null, this.c.f3398b.S, this.c.f3398b.T);
        }
        jv.f3440a.post(new ax(this, irVar));
    }

    public final void c_() {
        synchronized (this.e) {
            if (this.f != null) {
                this.f.cancel(true);
            }
        }
    }
}
