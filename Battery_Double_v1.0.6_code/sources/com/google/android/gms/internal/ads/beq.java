package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.a.a;
import com.google.android.gms.common.internal.a.c;

@cm
public final class beq extends a {
    public static final Creator<beq> CREATOR = new ber();

    /* renamed from: a reason: collision with root package name */
    private final int f3175a;

    /* renamed from: b reason: collision with root package name */
    private final int f3176b;
    private final int c;

    beq(int i, int i2, int i3) {
        this.f3175a = i;
        this.f3176b = i2;
        this.c = i3;
    }

    public static beq a(te teVar) {
        return new beq(teVar.f3704a, teVar.f3705b, teVar.c);
    }

    public final String toString() {
        int i = this.f3175a;
        int i2 = this.f3176b;
        return i + "." + i2 + "." + this.c;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = c.a(parcel);
        c.a(parcel, 1, this.f3175a);
        c.a(parcel, 2, this.f3176b);
        c.a(parcel, 3, this.c);
        c.a(parcel, a2);
    }
}
