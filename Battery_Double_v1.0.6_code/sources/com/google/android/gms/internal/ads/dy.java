package com.google.android.gms.internal.ads;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;

public final class dy extends ajk implements dw {
    dy(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.request.IAdResponseListener");
    }

    public final void a(dp dpVar) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (Parcelable) dpVar);
        b(1, r_);
    }
}
