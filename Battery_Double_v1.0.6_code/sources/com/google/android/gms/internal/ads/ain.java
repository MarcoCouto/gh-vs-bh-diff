package com.google.android.gms.internal.ads;

import java.lang.reflect.InvocationTargetException;

public final class ain extends ajj {
    private static volatile String d = null;
    private static final Object e = new Object();

    public ain(ahy ahy, String str, String str2, zz zzVar, int i, int i2) {
        super(ahy, str, str2, zzVar, i, 29);
    }

    /* access modifiers changed from: protected */
    public final void a() throws IllegalAccessException, InvocationTargetException {
        this.f2637b.o = "E";
        if (d == null) {
            synchronized (e) {
                if (d == null) {
                    d = (String) this.c.invoke(null, new Object[]{this.f2636a.a()});
                }
            }
        }
        synchronized (this.f2637b) {
            this.f2637b.o = agg.a(d.getBytes(), true);
        }
    }
}
