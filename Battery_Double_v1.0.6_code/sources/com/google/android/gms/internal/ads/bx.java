package com.google.android.gms.internal.ads;

import org.json.JSONObject;

final /* synthetic */ class bx implements mx {

    /* renamed from: a reason: collision with root package name */
    private final bu f3211a;

    /* renamed from: b reason: collision with root package name */
    private final JSONObject f3212b;

    bx(bu buVar, JSONObject jSONObject) {
        this.f3211a = buVar;
        this.f3212b = jSONObject;
    }

    public final nn a(Object obj) {
        return this.f3211a.c(this.f3212b, (qn) obj);
    }
}
