package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.abp.b;
import com.google.android.gms.internal.ads.abp.e;

public final class wp extends abp<wp, a> implements acy {
    private static volatile adi<wp> zzakh;
    /* access modifiers changed from: private */
    public static final wp zzdjt = new wp();
    private int zzdih;
    private wl zzdjj;
    private aah zzdjr = aah.f2393a;
    private aah zzdjs = aah.f2393a;

    public static final class a extends com.google.android.gms.internal.ads.abp.a<wp, a> implements acy {
        private a() {
            super(wp.zzdjt);
        }

        /* synthetic */ a(wq wqVar) {
            this();
        }

        public final a a(int i) {
            b();
            ((wp) this.f2433a).b(0);
            return this;
        }

        public final a a(aah aah) {
            b();
            ((wp) this.f2433a).b(aah);
            return this;
        }

        public final a a(wl wlVar) {
            b();
            ((wp) this.f2433a).a(wlVar);
            return this;
        }

        public final a b(aah aah) {
            b();
            ((wp) this.f2433a).c(aah);
            return this;
        }
    }

    static {
        abp.a(wp.class, zzdjt);
    }

    private wp() {
    }

    public static wp a(aah aah) throws abv {
        return (wp) abp.a(zzdjt, aah);
    }

    /* access modifiers changed from: private */
    public final void a(wl wlVar) {
        if (wlVar == null) {
            throw new NullPointerException();
        }
        this.zzdjj = wlVar;
    }

    /* access modifiers changed from: private */
    public final void b(int i) {
        this.zzdih = i;
    }

    /* access modifiers changed from: private */
    public final void b(aah aah) {
        if (aah == null) {
            throw new NullPointerException();
        }
        this.zzdjr = aah;
    }

    /* access modifiers changed from: private */
    public final void c(aah aah) {
        if (aah == null) {
            throw new NullPointerException();
        }
        this.zzdjs = aah;
    }

    public static a e() {
        return (a) ((com.google.android.gms.internal.ads.abp.a) zzdjt.a(e.e, (Object) null, (Object) null));
    }

    public static wp f() {
        return zzdjt;
    }

    public final int a() {
        return this.zzdih;
    }

    /* JADX WARNING: type inference failed for: r0v6, types: [com.google.android.gms.internal.ads.adi<com.google.android.gms.internal.ads.wp>] */
    /* JADX WARNING: type inference failed for: r0v7, types: [java.lang.Object] */
    /* JADX WARNING: type inference failed for: r0v8, types: [com.google.android.gms.internal.ads.abp$b, com.google.android.gms.internal.ads.adi<com.google.android.gms.internal.ads.wp>] */
    /* JADX WARNING: type inference failed for: r0v16 */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r0v8, types: [com.google.android.gms.internal.ads.abp$b, com.google.android.gms.internal.ads.adi<com.google.android.gms.internal.ads.wp>]
  assigns: [com.google.android.gms.internal.ads.abp$b]
  uses: [com.google.android.gms.internal.ads.adi<com.google.android.gms.internal.ads.wp>]
  mth insns count: 44
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
    	at jadx.core.ProcessClass.process(ProcessClass.java:35)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Unknown variable types count: 2 */
    public final Object a(int i, Object obj, Object obj2) {
        ? r0;
        switch (wq.f3761a[i - 1]) {
            case 1:
                return new wp();
            case 2:
                return new a(null);
            case 3:
                Object[] objArr = {"zzdih", "zzdjj", "zzdjr", "zzdjs"};
                return a((acw) zzdjt, "\u0000\u0004\u0000\u0000\u0001\u0004\u0004\u0005\u0000\u0000\u0000\u0001\u000b\u0002\t\u0003\n\u0004\n", objArr);
            case 4:
                return zzdjt;
            case 5:
                adi<wp> adi = zzakh;
                if (adi != null) {
                    return adi;
                }
                synchronized (wp.class) {
                    r0 = zzakh;
                    if (r0 == 0) {
                        ? bVar = new b(zzdjt);
                        zzakh = bVar;
                        r0 = bVar;
                    }
                }
                return r0;
            case 6:
                return Byte.valueOf(1);
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    public final wl b() {
        return this.zzdjj == null ? wl.d() : this.zzdjj;
    }

    public final aah c() {
        return this.zzdjr;
    }

    public final aah d() {
        return this.zzdjs;
    }
}
