package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.ax;
import com.google.android.gms.ads.internal.gmsg.ae;
import java.util.Map;

@cm
public final class qc implements ae<pm> {
    private static Integer a(Map<String, String> map, String str) {
        if (!map.containsKey(str)) {
            return null;
        }
        try {
            return Integer.valueOf(Integer.parseInt((String) map.get(str)));
        } catch (NumberFormatException e) {
            String str2 = (String) map.get(str);
            jm.e(new StringBuilder(String.valueOf(str).length() + 39 + String.valueOf(str2).length()).append("Precache invalid numeric parameter '").append(str).append("': ").append(str2).toString());
            return null;
        }
    }

    public final /* synthetic */ void zza(Object obj, Map map) {
        pm pmVar = (pm) obj;
        ax.z();
        if (!map.containsKey("abort")) {
            String str = (String) map.get("src");
            if (str != null) {
                if (px.b(pmVar) != null) {
                    jm.e("Precache task is already running.");
                    return;
                } else if (pmVar.e() == null) {
                    jm.e("Precache requires a dependency provider.");
                    return;
                } else {
                    pl plVar = new pl((String) map.get("flags"));
                    Integer a2 = a(map, "player");
                    if (a2 == null) {
                        a2 = Integer.valueOf(0);
                    }
                    new pv(pmVar, pmVar.e().f2019a.a(pmVar, a2.intValue(), null, plVar), str).c();
                }
            } else if (px.b(pmVar) == null) {
                jm.e("Precache must specify a source.");
                return;
            }
            Integer a3 = a(map, "minBufferMs");
            if (a3 != null) {
                a3.intValue();
            }
            Integer a4 = a(map, "maxBufferMs");
            if (a4 != null) {
                a4.intValue();
            }
            Integer a5 = a(map, "bufferForPlaybackMs");
            if (a5 != null) {
                a5.intValue();
            }
            Integer a6 = a(map, "bufferForPlaybackAfterRebufferMs");
            if (a6 != null) {
                a6.intValue();
            }
        } else if (!px.a(pmVar)) {
            jm.e("Precache abort but no precache task running.");
        }
    }
}
