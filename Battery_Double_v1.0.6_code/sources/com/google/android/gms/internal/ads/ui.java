package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.xn.b;
import com.google.android.gms.internal.ads.xp.a;
import java.nio.charset.Charset;
import java.security.GeneralSecurityException;

final class ui {

    /* renamed from: a reason: collision with root package name */
    private static final Charset f3723a = Charset.forName("UTF-8");

    public static xp a(xn xnVar) {
        a a2 = xp.a().a(xnVar.a());
        for (b bVar : xnVar.b()) {
            a2.a((xp.b) xp.b.a().a(bVar.b().a()).a(bVar.c()).a(bVar.e()).a(bVar.d()).c());
        }
        return (xp) a2.c();
    }

    public static void b(xn xnVar) throws GeneralSecurityException {
        if (xnVar.c() == 0) {
            throw new GeneralSecurityException("empty keyset");
        }
        int a2 = xnVar.a();
        boolean z = true;
        boolean z2 = false;
        for (b bVar : xnVar.b()) {
            if (!bVar.a()) {
                throw new GeneralSecurityException(String.format("key %d has no key data", new Object[]{Integer.valueOf(bVar.d())}));
            } else if (bVar.e() == ya.UNKNOWN_PREFIX) {
                throw new GeneralSecurityException(String.format("key %d has unknown prefix", new Object[]{Integer.valueOf(bVar.d())}));
            } else if (bVar.c() == xh.UNKNOWN_STATUS) {
                throw new GeneralSecurityException(String.format("key %d has unknown status", new Object[]{Integer.valueOf(bVar.d())}));
            } else {
                if (bVar.c() == xh.ENABLED && bVar.d() == a2) {
                    if (z2) {
                        throw new GeneralSecurityException("keyset contains multiple primary keys");
                    }
                    z2 = true;
                }
                z = bVar.b().c() != xe.b.ASYMMETRIC_PUBLIC ? false : z;
            }
        }
        if (!z2 && !z) {
            throw new GeneralSecurityException("keyset doesn't contain a valid primary key");
        }
    }
}
