package com.google.android.gms.internal.ads;

import java.io.IOException;

public final class aco<K, V> {
    static <K, V> int a(acp<K, V> acp, K k, V v) {
        return abh.a(acp.f2461a, 1, k) + abh.a(acp.c, 2, v);
    }

    static <K, V> void a(aav aav, acp<K, V> acp, K k, V v) throws IOException {
        abh.a(aav, acp.f2461a, 1, k);
        abh.a(aav, acp.c, 2, v);
    }
}
