package com.google.android.gms.internal.ads;

import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.webkit.JsResult;

final class qg implements OnCancelListener {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ JsResult f3629a;

    qg(JsResult jsResult) {
        this.f3629a = jsResult;
    }

    public final void onCancel(DialogInterface dialogInterface) {
        this.f3629a.cancel();
    }
}
