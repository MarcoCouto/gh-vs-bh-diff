package com.google.android.gms.internal.ads;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class ajh {

    /* renamed from: a reason: collision with root package name */
    private static final String f2633a = ajh.class.getSimpleName();

    /* renamed from: b reason: collision with root package name */
    private final ahy f2634b;
    private final String c;
    private final String d;
    private final int e = 2;
    private volatile Method f = null;
    private final Class<?>[] g;
    private CountDownLatch h = new CountDownLatch(1);

    public ajh(ahy ahy, String str, String str2, Class<?>... clsArr) {
        this.f2634b = ahy;
        this.c = str;
        this.d = str2;
        this.g = clsArr;
        this.f2634b.c().submit(new aji(this));
    }

    private final String a(byte[] bArr, String str) throws ahk, UnsupportedEncodingException {
        return new String(this.f2634b.e().a(bArr, str), "UTF-8");
    }

    /* access modifiers changed from: private */
    public final void b() {
        try {
            Class loadClass = this.f2634b.d().loadClass(a(this.f2634b.f(), this.c));
            if (loadClass != null) {
                this.f = loadClass.getMethod(a(this.f2634b.f(), this.d), this.g);
                if (this.f == null) {
                    this.h.countDown();
                } else {
                    this.h.countDown();
                }
            }
        } catch (ahk e2) {
        } catch (UnsupportedEncodingException e3) {
        } catch (ClassNotFoundException e4) {
        } catch (NoSuchMethodException e5) {
        } catch (NullPointerException e6) {
        } finally {
            this.h.countDown();
        }
    }

    public final Method a() {
        if (this.f != null) {
            return this.f;
        }
        try {
            if (this.h.await(2, TimeUnit.SECONDS)) {
                return this.f;
            }
            return null;
        } catch (InterruptedException e2) {
            return null;
        }
    }
}
