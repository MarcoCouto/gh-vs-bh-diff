package com.google.android.gms.internal.ads;

import android.content.Context;
import android.text.TextUtils;
import com.google.ads.mediation.AbstractAdViewAdapter;
import com.google.android.gms.ads.internal.ax;
import com.google.android.gms.ads.internal.bb;
import com.google.android.gms.ads.internal.bu;
import com.google.android.gms.common.internal.aa;
import java.util.Arrays;
import java.util.Collections;
import org.json.JSONException;
import org.json.JSONObject;

@cm
public final class fz extends bb implements hc {
    private static fz k;
    private boolean l;
    private boolean m;
    private final ig n;
    private final fw o = new fw(this.e, this.j, this, this, this);

    public fz(Context context, bu buVar, aot aot, bcr bcr, mu muVar) {
        super(context, aot, null, bcr, muVar, buVar);
        k = this;
        this.n = new ig(context, null);
    }

    public static fz J() {
        return k;
    }

    private static is b(is isVar) {
        jm.a("Creating mediation ad response for non-mediated rewarded ad.");
        try {
            JSONObject a2 = ez.a(isVar.f3398b);
            a2.remove("impression_urls");
            JSONObject jSONObject = new JSONObject();
            jSONObject.put(AbstractAdViewAdapter.AD_UNIT_ID_PARAMETER, isVar.f3397a.e);
            String jSONObject2 = jSONObject.toString();
            bcb bcb = new bcb(Arrays.asList(new bca[]{new bca(a2.toString(), null, Arrays.asList(new String[]{"com.google.ads.mediation.admob.AdMobAdapter"}), null, null, Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), jSONObject2, null, Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), null, null, null, null, null, Collections.emptyList(), null, -1)}), ((Long) ape.f().a(asi.bB)).longValue(), Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), false, "", -1, 0, 1, null, 0, -1, -1, false);
            return new is(isVar.f3397a, isVar.f3398b, bcb, isVar.d, isVar.e, isVar.f, isVar.g, isVar.h, isVar.i, null);
        } catch (JSONException e) {
            jm.b("Unable to generate ad state for non-mediated rewarded video.", e);
            return new is(isVar.f3397a, isVar.f3398b, null, isVar.d, 0, isVar.f, isVar.g, isVar.h, isVar.i, null);
        }
    }

    public final void K() {
        aa.b("showAd must be called on the main UI thread.");
        if (!L()) {
            jm.e("The reward video has not loaded.");
        } else {
            this.o.a(this.m);
        }
    }

    public final boolean L() {
        aa.b("isLoaded must be called on the main UI thread.");
        return this.e.g == null && this.e.h == null && this.e.j != null;
    }

    public final void a(Context context) {
        this.o.a(context);
    }

    public final void a(gt gtVar) {
        aa.b("loadAd must be called on the main UI thread.");
        if (TextUtils.isEmpty(gtVar.f3345b)) {
            jm.e("Invalid ad unit id. Aborting.");
            jv.f3440a.post(new ga(this));
            return;
        }
        this.l = false;
        this.e.f1990b = gtVar.f3345b;
        this.n.a(gtVar.f3345b);
        super.b(gtVar.f3344a);
    }

    public final void a(hp hpVar) {
        hp a2 = this.o.a(hpVar);
        if (ax.B().e(this.e.c) && a2 != null) {
            ax.B().a(this.e.c, ax.B().j(this.e.c), this.e.f1990b, a2.f3369a, a2.f3370b);
        }
        b(a2);
    }

    public final void a(is isVar, asv asv) {
        if (isVar.e != -2) {
            jv.f3440a.post(new gb(this, isVar));
            return;
        }
        this.e.k = isVar;
        if (isVar.c == null) {
            this.e.k = b(isVar);
        }
        this.o.c();
    }

    /* access modifiers changed from: protected */
    public final boolean a(aop aop, ir irVar, boolean z) {
        return false;
    }

    public final boolean a(ir irVar, ir irVar2) {
        b(irVar2, false);
        return fw.a(irVar, irVar2);
    }

    public final hk b(String str) {
        return this.o.a(str);
    }

    public final void b() {
        this.o.g();
        A();
    }

    public final void c() {
        if (ax.B().e(this.e.c)) {
            this.n.a(false);
        }
        u();
    }

    public final void c(boolean z) {
        aa.b("setImmersiveMode must be called on the main UI thread.");
        this.m = z;
    }

    public final void j() {
        this.o.f();
        super.j();
    }

    public final void l_() {
        if (ax.B().e(this.e.c)) {
            this.n.a(true);
        }
        a(this.e.j, false);
        w();
    }

    public final void m_() {
        this.o.h();
        B();
    }

    public final void n_() {
        e();
    }

    public final void o() {
        this.o.d();
    }

    public final void o_() {
        v();
    }

    public final void p() {
        this.o.e();
    }

    /* access modifiers changed from: protected */
    public final void u() {
        this.e.j = null;
        super.u();
    }
}
