package com.google.android.gms.internal.ads;

import org.json.JSONObject;

final /* synthetic */ class bk implements rx {

    /* renamed from: a reason: collision with root package name */
    private final qn f3191a;

    /* renamed from: b reason: collision with root package name */
    private final JSONObject f3192b;

    bk(qn qnVar, JSONObject jSONObject) {
        this.f3191a = qnVar;
        this.f3192b = jSONObject;
    }

    public final void a() {
        this.f3191a.b("google.afma.nativeAds.renderVideo", this.f3192b);
    }
}
