package com.google.android.gms.internal.ads;

final class aap implements aal {
    private aap() {
    }

    /* synthetic */ aap(aai aai) {
        this();
    }

    public final byte[] a(byte[] bArr, int i, int i2) {
        byte[] bArr2 = new byte[i2];
        System.arraycopy(bArr, i, bArr2, 0, i2);
        return bArr2;
    }
}
