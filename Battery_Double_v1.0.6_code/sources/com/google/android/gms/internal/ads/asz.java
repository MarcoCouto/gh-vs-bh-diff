package com.google.android.gms.internal.ads;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.b.a;

public interface asz extends IInterface {
    String a() throws RemoteException;

    void a(a aVar) throws RemoteException;

    String b() throws RemoteException;

    void c() throws RemoteException;

    void d() throws RemoteException;
}
