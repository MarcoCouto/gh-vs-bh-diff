package com.google.android.gms.internal.ads;

final class fh {

    /* renamed from: a reason: collision with root package name */
    public final String f3312a;

    /* renamed from: b reason: collision with root package name */
    public final long f3313b;
    public final long c;

    public fh(String str, long j, long j2) {
        this.f3312a = str;
        this.f3313b = j;
        this.c = j2;
    }
}
