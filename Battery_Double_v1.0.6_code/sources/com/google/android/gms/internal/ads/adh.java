package com.google.android.gms.internal.ads;

final class adh {

    /* renamed from: a reason: collision with root package name */
    private static final adf f2474a = c();

    /* renamed from: b reason: collision with root package name */
    private static final adf f2475b = new adg();

    static adf a() {
        return f2474a;
    }

    static adf b() {
        return f2475b;
    }

    private static adf c() {
        try {
            return (adf) Class.forName("com.google.protobuf.NewInstanceSchemaFull").getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception e) {
            return null;
        }
    }
}
