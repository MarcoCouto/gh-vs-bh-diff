package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.ads.internal.ax;
import java.util.concurrent.Callable;

final class fl implements Callable<fi> {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ Context f3319a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ fk f3320b;

    fl(fk fkVar, Context context) {
        this.f3320b = fkVar;
        this.f3319a = context;
    }

    public final /* synthetic */ Object call() throws Exception {
        fi a2;
        fm fmVar = (fm) this.f3320b.f3318a.get(this.f3319a);
        if (fmVar != null) {
            if (!(fmVar.f3321a + ((Long) ape.f().a(asi.bq)).longValue() < ax.l().a())) {
                if (((Boolean) ape.f().a(asi.bp)).booleanValue()) {
                    a2 = new fj(this.f3319a, fmVar.f3322b).a();
                    this.f3320b.f3318a.put(this.f3319a, new fm(this.f3320b, a2));
                    return a2;
                }
            }
        }
        a2 = new fj(this.f3319a).a();
        this.f3320b.f3318a.put(this.f3319a, new fm(this.f3320b, a2));
        return a2;
    }
}
