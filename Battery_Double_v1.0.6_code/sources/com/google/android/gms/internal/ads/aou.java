package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.a.b;

public final class aou implements Creator<aot> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        aot[] aotArr = null;
        boolean z = false;
        int b2 = b.b(parcel);
        boolean z2 = false;
        boolean z3 = false;
        int i = 0;
        int i2 = 0;
        boolean z4 = false;
        int i3 = 0;
        int i4 = 0;
        String str = null;
        while (parcel.dataPosition() < b2) {
            int a2 = b.a(parcel);
            switch (b.a(a2)) {
                case 2:
                    str = b.k(parcel, a2);
                    break;
                case 3:
                    i4 = b.d(parcel, a2);
                    break;
                case 4:
                    i3 = b.d(parcel, a2);
                    break;
                case 5:
                    z4 = b.c(parcel, a2);
                    break;
                case 6:
                    i2 = b.d(parcel, a2);
                    break;
                case 7:
                    i = b.d(parcel, a2);
                    break;
                case 8:
                    aotArr = (aot[]) b.b(parcel, a2, aot.CREATOR);
                    break;
                case 9:
                    z3 = b.c(parcel, a2);
                    break;
                case 10:
                    z2 = b.c(parcel, a2);
                    break;
                case 11:
                    z = b.c(parcel, a2);
                    break;
                default:
                    b.b(parcel, a2);
                    break;
            }
        }
        b.r(parcel, b2);
        return new aot(str, i4, i3, z4, i2, i, aotArr, z3, z2, z);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new aot[i];
    }
}
