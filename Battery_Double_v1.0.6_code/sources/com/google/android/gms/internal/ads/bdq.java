package com.google.android.gms.internal.ads;

import android.os.RemoteException;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.b.i;
import com.google.android.gms.ads.mediation.MediationBannerAdapter;
import com.google.android.gms.ads.mediation.MediationInterstitialAdapter;
import com.google.android.gms.ads.mediation.MediationNativeAdapter;
import com.google.android.gms.ads.mediation.c;
import com.google.android.gms.ads.mediation.d;
import com.google.android.gms.ads.mediation.e;
import com.google.android.gms.ads.mediation.f;
import com.google.android.gms.ads.mediation.l;
import com.google.android.gms.common.internal.aa;

@cm
public final class bdq implements c, d, e {

    /* renamed from: a reason: collision with root package name */
    private final bcx f3151a;

    /* renamed from: b reason: collision with root package name */
    private f f3152b;
    private l c;
    private i d;

    public bdq(bcx bcx) {
        this.f3151a = bcx;
    }

    private static void a(MediationNativeAdapter mediationNativeAdapter, l lVar, f fVar) {
        if (!(mediationNativeAdapter instanceof AdMobAdapter)) {
            com.google.android.gms.ads.i iVar = new com.google.android.gms.ads.i();
            iVar.a((aqs) new bdn());
            if (lVar != null && lVar.k()) {
                lVar.a(iVar);
            }
            if (fVar != null && fVar.h()) {
                fVar.a(iVar);
            }
        }
    }

    public final f a() {
        return this.f3152b;
    }

    public final void a(MediationBannerAdapter mediationBannerAdapter) {
        aa.b("#008 Must be called on the main UI thread.");
        ms.b("Adapter called onAdLoaded.");
        try {
            this.f3151a.e();
        } catch (RemoteException e) {
            ms.d("#007 Could not call remote method.", e);
        }
    }

    public final void a(MediationBannerAdapter mediationBannerAdapter, int i) {
        aa.b("#008 Must be called on the main UI thread.");
        ms.b("Adapter called onAdFailedToLoad with error. " + i);
        try {
            this.f3151a.a(i);
        } catch (RemoteException e) {
            ms.d("#007 Could not call remote method.", e);
        }
    }

    public final void a(MediationBannerAdapter mediationBannerAdapter, String str, String str2) {
        aa.b("#008 Must be called on the main UI thread.");
        ms.b("Adapter called onAppEvent.");
        try {
            this.f3151a.a(str, str2);
        } catch (RemoteException e) {
            ms.d("#007 Could not call remote method.", e);
        }
    }

    public final void a(MediationInterstitialAdapter mediationInterstitialAdapter) {
        aa.b("#008 Must be called on the main UI thread.");
        ms.b("Adapter called onAdLoaded.");
        try {
            this.f3151a.e();
        } catch (RemoteException e) {
            ms.d("#007 Could not call remote method.", e);
        }
    }

    public final void a(MediationInterstitialAdapter mediationInterstitialAdapter, int i) {
        aa.b("#008 Must be called on the main UI thread.");
        ms.b("Adapter called onAdFailedToLoad with error " + i + ".");
        try {
            this.f3151a.a(i);
        } catch (RemoteException e) {
            ms.d("#007 Could not call remote method.", e);
        }
    }

    public final void a(MediationNativeAdapter mediationNativeAdapter) {
        aa.b("#008 Must be called on the main UI thread.");
        ms.b("Adapter called onAdOpened.");
        try {
            this.f3151a.d();
        } catch (RemoteException e) {
            ms.d("#007 Could not call remote method.", e);
        }
    }

    public final void a(MediationNativeAdapter mediationNativeAdapter, int i) {
        aa.b("#008 Must be called on the main UI thread.");
        ms.b("Adapter called onAdFailedToLoad with error " + i + ".");
        try {
            this.f3151a.a(i);
        } catch (RemoteException e) {
            ms.d("#007 Could not call remote method.", e);
        }
    }

    public final void a(MediationNativeAdapter mediationNativeAdapter, i iVar) {
        aa.b("#008 Must be called on the main UI thread.");
        String str = "Adapter called onAdLoaded with template id ";
        String valueOf = String.valueOf(iVar.a());
        ms.b(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
        this.d = iVar;
        try {
            this.f3151a.e();
        } catch (RemoteException e) {
            ms.d("#007 Could not call remote method.", e);
        }
    }

    public final void a(MediationNativeAdapter mediationNativeAdapter, i iVar, String str) {
        if (iVar instanceof avw) {
            try {
                this.f3151a.a(((avw) iVar).b(), str);
            } catch (RemoteException e) {
                ms.d("#007 Could not call remote method.", e);
            }
        } else {
            ms.e("Unexpected native custom template ad type.");
        }
    }

    public final void a(MediationNativeAdapter mediationNativeAdapter, f fVar) {
        aa.b("#008 Must be called on the main UI thread.");
        ms.b("Adapter called onAdLoaded.");
        this.f3152b = fVar;
        this.c = null;
        a(mediationNativeAdapter, this.c, this.f3152b);
        try {
            this.f3151a.e();
        } catch (RemoteException e) {
            ms.d("#007 Could not call remote method.", e);
        }
    }

    public final void a(MediationNativeAdapter mediationNativeAdapter, l lVar) {
        aa.b("#008 Must be called on the main UI thread.");
        ms.b("Adapter called onAdLoaded.");
        this.c = lVar;
        this.f3152b = null;
        a(mediationNativeAdapter, this.c, this.f3152b);
        try {
            this.f3151a.e();
        } catch (RemoteException e) {
            ms.d("#007 Could not call remote method.", e);
        }
    }

    public final l b() {
        return this.c;
    }

    public final void b(MediationBannerAdapter mediationBannerAdapter) {
        aa.b("#008 Must be called on the main UI thread.");
        ms.b("Adapter called onAdOpened.");
        try {
            this.f3151a.d();
        } catch (RemoteException e) {
            ms.d("#007 Could not call remote method.", e);
        }
    }

    public final void b(MediationInterstitialAdapter mediationInterstitialAdapter) {
        aa.b("#008 Must be called on the main UI thread.");
        ms.b("Adapter called onAdOpened.");
        try {
            this.f3151a.d();
        } catch (RemoteException e) {
            ms.d("#007 Could not call remote method.", e);
        }
    }

    public final void b(MediationNativeAdapter mediationNativeAdapter) {
        aa.b("#008 Must be called on the main UI thread.");
        ms.b("Adapter called onAdClosed.");
        try {
            this.f3151a.b();
        } catch (RemoteException e) {
            ms.d("#007 Could not call remote method.", e);
        }
    }

    public final i c() {
        return this.d;
    }

    public final void c(MediationBannerAdapter mediationBannerAdapter) {
        aa.b("#008 Must be called on the main UI thread.");
        ms.b("Adapter called onAdClosed.");
        try {
            this.f3151a.b();
        } catch (RemoteException e) {
            ms.d("#007 Could not call remote method.", e);
        }
    }

    public final void c(MediationInterstitialAdapter mediationInterstitialAdapter) {
        aa.b("#008 Must be called on the main UI thread.");
        ms.b("Adapter called onAdClosed.");
        try {
            this.f3151a.b();
        } catch (RemoteException e) {
            ms.d("#007 Could not call remote method.", e);
        }
    }

    public final void c(MediationNativeAdapter mediationNativeAdapter) {
        aa.b("#008 Must be called on the main UI thread.");
        ms.b("Adapter called onAdLeftApplication.");
        try {
            this.f3151a.c();
        } catch (RemoteException e) {
            ms.d("#007 Could not call remote method.", e);
        }
    }

    public final void d(MediationBannerAdapter mediationBannerAdapter) {
        aa.b("#008 Must be called on the main UI thread.");
        ms.b("Adapter called onAdLeftApplication.");
        try {
            this.f3151a.c();
        } catch (RemoteException e) {
            ms.d("#007 Could not call remote method.", e);
        }
    }

    public final void d(MediationInterstitialAdapter mediationInterstitialAdapter) {
        aa.b("#008 Must be called on the main UI thread.");
        ms.b("Adapter called onAdLeftApplication.");
        try {
            this.f3151a.c();
        } catch (RemoteException e) {
            ms.d("#007 Could not call remote method.", e);
        }
    }

    public final void d(MediationNativeAdapter mediationNativeAdapter) {
        aa.b("#008 Must be called on the main UI thread.");
        f fVar = this.f3152b;
        l lVar = this.c;
        if (this.d == null) {
            if (fVar == null && lVar == null) {
                ms.d("#007 Could not call remote method.", null);
                return;
            } else if (lVar != null && !lVar.q()) {
                ms.b("Could not call onAdClicked since setOverrideClickHandling is not set to true");
                return;
            } else if (fVar != null && !fVar.b()) {
                ms.b("Could not call onAdClicked since setOverrideClickHandling is not set to true");
                return;
            }
        }
        ms.b("Adapter called onAdClicked.");
        try {
            this.f3151a.a();
        } catch (RemoteException e) {
            ms.d("#007 Could not call remote method.", e);
        }
    }

    public final void e(MediationBannerAdapter mediationBannerAdapter) {
        aa.b("#008 Must be called on the main UI thread.");
        ms.b("Adapter called onAdClicked.");
        try {
            this.f3151a.a();
        } catch (RemoteException e) {
            ms.d("#007 Could not call remote method.", e);
        }
    }

    public final void e(MediationInterstitialAdapter mediationInterstitialAdapter) {
        aa.b("#008 Must be called on the main UI thread.");
        ms.b("Adapter called onAdClicked.");
        try {
            this.f3151a.a();
        } catch (RemoteException e) {
            ms.d("#007 Could not call remote method.", e);
        }
    }

    public final void e(MediationNativeAdapter mediationNativeAdapter) {
        aa.b("#008 Must be called on the main UI thread.");
        f fVar = this.f3152b;
        l lVar = this.c;
        if (this.d == null) {
            if (fVar == null && lVar == null) {
                ms.d("#007 Could not call remote method.", null);
                return;
            } else if (lVar != null && !lVar.p()) {
                ms.b("Could not call onAdImpression since setOverrideImpressionRecording is not set to true");
                return;
            } else if (fVar != null && !fVar.a()) {
                ms.b("Could not call onAdImpression since setOverrideImpressionRecording is not set to true");
                return;
            }
        }
        ms.b("Adapter called onAdImpression.");
        try {
            this.f3151a.f();
        } catch (RemoteException e) {
            ms.d("#007 Could not call remote method.", e);
        }
    }
}
