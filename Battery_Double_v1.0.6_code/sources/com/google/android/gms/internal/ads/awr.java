package com.google.android.gms.internal.ads;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;

public final class awr extends ajk implements awq {
    awr(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.formats.client.IUnconfirmedClickListener");
    }

    public final void a() throws RemoteException {
        b(2, r_());
    }

    public final void a(String str) throws RemoteException {
        Parcel r_ = r_();
        r_.writeString(str);
        b(1, r_);
    }
}
