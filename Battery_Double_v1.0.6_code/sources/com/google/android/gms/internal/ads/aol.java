package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.a;

@cm
public final class aol extends apl {

    /* renamed from: a reason: collision with root package name */
    private final a f2807a;

    public aol(a aVar) {
        this.f2807a = aVar;
    }

    public final void a() {
        this.f2807a.c();
    }

    public final void a(int i) {
        this.f2807a.a(i);
    }

    public final void b() {
        this.f2807a.d();
    }

    public final void c() {
        this.f2807a.a();
    }

    public final void d() {
        this.f2807a.b();
    }

    public final void e() {
        this.f2807a.e();
    }

    public final void f() {
        this.f2807a.f();
    }
}
