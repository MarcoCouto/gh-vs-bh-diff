package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.a.a;
import com.google.android.gms.common.internal.a.c;

@cm
public final class aqy extends a {
    public static final Creator<aqy> CREATOR = new aqz();

    /* renamed from: a reason: collision with root package name */
    public final int f2841a;

    public aqy(int i) {
        this.f2841a = i;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = c.a(parcel);
        c.a(parcel, 2, this.f2841a);
        c.a(parcel, a2);
    }
}
