package com.google.android.gms.internal.ads;

import java.io.IOException;

public final class anv extends afh<anv> {

    /* renamed from: a reason: collision with root package name */
    private Integer f2780a;

    /* renamed from: b reason: collision with root package name */
    private Integer f2781b;

    public anv() {
        this.f2780a = null;
        this.f2781b = null;
        this.Y = null;
        this.Z = -1;
    }

    /* access modifiers changed from: protected */
    public final int a() {
        int a2 = super.a();
        if (this.f2780a != null) {
            a2 += aff.b(1, this.f2780a.intValue());
        }
        return this.f2781b != null ? a2 + aff.b(2, this.f2781b.intValue()) : a2;
    }

    public final /* synthetic */ afn a(afd afd) throws IOException {
        while (true) {
            int a2 = afd.a();
            switch (a2) {
                case 0:
                    break;
                case 8:
                    this.f2780a = Integer.valueOf(afd.g());
                    continue;
                case 16:
                    this.f2781b = Integer.valueOf(afd.g());
                    continue;
                default:
                    if (!super.a(afd, a2)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    public final void a(aff aff) throws IOException {
        if (this.f2780a != null) {
            aff.a(1, this.f2780a.intValue());
        }
        if (this.f2781b != null) {
            aff.a(2, this.f2781b.intValue());
        }
        super.a(aff);
    }
}
