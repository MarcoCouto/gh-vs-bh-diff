package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.abp.e;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

final class aax implements afc {

    /* renamed from: a reason: collision with root package name */
    private final aav f2408a;

    private aax(aav aav) {
        this.f2408a = (aav) abr.a(aav, "output");
    }

    public static aax a(aav aav) {
        return aav.f2406a != null ? aav.f2406a : new aax(aav);
    }

    public final int a() {
        return e.j;
    }

    public final void a(int i) throws IOException {
        this.f2408a.a(i, 3);
    }

    public final void a(int i, double d) throws IOException {
        this.f2408a.a(i, d);
    }

    public final void a(int i, float f) throws IOException {
        this.f2408a.a(i, f);
    }

    public final void a(int i, int i2) throws IOException {
        this.f2408a.e(i, i2);
    }

    public final void a(int i, long j) throws IOException {
        this.f2408a.a(i, j);
    }

    public final void a(int i, aah aah) throws IOException {
        this.f2408a.a(i, aah);
    }

    public final <K, V> void a(int i, acp<K, V> acp, Map<K, V> map) throws IOException {
        for (Entry entry : map.entrySet()) {
            this.f2408a.a(i, 2);
            this.f2408a.b(aco.a(acp, entry.getKey(), entry.getValue()));
            aco.a(this.f2408a, acp, entry.getKey(), entry.getValue());
        }
    }

    public final void a(int i, Object obj) throws IOException {
        if (obj instanceof aah) {
            this.f2408a.b(i, (aah) obj);
        } else {
            this.f2408a.a(i, (acw) obj);
        }
    }

    public final void a(int i, Object obj, adp adp) throws IOException {
        this.f2408a.a(i, (acw) obj, adp);
    }

    public final void a(int i, String str) throws IOException {
        this.f2408a.a(i, str);
    }

    public final void a(int i, List<String> list) throws IOException {
        int i2 = 0;
        if (list instanceof acf) {
            acf acf = (acf) list;
            while (true) {
                int i3 = i2;
                if (i3 < list.size()) {
                    Object b2 = acf.b(i3);
                    if (b2 instanceof String) {
                        this.f2408a.a(i, (String) b2);
                    } else {
                        this.f2408a.a(i, (aah) b2);
                    }
                    i2 = i3 + 1;
                } else {
                    return;
                }
            }
        } else {
            while (i2 < list.size()) {
                this.f2408a.a(i, (String) list.get(i2));
                i2++;
            }
        }
    }

    public final void a(int i, List<?> list, adp adp) throws IOException {
        for (int i2 = 0; i2 < list.size(); i2++) {
            a(i, list.get(i2), adp);
        }
    }

    public final void a(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.f2408a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += aav.f(((Integer) list.get(i4)).intValue());
            }
            this.f2408a.b(i3);
            while (i2 < list.size()) {
                this.f2408a.a(((Integer) list.get(i2)).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.f2408a.b(i, ((Integer) list.get(i2)).intValue());
            i2++;
        }
    }

    public final void a(int i, boolean z) throws IOException {
        this.f2408a.a(i, z);
    }

    public final void b(int i) throws IOException {
        this.f2408a.a(i, 4);
    }

    public final void b(int i, int i2) throws IOException {
        this.f2408a.b(i, i2);
    }

    public final void b(int i, long j) throws IOException {
        this.f2408a.c(i, j);
    }

    public final void b(int i, Object obj, adp adp) throws IOException {
        aav aav = this.f2408a;
        acw acw = (acw) obj;
        aav.a(i, 3);
        adp.a(acw, (afc) aav.f2406a);
        aav.a(i, 4);
    }

    public final void b(int i, List<aah> list) throws IOException {
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < list.size()) {
                this.f2408a.a(i, (aah) list.get(i3));
                i2 = i3 + 1;
            } else {
                return;
            }
        }
    }

    public final void b(int i, List<?> list, adp adp) throws IOException {
        for (int i2 = 0; i2 < list.size(); i2++) {
            b(i, list.get(i2), adp);
        }
    }

    public final void b(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.f2408a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += aav.i(((Integer) list.get(i4)).intValue());
            }
            this.f2408a.b(i3);
            while (i2 < list.size()) {
                this.f2408a.d(((Integer) list.get(i2)).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.f2408a.e(i, ((Integer) list.get(i2)).intValue());
            i2++;
        }
    }

    public final void c(int i, int i2) throws IOException {
        this.f2408a.b(i, i2);
    }

    public final void c(int i, long j) throws IOException {
        this.f2408a.a(i, j);
    }

    public final void c(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.f2408a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += aav.d(((Long) list.get(i4)).longValue());
            }
            this.f2408a.b(i3);
            while (i2 < list.size()) {
                this.f2408a.a(((Long) list.get(i2)).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.f2408a.a(i, ((Long) list.get(i2)).longValue());
            i2++;
        }
    }

    public final void d(int i, int i2) throws IOException {
        this.f2408a.e(i, i2);
    }

    public final void d(int i, long j) throws IOException {
        this.f2408a.c(i, j);
    }

    public final void d(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.f2408a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += aav.e(((Long) list.get(i4)).longValue());
            }
            this.f2408a.b(i3);
            while (i2 < list.size()) {
                this.f2408a.a(((Long) list.get(i2)).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.f2408a.a(i, ((Long) list.get(i2)).longValue());
            i2++;
        }
    }

    public final void e(int i, int i2) throws IOException {
        this.f2408a.c(i, i2);
    }

    public final void e(int i, long j) throws IOException {
        this.f2408a.b(i, j);
    }

    public final void e(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.f2408a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += aav.g(((Long) list.get(i4)).longValue());
            }
            this.f2408a.b(i3);
            while (i2 < list.size()) {
                this.f2408a.c(((Long) list.get(i2)).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.f2408a.c(i, ((Long) list.get(i2)).longValue());
            i2++;
        }
    }

    public final void f(int i, int i2) throws IOException {
        this.f2408a.d(i, i2);
    }

    public final void f(int i, List<Float> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.f2408a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += aav.b(((Float) list.get(i4)).floatValue());
            }
            this.f2408a.b(i3);
            while (i2 < list.size()) {
                this.f2408a.a(((Float) list.get(i2)).floatValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.f2408a.a(i, ((Float) list.get(i2)).floatValue());
            i2++;
        }
    }

    public final void g(int i, List<Double> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.f2408a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += aav.b(((Double) list.get(i4)).doubleValue());
            }
            this.f2408a.b(i3);
            while (i2 < list.size()) {
                this.f2408a.a(((Double) list.get(i2)).doubleValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.f2408a.a(i, ((Double) list.get(i2)).doubleValue());
            i2++;
        }
    }

    public final void h(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.f2408a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += aav.k(((Integer) list.get(i4)).intValue());
            }
            this.f2408a.b(i3);
            while (i2 < list.size()) {
                this.f2408a.a(((Integer) list.get(i2)).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.f2408a.b(i, ((Integer) list.get(i2)).intValue());
            i2++;
        }
    }

    public final void i(int i, List<Boolean> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.f2408a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += aav.b(((Boolean) list.get(i4)).booleanValue());
            }
            this.f2408a.b(i3);
            while (i2 < list.size()) {
                this.f2408a.a(((Boolean) list.get(i2)).booleanValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.f2408a.a(i, ((Boolean) list.get(i2)).booleanValue());
            i2++;
        }
    }

    public final void j(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.f2408a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += aav.g(((Integer) list.get(i4)).intValue());
            }
            this.f2408a.b(i3);
            while (i2 < list.size()) {
                this.f2408a.b(((Integer) list.get(i2)).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.f2408a.c(i, ((Integer) list.get(i2)).intValue());
            i2++;
        }
    }

    public final void k(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.f2408a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += aav.j(((Integer) list.get(i4)).intValue());
            }
            this.f2408a.b(i3);
            while (i2 < list.size()) {
                this.f2408a.d(((Integer) list.get(i2)).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.f2408a.e(i, ((Integer) list.get(i2)).intValue());
            i2++;
        }
    }

    public final void l(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.f2408a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += aav.h(((Long) list.get(i4)).longValue());
            }
            this.f2408a.b(i3);
            while (i2 < list.size()) {
                this.f2408a.c(((Long) list.get(i2)).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.f2408a.c(i, ((Long) list.get(i2)).longValue());
            i2++;
        }
    }

    public final void m(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.f2408a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += aav.h(((Integer) list.get(i4)).intValue());
            }
            this.f2408a.b(i3);
            while (i2 < list.size()) {
                this.f2408a.c(((Integer) list.get(i2)).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.f2408a.d(i, ((Integer) list.get(i2)).intValue());
            i2++;
        }
    }

    public final void n(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.f2408a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += aav.f(((Long) list.get(i4)).longValue());
            }
            this.f2408a.b(i3);
            while (i2 < list.size()) {
                this.f2408a.b(((Long) list.get(i2)).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.f2408a.b(i, ((Long) list.get(i2)).longValue());
            i2++;
        }
    }
}
