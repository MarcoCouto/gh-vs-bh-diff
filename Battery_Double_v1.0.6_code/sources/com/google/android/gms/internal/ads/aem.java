package com.google.android.gms.internal.ads;

import java.util.ListIterator;

final class aem implements ListIterator<String> {

    /* renamed from: a reason: collision with root package name */
    private ListIterator<String> f2507a = this.c.f2506a.listIterator(this.f2508b);

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ int f2508b;
    private final /* synthetic */ ael c;

    aem(ael ael, int i) {
        this.c = ael;
        this.f2508b = i;
    }

    public final /* synthetic */ void add(Object obj) {
        throw new UnsupportedOperationException();
    }

    public final boolean hasNext() {
        return this.f2507a.hasNext();
    }

    public final boolean hasPrevious() {
        return this.f2507a.hasPrevious();
    }

    public final /* synthetic */ Object next() {
        return (String) this.f2507a.next();
    }

    public final int nextIndex() {
        return this.f2507a.nextIndex();
    }

    public final /* synthetic */ Object previous() {
        return (String) this.f2507a.previous();
    }

    public final int previousIndex() {
        return this.f2507a.previousIndex();
    }

    public final void remove() {
        throw new UnsupportedOperationException();
    }

    public final /* synthetic */ void set(Object obj) {
        throw new UnsupportedOperationException();
    }
}
