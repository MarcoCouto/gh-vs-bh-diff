package com.google.android.gms.internal.ads;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

final class adj {

    /* renamed from: a reason: collision with root package name */
    private static final adj f2476a = new adj();

    /* renamed from: b reason: collision with root package name */
    private final adq f2477b;
    private final ConcurrentMap<Class<?>, adp<?>> c = new ConcurrentHashMap();

    private adj() {
        adq adq = null;
        String[] strArr = {"com.google.protobuf.AndroidProto3SchemaFactory"};
        for (int i = 0; i <= 0; i++) {
            adq = a(strArr[0]);
            if (adq != null) {
                break;
            }
        }
        if (adq == null) {
            adq = new acl();
        }
        this.f2477b = adq;
    }

    public static adj a() {
        return f2476a;
    }

    private static adq a(String str) {
        try {
            return (adq) Class.forName(str).getConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Throwable th) {
            return null;
        }
    }

    public final <T> adp<T> a(Class<T> cls) {
        abr.a(cls, "messageType");
        adp<T> adp = (adp) this.c.get(cls);
        if (adp != null) {
            return adp;
        }
        adp a2 = this.f2477b.a(cls);
        abr.a(cls, "messageType");
        abr.a(a2, "schema");
        adp<T> adp2 = (adp) this.c.putIfAbsent(cls, a2);
        return adp2 != null ? adp2 : a2;
    }

    public final <T> adp<T> a(T t) {
        return a(t.getClass());
    }
}
