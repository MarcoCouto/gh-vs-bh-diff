package com.google.android.gms.internal.ads;

import java.security.GeneralSecurityException;

final class va implements ty {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ uf f3734a;

    va(uf ufVar) {
        this.f3734a = ufVar;
    }

    public final byte[] a(byte[] bArr, byte[] bArr2) throws GeneralSecurityException {
        return yh.a(this.f3734a.a().b(), ((ty) this.f3734a.a().a()).a(bArr, bArr2));
    }
}
