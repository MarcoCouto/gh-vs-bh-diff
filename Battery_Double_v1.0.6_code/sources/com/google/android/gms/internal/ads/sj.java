package com.google.android.gms.internal.ads;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.Build.VERSION;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.ViewTreeObserver.OnScrollChangedListener;
import android.view.WindowManager;
import android.webkit.DownloadListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.google.android.gms.ads.internal.aq;
import com.google.android.gms.ads.internal.ax;
import com.google.android.gms.ads.internal.bu;
import com.google.android.gms.ads.internal.gmsg.ae;
import com.google.android.gms.ads.internal.overlay.c;
import com.google.android.gms.ads.internal.overlay.d;
import com.google.android.gms.common.util.n;
import com.google.android.gms.common.util.o;
import com.hmatalonga.greenhub.Config;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;

@cm
final class sj extends so implements OnGlobalLayoutListener, DownloadListener, azp, qn {
    private WeakReference<OnClickListener> A;
    private d B;
    private mg C;
    private int D = -1;
    private int E = -1;
    private int F = -1;
    private int G = -1;
    private float H;
    private Map<String, py> I;
    private final WindowManager J;

    /* renamed from: b reason: collision with root package name */
    private final ahh f3685b;
    private final mu c;
    private final aq d;
    private final bu e;
    private sc f;
    private d g;
    private sb h;
    private String i;
    private boolean j;
    private boolean k;
    private boolean l;
    private int m;
    private boolean n = true;
    private boolean o = false;
    private String p = "";
    private rd q;
    private boolean r;
    private boolean s;
    private atw t;
    private int u;
    /* access modifiers changed from: private */
    public int v;
    private ast w;
    private ast x;
    private ast y;
    private asu z;

    private sj(sa saVar, sb sbVar, String str, boolean z2, boolean z3, ahh ahh, mu muVar, asv asv, aq aqVar, bu buVar, amw amw) {
        super(saVar);
        this.h = sbVar;
        this.i = str;
        this.k = z2;
        this.m = -1;
        this.f3685b = ahh;
        this.c = muVar;
        this.d = aqVar;
        this.e = buVar;
        this.J = (WindowManager) getContext().getSystemService("window");
        this.C = new mg(K().a(), this, this, null);
        ax.e().a((Context) saVar, muVar.f3528a, getSettings());
        setDownloadListener(this);
        this.H = K().getResources().getDisplayMetrics().density;
        O();
        if (n.e()) {
            addJavascriptInterface(rg.a(this), "googleAdsJsInterface");
        }
        S();
        this.z = new asu(new asv(true, "make_wv", this.i));
        this.z.a().a(asv);
        this.x = aso.a(this.z.a());
        this.z.a("native:view_create", this.x);
        this.y = null;
        this.w = null;
        ax.g().b((Context) saVar);
    }

    private final boolean M() {
        int i2;
        int i3;
        if (!this.f.b() && !this.f.c()) {
            return false;
        }
        ax.e();
        DisplayMetrics a2 = jv.a(this.J);
        ape.a();
        int b2 = mh.b(a2, a2.widthPixels);
        ape.a();
        int b3 = mh.b(a2, a2.heightPixels);
        Activity a3 = K().a();
        if (a3 == null || a3.getWindow() == null) {
            i2 = b3;
            i3 = b2;
        } else {
            ax.e();
            int[] a4 = jv.a(a3);
            ape.a();
            i3 = mh.b(a2, a4[0]);
            ape.a();
            i2 = mh.b(a2, a4[1]);
        }
        if (this.E == b2 && this.D == b3 && this.F == i3 && this.G == i2) {
            return false;
        }
        boolean z2 = (this.E == b2 && this.D == b3) ? false : true;
        this.E = b2;
        this.D = b3;
        this.F = i3;
        this.G = i2;
        new n(this).a(b2, b3, i3, i2, a2.density, this.J.getDefaultDisplay().getRotation());
        return z2;
    }

    private final void N() {
        aso.a(this.z.a(), this.x, "aeh2");
    }

    private final synchronized void O() {
        if (this.k || this.h.d()) {
            jm.b("Enabling hardware acceleration on an overlay.");
            Q();
        } else if (VERSION.SDK_INT < 18) {
            jm.b("Disabling hardware acceleration on an AdView.");
            P();
        } else {
            jm.b("Enabling hardware acceleration on an AdView.");
            Q();
        }
    }

    private final synchronized void P() {
        if (!this.l) {
            ax.g().c((View) this);
        }
        this.l = true;
    }

    private final synchronized void Q() {
        if (this.l) {
            ax.g().b((View) this);
        }
        this.l = false;
    }

    private final synchronized void R() {
        this.I = null;
    }

    private final void S() {
        if (this.z != null) {
            asv a2 = this.z.a();
            if (a2 != null && ax.i().b() != null) {
                ax.i().b().a(a2);
            }
        }
    }

    static sj a(Context context, sb sbVar, String str, boolean z2, boolean z3, ahh ahh, mu muVar, asv asv, aq aqVar, bu buVar, amw amw) {
        return new sj(new sa(context), sbVar, str, z2, z3, ahh, muVar, asv, aqVar, buVar, amw);
    }

    private final void g(boolean z2) {
        HashMap hashMap = new HashMap();
        hashMap.put("isVisible", z2 ? "1" : Config.NOTIFICATION_DEFAULT_PRIORITY);
        azq.a((azp) this, "onAdVisibilityChanged", (Map) hashMap);
    }

    public final synchronized boolean C() {
        return this.n;
    }

    public final synchronized boolean D() {
        return this.o;
    }

    public final synchronized boolean E() {
        return this.u > 0;
    }

    public final void F() {
        this.C.a();
    }

    public final void G() {
        if (this.y == null) {
            this.y = aso.a(this.z.a());
            this.z.a("native:view_load", this.y);
        }
    }

    public final synchronized atw H() {
        return this.t;
    }

    public final void I() {
        setBackgroundColor(0);
    }

    public final void J() {
        jm.a("Cannot add text view to inner AdWebView");
    }

    public final pd a() {
        return null;
    }

    public final void a(int i2) {
        if (i2 == 0) {
            aso.a(this.z.a(), this.x, "aebb2");
        }
        N();
        if (this.z.a() != null) {
            this.z.a().a("close_type", String.valueOf(i2));
        }
        HashMap hashMap = new HashMap(2);
        hashMap.put("closetype", String.valueOf(i2));
        hashMap.put("version", this.c.f3528a);
        azq.a((azp) this, "onhide", (Map) hashMap);
    }

    public final void a(Context context) {
        K().setBaseContext(context);
        this.C.a(K().a());
    }

    public final void a(c cVar) {
        this.f.a(cVar);
    }

    public final synchronized void a(d dVar) {
        this.g = dVar;
    }

    public final void a(aku aku) {
        synchronized (this) {
            this.r = aku.f2678a;
        }
        g(aku.f2678a);
    }

    public final synchronized void a(atw atw) {
        this.t = atw;
    }

    public final synchronized void a(rd rdVar) {
        if (this.q != null) {
            jm.c("Attempt to create multiple AdWebViewVideoControllers.");
        } else {
            this.q = rdVar;
        }
    }

    public final synchronized void a(sb sbVar) {
        this.h = sbVar;
        requestLayout();
    }

    public final void a(sc scVar) {
        this.f = scVar;
    }

    public final synchronized void a(String str) {
        if (str == null) {
            str = "";
        }
        this.p = str;
    }

    public final void a(String str, ae<? super qn> aeVar) {
        if (this.f != null) {
            this.f.a(str, aeVar);
        }
    }

    public final void a(String str, o<ae<? super qn>> oVar) {
        if (this.f != null) {
            this.f.a(str, oVar);
        }
    }

    public final void a(String str, String str2) {
        azq.a((azp) this, str, str2);
    }

    public final synchronized void a(String str, String str2, String str3) {
        String str4;
        if (((Boolean) ape.f().a(asi.aB)).booleanValue()) {
            str4 = rp.a(str2, rp.a());
        } else {
            str4 = str2;
        }
        super.loadDataWithBaseURL(str, str4, "text/html", "UTF-8", str3);
    }

    public final void a(String str, Map map) {
        azq.a((azp) this, str, map);
    }

    public final void a(String str, JSONObject jSONObject) {
        azq.b(this, str, jSONObject);
    }

    public final void a(boolean z2) {
        this.f.a(z2);
    }

    public final void a(boolean z2, int i2) {
        this.f.a(z2, i2);
    }

    public final void a(boolean z2, int i2, String str) {
        this.f.a(z2, i2, str);
    }

    public final void a(boolean z2, int i2, String str, String str2) {
        this.f.a(z2, i2, str, str2);
    }

    public final synchronized rd b() {
        return this.q;
    }

    public final synchronized void b(d dVar) {
        this.B = dVar;
    }

    public final synchronized void b(String str) {
        if (!A()) {
            super.b(str);
        } else {
            jm.e("The webview is destroyed. Ignoring action.");
        }
    }

    public final void b(String str, ae<? super qn> aeVar) {
        if (this.f != null) {
            this.f.b(str, aeVar);
        }
    }

    public final void b(String str, JSONObject jSONObject) {
        azq.a((azp) this, str, jSONObject);
    }

    public final synchronized void b(boolean z2) {
        boolean z3 = z2 != this.k;
        this.k = z2;
        O();
        if (z3) {
            new n(this).c(z2 ? "expanded" : "default");
        }
    }

    public final ast c() {
        return this.x;
    }

    public final synchronized void c(boolean z2) {
        if (this.g != null) {
            this.g.a(this.f.b(), z2);
        } else {
            this.j = z2;
        }
    }

    public final Activity d() {
        return K().a();
    }

    public final synchronized void d(boolean z2) {
        this.n = z2;
    }

    public final bu e() {
        return this.e;
    }

    public final synchronized void e(boolean z2) {
        this.u = (z2 ? 1 : -1) + this.u;
        if (this.u <= 0 && this.g != null) {
            this.g.q();
        }
    }

    public final void f() {
        d r2 = r();
        if (r2 != null) {
            r2.p();
        }
    }

    /* access modifiers changed from: protected */
    public final synchronized void f(boolean z2) {
        if (!z2) {
            S();
            this.C.b();
            if (this.g != null) {
                this.g.a();
                this.g.k();
                this.g = null;
            }
        }
        this.f.k();
        ax.z();
        px.a((pm) this);
        R();
    }

    public final synchronized String g() {
        return this.p;
    }

    public final OnClickListener getOnClickListener() {
        return (OnClickListener) this.A.get();
    }

    public final synchronized int getRequestedOrientation() {
        return this.m;
    }

    public final View getView() {
        return this;
    }

    public final WebView getWebView() {
        return this;
    }

    public final synchronized void h_() {
        this.o = true;
        if (this.d != null) {
            this.d.h_();
        }
    }

    public final synchronized void i_() {
        this.o = false;
        if (this.d != null) {
            this.d.i_();
        }
    }

    public final asu j() {
        return this.z;
    }

    public final mu k() {
        return this.c;
    }

    public final int l() {
        return getMeasuredHeight();
    }

    public final int m() {
        return getMeasuredWidth();
    }

    public final void n() {
        N();
        HashMap hashMap = new HashMap(1);
        hashMap.put("version", this.c.f3528a);
        azq.a((azp) this, "onhide", (Map) hashMap);
    }

    public final void o() {
        if (this.w == null) {
            aso.a(this.z.a(), this.x, "aes2");
            this.w = aso.a(this.z.a());
            this.z.a("native:view_show", this.w);
        }
        HashMap hashMap = new HashMap(1);
        hashMap.put("version", this.c.f3528a);
        azq.a((azp) this, "onshow", (Map) hashMap);
    }

    /* access modifiers changed from: protected */
    public final synchronized void onAttachedToWindow() {
        boolean z2;
        super.onAttachedToWindow();
        if (!A()) {
            this.C.c();
        }
        boolean z3 = this.r;
        if (this.f == null || !this.f.c()) {
            z2 = z3;
        } else {
            if (!this.s) {
                OnGlobalLayoutListener d2 = this.f.d();
                if (d2 != null) {
                    ax.A();
                    if (this == null) {
                        throw null;
                    }
                    og.a((View) this, d2);
                }
                OnScrollChangedListener e2 = this.f.e();
                if (e2 != null) {
                    ax.A();
                    if (this == null) {
                        throw null;
                    }
                    og.a((View) this, e2);
                }
                this.s = true;
            }
            M();
            z2 = true;
        }
        g(z2);
    }

    /* access modifiers changed from: protected */
    public final void onDetachedFromWindow() {
        synchronized (this) {
            if (!A()) {
                this.C.d();
            }
            super.onDetachedFromWindow();
            if (this.s && this.f != null && this.f.c() && getViewTreeObserver() != null && getViewTreeObserver().isAlive()) {
                OnGlobalLayoutListener d2 = this.f.d();
                if (d2 != null) {
                    ax.g().a(getViewTreeObserver(), d2);
                }
                OnScrollChangedListener e2 = this.f.e();
                if (e2 != null) {
                    getViewTreeObserver().removeOnScrollChangedListener(e2);
                }
                this.s = false;
            }
        }
        g(false);
    }

    public final void onDownloadStart(String str, String str2, String str3, String str4, long j2) {
        try {
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setDataAndType(Uri.parse(str), str4);
            ax.e();
            jv.a(getContext(), intent);
        } catch (ActivityNotFoundException e2) {
            jm.b(new StringBuilder(String.valueOf(str).length() + 51 + String.valueOf(str4).length()).append("Couldn't find an Activity to view url/mimetype: ").append(str).append(" / ").append(str4).toString());
        }
    }

    /* access modifiers changed from: protected */
    @TargetApi(21)
    public final void onDraw(Canvas canvas) {
        if (VERSION.SDK_INT != 21 || !canvas.isHardwareAccelerated() || isAttachedToWindow()) {
            super.onDraw(canvas);
            if (this.f != null && this.f.l() != null) {
                this.f.l().a();
            }
        }
    }

    public final boolean onGenericMotionEvent(MotionEvent motionEvent) {
        if (((Boolean) ape.f().a(asi.ay)).booleanValue()) {
            float axisValue = motionEvent.getAxisValue(9);
            float axisValue2 = motionEvent.getAxisValue(10);
            if (motionEvent.getActionMasked() == 8 && ((axisValue > 0.0f && !canScrollVertically(-1)) || ((axisValue < 0.0f && !canScrollVertically(1)) || ((axisValue2 > 0.0f && !canScrollHorizontally(-1)) || (axisValue2 < 0.0f && !canScrollHorizontally(1)))))) {
                return false;
            }
        }
        return super.onGenericMotionEvent(motionEvent);
    }

    public final void onGlobalLayout() {
        boolean M = M();
        d r2 = r();
        if (r2 != null && M) {
            r2.o();
        }
    }

    /* access modifiers changed from: protected */
    @SuppressLint({"DrawAllocation"})
    public final synchronized void onMeasure(int i2, int i3) {
        boolean z2;
        int size;
        if (A()) {
            setMeasuredDimension(0, 0);
        } else if (isInEditMode() || this.k || this.h.e()) {
            super.onMeasure(i2, i3);
        } else if (this.h.f()) {
            rd b2 = b();
            float f2 = b2 != null ? b2.e() : 0.0f;
            if (f2 == 0.0f) {
                super.onMeasure(i2, i3);
            } else {
                int size2 = MeasureSpec.getSize(i2);
                int size3 = MeasureSpec.getSize(i3);
                int i4 = (int) (((float) size3) * f2);
                int i5 = (int) (((float) size2) / f2);
                if (size3 == 0 && i5 != 0) {
                    i4 = (int) (((float) i5) * f2);
                    size3 = i5;
                } else if (size2 == 0 && i4 != 0) {
                    i5 = (int) (((float) i4) / f2);
                    size2 = i4;
                }
                setMeasuredDimension(Math.min(i4, size2), Math.min(i5, size3));
            }
        } else if (this.h.c()) {
            if (((Boolean) ape.f().a(asi.cm)).booleanValue() || !n.e()) {
                super.onMeasure(i2, i3);
            } else {
                a("/contentHeight", (ae<? super qn>) new sk<Object>(this));
                b("(function() {  var height = -1;  if (document.body) {    height = document.body.offsetHeight;  } else if (document.documentElement) {    height = document.documentElement.offsetHeight;  }  var url = 'gmsg://mobileads.google.com/contentHeight?';  url += 'height=' + height;  try {    window.googleAdsJsInterface.notify(url);  } catch (e) {    var frame = document.getElementById('afma-notify-fluid');    if (!frame) {      frame = document.createElement('IFRAME');      frame.id = 'afma-notify-fluid';      frame.style.display = 'none';      var body = document.body || document.documentElement;      body.appendChild(frame);    }    frame.src = url;  }})();");
                int size4 = MeasureSpec.getSize(i2);
                switch (this.v) {
                    case -1:
                        size = MeasureSpec.getSize(i3);
                        break;
                    default:
                        size = (int) (((float) this.v) * this.H);
                        break;
                }
                setMeasuredDimension(size4, size);
            }
        } else if (this.h.d()) {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            this.J.getDefaultDisplay().getMetrics(displayMetrics);
            setMeasuredDimension(displayMetrics.widthPixels, displayMetrics.heightPixels);
        } else {
            int mode = MeasureSpec.getMode(i2);
            int size5 = MeasureSpec.getSize(i2);
            int mode2 = MeasureSpec.getMode(i3);
            int size6 = MeasureSpec.getSize(i3);
            int i6 = (mode == Integer.MIN_VALUE || mode == 1073741824) ? size5 : Integer.MAX_VALUE;
            int i7 = (mode2 == Integer.MIN_VALUE || mode2 == 1073741824) ? size6 : Integer.MAX_VALUE;
            boolean z3 = this.h.f3673b > i6 || this.h.f3672a > i7;
            if (((Boolean) ape.f().a(asi.dh)).booleanValue()) {
                z2 = ((float) this.h.f3673b) / this.H <= ((float) i6) / this.H && ((float) this.h.f3672a) / this.H <= ((float) i7) / this.H;
                if (!z3) {
                    z2 = z3;
                }
            } else {
                z2 = z3;
            }
            if (z2) {
                jm.e("Not enough space to show ad. Needs " + ((int) (((float) this.h.f3673b) / this.H)) + "x" + ((int) (((float) this.h.f3672a) / this.H)) + " dp, but only has " + ((int) (((float) size5) / this.H)) + "x" + ((int) (((float) size6) / this.H)) + " dp.");
                if (getVisibility() != 8) {
                    setVisibility(4);
                }
                setMeasuredDimension(0, 0);
            } else {
                if (getVisibility() != 8) {
                    setVisibility(0);
                }
                setMeasuredDimension(this.h.f3673b, this.h.f3672a);
            }
        }
    }

    public final void onPause() {
        try {
            if (n.a()) {
                super.onPause();
            }
        } catch (Exception e2) {
            jm.b("Could not pause webview.", e2);
        }
    }

    public final void onResume() {
        try {
            if (n.a()) {
                super.onResume();
            }
        } catch (Exception e2) {
            jm.b("Could not resume webview.", e2);
        }
    }

    public final boolean onTouchEvent(MotionEvent motionEvent) {
        if (this.f.c()) {
            synchronized (this) {
                if (this.t != null) {
                    this.t.a(motionEvent);
                }
            }
        } else if (this.f3685b != null) {
            this.f3685b.a(motionEvent);
        }
        return super.onTouchEvent(motionEvent);
    }

    public final void p() {
        HashMap hashMap = new HashMap(3);
        hashMap.put("app_muted", String.valueOf(ax.D().b()));
        hashMap.put("app_volume", String.valueOf(ax.D().a()));
        hashMap.put("device_volume", String.valueOf(kn.a(getContext())));
        azq.a((azp) this, "volume", (Map) hashMap);
    }

    public final Context q() {
        return K().b();
    }

    public final synchronized d r() {
        return this.g;
    }

    public final synchronized d s() {
        return this.B;
    }

    public final void setOnClickListener(OnClickListener onClickListener) {
        this.A = new WeakReference<>(onClickListener);
        super.setOnClickListener(onClickListener);
    }

    public final synchronized void setRequestedOrientation(int i2) {
        this.m = i2;
        if (this.g != null) {
            this.g.a(this.m);
        }
    }

    public final void stopLoading() {
        try {
            super.stopLoading();
        } catch (Exception e2) {
            jm.b("Could not stop loading webview.", e2);
        }
    }

    public final synchronized sb t() {
        return this.h;
    }

    public final synchronized String u() {
        return this.i;
    }

    public final /* synthetic */ rv v() {
        return this.f;
    }

    public final WebViewClient w() {
        return this.f3687a;
    }

    public final synchronized boolean x() {
        return this.j;
    }

    public final ahh y() {
        return this.f3685b;
    }

    public final synchronized boolean z() {
        return this.k;
    }
}
