package com.google.android.gms.internal.ads;

import java.util.Arrays;
import java.util.Collection;
import java.util.RandomAccess;

final class aaf extends aab<Boolean> implements abu<Boolean>, RandomAccess {

    /* renamed from: a reason: collision with root package name */
    private static final aaf f2391a;

    /* renamed from: b reason: collision with root package name */
    private boolean[] f2392b;
    private int c;

    static {
        aaf aaf = new aaf();
        f2391a = aaf;
        aaf.b();
    }

    aaf() {
        this(new boolean[10], 0);
    }

    private aaf(boolean[] zArr, int i) {
        this.f2392b = zArr;
        this.c = i;
    }

    private final void a(int i, boolean z) {
        c();
        if (i < 0 || i > this.c) {
            throw new IndexOutOfBoundsException(c(i));
        }
        if (this.c < this.f2392b.length) {
            System.arraycopy(this.f2392b, i, this.f2392b, i + 1, this.c - i);
        } else {
            boolean[] zArr = new boolean[(((this.c * 3) / 2) + 1)];
            System.arraycopy(this.f2392b, 0, zArr, 0, i);
            System.arraycopy(this.f2392b, i, zArr, i + 1, this.c - i);
            this.f2392b = zArr;
        }
        this.f2392b[i] = z;
        this.c++;
        this.modCount++;
    }

    private final void b(int i) {
        if (i < 0 || i >= this.c) {
            throw new IndexOutOfBoundsException(c(i));
        }
    }

    private final String c(int i) {
        return "Index:" + i + ", Size:" + this.c;
    }

    public final /* synthetic */ abu a(int i) {
        if (i >= this.c) {
            return new aaf(Arrays.copyOf(this.f2392b, i), this.c);
        }
        throw new IllegalArgumentException();
    }

    public final void a(boolean z) {
        a(this.c, z);
    }

    public final /* synthetic */ void add(int i, Object obj) {
        a(i, ((Boolean) obj).booleanValue());
    }

    public final boolean addAll(Collection<? extends Boolean> collection) {
        c();
        abr.a(collection);
        if (!(collection instanceof aaf)) {
            return super.addAll(collection);
        }
        aaf aaf = (aaf) collection;
        if (aaf.c == 0) {
            return false;
        }
        if (Integer.MAX_VALUE - this.c < aaf.c) {
            throw new OutOfMemoryError();
        }
        int i = this.c + aaf.c;
        if (i > this.f2392b.length) {
            this.f2392b = Arrays.copyOf(this.f2392b, i);
        }
        System.arraycopy(aaf.f2392b, 0, this.f2392b, this.c, aaf.c);
        this.c = i;
        this.modCount++;
        return true;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof aaf)) {
            return super.equals(obj);
        }
        aaf aaf = (aaf) obj;
        if (this.c != aaf.c) {
            return false;
        }
        boolean[] zArr = aaf.f2392b;
        for (int i = 0; i < this.c; i++) {
            if (this.f2392b[i] != zArr[i]) {
                return false;
            }
        }
        return true;
    }

    public final /* synthetic */ Object get(int i) {
        b(i);
        return Boolean.valueOf(this.f2392b[i]);
    }

    public final int hashCode() {
        int i = 1;
        for (int i2 = 0; i2 < this.c; i2++) {
            i = (i * 31) + abr.a(this.f2392b[i2]);
        }
        return i;
    }

    public final /* synthetic */ Object remove(int i) {
        c();
        b(i);
        boolean z = this.f2392b[i];
        if (i < this.c - 1) {
            System.arraycopy(this.f2392b, i + 1, this.f2392b, i, this.c - i);
        }
        this.c--;
        this.modCount++;
        return Boolean.valueOf(z);
    }

    public final boolean remove(Object obj) {
        c();
        for (int i = 0; i < this.c; i++) {
            if (obj.equals(Boolean.valueOf(this.f2392b[i]))) {
                System.arraycopy(this.f2392b, i + 1, this.f2392b, i, this.c - i);
                this.c--;
                this.modCount++;
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public final void removeRange(int i, int i2) {
        c();
        if (i2 < i) {
            throw new IndexOutOfBoundsException("toIndex < fromIndex");
        }
        System.arraycopy(this.f2392b, i2, this.f2392b, i, this.c - i2);
        this.c -= i2 - i;
        this.modCount++;
    }

    public final /* synthetic */ Object set(int i, Object obj) {
        boolean booleanValue = ((Boolean) obj).booleanValue();
        c();
        b(i);
        boolean z = this.f2392b[i];
        this.f2392b[i] = booleanValue;
        return Boolean.valueOf(z);
    }

    public final int size() {
        return this.c;
    }
}
