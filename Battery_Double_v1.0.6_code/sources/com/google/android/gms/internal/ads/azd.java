package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.util.Base64;
import com.google.android.gms.ads.internal.ax;
import com.google.android.gms.common.internal.a.b.a;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

@cm
final class azd {

    /* renamed from: a reason: collision with root package name */
    final aop f3028a;

    /* renamed from: b reason: collision with root package name */
    final String f3029b;
    final int c;

    private azd(aop aop, String str, int i) {
        this.f3028a = aop;
        this.f3029b = str;
        this.c = i;
    }

    azd(ayy ayy) {
        this(ayy.a(), ayy.c(), ayy.b());
    }

    static azd a(String str) throws IOException {
        String[] split = str.split("\u0000");
        if (split.length != 3) {
            throw new IOException("Incorrect field count for QueueSeed.");
        }
        Parcel obtain = Parcel.obtain();
        try {
            String str2 = new String(Base64.decode(split[0], 0), "UTF-8");
            int parseInt = Integer.parseInt(split[1]);
            byte[] decode = Base64.decode(split[2], 0);
            obtain.unmarshall(decode, 0, decode.length);
            obtain.setDataPosition(0);
            azd azd = new azd((aop) aop.CREATOR.createFromParcel(obtain), str2, parseInt);
            obtain.recycle();
            return azd;
        } catch (a | IllegalArgumentException | IllegalStateException e) {
            ax.i().a(e, "QueueSeed.decode");
            throw new IOException("Malformed QueueSeed encoding.", e);
        } catch (Throwable th) {
            obtain.recycle();
            throw th;
        }
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: 0000 */
    public final String a() {
        Parcel obtain = Parcel.obtain();
        try {
            String encodeToString = Base64.encodeToString(this.f3029b.getBytes("UTF-8"), 0);
            String num = Integer.toString(this.c);
            this.f3028a.writeToParcel(obtain, 0);
            String encodeToString2 = Base64.encodeToString(obtain.marshall(), 0);
            String sb = new StringBuilder(String.valueOf(encodeToString).length() + 2 + String.valueOf(num).length() + String.valueOf(encodeToString2).length()).append(encodeToString).append("\u0000").append(num).append("\u0000").append(encodeToString2).toString();
            obtain.recycle();
            return sb;
        } catch (UnsupportedEncodingException e) {
            jm.c("QueueSeed encode failed because UTF-8 is not available.");
            obtain.recycle();
            return "";
        } catch (Throwable th) {
            obtain.recycle();
            throw th;
        }
    }
}
