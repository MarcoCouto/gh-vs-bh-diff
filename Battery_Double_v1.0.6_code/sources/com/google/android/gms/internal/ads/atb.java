package com.google.android.gms.internal.ads;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.b.a;

public final class atb extends ajk implements asz {
    atb(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.customrenderedad.client.ICustomRenderedAd");
    }

    public final String a() throws RemoteException {
        Parcel a2 = a(1, r_());
        String readString = a2.readString();
        a2.recycle();
        return readString;
    }

    public final void a(a aVar) throws RemoteException {
        Parcel r_ = r_();
        ajm.a(r_, (IInterface) aVar);
        b(3, r_);
    }

    public final String b() throws RemoteException {
        Parcel a2 = a(2, r_());
        String readString = a2.readString();
        a2.recycle();
        return readString;
    }

    public final void c() throws RemoteException {
        b(4, r_());
    }

    public final void d() throws RemoteException {
        b(5, r_());
    }
}
