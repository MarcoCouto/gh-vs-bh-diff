package com.google.android.gms.internal.ads;

import org.json.JSONObject;

final /* synthetic */ class bj implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final bi f3189a;

    /* renamed from: b reason: collision with root package name */
    private final JSONObject f3190b;
    private final ny c;

    bj(bi biVar, JSONObject jSONObject, ny nyVar) {
        this.f3189a = biVar;
        this.f3190b = jSONObject;
        this.c = nyVar;
    }

    public final void run() {
        this.f3189a.a(this.f3190b, this.c);
    }
}
