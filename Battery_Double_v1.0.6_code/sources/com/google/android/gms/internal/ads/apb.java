package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.RemoteException;
import com.google.android.gms.b.b;

final class apb extends a<apq> {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ Context f2828a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ String f2829b;
    private final /* synthetic */ bcr c;
    private final /* synthetic */ aox d;

    apb(aox aox, Context context, String str, bcr bcr) {
        this.d = aox;
        this.f2828a = context;
        this.f2829b = str;
        this.c = bcr;
        super();
    }

    public final /* synthetic */ Object a() throws RemoteException {
        apq a2 = this.d.d.a(this.f2828a, this.f2829b, this.c);
        if (a2 != null) {
            return a2;
        }
        aox.a(this.f2828a, "native_ad");
        return new arh();
    }

    public final /* synthetic */ Object a(aqh aqh) throws RemoteException {
        return aqh.createAdLoaderBuilder(b.a(this.f2828a), this.f2829b, this.c, 12451000);
    }
}
