package com.google.android.gms.internal.ads;

import android.text.TextUtils;
import com.google.android.gms.ads.internal.gmsg.ae;
import java.lang.ref.WeakReference;
import java.util.Map;

public final class aue implements ae<Object> {

    /* renamed from: a reason: collision with root package name */
    private final WeakReference<aua> f2923a;

    /* renamed from: b reason: collision with root package name */
    private final String f2924b;

    public aue(aua aua, String str) {
        this.f2923a = new WeakReference<>(aua);
        this.f2924b = str;
    }

    public final void zza(Object obj, Map<String, String> map) {
        String str = (String) map.get("ads_id");
        String str2 = (String) map.get("eventName");
        if (!TextUtils.isEmpty(str) && this.f2924b.equals(str)) {
            try {
                Integer.parseInt((String) map.get("eventType"));
            } catch (Exception e) {
                jm.b("Parse Scion log event type error", e);
            }
            if ("_ai".equals(str2)) {
                aua aua = (aua) this.f2923a.get();
                if (aua != null) {
                    aua.y();
                }
            } else if ("_ac".equals(str2)) {
                aua aua2 = (aua) this.f2923a.get();
                if (aua2 != null) {
                    aua2.z();
                }
            }
        }
    }
}
