package com.google.android.gms.internal.c;

import android.content.ComponentName;

final class et implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ ep f4047a;

    et(ep epVar) {
        this.f4047a = epVar;
    }

    public final void run() {
        this.f4047a.f4039a.a(new ComponentName(this.f4047a.f4039a.k(), "com.google.android.gms.measurement.AppMeasurementService"));
    }
}
