package com.google.android.gms.internal.c;

final /* synthetic */ class hb implements hh {

    /* renamed from: a reason: collision with root package name */
    private final String f4108a;

    /* renamed from: b reason: collision with root package name */
    private final boolean f4109b = false;

    hb(String str, boolean z) {
        this.f4108a = str;
    }

    public final Object a() {
        return Boolean.valueOf(gt.a(gy.c.getContentResolver(), this.f4108a, this.f4109b));
    }
}
