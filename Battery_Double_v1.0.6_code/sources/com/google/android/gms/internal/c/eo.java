package com.google.android.gms.internal.c;

import android.os.RemoteException;
import java.util.concurrent.atomic.AtomicReference;

final class eo implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ AtomicReference f4037a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ s f4038b;
    private final /* synthetic */ boolean c;
    private final /* synthetic */ eb d;

    eo(eb ebVar, AtomicReference atomicReference, s sVar, boolean z) {
        this.d = ebVar;
        this.f4037a = atomicReference;
        this.f4038b = sVar;
        this.c = z;
    }

    /* JADX INFO: finally extract failed */
    public final void run() {
        synchronized (this.f4037a) {
            try {
                ar d2 = this.d.f4014b;
                if (d2 == null) {
                    this.d.q().v().a("Failed to get user properties");
                    this.f4037a.notify();
                    return;
                }
                this.f4037a.set(d2.a(this.f4038b, this.c));
                this.d.C();
                this.f4037a.notify();
            } catch (RemoteException e) {
                this.d.q().v().a("Failed to get user properties", e);
                this.f4037a.notify();
            } catch (Throwable th) {
                this.f4037a.notify();
                throw th;
            }
        }
    }
}
