package com.google.android.gms.internal.c;

import java.io.IOException;

public final class gk extends d<gk> {
    public long[] c;
    public long[] d;

    public gk() {
        this.c = m.f4116b;
        this.d = m.f4116b;
        this.f3962a = null;
        this.f4112b = -1;
    }

    /* access modifiers changed from: protected */
    public final int a() {
        int i;
        int a2 = super.a();
        if (this.c == null || this.c.length <= 0) {
            i = a2;
        } else {
            int i2 = 0;
            for (long a3 : this.c) {
                i2 += b.a(a3);
            }
            i = a2 + i2 + (this.c.length * 1);
        }
        if (this.d == null || this.d.length <= 0) {
            return i;
        }
        int i3 = 0;
        for (long a4 : this.d) {
            i3 += b.a(a4);
        }
        return i + i3 + (this.d.length * 1);
    }

    public final /* synthetic */ j a(a aVar) throws IOException {
        while (true) {
            int a2 = aVar.a();
            switch (a2) {
                case 0:
                    break;
                case 8:
                    int a3 = m.a(aVar, 8);
                    int length = this.c == null ? 0 : this.c.length;
                    long[] jArr = new long[(a3 + length)];
                    if (length != 0) {
                        System.arraycopy(this.c, 0, jArr, 0, length);
                    }
                    while (length < jArr.length - 1) {
                        jArr[length] = aVar.e();
                        aVar.a();
                        length++;
                    }
                    jArr[length] = aVar.e();
                    this.c = jArr;
                    continue;
                case 10:
                    int c2 = aVar.c(aVar.d());
                    int i = aVar.i();
                    int i2 = 0;
                    while (aVar.h() > 0) {
                        aVar.e();
                        i2++;
                    }
                    aVar.e(i);
                    int length2 = this.c == null ? 0 : this.c.length;
                    long[] jArr2 = new long[(i2 + length2)];
                    if (length2 != 0) {
                        System.arraycopy(this.c, 0, jArr2, 0, length2);
                    }
                    while (length2 < jArr2.length) {
                        jArr2[length2] = aVar.e();
                        length2++;
                    }
                    this.c = jArr2;
                    aVar.d(c2);
                    continue;
                case 16:
                    int a4 = m.a(aVar, 16);
                    int length3 = this.d == null ? 0 : this.d.length;
                    long[] jArr3 = new long[(a4 + length3)];
                    if (length3 != 0) {
                        System.arraycopy(this.d, 0, jArr3, 0, length3);
                    }
                    while (length3 < jArr3.length - 1) {
                        jArr3[length3] = aVar.e();
                        aVar.a();
                        length3++;
                    }
                    jArr3[length3] = aVar.e();
                    this.d = jArr3;
                    continue;
                case 18:
                    int c3 = aVar.c(aVar.d());
                    int i3 = aVar.i();
                    int i4 = 0;
                    while (aVar.h() > 0) {
                        aVar.e();
                        i4++;
                    }
                    aVar.e(i3);
                    int length4 = this.d == null ? 0 : this.d.length;
                    long[] jArr4 = new long[(i4 + length4)];
                    if (length4 != 0) {
                        System.arraycopy(this.d, 0, jArr4, 0, length4);
                    }
                    while (length4 < jArr4.length) {
                        jArr4[length4] = aVar.e();
                        length4++;
                    }
                    this.d = jArr4;
                    aVar.d(c3);
                    continue;
                default:
                    if (!super.a(aVar, a2)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    public final void a(b bVar) throws IOException {
        if (this.c != null && this.c.length > 0) {
            for (long a2 : this.c) {
                bVar.a(1, a2);
            }
        }
        if (this.d != null && this.d.length > 0) {
            for (long a3 : this.d) {
                bVar.a(2, a3);
            }
        }
        super.a(bVar);
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof gk)) {
            return false;
        }
        gk gkVar = (gk) obj;
        if (!h.a(this.c, gkVar.c)) {
            return false;
        }
        if (!h.a(this.d, gkVar.d)) {
            return false;
        }
        return (this.f3962a == null || this.f3962a.b()) ? gkVar.f3962a == null || gkVar.f3962a.b() : this.f3962a.equals(gkVar.f3962a);
    }

    public final int hashCode() {
        return ((this.f3962a == null || this.f3962a.b()) ? 0 : this.f3962a.hashCode()) + ((((((getClass().getName().hashCode() + 527) * 31) + h.a(this.c)) * 31) + h.a(this.d)) * 31);
    }
}
