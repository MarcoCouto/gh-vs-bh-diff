package com.google.android.gms.internal.c;

import android.os.Bundle;
import android.text.TextUtils;
import com.google.android.gms.common.internal.aa;
import java.util.Iterator;

public final class ai {

    /* renamed from: a reason: collision with root package name */
    final String f3857a;

    /* renamed from: b reason: collision with root package name */
    final String f3858b;
    final long c;
    final long d;
    final ak e;
    private final String f;

    ai(ce ceVar, String str, String str2, String str3, long j, long j2, Bundle bundle) {
        ak akVar;
        aa.a(str2);
        aa.a(str3);
        this.f3857a = str2;
        this.f3858b = str3;
        if (TextUtils.isEmpty(str)) {
            str = null;
        }
        this.f = str;
        this.c = j;
        this.d = j2;
        if (this.d != 0 && this.d > this.c) {
            ceVar.q().y().a("Event created with reverse previous/current timestamps. appId", az.a(str2));
        }
        if (bundle == null || bundle.isEmpty()) {
            akVar = new ak(new Bundle());
        } else {
            Bundle bundle2 = new Bundle(bundle);
            Iterator it = bundle2.keySet().iterator();
            while (it.hasNext()) {
                String str4 = (String) it.next();
                if (str4 == null) {
                    ceVar.q().v().a("Param name can't be null");
                    it.remove();
                } else {
                    Object a2 = ceVar.m().a(str4, bundle2.get(str4));
                    if (a2 == null) {
                        ceVar.q().y().a("Param value can't be null", ceVar.n().b(str4));
                        it.remove();
                    } else {
                        ceVar.m().a(bundle2, str4, a2);
                    }
                }
            }
            akVar = new ak(bundle2);
        }
        this.e = akVar;
    }

    private ai(ce ceVar, String str, String str2, String str3, long j, long j2, ak akVar) {
        aa.a(str2);
        aa.a(str3);
        aa.a(akVar);
        this.f3857a = str2;
        this.f3858b = str3;
        if (TextUtils.isEmpty(str)) {
            str = null;
        }
        this.f = str;
        this.c = j;
        this.d = j2;
        if (this.d != 0 && this.d > this.c) {
            ceVar.q().y().a("Event created with reverse previous/current timestamps. appId, name", az.a(str2), az.a(str3));
        }
        this.e = akVar;
    }

    /* access modifiers changed from: 0000 */
    public final ai a(ce ceVar, long j) {
        return new ai(ceVar, this.f, this.f3857a, this.f3858b, this.c, j, this.e);
    }

    public final String toString() {
        String str = this.f3857a;
        String str2 = this.f3858b;
        String valueOf = String.valueOf(this.e);
        return new StringBuilder(String.valueOf(str).length() + 33 + String.valueOf(str2).length() + String.valueOf(valueOf).length()).append("Event{appId='").append(str).append("', name='").append(str2).append("', params=").append(valueOf).append('}').toString();
    }
}
