package com.google.android.gms.internal.c;

import android.content.BroadcastReceiver.PendingResult;
import android.content.Context;
import android.os.Bundle;
import com.google.android.gms.measurement.AppMeasurement;

final class bw implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ ce f3911a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ long f3912b;
    private final /* synthetic */ Bundle c;
    private final /* synthetic */ Context d;
    private final /* synthetic */ az e;
    private final /* synthetic */ PendingResult f;

    bw(bu buVar, ce ceVar, long j, Bundle bundle, Context context, az azVar, PendingResult pendingResult) {
        this.f3911a = ceVar;
        this.f3912b = j;
        this.c = bundle;
        this.d = context;
        this.e = azVar;
        this.f = pendingResult;
    }

    public final void run() {
        long a2 = this.f3911a.c().h.a();
        long j = this.f3912b;
        if (a2 > 0 && (j >= a2 || j <= 0)) {
            j = a2 - 1;
        }
        if (j > 0) {
            this.c.putLong("click_timestamp", j);
        }
        this.c.putString("_cis", "referrer broadcast");
        AppMeasurement.getInstance(this.d).logEventInternal("auto", "_cmp", this.c);
        this.e.C().a("Install campaign recorded");
        if (this.f != null) {
            this.f.finish();
        }
    }
}
