package com.google.android.gms.internal.c;

import java.util.List;
import java.util.concurrent.Callable;

final class cw implements Callable<List<fs>> {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ s f3956a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ cg f3957b;

    cw(cg cgVar, s sVar) {
        this.f3957b = cgVar;
        this.f3956a = sVar;
    }

    public final /* synthetic */ Object call() throws Exception {
        this.f3957b.f3924a.H();
        return this.f3957b.f3924a.D().a(this.f3956a.f4127a);
    }
}
