package com.google.android.gms.internal.c;

import android.content.SharedPreferences.Editor;
import com.google.android.gms.common.internal.aa;

public final class bp {

    /* renamed from: a reason: collision with root package name */
    private final String f3902a;

    /* renamed from: b reason: collision with root package name */
    private final String f3903b = null;
    private boolean c;
    private String d;
    private final /* synthetic */ bk e;

    public bp(bk bkVar, String str, String str2) {
        this.e = bkVar;
        aa.a(str);
        this.f3902a = str;
    }

    public final String a() {
        if (!this.c) {
            this.c = true;
            this.d = this.e.C().getString(this.f3902a, null);
        }
        return this.d;
    }

    public final void a(String str) {
        if (!ft.b(str, this.d)) {
            Editor edit = this.e.C().edit();
            edit.putString(this.f3902a, str);
            edit.apply();
            this.d = str;
        }
    }
}
