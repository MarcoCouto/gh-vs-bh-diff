package com.google.android.gms.internal.c;

import com.google.android.gms.common.internal.aa;

final class aj {

    /* renamed from: a reason: collision with root package name */
    final String f3859a;

    /* renamed from: b reason: collision with root package name */
    final String f3860b;
    final long c;
    final long d;
    final long e;
    final long f;
    final Long g;
    final Long h;
    final Boolean i;

    aj(String str, String str2, long j, long j2, long j3, long j4, Long l, Long l2, Boolean bool) {
        aa.a(str);
        aa.a(str2);
        aa.b(j >= 0);
        aa.b(j2 >= 0);
        aa.b(j4 >= 0);
        this.f3859a = str;
        this.f3860b = str2;
        this.c = j;
        this.d = j2;
        this.e = j3;
        this.f = j4;
        this.g = l;
        this.h = l2;
        this.i = bool;
    }

    /* access modifiers changed from: 0000 */
    public final aj a() {
        return new aj(this.f3859a, this.f3860b, this.c + 1, this.d + 1, this.e, this.f, this.g, this.h, this.i);
    }

    /* access modifiers changed from: 0000 */
    public final aj a(long j) {
        return new aj(this.f3859a, this.f3860b, this.c, this.d, j, this.f, this.g, this.h, this.i);
    }

    /* access modifiers changed from: 0000 */
    public final aj a(Long l, Long l2, Boolean bool) {
        return new aj(this.f3859a, this.f3860b, this.c, this.d, this.e, this.f, l, l2, (bool == null || bool.booleanValue()) ? bool : null);
    }

    /* access modifiers changed from: 0000 */
    public final aj b(long j) {
        return new aj(this.f3859a, this.f3860b, this.c, this.d, this.e, j, this.g, this.h, this.i);
    }
}
