package com.google.android.gms.internal.c;

final class cs implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ an f3948a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ String f3949b;
    private final /* synthetic */ cg c;

    cs(cg cgVar, an anVar, String str) {
        this.c = cgVar;
        this.f3948a = anVar;
        this.f3949b = str;
    }

    public final void run() {
        this.c.f3924a.H();
        this.c.f3924a.a(this.f3948a, this.f3949b);
    }
}
