package com.google.android.gms.internal.c;

import java.io.IOException;

public final class gf extends d<gf> {
    private static volatile gf[] g;
    public Integer c;
    public gk d;
    public gk e;
    public Boolean f;

    public gf() {
        this.c = null;
        this.d = null;
        this.e = null;
        this.f = null;
        this.f3962a = null;
        this.f4112b = -1;
    }

    public static gf[] e() {
        if (g == null) {
            synchronized (h.f4106b) {
                if (g == null) {
                    g = new gf[0];
                }
            }
        }
        return g;
    }

    /* access modifiers changed from: protected */
    public final int a() {
        int a2 = super.a();
        if (this.c != null) {
            a2 += b.b(1, this.c.intValue());
        }
        if (this.d != null) {
            a2 += b.b(2, (j) this.d);
        }
        if (this.e != null) {
            a2 += b.b(3, (j) this.e);
        }
        if (this.f == null) {
            return a2;
        }
        this.f.booleanValue();
        return a2 + b.b(4) + 1;
    }

    public final /* synthetic */ j a(a aVar) throws IOException {
        while (true) {
            int a2 = aVar.a();
            switch (a2) {
                case 0:
                    break;
                case 8:
                    this.c = Integer.valueOf(aVar.d());
                    continue;
                case 18:
                    if (this.d == null) {
                        this.d = new gk();
                    }
                    aVar.a((j) this.d);
                    continue;
                case 26:
                    if (this.e == null) {
                        this.e = new gk();
                    }
                    aVar.a((j) this.e);
                    continue;
                case 32:
                    this.f = Boolean.valueOf(aVar.b());
                    continue;
                default:
                    if (!super.a(aVar, a2)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    public final void a(b bVar) throws IOException {
        if (this.c != null) {
            bVar.a(1, this.c.intValue());
        }
        if (this.d != null) {
            bVar.a(2, (j) this.d);
        }
        if (this.e != null) {
            bVar.a(3, (j) this.e);
        }
        if (this.f != null) {
            bVar.a(4, this.f.booleanValue());
        }
        super.a(bVar);
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof gf)) {
            return false;
        }
        gf gfVar = (gf) obj;
        if (this.c == null) {
            if (gfVar.c != null) {
                return false;
            }
        } else if (!this.c.equals(gfVar.c)) {
            return false;
        }
        if (this.d == null) {
            if (gfVar.d != null) {
                return false;
            }
        } else if (!this.d.equals(gfVar.d)) {
            return false;
        }
        if (this.e == null) {
            if (gfVar.e != null) {
                return false;
            }
        } else if (!this.e.equals(gfVar.e)) {
            return false;
        }
        if (this.f == null) {
            if (gfVar.f != null) {
                return false;
            }
        } else if (!this.f.equals(gfVar.f)) {
            return false;
        }
        return (this.f3962a == null || this.f3962a.b()) ? gfVar.f3962a == null || gfVar.f3962a.b() : this.f3962a.equals(gfVar.f3962a);
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = (this.c == null ? 0 : this.c.hashCode()) + ((getClass().getName().hashCode() + 527) * 31);
        gk gkVar = this.d;
        int hashCode2 = (gkVar == null ? 0 : gkVar.hashCode()) + (hashCode * 31);
        gk gkVar2 = this.e;
        int hashCode3 = ((this.f == null ? 0 : this.f.hashCode()) + (((gkVar2 == null ? 0 : gkVar2.hashCode()) + (hashCode2 * 31)) * 31)) * 31;
        if (this.f3962a != null && !this.f3962a.b()) {
            i = this.f3962a.hashCode();
        }
        return hashCode3 + i;
    }
}
