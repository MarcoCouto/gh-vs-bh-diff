package com.google.android.gms.internal.c;

import android.content.Context;
import android.os.Bundle;
import com.google.android.gms.common.internal.aa;
import com.google.android.gms.measurement.AppMeasurement.a;
import com.google.android.gms.measurement.AppMeasurement.d;
import com.google.android.gms.measurement.AppMeasurement.e;
import java.util.concurrent.atomic.AtomicReference;

public final class ax extends da {

    /* renamed from: a reason: collision with root package name */
    private static final AtomicReference<String[]> f3875a = new AtomicReference<>();

    /* renamed from: b reason: collision with root package name */
    private static final AtomicReference<String[]> f3876b = new AtomicReference<>();
    private static final AtomicReference<String[]> c = new AtomicReference<>();

    ax(ce ceVar) {
        super(ceVar);
    }

    private final String a(ak akVar) {
        if (akVar == null) {
            return null;
        }
        return !v() ? akVar.toString() : a(akVar.b());
    }

    private static String a(String str, String[] strArr, String[] strArr2, AtomicReference<String[]> atomicReference) {
        int i = 0;
        aa.a(strArr);
        aa.a(strArr2);
        aa.a(atomicReference);
        aa.b(strArr.length == strArr2.length);
        while (true) {
            if (i >= strArr.length) {
                break;
            } else if (ft.b(str, strArr[i])) {
                synchronized (atomicReference) {
                    String[] strArr3 = (String[]) atomicReference.get();
                    if (strArr3 == null) {
                        strArr3 = new String[strArr2.length];
                        atomicReference.set(strArr3);
                    }
                    if (strArr3[i] == null) {
                        StringBuilder sb = new StringBuilder();
                        sb.append(strArr2[i]);
                        sb.append("(");
                        sb.append(strArr[i]);
                        sb.append(")");
                        strArr3[i] = sb.toString();
                    }
                    str = strArr3[i];
                }
            } else {
                i++;
            }
        }
        return str;
    }

    private static void a(StringBuilder sb, int i) {
        for (int i2 = 0; i2 < i; i2++) {
            sb.append("  ");
        }
    }

    private final void a(StringBuilder sb, int i, fy fyVar) {
        String[] strArr;
        if (fyVar != null) {
            a(sb, i);
            sb.append("filter {\n");
            a(sb, i, "complement", (Object) fyVar.e);
            a(sb, i, "param_name", (Object) b(fyVar.f));
            int i2 = i + 1;
            String str = "string_filter";
            gb gbVar = fyVar.c;
            if (gbVar != null) {
                a(sb, i2);
                sb.append(str);
                sb.append(" {\n");
                if (gbVar.c != null) {
                    String str2 = "UNKNOWN_MATCH_TYPE";
                    switch (gbVar.c.intValue()) {
                        case 1:
                            str2 = "REGEXP";
                            break;
                        case 2:
                            str2 = "BEGINS_WITH";
                            break;
                        case 3:
                            str2 = "ENDS_WITH";
                            break;
                        case 4:
                            str2 = "PARTIAL";
                            break;
                        case 5:
                            str2 = "EXACT";
                            break;
                        case 6:
                            str2 = "IN_LIST";
                            break;
                    }
                    a(sb, i2, "match_type", (Object) str2);
                }
                a(sb, i2, "expression", (Object) gbVar.d);
                a(sb, i2, "case_sensitive", (Object) gbVar.e);
                if (gbVar.f.length > 0) {
                    a(sb, i2 + 1);
                    sb.append("expression_list {\n");
                    for (String str3 : gbVar.f) {
                        a(sb, i2 + 2);
                        sb.append(str3);
                        sb.append("\n");
                    }
                    sb.append("}\n");
                }
                a(sb, i2);
                sb.append("}\n");
            }
            a(sb, i + 1, "number_filter", fyVar.d);
            a(sb, i);
            sb.append("}\n");
        }
    }

    private final void a(StringBuilder sb, int i, String str, fz fzVar) {
        if (fzVar != null) {
            a(sb, i);
            sb.append(str);
            sb.append(" {\n");
            if (fzVar.c != null) {
                String str2 = "UNKNOWN_COMPARISON_TYPE";
                switch (fzVar.c.intValue()) {
                    case 1:
                        str2 = "LESS_THAN";
                        break;
                    case 2:
                        str2 = "GREATER_THAN";
                        break;
                    case 3:
                        str2 = "EQUAL";
                        break;
                    case 4:
                        str2 = "BETWEEN";
                        break;
                }
                a(sb, i, "comparison_type", (Object) str2);
            }
            a(sb, i, "match_as_float", (Object) fzVar.d);
            a(sb, i, "comparison_value", (Object) fzVar.e);
            a(sb, i, "min_comparison_value", (Object) fzVar.f);
            a(sb, i, "max_comparison_value", (Object) fzVar.g);
            a(sb, i);
            sb.append("}\n");
        }
    }

    private static void a(StringBuilder sb, int i, String str, gk gkVar) {
        int i2 = 0;
        if (gkVar != null) {
            a(sb, 3);
            sb.append(str);
            sb.append(" {\n");
            if (gkVar.d != null) {
                a(sb, 4);
                sb.append("results: ");
                long[] jArr = gkVar.d;
                int length = jArr.length;
                int i3 = 0;
                int i4 = 0;
                while (i3 < length) {
                    Long valueOf = Long.valueOf(jArr[i3]);
                    int i5 = i4 + 1;
                    if (i4 != 0) {
                        sb.append(", ");
                    }
                    sb.append(valueOf);
                    i3++;
                    i4 = i5;
                }
                sb.append(10);
            }
            if (gkVar.c != null) {
                a(sb, 4);
                sb.append("status: ");
                long[] jArr2 = gkVar.c;
                int length2 = jArr2.length;
                int i6 = 0;
                while (i2 < length2) {
                    Long valueOf2 = Long.valueOf(jArr2[i2]);
                    int i7 = i6 + 1;
                    if (i6 != 0) {
                        sb.append(", ");
                    }
                    sb.append(valueOf2);
                    i2++;
                    i6 = i7;
                }
                sb.append(10);
            }
            a(sb, 3);
            sb.append("}\n");
        }
    }

    private static void a(StringBuilder sb, int i, String str, Object obj) {
        if (obj != null) {
            a(sb, i + 1);
            sb.append(str);
            sb.append(": ");
            sb.append(obj);
            sb.append(10);
        }
    }

    private final boolean v() {
        return this.q.q().a(3);
    }

    /* access modifiers changed from: protected */
    public final String a(Bundle bundle) {
        if (bundle == null) {
            return null;
        }
        if (!v()) {
            return bundle.toString();
        }
        StringBuilder sb = new StringBuilder();
        for (String str : bundle.keySet()) {
            if (sb.length() != 0) {
                sb.append(", ");
            } else {
                sb.append("Bundle[{");
            }
            sb.append(b(str));
            sb.append("=");
            sb.append(bundle.get(str));
        }
        sb.append("}]");
        return sb.toString();
    }

    /* access modifiers changed from: protected */
    public final String a(ai aiVar) {
        if (aiVar == null) {
            return null;
        }
        if (!v()) {
            return aiVar.toString();
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Event{appId='");
        sb.append(aiVar.f3857a);
        sb.append("', name='");
        sb.append(a(aiVar.f3858b));
        sb.append("', params=");
        sb.append(a(aiVar.e));
        sb.append("}");
        return sb.toString();
    }

    /* access modifiers changed from: protected */
    public final String a(an anVar) {
        if (anVar == null) {
            return null;
        }
        if (!v()) {
            return anVar.toString();
        }
        StringBuilder sb = new StringBuilder();
        sb.append("origin=");
        sb.append(anVar.c);
        sb.append(",name=");
        sb.append(a(anVar.f3864a));
        sb.append(",params=");
        sb.append(a(anVar.f3865b));
        return sb.toString();
    }

    /* access modifiers changed from: protected */
    public final String a(fx fxVar) {
        if (fxVar == null) {
            return "null";
        }
        StringBuilder sb = new StringBuilder();
        sb.append("\nevent_filter {\n");
        a(sb, 0, "filter_id", (Object) fxVar.c);
        a(sb, 0, "event_name", (Object) a(fxVar.d));
        a(sb, 1, "event_count_filter", fxVar.f);
        sb.append("  filters {\n");
        for (fy a2 : fxVar.e) {
            a(sb, 2, a2);
        }
        a(sb, 1);
        sb.append("}\n}\n");
        return sb.toString();
    }

    /* access modifiers changed from: protected */
    public final String a(ga gaVar) {
        if (gaVar == null) {
            return "null";
        }
        StringBuilder sb = new StringBuilder();
        sb.append("\nproperty_filter {\n");
        a(sb, 0, "filter_id", (Object) gaVar.c);
        a(sb, 0, "property_name", (Object) c(gaVar.d));
        a(sb, 1, gaVar.e);
        sb.append("}\n");
        return sb.toString();
    }

    /* access modifiers changed from: protected */
    public final String a(gi giVar) {
        gj[] gjVarArr;
        StringBuilder sb = new StringBuilder();
        sb.append("\nbatch {\n");
        if (giVar.c != null) {
            for (gj gjVar : giVar.c) {
                if (!(gjVar == null || gjVar == null)) {
                    a(sb, 1);
                    sb.append("bundle {\n");
                    a(sb, 1, "protocol_version", (Object) gjVar.c);
                    a(sb, 1, "platform", (Object) gjVar.k);
                    a(sb, 1, "gmp_version", (Object) gjVar.s);
                    a(sb, 1, "uploading_gmp_version", (Object) gjVar.t);
                    a(sb, 1, "config_version", (Object) gjVar.G);
                    a(sb, 1, "gmp_app_id", (Object) gjVar.A);
                    a(sb, 1, "app_id", (Object) gjVar.q);
                    a(sb, 1, "app_version", (Object) gjVar.r);
                    a(sb, 1, "app_version_major", (Object) gjVar.E);
                    a(sb, 1, "firebase_instance_id", (Object) gjVar.D);
                    a(sb, 1, "dev_cert_hash", (Object) gjVar.x);
                    a(sb, 1, "app_store", (Object) gjVar.p);
                    a(sb, 1, "upload_timestamp_millis", (Object) gjVar.f);
                    a(sb, 1, "start_timestamp_millis", (Object) gjVar.g);
                    a(sb, 1, "end_timestamp_millis", (Object) gjVar.h);
                    a(sb, 1, "previous_bundle_start_timestamp_millis", (Object) gjVar.i);
                    a(sb, 1, "previous_bundle_end_timestamp_millis", (Object) gjVar.j);
                    a(sb, 1, "app_instance_id", (Object) gjVar.w);
                    a(sb, 1, "resettable_device_id", (Object) gjVar.u);
                    a(sb, 1, "device_id", (Object) gjVar.F);
                    a(sb, 1, "limited_ad_tracking", (Object) gjVar.v);
                    a(sb, 1, "os_version", (Object) gjVar.l);
                    a(sb, 1, "device_model", (Object) gjVar.m);
                    a(sb, 1, "user_default_language", (Object) gjVar.n);
                    a(sb, 1, "time_zone_offset_minutes", (Object) gjVar.o);
                    a(sb, 1, "bundle_sequential_index", (Object) gjVar.y);
                    a(sb, 1, "service_upload", (Object) gjVar.B);
                    a(sb, 1, "health_monitor", (Object) gjVar.z);
                    if (!(gjVar.H == null || gjVar.H.longValue() == 0)) {
                        a(sb, 1, "android_id", (Object) gjVar.H);
                    }
                    if (gjVar.J != null) {
                        a(sb, 1, "retry_counter", (Object) gjVar.J);
                    }
                    gl[] glVarArr = gjVar.e;
                    if (glVarArr != null) {
                        for (gl glVar : glVarArr) {
                            if (glVar != null) {
                                a(sb, 2);
                                sb.append("user_property {\n");
                                a(sb, 2, "set_timestamp_millis", (Object) glVar.c);
                                a(sb, 2, "name", (Object) c(glVar.d));
                                a(sb, 2, "string_value", (Object) glVar.e);
                                a(sb, 2, "int_value", (Object) glVar.f);
                                a(sb, 2, "double_value", (Object) glVar.g);
                                a(sb, 2);
                                sb.append("}\n");
                            }
                        }
                    }
                    gf[] gfVarArr = gjVar.C;
                    if (gfVarArr != null) {
                        for (gf gfVar : gfVarArr) {
                            if (gfVar != null) {
                                a(sb, 2);
                                sb.append("audience_membership {\n");
                                a(sb, 2, "audience_id", (Object) gfVar.c);
                                a(sb, 2, "new_audience", (Object) gfVar.f);
                                a(sb, 2, "current_data", gfVar.d);
                                a(sb, 2, "previous_data", gfVar.e);
                                a(sb, 2);
                                sb.append("}\n");
                            }
                        }
                    }
                    gg[] ggVarArr = gjVar.d;
                    if (ggVarArr != null) {
                        for (gg ggVar : ggVarArr) {
                            if (ggVar != null) {
                                a(sb, 2);
                                sb.append("event {\n");
                                a(sb, 2, "name", (Object) a(ggVar.d));
                                a(sb, 2, "timestamp_millis", (Object) ggVar.e);
                                a(sb, 2, "previous_timestamp_millis", (Object) ggVar.f);
                                a(sb, 2, "count", (Object) ggVar.g);
                                gh[] ghVarArr = ggVar.c;
                                if (ghVarArr != null) {
                                    for (gh ghVar : ghVarArr) {
                                        if (ghVar != null) {
                                            a(sb, 3);
                                            sb.append("param {\n");
                                            a(sb, 3, "name", (Object) b(ghVar.c));
                                            a(sb, 3, "string_value", (Object) ghVar.d);
                                            a(sb, 3, "int_value", (Object) ghVar.e);
                                            a(sb, 3, "double_value", (Object) ghVar.f);
                                            a(sb, 3);
                                            sb.append("}\n");
                                        }
                                    }
                                }
                                a(sb, 2);
                                sb.append("}\n");
                            }
                        }
                    }
                    a(sb, 1);
                    sb.append("}\n");
                }
            }
        }
        sb.append("}\n");
        return sb.toString();
    }

    /* access modifiers changed from: protected */
    public final String a(String str) {
        if (str == null) {
            return null;
        }
        return v() ? a(str, a.f4139b, a.f4138a, f3875a) : str;
    }

    public final /* bridge */ /* synthetic */ void a() {
        super.a();
    }

    /* access modifiers changed from: protected */
    public final String b(String str) {
        if (str == null) {
            return null;
        }
        return v() ? a(str, d.f4141b, d.f4140a, f3876b) : str;
    }

    public final /* bridge */ /* synthetic */ void b() {
        super.b();
    }

    /* access modifiers changed from: protected */
    public final String c(String str) {
        if (str == null) {
            return null;
        }
        if (!v()) {
            return str;
        }
        if (!str.startsWith("_exp_")) {
            return a(str, e.f4143b, e.f4142a, c);
        }
        StringBuilder sb = new StringBuilder();
        sb.append("experiment_id");
        sb.append("(");
        sb.append(str);
        sb.append(")");
        return sb.toString();
    }

    public final /* bridge */ /* synthetic */ void c() {
        super.c();
    }

    public final /* bridge */ /* synthetic */ n d() {
        return super.d();
    }

    public final /* bridge */ /* synthetic */ dd e() {
        return super.e();
    }

    public final /* bridge */ /* synthetic */ au f() {
        return super.f();
    }

    public final /* bridge */ /* synthetic */ ah g() {
        return super.g();
    }

    public final /* bridge */ /* synthetic */ eb h() {
        return super.h();
    }

    public final /* bridge */ /* synthetic */ dy i() {
        return super.i();
    }

    public final /* bridge */ /* synthetic */ com.google.android.gms.common.util.e j() {
        return super.j();
    }

    public final /* bridge */ /* synthetic */ Context k() {
        return super.k();
    }

    public final /* bridge */ /* synthetic */ av l() {
        return super.l();
    }

    public final /* bridge */ /* synthetic */ ax m() {
        return super.m();
    }

    public final /* bridge */ /* synthetic */ ft n() {
        return super.n();
    }

    public final /* bridge */ /* synthetic */ fa o() {
        return super.o();
    }

    public final /* bridge */ /* synthetic */ bz p() {
        return super.p();
    }

    public final /* bridge */ /* synthetic */ az q() {
        return super.q();
    }

    public final /* bridge */ /* synthetic */ bk r() {
        return super.r();
    }

    public final /* bridge */ /* synthetic */ y s() {
        return super.s();
    }

    /* access modifiers changed from: protected */
    public final boolean t() {
        return false;
    }
}
