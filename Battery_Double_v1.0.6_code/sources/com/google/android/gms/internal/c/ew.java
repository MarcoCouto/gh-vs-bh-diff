package com.google.android.gms.internal.c;

import android.content.Intent;

final /* synthetic */ class ew implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final ev f4050a;

    /* renamed from: b reason: collision with root package name */
    private final int f4051b;
    private final az c;
    private final Intent d;

    ew(ev evVar, int i, az azVar, Intent intent) {
        this.f4050a = evVar;
        this.f4051b = i;
        this.c = azVar;
        this.d = intent;
    }

    public final void run() {
        this.f4050a.a(this.f4051b, this.c, this.d);
    }
}
