package com.google.android.gms.internal.c;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import com.google.android.gms.common.internal.a.a;
import com.google.android.gms.common.internal.a.c;
import com.google.android.gms.common.internal.aa;

public final class s extends a {
    public static final Creator<s> CREATOR = new t();

    /* renamed from: a reason: collision with root package name */
    public final String f4127a;

    /* renamed from: b reason: collision with root package name */
    public final String f4128b;
    public final String c;
    public final String d;
    public final long e;
    public final long f;
    public final String g;
    public final boolean h;
    public final boolean i;
    public final long j;
    public final String k;
    public final long l;
    public final long m;
    public final int n;
    public final boolean o;
    public final boolean p;
    public final boolean q;

    s(String str, String str2, String str3, long j2, String str4, long j3, long j4, String str5, boolean z, boolean z2, String str6, long j5, long j6, int i2, boolean z3, boolean z4, boolean z5) {
        aa.a(str);
        this.f4127a = str;
        if (TextUtils.isEmpty(str2)) {
            str2 = null;
        }
        this.f4128b = str2;
        this.c = str3;
        this.j = j2;
        this.d = str4;
        this.e = j3;
        this.f = j4;
        this.g = str5;
        this.h = z;
        this.i = z2;
        this.k = str6;
        this.l = j5;
        this.m = j6;
        this.n = i2;
        this.o = z3;
        this.p = z4;
        this.q = z5;
    }

    s(String str, String str2, String str3, String str4, long j2, long j3, String str5, boolean z, boolean z2, long j4, String str6, long j5, long j6, int i2, boolean z3, boolean z4, boolean z5) {
        this.f4127a = str;
        this.f4128b = str2;
        this.c = str3;
        this.j = j4;
        this.d = str4;
        this.e = j2;
        this.f = j3;
        this.g = str5;
        this.h = z;
        this.i = z2;
        this.k = str6;
        this.l = j5;
        this.m = j6;
        this.n = i2;
        this.o = z3;
        this.p = z4;
        this.q = z5;
    }

    public final void writeToParcel(Parcel parcel, int i2) {
        int a2 = c.a(parcel);
        c.a(parcel, 2, this.f4127a, false);
        c.a(parcel, 3, this.f4128b, false);
        c.a(parcel, 4, this.c, false);
        c.a(parcel, 5, this.d, false);
        c.a(parcel, 6, this.e);
        c.a(parcel, 7, this.f);
        c.a(parcel, 8, this.g, false);
        c.a(parcel, 9, this.h);
        c.a(parcel, 10, this.i);
        c.a(parcel, 11, this.j);
        c.a(parcel, 12, this.k, false);
        c.a(parcel, 13, this.l);
        c.a(parcel, 14, this.m);
        c.a(parcel, 15, this.n);
        c.a(parcel, 16, this.o);
        c.a(parcel, 17, this.p);
        c.a(parcel, 18, this.q);
        c.a(parcel, a2);
    }
}
