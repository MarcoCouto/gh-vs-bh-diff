package com.google.android.gms.internal.c;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Pattern;

public class gt {

    /* renamed from: a reason: collision with root package name */
    public static final Pattern f4096a = Pattern.compile("^(1|true|t|on|yes|y)$", 2);

    /* renamed from: b reason: collision with root package name */
    public static final Pattern f4097b = Pattern.compile("^(0|false|f|off|no|n)$", 2);
    private static final Uri c = Uri.parse("content://com.google.android.gsf.gservices");
    private static final Uri d = Uri.parse("content://com.google.android.gsf.gservices/prefix");
    /* access modifiers changed from: private */
    public static final AtomicBoolean e = new AtomicBoolean();
    private static HashMap<String, String> f;
    private static final HashMap<String, Boolean> g = new HashMap<>();
    private static final HashMap<String, Integer> h = new HashMap<>();
    private static final HashMap<String, Long> i = new HashMap<>();
    private static final HashMap<String, Float> j = new HashMap<>();
    private static Object k;
    private static boolean l;
    private static String[] m = new String[0];

    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        return r0;
     */
    private static <T> T a(HashMap<String, T> hashMap, String str, T t) {
        synchronized (gt.class) {
            if (!hashMap.containsKey(str)) {
                return null;
            }
            T t2 = hashMap.get(str);
            if (t2 == null) {
                t2 = t;
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0030, code lost:
        if (l == false) goto L_0x003a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0038, code lost:
        if (f.isEmpty() == false) goto L_0x0060;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x003a, code lost:
        f.putAll(a(r9, m));
        l = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x004e, code lost:
        if (f.containsKey(r10) == false) goto L_0x0060;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0050, code lost:
        r0 = (java.lang.String) f.get(r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0058, code lost:
        if (r0 == null) goto L_0x005b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x005a, code lost:
        r2 = r0;
     */
    public static String a(ContentResolver contentResolver, String str, String str2) {
        String str3 = null;
        synchronized (gt.class) {
            a(contentResolver);
            Object obj = k;
            if (f.containsKey(str)) {
                String str4 = (String) f.get(str);
                if (str4 != null) {
                    str3 = str4;
                }
            } else {
                String[] strArr = m;
                int length = strArr.length;
                int i2 = 0;
                while (true) {
                    if (i2 >= length) {
                        Cursor query = contentResolver.query(c, null, null, new String[]{str}, null);
                        if (query != null) {
                            try {
                                if (query.moveToFirst()) {
                                    String string = query.getString(1);
                                    if (string != null && string.equals(null)) {
                                        string = null;
                                    }
                                    a(obj, str, string);
                                    if (string != null) {
                                        str3 = string;
                                    }
                                    if (query != null) {
                                        query.close();
                                    }
                                }
                            } catch (Throwable th) {
                                if (query != null) {
                                    query.close();
                                }
                                throw th;
                            }
                        }
                        a(obj, str, (String) null);
                        if (query != null) {
                            query.close();
                        }
                    } else if (str.startsWith(strArr[i2])) {
                        break;
                    } else {
                        i2++;
                    }
                }
            }
        }
        return str3;
    }

    private static Map<String, String> a(ContentResolver contentResolver, String... strArr) {
        Cursor query = contentResolver.query(d, null, null, strArr, null);
        TreeMap treeMap = new TreeMap();
        if (query != null) {
            while (query.moveToNext()) {
                try {
                    treeMap.put(query.getString(0), query.getString(1));
                } finally {
                    query.close();
                }
            }
        }
        return treeMap;
    }

    private static void a(ContentResolver contentResolver) {
        if (f == null) {
            e.set(false);
            f = new HashMap<>();
            k = new Object();
            l = false;
            contentResolver.registerContentObserver(c, true, new gu(null));
        } else if (e.getAndSet(false)) {
            f.clear();
            g.clear();
            h.clear();
            i.clear();
            j.clear();
            k = new Object();
            l = false;
        }
    }

    private static void a(Object obj, String str, String str2) {
        synchronized (gt.class) {
            if (obj == k) {
                f.put(str, str2);
            }
        }
    }

    public static boolean a(ContentResolver contentResolver, String str, boolean z) {
        Object b2 = b(contentResolver);
        Boolean bool = (Boolean) a(g, str, (T) Boolean.valueOf(z));
        if (bool != null) {
            return bool.booleanValue();
        }
        String a2 = a(contentResolver, str, (String) null);
        if (a2 != null && !a2.equals("")) {
            if (f4096a.matcher(a2).matches()) {
                bool = Boolean.valueOf(true);
                z = true;
            } else if (f4097b.matcher(a2).matches()) {
                bool = Boolean.valueOf(false);
                z = false;
            } else {
                Log.w("Gservices", "attempt to read gservices key " + str + " (value \"" + a2 + "\") as boolean");
            }
        }
        HashMap<String, Boolean> hashMap = g;
        synchronized (gt.class) {
            if (b2 == k) {
                hashMap.put(str, bool);
                f.remove(str);
            }
        }
        return z;
    }

    private static Object b(ContentResolver contentResolver) {
        Object obj;
        synchronized (gt.class) {
            a(contentResolver);
            obj = k;
        }
        return obj;
    }
}
