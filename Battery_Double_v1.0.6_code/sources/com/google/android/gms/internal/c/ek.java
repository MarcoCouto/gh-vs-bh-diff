package com.google.android.gms.internal.c;

import android.os.RemoteException;
import android.text.TextUtils;

final class ek implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ boolean f4029a = true;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ boolean f4030b;
    private final /* synthetic */ w c;
    private final /* synthetic */ s d;
    private final /* synthetic */ w e;
    private final /* synthetic */ eb f;

    ek(eb ebVar, boolean z, boolean z2, w wVar, s sVar, w wVar2) {
        this.f = ebVar;
        this.f4030b = z2;
        this.c = wVar;
        this.d = sVar;
        this.e = wVar2;
    }

    public final void run() {
        ar d2 = this.f.f4014b;
        if (d2 == null) {
            this.f.q().v().a("Discarding data. Failed to send conditional user property to service");
            return;
        }
        if (this.f4029a) {
            this.f.a(d2, this.f4030b ? null : this.c, this.d);
        } else {
            try {
                if (TextUtils.isEmpty(this.e.f4129a)) {
                    d2.a(this.c, this.d);
                } else {
                    d2.a(this.c);
                }
            } catch (RemoteException e2) {
                this.f.q().v().a("Failed to send conditional user property to the service", e2);
            }
        }
        this.f.C();
    }
}
