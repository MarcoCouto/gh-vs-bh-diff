package com.google.android.gms.internal.c;

import java.util.concurrent.atomic.AtomicReference;

final class di implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ AtomicReference f3974a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ dd f3975b;

    di(dd ddVar, AtomicReference atomicReference) {
        this.f3975b = ddVar;
        this.f3974a = atomicReference;
    }

    public final void run() {
        this.f3975b.h().a(this.f3974a);
    }
}
