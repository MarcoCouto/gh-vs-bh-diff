package com.google.android.gms.internal.c;

import com.google.android.gms.common.internal.aa;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

final class cc<V> extends FutureTask<V> implements Comparable<cc> {

    /* renamed from: a reason: collision with root package name */
    final boolean f3918a;

    /* renamed from: b reason: collision with root package name */
    private final long f3919b = bz.k.getAndIncrement();
    private final String c;
    private final /* synthetic */ bz d;

    cc(bz bzVar, Runnable runnable, boolean z, String str) {
        this.d = bzVar;
        super(runnable, null);
        aa.a(str);
        this.c = str;
        this.f3918a = false;
        if (this.f3919b == Long.MAX_VALUE) {
            bzVar.q().v().a("Tasks index overflow");
        }
    }

    cc(bz bzVar, Callable<V> callable, boolean z, String str) {
        this.d = bzVar;
        super(callable);
        aa.a(str);
        this.c = str;
        this.f3918a = z;
        if (this.f3919b == Long.MAX_VALUE) {
            bzVar.q().v().a("Tasks index overflow");
        }
    }

    public final /* synthetic */ int compareTo(Object obj) {
        cc ccVar = (cc) obj;
        if (this.f3918a != ccVar.f3918a) {
            return this.f3918a ? -1 : 1;
        }
        if (this.f3919b < ccVar.f3919b) {
            return -1;
        }
        if (this.f3919b > ccVar.f3919b) {
            return 1;
        }
        this.d.q().w().a("Two tasks share the same index. index", Long.valueOf(this.f3919b));
        return 0;
    }

    /* access modifiers changed from: protected */
    public final void setException(Throwable th) {
        this.d.q().v().a(this.c, th);
        if (th instanceof ca) {
            Thread.getDefaultUncaughtExceptionHandler().uncaughtException(Thread.currentThread(), th);
        }
        super.setException(th);
    }
}
