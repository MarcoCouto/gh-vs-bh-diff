package com.google.android.gms.internal.c;

final class ck implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ w f3932a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ cg f3933b;

    ck(cg cgVar, w wVar) {
        this.f3933b = cgVar;
        this.f3932a = wVar;
    }

    public final void run() {
        this.f3933b.f3924a.H();
        fk a2 = this.f3933b.f3924a;
        w wVar = this.f3932a;
        s a3 = a2.a(wVar.f4129a);
        if (a3 != null) {
            a2.b(wVar, a3);
        }
    }
}
