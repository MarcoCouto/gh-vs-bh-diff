package com.google.android.gms.internal.c;

final class cj implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ w f3930a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ s f3931b;
    private final /* synthetic */ cg c;

    cj(cg cgVar, w wVar, s sVar) {
        this.c = cgVar;
        this.f3930a = wVar;
        this.f3931b = sVar;
    }

    public final void run() {
        this.c.f3924a.H();
        this.c.f3924a.a(this.f3930a, this.f3931b);
    }
}
