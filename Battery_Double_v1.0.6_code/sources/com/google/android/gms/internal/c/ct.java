package com.google.android.gms.internal.c;

import java.util.concurrent.Callable;

final class ct implements Callable<byte[]> {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ an f3950a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ String f3951b;
    private final /* synthetic */ cg c;

    ct(cg cgVar, an anVar, String str) {
        this.c = cgVar;
        this.f3950a = anVar;
        this.f3951b = str;
    }

    public final /* synthetic */ Object call() throws Exception {
        this.c.f3924a.H();
        return this.c.f3924a.b(this.f3950a, this.f3951b);
    }
}
