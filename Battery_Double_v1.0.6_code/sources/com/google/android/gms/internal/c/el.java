package com.google.android.gms.internal.c;

import android.os.RemoteException;
import android.text.TextUtils;
import java.util.Collections;
import java.util.concurrent.atomic.AtomicReference;

final class el implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ AtomicReference f4031a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ String f4032b;
    private final /* synthetic */ String c;
    private final /* synthetic */ String d;
    private final /* synthetic */ s e;
    private final /* synthetic */ eb f;

    el(eb ebVar, AtomicReference atomicReference, String str, String str2, String str3, s sVar) {
        this.f = ebVar;
        this.f4031a = atomicReference;
        this.f4032b = str;
        this.c = str2;
        this.d = str3;
        this.e = sVar;
    }

    /* JADX INFO: finally extract failed */
    public final void run() {
        synchronized (this.f4031a) {
            try {
                ar d2 = this.f.f4014b;
                if (d2 == null) {
                    this.f.q().v().a("Failed to get conditional properties", az.a(this.f4032b), this.c, this.d);
                    this.f4031a.set(Collections.emptyList());
                    this.f4031a.notify();
                    return;
                }
                if (TextUtils.isEmpty(this.f4032b)) {
                    this.f4031a.set(d2.a(this.c, this.d, this.e));
                } else {
                    this.f4031a.set(d2.a(this.f4032b, this.c, this.d));
                }
                this.f.C();
                this.f4031a.notify();
            } catch (RemoteException e2) {
                this.f.q().v().a("Failed to get conditional properties", az.a(this.f4032b), this.c, e2);
                this.f4031a.set(Collections.emptyList());
                this.f4031a.notify();
            } catch (Throwable th) {
                this.f4031a.notify();
                throw th;
            }
        }
    }
}
