package com.google.android.gms.internal.c;

final class dj implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ long f3976a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ dd f3977b;

    dj(dd ddVar, long j) {
        this.f3977b = ddVar;
        this.f3976a = j;
    }

    public final void run() {
        boolean z = true;
        dd ddVar = this.f3977b;
        long j = this.f3976a;
        ddVar.c();
        ddVar.F();
        ddVar.q().B().a("Resetting analytics data (FE)");
        ddVar.o().v();
        if (ddVar.s().j(ddVar.f().w())) {
            ddVar.r().h.a(j);
        }
        boolean x = ddVar.q.x();
        if (!ddVar.s().u()) {
            ddVar.r().d(!x);
        }
        ddVar.h().x();
        if (x) {
            z = false;
        }
        ddVar.f3966b = z;
    }
}
