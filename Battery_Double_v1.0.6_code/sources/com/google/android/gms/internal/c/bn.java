package com.google.android.gms.internal.c;

import android.content.SharedPreferences.Editor;
import com.google.android.gms.common.internal.aa;

public final class bn {

    /* renamed from: a reason: collision with root package name */
    private final String f3898a;

    /* renamed from: b reason: collision with root package name */
    private final long f3899b;
    private boolean c;
    private long d;
    private final /* synthetic */ bk e;

    public bn(bk bkVar, String str, long j) {
        this.e = bkVar;
        aa.a(str);
        this.f3898a = str;
        this.f3899b = j;
    }

    public final long a() {
        if (!this.c) {
            this.c = true;
            this.d = this.e.C().getLong(this.f3898a, this.f3899b);
        }
        return this.d;
    }

    public final void a(long j) {
        Editor edit = this.e.C().edit();
        edit.putLong(this.f3898a, j);
        edit.apply();
        this.d = j;
    }
}
