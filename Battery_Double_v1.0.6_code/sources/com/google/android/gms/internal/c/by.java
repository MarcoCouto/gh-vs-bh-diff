package com.google.android.gms.internal.c;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteException;
import android.support.v4.h.a;
import android.text.TextUtils;
import com.google.android.gms.common.internal.aa;
import com.google.android.gms.measurement.AppMeasurement;
import com.google.android.gms.measurement.AppMeasurement.d;
import com.google.android.gms.measurement.AppMeasurement.e;
import java.io.IOException;
import java.util.Map;

public final class by extends fj implements aa {

    /* renamed from: b reason: collision with root package name */
    private static int f3913b = 65535;
    private static int c = 2;
    private final Map<String, Map<String, String>> d = new a();
    private final Map<String, Map<String, Boolean>> e = new a();
    private final Map<String, Map<String, Boolean>> f = new a();
    private final Map<String, gd> g = new a();
    private final Map<String, Map<String, Integer>> h = new a();
    private final Map<String, String> i = new a();

    by(fk fkVar) {
        super(fkVar);
    }

    private final gd a(String str, byte[] bArr) {
        if (bArr == null) {
            return new gd();
        }
        a a2 = a.a(bArr, 0, bArr.length);
        gd gdVar = new gd();
        try {
            gdVar.a(a2);
            q().C().a("Parsed config. version, gmp_app_id", gdVar.c, gdVar.d);
            return gdVar;
        } catch (IOException e2) {
            q().y().a("Unable to merge remote config. appId", az.a(str), e2);
            return new gd();
        }
    }

    private static Map<String, String> a(gd gdVar) {
        ge[] geVarArr;
        a aVar = new a();
        if (!(gdVar == null || gdVar.e == null)) {
            for (ge geVar : gdVar.e) {
                if (geVar != null) {
                    aVar.put(geVar.c, geVar.d);
                }
            }
        }
        return aVar;
    }

    private final void a(String str, gd gdVar) {
        gc[] gcVarArr;
        a aVar = new a();
        a aVar2 = new a();
        a aVar3 = new a();
        if (!(gdVar == null || gdVar.f == null)) {
            for (gc gcVar : gdVar.f) {
                if (TextUtils.isEmpty(gcVar.c)) {
                    q().y().a("EventConfig contained null event name");
                } else {
                    String a2 = AppMeasurement.a.a(gcVar.c);
                    if (!TextUtils.isEmpty(a2)) {
                        gcVar.c = a2;
                    }
                    aVar.put(gcVar.c, gcVar.d);
                    aVar2.put(gcVar.c, gcVar.e);
                    if (gcVar.f != null) {
                        if (gcVar.f.intValue() < c || gcVar.f.intValue() > f3913b) {
                            q().y().a("Invalid sampling rate. Event name, sample rate", gcVar.c, gcVar.f);
                        } else {
                            aVar3.put(gcVar.c, gcVar.f);
                        }
                    }
                }
            }
        }
        this.e.put(str, aVar);
        this.f.put(str, aVar2);
        this.h.put(str, aVar3);
    }

    private final void g(String str) {
        N();
        c();
        aa.a(str);
        if (this.g.get(str) == null) {
            byte[] d2 = t_().d(str);
            if (d2 == null) {
                this.d.put(str, null);
                this.e.put(str, null);
                this.f.put(str, null);
                this.g.put(str, null);
                this.i.put(str, null);
                this.h.put(str, null);
                return;
            }
            gd a2 = a(str, d2);
            this.d.put(str, a(a2));
            a(str, a2);
            this.g.put(str, a2);
            this.i.put(str, null);
        }
    }

    /* access modifiers changed from: protected */
    public final gd a(String str) {
        N();
        c();
        aa.a(str);
        g(str);
        return (gd) this.g.get(str);
    }

    public final String a(String str, String str2) {
        c();
        g(str);
        Map map = (Map) this.d.get(str);
        if (map != null) {
            return (String) map.get(str2);
        }
        return null;
    }

    public final /* bridge */ /* synthetic */ void a() {
        super.a();
    }

    /* access modifiers changed from: protected */
    public final boolean a(String str, byte[] bArr, String str2) {
        fx[] fxVarArr;
        ga[] gaVarArr;
        fy[] fyVarArr;
        N();
        c();
        aa.a(str);
        gd a2 = a(str, bArr);
        if (a2 == null) {
            return false;
        }
        a(str, a2);
        this.g.put(str, a2);
        this.i.put(str, str2);
        this.d.put(str, a(a2));
        u u_ = u_();
        fw[] fwVarArr = a2.g;
        aa.a(fwVarArr);
        for (fw fwVar : fwVarArr) {
            for (fx fxVar : fwVar.e) {
                String a3 = AppMeasurement.a.a(fxVar.d);
                if (a3 != null) {
                    fxVar.d = a3;
                }
                for (fy fyVar : fxVar.e) {
                    String a4 = d.a(fyVar.f);
                    if (a4 != null) {
                        fyVar.f = a4;
                    }
                }
            }
            for (ga gaVar : fwVar.d) {
                String a5 = e.a(gaVar.d);
                if (a5 != null) {
                    gaVar.d = a5;
                }
            }
        }
        u_.t_().a(str, fwVarArr);
        try {
            a2.g = null;
            byte[] bArr2 = new byte[a2.d()];
            a2.a(b.a(bArr2, 0, bArr2.length));
            bArr = bArr2;
        } catch (IOException e2) {
            q().y().a("Unable to serialize reduced-size config. Storing full config instead. appId", az.a(str), e2);
        }
        ab t_ = t_();
        aa.a(str);
        t_.c();
        t_.N();
        ContentValues contentValues = new ContentValues();
        contentValues.put("remote_config", bArr);
        try {
            if (((long) t_.x().update("apps", contentValues, "app_id = ?", new String[]{str})) == 0) {
                t_.q().v().a("Failed to update remote config (got 0). appId", az.a(str));
            }
        } catch (SQLiteException e3) {
            t_.q().v().a("Error storing remote config. appId", az.a(str), e3);
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public final String b(String str) {
        c();
        return (String) this.i.get(str);
    }

    public final /* bridge */ /* synthetic */ void b() {
        super.b();
    }

    /* access modifiers changed from: 0000 */
    public final boolean b(String str, String str2) {
        c();
        g(str);
        if (e(str) && ft.h(str2)) {
            return true;
        }
        if (f(str) && ft.a(str2)) {
            return true;
        }
        Map map = (Map) this.e.get(str);
        if (map == null) {
            return false;
        }
        Boolean bool = (Boolean) map.get(str2);
        if (bool == null) {
            return false;
        }
        return bool.booleanValue();
    }

    public final /* bridge */ /* synthetic */ void c() {
        super.c();
    }

    /* access modifiers changed from: protected */
    public final void c(String str) {
        c();
        this.i.put(str, null);
    }

    /* access modifiers changed from: 0000 */
    public final boolean c(String str, String str2) {
        c();
        g(str);
        if ("ecommerce_purchase".equals(str2)) {
            return true;
        }
        Map map = (Map) this.f.get(str);
        if (map == null) {
            return false;
        }
        Boolean bool = (Boolean) map.get(str2);
        if (bool == null) {
            return false;
        }
        return bool.booleanValue();
    }

    /* access modifiers changed from: 0000 */
    public final int d(String str, String str2) {
        c();
        g(str);
        Map map = (Map) this.h.get(str);
        if (map == null) {
            return 1;
        }
        Integer num = (Integer) map.get(str2);
        if (num == null) {
            return 1;
        }
        return num.intValue();
    }

    public final /* bridge */ /* synthetic */ n d() {
        return super.d();
    }

    /* access modifiers changed from: 0000 */
    public final void d(String str) {
        c();
        this.g.remove(str);
    }

    public final /* bridge */ /* synthetic */ dd e() {
        return super.e();
    }

    /* access modifiers changed from: 0000 */
    public final boolean e(String str) {
        return "1".equals(a(str, "measurement.upload.blacklist_internal"));
    }

    public final /* bridge */ /* synthetic */ au f() {
        return super.f();
    }

    /* access modifiers changed from: 0000 */
    public final boolean f(String str) {
        return "1".equals(a(str, "measurement.upload.blacklist_public"));
    }

    public final /* bridge */ /* synthetic */ ah g() {
        return super.g();
    }

    public final /* bridge */ /* synthetic */ eb h() {
        return super.h();
    }

    public final /* bridge */ /* synthetic */ dy i() {
        return super.i();
    }

    public final /* bridge */ /* synthetic */ com.google.android.gms.common.util.e j() {
        return super.j();
    }

    public final /* bridge */ /* synthetic */ Context k() {
        return super.k();
    }

    public final /* bridge */ /* synthetic */ av l() {
        return super.l();
    }

    public final /* bridge */ /* synthetic */ ax m() {
        return super.m();
    }

    public final /* bridge */ /* synthetic */ ft n() {
        return super.n();
    }

    public final /* bridge */ /* synthetic */ fa o() {
        return super.o();
    }

    public final /* bridge */ /* synthetic */ bz p() {
        return super.p();
    }

    public final /* bridge */ /* synthetic */ az q() {
        return super.q();
    }

    public final /* bridge */ /* synthetic */ bk r() {
        return super.r();
    }

    public final /* bridge */ /* synthetic */ y s() {
        return super.s();
    }

    /* access modifiers changed from: protected */
    public final boolean t() {
        return false;
    }

    public final /* bridge */ /* synthetic */ ab t_() {
        return super.t_();
    }

    public final /* bridge */ /* synthetic */ u u_() {
        return super.u_();
    }
}
