package com.google.android.gms.internal.c;

final class es implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ ar f4045a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ ep f4046b;

    es(ep epVar, ar arVar) {
        this.f4046b = epVar;
        this.f4045a = arVar;
    }

    public final void run() {
        synchronized (this.f4046b) {
            this.f4046b.f4040b = false;
            if (!this.f4046b.f4039a.v()) {
                this.f4046b.f4039a.q().B().a("Connected to remote service");
                this.f4046b.f4039a.a(this.f4045a);
            }
        }
    }
}
