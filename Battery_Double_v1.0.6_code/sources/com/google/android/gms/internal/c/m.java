package com.google.android.gms.internal.c;

import java.io.IOException;

public final class m {

    /* renamed from: a reason: collision with root package name */
    public static final int[] f4115a = new int[0];

    /* renamed from: b reason: collision with root package name */
    public static final long[] f4116b = new long[0];
    public static final String[] c = new String[0];
    public static final byte[] d = new byte[0];
    private static final int e = 11;
    private static final int f = 12;
    private static final int g = 16;
    private static final int h = 26;
    private static final float[] i = new float[0];
    private static final double[] j = new double[0];
    private static final boolean[] k = new boolean[0];
    private static final byte[][] l = new byte[0][];

    public static final int a(a aVar, int i2) throws IOException {
        int i3 = 1;
        int i4 = aVar.i();
        aVar.b(i2);
        while (aVar.a() == i2) {
            aVar.b(i2);
            i3++;
        }
        aVar.b(i4, i2);
        return i3;
    }
}
