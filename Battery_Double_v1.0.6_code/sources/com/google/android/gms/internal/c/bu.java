package com.google.android.gms.internal.c;

import android.content.BroadcastReceiver.PendingResult;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.Bundle;
import com.google.android.gms.common.internal.aa;

public final class bu {

    /* renamed from: a reason: collision with root package name */
    private final bx f3908a;

    public bu(bx bxVar) {
        aa.a(bxVar);
        this.f3908a = bxVar;
    }

    public static boolean a(Context context) {
        aa.a(context);
        try {
            PackageManager packageManager = context.getPackageManager();
            if (packageManager == null) {
                return false;
            }
            ActivityInfo receiverInfo = packageManager.getReceiverInfo(new ComponentName(context, "com.google.android.gms.measurement.AppMeasurementReceiver"), 0);
            return receiverInfo != null && receiverInfo.enabled;
        } catch (NameNotFoundException e) {
            return false;
        }
    }

    public final void a(Context context, Intent intent) {
        ce a2 = ce.a(context);
        az q = a2.q();
        if (intent == null) {
            q.y().a("Receiver called with null intent");
            return;
        }
        String action = intent.getAction();
        q.C().a("Local receiver got", action);
        if ("com.google.android.gms.measurement.UPLOAD".equals(action)) {
            Intent className = new Intent().setClassName(context, "com.google.android.gms.measurement.AppMeasurementService");
            className.setAction("com.google.android.gms.measurement.UPLOAD");
            q.C().a("Starting wakeful intent.");
            this.f3908a.a(context, className);
        } else if ("com.android.vending.INSTALL_REFERRER".equals(action)) {
            try {
                a2.p().a((Runnable) new bv(this, a2, q));
            } catch (Exception e) {
                q.y().a("Install Referrer Reporter encountered a problem", e);
            }
            PendingResult a3 = this.f3908a.a();
            String stringExtra = intent.getStringExtra("referrer");
            if (stringExtra == null) {
                q.C().a("Install referrer extras are null");
                if (a3 != null) {
                    a3.finish();
                    return;
                }
                return;
            }
            q.A().a("Install referrer extras are", stringExtra);
            if (!stringExtra.contains("?")) {
                String str = "?";
                String valueOf = String.valueOf(stringExtra);
                stringExtra = valueOf.length() != 0 ? str.concat(valueOf) : new String(str);
            }
            Bundle a4 = a2.m().a(Uri.parse(stringExtra));
            if (a4 == null) {
                q.C().a("No campaign defined in install referrer broadcast");
                if (a3 != null) {
                    a3.finish();
                    return;
                }
                return;
            }
            long longExtra = 1000 * intent.getLongExtra("referrer_timestamp_seconds", 0);
            if (longExtra == 0) {
                q.y().a("Install referrer is missing timestamp");
            }
            a2.p().a((Runnable) new bw(this, a2, longExtra, a4, context, q, a3));
        }
    }
}
