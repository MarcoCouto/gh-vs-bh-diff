package com.google.android.gms.internal.c;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.Parcelable;
import android.support.v4.h.a;
import android.text.TextUtils;
import android.util.Pair;
import com.google.android.gms.common.internal.aa;
import com.hmatalonga.greenhub.Config;
import java.io.File;
import java.io.IOException;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

final class ab extends fj {
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public static final String[] f3847b = {"last_bundled_timestamp", "ALTER TABLE events ADD COLUMN last_bundled_timestamp INTEGER;", "last_sampled_complex_event_id", "ALTER TABLE events ADD COLUMN last_sampled_complex_event_id INTEGER;", "last_sampling_rate", "ALTER TABLE events ADD COLUMN last_sampling_rate INTEGER;", "last_exempt_from_sampling", "ALTER TABLE events ADD COLUMN last_exempt_from_sampling INTEGER;"};
    /* access modifiers changed from: private */
    public static final String[] c = {"origin", "ALTER TABLE user_attributes ADD COLUMN origin TEXT;"};
    /* access modifiers changed from: private */
    public static final String[] d = {"app_version", "ALTER TABLE apps ADD COLUMN app_version TEXT;", "app_store", "ALTER TABLE apps ADD COLUMN app_store TEXT;", "gmp_version", "ALTER TABLE apps ADD COLUMN gmp_version INTEGER;", "dev_cert_hash", "ALTER TABLE apps ADD COLUMN dev_cert_hash INTEGER;", "measurement_enabled", "ALTER TABLE apps ADD COLUMN measurement_enabled INTEGER;", "last_bundle_start_timestamp", "ALTER TABLE apps ADD COLUMN last_bundle_start_timestamp INTEGER;", "day", "ALTER TABLE apps ADD COLUMN day INTEGER;", "daily_public_events_count", "ALTER TABLE apps ADD COLUMN daily_public_events_count INTEGER;", "daily_events_count", "ALTER TABLE apps ADD COLUMN daily_events_count INTEGER;", "daily_conversions_count", "ALTER TABLE apps ADD COLUMN daily_conversions_count INTEGER;", "remote_config", "ALTER TABLE apps ADD COLUMN remote_config BLOB;", "config_fetched_time", "ALTER TABLE apps ADD COLUMN config_fetched_time INTEGER;", "failed_config_fetch_time", "ALTER TABLE apps ADD COLUMN failed_config_fetch_time INTEGER;", "app_version_int", "ALTER TABLE apps ADD COLUMN app_version_int INTEGER;", "firebase_instance_id", "ALTER TABLE apps ADD COLUMN firebase_instance_id TEXT;", "daily_error_events_count", "ALTER TABLE apps ADD COLUMN daily_error_events_count INTEGER;", "daily_realtime_events_count", "ALTER TABLE apps ADD COLUMN daily_realtime_events_count INTEGER;", "health_monitor_sample", "ALTER TABLE apps ADD COLUMN health_monitor_sample TEXT;", "android_id", "ALTER TABLE apps ADD COLUMN android_id INTEGER;", "adid_reporting_enabled", "ALTER TABLE apps ADD COLUMN adid_reporting_enabled INTEGER;", "ssaid_reporting_enabled", "ALTER TABLE apps ADD COLUMN ssaid_reporting_enabled INTEGER;"};
    /* access modifiers changed from: private */
    public static final String[] e = {"realtime", "ALTER TABLE raw_events ADD COLUMN realtime INTEGER;"};
    /* access modifiers changed from: private */
    public static final String[] f = {"has_realtime", "ALTER TABLE queue ADD COLUMN has_realtime INTEGER;", "retry_count", "ALTER TABLE queue ADD COLUMN retry_count INTEGER;"};
    /* access modifiers changed from: private */
    public static final String[] g = {"previous_install_count", "ALTER TABLE app2 ADD COLUMN previous_install_count INTEGER;"};
    private final ae h = new ae(this, k(), "google_app_measurement.db");
    /* access modifiers changed from: private */
    public final ff i = new ff(j());

    ab(fk fkVar) {
        super(fkVar);
    }

    private final boolean P() {
        return k().getDatabasePath("google_app_measurement.db").exists();
    }

    private final long a(String str, String[] strArr, long j) {
        Cursor cursor = null;
        try {
            Cursor rawQuery = x().rawQuery(str, strArr);
            if (rawQuery.moveToFirst()) {
                j = rawQuery.getLong(0);
                if (rawQuery != null) {
                    rawQuery.close();
                }
            } else if (rawQuery != null) {
                rawQuery.close();
            }
            return j;
        } catch (SQLiteException e2) {
            q().v().a("Database error", str, e2);
            throw e2;
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    private final Object a(Cursor cursor, int i2) {
        int type = cursor.getType(i2);
        switch (type) {
            case 0:
                q().v().a("Loaded invalid null value from database");
                return null;
            case 1:
                return Long.valueOf(cursor.getLong(i2));
            case 2:
                return Double.valueOf(cursor.getDouble(i2));
            case 3:
                return cursor.getString(i2);
            case 4:
                q().v().a("Loaded invalid blob type value, ignoring it");
                return null;
            default:
                q().v().a("Loaded invalid unknown value type, ignoring it", Integer.valueOf(type));
                return null;
        }
    }

    private static Set<String> a(SQLiteDatabase sQLiteDatabase, String str) {
        HashSet hashSet = new HashSet();
        Cursor rawQuery = sQLiteDatabase.rawQuery(new StringBuilder(String.valueOf(str).length() + 22).append("SELECT * FROM ").append(str).append(" LIMIT 0").toString(), null);
        try {
            Collections.addAll(hashSet, rawQuery.getColumnNames());
            return hashSet;
        } finally {
            rawQuery.close();
        }
    }

    private static void a(ContentValues contentValues, String str, Object obj) {
        aa.a(str);
        aa.a(obj);
        if (obj instanceof String) {
            contentValues.put(str, (String) obj);
        } else if (obj instanceof Long) {
            contentValues.put(str, (Long) obj);
        } else if (obj instanceof Double) {
            contentValues.put(str, (Double) obj);
        } else {
            throw new IllegalArgumentException("Invalid value type");
        }
    }

    static void a(az azVar, SQLiteDatabase sQLiteDatabase) {
        if (azVar == null) {
            throw new IllegalArgumentException("Monitor must not be null");
        }
        File file = new File(sQLiteDatabase.getPath());
        if (!file.setReadable(false, false)) {
            azVar.y().a("Failed to turn off database read permission");
        }
        if (!file.setWritable(false, false)) {
            azVar.y().a("Failed to turn off database write permission");
        }
        if (!file.setReadable(true, true)) {
            azVar.y().a("Failed to turn on database read permission for owner");
        }
        if (!file.setWritable(true, true)) {
            azVar.y().a("Failed to turn on database write permission for owner");
        }
    }

    static void a(az azVar, SQLiteDatabase sQLiteDatabase, String str, String str2, String str3, String[] strArr) throws SQLiteException {
        String[] split;
        if (azVar == null) {
            throw new IllegalArgumentException("Monitor must not be null");
        }
        if (!a(azVar, sQLiteDatabase, str)) {
            sQLiteDatabase.execSQL(str2);
        }
        if (azVar == null) {
            try {
                throw new IllegalArgumentException("Monitor must not be null");
            } catch (SQLiteException e2) {
                azVar.v().a("Failed to verify columns on table that was just created", str);
                throw e2;
            }
        } else {
            Set a2 = a(sQLiteDatabase, str);
            for (String str4 : str3.split(",")) {
                if (!a2.remove(str4)) {
                    throw new SQLiteException(new StringBuilder(String.valueOf(str).length() + 35 + String.valueOf(str4).length()).append("Table ").append(str).append(" is missing required column: ").append(str4).toString());
                }
            }
            if (strArr != null) {
                for (int i2 = 0; i2 < strArr.length; i2 += 2) {
                    if (!a2.remove(strArr[i2])) {
                        sQLiteDatabase.execSQL(strArr[i2 + 1]);
                    }
                }
            }
            if (!a2.isEmpty()) {
                azVar.y().a("Table has extra columns. table, columns", str, TextUtils.join(", ", a2));
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x0045  */
    private static boolean a(az azVar, SQLiteDatabase sQLiteDatabase, String str) {
        Cursor cursor;
        Cursor cursor2 = null;
        if (azVar == null) {
            throw new IllegalArgumentException("Monitor must not be null");
        }
        try {
            SQLiteDatabase sQLiteDatabase2 = sQLiteDatabase;
            cursor = sQLiteDatabase2.query("SQLITE_MASTER", new String[]{"name"}, "name=?", new String[]{str}, null, null, null);
            try {
                boolean moveToFirst = cursor.moveToFirst();
                if (cursor == null) {
                    return moveToFirst;
                }
                cursor.close();
                return moveToFirst;
            } catch (SQLiteException e2) {
                e = e2;
            }
        } catch (SQLiteException e3) {
            e = e3;
            cursor = null;
        } catch (Throwable th) {
            th = th;
            if (cursor2 != null) {
            }
            throw th;
        }
        try {
            azVar.y().a("Error querying for table", str, e);
            if (cursor != null) {
                cursor.close();
            }
            return false;
        } catch (Throwable th2) {
            th = th2;
            cursor2 = cursor;
            if (cursor2 != null) {
                cursor2.close();
            }
            throw th;
        }
    }

    private final boolean a(String str, int i2, fx fxVar) {
        N();
        c();
        aa.a(str);
        aa.a(fxVar);
        if (TextUtils.isEmpty(fxVar.d)) {
            q().y().a("Event filter had no event name. Audience definition ignored. appId, audienceId, filterId", az.a(str), Integer.valueOf(i2), String.valueOf(fxVar.c));
            return false;
        }
        try {
            byte[] bArr = new byte[fxVar.d()];
            b a2 = b.a(bArr, 0, bArr.length);
            fxVar.a(a2);
            a2.a();
            ContentValues contentValues = new ContentValues();
            contentValues.put("app_id", str);
            contentValues.put("audience_id", Integer.valueOf(i2));
            contentValues.put("filter_id", fxVar.c);
            contentValues.put("event_name", fxVar.d);
            contentValues.put("data", bArr);
            try {
                if (x().insertWithOnConflict("event_filters", null, contentValues, 5) == -1) {
                    q().v().a("Failed to insert event filter (got -1). appId", az.a(str));
                }
                return true;
            } catch (SQLiteException e2) {
                q().v().a("Error storing event filter. appId", az.a(str), e2);
                return false;
            }
        } catch (IOException e3) {
            q().v().a("Configuration loss. Failed to serialize event filter. appId", az.a(str), e3);
            return false;
        }
    }

    private final boolean a(String str, int i2, ga gaVar) {
        N();
        c();
        aa.a(str);
        aa.a(gaVar);
        if (TextUtils.isEmpty(gaVar.d)) {
            q().y().a("Property filter had no property name. Audience definition ignored. appId, audienceId, filterId", az.a(str), Integer.valueOf(i2), String.valueOf(gaVar.c));
            return false;
        }
        try {
            byte[] bArr = new byte[gaVar.d()];
            b a2 = b.a(bArr, 0, bArr.length);
            gaVar.a(a2);
            a2.a();
            ContentValues contentValues = new ContentValues();
            contentValues.put("app_id", str);
            contentValues.put("audience_id", Integer.valueOf(i2));
            contentValues.put("filter_id", gaVar.c);
            contentValues.put("property_name", gaVar.d);
            contentValues.put("data", bArr);
            try {
                if (x().insertWithOnConflict("property_filters", null, contentValues, 5) != -1) {
                    return true;
                }
                q().v().a("Failed to insert property filter (got -1). appId", az.a(str));
                return false;
            } catch (SQLiteException e2) {
                q().v().a("Error storing property filter. appId", az.a(str), e2);
                return false;
            }
        } catch (IOException e3) {
            q().v().a("Configuration loss. Failed to serialize property filter. appId", az.a(str), e3);
            return false;
        }
    }

    private final boolean a(String str, List<Integer> list) {
        aa.a(str);
        N();
        c();
        SQLiteDatabase x = x();
        try {
            long b2 = b("select count(1) from audience_filter_values where app_id=?", new String[]{str});
            int max = Math.max(0, Math.min(Config.STARTUP_CURRENT_INTERVAL, s().b(str, ap.G)));
            if (b2 <= ((long) max)) {
                return false;
            }
            ArrayList arrayList = new ArrayList();
            for (int i2 = 0; i2 < list.size(); i2++) {
                Integer num = (Integer) list.get(i2);
                if (num == null || !(num instanceof Integer)) {
                    return false;
                }
                arrayList.add(Integer.toString(num.intValue()));
            }
            String join = TextUtils.join(",", arrayList);
            String sb = new StringBuilder(String.valueOf(join).length() + 2).append("(").append(join).append(")").toString();
            return x.delete("audience_filter_values", new StringBuilder(String.valueOf(sb).length() + 140).append("audience_id in (select audience_id from audience_filter_values where app_id=? and audience_id not in ").append(sb).append(" order by rowid desc limit -1 offset ?)").toString(), new String[]{str, Integer.toString(max)}) > 0;
        } catch (SQLiteException e2) {
            q().v().a("Database error querying filters. appId", az.a(str), e2);
            return false;
        }
    }

    private final long b(String str, String[] strArr) {
        Cursor cursor = null;
        try {
            cursor = x().rawQuery(str, strArr);
            if (cursor.moveToFirst()) {
                long j = cursor.getLong(0);
                if (cursor != null) {
                    cursor.close();
                }
                return j;
            }
            throw new SQLiteException("Database returned empty set");
        } catch (SQLiteException e2) {
            q().v().a("Database error", str, e2);
            throw e2;
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    /* access modifiers changed from: 0000 */
    public final void A() {
        c();
        N();
        if (P()) {
            long a2 = r().f.a();
            long b2 = j().b();
            if (Math.abs(b2 - a2) > ((Long) ap.z.b()).longValue()) {
                r().f.a(b2);
                c();
                N();
                if (P()) {
                    int delete = x().delete("queue", "abs(bundle_end_timestamp - ?) > cast(? as integer)", new String[]{String.valueOf(j().a()), String.valueOf(y.v())});
                    if (delete > 0) {
                        q().C().a("Deleted stale rows. rowsDeleted", Integer.valueOf(delete));
                    }
                }
            }
        }
    }

    public final long B() {
        return a("select max(bundle_end_timestamp) from queue", (String[]) null, 0);
    }

    public final long C() {
        return a("select max(timestamp) from raw_events", (String[]) null, 0);
    }

    public final boolean D() {
        return b("select count(1) > 0 from raw_events", (String[]) null) != 0;
    }

    public final boolean E() {
        return b("select count(1) > 0 from raw_events where realtime = 1", (String[]) null) != 0;
    }

    public final long F() {
        long j = -1;
        Cursor cursor = null;
        try {
            cursor = x().rawQuery("select rowid from raw_events order by rowid desc limit 1;", null);
            if (cursor.moveToFirst()) {
                j = cursor.getLong(0);
                if (cursor != null) {
                    cursor.close();
                }
            } else if (cursor != null) {
                cursor.close();
            }
        } catch (SQLiteException e2) {
            q().v().a("Error querying raw events", e2);
            if (cursor != null) {
                cursor.close();
            }
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
        return j;
    }

    public final long a(gj gjVar) throws IOException {
        long c2;
        c();
        N();
        aa.a(gjVar);
        aa.a(gjVar.q);
        try {
            byte[] bArr = new byte[gjVar.d()];
            b a2 = b.a(bArr, 0, bArr.length);
            gjVar.a(a2);
            a2.a();
            ft n = n();
            aa.a(bArr);
            n.c();
            MessageDigest f2 = ft.f("MD5");
            if (f2 == null) {
                n.q().v().a("Failed to get MD5");
                c2 = 0;
            } else {
                c2 = ft.c(f2.digest(bArr));
            }
            ContentValues contentValues = new ContentValues();
            contentValues.put("app_id", gjVar.q);
            contentValues.put("metadata_fingerprint", Long.valueOf(c2));
            contentValues.put("metadata", bArr);
            try {
                x().insertWithOnConflict("raw_events_metadata", null, contentValues, 4);
                return c2;
            } catch (SQLiteException e2) {
                q().v().a("Error storing raw event metadata. appId", az.a(gjVar.q), e2);
                throw e2;
            }
        } catch (IOException e3) {
            q().v().a("Data loss. Failed to serialize event metadata. appId", az.a(gjVar.q), e3);
            throw e3;
        }
    }

    /* JADX WARNING: type inference failed for: r0v0 */
    /* JADX WARNING: type inference failed for: r2v0, types: [android.database.Cursor] */
    /* JADX WARNING: type inference failed for: r2v1 */
    /* JADX WARNING: type inference failed for: r2v2, types: [android.database.Cursor] */
    /* JADX WARNING: type inference failed for: r2v3 */
    /* JADX WARNING: type inference failed for: r0v5, types: [android.util.Pair<com.google.android.gms.internal.c.gg, java.lang.Long>] */
    /* JADX WARNING: type inference failed for: r0v7, types: [android.util.Pair] */
    /* JADX WARNING: type inference failed for: r2v6 */
    /* JADX WARNING: type inference failed for: r2v7 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x008f  */
    /* JADX WARNING: Unknown variable types count: 3 */
    public final Pair<gg, Long> a(String str, Long l) {
        ? r2;
        Throwable th;
        ? r22;
        ? r0 = 0;
        c();
        N();
        try {
            Cursor rawQuery = x().rawQuery("select main_event, children_to_process from main_event_params where app_id=? and event_id=?", new String[]{str, String.valueOf(l)});
            try {
                if (!rawQuery.moveToFirst()) {
                    q().C().a("Main event not found");
                    if (rawQuery != 0) {
                        rawQuery.close();
                    }
                } else {
                    byte[] blob = rawQuery.getBlob(0);
                    Long valueOf = Long.valueOf(rawQuery.getLong(1));
                    a a2 = a.a(blob, 0, blob.length);
                    gg ggVar = new gg();
                    try {
                        ggVar.a(a2);
                        r0 = Pair.create(ggVar, valueOf);
                        if (rawQuery != 0) {
                            rawQuery.close();
                        }
                    } catch (IOException e2) {
                        q().v().a("Failed to merge main event. appId, eventId", az.a(str), l, e2);
                        if (rawQuery != 0) {
                            rawQuery.close();
                        }
                    }
                }
            } catch (SQLiteException e3) {
                e = e3;
                r22 = rawQuery;
            }
        } catch (SQLiteException e4) {
            e = e4;
            r22 = r0;
        } catch (Throwable th2) {
            r2 = r0;
            th = th2;
            if (r2 != 0) {
            }
            throw th;
        }
        return r0;
        try {
            q().v().a("Error selecting main event", e);
            if (r22 != 0) {
                r22.close();
            }
            return r0;
        } catch (Throwable th3) {
            th = th3;
            r2 = r22;
            if (r2 != 0) {
                r2.close();
            }
            throw th;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:39:0x0135  */
    public final ac a(long j, String str, boolean z, boolean z2, boolean z3, boolean z4, boolean z5) {
        Cursor cursor;
        aa.a(str);
        c();
        N();
        String[] strArr = {str};
        ac acVar = new ac();
        try {
            SQLiteDatabase x = x();
            cursor = x.query("apps", new String[]{"day", "daily_events_count", "daily_public_events_count", "daily_conversions_count", "daily_error_events_count", "daily_realtime_events_count"}, "app_id=?", new String[]{str}, null, null, null);
            try {
                if (!cursor.moveToFirst()) {
                    q().y().a("Not updating daily counts, app is not known. appId", az.a(str));
                    if (cursor != null) {
                        cursor.close();
                    }
                    return acVar;
                }
                if (cursor.getLong(0) == j) {
                    acVar.f3849b = cursor.getLong(1);
                    acVar.f3848a = cursor.getLong(2);
                    acVar.c = cursor.getLong(3);
                    acVar.d = cursor.getLong(4);
                    acVar.e = cursor.getLong(5);
                }
                if (z) {
                    acVar.f3849b++;
                }
                if (z2) {
                    acVar.f3848a++;
                }
                if (z3) {
                    acVar.c++;
                }
                if (z4) {
                    acVar.d++;
                }
                if (z5) {
                    acVar.e++;
                }
                ContentValues contentValues = new ContentValues();
                contentValues.put("day", Long.valueOf(j));
                contentValues.put("daily_public_events_count", Long.valueOf(acVar.f3848a));
                contentValues.put("daily_events_count", Long.valueOf(acVar.f3849b));
                contentValues.put("daily_conversions_count", Long.valueOf(acVar.c));
                contentValues.put("daily_error_events_count", Long.valueOf(acVar.d));
                contentValues.put("daily_realtime_events_count", Long.valueOf(acVar.e));
                x.update("apps", contentValues, "app_id=?", strArr);
                if (cursor != null) {
                    cursor.close();
                }
                return acVar;
            } catch (SQLiteException e2) {
                e = e2;
                try {
                    q().v().a("Error updating daily counts. appId", az.a(str), e);
                    if (cursor != null) {
                        cursor.close();
                    }
                    return acVar;
                } catch (Throwable th) {
                    th = th;
                    if (cursor != null) {
                    }
                    throw th;
                }
            }
        } catch (SQLiteException e3) {
            e = e3;
            cursor = null;
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:47:0x011b  */
    public final aj a(String str, String str2) {
        Cursor cursor;
        Cursor cursor2;
        aa.a(str);
        aa.a(str2);
        c();
        N();
        try {
            cursor = x().query("events", new String[]{"lifetime_count", "current_bundle_count", "last_fire_timestamp", "last_bundled_timestamp", "last_sampled_complex_event_id", "last_sampling_rate", "last_exempt_from_sampling"}, "app_id=? and name=?", new String[]{str, str2}, null, null, null);
            try {
                if (!cursor.moveToFirst()) {
                    if (cursor != null) {
                        cursor.close();
                    }
                    return null;
                }
                long j = cursor.getLong(0);
                long j2 = cursor.getLong(1);
                long j3 = cursor.getLong(2);
                long j4 = cursor.isNull(3) ? 0 : cursor.getLong(3);
                Long valueOf = cursor.isNull(4) ? null : Long.valueOf(cursor.getLong(4));
                Long valueOf2 = cursor.isNull(5) ? null : Long.valueOf(cursor.getLong(5));
                Boolean bool = null;
                if (!cursor.isNull(6)) {
                    bool = Boolean.valueOf(cursor.getLong(6) == 1);
                }
                aj ajVar = new aj(str, str2, j, j2, j3, j4, valueOf, valueOf2, bool);
                if (cursor.moveToNext()) {
                    q().v().a("Got multiple records for event aggregates, expected one. appId", az.a(str));
                }
                if (cursor == null) {
                    return ajVar;
                }
                cursor.close();
                return ajVar;
            } catch (SQLiteException e2) {
                e = e2;
                cursor2 = cursor;
            } catch (Throwable th) {
                th = th;
                if (cursor != null) {
                }
                throw th;
            }
        } catch (SQLiteException e3) {
            e = e3;
            cursor2 = null;
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
            if (cursor != null) {
            }
            throw th;
        }
        try {
            q().v().a("Error querying events. appId", az.a(str), m().a(str2), e);
            if (cursor2 != null) {
                cursor2.close();
            }
            return null;
        } catch (Throwable th3) {
            th = th3;
            cursor = cursor2;
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    /* JADX WARNING: type inference failed for: r2v0, types: [android.database.Cursor] */
    /* JADX WARNING: type inference failed for: r2v1 */
    /* JADX WARNING: type inference failed for: r2v2, types: [android.database.Cursor] */
    /* JADX WARNING: type inference failed for: r2v3 */
    /* JADX WARNING: type inference failed for: r2v6 */
    /* JADX WARNING: type inference failed for: r2v7 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0059  */
    /* JADX WARNING: Unknown variable types count: 2 */
    public final String a(long j) {
        ? r2;
        Throwable th;
        ? r22;
        String str = 0;
        c();
        N();
        try {
            Cursor rawQuery = x().rawQuery("select app_id from apps where app_id in (select distinct app_id from raw_events) and config_fetched_time < ? order by failed_config_fetch_time limit 1;", new String[]{String.valueOf(j)});
            try {
                if (!rawQuery.moveToFirst()) {
                    q().C().a("No expired configs for apps with pending events");
                    if (rawQuery != 0) {
                        rawQuery.close();
                    }
                } else {
                    str = rawQuery.getString(0);
                    if (rawQuery != 0) {
                        rawQuery.close();
                    }
                }
            } catch (SQLiteException e2) {
                e = e2;
                r22 = rawQuery;
            }
        } catch (SQLiteException e3) {
            e = e3;
            r22 = str;
        } catch (Throwable th2) {
            r2 = str;
            th = th2;
            if (r2 != 0) {
            }
            throw th;
        }
        return str;
        try {
            q().v().a("Error selecting expired configs", e);
            if (r22 != 0) {
                r22.close();
            }
            return str;
        } catch (Throwable th3) {
            th = th3;
            r2 = r22;
            if (r2 != 0) {
                r2.close();
            }
            throw th;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:33:0x00aa  */
    public final List<fs> a(String str) {
        Cursor cursor;
        Cursor cursor2 = null;
        aa.a(str);
        c();
        N();
        ArrayList arrayList = new ArrayList();
        try {
            Cursor query = x().query("user_attributes", new String[]{"name", "origin", "set_timestamp", "value"}, "app_id=?", new String[]{str}, null, null, "rowid", "1000");
            try {
                if (!query.moveToFirst()) {
                    if (query != null) {
                        query.close();
                    }
                    return arrayList;
                }
                do {
                    String string = query.getString(0);
                    String string2 = query.getString(1);
                    if (string2 == null) {
                        string2 = "";
                    }
                    long j = query.getLong(2);
                    Object a2 = a(query, 3);
                    if (a2 == null) {
                        q().v().a("Read invalid user property value, ignoring it. appId", az.a(str));
                    } else {
                        arrayList.add(new fs(str, string2, string, j, a2));
                    }
                } while (query.moveToNext());
                if (query != null) {
                    query.close();
                }
                return arrayList;
            } catch (SQLiteException e2) {
                e = e2;
                cursor = query;
            } catch (Throwable th) {
                th = th;
                cursor2 = query;
                if (cursor2 != null) {
                }
                throw th;
            }
        } catch (SQLiteException e3) {
            e = e3;
            cursor = null;
        } catch (Throwable th2) {
            th = th2;
            if (cursor2 != null) {
            }
            throw th;
        }
        try {
            q().v().a("Error querying user properties. appId", az.a(str), e);
            if (cursor != null) {
                cursor.close();
            }
            return null;
        } catch (Throwable th3) {
            th = th3;
            cursor2 = cursor;
            if (cursor2 != null) {
                cursor2.close();
            }
            throw th;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:57:0x00fe  */
    public final List<Pair<gj, Long>> a(String str, int i2, int i3) {
        Cursor cursor;
        Cursor cursor2;
        List<Pair<gj, Long>> emptyList;
        int i4;
        boolean z = true;
        c();
        N();
        aa.b(i2 > 0);
        if (i3 <= 0) {
            z = false;
        }
        aa.b(z);
        aa.a(str);
        try {
            cursor = x().query("queue", new String[]{"rowid", "data", "retry_count"}, "app_id=?", new String[]{str}, null, null, "rowid", String.valueOf(i2));
            try {
                if (!cursor.moveToFirst()) {
                    emptyList = Collections.emptyList();
                    if (cursor != null) {
                        cursor.close();
                    }
                } else {
                    emptyList = new ArrayList<>();
                    int i5 = 0;
                    while (true) {
                        long j = cursor.getLong(0);
                        try {
                            byte[] b2 = n().b(cursor.getBlob(1));
                            if (!emptyList.isEmpty() && b2.length + i5 > i3) {
                                break;
                            }
                            a a2 = a.a(b2, 0, b2.length);
                            gj gjVar = new gj();
                            try {
                                gjVar.a(a2);
                                if (!cursor.isNull(2)) {
                                    gjVar.J = Integer.valueOf(cursor.getInt(2));
                                }
                                i4 = b2.length + i5;
                                emptyList.add(Pair.create(gjVar, Long.valueOf(j)));
                            } catch (IOException e2) {
                                q().v().a("Failed to merge queued bundle. appId", az.a(str), e2);
                                i4 = i5;
                            }
                            if (!cursor.moveToNext() || i4 > i3) {
                                break;
                            }
                            i5 = i4;
                        } catch (IOException e3) {
                            q().v().a("Failed to unzip queued bundle. appId", az.a(str), e3);
                            i4 = i5;
                        }
                    }
                    if (cursor != null) {
                        cursor.close();
                    }
                }
            } catch (SQLiteException e4) {
                e = e4;
                cursor2 = cursor;
                try {
                    q().v().a("Error querying bundles. appId", az.a(str), e);
                    emptyList = Collections.emptyList();
                    if (cursor2 != null) {
                        cursor2.close();
                    }
                    return emptyList;
                } catch (Throwable th) {
                    th = th;
                    cursor = cursor2;
                }
            } catch (Throwable th2) {
                th = th2;
                if (cursor != null) {
                }
                throw th;
            }
        } catch (SQLiteException e5) {
            e = e5;
            cursor2 = null;
        } catch (Throwable th3) {
            th = th3;
            cursor = null;
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
        return emptyList;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0102, code lost:
        r10.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0106, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x0107, code lost:
        r10 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x010f, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x0110, code lost:
        r1 = r7;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0102  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0106 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:9:0x007c] */
    public final List<fs> a(String str, String str2, String str3) {
        Cursor cursor;
        String string;
        Cursor cursor2 = null;
        aa.a(str);
        c();
        N();
        ArrayList arrayList = new ArrayList();
        try {
            ArrayList arrayList2 = new ArrayList(3);
            arrayList2.add(str);
            StringBuilder sb = new StringBuilder("app_id=?");
            if (!TextUtils.isEmpty(str2)) {
                arrayList2.add(str2);
                sb.append(" and origin=?");
            }
            if (!TextUtils.isEmpty(str3)) {
                arrayList2.add(String.valueOf(str3).concat("*"));
                sb.append(" and name glob ?");
            }
            String[] strArr = {"name", "set_timestamp", "value", "origin"};
            Cursor query = x().query("user_attributes", strArr, sb.toString(), (String[]) arrayList2.toArray(new String[arrayList2.size()]), null, null, "rowid", "1001");
            try {
                if (!query.moveToFirst()) {
                    if (query != null) {
                        query.close();
                    }
                    return arrayList;
                }
                while (true) {
                    if (arrayList.size() >= 1000) {
                        q().v().a("Read more than the max allowed user properties, ignoring excess", Integer.valueOf(1000));
                        break;
                    }
                    String string2 = query.getString(0);
                    long j = query.getLong(1);
                    Object a2 = a(query, 2);
                    string = query.getString(3);
                    if (a2 == null) {
                        q().v().a("(2)Read invalid user property value, ignoring it", az.a(str), string, str3);
                    } else {
                        arrayList.add(new fs(str, string, string2, j, a2));
                    }
                    if (!query.moveToNext()) {
                        break;
                    }
                    str2 = string;
                }
                if (query != null) {
                    query.close();
                }
                return arrayList;
            } catch (SQLiteException e2) {
                e = e2;
                cursor = query;
                str2 = string;
            } catch (Throwable th) {
            }
        } catch (SQLiteException e3) {
            e = e3;
            cursor = null;
            try {
                q().v().a("(2)Error querying user properties", az.a(str), str2, e);
                if (cursor != null) {
                    cursor.close();
                }
                return null;
            } catch (Throwable th2) {
                th = th2;
                cursor2 = cursor;
                if (cursor2 != null) {
                }
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            if (cursor2 != null) {
            }
            throw th;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:33:0x0165  */
    public final List<w> a(String str, String[] strArr) {
        Cursor cursor;
        Cursor cursor2;
        c();
        N();
        ArrayList arrayList = new ArrayList();
        try {
            cursor = x().query("conditional_properties", new String[]{"app_id", "origin", "name", "value", "active", "trigger_event_name", "trigger_timeout", "timed_out_event", "creation_timestamp", "triggered_event", "triggered_timestamp", "time_to_live", "expired_event"}, str, strArr, null, null, "rowid", "1001");
            try {
                if (!cursor.moveToFirst()) {
                    if (cursor != null) {
                        cursor.close();
                    }
                    return arrayList;
                }
                while (true) {
                    if (arrayList.size() < 1000) {
                        String string = cursor.getString(0);
                        String string2 = cursor.getString(1);
                        String string3 = cursor.getString(2);
                        Object a2 = a(cursor, 3);
                        boolean z = cursor.getInt(4) != 0;
                        String string4 = cursor.getString(5);
                        long j = cursor.getLong(6);
                        an anVar = (an) n().a(cursor.getBlob(7), an.CREATOR);
                        long j2 = cursor.getLong(8);
                        an anVar2 = (an) n().a(cursor.getBlob(9), an.CREATOR);
                        long j3 = cursor.getLong(10);
                        ArrayList arrayList2 = arrayList;
                        arrayList2.add(new w(string, string2, new fq(string3, j3, a2, string2), j2, z, string4, anVar, j, anVar2, cursor.getLong(11), (an) n().a(cursor.getBlob(12), an.CREATOR)));
                        if (!cursor.moveToNext()) {
                            break;
                        }
                    } else {
                        q().v().a("Read more than the max allowed conditional properties, ignoring extra", Integer.valueOf(1000));
                        break;
                    }
                }
                if (cursor != null) {
                    cursor.close();
                }
                return arrayList;
            } catch (SQLiteException e2) {
                e = e2;
                cursor2 = cursor;
            } catch (Throwable th) {
                th = th;
                if (cursor != null) {
                }
                throw th;
            }
        } catch (SQLiteException e3) {
            e = e3;
            cursor2 = null;
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
        try {
            q().v().a("Error querying conditional user property value", e);
            List<w> emptyList = Collections.emptyList();
            if (cursor2 == null) {
                return emptyList;
            }
            cursor2.close();
            return emptyList;
        } catch (Throwable th3) {
            th = th3;
            cursor = cursor2;
        }
    }

    public final void a(aj ajVar) {
        Long l = null;
        aa.a(ajVar);
        c();
        N();
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", ajVar.f3859a);
        contentValues.put("name", ajVar.f3860b);
        contentValues.put("lifetime_count", Long.valueOf(ajVar.c));
        contentValues.put("current_bundle_count", Long.valueOf(ajVar.d));
        contentValues.put("last_fire_timestamp", Long.valueOf(ajVar.e));
        contentValues.put("last_bundled_timestamp", Long.valueOf(ajVar.f));
        contentValues.put("last_sampled_complex_event_id", ajVar.g);
        contentValues.put("last_sampling_rate", ajVar.h);
        if (ajVar.i != null && ajVar.i.booleanValue()) {
            l = Long.valueOf(1);
        }
        contentValues.put("last_exempt_from_sampling", l);
        try {
            if (x().insertWithOnConflict("events", null, contentValues, 5) == -1) {
                q().v().a("Failed to insert/update event aggregates (got -1). appId", az.a(ajVar.f3859a));
            }
        } catch (SQLiteException e2) {
            q().v().a("Error storing event aggregates. appId", az.a(ajVar.f3859a), e2);
        }
    }

    public final void a(r rVar) {
        aa.a(rVar);
        c();
        N();
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", rVar.b());
        contentValues.put("app_instance_id", rVar.c());
        contentValues.put("gmp_app_id", rVar.d());
        contentValues.put("resettable_device_id_hash", rVar.e());
        contentValues.put("last_bundle_index", Long.valueOf(rVar.o()));
        contentValues.put("last_bundle_start_timestamp", Long.valueOf(rVar.g()));
        contentValues.put("last_bundle_end_timestamp", Long.valueOf(rVar.h()));
        contentValues.put("app_version", rVar.i());
        contentValues.put("app_store", rVar.k());
        contentValues.put("gmp_version", Long.valueOf(rVar.l()));
        contentValues.put("dev_cert_hash", Long.valueOf(rVar.m()));
        contentValues.put("measurement_enabled", Boolean.valueOf(rVar.n()));
        contentValues.put("day", Long.valueOf(rVar.s()));
        contentValues.put("daily_public_events_count", Long.valueOf(rVar.t()));
        contentValues.put("daily_events_count", Long.valueOf(rVar.u()));
        contentValues.put("daily_conversions_count", Long.valueOf(rVar.v()));
        contentValues.put("config_fetched_time", Long.valueOf(rVar.p()));
        contentValues.put("failed_config_fetch_time", Long.valueOf(rVar.q()));
        contentValues.put("app_version_int", Long.valueOf(rVar.j()));
        contentValues.put("firebase_instance_id", rVar.f());
        contentValues.put("daily_error_events_count", Long.valueOf(rVar.x()));
        contentValues.put("daily_realtime_events_count", Long.valueOf(rVar.w()));
        contentValues.put("health_monitor_sample", rVar.y());
        contentValues.put("android_id", Long.valueOf(rVar.A()));
        contentValues.put("adid_reporting_enabled", Boolean.valueOf(rVar.B()));
        contentValues.put("ssaid_reporting_enabled", Boolean.valueOf(rVar.C()));
        try {
            SQLiteDatabase x = x();
            if (((long) x.update("apps", contentValues, "app_id = ?", new String[]{rVar.b()})) == 0 && x.insertWithOnConflict("apps", null, contentValues, 5) == -1) {
                q().v().a("Failed to insert/update app (got -1). appId", az.a(rVar.b()));
            }
        } catch (SQLiteException e2) {
            q().v().a("Error storing app. appId", az.a(rVar.b()), e2);
        }
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
        r8 = r3.d;
        r9 = r8.length;
        r2 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x00a2, code lost:
        if (r2 >= r9) goto L_0x00c1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x00a8, code lost:
        if (r8[r2].c != null) goto L_0x00be;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x00aa, code lost:
        q().y().a("Property filter with no ID. Audience definition ignored. appId, audienceId", com.google.android.gms.internal.c.az.a(r13), r3.c);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x00be, code lost:
        r2 = r2 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x00c1, code lost:
        r8 = r3.e;
        r9 = r8.length;
        r2 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x00c5, code lost:
        if (r2 >= r9) goto L_0x013c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00cd, code lost:
        if (a(r13, r7, r8[r2]) != false) goto L_0x011a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x00cf, code lost:
        r2 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00d0, code lost:
        if (r2 == false) goto L_0x00e1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x00d2, code lost:
        r8 = r3.d;
        r9 = r8.length;
        r3 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00d6, code lost:
        if (r3 >= r9) goto L_0x00e1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00de, code lost:
        if (a(r13, r7, r8[r3]) != false) goto L_0x011d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00e0, code lost:
        r2 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00e1, code lost:
        if (r2 != false) goto L_0x006d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00e3, code lost:
        N();
        c();
        com.google.android.gms.common.internal.aa.a(r13);
        r2 = x();
        r2.delete("property_filters", "app_id=? and audience_id=?", new java.lang.String[]{r13, java.lang.String.valueOf(r7)});
        r2.delete("event_filters", "app_id=? and audience_id=?", new java.lang.String[]{r13, java.lang.String.valueOf(r7)});
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x011a, code lost:
        r2 = r2 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x011d, code lost:
        r3 = r3 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x013c, code lost:
        r2 = true;
     */
    public final void a(String str, fw[] fwVarArr) {
        N();
        c();
        aa.a(str);
        aa.a(fwVarArr);
        SQLiteDatabase x = x();
        x.beginTransaction();
        try {
            N();
            c();
            aa.a(str);
            SQLiteDatabase x2 = x();
            x2.delete("property_filters", "app_id=?", new String[]{str});
            x2.delete("event_filters", "app_id=?", new String[]{str});
            int length = fwVarArr.length;
            for (int i2 = 0; i2 < length; i2++) {
                fw fwVar = fwVarArr[i2];
                N();
                c();
                aa.a(str);
                aa.a(fwVar);
                aa.a(fwVar.e);
                aa.a(fwVar.d);
                if (fwVar.c != null) {
                    int intValue = fwVar.c.intValue();
                    fx[] fxVarArr = fwVar.e;
                    int length2 = fxVarArr.length;
                    int i3 = 0;
                    while (true) {
                        if (i3 >= length2) {
                            break;
                        } else if (fxVarArr[i3].c == null) {
                            q().y().a("Event filter with no ID. Audience definition ignored. appId, audienceId", az.a(str), fwVar.c);
                            break;
                        } else {
                            i3++;
                        }
                    }
                } else {
                    q().y().a("Audience with no ID. appId", az.a(str));
                }
            }
            ArrayList arrayList = new ArrayList();
            for (fw fwVar2 : fwVarArr) {
                arrayList.add(fwVar2.c);
            }
            a(str, (List<Integer>) arrayList);
            x.setTransactionSuccessful();
        } finally {
            x.endTransaction();
        }
    }

    /* access modifiers changed from: 0000 */
    public final void a(List<Long> list) {
        c();
        N();
        aa.a(list);
        aa.a(list.size());
        if (P()) {
            String join = TextUtils.join(",", list);
            String sb = new StringBuilder(String.valueOf(join).length() + 2).append("(").append(join).append(")").toString();
            if (b(new StringBuilder(String.valueOf(sb).length() + 80).append("SELECT COUNT(1) FROM queue WHERE rowid IN ").append(sb).append(" AND retry_count =  2147483647 LIMIT 1").toString(), (String[]) null) > 0) {
                q().y().a("The number of upload retries exceeds the limit. Will remain unchanged.");
            }
            try {
                x().execSQL(new StringBuilder(String.valueOf(sb).length() + 127).append("UPDATE queue SET retry_count = IFNULL(retry_count, 0) + 1 WHERE rowid IN ").append(sb).append(" AND (retry_count IS NULL OR retry_count < 2147483647)").toString());
            } catch (SQLiteException e2) {
                q().v().a("Error incrementing retry count. error", e2);
            }
        }
    }

    public final boolean a(ai aiVar, long j, boolean z) {
        c();
        N();
        aa.a(aiVar);
        aa.a(aiVar.f3857a);
        gg ggVar = new gg();
        ggVar.f = Long.valueOf(aiVar.d);
        ggVar.c = new gh[aiVar.e.a()];
        Iterator it = aiVar.e.iterator();
        int i2 = 0;
        while (it.hasNext()) {
            String str = (String) it.next();
            gh ghVar = new gh();
            int i3 = i2 + 1;
            ggVar.c[i2] = ghVar;
            ghVar.c = str;
            n().a(ghVar, aiVar.e.a(str));
            i2 = i3;
        }
        try {
            byte[] bArr = new byte[ggVar.d()];
            b a2 = b.a(bArr, 0, bArr.length);
            ggVar.a(a2);
            a2.a();
            q().C().a("Saving event, name, data size", m().a(aiVar.f3858b), Integer.valueOf(bArr.length));
            ContentValues contentValues = new ContentValues();
            contentValues.put("app_id", aiVar.f3857a);
            contentValues.put("name", aiVar.f3858b);
            contentValues.put("timestamp", Long.valueOf(aiVar.c));
            contentValues.put("metadata_fingerprint", Long.valueOf(j));
            contentValues.put("data", bArr);
            contentValues.put("realtime", Integer.valueOf(z ? 1 : 0));
            try {
                if (x().insert("raw_events", null, contentValues) != -1) {
                    return true;
                }
                q().v().a("Failed to insert raw event (got -1). appId", az.a(aiVar.f3857a));
                return false;
            } catch (SQLiteException e2) {
                q().v().a("Error storing raw event. appId", az.a(aiVar.f3857a), e2);
                return false;
            }
        } catch (IOException e3) {
            q().v().a("Data loss. Failed to serialize event params/data. appId", az.a(aiVar.f3857a), e3);
            return false;
        }
    }

    public final boolean a(fs fsVar) {
        aa.a(fsVar);
        c();
        N();
        if (c(fsVar.f4084a, fsVar.c) == null) {
            if (ft.a(fsVar.c)) {
                if (b("select count(1) from user_attributes where app_id=? and name not like '!_%' escape '!'", new String[]{fsVar.f4084a}) >= 25) {
                    return false;
                }
            } else {
                if (b("select count(1) from user_attributes where app_id=? and origin=? AND name like '!_%' escape '!'", new String[]{fsVar.f4084a, fsVar.f4085b}) >= 25) {
                    return false;
                }
            }
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", fsVar.f4084a);
        contentValues.put("origin", fsVar.f4085b);
        contentValues.put("name", fsVar.c);
        contentValues.put("set_timestamp", Long.valueOf(fsVar.d));
        a(contentValues, "value", fsVar.e);
        try {
            if (x().insertWithOnConflict("user_attributes", null, contentValues, 5) == -1) {
                q().v().a("Failed to insert/update user property (got -1). appId", az.a(fsVar.f4084a));
            }
        } catch (SQLiteException e2) {
            q().v().a("Error storing user property. appId", az.a(fsVar.f4084a), e2);
        }
        return true;
    }

    public final boolean a(gj gjVar, boolean z) {
        c();
        N();
        aa.a(gjVar);
        aa.a(gjVar.q);
        aa.a(gjVar.h);
        A();
        long a2 = j().a();
        if (gjVar.h.longValue() < a2 - y.v() || gjVar.h.longValue() > y.v() + a2) {
            q().y().a("Storing bundle outside of the max uploading time span. appId, now, timestamp", az.a(gjVar.q), Long.valueOf(a2), gjVar.h);
        }
        try {
            byte[] bArr = new byte[gjVar.d()];
            b a3 = b.a(bArr, 0, bArr.length);
            gjVar.a(a3);
            a3.a();
            byte[] a4 = n().a(bArr);
            q().C().a("Saving bundle, size", Integer.valueOf(a4.length));
            ContentValues contentValues = new ContentValues();
            contentValues.put("app_id", gjVar.q);
            contentValues.put("bundle_end_timestamp", gjVar.h);
            contentValues.put("data", a4);
            contentValues.put("has_realtime", Integer.valueOf(z ? 1 : 0));
            if (gjVar.J != null) {
                contentValues.put("retry_count", gjVar.J);
            }
            try {
                if (x().insert("queue", null, contentValues) != -1) {
                    return true;
                }
                q().v().a("Failed to insert bundle (got -1). appId", az.a(gjVar.q));
                return false;
            } catch (SQLiteException e2) {
                q().v().a("Error storing bundle. appId", az.a(gjVar.q), e2);
                return false;
            }
        } catch (IOException e3) {
            q().v().a("Data loss. Failed to serialize bundle. appId", az.a(gjVar.q), e3);
            return false;
        }
    }

    public final boolean a(w wVar) {
        aa.a(wVar);
        c();
        N();
        if (c(wVar.f4129a, wVar.c.f4082a) == null) {
            if (b("SELECT COUNT(1) FROM conditional_properties WHERE app_id=?", new String[]{wVar.f4129a}) >= 1000) {
                return false;
            }
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", wVar.f4129a);
        contentValues.put("origin", wVar.f4130b);
        contentValues.put("name", wVar.c.f4082a);
        a(contentValues, "value", wVar.c.a());
        contentValues.put("active", Boolean.valueOf(wVar.e));
        contentValues.put("trigger_event_name", wVar.f);
        contentValues.put("trigger_timeout", Long.valueOf(wVar.h));
        n();
        contentValues.put("timed_out_event", ft.a((Parcelable) wVar.g));
        contentValues.put("creation_timestamp", Long.valueOf(wVar.d));
        n();
        contentValues.put("triggered_event", ft.a((Parcelable) wVar.i));
        contentValues.put("triggered_timestamp", Long.valueOf(wVar.c.f4083b));
        contentValues.put("time_to_live", Long.valueOf(wVar.j));
        n();
        contentValues.put("expired_event", ft.a((Parcelable) wVar.k));
        try {
            if (x().insertWithOnConflict("conditional_properties", null, contentValues, 5) == -1) {
                q().v().a("Failed to insert/update conditional user property (got -1)", az.a(wVar.f4129a));
            }
        } catch (SQLiteException e2) {
            q().v().a("Error storing conditional user property", az.a(wVar.f4129a), e2);
        }
        return true;
    }

    public final boolean a(String str, Long l, long j, gg ggVar) {
        c();
        N();
        aa.a(ggVar);
        aa.a(str);
        aa.a(l);
        try {
            byte[] bArr = new byte[ggVar.d()];
            b a2 = b.a(bArr, 0, bArr.length);
            ggVar.a(a2);
            a2.a();
            q().C().a("Saving complex main event, appId, data size", m().a(str), Integer.valueOf(bArr.length));
            ContentValues contentValues = new ContentValues();
            contentValues.put("app_id", str);
            contentValues.put("event_id", l);
            contentValues.put("children_to_process", Long.valueOf(j));
            contentValues.put("main_event", bArr);
            try {
                if (x().insertWithOnConflict("main_event_params", null, contentValues, 5) != -1) {
                    return true;
                }
                q().v().a("Failed to insert complex main event (got -1). appId", az.a(str));
                return false;
            } catch (SQLiteException e2) {
                q().v().a("Error storing complex main event. appId", az.a(str), e2);
                return false;
            }
        } catch (IOException e3) {
            q().v().a("Data loss. Failed to serialize event params/data. appId, eventId", az.a(str), l, e3);
            return false;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:53:0x0225  */
    public final r b(String str) {
        Cursor cursor;
        aa.a(str);
        c();
        N();
        try {
            cursor = x().query("apps", new String[]{"app_instance_id", "gmp_app_id", "resettable_device_id_hash", "last_bundle_index", "last_bundle_start_timestamp", "last_bundle_end_timestamp", "app_version", "app_store", "gmp_version", "dev_cert_hash", "measurement_enabled", "day", "daily_public_events_count", "daily_events_count", "daily_conversions_count", "config_fetched_time", "failed_config_fetch_time", "app_version_int", "firebase_instance_id", "daily_error_events_count", "daily_realtime_events_count", "health_monitor_sample", "android_id", "adid_reporting_enabled", "ssaid_reporting_enabled"}, "app_id=?", new String[]{str}, null, null, null);
            try {
                if (!cursor.moveToFirst()) {
                    if (cursor != null) {
                        cursor.close();
                    }
                    return null;
                }
                r rVar = new r(this.f4071a.J(), str);
                rVar.a(cursor.getString(0));
                rVar.b(cursor.getString(1));
                rVar.c(cursor.getString(2));
                rVar.f(cursor.getLong(3));
                rVar.a(cursor.getLong(4));
                rVar.b(cursor.getLong(5));
                rVar.e(cursor.getString(6));
                rVar.f(cursor.getString(7));
                rVar.d(cursor.getLong(8));
                rVar.e(cursor.getLong(9));
                rVar.a(cursor.isNull(10) || cursor.getInt(10) != 0);
                rVar.i(cursor.getLong(11));
                rVar.j(cursor.getLong(12));
                rVar.k(cursor.getLong(13));
                rVar.l(cursor.getLong(14));
                rVar.g(cursor.getLong(15));
                rVar.h(cursor.getLong(16));
                rVar.c(cursor.isNull(17) ? -2147483648L : (long) cursor.getInt(17));
                rVar.d(cursor.getString(18));
                rVar.n(cursor.getLong(19));
                rVar.m(cursor.getLong(20));
                rVar.g(cursor.getString(21));
                rVar.o(cursor.isNull(22) ? 0 : cursor.getLong(22));
                rVar.b(cursor.isNull(23) || cursor.getInt(23) != 0);
                rVar.c(cursor.isNull(24) || cursor.getInt(24) != 0);
                rVar.a();
                if (cursor.moveToNext()) {
                    q().v().a("Got multiple records for app, expected one. appId", az.a(str));
                }
                if (cursor == null) {
                    return rVar;
                }
                cursor.close();
                return rVar;
            } catch (SQLiteException e2) {
                e = e2;
            }
        } catch (SQLiteException e3) {
            e = e3;
            cursor = null;
        } catch (Throwable th) {
            th = th;
            cursor = null;
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
        try {
            q().v().a("Error querying app. appId", az.a(str), e);
            if (cursor != null) {
                cursor.close();
            }
            return null;
        } catch (Throwable th2) {
            th = th2;
            if (cursor != null) {
            }
            throw th;
        }
    }

    public final List<w> b(String str, String str2, String str3) {
        aa.a(str);
        c();
        N();
        ArrayList arrayList = new ArrayList(3);
        arrayList.add(str);
        StringBuilder sb = new StringBuilder("app_id=?");
        if (!TextUtils.isEmpty(str2)) {
            arrayList.add(str2);
            sb.append(" and origin=?");
        }
        if (!TextUtils.isEmpty(str3)) {
            arrayList.add(String.valueOf(str3).concat("*"));
            sb.append(" and name glob ?");
        }
        return a(sb.toString(), (String[]) arrayList.toArray(new String[arrayList.size()]));
    }

    public final void b(String str, String str2) {
        aa.a(str);
        aa.a(str2);
        c();
        N();
        try {
            q().C().a("Deleted user attribute rows", Integer.valueOf(x().delete("user_attributes", "app_id=? and name=?", new String[]{str, str2})));
        } catch (SQLiteException e2) {
            q().v().a("Error deleting user attribute. appId", az.a(str), m().c(str2), e2);
        }
    }

    public final long c(String str) {
        aa.a(str);
        c();
        N();
        try {
            return (long) x().delete("raw_events", "rowid in (select rowid from raw_events where app_id=? order by rowid desc limit -1 offset ?)", new String[]{str, String.valueOf(Math.max(0, Math.min(1000000, s().b(str, ap.q))))});
        } catch (SQLiteException e2) {
            q().v().a("Error deleting over the limit events. appId", az.a(str), e2);
            return 0;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x009c  */
    public final fs c(String str, String str2) {
        Cursor cursor;
        Cursor cursor2 = null;
        aa.a(str);
        aa.a(str2);
        c();
        N();
        try {
            Cursor query = x().query("user_attributes", new String[]{"set_timestamp", "value", "origin"}, "app_id=? and name=?", new String[]{str, str2}, null, null, null);
            try {
                if (!query.moveToFirst()) {
                    if (query != null) {
                        query.close();
                    }
                    return null;
                }
                String str3 = str;
                fs fsVar = new fs(str3, query.getString(2), str2, query.getLong(0), a(query, 1));
                if (query.moveToNext()) {
                    q().v().a("Got multiple records for user property, expected one. appId", az.a(str));
                }
                if (query == null) {
                    return fsVar;
                }
                query.close();
                return fsVar;
            } catch (SQLiteException e2) {
                e = e2;
                cursor = query;
            } catch (Throwable th) {
                th = th;
                cursor2 = query;
                if (cursor2 != null) {
                }
                throw th;
            }
        } catch (SQLiteException e3) {
            e = e3;
            cursor = null;
        } catch (Throwable th2) {
            th = th2;
            if (cursor2 != null) {
            }
            throw th;
        }
        try {
            q().v().a("Error querying user property. appId", az.a(str), m().c(str2), e);
            if (cursor != null) {
                cursor.close();
            }
            return null;
        } catch (Throwable th3) {
            th = th3;
            cursor2 = cursor;
            if (cursor2 != null) {
                cursor2.close();
            }
            throw th;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:30:0x014d  */
    public final w d(String str, String str2) {
        Cursor cursor;
        Cursor cursor2;
        aa.a(str);
        aa.a(str2);
        c();
        N();
        try {
            cursor = x().query("conditional_properties", new String[]{"origin", "value", "active", "trigger_event_name", "trigger_timeout", "timed_out_event", "creation_timestamp", "triggered_event", "triggered_timestamp", "time_to_live", "expired_event"}, "app_id=? and name=?", new String[]{str, str2}, null, null, null);
            try {
                if (!cursor.moveToFirst()) {
                    if (cursor != null) {
                        cursor.close();
                    }
                    return null;
                }
                String string = cursor.getString(0);
                Object a2 = a(cursor, 1);
                boolean z = cursor.getInt(2) != 0;
                String string2 = cursor.getString(3);
                long j = cursor.getLong(4);
                an anVar = (an) n().a(cursor.getBlob(5), an.CREATOR);
                long j2 = cursor.getLong(6);
                an anVar2 = (an) n().a(cursor.getBlob(7), an.CREATOR);
                long j3 = cursor.getLong(8);
                w wVar = new w(str, string, new fq(str2, j3, a2, string), j2, z, string2, anVar, j, anVar2, cursor.getLong(9), (an) n().a(cursor.getBlob(10), an.CREATOR));
                if (cursor.moveToNext()) {
                    q().v().a("Got multiple records for conditional property, expected one", az.a(str), m().c(str2));
                }
                if (cursor == null) {
                    return wVar;
                }
                cursor.close();
                return wVar;
            } catch (SQLiteException e2) {
                e = e2;
                cursor2 = cursor;
            } catch (Throwable th) {
                th = th;
                if (cursor != null) {
                }
                throw th;
            }
        } catch (SQLiteException e3) {
            e = e3;
            cursor2 = null;
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
            if (cursor != null) {
            }
            throw th;
        }
        try {
            q().v().a("Error querying conditional property", az.a(str), m().c(str2), e);
            if (cursor2 != null) {
                cursor2.close();
            }
            return null;
        } catch (Throwable th3) {
            th = th3;
            cursor = cursor2;
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:26:0x0074  */
    public final byte[] d(String str) {
        Cursor cursor;
        aa.a(str);
        c();
        N();
        try {
            cursor = x().query("apps", new String[]{"remote_config"}, "app_id=?", new String[]{str}, null, null, null);
            try {
                if (!cursor.moveToFirst()) {
                    if (cursor != null) {
                        cursor.close();
                    }
                    return null;
                }
                byte[] blob = cursor.getBlob(0);
                if (cursor.moveToNext()) {
                    q().v().a("Got multiple records for app config, expected one. appId", az.a(str));
                }
                if (cursor == null) {
                    return blob;
                }
                cursor.close();
                return blob;
            } catch (SQLiteException e2) {
                e = e2;
            }
        } catch (SQLiteException e3) {
            e = e3;
            cursor = null;
        } catch (Throwable th) {
            th = th;
            cursor = null;
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
        try {
            q().v().a("Error querying remote config. appId", az.a(str), e);
            if (cursor != null) {
                cursor.close();
            }
            return null;
        } catch (Throwable th2) {
            th = th2;
            if (cursor != null) {
            }
            throw th;
        }
    }

    public final int e(String str, String str2) {
        boolean z = false;
        aa.a(str);
        aa.a(str2);
        c();
        N();
        try {
            return x().delete("conditional_properties", "app_id=? and name=?", new String[]{str, str2});
        } catch (SQLiteException e2) {
            q().v().a("Error deleting conditional property", az.a(str), m().c(str2), e2);
            return z;
        }
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x009d  */
    public final Map<Integer, gk> e(String str) {
        Cursor cursor;
        N();
        c();
        aa.a(str);
        try {
            cursor = x().query("audience_filter_values", new String[]{"audience_id", "current_results"}, "app_id=?", new String[]{str}, null, null, null);
            try {
                if (!cursor.moveToFirst()) {
                    if (cursor != null) {
                        cursor.close();
                    }
                    return null;
                }
                a aVar = new a();
                do {
                    int i2 = cursor.getInt(0);
                    byte[] blob = cursor.getBlob(1);
                    a a2 = a.a(blob, 0, blob.length);
                    gk gkVar = new gk();
                    try {
                        gkVar.a(a2);
                        aVar.put(Integer.valueOf(i2), gkVar);
                    } catch (IOException e2) {
                        q().v().a("Failed to merge filter results. appId, audienceId, error", az.a(str), Integer.valueOf(i2), e2);
                    }
                } while (cursor.moveToNext());
                if (cursor == null) {
                    return aVar;
                }
                cursor.close();
                return aVar;
            } catch (SQLiteException e3) {
                e = e3;
            }
        } catch (SQLiteException e4) {
            e = e4;
            cursor = null;
        } catch (Throwable th) {
            th = th;
            cursor = null;
            if (cursor != null) {
            }
            throw th;
        }
        try {
            q().v().a("Database error querying filter results. appId", az.a(str), e);
            if (cursor != null) {
                cursor.close();
            }
            return null;
        } catch (Throwable th2) {
            th = th2;
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    public final long f(String str) {
        aa.a(str);
        return a("select count(1) from events where app_id=? and name not like '!_%' escape '!'", new String[]{str}, 0);
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00b7  */
    public final Map<Integer, List<fx>> f(String str, String str2) {
        Cursor cursor;
        N();
        c();
        aa.a(str);
        aa.a(str2);
        a aVar = new a();
        try {
            cursor = x().query("event_filters", new String[]{"audience_id", "data"}, "app_id=? AND event_name=?", new String[]{str, str2}, null, null, null);
            try {
                if (!cursor.moveToFirst()) {
                    Map<Integer, List<fx>> emptyMap = Collections.emptyMap();
                    if (cursor == null) {
                        return emptyMap;
                    }
                    cursor.close();
                    return emptyMap;
                }
                do {
                    byte[] blob = cursor.getBlob(1);
                    a a2 = a.a(blob, 0, blob.length);
                    fx fxVar = new fx();
                    try {
                        fxVar.a(a2);
                        int i2 = cursor.getInt(0);
                        List list = (List) aVar.get(Integer.valueOf(i2));
                        if (list == null) {
                            list = new ArrayList();
                            aVar.put(Integer.valueOf(i2), list);
                        }
                        list.add(fxVar);
                    } catch (IOException e2) {
                        q().v().a("Failed to merge filter. appId", az.a(str), e2);
                    }
                } while (cursor.moveToNext());
                if (cursor != null) {
                    cursor.close();
                }
                return aVar;
            } catch (SQLiteException e3) {
                e = e3;
            }
        } catch (SQLiteException e4) {
            e = e4;
            cursor = null;
        } catch (Throwable th) {
            th = th;
            cursor = null;
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
        try {
            q().v().a("Database error querying filters. appId", az.a(str), e);
            if (cursor != null) {
                cursor.close();
            }
            return null;
        } catch (Throwable th2) {
            th = th2;
            if (cursor != null) {
            }
            throw th;
        }
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00b7  */
    public final Map<Integer, List<ga>> g(String str, String str2) {
        Cursor cursor;
        N();
        c();
        aa.a(str);
        aa.a(str2);
        a aVar = new a();
        try {
            cursor = x().query("property_filters", new String[]{"audience_id", "data"}, "app_id=? AND property_name=?", new String[]{str, str2}, null, null, null);
            try {
                if (!cursor.moveToFirst()) {
                    Map<Integer, List<ga>> emptyMap = Collections.emptyMap();
                    if (cursor == null) {
                        return emptyMap;
                    }
                    cursor.close();
                    return emptyMap;
                }
                do {
                    byte[] blob = cursor.getBlob(1);
                    a a2 = a.a(blob, 0, blob.length);
                    ga gaVar = new ga();
                    try {
                        gaVar.a(a2);
                        int i2 = cursor.getInt(0);
                        List list = (List) aVar.get(Integer.valueOf(i2));
                        if (list == null) {
                            list = new ArrayList();
                            aVar.put(Integer.valueOf(i2), list);
                        }
                        list.add(gaVar);
                    } catch (IOException e2) {
                        q().v().a("Failed to merge filter", az.a(str), e2);
                    }
                } while (cursor.moveToNext());
                if (cursor != null) {
                    cursor.close();
                }
                return aVar;
            } catch (SQLiteException e3) {
                e = e3;
            }
        } catch (SQLiteException e4) {
            e = e4;
            cursor = null;
        } catch (Throwable th) {
            th = th;
            cursor = null;
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
        try {
            q().v().a("Database error querying filters. appId", az.a(str), e);
            if (cursor != null) {
                cursor.close();
            }
            return null;
        } catch (Throwable th2) {
            th = th2;
            if (cursor != null) {
            }
            throw th;
        }
    }

    /* access modifiers changed from: protected */
    public final long h(String str, String str2) {
        SQLiteException e2;
        long j;
        aa.a(str);
        aa.a(str2);
        c();
        N();
        SQLiteDatabase x = x();
        x.beginTransaction();
        try {
            j = a(new StringBuilder(String.valueOf(str2).length() + 32).append("select ").append(str2).append(" from app2 where app_id=?").toString(), new String[]{str}, -1);
            if (j == -1) {
                ContentValues contentValues = new ContentValues();
                contentValues.put("app_id", str);
                contentValues.put("first_open_count", Integer.valueOf(0));
                contentValues.put("previous_install_count", Integer.valueOf(0));
                if (x.insertWithOnConflict("app2", null, contentValues, 5) == -1) {
                    q().v().a("Failed to insert column (got -1). appId", az.a(str), str2);
                    x.endTransaction();
                    return -1;
                }
                j = 0;
            }
            try {
                ContentValues contentValues2 = new ContentValues();
                contentValues2.put("app_id", str);
                contentValues2.put(str2, Long.valueOf(1 + j));
                if (((long) x.update("app2", contentValues2, "app_id = ?", new String[]{str})) == 0) {
                    q().v().a("Failed to update column (got 0). appId", az.a(str), str2);
                    x.endTransaction();
                    return -1;
                }
                x.setTransactionSuccessful();
                x.endTransaction();
                return j;
            } catch (SQLiteException e3) {
                e2 = e3;
                try {
                    q().v().a("Error inserting column. appId", az.a(str), str2, e2);
                    return j;
                } finally {
                    x.endTransaction();
                }
            }
        } catch (SQLiteException e4) {
            e2 = e4;
            j = 0;
            q().v().a("Error inserting column. appId", az.a(str), str2, e2);
            return j;
        }
    }

    /* access modifiers changed from: protected */
    public final boolean t() {
        return false;
    }

    public final void u() {
        N();
        x().beginTransaction();
    }

    public final void v() {
        N();
        x().setTransactionSuccessful();
    }

    public final void w() {
        N();
        x().endTransaction();
    }

    /* access modifiers changed from: 0000 */
    public final SQLiteDatabase x() {
        c();
        try {
            return this.h.getWritableDatabase();
        } catch (SQLiteException e2) {
            q().y().a("Error opening database", e2);
            throw e2;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x003d  */
    public final String y() {
        Cursor cursor;
        Throwable th;
        String str = null;
        try {
            cursor = x().rawQuery("select app_id from queue order by has_realtime desc, rowid asc limit 1;", null);
            try {
                if (cursor.moveToFirst()) {
                    str = cursor.getString(0);
                    if (cursor != null) {
                        cursor.close();
                    }
                } else if (cursor != null) {
                    cursor.close();
                }
            } catch (SQLiteException e2) {
                e = e2;
                try {
                    q().v().a("Database error getting next bundle app id", e);
                    if (cursor != null) {
                        cursor.close();
                    }
                    return str;
                } catch (Throwable th2) {
                    th = th2;
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            }
        } catch (SQLiteException e3) {
            e = e3;
            cursor = null;
        } catch (Throwable th3) {
            cursor = null;
            th = th3;
            if (cursor != null) {
            }
            throw th;
        }
        return str;
    }

    public final boolean z() {
        return b("select count(1) > 0 from queue where has_realtime = 1", (String[]) null) != 0;
    }
}
