package com.google.android.gms.internal.c;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

final class fu extends SSLSocketFactory {

    /* renamed from: a reason: collision with root package name */
    private final SSLSocketFactory f4088a;

    fu() {
        this(HttpsURLConnection.getDefaultSSLSocketFactory());
    }

    private fu(SSLSocketFactory sSLSocketFactory) {
        this.f4088a = sSLSocketFactory;
    }

    private final SSLSocket a(SSLSocket sSLSocket) {
        return new fv(this, sSLSocket);
    }

    public final Socket createSocket() throws IOException {
        return a((SSLSocket) this.f4088a.createSocket());
    }

    public final Socket createSocket(String str, int i) throws IOException {
        return a((SSLSocket) this.f4088a.createSocket(str, i));
    }

    public final Socket createSocket(String str, int i, InetAddress inetAddress, int i2) throws IOException {
        return a((SSLSocket) this.f4088a.createSocket(str, i, inetAddress, i2));
    }

    public final Socket createSocket(InetAddress inetAddress, int i) throws IOException {
        return a((SSLSocket) this.f4088a.createSocket(inetAddress, i));
    }

    public final Socket createSocket(InetAddress inetAddress, int i, InetAddress inetAddress2, int i2) throws IOException {
        return a((SSLSocket) this.f4088a.createSocket(inetAddress, i, inetAddress2, i2));
    }

    public final Socket createSocket(Socket socket, String str, int i, boolean z) throws IOException {
        return a((SSLSocket) this.f4088a.createSocket(socket, str, i, z));
    }

    public final String[] getDefaultCipherSuites() {
        return this.f4088a.getDefaultCipherSuites();
    }

    public final String[] getSupportedCipherSuites() {
        return this.f4088a.getSupportedCipherSuites();
    }
}
