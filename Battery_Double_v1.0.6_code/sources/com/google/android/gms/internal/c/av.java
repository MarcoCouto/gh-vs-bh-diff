package com.google.android.gms.internal.c;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabaseLockedException;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteFullException;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemClock;
import com.google.android.gms.common.internal.a.a;
import com.google.android.gms.common.internal.a.b;
import com.google.android.gms.common.util.e;
import java.util.ArrayList;
import java.util.List;

public final class av extends da {

    /* renamed from: a reason: collision with root package name */
    private final aw f3872a = new aw(this, k(), "google_app_measurement_local.db");

    /* renamed from: b reason: collision with root package name */
    private boolean f3873b;

    av(ce ceVar) {
        super(ceVar);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00cf, code lost:
        r6 = r6 + 1;
     */
    private final boolean a(int i, byte[] bArr) {
        c();
        if (this.f3873b) {
            return false;
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("type", Integer.valueOf(i));
        contentValues.put("entry", bArr);
        int i2 = 0;
        int i3 = 5;
        while (i2 < 5) {
            SQLiteDatabase sQLiteDatabase = null;
            Cursor cursor = null;
            try {
                sQLiteDatabase = w();
                if (sQLiteDatabase == null) {
                    this.f3873b = true;
                    if (sQLiteDatabase != null) {
                        sQLiteDatabase.close();
                    }
                    return false;
                }
                sQLiteDatabase.beginTransaction();
                long j = 0;
                Cursor rawQuery = sQLiteDatabase.rawQuery("select count(1) from messages", null);
                if (rawQuery != null && rawQuery.moveToFirst()) {
                    j = rawQuery.getLong(0);
                }
                if (j >= 100000) {
                    q().v().a("Data loss, local db full");
                    long j2 = (100000 - j) + 1;
                    long delete = (long) sQLiteDatabase.delete("messages", "rowid in (select rowid from messages order by rowid asc limit ?)", new String[]{Long.toString(j2)});
                    if (delete != j2) {
                        q().v().a("Different delete count than expected in local db. expected, received, difference", Long.valueOf(j2), Long.valueOf(delete), Long.valueOf(j2 - delete));
                    }
                }
                sQLiteDatabase.insertOrThrow("messages", null, contentValues);
                sQLiteDatabase.setTransactionSuccessful();
                sQLiteDatabase.endTransaction();
                if (rawQuery != null) {
                    rawQuery.close();
                }
                if (sQLiteDatabase != null) {
                    sQLiteDatabase.close();
                }
                return true;
            } catch (SQLiteFullException e) {
                q().v().a("Error writing entry to local database", e);
                this.f3873b = true;
                if (cursor != null) {
                    cursor.close();
                }
                if (sQLiteDatabase != null) {
                    sQLiteDatabase.close();
                }
            } catch (SQLiteDatabaseLockedException e2) {
                SystemClock.sleep((long) i3);
                i3 += 20;
                if (cursor != null) {
                    cursor.close();
                }
                if (sQLiteDatabase != null) {
                    sQLiteDatabase.close();
                }
            } catch (SQLiteException e3) {
                if (sQLiteDatabase != null) {
                    if (sQLiteDatabase.inTransaction()) {
                        sQLiteDatabase.endTransaction();
                    }
                }
                q().v().a("Error writing entry to local database", e3);
                this.f3873b = true;
                if (cursor != null) {
                    cursor.close();
                }
                if (sQLiteDatabase != null) {
                    sQLiteDatabase.close();
                }
            } catch (Throwable th) {
                if (cursor != null) {
                    cursor.close();
                }
                if (sQLiteDatabase != null) {
                    sQLiteDatabase.close();
                }
                throw th;
            }
        }
        q().y().a("Failed to write entry to local database");
        return false;
    }

    private final SQLiteDatabase w() throws SQLiteException {
        if (this.f3873b) {
            return null;
        }
        SQLiteDatabase writableDatabase = this.f3872a.getWritableDatabase();
        if (writableDatabase != null) {
            return writableDatabase;
        }
        this.f3873b = true;
        return null;
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Removed duplicated region for block: B:154:0x00b6 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x00d8  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x00dd  */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x00ec A[SYNTHETIC, Splitter:B:66:0x00ec] */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x0107  */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x010c  */
    /* JADX WARNING: Removed duplicated region for block: B:89:0x013a  */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x013f  */
    public final List<a> a(int i) {
        int i2;
        int i3;
        SQLiteDatabase sQLiteDatabase;
        int i4;
        SQLiteDatabase sQLiteDatabase2;
        Cursor cursor;
        Cursor cursor2;
        w wVar;
        fq fqVar;
        c();
        if (this.f3873b) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        if (!k().getDatabasePath("google_app_measurement_local.db").exists()) {
            return arrayList;
        }
        i2 = 5;
        i3 = 0;
        while (i3 < 5) {
            sQLiteDatabase = null;
            try {
                SQLiteDatabase w = w();
                if (w == null) {
                    try {
                        this.f3873b = true;
                        if (w != null) {
                            w.close();
                        }
                        return null;
                    } catch (SQLiteFullException e) {
                        sQLiteDatabase2 = w;
                        e = e;
                        cursor = null;
                    } catch (SQLiteDatabaseLockedException e2) {
                        cursor2 = null;
                        sQLiteDatabase = w;
                        try {
                            SystemClock.sleep((long) i2);
                            i4 = i2 + 20;
                            if (cursor2 != null) {
                            }
                            if (sQLiteDatabase != null) {
                            }
                            i3++;
                            i2 = i4;
                        } catch (Throwable th) {
                            th = th;
                            if (cursor2 != null) {
                            }
                            if (sQLiteDatabase != null) {
                            }
                            throw th;
                        }
                    } catch (SQLiteException e3) {
                        cursor2 = null;
                        SQLiteException sQLiteException = e3;
                        sQLiteDatabase = w;
                        e = sQLiteException;
                        if (sQLiteDatabase != null) {
                        }
                        q().v().a("Error reading entries from local database", e);
                        this.f3873b = true;
                        if (cursor2 != null) {
                        }
                        if (sQLiteDatabase != null) {
                        }
                        i4 = i2;
                        i3++;
                        i2 = i4;
                    } catch (Throwable th2) {
                        cursor2 = null;
                        Throwable th3 = th2;
                        sQLiteDatabase = w;
                        th = th3;
                        if (cursor2 != null) {
                        }
                        if (sQLiteDatabase != null) {
                        }
                        throw th;
                    }
                } else {
                    w.beginTransaction();
                    cursor2 = w.query("messages", new String[]{"rowid", "type", "entry"}, null, null, null, null, "rowid asc", Integer.toString(100));
                    long j = -1;
                    while (cursor2.moveToNext()) {
                        try {
                            j = cursor2.getLong(0);
                            int i5 = cursor2.getInt(1);
                            byte[] blob = cursor2.getBlob(2);
                            if (i5 == 0) {
                                Parcel obtain = Parcel.obtain();
                                try {
                                    obtain.unmarshall(blob, 0, blob.length);
                                    obtain.setDataPosition(0);
                                    an anVar = (an) an.CREATOR.createFromParcel(obtain);
                                    if (anVar != null) {
                                        arrayList.add(anVar);
                                    }
                                } catch (b.a e4) {
                                    q().v().a("Failed to load event from local database");
                                } finally {
                                    obtain.recycle();
                                }
                            } else if (i5 == 1) {
                                Parcel obtain2 = Parcel.obtain();
                                try {
                                    obtain2.unmarshall(blob, 0, blob.length);
                                    obtain2.setDataPosition(0);
                                    fqVar = (fq) fq.CREATOR.createFromParcel(obtain2);
                                    obtain2.recycle();
                                } catch (b.a e5) {
                                    q().v().a("Failed to load user property from local database");
                                    obtain2.recycle();
                                    fqVar = null;
                                } catch (Throwable th4) {
                                    obtain2.recycle();
                                    throw th4;
                                }
                                if (fqVar != null) {
                                    arrayList.add(fqVar);
                                }
                            } else if (i5 == 2) {
                                Parcel obtain3 = Parcel.obtain();
                                try {
                                    obtain3.unmarshall(blob, 0, blob.length);
                                    obtain3.setDataPosition(0);
                                    wVar = (w) w.CREATOR.createFromParcel(obtain3);
                                    obtain3.recycle();
                                } catch (b.a e6) {
                                    q().v().a("Failed to load user property from local database");
                                    obtain3.recycle();
                                    wVar = null;
                                } catch (Throwable th5) {
                                    obtain3.recycle();
                                    throw th5;
                                }
                                if (wVar != null) {
                                    arrayList.add(wVar);
                                }
                            } else {
                                q().v().a("Unknown record type in local database");
                            }
                        } catch (SQLiteFullException e7) {
                            SQLiteFullException sQLiteFullException = e7;
                            cursor = cursor2;
                            sQLiteDatabase2 = w;
                            e = sQLiteFullException;
                        } catch (SQLiteDatabaseLockedException e8) {
                            sQLiteDatabase = w;
                            SystemClock.sleep((long) i2);
                            i4 = i2 + 20;
                            if (cursor2 != null) {
                                cursor2.close();
                            }
                            if (sQLiteDatabase != null) {
                                sQLiteDatabase.close();
                            }
                            i3++;
                            i2 = i4;
                        } catch (SQLiteException e9) {
                            SQLiteException sQLiteException2 = e9;
                            sQLiteDatabase = w;
                            e = sQLiteException2;
                            if (sQLiteDatabase != null) {
                                if (sQLiteDatabase.inTransaction()) {
                                    sQLiteDatabase.endTransaction();
                                }
                            }
                            q().v().a("Error reading entries from local database", e);
                            this.f3873b = true;
                            if (cursor2 != null) {
                                cursor2.close();
                            }
                            if (sQLiteDatabase != null) {
                                sQLiteDatabase.close();
                                i4 = i2;
                                i3++;
                                i2 = i4;
                            }
                            i4 = i2;
                            i3++;
                            i2 = i4;
                        } catch (Throwable th6) {
                            Throwable th7 = th6;
                            sQLiteDatabase = w;
                            th = th7;
                            if (cursor2 != null) {
                                cursor2.close();
                            }
                            if (sQLiteDatabase != null) {
                                sQLiteDatabase.close();
                            }
                            throw th;
                        }
                    }
                    if (w.delete("messages", "rowid <= ?", new String[]{Long.toString(j)}) < arrayList.size()) {
                        q().v().a("Fewer entries removed from local database than expected");
                    }
                    w.setTransactionSuccessful();
                    w.endTransaction();
                    if (cursor2 != null) {
                        cursor2.close();
                    }
                    if (w != null) {
                        w.close();
                    }
                    return arrayList;
                }
            } catch (SQLiteFullException e10) {
                e = e10;
                sQLiteDatabase2 = null;
                cursor = null;
            } catch (SQLiteDatabaseLockedException e11) {
                cursor2 = null;
                SystemClock.sleep((long) i2);
                i4 = i2 + 20;
                if (cursor2 != null) {
                }
                if (sQLiteDatabase != null) {
                }
                i3++;
                i2 = i4;
            } catch (SQLiteException e12) {
                e = e12;
                cursor2 = null;
                if (sQLiteDatabase != null) {
                }
                q().v().a("Error reading entries from local database", e);
                this.f3873b = true;
                if (cursor2 != null) {
                }
                if (sQLiteDatabase != null) {
                }
                i4 = i2;
                i3++;
                i2 = i4;
            } catch (Throwable th8) {
                th = th8;
                cursor2 = null;
                if (cursor2 != null) {
                }
                if (sQLiteDatabase != null) {
                }
                throw th;
            }
        }
        q().y().a("Failed to read events from database in reasonable time");
        return null;
        try {
            q().v().a("Error reading entries from local database", e);
            this.f3873b = true;
            if (cursor != null) {
                cursor.close();
            }
            if (sQLiteDatabase2 != null) {
                sQLiteDatabase2.close();
                i4 = i2;
                i3++;
                i2 = i4;
            }
            i4 = i2;
            i3++;
            i2 = i4;
        } catch (Throwable th9) {
            th = th9;
            Cursor cursor3 = cursor;
            sQLiteDatabase = sQLiteDatabase2;
            cursor2 = cursor3;
            if (cursor2 != null) {
            }
            if (sQLiteDatabase != null) {
            }
            throw th;
        }
    }

    public final /* bridge */ /* synthetic */ void a() {
        super.a();
    }

    public final boolean a(an anVar) {
        Parcel obtain = Parcel.obtain();
        anVar.writeToParcel(obtain, 0);
        byte[] marshall = obtain.marshall();
        obtain.recycle();
        if (marshall.length <= 131072) {
            return a(0, marshall);
        }
        q().y().a("Event is too long for local database. Sending event directly to service");
        return false;
    }

    public final boolean a(fq fqVar) {
        Parcel obtain = Parcel.obtain();
        fqVar.writeToParcel(obtain, 0);
        byte[] marshall = obtain.marshall();
        obtain.recycle();
        if (marshall.length <= 131072) {
            return a(1, marshall);
        }
        q().y().a("User property too long for local database. Sending directly to service");
        return false;
    }

    public final boolean a(w wVar) {
        n();
        byte[] a2 = ft.a((Parcelable) wVar);
        if (a2.length <= 131072) {
            return a(2, a2);
        }
        q().y().a("Conditional user property too long for local database. Sending directly to service");
        return false;
    }

    public final /* bridge */ /* synthetic */ void b() {
        super.b();
    }

    public final /* bridge */ /* synthetic */ void c() {
        super.c();
    }

    public final /* bridge */ /* synthetic */ n d() {
        return super.d();
    }

    public final /* bridge */ /* synthetic */ dd e() {
        return super.e();
    }

    public final /* bridge */ /* synthetic */ au f() {
        return super.f();
    }

    public final /* bridge */ /* synthetic */ ah g() {
        return super.g();
    }

    public final /* bridge */ /* synthetic */ eb h() {
        return super.h();
    }

    public final /* bridge */ /* synthetic */ dy i() {
        return super.i();
    }

    public final /* bridge */ /* synthetic */ e j() {
        return super.j();
    }

    public final /* bridge */ /* synthetic */ Context k() {
        return super.k();
    }

    public final /* bridge */ /* synthetic */ av l() {
        return super.l();
    }

    public final /* bridge */ /* synthetic */ ax m() {
        return super.m();
    }

    public final /* bridge */ /* synthetic */ ft n() {
        return super.n();
    }

    public final /* bridge */ /* synthetic */ fa o() {
        return super.o();
    }

    public final /* bridge */ /* synthetic */ bz p() {
        return super.p();
    }

    public final /* bridge */ /* synthetic */ az q() {
        return super.q();
    }

    public final /* bridge */ /* synthetic */ bk r() {
        return super.r();
    }

    public final /* bridge */ /* synthetic */ y s() {
        return super.s();
    }

    /* access modifiers changed from: protected */
    public final boolean t() {
        return false;
    }

    public final void v() {
        c();
        try {
            int delete = w().delete("messages", null, null) + 0;
            if (delete > 0) {
                q().C().a("Reset local analytics data. records", Integer.valueOf(delete));
            }
        } catch (SQLiteException e) {
            q().v().a("Error resetting local analytics data. error", e);
        }
    }
}
