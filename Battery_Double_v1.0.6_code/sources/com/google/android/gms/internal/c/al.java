package com.google.android.gms.internal.c;

import java.util.Iterator;

final class al implements Iterator<String> {

    /* renamed from: a reason: collision with root package name */
    private Iterator<String> f3862a = this.f3863b.f3861a.keySet().iterator();

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ ak f3863b;

    al(ak akVar) {
        this.f3863b = akVar;
    }

    public final boolean hasNext() {
        return this.f3862a.hasNext();
    }

    public final /* synthetic */ Object next() {
        return (String) this.f3862a.next();
    }

    public final void remove() {
        throw new UnsupportedOperationException("Remove not supported");
    }
}
