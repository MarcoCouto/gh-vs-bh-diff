package com.google.android.gms.internal.c;

import java.io.IOException;

public final class gc extends d<gc> {
    private static volatile gc[] g;
    public String c;
    public Boolean d;
    public Boolean e;
    public Integer f;

    public gc() {
        this.c = null;
        this.d = null;
        this.e = null;
        this.f = null;
        this.f3962a = null;
        this.f4112b = -1;
    }

    public static gc[] e() {
        if (g == null) {
            synchronized (h.f4106b) {
                if (g == null) {
                    g = new gc[0];
                }
            }
        }
        return g;
    }

    /* access modifiers changed from: protected */
    public final int a() {
        int a2 = super.a();
        if (this.c != null) {
            a2 += b.b(1, this.c);
        }
        if (this.d != null) {
            this.d.booleanValue();
            a2 += b.b(2) + 1;
        }
        if (this.e != null) {
            this.e.booleanValue();
            a2 += b.b(3) + 1;
        }
        return this.f != null ? a2 + b.b(4, this.f.intValue()) : a2;
    }

    public final /* synthetic */ j a(a aVar) throws IOException {
        while (true) {
            int a2 = aVar.a();
            switch (a2) {
                case 0:
                    break;
                case 10:
                    this.c = aVar.c();
                    continue;
                case 16:
                    this.d = Boolean.valueOf(aVar.b());
                    continue;
                case 24:
                    this.e = Boolean.valueOf(aVar.b());
                    continue;
                case 32:
                    this.f = Integer.valueOf(aVar.d());
                    continue;
                default:
                    if (!super.a(aVar, a2)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    public final void a(b bVar) throws IOException {
        if (this.c != null) {
            bVar.a(1, this.c);
        }
        if (this.d != null) {
            bVar.a(2, this.d.booleanValue());
        }
        if (this.e != null) {
            bVar.a(3, this.e.booleanValue());
        }
        if (this.f != null) {
            bVar.a(4, this.f.intValue());
        }
        super.a(bVar);
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof gc)) {
            return false;
        }
        gc gcVar = (gc) obj;
        if (this.c == null) {
            if (gcVar.c != null) {
                return false;
            }
        } else if (!this.c.equals(gcVar.c)) {
            return false;
        }
        if (this.d == null) {
            if (gcVar.d != null) {
                return false;
            }
        } else if (!this.d.equals(gcVar.d)) {
            return false;
        }
        if (this.e == null) {
            if (gcVar.e != null) {
                return false;
            }
        } else if (!this.e.equals(gcVar.e)) {
            return false;
        }
        if (this.f == null) {
            if (gcVar.f != null) {
                return false;
            }
        } else if (!this.f.equals(gcVar.f)) {
            return false;
        }
        return (this.f3962a == null || this.f3962a.b()) ? gcVar.f3962a == null || gcVar.f3962a.b() : this.f3962a.equals(gcVar.f3962a);
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = ((this.f == null ? 0 : this.f.hashCode()) + (((this.e == null ? 0 : this.e.hashCode()) + (((this.d == null ? 0 : this.d.hashCode()) + (((this.c == null ? 0 : this.c.hashCode()) + ((getClass().getName().hashCode() + 527) * 31)) * 31)) * 31)) * 31)) * 31;
        if (this.f3962a != null && !this.f3962a.b()) {
            i = this.f3962a.hashCode();
        }
        return hashCode + i;
    }
}
