package com.google.android.gms.internal.c;

import android.content.Context;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.h.a;
import android.text.TextUtils;
import com.google.android.gms.common.internal.aa;
import com.google.android.gms.common.util.e;
import com.google.android.gms.common.util.f;
import com.google.android.gms.d.g;
import com.google.android.gms.d.j;
import com.google.android.gms.measurement.AppMeasurement;
import com.google.android.gms.measurement.AppMeasurement.ConditionalUserProperty;
import com.google.android.gms.measurement.AppMeasurement.b;
import com.google.android.gms.measurement.AppMeasurement.c;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicReference;

public final class dd extends da {

    /* renamed from: a reason: collision with root package name */
    protected dw f3965a;

    /* renamed from: b reason: collision with root package name */
    protected boolean f3966b = true;
    private b c;
    private final Set<c> d = new CopyOnWriteArraySet();
    private boolean e;
    private final AtomicReference<String> f = new AtomicReference<>();

    protected dd(ce ceVar) {
        super(ceVar);
    }

    private final void a(String str, String str2, long j, Bundle bundle, boolean z, boolean z2, boolean z3, String str3) {
        Bundle bundle2;
        if (bundle == null) {
            bundle2 = new Bundle();
        } else {
            bundle2 = new Bundle(bundle);
            for (String str4 : bundle2.keySet()) {
                Object obj = bundle2.get(str4);
                if (obj instanceof Bundle) {
                    bundle2.putBundle(str4, new Bundle((Bundle) obj));
                } else if (obj instanceof Parcelable[]) {
                    Parcelable[] parcelableArr = (Parcelable[]) obj;
                    int i = 0;
                    while (true) {
                        int i2 = i;
                        if (i2 >= parcelableArr.length) {
                            break;
                        }
                        if (parcelableArr[i2] instanceof Bundle) {
                            parcelableArr[i2] = new Bundle((Bundle) parcelableArr[i2]);
                        }
                        i = i2 + 1;
                    }
                } else if (obj instanceof ArrayList) {
                    ArrayList arrayList = (ArrayList) obj;
                    int i3 = 0;
                    while (true) {
                        int i4 = i3;
                        if (i4 >= arrayList.size()) {
                            break;
                        }
                        Object obj2 = arrayList.get(i4);
                        if (obj2 instanceof Bundle) {
                            arrayList.set(i4, new Bundle((Bundle) obj2));
                        }
                        i3 = i4 + 1;
                    }
                }
            }
        }
        p().a((Runnable) new dv(this, str, str2, j, bundle2, z, z2, z3, str3));
    }

    private final void a(String str, String str2, long j, Object obj) {
        p().a((Runnable) new df(this, str, str2, obj, j));
    }

    private final void a(String str, String str2, Bundle bundle, boolean z, boolean z2, boolean z3, String str3) {
        a(str, str2, j().a(), bundle, true, z2, z3, null);
    }

    /* access modifiers changed from: private */
    public final void a(String str, String str2, Object obj, long j) {
        aa.a(str);
        aa.a(str2);
        c();
        F();
        if (!this.q.x()) {
            q().B().a("User property not set since app measurement is disabled");
        } else if (this.q.B()) {
            q().B().a("Setting user property (FE)", m().a(str2), obj);
            h().a(new fq(str2, j, obj, str));
        }
    }

    private final List<ConditionalUserProperty> b(String str, String str2, String str3) {
        if (p().w()) {
            q().v().a("Cannot get conditional user properties from analytics worker thread");
            return Collections.emptyList();
        }
        p();
        if (bz.v()) {
            q().v().a("Cannot get conditional user properties from main thread");
            return Collections.emptyList();
        }
        AtomicReference atomicReference = new AtomicReference();
        synchronized (atomicReference) {
            this.q.p().a((Runnable) new dm(this, atomicReference, str, str2, str3));
            try {
                atomicReference.wait(5000);
            } catch (InterruptedException e2) {
                q().y().a("Interrupted waiting for get conditional user properties", str, e2);
            }
        }
        List<w> list = (List) atomicReference.get();
        if (list == null) {
            q().y().a("Timed out waiting for get conditional user properties", str);
            return Collections.emptyList();
        }
        ArrayList arrayList = new ArrayList(list.size());
        for (w wVar : list) {
            ConditionalUserProperty conditionalUserProperty = new ConditionalUserProperty();
            conditionalUserProperty.mAppId = str;
            conditionalUserProperty.mOrigin = str2;
            conditionalUserProperty.mCreationTimestamp = wVar.d;
            conditionalUserProperty.mName = wVar.c.f4082a;
            conditionalUserProperty.mValue = wVar.c.a();
            conditionalUserProperty.mActive = wVar.e;
            conditionalUserProperty.mTriggerEventName = wVar.f;
            if (wVar.g != null) {
                conditionalUserProperty.mTimedOutEventName = wVar.g.f3864a;
                if (wVar.g.f3865b != null) {
                    conditionalUserProperty.mTimedOutEventParams = wVar.g.f3865b.b();
                }
            }
            conditionalUserProperty.mTriggerTimeout = wVar.h;
            if (wVar.i != null) {
                conditionalUserProperty.mTriggeredEventName = wVar.i.f3864a;
                if (wVar.i.f3865b != null) {
                    conditionalUserProperty.mTriggeredEventParams = wVar.i.f3865b.b();
                }
            }
            conditionalUserProperty.mTriggeredTimestamp = wVar.c.f4083b;
            conditionalUserProperty.mTimeToLive = wVar.j;
            if (wVar.k != null) {
                conditionalUserProperty.mExpiredEventName = wVar.k.f3864a;
                if (wVar.k.f3865b != null) {
                    conditionalUserProperty.mExpiredEventParams = wVar.k.f3865b.b();
                }
            }
            arrayList.add(conditionalUserProperty);
        }
        return arrayList;
    }

    private final Map<String, Object> b(String str, String str2, String str3, boolean z) {
        if (p().w()) {
            q().v().a("Cannot get user properties from analytics worker thread");
            return Collections.emptyMap();
        }
        p();
        if (bz.v()) {
            q().v().a("Cannot get user properties from main thread");
            return Collections.emptyMap();
        }
        AtomicReference atomicReference = new AtomicReference();
        synchronized (atomicReference) {
            this.q.p().a((Runnable) new dn(this, atomicReference, str, str2, str3, z));
            try {
                atomicReference.wait(5000);
            } catch (InterruptedException e2) {
                q().y().a("Interrupted waiting for get user properties", e2);
            }
        }
        List<fq> list = (List) atomicReference.get();
        if (list == null) {
            q().y().a("Timed out waiting for get user properties");
            return Collections.emptyMap();
        }
        a aVar = new a(list.size());
        for (fq fqVar : list) {
            aVar.put(fqVar.f4082a, fqVar.a());
        }
        return aVar;
    }

    /* access modifiers changed from: private */
    public final void b(String str, String str2, long j, Bundle bundle, boolean z, boolean z2, boolean z3, String str3) {
        int i;
        aa.a(str);
        aa.a(str2);
        aa.a(bundle);
        c();
        F();
        if (!this.q.x()) {
            q().B().a("Event not sent since app measurement is disabled");
            return;
        }
        if (!this.e) {
            this.e = true;
            try {
                try {
                    Class.forName("com.google.android.gms.tagmanager.TagManagerService").getDeclaredMethod("initialize", new Class[]{Context.class}).invoke(null, new Object[]{k()});
                } catch (Exception e2) {
                    q().y().a("Failed to invoke Tag Manager's initialize() method", e2);
                }
            } catch (ClassNotFoundException e3) {
                q().A().a("Tag Manager is not found and thus will not be used");
            }
        }
        if (z3 && !"_iap".equals(str2)) {
            ft m = this.q.m();
            int i2 = !m.a("event", str2) ? 2 : !m.a("event", AppMeasurement.a.f4138a, str2) ? 13 : !m.a("event", 40, str2) ? 2 : 0;
            if (i2 != 0) {
                q().x().a("Invalid public event name. Event will not be logged (FE)", m().a(str2));
                this.q.m();
                this.q.m().a(i2, "_ev", ft.a(str2, 40, true), str2 != null ? str2.length() : 0);
                return;
            }
        }
        dx v = i().v();
        if (v != null) {
            if (!bundle.containsKey("_sc")) {
                v.d = true;
            }
        }
        dy.a(v, bundle, z && z3);
        boolean equals = "am".equals(str);
        boolean h = ft.h(str2);
        if (z && this.c != null && !h && !equals) {
            q().B().a("Passing event to registered event handler (FE)", m().a(str2), m().a(bundle));
            this.c.a(str, str2, bundle, j);
        } else if (this.q.B()) {
            int b2 = n().b(str2);
            if (b2 != 0) {
                q().x().a("Invalid event name. Event will not be logged (FE)", m().a(str2));
                n();
                this.q.m().a(str3, b2, "_ev", ft.a(str2, 40, true), str2 != null ? str2.length() : 0);
                return;
            }
            List a2 = f.a((T[]) new String[]{"_o", "_sn", "_sc", "_si"});
            Bundle a3 = n().a(str2, bundle, a2, z3, true);
            dx dxVar = (a3 == null || !a3.containsKey("_sc") || !a3.containsKey("_si")) ? null : new dx(a3.getString("_sn"), a3.getString("_sc"), Long.valueOf(a3.getLong("_si")).longValue());
            dx dxVar2 = dxVar == null ? v : dxVar;
            ArrayList arrayList = new ArrayList();
            arrayList.add(a3);
            long nextLong = n().w().nextLong();
            int i3 = 0;
            String[] strArr = (String[]) a3.keySet().toArray(new String[bundle.size()]);
            Arrays.sort(strArr);
            int length = strArr.length;
            int i4 = 0;
            while (i4 < length) {
                String str4 = strArr[i4];
                Object obj = a3.get(str4);
                n();
                Bundle[] a4 = ft.a(obj);
                if (a4 != null) {
                    a3.putInt(str4, a4.length);
                    int i5 = 0;
                    while (true) {
                        int i6 = i5;
                        if (i6 >= a4.length) {
                            break;
                        }
                        Bundle bundle2 = a4[i6];
                        dy.a(dxVar2, bundle2, true);
                        Bundle a5 = n().a("_ep", bundle2, a2, z3, false);
                        a5.putString("_en", str2);
                        a5.putLong("_eid", nextLong);
                        a5.putString("_gn", str4);
                        a5.putInt("_ll", a4.length);
                        a5.putInt("_i", i6);
                        arrayList.add(a5);
                        i5 = i6 + 1;
                    }
                    i = a4.length + i3;
                } else {
                    i = i3;
                }
                i4++;
                i3 = i;
            }
            if (i3 != 0) {
                a3.putLong("_eid", nextLong);
                a3.putInt("_epc", i3);
            }
            int i7 = 0;
            while (true) {
                int i8 = i7;
                if (i8 >= arrayList.size()) {
                    break;
                }
                Bundle bundle3 = (Bundle) arrayList.get(i8);
                String str5 = i8 != 0 ? "_ep" : str2;
                bundle3.putString("_o", str);
                Bundle bundle4 = z2 ? n().a(bundle3) : bundle3;
                q().B().a("Logging event (FE)", m().a(str2), m().a(bundle4));
                h().a(new an(str5, new ak(bundle4), str, j), str3);
                if (!equals) {
                    for (c a6 : this.d) {
                        a6.a(str, str2, new Bundle(bundle4), j);
                    }
                }
                i7 = i8 + 1;
            }
            if (i().v() != null && "_ae".equals(str2)) {
                o().a(true);
            }
        }
    }

    private final void b(String str, String str2, String str3, Bundle bundle) {
        long a2 = j().a();
        aa.a(str2);
        ConditionalUserProperty conditionalUserProperty = new ConditionalUserProperty();
        conditionalUserProperty.mAppId = str;
        conditionalUserProperty.mName = str2;
        conditionalUserProperty.mCreationTimestamp = a2;
        if (str3 != null) {
            conditionalUserProperty.mExpiredEventName = str3;
            conditionalUserProperty.mExpiredEventParams = bundle;
        }
        p().a((Runnable) new dl(this, conditionalUserProperty));
    }

    private final void c(ConditionalUserProperty conditionalUserProperty) {
        long a2 = j().a();
        aa.a(conditionalUserProperty);
        aa.a(conditionalUserProperty.mName);
        aa.a(conditionalUserProperty.mOrigin);
        aa.a(conditionalUserProperty.mValue);
        conditionalUserProperty.mCreationTimestamp = a2;
        String str = conditionalUserProperty.mName;
        Object obj = conditionalUserProperty.mValue;
        if (n().d(str) != 0) {
            q().v().a("Invalid conditional user property name", m().c(str));
        } else if (n().b(str, obj) != 0) {
            q().v().a("Invalid conditional user property value", m().c(str), obj);
        } else {
            Object c2 = n().c(str, obj);
            if (c2 == null) {
                q().v().a("Unable to normalize conditional user property value", m().c(str), obj);
                return;
            }
            conditionalUserProperty.mValue = c2;
            long j = conditionalUserProperty.mTriggerTimeout;
            if (TextUtils.isEmpty(conditionalUserProperty.mTriggerEventName) || (j <= 15552000000L && j >= 1)) {
                long j2 = conditionalUserProperty.mTimeToLive;
                if (j2 > 15552000000L || j2 < 1) {
                    q().v().a("Invalid conditional user property time to live", m().c(str), Long.valueOf(j2));
                } else {
                    p().a((Runnable) new dk(this, conditionalUserProperty));
                }
            } else {
                q().v().a("Invalid conditional user property timeout", m().c(str), Long.valueOf(j));
            }
        }
    }

    /* access modifiers changed from: private */
    public final void c(boolean z) {
        c();
        F();
        q().B().a("Setting app measurement enabled (FE)", Boolean.valueOf(z));
        r().b(z);
        if (!s().i(f().w())) {
            h().w();
        } else if (!this.q.x() || !this.f3966b) {
            h().w();
        } else {
            q().B().a("Recording app launch after enabling measurement for the first time (FE)");
            D();
        }
    }

    /* access modifiers changed from: private */
    public final void d(ConditionalUserProperty conditionalUserProperty) {
        c();
        F();
        aa.a(conditionalUserProperty);
        aa.a(conditionalUserProperty.mName);
        aa.a(conditionalUserProperty.mOrigin);
        aa.a(conditionalUserProperty.mValue);
        if (!this.q.x()) {
            q().B().a("Conditional property not sent since Firebase Analytics is disabled");
            return;
        }
        fq fqVar = new fq(conditionalUserProperty.mName, conditionalUserProperty.mTriggeredTimestamp, conditionalUserProperty.mValue, conditionalUserProperty.mOrigin);
        try {
            an a2 = n().a(conditionalUserProperty.mTriggeredEventName, conditionalUserProperty.mTriggeredEventParams, conditionalUserProperty.mOrigin, 0, true, false);
            h().a(new w(conditionalUserProperty.mAppId, conditionalUserProperty.mOrigin, fqVar, conditionalUserProperty.mCreationTimestamp, false, conditionalUserProperty.mTriggerEventName, n().a(conditionalUserProperty.mTimedOutEventName, conditionalUserProperty.mTimedOutEventParams, conditionalUserProperty.mOrigin, 0, true, false), conditionalUserProperty.mTriggerTimeout, a2, conditionalUserProperty.mTimeToLive, n().a(conditionalUserProperty.mExpiredEventName, conditionalUserProperty.mExpiredEventParams, conditionalUserProperty.mOrigin, 0, true, false)));
        } catch (IllegalArgumentException e2) {
        }
    }

    /* access modifiers changed from: private */
    public final void e(ConditionalUserProperty conditionalUserProperty) {
        c();
        F();
        aa.a(conditionalUserProperty);
        aa.a(conditionalUserProperty.mName);
        if (!this.q.x()) {
            q().B().a("Conditional property not cleared since Firebase Analytics is disabled");
            return;
        }
        fq fqVar = new fq(conditionalUserProperty.mName, 0, null, null);
        try {
            h().a(new w(conditionalUserProperty.mAppId, conditionalUserProperty.mOrigin, fqVar, conditionalUserProperty.mCreationTimestamp, conditionalUserProperty.mActive, conditionalUserProperty.mTriggerEventName, null, conditionalUserProperty.mTriggerTimeout, null, conditionalUserProperty.mTimeToLive, n().a(conditionalUserProperty.mExpiredEventName, conditionalUserProperty.mExpiredEventParams, conditionalUserProperty.mOrigin, conditionalUserProperty.mCreationTimestamp, true, false)));
        } catch (IllegalArgumentException e2) {
        }
    }

    public final g<String> A() {
        try {
            String w = r().w();
            return w != null ? j.a(w) : j.a((Executor) p().x(), (Callable<TResult>) new dh<TResult>(this));
        } catch (Exception e2) {
            q().y().a("Failed to schedule task for getAppInstanceId");
            return j.a(e2);
        }
    }

    public final String B() {
        return (String) this.f.get();
    }

    public final void C() {
        p().a((Runnable) new dj(this, j().a()));
    }

    public final void D() {
        c();
        F();
        if (this.q.B()) {
            h().y();
            this.f3966b = false;
            String z = r().z();
            if (!TextUtils.isEmpty(z)) {
                g().F();
                if (!z.equals(VERSION.RELEASE)) {
                    Bundle bundle = new Bundle();
                    bundle.putString("_po", z);
                    a("auto", "_ou", bundle);
                }
            }
        }
    }

    public final List<ConditionalUserProperty> a(String str, String str2) {
        return b((String) null, str, str2);
    }

    public final List<ConditionalUserProperty> a(String str, String str2, String str3) {
        aa.a(str);
        a();
        return b(str, str2, str3);
    }

    public final Map<String, Object> a(String str, String str2, String str3, boolean z) {
        aa.a(str);
        a();
        return b(str, str2, str3, z);
    }

    public final Map<String, Object> a(String str, String str2, boolean z) {
        return b((String) null, str, str2, z);
    }

    public final /* bridge */ /* synthetic */ void a() {
        super.a();
    }

    public final void a(long j) {
        p().a((Runnable) new dt(this, j));
    }

    public final void a(ConditionalUserProperty conditionalUserProperty) {
        aa.a(conditionalUserProperty);
        ConditionalUserProperty conditionalUserProperty2 = new ConditionalUserProperty(conditionalUserProperty);
        if (!TextUtils.isEmpty(conditionalUserProperty2.mAppId)) {
            q().y().a("Package name should be null when calling setConditionalUserProperty");
        }
        conditionalUserProperty2.mAppId = null;
        c(conditionalUserProperty2);
    }

    public final void a(b bVar) {
        c();
        F();
        if (!(bVar == null || bVar == this.c)) {
            aa.a(this.c == null, (Object) "EventInterceptor already set.");
        }
        this.c = bVar;
    }

    public final void a(c cVar) {
        F();
        aa.a(cVar);
        if (!this.d.add(cVar)) {
            q().y().a("OnEventListener already registered");
        }
    }

    /* access modifiers changed from: 0000 */
    public final void a(String str) {
        this.f.set(str);
    }

    public final void a(String str, String str2, Bundle bundle) {
        a(str, str2, bundle, true, this.c == null || ft.h(str2), false, null);
    }

    public final void a(String str, String str2, Bundle bundle, long j) {
        a(str, str2, j, bundle, false, true, true, null);
    }

    public final void a(String str, String str2, Bundle bundle, boolean z) {
        a(str, str2, bundle, true, this.c == null || ft.h(str2), true, null);
    }

    public final void a(String str, String str2, Object obj) {
        int i = 0;
        aa.a(str);
        long a2 = j().a();
        int d2 = n().d(str2);
        if (d2 != 0) {
            n();
            String a3 = ft.a(str2, 24, true);
            if (str2 != null) {
                i = str2.length();
            }
            this.q.m().a(d2, "_ev", a3, i);
        } else if (obj != null) {
            int b2 = n().b(str2, obj);
            if (b2 != 0) {
                n();
                String a4 = ft.a(str2, 24, true);
                if ((obj instanceof String) || (obj instanceof CharSequence)) {
                    i = String.valueOf(obj).length();
                }
                this.q.m().a(b2, "_ev", a4, i);
                return;
            }
            Object c2 = n().c(str2, obj);
            if (c2 != null) {
                a(str, str2, a2, c2);
            }
        } else {
            a(str, str2, a2, (Object) null);
        }
    }

    public final void a(String str, String str2, String str3, Bundle bundle) {
        aa.a(str);
        a();
        b(str, str2, str3, bundle);
    }

    public final void a(boolean z) {
        F();
        p().a((Runnable) new ds(this, z));
    }

    public final List<fq> b(boolean z) {
        F();
        q().B().a("Fetching user attributes (FE)");
        if (p().w()) {
            q().v().a("Cannot get all user properties from analytics worker thread");
            return Collections.emptyList();
        }
        p();
        if (bz.v()) {
            q().v().a("Cannot get all user properties from main thread");
            return Collections.emptyList();
        }
        AtomicReference atomicReference = new AtomicReference();
        synchronized (atomicReference) {
            this.q.p().a((Runnable) new dg(this, atomicReference, z));
            try {
                atomicReference.wait(5000);
            } catch (InterruptedException e2) {
                q().y().a("Interrupted waiting for get user properties", e2);
            }
        }
        List<fq> list = (List) atomicReference.get();
        if (list != null) {
            return list;
        }
        q().y().a("Timed out waiting for get user properties");
        return Collections.emptyList();
    }

    public final /* bridge */ /* synthetic */ void b() {
        super.b();
    }

    public final void b(long j) {
        p().a((Runnable) new du(this, j));
    }

    public final void b(ConditionalUserProperty conditionalUserProperty) {
        aa.a(conditionalUserProperty);
        aa.a(conditionalUserProperty.mAppId);
        a();
        c(new ConditionalUserProperty(conditionalUserProperty));
    }

    public final void b(c cVar) {
        F();
        aa.a(cVar);
        if (!this.d.remove(cVar)) {
            q().y().a("OnEventListener had not been registered");
        }
    }

    /* access modifiers changed from: 0000 */
    public final void b(String str, String str2, Bundle bundle) {
        c();
        b(str, str2, j().a(), bundle, true, this.c == null || ft.h(str2), false, null);
    }

    /* access modifiers changed from: 0000 */
    public final String c(long j) {
        AtomicReference atomicReference = new AtomicReference();
        synchronized (atomicReference) {
            p().a((Runnable) new di(this, atomicReference));
            try {
                atomicReference.wait(j);
            } catch (InterruptedException e2) {
                q().y().a("Interrupted waiting for app instance id");
                return null;
            }
        }
        return (String) atomicReference.get();
    }

    public final /* bridge */ /* synthetic */ void c() {
        super.c();
    }

    public final void c(String str, String str2, Bundle bundle) {
        b((String) null, str, str2, bundle);
    }

    public final /* bridge */ /* synthetic */ n d() {
        return super.d();
    }

    public final /* bridge */ /* synthetic */ dd e() {
        return super.e();
    }

    public final /* bridge */ /* synthetic */ au f() {
        return super.f();
    }

    public final /* bridge */ /* synthetic */ ah g() {
        return super.g();
    }

    public final /* bridge */ /* synthetic */ eb h() {
        return super.h();
    }

    public final /* bridge */ /* synthetic */ dy i() {
        return super.i();
    }

    public final /* bridge */ /* synthetic */ e j() {
        return super.j();
    }

    public final /* bridge */ /* synthetic */ Context k() {
        return super.k();
    }

    public final /* bridge */ /* synthetic */ av l() {
        return super.l();
    }

    public final /* bridge */ /* synthetic */ ax m() {
        return super.m();
    }

    public final /* bridge */ /* synthetic */ ft n() {
        return super.n();
    }

    public final /* bridge */ /* synthetic */ fa o() {
        return super.o();
    }

    public final /* bridge */ /* synthetic */ bz p() {
        return super.p();
    }

    public final /* bridge */ /* synthetic */ az q() {
        return super.q();
    }

    public final /* bridge */ /* synthetic */ bk r() {
        return super.r();
    }

    public final /* bridge */ /* synthetic */ y s() {
        return super.s();
    }

    /* access modifiers changed from: protected */
    public final boolean t() {
        return false;
    }

    public final Boolean v() {
        AtomicReference atomicReference = new AtomicReference();
        return (Boolean) p().a(atomicReference, 15000, "boolean test flag value", new de(this, atomicReference));
    }

    public final String w() {
        AtomicReference atomicReference = new AtomicReference();
        return (String) p().a(atomicReference, 15000, "String test flag value", new Cdo(this, atomicReference));
    }

    public final Long x() {
        AtomicReference atomicReference = new AtomicReference();
        return (Long) p().a(atomicReference, 15000, "long test flag value", new dp(this, atomicReference));
    }

    public final Integer y() {
        AtomicReference atomicReference = new AtomicReference();
        return (Integer) p().a(atomicReference, 15000, "int test flag value", new dq(this, atomicReference));
    }

    public final Double z() {
        AtomicReference atomicReference = new AtomicReference();
        return (Double) p().a(atomicReference, 15000, "double test flag value", new dr(this, atomicReference));
    }
}
