package com.google.android.gms.internal.c;

import java.io.IOException;

public final class gg extends d<gg> {
    private static volatile gg[] h;
    public gh[] c;
    public String d;
    public Long e;
    public Long f;
    public Integer g;

    public gg() {
        this.c = gh.e();
        this.d = null;
        this.e = null;
        this.f = null;
        this.g = null;
        this.f3962a = null;
        this.f4112b = -1;
    }

    public static gg[] e() {
        if (h == null) {
            synchronized (h.f4106b) {
                if (h == null) {
                    h = new gg[0];
                }
            }
        }
        return h;
    }

    /* access modifiers changed from: protected */
    public final int a() {
        int a2 = super.a();
        if (this.c != null && this.c.length > 0) {
            for (gh ghVar : this.c) {
                if (ghVar != null) {
                    a2 += b.b(1, (j) ghVar);
                }
            }
        }
        if (this.d != null) {
            a2 += b.b(2, this.d);
        }
        if (this.e != null) {
            a2 += b.c(3, this.e.longValue());
        }
        if (this.f != null) {
            a2 += b.c(4, this.f.longValue());
        }
        return this.g != null ? a2 + b.b(5, this.g.intValue()) : a2;
    }

    public final /* synthetic */ j a(a aVar) throws IOException {
        while (true) {
            int a2 = aVar.a();
            switch (a2) {
                case 0:
                    break;
                case 10:
                    int a3 = m.a(aVar, 10);
                    int length = this.c == null ? 0 : this.c.length;
                    gh[] ghVarArr = new gh[(a3 + length)];
                    if (length != 0) {
                        System.arraycopy(this.c, 0, ghVarArr, 0, length);
                    }
                    while (length < ghVarArr.length - 1) {
                        ghVarArr[length] = new gh();
                        aVar.a((j) ghVarArr[length]);
                        aVar.a();
                        length++;
                    }
                    ghVarArr[length] = new gh();
                    aVar.a((j) ghVarArr[length]);
                    this.c = ghVarArr;
                    continue;
                case 18:
                    this.d = aVar.c();
                    continue;
                case 24:
                    this.e = Long.valueOf(aVar.e());
                    continue;
                case 32:
                    this.f = Long.valueOf(aVar.e());
                    continue;
                case 40:
                    this.g = Integer.valueOf(aVar.d());
                    continue;
                default:
                    if (!super.a(aVar, a2)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    public final void a(b bVar) throws IOException {
        if (this.c != null && this.c.length > 0) {
            for (gh ghVar : this.c) {
                if (ghVar != null) {
                    bVar.a(1, (j) ghVar);
                }
            }
        }
        if (this.d != null) {
            bVar.a(2, this.d);
        }
        if (this.e != null) {
            bVar.b(3, this.e.longValue());
        }
        if (this.f != null) {
            bVar.b(4, this.f.longValue());
        }
        if (this.g != null) {
            bVar.a(5, this.g.intValue());
        }
        super.a(bVar);
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof gg)) {
            return false;
        }
        gg ggVar = (gg) obj;
        if (!h.a((Object[]) this.c, (Object[]) ggVar.c)) {
            return false;
        }
        if (this.d == null) {
            if (ggVar.d != null) {
                return false;
            }
        } else if (!this.d.equals(ggVar.d)) {
            return false;
        }
        if (this.e == null) {
            if (ggVar.e != null) {
                return false;
            }
        } else if (!this.e.equals(ggVar.e)) {
            return false;
        }
        if (this.f == null) {
            if (ggVar.f != null) {
                return false;
            }
        } else if (!this.f.equals(ggVar.f)) {
            return false;
        }
        if (this.g == null) {
            if (ggVar.g != null) {
                return false;
            }
        } else if (!this.g.equals(ggVar.g)) {
            return false;
        }
        return (this.f3962a == null || this.f3962a.b()) ? ggVar.f3962a == null || ggVar.f3962a.b() : this.f3962a.equals(ggVar.f3962a);
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = ((this.g == null ? 0 : this.g.hashCode()) + (((this.f == null ? 0 : this.f.hashCode()) + (((this.e == null ? 0 : this.e.hashCode()) + (((this.d == null ? 0 : this.d.hashCode()) + ((((getClass().getName().hashCode() + 527) * 31) + h.a((Object[]) this.c)) * 31)) * 31)) * 31)) * 31)) * 31;
        if (this.f3962a != null && !this.f3962a.b()) {
            i = this.f3962a.hashCode();
        }
        return hashCode + i;
    }
}
