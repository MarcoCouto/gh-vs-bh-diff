package com.google.android.gms.internal.c;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.List;

public final class at extends gm implements ar {
    at(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.measurement.internal.IMeasurementService");
    }

    public final List<fq> a(s sVar, boolean z) throws RemoteException {
        Parcel a2 = a();
        go.a(a2, (Parcelable) sVar);
        go.a(a2, z);
        Parcel a3 = a(7, a2);
        ArrayList createTypedArrayList = a3.createTypedArrayList(fq.CREATOR);
        a3.recycle();
        return createTypedArrayList;
    }

    public final List<w> a(String str, String str2, s sVar) throws RemoteException {
        Parcel a2 = a();
        a2.writeString(str);
        a2.writeString(str2);
        go.a(a2, (Parcelable) sVar);
        Parcel a3 = a(16, a2);
        ArrayList createTypedArrayList = a3.createTypedArrayList(w.CREATOR);
        a3.recycle();
        return createTypedArrayList;
    }

    public final List<w> a(String str, String str2, String str3) throws RemoteException {
        Parcel a2 = a();
        a2.writeString(str);
        a2.writeString(str2);
        a2.writeString(str3);
        Parcel a3 = a(17, a2);
        ArrayList createTypedArrayList = a3.createTypedArrayList(w.CREATOR);
        a3.recycle();
        return createTypedArrayList;
    }

    public final List<fq> a(String str, String str2, String str3, boolean z) throws RemoteException {
        Parcel a2 = a();
        a2.writeString(str);
        a2.writeString(str2);
        a2.writeString(str3);
        go.a(a2, z);
        Parcel a3 = a(15, a2);
        ArrayList createTypedArrayList = a3.createTypedArrayList(fq.CREATOR);
        a3.recycle();
        return createTypedArrayList;
    }

    public final List<fq> a(String str, String str2, boolean z, s sVar) throws RemoteException {
        Parcel a2 = a();
        a2.writeString(str);
        a2.writeString(str2);
        go.a(a2, z);
        go.a(a2, (Parcelable) sVar);
        Parcel a3 = a(14, a2);
        ArrayList createTypedArrayList = a3.createTypedArrayList(fq.CREATOR);
        a3.recycle();
        return createTypedArrayList;
    }

    public final void a(long j, String str, String str2, String str3) throws RemoteException {
        Parcel a2 = a();
        a2.writeLong(j);
        a2.writeString(str);
        a2.writeString(str2);
        a2.writeString(str3);
        b(10, a2);
    }

    public final void a(an anVar, s sVar) throws RemoteException {
        Parcel a2 = a();
        go.a(a2, (Parcelable) anVar);
        go.a(a2, (Parcelable) sVar);
        b(1, a2);
    }

    public final void a(an anVar, String str, String str2) throws RemoteException {
        Parcel a2 = a();
        go.a(a2, (Parcelable) anVar);
        a2.writeString(str);
        a2.writeString(str2);
        b(5, a2);
    }

    public final void a(fq fqVar, s sVar) throws RemoteException {
        Parcel a2 = a();
        go.a(a2, (Parcelable) fqVar);
        go.a(a2, (Parcelable) sVar);
        b(2, a2);
    }

    public final void a(s sVar) throws RemoteException {
        Parcel a2 = a();
        go.a(a2, (Parcelable) sVar);
        b(4, a2);
    }

    public final void a(w wVar) throws RemoteException {
        Parcel a2 = a();
        go.a(a2, (Parcelable) wVar);
        b(13, a2);
    }

    public final void a(w wVar, s sVar) throws RemoteException {
        Parcel a2 = a();
        go.a(a2, (Parcelable) wVar);
        go.a(a2, (Parcelable) sVar);
        b(12, a2);
    }

    public final byte[] a(an anVar, String str) throws RemoteException {
        Parcel a2 = a();
        go.a(a2, (Parcelable) anVar);
        a2.writeString(str);
        Parcel a3 = a(9, a2);
        byte[] createByteArray = a3.createByteArray();
        a3.recycle();
        return createByteArray;
    }

    public final void b(s sVar) throws RemoteException {
        Parcel a2 = a();
        go.a(a2, (Parcelable) sVar);
        b(6, a2);
    }

    public final String c(s sVar) throws RemoteException {
        Parcel a2 = a();
        go.a(a2, (Parcelable) sVar);
        Parcel a3 = a(11, a2);
        String readString = a3.readString();
        a3.recycle();
        return readString;
    }

    public final void d(s sVar) throws RemoteException {
        Parcel a2 = a();
        go.a(a2, (Parcelable) sVar);
        b(18, a2);
    }
}
