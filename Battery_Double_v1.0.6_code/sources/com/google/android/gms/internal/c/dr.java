package com.google.android.gms.internal.c;

import java.util.concurrent.atomic.AtomicReference;

final class dr implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ AtomicReference f3992a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ dd f3993b;

    dr(dd ddVar, AtomicReference atomicReference) {
        this.f3993b = ddVar;
        this.f3992a = atomicReference;
    }

    public final void run() {
        synchronized (this.f3992a) {
            try {
                AtomicReference atomicReference = this.f3992a;
                y s = this.f3993b.s();
                atomicReference.set(Double.valueOf(s.c(s.f().w(), ap.M)));
                this.f3992a.notify();
            } catch (Throwable th) {
                this.f3992a.notify();
                throw th;
            }
        }
    }
}
