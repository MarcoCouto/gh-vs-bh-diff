package com.google.android.gms.internal.c;

import java.util.concurrent.atomic.AtomicReference;

final class dg implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ AtomicReference f3971a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ boolean f3972b;
    private final /* synthetic */ dd c;

    dg(dd ddVar, AtomicReference atomicReference, boolean z) {
        this.c = ddVar;
        this.f3971a = atomicReference;
        this.f3972b = z;
    }

    public final void run() {
        this.c.h().a(this.f3971a, this.f3972b);
    }
}
