package com.google.android.gms.internal.c;

import com.google.android.gms.common.internal.aa;

public final class aq<V> {

    /* renamed from: a reason: collision with root package name */
    private final gy<V> f3868a;

    /* renamed from: b reason: collision with root package name */
    private final String f3869b;

    private aq(String str, gy<V> gyVar) {
        aa.a(gyVar);
        this.f3868a = gyVar;
        this.f3869b = str;
    }

    static aq<Double> a(String str, double d, double d2) {
        return new aq<>(str, ap.V.a(str, -3.0d));
    }

    static aq<Integer> a(String str, int i, int i2) {
        return new aq<>(str, ap.V.a(str, i));
    }

    static aq<Long> a(String str, long j, long j2) {
        return new aq<>(str, ap.V.a(str, j));
    }

    static aq<String> a(String str, String str2, String str3) {
        return new aq<>(str, ap.V.a(str, str2));
    }

    static aq<Boolean> a(String str, boolean z, boolean z2) {
        return new aq<>(str, ap.V.a(str, z));
    }

    public final V a(V v) {
        return v != null ? v : this.f3868a.a();
    }

    public final String a() {
        return this.f3869b;
    }

    public final V b() {
        return this.f3868a.a();
    }
}
