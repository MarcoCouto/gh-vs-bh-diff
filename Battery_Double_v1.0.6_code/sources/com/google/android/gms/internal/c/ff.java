package com.google.android.gms.internal.c;

import com.google.android.gms.common.internal.aa;
import com.google.android.gms.common.util.e;

final class ff {

    /* renamed from: a reason: collision with root package name */
    private final e f4066a;

    /* renamed from: b reason: collision with root package name */
    private long f4067b;

    public ff(e eVar) {
        aa.a(eVar);
        this.f4066a = eVar;
    }

    public final void a() {
        this.f4067b = this.f4066a.b();
    }

    public final boolean a(long j) {
        return this.f4067b == 0 || this.f4066a.b() - this.f4067b >= 3600000;
    }

    public final void b() {
        this.f4067b = 0;
    }
}
