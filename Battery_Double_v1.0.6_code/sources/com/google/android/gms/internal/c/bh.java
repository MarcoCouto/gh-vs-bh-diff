package com.google.android.gms.internal.c;

import com.google.android.gms.common.internal.aa;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;
import java.util.Map.Entry;

final class bh implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final URL f3888a;

    /* renamed from: b reason: collision with root package name */
    private final byte[] f3889b;
    private final bf c;
    private final String d;
    private final Map<String, String> e;
    private final /* synthetic */ bd f;

    public bh(bd bdVar, String str, URL url, byte[] bArr, Map<String, String> map, bf bfVar) {
        this.f = bdVar;
        aa.a(str);
        aa.a(url);
        aa.a(bfVar);
        this.f3888a = url;
        this.f3889b = bArr;
        this.c = bfVar;
        this.d = str;
        this.e = map;
    }

    /* JADX WARNING: Removed duplicated region for block: B:36:0x00e3 A[SYNTHETIC, Splitter:B:36:0x00e3] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00e8  */
    public final void run() {
        Map map;
        int i;
        OutputStream outputStream;
        HttpURLConnection httpURLConnection;
        Throwable th;
        Map map2;
        HttpURLConnection httpURLConnection2;
        this.f.b();
        int i2 = 0;
        try {
            httpURLConnection2 = this.f.a(this.f3888a);
            try {
                if (this.e != null) {
                    for (Entry entry : this.e.entrySet()) {
                        httpURLConnection2.addRequestProperty((String) entry.getKey(), (String) entry.getValue());
                    }
                }
                if (this.f3889b != null) {
                    byte[] a2 = this.f.n().a(this.f3889b);
                    this.f.q().C().a("Uploading data. size", Integer.valueOf(a2.length));
                    httpURLConnection2.setDoOutput(true);
                    httpURLConnection2.addRequestProperty("Content-Encoding", "gzip");
                    httpURLConnection2.setFixedLengthStreamingMode(a2.length);
                    httpURLConnection2.connect();
                    outputStream = httpURLConnection2.getOutputStream();
                    try {
                        outputStream.write(a2);
                        outputStream.close();
                    } catch (IOException e2) {
                        e = e2;
                        map = null;
                        i = 0;
                        httpURLConnection = httpURLConnection2;
                        if (outputStream != null) {
                            try {
                                outputStream.close();
                            } catch (IOException e3) {
                                this.f.q().v().a("Error closing HTTP compressed POST connection output stream. appId", az.a(this.d), e3);
                            }
                        }
                        if (httpURLConnection != null) {
                            httpURLConnection.disconnect();
                        }
                        this.f.p().a((Runnable) new bg(this.d, this.c, i, e, null, map));
                        return;
                    } catch (Throwable th2) {
                        th = th2;
                        map2 = null;
                        if (outputStream != null) {
                        }
                        if (httpURLConnection2 != null) {
                        }
                        this.f.p().a((Runnable) new bg(this.d, this.c, i2, null, null, map2));
                        throw th;
                    }
                }
                i2 = httpURLConnection2.getResponseCode();
                map2 = httpURLConnection2.getHeaderFields();
            } catch (IOException e4) {
                e = e4;
                map = null;
                i = i2;
                outputStream = null;
                httpURLConnection = httpURLConnection2;
            } catch (Throwable th3) {
                th = th3;
                map2 = null;
                outputStream = null;
                if (outputStream != null) {
                }
                if (httpURLConnection2 != null) {
                }
                this.f.p().a((Runnable) new bg(this.d, this.c, i2, null, null, map2));
                throw th;
            }
            try {
                byte[] a3 = bd.a(httpURLConnection2);
                if (httpURLConnection2 != null) {
                    httpURLConnection2.disconnect();
                }
                this.f.p().a((Runnable) new bg(this.d, this.c, i2, null, a3, map2));
            } catch (IOException e5) {
                e = e5;
                map = map2;
                i = i2;
                outputStream = null;
                httpURLConnection = httpURLConnection2;
            } catch (Throwable th4) {
                th = th4;
                outputStream = null;
                if (outputStream != null) {
                }
                if (httpURLConnection2 != null) {
                }
                this.f.p().a((Runnable) new bg(this.d, this.c, i2, null, null, map2));
                throw th;
            }
        } catch (IOException e6) {
            e = e6;
            map = null;
            i = 0;
            outputStream = null;
            httpURLConnection = null;
        } catch (Throwable th5) {
            th = th5;
            map2 = null;
            httpURLConnection2 = null;
            outputStream = null;
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e7) {
                    this.f.q().v().a("Error closing HTTP compressed POST connection output stream. appId", az.a(this.d), e7);
                }
            }
            if (httpURLConnection2 != null) {
                httpURLConnection2.disconnect();
            }
            this.f.p().a((Runnable) new bg(this.d, this.c, i2, null, null, map2));
            throw th;
        }
    }
}
