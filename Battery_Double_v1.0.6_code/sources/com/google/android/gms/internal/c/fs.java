package com.google.android.gms.internal.c;

import com.google.android.gms.common.internal.aa;

final class fs {

    /* renamed from: a reason: collision with root package name */
    final String f4084a;

    /* renamed from: b reason: collision with root package name */
    final String f4085b;
    final String c;
    final long d;
    final Object e;

    fs(String str, String str2, String str3, long j, Object obj) {
        aa.a(str);
        aa.a(str3);
        aa.a(obj);
        this.f4084a = str;
        this.f4085b = str2;
        this.c = str3;
        this.d = j;
        this.e = obj;
    }
}
