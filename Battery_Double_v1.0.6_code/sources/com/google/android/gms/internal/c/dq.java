package com.google.android.gms.internal.c;

import java.util.concurrent.atomic.AtomicReference;

final class dq implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ AtomicReference f3990a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ dd f3991b;

    dq(dd ddVar, AtomicReference atomicReference) {
        this.f3991b = ddVar;
        this.f3990a = atomicReference;
    }

    public final void run() {
        synchronized (this.f3990a) {
            try {
                AtomicReference atomicReference = this.f3990a;
                y s = this.f3991b.s();
                atomicReference.set(Integer.valueOf(s.b(s.f().w(), ap.L)));
                this.f3990a.notify();
            } catch (Throwable th) {
                this.f3990a.notify();
                throw th;
            }
        }
    }
}
