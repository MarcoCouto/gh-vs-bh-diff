package com.google.android.gms.internal.c;

import android.content.Context;
import android.os.Looper;
import com.google.android.gms.common.internal.aa;
import com.google.android.gms.common.util.e;
import java.lang.Thread.UncaughtExceptionHandler;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

public final class bz extends da {
    /* access modifiers changed from: private */
    public static final AtomicLong k = new AtomicLong(Long.MIN_VALUE);

    /* renamed from: a reason: collision with root package name */
    private ExecutorService f3914a;
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public cd f3915b;
    /* access modifiers changed from: private */
    public cd c;
    private final PriorityBlockingQueue<cc<?>> d = new PriorityBlockingQueue<>();
    private final BlockingQueue<cc<?>> e = new LinkedBlockingQueue();
    private final UncaughtExceptionHandler f = new cb(this, "Thread death: Uncaught exception on worker thread");
    private final UncaughtExceptionHandler g = new cb(this, "Thread death: Uncaught exception on network thread");
    /* access modifiers changed from: private */
    public final Object h = new Object();
    /* access modifiers changed from: private */
    public final Semaphore i = new Semaphore(2);
    /* access modifiers changed from: private */
    public volatile boolean j;

    bz(ce ceVar) {
        super(ceVar);
    }

    private final void a(cc<?> ccVar) {
        synchronized (this.h) {
            this.d.add(ccVar);
            if (this.f3915b == null) {
                this.f3915b = new cd(this, "Measurement Worker", this.d);
                this.f3915b.setUncaughtExceptionHandler(this.f);
                this.f3915b.start();
            } else {
                this.f3915b.a();
            }
        }
    }

    public static boolean v() {
        return Looper.myLooper() == Looper.getMainLooper();
    }

    /* access modifiers changed from: 0000 */
    public final <T> T a(AtomicReference<T> atomicReference, long j2, String str, Runnable runnable) {
        synchronized (atomicReference) {
            p().a(runnable);
            try {
                atomicReference.wait(15000);
            } catch (InterruptedException e2) {
                bb y = q().y();
                String str2 = "Interrupted waiting for ";
                String valueOf = String.valueOf(str);
                y.a(valueOf.length() != 0 ? str2.concat(valueOf) : new String(str2));
                return null;
            }
        }
        Object obj = atomicReference.get();
        if (obj == null) {
            bb y2 = q().y();
            String str3 = "Timed out waiting for ";
            String valueOf2 = String.valueOf(str);
            y2.a(valueOf2.length() != 0 ? str3.concat(valueOf2) : new String(str3));
        }
        return obj;
    }

    public final <V> Future<V> a(Callable<V> callable) throws IllegalStateException {
        F();
        aa.a(callable);
        cc ccVar = new cc(this, callable, false, "Task exception on worker thread");
        if (Thread.currentThread() == this.f3915b) {
            if (!this.d.isEmpty()) {
                q().y().a("Callable skipped the worker queue.");
            }
            ccVar.run();
        } else {
            a(ccVar);
        }
        return ccVar;
    }

    public final /* bridge */ /* synthetic */ void a() {
        super.a();
    }

    public final void a(Runnable runnable) throws IllegalStateException {
        F();
        aa.a(runnable);
        a(new cc<>(this, runnable, false, "Task exception on worker thread"));
    }

    public final <V> Future<V> b(Callable<V> callable) throws IllegalStateException {
        F();
        aa.a(callable);
        cc ccVar = new cc(this, callable, true, "Task exception on worker thread");
        if (Thread.currentThread() == this.f3915b) {
            ccVar.run();
        } else {
            a(ccVar);
        }
        return ccVar;
    }

    public final void b() {
        if (Thread.currentThread() != this.c) {
            throw new IllegalStateException("Call expected from network thread");
        }
    }

    public final void b(Runnable runnable) throws IllegalStateException {
        F();
        aa.a(runnable);
        cc ccVar = new cc(this, runnable, false, "Task exception on network thread");
        synchronized (this.h) {
            this.e.add(ccVar);
            if (this.c == null) {
                this.c = new cd(this, "Measurement Network", this.e);
                this.c.setUncaughtExceptionHandler(this.g);
                this.c.start();
            } else {
                this.c.a();
            }
        }
    }

    public final void c() {
        if (Thread.currentThread() != this.f3915b) {
            throw new IllegalStateException("Call expected from worker thread");
        }
    }

    public final /* bridge */ /* synthetic */ n d() {
        return super.d();
    }

    public final /* bridge */ /* synthetic */ dd e() {
        return super.e();
    }

    public final /* bridge */ /* synthetic */ au f() {
        return super.f();
    }

    public final /* bridge */ /* synthetic */ ah g() {
        return super.g();
    }

    public final /* bridge */ /* synthetic */ eb h() {
        return super.h();
    }

    public final /* bridge */ /* synthetic */ dy i() {
        return super.i();
    }

    public final /* bridge */ /* synthetic */ e j() {
        return super.j();
    }

    public final /* bridge */ /* synthetic */ Context k() {
        return super.k();
    }

    public final /* bridge */ /* synthetic */ av l() {
        return super.l();
    }

    public final /* bridge */ /* synthetic */ ax m() {
        return super.m();
    }

    public final /* bridge */ /* synthetic */ ft n() {
        return super.n();
    }

    public final /* bridge */ /* synthetic */ fa o() {
        return super.o();
    }

    public final /* bridge */ /* synthetic */ bz p() {
        return super.p();
    }

    public final /* bridge */ /* synthetic */ az q() {
        return super.q();
    }

    public final /* bridge */ /* synthetic */ bk r() {
        return super.r();
    }

    public final /* bridge */ /* synthetic */ y s() {
        return super.s();
    }

    /* access modifiers changed from: protected */
    public final boolean t() {
        return false;
    }

    public final boolean w() {
        return Thread.currentThread() == this.f3915b;
    }

    /* access modifiers changed from: 0000 */
    public final ExecutorService x() {
        ExecutorService executorService;
        synchronized (this.h) {
            if (this.f3914a == null) {
                this.f3914a = new ThreadPoolExecutor(0, 1, 30, TimeUnit.SECONDS, new ArrayBlockingQueue(100));
            }
            executorService = this.f3914a;
        }
        return executorService;
    }
}
