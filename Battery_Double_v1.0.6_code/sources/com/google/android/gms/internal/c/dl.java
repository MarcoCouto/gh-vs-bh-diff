package com.google.android.gms.internal.c;

import com.google.android.gms.measurement.AppMeasurement.ConditionalUserProperty;

final class dl implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ ConditionalUserProperty f3980a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ dd f3981b;

    dl(dd ddVar, ConditionalUserProperty conditionalUserProperty) {
        this.f3981b = ddVar;
        this.f3980a = conditionalUserProperty;
    }

    public final void run() {
        this.f3981b.e(this.f3980a);
    }
}
