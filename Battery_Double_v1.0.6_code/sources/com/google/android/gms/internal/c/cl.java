package com.google.android.gms.internal.c;

final class cl implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ w f3934a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ cg f3935b;

    cl(cg cgVar, w wVar) {
        this.f3935b = cgVar;
        this.f3934a = wVar;
    }

    public final void run() {
        this.f3935b.f3924a.H();
        fk a2 = this.f3935b.f3924a;
        w wVar = this.f3934a;
        s a3 = a2.a(wVar.f4129a);
        if (a3 != null) {
            a2.a(wVar, a3);
        }
    }
}
