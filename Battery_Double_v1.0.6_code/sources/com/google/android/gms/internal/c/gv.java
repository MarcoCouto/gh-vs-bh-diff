package com.google.android.gms.internal.c;

import android.content.ContentResolver;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.util.Log;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public final class gv {

    /* renamed from: a reason: collision with root package name */
    private static final ConcurrentHashMap<Uri, gv> f4098a = new ConcurrentHashMap<>();
    private static final String[] i = {"key", "value"};

    /* renamed from: b reason: collision with root package name */
    private final ContentResolver f4099b;
    private final Uri c;
    private final ContentObserver d;
    private final Object e = new Object();
    private volatile Map<String, String> f;
    private final Object g = new Object();
    private final List<gx> h = new ArrayList();

    private gv(ContentResolver contentResolver, Uri uri) {
        this.f4099b = contentResolver;
        this.c = uri;
        this.d = new gw(this, null);
    }

    public static gv a(ContentResolver contentResolver, Uri uri) {
        gv gvVar = (gv) f4098a.get(uri);
        if (gvVar != null) {
            return gvVar;
        }
        gv gvVar2 = new gv(contentResolver, uri);
        gv gvVar3 = (gv) f4098a.putIfAbsent(uri, gvVar2);
        if (gvVar3 != null) {
            return gvVar3;
        }
        gvVar2.f4099b.registerContentObserver(gvVar2.c, false, gvVar2.d);
        return gvVar2;
    }

    private final Map<String, String> c() {
        Cursor query;
        try {
            HashMap hashMap = new HashMap();
            query = this.f4099b.query(this.c, i, null, null, null);
            if (query != null) {
                while (query.moveToNext()) {
                    hashMap.put(query.getString(0), query.getString(1));
                }
                query.close();
            }
            return hashMap;
        } catch (SQLiteException | SecurityException e2) {
            Log.e("ConfigurationContentLoader", "PhenotypeFlag unable to load ContentProvider, using default values");
            return null;
        } catch (Throwable th) {
            query.close();
            throw th;
        }
    }

    /* access modifiers changed from: private */
    public final void d() {
        synchronized (this.g) {
            for (gx a2 : this.h) {
                a2.a();
            }
        }
    }

    public final Map<String, String> a() {
        Map<String, String> c2 = gy.a("gms:phenotype:phenotype_flag:debug_disable_caching", false) ? c() : this.f;
        if (c2 == null) {
            synchronized (this.e) {
                c2 = this.f;
                if (c2 == null) {
                    c2 = c();
                    this.f = c2;
                }
            }
        }
        return c2 != null ? c2 : Collections.emptyMap();
    }

    public final void b() {
        synchronized (this.e) {
            this.f = null;
        }
    }
}
