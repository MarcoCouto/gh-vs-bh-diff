package com.google.android.gms.internal.c;

final class en implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ boolean f4035a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ fq f4036b;
    private final /* synthetic */ s c;
    private final /* synthetic */ eb d;

    en(eb ebVar, boolean z, fq fqVar, s sVar) {
        this.d = ebVar;
        this.f4035a = z;
        this.f4036b = fqVar;
        this.c = sVar;
    }

    public final void run() {
        ar d2 = this.d.f4014b;
        if (d2 == null) {
            this.d.q().v().a("Discarding data. Failed to set user attribute");
            return;
        }
        this.d.a(d2, this.f4035a ? null : this.f4036b, this.c);
        this.d.C();
    }
}
