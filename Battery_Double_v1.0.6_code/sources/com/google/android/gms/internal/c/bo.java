package com.google.android.gms.internal.c;

import android.content.SharedPreferences.Editor;
import android.util.Pair;
import com.google.android.gms.common.internal.aa;

public final class bo {

    /* renamed from: a reason: collision with root package name */
    private final String f3900a;

    /* renamed from: b reason: collision with root package name */
    private final String f3901b;
    private final String c;
    private final long d;
    private final /* synthetic */ bk e;

    private bo(bk bkVar, String str, long j) {
        this.e = bkVar;
        aa.a(str);
        aa.b(j > 0);
        this.f3900a = String.valueOf(str).concat(":start");
        this.f3901b = String.valueOf(str).concat(":count");
        this.c = String.valueOf(str).concat(":value");
        this.d = j;
    }

    private final void b() {
        this.e.c();
        long a2 = this.e.j().a();
        Editor edit = this.e.C().edit();
        edit.remove(this.f3901b);
        edit.remove(this.c);
        edit.putLong(this.f3900a, a2);
        edit.apply();
    }

    private final long c() {
        return this.e.C().getLong(this.f3900a, 0);
    }

    public final Pair<String, Long> a() {
        long abs;
        this.e.c();
        this.e.c();
        long c2 = c();
        if (c2 == 0) {
            b();
            abs = 0;
        } else {
            abs = Math.abs(c2 - this.e.j().a());
        }
        if (abs < this.d) {
            return null;
        }
        if (abs > (this.d << 1)) {
            b();
            return null;
        }
        String string = this.e.C().getString(this.c, null);
        long j = this.e.C().getLong(this.f3901b, 0);
        b();
        return (string == null || j <= 0) ? bk.f3894a : new Pair<>(string, Long.valueOf(j));
    }

    public final void a(String str, long j) {
        this.e.c();
        if (c() == 0) {
            b();
        }
        if (str == null) {
            str = "";
        }
        long j2 = this.e.C().getLong(this.f3901b, 0);
        if (j2 <= 0) {
            Editor edit = this.e.C().edit();
            edit.putString(this.c, str);
            edit.putLong(this.f3901b, 1);
            edit.apply();
            return;
        }
        boolean z = (this.e.n().w().nextLong() & Long.MAX_VALUE) < Long.MAX_VALUE / (j2 + 1);
        Editor edit2 = this.e.C().edit();
        if (z) {
            edit2.putString(this.c, str);
        }
        edit2.putLong(this.f3901b, j2 + 1);
        edit2.apply();
    }
}
