package com.google.android.gms.internal.c;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.a.a;
import com.google.android.gms.common.internal.a.c;
import java.util.Iterator;

public final class ak extends a implements Iterable<String> {
    public static final Creator<ak> CREATOR = new am();
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public final Bundle f3861a;

    ak(Bundle bundle) {
        this.f3861a = bundle;
    }

    public final int a() {
        return this.f3861a.size();
    }

    /* access modifiers changed from: 0000 */
    public final Object a(String str) {
        return this.f3861a.get(str);
    }

    public final Bundle b() {
        return new Bundle(this.f3861a);
    }

    /* access modifiers changed from: 0000 */
    public final Long b(String str) {
        return Long.valueOf(this.f3861a.getLong(str));
    }

    /* access modifiers changed from: 0000 */
    public final Double c(String str) {
        return Double.valueOf(this.f3861a.getDouble(str));
    }

    /* access modifiers changed from: 0000 */
    public final String d(String str) {
        return this.f3861a.getString(str);
    }

    public final Iterator<String> iterator() {
        return new al(this);
    }

    public final String toString() {
        return this.f3861a.toString();
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = c.a(parcel);
        c.a(parcel, 2, b(), false);
        c.a(parcel, a2);
    }
}
