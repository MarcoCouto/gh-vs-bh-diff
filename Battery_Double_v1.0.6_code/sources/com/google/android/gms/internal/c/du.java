package com.google.android.gms.internal.c;

final class du implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ long f3998a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ dd f3999b;

    du(dd ddVar, long j) {
        this.f3999b = ddVar;
        this.f3998a = j;
    }

    public final void run() {
        this.f3999b.r().l.a(this.f3998a);
        this.f3999b.q().B().a("Session timeout duration set", Long.valueOf(this.f3998a));
    }
}
