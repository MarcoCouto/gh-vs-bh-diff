package com.google.android.gms.internal.c;

import android.content.Context;
import com.google.android.gms.common.internal.aa;

public final class dc extends fp {

    /* renamed from: a reason: collision with root package name */
    final Context f3964a;

    public dc(Context context) {
        aa.a(context);
        Context applicationContext = context.getApplicationContext();
        aa.a(applicationContext);
        this.f3964a = applicationContext;
    }
}
