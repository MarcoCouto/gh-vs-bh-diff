package com.google.android.gms.internal.c;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;

final class bs implements ServiceConnection {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ bq f3906a;

    private bs(bq bqVar) {
        this.f3906a = bqVar;
    }

    public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        if (iBinder == null) {
            this.f3906a.f3905b.q().y().a("Install Referrer connection returned with null binder");
            return;
        }
        try {
            this.f3906a.f3904a = gr.a(iBinder);
            if (this.f3906a.f3904a == null) {
                this.f3906a.f3905b.q().y().a("Install Referrer Service implementation was not found");
                return;
            }
            this.f3906a.f3905b.q().A().a("Install Referrer Service connected");
            this.f3906a.f3905b.p().a((Runnable) new bt(this));
        } catch (Exception e) {
            this.f3906a.f3905b.q().y().a("Exception occurred while calling Install Referrer API", e);
        }
    }

    public final void onServiceDisconnected(ComponentName componentName) {
        this.f3906a.f3904a = null;
        this.f3906a.f3905b.q().A().a("Install Referrer Service disconnected");
    }
}
