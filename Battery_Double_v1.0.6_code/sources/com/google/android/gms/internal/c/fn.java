package com.google.android.gms.internal.c;

import java.util.concurrent.Callable;

final class fn implements Callable<String> {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ s f4078a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ fk f4079b;

    fn(fk fkVar, s sVar) {
        this.f4079b = fkVar;
        this.f4078a = sVar;
    }

    public final /* synthetic */ Object call() throws Exception {
        r b2 = this.f4079b.b().i(this.f4078a.f4127a) ? this.f4079b.e(this.f4078a) : this.f4079b.D().b(this.f4078a.f4127a);
        if (b2 != null) {
            return b2.c();
        }
        this.f4079b.q().y().a("App info was null when attempting to get app instance id");
        return null;
    }
}
