package com.google.android.gms.internal.c;

import java.io.IOException;

public final class gb extends d<gb> {
    public Integer c;
    public String d;
    public Boolean e;
    public String[] f;

    public gb() {
        this.c = null;
        this.d = null;
        this.e = null;
        this.f = m.c;
        this.f3962a = null;
        this.f4112b = -1;
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public final gb a(a aVar) throws IOException {
        while (true) {
            int a2 = aVar.a();
            switch (a2) {
                case 0:
                    break;
                case 8:
                    int i = aVar.i();
                    try {
                        int d2 = aVar.d();
                        if (d2 < 0 || d2 > 6) {
                            throw new IllegalArgumentException(d2 + " is not a valid enum MatchType");
                        }
                        this.c = Integer.valueOf(d2);
                        continue;
                    } catch (IllegalArgumentException e2) {
                        aVar.e(i);
                        a(aVar, a2);
                        break;
                    }
                case 18:
                    this.d = aVar.c();
                    continue;
                case 24:
                    this.e = Boolean.valueOf(aVar.b());
                    continue;
                case 34:
                    int a3 = m.a(aVar, 34);
                    int length = this.f == null ? 0 : this.f.length;
                    String[] strArr = new String[(a3 + length)];
                    if (length != 0) {
                        System.arraycopy(this.f, 0, strArr, 0, length);
                    }
                    while (length < strArr.length - 1) {
                        strArr[length] = aVar.c();
                        aVar.a();
                        length++;
                    }
                    strArr[length] = aVar.c();
                    this.f = strArr;
                    continue;
                default:
                    if (!super.a(aVar, a2)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    /* access modifiers changed from: protected */
    public final int a() {
        int a2 = super.a();
        if (this.c != null) {
            a2 += b.b(1, this.c.intValue());
        }
        if (this.d != null) {
            a2 += b.b(2, this.d);
        }
        if (this.e != null) {
            this.e.booleanValue();
            a2 += b.b(3) + 1;
        }
        if (this.f == null || this.f.length <= 0) {
            return a2;
        }
        int i = 0;
        int i2 = 0;
        for (String str : this.f) {
            if (str != null) {
                i2++;
                i += b.a(str);
            }
        }
        return a2 + i + (i2 * 1);
    }

    public final void a(b bVar) throws IOException {
        if (this.c != null) {
            bVar.a(1, this.c.intValue());
        }
        if (this.d != null) {
            bVar.a(2, this.d);
        }
        if (this.e != null) {
            bVar.a(3, this.e.booleanValue());
        }
        if (this.f != null && this.f.length > 0) {
            for (String str : this.f) {
                if (str != null) {
                    bVar.a(4, str);
                }
            }
        }
        super.a(bVar);
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof gb)) {
            return false;
        }
        gb gbVar = (gb) obj;
        if (this.c == null) {
            if (gbVar.c != null) {
                return false;
            }
        } else if (!this.c.equals(gbVar.c)) {
            return false;
        }
        if (this.d == null) {
            if (gbVar.d != null) {
                return false;
            }
        } else if (!this.d.equals(gbVar.d)) {
            return false;
        }
        if (this.e == null) {
            if (gbVar.e != null) {
                return false;
            }
        } else if (!this.e.equals(gbVar.e)) {
            return false;
        }
        if (!h.a((Object[]) this.f, (Object[]) gbVar.f)) {
            return false;
        }
        return (this.f3962a == null || this.f3962a.b()) ? gbVar.f3962a == null || gbVar.f3962a.b() : this.f3962a.equals(gbVar.f3962a);
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = ((((this.e == null ? 0 : this.e.hashCode()) + (((this.d == null ? 0 : this.d.hashCode()) + (((this.c == null ? 0 : this.c.intValue()) + ((getClass().getName().hashCode() + 527) * 31)) * 31)) * 31)) * 31) + h.a((Object[]) this.f)) * 31;
        if (this.f3962a != null && !this.f3962a.b()) {
            i = this.f3962a.hashCode();
        }
        return hashCode + i;
    }
}
