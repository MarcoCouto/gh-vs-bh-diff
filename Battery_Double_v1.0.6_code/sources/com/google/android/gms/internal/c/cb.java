package com.google.android.gms.internal.c;

import com.google.android.gms.common.internal.aa;
import java.lang.Thread.UncaughtExceptionHandler;

final class cb implements UncaughtExceptionHandler {

    /* renamed from: a reason: collision with root package name */
    private final String f3916a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ bz f3917b;

    public cb(bz bzVar, String str) {
        this.f3917b = bzVar;
        aa.a(str);
        this.f3916a = str;
    }

    public final synchronized void uncaughtException(Thread thread, Throwable th) {
        this.f3917b.q().v().a(this.f3916a, th);
    }
}
