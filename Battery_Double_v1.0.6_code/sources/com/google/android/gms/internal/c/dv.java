package com.google.android.gms.internal.c;

import android.os.Bundle;

final class dv implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ String f4000a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ String f4001b;
    private final /* synthetic */ long c;
    private final /* synthetic */ Bundle d;
    private final /* synthetic */ boolean e;
    private final /* synthetic */ boolean f;
    private final /* synthetic */ boolean g;
    private final /* synthetic */ String h;
    private final /* synthetic */ dd i;

    dv(dd ddVar, String str, String str2, long j, Bundle bundle, boolean z, boolean z2, boolean z3, String str3) {
        this.i = ddVar;
        this.f4000a = str;
        this.f4001b = str2;
        this.c = j;
        this.d = bundle;
        this.e = z;
        this.f = z2;
        this.g = z3;
        this.h = str3;
    }

    public final void run() {
        this.i.b(this.f4000a, this.f4001b, this.c, this.d, this.e, this.f, this.g, this.h);
    }
}
