package com.google.android.gms.internal.c;

import android.net.Uri;

public final class hi {

    /* renamed from: a reason: collision with root package name */
    private final String f4110a;
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public final Uri f4111b;
    /* access modifiers changed from: private */
    public final String c;
    /* access modifiers changed from: private */
    public final String d;
    private final boolean e;
    private final boolean f;

    public hi(Uri uri) {
        this(null, uri, "", "", false, false);
    }

    private hi(String str, Uri uri, String str2, String str3, boolean z, boolean z2) {
        this.f4110a = null;
        this.f4111b = uri;
        this.c = str2;
        this.d = str3;
        this.e = false;
        this.f = false;
    }

    public final gy<Double> a(String str, double d2) {
        return gy.b(this, str, d2);
    }

    public final gy<Integer> a(String str, int i) {
        return gy.b(this, str, i);
    }

    public final gy<Long> a(String str, long j) {
        return gy.b(this, str, j);
    }

    public final gy<String> a(String str, String str2) {
        return gy.b(this, str, str2);
    }

    public final gy<Boolean> a(String str, boolean z) {
        return gy.b(this, str, z);
    }
}
