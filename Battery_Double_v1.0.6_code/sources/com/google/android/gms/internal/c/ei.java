package com.google.android.gms.internal.c;

import android.os.RemoteException;

final class ei implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ s f4025a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ eb f4026b;

    ei(eb ebVar, s sVar) {
        this.f4026b = ebVar;
        this.f4025a = sVar;
    }

    public final void run() {
        ar d = this.f4026b.f4014b;
        if (d == null) {
            this.f4026b.q().v().a("Failed to send measurementEnabled to service");
            return;
        }
        try {
            d.b(this.f4025a);
            this.f4026b.C();
        } catch (RemoteException e) {
            this.f4026b.q().v().a("Failed to send measurementEnabled to the service", e);
        }
    }
}
