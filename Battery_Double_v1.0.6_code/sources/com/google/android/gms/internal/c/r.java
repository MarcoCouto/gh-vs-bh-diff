package com.google.android.gms.internal.c;

import android.text.TextUtils;
import com.google.android.gms.common.internal.aa;

final class r {
    private long A;
    private long B;

    /* renamed from: a reason: collision with root package name */
    private final ce f4125a;

    /* renamed from: b reason: collision with root package name */
    private final String f4126b;
    private String c;
    private String d;
    private String e;
    private String f;
    private long g;
    private long h;
    private long i;
    private String j;
    private long k;
    private String l;
    private long m;
    private long n;
    private boolean o;
    private long p;
    private boolean q;
    private boolean r;
    private long s;
    private long t;
    private long u;
    private long v;
    private long w;
    private long x;
    private String y;
    private boolean z;

    r(ce ceVar, String str) {
        aa.a(ceVar);
        aa.a(str);
        this.f4125a = ceVar;
        this.f4126b = str;
        this.f4125a.w();
    }

    public final long A() {
        this.f4125a.w();
        return this.p;
    }

    public final boolean B() {
        this.f4125a.w();
        return this.q;
    }

    public final boolean C() {
        this.f4125a.w();
        return this.r;
    }

    public final void a() {
        this.f4125a.w();
        this.z = false;
    }

    public final void a(long j2) {
        this.f4125a.w();
        this.z = (this.h != j2) | this.z;
        this.h = j2;
    }

    public final void a(String str) {
        this.f4125a.w();
        this.z = (!ft.b(this.c, str)) | this.z;
        this.c = str;
    }

    public final void a(boolean z2) {
        this.f4125a.w();
        this.z = (this.o != z2) | this.z;
        this.o = z2;
    }

    public final String b() {
        this.f4125a.w();
        return this.f4126b;
    }

    public final void b(long j2) {
        this.f4125a.w();
        this.z = (this.i != j2) | this.z;
        this.i = j2;
    }

    public final void b(String str) {
        this.f4125a.w();
        if (TextUtils.isEmpty(str)) {
            str = null;
        }
        this.z = (!ft.b(this.d, str)) | this.z;
        this.d = str;
    }

    public final void b(boolean z2) {
        this.f4125a.w();
        this.z = this.q != z2;
        this.q = z2;
    }

    public final String c() {
        this.f4125a.w();
        return this.c;
    }

    public final void c(long j2) {
        this.f4125a.w();
        this.z = (this.k != j2) | this.z;
        this.k = j2;
    }

    public final void c(String str) {
        this.f4125a.w();
        this.z = (!ft.b(this.e, str)) | this.z;
        this.e = str;
    }

    public final void c(boolean z2) {
        this.f4125a.w();
        this.z = this.r != z2;
        this.r = z2;
    }

    public final String d() {
        this.f4125a.w();
        return this.d;
    }

    public final void d(long j2) {
        this.f4125a.w();
        this.z = (this.m != j2) | this.z;
        this.m = j2;
    }

    public final void d(String str) {
        this.f4125a.w();
        this.z = (!ft.b(this.f, str)) | this.z;
        this.f = str;
    }

    public final String e() {
        this.f4125a.w();
        return this.e;
    }

    public final void e(long j2) {
        this.f4125a.w();
        this.z = (this.n != j2) | this.z;
        this.n = j2;
    }

    public final void e(String str) {
        this.f4125a.w();
        this.z = (!ft.b(this.j, str)) | this.z;
        this.j = str;
    }

    public final String f() {
        this.f4125a.w();
        return this.f;
    }

    public final void f(long j2) {
        boolean z2 = true;
        aa.b(j2 >= 0);
        this.f4125a.w();
        boolean z3 = this.z;
        if (this.g == j2) {
            z2 = false;
        }
        this.z = z3 | z2;
        this.g = j2;
    }

    public final void f(String str) {
        this.f4125a.w();
        this.z = (!ft.b(this.l, str)) | this.z;
        this.l = str;
    }

    public final long g() {
        this.f4125a.w();
        return this.h;
    }

    public final void g(long j2) {
        this.f4125a.w();
        this.z = (this.A != j2) | this.z;
        this.A = j2;
    }

    public final void g(String str) {
        this.f4125a.w();
        this.z = (!ft.b(this.y, str)) | this.z;
        this.y = str;
    }

    public final long h() {
        this.f4125a.w();
        return this.i;
    }

    public final void h(long j2) {
        this.f4125a.w();
        this.z = (this.B != j2) | this.z;
        this.B = j2;
    }

    public final String i() {
        this.f4125a.w();
        return this.j;
    }

    public final void i(long j2) {
        this.f4125a.w();
        this.z = (this.s != j2) | this.z;
        this.s = j2;
    }

    public final long j() {
        this.f4125a.w();
        return this.k;
    }

    public final void j(long j2) {
        this.f4125a.w();
        this.z = (this.t != j2) | this.z;
        this.t = j2;
    }

    public final String k() {
        this.f4125a.w();
        return this.l;
    }

    public final void k(long j2) {
        this.f4125a.w();
        this.z = (this.u != j2) | this.z;
        this.u = j2;
    }

    public final long l() {
        this.f4125a.w();
        return this.m;
    }

    public final void l(long j2) {
        this.f4125a.w();
        this.z = (this.v != j2) | this.z;
        this.v = j2;
    }

    public final long m() {
        this.f4125a.w();
        return this.n;
    }

    public final void m(long j2) {
        this.f4125a.w();
        this.z = (this.x != j2) | this.z;
        this.x = j2;
    }

    public final void n(long j2) {
        this.f4125a.w();
        this.z = (this.w != j2) | this.z;
        this.w = j2;
    }

    public final boolean n() {
        this.f4125a.w();
        return this.o;
    }

    public final long o() {
        this.f4125a.w();
        return this.g;
    }

    public final void o(long j2) {
        this.f4125a.w();
        this.z = (this.p != j2) | this.z;
        this.p = j2;
    }

    public final long p() {
        this.f4125a.w();
        return this.A;
    }

    public final long q() {
        this.f4125a.w();
        return this.B;
    }

    public final void r() {
        this.f4125a.w();
        long j2 = this.g + 1;
        if (j2 > 2147483647L) {
            this.f4125a.q().y().a("Bundle index overflow. appId", az.a(this.f4126b));
            j2 = 0;
        }
        this.z = true;
        this.g = j2;
    }

    public final long s() {
        this.f4125a.w();
        return this.s;
    }

    public final long t() {
        this.f4125a.w();
        return this.t;
    }

    public final long u() {
        this.f4125a.w();
        return this.u;
    }

    public final long v() {
        this.f4125a.w();
        return this.v;
    }

    public final long w() {
        this.f4125a.w();
        return this.x;
    }

    public final long x() {
        this.f4125a.w();
        return this.w;
    }

    public final String y() {
        this.f4125a.w();
        return this.y;
    }

    public final String z() {
        this.f4125a.w();
        String str = this.y;
        g((String) null);
        return str;
    }
}
