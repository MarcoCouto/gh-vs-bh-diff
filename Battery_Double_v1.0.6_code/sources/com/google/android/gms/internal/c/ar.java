package com.google.android.gms.internal.c;

import android.os.IInterface;
import android.os.RemoteException;
import java.util.List;

public interface ar extends IInterface {
    List<fq> a(s sVar, boolean z) throws RemoteException;

    List<w> a(String str, String str2, s sVar) throws RemoteException;

    List<w> a(String str, String str2, String str3) throws RemoteException;

    List<fq> a(String str, String str2, String str3, boolean z) throws RemoteException;

    List<fq> a(String str, String str2, boolean z, s sVar) throws RemoteException;

    void a(long j, String str, String str2, String str3) throws RemoteException;

    void a(an anVar, s sVar) throws RemoteException;

    void a(an anVar, String str, String str2) throws RemoteException;

    void a(fq fqVar, s sVar) throws RemoteException;

    void a(s sVar) throws RemoteException;

    void a(w wVar) throws RemoteException;

    void a(w wVar, s sVar) throws RemoteException;

    byte[] a(an anVar, String str) throws RemoteException;

    void b(s sVar) throws RemoteException;

    String c(s sVar) throws RemoteException;

    void d(s sVar) throws RemoteException;
}
