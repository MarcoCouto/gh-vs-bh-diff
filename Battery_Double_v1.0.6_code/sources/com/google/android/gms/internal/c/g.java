package com.google.android.gms.internal.c;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

final class g implements Cloneable {

    /* renamed from: a reason: collision with root package name */
    private e<?, ?> f4090a;

    /* renamed from: b reason: collision with root package name */
    private Object f4091b;
    private List<l> c = new ArrayList();

    g() {
    }

    private final byte[] b() throws IOException {
        byte[] bArr = new byte[a()];
        a(b.a(bArr));
        return bArr;
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public final g clone() {
        int i = 0;
        g gVar = new g();
        try {
            gVar.f4090a = this.f4090a;
            if (this.c == null) {
                gVar.c = null;
            } else {
                gVar.c.addAll(this.c);
            }
            if (this.f4091b != null) {
                if (this.f4091b instanceof j) {
                    gVar.f4091b = (j) ((j) this.f4091b).clone();
                } else if (this.f4091b instanceof byte[]) {
                    gVar.f4091b = ((byte[]) this.f4091b).clone();
                } else if (this.f4091b instanceof byte[][]) {
                    byte[][] bArr = (byte[][]) this.f4091b;
                    byte[][] bArr2 = new byte[bArr.length][];
                    gVar.f4091b = bArr2;
                    for (int i2 = 0; i2 < bArr.length; i2++) {
                        bArr2[i2] = (byte[]) bArr[i2].clone();
                    }
                } else if (this.f4091b instanceof boolean[]) {
                    gVar.f4091b = ((boolean[]) this.f4091b).clone();
                } else if (this.f4091b instanceof int[]) {
                    gVar.f4091b = ((int[]) this.f4091b).clone();
                } else if (this.f4091b instanceof long[]) {
                    gVar.f4091b = ((long[]) this.f4091b).clone();
                } else if (this.f4091b instanceof float[]) {
                    gVar.f4091b = ((float[]) this.f4091b).clone();
                } else if (this.f4091b instanceof double[]) {
                    gVar.f4091b = ((double[]) this.f4091b).clone();
                } else if (this.f4091b instanceof j[]) {
                    j[] jVarArr = (j[]) this.f4091b;
                    j[] jVarArr2 = new j[jVarArr.length];
                    gVar.f4091b = jVarArr2;
                    while (true) {
                        int i3 = i;
                        if (i3 >= jVarArr.length) {
                            break;
                        }
                        jVarArr2[i3] = (j) jVarArr[i3].clone();
                        i = i3 + 1;
                    }
                }
            }
            return gVar;
        } catch (CloneNotSupportedException e) {
            throw new AssertionError(e);
        }
    }

    /* access modifiers changed from: 0000 */
    public final int a() {
        int i = 0;
        if (this.f4091b != null) {
            e<?, ?> eVar = this.f4090a;
            Object obj = this.f4091b;
            if (!eVar.c) {
                return eVar.a(obj);
            }
            int length = Array.getLength(obj);
            for (int i2 = 0; i2 < length; i2++) {
                if (Array.get(obj, i2) != null) {
                    i += eVar.a(Array.get(obj, i2));
                }
            }
            return i;
        }
        Iterator it = this.c.iterator();
        while (true) {
            int i3 = i;
            if (!it.hasNext()) {
                return i3;
            }
            l lVar = (l) it.next();
            i = lVar.f4114b.length + b.d(lVar.f4113a) + 0 + i3;
        }
    }

    /* access modifiers changed from: 0000 */
    public final void a(b bVar) throws IOException {
        if (this.f4091b != null) {
            e<?, ?> eVar = this.f4090a;
            Object obj = this.f4091b;
            if (eVar.c) {
                int length = Array.getLength(obj);
                for (int i = 0; i < length; i++) {
                    Object obj2 = Array.get(obj, i);
                    if (obj2 != null) {
                        eVar.a(obj2, bVar);
                    }
                }
                return;
            }
            eVar.a(obj, bVar);
            return;
        }
        for (l lVar : this.c) {
            bVar.c(lVar.f4113a);
            bVar.b(lVar.f4114b);
        }
    }

    /* access modifiers changed from: 0000 */
    public final void a(l lVar) throws IOException {
        Object a2;
        if (this.c != null) {
            this.c.add(lVar);
            return;
        }
        if (this.f4091b instanceof j) {
            byte[] bArr = lVar.f4114b;
            a a3 = a.a(bArr, 0, bArr.length);
            int d = a3.d();
            if (d != bArr.length - b.a(d)) {
                throw i.a();
            }
            a2 = ((j) this.f4091b).a(a3);
        } else if (this.f4091b instanceof j[]) {
            j[] jVarArr = (j[]) this.f4090a.a(Collections.singletonList(lVar));
            j[] jVarArr2 = (j[]) this.f4091b;
            a2 = (j[]) Arrays.copyOf(jVarArr2, jVarArr2.length + jVarArr.length);
            System.arraycopy(jVarArr, 0, a2, jVarArr2.length, jVarArr.length);
        } else {
            a2 = this.f4090a.a(Collections.singletonList(lVar));
        }
        this.f4090a = this.f4090a;
        this.f4091b = a2;
        this.c = null;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof g)) {
            return false;
        }
        g gVar = (g) obj;
        if (this.f4091b == null || gVar.f4091b == null) {
            if (this.c != null && gVar.c != null) {
                return this.c.equals(gVar.c);
            }
            try {
                return Arrays.equals(b(), gVar.b());
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        } else if (this.f4090a == gVar.f4090a) {
            return !this.f4090a.f4009a.isArray() ? this.f4091b.equals(gVar.f4091b) : this.f4091b instanceof byte[] ? Arrays.equals((byte[]) this.f4091b, (byte[]) gVar.f4091b) : this.f4091b instanceof int[] ? Arrays.equals((int[]) this.f4091b, (int[]) gVar.f4091b) : this.f4091b instanceof long[] ? Arrays.equals((long[]) this.f4091b, (long[]) gVar.f4091b) : this.f4091b instanceof float[] ? Arrays.equals((float[]) this.f4091b, (float[]) gVar.f4091b) : this.f4091b instanceof double[] ? Arrays.equals((double[]) this.f4091b, (double[]) gVar.f4091b) : this.f4091b instanceof boolean[] ? Arrays.equals((boolean[]) this.f4091b, (boolean[]) gVar.f4091b) : Arrays.deepEquals((Object[]) this.f4091b, (Object[]) gVar.f4091b);
        } else {
            return false;
        }
    }

    public final int hashCode() {
        try {
            return Arrays.hashCode(b()) + 527;
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }
}
