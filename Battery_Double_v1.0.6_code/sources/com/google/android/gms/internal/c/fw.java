package com.google.android.gms.internal.c;

import java.io.IOException;

public final class fw extends d<fw> {
    private static volatile fw[] f;
    public Integer c;
    public ga[] d;
    public fx[] e;

    public fw() {
        this.c = null;
        this.d = ga.e();
        this.e = fx.e();
        this.f3962a = null;
        this.f4112b = -1;
    }

    public static fw[] e() {
        if (f == null) {
            synchronized (h.f4106b) {
                if (f == null) {
                    f = new fw[0];
                }
            }
        }
        return f;
    }

    /* access modifiers changed from: protected */
    public final int a() {
        int a2 = super.a();
        if (this.c != null) {
            a2 += b.b(1, this.c.intValue());
        }
        if (this.d != null && this.d.length > 0) {
            int i = a2;
            for (ga gaVar : this.d) {
                if (gaVar != null) {
                    i += b.b(2, (j) gaVar);
                }
            }
            a2 = i;
        }
        if (this.e != null && this.e.length > 0) {
            for (fx fxVar : this.e) {
                if (fxVar != null) {
                    a2 += b.b(3, (j) fxVar);
                }
            }
        }
        return a2;
    }

    public final /* synthetic */ j a(a aVar) throws IOException {
        while (true) {
            int a2 = aVar.a();
            switch (a2) {
                case 0:
                    break;
                case 8:
                    this.c = Integer.valueOf(aVar.d());
                    continue;
                case 18:
                    int a3 = m.a(aVar, 18);
                    int length = this.d == null ? 0 : this.d.length;
                    ga[] gaVarArr = new ga[(a3 + length)];
                    if (length != 0) {
                        System.arraycopy(this.d, 0, gaVarArr, 0, length);
                    }
                    while (length < gaVarArr.length - 1) {
                        gaVarArr[length] = new ga();
                        aVar.a((j) gaVarArr[length]);
                        aVar.a();
                        length++;
                    }
                    gaVarArr[length] = new ga();
                    aVar.a((j) gaVarArr[length]);
                    this.d = gaVarArr;
                    continue;
                case 26:
                    int a4 = m.a(aVar, 26);
                    int length2 = this.e == null ? 0 : this.e.length;
                    fx[] fxVarArr = new fx[(a4 + length2)];
                    if (length2 != 0) {
                        System.arraycopy(this.e, 0, fxVarArr, 0, length2);
                    }
                    while (length2 < fxVarArr.length - 1) {
                        fxVarArr[length2] = new fx();
                        aVar.a((j) fxVarArr[length2]);
                        aVar.a();
                        length2++;
                    }
                    fxVarArr[length2] = new fx();
                    aVar.a((j) fxVarArr[length2]);
                    this.e = fxVarArr;
                    continue;
                default:
                    if (!super.a(aVar, a2)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    public final void a(b bVar) throws IOException {
        if (this.c != null) {
            bVar.a(1, this.c.intValue());
        }
        if (this.d != null && this.d.length > 0) {
            for (ga gaVar : this.d) {
                if (gaVar != null) {
                    bVar.a(2, (j) gaVar);
                }
            }
        }
        if (this.e != null && this.e.length > 0) {
            for (fx fxVar : this.e) {
                if (fxVar != null) {
                    bVar.a(3, (j) fxVar);
                }
            }
        }
        super.a(bVar);
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof fw)) {
            return false;
        }
        fw fwVar = (fw) obj;
        if (this.c == null) {
            if (fwVar.c != null) {
                return false;
            }
        } else if (!this.c.equals(fwVar.c)) {
            return false;
        }
        if (!h.a((Object[]) this.d, (Object[]) fwVar.d)) {
            return false;
        }
        if (!h.a((Object[]) this.e, (Object[]) fwVar.e)) {
            return false;
        }
        return (this.f3962a == null || this.f3962a.b()) ? fwVar.f3962a == null || fwVar.f3962a.b() : this.f3962a.equals(fwVar.f3962a);
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = ((((((this.c == null ? 0 : this.c.hashCode()) + ((getClass().getName().hashCode() + 527) * 31)) * 31) + h.a((Object[]) this.d)) * 31) + h.a((Object[]) this.e)) * 31;
        if (this.f3962a != null && !this.f3962a.b()) {
            i = this.f3962a.hashCode();
        }
        return hashCode + i;
    }
}
