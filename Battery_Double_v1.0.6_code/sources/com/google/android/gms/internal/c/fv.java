package com.google.android.gms.internal.c;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.SocketAddress;
import java.net.SocketException;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.Arrays;
import javax.net.ssl.HandshakeCompletedListener;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;

final class fv extends SSLSocket {

    /* renamed from: a reason: collision with root package name */
    private final SSLSocket f4089a;

    fv(fu fuVar, SSLSocket sSLSocket) {
        this.f4089a = sSLSocket;
    }

    public final void addHandshakeCompletedListener(HandshakeCompletedListener handshakeCompletedListener) {
        this.f4089a.addHandshakeCompletedListener(handshakeCompletedListener);
    }

    public final void bind(SocketAddress socketAddress) throws IOException {
        this.f4089a.bind(socketAddress);
    }

    public final synchronized void close() throws IOException {
        this.f4089a.close();
    }

    public final void connect(SocketAddress socketAddress) throws IOException {
        this.f4089a.connect(socketAddress);
    }

    public final void connect(SocketAddress socketAddress, int i) throws IOException {
        this.f4089a.connect(socketAddress, i);
    }

    public final boolean equals(Object obj) {
        return this.f4089a.equals(obj);
    }

    public final SocketChannel getChannel() {
        return this.f4089a.getChannel();
    }

    public final boolean getEnableSessionCreation() {
        return this.f4089a.getEnableSessionCreation();
    }

    public final String[] getEnabledCipherSuites() {
        return this.f4089a.getEnabledCipherSuites();
    }

    public final String[] getEnabledProtocols() {
        return this.f4089a.getEnabledProtocols();
    }

    public final InetAddress getInetAddress() {
        return this.f4089a.getInetAddress();
    }

    public final InputStream getInputStream() throws IOException {
        return this.f4089a.getInputStream();
    }

    public final boolean getKeepAlive() throws SocketException {
        return this.f4089a.getKeepAlive();
    }

    public final InetAddress getLocalAddress() {
        return this.f4089a.getLocalAddress();
    }

    public final int getLocalPort() {
        return this.f4089a.getLocalPort();
    }

    public final SocketAddress getLocalSocketAddress() {
        return this.f4089a.getLocalSocketAddress();
    }

    public final boolean getNeedClientAuth() {
        return this.f4089a.getNeedClientAuth();
    }

    public final boolean getOOBInline() throws SocketException {
        return this.f4089a.getOOBInline();
    }

    public final OutputStream getOutputStream() throws IOException {
        return this.f4089a.getOutputStream();
    }

    public final int getPort() {
        return this.f4089a.getPort();
    }

    public final synchronized int getReceiveBufferSize() throws SocketException {
        return this.f4089a.getReceiveBufferSize();
    }

    public final SocketAddress getRemoteSocketAddress() {
        return this.f4089a.getRemoteSocketAddress();
    }

    public final boolean getReuseAddress() throws SocketException {
        return this.f4089a.getReuseAddress();
    }

    public final synchronized int getSendBufferSize() throws SocketException {
        return this.f4089a.getSendBufferSize();
    }

    public final SSLSession getSession() {
        return this.f4089a.getSession();
    }

    public final int getSoLinger() throws SocketException {
        return this.f4089a.getSoLinger();
    }

    public final synchronized int getSoTimeout() throws SocketException {
        return this.f4089a.getSoTimeout();
    }

    public final String[] getSupportedCipherSuites() {
        return this.f4089a.getSupportedCipherSuites();
    }

    public final String[] getSupportedProtocols() {
        return this.f4089a.getSupportedProtocols();
    }

    public final boolean getTcpNoDelay() throws SocketException {
        return this.f4089a.getTcpNoDelay();
    }

    public final int getTrafficClass() throws SocketException {
        return this.f4089a.getTrafficClass();
    }

    public final boolean getUseClientMode() {
        return this.f4089a.getUseClientMode();
    }

    public final boolean getWantClientAuth() {
        return this.f4089a.getWantClientAuth();
    }

    public final boolean isBound() {
        return this.f4089a.isBound();
    }

    public final boolean isClosed() {
        return this.f4089a.isClosed();
    }

    public final boolean isConnected() {
        return this.f4089a.isConnected();
    }

    public final boolean isInputShutdown() {
        return this.f4089a.isInputShutdown();
    }

    public final boolean isOutputShutdown() {
        return this.f4089a.isOutputShutdown();
    }

    public final void removeHandshakeCompletedListener(HandshakeCompletedListener handshakeCompletedListener) {
        this.f4089a.removeHandshakeCompletedListener(handshakeCompletedListener);
    }

    public final void sendUrgentData(int i) throws IOException {
        this.f4089a.sendUrgentData(i);
    }

    public final void setEnableSessionCreation(boolean z) {
        this.f4089a.setEnableSessionCreation(z);
    }

    public final void setEnabledCipherSuites(String[] strArr) {
        this.f4089a.setEnabledCipherSuites(strArr);
    }

    public final void setEnabledProtocols(String[] strArr) {
        if (strArr != null && Arrays.asList(strArr).contains("SSLv3")) {
            ArrayList arrayList = new ArrayList(Arrays.asList(this.f4089a.getEnabledProtocols()));
            if (arrayList.size() > 1) {
                arrayList.remove("SSLv3");
            }
            strArr = (String[]) arrayList.toArray(new String[arrayList.size()]);
        }
        this.f4089a.setEnabledProtocols(strArr);
    }

    public final void setKeepAlive(boolean z) throws SocketException {
        this.f4089a.setKeepAlive(z);
    }

    public final void setNeedClientAuth(boolean z) {
        this.f4089a.setNeedClientAuth(z);
    }

    public final void setOOBInline(boolean z) throws SocketException {
        this.f4089a.setOOBInline(z);
    }

    public final void setPerformancePreferences(int i, int i2, int i3) {
        this.f4089a.setPerformancePreferences(i, i2, i3);
    }

    public final synchronized void setReceiveBufferSize(int i) throws SocketException {
        this.f4089a.setReceiveBufferSize(i);
    }

    public final void setReuseAddress(boolean z) throws SocketException {
        this.f4089a.setReuseAddress(z);
    }

    public final synchronized void setSendBufferSize(int i) throws SocketException {
        this.f4089a.setSendBufferSize(i);
    }

    public final void setSoLinger(boolean z, int i) throws SocketException {
        this.f4089a.setSoLinger(z, i);
    }

    public final synchronized void setSoTimeout(int i) throws SocketException {
        this.f4089a.setSoTimeout(i);
    }

    public final void setTcpNoDelay(boolean z) throws SocketException {
        this.f4089a.setTcpNoDelay(z);
    }

    public final void setTrafficClass(int i) throws SocketException {
        this.f4089a.setTrafficClass(i);
    }

    public final void setUseClientMode(boolean z) {
        this.f4089a.setUseClientMode(z);
    }

    public final void setWantClientAuth(boolean z) {
        this.f4089a.setWantClientAuth(z);
    }

    public final void shutdownInput() throws IOException {
        this.f4089a.shutdownInput();
    }

    public final void shutdownOutput() throws IOException {
        this.f4089a.shutdownOutput();
    }

    public final void startHandshake() throws IOException {
        this.f4089a.startHandshake();
    }

    public final String toString() {
        return this.f4089a.toString();
    }
}
