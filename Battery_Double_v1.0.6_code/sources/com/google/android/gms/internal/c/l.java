package com.google.android.gms.internal.c;

import java.util.Arrays;

final class l {

    /* renamed from: a reason: collision with root package name */
    final int f4113a;

    /* renamed from: b reason: collision with root package name */
    final byte[] f4114b;

    l(int i, byte[] bArr) {
        this.f4113a = i;
        this.f4114b = bArr;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof l)) {
            return false;
        }
        l lVar = (l) obj;
        return this.f4113a == lVar.f4113a && Arrays.equals(this.f4114b, lVar.f4114b);
    }

    public final int hashCode() {
        return ((this.f4113a + 527) * 31) + Arrays.hashCode(this.f4114b);
    }
}
