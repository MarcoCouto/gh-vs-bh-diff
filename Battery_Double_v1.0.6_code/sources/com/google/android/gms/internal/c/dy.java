package com.google.android.gms.internal.c;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.h.a;
import com.google.android.gms.common.internal.aa;
import com.google.android.gms.common.util.e;
import java.util.Map;

public final class dy extends da {

    /* renamed from: a reason: collision with root package name */
    protected dx f4005a;

    /* renamed from: b reason: collision with root package name */
    private volatile dx f4006b;
    private dx c;
    private long d;
    private final Map<Activity, dx> e = new a();
    private dx f;
    private String g;

    public dy(ce ceVar) {
        super(ceVar);
    }

    private static String a(String str) {
        String[] split = str.split("\\.");
        String str2 = split.length > 0 ? split[split.length - 1] : "";
        return str2.length() > 100 ? str2.substring(0, 100) : str2;
    }

    private final void a(Activity activity, dx dxVar, boolean z) {
        dx dxVar2 = this.f4006b == null ? this.c : this.f4006b;
        if (dxVar.f4004b == null) {
            dxVar = new dx(dxVar.f4003a, a(activity.getClass().getCanonicalName()), dxVar.c);
        }
        this.c = this.f4006b;
        this.d = j().b();
        this.f4006b = dxVar;
        p().a((Runnable) new dz(this, z, dxVar2, dxVar));
    }

    /* access modifiers changed from: private */
    public final void a(dx dxVar) {
        d().a(j().b());
        if (o().a(dxVar.d)) {
            dxVar.d = false;
        }
    }

    public static void a(dx dxVar, Bundle bundle, boolean z) {
        if (bundle != null && dxVar != null && (!bundle.containsKey("_sc") || z)) {
            if (dxVar.f4003a != null) {
                bundle.putString("_sn", dxVar.f4003a);
            } else {
                bundle.remove("_sn");
            }
            bundle.putString("_sc", dxVar.f4004b);
            bundle.putLong("_si", dxVar.c);
        } else if (bundle != null && dxVar == null && z) {
            bundle.remove("_sn");
            bundle.remove("_sc");
            bundle.remove("_si");
        }
    }

    private final dx d(Activity activity) {
        aa.a(activity);
        dx dxVar = (dx) this.e.get(activity);
        if (dxVar != null) {
            return dxVar;
        }
        dx dxVar2 = new dx(null, a(activity.getClass().getCanonicalName()), n().v());
        this.e.put(activity, dxVar2);
        return dxVar2;
    }

    public final /* bridge */ /* synthetic */ void a() {
        super.a();
    }

    public final void a(Activity activity) {
        a(activity, d(activity), false);
        n d2 = d();
        d2.p().a((Runnable) new q(d2, d2.j().b()));
    }

    public final void a(Activity activity, Bundle bundle) {
        if (bundle != null) {
            Bundle bundle2 = bundle.getBundle("com.google.firebase.analytics.screen_service");
            if (bundle2 != null) {
                this.e.put(activity, new dx(bundle2.getString("name"), bundle2.getString("referrer_name"), bundle2.getLong("id")));
            }
        }
    }

    public final void a(Activity activity, String str, String str2) {
        p();
        if (!bz.v()) {
            q().y().a("setCurrentScreen must be called from the main thread");
        } else if (this.f4006b == null) {
            q().y().a("setCurrentScreen cannot be called while no activity active");
        } else if (this.e.get(activity) == null) {
            q().y().a("setCurrentScreen must be called with an activity in the activity lifecycle");
        } else {
            if (str2 == null) {
                str2 = a(activity.getClass().getCanonicalName());
            }
            boolean equals = this.f4006b.f4004b.equals(str2);
            boolean b2 = ft.b(this.f4006b.f4003a, str);
            if (equals && b2) {
                q().z().a("setCurrentScreen cannot be called with the same class and name");
            } else if (str != null && (str.length() <= 0 || str.length() > 100)) {
                q().y().a("Invalid screen name length in setCurrentScreen. Length", Integer.valueOf(str.length()));
            } else if (str2 == null || (str2.length() > 0 && str2.length() <= 100)) {
                q().C().a("Setting current screen to name, class", str == null ? "null" : str, str2);
                dx dxVar = new dx(str, str2, n().v());
                this.e.put(activity, dxVar);
                a(activity, dxVar, true);
            } else {
                q().y().a("Invalid class name length in setCurrentScreen. Length", Integer.valueOf(str2.length()));
            }
        }
    }

    public final void a(String str, dx dxVar) {
        c();
        synchronized (this) {
            if (this.g == null || this.g.equals(str) || dxVar != null) {
                this.g = str;
                this.f = dxVar;
            }
        }
    }

    public final /* bridge */ /* synthetic */ void b() {
        super.b();
    }

    public final void b(Activity activity) {
        dx d2 = d(activity);
        this.c = this.f4006b;
        this.d = j().b();
        this.f4006b = null;
        p().a((Runnable) new ea(this, d2));
    }

    public final void b(Activity activity, Bundle bundle) {
        if (bundle != null) {
            dx dxVar = (dx) this.e.get(activity);
            if (dxVar != null) {
                Bundle bundle2 = new Bundle();
                bundle2.putLong("id", dxVar.c);
                bundle2.putString("name", dxVar.f4003a);
                bundle2.putString("referrer_name", dxVar.f4004b);
                bundle.putBundle("com.google.firebase.analytics.screen_service", bundle2);
            }
        }
    }

    public final /* bridge */ /* synthetic */ void c() {
        super.c();
    }

    public final void c(Activity activity) {
        this.e.remove(activity);
    }

    public final /* bridge */ /* synthetic */ n d() {
        return super.d();
    }

    public final /* bridge */ /* synthetic */ dd e() {
        return super.e();
    }

    public final /* bridge */ /* synthetic */ au f() {
        return super.f();
    }

    public final /* bridge */ /* synthetic */ ah g() {
        return super.g();
    }

    public final /* bridge */ /* synthetic */ eb h() {
        return super.h();
    }

    public final /* bridge */ /* synthetic */ dy i() {
        return super.i();
    }

    public final /* bridge */ /* synthetic */ e j() {
        return super.j();
    }

    public final /* bridge */ /* synthetic */ Context k() {
        return super.k();
    }

    public final /* bridge */ /* synthetic */ av l() {
        return super.l();
    }

    public final /* bridge */ /* synthetic */ ax m() {
        return super.m();
    }

    public final /* bridge */ /* synthetic */ ft n() {
        return super.n();
    }

    public final /* bridge */ /* synthetic */ fa o() {
        return super.o();
    }

    public final /* bridge */ /* synthetic */ bz p() {
        return super.p();
    }

    public final /* bridge */ /* synthetic */ az q() {
        return super.q();
    }

    public final /* bridge */ /* synthetic */ bk r() {
        return super.r();
    }

    public final /* bridge */ /* synthetic */ y s() {
        return super.s();
    }

    /* access modifiers changed from: protected */
    public final boolean t() {
        return false;
    }

    public final dx v() {
        F();
        c();
        return this.f4005a;
    }

    public final dx w() {
        return this.f4006b;
    }
}
