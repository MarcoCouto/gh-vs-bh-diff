package com.google.android.gms.internal.c;

import java.io.IOException;

public final class fx extends d<fx> {
    private static volatile fx[] g;
    public Integer c;
    public String d;
    public fy[] e;
    public fz f;
    private Boolean h;

    public fx() {
        this.c = null;
        this.d = null;
        this.e = fy.e();
        this.h = null;
        this.f = null;
        this.f3962a = null;
        this.f4112b = -1;
    }

    public static fx[] e() {
        if (g == null) {
            synchronized (h.f4106b) {
                if (g == null) {
                    g = new fx[0];
                }
            }
        }
        return g;
    }

    /* access modifiers changed from: protected */
    public final int a() {
        int a2 = super.a();
        if (this.c != null) {
            a2 += b.b(1, this.c.intValue());
        }
        if (this.d != null) {
            a2 += b.b(2, this.d);
        }
        if (this.e != null && this.e.length > 0) {
            int i = a2;
            for (fy fyVar : this.e) {
                if (fyVar != null) {
                    i += b.b(3, (j) fyVar);
                }
            }
            a2 = i;
        }
        if (this.h != null) {
            this.h.booleanValue();
            a2 += b.b(4) + 1;
        }
        return this.f != null ? a2 + b.b(5, (j) this.f) : a2;
    }

    public final /* synthetic */ j a(a aVar) throws IOException {
        while (true) {
            int a2 = aVar.a();
            switch (a2) {
                case 0:
                    break;
                case 8:
                    this.c = Integer.valueOf(aVar.d());
                    continue;
                case 18:
                    this.d = aVar.c();
                    continue;
                case 26:
                    int a3 = m.a(aVar, 26);
                    int length = this.e == null ? 0 : this.e.length;
                    fy[] fyVarArr = new fy[(a3 + length)];
                    if (length != 0) {
                        System.arraycopy(this.e, 0, fyVarArr, 0, length);
                    }
                    while (length < fyVarArr.length - 1) {
                        fyVarArr[length] = new fy();
                        aVar.a((j) fyVarArr[length]);
                        aVar.a();
                        length++;
                    }
                    fyVarArr[length] = new fy();
                    aVar.a((j) fyVarArr[length]);
                    this.e = fyVarArr;
                    continue;
                case 32:
                    this.h = Boolean.valueOf(aVar.b());
                    continue;
                case 42:
                    if (this.f == null) {
                        this.f = new fz();
                    }
                    aVar.a((j) this.f);
                    continue;
                default:
                    if (!super.a(aVar, a2)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    public final void a(b bVar) throws IOException {
        if (this.c != null) {
            bVar.a(1, this.c.intValue());
        }
        if (this.d != null) {
            bVar.a(2, this.d);
        }
        if (this.e != null && this.e.length > 0) {
            for (fy fyVar : this.e) {
                if (fyVar != null) {
                    bVar.a(3, (j) fyVar);
                }
            }
        }
        if (this.h != null) {
            bVar.a(4, this.h.booleanValue());
        }
        if (this.f != null) {
            bVar.a(5, (j) this.f);
        }
        super.a(bVar);
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof fx)) {
            return false;
        }
        fx fxVar = (fx) obj;
        if (this.c == null) {
            if (fxVar.c != null) {
                return false;
            }
        } else if (!this.c.equals(fxVar.c)) {
            return false;
        }
        if (this.d == null) {
            if (fxVar.d != null) {
                return false;
            }
        } else if (!this.d.equals(fxVar.d)) {
            return false;
        }
        if (!h.a((Object[]) this.e, (Object[]) fxVar.e)) {
            return false;
        }
        if (this.h == null) {
            if (fxVar.h != null) {
                return false;
            }
        } else if (!this.h.equals(fxVar.h)) {
            return false;
        }
        if (this.f == null) {
            if (fxVar.f != null) {
                return false;
            }
        } else if (!this.f.equals(fxVar.f)) {
            return false;
        }
        return (this.f3962a == null || this.f3962a.b()) ? fxVar.f3962a == null || fxVar.f3962a.b() : this.f3962a.equals(fxVar.f3962a);
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = (this.h == null ? 0 : this.h.hashCode()) + (((((this.d == null ? 0 : this.d.hashCode()) + (((this.c == null ? 0 : this.c.hashCode()) + ((getClass().getName().hashCode() + 527) * 31)) * 31)) * 31) + h.a((Object[]) this.e)) * 31);
        fz fzVar = this.f;
        int hashCode2 = ((fzVar == null ? 0 : fzVar.hashCode()) + (hashCode * 31)) * 31;
        if (this.f3962a != null && !this.f3962a.b()) {
            i = this.f3962a.hashCode();
        }
        return hashCode2 + i;
    }
}
