package com.google.android.gms.internal.c;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.a.a;
import com.google.android.gms.common.internal.a.c;
import com.google.android.gms.common.internal.aa;

public final class an extends a {
    public static final Creator<an> CREATOR = new ao();

    /* renamed from: a reason: collision with root package name */
    public final String f3864a;

    /* renamed from: b reason: collision with root package name */
    public final ak f3865b;
    public final String c;
    public final long d;

    an(an anVar, long j) {
        aa.a(anVar);
        this.f3864a = anVar.f3864a;
        this.f3865b = anVar.f3865b;
        this.c = anVar.c;
        this.d = j;
    }

    public an(String str, ak akVar, String str2, long j) {
        this.f3864a = str;
        this.f3865b = akVar;
        this.c = str2;
        this.d = j;
    }

    public final String toString() {
        String str = this.c;
        String str2 = this.f3864a;
        String valueOf = String.valueOf(this.f3865b);
        return new StringBuilder(String.valueOf(str).length() + 21 + String.valueOf(str2).length() + String.valueOf(valueOf).length()).append("origin=").append(str).append(",name=").append(str2).append(",params=").append(valueOf).toString();
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = c.a(parcel);
        c.a(parcel, 2, this.f3864a, false);
        c.a(parcel, 3, (Parcelable) this.f3865b, i, false);
        c.a(parcel, 4, this.c, false);
        c.a(parcel, 5, this.d);
        c.a(parcel, a2);
    }
}
