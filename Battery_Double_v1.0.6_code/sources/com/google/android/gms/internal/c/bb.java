package com.google.android.gms.internal.c;

public final class bb {

    /* renamed from: a reason: collision with root package name */
    private final int f3882a;

    /* renamed from: b reason: collision with root package name */
    private final boolean f3883b;
    private final boolean c;
    private final /* synthetic */ az d;

    bb(az azVar, int i, boolean z, boolean z2) {
        this.d = azVar;
        this.f3882a = i;
        this.f3883b = z;
        this.c = z2;
    }

    public final void a(String str) {
        this.d.a(this.f3882a, this.f3883b, this.c, str, null, null, null);
    }

    public final void a(String str, Object obj) {
        this.d.a(this.f3882a, this.f3883b, this.c, str, obj, null, null);
    }

    public final void a(String str, Object obj, Object obj2) {
        this.d.a(this.f3882a, this.f3883b, this.c, str, obj, obj2, null);
    }

    public final void a(String str, Object obj, Object obj2, Object obj3) {
        this.d.a(this.f3882a, this.f3883b, this.c, str, obj, obj2, obj3);
    }
}
