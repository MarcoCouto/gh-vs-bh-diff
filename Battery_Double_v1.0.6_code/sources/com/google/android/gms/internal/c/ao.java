package com.google.android.gms.internal.c;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.a.b;

public final class ao implements Creator<an> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        String str = null;
        int b2 = b.b(parcel);
        long j = 0;
        ak akVar = null;
        String str2 = null;
        while (parcel.dataPosition() < b2) {
            int a2 = b.a(parcel);
            switch (b.a(a2)) {
                case 2:
                    str2 = b.k(parcel, a2);
                    break;
                case 3:
                    akVar = (ak) b.a(parcel, a2, ak.CREATOR);
                    break;
                case 4:
                    str = b.k(parcel, a2);
                    break;
                case 5:
                    j = b.f(parcel, a2);
                    break;
                default:
                    b.b(parcel, a2);
                    break;
            }
        }
        b.r(parcel, b2);
        return new an(str2, akVar, str, j);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new an[i];
    }
}
