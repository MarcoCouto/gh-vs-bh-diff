package com.google.android.gms.internal.c;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.a.b;

public final class x implements Creator<w> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b2 = b.b(parcel);
        String str = null;
        String str2 = null;
        fq fqVar = null;
        long j = 0;
        boolean z = false;
        String str3 = null;
        an anVar = null;
        long j2 = 0;
        an anVar2 = null;
        long j3 = 0;
        an anVar3 = null;
        while (parcel.dataPosition() < b2) {
            int a2 = b.a(parcel);
            switch (b.a(a2)) {
                case 2:
                    str = b.k(parcel, a2);
                    break;
                case 3:
                    str2 = b.k(parcel, a2);
                    break;
                case 4:
                    fqVar = (fq) b.a(parcel, a2, fq.CREATOR);
                    break;
                case 5:
                    j = b.f(parcel, a2);
                    break;
                case 6:
                    z = b.c(parcel, a2);
                    break;
                case 7:
                    str3 = b.k(parcel, a2);
                    break;
                case 8:
                    anVar = (an) b.a(parcel, a2, an.CREATOR);
                    break;
                case 9:
                    j2 = b.f(parcel, a2);
                    break;
                case 10:
                    anVar2 = (an) b.a(parcel, a2, an.CREATOR);
                    break;
                case 11:
                    j3 = b.f(parcel, a2);
                    break;
                case 12:
                    anVar3 = (an) b.a(parcel, a2, an.CREATOR);
                    break;
                default:
                    b.b(parcel, a2);
                    break;
            }
        }
        b.r(parcel, b2);
        return new w(str, str2, fqVar, j, z, str3, anVar, j2, anVar2, j3, anVar3);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new w[i];
    }
}
