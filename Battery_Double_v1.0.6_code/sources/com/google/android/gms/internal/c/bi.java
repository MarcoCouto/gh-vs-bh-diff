package com.google.android.gms.internal.c;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import com.google.android.gms.common.internal.aa;

class bi extends BroadcastReceiver {

    /* renamed from: a reason: collision with root package name */
    private static final String f3890a = bi.class.getName();
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public final fk f3891b;
    private boolean c;
    private boolean d;

    bi(fk fkVar) {
        aa.a(fkVar);
        this.f3891b = fkVar;
    }

    public final void a() {
        this.f3891b.F();
        this.f3891b.w();
        if (!this.c) {
            this.f3891b.k().registerReceiver(this, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
            this.d = this.f3891b.C().u();
            this.f3891b.q().C().a("Registering connectivity change receiver. Network connected", Boolean.valueOf(this.d));
            this.c = true;
        }
    }

    public final void b() {
        this.f3891b.F();
        this.f3891b.w();
        this.f3891b.w();
        if (this.c) {
            this.f3891b.q().C().a("Unregistering connectivity change receiver");
            this.c = false;
            this.d = false;
            try {
                this.f3891b.k().unregisterReceiver(this);
            } catch (IllegalArgumentException e) {
                this.f3891b.q().v().a("Failed to unregister the network broadcast receiver", e);
            }
        }
    }

    public void onReceive(Context context, Intent intent) {
        this.f3891b.F();
        String action = intent.getAction();
        this.f3891b.q().C().a("NetworkBroadcastReceiver received action", action);
        if ("android.net.conn.CONNECTIVITY_CHANGE".equals(action)) {
            boolean u = this.f3891b.C().u();
            if (this.d != u) {
                this.d = u;
                this.f3891b.p().a((Runnable) new bj(this, u));
                return;
            }
            return;
        }
        this.f3891b.q().y().a("NetworkBroadcastReceiver received unknown action", action);
    }
}
