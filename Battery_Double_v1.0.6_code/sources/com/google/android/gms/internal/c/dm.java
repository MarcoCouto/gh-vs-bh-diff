package com.google.android.gms.internal.c;

import java.util.concurrent.atomic.AtomicReference;

final class dm implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ AtomicReference f3982a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ String f3983b;
    private final /* synthetic */ String c;
    private final /* synthetic */ String d;
    private final /* synthetic */ dd e;

    dm(dd ddVar, AtomicReference atomicReference, String str, String str2, String str3) {
        this.e = ddVar;
        this.f3982a = atomicReference;
        this.f3983b = str;
        this.c = str2;
        this.d = str3;
    }

    public final void run() {
        this.e.q.s().a(this.f3982a, this.f3983b, this.c, this.d);
    }
}
