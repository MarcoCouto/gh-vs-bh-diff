package com.google.android.gms.internal.c;

final class bv implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ ce f3909a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ az f3910b;

    bv(bu buVar, ce ceVar, az azVar) {
        this.f3909a = ceVar;
        this.f3910b = azVar;
    }

    public final void run() {
        if (this.f3909a.f() == null) {
            this.f3910b.v().a("Install Referrer Reporter is null");
        } else {
            this.f3909a.f().a();
        }
    }
}
