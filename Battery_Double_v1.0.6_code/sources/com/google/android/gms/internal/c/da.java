package com.google.android.gms.internal.c;

abstract class da extends cz {

    /* renamed from: a reason: collision with root package name */
    private boolean f3963a;

    da(ce ceVar) {
        super(ceVar);
        this.q.a(this);
    }

    /* access modifiers changed from: 0000 */
    public final boolean E() {
        return this.f3963a;
    }

    /* access modifiers changed from: protected */
    public final void F() {
        if (!E()) {
            throw new IllegalStateException("Not initialized");
        }
    }

    public final void G() {
        if (this.f3963a) {
            throw new IllegalStateException("Can't initialize twice");
        } else if (!t()) {
            this.q.A();
            this.f3963a = true;
        }
    }

    public final void H() {
        if (this.f3963a) {
            throw new IllegalStateException("Can't initialize twice");
        }
        v_();
        this.q.A();
        this.f3963a = true;
    }

    /* access modifiers changed from: protected */
    public abstract boolean t();

    /* access modifiers changed from: protected */
    public void v_() {
    }
}
