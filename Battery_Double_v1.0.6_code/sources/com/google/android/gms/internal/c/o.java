package com.google.android.gms.internal.c;

final class o implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ String f4119a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ long f4120b;
    private final /* synthetic */ n c;

    o(n nVar, String str, long j) {
        this.c = nVar;
        this.f4119a = str;
        this.f4120b = j;
    }

    public final void run() {
        this.c.a(this.f4119a, this.f4120b);
    }
}
