package com.google.android.gms.internal.c;

final class df implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ String f3969a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ String f3970b;
    private final /* synthetic */ Object c;
    private final /* synthetic */ long d;
    private final /* synthetic */ dd e;

    df(dd ddVar, String str, String str2, Object obj, long j) {
        this.e = ddVar;
        this.f3969a = str;
        this.f3970b = str2;
        this.c = obj;
        this.d = j;
    }

    public final void run() {
        this.e.a(this.f3969a, this.f3970b, this.c, this.d);
    }
}
