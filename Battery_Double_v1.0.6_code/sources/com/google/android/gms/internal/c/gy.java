package com.google.android.gms.internal.c;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Binder;
import android.os.Build.VERSION;
import android.support.v4.b.c;
import android.util.Log;

public abstract class gy<T> {

    /* renamed from: b reason: collision with root package name */
    private static final Object f4101b = new Object();
    @SuppressLint({"StaticFieldLeak"})
    private static Context c = null;
    private static boolean d = false;
    private static volatile Boolean e = null;
    private static volatile Boolean f = null;

    /* renamed from: a reason: collision with root package name */
    final String f4102a;
    private final hi g;
    private final String h;
    private final T i;
    private T j;
    private volatile gv k;
    private volatile SharedPreferences l;

    private gy(hi hiVar, String str, T t) {
        this.j = null;
        this.k = null;
        this.l = null;
        if (hiVar.f4111b == null) {
            throw new IllegalArgumentException("Must pass a valid SharedPreferences file name or ContentProvider URI");
        }
        this.g = hiVar;
        String valueOf = String.valueOf(hiVar.c);
        String valueOf2 = String.valueOf(str);
        this.h = valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf);
        String valueOf3 = String.valueOf(hiVar.d);
        String valueOf4 = String.valueOf(str);
        this.f4102a = valueOf4.length() != 0 ? valueOf3.concat(valueOf4) : new String(valueOf3);
        this.i = t;
    }

    /* synthetic */ gy(hi hiVar, String str, Object obj, hc hcVar) {
        this(hiVar, str, obj);
    }

    private static <V> V a(hh<V> hhVar) {
        long clearCallingIdentity;
        try {
            return hhVar.a();
        } catch (SecurityException e2) {
            clearCallingIdentity = Binder.clearCallingIdentity();
            V a2 = hhVar.a();
            Binder.restoreCallingIdentity(clearCallingIdentity);
            return a2;
        } catch (Throwable th) {
            Binder.restoreCallingIdentity(clearCallingIdentity);
            throw th;
        }
    }

    public static void a(Context context) {
        synchronized (f4101b) {
            if (VERSION.SDK_INT < 24 || !context.isDeviceProtectedStorage()) {
                Context applicationContext = context.getApplicationContext();
                if (applicationContext != null) {
                    context = applicationContext;
                }
            }
            if (c != context) {
                e = null;
            }
            c = context;
        }
        d = false;
    }

    static boolean a(String str, boolean z) {
        try {
            if (e()) {
                return ((Boolean) a((hh<V>) new hb<V>(str, false))).booleanValue();
            }
            return false;
        } catch (SecurityException e2) {
            Log.e("PhenotypeFlag", "Unable to read GServices, returning default value.", e2);
            return false;
        }
    }

    /* access modifiers changed from: private */
    public static gy<Double> b(hi hiVar, String str, double d2) {
        return new hf(hiVar, str, Double.valueOf(d2));
    }

    /* access modifiers changed from: private */
    public static gy<Integer> b(hi hiVar, String str, int i2) {
        return new hd(hiVar, str, Integer.valueOf(i2));
    }

    /* access modifiers changed from: private */
    public static gy<Long> b(hi hiVar, String str, long j2) {
        return new hc(hiVar, str, Long.valueOf(j2));
    }

    /* access modifiers changed from: private */
    public static gy<String> b(hi hiVar, String str, String str2) {
        return new hg(hiVar, str, str2);
    }

    /* access modifiers changed from: private */
    public static gy<Boolean> b(hi hiVar, String str, boolean z) {
        return new he(hiVar, str, Boolean.valueOf(z));
    }

    @TargetApi(24)
    private final T c() {
        if (a("gms:phenotype:phenotype_flag:debug_bypass_phenotype", false)) {
            String str = "PhenotypeFlag";
            String str2 = "Bypass reading Phenotype values for flag: ";
            String valueOf = String.valueOf(this.f4102a);
            Log.w(str, valueOf.length() != 0 ? str2.concat(valueOf) : new String(str2));
        } else if (this.g.f4111b != null) {
            if (this.k == null) {
                this.k = gv.a(c.getContentResolver(), this.g.f4111b);
            }
            String str3 = (String) a((hh<V>) new gz<V>(this, this.k));
            if (str3 != null) {
                return a(str3);
            }
        } else {
            hi hiVar = this.g;
        }
        return null;
    }

    private final T d() {
        hi hiVar = this.g;
        if (e()) {
            try {
                String str = (String) a((hh<V>) new ha<V>(this));
                if (str != null) {
                    return a(str);
                }
            } catch (SecurityException e2) {
                SecurityException securityException = e2;
                String str2 = "PhenotypeFlag";
                String str3 = "Unable to read GServices for flag: ";
                String valueOf = String.valueOf(this.f4102a);
                Log.e(str2, valueOf.length() != 0 ? str3.concat(valueOf) : new String(str3), securityException);
            }
        }
        return null;
    }

    private static boolean e() {
        boolean z = false;
        if (e == null) {
            if (c == null) {
                return false;
            }
            if (c.b(c, "com.google.android.providers.gsf.permission.READ_GSERVICES") == 0) {
                z = true;
            }
            e = Boolean.valueOf(z);
        }
        return e.booleanValue();
    }

    public final T a() {
        if (c == null) {
            throw new IllegalStateException("Must call PhenotypeFlag.init() first");
        }
        hi hiVar = this.g;
        T c2 = c();
        if (c2 != null) {
            return c2;
        }
        T d2 = d();
        return d2 == null ? this.i : d2;
    }

    /* access modifiers changed from: protected */
    public abstract T a(String str);

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ String b() {
        return gt.a(c.getContentResolver(), this.h, (String) null);
    }
}
