package com.google.android.gms.internal.c;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Build.VERSION;
import android.text.TextUtils;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.d;
import com.google.android.gms.common.b.a;
import com.google.android.gms.common.util.e;
import com.google.firebase.iid.FirebaseInstanceId;
import com.hmatalonga.greenhub.models.Network;
import java.math.BigInteger;
import java.util.Locale;

public final class au extends da {

    /* renamed from: a reason: collision with root package name */
    private String f3870a;

    /* renamed from: b reason: collision with root package name */
    private String f3871b;
    private int c;
    private String d;
    private String e;
    private long f;
    private long g;
    private int h;
    private String i;

    au(ce ceVar) {
        super(ceVar);
    }

    private final String A() {
        String str = null;
        c();
        if (s().h(this.f3870a) && !this.q.x()) {
            return str;
        }
        try {
            return FirebaseInstanceId.a().c();
        } catch (IllegalStateException e2) {
            q().y().a("Failed to retrieve Firebase Instance Id");
            return str;
        }
    }

    /* access modifiers changed from: 0000 */
    public final s a(String str) {
        c();
        String w = w();
        String x = x();
        F();
        String str2 = this.f3871b;
        long y = (long) y();
        F();
        String str3 = this.d;
        F();
        c();
        if (this.f == 0) {
            this.f = this.q.m().b(k(), k().getPackageName());
        }
        long j = this.f;
        boolean x2 = this.q.x();
        boolean z = !r().p;
        String A = A();
        F();
        long y2 = this.q.y();
        int z2 = z();
        Boolean b2 = s().b("google_analytics_adid_collection_enabled");
        boolean booleanValue = Boolean.valueOf(b2 == null || b2.booleanValue()).booleanValue();
        Boolean b3 = s().b("google_analytics_ssaid_collection_enabled");
        return new s(w, x, str2, y, str3, 12451, j, str, x2, z, A, 0, y2, z2, booleanValue, Boolean.valueOf(b3 == null || b3.booleanValue()).booleanValue(), r().A());
    }

    public final /* bridge */ /* synthetic */ void a() {
        super.a();
    }

    public final /* bridge */ /* synthetic */ void b() {
        super.b();
    }

    public final /* bridge */ /* synthetic */ void c() {
        super.c();
    }

    public final /* bridge */ /* synthetic */ n d() {
        return super.d();
    }

    public final /* bridge */ /* synthetic */ dd e() {
        return super.e();
    }

    public final /* bridge */ /* synthetic */ au f() {
        return super.f();
    }

    public final /* bridge */ /* synthetic */ ah g() {
        return super.g();
    }

    public final /* bridge */ /* synthetic */ eb h() {
        return super.h();
    }

    public final /* bridge */ /* synthetic */ dy i() {
        return super.i();
    }

    public final /* bridge */ /* synthetic */ e j() {
        return super.j();
    }

    public final /* bridge */ /* synthetic */ Context k() {
        return super.k();
    }

    public final /* bridge */ /* synthetic */ av l() {
        return super.l();
    }

    public final /* bridge */ /* synthetic */ ax m() {
        return super.m();
    }

    public final /* bridge */ /* synthetic */ ft n() {
        return super.n();
    }

    public final /* bridge */ /* synthetic */ fa o() {
        return super.o();
    }

    public final /* bridge */ /* synthetic */ bz p() {
        return super.p();
    }

    public final /* bridge */ /* synthetic */ az q() {
        return super.q();
    }

    public final /* bridge */ /* synthetic */ bk r() {
        return super.r();
    }

    public final /* bridge */ /* synthetic */ y s() {
        return super.s();
    }

    /* access modifiers changed from: protected */
    public final boolean t() {
        return true;
    }

    /* access modifiers changed from: 0000 */
    public final String v() {
        byte[] bArr = new byte[16];
        n().w().nextBytes(bArr);
        return String.format(Locale.US, "%032x", new Object[]{new BigInteger(1, bArr)});
    }

    /* access modifiers changed from: protected */
    public final void v_() {
        boolean z;
        int i2 = 1;
        String str = Network.TYPE_UNKNOWN;
        String str2 = "Unknown";
        int i3 = Integer.MIN_VALUE;
        String str3 = "Unknown";
        String packageName = k().getPackageName();
        PackageManager packageManager = k().getPackageManager();
        if (packageManager == null) {
            q().v().a("PackageManager is null, app identity information might be inaccurate. appId", az.a(packageName));
        } else {
            try {
                str = packageManager.getInstallerPackageName(packageName);
            } catch (IllegalArgumentException e2) {
                q().v().a("Error retrieving app installer package name. appId", az.a(packageName));
            }
            if (str == null) {
                str = "manual_install";
            } else if ("com.android.vending".equals(str)) {
                str = "";
            }
            try {
                PackageInfo packageInfo = packageManager.getPackageInfo(k().getPackageName(), 0);
                if (packageInfo != null) {
                    CharSequence applicationLabel = packageManager.getApplicationLabel(packageInfo.applicationInfo);
                    if (!TextUtils.isEmpty(applicationLabel)) {
                        str3 = applicationLabel.toString();
                    }
                    str2 = packageInfo.versionName;
                    i3 = packageInfo.versionCode;
                }
            } catch (NameNotFoundException e3) {
                q().v().a("Error retrieving package info. appId, appName", az.a(packageName), str3);
            }
        }
        this.f3870a = packageName;
        this.d = str;
        this.f3871b = str2;
        this.c = i3;
        this.e = str3;
        this.f = 0;
        Status a2 = d.a(k());
        boolean z2 = a2 != null && a2.c();
        if (!z2) {
            if (a2 == null) {
                q().v().a("GoogleService failed to initialize (no status)");
            } else {
                q().v().a("GoogleService failed to initialize, status", Integer.valueOf(a2.d()), a2.b());
            }
        }
        if (z2) {
            Boolean b2 = s().b("firebase_analytics_collection_enabled");
            if (s().u()) {
                q().A().a("Collection disabled with firebase_analytics_collection_deactivated=1");
                z = false;
            } else if (b2 != null && !b2.booleanValue()) {
                q().A().a("Collection disabled with firebase_analytics_collection_enabled=0");
                z = false;
            } else if (b2 != null || !d.b()) {
                q().C().a("Collection enabled");
                z = true;
            } else {
                q().A().a("Collection disabled with google_app_measurement_enable=0");
                z = false;
            }
        } else {
            z = false;
        }
        this.i = "";
        this.g = 0;
        try {
            String a3 = d.a();
            if (TextUtils.isEmpty(a3)) {
                a3 = "";
            }
            this.i = a3;
            if (z) {
                q().C().a("App package, google app id", this.f3870a, this.i);
            }
        } catch (IllegalStateException e4) {
            q().v().a("getGoogleAppId or isMeasurementEnabled failed with exception. appId", az.a(packageName), e4);
        }
        if (VERSION.SDK_INT >= 16) {
            if (!a.a(k())) {
                i2 = 0;
            }
            this.h = i2;
            return;
        }
        this.h = 0;
    }

    /* access modifiers changed from: 0000 */
    public final String w() {
        F();
        return this.f3870a;
    }

    /* access modifiers changed from: 0000 */
    public final String x() {
        F();
        return this.i;
    }

    /* access modifiers changed from: 0000 */
    public final int y() {
        F();
        return this.c;
    }

    /* access modifiers changed from: 0000 */
    public final int z() {
        F();
        return this.h;
    }
}
