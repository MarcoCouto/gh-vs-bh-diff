package com.google.android.gms.internal.c;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application.ActivityLifecycleCallbacks;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;

@TargetApi(14)
final class dw implements ActivityLifecycleCallbacks {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ dd f4002a;

    private dw(dd ddVar) {
        this.f4002a = ddVar;
    }

    /* synthetic */ dw(dd ddVar, de deVar) {
        this(ddVar);
    }

    public final void onActivityCreated(Activity activity, Bundle bundle) {
        try {
            this.f4002a.q().C().a("onActivityCreated");
            Intent intent = activity.getIntent();
            if (intent != null) {
                Uri data = intent.getData();
                if (data != null && data.isHierarchical()) {
                    if (bundle == null) {
                        Bundle a2 = this.f4002a.n().a(data);
                        this.f4002a.n();
                        String str = ft.a(intent) ? "gs" : "auto";
                        if (a2 != null) {
                            this.f4002a.a(str, "_cmp", a2);
                        }
                    }
                    String queryParameter = data.getQueryParameter("referrer");
                    if (!TextUtils.isEmpty(queryParameter)) {
                        if (!(queryParameter.contains("gclid") && (queryParameter.contains("utm_campaign") || queryParameter.contains("utm_source") || queryParameter.contains("utm_medium") || queryParameter.contains("utm_term") || queryParameter.contains("utm_content")))) {
                            this.f4002a.q().B().a("Activity created with data 'referrer' param without gclid and at least one utm field");
                            return;
                        }
                        this.f4002a.q().B().a("Activity created with referrer", queryParameter);
                        if (!TextUtils.isEmpty(queryParameter)) {
                            this.f4002a.a("auto", "_ldl", (Object) queryParameter);
                        }
                    } else {
                        return;
                    }
                }
            }
        } catch (Exception e) {
            this.f4002a.q().v().a("Throwable caught in onActivityCreated", e);
        }
        this.f4002a.i().a(activity, bundle);
    }

    public final void onActivityDestroyed(Activity activity) {
        this.f4002a.i().c(activity);
    }

    public final void onActivityPaused(Activity activity) {
        this.f4002a.i().b(activity);
        fa o = this.f4002a.o();
        o.p().a((Runnable) new fe(o, o.j().b()));
    }

    public final void onActivityResumed(Activity activity) {
        this.f4002a.i().a(activity);
        fa o = this.f4002a.o();
        o.p().a((Runnable) new fd(o, o.j().b()));
    }

    public final void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        this.f4002a.i().b(activity, bundle);
    }

    public final void onActivityStarted(Activity activity) {
    }

    public final void onActivityStopped(Activity activity) {
    }
}
