package com.google.android.gms.internal.c;

import android.os.Handler;
import com.google.android.gms.common.internal.aa;

abstract class af {

    /* renamed from: b reason: collision with root package name */
    private static volatile Handler f3851b;

    /* renamed from: a reason: collision with root package name */
    private final db f3852a;
    private final Runnable c;
    /* access modifiers changed from: private */
    public volatile long d;

    af(db dbVar) {
        aa.a(dbVar);
        this.f3852a = dbVar;
        this.c = new ag(this, dbVar);
    }

    private final Handler d() {
        Handler handler;
        if (f3851b != null) {
            return f3851b;
        }
        synchronized (af.class) {
            if (f3851b == null) {
                f3851b = new Handler(this.f3852a.k().getMainLooper());
            }
            handler = f3851b;
        }
        return handler;
    }

    public abstract void a();

    public final void a(long j) {
        c();
        if (j >= 0) {
            this.d = this.f3852a.j().a();
            if (!d().postDelayed(this.c, j)) {
                this.f3852a.q().v().a("Failed to schedule delayed post. time", Long.valueOf(j));
            }
        }
    }

    public final boolean b() {
        return this.d != 0;
    }

    /* access modifiers changed from: 0000 */
    public final void c() {
        this.d = 0;
        d().removeCallbacks(this.c);
    }
}
