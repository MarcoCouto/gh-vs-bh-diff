package com.google.android.gms.internal.c;

import android.os.Binder;
import android.text.TextUtils;
import com.google.android.gms.common.internal.aa;
import com.google.android.gms.common.n;
import com.google.android.gms.common.o;
import com.google.android.gms.common.util.t;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;

public final class cg extends as {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public final fk f3924a;

    /* renamed from: b reason: collision with root package name */
    private Boolean f3925b;
    private String c;

    public cg(fk fkVar) {
        this(fkVar, null);
    }

    private cg(fk fkVar, String str) {
        aa.a(fkVar);
        this.f3924a = fkVar;
        this.c = null;
    }

    private final void a(Runnable runnable) {
        aa.a(runnable);
        if (!((Boolean) ap.T.b()).booleanValue() || !this.f3924a.p().w()) {
            this.f3924a.p().a(runnable);
        } else {
            runnable.run();
        }
    }

    private final void a(String str, boolean z) {
        boolean z2 = false;
        if (TextUtils.isEmpty(str)) {
            this.f3924a.q().v().a("Measurement Service called without app package");
            throw new SecurityException("Measurement Service called without app package");
        }
        if (z) {
            try {
                if (this.f3925b == null) {
                    if ("com.google.android.gms".equals(this.c) || t.a(this.f3924a.k(), Binder.getCallingUid()) || o.a(this.f3924a.k()).a(Binder.getCallingUid())) {
                        z2 = true;
                    }
                    this.f3925b = Boolean.valueOf(z2);
                }
                if (this.f3925b.booleanValue()) {
                    return;
                }
            } catch (SecurityException e) {
                this.f3924a.q().v().a("Measurement Service called with invalid calling package. appId", az.a(str));
                throw e;
            }
        }
        if (this.c == null && n.uidHasPackageName(this.f3924a.k(), Binder.getCallingUid(), str)) {
            this.c = str;
        }
        if (!str.equals(this.c)) {
            throw new SecurityException(String.format("Unknown calling package name '%s'.", new Object[]{str}));
        }
    }

    private final void b(s sVar, boolean z) {
        aa.a(sVar);
        a(sVar.f4127a, false);
        this.f3924a.m().e(sVar.f4128b);
    }

    public final List<fq> a(s sVar, boolean z) {
        b(sVar, false);
        try {
            List<fs> list = (List) this.f3924a.p().a((Callable<V>) new cw<V>(this, sVar)).get();
            ArrayList arrayList = new ArrayList(list.size());
            for (fs fsVar : list) {
                if (z || !ft.h(fsVar.c)) {
                    arrayList.add(new fq(fsVar));
                }
            }
            return arrayList;
        } catch (InterruptedException | ExecutionException e) {
            this.f3924a.q().v().a("Failed to get user attributes. appId", az.a(sVar.f4127a), e);
            return null;
        }
    }

    public final List<w> a(String str, String str2, s sVar) {
        b(sVar, false);
        try {
            return (List) this.f3924a.p().a((Callable<V>) new co<V>(this, sVar, str, str2)).get();
        } catch (InterruptedException | ExecutionException e) {
            this.f3924a.q().v().a("Failed to get conditional user properties", e);
            return Collections.emptyList();
        }
    }

    public final List<w> a(String str, String str2, String str3) {
        a(str, true);
        try {
            return (List) this.f3924a.p().a((Callable<V>) new cp<V>(this, str, str2, str3)).get();
        } catch (InterruptedException | ExecutionException e) {
            this.f3924a.q().v().a("Failed to get conditional user properties", e);
            return Collections.emptyList();
        }
    }

    public final List<fq> a(String str, String str2, String str3, boolean z) {
        a(str, true);
        try {
            List<fs> list = (List) this.f3924a.p().a((Callable<V>) new cn<V>(this, str, str2, str3)).get();
            ArrayList arrayList = new ArrayList(list.size());
            for (fs fsVar : list) {
                if (z || !ft.h(fsVar.c)) {
                    arrayList.add(new fq(fsVar));
                }
            }
            return arrayList;
        } catch (InterruptedException | ExecutionException e) {
            this.f3924a.q().v().a("Failed to get user attributes. appId", az.a(str), e);
            return Collections.emptyList();
        }
    }

    public final List<fq> a(String str, String str2, boolean z, s sVar) {
        b(sVar, false);
        try {
            List<fs> list = (List) this.f3924a.p().a((Callable<V>) new cm<V>(this, sVar, str, str2)).get();
            ArrayList arrayList = new ArrayList(list.size());
            for (fs fsVar : list) {
                if (z || !ft.h(fsVar.c)) {
                    arrayList.add(new fq(fsVar));
                }
            }
            return arrayList;
        } catch (InterruptedException | ExecutionException e) {
            this.f3924a.q().v().a("Failed to get user attributes. appId", az.a(sVar.f4127a), e);
            return Collections.emptyList();
        }
    }

    public final void a(long j, String str, String str2, String str3) {
        a((Runnable) new cy(this, str2, str3, str, j));
    }

    public final void a(an anVar, s sVar) {
        aa.a(anVar);
        b(sVar, false);
        a((Runnable) new cr(this, anVar, sVar));
    }

    public final void a(an anVar, String str, String str2) {
        aa.a(anVar);
        aa.a(str);
        a(str, true);
        a((Runnable) new cs(this, anVar, str));
    }

    public final void a(fq fqVar, s sVar) {
        aa.a(fqVar);
        b(sVar, false);
        if (fqVar.a() == null) {
            a((Runnable) new cu(this, fqVar, sVar));
        } else {
            a((Runnable) new cv(this, fqVar, sVar));
        }
    }

    public final void a(s sVar) {
        b(sVar, false);
        a((Runnable) new cx(this, sVar));
    }

    public final void a(w wVar) {
        aa.a(wVar);
        aa.a(wVar.c);
        a(wVar.f4129a, true);
        w wVar2 = new w(wVar);
        if (wVar.c.a() == null) {
            a((Runnable) new ck(this, wVar2));
        } else {
            a((Runnable) new cl(this, wVar2));
        }
    }

    public final void a(w wVar, s sVar) {
        aa.a(wVar);
        aa.a(wVar.c);
        b(sVar, false);
        w wVar2 = new w(wVar);
        wVar2.f4129a = sVar.f4127a;
        if (wVar.c.a() == null) {
            a((Runnable) new ci(this, wVar2, sVar));
        } else {
            a((Runnable) new cj(this, wVar2, sVar));
        }
    }

    public final byte[] a(an anVar, String str) {
        aa.a(str);
        aa.a(anVar);
        a(str, true);
        this.f3924a.q().B().a("Log and bundle. event", this.f3924a.n().a(anVar.f3864a));
        long c2 = this.f3924a.j().c() / 1000000;
        try {
            byte[] bArr = (byte[]) this.f3924a.p().b((Callable<V>) new ct<V>(this, anVar, str)).get();
            if (bArr == null) {
                this.f3924a.q().v().a("Log and bundle returned null. appId", az.a(str));
                bArr = new byte[0];
            }
            this.f3924a.q().B().a("Log and bundle processed. event, size, time_ms", this.f3924a.n().a(anVar.f3864a), Integer.valueOf(bArr.length), Long.valueOf((this.f3924a.j().c() / 1000000) - c2));
            return bArr;
        } catch (InterruptedException | ExecutionException e) {
            this.f3924a.q().v().a("Failed to log and bundle. appId, event, error", az.a(str), this.f3924a.n().a(anVar.f3864a), e);
            return null;
        }
    }

    public final void b(s sVar) {
        b(sVar, false);
        a((Runnable) new ch(this, sVar));
    }

    public final String c(s sVar) {
        b(sVar, false);
        return this.f3924a.d(sVar);
    }

    public final void d(s sVar) {
        a(sVar.f4127a, false);
        a((Runnable) new cq(this, sVar));
    }
}
