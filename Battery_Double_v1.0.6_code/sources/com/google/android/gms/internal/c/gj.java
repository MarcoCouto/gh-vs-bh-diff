package com.google.android.gms.internal.c;

import java.io.IOException;

public final class gj extends d<gj> {
    private static volatile gj[] K;
    public String A;
    public Boolean B;
    public gf[] C;
    public String D;
    public Integer E;
    public String F;
    public Long G;
    public Long H;
    public String I;
    public Integer J;
    private Integer L;
    private Integer M;
    private String N;
    public Integer c;
    public gg[] d;
    public gl[] e;
    public Long f;
    public Long g;
    public Long h;
    public Long i;
    public Long j;
    public String k;
    public String l;
    public String m;
    public String n;
    public Integer o;
    public String p;
    public String q;
    public String r;
    public Long s;
    public Long t;
    public String u;
    public Boolean v;
    public String w;
    public Long x;
    public Integer y;
    public String z;

    public gj() {
        this.c = null;
        this.d = gg.e();
        this.e = gl.e();
        this.f = null;
        this.g = null;
        this.h = null;
        this.i = null;
        this.j = null;
        this.k = null;
        this.l = null;
        this.m = null;
        this.n = null;
        this.o = null;
        this.p = null;
        this.q = null;
        this.r = null;
        this.s = null;
        this.t = null;
        this.u = null;
        this.v = null;
        this.w = null;
        this.x = null;
        this.y = null;
        this.z = null;
        this.A = null;
        this.B = null;
        this.C = gf.e();
        this.D = null;
        this.E = null;
        this.L = null;
        this.M = null;
        this.F = null;
        this.G = null;
        this.H = null;
        this.I = null;
        this.N = null;
        this.J = null;
        this.f3962a = null;
        this.f4112b = -1;
    }

    public static gj[] e() {
        if (K == null) {
            synchronized (h.f4106b) {
                if (K == null) {
                    K = new gj[0];
                }
            }
        }
        return K;
    }

    /* access modifiers changed from: protected */
    public final int a() {
        int a2 = super.a();
        if (this.c != null) {
            a2 += b.b(1, this.c.intValue());
        }
        if (this.d != null && this.d.length > 0) {
            int i2 = a2;
            for (gg ggVar : this.d) {
                if (ggVar != null) {
                    i2 += b.b(2, (j) ggVar);
                }
            }
            a2 = i2;
        }
        if (this.e != null && this.e.length > 0) {
            int i3 = a2;
            for (gl glVar : this.e) {
                if (glVar != null) {
                    i3 += b.b(3, (j) glVar);
                }
            }
            a2 = i3;
        }
        if (this.f != null) {
            a2 += b.c(4, this.f.longValue());
        }
        if (this.g != null) {
            a2 += b.c(5, this.g.longValue());
        }
        if (this.h != null) {
            a2 += b.c(6, this.h.longValue());
        }
        if (this.j != null) {
            a2 += b.c(7, this.j.longValue());
        }
        if (this.k != null) {
            a2 += b.b(8, this.k);
        }
        if (this.l != null) {
            a2 += b.b(9, this.l);
        }
        if (this.m != null) {
            a2 += b.b(10, this.m);
        }
        if (this.n != null) {
            a2 += b.b(11, this.n);
        }
        if (this.o != null) {
            a2 += b.b(12, this.o.intValue());
        }
        if (this.p != null) {
            a2 += b.b(13, this.p);
        }
        if (this.q != null) {
            a2 += b.b(14, this.q);
        }
        if (this.r != null) {
            a2 += b.b(16, this.r);
        }
        if (this.s != null) {
            a2 += b.c(17, this.s.longValue());
        }
        if (this.t != null) {
            a2 += b.c(18, this.t.longValue());
        }
        if (this.u != null) {
            a2 += b.b(19, this.u);
        }
        if (this.v != null) {
            this.v.booleanValue();
            a2 += b.b(20) + 1;
        }
        if (this.w != null) {
            a2 += b.b(21, this.w);
        }
        if (this.x != null) {
            a2 += b.c(22, this.x.longValue());
        }
        if (this.y != null) {
            a2 += b.b(23, this.y.intValue());
        }
        if (this.z != null) {
            a2 += b.b(24, this.z);
        }
        if (this.A != null) {
            a2 += b.b(25, this.A);
        }
        if (this.i != null) {
            a2 += b.c(26, this.i.longValue());
        }
        if (this.B != null) {
            this.B.booleanValue();
            a2 += b.b(28) + 1;
        }
        if (this.C != null && this.C.length > 0) {
            for (gf gfVar : this.C) {
                if (gfVar != null) {
                    a2 += b.b(29, (j) gfVar);
                }
            }
        }
        if (this.D != null) {
            a2 += b.b(30, this.D);
        }
        if (this.E != null) {
            a2 += b.b(31, this.E.intValue());
        }
        if (this.L != null) {
            a2 += b.b(32, this.L.intValue());
        }
        if (this.M != null) {
            a2 += b.b(33, this.M.intValue());
        }
        if (this.F != null) {
            a2 += b.b(34, this.F);
        }
        if (this.G != null) {
            a2 += b.c(35, this.G.longValue());
        }
        if (this.H != null) {
            a2 += b.c(36, this.H.longValue());
        }
        if (this.I != null) {
            a2 += b.b(37, this.I);
        }
        if (this.N != null) {
            a2 += b.b(38, this.N);
        }
        return this.J != null ? a2 + b.b(39, this.J.intValue()) : a2;
    }

    public final /* synthetic */ j a(a aVar) throws IOException {
        while (true) {
            int a2 = aVar.a();
            switch (a2) {
                case 0:
                    break;
                case 8:
                    this.c = Integer.valueOf(aVar.d());
                    continue;
                case 18:
                    int a3 = m.a(aVar, 18);
                    int length = this.d == null ? 0 : this.d.length;
                    gg[] ggVarArr = new gg[(a3 + length)];
                    if (length != 0) {
                        System.arraycopy(this.d, 0, ggVarArr, 0, length);
                    }
                    while (length < ggVarArr.length - 1) {
                        ggVarArr[length] = new gg();
                        aVar.a((j) ggVarArr[length]);
                        aVar.a();
                        length++;
                    }
                    ggVarArr[length] = new gg();
                    aVar.a((j) ggVarArr[length]);
                    this.d = ggVarArr;
                    continue;
                case 26:
                    int a4 = m.a(aVar, 26);
                    int length2 = this.e == null ? 0 : this.e.length;
                    gl[] glVarArr = new gl[(a4 + length2)];
                    if (length2 != 0) {
                        System.arraycopy(this.e, 0, glVarArr, 0, length2);
                    }
                    while (length2 < glVarArr.length - 1) {
                        glVarArr[length2] = new gl();
                        aVar.a((j) glVarArr[length2]);
                        aVar.a();
                        length2++;
                    }
                    glVarArr[length2] = new gl();
                    aVar.a((j) glVarArr[length2]);
                    this.e = glVarArr;
                    continue;
                case 32:
                    this.f = Long.valueOf(aVar.e());
                    continue;
                case 40:
                    this.g = Long.valueOf(aVar.e());
                    continue;
                case 48:
                    this.h = Long.valueOf(aVar.e());
                    continue;
                case 56:
                    this.j = Long.valueOf(aVar.e());
                    continue;
                case 66:
                    this.k = aVar.c();
                    continue;
                case 74:
                    this.l = aVar.c();
                    continue;
                case 82:
                    this.m = aVar.c();
                    continue;
                case 90:
                    this.n = aVar.c();
                    continue;
                case 96:
                    this.o = Integer.valueOf(aVar.d());
                    continue;
                case 106:
                    this.p = aVar.c();
                    continue;
                case 114:
                    this.q = aVar.c();
                    continue;
                case 130:
                    this.r = aVar.c();
                    continue;
                case 136:
                    this.s = Long.valueOf(aVar.e());
                    continue;
                case 144:
                    this.t = Long.valueOf(aVar.e());
                    continue;
                case 154:
                    this.u = aVar.c();
                    continue;
                case 160:
                    this.v = Boolean.valueOf(aVar.b());
                    continue;
                case 170:
                    this.w = aVar.c();
                    continue;
                case 176:
                    this.x = Long.valueOf(aVar.e());
                    continue;
                case 184:
                    this.y = Integer.valueOf(aVar.d());
                    continue;
                case 194:
                    this.z = aVar.c();
                    continue;
                case 202:
                    this.A = aVar.c();
                    continue;
                case 208:
                    this.i = Long.valueOf(aVar.e());
                    continue;
                case 224:
                    this.B = Boolean.valueOf(aVar.b());
                    continue;
                case 234:
                    int a5 = m.a(aVar, 234);
                    int length3 = this.C == null ? 0 : this.C.length;
                    gf[] gfVarArr = new gf[(a5 + length3)];
                    if (length3 != 0) {
                        System.arraycopy(this.C, 0, gfVarArr, 0, length3);
                    }
                    while (length3 < gfVarArr.length - 1) {
                        gfVarArr[length3] = new gf();
                        aVar.a((j) gfVarArr[length3]);
                        aVar.a();
                        length3++;
                    }
                    gfVarArr[length3] = new gf();
                    aVar.a((j) gfVarArr[length3]);
                    this.C = gfVarArr;
                    continue;
                case 242:
                    this.D = aVar.c();
                    continue;
                case 248:
                    this.E = Integer.valueOf(aVar.d());
                    continue;
                case 256:
                    this.L = Integer.valueOf(aVar.d());
                    continue;
                case 264:
                    this.M = Integer.valueOf(aVar.d());
                    continue;
                case 274:
                    this.F = aVar.c();
                    continue;
                case 280:
                    this.G = Long.valueOf(aVar.e());
                    continue;
                case 288:
                    this.H = Long.valueOf(aVar.e());
                    continue;
                case 298:
                    this.I = aVar.c();
                    continue;
                case 306:
                    this.N = aVar.c();
                    continue;
                case 312:
                    this.J = Integer.valueOf(aVar.d());
                    continue;
                default:
                    if (!super.a(aVar, a2)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    public final void a(b bVar) throws IOException {
        if (this.c != null) {
            bVar.a(1, this.c.intValue());
        }
        if (this.d != null && this.d.length > 0) {
            for (gg ggVar : this.d) {
                if (ggVar != null) {
                    bVar.a(2, (j) ggVar);
                }
            }
        }
        if (this.e != null && this.e.length > 0) {
            for (gl glVar : this.e) {
                if (glVar != null) {
                    bVar.a(3, (j) glVar);
                }
            }
        }
        if (this.f != null) {
            bVar.b(4, this.f.longValue());
        }
        if (this.g != null) {
            bVar.b(5, this.g.longValue());
        }
        if (this.h != null) {
            bVar.b(6, this.h.longValue());
        }
        if (this.j != null) {
            bVar.b(7, this.j.longValue());
        }
        if (this.k != null) {
            bVar.a(8, this.k);
        }
        if (this.l != null) {
            bVar.a(9, this.l);
        }
        if (this.m != null) {
            bVar.a(10, this.m);
        }
        if (this.n != null) {
            bVar.a(11, this.n);
        }
        if (this.o != null) {
            bVar.a(12, this.o.intValue());
        }
        if (this.p != null) {
            bVar.a(13, this.p);
        }
        if (this.q != null) {
            bVar.a(14, this.q);
        }
        if (this.r != null) {
            bVar.a(16, this.r);
        }
        if (this.s != null) {
            bVar.b(17, this.s.longValue());
        }
        if (this.t != null) {
            bVar.b(18, this.t.longValue());
        }
        if (this.u != null) {
            bVar.a(19, this.u);
        }
        if (this.v != null) {
            bVar.a(20, this.v.booleanValue());
        }
        if (this.w != null) {
            bVar.a(21, this.w);
        }
        if (this.x != null) {
            bVar.b(22, this.x.longValue());
        }
        if (this.y != null) {
            bVar.a(23, this.y.intValue());
        }
        if (this.z != null) {
            bVar.a(24, this.z);
        }
        if (this.A != null) {
            bVar.a(25, this.A);
        }
        if (this.i != null) {
            bVar.b(26, this.i.longValue());
        }
        if (this.B != null) {
            bVar.a(28, this.B.booleanValue());
        }
        if (this.C != null && this.C.length > 0) {
            for (gf gfVar : this.C) {
                if (gfVar != null) {
                    bVar.a(29, (j) gfVar);
                }
            }
        }
        if (this.D != null) {
            bVar.a(30, this.D);
        }
        if (this.E != null) {
            bVar.a(31, this.E.intValue());
        }
        if (this.L != null) {
            bVar.a(32, this.L.intValue());
        }
        if (this.M != null) {
            bVar.a(33, this.M.intValue());
        }
        if (this.F != null) {
            bVar.a(34, this.F);
        }
        if (this.G != null) {
            bVar.b(35, this.G.longValue());
        }
        if (this.H != null) {
            bVar.b(36, this.H.longValue());
        }
        if (this.I != null) {
            bVar.a(37, this.I);
        }
        if (this.N != null) {
            bVar.a(38, this.N);
        }
        if (this.J != null) {
            bVar.a(39, this.J.intValue());
        }
        super.a(bVar);
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof gj)) {
            return false;
        }
        gj gjVar = (gj) obj;
        if (this.c == null) {
            if (gjVar.c != null) {
                return false;
            }
        } else if (!this.c.equals(gjVar.c)) {
            return false;
        }
        if (!h.a((Object[]) this.d, (Object[]) gjVar.d)) {
            return false;
        }
        if (!h.a((Object[]) this.e, (Object[]) gjVar.e)) {
            return false;
        }
        if (this.f == null) {
            if (gjVar.f != null) {
                return false;
            }
        } else if (!this.f.equals(gjVar.f)) {
            return false;
        }
        if (this.g == null) {
            if (gjVar.g != null) {
                return false;
            }
        } else if (!this.g.equals(gjVar.g)) {
            return false;
        }
        if (this.h == null) {
            if (gjVar.h != null) {
                return false;
            }
        } else if (!this.h.equals(gjVar.h)) {
            return false;
        }
        if (this.i == null) {
            if (gjVar.i != null) {
                return false;
            }
        } else if (!this.i.equals(gjVar.i)) {
            return false;
        }
        if (this.j == null) {
            if (gjVar.j != null) {
                return false;
            }
        } else if (!this.j.equals(gjVar.j)) {
            return false;
        }
        if (this.k == null) {
            if (gjVar.k != null) {
                return false;
            }
        } else if (!this.k.equals(gjVar.k)) {
            return false;
        }
        if (this.l == null) {
            if (gjVar.l != null) {
                return false;
            }
        } else if (!this.l.equals(gjVar.l)) {
            return false;
        }
        if (this.m == null) {
            if (gjVar.m != null) {
                return false;
            }
        } else if (!this.m.equals(gjVar.m)) {
            return false;
        }
        if (this.n == null) {
            if (gjVar.n != null) {
                return false;
            }
        } else if (!this.n.equals(gjVar.n)) {
            return false;
        }
        if (this.o == null) {
            if (gjVar.o != null) {
                return false;
            }
        } else if (!this.o.equals(gjVar.o)) {
            return false;
        }
        if (this.p == null) {
            if (gjVar.p != null) {
                return false;
            }
        } else if (!this.p.equals(gjVar.p)) {
            return false;
        }
        if (this.q == null) {
            if (gjVar.q != null) {
                return false;
            }
        } else if (!this.q.equals(gjVar.q)) {
            return false;
        }
        if (this.r == null) {
            if (gjVar.r != null) {
                return false;
            }
        } else if (!this.r.equals(gjVar.r)) {
            return false;
        }
        if (this.s == null) {
            if (gjVar.s != null) {
                return false;
            }
        } else if (!this.s.equals(gjVar.s)) {
            return false;
        }
        if (this.t == null) {
            if (gjVar.t != null) {
                return false;
            }
        } else if (!this.t.equals(gjVar.t)) {
            return false;
        }
        if (this.u == null) {
            if (gjVar.u != null) {
                return false;
            }
        } else if (!this.u.equals(gjVar.u)) {
            return false;
        }
        if (this.v == null) {
            if (gjVar.v != null) {
                return false;
            }
        } else if (!this.v.equals(gjVar.v)) {
            return false;
        }
        if (this.w == null) {
            if (gjVar.w != null) {
                return false;
            }
        } else if (!this.w.equals(gjVar.w)) {
            return false;
        }
        if (this.x == null) {
            if (gjVar.x != null) {
                return false;
            }
        } else if (!this.x.equals(gjVar.x)) {
            return false;
        }
        if (this.y == null) {
            if (gjVar.y != null) {
                return false;
            }
        } else if (!this.y.equals(gjVar.y)) {
            return false;
        }
        if (this.z == null) {
            if (gjVar.z != null) {
                return false;
            }
        } else if (!this.z.equals(gjVar.z)) {
            return false;
        }
        if (this.A == null) {
            if (gjVar.A != null) {
                return false;
            }
        } else if (!this.A.equals(gjVar.A)) {
            return false;
        }
        if (this.B == null) {
            if (gjVar.B != null) {
                return false;
            }
        } else if (!this.B.equals(gjVar.B)) {
            return false;
        }
        if (!h.a((Object[]) this.C, (Object[]) gjVar.C)) {
            return false;
        }
        if (this.D == null) {
            if (gjVar.D != null) {
                return false;
            }
        } else if (!this.D.equals(gjVar.D)) {
            return false;
        }
        if (this.E == null) {
            if (gjVar.E != null) {
                return false;
            }
        } else if (!this.E.equals(gjVar.E)) {
            return false;
        }
        if (this.L == null) {
            if (gjVar.L != null) {
                return false;
            }
        } else if (!this.L.equals(gjVar.L)) {
            return false;
        }
        if (this.M == null) {
            if (gjVar.M != null) {
                return false;
            }
        } else if (!this.M.equals(gjVar.M)) {
            return false;
        }
        if (this.F == null) {
            if (gjVar.F != null) {
                return false;
            }
        } else if (!this.F.equals(gjVar.F)) {
            return false;
        }
        if (this.G == null) {
            if (gjVar.G != null) {
                return false;
            }
        } else if (!this.G.equals(gjVar.G)) {
            return false;
        }
        if (this.H == null) {
            if (gjVar.H != null) {
                return false;
            }
        } else if (!this.H.equals(gjVar.H)) {
            return false;
        }
        if (this.I == null) {
            if (gjVar.I != null) {
                return false;
            }
        } else if (!this.I.equals(gjVar.I)) {
            return false;
        }
        if (this.N == null) {
            if (gjVar.N != null) {
                return false;
            }
        } else if (!this.N.equals(gjVar.N)) {
            return false;
        }
        if (this.J == null) {
            if (gjVar.J != null) {
                return false;
            }
        } else if (!this.J.equals(gjVar.J)) {
            return false;
        }
        return (this.f3962a == null || this.f3962a.b()) ? gjVar.f3962a == null || gjVar.f3962a.b() : this.f3962a.equals(gjVar.f3962a);
    }

    public final int hashCode() {
        int i2 = 0;
        int hashCode = ((this.J == null ? 0 : this.J.hashCode()) + (((this.N == null ? 0 : this.N.hashCode()) + (((this.I == null ? 0 : this.I.hashCode()) + (((this.H == null ? 0 : this.H.hashCode()) + (((this.G == null ? 0 : this.G.hashCode()) + (((this.F == null ? 0 : this.F.hashCode()) + (((this.M == null ? 0 : this.M.hashCode()) + (((this.L == null ? 0 : this.L.hashCode()) + (((this.E == null ? 0 : this.E.hashCode()) + (((this.D == null ? 0 : this.D.hashCode()) + (((((this.B == null ? 0 : this.B.hashCode()) + (((this.A == null ? 0 : this.A.hashCode()) + (((this.z == null ? 0 : this.z.hashCode()) + (((this.y == null ? 0 : this.y.hashCode()) + (((this.x == null ? 0 : this.x.hashCode()) + (((this.w == null ? 0 : this.w.hashCode()) + (((this.v == null ? 0 : this.v.hashCode()) + (((this.u == null ? 0 : this.u.hashCode()) + (((this.t == null ? 0 : this.t.hashCode()) + (((this.s == null ? 0 : this.s.hashCode()) + (((this.r == null ? 0 : this.r.hashCode()) + (((this.q == null ? 0 : this.q.hashCode()) + (((this.p == null ? 0 : this.p.hashCode()) + (((this.o == null ? 0 : this.o.hashCode()) + (((this.n == null ? 0 : this.n.hashCode()) + (((this.m == null ? 0 : this.m.hashCode()) + (((this.l == null ? 0 : this.l.hashCode()) + (((this.k == null ? 0 : this.k.hashCode()) + (((this.j == null ? 0 : this.j.hashCode()) + (((this.i == null ? 0 : this.i.hashCode()) + (((this.h == null ? 0 : this.h.hashCode()) + (((this.g == null ? 0 : this.g.hashCode()) + (((this.f == null ? 0 : this.f.hashCode()) + (((((((this.c == null ? 0 : this.c.hashCode()) + ((getClass().getName().hashCode() + 527) * 31)) * 31) + h.a((Object[]) this.d)) * 31) + h.a((Object[]) this.e)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31) + h.a((Object[]) this.C)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31;
        if (this.f3962a != null && !this.f3962a.b()) {
            i2 = this.f3962a.hashCode();
        }
        return hashCode + i2;
    }
}
