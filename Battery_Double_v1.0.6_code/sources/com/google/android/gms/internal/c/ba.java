package com.google.android.gms.internal.c;

final class ba implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ int f3880a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ String f3881b;
    private final /* synthetic */ Object c;
    private final /* synthetic */ Object d;
    private final /* synthetic */ Object e;
    private final /* synthetic */ az f;

    ba(az azVar, int i, String str, Object obj, Object obj2, Object obj3) {
        this.f = azVar;
        this.f3880a = i;
        this.f3881b = str;
        this.c = obj;
        this.d = obj2;
        this.e = obj3;
    }

    public final void run() {
        bk c2 = this.f.q.c();
        if (!c2.E()) {
            this.f.a(6, "Persisted config not initialized. Not logging error/warn");
            return;
        }
        if (this.f.f3877a == 0) {
            if (this.f.s().t()) {
                this.f.f3877a = 'C';
            } else {
                this.f.f3877a = 'c';
            }
        }
        if (this.f.f3878b < 0) {
            this.f.f3878b = 12451;
        }
        char charAt = "01VDIWEA?".charAt(this.f3880a);
        char a2 = this.f.f3877a;
        long b2 = this.f.f3878b;
        String a3 = az.a(true, this.f3881b, this.c, this.d, this.e);
        String sb = new StringBuilder(String.valueOf(a3).length() + 24).append("2").append(charAt).append(a2).append(b2).append(":").append(a3).toString();
        if (sb.length() > 1024) {
            sb = this.f3881b.substring(0, 1024);
        }
        c2.f3895b.a(sb, 1);
    }
}
