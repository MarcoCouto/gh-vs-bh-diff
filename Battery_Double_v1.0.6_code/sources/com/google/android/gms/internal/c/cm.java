package com.google.android.gms.internal.c;

import java.util.List;
import java.util.concurrent.Callable;

final class cm implements Callable<List<fs>> {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ s f3936a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ String f3937b;
    private final /* synthetic */ String c;
    private final /* synthetic */ cg d;

    cm(cg cgVar, s sVar, String str, String str2) {
        this.d = cgVar;
        this.f3936a = sVar;
        this.f3937b = str;
        this.c = str2;
    }

    public final /* synthetic */ Object call() throws Exception {
        this.d.f3924a.H();
        return this.d.f3924a.D().a(this.f3936a.f4127a, this.f3937b, this.c);
    }
}
