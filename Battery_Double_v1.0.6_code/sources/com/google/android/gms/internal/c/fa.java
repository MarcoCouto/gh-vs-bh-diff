package com.google.android.gms.internal.c;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import com.google.android.gms.common.util.e;

public final class fa extends da {

    /* renamed from: a reason: collision with root package name */
    private Handler f4058a;

    /* renamed from: b reason: collision with root package name */
    private long f4059b = j().b();
    private final af c = new fb(this, this.q);
    private final af d = new fc(this, this.q);

    fa(ce ceVar) {
        super(ceVar);
    }

    /* access modifiers changed from: private */
    public final void a(long j) {
        c();
        w();
        this.c.c();
        this.d.c();
        q().C().a("Activity resumed, time", Long.valueOf(j));
        this.f4059b = j;
        if (j().a() - r().l.a() > r().n.a()) {
            r().m.a(true);
            r().o.a(0);
        }
        if (r().m.a()) {
            this.c.a(Math.max(0, r().k.a() - r().o.a()));
        } else {
            this.d.a(Math.max(0, 3600000 - r().o.a()));
        }
    }

    /* access modifiers changed from: private */
    public final void b(long j) {
        c();
        w();
        this.c.c();
        this.d.c();
        q().C().a("Activity paused, time", Long.valueOf(j));
        if (this.f4059b != 0) {
            r().o.a(r().o.a() + (j - this.f4059b));
        }
    }

    private final void w() {
        synchronized (this) {
            if (this.f4058a == null) {
                this.f4058a = new Handler(Looper.getMainLooper());
            }
        }
    }

    /* access modifiers changed from: private */
    public final void x() {
        c();
        a(false);
        d().a(j().b());
    }

    public final /* bridge */ /* synthetic */ void a() {
        super.a();
    }

    public final boolean a(boolean z) {
        c();
        F();
        long b2 = j().b();
        r().n.a(j().a());
        long j = b2 - this.f4059b;
        if (z || j >= 1000) {
            r().o.a(j);
            q().C().a("Recording user engagement, ms", Long.valueOf(j));
            Bundle bundle = new Bundle();
            bundle.putLong("_et", j);
            dy.a(i().v(), bundle, true);
            e().a("auto", "_e", bundle);
            this.f4059b = b2;
            this.d.c();
            this.d.a(Math.max(0, 3600000 - r().o.a()));
            return true;
        }
        q().C().a("Screen exposed for less than 1000 ms. Event not sent. time", Long.valueOf(j));
        return false;
    }

    public final /* bridge */ /* synthetic */ void b() {
        super.b();
    }

    public final /* bridge */ /* synthetic */ void c() {
        super.c();
    }

    public final /* bridge */ /* synthetic */ n d() {
        return super.d();
    }

    public final /* bridge */ /* synthetic */ dd e() {
        return super.e();
    }

    public final /* bridge */ /* synthetic */ au f() {
        return super.f();
    }

    public final /* bridge */ /* synthetic */ ah g() {
        return super.g();
    }

    public final /* bridge */ /* synthetic */ eb h() {
        return super.h();
    }

    public final /* bridge */ /* synthetic */ dy i() {
        return super.i();
    }

    public final /* bridge */ /* synthetic */ e j() {
        return super.j();
    }

    public final /* bridge */ /* synthetic */ Context k() {
        return super.k();
    }

    public final /* bridge */ /* synthetic */ av l() {
        return super.l();
    }

    public final /* bridge */ /* synthetic */ ax m() {
        return super.m();
    }

    public final /* bridge */ /* synthetic */ ft n() {
        return super.n();
    }

    public final /* bridge */ /* synthetic */ fa o() {
        return super.o();
    }

    public final /* bridge */ /* synthetic */ bz p() {
        return super.p();
    }

    public final /* bridge */ /* synthetic */ az q() {
        return super.q();
    }

    public final /* bridge */ /* synthetic */ bk r() {
        return super.r();
    }

    public final /* bridge */ /* synthetic */ y s() {
        return super.s();
    }

    /* access modifiers changed from: protected */
    public final boolean t() {
        return false;
    }

    /* access modifiers changed from: 0000 */
    public final void v() {
        this.c.c();
        this.d.c();
        this.f4059b = 0;
    }
}
