package com.google.android.gms.internal.c;

import com.google.android.gms.common.internal.aa;

class fi extends cz implements v {

    /* renamed from: a reason: collision with root package name */
    protected final fk f4071a;

    fi(fk fkVar) {
        super(fkVar.J());
        aa.a(fkVar);
        this.f4071a = fkVar;
    }

    public ab t_() {
        return this.f4071a.D();
    }

    public u u_() {
        return this.f4071a.E();
    }
}
