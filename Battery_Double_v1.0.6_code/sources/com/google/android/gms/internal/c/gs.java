package com.google.android.gms.internal.c;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;

public final class gs extends gm implements gq {
    gs(IBinder iBinder) {
        super(iBinder, "com.google.android.finsky.externalreferrer.IGetInstallReferrerService");
    }

    public final Bundle a(Bundle bundle) throws RemoteException {
        Parcel a2 = a();
        go.a(a2, (Parcelable) bundle);
        Parcel a3 = a(1, a2);
        Bundle bundle2 = (Bundle) go.a(a3, Bundle.CREATOR);
        a3.recycle();
        return bundle2;
    }
}
