package com.google.android.gms.internal.c;

final class eq implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ ar f4041a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ ep f4042b;

    eq(ep epVar, ar arVar) {
        this.f4042b = epVar;
        this.f4041a = arVar;
    }

    public final void run() {
        synchronized (this.f4042b) {
            this.f4042b.f4040b = false;
            if (!this.f4042b.f4039a.v()) {
                this.f4042b.f4039a.q().C().a("Connected to service");
                this.f4042b.f4039a.a(this.f4041a);
            }
        }
    }
}
