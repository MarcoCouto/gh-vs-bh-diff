package com.google.android.gms.internal.c;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.text.TextUtils;
import com.google.android.gms.common.b.c;
import com.google.android.gms.common.internal.aa;
import com.google.android.gms.common.util.e;
import com.google.android.gms.common.util.p;
import java.lang.reflect.InvocationTargetException;

public final class y extends cz {

    /* renamed from: a reason: collision with root package name */
    private aa f4131a = z.f4133a;

    /* renamed from: b reason: collision with root package name */
    private Boolean f4132b;

    y(ce ceVar) {
        super(ceVar);
    }

    public static long v() {
        return ((Long) ap.E.b()).longValue();
    }

    public static long w() {
        return ((Long) ap.e.b()).longValue();
    }

    public static boolean y() {
        return ((Boolean) ap.f3866a.b()).booleanValue();
    }

    /* access modifiers changed from: 0000 */
    public final String A() {
        String w = f().w();
        aq<String> aqVar = ap.J;
        return w == null ? (String) aqVar.b() : (String) aqVar.a(this.f4131a.a(w, aqVar.a()));
    }

    public final int a(String str) {
        return b(str, ap.p);
    }

    public final long a(String str, aq<Long> aqVar) {
        if (str == null) {
            return ((Long) aqVar.b()).longValue();
        }
        String a2 = this.f4131a.a(str, aqVar.a());
        if (TextUtils.isEmpty(a2)) {
            return ((Long) aqVar.b()).longValue();
        }
        try {
            return ((Long) aqVar.a(Long.valueOf(Long.parseLong(a2)))).longValue();
        } catch (NumberFormatException e) {
            return ((Long) aqVar.b()).longValue();
        }
    }

    public final /* bridge */ /* synthetic */ void a() {
        super.a();
    }

    /* access modifiers changed from: 0000 */
    public final void a(aa aaVar) {
        this.f4131a = aaVar;
    }

    public final int b(String str, aq<Integer> aqVar) {
        if (str == null) {
            return ((Integer) aqVar.b()).intValue();
        }
        String a2 = this.f4131a.a(str, aqVar.a());
        if (TextUtils.isEmpty(a2)) {
            return ((Integer) aqVar.b()).intValue();
        }
        try {
            return ((Integer) aqVar.a(Integer.valueOf(Integer.parseInt(a2)))).intValue();
        } catch (NumberFormatException e) {
            return ((Integer) aqVar.b()).intValue();
        }
    }

    /* access modifiers changed from: 0000 */
    public final Boolean b(String str) {
        aa.a(str);
        try {
            if (k().getPackageManager() == null) {
                q().v().a("Failed to load metadata: PackageManager is null");
                return null;
            }
            ApplicationInfo a2 = c.b(k()).a(k().getPackageName(), 128);
            if (a2 == null) {
                q().v().a("Failed to load metadata: ApplicationInfo is null");
                return null;
            } else if (a2.metaData == null) {
                q().v().a("Failed to load metadata: Metadata bundle is null");
                return null;
            } else if (a2.metaData.containsKey(str)) {
                return Boolean.valueOf(a2.metaData.getBoolean(str));
            } else {
                return null;
            }
        } catch (NameNotFoundException e) {
            q().v().a("Failed to load metadata: Package name not found", e);
            return null;
        }
    }

    public final /* bridge */ /* synthetic */ void b() {
        super.b();
    }

    public final double c(String str, aq<Double> aqVar) {
        if (str == null) {
            return ((Double) aqVar.b()).doubleValue();
        }
        String a2 = this.f4131a.a(str, aqVar.a());
        if (TextUtils.isEmpty(a2)) {
            return ((Double) aqVar.b()).doubleValue();
        }
        try {
            return ((Double) aqVar.a(Double.valueOf(Double.parseDouble(a2)))).doubleValue();
        } catch (NumberFormatException e) {
            return ((Double) aqVar.b()).doubleValue();
        }
    }

    public final /* bridge */ /* synthetic */ void c() {
        super.c();
    }

    public final boolean c(String str) {
        return "1".equals(this.f4131a.a(str, "gaia_collection_enabled"));
    }

    public final /* bridge */ /* synthetic */ n d() {
        return super.d();
    }

    public final boolean d(String str) {
        return "1".equals(this.f4131a.a(str, "measurement.event_sampling_enabled"));
    }

    public final boolean d(String str, aq<Boolean> aqVar) {
        if (str == null) {
            return ((Boolean) aqVar.b()).booleanValue();
        }
        String a2 = this.f4131a.a(str, aqVar.a());
        return TextUtils.isEmpty(a2) ? ((Boolean) aqVar.b()).booleanValue() : ((Boolean) aqVar.a(Boolean.valueOf(Boolean.parseBoolean(a2)))).booleanValue();
    }

    public final /* bridge */ /* synthetic */ dd e() {
        return super.e();
    }

    /* access modifiers changed from: 0000 */
    public final boolean e(String str) {
        return d(str, ap.N);
    }

    public final /* bridge */ /* synthetic */ au f() {
        return super.f();
    }

    /* access modifiers changed from: 0000 */
    public final boolean f(String str) {
        return d(str, ap.P);
    }

    public final /* bridge */ /* synthetic */ ah g() {
        return super.g();
    }

    /* access modifiers changed from: 0000 */
    public final boolean g(String str) {
        return d(str, ap.Q);
    }

    public final /* bridge */ /* synthetic */ eb h() {
        return super.h();
    }

    /* access modifiers changed from: 0000 */
    public final boolean h(String str) {
        return d(str, ap.R);
    }

    public final /* bridge */ /* synthetic */ dy i() {
        return super.i();
    }

    /* access modifiers changed from: 0000 */
    public final boolean i(String str) {
        return d(str, ap.S);
    }

    public final /* bridge */ /* synthetic */ e j() {
        return super.j();
    }

    /* access modifiers changed from: 0000 */
    public final boolean j(String str) {
        return d(str, ap.U);
    }

    public final /* bridge */ /* synthetic */ Context k() {
        return super.k();
    }

    public final /* bridge */ /* synthetic */ av l() {
        return super.l();
    }

    public final /* bridge */ /* synthetic */ ax m() {
        return super.m();
    }

    public final /* bridge */ /* synthetic */ ft n() {
        return super.n();
    }

    public final /* bridge */ /* synthetic */ fa o() {
        return super.o();
    }

    public final /* bridge */ /* synthetic */ bz p() {
        return super.p();
    }

    public final /* bridge */ /* synthetic */ az q() {
        return super.q();
    }

    public final /* bridge */ /* synthetic */ bk r() {
        return super.r();
    }

    public final /* bridge */ /* synthetic */ y s() {
        return super.s();
    }

    public final boolean t() {
        if (this.f4132b == null) {
            synchronized (this) {
                if (this.f4132b == null) {
                    ApplicationInfo applicationInfo = k().getApplicationInfo();
                    String a2 = p.a();
                    if (applicationInfo != null) {
                        String str = applicationInfo.processName;
                        this.f4132b = Boolean.valueOf(str != null && str.equals(a2));
                    }
                    if (this.f4132b == null) {
                        this.f4132b = Boolean.TRUE;
                        q().v().a("My process not in the list of running processes");
                    }
                }
            }
        }
        return this.f4132b.booleanValue();
    }

    public final boolean u() {
        Boolean b2 = b("firebase_analytics_collection_deactivated");
        return b2 != null && b2.booleanValue();
    }

    public final String x() {
        try {
            return (String) Class.forName("android.os.SystemProperties").getMethod("get", new Class[]{String.class, String.class}).invoke(null, new Object[]{"debug.firebase.analytics.app", ""});
        } catch (ClassNotFoundException e) {
            q().v().a("Could not find SystemProperties class", e);
        } catch (NoSuchMethodException e2) {
            q().v().a("Could not find SystemProperties.get() method", e2);
        } catch (IllegalAccessException e3) {
            q().v().a("Could not access SystemProperties.get()", e3);
        } catch (InvocationTargetException e4) {
            q().v().a("SystemProperties.get() threw an exception", e4);
        }
        return "";
    }

    /* access modifiers changed from: 0000 */
    public final boolean z() {
        return d(f().w(), ap.I);
    }
}
