package com.google.android.gms.internal.c;

import com.google.android.gms.internal.c.d;
import java.io.IOException;

public abstract class d<M extends d<M>> extends j {

    /* renamed from: a reason: collision with root package name */
    protected f f3962a;

    /* access modifiers changed from: protected */
    public int a() {
        if (this.f3962a == null) {
            return 0;
        }
        int i = 0;
        for (int i2 = 0; i2 < this.f3962a.a(); i2++) {
            i += this.f3962a.b(i2).a();
        }
        return i;
    }

    public void a(b bVar) throws IOException {
        if (this.f3962a != null) {
            for (int i = 0; i < this.f3962a.a(); i++) {
                this.f3962a.b(i).a(bVar);
            }
        }
    }

    /* access modifiers changed from: protected */
    public final boolean a(a aVar, int i) throws IOException {
        int i2 = aVar.i();
        if (!aVar.b(i)) {
            return false;
        }
        int i3 = i >>> 3;
        l lVar = new l(i, aVar.a(i2, aVar.i() - i2));
        g gVar = null;
        if (this.f3962a == null) {
            this.f3962a = new f();
        } else {
            gVar = this.f3962a.a(i3);
        }
        if (gVar == null) {
            gVar = new g();
            this.f3962a.a(i3, gVar);
        }
        gVar.a(lVar);
        return true;
    }

    public final /* synthetic */ j b() throws CloneNotSupportedException {
        return (d) clone();
    }

    public /* synthetic */ Object clone() throws CloneNotSupportedException {
        d dVar = (d) super.clone();
        h.a(this, dVar);
        return dVar;
    }
}
