package com.google.android.gms.internal.c;

import java.io.IOException;

public final class fz extends d<fz> {
    public Integer c;
    public Boolean d;
    public String e;
    public String f;
    public String g;

    public fz() {
        this.c = null;
        this.d = null;
        this.e = null;
        this.f = null;
        this.g = null;
        this.f3962a = null;
        this.f4112b = -1;
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public final fz a(a aVar) throws IOException {
        while (true) {
            int a2 = aVar.a();
            switch (a2) {
                case 0:
                    break;
                case 8:
                    int i = aVar.i();
                    try {
                        int d2 = aVar.d();
                        if (d2 < 0 || d2 > 4) {
                            throw new IllegalArgumentException(d2 + " is not a valid enum ComparisonType");
                        }
                        this.c = Integer.valueOf(d2);
                        continue;
                    } catch (IllegalArgumentException e2) {
                        aVar.e(i);
                        a(aVar, a2);
                        break;
                    }
                case 16:
                    this.d = Boolean.valueOf(aVar.b());
                    continue;
                case 26:
                    this.e = aVar.c();
                    continue;
                case 34:
                    this.f = aVar.c();
                    continue;
                case 42:
                    this.g = aVar.c();
                    continue;
                default:
                    if (!super.a(aVar, a2)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    /* access modifiers changed from: protected */
    public final int a() {
        int a2 = super.a();
        if (this.c != null) {
            a2 += b.b(1, this.c.intValue());
        }
        if (this.d != null) {
            this.d.booleanValue();
            a2 += b.b(2) + 1;
        }
        if (this.e != null) {
            a2 += b.b(3, this.e);
        }
        if (this.f != null) {
            a2 += b.b(4, this.f);
        }
        return this.g != null ? a2 + b.b(5, this.g) : a2;
    }

    public final void a(b bVar) throws IOException {
        if (this.c != null) {
            bVar.a(1, this.c.intValue());
        }
        if (this.d != null) {
            bVar.a(2, this.d.booleanValue());
        }
        if (this.e != null) {
            bVar.a(3, this.e);
        }
        if (this.f != null) {
            bVar.a(4, this.f);
        }
        if (this.g != null) {
            bVar.a(5, this.g);
        }
        super.a(bVar);
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof fz)) {
            return false;
        }
        fz fzVar = (fz) obj;
        if (this.c == null) {
            if (fzVar.c != null) {
                return false;
            }
        } else if (!this.c.equals(fzVar.c)) {
            return false;
        }
        if (this.d == null) {
            if (fzVar.d != null) {
                return false;
            }
        } else if (!this.d.equals(fzVar.d)) {
            return false;
        }
        if (this.e == null) {
            if (fzVar.e != null) {
                return false;
            }
        } else if (!this.e.equals(fzVar.e)) {
            return false;
        }
        if (this.f == null) {
            if (fzVar.f != null) {
                return false;
            }
        } else if (!this.f.equals(fzVar.f)) {
            return false;
        }
        if (this.g == null) {
            if (fzVar.g != null) {
                return false;
            }
        } else if (!this.g.equals(fzVar.g)) {
            return false;
        }
        return (this.f3962a == null || this.f3962a.b()) ? fzVar.f3962a == null || fzVar.f3962a.b() : this.f3962a.equals(fzVar.f3962a);
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = ((this.g == null ? 0 : this.g.hashCode()) + (((this.f == null ? 0 : this.f.hashCode()) + (((this.e == null ? 0 : this.e.hashCode()) + (((this.d == null ? 0 : this.d.hashCode()) + (((this.c == null ? 0 : this.c.intValue()) + ((getClass().getName().hashCode() + 527) * 31)) * 31)) * 31)) * 31)) * 31)) * 31;
        if (this.f3962a != null && !this.f3962a.b()) {
            i = this.f3962a.hashCode();
        }
        return hashCode + i;
    }
}
