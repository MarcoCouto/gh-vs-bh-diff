package com.google.android.gms.internal.c;

public final class dx {

    /* renamed from: a reason: collision with root package name */
    public final String f4003a;

    /* renamed from: b reason: collision with root package name */
    public final String f4004b;
    public final long c;
    boolean d = false;

    public dx(String str, String str2, long j) {
        this.f4003a = str;
        this.f4004b = str2;
        this.c = j;
    }
}
