package com.google.android.gms.internal.c;

import android.util.Log;

final class hc extends gy<Long> {
    hc(hi hiVar, String str, Long l) {
        super(hiVar, str, l, null);
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public final Long a(String str) {
        try {
            return Long.valueOf(Long.parseLong(str));
        } catch (NumberFormatException e) {
            String str2 = this.f4102a;
            Log.e("PhenotypeFlag", new StringBuilder(String.valueOf(str2).length() + 25 + String.valueOf(str).length()).append("Invalid long value for ").append(str2).append(": ").append(str).toString());
            return null;
        }
    }
}
