package com.google.android.gms.internal.c;

import java.util.concurrent.atomic.AtomicReference;

final class de implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ AtomicReference f3967a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ dd f3968b;

    de(dd ddVar, AtomicReference atomicReference) {
        this.f3968b = ddVar;
        this.f3967a = atomicReference;
    }

    public final void run() {
        synchronized (this.f3967a) {
            try {
                this.f3967a.set(Boolean.valueOf(this.f3968b.s().z()));
                this.f3967a.notify();
            } catch (Throwable th) {
                this.f3967a.notify();
                throw th;
            }
        }
    }
}
