package com.google.android.gms.internal.c;

import java.io.IOException;

public abstract class j {

    /* renamed from: b reason: collision with root package name */
    protected volatile int f4112b = -1;

    /* access modifiers changed from: protected */
    public int a() {
        return 0;
    }

    public abstract j a(a aVar) throws IOException;

    public void a(b bVar) throws IOException {
    }

    /* renamed from: b */
    public j clone() throws CloneNotSupportedException {
        return (j) super.clone();
    }

    public final int c() {
        if (this.f4112b < 0) {
            d();
        }
        return this.f4112b;
    }

    public final int d() {
        int a2 = a();
        this.f4112b = a2;
        return a2;
    }

    public String toString() {
        return k.a(this);
    }
}
