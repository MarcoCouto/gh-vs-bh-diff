package com.google.android.gms.internal.c;

import android.os.Bundle;

final class fb extends af {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ fa f4060a;

    fb(fa faVar, db dbVar) {
        this.f4060a = faVar;
        super(dbVar);
    }

    public final void a() {
        fa faVar = this.f4060a;
        faVar.c();
        faVar.q().C().a("Session started, time", Long.valueOf(faVar.j().b()));
        faVar.r().m.a(false);
        faVar.e().b("auto", "_s", new Bundle());
        faVar.r().n.a(faVar.j().a());
    }
}
