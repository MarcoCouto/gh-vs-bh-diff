package com.google.android.gms.internal.c;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.google.android.gms.common.internal.e;
import com.google.android.gms.common.internal.e.a;
import com.google.android.gms.common.internal.e.b;

public final class ay extends e<ar> {
    public ay(Context context, Looper looper, a aVar, b bVar) {
        super(context, looper, 93, aVar, bVar, null);
    }

    public final /* synthetic */ IInterface a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.measurement.internal.IMeasurementService");
        return queryLocalInterface instanceof ar ? (ar) queryLocalInterface : new at(iBinder);
    }

    /* access modifiers changed from: protected */
    public final String i() {
        return "com.google.android.gms.measurement.START";
    }

    /* access modifiers changed from: protected */
    public final String l() {
        return "com.google.android.gms.measurement.internal.IMeasurementService";
    }
}
