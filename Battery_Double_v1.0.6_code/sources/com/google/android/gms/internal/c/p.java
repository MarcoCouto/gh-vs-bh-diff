package com.google.android.gms.internal.c;

final class p implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ String f4121a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ long f4122b;
    private final /* synthetic */ n c;

    p(n nVar, String str, long j) {
        this.c = nVar;
        this.f4121a = str;
        this.f4122b = j;
    }

    public final void run() {
        this.c.b(this.f4121a, this.f4122b);
    }
}
