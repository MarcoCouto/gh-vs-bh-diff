package com.google.android.gms.internal.c;

import java.io.IOException;

public final class gl extends d<gl> {
    private static volatile gl[] h;
    public Long c;
    public String d;
    public String e;
    public Long f;
    public Double g;
    private Float i;

    public gl() {
        this.c = null;
        this.d = null;
        this.e = null;
        this.f = null;
        this.i = null;
        this.g = null;
        this.f3962a = null;
        this.f4112b = -1;
    }

    public static gl[] e() {
        if (h == null) {
            synchronized (h.f4106b) {
                if (h == null) {
                    h = new gl[0];
                }
            }
        }
        return h;
    }

    /* access modifiers changed from: protected */
    public final int a() {
        int a2 = super.a();
        if (this.c != null) {
            a2 += b.c(1, this.c.longValue());
        }
        if (this.d != null) {
            a2 += b.b(2, this.d);
        }
        if (this.e != null) {
            a2 += b.b(3, this.e);
        }
        if (this.f != null) {
            a2 += b.c(4, this.f.longValue());
        }
        if (this.i != null) {
            this.i.floatValue();
            a2 += b.b(5) + 4;
        }
        if (this.g == null) {
            return a2;
        }
        this.g.doubleValue();
        return a2 + b.b(6) + 8;
    }

    public final /* synthetic */ j a(a aVar) throws IOException {
        while (true) {
            int a2 = aVar.a();
            switch (a2) {
                case 0:
                    break;
                case 8:
                    this.c = Long.valueOf(aVar.e());
                    continue;
                case 18:
                    this.d = aVar.c();
                    continue;
                case 26:
                    this.e = aVar.c();
                    continue;
                case 32:
                    this.f = Long.valueOf(aVar.e());
                    continue;
                case 45:
                    this.i = Float.valueOf(Float.intBitsToFloat(aVar.f()));
                    continue;
                case 49:
                    this.g = Double.valueOf(Double.longBitsToDouble(aVar.g()));
                    continue;
                default:
                    if (!super.a(aVar, a2)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    public final void a(b bVar) throws IOException {
        if (this.c != null) {
            bVar.b(1, this.c.longValue());
        }
        if (this.d != null) {
            bVar.a(2, this.d);
        }
        if (this.e != null) {
            bVar.a(3, this.e);
        }
        if (this.f != null) {
            bVar.b(4, this.f.longValue());
        }
        if (this.i != null) {
            bVar.a(5, this.i.floatValue());
        }
        if (this.g != null) {
            bVar.a(6, this.g.doubleValue());
        }
        super.a(bVar);
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof gl)) {
            return false;
        }
        gl glVar = (gl) obj;
        if (this.c == null) {
            if (glVar.c != null) {
                return false;
            }
        } else if (!this.c.equals(glVar.c)) {
            return false;
        }
        if (this.d == null) {
            if (glVar.d != null) {
                return false;
            }
        } else if (!this.d.equals(glVar.d)) {
            return false;
        }
        if (this.e == null) {
            if (glVar.e != null) {
                return false;
            }
        } else if (!this.e.equals(glVar.e)) {
            return false;
        }
        if (this.f == null) {
            if (glVar.f != null) {
                return false;
            }
        } else if (!this.f.equals(glVar.f)) {
            return false;
        }
        if (this.i == null) {
            if (glVar.i != null) {
                return false;
            }
        } else if (!this.i.equals(glVar.i)) {
            return false;
        }
        if (this.g == null) {
            if (glVar.g != null) {
                return false;
            }
        } else if (!this.g.equals(glVar.g)) {
            return false;
        }
        return (this.f3962a == null || this.f3962a.b()) ? glVar.f3962a == null || glVar.f3962a.b() : this.f3962a.equals(glVar.f3962a);
    }

    public final int hashCode() {
        int i2 = 0;
        int hashCode = ((this.g == null ? 0 : this.g.hashCode()) + (((this.i == null ? 0 : this.i.hashCode()) + (((this.f == null ? 0 : this.f.hashCode()) + (((this.e == null ? 0 : this.e.hashCode()) + (((this.d == null ? 0 : this.d.hashCode()) + (((this.c == null ? 0 : this.c.hashCode()) + ((getClass().getName().hashCode() + 527) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31;
        if (this.f3962a != null && !this.f3962a.b()) {
            i2 = this.f3962a.hashCode();
        }
        return hashCode + i2;
    }
}
