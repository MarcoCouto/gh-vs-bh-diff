package com.google.android.gms.internal.c;

import android.os.Process;
import com.google.android.gms.common.internal.aa;
import java.util.concurrent.BlockingQueue;

final class cd extends Thread {

    /* renamed from: a reason: collision with root package name */
    private final Object f3920a = new Object();

    /* renamed from: b reason: collision with root package name */
    private final BlockingQueue<cc<?>> f3921b;
    private final /* synthetic */ bz c;

    public cd(bz bzVar, String str, BlockingQueue<cc<?>> blockingQueue) {
        this.c = bzVar;
        aa.a(str);
        aa.a(blockingQueue);
        this.f3921b = blockingQueue;
        setName(str);
    }

    private final void a(InterruptedException interruptedException) {
        this.c.q().y().a(String.valueOf(getName()).concat(" was interrupted"), interruptedException);
    }

    public final void a() {
        synchronized (this.f3920a) {
            this.f3920a.notifyAll();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:44:0x008b, code lost:
        r1 = r6.c.h;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x0091, code lost:
        monitor-enter(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:?, code lost:
        r6.c.i.release();
        r6.c.h.notifyAll();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00aa, code lost:
        if (r6 != r6.c.f3915b) goto L_0x00bc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00ac, code lost:
        r6.c.f3915b = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00b2, code lost:
        monitor-exit(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00b3, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x00c2, code lost:
        if (r6 != r6.c.c) goto L_0x00ce;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x00c4, code lost:
        r6.c.c = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:?, code lost:
        r6.c.q().v().a("Current scheduler thread is neither worker nor network");
     */
    public final void run() {
        boolean z = false;
        while (!z) {
            try {
                this.c.i.acquire();
                z = true;
            } catch (InterruptedException e) {
                a(e);
            }
        }
        try {
            int threadPriority = Process.getThreadPriority(Process.myTid());
            while (true) {
                cc ccVar = (cc) this.f3921b.poll();
                if (ccVar != null) {
                    Process.setThreadPriority(ccVar.f3918a ? threadPriority : 10);
                    ccVar.run();
                } else {
                    synchronized (this.f3920a) {
                        if (this.f3921b.peek() == null && !this.c.j) {
                            try {
                                this.f3920a.wait(30000);
                            } catch (InterruptedException e2) {
                                a(e2);
                            }
                        }
                    }
                    synchronized (this.c.h) {
                        if (this.f3921b.peek() == null) {
                        }
                    }
                }
            }
        } catch (Throwable th) {
            synchronized (this.c.h) {
                this.c.i.release();
                this.c.h.notifyAll();
                if (this == this.c.f3915b) {
                    this.c.f3915b = null;
                } else if (this == this.c.c) {
                    this.c.c = null;
                } else {
                    this.c.q().v().a("Current scheduler thread is neither worker nor network");
                }
                throw th;
            }
        }
    }
}
