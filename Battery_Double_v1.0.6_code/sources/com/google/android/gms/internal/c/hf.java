package com.google.android.gms.internal.c;

import android.util.Log;

final class hf extends gy<Double> {
    hf(hi hiVar, String str, Double d) {
        super(hiVar, str, d, null);
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public final Double a(String str) {
        try {
            return Double.valueOf(Double.parseDouble(str));
        } catch (NumberFormatException e) {
            String str2 = this.f4102a;
            Log.e("PhenotypeFlag", new StringBuilder(String.valueOf(str2).length() + 27 + String.valueOf(str).length()).append("Invalid double value for ").append(str2).append(": ").append(str).toString());
            return null;
        }
    }
}
