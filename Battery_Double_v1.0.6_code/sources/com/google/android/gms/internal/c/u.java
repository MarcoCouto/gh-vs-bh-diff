package com.google.android.gms.internal.c;

import android.content.ContentValues;
import android.database.sqlite.SQLiteException;
import android.support.v4.h.a;
import android.text.TextUtils;
import android.util.Pair;
import com.google.android.gms.common.internal.aa;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

final class u extends fj {
    u(fk fkVar) {
        super(fkVar);
    }

    private final Boolean a(double d, fz fzVar) {
        try {
            return a(new BigDecimal(d), fzVar, Math.ulp(d));
        } catch (NumberFormatException e) {
            return null;
        }
    }

    private final Boolean a(long j, fz fzVar) {
        try {
            return a(new BigDecimal(j), fzVar, 0.0d);
        } catch (NumberFormatException e) {
            return null;
        }
    }

    private static Boolean a(Boolean bool, boolean z) {
        if (bool == null) {
            return null;
        }
        return Boolean.valueOf(bool.booleanValue() ^ z);
    }

    private final Boolean a(String str, int i, boolean z, String str2, List<String> list, String str3) {
        if (str == null) {
            return null;
        }
        if (i == 6) {
            if (list == null || list.size() == 0) {
                return null;
            }
        } else if (str2 == null) {
            return null;
        }
        if (!z && i != 1) {
            str = str.toUpperCase(Locale.ENGLISH);
        }
        switch (i) {
            case 1:
                try {
                    return Boolean.valueOf(Pattern.compile(str3, z ? 0 : 66).matcher(str).matches());
                } catch (PatternSyntaxException e) {
                    q().y().a("Invalid regular expression in REGEXP audience filter. expression", str3);
                    return null;
                }
            case 2:
                return Boolean.valueOf(str.startsWith(str2));
            case 3:
                return Boolean.valueOf(str.endsWith(str2));
            case 4:
                return Boolean.valueOf(str.contains(str2));
            case 5:
                return Boolean.valueOf(str.equals(str2));
            case 6:
                return Boolean.valueOf(list.contains(str));
            default:
                return null;
        }
    }

    private final Boolean a(String str, fz fzVar) {
        Boolean bool = null;
        if (!ft.j(str)) {
            return bool;
        }
        try {
            return a(new BigDecimal(str), fzVar, 0.0d);
        } catch (NumberFormatException e) {
            return bool;
        }
    }

    private final Boolean a(String str, gb gbVar) {
        List arrayList;
        String str2 = null;
        aa.a(gbVar);
        if (str == null || gbVar.c == null || gbVar.c.intValue() == 0) {
            return null;
        }
        if (gbVar.c.intValue() == 6) {
            if (gbVar.f == null || gbVar.f.length == 0) {
                return null;
            }
        } else if (gbVar.d == null) {
            return null;
        }
        int intValue = gbVar.c.intValue();
        boolean z = gbVar.e != null && gbVar.e.booleanValue();
        String upperCase = (z || intValue == 1 || intValue == 6) ? gbVar.d : gbVar.d.toUpperCase(Locale.ENGLISH);
        if (gbVar.f == null) {
            arrayList = null;
        } else {
            String[] strArr = gbVar.f;
            if (z) {
                arrayList = Arrays.asList(strArr);
            } else {
                arrayList = new ArrayList();
                for (String upperCase2 : strArr) {
                    arrayList.add(upperCase2.toUpperCase(Locale.ENGLISH));
                }
            }
        }
        if (intValue == 1) {
            str2 = upperCase;
        }
        return a(str, intValue, z, upperCase, arrayList, str2);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:38:0x007e, code lost:
        if (r5 != null) goto L_0x0080;
     */
    private static Boolean a(BigDecimal bigDecimal, fz fzVar, double d) {
        BigDecimal bigDecimal2;
        BigDecimal bigDecimal3;
        BigDecimal bigDecimal4;
        boolean z = true;
        aa.a(fzVar);
        if (fzVar.c == null || fzVar.c.intValue() == 0) {
            return null;
        }
        if (fzVar.c.intValue() == 4) {
            if (fzVar.f == null || fzVar.g == null) {
                return null;
            }
        } else if (fzVar.e == null) {
            return null;
        }
        int intValue = fzVar.c.intValue();
        if (fzVar.c.intValue() == 4) {
            if (!ft.j(fzVar.f) || !ft.j(fzVar.g)) {
                return null;
            }
            try {
                bigDecimal2 = new BigDecimal(fzVar.f);
                bigDecimal4 = new BigDecimal(fzVar.g);
                bigDecimal3 = null;
            } catch (NumberFormatException e) {
                return null;
            }
        } else if (!ft.j(fzVar.e)) {
            return null;
        } else {
            try {
                bigDecimal2 = null;
                bigDecimal3 = new BigDecimal(fzVar.e);
                bigDecimal4 = null;
            } catch (NumberFormatException e2) {
                return null;
            }
        }
        if (intValue == 4) {
            if (bigDecimal2 == null) {
                return null;
            }
        }
        switch (intValue) {
            case 1:
                if (bigDecimal.compareTo(bigDecimal3) != -1) {
                    z = false;
                }
                return Boolean.valueOf(z);
            case 2:
                if (bigDecimal.compareTo(bigDecimal3) != 1) {
                    z = false;
                }
                return Boolean.valueOf(z);
            case 3:
                if (d != 0.0d) {
                    if (!(bigDecimal.compareTo(bigDecimal3.subtract(new BigDecimal(d).multiply(new BigDecimal(2)))) == 1 && bigDecimal.compareTo(bigDecimal3.add(new BigDecimal(d).multiply(new BigDecimal(2)))) == -1)) {
                        z = false;
                    }
                    return Boolean.valueOf(z);
                }
                if (bigDecimal.compareTo(bigDecimal3) != 0) {
                    z = false;
                }
                return Boolean.valueOf(z);
            case 4:
                if (bigDecimal.compareTo(bigDecimal2) == -1 || bigDecimal.compareTo(bigDecimal4) == 1) {
                    z = false;
                }
                return Boolean.valueOf(z);
        }
        return null;
    }

    /* JADX WARNING: type inference failed for: r8v14 */
    /* JADX WARNING: type inference failed for: r8v23 */
    /* JADX WARNING: type inference failed for: r8v31 */
    /* JADX WARNING: type inference failed for: r8v37 */
    /* JADX WARNING: type inference failed for: r9v11 */
    /* JADX WARNING: type inference failed for: r9v15, types: [java.lang.String] */
    /* JADX WARNING: type inference failed for: r8v42 */
    /* JADX WARNING: type inference failed for: r5v59 */
    /* JADX WARNING: type inference failed for: r5v64 */
    /* JADX WARNING: type inference failed for: r5v71 */
    /* JADX WARNING: type inference failed for: r5v74 */
    /* JADX WARNING: type inference failed for: r5v78 */
    /* JADX WARNING: type inference failed for: r5v90 */
    /* JADX WARNING: type inference failed for: r5v94 */
    /* JADX WARNING: type inference failed for: r5v104 */
    /* JADX WARNING: type inference failed for: r5v108 */
    /* JADX WARNING: type inference failed for: r5v119 */
    /* JADX WARNING: type inference failed for: r5v122 */
    /* JADX WARNING: type inference failed for: r6v39 */
    /* JADX WARNING: type inference failed for: r6v43, types: [java.lang.String] */
    /* JADX WARNING: type inference failed for: r5v131 */
    /* JADX WARNING: type inference failed for: r9v24 */
    /* JADX WARNING: type inference failed for: r6v69 */
    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:121:0x0465  */
    /* JADX WARNING: Removed duplicated region for block: B:124:0x046e  */
    /* JADX WARNING: Removed duplicated region for block: B:212:0x076a  */
    /* JADX WARNING: Removed duplicated region for block: B:213:0x076d  */
    /* JADX WARNING: Removed duplicated region for block: B:317:0x0bc2  */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x0238  */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x0283  */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x02ac  */
    /* JADX WARNING: Removed duplicated region for block: B:96:0x0345  */
    /* JADX WARNING: Unknown variable types count: 19 */
    public final gf[] a(String str, gg[] ggVarArr, gl[] glVarArr) {
        Map map;
        Boolean bool;
        gh[] ghVarArr;
        String str2;
        Long l;
        long j;
        gg ggVar;
        aj a2;
        aj a3;
        Map map2;
        Map map3;
        BitSet bitSet;
        BitSet bitSet2;
        Boolean a4;
        Boolean bool2;
        Long l2;
        int i;
        aa.a(str);
        HashSet hashSet = new HashSet();
        a aVar = new a();
        a aVar2 = new a();
        a aVar3 = new a();
        Map e = t_().e(str);
        if (e != null) {
            for (Integer intValue : e.keySet()) {
                int intValue2 = intValue.intValue();
                gk gkVar = (gk) e.get(Integer.valueOf(intValue2));
                BitSet bitSet3 = (BitSet) aVar2.get(Integer.valueOf(intValue2));
                BitSet bitSet4 = (BitSet) aVar3.get(Integer.valueOf(intValue2));
                if (bitSet3 == null) {
                    bitSet3 = new BitSet();
                    aVar2.put(Integer.valueOf(intValue2), bitSet3);
                    bitSet4 = new BitSet();
                    aVar3.put(Integer.valueOf(intValue2), bitSet4);
                }
                for (int i2 = 0; i2 < (gkVar.c.length << 6); i2++) {
                    if (ft.a(gkVar.c, i2)) {
                        q().C().a("Filter already evaluated. audience ID, filter ID", Integer.valueOf(intValue2), Integer.valueOf(i2));
                        bitSet4.set(i2);
                        if (ft.a(gkVar.d, i2)) {
                            bitSet3.set(i2);
                        }
                    }
                }
                gf gfVar = new gf();
                aVar.put(Integer.valueOf(intValue2), gfVar);
                gfVar.f = Boolean.valueOf(false);
                gfVar.e = gkVar;
                gfVar.d = new gk();
                gfVar.d.d = ft.a(bitSet3);
                gfVar.d.c = ft.a(bitSet4);
            }
        }
        if (ggVarArr != null) {
            gg ggVar2 = null;
            long j2 = 0;
            Long l3 = null;
            a aVar4 = new a();
            int length = ggVarArr.length;
            int i3 = 0;
            while (true) {
                int i4 = i3;
                if (i4 >= length) {
                    break;
                }
                gg ggVar3 = ggVarArr[i4];
                String str3 = ggVar3.d;
                gh[] ghVarArr2 = ggVar3.c;
                if (s().d(str, ap.O)) {
                    n();
                    Long l4 = (Long) ft.b(ggVar3, "_eid");
                    boolean z = l4 != null;
                    if (z && str3.equals("_ep")) {
                        n();
                        String str4 = (String) ft.b(ggVar3, "_en");
                        if (TextUtils.isEmpty(str4)) {
                            q().v().a("Extra parameter without an event name. eventId", l4);
                        } else {
                            if (ggVar2 == null || l3 == null || l4.longValue() != l3.longValue()) {
                                Pair a5 = t_().a(str, l4);
                                if (a5 == null || a5.first == null) {
                                    q().v().a("Extra parameter without existing main event. eventName, eventId", str4, l4);
                                } else {
                                    gg ggVar4 = (gg) a5.first;
                                    j2 = ((Long) a5.second).longValue();
                                    n();
                                    l2 = (Long) ft.b(ggVar4, "_eid");
                                    ggVar2 = ggVar4;
                                }
                            } else {
                                l2 = l3;
                            }
                            long j3 = j2 - 1;
                            if (j3 <= 0) {
                                ab t_ = t_();
                                t_.c();
                                t_.q().C().a("Clearing complex main event info. appId", str);
                                try {
                                    t_.x().execSQL("delete from main_event_params where app_id=?", new String[]{str});
                                } catch (SQLiteException e2) {
                                    t_.q().v().a("Error clearing complex main event", e2);
                                }
                            } else {
                                t_().a(str, l4, j3, ggVar2);
                            }
                            gh[] ghVarArr3 = new gh[(ggVar2.c.length + ghVarArr2.length)];
                            int i5 = 0;
                            gh[] ghVarArr4 = ggVar2.c;
                            int length2 = ghVarArr4.length;
                            int i6 = 0;
                            while (i6 < length2) {
                                gh ghVar = ghVarArr4[i6];
                                n();
                                if (ft.a(ggVar3, ghVar.c) == null) {
                                    i = i5 + 1;
                                    ghVarArr3[i5] = ghVar;
                                } else {
                                    i = i5;
                                }
                                i6++;
                                i5 = i;
                            }
                            if (i5 > 0) {
                                int length3 = ghVarArr2.length;
                                int i7 = 0;
                                while (i7 < length3) {
                                    int i8 = i5 + 1;
                                    ghVarArr3[i5] = ghVarArr2[i7];
                                    i7++;
                                    i5 = i8;
                                }
                                ghVarArr = i5 == ghVarArr3.length ? ghVarArr3 : (gh[]) Arrays.copyOf(ghVarArr3, i5);
                                str2 = str4;
                                l = l2;
                                j = j3;
                                ggVar = ggVar2;
                            } else {
                                q().y().a("No unique parameters in main event. eventName", str4);
                                ghVarArr = ghVarArr2;
                                str2 = str4;
                                l = l2;
                                j = j3;
                                ggVar = ggVar2;
                            }
                        }
                        i3 = i4 + 1;
                    } else if (z) {
                        n();
                        Long valueOf = Long.valueOf(0);
                        Object b2 = ft.b(ggVar3, "_epc");
                        if (b2 != null) {
                            valueOf = b2;
                        }
                        long longValue = valueOf.longValue();
                        if (longValue <= 0) {
                            q().y().a("Complex event with zero extra param count. eventName", str3);
                            ghVarArr = ghVarArr2;
                            str2 = str3;
                            l = l4;
                            j = longValue;
                            ggVar = ggVar3;
                        } else {
                            t_().a(str, l4, longValue, ggVar3);
                            ghVarArr = ghVarArr2;
                            str2 = str3;
                            l = l4;
                            j = longValue;
                            ggVar = ggVar3;
                        }
                    }
                    a2 = t_().a(str, ggVar3.d);
                    if (a2 != null) {
                        q().y().a("Event aggregate wasn't created during raw event logging. appId, event", az.a(str), m().a(str2));
                        a3 = new aj(str, ggVar3.d, 1, 1, ggVar3.e.longValue(), 0, null, null, null);
                    } else {
                        a3 = a2.a();
                    }
                    t_().a(a3);
                    long j4 = a3.c;
                    map2 = (Map) aVar4.get(str2);
                    if (map2 != null) {
                        Map f = t_().f(str, str2);
                        if (f == null) {
                            f = new a();
                        }
                        aVar4.put(str2, f);
                        map3 = f;
                    } else {
                        map3 = map2;
                    }
                    for (Integer intValue3 : map3.keySet()) {
                        int intValue4 = intValue3.intValue();
                        if (hashSet.contains(Integer.valueOf(intValue4))) {
                            q().C().a("Skipping failed audience ID", Integer.valueOf(intValue4));
                        } else {
                            BitSet bitSet5 = (BitSet) aVar2.get(Integer.valueOf(intValue4));
                            BitSet bitSet6 = (BitSet) aVar3.get(Integer.valueOf(intValue4));
                            if (((gf) aVar.get(Integer.valueOf(intValue4))) == null) {
                                gf gfVar2 = new gf();
                                aVar.put(Integer.valueOf(intValue4), gfVar2);
                                gfVar2.f = Boolean.valueOf(true);
                                BitSet bitSet7 = new BitSet();
                                aVar2.put(Integer.valueOf(intValue4), bitSet7);
                                BitSet bitSet8 = new BitSet();
                                aVar3.put(Integer.valueOf(intValue4), bitSet8);
                                bitSet = bitSet8;
                                bitSet2 = bitSet7;
                            } else {
                                bitSet = bitSet6;
                                bitSet2 = bitSet5;
                            }
                            for (fx fxVar : (List) map3.get(Integer.valueOf(intValue4))) {
                                if (q().a(2)) {
                                    q().C().a("Evaluating filter. audience, filter, event", Integer.valueOf(intValue4), fxVar.c, m().a(fxVar.d));
                                    q().C().a("Filter definition", m().a(fxVar));
                                }
                                if (fxVar.c == null || fxVar.c.intValue() > 256) {
                                    q().y().a("Invalid event filter ID. appId, id", az.a(str), String.valueOf(fxVar.c));
                                } else if (bitSet2.get(fxVar.c.intValue())) {
                                    q().C().a("Event filter already evaluated true. audience ID, filter ID", Integer.valueOf(intValue4), fxVar.c);
                                } else {
                                    if (fxVar.f != null) {
                                        Boolean a6 = a(j4, fxVar.f);
                                        if (a6 == null) {
                                            bool2 = 0;
                                        } else if (!a6.booleanValue()) {
                                            bool2 = Boolean.valueOf(false);
                                        }
                                        q().C().a("Event filter result", bool2 != 0 ? "null" : bool2);
                                        if (bool2 != 0) {
                                            hashSet.add(Integer.valueOf(intValue4));
                                        } else {
                                            bitSet.set(fxVar.c.intValue());
                                            if (bool2.booleanValue()) {
                                                bitSet2.set(fxVar.c.intValue());
                                            }
                                        }
                                    }
                                    HashSet hashSet2 = new HashSet();
                                    fy[] fyVarArr = fxVar.e;
                                    int length4 = fyVarArr.length;
                                    int i9 = 0;
                                    while (true) {
                                        if (i9 >= length4) {
                                            a aVar5 = new a();
                                            int length5 = ghVarArr.length;
                                            int i10 = 0;
                                            while (true) {
                                                if (i10 >= length5) {
                                                    fy[] fyVarArr2 = fxVar.e;
                                                    int length6 = fyVarArr2.length;
                                                    int i11 = 0;
                                                    while (true) {
                                                        int i12 = i11;
                                                        if (i12 >= length6) {
                                                            bool2 = Boolean.valueOf(true);
                                                            break;
                                                        }
                                                        fy fyVar = fyVarArr2[i12];
                                                        boolean equals = Boolean.TRUE.equals(fyVar.e);
                                                        String str5 = fyVar.f;
                                                        if (TextUtils.isEmpty(str5)) {
                                                            q().y().a("Event has empty param name. event", m().a(str2));
                                                            bool2 = 0;
                                                            break;
                                                        }
                                                        Object obj = aVar5.get(str5);
                                                        if (obj instanceof Long) {
                                                            if (fyVar.d == null) {
                                                                q().y().a("No number filter for long param. event, param", m().a(str2), m().b(str5));
                                                                bool2 = 0;
                                                                break;
                                                            }
                                                            Boolean a7 = a(((Long) obj).longValue(), fyVar.d);
                                                            if (a7 == null) {
                                                                bool2 = 0;
                                                                break;
                                                            }
                                                            if ((!a7.booleanValue()) ^ equals) {
                                                                bool2 = Boolean.valueOf(false);
                                                                break;
                                                            }
                                                        } else if (obj instanceof Double) {
                                                            if (fyVar.d == null) {
                                                                q().y().a("No number filter for double param. event, param", m().a(str2), m().b(str5));
                                                                bool2 = 0;
                                                                break;
                                                            }
                                                            Boolean a8 = a(((Double) obj).doubleValue(), fyVar.d);
                                                            if (a8 == null) {
                                                                bool2 = 0;
                                                                break;
                                                            }
                                                            if ((!a8.booleanValue()) ^ equals) {
                                                                bool2 = Boolean.valueOf(false);
                                                                break;
                                                            }
                                                        } else if (obj instanceof String) {
                                                            if (fyVar.c == null) {
                                                                if (fyVar.d != null) {
                                                                    if (!ft.j((String) obj)) {
                                                                        q().y().a("Invalid param value for number filter. event, param", m().a(str2), m().b(str5));
                                                                        bool2 = 0;
                                                                        break;
                                                                    }
                                                                    a4 = a((String) obj, fyVar.d);
                                                                } else {
                                                                    q().y().a("No filter for String param. event, param", m().a(str2), m().b(str5));
                                                                    bool2 = 0;
                                                                    break;
                                                                }
                                                            } else {
                                                                a4 = a((String) obj, fyVar.c);
                                                            }
                                                            if (a4 == null) {
                                                                bool2 = 0;
                                                                break;
                                                            }
                                                            if ((!a4.booleanValue()) ^ equals) {
                                                                bool2 = Boolean.valueOf(false);
                                                                break;
                                                            }
                                                        } else if (obj == null) {
                                                            q().C().a("Missing param for filter. event, param", m().a(str2), m().b(str5));
                                                            bool2 = Boolean.valueOf(false);
                                                        } else {
                                                            q().y().a("Unknown param type. event, param", m().a(str2), m().b(str5));
                                                            bool2 = 0;
                                                        }
                                                        i11 = i12 + 1;
                                                    }
                                                } else {
                                                    gh ghVar2 = ghVarArr[i10];
                                                    if (hashSet2.contains(ghVar2.c)) {
                                                        if (ghVar2.e == null) {
                                                            if (ghVar2.f == null) {
                                                                if (ghVar2.d == null) {
                                                                    q().y().a("Unknown value for param. event, param", m().a(str2), m().b(ghVar2.c));
                                                                    bool2 = 0;
                                                                    break;
                                                                }
                                                                aVar5.put(ghVar2.c, ghVar2.d);
                                                            } else {
                                                                aVar5.put(ghVar2.c, ghVar2.f);
                                                            }
                                                        } else {
                                                            aVar5.put(ghVar2.c, ghVar2.e);
                                                        }
                                                    }
                                                    i10++;
                                                }
                                            }
                                        } else {
                                            fy fyVar2 = fyVarArr[i9];
                                            if (TextUtils.isEmpty(fyVar2.f)) {
                                                q().y().a("null or empty param name in filter. event", m().a(str2));
                                                bool2 = 0;
                                                break;
                                            }
                                            hashSet2.add(fyVar2.f);
                                            i9++;
                                        }
                                    }
                                    q().C().a("Event filter result", bool2 != 0 ? "null" : bool2);
                                    if (bool2 != 0) {
                                    }
                                }
                            }
                        }
                    }
                    l3 = l;
                    j2 = j;
                    ggVar2 = ggVar;
                    i3 = i4 + 1;
                }
                ghVarArr = ghVarArr2;
                str2 = str3;
                l = l3;
                j = j2;
                ggVar = ggVar2;
                a2 = t_().a(str, ggVar3.d);
                if (a2 != null) {
                }
                t_().a(a3);
                long j42 = a3.c;
                map2 = (Map) aVar4.get(str2);
                if (map2 != null) {
                }
                while (r11.hasNext()) {
                }
                l3 = l;
                j2 = j;
                ggVar2 = ggVar;
                i3 = i4 + 1;
            }
        }
        if (glVarArr != null) {
            a aVar6 = new a();
            for (gl glVar : glVarArr) {
                Map map4 = (Map) aVar6.get(glVar.d);
                if (map4 == null) {
                    Map g = t_().g(str, glVar.d);
                    if (g == null) {
                        g = new a();
                    }
                    aVar6.put(glVar.d, g);
                    map = g;
                } else {
                    map = map4;
                }
                for (Integer intValue5 : map.keySet()) {
                    int intValue6 = intValue5.intValue();
                    if (!hashSet.contains(Integer.valueOf(intValue6))) {
                        BitSet bitSet9 = (BitSet) aVar2.get(Integer.valueOf(intValue6));
                        BitSet bitSet10 = (BitSet) aVar3.get(Integer.valueOf(intValue6));
                        if (((gf) aVar.get(Integer.valueOf(intValue6))) == null) {
                            gf gfVar3 = new gf();
                            aVar.put(Integer.valueOf(intValue6), gfVar3);
                            gfVar3.f = Boolean.valueOf(true);
                            bitSet9 = new BitSet();
                            aVar2.put(Integer.valueOf(intValue6), bitSet9);
                            bitSet10 = new BitSet();
                            aVar3.put(Integer.valueOf(intValue6), bitSet10);
                        }
                        Iterator it = ((List) map.get(Integer.valueOf(intValue6))).iterator();
                        while (true) {
                            if (!it.hasNext()) {
                                break;
                            }
                            ga gaVar = (ga) it.next();
                            if (q().a(2)) {
                                q().C().a("Evaluating filter. audience, filter, property", Integer.valueOf(intValue6), gaVar.c, m().c(gaVar.d));
                                q().C().a("Filter definition", m().a(gaVar));
                            }
                            if (gaVar.c == null || gaVar.c.intValue() > 256) {
                                q().y().a("Invalid property filter ID. appId, id", az.a(str), String.valueOf(gaVar.c));
                                hashSet.add(Integer.valueOf(intValue6));
                            } else if (bitSet9.get(gaVar.c.intValue())) {
                                q().C().a("Property filter already evaluated true. audience ID, filter ID", Integer.valueOf(intValue6), gaVar.c);
                            } else {
                                fy fyVar3 = gaVar.e;
                                if (fyVar3 == null) {
                                    q().y().a("Missing property filter. property", m().c(glVar.d));
                                    bool = 0;
                                } else {
                                    boolean equals2 = Boolean.TRUE.equals(fyVar3.e);
                                    if (glVar.f != null) {
                                        if (fyVar3.d == null) {
                                            q().y().a("No number filter for long property. property", m().c(glVar.d));
                                            bool = 0;
                                        } else {
                                            bool = a(a(glVar.f.longValue(), fyVar3.d), equals2);
                                        }
                                    } else if (glVar.g != null) {
                                        if (fyVar3.d == null) {
                                            q().y().a("No number filter for double property. property", m().c(glVar.d));
                                            bool = 0;
                                        } else {
                                            bool = a(a(glVar.g.doubleValue(), fyVar3.d), equals2);
                                        }
                                    } else if (glVar.e == null) {
                                        q().y().a("User property has no value, property", m().c(glVar.d));
                                        bool = 0;
                                    } else if (fyVar3.c == null) {
                                        if (fyVar3.d == null) {
                                            q().y().a("No string or number filter defined. property", m().c(glVar.d));
                                        } else if (ft.j(glVar.e)) {
                                            bool = a(a(glVar.e, fyVar3.d), equals2);
                                        } else {
                                            q().y().a("Invalid user property value for Numeric number filter. property, value", m().c(glVar.d), glVar.e);
                                        }
                                        bool = 0;
                                    } else {
                                        bool = a(a(glVar.e, fyVar3.c), equals2);
                                    }
                                }
                                q().C().a("Property filter result", bool == 0 ? "null" : bool);
                                if (bool == 0) {
                                    hashSet.add(Integer.valueOf(intValue6));
                                } else {
                                    bitSet10.set(gaVar.c.intValue());
                                    if (bool.booleanValue()) {
                                        bitSet9.set(gaVar.c.intValue());
                                    }
                                }
                            }
                        }
                    } else {
                        q().C().a("Skipping failed audience ID", Integer.valueOf(intValue6));
                    }
                }
            }
        }
        gf[] gfVarArr = new gf[aVar2.size()];
        int i13 = 0;
        for (Integer intValue7 : aVar2.keySet()) {
            int intValue8 = intValue7.intValue();
            if (!hashSet.contains(Integer.valueOf(intValue8))) {
                gf gfVar4 = (gf) aVar.get(Integer.valueOf(intValue8));
                gf gfVar5 = gfVar4 == null ? new gf() : gfVar4;
                int i14 = i13 + 1;
                gfVarArr[i13] = gfVar5;
                gfVar5.c = Integer.valueOf(intValue8);
                gfVar5.d = new gk();
                gfVar5.d.d = ft.a((BitSet) aVar2.get(Integer.valueOf(intValue8)));
                gfVar5.d.c = ft.a((BitSet) aVar3.get(Integer.valueOf(intValue8)));
                ab t_2 = t_();
                gk gkVar2 = gfVar5.d;
                t_2.N();
                t_2.c();
                aa.a(str);
                aa.a(gkVar2);
                try {
                    byte[] bArr = new byte[gkVar2.d()];
                    b a9 = b.a(bArr, 0, bArr.length);
                    gkVar2.a(a9);
                    a9.a();
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("app_id", str);
                    contentValues.put("audience_id", Integer.valueOf(intValue8));
                    contentValues.put("current_results", bArr);
                    try {
                        if (t_2.x().insertWithOnConflict("audience_filter_values", null, contentValues, 5) == -1) {
                            t_2.q().v().a("Failed to insert filter results (got -1). appId", az.a(str));
                        }
                        i13 = i14;
                    } catch (SQLiteException e3) {
                        t_2.q().v().a("Error storing filter results. appId", az.a(str), e3);
                        i13 = i14;
                    }
                } catch (IOException e4) {
                    t_2.q().v().a("Configuration loss. Failed to serialize filter results. appId", az.a(str), e4);
                    i13 = i14;
                }
            }
        }
        return (gf[]) Arrays.copyOf(gfVarArr, i13);
    }

    /* access modifiers changed from: protected */
    public final boolean t() {
        return false;
    }
}
