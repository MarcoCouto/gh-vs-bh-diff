package com.google.android.gms.internal.c;

import android.content.Context;
import com.google.android.gms.common.internal.aa;
import com.google.android.gms.common.util.e;

class cz implements db {
    protected final ce q;

    cz(ce ceVar) {
        aa.a(ceVar);
        this.q = ceVar;
    }

    public void a() {
        ce.z();
    }

    public void b() {
        this.q.p().b();
    }

    public void c() {
        this.q.p().c();
    }

    public n d() {
        return this.q.v();
    }

    public dd e() {
        return this.q.h();
    }

    public au f() {
        return this.q.u();
    }

    public ah g() {
        return this.q.t();
    }

    public eb h() {
        return this.q.s();
    }

    public dy i() {
        return this.q.r();
    }

    public e j() {
        return this.q.j();
    }

    public Context k() {
        return this.q.k();
    }

    public av l() {
        return this.q.o();
    }

    public ax m() {
        return this.q.n();
    }

    public ft n() {
        return this.q.m();
    }

    public fa o() {
        return this.q.e();
    }

    public bz p() {
        return this.q.p();
    }

    public az q() {
        return this.q.q();
    }

    public bk r() {
        return this.q.c();
    }

    public y s() {
        return this.q.b();
    }
}
