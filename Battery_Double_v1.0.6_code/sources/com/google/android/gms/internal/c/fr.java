package com.google.android.gms.internal.c;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.a.b;

public final class fr implements Creator<fq> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        Double d = null;
        int b2 = b.b(parcel);
        int i = 0;
        long j = 0;
        String str = null;
        String str2 = null;
        Float f = null;
        Long l = null;
        String str3 = null;
        while (parcel.dataPosition() < b2) {
            int a2 = b.a(parcel);
            switch (b.a(a2)) {
                case 1:
                    i = b.d(parcel, a2);
                    break;
                case 2:
                    str3 = b.k(parcel, a2);
                    break;
                case 3:
                    j = b.f(parcel, a2);
                    break;
                case 4:
                    l = b.g(parcel, a2);
                    break;
                case 5:
                    f = b.i(parcel, a2);
                    break;
                case 6:
                    str2 = b.k(parcel, a2);
                    break;
                case 7:
                    str = b.k(parcel, a2);
                    break;
                case 8:
                    d = b.j(parcel, a2);
                    break;
                default:
                    b.b(parcel, a2);
                    break;
            }
        }
        b.r(parcel, b2);
        return new fq(i, str3, j, l, f, str2, str, d);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new fq[i];
    }
}
