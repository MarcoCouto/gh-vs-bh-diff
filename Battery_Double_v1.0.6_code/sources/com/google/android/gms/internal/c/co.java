package com.google.android.gms.internal.c;

import java.util.List;
import java.util.concurrent.Callable;

final class co implements Callable<List<w>> {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ s f3940a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ String f3941b;
    private final /* synthetic */ String c;
    private final /* synthetic */ cg d;

    co(cg cgVar, s sVar, String str, String str2) {
        this.d = cgVar;
        this.f3940a = sVar;
        this.f3941b = str;
        this.c = str2;
    }

    public final /* synthetic */ Object call() throws Exception {
        this.d.f3924a.H();
        return this.d.f3924a.D().b(this.f3940a.f4127a, this.f3941b, this.c);
    }
}
