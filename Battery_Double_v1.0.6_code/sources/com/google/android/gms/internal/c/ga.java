package com.google.android.gms.internal.c;

import java.io.IOException;

public final class ga extends d<ga> {
    private static volatile ga[] f;
    public Integer c;
    public String d;
    public fy e;

    public ga() {
        this.c = null;
        this.d = null;
        this.e = null;
        this.f3962a = null;
        this.f4112b = -1;
    }

    public static ga[] e() {
        if (f == null) {
            synchronized (h.f4106b) {
                if (f == null) {
                    f = new ga[0];
                }
            }
        }
        return f;
    }

    /* access modifiers changed from: protected */
    public final int a() {
        int a2 = super.a();
        if (this.c != null) {
            a2 += b.b(1, this.c.intValue());
        }
        if (this.d != null) {
            a2 += b.b(2, this.d);
        }
        return this.e != null ? a2 + b.b(3, (j) this.e) : a2;
    }

    public final /* synthetic */ j a(a aVar) throws IOException {
        while (true) {
            int a2 = aVar.a();
            switch (a2) {
                case 0:
                    break;
                case 8:
                    this.c = Integer.valueOf(aVar.d());
                    continue;
                case 18:
                    this.d = aVar.c();
                    continue;
                case 26:
                    if (this.e == null) {
                        this.e = new fy();
                    }
                    aVar.a((j) this.e);
                    continue;
                default:
                    if (!super.a(aVar, a2)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    public final void a(b bVar) throws IOException {
        if (this.c != null) {
            bVar.a(1, this.c.intValue());
        }
        if (this.d != null) {
            bVar.a(2, this.d);
        }
        if (this.e != null) {
            bVar.a(3, (j) this.e);
        }
        super.a(bVar);
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ga)) {
            return false;
        }
        ga gaVar = (ga) obj;
        if (this.c == null) {
            if (gaVar.c != null) {
                return false;
            }
        } else if (!this.c.equals(gaVar.c)) {
            return false;
        }
        if (this.d == null) {
            if (gaVar.d != null) {
                return false;
            }
        } else if (!this.d.equals(gaVar.d)) {
            return false;
        }
        if (this.e == null) {
            if (gaVar.e != null) {
                return false;
            }
        } else if (!this.e.equals(gaVar.e)) {
            return false;
        }
        return (this.f3962a == null || this.f3962a.b()) ? gaVar.f3962a == null || gaVar.f3962a.b() : this.f3962a.equals(gaVar.f3962a);
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = (this.d == null ? 0 : this.d.hashCode()) + (((this.c == null ? 0 : this.c.hashCode()) + ((getClass().getName().hashCode() + 527) * 31)) * 31);
        fy fyVar = this.e;
        int hashCode2 = ((fyVar == null ? 0 : fyVar.hashCode()) + (hashCode * 31)) * 31;
        if (this.f3962a != null && !this.f3962a.b()) {
            i = this.f3962a.hashCode();
        }
        return hashCode2 + i;
    }
}
