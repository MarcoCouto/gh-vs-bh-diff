package com.google.android.gms.internal.c;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.a.a;
import com.google.android.gms.common.internal.a.c;
import com.google.android.gms.common.internal.aa;

public final class w extends a {
    public static final Creator<w> CREATOR = new x();

    /* renamed from: a reason: collision with root package name */
    public String f4129a;

    /* renamed from: b reason: collision with root package name */
    public String f4130b;
    public fq c;
    public long d;
    public boolean e;
    public String f;
    public an g;
    public long h;
    public an i;
    public long j;
    public an k;

    w(w wVar) {
        aa.a(wVar);
        this.f4129a = wVar.f4129a;
        this.f4130b = wVar.f4130b;
        this.c = wVar.c;
        this.d = wVar.d;
        this.e = wVar.e;
        this.f = wVar.f;
        this.g = wVar.g;
        this.h = wVar.h;
        this.i = wVar.i;
        this.j = wVar.j;
        this.k = wVar.k;
    }

    w(String str, String str2, fq fqVar, long j2, boolean z, String str3, an anVar, long j3, an anVar2, long j4, an anVar3) {
        this.f4129a = str;
        this.f4130b = str2;
        this.c = fqVar;
        this.d = j2;
        this.e = z;
        this.f = str3;
        this.g = anVar;
        this.h = j3;
        this.i = anVar2;
        this.j = j4;
        this.k = anVar3;
    }

    public final void writeToParcel(Parcel parcel, int i2) {
        int a2 = c.a(parcel);
        c.a(parcel, 2, this.f4129a, false);
        c.a(parcel, 3, this.f4130b, false);
        c.a(parcel, 4, (Parcelable) this.c, i2, false);
        c.a(parcel, 5, this.d);
        c.a(parcel, 6, this.e);
        c.a(parcel, 7, this.f, false);
        c.a(parcel, 8, (Parcelable) this.g, i2, false);
        c.a(parcel, 9, this.h);
        c.a(parcel, 10, (Parcelable) this.i, i2, false);
        c.a(parcel, 11, this.j);
        c.a(parcel, 12, (Parcelable) this.k, i2, false);
        c.a(parcel, a2);
    }
}
