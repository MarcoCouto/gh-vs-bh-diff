package com.google.android.gms.internal.c;

import android.os.RemoteException;

final class eg implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ dx f4022a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ eb f4023b;

    eg(eb ebVar, dx dxVar) {
        this.f4023b = ebVar;
        this.f4022a = dxVar;
    }

    public final void run() {
        ar d = this.f4023b.f4014b;
        if (d == null) {
            this.f4023b.q().v().a("Failed to send current screen to service");
            return;
        }
        try {
            if (this.f4022a == null) {
                d.a(0, (String) null, (String) null, this.f4023b.k().getPackageName());
            } else {
                d.a(this.f4022a.c, this.f4022a.f4003a, this.f4022a.f4004b, this.f4023b.k().getPackageName());
            }
            this.f4023b.C();
        } catch (RemoteException e) {
            this.f4023b.q().v().a("Failed to send current screen to the service", e);
        }
    }
}
