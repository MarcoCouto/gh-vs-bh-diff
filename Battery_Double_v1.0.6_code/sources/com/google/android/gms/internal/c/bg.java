package com.google.android.gms.internal.c;

import com.google.android.gms.common.internal.aa;
import java.util.List;
import java.util.Map;

final class bg implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final bf f3886a;

    /* renamed from: b reason: collision with root package name */
    private final int f3887b;
    private final Throwable c;
    private final byte[] d;
    private final String e;
    private final Map<String, List<String>> f;

    private bg(String str, bf bfVar, int i, Throwable th, byte[] bArr, Map<String, List<String>> map) {
        aa.a(bfVar);
        this.f3886a = bfVar;
        this.f3887b = i;
        this.c = th;
        this.d = bArr;
        this.e = str;
        this.f = map;
    }

    public final void run() {
        this.f3886a.a(this.e, this.f3887b, this.c, this.d, this.f);
    }
}
