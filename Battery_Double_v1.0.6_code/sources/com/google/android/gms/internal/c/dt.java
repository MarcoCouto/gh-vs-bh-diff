package com.google.android.gms.internal.c;

final class dt implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ long f3996a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ dd f3997b;

    dt(dd ddVar, long j) {
        this.f3997b = ddVar;
        this.f3996a = j;
    }

    public final void run() {
        this.f3997b.r().k.a(this.f3996a);
        this.f3997b.q().B().a("Minimum session duration set", Long.valueOf(this.f3996a));
    }
}
