package com.google.android.gms.internal.c;

abstract class fj extends fi {

    /* renamed from: b reason: collision with root package name */
    private boolean f4072b;

    fj(fk fkVar) {
        super(fkVar);
        this.f4071a.a(this);
    }

    /* access modifiers changed from: 0000 */
    public final boolean M() {
        return this.f4072b;
    }

    /* access modifiers changed from: protected */
    public final void N() {
        if (!M()) {
            throw new IllegalStateException("Not initialized");
        }
    }

    public final void O() {
        if (this.f4072b) {
            throw new IllegalStateException("Can't initialize twice");
        }
        t();
        this.f4071a.I();
        this.f4072b = true;
    }

    /* access modifiers changed from: protected */
    public abstract boolean t();
}
