package com.google.android.gms.internal.c;

import java.io.IOException;

public final class fy extends d<fy> {
    private static volatile fy[] g;
    public gb c;
    public fz d;
    public Boolean e;
    public String f;

    public fy() {
        this.c = null;
        this.d = null;
        this.e = null;
        this.f = null;
        this.f3962a = null;
        this.f4112b = -1;
    }

    public static fy[] e() {
        if (g == null) {
            synchronized (h.f4106b) {
                if (g == null) {
                    g = new fy[0];
                }
            }
        }
        return g;
    }

    /* access modifiers changed from: protected */
    public final int a() {
        int a2 = super.a();
        if (this.c != null) {
            a2 += b.b(1, (j) this.c);
        }
        if (this.d != null) {
            a2 += b.b(2, (j) this.d);
        }
        if (this.e != null) {
            this.e.booleanValue();
            a2 += b.b(3) + 1;
        }
        return this.f != null ? a2 + b.b(4, this.f) : a2;
    }

    public final /* synthetic */ j a(a aVar) throws IOException {
        while (true) {
            int a2 = aVar.a();
            switch (a2) {
                case 0:
                    break;
                case 10:
                    if (this.c == null) {
                        this.c = new gb();
                    }
                    aVar.a((j) this.c);
                    continue;
                case 18:
                    if (this.d == null) {
                        this.d = new fz();
                    }
                    aVar.a((j) this.d);
                    continue;
                case 24:
                    this.e = Boolean.valueOf(aVar.b());
                    continue;
                case 34:
                    this.f = aVar.c();
                    continue;
                default:
                    if (!super.a(aVar, a2)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    public final void a(b bVar) throws IOException {
        if (this.c != null) {
            bVar.a(1, (j) this.c);
        }
        if (this.d != null) {
            bVar.a(2, (j) this.d);
        }
        if (this.e != null) {
            bVar.a(3, this.e.booleanValue());
        }
        if (this.f != null) {
            bVar.a(4, this.f);
        }
        super.a(bVar);
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof fy)) {
            return false;
        }
        fy fyVar = (fy) obj;
        if (this.c == null) {
            if (fyVar.c != null) {
                return false;
            }
        } else if (!this.c.equals(fyVar.c)) {
            return false;
        }
        if (this.d == null) {
            if (fyVar.d != null) {
                return false;
            }
        } else if (!this.d.equals(fyVar.d)) {
            return false;
        }
        if (this.e == null) {
            if (fyVar.e != null) {
                return false;
            }
        } else if (!this.e.equals(fyVar.e)) {
            return false;
        }
        if (this.f == null) {
            if (fyVar.f != null) {
                return false;
            }
        } else if (!this.f.equals(fyVar.f)) {
            return false;
        }
        return (this.f3962a == null || this.f3962a.b()) ? fyVar.f3962a == null || fyVar.f3962a.b() : this.f3962a.equals(fyVar.f3962a);
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = getClass().getName().hashCode() + 527;
        gb gbVar = this.c;
        int hashCode2 = (gbVar == null ? 0 : gbVar.hashCode()) + (hashCode * 31);
        fz fzVar = this.d;
        int hashCode3 = ((this.f == null ? 0 : this.f.hashCode()) + (((this.e == null ? 0 : this.e.hashCode()) + (((fzVar == null ? 0 : fzVar.hashCode()) + (hashCode2 * 31)) * 31)) * 31)) * 31;
        if (this.f3962a != null && !this.f3962a.b()) {
            i = this.f3962a.hashCode();
        }
        return hashCode3 + i;
    }
}
