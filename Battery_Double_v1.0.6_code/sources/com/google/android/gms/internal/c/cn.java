package com.google.android.gms.internal.c;

import java.util.List;
import java.util.concurrent.Callable;

final class cn implements Callable<List<fs>> {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ String f3938a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ String f3939b;
    private final /* synthetic */ String c;
    private final /* synthetic */ cg d;

    cn(cg cgVar, String str, String str2, String str3) {
        this.d = cgVar;
        this.f3938a = str;
        this.f3939b = str2;
        this.c = str3;
    }

    public final /* synthetic */ Object call() throws Exception {
        this.d.f3924a.H();
        return this.d.f3924a.D().a(this.f3938a, this.f3939b, this.c);
    }
}
