package com.google.android.gms.internal.c;

import com.google.android.gms.internal.c.d;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public final class e<M extends d<M>, T> {

    /* renamed from: a reason: collision with root package name */
    protected final Class<T> f4009a;

    /* renamed from: b reason: collision with root package name */
    public final int f4010b;
    protected final boolean c;
    private final int d;

    private final Object a(a aVar) {
        Class<T> cls = this.c ? this.f4009a.getComponentType() : this.f4009a;
        try {
            switch (this.d) {
                case 10:
                    j jVar = (j) cls.newInstance();
                    aVar.a(jVar, this.f4010b >>> 3);
                    return jVar;
                case 11:
                    j jVar2 = (j) cls.newInstance();
                    aVar.a(jVar2);
                    return jVar2;
                default:
                    throw new IllegalArgumentException("Unknown type " + this.d);
            }
        } catch (InstantiationException e) {
            String valueOf = String.valueOf(cls);
            throw new IllegalArgumentException(new StringBuilder(String.valueOf(valueOf).length() + 33).append("Error creating instance of class ").append(valueOf).toString(), e);
        } catch (IllegalAccessException e2) {
            String valueOf2 = String.valueOf(cls);
            throw new IllegalArgumentException(new StringBuilder(String.valueOf(valueOf2).length() + 33).append("Error creating instance of class ").append(valueOf2).toString(), e2);
        } catch (IOException e3) {
            throw new IllegalArgumentException("Error reading extension field", e3);
        }
    }

    /* access modifiers changed from: protected */
    public final int a(Object obj) {
        int i = this.f4010b >>> 3;
        switch (this.d) {
            case 10:
                return (b.b(i) << 1) + ((j) obj).d();
            case 11:
                return b.b(i, (j) obj);
            default:
                throw new IllegalArgumentException("Unknown type " + this.d);
        }
    }

    /* access modifiers changed from: 0000 */
    public final T a(List<l> list) {
        if (list == null) {
            return null;
        }
        if (this.c) {
            ArrayList arrayList = new ArrayList();
            for (int i = 0; i < list.size(); i++) {
                l lVar = (l) list.get(i);
                if (lVar.f4114b.length != 0) {
                    arrayList.add(a(a.a(lVar.f4114b)));
                }
            }
            int size = arrayList.size();
            if (size == 0) {
                return null;
            }
            T cast = this.f4009a.cast(Array.newInstance(this.f4009a.getComponentType(), size));
            for (int i2 = 0; i2 < size; i2++) {
                Array.set(cast, i2, arrayList.get(i2));
            }
            return cast;
        } else if (list.isEmpty()) {
            return null;
        } else {
            return this.f4009a.cast(a(a.a(((l) list.get(list.size() - 1)).f4114b)));
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Object obj, b bVar) {
        try {
            bVar.c(this.f4010b);
            switch (this.d) {
                case 10:
                    int i = this.f4010b >>> 3;
                    ((j) obj).a(bVar);
                    bVar.c(i, 4);
                    return;
                case 11:
                    bVar.a((j) obj);
                    return;
                default:
                    throw new IllegalArgumentException("Unknown type " + this.d);
            }
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
        throw new IllegalStateException(e);
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof e)) {
            return false;
        }
        e eVar = (e) obj;
        return this.d == eVar.d && this.f4009a == eVar.f4009a && this.f4010b == eVar.f4010b && this.c == eVar.c;
    }

    public final int hashCode() {
        return (this.c ? 1 : 0) + ((((((this.d + 1147) * 31) + this.f4009a.hashCode()) * 31) + this.f4010b) * 31);
    }
}
