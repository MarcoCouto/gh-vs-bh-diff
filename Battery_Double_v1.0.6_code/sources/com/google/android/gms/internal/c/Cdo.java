package com.google.android.gms.internal.c;

import java.util.concurrent.atomic.AtomicReference;

/* renamed from: com.google.android.gms.internal.c.do reason: invalid class name */
final class Cdo implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ AtomicReference f3986a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ dd f3987b;

    Cdo(dd ddVar, AtomicReference atomicReference) {
        this.f3987b = ddVar;
        this.f3986a = atomicReference;
    }

    public final void run() {
        synchronized (this.f3986a) {
            try {
                this.f3986a.set(this.f3987b.s().A());
                this.f3986a.notify();
            } catch (Throwable th) {
                this.f3986a.notify();
                throw th;
            }
        }
    }
}
