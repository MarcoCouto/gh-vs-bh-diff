package com.google.android.gms.internal.c;

import android.os.RemoteException;
import java.util.concurrent.atomic.AtomicReference;

final class ee implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ AtomicReference f4018a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ s f4019b;
    private final /* synthetic */ eb c;

    ee(eb ebVar, AtomicReference atomicReference, s sVar) {
        this.c = ebVar;
        this.f4018a = atomicReference;
        this.f4019b = sVar;
    }

    /* JADX INFO: finally extract failed */
    public final void run() {
        synchronized (this.f4018a) {
            try {
                ar d = this.c.f4014b;
                if (d == null) {
                    this.c.q().v().a("Failed to get app instance id");
                    this.f4018a.notify();
                    return;
                }
                this.f4018a.set(d.c(this.f4019b));
                String str = (String) this.f4018a.get();
                if (str != null) {
                    this.c.e().a(str);
                    this.c.r().j.a(str);
                }
                this.c.C();
                this.f4018a.notify();
            } catch (RemoteException e) {
                this.c.q().v().a("Failed to get app instance id", e);
                this.f4018a.notify();
            } catch (Throwable th) {
                this.f4018a.notify();
                throw th;
            }
        }
    }
}
