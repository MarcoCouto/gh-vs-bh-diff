package com.google.android.gms.internal.c;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.h.a;
import com.google.android.gms.common.internal.aa;
import com.google.android.gms.common.util.e;
import java.util.Map;

public final class n extends cz {

    /* renamed from: a reason: collision with root package name */
    private final Map<String, Long> f4117a = new a();

    /* renamed from: b reason: collision with root package name */
    private final Map<String, Integer> f4118b = new a();
    private long c;

    public n(ce ceVar) {
        super(ceVar);
    }

    private final void a(long j, dx dxVar) {
        if (dxVar == null) {
            q().C().a("Not logging ad exposure. No active activity");
        } else if (j < 1000) {
            q().C().a("Not logging ad exposure. Less than 1000 ms. exposure", Long.valueOf(j));
        } else {
            Bundle bundle = new Bundle();
            bundle.putLong("_xt", j);
            dy.a(dxVar, bundle, true);
            e().a("am", "_xa", bundle);
        }
    }

    /* access modifiers changed from: private */
    public final void a(String str, long j) {
        c();
        aa.a(str);
        if (this.f4118b.isEmpty()) {
            this.c = j;
        }
        Integer num = (Integer) this.f4118b.get(str);
        if (num != null) {
            this.f4118b.put(str, Integer.valueOf(num.intValue() + 1));
        } else if (this.f4118b.size() >= 100) {
            q().y().a("Too many ads visible");
        } else {
            this.f4118b.put(str, Integer.valueOf(1));
            this.f4117a.put(str, Long.valueOf(j));
        }
    }

    private final void a(String str, long j, dx dxVar) {
        if (dxVar == null) {
            q().C().a("Not logging ad unit exposure. No active activity");
        } else if (j < 1000) {
            q().C().a("Not logging ad unit exposure. Less than 1000 ms. exposure", Long.valueOf(j));
        } else {
            Bundle bundle = new Bundle();
            bundle.putString("_ai", str);
            bundle.putLong("_xt", j);
            dy.a(dxVar, bundle, true);
            e().a("am", "_xu", bundle);
        }
    }

    /* access modifiers changed from: private */
    public final void b(long j) {
        for (String put : this.f4117a.keySet()) {
            this.f4117a.put(put, Long.valueOf(j));
        }
        if (!this.f4117a.isEmpty()) {
            this.c = j;
        }
    }

    /* access modifiers changed from: private */
    public final void b(String str, long j) {
        c();
        aa.a(str);
        Integer num = (Integer) this.f4118b.get(str);
        if (num != null) {
            dx v = i().v();
            int intValue = num.intValue() - 1;
            if (intValue == 0) {
                this.f4118b.remove(str);
                Long l = (Long) this.f4117a.get(str);
                if (l == null) {
                    q().v().a("First ad unit exposure time was never set");
                } else {
                    long longValue = j - l.longValue();
                    this.f4117a.remove(str);
                    a(str, longValue, v);
                }
                if (!this.f4118b.isEmpty()) {
                    return;
                }
                if (this.c == 0) {
                    q().v().a("First ad exposure time was never set");
                    return;
                }
                a(j - this.c, v);
                this.c = 0;
                return;
            }
            this.f4118b.put(str, Integer.valueOf(intValue));
            return;
        }
        q().v().a("Call to endAdUnitExposure for unknown ad unit id", str);
    }

    public final /* bridge */ /* synthetic */ void a() {
        super.a();
    }

    public final void a(long j) {
        dx v = i().v();
        for (String str : this.f4117a.keySet()) {
            a(str, j - ((Long) this.f4117a.get(str)).longValue(), v);
        }
        if (!this.f4117a.isEmpty()) {
            a(j - this.c, v);
        }
        b(j);
    }

    public final void a(String str) {
        if (str == null || str.length() == 0) {
            q().v().a("Ad unit id must be a non-empty string");
            return;
        }
        p().a((Runnable) new o(this, str, j().b()));
    }

    public final /* bridge */ /* synthetic */ void b() {
        super.b();
    }

    public final void b(String str) {
        if (str == null || str.length() == 0) {
            q().v().a("Ad unit id must be a non-empty string");
            return;
        }
        p().a((Runnable) new p(this, str, j().b()));
    }

    public final /* bridge */ /* synthetic */ void c() {
        super.c();
    }

    public final /* bridge */ /* synthetic */ n d() {
        return super.d();
    }

    public final /* bridge */ /* synthetic */ dd e() {
        return super.e();
    }

    public final /* bridge */ /* synthetic */ au f() {
        return super.f();
    }

    public final /* bridge */ /* synthetic */ ah g() {
        return super.g();
    }

    public final /* bridge */ /* synthetic */ eb h() {
        return super.h();
    }

    public final /* bridge */ /* synthetic */ dy i() {
        return super.i();
    }

    public final /* bridge */ /* synthetic */ e j() {
        return super.j();
    }

    public final /* bridge */ /* synthetic */ Context k() {
        return super.k();
    }

    public final /* bridge */ /* synthetic */ av l() {
        return super.l();
    }

    public final /* bridge */ /* synthetic */ ax m() {
        return super.m();
    }

    public final /* bridge */ /* synthetic */ ft n() {
        return super.n();
    }

    public final /* bridge */ /* synthetic */ fa o() {
        return super.o();
    }

    public final /* bridge */ /* synthetic */ bz p() {
        return super.p();
    }

    public final /* bridge */ /* synthetic */ az q() {
        return super.q();
    }

    public final /* bridge */ /* synthetic */ bk r() {
        return super.r();
    }

    public final /* bridge */ /* synthetic */ y s() {
        return super.s();
    }
}
