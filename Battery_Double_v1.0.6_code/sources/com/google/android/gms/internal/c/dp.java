package com.google.android.gms.internal.c;

import java.util.concurrent.atomic.AtomicReference;

final class dp implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ AtomicReference f3988a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ dd f3989b;

    dp(dd ddVar, AtomicReference atomicReference) {
        this.f3989b = ddVar;
        this.f3988a = atomicReference;
    }

    public final void run() {
        synchronized (this.f3988a) {
            try {
                AtomicReference atomicReference = this.f3988a;
                y s = this.f3989b.s();
                atomicReference.set(Long.valueOf(s.a(s.f().w(), ap.K)));
                this.f3988a.notify();
            } catch (Throwable th) {
                this.f3988a.notify();
                throw th;
            }
        }
    }
}
