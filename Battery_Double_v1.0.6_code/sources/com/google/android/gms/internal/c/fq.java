package com.google.android.gms.internal.c;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.a.a;
import com.google.android.gms.common.internal.a.c;
import com.google.android.gms.common.internal.aa;

public final class fq extends a {
    public static final Creator<fq> CREATOR = new fr();

    /* renamed from: a reason: collision with root package name */
    public final String f4082a;

    /* renamed from: b reason: collision with root package name */
    public final long f4083b;
    public final String c;
    private final int d;
    private final Long e;
    private final Float f;
    private final String g;
    private final Double h;

    fq(int i, String str, long j, Long l, Float f2, String str2, String str3, Double d2) {
        Double d3 = null;
        this.d = i;
        this.f4082a = str;
        this.f4083b = j;
        this.e = l;
        this.f = null;
        if (i == 1) {
            if (f2 != null) {
                d3 = Double.valueOf(f2.doubleValue());
            }
            this.h = d3;
        } else {
            this.h = d2;
        }
        this.g = str2;
        this.c = str3;
    }

    fq(fs fsVar) {
        this(fsVar.c, fsVar.d, fsVar.e, fsVar.f4085b);
    }

    fq(String str, long j, Object obj, String str2) {
        aa.a(str);
        this.d = 2;
        this.f4082a = str;
        this.f4083b = j;
        this.c = str2;
        if (obj == null) {
            this.e = null;
            this.f = null;
            this.h = null;
            this.g = null;
        } else if (obj instanceof Long) {
            this.e = (Long) obj;
            this.f = null;
            this.h = null;
            this.g = null;
        } else if (obj instanceof String) {
            this.e = null;
            this.f = null;
            this.h = null;
            this.g = (String) obj;
        } else if (obj instanceof Double) {
            this.e = null;
            this.f = null;
            this.h = (Double) obj;
            this.g = null;
        } else {
            throw new IllegalArgumentException("User attribute given of un-supported type");
        }
    }

    public final Object a() {
        if (this.e != null) {
            return this.e;
        }
        if (this.h != null) {
            return this.h;
        }
        if (this.g != null) {
            return this.g;
        }
        return null;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = c.a(parcel);
        c.a(parcel, 1, this.d);
        c.a(parcel, 2, this.f4082a, false);
        c.a(parcel, 3, this.f4083b);
        c.a(parcel, 4, this.e, false);
        c.a(parcel, 5, (Float) null, false);
        c.a(parcel, 6, this.g, false);
        c.a(parcel, 7, this.c, false);
        c.a(parcel, 8, this.h, false);
        c.a(parcel, a2);
    }
}
