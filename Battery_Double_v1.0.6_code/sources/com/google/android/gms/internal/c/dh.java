package com.google.android.gms.internal.c;

import java.util.concurrent.Callable;
import java.util.concurrent.TimeoutException;

final class dh implements Callable<String> {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ dd f3973a;

    dh(dd ddVar) {
        this.f3973a = ddVar;
    }

    public final /* synthetic */ Object call() throws Exception {
        String w = this.f3973a.r().w();
        if (w == null) {
            dd e = this.f3973a.e();
            if (e.p().w()) {
                e.q().v().a("Cannot retrieve app instance id from analytics worker thread");
                w = null;
            } else {
                e.p();
                if (bz.v()) {
                    e.q().v().a("Cannot retrieve app instance id from main thread");
                    w = null;
                } else {
                    long b2 = e.j().b();
                    w = e.c(120000);
                    long b3 = e.j().b() - b2;
                    if (w == null && b3 < 120000) {
                        w = e.c(120000 - b3);
                    }
                }
            }
            if (w == null) {
                throw new TimeoutException();
            }
            this.f3973a.r().d(w);
        }
        return w;
    }
}
