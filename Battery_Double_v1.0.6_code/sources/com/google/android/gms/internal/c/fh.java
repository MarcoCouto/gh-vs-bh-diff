package com.google.android.gms.internal.c;

final class fh extends af {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ fk f4069a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ fg f4070b;

    fh(fg fgVar, db dbVar, fk fkVar) {
        this.f4070b = fgVar;
        this.f4069a = fkVar;
        super(dbVar);
    }

    public final void a() {
        this.f4070b.u();
        this.f4070b.q().C().a("Starting upload from DelayedRunnable");
        this.f4069a.G();
    }
}
