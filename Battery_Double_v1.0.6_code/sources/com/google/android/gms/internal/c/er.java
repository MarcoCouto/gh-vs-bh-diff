package com.google.android.gms.internal.c;

import android.content.ComponentName;

final class er implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ ComponentName f4043a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ ep f4044b;

    er(ep epVar, ComponentName componentName) {
        this.f4044b = epVar;
        this.f4043a = componentName;
    }

    public final void run() {
        this.f4044b.f4039a.a(this.f4043a);
    }
}
