package com.google.android.gms.internal.c;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.os.RemoteException;
import com.google.android.gms.common.internal.aa;
import com.google.android.gms.common.internal.e.a;
import com.google.android.gms.common.internal.e.b;

public final class ep implements ServiceConnection, a, b {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ eb f4039a;
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public volatile boolean f4040b;
    private volatile ay c;

    protected ep(eb ebVar) {
        this.f4039a = ebVar;
    }

    public final void a() {
        this.f4039a.c();
        Context k = this.f4039a.k();
        synchronized (this) {
            if (this.f4040b) {
                this.f4039a.q().C().a("Connection attempt already in progress");
            } else if (this.c != null) {
                this.f4039a.q().C().a("Already awaiting connection attempt");
            } else {
                this.c = new ay(k, Looper.getMainLooper(), this, this);
                this.f4039a.q().C().a("Connecting to remote service");
                this.f4040b = true;
                this.c.o();
            }
        }
    }

    public final void a(int i) {
        aa.b("MeasurementServiceConnection.onConnectionSuspended");
        this.f4039a.q().B().a("Service connection suspended");
        this.f4039a.p().a((Runnable) new et(this));
    }

    public final void a(Intent intent) {
        this.f4039a.c();
        Context k = this.f4039a.k();
        com.google.android.gms.common.a.a a2 = com.google.android.gms.common.a.a.a();
        synchronized (this) {
            if (this.f4040b) {
                this.f4039a.q().C().a("Connection attempt already in progress");
                return;
            }
            this.f4039a.q().C().a("Using local app measurement service");
            this.f4040b = true;
            a2.a(k, intent, (ServiceConnection) this.f4039a.f4013a, 129);
        }
    }

    public final void a(Bundle bundle) {
        aa.b("MeasurementServiceConnection.onConnected");
        synchronized (this) {
            try {
                ar arVar = (ar) this.c.x();
                this.c = null;
                this.f4039a.p().a((Runnable) new es(this, arVar));
            } catch (DeadObjectException | IllegalStateException e) {
                this.c = null;
                this.f4040b = false;
            }
        }
    }

    public final void a(com.google.android.gms.common.b bVar) {
        aa.b("MeasurementServiceConnection.onConnectionFailed");
        az d = this.f4039a.q.d();
        if (d != null) {
            d.y().a("Service connection failed", bVar);
        }
        synchronized (this) {
            this.f4040b = false;
            this.c = null;
        }
        this.f4039a.p().a((Runnable) new eu(this));
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x003d  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x008e A[SYNTHETIC, Splitter:B:39:0x008e] */
    public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        ar arVar;
        aa.b("MeasurementServiceConnection.onServiceConnected");
        synchronized (this) {
            if (iBinder == null) {
                this.f4040b = false;
                this.f4039a.q().v().a("Service connected with null binder");
                return;
            }
            try {
                String interfaceDescriptor = iBinder.getInterfaceDescriptor();
                if ("com.google.android.gms.measurement.internal.IMeasurementService".equals(interfaceDescriptor)) {
                    if (iBinder == null) {
                        arVar = null;
                    } else {
                        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.measurement.internal.IMeasurementService");
                        arVar = queryLocalInterface instanceof ar ? (ar) queryLocalInterface : new at(iBinder);
                    }
                    try {
                        this.f4039a.q().C().a("Bound to IMeasurementService interface");
                    } catch (RemoteException e) {
                        this.f4039a.q().v().a("Service connect failed to get IMeasurementService");
                        if (arVar == null) {
                        }
                    }
                    if (arVar == null) {
                        this.f4040b = false;
                        try {
                            com.google.android.gms.common.a.a.a().a(this.f4039a.k(), this.f4039a.f4013a);
                        } catch (IllegalArgumentException e2) {
                        }
                    } else {
                        this.f4039a.p().a((Runnable) new eq(this, arVar));
                    }
                }
                this.f4039a.q().v().a("Got binder with a wrong descriptor", interfaceDescriptor);
                arVar = null;
                if (arVar == null) {
                }
            } catch (RemoteException e3) {
                arVar = null;
            }
        }
    }

    public final void onServiceDisconnected(ComponentName componentName) {
        aa.b("MeasurementServiceConnection.onServiceDisconnected");
        this.f4039a.q().B().a("Service disconnected");
        this.f4039a.p().a((Runnable) new er(this, componentName));
    }
}
