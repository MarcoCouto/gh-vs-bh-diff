package com.google.android.gms.internal.c;

final class cy implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ String f3960a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ String f3961b;
    private final /* synthetic */ String c;
    private final /* synthetic */ long d;
    private final /* synthetic */ cg e;

    cy(cg cgVar, String str, String str2, String str3, long j) {
        this.e = cgVar;
        this.f3960a = str;
        this.f3961b = str2;
        this.c = str3;
        this.d = j;
    }

    public final void run() {
        if (this.f3960a == null) {
            this.e.f3924a.J().r().a(this.f3961b, (dx) null);
            return;
        }
        this.e.f3924a.J().r().a(this.f3961b, new dx(this.c, this.f3960a, this.d));
    }
}
