package com.google.android.gms.internal.c;

import android.content.SharedPreferences.Editor;
import com.google.android.gms.common.internal.aa;

public final class bm {

    /* renamed from: a reason: collision with root package name */
    private final String f3896a;

    /* renamed from: b reason: collision with root package name */
    private final boolean f3897b = true;
    private boolean c;
    private boolean d;
    private final /* synthetic */ bk e;

    public bm(bk bkVar, String str, boolean z) {
        this.e = bkVar;
        aa.a(str);
        this.f3896a = str;
    }

    public final void a(boolean z) {
        Editor edit = this.e.C().edit();
        edit.putBoolean(this.f3896a, z);
        edit.apply();
        this.d = z;
    }

    public final boolean a() {
        if (!this.c) {
            this.c = true;
            this.d = this.e.C().getBoolean(this.f3896a, this.f3897b);
        }
        return this.d;
    }
}
