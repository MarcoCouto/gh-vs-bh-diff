package com.google.android.gms.internal.c;

import com.google.android.gms.measurement.AppMeasurement.ConditionalUserProperty;

final class dk implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ ConditionalUserProperty f3978a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ dd f3979b;

    dk(dd ddVar, ConditionalUserProperty conditionalUserProperty) {
        this.f3979b = ddVar;
        this.f3978a = conditionalUserProperty;
    }

    public final void run() {
        this.f3979b.d(this.f3978a);
    }
}
