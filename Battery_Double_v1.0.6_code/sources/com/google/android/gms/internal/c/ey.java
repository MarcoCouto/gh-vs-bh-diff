package com.google.android.gms.internal.c;

final class ey implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ fk f4054a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ Runnable f4055b;

    ey(ev evVar, fk fkVar, Runnable runnable) {
        this.f4054a = fkVar;
        this.f4055b = runnable;
    }

    public final void run() {
        this.f4054a.H();
        this.f4054a.a(this.f4055b);
        this.f4054a.G();
    }
}
