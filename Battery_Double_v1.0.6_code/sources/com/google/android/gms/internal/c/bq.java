package com.google.android.gms.internal.c;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import com.google.android.gms.common.a.a;
import com.google.android.gms.common.b.b;
import com.google.android.gms.common.b.c;
import java.util.List;

public final class bq {

    /* renamed from: a reason: collision with root package name */
    volatile gq f3904a;
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public final ce f3905b;
    private ServiceConnection c;

    bq(ce ceVar) {
        this.f3905b = ceVar;
    }

    private final boolean c() {
        try {
            b b2 = c.b(this.f3905b.k());
            if (b2 != null) {
                return b2.b("com.android.vending", 128).versionCode >= 80837300;
            }
            this.f3905b.q().A().a("Failed to retrieve Package Manager to check Play Store compatibility");
            return false;
        } catch (Exception e) {
            this.f3905b.q().A().a("Failed to retrieve Play Store version", e);
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.f3905b.w();
        if (!c()) {
            this.f3905b.q().A().a("Install Referrer Reporter is not available");
            this.c = null;
            return;
        }
        this.c = new bs(this);
        this.f3905b.q().A().a("Install Referrer Reporter is initializing");
        this.f3905b.w();
        Intent intent = new Intent("com.google.android.finsky.BIND_GET_INSTALL_REFERRER_SERVICE");
        intent.setComponent(new ComponentName("com.android.vending", "com.google.android.finsky.externalreferrer.GetInstallReferrerService"));
        PackageManager packageManager = this.f3905b.k().getPackageManager();
        if (packageManager == null) {
            this.f3905b.q().y().a("Failed to obtain Package Manager to verify binding conditions");
            return;
        }
        List queryIntentServices = packageManager.queryIntentServices(intent, 0);
        if (queryIntentServices == null || queryIntentServices.isEmpty()) {
            this.f3905b.q().A().a("Play Service for fetching Install Referrer is unavailable on device");
            return;
        }
        ResolveInfo resolveInfo = (ResolveInfo) queryIntentServices.get(0);
        if (resolveInfo.serviceInfo != null) {
            String str = resolveInfo.serviceInfo.packageName;
            if (resolveInfo.serviceInfo.name == null || this.c == null || !"com.android.vending".equals(str) || !c()) {
                this.f3905b.q().A().a("Play Store missing or incompatible. Version 8.3.73 or later required");
                return;
            }
            try {
                this.f3905b.q().A().a("Install Referrer Service is", a.a().a(this.f3905b.k(), new Intent(intent), this.c, 1) ? "available" : "not available");
            } catch (Exception e) {
                this.f3905b.q().v().a("Exception occurred while binding to Install Referrer Service", e.getMessage());
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public final void a(Bundle bundle) {
        this.f3905b.w();
        if (bundle != null) {
            long j = bundle.getLong("install_begin_timestamp_seconds", 0) * 1000;
            if (j == 0) {
                this.f3905b.q().v().a("Service response is missing Install Referrer install timestamp");
                return;
            }
            String string = bundle.getString("install_referrer");
            if (string == null || string.isEmpty()) {
                this.f3905b.q().v().a("No referrer defined in install referrer response");
                return;
            }
            this.f3905b.q().C().a("InstallReferrer API result", string);
            ft m = this.f3905b.m();
            String str = "?";
            String valueOf = String.valueOf(string);
            Bundle a2 = m.a(Uri.parse(valueOf.length() != 0 ? str.concat(valueOf) : new String(str)));
            if (a2 == null) {
                this.f3905b.q().v().a("No campaign params defined in install referrer result");
                return;
            }
            String string2 = a2.getString("medium");
            if (string2 != null && !"(not set)".equalsIgnoreCase(string2) && !"organic".equalsIgnoreCase(string2)) {
                long j2 = bundle.getLong("referrer_click_timestamp_seconds", 0) * 1000;
                if (j2 == 0) {
                    this.f3905b.q().v().a("Install Referrer is missing click timestamp for ad campaign");
                    return;
                }
                a2.putLong("click_timestamp", j2);
            }
            if (j == this.f3905b.c().i.a()) {
                this.f3905b.q().C().a("Campaign has already been logged");
                return;
            }
            a2.putString("_cis", "referrer API");
            this.f3905b.c().i.a(j);
            this.f3905b.h().a("auto", "_cmp", a2);
            if (this.c != null) {
                a.a().a(this.f3905b.k(), this.c);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public final Bundle b() {
        this.f3905b.w();
        if (this.f3904a == null) {
            this.f3905b.q().y().a("Attempting to use Install Referrer Service while it is not initialized");
            return null;
        }
        Bundle bundle = new Bundle();
        bundle.putString("package_name", this.f3905b.k().getPackageName());
        try {
            Bundle a2 = this.f3904a.a(bundle);
            if (a2 != null) {
                return a2;
            }
            this.f3905b.q().v().a("Install Referrer Service returned a null response");
            return null;
        } catch (Exception e) {
            this.f3905b.q().v().a("Exception occurred while retrieving the Install Referrer", e.getMessage());
            return null;
        }
    }
}
