package com.google.android.gms.internal.c;

import android.os.RemoteException;

final class ed implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ s f4016a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ eb f4017b;

    ed(eb ebVar, s sVar) {
        this.f4017b = ebVar;
        this.f4016a = sVar;
    }

    public final void run() {
        ar d = this.f4017b.f4014b;
        if (d == null) {
            this.f4017b.q().v().a("Failed to reset data on the service; null service");
            return;
        }
        try {
            d.d(this.f4016a);
        } catch (RemoteException e) {
            this.f4017b.q().v().a("Failed to reset data on the service", e);
        }
        this.f4017b.C();
    }
}
