package com.google.android.gms.internal.c;

import java.io.IOException;

public final class gh extends d<gh> {
    private static volatile gh[] g;
    public String c;
    public String d;
    public Long e;
    public Double f;
    private Float h;

    public gh() {
        this.c = null;
        this.d = null;
        this.e = null;
        this.h = null;
        this.f = null;
        this.f3962a = null;
        this.f4112b = -1;
    }

    public static gh[] e() {
        if (g == null) {
            synchronized (h.f4106b) {
                if (g == null) {
                    g = new gh[0];
                }
            }
        }
        return g;
    }

    /* access modifiers changed from: protected */
    public final int a() {
        int a2 = super.a();
        if (this.c != null) {
            a2 += b.b(1, this.c);
        }
        if (this.d != null) {
            a2 += b.b(2, this.d);
        }
        if (this.e != null) {
            a2 += b.c(3, this.e.longValue());
        }
        if (this.h != null) {
            this.h.floatValue();
            a2 += b.b(4) + 4;
        }
        if (this.f == null) {
            return a2;
        }
        this.f.doubleValue();
        return a2 + b.b(5) + 8;
    }

    public final /* synthetic */ j a(a aVar) throws IOException {
        while (true) {
            int a2 = aVar.a();
            switch (a2) {
                case 0:
                    break;
                case 10:
                    this.c = aVar.c();
                    continue;
                case 18:
                    this.d = aVar.c();
                    continue;
                case 24:
                    this.e = Long.valueOf(aVar.e());
                    continue;
                case 37:
                    this.h = Float.valueOf(Float.intBitsToFloat(aVar.f()));
                    continue;
                case 41:
                    this.f = Double.valueOf(Double.longBitsToDouble(aVar.g()));
                    continue;
                default:
                    if (!super.a(aVar, a2)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    public final void a(b bVar) throws IOException {
        if (this.c != null) {
            bVar.a(1, this.c);
        }
        if (this.d != null) {
            bVar.a(2, this.d);
        }
        if (this.e != null) {
            bVar.b(3, this.e.longValue());
        }
        if (this.h != null) {
            bVar.a(4, this.h.floatValue());
        }
        if (this.f != null) {
            bVar.a(5, this.f.doubleValue());
        }
        super.a(bVar);
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof gh)) {
            return false;
        }
        gh ghVar = (gh) obj;
        if (this.c == null) {
            if (ghVar.c != null) {
                return false;
            }
        } else if (!this.c.equals(ghVar.c)) {
            return false;
        }
        if (this.d == null) {
            if (ghVar.d != null) {
                return false;
            }
        } else if (!this.d.equals(ghVar.d)) {
            return false;
        }
        if (this.e == null) {
            if (ghVar.e != null) {
                return false;
            }
        } else if (!this.e.equals(ghVar.e)) {
            return false;
        }
        if (this.h == null) {
            if (ghVar.h != null) {
                return false;
            }
        } else if (!this.h.equals(ghVar.h)) {
            return false;
        }
        if (this.f == null) {
            if (ghVar.f != null) {
                return false;
            }
        } else if (!this.f.equals(ghVar.f)) {
            return false;
        }
        return (this.f3962a == null || this.f3962a.b()) ? ghVar.f3962a == null || ghVar.f3962a.b() : this.f3962a.equals(ghVar.f3962a);
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = ((this.f == null ? 0 : this.f.hashCode()) + (((this.h == null ? 0 : this.h.hashCode()) + (((this.e == null ? 0 : this.e.hashCode()) + (((this.d == null ? 0 : this.d.hashCode()) + (((this.c == null ? 0 : this.c.hashCode()) + ((getClass().getName().hashCode() + 527) * 31)) * 31)) * 31)) * 31)) * 31)) * 31;
        if (this.f3962a != null && !this.f3962a.b()) {
            i = this.f3962a.hashCode();
        }
        return hashCode + i;
    }
}
