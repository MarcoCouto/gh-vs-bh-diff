package com.google.android.gms.internal.c;

final class ag implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ db f3853a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ af f3854b;

    ag(af afVar, db dbVar) {
        this.f3854b = afVar;
        this.f3853a = dbVar;
    }

    public final void run() {
        this.f3853a.p();
        if (bz.v()) {
            this.f3853a.p().a((Runnable) this);
            return;
        }
        boolean b2 = this.f3854b.b();
        this.f3854b.d = 0;
        if (b2) {
            this.f3854b.a();
        }
    }
}
