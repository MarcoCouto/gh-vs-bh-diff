package com.google.android.gms.internal.c;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.net.Uri.Builder;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.support.v4.h.a;
import android.text.TextUtils;
import android.util.Pair;
import com.google.android.gms.common.b.c;
import com.google.android.gms.common.internal.aa;
import com.google.android.gms.common.util.b;
import com.google.android.gms.common.util.e;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class fk implements v {

    /* renamed from: a reason: collision with root package name */
    by f4073a;

    /* renamed from: b reason: collision with root package name */
    bd f4074b;
    long c;
    private ab d;
    private bi e;
    private fg f;
    private u g;
    private ce h;
    private boolean i = false;
    private boolean j;
    private long k;
    private List<Runnable> l;
    private int m;
    private int n;
    private boolean o;
    private boolean p;
    private boolean q;
    private FileLock r;
    private FileChannel s;
    private List<Long> t;
    private List<Long> u;

    private final int a(FileChannel fileChannel) {
        int i2 = 0;
        w();
        if (fileChannel == null || !fileChannel.isOpen()) {
            q().v().a("Bad channel to read from");
            return i2;
        }
        ByteBuffer allocate = ByteBuffer.allocate(4);
        try {
            fileChannel.position(0);
            int read = fileChannel.read(allocate);
            if (read == 4) {
                allocate.flip();
                return allocate.getInt();
            } else if (read == -1) {
                return i2;
            } else {
                q().y().a("Unexpected data length. Bytes read", Integer.valueOf(read));
                return i2;
            }
        } catch (IOException e2) {
            q().v().a("Failed to read from channel", e2);
            return i2;
        }
    }

    private final s a(Context context, String str, String str2, boolean z, boolean z2, boolean z3, long j2) {
        String str3;
        String str4 = "Unknown";
        String str5 = "Unknown";
        int i2 = Integer.MIN_VALUE;
        String str6 = "Unknown";
        PackageManager packageManager = context.getPackageManager();
        if (packageManager == null) {
            q().v().a("PackageManager is null, can not log app install information");
            return null;
        }
        try {
            str4 = packageManager.getInstallerPackageName(str);
        } catch (IllegalArgumentException e2) {
            q().v().a("Error retrieving installer package name. appId", az.a(str));
        }
        if (str4 == null) {
            str4 = "manual_install";
        } else if ("com.android.vending".equals(str4)) {
            str4 = "";
        }
        try {
            PackageInfo b2 = c.b(context).b(str, 0);
            if (b2 != null) {
                CharSequence b3 = c.b(context).b(str);
                str3 = !TextUtils.isEmpty(b3) ? b3.toString() : str6;
                try {
                    str5 = b2.versionName;
                    i2 = b2.versionCode;
                } catch (NameNotFoundException e3) {
                    q().v().a("Error retrieving newly installed package info. appId, appName", az.a(str), str3);
                    return null;
                }
            }
            long j3 = 0;
            if (b().j(str)) {
                j3 = j2;
            }
            return new s(str, str2, str5, (long) i2, str4, 12451, m().b(context, str), (String) null, z, false, "", 0, j3, 0, z2, z3, false);
        } catch (NameNotFoundException e4) {
            str3 = str6;
            q().v().a("Error retrieving newly installed package info. appId, appName", az.a(str), str3);
            return null;
        }
    }

    private final void a(r rVar) {
        Map map;
        w();
        if (TextUtils.isEmpty(rVar.d())) {
            a(rVar.b(), 204, null, null, null);
            return;
        }
        String d2 = rVar.d();
        String c2 = rVar.c();
        Builder builder = new Builder();
        Builder encodedAuthority = builder.scheme((String) ap.f.b()).encodedAuthority((String) ap.g.b());
        String str = "config/app/";
        String valueOf = String.valueOf(d2);
        encodedAuthority.path(valueOf.length() != 0 ? str.concat(valueOf) : new String(str)).appendQueryParameter("app_instance_id", c2).appendQueryParameter("platform", "android").appendQueryParameter("gmp_version", "12451");
        String uri = builder.build().toString();
        try {
            URL url = new URL(uri);
            q().C().a("Fetching remote configuration", rVar.b());
            gd a2 = d().a(rVar.b());
            String b2 = d().b(rVar.b());
            if (a2 == null || TextUtils.isEmpty(b2)) {
                map = null;
            } else {
                a aVar = new a();
                aVar.put("If-Modified-Since", b2);
                map = aVar;
            }
            this.o = true;
            bd C = C();
            String b3 = rVar.b();
            fm fmVar = new fm(this);
            C.c();
            C.N();
            aa.a(url);
            aa.a(fmVar);
            C.p().b((Runnable) new bh(C, b3, url, null, map, fmVar));
        } catch (MalformedURLException e2) {
            q().v().a("Failed to parse config URL. Not fetching. appId", az.a(rVar.b()), uri);
        }
    }

    private final boolean a(int i2, FileChannel fileChannel) {
        w();
        if (fileChannel == null || !fileChannel.isOpen()) {
            q().v().a("Bad channel to read from");
            return false;
        }
        ByteBuffer allocate = ByteBuffer.allocate(4);
        allocate.putInt(i2);
        allocate.flip();
        try {
            fileChannel.truncate(0);
            fileChannel.write(allocate);
            fileChannel.force(true);
            if (fileChannel.size() == 4) {
                return true;
            }
            q().v().a("Error writing to channel. Bytes written", Long.valueOf(fileChannel.size()));
            return true;
        } catch (IOException e2) {
            q().v().a("Failed to write to channel", e2);
            return false;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:141:0x0375, code lost:
        if (com.google.android.gms.internal.c.ft.k(r12.d) != false) goto L_0x0377;
     */
    /* JADX WARNING: Removed duplicated region for block: B:134:0x0350 A[Catch:{ IOException -> 0x02b5, all -> 0x01b8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:192:0x0533 A[Catch:{ IOException -> 0x02b5, all -> 0x01b8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0089 A[Catch:{ IOException -> 0x02b5, all -> 0x01b8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0096 A[Catch:{ IOException -> 0x02b5, all -> 0x01b8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:380:0x0bb2 A[Catch:{ IOException -> 0x02b5, all -> 0x01b8 }] */
    private final boolean a(String str, long j2) {
        int i2;
        boolean z;
        aj ajVar;
        boolean z2;
        long j3;
        int i3;
        boolean z3;
        boolean z4;
        boolean z5;
        String str2;
        Cursor cursor;
        String str3;
        String str4;
        String[] strArr;
        D().u();
        fo foVar = new fo(this, null);
        ab D = D();
        String str5 = null;
        long j4 = this.c;
        aa.a(foVar);
        D.c();
        D.N();
        Cursor cursor2 = null;
        try {
            SQLiteDatabase x = D.x();
            if (TextUtils.isEmpty(null)) {
                String[] strArr2 = j4 != -1 ? new String[]{String.valueOf(j4), String.valueOf(j2)} : new String[]{String.valueOf(j2)};
                String str6 = j4 != -1 ? "rowid <= ? and " : "";
                Cursor rawQuery = x.rawQuery(new StringBuilder(String.valueOf(str6).length() + 148).append("select app_id, metadata_fingerprint from raw_events where ").append(str6).append("app_id in (select app_id from apps where config_fetched_time >= ?) order by rowid limit 1;").toString(), strArr2);
                if (!rawQuery.moveToFirst()) {
                    if (rawQuery != null) {
                        rawQuery.close();
                    }
                    if (!(foVar.c != null || foVar.c.isEmpty())) {
                        boolean z6 = false;
                        gj gjVar = foVar.f4080a;
                        gjVar.d = new gg[foVar.c.size()];
                        int i4 = 0;
                        long j5 = 0;
                        boolean e2 = b().e(gjVar.q);
                        int i5 = 0;
                        while (i5 < foVar.c.size()) {
                            gg ggVar = (gg) foVar.c.get(i5);
                            if (d().b(foVar.f4080a.q, ggVar.d)) {
                                q().y().a("Dropping blacklisted raw event. appId", az.a(foVar.f4080a.q), n().a(ggVar.d));
                                if ((d().e(foVar.f4080a.q) || d().f(foVar.f4080a.q)) || "_err".equals(ggVar.d)) {
                                    j3 = j5;
                                    i3 = i4;
                                    z2 = z6;
                                } else {
                                    m().a(foVar.f4080a.q, 11, "_ev", ggVar.d, 0);
                                    j3 = j5;
                                    i3 = i4;
                                    z2 = z6;
                                }
                            } else {
                                boolean c2 = d().c(foVar.f4080a.q, ggVar.d);
                                if (!c2) {
                                    m();
                                }
                                boolean z7 = false;
                                boolean z8 = false;
                                if (ggVar.c == null) {
                                    ggVar.c = new gh[0];
                                }
                                gh[] ghVarArr = ggVar.c;
                                int length = ghVarArr.length;
                                int i6 = 0;
                                while (i6 < length) {
                                    gh ghVar = ghVarArr[i6];
                                    if ("_c".equals(ghVar.c)) {
                                        ghVar.e = Long.valueOf(1);
                                        boolean z9 = z8;
                                        z5 = true;
                                        z4 = z9;
                                    } else if ("_r".equals(ghVar.c)) {
                                        ghVar.e = Long.valueOf(1);
                                        z4 = true;
                                        z5 = z7;
                                    } else {
                                        z4 = z8;
                                        z5 = z7;
                                    }
                                    i6++;
                                    z7 = z5;
                                    z8 = z4;
                                }
                                if (!z7 && c2) {
                                    q().C().a("Marking event as conversion", n().a(ggVar.d));
                                    gh[] ghVarArr2 = (gh[]) Arrays.copyOf(ggVar.c, ggVar.c.length + 1);
                                    gh ghVar2 = new gh();
                                    ghVar2.c = "_c";
                                    ghVar2.e = Long.valueOf(1);
                                    ghVarArr2[ghVarArr2.length - 1] = ghVar2;
                                    ggVar.c = ghVarArr2;
                                }
                                if (!z8) {
                                    q().C().a("Marking event as real-time", n().a(ggVar.d));
                                    gh[] ghVarArr3 = (gh[]) Arrays.copyOf(ggVar.c, ggVar.c.length + 1);
                                    gh ghVar3 = new gh();
                                    ghVar3.c = "_r";
                                    ghVar3.e = Long.valueOf(1);
                                    ghVarArr3[ghVarArr3.length - 1] = ghVar3;
                                    ggVar.c = ghVarArr3;
                                }
                                if (D().a(g(), foVar.f4080a.q, false, false, false, false, true).e > ((long) b().a(foVar.f4080a.q))) {
                                    int i7 = 0;
                                    while (true) {
                                        if (i7 >= ggVar.c.length) {
                                            break;
                                        } else if ("_r".equals(ggVar.c[i7].c)) {
                                            gh[] ghVarArr4 = new gh[(ggVar.c.length - 1)];
                                            if (i7 > 0) {
                                                System.arraycopy(ggVar.c, 0, ghVarArr4, 0, i7);
                                            }
                                            if (i7 < ghVarArr4.length) {
                                                System.arraycopy(ggVar.c, i7 + 1, ghVarArr4, i7, ghVarArr4.length - i7);
                                            }
                                            ggVar.c = ghVarArr4;
                                        } else {
                                            i7++;
                                        }
                                    }
                                } else {
                                    z6 = true;
                                }
                                if (ft.a(ggVar.d) && c2 && D().a(g(), foVar.f4080a.q, false, false, true, false, false).c > ((long) b().b(foVar.f4080a.q, ap.o))) {
                                    q().y().a("Too many conversions. Not logging as conversion. appId", az.a(foVar.f4080a.q));
                                    boolean z10 = false;
                                    gh ghVar4 = null;
                                    gh[] ghVarArr5 = ggVar.c;
                                    int length2 = ghVarArr5.length;
                                    int i8 = 0;
                                    while (i8 < length2) {
                                        gh ghVar5 = ghVarArr5[i8];
                                        if ("_c".equals(ghVar5.c)) {
                                            z3 = z10;
                                        } else if ("_err".equals(ghVar5.c)) {
                                            gh ghVar6 = ghVar4;
                                            z3 = true;
                                            ghVar5 = ghVar6;
                                        } else {
                                            ghVar5 = ghVar4;
                                            z3 = z10;
                                        }
                                        i8++;
                                        z10 = z3;
                                        ghVar4 = ghVar5;
                                    }
                                    if (z10 && ghVar4 != null) {
                                        ggVar.c = (gh[]) b.a((T[]) ggVar.c, (T[]) new gh[]{ghVar4});
                                        z2 = z6;
                                        if (e2) {
                                        }
                                        j3 = j5;
                                        i3 = i4 + 1;
                                        gjVar.d[i4] = ggVar;
                                    } else if (ghVar4 != null) {
                                        ghVar4.c = "_err";
                                        ghVar4.e = Long.valueOf(10);
                                        z2 = z6;
                                        if (e2 || !"_e".equals(ggVar.d)) {
                                            j3 = j5;
                                        } else if (ggVar.c == null || ggVar.c.length == 0) {
                                            q().y().a("Engagement event does not contain any parameters. appId", az.a(foVar.f4080a.q));
                                            j3 = j5;
                                        } else {
                                            m();
                                            Long l2 = (Long) ft.b(ggVar, "_et");
                                            if (l2 == null) {
                                                q().y().a("Engagement event does not include duration. appId", az.a(foVar.f4080a.q));
                                                j3 = j5;
                                            } else {
                                                j3 = j5 + l2.longValue();
                                            }
                                        }
                                        i3 = i4 + 1;
                                        gjVar.d[i4] = ggVar;
                                    } else {
                                        q().v().a("Did not find conversion parameter. appId", az.a(foVar.f4080a.q));
                                    }
                                }
                                z2 = z6;
                                if (e2) {
                                }
                                j3 = j5;
                                i3 = i4 + 1;
                                gjVar.d[i4] = ggVar;
                            }
                            i5++;
                            j5 = j3;
                            i4 = i3;
                            z6 = z2;
                        }
                        if (i4 < foVar.c.size()) {
                            gjVar.d = (gg[]) Arrays.copyOf(gjVar.d, i4);
                        }
                        if (e2) {
                            fs c3 = D().c(gjVar.q, "_lte");
                            fs fsVar = (c3 == null || c3.e == null) ? new fs(gjVar.q, "auto", "_lte", j().a(), Long.valueOf(j5)) : new fs(gjVar.q, "auto", "_lte", j().a(), Long.valueOf(((Long) c3.e).longValue() + j5));
                            gl glVar = new gl();
                            glVar.d = "_lte";
                            glVar.c = Long.valueOf(j().a());
                            glVar.f = (Long) fsVar.e;
                            boolean z11 = false;
                            int i9 = 0;
                            while (true) {
                                if (i9 >= gjVar.e.length) {
                                    break;
                                } else if ("_lte".equals(gjVar.e[i9].d)) {
                                    gjVar.e[i9] = glVar;
                                    z11 = true;
                                    break;
                                } else {
                                    i9++;
                                }
                            }
                            if (!z11) {
                                gjVar.e = (gl[]) Arrays.copyOf(gjVar.e, gjVar.e.length + 1);
                                gjVar.e[foVar.f4080a.e.length - 1] = glVar;
                            }
                            if (j5 > 0) {
                                D().a(fsVar);
                                q().B().a("Updated lifetime engagement user property with value. Value", fsVar.e);
                            }
                        }
                        gjVar.C = a(gjVar.q, gjVar.e, gjVar.d);
                        if (b().d(foVar.f4080a.q)) {
                            HashMap hashMap = new HashMap();
                            gg[] ggVarArr = new gg[gjVar.d.length];
                            int i10 = 0;
                            SecureRandom w = m().w();
                            gg[] ggVarArr2 = gjVar.d;
                            int length3 = ggVarArr2.length;
                            int i11 = 0;
                            while (i11 < length3) {
                                gg ggVar2 = ggVarArr2[i11];
                                if (ggVar2.d.equals("_ep")) {
                                    m();
                                    String str7 = (String) ft.b(ggVar2, "_en");
                                    aj ajVar2 = (aj) hashMap.get(str7);
                                    if (ajVar2 == null) {
                                        ajVar2 = D().a(foVar.f4080a.q, str7);
                                        hashMap.put(str7, ajVar2);
                                    }
                                    if (ajVar2.g == null) {
                                        if (ajVar2.h.longValue() > 1) {
                                            m();
                                            ggVar2.c = ft.a(ggVar2.c, "_sr", (Object) ajVar2.h);
                                        }
                                        if (ajVar2.i != null && ajVar2.i.booleanValue()) {
                                            m();
                                            ggVar2.c = ft.a(ggVar2.c, "_efs", (Object) Long.valueOf(1));
                                        }
                                        i2 = i10 + 1;
                                        ggVarArr[i10] = ggVar2;
                                    }
                                    i2 = i10;
                                } else {
                                    String str8 = "_dbg";
                                    Long valueOf = Long.valueOf(1);
                                    if (!TextUtils.isEmpty(str8) && valueOf != null) {
                                        gh[] ghVarArr6 = ggVar2.c;
                                        int length4 = ghVarArr6.length;
                                        int i12 = 0;
                                        while (true) {
                                            if (i12 >= length4) {
                                                z = false;
                                                break;
                                            }
                                            gh ghVar7 = ghVarArr6[i12];
                                            if (str8.equals(ghVar7.c)) {
                                                z = ((valueOf instanceof Long) && valueOf.equals(ghVar7.e)) || ((valueOf instanceof String) && valueOf.equals(ghVar7.d)) || ((valueOf instanceof Double) && valueOf.equals(ghVar7.f));
                                            } else {
                                                i12++;
                                            }
                                        }
                                    } else {
                                        z = false;
                                    }
                                    int i13 = !z ? d().d(foVar.f4080a.q, ggVar2.d) : 1;
                                    if (i13 <= 0) {
                                        q().y().a("Sample rate must be positive. event, rate", ggVar2.d, Integer.valueOf(i13));
                                        i2 = i10 + 1;
                                        ggVarArr[i10] = ggVar2;
                                    } else {
                                        aj ajVar3 = (aj) hashMap.get(ggVar2.d);
                                        if (ajVar3 == null) {
                                            ajVar = D().a(foVar.f4080a.q, ggVar2.d);
                                            if (ajVar == null) {
                                                q().y().a("Event being bundled has no eventAggregate. appId, eventName", foVar.f4080a.q, ggVar2.d);
                                                ajVar = new aj(foVar.f4080a.q, ggVar2.d, 1, 1, ggVar2.e.longValue(), 0, null, null, null);
                                            }
                                        } else {
                                            ajVar = ajVar3;
                                        }
                                        m();
                                        Long l3 = (Long) ft.b(ggVar2, "_eid");
                                        Boolean valueOf2 = Boolean.valueOf(l3 != null);
                                        if (i13 == 1) {
                                            i2 = i10 + 1;
                                            ggVarArr[i10] = ggVar2;
                                            if (valueOf2.booleanValue() && !(ajVar.g == null && ajVar.h == null && ajVar.i == null)) {
                                                hashMap.put(ggVar2.d, ajVar.a(null, null, null));
                                            }
                                        } else if (w.nextInt(i13) == 0) {
                                            m();
                                            ggVar2.c = ft.a(ggVar2.c, "_sr", (Object) Long.valueOf((long) i13));
                                            i2 = i10 + 1;
                                            ggVarArr[i10] = ggVar2;
                                            if (valueOf2.booleanValue()) {
                                                ajVar = ajVar.a(null, Long.valueOf((long) i13), null);
                                            }
                                            hashMap.put(ggVar2.d, ajVar.b(ggVar2.e.longValue()));
                                        } else {
                                            if (Math.abs(ggVar2.e.longValue() - ajVar.f) >= 86400000) {
                                                m();
                                                ggVar2.c = ft.a(ggVar2.c, "_efs", (Object) Long.valueOf(1));
                                                m();
                                                ggVar2.c = ft.a(ggVar2.c, "_sr", (Object) Long.valueOf((long) i13));
                                                i2 = i10 + 1;
                                                ggVarArr[i10] = ggVar2;
                                                if (valueOf2.booleanValue()) {
                                                    ajVar = ajVar.a(null, Long.valueOf((long) i13), Boolean.valueOf(true));
                                                }
                                                hashMap.put(ggVar2.d, ajVar.b(ggVar2.e.longValue()));
                                            } else {
                                                if (valueOf2.booleanValue()) {
                                                    hashMap.put(ggVar2.d, ajVar.a(l3, null, null));
                                                }
                                                i2 = i10;
                                            }
                                        }
                                    }
                                }
                                i11++;
                                i10 = i2;
                            }
                            if (i10 < gjVar.d.length) {
                                gjVar.d = (gg[]) Arrays.copyOf(ggVarArr, i10);
                            }
                            for (Entry value : hashMap.entrySet()) {
                                D().a((aj) value.getValue());
                            }
                        }
                        gjVar.g = Long.valueOf(Long.MAX_VALUE);
                        gjVar.h = Long.valueOf(Long.MIN_VALUE);
                        for (gg ggVar3 : gjVar.d) {
                            if (ggVar3.e.longValue() < gjVar.g.longValue()) {
                                gjVar.g = ggVar3.e;
                            }
                            if (ggVar3.e.longValue() > gjVar.h.longValue()) {
                                gjVar.h = ggVar3.e;
                            }
                        }
                        String str9 = foVar.f4080a.q;
                        r b2 = D().b(str9);
                        if (b2 == null) {
                            q().v().a("Bundling raw events w/o app info. appId", az.a(foVar.f4080a.q));
                        } else if (gjVar.d.length > 0) {
                            long h2 = b2.h();
                            gjVar.j = h2 != 0 ? Long.valueOf(h2) : null;
                            long g2 = b2.g();
                            if (g2 != 0) {
                                h2 = g2;
                            }
                            gjVar.i = h2 != 0 ? Long.valueOf(h2) : null;
                            b2.r();
                            gjVar.y = Integer.valueOf((int) b2.o());
                            b2.a(gjVar.g.longValue());
                            b2.b(gjVar.h.longValue());
                            gjVar.z = b2.z();
                            D().a(b2);
                        }
                        if (gjVar.d.length > 0) {
                            gd a2 = d().a(foVar.f4080a.q);
                            if (a2 != null && a2.c != null) {
                                gjVar.G = a2.c;
                            } else if (TextUtils.isEmpty(foVar.f4080a.A)) {
                                gjVar.G = Long.valueOf(-1);
                            } else {
                                q().y().a("Did not find measurement config or missing version info. appId", az.a(foVar.f4080a.q));
                            }
                            D().a(gjVar, z6);
                        }
                        ab D2 = D();
                        List<Long> list = foVar.f4081b;
                        aa.a(list);
                        D2.c();
                        D2.N();
                        StringBuilder sb = new StringBuilder("rowid in (");
                        for (int i14 = 0; i14 < list.size(); i14++) {
                            if (i14 != 0) {
                                sb.append(",");
                            }
                            sb.append(((Long) list.get(i14)).longValue());
                        }
                        sb.append(")");
                        int delete = D2.x().delete("raw_events", sb.toString(), null);
                        if (delete != list.size()) {
                            D2.q().v().a("Deleted fewer rows from raw events table than expected", Integer.valueOf(delete), Integer.valueOf(list.size()));
                        }
                        ab D3 = D();
                        try {
                            D3.x().execSQL("delete from raw_events_metadata where app_id=? and metadata_fingerprint not in (select distinct metadata_fingerprint from raw_events where app_id=?)", new String[]{str9, str9});
                        } catch (SQLiteException e3) {
                            D3.q().v().a("Failed to remove unused event metadata. appId", az.a(str9), e3);
                        }
                        D().v();
                        D().w();
                        return true;
                    }
                    D().v();
                    D().w();
                    return false;
                }
                String string = rawQuery.getString(0);
                String string2 = rawQuery.getString(1);
                rawQuery.close();
                str2 = string2;
                cursor = rawQuery;
                str3 = string;
            } else {
                String[] strArr3 = j4 != -1 ? new String[]{null, String.valueOf(j4)} : new String[]{null};
                String str10 = j4 != -1 ? " and rowid <= ?" : "";
                Cursor rawQuery2 = x.rawQuery(new StringBuilder(String.valueOf(str10).length() + 84).append("select metadata_fingerprint from raw_events where app_id = ?").append(str10).append(" order by rowid limit 1;").toString(), strArr3);
                if (!rawQuery2.moveToFirst()) {
                    if (rawQuery2 != null) {
                        rawQuery2.close();
                    }
                    if (!(foVar.c != null || foVar.c.isEmpty())) {
                    }
                } else {
                    String string3 = rawQuery2.getString(0);
                    rawQuery2.close();
                    str2 = string3;
                    cursor = rawQuery2;
                    str3 = null;
                }
            }
            try {
                cursor = x.query("raw_events_metadata", new String[]{"metadata"}, "app_id = ? and metadata_fingerprint = ?", new String[]{str3, str2}, null, null, "rowid", "2");
                if (!cursor.moveToFirst()) {
                    D.q().v().a("Raw event metadata record is missing. appId", az.a(str3));
                    if (cursor != null) {
                        cursor.close();
                    }
                    if (!(foVar.c != null || foVar.c.isEmpty())) {
                    }
                } else {
                    byte[] blob = cursor.getBlob(0);
                    a a3 = a.a(blob, 0, blob.length);
                    gj gjVar2 = new gj();
                    try {
                        gjVar2.a(a3);
                        if (cursor.moveToNext()) {
                            D.q().y().a("Get multiple raw event metadata records, expected one. appId", az.a(str3));
                        }
                        cursor.close();
                        foVar.a(gjVar2);
                        if (j4 != -1) {
                            str4 = "app_id = ? and metadata_fingerprint = ? and rowid <= ?";
                            strArr = new String[]{str3, str2, String.valueOf(j4)};
                        } else {
                            str4 = "app_id = ? and metadata_fingerprint = ?";
                            strArr = new String[]{str3, str2};
                        }
                        cursor2 = x.query("raw_events", new String[]{"rowid", "name", "timestamp", "data"}, str4, strArr, null, null, "rowid", null);
                        try {
                            if (!cursor2.moveToFirst()) {
                                D.q().y().a("Raw event data disappeared while in transaction. appId", az.a(str3));
                                if (cursor2 != null) {
                                    cursor2.close();
                                }
                                if (!(foVar.c != null || foVar.c.isEmpty())) {
                                }
                            } else {
                                do {
                                    long j6 = cursor2.getLong(0);
                                    byte[] blob2 = cursor2.getBlob(3);
                                    a a4 = a.a(blob2, 0, blob2.length);
                                    gg ggVar4 = new gg();
                                    try {
                                        ggVar4.a(a4);
                                        ggVar4.d = cursor2.getString(1);
                                        ggVar4.e = Long.valueOf(cursor2.getLong(2));
                                        if (!foVar.a(j6, ggVar4)) {
                                            if (cursor2 != null) {
                                                cursor2.close();
                                            }
                                            if (!(foVar.c != null || foVar.c.isEmpty())) {
                                            }
                                        }
                                    } catch (IOException e4) {
                                        D.q().v().a("Data loss. Failed to merge raw event. appId", az.a(str3), e4);
                                    }
                                } while (cursor2.moveToNext());
                                if (cursor2 != null) {
                                    cursor2.close();
                                }
                                if (!(foVar.c != null || foVar.c.isEmpty())) {
                                }
                            }
                        } catch (SQLiteException e5) {
                            e = e5;
                            str5 = str3;
                        }
                    } catch (IOException e6) {
                        D.q().v().a("Data loss. Failed to merge raw event metadata. appId", az.a(str3), e6);
                        if (cursor != null) {
                            cursor.close();
                        }
                    } catch (Throwable th) {
                        D().w();
                        throw th;
                    }
                }
            } catch (SQLiteException e7) {
                e = e7;
                cursor2 = cursor;
                str5 = str3;
            } catch (Throwable th2) {
                th = th2;
                cursor2 = cursor;
                if (cursor2 != null) {
                    cursor2.close();
                }
                throw th;
            }
        } catch (SQLiteException e8) {
            e = e8;
            try {
                D.q().v().a("Data loss. Error selecting raw event. appId", az.a(str5), e);
                if (cursor2 != null) {
                    cursor2.close();
                }
                if (!(foVar.c != null || foVar.c.isEmpty())) {
                }
            } catch (Throwable th3) {
                th = th3;
                if (cursor2 != null) {
                }
                throw th;
            }
        }
    }

    private final boolean a(String str, an anVar) {
        long longValue;
        fs fsVar;
        String d2 = anVar.f3865b.d("currency");
        if ("ecommerce_purchase".equals(anVar.f3864a)) {
            double doubleValue = anVar.f3865b.c("value").doubleValue() * 1000000.0d;
            if (doubleValue == 0.0d) {
                doubleValue = ((double) anVar.f3865b.b("value").longValue()) * 1000000.0d;
            }
            if (doubleValue > 9.223372036854776E18d || doubleValue < -9.223372036854776E18d) {
                q().y().a("Data lost. Currency value is too big. appId", az.a(str), Double.valueOf(doubleValue));
                return false;
            }
            longValue = Math.round(doubleValue);
        } else {
            longValue = anVar.f3865b.b("value").longValue();
        }
        if (!TextUtils.isEmpty(d2)) {
            String upperCase = d2.toUpperCase(Locale.US);
            if (upperCase.matches("[A-Z]{3}")) {
                String valueOf = String.valueOf("_ltv_");
                String valueOf2 = String.valueOf(upperCase);
                String str2 = valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf);
                fs c2 = D().c(str, str2);
                if (c2 == null || !(c2.e instanceof Long)) {
                    ab D = D();
                    int b2 = b().b(str, ap.F) - 1;
                    aa.a(str);
                    D.c();
                    D.N();
                    try {
                        D.x().execSQL("delete from user_attributes where app_id=? and name in (select name from user_attributes where app_id=? and name like '_ltv_%' order by set_timestamp desc limit ?,10);", new String[]{str, str, String.valueOf(b2)});
                    } catch (SQLiteException e2) {
                        D.q().v().a("Error pruning currencies. appId", az.a(str), e2);
                    }
                    fsVar = new fs(str, anVar.c, str2, j().a(), Long.valueOf(longValue));
                } else {
                    fsVar = new fs(str, anVar.c, str2, j().a(), Long.valueOf(longValue + ((Long) c2.e).longValue()));
                }
                if (!D().a(fsVar)) {
                    q().v().a("Too many unique user properties are set. Ignoring user property. appId", az.a(str), n().c(fsVar.c), fsVar.e);
                    m().a(str, 9, (String) null, (String) null, 0);
                }
            }
        }
        return true;
    }

    private final gf[] a(String str, gl[] glVarArr, gg[] ggVarArr) {
        aa.a(str);
        return E().a(str, ggVarArr, glVarArr);
    }

    private final Boolean b(r rVar) {
        try {
            if (rVar.j() != -2147483648L) {
                if (rVar.j() == ((long) c.b(k()).b(rVar.b(), 0).versionCode)) {
                    return Boolean.valueOf(true);
                }
            } else {
                String str = c.b(k()).b(rVar.b(), 0).versionName;
                if (rVar.i() != null && rVar.i().equals(str)) {
                    return Boolean.valueOf(true);
                }
            }
            return Boolean.valueOf(false);
        } catch (NameNotFoundException e2) {
            return null;
        }
    }

    private final void b(an anVar, s sVar) {
        aj a2;
        ai aiVar;
        gj gjVar;
        boolean z;
        aa.a(sVar);
        aa.a(sVar.f4127a);
        long nanoTime = System.nanoTime();
        w();
        F();
        String str = sVar.f4127a;
        m();
        if (ft.a(anVar, sVar)) {
            if (!sVar.h) {
                e(sVar);
            } else if (d().b(str, anVar.f3864a)) {
                q().y().a("Dropping blacklisted event. appId", az.a(str), n().a(anVar.f3864a));
                boolean z2 = d().e(str) || d().f(str);
                if (!z2 && !"_err".equals(anVar.f3864a)) {
                    m().a(str, 11, "_ev", anVar.f3864a, 0);
                }
                if (z2) {
                    r b2 = D().b(str);
                    if (b2 != null) {
                        if (Math.abs(j().a() - Math.max(b2.q(), b2.p())) > ((Long) ap.A.b()).longValue()) {
                            q().B().a("Fetching config for blacklisted app");
                            a(b2);
                        }
                    }
                }
            } else {
                if (q().a(2)) {
                    q().C().a("Logging event", n().a(anVar));
                }
                D().u();
                try {
                    e(sVar);
                    if (("_iap".equals(anVar.f3864a) || "ecommerce_purchase".equals(anVar.f3864a)) && !a(str, anVar)) {
                        D().v();
                        D().w();
                        return;
                    }
                    boolean a3 = ft.a(anVar.f3864a);
                    boolean equals = "_err".equals(anVar.f3864a);
                    ac a4 = D().a(g(), str, true, a3, false, equals, false);
                    long intValue = a4.f3849b - ((long) ((Integer) ap.l.b()).intValue());
                    if (intValue > 0) {
                        if (intValue % 1000 == 1) {
                            q().v().a("Data loss. Too many events logged. appId, count", az.a(str), Long.valueOf(a4.f3849b));
                        }
                        D().v();
                        D().w();
                        return;
                    }
                    if (a3) {
                        long intValue2 = a4.f3848a - ((long) ((Integer) ap.n.b()).intValue());
                        if (intValue2 > 0) {
                            if (intValue2 % 1000 == 1) {
                                q().v().a("Data loss. Too many public events logged. appId, count", az.a(str), Long.valueOf(a4.f3848a));
                            }
                            m().a(str, 16, "_ev", anVar.f3864a, 0);
                            D().v();
                            D().w();
                            return;
                        }
                    }
                    if (equals) {
                        long max = a4.d - ((long) Math.max(0, Math.min(1000000, b().b(sVar.f4127a, ap.m))));
                        if (max > 0) {
                            if (max == 1) {
                                q().v().a("Too many error events logged. appId, count", az.a(str), Long.valueOf(a4.d));
                            }
                            D().v();
                            D().w();
                            return;
                        }
                    }
                    Bundle b3 = anVar.f3865b.b();
                    m().a(b3, "_o", (Object) anVar.c);
                    if (m().i(str)) {
                        m().a(b3, "_dbg", (Object) Long.valueOf(1));
                        m().a(b3, "_r", (Object) Long.valueOf(1));
                    }
                    long c2 = D().c(str);
                    if (c2 > 0) {
                        q().y().a("Data lost. Too many events stored on disk, deleted. appId", az.a(str), Long.valueOf(c2));
                    }
                    ai aiVar2 = new ai(this.h, anVar.c, str, anVar.f3864a, anVar.d, 0, b3);
                    aj a5 = D().a(str, aiVar2.f3858b);
                    if (a5 != null) {
                        ai a6 = aiVar2.a(this.h, a5.e);
                        a2 = a5.a(a6.c);
                        aiVar = a6;
                    } else if (D().f(str) < 500 || !a3) {
                        a2 = new aj(str, aiVar2.f3858b, 0, 0, aiVar2.c, 0, null, null, null);
                        aiVar = aiVar2;
                    } else {
                        q().v().a("Too many event names used, ignoring event. appId, name, supported count", az.a(str), n().a(aiVar2.f3858b), Integer.valueOf(500));
                        m().a(str, 8, (String) null, (String) null, 0);
                        D().w();
                        return;
                    }
                    D().a(a2);
                    w();
                    F();
                    aa.a(aiVar);
                    aa.a(sVar);
                    aa.a(aiVar.f3857a);
                    aa.b(aiVar.f3857a.equals(sVar.f4127a));
                    gjVar = new gj();
                    gjVar.c = Integer.valueOf(1);
                    gjVar.k = "android";
                    gjVar.q = sVar.f4127a;
                    gjVar.p = sVar.d;
                    gjVar.r = sVar.c;
                    gjVar.E = sVar.j == -2147483648L ? null : Integer.valueOf((int) sVar.j);
                    gjVar.s = Long.valueOf(sVar.e);
                    gjVar.A = sVar.f4128b;
                    gjVar.x = sVar.f == 0 ? null : Long.valueOf(sVar.f);
                    Pair a7 = c().a(sVar.f4127a);
                    if (a7 == null || TextUtils.isEmpty((CharSequence) a7.first)) {
                        if (!t().a(k()) && sVar.p) {
                            String string = Secure.getString(k().getContentResolver(), "android_id");
                            if (string == null) {
                                q().y().a("null secure ID. appId", az.a(gjVar.q));
                                string = "null";
                            } else if (string.isEmpty()) {
                                q().y().a("empty secure ID. appId", az.a(gjVar.q));
                            }
                            gjVar.F = string;
                        }
                    } else if (sVar.o) {
                        gjVar.u = (String) a7.first;
                        gjVar.v = (Boolean) a7.second;
                    }
                    t().F();
                    gjVar.m = Build.MODEL;
                    t().F();
                    gjVar.l = VERSION.RELEASE;
                    gjVar.o = Integer.valueOf((int) t().u());
                    gjVar.n = t().v();
                    gjVar.t = null;
                    gjVar.f = null;
                    gjVar.g = null;
                    gjVar.h = null;
                    gjVar.H = Long.valueOf(sVar.l);
                    if (this.h.x() && y.y()) {
                        gjVar.I = null;
                    }
                    r b4 = D().b(sVar.f4127a);
                    if (b4 == null) {
                        b4 = new r(this.h, sVar.f4127a);
                        b4.a(this.h.u().v());
                        b4.d(sVar.k);
                        b4.b(sVar.f4128b);
                        b4.c(c().b(sVar.f4127a));
                        b4.f(0);
                        b4.a(0);
                        b4.b(0);
                        b4.e(sVar.c);
                        b4.c(sVar.j);
                        b4.f(sVar.d);
                        b4.d(sVar.e);
                        b4.e(sVar.f);
                        b4.a(sVar.h);
                        b4.o(sVar.l);
                        D().a(b4);
                    }
                    gjVar.w = b4.c();
                    gjVar.D = b4.f();
                    List a8 = D().a(sVar.f4127a);
                    gjVar.e = new gl[a8.size()];
                    for (int i2 = 0; i2 < a8.size(); i2++) {
                        gl glVar = new gl();
                        gjVar.e[i2] = glVar;
                        glVar.d = ((fs) a8.get(i2)).c;
                        glVar.c = Long.valueOf(((fs) a8.get(i2)).d);
                        m().a(glVar, ((fs) a8.get(i2)).e);
                    }
                    long a9 = D().a(gjVar);
                    ab D = D();
                    if (aiVar.e != null) {
                        Iterator it = aiVar.e.iterator();
                        while (true) {
                            if (it.hasNext()) {
                                if ("_r".equals((String) it.next())) {
                                    z = true;
                                    break;
                                }
                            } else {
                                boolean c3 = d().c(aiVar.f3857a, aiVar.f3858b);
                                ac a10 = D().a(g(), aiVar.f3857a, false, false, false, false, false);
                                if (c3 && a10.e < ((long) b().a(aiVar.f3857a))) {
                                    z = true;
                                }
                            }
                        }
                    }
                    z = false;
                    if (D.a(aiVar, a9, z)) {
                        this.k = 0;
                    }
                    D().v();
                    if (q().a(2)) {
                        q().C().a("Event recorded", n().a(aiVar));
                    }
                    D().w();
                    i();
                    q().C().a("Background event processing time, ms", Long.valueOf(((System.nanoTime() - nanoTime) + 500000) / 1000000));
                } catch (IOException e2) {
                    q().v().a("Data loss. Failed to insert raw event metadata. appId", az.a(gjVar.q), e2);
                } catch (Throwable th) {
                    D().w();
                    throw th;
                }
            }
        }
    }

    private static void b(fj fjVar) {
        if (fjVar == null) {
            throw new IllegalStateException("Upload component not created");
        } else if (!fjVar.M()) {
            String valueOf = String.valueOf(fjVar.getClass());
            throw new IllegalStateException(new StringBuilder(String.valueOf(valueOf).length() + 27).append("Component not initialized: ").append(valueOf).toString());
        }
    }

    private final by d() {
        b((fj) this.f4073a);
        return this.f4073a;
    }

    private final bi e() {
        if (this.e != null) {
            return this.e;
        }
        throw new IllegalStateException("Network broadcast receiver not created");
    }

    /* access modifiers changed from: private */
    public final r e(s sVar) {
        boolean z = true;
        w();
        F();
        aa.a(sVar);
        aa.a(sVar.f4127a);
        r b2 = D().b(sVar.f4127a);
        String b3 = c().b(sVar.f4127a);
        boolean z2 = false;
        if (b2 == null) {
            r rVar = new r(this.h, sVar.f4127a);
            rVar.a(this.h.u().v());
            rVar.c(b3);
            b2 = rVar;
            z2 = true;
        } else if (!b3.equals(b2.e())) {
            b2.c(b3);
            b2.a(this.h.u().v());
            z2 = true;
        }
        if (!TextUtils.isEmpty(sVar.f4128b) && !sVar.f4128b.equals(b2.d())) {
            b2.b(sVar.f4128b);
            z2 = true;
        }
        if (!TextUtils.isEmpty(sVar.k) && !sVar.k.equals(b2.f())) {
            b2.d(sVar.k);
            z2 = true;
        }
        if (!(sVar.e == 0 || sVar.e == b2.l())) {
            b2.d(sVar.e);
            z2 = true;
        }
        if (!TextUtils.isEmpty(sVar.c) && !sVar.c.equals(b2.i())) {
            b2.e(sVar.c);
            z2 = true;
        }
        if (sVar.j != b2.j()) {
            b2.c(sVar.j);
            z2 = true;
        }
        if (sVar.d != null && !sVar.d.equals(b2.k())) {
            b2.f(sVar.d);
            z2 = true;
        }
        if (sVar.f != b2.m()) {
            b2.e(sVar.f);
            z2 = true;
        }
        if (sVar.h != b2.n()) {
            b2.a(sVar.h);
            z2 = true;
        }
        if (!TextUtils.isEmpty(sVar.g) && !sVar.g.equals(b2.y())) {
            b2.g(sVar.g);
            z2 = true;
        }
        if (sVar.l != b2.A()) {
            b2.o(sVar.l);
            z2 = true;
        }
        if (sVar.o != b2.B()) {
            b2.b(sVar.o);
            z2 = true;
        }
        if (sVar.p != b2.C()) {
            b2.c(sVar.p);
        } else {
            z = z2;
        }
        if (z) {
            D().a(b2);
        }
        return b2;
    }

    private final fg f() {
        b((fj) this.f);
        return this.f;
    }

    private final long g() {
        long a2 = j().a();
        bk c2 = c();
        c2.F();
        c2.c();
        long a3 = c2.g.a();
        if (a3 == 0) {
            a3 = 1 + ((long) c2.n().w().nextInt(86400000));
            c2.g.a(a3);
        }
        return ((((a3 + a2) / 1000) / 60) / 60) / 24;
    }

    private final boolean h() {
        w();
        F();
        return D().D() || !TextUtils.isEmpty(D().y());
    }

    private final void i() {
        long max;
        long j2;
        w();
        F();
        if (r()) {
            if (this.k > 0) {
                long abs = 3600000 - Math.abs(j().b() - this.k);
                if (abs > 0) {
                    q().C().a("Upload has been suspended. Will update scheduling later in approximately ms", Long.valueOf(abs));
                    e().b();
                    f().u();
                    return;
                }
                this.k = 0;
            }
            if (!this.h.B() || !h()) {
                q().C().a("Nothing to upload or uploading impossible");
                e().b();
                f().u();
                return;
            }
            long a2 = j().a();
            long max2 = Math.max(0, ((Long) ap.B.b()).longValue());
            boolean z = D().E() || D().z();
            if (z) {
                String x = b().x();
                max = (TextUtils.isEmpty(x) || ".none.".equals(x)) ? Math.max(0, ((Long) ap.v.b()).longValue()) : Math.max(0, ((Long) ap.w.b()).longValue());
            } else {
                max = Math.max(0, ((Long) ap.u.b()).longValue());
            }
            long a3 = c().c.a();
            long a4 = c().d.a();
            long max3 = Math.max(D().B(), D().C());
            if (max3 == 0) {
                j2 = 0;
            } else {
                long abs2 = a2 - Math.abs(max3 - a2);
                long abs3 = a2 - Math.abs(a4 - a2);
                long max4 = Math.max(a2 - Math.abs(a3 - a2), abs3);
                j2 = abs2 + max2;
                if (z && max4 > 0) {
                    j2 = Math.min(abs2, max4) + max;
                }
                if (!m().a(max4, max)) {
                    j2 = max4 + max;
                }
                if (abs3 != 0 && abs3 >= abs2) {
                    int i2 = 0;
                    while (true) {
                        int i3 = i2;
                        if (i3 >= Math.min(20, Math.max(0, ((Integer) ap.D.b()).intValue()))) {
                            j2 = 0;
                            break;
                        }
                        j2 += (1 << i3) * Math.max(0, ((Long) ap.C.b()).longValue());
                        if (j2 > abs3) {
                            break;
                        }
                        i2 = i3 + 1;
                    }
                }
            }
            if (j2 == 0) {
                q().C().a("Next upload time is 0");
                e().b();
                f().u();
            } else if (!C().u()) {
                q().C().a("No network");
                e().a();
                f().u();
            } else {
                long a5 = c().e.a();
                long max5 = Math.max(0, ((Long) ap.s.b()).longValue());
                long j3 = !m().a(a5, max5) ? Math.max(j2, max5 + a5) : j2;
                e().b();
                long a6 = j3 - j().a();
                if (a6 <= 0) {
                    a6 = Math.max(0, ((Long) ap.x.b()).longValue());
                    c().c.a(j().a());
                }
                q().C().a("Upload scheduled in approximately ms", Long.valueOf(a6));
                f().a(a6);
            }
        }
    }

    private final void l() {
        w();
        if (this.o || this.p || this.q) {
            q().C().a("Not stopping services. fetch, network, upload", Boolean.valueOf(this.o), Boolean.valueOf(this.p), Boolean.valueOf(this.q));
            return;
        }
        q().C().a("Stopping uploading service(s)");
        if (this.l != null) {
            for (Runnable run : this.l) {
                run.run();
            }
            this.l.clear();
        }
    }

    private final boolean o() {
        w();
        try {
            this.s = new RandomAccessFile(new File(k().getFilesDir(), "google_app_measurement.db"), "rw").getChannel();
            this.r = this.s.tryLock();
            if (this.r != null) {
                q().C().a("Storage concurrent access okay");
                return true;
            }
            q().v().a("Storage concurrent data access panic");
            return false;
        } catch (FileNotFoundException e2) {
            q().v().a("Failed to acquire storage lock", e2);
        } catch (IOException e3) {
            q().v().a("Failed to access storage lock file", e3);
        }
    }

    private final boolean r() {
        w();
        F();
        return this.j;
    }

    public final bd C() {
        b((fj) this.f4074b);
        return this.f4074b;
    }

    public final ab D() {
        b((fj) this.d);
        return this.d;
    }

    public final u E() {
        b((fj) this.g);
        return this.g;
    }

    /* access modifiers changed from: 0000 */
    public final void F() {
        if (!this.i) {
            throw new IllegalStateException("UploadController is not initialized");
        }
    }

    public final void G() {
        String y;
        Object obj;
        List list;
        String str;
        w();
        F();
        this.q = true;
        try {
            Boolean A = this.h.s().A();
            if (A == null) {
                q().y().a("Upload data called on the client side before use of service was decided");
                this.q = false;
                l();
            } else if (A.booleanValue()) {
                q().v().a("Upload called in the client side when service should be used");
                this.q = false;
                l();
            } else if (this.k > 0) {
                i();
                this.q = false;
                l();
            } else {
                w();
                if (this.t != null) {
                    q().C().a("Uploading requested multiple times");
                    this.q = false;
                    l();
                } else if (!C().u()) {
                    q().C().a("Network not connected, ignoring upload request");
                    i();
                    this.q = false;
                    l();
                } else {
                    long a2 = j().a();
                    a((String) null, a2 - y.w());
                    long a3 = c().c.a();
                    if (a3 != 0) {
                        q().B().a("Uploading events. Elapsed time since last upload attempt (ms)", Long.valueOf(Math.abs(a2 - a3)));
                    }
                    y = D().y();
                    if (!TextUtils.isEmpty(y)) {
                        if (this.c == -1) {
                            this.c = D().F();
                        }
                        List a4 = D().a(y, b().b(y, ap.h), Math.max(0, b().b(y, ap.i)));
                        if (!a4.isEmpty()) {
                            Iterator it = a4.iterator();
                            while (true) {
                                if (!it.hasNext()) {
                                    obj = null;
                                    break;
                                }
                                gj gjVar = (gj) ((Pair) it.next()).first;
                                if (!TextUtils.isEmpty(gjVar.u)) {
                                    obj = gjVar.u;
                                    break;
                                }
                            }
                            if (obj != null) {
                                int i2 = 0;
                                while (true) {
                                    if (i2 >= a4.size()) {
                                        break;
                                    }
                                    gj gjVar2 = (gj) ((Pair) a4.get(i2)).first;
                                    if (!TextUtils.isEmpty(gjVar2.u) && !gjVar2.u.equals(obj)) {
                                        list = a4.subList(0, i2);
                                        break;
                                    }
                                    i2++;
                                }
                            }
                            list = a4;
                            gi giVar = new gi();
                            giVar.c = new gj[list.size()];
                            ArrayList arrayList = new ArrayList(list.size());
                            boolean z = y.y() && b().c(y);
                            for (int i3 = 0; i3 < giVar.c.length; i3++) {
                                giVar.c[i3] = (gj) ((Pair) list.get(i3)).first;
                                arrayList.add((Long) ((Pair) list.get(i3)).second);
                                giVar.c[i3].t = Long.valueOf(12451);
                                giVar.c[i3].f = Long.valueOf(a2);
                                giVar.c[i3].B = Boolean.valueOf(false);
                                if (!z) {
                                    giVar.c[i3].I = null;
                                }
                            }
                            Object obj2 = q().a(2) ? n().a(giVar) : null;
                            byte[] a5 = m().a(giVar);
                            str = (String) ap.r.b();
                            URL url = new URL(str);
                            aa.b(!arrayList.isEmpty());
                            if (this.t != null) {
                                q().v().a("Set uploading progress before finishing the previous upload");
                            } else {
                                this.t = new ArrayList(arrayList);
                            }
                            c().d.a(a2);
                            String str2 = "?";
                            if (giVar.c.length > 0) {
                                str2 = giVar.c[0].q;
                            }
                            q().C().a("Uploading data. app, uncompressed size, data", str2, Integer.valueOf(a5.length), obj2);
                            this.p = true;
                            bd C = C();
                            fl flVar = new fl(this, y);
                            C.c();
                            C.N();
                            aa.a(url);
                            aa.a(a5);
                            aa.a(flVar);
                            C.p().b((Runnable) new bh(C, y, url, a5, null, flVar));
                        }
                    } else {
                        this.c = -1;
                        String a6 = D().a(a2 - y.w());
                        if (!TextUtils.isEmpty(a6)) {
                            r b2 = D().b(a6);
                            if (b2 != null) {
                                a(b2);
                            }
                        }
                    }
                    this.q = false;
                    l();
                }
            }
        } catch (MalformedURLException e2) {
            q().v().a("Failed to parse upload URL. Not uploading. appId", az.a(y), str);
        } catch (Throwable th) {
            this.q = false;
            l();
            throw th;
        }
    }

    /* access modifiers changed from: 0000 */
    public final void H() {
        w();
        F();
        if (!this.j) {
            q().A().a("This instance being marked as an uploader");
            w();
            F();
            if (r() && o()) {
                int a2 = a(this.s);
                int y = this.h.u().y();
                w();
                if (a2 > y) {
                    q().v().a("Panic: can't downgrade version. Previous, current version", Integer.valueOf(a2), Integer.valueOf(y));
                } else if (a2 < y) {
                    if (a(y, this.s)) {
                        q().C().a("Storage version upgraded. Previous, current version", Integer.valueOf(a2), Integer.valueOf(y));
                    } else {
                        q().v().a("Storage version upgrade failed. Previous, current version", Integer.valueOf(a2), Integer.valueOf(y));
                    }
                }
            }
            this.j = true;
            i();
        }
    }

    /* access modifiers changed from: 0000 */
    public final void I() {
        this.n++;
    }

    /* access modifiers changed from: 0000 */
    public final ce J() {
        return this.h;
    }

    /* access modifiers changed from: 0000 */
    public final s a(String str) {
        r b2 = D().b(str);
        if (b2 == null || TextUtils.isEmpty(b2.i())) {
            q().B().a("No app data available; dropping", str);
            return null;
        }
        Boolean b3 = b(b2);
        if (b3 == null || b3.booleanValue()) {
            return new s(str, b2.d(), b2.i(), b2.j(), b2.k(), b2.l(), b2.m(), (String) null, b2.n(), false, b2.f(), b2.A(), 0, 0, b2.B(), b2.C(), false);
        }
        q().v().a("App version does not match; dropping. appId", az.a(str));
        return null;
    }

    /* access modifiers changed from: protected */
    public void a() {
        w();
        D().A();
        if (c().c.a() == 0) {
            c().c.a(j().a());
        }
        i();
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: protected */
    public final void a(int i2, Throwable th, byte[] bArr, String str) {
        ab D;
        w();
        F();
        if (bArr == null) {
            try {
                bArr = new byte[0];
            } catch (Throwable th2) {
                this.p = false;
                l();
                throw th2;
            }
        }
        List<Long> list = this.t;
        this.t = null;
        if ((i2 == 200 || i2 == 204) && th == null) {
            try {
                c().c.a(j().a());
                c().d.a(0);
                i();
                q().C().a("Successful upload. Got network response. code, size", Integer.valueOf(i2), Integer.valueOf(bArr.length));
                D().u();
                try {
                    for (Long l2 : list) {
                        try {
                            D = D();
                            long longValue = l2.longValue();
                            D.c();
                            D.N();
                            if (D.x().delete("queue", "rowid=?", new String[]{String.valueOf(longValue)}) != 1) {
                                throw new SQLiteException("Deleted fewer rows from queue than expected");
                            }
                        } catch (SQLiteException e2) {
                            D.q().v().a("Failed to delete a bundle in a queue table", e2);
                            throw e2;
                        } catch (SQLiteException e3) {
                            if (this.u == null || !this.u.contains(l2)) {
                                throw e3;
                            }
                        }
                    }
                    D().v();
                    D().w();
                    this.u = null;
                    if (!C().u() || !h()) {
                        this.c = -1;
                        i();
                    } else {
                        G();
                    }
                    this.k = 0;
                } catch (Throwable th3) {
                    D().w();
                    throw th3;
                }
            } catch (SQLiteException e4) {
                q().v().a("Database error while trying to delete uploaded bundles", e4);
                this.k = j().b();
                q().C().a("Disable upload, time", Long.valueOf(this.k));
            }
        } else {
            q().C().a("Network upload failed. Will retry later. code, error", Integer.valueOf(i2), th);
            c().d.a(j().a());
            if (i2 == 503 || i2 == 429) {
                c().e.a(j().a());
            }
            if (b().g(str)) {
                D().a(list);
            }
            i();
        }
        this.p = false;
        l();
    }

    /* access modifiers changed from: 0000 */
    public final void a(an anVar, s sVar) {
        List<w> a2;
        List<w> a3;
        List<w> a4;
        aa.a(sVar);
        aa.a(sVar.f4127a);
        w();
        F();
        String str = sVar.f4127a;
        long j2 = anVar.d;
        m();
        if (ft.a(anVar, sVar)) {
            if (!sVar.h) {
                e(sVar);
                return;
            }
            D().u();
            try {
                ab D = D();
                aa.a(str);
                D.c();
                D.N();
                if (j2 < 0) {
                    D.q().y().a("Invalid time querying timed out conditional properties", az.a(str), Long.valueOf(j2));
                    a2 = Collections.emptyList();
                } else {
                    a2 = D.a("active=0 and app_id=? and abs(? - creation_timestamp) > trigger_timeout", new String[]{str, String.valueOf(j2)});
                }
                for (w wVar : a2) {
                    if (wVar != null) {
                        q().B().a("User property timed out", wVar.f4129a, n().c(wVar.c.f4082a), wVar.c.a());
                        if (wVar.g != null) {
                            b(new an(wVar.g, j2), sVar);
                        }
                        D().e(str, wVar.c.f4082a);
                    }
                }
                ab D2 = D();
                aa.a(str);
                D2.c();
                D2.N();
                if (j2 < 0) {
                    D2.q().y().a("Invalid time querying expired conditional properties", az.a(str), Long.valueOf(j2));
                    a3 = Collections.emptyList();
                } else {
                    a3 = D2.a("active<>0 and app_id=? and abs(? - triggered_timestamp) > time_to_live", new String[]{str, String.valueOf(j2)});
                }
                ArrayList arrayList = new ArrayList(a3.size());
                for (w wVar2 : a3) {
                    if (wVar2 != null) {
                        q().B().a("User property expired", wVar2.f4129a, n().c(wVar2.c.f4082a), wVar2.c.a());
                        D().b(str, wVar2.c.f4082a);
                        if (wVar2.k != null) {
                            arrayList.add(wVar2.k);
                        }
                        D().e(str, wVar2.c.f4082a);
                    }
                }
                ArrayList arrayList2 = arrayList;
                int size = arrayList2.size();
                int i2 = 0;
                while (i2 < size) {
                    Object obj = arrayList2.get(i2);
                    i2++;
                    b(new an((an) obj, j2), sVar);
                }
                ab D3 = D();
                String str2 = anVar.f3864a;
                aa.a(str);
                aa.a(str2);
                D3.c();
                D3.N();
                if (j2 < 0) {
                    D3.q().y().a("Invalid time querying triggered conditional properties", az.a(str), D3.m().a(str2), Long.valueOf(j2));
                    a4 = Collections.emptyList();
                } else {
                    a4 = D3.a("active=0 and app_id=? and trigger_event_name=? and abs(? - creation_timestamp) <= trigger_timeout", new String[]{str, str2, String.valueOf(j2)});
                }
                ArrayList arrayList3 = new ArrayList(a4.size());
                for (w wVar3 : a4) {
                    if (wVar3 != null) {
                        fq fqVar = wVar3.c;
                        fs fsVar = new fs(wVar3.f4129a, wVar3.f4130b, fqVar.f4082a, j2, fqVar.a());
                        if (D().a(fsVar)) {
                            q().B().a("User property triggered", wVar3.f4129a, n().c(fsVar.c), fsVar.e);
                        } else {
                            q().v().a("Too many active user properties, ignoring", az.a(wVar3.f4129a), n().c(fsVar.c), fsVar.e);
                        }
                        if (wVar3.i != null) {
                            arrayList3.add(wVar3.i);
                        }
                        wVar3.c = new fq(fsVar);
                        wVar3.e = true;
                        D().a(wVar3);
                    }
                }
                b(anVar, sVar);
                ArrayList arrayList4 = arrayList3;
                int size2 = arrayList4.size();
                int i3 = 0;
                while (i3 < size2) {
                    Object obj2 = arrayList4.get(i3);
                    i3++;
                    b(new an((an) obj2, j2), sVar);
                }
                D().v();
            } finally {
                D().w();
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public final void a(an anVar, String str) {
        r b2 = D().b(str);
        if (b2 == null || TextUtils.isEmpty(b2.i())) {
            q().B().a("No app data available; dropping event", str);
            return;
        }
        Boolean b3 = b(b2);
        if (b3 == null) {
            if (!"_ui".equals(anVar.f3864a)) {
                q().y().a("Could not find package. appId", az.a(str));
            }
        } else if (!b3.booleanValue()) {
            q().v().a("App version does not match; dropping event. appId", az.a(str));
            return;
        }
        an anVar2 = anVar;
        a(anVar2, new s(str, b2.d(), b2.i(), b2.j(), b2.k(), b2.l(), b2.m(), (String) null, b2.n(), false, b2.f(), b2.A(), 0, 0, b2.B(), b2.C(), false));
    }

    /* access modifiers changed from: 0000 */
    public final void a(ce ceVar) {
        this.h = ceVar;
    }

    /* access modifiers changed from: 0000 */
    public final void a(fj fjVar) {
        this.m++;
    }

    /* access modifiers changed from: 0000 */
    public final void a(fp fpVar) {
        w();
        ab abVar = new ab(this.h);
        abVar.O();
        this.d = abVar;
        b().a((aa) this.f4073a);
        u uVar = new u(this.h);
        uVar.O();
        this.g = uVar;
        fg fgVar = new fg(this.h);
        fgVar.O();
        this.f = fgVar;
        this.e = new bi(this.h);
        if (this.m != this.n) {
            q().v().a("Not all upload components initialized", Integer.valueOf(this.m), Integer.valueOf(this.n));
        }
        this.i = true;
    }

    /* access modifiers changed from: 0000 */
    public final void a(fq fqVar, s sVar) {
        int i2 = 0;
        w();
        F();
        if (!TextUtils.isEmpty(sVar.f4128b)) {
            if (!sVar.h) {
                e(sVar);
                return;
            }
            int d2 = m().d(fqVar.f4082a);
            if (d2 != 0) {
                m();
                String a2 = ft.a(fqVar.f4082a, 24, true);
                if (fqVar.f4082a != null) {
                    i2 = fqVar.f4082a.length();
                }
                m().a(sVar.f4127a, d2, "_ev", a2, i2);
                return;
            }
            int b2 = m().b(fqVar.f4082a, fqVar.a());
            if (b2 != 0) {
                m();
                String a3 = ft.a(fqVar.f4082a, 24, true);
                Object a4 = fqVar.a();
                if (a4 != null && ((a4 instanceof String) || (a4 instanceof CharSequence))) {
                    i2 = String.valueOf(a4).length();
                }
                m().a(sVar.f4127a, b2, "_ev", a3, i2);
                return;
            }
            Object c2 = m().c(fqVar.f4082a, fqVar.a());
            if (c2 != null) {
                fs fsVar = new fs(sVar.f4127a, fqVar.c, fqVar.f4082a, fqVar.f4083b, c2);
                q().B().a("Setting user property", n().c(fsVar.c), c2);
                D().u();
                try {
                    e(sVar);
                    boolean a5 = D().a(fsVar);
                    D().v();
                    if (a5) {
                        q().B().a("User property set", n().c(fsVar.c), fsVar.e);
                    } else {
                        q().v().a("Too many unique user properties are set. Ignoring user property", n().c(fsVar.c), fsVar.e);
                        m().a(sVar.f4127a, 9, (String) null, (String) null, 0);
                    }
                } finally {
                    D().w();
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public final void a(s sVar) {
        w();
        F();
        aa.a(sVar.f4127a);
        e(sVar);
    }

    /* access modifiers changed from: 0000 */
    public final void a(w wVar, s sVar) {
        boolean z = true;
        aa.a(wVar);
        aa.a(wVar.f4129a);
        aa.a(wVar.f4130b);
        aa.a(wVar.c);
        aa.a(wVar.c.f4082a);
        w();
        F();
        if (!TextUtils.isEmpty(sVar.f4128b)) {
            if (!sVar.h) {
                e(sVar);
                return;
            }
            w wVar2 = new w(wVar);
            wVar2.e = false;
            D().u();
            try {
                w d2 = D().d(wVar2.f4129a, wVar2.c.f4082a);
                if (d2 != null && !d2.f4130b.equals(wVar2.f4130b)) {
                    q().y().a("Updating a conditional user property with different origin. name, origin, origin (from DB)", n().c(wVar2.c.f4082a), wVar2.f4130b, d2.f4130b);
                }
                if (d2 != null && d2.e) {
                    wVar2.f4130b = d2.f4130b;
                    wVar2.d = d2.d;
                    wVar2.h = d2.h;
                    wVar2.f = d2.f;
                    wVar2.i = d2.i;
                    wVar2.e = d2.e;
                    wVar2.c = new fq(wVar2.c.f4082a, d2.c.f4083b, wVar2.c.a(), d2.c.c);
                    z = false;
                } else if (TextUtils.isEmpty(wVar2.f)) {
                    wVar2.c = new fq(wVar2.c.f4082a, wVar2.d, wVar2.c.a(), wVar2.c.c);
                    wVar2.e = true;
                } else {
                    z = false;
                }
                if (wVar2.e) {
                    fq fqVar = wVar2.c;
                    fs fsVar = new fs(wVar2.f4129a, wVar2.f4130b, fqVar.f4082a, fqVar.f4083b, fqVar.a());
                    if (D().a(fsVar)) {
                        q().B().a("User property updated immediately", wVar2.f4129a, n().c(fsVar.c), fsVar.e);
                    } else {
                        q().v().a("(2)Too many active user properties, ignoring", az.a(wVar2.f4129a), n().c(fsVar.c), fsVar.e);
                    }
                    if (z && wVar2.i != null) {
                        b(new an(wVar2.i, wVar2.d), sVar);
                    }
                }
                if (D().a(wVar2)) {
                    q().B().a("Conditional property added", wVar2.f4129a, n().c(wVar2.c.f4082a), wVar2.c.a());
                } else {
                    q().v().a("Too many conditional properties, ignoring", az.a(wVar2.f4129a), n().c(wVar2.c.f4082a), wVar2.c.a());
                }
                D().v();
            } finally {
                D().w();
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public final void a(Runnable runnable) {
        w();
        if (this.l == null) {
            this.l = new ArrayList();
        }
        this.l.add(runnable);
    }

    /* access modifiers changed from: 0000 */
    public final void a(String str, int i2, Throwable th, byte[] bArr, Map<String, List<String>> map) {
        boolean z = true;
        w();
        F();
        aa.a(str);
        if (bArr == null) {
            try {
                bArr = new byte[0];
            } catch (Throwable th2) {
                this.o = false;
                l();
                throw th2;
            }
        }
        q().C().a("onConfigFetched. Response size", Integer.valueOf(bArr.length));
        D().u();
        r b2 = D().b(str);
        boolean z2 = (i2 == 200 || i2 == 204 || i2 == 304) && th == null;
        if (b2 == null) {
            q().y().a("App does not exist in onConfigFetched. appId", az.a(str));
        } else if (z2 || i2 == 404) {
            List list = map != null ? (List) map.get("Last-Modified") : null;
            String str2 = (list == null || list.size() <= 0) ? null : (String) list.get(0);
            if (i2 == 404 || i2 == 304) {
                if (d().a(str) == null && !d().a(str, null, null)) {
                    D().w();
                    this.o = false;
                    l();
                    return;
                }
            } else if (!d().a(str, bArr, str2)) {
                D().w();
                this.o = false;
                l();
                return;
            }
            b2.g(j().a());
            D().a(b2);
            if (i2 == 404) {
                q().z().a("Config not found. Using empty config. appId", str);
            } else {
                q().C().a("Successfully fetched config. Got network response. code, size", Integer.valueOf(i2), Integer.valueOf(bArr.length));
            }
            if (!C().u() || !h()) {
                i();
            } else {
                G();
            }
        } else {
            b2.h(j().a());
            D().a(b2);
            q().C().a("Fetching config failed. code, error", Integer.valueOf(i2), th);
            d().c(str);
            c().d.a(j().a());
            if (!(i2 == 503 || i2 == 429)) {
                z = false;
            }
            if (z) {
                c().e.a(j().a());
            }
            i();
        }
        D().v();
        D().w();
        this.o = false;
        l();
    }

    public final void a(boolean z) {
        i();
    }

    public y b() {
        return this.h.b();
    }

    /* access modifiers changed from: 0000 */
    public final void b(fq fqVar, s sVar) {
        w();
        F();
        if (!TextUtils.isEmpty(sVar.f4128b)) {
            if (!sVar.h) {
                e(sVar);
                return;
            }
            q().B().a("Removing user property", n().c(fqVar.f4082a));
            D().u();
            try {
                e(sVar);
                D().b(sVar.f4127a, fqVar.f4082a);
                D().v();
                q().B().a("User property removed", n().c(fqVar.f4082a));
            } finally {
                D().w();
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public final void b(s sVar) {
        if (this.t != null) {
            this.u = new ArrayList();
            this.u.addAll(this.t);
        }
        ab D = D();
        String str = sVar.f4127a;
        aa.a(str);
        D.c();
        D.N();
        try {
            SQLiteDatabase x = D.x();
            String[] strArr = {str};
            int delete = x.delete("main_event_params", "app_id=?", strArr) + x.delete("apps", "app_id=?", strArr) + 0 + x.delete("events", "app_id=?", strArr) + x.delete("user_attributes", "app_id=?", strArr) + x.delete("conditional_properties", "app_id=?", strArr) + x.delete("raw_events", "app_id=?", strArr) + x.delete("raw_events_metadata", "app_id=?", strArr) + x.delete("queue", "app_id=?", strArr) + x.delete("audience_filter_values", "app_id=?", strArr);
            if (delete > 0) {
                D.q().C().a("Reset analytics data. app, records", str, Integer.valueOf(delete));
            }
        } catch (SQLiteException e2) {
            D.q().v().a("Error resetting analytics data. appId, error", az.a(str), e2);
        }
        s a2 = a(k(), sVar.f4127a, sVar.f4128b, sVar.h, sVar.o, sVar.p, sVar.m);
        if (!b().i(sVar.f4127a) || sVar.h) {
            c(a2);
        }
    }

    /* access modifiers changed from: 0000 */
    public final void b(w wVar, s sVar) {
        aa.a(wVar);
        aa.a(wVar.f4129a);
        aa.a(wVar.c);
        aa.a(wVar.c.f4082a);
        w();
        F();
        if (!TextUtils.isEmpty(sVar.f4128b)) {
            if (!sVar.h) {
                e(sVar);
                return;
            }
            D().u();
            try {
                e(sVar);
                w d2 = D().d(wVar.f4129a, wVar.c.f4082a);
                if (d2 != null) {
                    q().B().a("Removing conditional user property", wVar.f4129a, n().c(wVar.c.f4082a));
                    D().e(wVar.f4129a, wVar.c.f4082a);
                    if (d2.e) {
                        D().b(wVar.f4129a, wVar.c.f4082a);
                    }
                    if (wVar.k != null) {
                        Bundle bundle = null;
                        if (wVar.k.f3865b != null) {
                            bundle = wVar.k.f3865b.b();
                        }
                        b(m().a(wVar.k.f3864a, bundle, d2.f4130b, wVar.k.d, true, false), sVar);
                    }
                } else {
                    q().y().a("Conditional user property doesn't exist", az.a(wVar.f4129a), n().c(wVar.c.f4082a));
                }
                D().v();
            } finally {
                D().w();
            }
        }
    }

    public final byte[] b(an anVar, String str) {
        long j2;
        gl glVar;
        F();
        w();
        ce.z();
        aa.a(anVar);
        aa.a(str);
        gi giVar = new gi();
        D().u();
        try {
            r b2 = D().b(str);
            if (b2 == null) {
                q().B().a("Log and bundle not available. package_name", str);
                return new byte[0];
            } else if (!b2.n()) {
                q().B().a("Log and bundle disabled. package_name", str);
                byte[] bArr = new byte[0];
                D().w();
                return bArr;
            } else {
                if (("_iap".equals(anVar.f3864a) || "ecommerce_purchase".equals(anVar.f3864a)) && !a(str, anVar)) {
                    q().y().a("Failed to handle purchase event at single event bundle creation. appId", az.a(str));
                }
                boolean e2 = b().e(str);
                Long valueOf = Long.valueOf(0);
                if (e2 && "_e".equals(anVar.f3864a)) {
                    if (anVar.f3865b == null || anVar.f3865b.a() == 0) {
                        q().y().a("The engagement event does not contain any parameters. appId", az.a(str));
                    } else if (anVar.f3865b.b("_et") == null) {
                        q().y().a("The engagement event does not include duration. appId", az.a(str));
                    } else {
                        valueOf = anVar.f3865b.b("_et");
                    }
                }
                gj gjVar = new gj();
                giVar.c = new gj[]{gjVar};
                gjVar.c = Integer.valueOf(1);
                gjVar.k = "android";
                gjVar.q = b2.b();
                gjVar.p = b2.k();
                gjVar.r = b2.i();
                long j3 = b2.j();
                gjVar.E = j3 == -2147483648L ? null : Integer.valueOf((int) j3);
                gjVar.s = Long.valueOf(b2.l());
                gjVar.A = b2.d();
                gjVar.x = Long.valueOf(b2.m());
                if (this.h.x() && y.y() && b().c(gjVar.q)) {
                    gjVar.I = null;
                }
                Pair a2 = c().a(b2.b());
                if (b2.B() && a2 != null && !TextUtils.isEmpty((CharSequence) a2.first)) {
                    gjVar.u = (String) a2.first;
                    gjVar.v = (Boolean) a2.second;
                }
                t().F();
                gjVar.m = Build.MODEL;
                t().F();
                gjVar.l = VERSION.RELEASE;
                gjVar.o = Integer.valueOf((int) t().u());
                gjVar.n = t().v();
                gjVar.w = b2.c();
                gjVar.D = b2.f();
                List a3 = D().a(b2.b());
                gjVar.e = new gl[a3.size()];
                fs fsVar = null;
                if (e2) {
                    fs c2 = D().c(gjVar.q, "_lte");
                    fsVar = (c2 == null || c2.e == null) ? new fs(gjVar.q, "auto", "_lte", j().a(), valueOf) : valueOf.longValue() > 0 ? new fs(gjVar.q, "auto", "_lte", j().a(), Long.valueOf(((Long) c2.e).longValue() + valueOf.longValue())) : c2;
                }
                gl glVar2 = null;
                int i2 = 0;
                while (i2 < a3.size()) {
                    gl glVar3 = new gl();
                    gjVar.e[i2] = glVar3;
                    glVar3.d = ((fs) a3.get(i2)).c;
                    glVar3.c = Long.valueOf(((fs) a3.get(i2)).d);
                    m().a(glVar3, ((fs) a3.get(i2)).e);
                    if (!e2 || !"_lte".equals(glVar3.d)) {
                        glVar = glVar2;
                    } else {
                        glVar3.f = (Long) fsVar.e;
                        glVar3.c = Long.valueOf(j().a());
                        glVar = glVar3;
                    }
                    i2++;
                    glVar2 = glVar;
                }
                if (e2 && glVar2 == null) {
                    gl glVar4 = new gl();
                    glVar4.d = "_lte";
                    glVar4.c = Long.valueOf(j().a());
                    glVar4.f = (Long) fsVar.e;
                    gjVar.e = (gl[]) Arrays.copyOf(gjVar.e, gjVar.e.length + 1);
                    gjVar.e[gjVar.e.length - 1] = glVar4;
                }
                if (valueOf.longValue() > 0) {
                    D().a(fsVar);
                }
                Bundle b3 = anVar.f3865b.b();
                if ("_iap".equals(anVar.f3864a)) {
                    b3.putLong("_c", 1);
                    q().B().a("Marking in-app purchase as real-time");
                    b3.putLong("_r", 1);
                }
                b3.putString("_o", anVar.c);
                if (m().i(gjVar.q)) {
                    m().a(b3, "_dbg", (Object) Long.valueOf(1));
                    m().a(b3, "_r", (Object) Long.valueOf(1));
                }
                aj a4 = D().a(str, anVar.f3864a);
                if (a4 == null) {
                    D().a(new aj(str, anVar.f3864a, 1, 0, anVar.d, 0, null, null, null));
                    j2 = 0;
                } else {
                    j2 = a4.e;
                    D().a(a4.a(anVar.d).a());
                }
                ai aiVar = new ai(this.h, anVar.c, str, anVar.f3864a, anVar.d, j2, b3);
                gg ggVar = new gg();
                gjVar.d = new gg[]{ggVar};
                ggVar.e = Long.valueOf(aiVar.c);
                ggVar.d = aiVar.f3858b;
                ggVar.f = Long.valueOf(aiVar.d);
                ggVar.c = new gh[aiVar.e.a()];
                Iterator it = aiVar.e.iterator();
                int i3 = 0;
                while (it.hasNext()) {
                    String str2 = (String) it.next();
                    gh ghVar = new gh();
                    int i4 = i3 + 1;
                    ggVar.c[i3] = ghVar;
                    ghVar.c = str2;
                    m().a(ghVar, aiVar.e.a(str2));
                    i3 = i4;
                }
                gjVar.C = a(b2.b(), gjVar.e, gjVar.d);
                gjVar.g = ggVar.e;
                gjVar.h = ggVar.e;
                long h2 = b2.h();
                gjVar.j = h2 != 0 ? Long.valueOf(h2) : null;
                long g2 = b2.g();
                if (g2 != 0) {
                    h2 = g2;
                }
                gjVar.i = h2 != 0 ? Long.valueOf(h2) : null;
                b2.r();
                gjVar.y = Integer.valueOf((int) b2.o());
                gjVar.t = Long.valueOf(12451);
                gjVar.f = Long.valueOf(j().a());
                gjVar.B = Boolean.TRUE;
                b2.a(gjVar.g.longValue());
                b2.b(gjVar.h.longValue());
                D().a(b2);
                D().v();
                D().w();
                try {
                    byte[] bArr2 = new byte[giVar.d()];
                    b a5 = b.a(bArr2, 0, bArr2.length);
                    giVar.a(a5);
                    a5.a();
                    return m().a(bArr2);
                } catch (IOException e3) {
                    q().v().a("Data loss. Failed to bundle and serialize. appId", az.a(str), e3);
                    return null;
                }
            }
        } finally {
            D().w();
        }
    }

    public bk c() {
        return this.h.c();
    }

    public final void c(s sVar) {
        int i2;
        r b2;
        ApplicationInfo applicationInfo;
        ab D;
        String b3;
        w();
        F();
        aa.a(sVar);
        aa.a(sVar.f4127a);
        if (!TextUtils.isEmpty(sVar.f4128b)) {
            r b4 = D().b(sVar.f4127a);
            if (b4 != null && TextUtils.isEmpty(b4.d()) && !TextUtils.isEmpty(sVar.f4128b)) {
                b4.g(0);
                D().a(b4);
                d().d(sVar.f4127a);
            }
            if (!sVar.h) {
                e(sVar);
                return;
            }
            long j2 = sVar.m;
            if (j2 == 0) {
                j2 = j().a();
            }
            int i3 = sVar.n;
            if (i3 == 0 || i3 == 1) {
                i2 = i3;
            } else {
                q().y().a("Incorrect app type, assuming installed app. appId, appType", az.a(sVar.f4127a), Integer.valueOf(i3));
                i2 = 0;
            }
            D().u();
            try {
                b2 = D().b(sVar.f4127a);
                if (!(b2 == null || b2.d() == null || b2.d().equals(sVar.f4128b))) {
                    q().y().a("New GMP App Id passed in. Removing cached database data. appId", az.a(b2.b()));
                    D = D();
                    b3 = b2.b();
                    D.N();
                    D.c();
                    aa.a(b3);
                    SQLiteDatabase x = D.x();
                    String[] strArr = {b3};
                    int delete = x.delete("audience_filter_values", "app_id=?", strArr) + x.delete("events", "app_id=?", strArr) + 0 + x.delete("user_attributes", "app_id=?", strArr) + x.delete("conditional_properties", "app_id=?", strArr) + x.delete("apps", "app_id=?", strArr) + x.delete("raw_events", "app_id=?", strArr) + x.delete("raw_events_metadata", "app_id=?", strArr) + x.delete("event_filters", "app_id=?", strArr) + x.delete("property_filters", "app_id=?", strArr);
                    if (delete > 0) {
                        D.q().C().a("Deleted application data. app, records", b3, Integer.valueOf(delete));
                    }
                    b2 = null;
                }
            } catch (SQLiteException e2) {
                D.q().v().a("Error deleting application data. appId, error", az.a(b3), e2);
            } catch (Throwable th) {
                D().w();
                throw th;
            }
            if (b2 != null) {
                if (b2.j() != -2147483648L) {
                    if (b2.j() != sVar.j) {
                        Bundle bundle = new Bundle();
                        bundle.putString("_pv", b2.i());
                        a(new an("_au", new ak(bundle), "auto", j2), sVar);
                    }
                } else if (b2.i() != null && !b2.i().equals(sVar.c)) {
                    Bundle bundle2 = new Bundle();
                    bundle2.putString("_pv", b2.i());
                    a(new an("_au", new ak(bundle2), "auto", j2), sVar);
                }
            }
            e(sVar);
            aj ajVar = null;
            if (i2 == 0) {
                ajVar = D().a(sVar.f4127a, "_f");
            } else if (i2 == 1) {
                ajVar = D().a(sVar.f4127a, "_v");
            }
            if (ajVar == null) {
                long j3 = (1 + (j2 / 3600000)) * 3600000;
                if (i2 == 0) {
                    a(new fq("_fot", j2, Long.valueOf(j3), "auto"), sVar);
                    w();
                    F();
                    Bundle bundle3 = new Bundle();
                    bundle3.putLong("_c", 1);
                    bundle3.putLong("_r", 1);
                    bundle3.putLong("_uwa", 0);
                    bundle3.putLong("_pfo", 0);
                    bundle3.putLong("_sys", 0);
                    bundle3.putLong("_sysu", 0);
                    if (b().i(sVar.f4127a) && sVar.q) {
                        bundle3.putLong("_dac", 1);
                    }
                    if (k().getPackageManager() == null) {
                        q().v().a("PackageManager is null, first open report might be inaccurate. appId", az.a(sVar.f4127a));
                    } else {
                        PackageInfo packageInfo = null;
                        try {
                            packageInfo = c.b(k()).b(sVar.f4127a, 0);
                        } catch (NameNotFoundException e3) {
                            q().v().a("Package info is null, first open report might be inaccurate. appId", az.a(sVar.f4127a), e3);
                        }
                        if (packageInfo != null) {
                            if (packageInfo.firstInstallTime != 0) {
                                boolean z = false;
                                if (packageInfo.firstInstallTime != packageInfo.lastUpdateTime) {
                                    bundle3.putLong("_uwa", 1);
                                } else {
                                    z = true;
                                }
                                a(new fq("_fi", j2, Long.valueOf(z ? 1 : 0), "auto"), sVar);
                            }
                        }
                        try {
                            applicationInfo = c.b(k()).a(sVar.f4127a, 0);
                        } catch (NameNotFoundException e4) {
                            q().v().a("Application info is null, first open report might be inaccurate. appId", az.a(sVar.f4127a), e4);
                            applicationInfo = null;
                        }
                        if (applicationInfo != null) {
                            if ((applicationInfo.flags & 1) != 0) {
                                bundle3.putLong("_sys", 1);
                            }
                            if ((applicationInfo.flags & 128) != 0) {
                                bundle3.putLong("_sysu", 1);
                            }
                        }
                    }
                    ab D2 = D();
                    String str = sVar.f4127a;
                    aa.a(str);
                    D2.c();
                    D2.N();
                    long h2 = D2.h(str, "first_open_count");
                    if (h2 >= 0) {
                        bundle3.putLong("_pfo", h2);
                    }
                    a(new an("_f", new ak(bundle3), "auto", j2), sVar);
                } else if (i2 == 1) {
                    a(new fq("_fvt", j2, Long.valueOf(j3), "auto"), sVar);
                    w();
                    F();
                    Bundle bundle4 = new Bundle();
                    bundle4.putLong("_c", 1);
                    bundle4.putLong("_r", 1);
                    if (b().i(sVar.f4127a) && sVar.q) {
                        bundle4.putLong("_dac", 1);
                    }
                    a(new an("_v", new ak(bundle4), "auto", j2), sVar);
                }
                Bundle bundle5 = new Bundle();
                bundle5.putLong("_et", 1);
                a(new an("_e", new ak(bundle5), "auto", j2), sVar);
            } else if (sVar.i) {
                a(new an("_cd", new ak(new Bundle()), "auto", j2), sVar);
            }
            D().v();
            D().w();
        }
    }

    public final String d(s sVar) {
        try {
            return (String) p().a((Callable<V>) new fn<V>(this, sVar)).get(30000, TimeUnit.MILLISECONDS);
        } catch (InterruptedException | ExecutionException | TimeoutException e2) {
            q().v().a("Failed to get app instance id. appId", az.a(sVar.f4127a), e2);
            return null;
        }
    }

    public e j() {
        return this.h.j();
    }

    public Context k() {
        return this.h.k();
    }

    public ft m() {
        return this.h.m();
    }

    public ax n() {
        return this.h.n();
    }

    public bz p() {
        return this.h.p();
    }

    public az q() {
        return this.h.q();
    }

    public ah t() {
        return this.h.t();
    }

    public void w() {
        p().c();
    }
}
