package com.google.android.gms.internal.c;

import android.os.RemoteException;

final class ef implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ s f4020a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ eb f4021b;

    ef(eb ebVar, s sVar) {
        this.f4021b = ebVar;
        this.f4020a = sVar;
    }

    public final void run() {
        ar d = this.f4021b.f4014b;
        if (d == null) {
            this.f4021b.q().v().a("Discarding data. Failed to send app launch");
            return;
        }
        try {
            d.a(this.f4020a);
            this.f4021b.a(d, null, this.f4020a);
            this.f4021b.C();
        } catch (RemoteException e) {
            this.f4021b.q().v().a("Failed to send app launch to the service", e);
        }
    }
}
