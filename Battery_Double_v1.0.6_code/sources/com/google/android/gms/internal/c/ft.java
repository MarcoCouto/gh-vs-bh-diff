package com.google.android.gms.internal.c;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ServiceInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import com.google.android.gms.common.b.c;
import com.google.android.gms.common.h;
import com.google.android.gms.common.internal.a.b.a;
import com.google.android.gms.common.internal.aa;
import com.google.android.gms.common.util.f;
import com.google.android.gms.measurement.AppMeasurement;
import com.google.android.gms.measurement.AppMeasurement.e;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import javax.security.auth.x500.X500Principal;

public final class ft extends da {

    /* renamed from: a reason: collision with root package name */
    private static final String[] f4086a = {"firebase_", "google_", "ga_"};

    /* renamed from: b reason: collision with root package name */
    private SecureRandom f4087b;
    private final AtomicLong c = new AtomicLong(0);
    private int d;
    private Integer e = null;

    ft(ce ceVar) {
        super(ceVar);
    }

    public static gh a(gg ggVar, String str) {
        gh[] ghVarArr;
        for (gh ghVar : ggVar.c) {
            if (ghVar.c.equals(str)) {
                return ghVar;
            }
        }
        return null;
    }

    private static Object a(int i, Object obj, boolean z) {
        if (obj == null) {
            return null;
        }
        if ((obj instanceof Long) || (obj instanceof Double)) {
            return obj;
        }
        if (obj instanceof Integer) {
            return Long.valueOf((long) ((Integer) obj).intValue());
        }
        if (obj instanceof Byte) {
            return Long.valueOf((long) ((Byte) obj).byteValue());
        }
        if (obj instanceof Short) {
            return Long.valueOf((long) ((Short) obj).shortValue());
        }
        if (obj instanceof Boolean) {
            return Long.valueOf(((Boolean) obj).booleanValue() ? 1 : 0);
        } else if (obj instanceof Float) {
            return Double.valueOf(((Float) obj).doubleValue());
        } else {
            if ((obj instanceof String) || (obj instanceof Character) || (obj instanceof CharSequence)) {
                return a(String.valueOf(obj), i, z);
            }
            return null;
        }
    }

    public static String a(String str, int i, boolean z) {
        if (str.codePointCount(0, str.length()) <= i) {
            return str;
        }
        if (z) {
            return String.valueOf(str.substring(0, str.offsetByCodePoints(0, i))).concat("...");
        }
        return null;
    }

    public static String a(String str, String[] strArr, String[] strArr2) {
        aa.a(strArr);
        aa.a(strArr2);
        int min = Math.min(strArr.length, strArr2.length);
        for (int i = 0; i < min; i++) {
            if (b(str, strArr[i])) {
                return strArr2[i];
            }
        }
        return null;
    }

    private static void a(Bundle bundle, Object obj) {
        aa.a(bundle);
        if (obj == null) {
            return;
        }
        if ((obj instanceof String) || (obj instanceof CharSequence)) {
            bundle.putLong("_el", (long) String.valueOf(obj).length());
        }
    }

    public static boolean a(Context context, String str) {
        try {
            PackageManager packageManager = context.getPackageManager();
            if (packageManager == null) {
                return false;
            }
            ServiceInfo serviceInfo = packageManager.getServiceInfo(new ComponentName(context, str), 0);
            return serviceInfo != null && serviceInfo.enabled;
        } catch (NameNotFoundException e2) {
            return false;
        }
    }

    public static boolean a(Intent intent) {
        String stringExtra = intent.getStringExtra("android.intent.extra.REFERRER_NAME");
        return "android-app://com.google.android.googlequicksearchbox/https/www.google.com".equals(stringExtra) || "https://www.google.com".equals(stringExtra) || "android-app://com.google.appcrawler".equals(stringExtra);
    }

    private static boolean a(Bundle bundle, int i) {
        if (bundle.getLong("_err") != 0) {
            return false;
        }
        bundle.putLong("_err", (long) i);
        return true;
    }

    static boolean a(an anVar, s sVar) {
        aa.a(anVar);
        aa.a(sVar);
        return !TextUtils.isEmpty(sVar.f4128b);
    }

    static boolean a(String str) {
        aa.a(str);
        return str.charAt(0) != '_' || str.equals("_ep");
    }

    private final boolean a(String str, String str2, int i, Object obj, boolean z) {
        Parcelable[] parcelableArr;
        if (obj == null || (obj instanceof Long) || (obj instanceof Float) || (obj instanceof Integer) || (obj instanceof Byte) || (obj instanceof Short) || (obj instanceof Boolean) || (obj instanceof Double)) {
            return true;
        }
        if ((obj instanceof String) || (obj instanceof Character) || (obj instanceof CharSequence)) {
            String valueOf = String.valueOf(obj);
            if (valueOf.codePointCount(0, valueOf.length()) <= i) {
                return true;
            }
            q().y().a("Value is too long; discarded. Value kind, name, value length", str, str2, Integer.valueOf(valueOf.length()));
            return false;
        } else if ((obj instanceof Bundle) && z) {
            return true;
        } else {
            if ((obj instanceof Parcelable[]) && z) {
                for (Parcelable parcelable : (Parcelable[]) obj) {
                    if (!(parcelable instanceof Bundle)) {
                        q().y().a("All Parcelable[] elements must be of type Bundle. Value type, name", parcelable.getClass(), str2);
                        return false;
                    }
                }
                return true;
            } else if (!(obj instanceof ArrayList) || !z) {
                return false;
            } else {
                ArrayList arrayList = (ArrayList) obj;
                int size = arrayList.size();
                int i2 = 0;
                while (i2 < size) {
                    Object obj2 = arrayList.get(i2);
                    i2++;
                    if (!(obj2 instanceof Bundle)) {
                        q().y().a("All ArrayList elements must be of type Bundle. Value type, name", obj2.getClass(), str2);
                        return false;
                    }
                }
                return true;
            }
        }
    }

    public static boolean a(long[] jArr, int i) {
        return i < (jArr.length << 6) && (jArr[i / 64] & (1 << (i % 64))) != 0;
    }

    static byte[] a(Parcelable parcelable) {
        if (parcelable == null) {
            return null;
        }
        Parcel obtain = Parcel.obtain();
        try {
            parcelable.writeToParcel(obtain, 0);
            return obtain.marshall();
        } finally {
            obtain.recycle();
        }
    }

    public static long[] a(BitSet bitSet) {
        int length = (bitSet.length() + 63) / 64;
        long[] jArr = new long[length];
        int i = 0;
        while (i < length) {
            jArr[i] = 0;
            int i2 = 0;
            while (i2 < 64 && (i << 6) + i2 < bitSet.length()) {
                if (bitSet.get((i << 6) + i2)) {
                    jArr[i] = jArr[i] | (1 << i2);
                }
                i2++;
            }
            i++;
        }
        return jArr;
    }

    public static Bundle[] a(Object obj) {
        if (obj instanceof Bundle) {
            return new Bundle[]{(Bundle) obj};
        } else if (obj instanceof Parcelable[]) {
            return (Bundle[]) Arrays.copyOf((Parcelable[]) obj, ((Parcelable[]) obj).length, Bundle[].class);
        } else {
            if (!(obj instanceof ArrayList)) {
                return null;
            }
            ArrayList arrayList = (ArrayList) obj;
            return (Bundle[]) arrayList.toArray(new Bundle[arrayList.size()]);
        }
    }

    static gh[] a(gh[] ghVarArr, String str, Object obj) {
        int length = ghVarArr.length;
        int i = 0;
        while (i < length) {
            gh ghVar = ghVarArr[i];
            if (str.equals(ghVar.c)) {
                ghVar.e = null;
                ghVar.d = null;
                ghVar.f = null;
                if (obj instanceof Long) {
                    ghVar.e = (Long) obj;
                    return ghVarArr;
                } else if (obj instanceof String) {
                    ghVar.d = (String) obj;
                    return ghVarArr;
                } else if (!(obj instanceof Double)) {
                    return ghVarArr;
                } else {
                    ghVar.f = (Double) obj;
                    return ghVarArr;
                }
            } else {
                i++;
            }
        }
        gh[] ghVarArr2 = new gh[(ghVarArr.length + 1)];
        System.arraycopy(ghVarArr, 0, ghVarArr2, 0, ghVarArr.length);
        gh ghVar2 = new gh();
        ghVar2.c = str;
        if (obj instanceof Long) {
            ghVar2.e = (Long) obj;
        } else if (obj instanceof String) {
            ghVar2.d = (String) obj;
        } else if (obj instanceof Double) {
            ghVar2.f = (Double) obj;
        }
        ghVarArr2[ghVarArr.length] = ghVar2;
        return ghVarArr2;
    }

    public static Object b(gg ggVar, String str) {
        gh a2 = a(ggVar, str);
        if (a2 != null) {
            if (a2.d != null) {
                return a2.d;
            }
            if (a2.e != null) {
                return a2.e;
            }
            if (a2.f != null) {
                return a2.f;
            }
        }
        return null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0033 A[Catch:{ IOException | ClassNotFoundException -> 0x003c }] */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0038 A[Catch:{ IOException | ClassNotFoundException -> 0x003c }] */
    public static Object b(Object obj) {
        ObjectOutputStream objectOutputStream;
        ObjectInputStream objectInputStream;
        if (obj == null) {
            return null;
        }
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
            try {
                objectOutputStream.writeObject(obj);
                objectOutputStream.flush();
                objectInputStream = new ObjectInputStream(new ByteArrayInputStream(byteArrayOutputStream.toByteArray()));
                try {
                    Object readObject = objectInputStream.readObject();
                    try {
                        objectOutputStream.close();
                        objectInputStream.close();
                        return readObject;
                    } catch (IOException | ClassNotFoundException e2) {
                        return null;
                    }
                } catch (Throwable th) {
                    th = th;
                }
            } catch (Throwable th2) {
                th = th2;
                objectInputStream = null;
                if (objectOutputStream != null) {
                    objectOutputStream.close();
                }
                if (objectInputStream != null) {
                    objectInputStream.close();
                }
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            objectInputStream = null;
            objectOutputStream = null;
            if (objectOutputStream != null) {
            }
            if (objectInputStream != null) {
            }
            throw th;
        }
    }

    public static boolean b(String str, String str2) {
        if (str == null && str2 == null) {
            return true;
        }
        if (str == null) {
            return false;
        }
        return str.equals(str2);
    }

    static long c(byte[] bArr) {
        int i = 0;
        aa.a(bArr);
        aa.a(bArr.length > 0);
        long j = 0;
        int length = bArr.length - 1;
        while (length >= 0 && length >= bArr.length - 8) {
            j += (((long) bArr[length]) & 255) << i;
            i += 8;
            length--;
        }
        return j;
    }

    private final boolean c(Context context, String str) {
        X500Principal x500Principal = new X500Principal("CN=Android Debug,O=Android,C=US");
        try {
            PackageInfo b2 = c.b(context).b(str, 64);
            if (!(b2 == null || b2.signatures == null || b2.signatures.length <= 0)) {
                return ((X509Certificate) CertificateFactory.getInstance("X.509").generateCertificate(new ByteArrayInputStream(b2.signatures[0].toByteArray()))).getSubjectX500Principal().equals(x500Principal);
            }
        } catch (CertificateException e2) {
            q().v().a("Error obtaining certificate", e2);
        } catch (NameNotFoundException e3) {
            q().v().a("Package name not found", e3);
        }
        return true;
    }

    private final boolean c(String str, String str2) {
        if (str2 == null) {
            q().v().a("Name is required and can't be null. Type", str);
            return false;
        } else if (str2.length() == 0) {
            q().v().a("Name is required and can't be empty. Type", str);
            return false;
        } else {
            int codePointAt = str2.codePointAt(0);
            if (Character.isLetter(codePointAt) || codePointAt == 95) {
                int length = str2.length();
                int charCount = Character.charCount(codePointAt);
                while (charCount < length) {
                    int codePointAt2 = str2.codePointAt(charCount);
                    if (codePointAt2 == 95 || Character.isLetterOrDigit(codePointAt2)) {
                        charCount += Character.charCount(codePointAt2);
                    } else {
                        q().v().a("Name must consist of letters, digits or _ (underscores). Type, name", str, str2);
                        return false;
                    }
                }
                return true;
            }
            q().v().a("Name must start with a letter or _ (underscore). Type, name", str, str2);
            return false;
        }
    }

    static MessageDigest f(String str) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= 2) {
                return null;
            }
            try {
                MessageDigest instance = MessageDigest.getInstance(str);
                if (instance != null) {
                    return instance;
                }
                i = i2 + 1;
            } catch (NoSuchAlgorithmException e2) {
            }
        }
    }

    public static boolean h(String str) {
        return !TextUtils.isEmpty(str) && str.startsWith("_");
    }

    static boolean j(String str) {
        return str != null && str.matches("(\\+|-)?([0-9]+\\.?[0-9]*|[0-9]*\\.?[0-9]+)") && str.length() <= 310;
    }

    static boolean k(String str) {
        aa.a(str);
        char c2 = 65535;
        switch (str.hashCode()) {
            case 94660:
                if (str.equals("_in")) {
                    c2 = 0;
                    break;
                }
                break;
            case 95025:
                if (str.equals("_ug")) {
                    c2 = 2;
                    break;
                }
                break;
            case 95027:
                if (str.equals("_ui")) {
                    c2 = 1;
                    break;
                }
                break;
        }
        switch (c2) {
            case 0:
            case 1:
            case 2:
                return true;
            default:
                return false;
        }
    }

    private static int l(String str) {
        if ("_ldl".equals(str)) {
            return 2048;
        }
        return "_id".equals(str) ? 256 : 36;
    }

    public final Bundle a(Uri uri) {
        String str;
        String str2;
        String str3;
        String str4;
        Bundle bundle = null;
        if (uri != null) {
            try {
                if (uri.isHierarchical()) {
                    str4 = uri.getQueryParameter("utm_campaign");
                    str3 = uri.getQueryParameter("utm_source");
                    str2 = uri.getQueryParameter("utm_medium");
                    str = uri.getQueryParameter("gclid");
                } else {
                    str = null;
                    str2 = null;
                    str3 = null;
                    str4 = null;
                }
                if (!TextUtils.isEmpty(str4) || !TextUtils.isEmpty(str3) || !TextUtils.isEmpty(str2) || !TextUtils.isEmpty(str)) {
                    bundle = new Bundle();
                    if (!TextUtils.isEmpty(str4)) {
                        bundle.putString("campaign", str4);
                    }
                    if (!TextUtils.isEmpty(str3)) {
                        bundle.putString("source", str3);
                    }
                    if (!TextUtils.isEmpty(str2)) {
                        bundle.putString("medium", str2);
                    }
                    if (!TextUtils.isEmpty(str)) {
                        bundle.putString("gclid", str);
                    }
                    String queryParameter = uri.getQueryParameter("utm_term");
                    if (!TextUtils.isEmpty(queryParameter)) {
                        bundle.putString("term", queryParameter);
                    }
                    String queryParameter2 = uri.getQueryParameter("utm_content");
                    if (!TextUtils.isEmpty(queryParameter2)) {
                        bundle.putString("content", queryParameter2);
                    }
                    String queryParameter3 = uri.getQueryParameter("aclid");
                    if (!TextUtils.isEmpty(queryParameter3)) {
                        bundle.putString("aclid", queryParameter3);
                    }
                    String queryParameter4 = uri.getQueryParameter("cp1");
                    if (!TextUtils.isEmpty(queryParameter4)) {
                        bundle.putString("cp1", queryParameter4);
                    }
                    String queryParameter5 = uri.getQueryParameter("anid");
                    if (!TextUtils.isEmpty(queryParameter5)) {
                        bundle.putString("anid", queryParameter5);
                    }
                }
            } catch (UnsupportedOperationException e2) {
                q().y().a("Install referrer url isn't a hierarchical URI", e2);
            }
        }
        return bundle;
    }

    /* access modifiers changed from: 0000 */
    public final Bundle a(Bundle bundle) {
        Bundle bundle2 = new Bundle();
        if (bundle != null) {
            for (String str : bundle.keySet()) {
                Object a2 = a(str, bundle.get(str));
                if (a2 == null) {
                    q().y().a("Param value can't be null", m().b(str));
                } else {
                    a(bundle2, str, a2);
                }
            }
        }
        return bundle2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:47:0x00ba  */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x0133  */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x016f  */
    public final Bundle a(String str, Bundle bundle, List<String> list, boolean z, boolean z2) {
        int i;
        int i2;
        boolean z3;
        int size;
        if (bundle == null) {
            return null;
        }
        Bundle bundle2 = new Bundle(bundle);
        int i3 = 0;
        for (String str2 : bundle.keySet()) {
            int i4 = 0;
            if (list == null || !list.contains(str2)) {
                if (z) {
                    i4 = !a("event param", str2) ? 3 : !a("event param", (String[]) null, str2) ? 14 : !a("event param", 40, str2) ? 3 : 0;
                }
                if (i4 == 0) {
                    i4 = !c("event param", str2) ? 3 : !a("event param", (String[]) null, str2) ? 14 : !a("event param", 40, str2) ? 3 : 0;
                }
            }
            if (i4 != 0) {
                if (a(bundle2, i4)) {
                    bundle2.putString("_ev", a(str2, 40, true));
                    if (i4 == 3) {
                        a(bundle2, (Object) str2);
                    }
                }
                bundle2.remove(str2);
            } else {
                Object obj = bundle.get(str2);
                c();
                if (z2) {
                    String str3 = "param";
                    if (obj instanceof Parcelable[]) {
                        size = ((Parcelable[]) obj).length;
                    } else if (obj instanceof ArrayList) {
                        size = ((ArrayList) obj).size();
                    } else {
                        z3 = true;
                        if (!z3) {
                            i = 17;
                            if (i != 0 || "_ev".equals(str2)) {
                                if (!a(str2)) {
                                    i2 = i3 + 1;
                                    if (i2 > 25) {
                                        q().v().a("Event can't contain more than 25 params", m().a(str), m().a(bundle));
                                        a(bundle2, 5);
                                        bundle2.remove(str2);
                                        i3 = i2;
                                    }
                                } else {
                                    i2 = i3;
                                }
                                i3 = i2;
                            } else {
                                if (a(bundle2, i)) {
                                    bundle2.putString("_ev", a(str2, 40, true));
                                    a(bundle2, bundle.get(str2));
                                }
                                bundle2.remove(str2);
                            }
                        }
                    }
                    if (size > 1000) {
                        q().y().a("Parameter array is too long; discarded. Value kind, name, array length", str3, str2, Integer.valueOf(size));
                        z3 = false;
                    } else {
                        z3 = true;
                    }
                    if (!z3) {
                    }
                }
                i = ((!s().f(f().w()) || !h(str)) && !h(str2)) ? a("param", str2, 100, obj, z2) : a("param", str2, 256, obj, z2) ? 0 : 4;
                if (i != 0) {
                }
                if (!a(str2)) {
                }
                i3 = i2;
            }
        }
        return bundle2;
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: 0000 */
    public final <T extends Parcelable> T a(byte[] bArr, Creator<T> creator) {
        if (bArr == null) {
            return null;
        }
        Parcel obtain = Parcel.obtain();
        try {
            obtain.unmarshall(bArr, 0, bArr.length);
            obtain.setDataPosition(0);
            T t = (Parcelable) creator.createFromParcel(obtain);
            obtain.recycle();
            return t;
        } catch (a e2) {
            q().v().a("Failed to load parcelable from buffer");
            obtain.recycle();
            return null;
        } catch (Throwable th) {
            obtain.recycle();
            throw th;
        }
    }

    /* access modifiers changed from: 0000 */
    public final an a(String str, Bundle bundle, String str2, long j, boolean z, boolean z2) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        if (b(str) != 0) {
            q().v().a("Invalid conditional property event name", m().c(str));
            throw new IllegalArgumentException();
        }
        Bundle bundle2 = bundle != null ? new Bundle(bundle) : new Bundle();
        bundle2.putString("_o", str2);
        return new an(str, new ak(a(a(str, bundle2, f.a("_o"), false, false))), str2, j);
    }

    public final Object a(String str, Object obj) {
        int i = 256;
        if ("_ev".equals(str)) {
            return a(256, obj, true);
        }
        if (!h(str)) {
            i = 100;
        }
        return a(i, obj, false);
    }

    public final /* bridge */ /* synthetic */ void a() {
        super.a();
    }

    public final void a(int i, String str, String str2, int i2) {
        a((String) null, i, str, str2, i2);
    }

    public final void a(Bundle bundle, String str, Object obj) {
        if (bundle != null) {
            if (obj instanceof Long) {
                bundle.putLong(str, ((Long) obj).longValue());
            } else if (obj instanceof String) {
                bundle.putString(str, String.valueOf(obj));
            } else if (obj instanceof Double) {
                bundle.putDouble(str, ((Double) obj).doubleValue());
            } else if (str != null) {
                q().z().a("Not putting event parameter. Invalid value type. name, type", m().b(str), obj != null ? obj.getClass().getSimpleName() : null);
            }
        }
    }

    public final void a(gh ghVar, Object obj) {
        aa.a(obj);
        ghVar.d = null;
        ghVar.e = null;
        ghVar.f = null;
        if (obj instanceof String) {
            ghVar.d = (String) obj;
        } else if (obj instanceof Long) {
            ghVar.e = (Long) obj;
        } else if (obj instanceof Double) {
            ghVar.f = (Double) obj;
        } else {
            q().v().a("Ignoring invalid (type) event param value", obj);
        }
    }

    public final void a(gl glVar, Object obj) {
        aa.a(obj);
        glVar.e = null;
        glVar.f = null;
        glVar.g = null;
        if (obj instanceof String) {
            glVar.e = (String) obj;
        } else if (obj instanceof Long) {
            glVar.f = (Long) obj;
        } else if (obj instanceof Double) {
            glVar.g = (Double) obj;
        } else {
            q().v().a("Ignoring invalid (type) user attribute value", obj);
        }
    }

    public final void a(String str, int i, String str2, String str3, int i2) {
        Bundle bundle = new Bundle();
        a(bundle, i);
        if (!TextUtils.isEmpty(str2)) {
            bundle.putString(str2, str3);
        }
        if (i == 6 || i == 7 || i == 2) {
            bundle.putLong("_el", (long) i2);
        }
        this.q.h().a("auto", "_err", bundle);
    }

    public final boolean a(long j, long j2) {
        return j == 0 || j2 <= 0 || Math.abs(j().a() - j) > j2;
    }

    /* access modifiers changed from: 0000 */
    public final boolean a(String str, int i, String str2) {
        if (str2 == null) {
            q().v().a("Name is required and can't be null. Type", str);
            return false;
        } else if (str2.codePointCount(0, str2.length()) <= i) {
            return true;
        } else {
            q().v().a("Name is too long. Type, maximum supported length, name", str, Integer.valueOf(i), str2);
            return false;
        }
    }

    /* access modifiers changed from: 0000 */
    public final boolean a(String str, String str2) {
        if (str2 == null) {
            q().v().a("Name is required and can't be null. Type", str);
            return false;
        } else if (str2.length() == 0) {
            q().v().a("Name is required and can't be empty. Type", str);
            return false;
        } else {
            int codePointAt = str2.codePointAt(0);
            if (!Character.isLetter(codePointAt)) {
                q().v().a("Name must start with a letter. Type, name", str, str2);
                return false;
            }
            int length = str2.length();
            int charCount = Character.charCount(codePointAt);
            while (charCount < length) {
                int codePointAt2 = str2.codePointAt(charCount);
                if (codePointAt2 == 95 || Character.isLetterOrDigit(codePointAt2)) {
                    charCount += Character.charCount(codePointAt2);
                } else {
                    q().v().a("Name must consist of letters, digits or _ (underscores). Type, name", str, str2);
                    return false;
                }
            }
            return true;
        }
    }

    /* access modifiers changed from: 0000 */
    public final boolean a(String str, String[] strArr, String str2) {
        boolean z;
        boolean z2;
        if (str2 == null) {
            q().v().a("Name is required and can't be null. Type", str);
            return false;
        }
        aa.a(str2);
        int i = 0;
        while (true) {
            if (i >= f4086a.length) {
                z = false;
                break;
            } else if (str2.startsWith(f4086a[i])) {
                z = true;
                break;
            } else {
                i++;
            }
        }
        if (z) {
            q().v().a("Name starts with reserved prefix. Type, name", str, str2);
            return false;
        }
        if (strArr != null) {
            aa.a(strArr);
            int i2 = 0;
            while (true) {
                if (i2 >= strArr.length) {
                    z2 = false;
                    break;
                } else if (b(str2, strArr[i2])) {
                    z2 = true;
                    break;
                } else {
                    i2++;
                }
            }
            if (z2) {
                q().v().a("Name is reserved. Type, name", str, str2);
                return false;
            }
        }
        return true;
    }

    public final byte[] a(gi giVar) {
        try {
            byte[] bArr = new byte[giVar.d()];
            b a2 = b.a(bArr, 0, bArr.length);
            giVar.a(a2);
            a2.a();
            return bArr;
        } catch (IOException e2) {
            q().v().a("Data loss. Failed to serialize batch", e2);
            return null;
        }
    }

    public final byte[] a(byte[] bArr) throws IOException {
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            GZIPOutputStream gZIPOutputStream = new GZIPOutputStream(byteArrayOutputStream);
            gZIPOutputStream.write(bArr);
            gZIPOutputStream.close();
            byteArrayOutputStream.close();
            return byteArrayOutputStream.toByteArray();
        } catch (IOException e2) {
            q().v().a("Failed to gzip content", e2);
            throw e2;
        }
    }

    public final int b(String str) {
        if (!c("event", str)) {
            return 2;
        }
        if (!a("event", AppMeasurement.a.f4138a, str)) {
            return 13;
        }
        return a("event", 40, str) ? 0 : 2;
    }

    public final int b(String str, Object obj) {
        return "_ldl".equals(str) ? a("user property referrer", str, l(str), obj, false) : a("user property", str, l(str), obj, false) ? 0 : 7;
    }

    /* access modifiers changed from: 0000 */
    public final long b(Context context, String str) {
        c();
        aa.a(context);
        aa.a(str);
        PackageManager packageManager = context.getPackageManager();
        MessageDigest f = f("MD5");
        if (f == null) {
            q().v().a("Could not get MD5 instance");
            return -1;
        }
        if (packageManager != null) {
            try {
                if (!c(context, str)) {
                    PackageInfo b2 = c.b(context).b(k().getPackageName(), 64);
                    if (b2.signatures != null && b2.signatures.length > 0) {
                        return c(f.digest(b2.signatures[0].toByteArray()));
                    }
                    q().y().a("Could not get signatures");
                    return -1;
                }
            } catch (NameNotFoundException e2) {
                q().v().a("Package name not found", e2);
            }
        }
        return 0;
    }

    public final /* bridge */ /* synthetic */ void b() {
        super.b();
    }

    public final byte[] b(byte[] bArr) throws IOException {
        try {
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bArr);
            GZIPInputStream gZIPInputStream = new GZIPInputStream(byteArrayInputStream);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            byte[] bArr2 = new byte[1024];
            while (true) {
                int read = gZIPInputStream.read(bArr2);
                if (read > 0) {
                    byteArrayOutputStream.write(bArr2, 0, read);
                } else {
                    gZIPInputStream.close();
                    byteArrayInputStream.close();
                    return byteArrayOutputStream.toByteArray();
                }
            }
        } catch (IOException e2) {
            q().v().a("Failed to ungzip content", e2);
            throw e2;
        }
    }

    public final int c(String str) {
        if (!a("user property", str)) {
            return 6;
        }
        if (!a("user property", e.f4142a, str)) {
            return 15;
        }
        return a("user property", 24, str) ? 0 : 6;
    }

    public final Object c(String str, Object obj) {
        return "_ldl".equals(str) ? a(l(str), obj, true) : a(l(str), obj, false);
    }

    public final /* bridge */ /* synthetic */ void c() {
        super.c();
    }

    public final int d(String str) {
        if (!c("user property", str)) {
            return 6;
        }
        if (!a("user property", e.f4142a, str)) {
            return 15;
        }
        return a("user property", 24, str) ? 0 : 6;
    }

    public final /* bridge */ /* synthetic */ n d() {
        return super.d();
    }

    public final /* bridge */ /* synthetic */ dd e() {
        return super.e();
    }

    public final boolean e(String str) {
        if (TextUtils.isEmpty(str)) {
            q().v().a("Missing google_app_id. Firebase Analytics disabled. See https://goo.gl/NAOOOI");
            return false;
        }
        aa.a(str);
        if (str.matches("^1:\\d+:android:[a-f0-9]+$")) {
            return true;
        }
        q().v().a("Invalid google_app_id. Firebase Analytics disabled. See https://goo.gl/NAOOOI. provided id", str);
        return false;
    }

    public final /* bridge */ /* synthetic */ au f() {
        return super.f();
    }

    public final /* bridge */ /* synthetic */ ah g() {
        return super.g();
    }

    public final boolean g(String str) {
        c();
        if (c.b(k()).a(str) == 0) {
            return true;
        }
        q().B().a("Permission not granted", str);
        return false;
    }

    public final /* bridge */ /* synthetic */ eb h() {
        return super.h();
    }

    public final /* bridge */ /* synthetic */ dy i() {
        return super.i();
    }

    public final boolean i(String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        return s().x().equals(str);
    }

    public final /* bridge */ /* synthetic */ com.google.android.gms.common.util.e j() {
        return super.j();
    }

    public final /* bridge */ /* synthetic */ Context k() {
        return super.k();
    }

    public final /* bridge */ /* synthetic */ av l() {
        return super.l();
    }

    public final /* bridge */ /* synthetic */ ax m() {
        return super.m();
    }

    public final /* bridge */ /* synthetic */ ft n() {
        return super.n();
    }

    public final /* bridge */ /* synthetic */ fa o() {
        return super.o();
    }

    public final /* bridge */ /* synthetic */ bz p() {
        return super.p();
    }

    public final /* bridge */ /* synthetic */ az q() {
        return super.q();
    }

    public final /* bridge */ /* synthetic */ bk r() {
        return super.r();
    }

    public final /* bridge */ /* synthetic */ y s() {
        return super.s();
    }

    /* access modifiers changed from: protected */
    public final boolean t() {
        return true;
    }

    public final long v() {
        long andIncrement;
        if (this.c.get() == 0) {
            synchronized (this.c) {
                long nextLong = new Random(System.nanoTime() ^ j().a()).nextLong();
                int i = this.d + 1;
                this.d = i;
                andIncrement = nextLong + ((long) i);
            }
        } else {
            synchronized (this.c) {
                this.c.compareAndSet(-1, 1);
                andIncrement = this.c.getAndIncrement();
            }
        }
        return andIncrement;
    }

    /* access modifiers changed from: protected */
    public final void v_() {
        c();
        SecureRandom secureRandom = new SecureRandom();
        long nextLong = secureRandom.nextLong();
        if (nextLong == 0) {
            nextLong = secureRandom.nextLong();
            if (nextLong == 0) {
                q().y().a("Utils falling back to Random for random id");
            }
        }
        this.c.set(nextLong);
    }

    /* access modifiers changed from: 0000 */
    public final SecureRandom w() {
        c();
        if (this.f4087b == null) {
            this.f4087b = new SecureRandom();
        }
        return this.f4087b;
    }

    public final int x() {
        if (this.e == null) {
            this.e = Integer.valueOf(h.b().b(k()) / 1000);
        }
        return this.e.intValue();
    }
}
