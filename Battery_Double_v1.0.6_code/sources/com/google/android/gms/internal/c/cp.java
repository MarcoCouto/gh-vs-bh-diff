package com.google.android.gms.internal.c;

import java.util.List;
import java.util.concurrent.Callable;

final class cp implements Callable<List<w>> {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ String f3942a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ String f3943b;
    private final /* synthetic */ String c;
    private final /* synthetic */ cg d;

    cp(cg cgVar, String str, String str2, String str3) {
        this.d = cgVar;
        this.f3942a = str;
        this.f3943b = str2;
        this.c = str3;
    }

    public final /* synthetic */ Object call() throws Exception {
        this.d.f3924a.H();
        return this.d.f3924a.D().b(this.f3942a, this.f3943b, this.c);
    }
}
