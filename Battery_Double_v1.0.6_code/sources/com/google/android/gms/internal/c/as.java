package com.google.android.gms.internal.c;

import android.os.Parcel;
import android.os.RemoteException;
import java.util.List;

public abstract class as extends gn implements ar {
    public as() {
        super("com.google.android.gms.measurement.internal.IMeasurementService");
    }

    /* access modifiers changed from: protected */
    public final boolean a(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        switch (i) {
            case 1:
                a((an) go.a(parcel, an.CREATOR), (s) go.a(parcel, s.CREATOR));
                parcel2.writeNoException();
                break;
            case 2:
                a((fq) go.a(parcel, fq.CREATOR), (s) go.a(parcel, s.CREATOR));
                parcel2.writeNoException();
                break;
            case 4:
                a((s) go.a(parcel, s.CREATOR));
                parcel2.writeNoException();
                break;
            case 5:
                a((an) go.a(parcel, an.CREATOR), parcel.readString(), parcel.readString());
                parcel2.writeNoException();
                break;
            case 6:
                b((s) go.a(parcel, s.CREATOR));
                parcel2.writeNoException();
                break;
            case 7:
                List a2 = a((s) go.a(parcel, s.CREATOR), go.a(parcel));
                parcel2.writeNoException();
                parcel2.writeTypedList(a2);
                break;
            case 9:
                byte[] a3 = a((an) go.a(parcel, an.CREATOR), parcel.readString());
                parcel2.writeNoException();
                parcel2.writeByteArray(a3);
                break;
            case 10:
                a(parcel.readLong(), parcel.readString(), parcel.readString(), parcel.readString());
                parcel2.writeNoException();
                break;
            case 11:
                String c = c((s) go.a(parcel, s.CREATOR));
                parcel2.writeNoException();
                parcel2.writeString(c);
                break;
            case 12:
                a((w) go.a(parcel, w.CREATOR), (s) go.a(parcel, s.CREATOR));
                parcel2.writeNoException();
                break;
            case 13:
                a((w) go.a(parcel, w.CREATOR));
                parcel2.writeNoException();
                break;
            case 14:
                List a4 = a(parcel.readString(), parcel.readString(), go.a(parcel), (s) go.a(parcel, s.CREATOR));
                parcel2.writeNoException();
                parcel2.writeTypedList(a4);
                break;
            case 15:
                List a5 = a(parcel.readString(), parcel.readString(), parcel.readString(), go.a(parcel));
                parcel2.writeNoException();
                parcel2.writeTypedList(a5);
                break;
            case 16:
                List a6 = a(parcel.readString(), parcel.readString(), (s) go.a(parcel, s.CREATOR));
                parcel2.writeNoException();
                parcel2.writeTypedList(a6);
                break;
            case 17:
                List a7 = a(parcel.readString(), parcel.readString(), parcel.readString());
                parcel2.writeNoException();
                parcel2.writeTypedList(a7);
                break;
            case 18:
                d((s) go.a(parcel, s.CREATOR));
                parcel2.writeNoException();
                break;
            default:
                return false;
        }
        return true;
    }
}
