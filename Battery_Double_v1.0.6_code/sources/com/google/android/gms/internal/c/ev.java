package com.google.android.gms.internal.c;

import android.annotation.TargetApi;
import android.app.job.JobParameters;
import android.content.Context;
import android.content.Intent;
import android.os.Build.VERSION;
import android.os.IBinder;
import com.google.android.gms.common.internal.aa;
import com.google.android.gms.internal.c.ez;

public final class ev<T extends Context & ez> {

    /* renamed from: a reason: collision with root package name */
    private final T f4049a;

    public ev(T t) {
        aa.a(t);
        this.f4049a = t;
    }

    private final void a(Runnable runnable) {
        ce a2 = ce.a((Context) this.f4049a);
        a2.p().a((Runnable) new ey(this, a2, runnable));
    }

    public static boolean a(Context context, boolean z) {
        aa.a(context);
        return VERSION.SDK_INT >= 24 ? ft.a(context, "com.google.android.gms.measurement.AppMeasurementJobService") : ft.a(context, "com.google.android.gms.measurement.AppMeasurementService");
    }

    private final az c() {
        return ce.a((Context) this.f4049a).q();
    }

    public final int a(Intent intent, int i, int i2) {
        az q = ce.a((Context) this.f4049a).q();
        if (intent == null) {
            q.y().a("AppMeasurementService started with null intent");
        } else {
            String action = intent.getAction();
            q.C().a("Local AppMeasurementService called. startId, action", Integer.valueOf(i2), action);
            if ("com.google.android.gms.measurement.UPLOAD".equals(action)) {
                a((Runnable) new ew(this, i2, q, intent));
            }
        }
        return 2;
    }

    public final IBinder a(Intent intent) {
        if (intent == null) {
            c().v().a("onBind called with null intent");
            return null;
        }
        String action = intent.getAction();
        if ("com.google.android.gms.measurement.START".equals(action)) {
            return new cg(ce.a((Context) this.f4049a));
        }
        c().y().a("onBind received unknown action", action);
        return null;
    }

    public final void a() {
        ce.a((Context) this.f4049a).q().C().a("Local AppMeasurementService is starting up");
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ void a(int i, az azVar, Intent intent) {
        if (((ez) this.f4049a).a(i)) {
            azVar.C().a("Local AppMeasurementService processed last upload request. StartId", Integer.valueOf(i));
            c().C().a("Completed wakeful intent.");
            ((ez) this.f4049a).a(intent);
        }
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ void a(az azVar, JobParameters jobParameters) {
        azVar.C().a("AppMeasurementJobService processed last upload request.");
        ((ez) this.f4049a).a(jobParameters, false);
    }

    @TargetApi(24)
    public final boolean a(JobParameters jobParameters) {
        az q = ce.a((Context) this.f4049a).q();
        String string = jobParameters.getExtras().getString("action");
        q.C().a("Local AppMeasurementJobService called. action", string);
        if ("com.google.android.gms.measurement.UPLOAD".equals(string)) {
            a((Runnable) new ex(this, q, jobParameters));
        }
        return true;
    }

    public final void b() {
        ce.a((Context) this.f4049a).q().C().a("Local AppMeasurementService is shutting down");
    }

    public final boolean b(Intent intent) {
        if (intent == null) {
            c().v().a("onUnbind called with null intent");
        } else {
            c().C().a("onUnbind called for intent. action", intent.getAction());
        }
        return true;
    }

    public final void c(Intent intent) {
        if (intent == null) {
            c().v().a("onRebind called with null intent");
            return;
        }
        c().C().a("onRebind called. action", intent.getAction());
    }
}
