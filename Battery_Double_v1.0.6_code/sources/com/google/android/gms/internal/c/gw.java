package com.google.android.gms.internal.c;

import android.database.ContentObserver;
import android.os.Handler;

final class gw extends ContentObserver {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ gv f4100a;

    gw(gv gvVar, Handler handler) {
        this.f4100a = gvVar;
        super(null);
    }

    public final void onChange(boolean z) {
        this.f4100a.b();
        this.f4100a.d();
    }
}
