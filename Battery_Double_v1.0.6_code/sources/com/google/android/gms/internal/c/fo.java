package com.google.android.gms.internal.c;

import com.google.android.gms.common.internal.aa;
import java.util.ArrayList;
import java.util.List;

final class fo implements ad {

    /* renamed from: a reason: collision with root package name */
    gj f4080a;

    /* renamed from: b reason: collision with root package name */
    List<Long> f4081b;
    List<gg> c;
    private long d;
    private final /* synthetic */ fk e;

    private fo(fk fkVar) {
        this.e = fkVar;
    }

    /* synthetic */ fo(fk fkVar, fl flVar) {
        this(fkVar);
    }

    private static long a(gg ggVar) {
        return ((ggVar.e.longValue() / 1000) / 60) / 60;
    }

    public final void a(gj gjVar) {
        aa.a(gjVar);
        this.f4080a = gjVar;
    }

    public final boolean a(long j, gg ggVar) {
        aa.a(ggVar);
        if (this.c == null) {
            this.c = new ArrayList();
        }
        if (this.f4081b == null) {
            this.f4081b = new ArrayList();
        }
        if (this.c.size() > 0 && a((gg) this.c.get(0)) != a(ggVar)) {
            return false;
        }
        long d2 = this.d + ((long) ggVar.d());
        if (d2 >= ((long) Math.max(0, ((Integer) ap.j.b()).intValue()))) {
            return false;
        }
        this.d = d2;
        this.c.add(ggVar);
        this.f4081b.add(Long.valueOf(j));
        return this.c.size() < Math.max(1, ((Integer) ap.k.b()).intValue());
    }
}
