package com.google.android.gms.internal.c;

import java.util.List;
import java.util.Map;

final class fl implements bf {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ String f4075a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ fk f4076b;

    fl(fk fkVar, String str) {
        this.f4076b = fkVar;
        this.f4075a = str;
    }

    public final void a(String str, int i, Throwable th, byte[] bArr, Map<String, List<String>> map) {
        this.f4076b.a(i, th, bArr, this.f4075a);
    }
}
