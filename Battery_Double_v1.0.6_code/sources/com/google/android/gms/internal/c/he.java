package com.google.android.gms.internal.c;

import android.util.Log;

final class he extends gy<Boolean> {
    he(hi hiVar, String str, Boolean bool) {
        super(hiVar, str, bool, null);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(String str) {
        if (gt.f4096a.matcher(str).matches()) {
            return Boolean.valueOf(true);
        }
        if (gt.f4097b.matcher(str).matches()) {
            return Boolean.valueOf(false);
        }
        String str2 = this.f4102a;
        Log.e("PhenotypeFlag", new StringBuilder(String.valueOf(str2).length() + 28 + String.valueOf(str).length()).append("Invalid boolean value for ").append(str2).append(": ").append(str).toString());
        return null;
    }
}
