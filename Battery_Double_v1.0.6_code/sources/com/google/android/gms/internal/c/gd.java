package com.google.android.gms.internal.c;

import java.io.IOException;

public final class gd extends d<gd> {
    public Long c;
    public String d;
    public ge[] e;
    public gc[] f;
    public fw[] g;
    private Integer h;

    public gd() {
        this.c = null;
        this.d = null;
        this.h = null;
        this.e = ge.e();
        this.f = gc.e();
        this.g = fw.e();
        this.f3962a = null;
        this.f4112b = -1;
    }

    /* access modifiers changed from: protected */
    public final int a() {
        int a2 = super.a();
        if (this.c != null) {
            a2 += b.c(1, this.c.longValue());
        }
        if (this.d != null) {
            a2 += b.b(2, this.d);
        }
        if (this.h != null) {
            a2 += b.b(3, this.h.intValue());
        }
        if (this.e != null && this.e.length > 0) {
            int i = a2;
            for (ge geVar : this.e) {
                if (geVar != null) {
                    i += b.b(4, (j) geVar);
                }
            }
            a2 = i;
        }
        if (this.f != null && this.f.length > 0) {
            int i2 = a2;
            for (gc gcVar : this.f) {
                if (gcVar != null) {
                    i2 += b.b(5, (j) gcVar);
                }
            }
            a2 = i2;
        }
        if (this.g != null && this.g.length > 0) {
            for (fw fwVar : this.g) {
                if (fwVar != null) {
                    a2 += b.b(6, (j) fwVar);
                }
            }
        }
        return a2;
    }

    public final /* synthetic */ j a(a aVar) throws IOException {
        while (true) {
            int a2 = aVar.a();
            switch (a2) {
                case 0:
                    break;
                case 8:
                    this.c = Long.valueOf(aVar.e());
                    continue;
                case 18:
                    this.d = aVar.c();
                    continue;
                case 24:
                    this.h = Integer.valueOf(aVar.d());
                    continue;
                case 34:
                    int a3 = m.a(aVar, 34);
                    int length = this.e == null ? 0 : this.e.length;
                    ge[] geVarArr = new ge[(a3 + length)];
                    if (length != 0) {
                        System.arraycopy(this.e, 0, geVarArr, 0, length);
                    }
                    while (length < geVarArr.length - 1) {
                        geVarArr[length] = new ge();
                        aVar.a((j) geVarArr[length]);
                        aVar.a();
                        length++;
                    }
                    geVarArr[length] = new ge();
                    aVar.a((j) geVarArr[length]);
                    this.e = geVarArr;
                    continue;
                case 42:
                    int a4 = m.a(aVar, 42);
                    int length2 = this.f == null ? 0 : this.f.length;
                    gc[] gcVarArr = new gc[(a4 + length2)];
                    if (length2 != 0) {
                        System.arraycopy(this.f, 0, gcVarArr, 0, length2);
                    }
                    while (length2 < gcVarArr.length - 1) {
                        gcVarArr[length2] = new gc();
                        aVar.a((j) gcVarArr[length2]);
                        aVar.a();
                        length2++;
                    }
                    gcVarArr[length2] = new gc();
                    aVar.a((j) gcVarArr[length2]);
                    this.f = gcVarArr;
                    continue;
                case 50:
                    int a5 = m.a(aVar, 50);
                    int length3 = this.g == null ? 0 : this.g.length;
                    fw[] fwVarArr = new fw[(a5 + length3)];
                    if (length3 != 0) {
                        System.arraycopy(this.g, 0, fwVarArr, 0, length3);
                    }
                    while (length3 < fwVarArr.length - 1) {
                        fwVarArr[length3] = new fw();
                        aVar.a((j) fwVarArr[length3]);
                        aVar.a();
                        length3++;
                    }
                    fwVarArr[length3] = new fw();
                    aVar.a((j) fwVarArr[length3]);
                    this.g = fwVarArr;
                    continue;
                default:
                    if (!super.a(aVar, a2)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    public final void a(b bVar) throws IOException {
        if (this.c != null) {
            bVar.b(1, this.c.longValue());
        }
        if (this.d != null) {
            bVar.a(2, this.d);
        }
        if (this.h != null) {
            bVar.a(3, this.h.intValue());
        }
        if (this.e != null && this.e.length > 0) {
            for (ge geVar : this.e) {
                if (geVar != null) {
                    bVar.a(4, (j) geVar);
                }
            }
        }
        if (this.f != null && this.f.length > 0) {
            for (gc gcVar : this.f) {
                if (gcVar != null) {
                    bVar.a(5, (j) gcVar);
                }
            }
        }
        if (this.g != null && this.g.length > 0) {
            for (fw fwVar : this.g) {
                if (fwVar != null) {
                    bVar.a(6, (j) fwVar);
                }
            }
        }
        super.a(bVar);
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof gd)) {
            return false;
        }
        gd gdVar = (gd) obj;
        if (this.c == null) {
            if (gdVar.c != null) {
                return false;
            }
        } else if (!this.c.equals(gdVar.c)) {
            return false;
        }
        if (this.d == null) {
            if (gdVar.d != null) {
                return false;
            }
        } else if (!this.d.equals(gdVar.d)) {
            return false;
        }
        if (this.h == null) {
            if (gdVar.h != null) {
                return false;
            }
        } else if (!this.h.equals(gdVar.h)) {
            return false;
        }
        if (!h.a((Object[]) this.e, (Object[]) gdVar.e)) {
            return false;
        }
        if (!h.a((Object[]) this.f, (Object[]) gdVar.f)) {
            return false;
        }
        if (!h.a((Object[]) this.g, (Object[]) gdVar.g)) {
            return false;
        }
        return (this.f3962a == null || this.f3962a.b()) ? gdVar.f3962a == null || gdVar.f3962a.b() : this.f3962a.equals(gdVar.f3962a);
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = ((((((((this.h == null ? 0 : this.h.hashCode()) + (((this.d == null ? 0 : this.d.hashCode()) + (((this.c == null ? 0 : this.c.hashCode()) + ((getClass().getName().hashCode() + 527) * 31)) * 31)) * 31)) * 31) + h.a((Object[]) this.e)) * 31) + h.a((Object[]) this.f)) * 31) + h.a((Object[]) this.g)) * 31;
        if (this.f3962a != null && !this.f3962a.b()) {
            i = this.f3962a.hashCode();
        }
        return hashCode + i;
    }
}
