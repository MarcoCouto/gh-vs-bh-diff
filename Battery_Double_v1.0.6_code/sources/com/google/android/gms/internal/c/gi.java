package com.google.android.gms.internal.c;

import java.io.IOException;

public final class gi extends d<gi> {
    public gj[] c;

    public gi() {
        this.c = gj.e();
        this.f3962a = null;
        this.f4112b = -1;
    }

    /* access modifiers changed from: protected */
    public final int a() {
        int a2 = super.a();
        if (this.c != null && this.c.length > 0) {
            for (gj gjVar : this.c) {
                if (gjVar != null) {
                    a2 += b.b(1, (j) gjVar);
                }
            }
        }
        return a2;
    }

    public final /* synthetic */ j a(a aVar) throws IOException {
        while (true) {
            int a2 = aVar.a();
            switch (a2) {
                case 0:
                    break;
                case 10:
                    int a3 = m.a(aVar, 10);
                    int length = this.c == null ? 0 : this.c.length;
                    gj[] gjVarArr = new gj[(a3 + length)];
                    if (length != 0) {
                        System.arraycopy(this.c, 0, gjVarArr, 0, length);
                    }
                    while (length < gjVarArr.length - 1) {
                        gjVarArr[length] = new gj();
                        aVar.a((j) gjVarArr[length]);
                        aVar.a();
                        length++;
                    }
                    gjVarArr[length] = new gj();
                    aVar.a((j) gjVarArr[length]);
                    this.c = gjVarArr;
                    continue;
                default:
                    if (!super.a(aVar, a2)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    public final void a(b bVar) throws IOException {
        if (this.c != null && this.c.length > 0) {
            for (gj gjVar : this.c) {
                if (gjVar != null) {
                    bVar.a(1, (j) gjVar);
                }
            }
        }
        super.a(bVar);
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof gi)) {
            return false;
        }
        gi giVar = (gi) obj;
        if (!h.a((Object[]) this.c, (Object[]) giVar.c)) {
            return false;
        }
        return (this.f3962a == null || this.f3962a.b()) ? giVar.f3962a == null || giVar.f3962a.b() : this.f3962a.equals(giVar.f3962a);
    }

    public final int hashCode() {
        return ((this.f3962a == null || this.f3962a.b()) ? 0 : this.f3962a.hashCode()) + ((((getClass().getName().hashCode() + 527) * 31) + h.a((Object[]) this.c)) * 31);
    }
}
