package com.google.android.gms.internal.c;

import android.os.IBinder;
import android.os.IInterface;

public abstract class gr extends gn implements gq {
    public static gq a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.finsky.externalreferrer.IGetInstallReferrerService");
        return queryLocalInterface instanceof gq ? (gq) queryLocalInterface : new gs(iBinder);
    }
}
