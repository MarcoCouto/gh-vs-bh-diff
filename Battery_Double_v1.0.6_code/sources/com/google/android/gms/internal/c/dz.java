package com.google.android.gms.internal.c;

import android.os.Bundle;

final class dz implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ boolean f4007a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ dx f4008b;
    private final /* synthetic */ dx c;
    private final /* synthetic */ dy d;

    dz(dy dyVar, boolean z, dx dxVar, dx dxVar2) {
        this.d = dyVar;
        this.f4007a = z;
        this.f4008b = dxVar;
        this.c = dxVar2;
    }

    public final void run() {
        if (this.f4007a && this.d.f4005a != null) {
            this.d.a(this.d.f4005a);
        }
        if (this.f4008b == null || this.f4008b.c != this.c.c || !ft.b(this.f4008b.f4004b, this.c.f4004b) || !ft.b(this.f4008b.f4003a, this.c.f4003a)) {
            Bundle bundle = new Bundle();
            dy.a(this.c, bundle, true);
            if (this.f4008b != null) {
                if (this.f4008b.f4003a != null) {
                    bundle.putString("_pn", this.f4008b.f4003a);
                }
                bundle.putString("_pc", this.f4008b.f4004b);
                bundle.putLong("_pi", this.f4008b.c);
            }
            this.d.e().b("auto", "_vs", bundle);
        }
        this.d.f4005a = this.c;
        this.d.h().a(this.c);
    }
}
