package com.google.android.gms.internal.c;

import android.net.Uri;

public final class ap {
    public static aq<Long> A = aq.a("measurement.upload.refresh_blacklisted_config_interval", 604800000, 604800000);
    public static aq<Long> B = aq.a("measurement.upload.initial_upload_delay_time", 15000, 15000);
    public static aq<Long> C = aq.a("measurement.upload.retry_time", 1800000, 1800000);
    public static aq<Integer> D = aq.a("measurement.upload.retry_count", 6, 6);
    public static aq<Long> E = aq.a("measurement.upload.max_queue_time", 2419200000L, 2419200000L);
    public static aq<Integer> F = aq.a("measurement.lifetimevalue.max_currency_tracked", 4, 4);
    public static aq<Integer> G = aq.a("measurement.audience.filter_result_max_count", 200, 200);
    public static aq<Long> H = aq.a("measurement.service_client.idle_disconnect_millis", 5000, 5000);
    public static aq<Boolean> I = aq.a("measurement.test.boolean_flag", false, false);
    public static aq<String> J;
    public static aq<Long> K = aq.a("measurement.test.long_flag", -1, -1);
    public static aq<Integer> L = aq.a("measurement.test.int_flag", -2, -2);
    public static aq<Double> M = aq.a("measurement.test.double_flag", -3.0d, -3.0d);
    public static aq<Boolean> N = aq.a("measurement.lifetimevalue.user_engagement_tracking_enabled", false, false);
    public static aq<Boolean> O = aq.a("measurement.audience.complex_param_evaluation", false, false);
    public static aq<Boolean> P = aq.a("measurement.validation.internal_limits_internal_event_params", false, false);
    public static aq<Boolean> Q = aq.a("measurement.quality.unsuccessful_update_retry_counter", false, false);
    public static aq<Boolean> R = aq.a("measurement.iid.disable_on_collection_disabled", true, true);
    public static aq<Boolean> S = aq.a("measurement.app_launch.call_only_when_enabled", true, true);
    public static aq<Boolean> T = aq.a("measurement.run_on_worker_inline", true, false);
    public static aq<Boolean> U = aq.a("measurement.reset_analytics.persist_time", false, false);
    /* access modifiers changed from: private */
    public static final hi V;
    private static aq<Boolean> W = aq.a("measurement.log_third_party_store_events_enabled", false, false);
    private static aq<Boolean> X = aq.a("measurement.log_installs_enabled", false, false);
    private static aq<Boolean> Y = aq.a("measurement.log_upgrades_enabled", false, false);
    private static aq<Boolean> Z = aq.a("measurement.log_androidId_enabled", false, false);

    /* renamed from: a reason: collision with root package name */
    public static aq<Boolean> f3866a = aq.a("measurement.upload_dsid_enabled", false, false);
    private static aq<Boolean> aa = aq.a("measurement.audience.dynamic_filters", false, false);

    /* renamed from: b reason: collision with root package name */
    public static aq<String> f3867b = aq.a("measurement.log_tag", "FA", "FA-SVC");
    public static aq<Long> c = aq.a("measurement.ad_id_cache_time", 10000, 10000);
    public static aq<Long> d = aq.a("measurement.monitoring.sample_period_millis", 86400000, 86400000);
    public static aq<Long> e = aq.a("measurement.config.cache_time", 86400000, 3600000);
    public static aq<String> f;
    public static aq<String> g;
    public static aq<Integer> h = aq.a("measurement.upload.max_bundles", 100, 100);
    public static aq<Integer> i = aq.a("measurement.upload.max_batch_size", 65536, 65536);
    public static aq<Integer> j = aq.a("measurement.upload.max_bundle_size", 65536, 65536);
    public static aq<Integer> k = aq.a("measurement.upload.max_events_per_bundle", 1000, 1000);
    public static aq<Integer> l = aq.a("measurement.upload.max_events_per_day", 100000, 100000);
    public static aq<Integer> m = aq.a("measurement.upload.max_error_events_per_day", 1000, 1000);
    public static aq<Integer> n = aq.a("measurement.upload.max_public_events_per_day", 50000, 50000);
    public static aq<Integer> o = aq.a("measurement.upload.max_conversions_per_day", 500, 500);
    public static aq<Integer> p = aq.a("measurement.upload.max_realtime_events_per_day", 10, 10);
    public static aq<Integer> q = aq.a("measurement.store.max_stored_events_per_app", 100000, 100000);
    public static aq<String> r;
    public static aq<Long> s = aq.a("measurement.upload.backoff_period", 43200000, 43200000);
    public static aq<Long> t = aq.a("measurement.upload.window_interval", 3600000, 3600000);
    public static aq<Long> u = aq.a("measurement.upload.interval", 3600000, 3600000);
    public static aq<Long> v = aq.a("measurement.upload.realtime_upload_interval", 10000, 10000);
    public static aq<Long> w = aq.a("measurement.upload.debug_upload_interval", 1000, 1000);
    public static aq<Long> x = aq.a("measurement.upload.minimum_delay", 500, 500);
    public static aq<Long> y = aq.a("measurement.alarm_manager.minimum_interval", 60000, 60000);
    public static aq<Long> z = aq.a("measurement.upload.stale_data_deletion_interval", 86400000, 86400000);

    static {
        String str = "content://com.google.android.gms.phenotype/";
        String valueOf = String.valueOf(Uri.encode("com.google.android.gms.measurement"));
        V = new hi(Uri.parse(valueOf.length() != 0 ? str.concat(valueOf) : new String(str)));
        String str2 = "https";
        f = aq.a("measurement.config.url_scheme", str2, str2);
        String str3 = "app-measurement.com";
        g = aq.a("measurement.config.url_authority", str3, str3);
        String str4 = "https://app-measurement.com/a";
        r = aq.a("measurement.upload.url", str4, str4);
        String str5 = "---";
        J = aq.a("measurement.test.string_flag", str5, str5);
    }
}
