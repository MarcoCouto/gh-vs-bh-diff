package com.google.android.gms.internal.c;

import android.os.RemoteException;
import android.text.TextUtils;
import java.util.Collections;
import java.util.concurrent.atomic.AtomicReference;

final class em implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ AtomicReference f4033a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ String f4034b;
    private final /* synthetic */ String c;
    private final /* synthetic */ String d;
    private final /* synthetic */ boolean e;
    private final /* synthetic */ s f;
    private final /* synthetic */ eb g;

    em(eb ebVar, AtomicReference atomicReference, String str, String str2, String str3, boolean z, s sVar) {
        this.g = ebVar;
        this.f4033a = atomicReference;
        this.f4034b = str;
        this.c = str2;
        this.d = str3;
        this.e = z;
        this.f = sVar;
    }

    /* JADX INFO: finally extract failed */
    public final void run() {
        synchronized (this.f4033a) {
            try {
                ar d2 = this.g.f4014b;
                if (d2 == null) {
                    this.g.q().v().a("Failed to get user properties", az.a(this.f4034b), this.c, this.d);
                    this.f4033a.set(Collections.emptyList());
                    this.f4033a.notify();
                    return;
                }
                if (TextUtils.isEmpty(this.f4034b)) {
                    this.f4033a.set(d2.a(this.c, this.d, this.e, this.f));
                } else {
                    this.f4033a.set(d2.a(this.f4034b, this.c, this.d, this.e));
                }
                this.g.C();
                this.f4033a.notify();
            } catch (RemoteException e2) {
                this.g.q().v().a("Failed to get user properties", az.a(this.f4034b), this.c, e2);
                this.f4033a.set(Collections.emptyList());
                this.f4033a.notify();
            } catch (Throwable th) {
                this.f4033a.notify();
                throw th;
            }
        }
    }
}
