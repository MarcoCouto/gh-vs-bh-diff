package com.google.android.gms.internal.c;

import android.app.job.JobParameters;

final /* synthetic */ class ex implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final ev f4052a;

    /* renamed from: b reason: collision with root package name */
    private final az f4053b;
    private final JobParameters c;

    ex(ev evVar, az azVar, JobParameters jobParameters) {
        this.f4052a = evVar;
        this.f4053b = azVar;
        this.c = jobParameters;
    }

    public final void run() {
        this.f4052a.a(this.f4053b, this.c);
    }
}
