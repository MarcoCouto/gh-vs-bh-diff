package com.google.android.gms.internal.c;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.RemoteException;
import com.google.android.gms.common.a.a;
import com.google.android.gms.common.h;
import com.google.android.gms.common.internal.aa;
import com.google.android.gms.common.util.e;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public final class eb extends da {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public final ep f4013a;
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public ar f4014b;
    private volatile Boolean c;
    private final af d;
    private final ff e;
    private final List<Runnable> f = new ArrayList();
    private final af g;

    protected eb(ce ceVar) {
        super(ceVar);
        this.e = new ff(ceVar.j());
        this.f4013a = new ep(this);
        this.d = new ec(this, ceVar);
        this.g = new eh(this, ceVar);
    }

    /* access modifiers changed from: private */
    public final void C() {
        c();
        this.e.a();
        this.d.a(((Long) ap.H.b()).longValue());
    }

    /* access modifiers changed from: private */
    public final void D() {
        c();
        if (v()) {
            q().C().a("Inactivity, disconnecting from the service");
            B();
        }
    }

    /* access modifiers changed from: private */
    public final void I() {
        c();
        q().C().a("Processing queued up service tasks", Integer.valueOf(this.f.size()));
        for (Runnable run : this.f) {
            try {
                run.run();
            } catch (Exception e2) {
                q().v().a("Task exception while flushing queue", e2);
            }
        }
        this.f.clear();
        this.g.c();
    }

    private final s a(boolean z) {
        return f().a(z ? q().D() : null);
    }

    /* access modifiers changed from: private */
    public final void a(ComponentName componentName) {
        c();
        if (this.f4014b != null) {
            this.f4014b = null;
            q().C().a("Disconnected from device MeasurementService", componentName);
            c();
            z();
        }
    }

    private final void a(Runnable runnable) throws IllegalStateException {
        c();
        if (v()) {
            runnable.run();
        } else if (((long) this.f.size()) >= 1000) {
            q().v().a("Discarding data. Max runnable queue size reached");
        } else {
            this.f.add(runnable);
            this.g.a(60000);
            z();
        }
    }

    /* access modifiers changed from: 0000 */
    public final Boolean A() {
        return this.c;
    }

    public final void B() {
        c();
        F();
        try {
            a.a().a(k(), this.f4013a);
        } catch (IllegalArgumentException | IllegalStateException e2) {
        }
        this.f4014b = null;
    }

    public final /* bridge */ /* synthetic */ void a() {
        super.a();
    }

    /* access modifiers changed from: protected */
    public final void a(an anVar, String str) {
        aa.a(anVar);
        c();
        F();
        a((Runnable) new ej(this, true, l().a(anVar), anVar, a(true), str));
    }

    /* access modifiers changed from: protected */
    public final void a(ar arVar) {
        c();
        aa.a(arVar);
        this.f4014b = arVar;
        C();
        I();
    }

    /* access modifiers changed from: 0000 */
    public final void a(ar arVar, com.google.android.gms.common.internal.a.a aVar, s sVar) {
        c();
        F();
        int i = 100;
        for (int i2 = 0; i2 < 1001 && i == 100; i2++) {
            ArrayList arrayList = new ArrayList();
            List a2 = l().a(100);
            if (a2 != null) {
                arrayList.addAll(a2);
                i = a2.size();
            } else {
                i = 0;
            }
            if (aVar != null && i < 100) {
                arrayList.add(aVar);
            }
            ArrayList arrayList2 = arrayList;
            int size = arrayList2.size();
            int i3 = 0;
            while (i3 < size) {
                Object obj = arrayList2.get(i3);
                i3++;
                com.google.android.gms.common.internal.a.a aVar2 = (com.google.android.gms.common.internal.a.a) obj;
                if (aVar2 instanceof an) {
                    try {
                        arVar.a((an) aVar2, sVar);
                    } catch (RemoteException e2) {
                        q().v().a("Failed to send event to the service", e2);
                    }
                } else if (aVar2 instanceof fq) {
                    try {
                        arVar.a((fq) aVar2, sVar);
                    } catch (RemoteException e3) {
                        q().v().a("Failed to send attribute to the service", e3);
                    }
                } else if (aVar2 instanceof w) {
                    try {
                        arVar.a((w) aVar2, sVar);
                    } catch (RemoteException e4) {
                        q().v().a("Failed to send conditional property to the service", e4);
                    }
                } else {
                    q().v().a("Discarding data. Unrecognized parcel type.");
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void a(dx dxVar) {
        c();
        F();
        a((Runnable) new eg(this, dxVar));
    }

    /* access modifiers changed from: protected */
    public final void a(fq fqVar) {
        c();
        F();
        a((Runnable) new en(this, l().a(fqVar), fqVar, a(true)));
    }

    /* access modifiers changed from: protected */
    public final void a(w wVar) {
        aa.a(wVar);
        c();
        F();
        a((Runnable) new ek(this, true, l().a(wVar), new w(wVar), a(true), wVar));
    }

    public final void a(AtomicReference<String> atomicReference) {
        c();
        F();
        a((Runnable) new ee(this, atomicReference, a(false)));
    }

    /* access modifiers changed from: protected */
    public final void a(AtomicReference<List<w>> atomicReference, String str, String str2, String str3) {
        c();
        F();
        a((Runnable) new el(this, atomicReference, str, str2, str3, a(false)));
    }

    /* access modifiers changed from: protected */
    public final void a(AtomicReference<List<fq>> atomicReference, String str, String str2, String str3, boolean z) {
        c();
        F();
        a((Runnable) new em(this, atomicReference, str, str2, str3, z, a(false)));
    }

    /* access modifiers changed from: protected */
    public final void a(AtomicReference<List<fq>> atomicReference, boolean z) {
        c();
        F();
        a((Runnable) new eo(this, atomicReference, a(false), z));
    }

    public final /* bridge */ /* synthetic */ void b() {
        super.b();
    }

    public final /* bridge */ /* synthetic */ void c() {
        super.c();
    }

    public final /* bridge */ /* synthetic */ n d() {
        return super.d();
    }

    public final /* bridge */ /* synthetic */ dd e() {
        return super.e();
    }

    public final /* bridge */ /* synthetic */ au f() {
        return super.f();
    }

    public final /* bridge */ /* synthetic */ ah g() {
        return super.g();
    }

    public final /* bridge */ /* synthetic */ eb h() {
        return super.h();
    }

    public final /* bridge */ /* synthetic */ dy i() {
        return super.i();
    }

    public final /* bridge */ /* synthetic */ e j() {
        return super.j();
    }

    public final /* bridge */ /* synthetic */ Context k() {
        return super.k();
    }

    public final /* bridge */ /* synthetic */ av l() {
        return super.l();
    }

    public final /* bridge */ /* synthetic */ ax m() {
        return super.m();
    }

    public final /* bridge */ /* synthetic */ ft n() {
        return super.n();
    }

    public final /* bridge */ /* synthetic */ fa o() {
        return super.o();
    }

    public final /* bridge */ /* synthetic */ bz p() {
        return super.p();
    }

    public final /* bridge */ /* synthetic */ az q() {
        return super.q();
    }

    public final /* bridge */ /* synthetic */ bk r() {
        return super.r();
    }

    public final /* bridge */ /* synthetic */ y s() {
        return super.s();
    }

    /* access modifiers changed from: protected */
    public final boolean t() {
        return false;
    }

    public final boolean v() {
        c();
        F();
        return this.f4014b != null;
    }

    /* access modifiers changed from: protected */
    public final void w() {
        c();
        F();
        a((Runnable) new ei(this, a(true)));
    }

    /* access modifiers changed from: protected */
    public final void x() {
        c();
        F();
        s a2 = a(false);
        l().v();
        a((Runnable) new ed(this, a2));
    }

    /* access modifiers changed from: protected */
    public final void y() {
        c();
        F();
        a((Runnable) new ef(this, a(true)));
    }

    /* access modifiers changed from: 0000 */
    public final void z() {
        boolean z;
        boolean z2;
        boolean z3 = true;
        c();
        F();
        if (!v()) {
            if (this.c == null) {
                c();
                F();
                Boolean x = r().x();
                if (x == null || !x.booleanValue()) {
                    if (f().z() != 1) {
                        q().C().a("Checking service availability");
                        int b2 = h.b().b(n().k(), 12451);
                        switch (b2) {
                            case 0:
                                q().C().a("Service available");
                                z = true;
                                z2 = true;
                                break;
                            case 1:
                                q().C().a("Service missing");
                                z = true;
                                z2 = false;
                                break;
                            case 2:
                                q().B().a("Service container out of date");
                                if (n().x() >= 12600) {
                                    Boolean x2 = r().x();
                                    z2 = x2 == null || x2.booleanValue();
                                    z = false;
                                    break;
                                } else {
                                    z = true;
                                    z2 = false;
                                    break;
                                }
                            case 3:
                                q().y().a("Service disabled");
                                z = false;
                                z2 = false;
                                break;
                            case 9:
                                q().y().a("Service invalid");
                                z = false;
                                z2 = false;
                                break;
                            case 18:
                                q().y().a("Service updating");
                                z = true;
                                z2 = true;
                                break;
                            default:
                                q().y().a("Unexpected service status", Integer.valueOf(b2));
                                z = false;
                                z2 = false;
                                break;
                        }
                    } else {
                        z = true;
                        z2 = true;
                    }
                    if (z) {
                        r().a(z2);
                    }
                } else {
                    z2 = true;
                }
                this.c = Boolean.valueOf(z2);
            }
            if (this.c.booleanValue()) {
                this.f4013a.a();
                return;
            }
            List queryIntentServices = k().getPackageManager().queryIntentServices(new Intent().setClassName(k(), "com.google.android.gms.measurement.AppMeasurementService"), 65536);
            if (queryIntentServices == null || queryIntentServices.size() <= 0) {
                z3 = false;
            }
            if (z3) {
                Intent intent = new Intent("com.google.android.gms.measurement.START");
                intent.setComponent(new ComponentName(k(), "com.google.android.gms.measurement.AppMeasurementService"));
                this.f4013a.a(intent);
                return;
            }
            q().v().a("Unable to use remote or local measurement implementation. Please register the AppMeasurementService service in the app manifest");
        }
    }
}
