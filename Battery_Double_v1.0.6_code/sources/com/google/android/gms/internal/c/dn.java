package com.google.android.gms.internal.c;

import java.util.concurrent.atomic.AtomicReference;

final class dn implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ AtomicReference f3984a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ String f3985b;
    private final /* synthetic */ String c;
    private final /* synthetic */ String d;
    private final /* synthetic */ boolean e;
    private final /* synthetic */ dd f;

    dn(dd ddVar, AtomicReference atomicReference, String str, String str2, String str3, boolean z) {
        this.f = ddVar;
        this.f3984a = atomicReference;
        this.f3985b = str;
        this.c = str2;
        this.d = str3;
        this.e = z;
    }

    public final void run() {
        this.f.q.s().a(this.f3984a, this.f3985b, this.c, this.d, this.e);
    }
}
