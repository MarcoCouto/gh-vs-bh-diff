package com.google.android.gms.internal.c;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Build.VERSION;
import android.text.TextUtils;
import android.util.Pair;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.ads.identifier.AdvertisingIdClient.Info;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.Locale;

final class bk extends da {

    /* renamed from: a reason: collision with root package name */
    static final Pair<String, Long> f3894a = new Pair<>("", Long.valueOf(0));

    /* renamed from: b reason: collision with root package name */
    public bo f3895b;
    public final bn c = new bn(this, "last_upload", 0);
    public final bn d = new bn(this, "last_upload_attempt", 0);
    public final bn e = new bn(this, "backoff", 0);
    public final bn f = new bn(this, "last_delete_stale", 0);
    public final bn g = new bn(this, "midnight_offset", 0);
    public final bn h = new bn(this, "first_open_time", 0);
    public final bn i = new bn(this, "app_install_time", 0);
    public final bp j = new bp(this, "app_instance_id", null);
    public final bn k = new bn(this, "time_before_start", 10000);
    public final bn l = new bn(this, "session_timeout", 1800000);
    public final bm m = new bm(this, "start_new_session", true);
    public final bn n = new bn(this, "last_pause_time", 0);
    public final bn o = new bn(this, "time_active", 0);
    public boolean p;
    private SharedPreferences r;
    private String s;
    private boolean t;
    private long u;
    private String v;
    private long w;
    private final Object x = new Object();

    bk(ce ceVar) {
        super(ceVar);
    }

    /* access modifiers changed from: private */
    public final SharedPreferences C() {
        c();
        F();
        return this.r;
    }

    /* access modifiers changed from: 0000 */
    public final boolean A() {
        c();
        return C().getBoolean("deferred_analytics_collection", false);
    }

    /* access modifiers changed from: 0000 */
    public final boolean B() {
        return this.r.contains("deferred_analytics_collection");
    }

    /* access modifiers changed from: 0000 */
    public final Pair<String, Boolean> a(String str) {
        c();
        long b2 = j().b();
        if (this.s != null && b2 < this.u) {
            return new Pair<>(this.s, Boolean.valueOf(this.t));
        }
        this.u = b2 + s().a(str, ap.c);
        AdvertisingIdClient.setShouldSkipGmsCoreVersionCheck(true);
        try {
            Info advertisingIdInfo = AdvertisingIdClient.getAdvertisingIdInfo(k());
            if (advertisingIdInfo != null) {
                this.s = advertisingIdInfo.getId();
                this.t = advertisingIdInfo.isLimitAdTrackingEnabled();
            }
            if (this.s == null) {
                this.s = "";
            }
        } catch (Exception e2) {
            q().B().a("Unable to get advertising id", e2);
            this.s = "";
        }
        AdvertisingIdClient.setShouldSkipGmsCoreVersionCheck(false);
        return new Pair<>(this.s, Boolean.valueOf(this.t));
    }

    /* access modifiers changed from: 0000 */
    public final void a(boolean z) {
        c();
        q().C().a("Setting useService", Boolean.valueOf(z));
        Editor edit = C().edit();
        edit.putBoolean("use_service", z);
        edit.apply();
    }

    /* access modifiers changed from: 0000 */
    public final String b(String str) {
        c();
        String str2 = (String) a(str).first;
        MessageDigest f2 = ft.f("MD5");
        if (f2 == null) {
            return null;
        }
        return String.format(Locale.US, "%032X", new Object[]{new BigInteger(1, f2.digest(str2.getBytes()))});
    }

    /* access modifiers changed from: 0000 */
    public final void b(boolean z) {
        c();
        q().C().a("Setting measurementEnabled", Boolean.valueOf(z));
        Editor edit = C().edit();
        edit.putBoolean("measurement_enabled", z);
        edit.apply();
    }

    /* access modifiers changed from: 0000 */
    public final void c(String str) {
        c();
        Editor edit = C().edit();
        edit.putString("gmp_app_id", str);
        edit.apply();
    }

    /* access modifiers changed from: 0000 */
    public final boolean c(boolean z) {
        c();
        return C().getBoolean("measurement_enabled", z);
    }

    /* access modifiers changed from: 0000 */
    public final void d(String str) {
        synchronized (this.x) {
            this.v = str;
            this.w = j().b();
        }
    }

    /* access modifiers changed from: 0000 */
    public final void d(boolean z) {
        c();
        q().C().a("Updating deferred analytics collection", Boolean.valueOf(z));
        Editor edit = C().edit();
        edit.putBoolean("deferred_analytics_collection", z);
        edit.apply();
    }

    /* access modifiers changed from: protected */
    public final boolean t() {
        return true;
    }

    /* access modifiers changed from: 0000 */
    public final String v() {
        c();
        return C().getString("gmp_app_id", null);
    }

    /* access modifiers changed from: protected */
    public final void v_() {
        this.r = k().getSharedPreferences("com.google.android.gms.measurement.prefs", 0);
        this.p = this.r.getBoolean("has_been_opened", false);
        if (!this.p) {
            Editor edit = this.r.edit();
            edit.putBoolean("has_been_opened", true);
            edit.apply();
        }
        this.f3895b = new bo(this, "health_monitor", Math.max(0, ((Long) ap.d.b()).longValue()));
    }

    /* access modifiers changed from: 0000 */
    public final String w() {
        String str;
        synchronized (this.x) {
            str = Math.abs(j().b() - this.w) < 1000 ? this.v : null;
        }
        return str;
    }

    /* access modifiers changed from: 0000 */
    public final Boolean x() {
        c();
        if (!C().contains("use_service")) {
            return null;
        }
        return Boolean.valueOf(C().getBoolean("use_service", false));
    }

    /* access modifiers changed from: 0000 */
    public final void y() {
        boolean z = true;
        c();
        q().C().a("Clearing collection preferences.");
        boolean contains = C().contains("measurement_enabled");
        if (contains) {
            z = c(true);
        }
        Editor edit = C().edit();
        edit.clear();
        edit.apply();
        if (contains) {
            b(z);
        }
    }

    /* access modifiers changed from: protected */
    public final String z() {
        c();
        String string = C().getString("previous_os_version", null);
        g().F();
        String str = VERSION.RELEASE;
        if (!TextUtils.isEmpty(str) && !str.equals(string)) {
            Editor edit = C().edit();
            edit.putString("previous_os_version", str);
            edit.apply();
        }
        return string;
    }
}
