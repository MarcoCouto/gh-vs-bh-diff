package com.google.android.gms.internal.c;

import android.os.RemoteException;
import android.text.TextUtils;

final class ej implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ boolean f4027a = true;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ boolean f4028b;
    private final /* synthetic */ an c;
    private final /* synthetic */ s d;
    private final /* synthetic */ String e;
    private final /* synthetic */ eb f;

    ej(eb ebVar, boolean z, boolean z2, an anVar, s sVar, String str) {
        this.f = ebVar;
        this.f4028b = z2;
        this.c = anVar;
        this.d = sVar;
        this.e = str;
    }

    public final void run() {
        ar d2 = this.f.f4014b;
        if (d2 == null) {
            this.f.q().v().a("Discarding data. Failed to send event to service");
            return;
        }
        if (this.f4027a) {
            this.f.a(d2, this.f4028b ? null : this.c, this.d);
        } else {
            try {
                if (TextUtils.isEmpty(this.e)) {
                    d2.a(this.c, this.d);
                } else {
                    d2.a(this.c, this.e, this.f.q().D());
                }
            } catch (RemoteException e2) {
                this.f.q().v().a("Failed to send event to the service", e2);
            }
        }
        this.f.C();
    }
}
