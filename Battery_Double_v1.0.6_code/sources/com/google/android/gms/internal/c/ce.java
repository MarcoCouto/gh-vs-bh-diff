package com.google.android.gms.internal.c;

import android.app.Application;
import android.content.Context;
import android.text.TextUtils;
import com.google.android.gms.common.api.internal.d;
import com.google.android.gms.common.b.c;
import com.google.android.gms.common.internal.aa;
import com.google.android.gms.common.util.e;
import com.google.android.gms.common.util.h;
import com.google.android.gms.measurement.AppMeasurement;
import com.google.firebase.analytics.FirebaseAnalytics;
import java.util.concurrent.atomic.AtomicReference;

public class ce extends fk implements db {
    private static volatile ce d;
    private int A;
    private int B;
    private final long C;
    private final Context e;
    private final y f;
    private final bk g;
    private final az h;
    private final bz i;
    private final fa j;
    private final AppMeasurement k;
    private final FirebaseAnalytics l;
    private final ft m;
    private final ax n;
    private final e o;
    private final dy p;
    private final dd q;
    private final n r;
    private av s;
    private eb t;
    private ah u;
    private au v;
    private bq w;
    private boolean x = false;
    private Boolean y;
    private long z;

    private ce(dc dcVar) {
        aa.a(dcVar);
        a(this);
        this.e = dcVar.f3964a;
        gy.a(this.e);
        this.c = -1;
        this.o = h.d();
        this.C = this.o.a();
        this.f = new y(this);
        bk bkVar = new bk(this);
        bkVar.G();
        this.g = bkVar;
        az azVar = new az(this);
        azVar.G();
        this.h = azVar;
        ft ftVar = new ft(this);
        ftVar.G();
        this.m = ftVar;
        ax axVar = new ax(this);
        axVar.G();
        this.n = axVar;
        this.r = new n(this);
        dy dyVar = new dy(this);
        dyVar.G();
        this.p = dyVar;
        dd ddVar = new dd(this);
        ddVar.G();
        this.q = ddVar;
        this.k = new AppMeasurement(this);
        this.l = new FirebaseAnalytics(this);
        fa faVar = new fa(this);
        faVar.G();
        this.j = faVar;
        bz bzVar = new bz(this);
        bzVar.G();
        this.i = bzVar;
        if (this.e.getApplicationContext() instanceof Application) {
            dd h2 = h();
            if (h2.k().getApplicationContext() instanceof Application) {
                Application application = (Application) h2.k().getApplicationContext();
                if (h2.f3965a == null) {
                    h2.f3965a = new dw(h2, null);
                }
                application.unregisterActivityLifecycleCallbacks(h2.f3965a);
                application.registerActivityLifecycleCallbacks(h2.f3965a);
                h2.q().C().a("Registered activity lifecycle callback");
            }
        } else {
            q().y().a("Application context is not an Application");
        }
        bd bdVar = new bd(this);
        bdVar.O();
        this.f4074b = bdVar;
        by byVar = new by(this);
        byVar.O();
        this.f4073a = byVar;
        this.i.a((Runnable) new cf(this, dcVar));
    }

    private final void K() {
        if (!this.x) {
            throw new IllegalStateException("AppMeasurement is not initialized");
        }
    }

    public static ce a(Context context) {
        aa.a(context);
        aa.a(context.getApplicationContext());
        if (d == null) {
            synchronized (ce.class) {
                if (d == null) {
                    d = new ce(new dc(context));
                }
            }
        }
        return d;
    }

    private static void a(cz czVar) {
        if (czVar == null) {
            throw new IllegalStateException("Component not created");
        }
    }

    private static void b(da daVar) {
        if (daVar == null) {
            throw new IllegalStateException("Component not created");
        } else if (!daVar.E()) {
            String valueOf = String.valueOf(daVar.getClass());
            throw new IllegalStateException(new StringBuilder(String.valueOf(valueOf).length() + 27).append("Component not initialized: ").append(valueOf).toString());
        }
    }

    static void z() {
        throw new IllegalStateException("Unexpected call on client side");
    }

    /* access modifiers changed from: 0000 */
    public final void A() {
        this.B++;
    }

    /* access modifiers changed from: protected */
    public final boolean B() {
        boolean z2 = false;
        K();
        w();
        if (this.y == null || this.z == 0 || (this.y != null && !this.y.booleanValue() && Math.abs(j().b() - this.z) > 1000)) {
            this.z = j().b();
            if (m().g("android.permission.INTERNET") && m().g("android.permission.ACCESS_NETWORK_STATE") && (c.b(k()).a() || (bu.a(k()) && ev.a(k(), false)))) {
                z2 = true;
            }
            this.y = Boolean.valueOf(z2);
            if (this.y.booleanValue()) {
                this.y = Boolean.valueOf(m().e(u().x()));
            }
        }
        return this.y.booleanValue();
    }

    /* access modifiers changed from: protected */
    public final void a() {
        boolean z2 = false;
        w();
        if (c().c.a() == 0) {
            c().c.a(j().a());
        }
        if (Long.valueOf(c().h.a()).longValue() == 0) {
            q().C().a("Persisting first open", Long.valueOf(this.C));
            c().h.a(this.C);
        }
        if (B()) {
            if (!TextUtils.isEmpty(u().x())) {
                String v2 = c().v();
                if (v2 == null) {
                    c().c(u().x());
                } else if (!v2.equals(u().x())) {
                    q().A().a("Rechecking which service to use due to a GMP App Id change");
                    c().y();
                    this.t.B();
                    this.t.z();
                    c().c(u().x());
                    c().h.a(this.C);
                    c().j.a(null);
                }
            }
            h().a(c().j.a());
            if (!TextUtils.isEmpty(u().x())) {
                boolean x2 = x();
                if (!c().B() && !b().u()) {
                    bk c = c();
                    if (!x2) {
                        z2 = true;
                    }
                    c.d(z2);
                }
                if (!b().i(u().w()) || x2) {
                    h().D();
                }
                s().a(new AtomicReference<>());
            }
        } else if (x()) {
            if (!m().g("android.permission.INTERNET")) {
                q().v().a("App is missing INTERNET permission");
            }
            if (!m().g("android.permission.ACCESS_NETWORK_STATE")) {
                q().v().a("App is missing ACCESS_NETWORK_STATE permission");
            }
            if (!c.b(k()).a()) {
                if (!bu.a(k())) {
                    q().v().a("AppMeasurementReceiver not registered/enabled");
                }
                if (!ev.a(k(), false)) {
                    q().v().a("AppMeasurementService not registered/enabled");
                }
            }
            q().v().a("Uploading is not possible. App measurement disabled");
        }
        super.a();
    }

    /* access modifiers changed from: 0000 */
    public final void a(da daVar) {
        this.A++;
    }

    /* access modifiers changed from: 0000 */
    public final void a(dc dcVar) {
        bb A2;
        String str;
        w();
        ah ahVar = new ah(this);
        ahVar.G();
        this.u = ahVar;
        au auVar = new au(this);
        auVar.G();
        this.v = auVar;
        av avVar = new av(this);
        avVar.G();
        this.s = avVar;
        eb ebVar = new eb(this);
        ebVar.G();
        this.t = ebVar;
        this.m.H();
        this.g.H();
        this.w = new bq(this);
        this.v.H();
        q().A().a("App measurement is starting up, version", Long.valueOf(12451));
        q().A().a("To enable debug logging run: adb shell setprop log.tag.FA VERBOSE");
        String w2 = auVar.w();
        if (m().i(w2)) {
            A2 = q().A();
            str = "Faster debug mode event logging enabled. To disable, run:\n  adb shell setprop debug.firebase.analytics.app .none.";
        } else {
            A2 = q().A();
            String str2 = "To enable faster debug mode event logging run:\n  adb shell setprop debug.firebase.analytics.app ";
            String valueOf = String.valueOf(w2);
            str = valueOf.length() != 0 ? str2.concat(valueOf) : new String(str2);
        }
        A2.a(str);
        q().B().a("Debug-level message logging enabled");
        if (this.A != this.B) {
            q().v().a("Not all components initialized", Integer.valueOf(this.A), Integer.valueOf(this.B));
        }
        super.a((fp) dcVar);
        this.x = true;
    }

    public final y b() {
        return this.f;
    }

    public final bk c() {
        a((cz) this.g);
        return this.g;
    }

    public final az d() {
        if (this.h == null || !this.h.E()) {
            return null;
        }
        return this.h;
    }

    public final fa e() {
        b(this.j);
        return this.j;
    }

    public final bq f() {
        return this.w;
    }

    /* access modifiers changed from: 0000 */
    public final bz g() {
        return this.i;
    }

    public final dd h() {
        b(this.q);
        return this.q;
    }

    public final AppMeasurement i() {
        return this.k;
    }

    public final e j() {
        return this.o;
    }

    public final Context k() {
        return this.e;
    }

    public final FirebaseAnalytics l() {
        return this.l;
    }

    public final ft m() {
        a((cz) this.m);
        return this.m;
    }

    public final ax n() {
        a((cz) this.n);
        return this.n;
    }

    public final av o() {
        b(this.s);
        return this.s;
    }

    public final bz p() {
        b(this.i);
        return this.i;
    }

    public final az q() {
        b(this.h);
        return this.h;
    }

    public final dy r() {
        b(this.p);
        return this.p;
    }

    public final eb s() {
        b(this.t);
        return this.t;
    }

    public final ah t() {
        b(this.u);
        return this.u;
    }

    public final au u() {
        b(this.v);
        return this.v;
    }

    public final n v() {
        a((cz) this.r);
        return this.r;
    }

    public final void w() {
        p().c();
    }

    public final boolean x() {
        boolean z2 = false;
        w();
        K();
        if (b().u()) {
            return false;
        }
        Boolean b2 = b().b("firebase_analytics_collection_enabled");
        if (b2 != null) {
            z2 = b2.booleanValue();
        } else if (!d.b()) {
            z2 = true;
        }
        return c().c(z2);
    }

    /* access modifiers changed from: 0000 */
    public final long y() {
        Long valueOf = Long.valueOf(c().h.a());
        return valueOf.longValue() == 0 ? this.C : Math.min(this.C, valueOf.longValue());
    }
}
