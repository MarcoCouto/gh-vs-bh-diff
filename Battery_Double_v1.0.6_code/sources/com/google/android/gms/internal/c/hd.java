package com.google.android.gms.internal.c;

import android.util.Log;

final class hd extends gy<Integer> {
    hd(hi hiVar, String str, Integer num) {
        super(hiVar, str, num, null);
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public final Integer a(String str) {
        try {
            return Integer.valueOf(Integer.parseInt(str));
        } catch (NumberFormatException e) {
            String str2 = this.f4102a;
            Log.e("PhenotypeFlag", new StringBuilder(String.valueOf(str2).length() + 28 + String.valueOf(str).length()).append("Invalid integer value for ").append(str2).append(": ").append(str).toString());
            return null;
        }
    }
}
