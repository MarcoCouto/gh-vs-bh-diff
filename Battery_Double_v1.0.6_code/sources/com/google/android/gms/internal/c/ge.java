package com.google.android.gms.internal.c;

import java.io.IOException;

public final class ge extends d<ge> {
    private static volatile ge[] e;
    public String c;
    public String d;

    public ge() {
        this.c = null;
        this.d = null;
        this.f3962a = null;
        this.f4112b = -1;
    }

    public static ge[] e() {
        if (e == null) {
            synchronized (h.f4106b) {
                if (e == null) {
                    e = new ge[0];
                }
            }
        }
        return e;
    }

    /* access modifiers changed from: protected */
    public final int a() {
        int a2 = super.a();
        if (this.c != null) {
            a2 += b.b(1, this.c);
        }
        return this.d != null ? a2 + b.b(2, this.d) : a2;
    }

    public final /* synthetic */ j a(a aVar) throws IOException {
        while (true) {
            int a2 = aVar.a();
            switch (a2) {
                case 0:
                    break;
                case 10:
                    this.c = aVar.c();
                    continue;
                case 18:
                    this.d = aVar.c();
                    continue;
                default:
                    if (!super.a(aVar, a2)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }

    public final void a(b bVar) throws IOException {
        if (this.c != null) {
            bVar.a(1, this.c);
        }
        if (this.d != null) {
            bVar.a(2, this.d);
        }
        super.a(bVar);
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ge)) {
            return false;
        }
        ge geVar = (ge) obj;
        if (this.c == null) {
            if (geVar.c != null) {
                return false;
            }
        } else if (!this.c.equals(geVar.c)) {
            return false;
        }
        if (this.d == null) {
            if (geVar.d != null) {
                return false;
            }
        } else if (!this.d.equals(geVar.d)) {
            return false;
        }
        return (this.f3962a == null || this.f3962a.b()) ? geVar.f3962a == null || geVar.f3962a.b() : this.f3962a.equals(geVar.f3962a);
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = ((this.d == null ? 0 : this.d.hashCode()) + (((this.c == null ? 0 : this.c.hashCode()) + ((getClass().getName().hashCode() + 527) * 31)) * 31)) * 31;
        if (this.f3962a != null && !this.f3962a.b()) {
            i = this.f3962a.hashCode();
        }
        return hashCode + i;
    }
}
