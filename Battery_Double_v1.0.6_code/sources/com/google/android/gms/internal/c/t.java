package com.google.android.gms.internal.c;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.a.b;

public final class t implements Creator<s> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b2 = b.b(parcel);
        String str = null;
        String str2 = null;
        String str3 = null;
        String str4 = null;
        long j = 0;
        long j2 = 0;
        String str5 = null;
        boolean z = true;
        boolean z2 = false;
        long j3 = -2147483648L;
        String str6 = null;
        long j4 = 0;
        long j5 = 0;
        int i = 0;
        boolean z3 = true;
        boolean z4 = true;
        boolean z5 = false;
        while (parcel.dataPosition() < b2) {
            int a2 = b.a(parcel);
            switch (b.a(a2)) {
                case 2:
                    str = b.k(parcel, a2);
                    break;
                case 3:
                    str2 = b.k(parcel, a2);
                    break;
                case 4:
                    str3 = b.k(parcel, a2);
                    break;
                case 5:
                    str4 = b.k(parcel, a2);
                    break;
                case 6:
                    j = b.f(parcel, a2);
                    break;
                case 7:
                    j2 = b.f(parcel, a2);
                    break;
                case 8:
                    str5 = b.k(parcel, a2);
                    break;
                case 9:
                    z = b.c(parcel, a2);
                    break;
                case 10:
                    z2 = b.c(parcel, a2);
                    break;
                case 11:
                    j3 = b.f(parcel, a2);
                    break;
                case 12:
                    str6 = b.k(parcel, a2);
                    break;
                case 13:
                    j4 = b.f(parcel, a2);
                    break;
                case 14:
                    j5 = b.f(parcel, a2);
                    break;
                case 15:
                    i = b.d(parcel, a2);
                    break;
                case 16:
                    z3 = b.c(parcel, a2);
                    break;
                case 17:
                    z4 = b.c(parcel, a2);
                    break;
                case 18:
                    z5 = b.c(parcel, a2);
                    break;
                default:
                    b.b(parcel, a2);
                    break;
            }
        }
        b.r(parcel, b2);
        return new s(str, str2, str3, str4, j, j2, str5, z, z2, j3, str6, j4, j5, i, z3, z4, z5);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new s[i];
    }
}
