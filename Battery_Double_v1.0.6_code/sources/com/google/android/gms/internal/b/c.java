package com.google.android.gms.internal.b;

import android.os.Parcel;
import android.os.Parcelable;

public class c {

    /* renamed from: a reason: collision with root package name */
    private static final ClassLoader f3837a = c.class.getClassLoader();

    private c() {
    }

    public static void a(Parcel parcel, Parcelable parcelable) {
        if (parcelable == null) {
            parcel.writeInt(0);
            return;
        }
        parcel.writeInt(1);
        parcelable.writeToParcel(parcel, 0);
    }
}
