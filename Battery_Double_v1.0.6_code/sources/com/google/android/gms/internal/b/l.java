package com.google.android.gms.internal.b;

final class l extends i {

    /* renamed from: a reason: collision with root package name */
    private final j f3844a = new j();

    l() {
    }

    public final void a(Throwable th, Throwable th2) {
        if (th2 == th) {
            throw new IllegalArgumentException("Self suppression is not allowed.", th2);
        } else if (th2 == null) {
            throw new NullPointerException("The suppressed exception cannot be null.");
        } else {
            this.f3844a.a(th, true).add(th2);
        }
    }
}
