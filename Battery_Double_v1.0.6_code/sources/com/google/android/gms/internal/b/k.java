package com.google.android.gms.internal.b;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;

final class k extends WeakReference<Throwable> {

    /* renamed from: a reason: collision with root package name */
    private final int f3843a;

    public k(Throwable th, ReferenceQueue<Throwable> referenceQueue) {
        super(th, referenceQueue);
        if (th == null) {
            throw new NullPointerException("The referent cannot be null");
        }
        this.f3843a = System.identityHashCode(th);
    }

    public final boolean equals(Object obj) {
        if (obj == null || obj.getClass() != getClass()) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        k kVar = (k) obj;
        return this.f3843a == kVar.f3843a && get() == kVar.get();
    }

    public final int hashCode() {
        return this.f3843a;
    }
}
