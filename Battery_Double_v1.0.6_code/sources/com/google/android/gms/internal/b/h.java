package com.google.android.gms.internal.b;

import java.io.PrintStream;

public final class h {

    /* renamed from: a reason: collision with root package name */
    private static final i f3838a;

    /* renamed from: b reason: collision with root package name */
    private static final int f3839b;

    static final class a extends i {
        a() {
        }

        public final void a(Throwable th, Throwable th2) {
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x0019  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0070  */
    static {
        Integer num;
        Throwable th;
        i aVar;
        try {
            num = a();
            if (num != null) {
                try {
                    if (num.intValue() >= 19) {
                        aVar = new m();
                        f3838a = aVar;
                        f3839b = num == null ? 1 : num.intValue();
                    }
                } catch (Throwable th2) {
                    th = th2;
                }
            }
            aVar = !Boolean.getBoolean("com.google.devtools.build.android.desugar.runtime.twr_disable_mimic") ? new l() : new a();
        } catch (Throwable th3) {
            Throwable th4 = th3;
            num = null;
            th = th4;
        }
        f3838a = aVar;
        f3839b = num == null ? 1 : num.intValue();
        PrintStream printStream = System.err;
        String name = a.class.getName();
        printStream.println(new StringBuilder(String.valueOf(name).length() + 132).append("An error has occured when initializing the try-with-resources desuguring strategy. The default strategy ").append(name).append("will be used. The error is: ").toString());
        th.printStackTrace(System.err);
        aVar = new a();
        f3838a = aVar;
        f3839b = num == null ? 1 : num.intValue();
    }

    private static Integer a() {
        try {
            return (Integer) Class.forName("android.os.Build$VERSION").getField("SDK_INT").get(null);
        } catch (Exception e) {
            System.err.println("Failed to retrieve value from android.os.Build$VERSION.SDK_INT due to the following exception.");
            e.printStackTrace(System.err);
            return null;
        }
    }

    public static void a(Throwable th, Throwable th2) {
        f3838a.a(th, th2);
    }
}
