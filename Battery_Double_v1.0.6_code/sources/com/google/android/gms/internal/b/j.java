package com.google.android.gms.internal.b;

import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;

final class j {

    /* renamed from: a reason: collision with root package name */
    private final ConcurrentHashMap<k, List<Throwable>> f3841a = new ConcurrentHashMap<>(16, 0.75f, 10);

    /* renamed from: b reason: collision with root package name */
    private final ReferenceQueue<Throwable> f3842b = new ReferenceQueue<>();

    j() {
    }

    public final List<Throwable> a(Throwable th, boolean z) {
        Reference poll = this.f3842b.poll();
        while (poll != null) {
            this.f3841a.remove(poll);
            poll = this.f3842b.poll();
        }
        List<Throwable> list = (List) this.f3841a.get(new k(th, null));
        if (list != null) {
            return list;
        }
        Vector vector = new Vector(2);
        List<Throwable> list2 = (List) this.f3841a.putIfAbsent(new k(th, this.f3842b), vector);
        return list2 == null ? vector : list2;
    }
}
