package com.google.android.gms.internal.a;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public class a implements IInterface {

    /* renamed from: a reason: collision with root package name */
    private final IBinder f2381a;

    /* renamed from: b reason: collision with root package name */
    private final String f2382b;

    protected a(IBinder iBinder, String str) {
        this.f2381a = iBinder;
        this.f2382b = str;
    }

    /* access modifiers changed from: protected */
    public final Parcel a(int i, Parcel parcel) throws RemoteException {
        parcel = Parcel.obtain();
        try {
            this.f2381a.transact(i, parcel, parcel, 0);
            parcel.readException();
            return parcel;
        } catch (RuntimeException e) {
            throw e;
        } finally {
            parcel.recycle();
        }
    }

    public IBinder asBinder() {
        return this.f2381a;
    }

    /* access modifiers changed from: protected */
    public final Parcel s_() {
        Parcel obtain = Parcel.obtain();
        obtain.writeInterfaceToken(this.f2382b);
        return obtain;
    }
}
