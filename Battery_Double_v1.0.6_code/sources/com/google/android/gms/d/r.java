package com.google.android.gms.d;

final class r implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ g f2362a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ q f2363b;

    r(q qVar, g gVar) {
        this.f2363b = qVar;
        this.f2362a = gVar;
    }

    public final void run() {
        synchronized (this.f2363b.f2361b) {
            if (this.f2363b.c != null) {
                this.f2363b.c.a(this.f2362a.e());
            }
        }
    }
}
