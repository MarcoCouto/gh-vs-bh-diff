package com.google.android.gms.d;

public class h<TResult> {

    /* renamed from: a reason: collision with root package name */
    private final x<TResult> f2344a = new x<>();

    public g<TResult> a() {
        return this.f2344a;
    }

    public void a(Exception exc) {
        this.f2344a.a(exc);
    }

    public void a(TResult tresult) {
        this.f2344a.a(tresult);
    }

    public boolean b(Exception exc) {
        return this.f2344a.b(exc);
    }

    public boolean b(TResult tresult) {
        return this.f2344a.b(tresult);
    }
}
