package com.google.android.gms.d;

import java.util.ArrayDeque;
import java.util.Queue;

final class v<TResult> {

    /* renamed from: a reason: collision with root package name */
    private final Object f2368a = new Object();

    /* renamed from: b reason: collision with root package name */
    private Queue<u<TResult>> f2369b;
    private boolean c;

    v() {
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0011, code lost:
        r1 = r2.f2368a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0013, code lost:
        monitor-enter(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
        r0 = (com.google.android.gms.d.u) r2.f2369b.poll();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x001c, code lost:
        if (r0 != null) goto L_0x0029;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x001e, code lost:
        r2.c = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0021, code lost:
        monitor-exit(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
        monitor-exit(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x002a, code lost:
        r0.a(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:?, code lost:
        return;
     */
    public final void a(g<TResult> gVar) {
        synchronized (this.f2368a) {
            if (this.f2369b != null && !this.c) {
                this.c = true;
            }
        }
    }

    public final void a(u<TResult> uVar) {
        synchronized (this.f2368a) {
            if (this.f2369b == null) {
                this.f2369b = new ArrayDeque();
            }
            this.f2369b.add(uVar);
        }
    }
}
