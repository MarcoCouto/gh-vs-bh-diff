package com.google.android.gms.d;

final class t implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ g f2366a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ s f2367b;

    t(s sVar, g gVar) {
        this.f2367b = sVar;
        this.f2366a = gVar;
    }

    public final void run() {
        synchronized (this.f2367b.f2365b) {
            if (this.f2367b.c != null) {
                this.f2367b.c.a(this.f2366a.d());
            }
        }
    }
}
