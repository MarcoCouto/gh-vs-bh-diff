package com.google.android.gms.d;

import com.google.android.gms.common.internal.aa;
import java.util.concurrent.CancellationException;
import java.util.concurrent.Executor;

final class x<TResult> extends g<TResult> {

    /* renamed from: a reason: collision with root package name */
    private final Object f2370a = new Object();

    /* renamed from: b reason: collision with root package name */
    private final v<TResult> f2371b = new v<>();
    private boolean c;
    private volatile boolean d;
    private TResult e;
    private Exception f;

    x() {
    }

    private final void g() {
        aa.a(this.c, (Object) "Task is not yet complete");
    }

    private final void h() {
        aa.a(!this.c, (Object) "Task is already complete");
    }

    private final void i() {
        if (this.d) {
            throw new CancellationException("Task is already canceled.");
        }
    }

    private final void j() {
        synchronized (this.f2370a) {
            if (this.c) {
                this.f2371b.a((g<TResult>) this);
            }
        }
    }

    public final g<TResult> a(c<TResult> cVar) {
        return a(i.f2345a, cVar);
    }

    public final <TContinuationResult> g<TContinuationResult> a(Executor executor, a<TResult, TContinuationResult> aVar) {
        x xVar = new x();
        this.f2371b.a((u<TResult>) new k<TResult>(executor, aVar, xVar));
        j();
        return xVar;
    }

    public final g<TResult> a(Executor executor, b bVar) {
        this.f2371b.a((u<TResult>) new m<TResult>(executor, bVar));
        j();
        return this;
    }

    public final g<TResult> a(Executor executor, c<TResult> cVar) {
        this.f2371b.a((u<TResult>) new o<TResult>(executor, cVar));
        j();
        return this;
    }

    public final g<TResult> a(Executor executor, d dVar) {
        this.f2371b.a((u<TResult>) new q<TResult>(executor, dVar));
        j();
        return this;
    }

    public final g<TResult> a(Executor executor, e<? super TResult> eVar) {
        this.f2371b.a((u<TResult>) new s<TResult>(executor, eVar));
        j();
        return this;
    }

    public final <X extends Throwable> TResult a(Class<X> cls) throws Throwable {
        TResult tresult;
        synchronized (this.f2370a) {
            g();
            i();
            if (cls.isInstance(this.f)) {
                throw ((Throwable) cls.cast(this.f));
            } else if (this.f != null) {
                throw new f(this.f);
            } else {
                tresult = this.e;
            }
        }
        return tresult;
    }

    public final void a(Exception exc) {
        aa.a(exc, (Object) "Exception must not be null");
        synchronized (this.f2370a) {
            h();
            this.c = true;
            this.f = exc;
        }
        this.f2371b.a((g<TResult>) this);
    }

    public final void a(TResult tresult) {
        synchronized (this.f2370a) {
            h();
            this.c = true;
            this.e = tresult;
        }
        this.f2371b.a((g<TResult>) this);
    }

    public final boolean a() {
        boolean z;
        synchronized (this.f2370a) {
            z = this.c;
        }
        return z;
    }

    public final boolean b() {
        boolean z;
        synchronized (this.f2370a) {
            z = this.c && !this.d && this.f == null;
        }
        return z;
    }

    public final boolean b(Exception exc) {
        boolean z = true;
        aa.a(exc, (Object) "Exception must not be null");
        synchronized (this.f2370a) {
            if (this.c) {
                z = false;
            } else {
                this.c = true;
                this.f = exc;
                this.f2371b.a((g<TResult>) this);
            }
        }
        return z;
    }

    public final boolean b(TResult tresult) {
        boolean z = true;
        synchronized (this.f2370a) {
            if (this.c) {
                z = false;
            } else {
                this.c = true;
                this.e = tresult;
                this.f2371b.a((g<TResult>) this);
            }
        }
        return z;
    }

    public final boolean c() {
        return this.d;
    }

    public final TResult d() {
        TResult tresult;
        synchronized (this.f2370a) {
            g();
            i();
            if (this.f != null) {
                throw new f(this.f);
            }
            tresult = this.e;
        }
        return tresult;
    }

    public final Exception e() {
        Exception exc;
        synchronized (this.f2370a) {
            exc = this.f;
        }
        return exc;
    }

    public final boolean f() {
        boolean z = true;
        synchronized (this.f2370a) {
            if (this.c) {
                z = false;
            } else {
                this.c = true;
                this.d = true;
                this.f2371b.a((g<TResult>) this);
            }
        }
        return z;
    }
}
