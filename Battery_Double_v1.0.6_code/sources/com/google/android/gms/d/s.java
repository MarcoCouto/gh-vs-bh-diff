package com.google.android.gms.d;

import java.util.concurrent.Executor;

final class s<TResult> implements u<TResult> {

    /* renamed from: a reason: collision with root package name */
    private final Executor f2364a;
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public final Object f2365b = new Object();
    /* access modifiers changed from: private */
    public e<? super TResult> c;

    public s(Executor executor, e<? super TResult> eVar) {
        this.f2364a = executor;
        this.c = eVar;
    }

    public final void a(g<TResult> gVar) {
        if (gVar.b()) {
            synchronized (this.f2365b) {
                if (this.c != null) {
                    this.f2364a.execute(new t(this, gVar));
                }
            }
        }
    }
}
