package com.google.android.gms.d;

import java.util.concurrent.Executor;

final class o<TResult> implements u<TResult> {

    /* renamed from: a reason: collision with root package name */
    private final Executor f2356a;
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public final Object f2357b = new Object();
    /* access modifiers changed from: private */
    public c<TResult> c;

    public o(Executor executor, c<TResult> cVar) {
        this.f2356a = executor;
        this.c = cVar;
    }

    public final void a(g<TResult> gVar) {
        synchronized (this.f2357b) {
            if (this.c != null) {
                this.f2356a.execute(new p(this, gVar));
            }
        }
    }
}
