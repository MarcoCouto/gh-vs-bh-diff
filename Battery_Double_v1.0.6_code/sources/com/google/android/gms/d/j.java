package com.google.android.gms.d;

import com.google.android.gms.common.internal.aa;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public final class j {

    private static final class a implements b {

        /* renamed from: a reason: collision with root package name */
        private final CountDownLatch f2348a;

        private a() {
            this.f2348a = new CountDownLatch(1);
        }

        /* synthetic */ a(y yVar) {
            this();
        }

        public final void a() {
            this.f2348a.countDown();
        }

        public final void a(Exception exc) {
            this.f2348a.countDown();
        }

        public final void a(Object obj) {
            this.f2348a.countDown();
        }

        public final boolean a(long j, TimeUnit timeUnit) throws InterruptedException {
            return this.f2348a.await(j, timeUnit);
        }

        public final void b() throws InterruptedException {
            this.f2348a.await();
        }
    }

    interface b extends b, d, e<Object> {
    }

    public static <TResult> g<TResult> a(Exception exc) {
        x xVar = new x();
        xVar.a(exc);
        return xVar;
    }

    public static <TResult> g<TResult> a(TResult tresult) {
        x xVar = new x();
        xVar.a(tresult);
        return xVar;
    }

    public static <TResult> g<TResult> a(Executor executor, Callable<TResult> callable) {
        aa.a(executor, (Object) "Executor must not be null");
        aa.a(callable, (Object) "Callback must not be null");
        x xVar = new x();
        executor.execute(new y(xVar, callable));
        return xVar;
    }

    public static <TResult> TResult a(g<TResult> gVar) throws ExecutionException, InterruptedException {
        aa.a();
        aa.a(gVar, (Object) "Task must not be null");
        if (gVar.a()) {
            return b(gVar);
        }
        a aVar = new a(null);
        a(gVar, (b) aVar);
        aVar.b();
        return b(gVar);
    }

    public static <TResult> TResult a(g<TResult> gVar, long j, TimeUnit timeUnit) throws ExecutionException, InterruptedException, TimeoutException {
        aa.a();
        aa.a(gVar, (Object) "Task must not be null");
        aa.a(timeUnit, (Object) "TimeUnit must not be null");
        if (gVar.a()) {
            return b(gVar);
        }
        a aVar = new a(null);
        a(gVar, (b) aVar);
        if (aVar.a(j, timeUnit)) {
            return b(gVar);
        }
        throw new TimeoutException("Timed out waiting for Task");
    }

    private static void a(g<?> gVar, b bVar) {
        gVar.a(i.f2346b, (e<? super TResult>) bVar);
        gVar.a(i.f2346b, (d) bVar);
        gVar.a(i.f2346b, (b) bVar);
    }

    private static <TResult> TResult b(g<TResult> gVar) throws ExecutionException {
        if (gVar.b()) {
            return gVar.d();
        }
        if (gVar.c()) {
            throw new CancellationException("Task is already canceled");
        }
        throw new ExecutionException(gVar.e());
    }
}
