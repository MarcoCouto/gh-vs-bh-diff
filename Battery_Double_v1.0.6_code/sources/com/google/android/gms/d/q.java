package com.google.android.gms.d;

import java.util.concurrent.Executor;

final class q<TResult> implements u<TResult> {

    /* renamed from: a reason: collision with root package name */
    private final Executor f2360a;
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public final Object f2361b = new Object();
    /* access modifiers changed from: private */
    public d c;

    public q(Executor executor, d dVar) {
        this.f2360a = executor;
        this.c = dVar;
    }

    public final void a(g<TResult> gVar) {
        if (!gVar.b() && !gVar.c()) {
            synchronized (this.f2361b) {
                if (this.c != null) {
                    this.f2360a.execute(new r(this, gVar));
                }
            }
        }
    }
}
