package com.google.android.gms.d;

import java.util.concurrent.Executor;

final class m<TResult> implements u<TResult> {

    /* renamed from: a reason: collision with root package name */
    private final Executor f2353a;
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public final Object f2354b = new Object();
    /* access modifiers changed from: private */
    public b c;

    public m(Executor executor, b bVar) {
        this.f2353a = executor;
        this.c = bVar;
    }

    public final void a(g gVar) {
        if (gVar.c()) {
            synchronized (this.f2354b) {
                if (this.c != null) {
                    this.f2353a.execute(new n(this));
                }
            }
        }
    }
}
