package com.google.android.gms.d;

final class p implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ g f2358a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ o f2359b;

    p(o oVar, g gVar) {
        this.f2359b = oVar;
        this.f2358a = gVar;
    }

    public final void run() {
        synchronized (this.f2359b.f2357b) {
            if (this.f2359b.c != null) {
                this.f2359b.c.a(this.f2358a);
            }
        }
    }
}
