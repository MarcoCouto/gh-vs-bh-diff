package com.google.android.gms.d;

import java.util.concurrent.Executor;

final class k<TResult, TContinuationResult> implements u<TResult> {

    /* renamed from: a reason: collision with root package name */
    private final Executor f2349a;
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public final a<TResult, TContinuationResult> f2350b;
    /* access modifiers changed from: private */
    public final x<TContinuationResult> c;

    public k(Executor executor, a<TResult, TContinuationResult> aVar, x<TContinuationResult> xVar) {
        this.f2349a = executor;
        this.f2350b = aVar;
        this.c = xVar;
    }

    public final void a(g<TResult> gVar) {
        this.f2349a.execute(new l(this, gVar));
    }
}
