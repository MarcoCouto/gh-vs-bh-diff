package com.google.android.gms.d;

import java.util.concurrent.Callable;

final class y implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ x f2372a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ Callable f2373b;

    y(x xVar, Callable callable) {
        this.f2372a = xVar;
        this.f2373b = callable;
    }

    public final void run() {
        try {
            this.f2372a.a(this.f2373b.call());
        } catch (Exception e) {
            this.f2372a.a(e);
        }
    }
}
