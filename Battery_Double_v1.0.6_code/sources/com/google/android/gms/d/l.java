package com.google.android.gms.d;

final class l implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ g f2351a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ k f2352b;

    l(k kVar, g gVar) {
        this.f2352b = kVar;
        this.f2351a = gVar;
    }

    public final void run() {
        if (this.f2351a.c()) {
            this.f2352b.c.f();
            return;
        }
        try {
            this.f2352b.c.a(this.f2352b.f2350b.a(this.f2351a));
        } catch (f e) {
            if (e.getCause() instanceof Exception) {
                this.f2352b.c.a((Exception) e.getCause());
            } else {
                this.f2352b.c.a((Exception) e);
            }
        } catch (Exception e2) {
            this.f2352b.c.a(e2);
        }
    }
}
