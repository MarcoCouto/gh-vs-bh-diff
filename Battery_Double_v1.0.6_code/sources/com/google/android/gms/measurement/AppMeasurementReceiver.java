package com.google.android.gms.measurement;

import android.content.BroadcastReceiver.PendingResult;
import android.content.Context;
import android.content.Intent;
import android.support.v4.b.d;
import com.google.android.gms.internal.c.bu;
import com.google.android.gms.internal.c.bx;

public final class AppMeasurementReceiver extends d implements bx {

    /* renamed from: a reason: collision with root package name */
    private bu f4146a;

    public final PendingResult a() {
        return goAsync();
    }

    public final void a(Context context, Intent intent) {
        startWakefulService(context, intent);
    }

    public final void onReceive(Context context, Intent intent) {
        if (this.f4146a == null) {
            this.f4146a = new bu(this);
        }
        this.f4146a.a(context, intent);
    }
}
