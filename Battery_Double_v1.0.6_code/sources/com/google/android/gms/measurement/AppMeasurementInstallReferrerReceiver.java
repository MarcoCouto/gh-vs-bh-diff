package com.google.android.gms.measurement;

import android.content.BroadcastReceiver;
import android.content.BroadcastReceiver.PendingResult;
import android.content.Context;
import android.content.Intent;
import com.google.android.gms.internal.c.bu;
import com.google.android.gms.internal.c.bx;

public final class AppMeasurementInstallReferrerReceiver extends BroadcastReceiver implements bx {

    /* renamed from: a reason: collision with root package name */
    private bu f4144a;

    public final PendingResult a() {
        return goAsync();
    }

    public final void a(Context context, Intent intent) {
    }

    public final void onReceive(Context context, Intent intent) {
        if (this.f4144a == null) {
            this.f4144a = new bu(this);
        }
        this.f4144a.a(context, intent);
    }
}
