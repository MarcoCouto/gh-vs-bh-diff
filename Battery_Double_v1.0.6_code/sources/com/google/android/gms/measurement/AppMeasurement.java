package com.google.android.gms.measurement;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Keep;
import com.google.android.gms.common.internal.aa;
import com.google.android.gms.internal.c.ce;
import com.google.android.gms.internal.c.dx;
import com.google.android.gms.internal.c.fq;
import com.google.android.gms.internal.c.ft;
import java.util.List;
import java.util.Map;

@Keep
@Deprecated
public class AppMeasurement {
    public static final String CRASH_ORIGIN = "crash";
    public static final String FCM_ORIGIN = "fcm";
    private final ce zzacw;

    public static class ConditionalUserProperty {
        @Keep
        public boolean mActive;
        @Keep
        public String mAppId;
        @Keep
        public long mCreationTimestamp;
        @Keep
        public String mExpiredEventName;
        @Keep
        public Bundle mExpiredEventParams;
        @Keep
        public String mName;
        @Keep
        public String mOrigin;
        @Keep
        public long mTimeToLive;
        @Keep
        public String mTimedOutEventName;
        @Keep
        public Bundle mTimedOutEventParams;
        @Keep
        public String mTriggerEventName;
        @Keep
        public long mTriggerTimeout;
        @Keep
        public String mTriggeredEventName;
        @Keep
        public Bundle mTriggeredEventParams;
        @Keep
        public long mTriggeredTimestamp;
        @Keep
        public Object mValue;

        public ConditionalUserProperty() {
        }

        public ConditionalUserProperty(ConditionalUserProperty conditionalUserProperty) {
            aa.a(conditionalUserProperty);
            this.mAppId = conditionalUserProperty.mAppId;
            this.mOrigin = conditionalUserProperty.mOrigin;
            this.mCreationTimestamp = conditionalUserProperty.mCreationTimestamp;
            this.mName = conditionalUserProperty.mName;
            if (conditionalUserProperty.mValue != null) {
                this.mValue = ft.b(conditionalUserProperty.mValue);
                if (this.mValue == null) {
                    this.mValue = conditionalUserProperty.mValue;
                }
            }
            this.mActive = conditionalUserProperty.mActive;
            this.mTriggerEventName = conditionalUserProperty.mTriggerEventName;
            this.mTriggerTimeout = conditionalUserProperty.mTriggerTimeout;
            this.mTimedOutEventName = conditionalUserProperty.mTimedOutEventName;
            if (conditionalUserProperty.mTimedOutEventParams != null) {
                this.mTimedOutEventParams = new Bundle(conditionalUserProperty.mTimedOutEventParams);
            }
            this.mTriggeredEventName = conditionalUserProperty.mTriggeredEventName;
            if (conditionalUserProperty.mTriggeredEventParams != null) {
                this.mTriggeredEventParams = new Bundle(conditionalUserProperty.mTriggeredEventParams);
            }
            this.mTriggeredTimestamp = conditionalUserProperty.mTriggeredTimestamp;
            this.mTimeToLive = conditionalUserProperty.mTimeToLive;
            this.mExpiredEventName = conditionalUserProperty.mExpiredEventName;
            if (conditionalUserProperty.mExpiredEventParams != null) {
                this.mExpiredEventParams = new Bundle(conditionalUserProperty.mExpiredEventParams);
            }
        }
    }

    public static final class a extends com.google.firebase.analytics.FirebaseAnalytics.a {

        /* renamed from: a reason: collision with root package name */
        public static final String[] f4138a = {"app_clear_data", "app_exception", "app_remove", "app_upgrade", "app_install", "app_update", "firebase_campaign", "error", "first_open", "first_visit", "in_app_purchase", "notification_dismiss", "notification_foreground", "notification_open", "notification_receive", "os_update", "session_start", "user_engagement", "ad_exposure", "adunit_exposure", "ad_query", "ad_activeview", "ad_impression", "ad_click", "ad_reward", "screen_view", "ga_extra_parameter"};

        /* renamed from: b reason: collision with root package name */
        public static final String[] f4139b = {"_cd", "_ae", "_ui", "_ug", "_in", "_au", "_cmp", "_err", "_f", "_v", "_iap", "_nd", "_nf", "_no", "_nr", "_ou", "_s", "_e", "_xa", "_xu", "_aq", "_aa", "_ai", "_ac", "_ar", "_vs", "_ep"};

        public static String a(String str) {
            return ft.a(str, f4138a, f4139b);
        }
    }

    public interface b {
        void a(String str, String str2, Bundle bundle, long j);
    }

    public interface c {
        void a(String str, String str2, Bundle bundle, long j);
    }

    public static final class d extends com.google.firebase.analytics.FirebaseAnalytics.b {

        /* renamed from: a reason: collision with root package name */
        public static final String[] f4140a = {"firebase_conversion", "engagement_time_msec", "exposure_time", "ad_event_id", "ad_unit_id", "firebase_error", "firebase_error_value", "firebase_error_length", "firebase_event_origin", "firebase_screen", "firebase_screen_class", "firebase_screen_id", "firebase_previous_screen", "firebase_previous_class", "firebase_previous_id", "message_device_time", "message_id", "message_name", "message_time", "previous_app_version", "previous_os_version", "topic", "update_with_analytics", "previous_first_open_count", "system_app", "system_app_update", "previous_install_count", "ga_event_id", "ga_extra_params_ct", "ga_group_name", "ga_list_length", "ga_index", "ga_event_name", "campaign_info_source", "deferred_analytics_collection"};

        /* renamed from: b reason: collision with root package name */
        public static final String[] f4141b = {"_c", "_et", "_xt", "_aeid", "_ai", "_err", "_ev", "_el", "_o", "_sn", "_sc", "_si", "_pn", "_pc", "_pi", "_ndt", "_nmid", "_nmn", "_nmt", "_pv", "_po", "_nt", "_uwa", "_pfo", "_sys", "_sysu", "_pin", "_eid", "_epc", "_gn", "_ll", "_i", "_en", "_cis", "_dac"};

        public static String a(String str) {
            return ft.a(str, f4140a, f4141b);
        }
    }

    public static final class e extends com.google.firebase.analytics.FirebaseAnalytics.c {

        /* renamed from: a reason: collision with root package name */
        public static final String[] f4142a = {"firebase_last_notification", "first_open_time", "first_visit_time", "last_deep_link_referrer", "user_id", "first_open_after_install", "lifetime_user_engagement"};

        /* renamed from: b reason: collision with root package name */
        public static final String[] f4143b = {"_ln", "_fot", "_fvt", "_ldl", "_id", "_fi", "_lte"};

        public static String a(String str) {
            return ft.a(str, f4142a, f4143b);
        }
    }

    public AppMeasurement(ce ceVar) {
        aa.a(ceVar);
        this.zzacw = ceVar;
    }

    @Keep
    @Deprecated
    public static AppMeasurement getInstance(Context context) {
        return ce.a(context).i();
    }

    @Keep
    public void beginAdUnitExposure(String str) {
        this.zzacw.v().a(str);
    }

    @Keep
    public void clearConditionalUserProperty(String str, String str2, Bundle bundle) {
        this.zzacw.h().c(str, str2, bundle);
    }

    /* access modifiers changed from: protected */
    @Keep
    public void clearConditionalUserPropertyAs(String str, String str2, String str3, Bundle bundle) {
        this.zzacw.h().a(str, str2, str3, bundle);
    }

    @Keep
    public void endAdUnitExposure(String str) {
        this.zzacw.v().b(str);
    }

    @Keep
    public long generateEventId() {
        return this.zzacw.m().v();
    }

    @Keep
    public String getAppInstanceId() {
        return this.zzacw.h().B();
    }

    public Boolean getBoolean() {
        return this.zzacw.h().v();
    }

    @Keep
    public List<ConditionalUserProperty> getConditionalUserProperties(String str, String str2) {
        return this.zzacw.h().a(str, str2);
    }

    /* access modifiers changed from: protected */
    @Keep
    public List<ConditionalUserProperty> getConditionalUserPropertiesAs(String str, String str2, String str3) {
        return this.zzacw.h().a(str, str2, str3);
    }

    @Keep
    public String getCurrentScreenClass() {
        dx w = this.zzacw.r().w();
        if (w != null) {
            return w.f4004b;
        }
        return null;
    }

    @Keep
    public String getCurrentScreenName() {
        dx w = this.zzacw.r().w();
        if (w != null) {
            return w.f4003a;
        }
        return null;
    }

    public Double getDouble() {
        return this.zzacw.h().z();
    }

    @Keep
    public String getGmpAppId() {
        try {
            return com.google.android.gms.common.api.internal.d.a();
        } catch (IllegalStateException e2) {
            this.zzacw.q().v().a("getGoogleAppId failed with exception", e2);
            return null;
        }
    }

    public Integer getInteger() {
        return this.zzacw.h().y();
    }

    public Long getLong() {
        return this.zzacw.h().x();
    }

    @Keep
    public int getMaxUserProperties(String str) {
        this.zzacw.h();
        aa.a(str);
        return 25;
    }

    public String getString() {
        return this.zzacw.h().w();
    }

    /* access modifiers changed from: protected */
    @Keep
    public Map<String, Object> getUserProperties(String str, String str2, boolean z) {
        return this.zzacw.h().a(str, str2, z);
    }

    public Map<String, Object> getUserProperties(boolean z) {
        List<fq> b2 = this.zzacw.h().b(z);
        android.support.v4.h.a aVar = new android.support.v4.h.a(b2.size());
        for (fq fqVar : b2) {
            aVar.put(fqVar.f4082a, fqVar.a());
        }
        return aVar;
    }

    /* access modifiers changed from: protected */
    @Keep
    public Map<String, Object> getUserPropertiesAs(String str, String str2, String str3, boolean z) {
        return this.zzacw.h().a(str, str2, str3, z);
    }

    public final void logEvent(String str, Bundle bundle) {
        if (bundle == null) {
            bundle = new Bundle();
        }
        this.zzacw.h().a("app", str, bundle, true);
    }

    @Keep
    public void logEventInternal(String str, String str2, Bundle bundle) {
        if (bundle == null) {
            bundle = new Bundle();
        }
        this.zzacw.h().a(str, str2, bundle);
    }

    public void logEventInternalNoInterceptor(String str, String str2, Bundle bundle, long j) {
        this.zzacw.h().a(str, str2, bundle == null ? new Bundle() : bundle, j);
    }

    public void registerOnMeasurementEventListener(c cVar) {
        this.zzacw.h().a(cVar);
    }

    @Keep
    public void setConditionalUserProperty(ConditionalUserProperty conditionalUserProperty) {
        this.zzacw.h().a(conditionalUserProperty);
    }

    /* access modifiers changed from: protected */
    @Keep
    public void setConditionalUserPropertyAs(ConditionalUserProperty conditionalUserProperty) {
        this.zzacw.h().b(conditionalUserProperty);
    }

    public void setEventInterceptor(b bVar) {
        this.zzacw.h().a(bVar);
    }

    @Deprecated
    public void setMeasurementEnabled(boolean z) {
        this.zzacw.h().a(z);
    }

    public final void setMinimumSessionDuration(long j) {
        this.zzacw.h().a(j);
    }

    public final void setSessionTimeoutDuration(long j) {
        this.zzacw.h().b(j);
    }

    public final void setUserProperty(String str, String str2) {
        int c2 = this.zzacw.m().c(str);
        if (c2 != 0) {
            this.zzacw.m();
            this.zzacw.m().a(c2, "_ev", ft.a(str, 24, true), str != null ? str.length() : 0);
            return;
        }
        setUserPropertyInternal("app", str, str2);
    }

    public void setUserPropertyInternal(String str, String str2, Object obj) {
        this.zzacw.h().a(str, str2, obj);
    }

    public void unregisterOnMeasurementEventListener(c cVar) {
        this.zzacw.h().b(cVar);
    }
}
