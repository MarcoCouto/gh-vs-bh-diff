package com.google.android.gms.b;

import android.os.IBinder;
import android.os.IInterface;
import com.google.android.gms.internal.d.b;

public interface a extends IInterface {

    /* renamed from: com.google.android.gms.b.a$a reason: collision with other inner class name */
    public static abstract class C0046a extends b implements a {

        /* renamed from: com.google.android.gms.b.a$a$a reason: collision with other inner class name */
        public static class C0047a extends com.google.android.gms.internal.d.a implements a {
            C0047a(IBinder iBinder) {
                super(iBinder, "com.google.android.gms.dynamic.IObjectWrapper");
            }
        }

        public C0046a() {
            super("com.google.android.gms.dynamic.IObjectWrapper");
        }

        public static a a(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.dynamic.IObjectWrapper");
            return queryLocalInterface instanceof a ? (a) queryLocalInterface : new C0047a(iBinder);
        }
    }
}
