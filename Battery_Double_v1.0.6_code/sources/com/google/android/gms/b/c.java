package com.google.android.gms.b;

import android.content.Context;
import android.os.IBinder;
import com.google.android.gms.common.internal.aa;
import com.google.android.gms.common.n;

public abstract class c<T> {

    /* renamed from: a reason: collision with root package name */
    private final String f2134a;

    /* renamed from: b reason: collision with root package name */
    private T f2135b;

    public static class a extends Exception {
        public a(String str) {
            super(str);
        }

        public a(String str, Throwable th) {
            super(str, th);
        }
    }

    protected c(String str) {
        this.f2134a = str;
    }

    /* access modifiers changed from: protected */
    public final T a(Context context) throws a {
        if (this.f2135b == null) {
            aa.a(context);
            Context remoteContext = n.getRemoteContext(context);
            if (remoteContext == null) {
                throw new a("Could not get remote context.");
            }
            try {
                this.f2135b = a((IBinder) remoteContext.getClassLoader().loadClass(this.f2134a).newInstance());
            } catch (ClassNotFoundException e) {
                throw new a("Could not load creator class.", e);
            } catch (InstantiationException e2) {
                throw new a("Could not instantiate creator.", e2);
            } catch (IllegalAccessException e3) {
                throw new a("Could not access creator.", e3);
            }
        }
        return this.f2135b;
    }

    /* access modifiers changed from: protected */
    public abstract T a(IBinder iBinder);
}
