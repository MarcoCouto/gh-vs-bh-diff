package com.google.android.gms.ads.identifier;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import com.google.android.gms.common.n;

public final class b {

    /* renamed from: a reason: collision with root package name */
    private SharedPreferences f1949a;

    public b(Context context) {
        try {
            Context remoteContext = n.getRemoteContext(context);
            this.f1949a = remoteContext == null ? null : remoteContext.getSharedPreferences("google_ads_flags", 0);
        } catch (Throwable th) {
            Log.w("GmscoreFlag", "Error while getting SharedPreferences ", th);
            this.f1949a = null;
        }
    }

    /* access modifiers changed from: 0000 */
    public final float a(String str, float f) {
        try {
            if (this.f1949a == null) {
                return 0.0f;
            }
            return this.f1949a.getFloat(str, 0.0f);
        } catch (Throwable th) {
            Log.w("GmscoreFlag", "Error while reading from SharedPreferences ", th);
            return 0.0f;
        }
    }

    /* access modifiers changed from: 0000 */
    public final String a(String str, String str2) {
        try {
            return this.f1949a == null ? str2 : this.f1949a.getString(str, str2);
        } catch (Throwable th) {
            Log.w("GmscoreFlag", "Error while reading from SharedPreferences ", th);
            return str2;
        }
    }

    public final boolean a(String str, boolean z) {
        try {
            if (this.f1949a == null) {
                return false;
            }
            return this.f1949a.getBoolean(str, false);
        } catch (Throwable th) {
            Log.w("GmscoreFlag", "Error while reading from SharedPreferences ", th);
            return false;
        }
    }
}
