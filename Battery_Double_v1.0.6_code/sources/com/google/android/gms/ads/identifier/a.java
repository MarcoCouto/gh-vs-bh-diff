package com.google.android.gms.ads.identifier;

import android.net.Uri;
import android.net.Uri.Builder;
import android.util.Log;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

final class a extends Thread {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ Map f1948a;

    a(AdvertisingIdClient advertisingIdClient, Map map) {
        this.f1948a = map;
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    public final void run() {
        HttpURLConnection httpURLConnection;
        new c();
        Map map = this.f1948a;
        Builder buildUpon = Uri.parse("https://pagead2.googlesyndication.com/pagead/gen_204?id=gmob-apps").buildUpon();
        for (String str : map.keySet()) {
            buildUpon.appendQueryParameter(str, (String) map.get(str));
        }
        String uri = buildUpon.build().toString();
        try {
            httpURLConnection = (HttpURLConnection) new URL(uri).openConnection();
            int responseCode = httpURLConnection.getResponseCode();
            if (responseCode < 200 || responseCode >= 300) {
                Log.w("HttpUrlPinger", new StringBuilder(String.valueOf(uri).length() + 65).append("Received non-success response code ").append(responseCode).append(" from pinging URL: ").append(uri).toString());
            }
            httpURLConnection.disconnect();
        } catch (IndexOutOfBoundsException e) {
            String message = e.getMessage();
            Log.w("HttpUrlPinger", new StringBuilder(String.valueOf(uri).length() + 32 + String.valueOf(message).length()).append("Error while parsing ping URL: ").append(uri).append(". ").append(message).toString(), e);
        } catch (IOException | RuntimeException e2) {
            String message2 = e2.getMessage();
            Log.w("HttpUrlPinger", new StringBuilder(String.valueOf(uri).length() + 27 + String.valueOf(message2).length()).append("Error while pinging URL: ").append(uri).append(". ").append(message2).toString(), e2);
        } catch (Throwable th) {
            httpURLConnection.disconnect();
            throw th;
        }
    }
}
