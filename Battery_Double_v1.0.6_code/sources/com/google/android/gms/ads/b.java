package com.google.android.gms.ads;

import android.content.Context;
import android.os.RemoteException;
import com.google.android.gms.ads.b.d;
import com.google.android.gms.common.internal.aa;
import com.google.android.gms.internal.ads.aol;
import com.google.android.gms.internal.ads.aos;
import com.google.android.gms.internal.ads.ape;
import com.google.android.gms.internal.ads.apk;
import com.google.android.gms.internal.ads.apn;
import com.google.android.gms.internal.ads.apq;
import com.google.android.gms.internal.ads.ara;
import com.google.android.gms.internal.ads.aul;
import com.google.android.gms.internal.ads.avx;
import com.google.android.gms.internal.ads.awa;
import com.google.android.gms.internal.ads.awn;
import com.google.android.gms.internal.ads.awy;
import com.google.android.gms.internal.ads.awz;
import com.google.android.gms.internal.ads.axa;
import com.google.android.gms.internal.ads.axc;
import com.google.android.gms.internal.ads.axd;
import com.google.android.gms.internal.ads.bcq;
import com.google.android.gms.internal.ads.bcr;
import com.google.android.gms.internal.ads.ms;

public class b {

    /* renamed from: a reason: collision with root package name */
    private final aos f1919a;

    /* renamed from: b reason: collision with root package name */
    private final Context f1920b;
    private final apn c;

    public static class a {

        /* renamed from: a reason: collision with root package name */
        private final Context f1921a;

        /* renamed from: b reason: collision with root package name */
        private final apq f1922b;

        private a(Context context, apq apq) {
            this.f1921a = context;
            this.f1922b = apq;
        }

        public a(Context context, String str) {
            this((Context) aa.a(context, (Object) "context cannot be null"), ape.b().a(context, str, (bcr) new bcq()));
        }

        public a a(a aVar) {
            try {
                this.f1922b.a((apk) new aol(aVar));
            } catch (RemoteException e) {
                ms.c("Failed to set AdListener.", e);
            }
            return this;
        }

        public a a(d dVar) {
            try {
                this.f1922b.a(new aul(dVar));
            } catch (RemoteException e) {
                ms.c("Failed to specify native ad options", e);
            }
            return this;
        }

        public a a(com.google.android.gms.ads.b.g.a aVar) {
            try {
                this.f1922b.a((avx) new awy(aVar));
            } catch (RemoteException e) {
                ms.c("Failed to add app install ad listener", e);
            }
            return this;
        }

        public a a(com.google.android.gms.ads.b.h.a aVar) {
            try {
                this.f1922b.a((awa) new awz(aVar));
            } catch (RemoteException e) {
                ms.c("Failed to add content ad listener", e);
            }
            return this;
        }

        public a a(com.google.android.gms.ads.b.k.a aVar) {
            try {
                this.f1922b.a((awn) new axd(aVar));
            } catch (RemoteException e) {
                ms.c("Failed to add google native ad listener", e);
            }
            return this;
        }

        public a a(String str, com.google.android.gms.ads.b.i.b bVar, com.google.android.gms.ads.b.i.a aVar) {
            try {
                this.f1922b.a(str, new axc(bVar), aVar == null ? null : new axa(aVar));
            } catch (RemoteException e) {
                ms.c("Failed to add custom template ad listener", e);
            }
            return this;
        }

        public b a() {
            try {
                return new b(this.f1921a, this.f1922b.a());
            } catch (RemoteException e) {
                ms.b("Failed to build AdLoader.", e);
                return null;
            }
        }
    }

    b(Context context, apn apn) {
        this(context, apn, aos.f2813a);
    }

    private b(Context context, apn apn, aos aos) {
        this.f1920b = context;
        this.c = apn;
        this.f1919a = aos;
    }

    private final void a(ara ara) {
        try {
            this.c.a(aos.a(this.f1920b, ara));
        } catch (RemoteException e) {
            ms.b("Failed to load ad.", e);
        }
    }

    public void a(c cVar) {
        a(cVar.a());
    }
}
