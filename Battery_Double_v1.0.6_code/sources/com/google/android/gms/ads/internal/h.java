package com.google.android.gms.ads.internal;

import android.app.Activity;
import android.content.Context;
import android.view.MotionEvent;
import android.view.View;
import com.google.android.gms.internal.ads.ahd;
import com.google.android.gms.internal.ads.ahg;
import com.google.android.gms.internal.ads.ape;
import com.google.android.gms.internal.ads.asi;
import com.google.android.gms.internal.ads.cm;
import com.google.android.gms.internal.ads.jm;
import com.google.android.gms.internal.ads.jt;
import com.google.android.gms.internal.ads.mh;
import com.google.android.gms.internal.ads.mu;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicReference;

@cm
public final class h implements ahd, Runnable {

    /* renamed from: a reason: collision with root package name */
    private final List<Object[]> f2063a;

    /* renamed from: b reason: collision with root package name */
    private final AtomicReference<ahd> f2064b;
    private Context c;
    private mu d;
    private CountDownLatch e;

    private h(Context context, mu muVar) {
        this.f2063a = new Vector();
        this.f2064b = new AtomicReference<>();
        this.e = new CountDownLatch(1);
        this.c = context;
        this.d = muVar;
        ape.a();
        if (mh.b()) {
            jt.a((Runnable) this);
        } else {
            run();
        }
    }

    public h(ay ayVar) {
        this(ayVar.c, ayVar.e);
    }

    private final boolean a() {
        try {
            this.e.await();
            return true;
        } catch (InterruptedException e2) {
            jm.c("Interrupted during GADSignals creation.", e2);
            return false;
        }
    }

    private static Context b(Context context) {
        Context applicationContext = context.getApplicationContext();
        return applicationContext == null ? context : applicationContext;
    }

    private final void b() {
        if (!this.f2063a.isEmpty()) {
            for (Object[] objArr : this.f2063a) {
                if (objArr.length == 1) {
                    ((ahd) this.f2064b.get()).a((MotionEvent) objArr[0]);
                } else if (objArr.length == 3) {
                    ((ahd) this.f2064b.get()).a(((Integer) objArr[0]).intValue(), ((Integer) objArr[1]).intValue(), ((Integer) objArr[2]).intValue());
                }
            }
            this.f2063a.clear();
        }
    }

    public final String a(Context context) {
        if (a()) {
            ahd ahd = (ahd) this.f2064b.get();
            if (ahd != null) {
                b();
                return ahd.a(b(context));
            }
        }
        return "";
    }

    public final String a(Context context, String str, View view) {
        return a(context, str, view, null);
    }

    public final String a(Context context, String str, View view, Activity activity) {
        if (a()) {
            ahd ahd = (ahd) this.f2064b.get();
            if (ahd != null) {
                b();
                return ahd.a(b(context), str, view, activity);
            }
        }
        return "";
    }

    public final void a(int i, int i2, int i3) {
        ahd ahd = (ahd) this.f2064b.get();
        if (ahd != null) {
            b();
            ahd.a(i, i2, i3);
            return;
        }
        this.f2063a.add(new Object[]{Integer.valueOf(i), Integer.valueOf(i2), Integer.valueOf(i3)});
    }

    public final void a(MotionEvent motionEvent) {
        ahd ahd = (ahd) this.f2064b.get();
        if (ahd != null) {
            b();
            ahd.a(motionEvent);
            return;
        }
        this.f2063a.add(new Object[]{motionEvent});
    }

    public final void a(View view) {
        ahd ahd = (ahd) this.f2064b.get();
        if (ahd != null) {
            ahd.a(view);
        }
    }

    public final void run() {
        try {
            this.f2064b.set(ahg.a(this.d.f3528a, b(this.c), !((Boolean) ape.f().a(asi.aL)).booleanValue() && (this.d.d)));
        } finally {
            this.e.countDown();
            this.c = null;
            this.d = null;
        }
    }
}
