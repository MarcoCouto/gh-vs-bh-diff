package com.google.android.gms.ads.internal;

import com.google.android.gms.internal.ads.akr;
import com.google.android.gms.internal.ads.akv;
import com.google.android.gms.internal.ads.ir;
import com.google.android.gms.internal.ads.ry;

final /* synthetic */ class b implements ry {

    /* renamed from: a reason: collision with root package name */
    private final akr f1993a;

    /* renamed from: b reason: collision with root package name */
    private final ir f1994b;

    b(akr akr, ir irVar) {
        this.f1993a = akr;
        this.f1994b = irVar;
    }

    public final void a() {
        this.f1993a.a((akv) this.f1994b.f3396b);
    }
}
