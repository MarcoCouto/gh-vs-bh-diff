package com.google.android.gms.ads.internal;

import android.os.RemoteException;
import com.google.android.gms.internal.ads.atn;
import com.google.android.gms.internal.ads.jm;

final class ai implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ atn f1964a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ ae f1965b;

    ai(ae aeVar, atn atn) {
        this.f1965b = aeVar;
        this.f1964a = atn;
    }

    public final void run() {
        try {
            if (this.f1965b.e.r != null) {
                this.f1965b.e.r.a(this.f1964a);
                this.f1965b.a(this.f1964a.j());
            }
        } catch (RemoteException e) {
            jm.d("#007 Could not call remote method.", e);
        }
    }
}
