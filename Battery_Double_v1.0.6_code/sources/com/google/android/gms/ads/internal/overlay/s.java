package com.google.android.gms.ads.internal.overlay;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.RemoteException;
import com.google.android.gms.ads.internal.ax;
import com.google.android.gms.b.a;
import com.google.android.gms.internal.ads.cm;

@cm
public final class s extends com.google.android.gms.internal.ads.s {

    /* renamed from: a reason: collision with root package name */
    private AdOverlayInfoParcel f2095a;

    /* renamed from: b reason: collision with root package name */
    private Activity f2096b;
    private boolean c = false;
    private boolean d = false;

    public s(Activity activity, AdOverlayInfoParcel adOverlayInfoParcel) {
        this.f2095a = adOverlayInfoParcel;
        this.f2096b = activity;
    }

    private final synchronized void a() {
        if (!this.d) {
            if (this.f2095a.c != null) {
                this.f2095a.c.p_();
            }
            this.d = true;
        }
    }

    public final void a(int i, int i2, Intent intent) throws RemoteException {
    }

    public final void a(Bundle bundle) {
        boolean z = false;
        if (bundle != null) {
            z = bundle.getBoolean("com.google.android.gms.ads.internal.overlay.hasResumed", false);
        }
        if (this.f2095a == null) {
            this.f2096b.finish();
        } else if (z) {
            this.f2096b.finish();
        } else {
            if (bundle == null) {
                if (this.f2095a.f2077b != null) {
                    this.f2095a.f2077b.e();
                }
                if (!(this.f2096b.getIntent() == null || !this.f2096b.getIntent().getBooleanExtra("shouldCallOnOverlayOpened", true) || this.f2095a.c == null)) {
                    this.f2095a.c.g();
                }
            }
            ax.b();
            if (!a.a((Context) this.f2096b, this.f2095a.f2076a, this.f2095a.i)) {
                this.f2096b.finish();
            }
        }
    }

    public final void a(a aVar) throws RemoteException {
    }

    public final void b(Bundle bundle) throws RemoteException {
        bundle.putBoolean("com.google.android.gms.ads.internal.overlay.hasResumed", this.c);
    }

    public final void d() throws RemoteException {
    }

    public final boolean e() throws RemoteException {
        return false;
    }

    public final void f() throws RemoteException {
    }

    public final void g() throws RemoteException {
    }

    public final void h() throws RemoteException {
        if (this.c) {
            this.f2096b.finish();
            return;
        }
        this.c = true;
        if (this.f2095a.c != null) {
            this.f2095a.c.f();
        }
    }

    public final void i() throws RemoteException {
        if (this.f2095a.c != null) {
            this.f2095a.c.d();
        }
        if (this.f2096b.isFinishing()) {
            a();
        }
    }

    public final void j() throws RemoteException {
        if (this.f2096b.isFinishing()) {
            a();
        }
    }

    public final void k() throws RemoteException {
        if (this.f2096b.isFinishing()) {
            a();
        }
    }

    public final void l() throws RemoteException {
    }
}
