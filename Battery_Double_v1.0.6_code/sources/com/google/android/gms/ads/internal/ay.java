package com.google.android.gms.ads.internal;

import android.content.Context;
import android.graphics.Rect;
import android.os.RemoteException;
import android.support.v4.h.m;
import android.view.View;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.ViewTreeObserver.OnScrollChangedListener;
import com.google.android.gms.internal.ads.ahd;
import com.google.android.gms.internal.ads.ahh;
import com.google.android.gms.internal.ads.aot;
import com.google.android.gms.internal.ads.ape;
import com.google.android.gms.internal.ads.aph;
import com.google.android.gms.internal.ads.apk;
import com.google.android.gms.internal.ads.aqa;
import com.google.android.gms.internal.ads.aqe;
import com.google.android.gms.internal.ads.aqk;
import com.google.android.gms.internal.ads.aqy;
import com.google.android.gms.internal.ads.arr;
import com.google.android.gms.internal.ads.asi;
import com.google.android.gms.internal.ads.atc;
import com.google.android.gms.internal.ads.aul;
import com.google.android.gms.internal.ads.avx;
import com.google.android.gms.internal.ads.awa;
import com.google.android.gms.internal.ads.awe;
import com.google.android.gms.internal.ads.awh;
import com.google.android.gms.internal.ads.awk;
import com.google.android.gms.internal.ads.awn;
import com.google.android.gms.internal.ads.cm;
import com.google.android.gms.internal.ads.gf;
import com.google.android.gms.internal.ads.gn;
import com.google.android.gms.internal.ads.ir;
import com.google.android.gms.internal.ads.is;
import com.google.android.gms.internal.ads.it;
import com.google.android.gms.internal.ads.je;
import com.google.android.gms.internal.ads.jh;
import com.google.android.gms.internal.ads.jm;
import com.google.android.gms.internal.ads.ko;
import com.google.android.gms.internal.ads.lw;
import com.google.android.gms.internal.ads.mh;
import com.google.android.gms.internal.ads.mu;
import com.google.android.gms.internal.ads.rv;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;

@cm
public final class ay implements OnGlobalLayoutListener, OnScrollChangedListener {
    List<Integer> A;
    atc B;
    gn C;
    gf D;
    public String E;
    List<String> F;
    public je G;
    View H;
    public int I;
    boolean J;
    private HashSet<it> K;
    private int L;
    private int M;
    private lw N;
    private boolean O;
    private boolean P;
    private boolean Q;

    /* renamed from: a reason: collision with root package name */
    final String f1989a;

    /* renamed from: b reason: collision with root package name */
    public String f1990b;
    public final Context c;
    final ahh d;
    public final mu e;
    az f;
    public jh g;
    public ko h;
    public aot i;
    public ir j;
    public is k;
    public it l;
    aph m;
    apk n;
    aqe o;
    aqa p;
    aqk q;
    avx r;
    awa s;
    awn t;
    m<String, awe> u;
    m<String, awh> v;
    aul w;
    arr x;
    aqy y;
    awk z;

    public ay(Context context, aot aot, String str, mu muVar) {
        this(context, aot, str, muVar, null);
    }

    private ay(Context context, aot aot, String str, mu muVar, ahh ahh) {
        this.G = null;
        this.H = null;
        this.I = 0;
        this.J = false;
        this.K = null;
        this.L = -1;
        this.M = -1;
        this.O = true;
        this.P = true;
        this.Q = false;
        asi.a(context);
        if (ax.i().b() != null) {
            List b2 = asi.b();
            if (muVar.f3529b != 0) {
                b2.add(Integer.toString(muVar.f3529b));
            }
            ax.i().b().a(b2);
        }
        this.f1989a = UUID.randomUUID().toString();
        if (aot.d || aot.h) {
            this.f = null;
        } else {
            this.f = new az(context, str, muVar.f3528a, this, this);
            this.f.setMinimumWidth(aot.f);
            this.f.setMinimumHeight(aot.c);
            this.f.setVisibility(4);
        }
        this.i = aot;
        this.f1990b = str;
        this.c = context;
        this.e = muVar;
        this.d = new ahh(new h(this));
        this.N = new lw(200);
        this.v = new m<>();
    }

    private final void b(boolean z2) {
        boolean z3 = true;
        if (this.f != null && this.j != null && this.j.f3396b != null && this.j.f3396b.v() != null) {
            if (!z2 || this.N.a()) {
                if (this.j.f3396b.v().b()) {
                    int[] iArr = new int[2];
                    this.f.getLocationOnScreen(iArr);
                    ape.a();
                    int b2 = mh.b(this.c, iArr[0]);
                    ape.a();
                    int b3 = mh.b(this.c, iArr[1]);
                    if (!(b2 == this.L && b3 == this.M)) {
                        this.L = b2;
                        this.M = b3;
                        rv v2 = this.j.f3396b.v();
                        int i2 = this.L;
                        int i3 = this.M;
                        if (z2) {
                            z3 = false;
                        }
                        v2.a(i2, i3, z3);
                    }
                }
                if (this.f != null) {
                    View findViewById = this.f.getRootView().findViewById(16908290);
                    if (findViewById != null) {
                        Rect rect = new Rect();
                        Rect rect2 = new Rect();
                        this.f.getGlobalVisibleRect(rect);
                        findViewById.getGlobalVisibleRect(rect2);
                        if (rect.top != rect2.top) {
                            this.O = false;
                        }
                        if (rect.bottom != rect2.bottom) {
                            this.P = false;
                        }
                    }
                }
            }
        }
    }

    public final HashSet<it> a() {
        return this.K;
    }

    /* access modifiers changed from: 0000 */
    public final void a(View view) {
        if (((Boolean) ape.f().a(asi.bG)).booleanValue()) {
            ahd a2 = this.d.a();
            if (a2 != null) {
                a2.a(view);
            }
        }
    }

    public final void a(HashSet<it> hashSet) {
        this.K = hashSet;
    }

    public final void a(boolean z2) {
        if (!(this.I != 0 || this.j == null || this.j.f3396b == null)) {
            this.j.f3396b.stopLoading();
        }
        if (this.g != null) {
            this.g.b();
        }
        if (this.h != null) {
            this.h.b();
        }
        if (z2) {
            this.j = null;
        }
    }

    public final void b() {
        if (this.j != null && this.j.f3396b != null) {
            this.j.f3396b.destroy();
        }
    }

    public final void c() {
        if (this.j != null && this.j.p != null) {
            try {
                this.j.p.c();
            } catch (RemoteException e2) {
                jm.e("Could not destroy mediation adapter.");
            }
        }
    }

    public final boolean d() {
        return this.I == 0;
    }

    public final boolean e() {
        return this.I == 1;
    }

    public final String f() {
        return (!this.O || !this.P) ? this.O ? this.Q ? "top-scrollable" : "top-locked" : this.P ? this.Q ? "bottom-scrollable" : "bottom-locked" : "" : "";
    }

    public final void onGlobalLayout() {
        b(false);
    }

    public final void onScrollChanged() {
        b(true);
        this.Q = true;
    }
}
