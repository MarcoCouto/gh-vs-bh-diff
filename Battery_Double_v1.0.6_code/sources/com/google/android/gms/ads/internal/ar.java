package com.google.android.gms.ads.internal;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.net.Uri.Builder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.RemoteException;
import android.text.TextUtils;
import android.view.ViewGroup.LayoutParams;
import android.webkit.WebView;
import com.google.android.gms.b.a;
import com.google.android.gms.b.b;
import com.google.android.gms.common.internal.aa;
import com.google.android.gms.internal.ads.af;
import com.google.android.gms.internal.ads.ahh;
import com.google.android.gms.internal.ads.ahi;
import com.google.android.gms.internal.ads.aop;
import com.google.android.gms.internal.ads.aot;
import com.google.android.gms.internal.ads.ape;
import com.google.android.gms.internal.ads.aph;
import com.google.android.gms.internal.ads.apk;
import com.google.android.gms.internal.ads.apw;
import com.google.android.gms.internal.ads.aqa;
import com.google.android.gms.internal.ads.aqe;
import com.google.android.gms.internal.ads.aqk;
import com.google.android.gms.internal.ads.aqs;
import com.google.android.gms.internal.ads.aqy;
import com.google.android.gms.internal.ads.arr;
import com.google.android.gms.internal.ads.asi;
import com.google.android.gms.internal.ads.atc;
import com.google.android.gms.internal.ads.cm;
import com.google.android.gms.internal.ads.gn;
import com.google.android.gms.internal.ads.jm;
import com.google.android.gms.internal.ads.jt;
import com.google.android.gms.internal.ads.mh;
import com.google.android.gms.internal.ads.mu;
import com.google.android.gms.internal.ads.y;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

@cm
public final class ar extends apw {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public final mu f1979a;

    /* renamed from: b reason: collision with root package name */
    private final aot f1980b;
    /* access modifiers changed from: private */
    public final Future<ahh> c = jt.a((Callable<T>) new au<T>(this));
    /* access modifiers changed from: private */
    public final Context d;
    private final aw e;
    /* access modifiers changed from: private */
    public WebView f = new WebView(this.d);
    /* access modifiers changed from: private */
    public apk g;
    /* access modifiers changed from: private */
    public ahh h;
    private AsyncTask<Void, Void, String> i;

    public ar(Context context, aot aot, String str, mu muVar) {
        this.d = context;
        this.f1979a = muVar;
        this.f1980b = aot;
        this.e = new aw(str);
        a(0);
        this.f.setVerticalScrollBarEnabled(false);
        this.f.getSettings().setJavaScriptEnabled(true);
        this.f.setWebViewClient(new as(this));
        this.f.setOnTouchListener(new at(this));
    }

    /* access modifiers changed from: private */
    public final String c(String str) {
        if (this.h == null) {
            return str;
        }
        Uri parse = Uri.parse(str);
        try {
            parse = this.h.a(parse, this.d, null, null);
        } catch (ahi e2) {
            jm.c("Unable to process ad data", e2);
        }
        return parse.toString();
    }

    /* access modifiers changed from: private */
    public final void d(String str) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setData(Uri.parse(str));
        this.d.startActivity(intent);
    }

    public final String D() {
        throw new IllegalStateException("getAdUnitId not implemented");
    }

    public final aqe E() {
        throw new IllegalStateException("getIAppEventListener not implemented");
    }

    public final apk F() {
        throw new IllegalStateException("getIAdListener not implemented");
    }

    public final void I() throws RemoteException {
        throw new IllegalStateException("Unused method");
    }

    public final String a() throws RemoteException {
        return null;
    }

    /* access modifiers changed from: 0000 */
    public final void a(int i2) {
        if (this.f != null) {
            this.f.setLayoutParams(new LayoutParams(-1, i2));
        }
    }

    public final void a(af afVar, String str) throws RemoteException {
        throw new IllegalStateException("Unused method");
    }

    public final void a(aot aot) throws RemoteException {
        throw new IllegalStateException("AdSize must be set before initialization");
    }

    public final void a(aph aph) throws RemoteException {
        throw new IllegalStateException("Unused method");
    }

    public final void a(apk apk) throws RemoteException {
        this.g = apk;
    }

    public final void a(aqa aqa) {
        throw new IllegalStateException("Unused method");
    }

    public final void a(aqe aqe) throws RemoteException {
        throw new IllegalStateException("Unused method");
    }

    public final void a(aqk aqk) throws RemoteException {
        throw new IllegalStateException("Unused method");
    }

    public final void a(aqy aqy) {
        throw new IllegalStateException("Unused method");
    }

    public final void a(arr arr) {
        throw new IllegalStateException("Unused method");
    }

    public final void a(atc atc) throws RemoteException {
        throw new IllegalStateException("Unused method");
    }

    public final void a(gn gnVar) throws RemoteException {
        throw new IllegalStateException("Unused method");
    }

    public final void a(y yVar) throws RemoteException {
        throw new IllegalStateException("Unused method");
    }

    public final void a(String str) throws RemoteException {
        throw new IllegalStateException("Unused method");
    }

    /* access modifiers changed from: 0000 */
    public final int b(String str) {
        int i2 = 0;
        String queryParameter = Uri.parse(str).getQueryParameter("height");
        if (TextUtils.isEmpty(queryParameter)) {
            return i2;
        }
        try {
            ape.a();
            return mh.a(this.d, Integer.parseInt(queryParameter));
        } catch (NumberFormatException e2) {
            return i2;
        }
    }

    public final void b(boolean z) throws RemoteException {
    }

    public final boolean b(aop aop) throws RemoteException {
        aa.a(this.f, (Object) "This Search Ad has already been torn down");
        this.e.a(aop, this.f1979a);
        this.i = new av(this, null).execute(new Void[0]);
        return true;
    }

    /* access modifiers changed from: 0000 */
    public final String c() {
        Builder builder = new Builder();
        builder.scheme("https://").appendEncodedPath((String) ape.f().a(asi.cx));
        builder.appendQueryParameter("query", this.e.b());
        builder.appendQueryParameter("pubId", this.e.c());
        Map d2 = this.e.d();
        for (String str : d2.keySet()) {
            builder.appendQueryParameter(str, (String) d2.get(str));
        }
        Uri build = builder.build();
        if (this.h != null) {
            try {
                build = this.h.a(build, this.d);
            } catch (ahi e2) {
                jm.c("Unable to process ad data", e2);
            }
        }
        String d3 = d();
        String encodedQuery = build.getEncodedQuery();
        return new StringBuilder(String.valueOf(d3).length() + 1 + String.valueOf(encodedQuery).length()).append(d3).append("#").append(encodedQuery).toString();
    }

    public final void c(boolean z) {
        throw new IllegalStateException("Unused method");
    }

    /* access modifiers changed from: 0000 */
    public final String d() {
        String a2 = this.e.a();
        String str = TextUtils.isEmpty(a2) ? "www.google.com" : a2;
        String str2 = (String) ape.f().a(asi.cx);
        return new StringBuilder(String.valueOf(str).length() + 8 + String.valueOf(str2).length()).append("https://").append(str).append(str2).toString();
    }

    public final void j() throws RemoteException {
        aa.b("destroy must be called on the main UI thread.");
        this.i.cancel(true);
        this.c.cancel(true);
        this.f.destroy();
        this.f = null;
    }

    public final a k() throws RemoteException {
        aa.b("getAdFrame must be called on the main UI thread.");
        return b.a(this.f);
    }

    public final aot l() throws RemoteException {
        return this.f1980b;
    }

    public final boolean m() throws RemoteException {
        return false;
    }

    public final void n() throws RemoteException {
        throw new IllegalStateException("Unused method");
    }

    public final void o() throws RemoteException {
        aa.b("pause must be called on the main UI thread.");
    }

    public final void p() throws RemoteException {
        aa.b("resume must be called on the main UI thread.");
    }

    public final Bundle q() {
        throw new IllegalStateException("Unused method");
    }

    public final String q_() throws RemoteException {
        return null;
    }

    public final void r() throws RemoteException {
    }

    public final boolean s() throws RemoteException {
        return false;
    }

    public final aqs t() {
        return null;
    }
}
