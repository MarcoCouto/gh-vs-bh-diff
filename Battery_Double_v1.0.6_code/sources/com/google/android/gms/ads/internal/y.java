package com.google.android.gms.ads.internal;

import android.os.RemoteException;
import android.view.View;
import com.google.android.gms.ads.internal.gmsg.ae;
import com.google.android.gms.b.b;
import com.google.android.gms.internal.ads.bdd;
import com.google.android.gms.internal.ads.bdh;
import com.google.android.gms.internal.ads.jm;
import com.google.android.gms.internal.ads.qn;
import java.util.Map;

final class y implements ae<qn> {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ bdd f2109a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ d f2110b;
    private final /* synthetic */ bdh c;

    y(bdd bdd, d dVar, bdh bdh) {
        this.f2109a = bdd;
        this.f2110b = dVar;
        this.c = bdh;
    }

    public final /* synthetic */ void zza(Object obj, Map map) {
        qn qnVar = (qn) obj;
        View view = qnVar.getView();
        if (view != null) {
            try {
                if (this.f2109a != null) {
                    if (!this.f2109a.k()) {
                        this.f2109a.a(b.a(view));
                        this.f2110b.f2026a.e();
                        return;
                    }
                    t.b(qnVar);
                } else if (this.c == null) {
                } else {
                    if (!this.c.i()) {
                        this.c.a(b.a(view));
                        this.f2110b.f2026a.e();
                        return;
                    }
                    t.b(qnVar);
                }
            } catch (RemoteException e) {
                jm.c("Unable to call handleClick on mapper", e);
            }
        }
    }
}
