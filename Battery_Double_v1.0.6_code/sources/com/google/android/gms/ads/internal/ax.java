package com.google.android.gms.ads.internal;

import android.os.Build.VERSION;
import com.google.android.gms.ads.internal.overlay.a;
import com.google.android.gms.ads.internal.overlay.l;
import com.google.android.gms.ads.internal.overlay.u;
import com.google.android.gms.ads.internal.overlay.v;
import com.google.android.gms.common.util.e;
import com.google.android.gms.common.util.h;
import com.google.android.gms.internal.ads.alj;
import com.google.android.gms.internal.ads.amg;
import com.google.android.gms.internal.ads.amh;
import com.google.android.gms.internal.ads.amv;
import com.google.android.gms.internal.ads.ao;
import com.google.android.gms.internal.ads.asn;
import com.google.android.gms.internal.ads.ayw;
import com.google.android.gms.internal.ads.azr;
import com.google.android.gms.internal.ads.bbj;
import com.google.android.gms.internal.ads.bck;
import com.google.android.gms.internal.ads.cm;
import com.google.android.gms.internal.ads.cn;
import com.google.android.gms.internal.ads.fk;
import com.google.android.gms.internal.ads.ih;
import com.google.android.gms.internal.ads.iw;
import com.google.android.gms.internal.ads.jf;
import com.google.android.gms.internal.ads.jv;
import com.google.android.gms.internal.ads.kb;
import com.google.android.gms.internal.ads.kg;
import com.google.android.gms.internal.ads.kh;
import com.google.android.gms.internal.ads.ki;
import com.google.android.gms.internal.ads.kj;
import com.google.android.gms.internal.ads.kk;
import com.google.android.gms.internal.ads.km;
import com.google.android.gms.internal.ads.kn;
import com.google.android.gms.internal.ads.kw;
import com.google.android.gms.internal.ads.lt;
import com.google.android.gms.internal.ads.lu;
import com.google.android.gms.internal.ads.md;
import com.google.android.gms.internal.ads.nz;
import com.google.android.gms.internal.ads.og;
import com.google.android.gms.internal.ads.p;
import com.google.android.gms.internal.ads.px;
import com.google.android.gms.internal.ads.qu;

@cm
public final class ax {

    /* renamed from: a reason: collision with root package name */
    private static final Object f1987a = new Object();

    /* renamed from: b reason: collision with root package name */
    private static ax f1988b;
    private final ad A;
    private final p B;
    private final amv C;
    private final ih D;
    private final px E;
    private final og F;
    private final azr G;
    private final kn H;
    private final md I;
    private final jf J;
    private final a c = new a();
    private final cn d = new cn();
    private final l e = new l();
    private final ao f = new ao();
    private final jv g = new jv();
    private final qu h = new qu();
    private final kb i;
    private final alj j;
    private final iw k;
    private final amg l;
    private final amh m;
    private final e n;
    private final e o;
    private final asn p;
    private final kw q;
    private final fk r;
    private final nz s;
    private final ayw t;
    private final bbj u;
    private final lt v;
    private final u w;
    private final v x;
    private final bck y;
    private final lu z;

    static {
        ax axVar = new ax();
        synchronized (f1987a) {
            f1988b = axVar;
        }
    }

    protected ax() {
        int i2 = VERSION.SDK_INT;
        kb kgVar = i2 >= 21 ? new km() : i2 >= 19 ? new kk() : i2 >= 18 ? new ki() : i2 >= 17 ? new kh() : i2 >= 16 ? new kj() : new kg();
        this.i = kgVar;
        this.j = new alj();
        this.k = new iw();
        this.J = new jf();
        this.l = new amg();
        this.m = new amh();
        this.n = h.d();
        this.o = new e();
        this.p = new asn();
        this.q = new kw();
        this.r = new fk();
        this.G = new azr();
        this.s = new nz();
        this.t = new ayw();
        this.u = new bbj();
        this.v = new lt();
        this.w = new u();
        this.x = new v();
        this.y = new bck();
        this.z = new lu();
        this.A = new ad();
        this.B = new p();
        this.C = new amv();
        this.D = new ih();
        this.E = new px();
        this.F = new og();
        this.H = new kn();
        this.I = new md();
    }

    public static og A() {
        return F().F;
    }

    public static ih B() {
        return F().D;
    }

    public static azr C() {
        return F().G;
    }

    public static kn D() {
        return F().H;
    }

    public static md E() {
        return F().I;
    }

    private static ax F() {
        ax axVar;
        synchronized (f1987a) {
            axVar = f1988b;
        }
        return axVar;
    }

    public static cn a() {
        return F().d;
    }

    public static a b() {
        return F().c;
    }

    public static l c() {
        return F().e;
    }

    public static ao d() {
        return F().f;
    }

    public static jv e() {
        return F().g;
    }

    public static qu f() {
        return F().h;
    }

    public static kb g() {
        return F().i;
    }

    public static alj h() {
        return F().j;
    }

    public static iw i() {
        return F().k;
    }

    public static jf j() {
        return F().J;
    }

    public static amh k() {
        return F().m;
    }

    public static e l() {
        return F().n;
    }

    public static e m() {
        return F().o;
    }

    public static asn n() {
        return F().p;
    }

    public static kw o() {
        return F().q;
    }

    public static fk p() {
        return F().r;
    }

    public static nz q() {
        return F().s;
    }

    public static ayw r() {
        return F().t;
    }

    public static bbj s() {
        return F().u;
    }

    public static lt t() {
        return F().v;
    }

    public static p u() {
        return F().B;
    }

    public static u v() {
        return F().w;
    }

    public static v w() {
        return F().x;
    }

    public static bck x() {
        return F().y;
    }

    public static lu y() {
        return F().z;
    }

    public static px z() {
        return F().E;
    }
}
