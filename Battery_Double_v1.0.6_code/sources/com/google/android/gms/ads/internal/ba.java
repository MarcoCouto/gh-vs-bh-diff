package com.google.android.gms.ads.internal;

import com.google.android.gms.internal.ads.amx;
import com.google.android.gms.internal.ads.ann;
import com.google.android.gms.internal.ads.is;

final class ba implements amx {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ is f1995a;

    ba(a aVar, is isVar) {
        this.f1995a = isVar;
    }

    public final void a(ann ann) {
        ann.f2764a = this.f1995a.f3397a.v;
    }
}
