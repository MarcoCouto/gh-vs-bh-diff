package com.google.android.gms.ads.internal.gmsg;

import com.google.android.gms.internal.ads.cm;
import com.google.android.gms.internal.ads.jm;
import java.util.Map;

@cm
public final class ah implements ae<Object> {

    /* renamed from: a reason: collision with root package name */
    private final ai f2043a;

    public ah(ai aiVar) {
        this.f2043a = aiVar;
    }

    public final void zza(Object obj, Map<String, String> map) {
        float f;
        boolean equals = "1".equals(map.get("transparentBackground"));
        boolean equals2 = "1".equals(map.get("blur"));
        try {
            if (map.get("blurRadius") != null) {
                f = Float.parseFloat((String) map.get("blurRadius"));
                this.f2043a.a(equals);
                this.f2043a.a(equals2, f);
            }
        } catch (NumberFormatException e) {
            jm.b("Fail to parse float", e);
        }
        f = 0.0f;
        this.f2043a.a(equals);
        this.f2043a.a(equals2, f);
    }
}
