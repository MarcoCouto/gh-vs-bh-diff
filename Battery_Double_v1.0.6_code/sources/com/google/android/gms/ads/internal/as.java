package com.google.android.gms.ads.internal;

import android.os.RemoteException;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.google.android.gms.internal.ads.ape;
import com.google.android.gms.internal.ads.asi;
import com.google.android.gms.internal.ads.jm;

final class as extends WebViewClient {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ ar f1981a;

    as(ar arVar) {
        this.f1981a = arVar;
    }

    public final void onReceivedError(WebView webView, WebResourceRequest webResourceRequest, WebResourceError webResourceError) {
        if (this.f1981a.g != null) {
            try {
                this.f1981a.g.a(0);
            } catch (RemoteException e) {
                jm.d("#007 Could not call remote method.", e);
            }
        }
    }

    public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
        if (str.startsWith(this.f1981a.d())) {
            return false;
        }
        if (str.startsWith((String) ape.f().a(asi.cu))) {
            if (this.f1981a.g != null) {
                try {
                    this.f1981a.g.a(3);
                } catch (RemoteException e) {
                    jm.d("#007 Could not call remote method.", e);
                }
            }
            this.f1981a.a(0);
            return true;
        }
        if (str.startsWith((String) ape.f().a(asi.cv))) {
            if (this.f1981a.g != null) {
                try {
                    this.f1981a.g.a(0);
                } catch (RemoteException e2) {
                    jm.d("#007 Could not call remote method.", e2);
                }
            }
            this.f1981a.a(0);
            return true;
        }
        if (str.startsWith((String) ape.f().a(asi.cw))) {
            if (this.f1981a.g != null) {
                try {
                    this.f1981a.g.c();
                } catch (RemoteException e3) {
                    jm.d("#007 Could not call remote method.", e3);
                }
            }
            this.f1981a.a(this.f1981a.b(str));
            return true;
        } else if (str.startsWith("gmsg://")) {
            return true;
        } else {
            if (this.f1981a.g != null) {
                try {
                    this.f1981a.g.b();
                } catch (RemoteException e4) {
                    jm.d("#007 Could not call remote method.", e4);
                }
            }
            this.f1981a.d(this.f1981a.c(str));
            return true;
        }
    }
}
