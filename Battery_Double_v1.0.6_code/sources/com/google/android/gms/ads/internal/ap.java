package com.google.android.gms.ads.internal;

import android.os.Handler;

public final class ap {

    /* renamed from: a reason: collision with root package name */
    private final Handler f1978a;

    public ap(Handler handler) {
        this.f1978a = handler;
    }

    public final void a(Runnable runnable) {
        this.f1978a.removeCallbacks(runnable);
    }

    public final boolean a(Runnable runnable, long j) {
        return this.f1978a.postDelayed(runnable, j);
    }
}
