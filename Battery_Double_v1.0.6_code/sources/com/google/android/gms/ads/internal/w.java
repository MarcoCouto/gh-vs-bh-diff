package com.google.android.gms.ads.internal;

import com.google.android.gms.ads.internal.gmsg.ae;
import com.google.android.gms.internal.ads.qn;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

final class w implements ae<qn> {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ CountDownLatch f2107a;

    w(CountDownLatch countDownLatch) {
        this.f2107a = countDownLatch;
    }

    public final /* synthetic */ void zza(Object obj, Map map) {
        qn qnVar = (qn) obj;
        this.f2107a.countDown();
        qnVar.getView().setVisibility(0);
    }
}
