package com.google.android.gms.ads.internal;

import com.google.android.gms.internal.ads.hc;
import com.google.android.gms.internal.ads.hp;

final class o implements hc {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ m f2075a;

    o(m mVar) {
        this.f2075a = mVar;
    }

    public final void a(hp hpVar) {
        this.f2075a.a(hpVar);
    }

    public final void b() {
        this.f2075a.d_();
    }

    public final void c() {
        this.f2075a.p_();
    }

    public final void l_() {
        this.f2075a.g();
    }

    public final void m_() {
        this.f2075a.e_();
    }

    public final void n_() {
        this.f2075a.e();
    }

    public final void o_() {
        this.f2075a.v();
    }
}
