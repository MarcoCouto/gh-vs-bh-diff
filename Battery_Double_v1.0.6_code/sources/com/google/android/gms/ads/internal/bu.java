package com.google.android.gms.ads.internal;

import android.content.Context;
import com.google.android.gms.internal.ads.anb;
import com.google.android.gms.internal.ads.cm;
import com.google.android.gms.internal.ads.hx;
import com.google.android.gms.internal.ads.hy;
import com.google.android.gms.internal.ads.id;
import com.google.android.gms.internal.ads.ox;
import com.google.android.gms.internal.ads.pe;
import com.google.android.gms.internal.ads.ps;
import com.google.android.gms.internal.ads.qd;

@cm
public final class bu {

    /* renamed from: a reason: collision with root package name */
    public final qd f2019a;

    /* renamed from: b reason: collision with root package name */
    public final ox f2020b;
    public final id c;
    public final anb d;

    private bu(qd qdVar, ox oxVar, id idVar, anb anb) {
        this.f2019a = qdVar;
        this.f2020b = oxVar;
        this.c = idVar;
        this.d = anb;
    }

    public static bu a(Context context) {
        return new bu(new ps(), new pe(), new hx(new hy()), new anb(context));
    }
}
