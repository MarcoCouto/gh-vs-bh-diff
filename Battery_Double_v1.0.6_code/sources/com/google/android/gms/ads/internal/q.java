package com.google.android.gms.ads.internal;

import com.google.android.gms.ads.internal.overlay.AdOverlayInfoParcel;
import com.google.android.gms.ads.internal.overlay.l;

final class q implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ AdOverlayInfoParcel f2099a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ p f2100b;

    q(p pVar, AdOverlayInfoParcel adOverlayInfoParcel) {
        this.f2100b = pVar;
        this.f2099a = adOverlayInfoParcel;
    }

    public final void run() {
        ax.c();
        l.a(this.f2100b.f2097a.e.c, this.f2099a, true);
    }
}
