package com.google.android.gms.ads.internal;

import com.google.android.gms.internal.ads.atn;
import com.google.android.gms.internal.ads.qn;
import com.google.android.gms.internal.ads.rw;

final /* synthetic */ class u implements rw {

    /* renamed from: a reason: collision with root package name */
    private final atn f2103a;

    /* renamed from: b reason: collision with root package name */
    private final String f2104b;
    private final qn c;

    u(atn atn, String str, qn qnVar) {
        this.f2103a = atn;
        this.f2104b = str;
        this.c = qnVar;
    }

    public final void a(boolean z) {
        t.a(this.f2103a, this.f2104b, this.c, z);
    }
}
