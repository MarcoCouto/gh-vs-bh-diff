package com.google.android.gms.ads.internal;

import com.google.android.gms.internal.ads.aop;

final class k implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ aop f2069a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ int f2070b;
    private final /* synthetic */ i c;

    k(i iVar, aop aop, int i) {
        this.c = iVar;
        this.f2069a = aop;
        this.f2070b = i;
    }

    public final void run() {
        synchronized (this.c.s) {
            this.c.b(this.f2069a, this.f2070b);
        }
    }
}
