package com.google.android.gms.ads.internal.overlay;

import android.content.Context;
import android.view.MotionEvent;
import android.widget.RelativeLayout;
import com.google.android.gms.internal.ads.cm;
import com.google.android.gms.internal.ads.kp;

@cm
final class h extends RelativeLayout {

    /* renamed from: a reason: collision with root package name */
    boolean f2084a;

    /* renamed from: b reason: collision with root package name */
    private kp f2085b;

    public h(Context context, String str, String str2) {
        super(context);
        this.f2085b = new kp(context, str);
        this.f2085b.b(str2);
    }

    public final boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        if (!this.f2084a) {
            this.f2085b.a(motionEvent);
        }
        return false;
    }
}
