package com.google.android.gms.ads.internal.gmsg;

import android.text.TextUtils;
import com.google.android.gms.internal.ads.cm;
import com.google.android.gms.internal.ads.jm;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

@cm
public final class g implements ae<Object> {

    /* renamed from: a reason: collision with root package name */
    private final Object f2052a = new Object();

    /* renamed from: b reason: collision with root package name */
    private final Map<String, h> f2053b = new HashMap();

    public final void a(String str, h hVar) {
        synchronized (this.f2052a) {
            this.f2053b.put(str, hVar);
        }
    }

    public final void zza(Object obj, Map<String, String> map) {
        String str;
        String str2 = (String) map.get("id");
        String str3 = (String) map.get("fail");
        String str4 = (String) map.get("fail_stack");
        String str5 = (String) map.get("result");
        String str6 = TextUtils.isEmpty(str4) ? "Unknown Fail Reason." : (String) map.get("fail_reason");
        if (TextUtils.isEmpty(str4)) {
            str = "";
        } else {
            String str7 = "\n";
            String valueOf = String.valueOf(str4);
            str = valueOf.length() != 0 ? str7.concat(valueOf) : new String(str7);
        }
        synchronized (this.f2052a) {
            h hVar = (h) this.f2053b.remove(str2);
            if (hVar == null) {
                String str8 = "Received result for unexpected method invocation: ";
                String valueOf2 = String.valueOf(str2);
                jm.e(valueOf2.length() != 0 ? str8.concat(valueOf2) : new String(str8));
                return;
            } else if (!TextUtils.isEmpty(str3)) {
                String valueOf3 = String.valueOf(str6);
                String valueOf4 = String.valueOf(str);
                hVar.a(valueOf4.length() != 0 ? valueOf3.concat(valueOf4) : new String(valueOf3));
                return;
            } else if (str5 == null) {
                hVar.a((JSONObject) null);
                return;
            } else {
                try {
                    JSONObject jSONObject = new JSONObject(str5);
                    if (jm.a()) {
                        String str9 = "Result GMSG: ";
                        String valueOf5 = String.valueOf(jSONObject.toString(2));
                        jm.a(valueOf5.length() != 0 ? str9.concat(valueOf5) : new String(str9));
                    }
                    hVar.a(jSONObject);
                } catch (JSONException e) {
                    hVar.a(e.getMessage());
                }
                return;
            }
        }
    }
}
