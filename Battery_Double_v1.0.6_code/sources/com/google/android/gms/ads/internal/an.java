package com.google.android.gms.ads.internal;

import com.google.android.gms.internal.ads.aop;
import com.google.android.gms.internal.ads.cm;
import com.google.android.gms.internal.ads.jm;
import com.google.android.gms.internal.ads.jv;
import java.lang.ref.WeakReference;

@cm
public final class an {

    /* renamed from: a reason: collision with root package name */
    private final ap f1974a;

    /* renamed from: b reason: collision with root package name */
    private final Runnable f1975b;
    /* access modifiers changed from: private */
    public aop c;
    /* access modifiers changed from: private */
    public boolean d;
    private boolean e;
    private long f;

    public an(a aVar) {
        this(aVar, new ap(jv.f3440a));
    }

    private an(a aVar, ap apVar) {
        this.d = false;
        this.e = false;
        this.f = 0;
        this.f1974a = apVar;
        this.f1975b = new ao(this, new WeakReference(aVar));
    }

    public final void a() {
        this.d = false;
        this.f1974a.a(this.f1975b);
    }

    public final void a(aop aop) {
        this.c = aop;
    }

    public final void a(aop aop, long j) {
        if (this.d) {
            jm.e("An ad refresh is already scheduled.");
            return;
        }
        this.c = aop;
        this.d = true;
        this.f = j;
        if (!this.e) {
            jm.d("Scheduling ad refresh " + j + " milliseconds from now.");
            this.f1974a.a(this.f1975b, j);
        }
    }

    public final void b() {
        this.e = true;
        if (this.d) {
            this.f1974a.a(this.f1975b);
        }
    }

    public final void b(aop aop) {
        a(aop, 60000);
    }

    public final void c() {
        this.e = false;
        if (this.d) {
            this.d = false;
            a(this.c, this.f);
        }
    }

    public final void d() {
        this.e = false;
        this.d = false;
        if (!(this.c == null || this.c.c == null)) {
            this.c.c.remove("_ad");
        }
        a(this.c, 0);
    }

    public final boolean e() {
        return this.d;
    }
}
