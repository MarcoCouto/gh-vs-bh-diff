package com.google.android.gms.ads.internal;

import android.os.RemoteException;
import com.google.android.gms.internal.ads.atu;
import com.google.android.gms.internal.ads.jm;

final class aj implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ atu f1966a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ ae f1967b;

    aj(ae aeVar, atu atu) {
        this.f1967b = aeVar;
        this.f1966a = atu;
    }

    public final void run() {
        try {
            if (this.f1967b.e.t != null) {
                this.f1967b.e.t.a(this.f1966a);
                this.f1967b.a(this.f1966a.n());
            }
        } catch (RemoteException e) {
            jm.d("#007 Could not call remote method.", e);
        }
    }
}
