package com.google.android.gms.ads.internal.gmsg;

import com.google.android.gms.internal.ads.cm;
import com.google.android.gms.internal.ads.jm;
import java.util.Map;

@cm
public final class l implements ae<Object> {

    /* renamed from: a reason: collision with root package name */
    private final m f2055a;

    public l(m mVar) {
        this.f2055a = mVar;
    }

    public final void zza(Object obj, Map<String, String> map) {
        String str = (String) map.get("name");
        if (str == null) {
            jm.e("App event with no name parameter.");
        } else {
            this.f2055a.a(str, (String) map.get("info"));
        }
    }
}
