package com.google.android.gms.ads.internal;

import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.text.TextUtils;
import android.util.Base64;
import android.view.View;
import android.view.View.OnClickListener;
import com.google.android.gms.ads.internal.gmsg.ae;
import com.google.android.gms.b.a;
import com.google.android.gms.b.b;
import com.google.android.gms.internal.ads.atn;
import com.google.android.gms.internal.ads.atp;
import com.google.android.gms.internal.ads.auw;
import com.google.android.gms.internal.ads.aux;
import com.google.android.gms.internal.ads.bci;
import com.google.android.gms.internal.ads.bdd;
import com.google.android.gms.internal.ads.bdh;
import com.google.android.gms.internal.ads.cm;
import com.google.android.gms.internal.ads.ir;
import com.google.android.gms.internal.ads.jm;
import com.google.android.gms.internal.ads.qn;
import com.google.android.gms.internal.ads.rw;
import java.io.ByteArrayOutputStream;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@cm
public final class t {
    public static View a(ir irVar) {
        if (irVar == null) {
            jm.c("AdState is null");
            return null;
        } else if (b(irVar) && irVar.f3396b != null) {
            return irVar.f3396b.getView();
        } else {
            try {
                a aVar = irVar.p != null ? irVar.p.a() : null;
                if (aVar != null) {
                    return (View) b.a(aVar);
                }
                jm.e("View in mediation adapter is null.");
                return null;
            } catch (RemoteException e) {
                jm.c("Could not get View from mediation adapter.", e);
                return null;
            }
        }
    }

    static ae<qn> a(bdd bdd, bdh bdh, d dVar) {
        return new y(bdd, dVar, bdh);
    }

    private static auw a(Object obj) {
        if (obj instanceof IBinder) {
            return aux.a((IBinder) obj);
        }
        return null;
    }

    private static String a(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        if (bitmap == null) {
            jm.e("Bitmap is null. Returning empty string");
            return "";
        }
        bitmap.compress(CompressFormat.PNG, 100, byteArrayOutputStream);
        String encodeToString = Base64.encodeToString(byteArrayOutputStream.toByteArray(), 0);
        String valueOf = String.valueOf("data:image/png;base64,");
        String valueOf2 = String.valueOf(encodeToString);
        return valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf);
    }

    private static String a(auw auw) {
        if (auw == null) {
            jm.e("Image is null. Returning empty string");
            return "";
        }
        try {
            Uri b2 = auw.b();
            if (b2 != null) {
                return b2.toString();
            }
        } catch (RemoteException e) {
            jm.e("Unable to get image uri. Trying data uri next");
        }
        return b(auw);
    }

    private static JSONObject a(Bundle bundle, String str) throws JSONException {
        JSONObject jSONObject = new JSONObject();
        if (bundle == null || TextUtils.isEmpty(str)) {
            return jSONObject;
        }
        JSONObject jSONObject2 = new JSONObject(str);
        Iterator keys = jSONObject2.keys();
        while (keys.hasNext()) {
            String str2 = (String) keys.next();
            if (bundle.containsKey(str2)) {
                if ("image".equals(jSONObject2.getString(str2))) {
                    Object obj = bundle.get(str2);
                    if (obj instanceof Bitmap) {
                        jSONObject.put(str2, a((Bitmap) obj));
                    } else {
                        jm.e("Invalid type. An image type extra should return a bitmap");
                    }
                } else if (bundle.get(str2) instanceof Bitmap) {
                    jm.e("Invalid asset type. Bitmap should be returned only for image type");
                } else {
                    jSONObject.put(str2, String.valueOf(bundle.get(str2)));
                }
            }
        }
        return jSONObject;
    }

    static final /* synthetic */ void a(atn atn, String str, qn qnVar, boolean z) {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("headline", atn.a());
            jSONObject.put("body", atn.c());
            jSONObject.put("call_to_action", atn.e());
            jSONObject.put("price", atn.h());
            jSONObject.put("star_rating", String.valueOf(atn.f()));
            jSONObject.put("store", atn.g());
            jSONObject.put("icon", a(atn.d()));
            JSONArray jSONArray = new JSONArray();
            List<Object> b2 = atn.b();
            if (b2 != null) {
                for (Object a2 : b2) {
                    jSONArray.put(a(a(a2)));
                }
            }
            jSONObject.put("images", jSONArray);
            jSONObject.put("extras", a(atn.n(), str));
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("assets", jSONObject);
            jSONObject2.put("template_id", "2");
            qnVar.b("google.afma.nativeExpressAds.loadAssets", jSONObject2);
        } catch (JSONException e) {
            jm.c("Exception occurred when loading assets", e);
        }
    }

    static final /* synthetic */ void a(atp atp, String str, qn qnVar, boolean z) {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("headline", atp.a());
            jSONObject.put("body", atp.e());
            jSONObject.put("call_to_action", atp.g());
            jSONObject.put("advertiser", atp.h());
            jSONObject.put("logo", a(atp.f()));
            JSONArray jSONArray = new JSONArray();
            List<Object> b2 = atp.b();
            if (b2 != null) {
                for (Object a2 : b2) {
                    jSONArray.put(a(a(a2)));
                }
            }
            jSONObject.put("images", jSONArray);
            jSONObject.put("extras", a(atp.n(), str));
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("assets", jSONObject);
            jSONObject2.put("template_id", "1");
            qnVar.b("google.afma.nativeExpressAds.loadAssets", jSONObject2);
        } catch (JSONException e) {
            jm.c("Exception occurred when loading assets", e);
        }
    }

    public static boolean a(qn qnVar, bci bci, CountDownLatch countDownLatch) {
        boolean z;
        try {
            View view = qnVar.getView();
            if (view == null) {
                jm.e("AdWebView is null");
                z = false;
            } else {
                view.setVisibility(4);
                List<String> list = bci.f3133b.r;
                if (list == null || list.isEmpty()) {
                    jm.e("No template ids present in mediation response");
                    z = false;
                } else {
                    qnVar.a("/nativeExpressAssetsLoaded", (ae<? super qn>) new w<Object>(countDownLatch));
                    qnVar.a("/nativeExpressAssetsLoadingFailed", (ae<? super qn>) new x<Object>(countDownLatch));
                    bdd h = bci.c.h();
                    bdh i = bci.c.i();
                    if (list.contains("2") && h != null) {
                        qnVar.v().a((rw) new u(new atn(h.a(), h.b(), h.c(), h.d(), h.e(), h.f(), h.g(), h.h(), null, h.l(), null, h.p() != null ? (View) b.a(h.p()) : null, h.q(), null), bci.f3133b.q, qnVar));
                    } else if (!list.contains("1") || i == null) {
                        jm.e("No matching template id and mapper");
                        z = false;
                    } else {
                        qnVar.v().a((rw) new v(new atp(i.a(), i.b(), i.c(), i.d(), i.e(), i.f(), null, i.j(), null, i.n() != null ? (View) b.a(i.n()) : null, i.o(), null), bci.f3133b.q, qnVar));
                    }
                    String str = bci.f3133b.o;
                    String str2 = bci.f3133b.p;
                    if (str2 != null) {
                        qnVar.loadDataWithBaseURL(str2, str, "text/html", "UTF-8", null);
                    } else {
                        qnVar.loadData(str, "text/html", "UTF-8");
                    }
                    z = true;
                }
            }
        } catch (RemoteException e) {
            jm.c("Unable to invoke load assets", e);
            z = false;
        } catch (RuntimeException e2) {
            countDownLatch.countDown();
            throw e2;
        }
        if (!z) {
            countDownLatch.countDown();
        }
        return z;
    }

    private static String b(auw auw) {
        try {
            a a2 = auw.a();
            if (a2 == null) {
                jm.e("Drawable is null. Returning empty string");
                return "";
            }
            Drawable drawable = (Drawable) b.a(a2);
            if (drawable instanceof BitmapDrawable) {
                return a(((BitmapDrawable) drawable).getBitmap());
            }
            jm.e("Drawable is not an instance of BitmapDrawable. Returning empty string");
            return "";
        } catch (RemoteException e) {
            jm.e("Unable to get drawable. Returning empty string");
            return "";
        }
    }

    /* access modifiers changed from: private */
    public static void b(qn qnVar) {
        OnClickListener onClickListener = qnVar.getOnClickListener();
        if (onClickListener != null) {
            onClickListener.onClick(qnVar.getView());
        }
    }

    public static boolean b(ir irVar) {
        return (irVar == null || !irVar.n || irVar.o == null || irVar.o.o == null) ? false : true;
    }
}
