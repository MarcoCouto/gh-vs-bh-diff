package com.google.android.gms.ads.internal.overlay;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable.Creator;

public final class b implements Creator<c> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        Intent intent = null;
        int b2 = com.google.android.gms.common.internal.a.b.b(parcel);
        String str = null;
        String str2 = null;
        String str3 = null;
        String str4 = null;
        String str5 = null;
        String str6 = null;
        String str7 = null;
        while (parcel.dataPosition() < b2) {
            int a2 = com.google.android.gms.common.internal.a.b.a(parcel);
            switch (com.google.android.gms.common.internal.a.b.a(a2)) {
                case 2:
                    str7 = com.google.android.gms.common.internal.a.b.k(parcel, a2);
                    break;
                case 3:
                    str6 = com.google.android.gms.common.internal.a.b.k(parcel, a2);
                    break;
                case 4:
                    str5 = com.google.android.gms.common.internal.a.b.k(parcel, a2);
                    break;
                case 5:
                    str4 = com.google.android.gms.common.internal.a.b.k(parcel, a2);
                    break;
                case 6:
                    str3 = com.google.android.gms.common.internal.a.b.k(parcel, a2);
                    break;
                case 7:
                    str2 = com.google.android.gms.common.internal.a.b.k(parcel, a2);
                    break;
                case 8:
                    str = com.google.android.gms.common.internal.a.b.k(parcel, a2);
                    break;
                case 9:
                    intent = (Intent) com.google.android.gms.common.internal.a.b.a(parcel, a2, Intent.CREATOR);
                    break;
                default:
                    com.google.android.gms.common.internal.a.b.b(parcel, a2);
                    break;
            }
        }
        com.google.android.gms.common.internal.a.b.r(parcel, b2);
        return new c(str7, str6, str5, str4, str3, str2, str, intent);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new c[i];
    }
}
