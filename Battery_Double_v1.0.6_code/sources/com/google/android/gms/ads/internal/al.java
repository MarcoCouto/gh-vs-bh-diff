package com.google.android.gms.ads.internal;

import android.os.RemoteException;
import com.google.android.gms.internal.ads.atr;
import com.google.android.gms.internal.ads.awh;
import com.google.android.gms.internal.ads.ir;
import com.google.android.gms.internal.ads.jm;

final class al implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ String f1970a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ ir f1971b;
    private final /* synthetic */ ae c;

    al(ae aeVar, String str, ir irVar) {
        this.c = aeVar;
        this.f1970a = str;
        this.f1971b = irVar;
    }

    public final void run() {
        try {
            ((awh) this.c.e.v.get(this.f1970a)).a((atr) this.f1971b.C);
        } catch (RemoteException e) {
            jm.d("#007 Could not call remote method.", e);
        }
    }
}
