package com.google.android.gms.ads.internal.overlay;

import android.graphics.Bitmap;
import com.google.android.gms.ads.internal.ax;
import com.google.android.gms.internal.ads.cm;
import com.google.android.gms.internal.ads.jh;
import com.google.android.gms.internal.ads.jv;

@cm
final class j extends jh {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ d f2088a;

    private j(d dVar) {
        this.f2088a = dVar;
    }

    /* synthetic */ j(d dVar, f fVar) {
        this(dVar);
    }

    public final void a() {
        Bitmap a2 = ax.y().a(Integer.valueOf(this.f2088a.f2081b.o.e));
        if (a2 != null) {
            jv.f3440a.post(new k(this, ax.g().a(this.f2088a.f2080a, a2, this.f2088a.f2081b.o.c, this.f2088a.f2081b.o.d)));
        }
    }

    public final void c_() {
    }
}
