package com.google.android.gms.ads.internal;

import com.google.android.gms.internal.ads.atp;
import com.google.android.gms.internal.ads.qn;
import com.google.android.gms.internal.ads.rw;

final /* synthetic */ class v implements rw {

    /* renamed from: a reason: collision with root package name */
    private final atp f2105a;

    /* renamed from: b reason: collision with root package name */
    private final String f2106b;
    private final qn c;

    v(atp atp, String str, qn qnVar) {
        this.f2105a = atp;
        this.f2106b = str;
        this.c = qnVar;
    }

    public final void a(boolean z) {
        t.a(this.f2105a, this.f2106b, this.c, z);
    }
}
