package com.google.android.gms.ads.internal;

import android.os.RemoteException;
import com.google.android.gms.internal.ads.atu;
import com.google.android.gms.internal.ads.jm;

final class bq implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ atu f2011a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ bo f2012b;

    bq(bo boVar, atu atu) {
        this.f2012b = boVar;
        this.f2011a = atu;
    }

    public final void run() {
        try {
            if (this.f2012b.e.t != null) {
                this.f2012b.e.t.a(this.f2011a);
                this.f2012b.a(this.f2011a.n());
            }
        } catch (RemoteException e) {
            jm.d("#007 Could not call remote method.", e);
        }
    }
}
