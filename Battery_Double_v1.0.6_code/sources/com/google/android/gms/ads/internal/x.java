package com.google.android.gms.ads.internal;

import com.google.android.gms.ads.internal.gmsg.ae;
import com.google.android.gms.internal.ads.jm;
import com.google.android.gms.internal.ads.qn;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

final class x implements ae<qn> {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ CountDownLatch f2108a;

    x(CountDownLatch countDownLatch) {
        this.f2108a = countDownLatch;
    }

    public final /* synthetic */ void zza(Object obj, Map map) {
        qn qnVar = (qn) obj;
        jm.e("Adapter returned an ad, but assets substitution failed");
        this.f2108a.countDown();
        qnVar.destroy();
    }
}
