package com.google.android.gms.ads.internal.gmsg;

import com.google.android.gms.ads.internal.bv;
import com.google.android.gms.common.util.f;
import com.google.android.gms.internal.ads.ape;
import com.google.android.gms.internal.ads.asi;
import com.google.android.gms.internal.ads.beu;
import com.google.android.gms.internal.ads.cm;
import com.google.android.gms.internal.ads.g;
import com.google.android.gms.internal.ads.jm;
import com.google.android.gms.internal.ads.o;
import com.google.android.gms.internal.ads.qn;
import java.util.Map;

@cm
public final class d implements ae<qn> {
    private static final Map<String, Integer> d = f.a((K[]) new String[]{"resize", "playVideo", "storePicture", "createCalendarEvent", "setOrientationProperties", "closeResizedAd", "unload"}, (V[]) new Integer[]{Integer.valueOf(1), Integer.valueOf(2), Integer.valueOf(3), Integer.valueOf(4), Integer.valueOf(5), Integer.valueOf(6), Integer.valueOf(7)});

    /* renamed from: a reason: collision with root package name */
    private final bv f2046a;

    /* renamed from: b reason: collision with root package name */
    private final com.google.android.gms.internal.ads.d f2047b;
    private final o c;

    public d(bv bvVar, com.google.android.gms.internal.ads.d dVar, o oVar) {
        this.f2046a = bvVar;
        this.f2047b = dVar;
        this.c = oVar;
    }

    public final /* synthetic */ void zza(Object obj, Map map) {
        qn qnVar = (qn) obj;
        int intValue = ((Integer) d.get((String) map.get("a"))).intValue();
        if (intValue == 5 || intValue == 7 || this.f2046a == null || this.f2046a.b()) {
            switch (intValue) {
                case 1:
                    this.f2047b.a(map);
                    return;
                case 3:
                    new g(qnVar, map).a();
                    return;
                case 4:
                    new beu(qnVar, map).a();
                    return;
                case 5:
                    new com.google.android.gms.internal.ads.f(qnVar, map).a();
                    return;
                case 6:
                    this.f2047b.a(true);
                    return;
                case 7:
                    if (((Boolean) ape.f().a(asi.M)).booleanValue()) {
                        this.c.L();
                        return;
                    }
                    return;
                default:
                    jm.d("Unknown MRAID command called.");
                    return;
            }
        } else {
            this.f2046a.a(null);
        }
    }
}
