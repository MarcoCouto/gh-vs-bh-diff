package com.google.android.gms.ads.internal;

import com.google.android.gms.internal.ads.aop;

final class j implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ aop f2067a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ i f2068b;

    j(i iVar, aop aop) {
        this.f2068b = iVar;
        this.f2067a = aop;
    }

    public final void run() {
        synchronized (this.f2068b.s) {
            if (this.f2068b.d()) {
                this.f2068b.b(this.f2067a);
            } else {
                this.f2068b.b(this.f2067a, 1);
            }
        }
    }
}
