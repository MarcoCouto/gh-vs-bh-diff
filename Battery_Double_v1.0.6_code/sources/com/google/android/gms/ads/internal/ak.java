package com.google.android.gms.ads.internal;

import android.os.RemoteException;
import com.google.android.gms.internal.ads.atp;
import com.google.android.gms.internal.ads.jm;

final class ak implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ atp f1968a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ ae f1969b;

    ak(ae aeVar, atp atp) {
        this.f1969b = aeVar;
        this.f1968a = atp;
    }

    public final void run() {
        try {
            if (this.f1969b.e.s != null) {
                this.f1969b.e.s.a(this.f1968a);
                this.f1969b.a(this.f1968a.j());
            }
        } catch (RemoteException e) {
            jm.d("#007 Could not call remote method.", e);
        }
    }
}
