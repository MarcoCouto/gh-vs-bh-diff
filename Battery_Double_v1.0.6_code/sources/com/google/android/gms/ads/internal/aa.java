package com.google.android.gms.ads.internal;

import com.google.android.gms.internal.ads.nt;

final /* synthetic */ class aa implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final z f1952a;

    /* renamed from: b reason: collision with root package name */
    private final Runnable f1953b;

    aa(z zVar, Runnable runnable) {
        this.f1952a = zVar;
        this.f1953b = runnable;
    }

    public final void run() {
        nt.f3558a.execute(new ac(this.f1952a, this.f1953b));
    }
}
