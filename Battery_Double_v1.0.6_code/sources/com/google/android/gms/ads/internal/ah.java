package com.google.android.gms.ads.internal;

import android.os.RemoteException;
import com.google.android.gms.internal.ads.atn;
import com.google.android.gms.internal.ads.atp;
import com.google.android.gms.internal.ads.atu;
import com.google.android.gms.internal.ads.aub;
import com.google.android.gms.internal.ads.jm;
import java.util.List;

final class ah implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ aub f1962a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ int f1963b;
    private final /* synthetic */ List c;
    private final /* synthetic */ ae d;

    ah(ae aeVar, aub aub, int i, List list) {
        this.d = aeVar;
        this.f1962a = aub;
        this.f1963b = i;
        this.c = list;
    }

    public final void run() {
        boolean z = true;
        try {
            if ((this.f1962a instanceof atp) && this.d.e.t != null) {
                ae aeVar = this.d;
                if (this.f1963b == this.c.size() - 1) {
                    z = false;
                }
                aeVar.c = z;
                atu a2 = ae.b(this.f1962a);
                this.d.e.t.a(a2);
                this.d.a(a2.n());
            } else if ((this.f1962a instanceof atp) && this.d.e.s != null) {
                ae aeVar2 = this.d;
                if (this.f1963b == this.c.size() - 1) {
                    z = false;
                }
                aeVar2.c = z;
                atp atp = (atp) this.f1962a;
                this.d.e.s.a(atp);
                this.d.a(atp.j());
            } else if ((this.f1962a instanceof atn) && this.d.e.t != null) {
                ae aeVar3 = this.d;
                if (this.f1963b == this.c.size() - 1) {
                    z = false;
                }
                aeVar3.c = z;
                atu a3 = ae.b(this.f1962a);
                this.d.e.t.a(a3);
                this.d.a(a3.n());
            } else if (!(this.f1962a instanceof atn) || this.d.e.r == null) {
                ae aeVar4 = this.d;
                if (this.f1963b == this.c.size() - 1) {
                    z = false;
                }
                aeVar4.a(3, z);
            } else {
                ae aeVar5 = this.d;
                if (this.f1963b == this.c.size() - 1) {
                    z = false;
                }
                aeVar5.c = z;
                atn atn = (atn) this.f1962a;
                this.d.e.r.a(atn);
                this.d.a(atn.j());
            }
        } catch (RemoteException e) {
            jm.d("#007 Could not call remote method.", e);
        }
    }
}
