package com.google.android.gms.ads.internal.gmsg;

import com.google.android.gms.internal.ads.cm;
import java.util.Map;

@cm
public interface ae<ContextT> {
    void zza(ContextT contextt, Map<String, String> map);
}
