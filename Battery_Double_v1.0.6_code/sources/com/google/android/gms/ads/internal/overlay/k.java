package com.google.android.gms.ads.internal.overlay;

import android.graphics.drawable.Drawable;

final class k implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ Drawable f2089a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ j f2090b;

    k(j jVar, Drawable drawable) {
        this.f2090b = jVar;
        this.f2089a = drawable;
    }

    public final void run() {
        this.f2090b.f2088a.f2080a.getWindow().setBackgroundDrawable(this.f2089a);
    }
}
