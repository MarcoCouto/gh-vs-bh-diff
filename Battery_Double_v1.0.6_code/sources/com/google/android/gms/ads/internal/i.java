package com.google.android.gms.ads.internal;

import android.content.Context;
import android.os.RemoteException;
import android.support.v4.h.m;
import com.google.android.gms.ads.b.j;
import com.google.android.gms.common.internal.aa;
import com.google.android.gms.internal.ads.aop;
import com.google.android.gms.internal.ads.aot;
import com.google.android.gms.internal.ads.ape;
import com.google.android.gms.internal.ads.apk;
import com.google.android.gms.internal.ads.apo;
import com.google.android.gms.internal.ads.aqk;
import com.google.android.gms.internal.ads.asi;
import com.google.android.gms.internal.ads.aul;
import com.google.android.gms.internal.ads.avx;
import com.google.android.gms.internal.ads.awa;
import com.google.android.gms.internal.ads.awe;
import com.google.android.gms.internal.ads.awh;
import com.google.android.gms.internal.ads.awk;
import com.google.android.gms.internal.ads.awn;
import com.google.android.gms.internal.ads.bcr;
import com.google.android.gms.internal.ads.cm;
import com.google.android.gms.internal.ads.jm;
import com.google.android.gms.internal.ads.jv;
import com.google.android.gms.internal.ads.mu;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

@cm
public final class i extends apo {

    /* renamed from: a reason: collision with root package name */
    private final Context f2065a;

    /* renamed from: b reason: collision with root package name */
    private final apk f2066b;
    private final bcr c;
    private final avx d;
    private final awn e;
    private final awa f;
    private final awk g;
    private final aot h;
    private final j i;
    private final m<String, awh> j;
    private final m<String, awe> k;
    private final aul l;
    private final List<String> m;
    private final aqk n;
    private final String o;
    private final mu p;
    private WeakReference<bb> q;
    private final bu r;
    /* access modifiers changed from: private */
    public final Object s = new Object();

    i(Context context, String str, bcr bcr, mu muVar, apk apk, avx avx, awn awn, awa awa, m<String, awh> mVar, m<String, awe> mVar2, aul aul, aqk aqk, bu buVar, awk awk, aot aot, j jVar) {
        this.f2065a = context;
        this.o = str;
        this.c = bcr;
        this.p = muVar;
        this.f2066b = apk;
        this.f = awa;
        this.d = avx;
        this.e = awn;
        this.j = mVar;
        this.k = mVar2;
        this.l = aul;
        this.m = f();
        this.n = aqk;
        this.r = buVar;
        this.g = awk;
        this.h = aot;
        this.i = jVar;
        asi.a(this.f2065a);
    }

    private final void a(int i2) {
        if (this.f2066b != null) {
            try {
                this.f2066b.a(0);
            } catch (RemoteException e2) {
                jm.c("Failed calling onAdFailedToLoad.", e2);
            }
        }
    }

    private static void a(Runnable runnable) {
        jv.f3440a.post(runnable);
    }

    /* access modifiers changed from: private */
    public final void b(aop aop) {
        if (((Boolean) ape.f().a(asi.cl)).booleanValue() || this.e == null) {
            bo boVar = new bo(this.f2065a, this.r, this.h, this.o, this.c, this.p);
            this.q = new WeakReference<>(boVar);
            awk awk = this.g;
            aa.b("setOnPublisherAdViewLoadedListener must be called on the main UI thread.");
            boVar.e.z = awk;
            if (this.i != null) {
                if (this.i.b() != null) {
                    boVar.a(this.i.b());
                }
                boVar.b(this.i.a());
            }
            avx avx = this.d;
            aa.b("setOnAppInstallAdLoadedListener must be called on the main UI thread.");
            boVar.e.r = avx;
            awn awn = this.e;
            aa.b("setOnUnifiedNativeAdLoadedListener must be called on the main UI thread.");
            boVar.e.t = awn;
            awa awa = this.f;
            aa.b("setOnContentAdLoadedListener must be called on the main UI thread.");
            boVar.e.s = awa;
            m<String, awh> mVar = this.j;
            aa.b("setOnCustomTemplateAdLoadedListeners must be called on the main UI thread.");
            boVar.e.v = mVar;
            m<String, awe> mVar2 = this.k;
            aa.b("setOnCustomClickListener must be called on the main UI thread.");
            boVar.e.u = mVar2;
            aul aul = this.l;
            aa.b("setNativeAdOptions must be called on the main UI thread.");
            boVar.e.w = aul;
            boVar.c(f());
            boVar.a(this.f2066b);
            boVar.a(this.n);
            ArrayList arrayList = new ArrayList();
            if (e()) {
                arrayList.add(Integer.valueOf(1));
            }
            if (this.g != null) {
                arrayList.add(Integer.valueOf(2));
            }
            boVar.d(arrayList);
            if (e()) {
                aop.c.putBoolean("ina", true);
            }
            if (this.g != null) {
                aop.c.putBoolean("iba", true);
            }
            boVar.b(aop);
            return;
        }
        a(0);
    }

    /* access modifiers changed from: private */
    public final void b(aop aop, int i2) {
        if (((Boolean) ape.f().a(asi.cl)).booleanValue() || this.e == null) {
            ae aeVar = new ae(this.f2065a, this.r, aot.a(this.f2065a), this.o, this.c, this.p);
            this.q = new WeakReference<>(aeVar);
            avx avx = this.d;
            aa.b("setOnAppInstallAdLoadedListener must be called on the main UI thread.");
            aeVar.e.r = avx;
            awn awn = this.e;
            aa.b("setOnUnifiedNativeAdLoadedListener must be called on the main UI thread.");
            aeVar.e.t = awn;
            awa awa = this.f;
            aa.b("setOnContentAdLoadedListener must be called on the main UI thread.");
            aeVar.e.s = awa;
            m<String, awh> mVar = this.j;
            aa.b("setOnCustomTemplateAdLoadedListeners must be called on the main UI thread.");
            aeVar.e.v = mVar;
            aeVar.a(this.f2066b);
            m<String, awe> mVar2 = this.k;
            aa.b("setOnCustomClickListener must be called on the main UI thread.");
            aeVar.e.u = mVar2;
            aeVar.c(f());
            aul aul = this.l;
            aa.b("setNativeAdOptions must be called on the main UI thread.");
            aeVar.e.w = aul;
            aeVar.a(this.n);
            aeVar.b(i2);
            aeVar.b(aop);
            return;
        }
        a(0);
    }

    /* access modifiers changed from: private */
    public final boolean d() {
        return ((Boolean) ape.f().a(asi.aM)).booleanValue() && this.g != null;
    }

    private final boolean e() {
        return (this.d == null && this.f == null && this.e == null && (this.j == null || this.j.size() <= 0)) ? false : true;
    }

    private final List<String> f() {
        ArrayList arrayList = new ArrayList();
        if (this.f != null) {
            arrayList.add("1");
        }
        if (this.d != null) {
            arrayList.add("2");
        }
        if (this.e != null) {
            arrayList.add("6");
        }
        if (this.j.size() > 0) {
            arrayList.add("3");
        }
        return arrayList;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        return r0;
     */
    public final String a() {
        synchronized (this.s) {
            if (this.q == null) {
                return null;
            }
            bb bbVar = (bb) this.q.get();
            String str = bbVar != null ? bbVar.a() : null;
        }
    }

    public final void a(aop aop) {
        a((Runnable) new j(this, aop));
    }

    public final void a(aop aop, int i2) {
        if (i2 <= 0) {
            throw new IllegalArgumentException("Number of ads has to be more than 0");
        }
        a((Runnable) new k(this, aop, i2));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        return r0;
     */
    public final String b() {
        synchronized (this.s) {
            if (this.q == null) {
                return null;
            }
            bb bbVar = (bb) this.q.get();
            String str = bbVar != null ? bbVar.q_() : null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        return r0;
     */
    public final boolean c() {
        synchronized (this.s) {
            if (this.q == null) {
                return false;
            }
            bb bbVar = (bb) this.q.get();
            boolean z = bbVar != null ? bbVar.s() : false;
        }
    }
}
