package com.google.android.gms.ads.internal;

import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

final class at implements OnTouchListener {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ ar f1982a;

    at(ar arVar) {
        this.f1982a = arVar;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        if (this.f1982a.h != null) {
            this.f1982a.h.a(motionEvent);
        }
        return false;
    }
}
