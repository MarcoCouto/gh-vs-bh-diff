package com.google.android.gms.ads.internal.gmsg;

import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.text.TextUtils;
import com.google.android.gms.internal.ads.ahh;
import com.google.android.gms.internal.ads.ahi;
import com.google.android.gms.internal.ads.azf;
import com.google.android.gms.internal.ads.cm;
import com.google.android.gms.internal.ads.il;
import com.google.android.gms.internal.ads.jm;
import com.google.android.gms.internal.ads.lv;
import com.google.android.gms.internal.ads.pm;
import com.google.android.gms.internal.ads.pt;
import com.google.android.gms.internal.ads.pu;
import com.google.android.gms.internal.ads.qn;
import com.google.android.gms.internal.ads.rj;
import com.google.android.gms.internal.ads.rr;
import com.google.android.gms.internal.ads.rt;
import com.google.android.gms.internal.ads.ru;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@cm
public final class o {

    /* renamed from: a reason: collision with root package name */
    public static final ae<qn> f2056a = p.f2058a;

    /* renamed from: b reason: collision with root package name */
    public static final ae<qn> f2057b = q.f2059a;
    public static final ae<qn> c = r.f2060a;
    public static final ae<qn> d = new w();
    public static final ae<qn> e = new x();
    public static final ae<qn> f = s.f2061a;
    public static final ae<Object> g = new y();
    public static final ae<qn> h = new z();
    public static final ae<qn> i = t.f2062a;
    public static final ae<qn> j = new aa();
    public static final ae<qn> k = new ab();
    public static final ae<pm> l = new pt();
    public static final ae<pm> m = new pu();
    public static final ae<qn> n = new n();
    public static final g o = new g();
    public static final ae<qn> p = new ac();
    public static final ae<qn> q = new ad();
    public static final ae<qn> r = new v();
    private static final ae<Object> s = new u();

    static final /* synthetic */ void a(azf azf, Map map) {
        String str = (String) map.get("u");
        if (str == null) {
            jm.e("URL missing from click GMSG.");
            return;
        }
        Uri parse = Uri.parse(str);
        try {
            ahh y = ((rr) azf).y();
            parse = (y == null || !y.a(parse)) ? parse : y.a(parse, ((rj) azf).getContext(), ((ru) azf).getView(), ((rj) azf).d());
        } catch (ahi e2) {
            String str2 = "Unable to append parameter to URL: ";
            String valueOf = String.valueOf(str);
            jm.e(valueOf.length() != 0 ? str2.concat(valueOf) : new String(str2));
        }
        new lv(((rj) azf).getContext(), ((rt) azf).k().f3528a, il.a(parse, ((rj) azf).getContext()).toString()).h();
    }

    static final /* synthetic */ void a(rj rjVar, Map map) {
        String str = (String) map.get("u");
        if (str == null) {
            jm.e("URL missing from httpTrack GMSG.");
        } else {
            new lv(rjVar.getContext(), ((rt) rjVar).k().f3528a, str).h();
        }
    }

    static final /* synthetic */ void a(rr rrVar, Map map) {
        String str = (String) map.get("ty");
        String str2 = (String) map.get("td");
        try {
            int parseInt = Integer.parseInt((String) map.get("tx"));
            int parseInt2 = Integer.parseInt(str);
            int parseInt3 = Integer.parseInt(str2);
            ahh y = rrVar.y();
            if (y != null) {
                y.a().a(parseInt, parseInt2, parseInt3);
            }
        } catch (NumberFormatException e2) {
            jm.e("Could not parse touch parameters from gmsg.");
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x006f  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00c3  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x010f  */
    static final /* synthetic */ void b(rj rjVar, Map map) {
        Intent intent;
        PackageManager packageManager = rjVar.getContext().getPackageManager();
        try {
            try {
                JSONArray jSONArray = new JSONObject((String) map.get("data")).getJSONArray("intents");
                JSONObject jSONObject = new JSONObject();
                for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                    try {
                        JSONObject jSONObject2 = jSONArray.getJSONObject(i2);
                        String optString = jSONObject2.optString("id");
                        String optString2 = jSONObject2.optString("u");
                        String optString3 = jSONObject2.optString("i");
                        String optString4 = jSONObject2.optString("m");
                        String optString5 = jSONObject2.optString("p");
                        String optString6 = jSONObject2.optString("c");
                        jSONObject2.optString("f");
                        jSONObject2.optString("e");
                        String optString7 = jSONObject2.optString("intent_url");
                        if (!TextUtils.isEmpty(optString7)) {
                            try {
                                intent = Intent.parseUri(optString7, 0);
                            } catch (URISyntaxException e2) {
                                String str = "Error parsing the url: ";
                                String valueOf = String.valueOf(optString7);
                                jm.b(valueOf.length() != 0 ? str.concat(valueOf) : new String(str), e2);
                            }
                            if (intent == null) {
                                intent = new Intent();
                                if (!TextUtils.isEmpty(optString2)) {
                                    intent.setData(Uri.parse(optString2));
                                }
                                if (!TextUtils.isEmpty(optString3)) {
                                    intent.setAction(optString3);
                                }
                                if (!TextUtils.isEmpty(optString4)) {
                                    intent.setType(optString4);
                                }
                                if (!TextUtils.isEmpty(optString5)) {
                                    intent.setPackage(optString5);
                                }
                                if (!TextUtils.isEmpty(optString6)) {
                                    String[] split = optString6.split("/", 2);
                                    if (split.length == 2) {
                                        intent.setComponent(new ComponentName(split[0], split[1]));
                                    }
                                }
                            }
                            jSONObject.put(optString, packageManager.resolveActivity(intent, 65536) == null);
                        }
                        intent = null;
                        if (intent == null) {
                        }
                        try {
                            jSONObject.put(optString, packageManager.resolveActivity(intent, 65536) == null);
                        } catch (JSONException e3) {
                            jm.b("Error constructing openable urls response.", e3);
                        }
                    } catch (JSONException e4) {
                        jm.b("Error parsing the intent data.", e4);
                    }
                }
                ((azf) rjVar).a("openableIntents", jSONObject);
            } catch (JSONException e5) {
                ((azf) rjVar).a("openableIntents", new JSONObject());
            }
        } catch (JSONException e6) {
            ((azf) rjVar).a("openableIntents", new JSONObject());
        }
    }

    static final /* synthetic */ void c(rj rjVar, Map map) {
        String str = (String) map.get("urls");
        if (TextUtils.isEmpty(str)) {
            jm.e("URLs missing in canOpenURLs GMSG.");
            return;
        }
        String[] split = str.split(",");
        HashMap hashMap = new HashMap();
        PackageManager packageManager = rjVar.getContext().getPackageManager();
        for (String str2 : split) {
            String[] split2 = str2.split(";", 2);
            hashMap.put(str2, Boolean.valueOf(packageManager.resolveActivity(new Intent(split2.length > 1 ? split2[1].trim() : "android.intent.action.VIEW", Uri.parse(split2[0].trim())), 65536) != null));
        }
        ((azf) rjVar).a("openableURLs", (Map<String, ?>) hashMap);
    }
}
