package com.google.android.gms.ads.internal.gmsg;

import android.content.Context;
import com.google.android.gms.ads.internal.ax;
import com.google.android.gms.internal.ads.cm;
import java.util.Map;

@cm
public final class c implements ae<Object> {

    /* renamed from: a reason: collision with root package name */
    private final Context f2045a;

    public c(Context context) {
        this.f2045a = context;
    }

    public final void zza(Object obj, Map<String, String> map) {
        if (ax.B().a(this.f2045a)) {
            ax.B().a(this.f2045a, (String) map.get("eventName"), (String) map.get("eventId"));
        }
    }
}
