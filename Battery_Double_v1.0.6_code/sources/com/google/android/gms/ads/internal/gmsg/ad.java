package com.google.android.gms.ads.internal.gmsg;

import com.google.android.gms.internal.ads.qn;
import java.util.Map;

final class ad implements ae<qn> {
    ad() {
    }

    public final /* synthetic */ void zza(Object obj, Map map) {
        qn qnVar = (qn) obj;
        if (map.keySet().contains("start")) {
            qnVar.e(true);
        }
        if (map.keySet().contains("stop")) {
            qnVar.e(false);
        }
    }
}
