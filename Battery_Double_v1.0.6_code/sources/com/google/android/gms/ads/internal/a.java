package com.google.android.gms.ads.internal;

import android.os.Bundle;
import android.os.RemoteException;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewParent;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.internal.gmsg.k;
import com.google.android.gms.ads.internal.gmsg.m;
import com.google.android.gms.ads.internal.overlay.t;
import com.google.android.gms.common.internal.aa;
import com.google.android.gms.common.util.i;
import com.google.android.gms.internal.ads.af;
import com.google.android.gms.internal.ads.ajt;
import com.google.android.gms.internal.ads.amx;
import com.google.android.gms.internal.ads.amy.a.b;
import com.google.android.gms.internal.ads.aoj;
import com.google.android.gms.internal.ads.aop;
import com.google.android.gms.internal.ads.aoq;
import com.google.android.gms.internal.ads.aot;
import com.google.android.gms.internal.ads.ap;
import com.google.android.gms.internal.ads.ape;
import com.google.android.gms.internal.ads.aph;
import com.google.android.gms.internal.ads.apk;
import com.google.android.gms.internal.ads.apw;
import com.google.android.gms.internal.ads.aqa;
import com.google.android.gms.internal.ads.aqe;
import com.google.android.gms.internal.ads.aqk;
import com.google.android.gms.internal.ads.aqs;
import com.google.android.gms.internal.ads.aqy;
import com.google.android.gms.internal.ads.arp;
import com.google.android.gms.internal.ads.arr;
import com.google.android.gms.internal.ads.asi;
import com.google.android.gms.internal.ads.ast;
import com.google.android.gms.internal.ads.asv;
import com.google.android.gms.internal.ads.atc;
import com.google.android.gms.internal.ads.cm;
import com.google.android.gms.internal.ads.co;
import com.google.android.gms.internal.ads.fx;
import com.google.android.gms.internal.ads.gc;
import com.google.android.gms.internal.ads.gf;
import com.google.android.gms.internal.ads.gn;
import com.google.android.gms.internal.ads.hp;
import com.google.android.gms.internal.ads.il;
import com.google.android.gms.internal.ads.ir;
import com.google.android.gms.internal.ads.is;
import com.google.android.gms.internal.ads.it;
import com.google.android.gms.internal.ads.jc;
import com.google.android.gms.internal.ads.je;
import com.google.android.gms.internal.ads.jj;
import com.google.android.gms.internal.ads.jm;
import com.google.android.gms.internal.ads.jv;
import com.google.android.gms.internal.ads.mh;
import com.google.android.gms.internal.ads.ms;
import com.google.android.gms.internal.ads.sb;
import com.google.android.gms.internal.ads.y;
import com.hmatalonga.greenhub.Config;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Timer;
import java.util.concurrent.CountDownLatch;
import org.json.JSONException;
import org.json.JSONObject;

@cm
public abstract class a extends apw implements k, m, t, aoj, ap, co, jc {

    /* renamed from: a reason: collision with root package name */
    protected asv f1950a;

    /* renamed from: b reason: collision with root package name */
    protected ast f1951b;
    protected boolean c = false;
    protected final an d;
    protected final ay e;
    protected transient aop f;
    protected final ajt g;
    protected com.google.android.gms.b.a h;
    protected final bu i;
    private ast j;
    private final Bundle k = new Bundle();
    private boolean l = false;

    a(ay ayVar, an anVar, bu buVar) {
        this.e = ayVar;
        this.d = new an(this);
        this.i = buVar;
        ax.e().b(this.e.c);
        ax.e().c(this.e.c);
        jj.a(this.e.c);
        ax.C().a(this.e.c);
        ax.i().a(this.e.c, this.e.e);
        ax.k().a(this.e.c);
        this.g = ax.i().g();
        ax.h().a(this.e.c);
        ax.E().a(this.e.c);
        if (((Boolean) ape.f().a(asi.cn)).booleanValue()) {
            Timer timer = new Timer();
            timer.schedule(new ab(this, new CountDownLatch(((Integer) ape.f().a(asi.cp)).intValue()), timer), 0, ((Long) ape.f().a(asi.co)).longValue());
        }
    }

    protected static boolean a(aop aop) {
        Bundle bundle = aop.m.getBundle("com.google.ads.mediation.admob.AdMobAdapter");
        return bundle == null || !bundle.containsKey("gw");
    }

    private static long b(String str) {
        int indexOf = str.indexOf("ufe");
        int indexOf2 = str.indexOf(44, indexOf);
        if (indexOf2 == -1) {
            indexOf2 = str.length();
        }
        try {
            return Long.parseLong(str.substring(indexOf + 4, indexOf2));
        } catch (IndexOutOfBoundsException | NumberFormatException e2) {
            ms.b("", e2);
            return -1;
        }
    }

    /* access modifiers changed from: protected */
    public final void A() {
        if (this.e.C != null) {
            try {
                this.e.C.c();
            } catch (RemoteException e2) {
                jm.d("#007 Could not call remote method.", e2);
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void B() {
        if (this.e.C != null) {
            try {
                this.e.C.f();
            } catch (RemoteException e2) {
                jm.d("#007 Could not call remote method.", e2);
            }
        }
    }

    public final void C() {
        ir irVar = this.e.j;
        if (irVar != null && !TextUtils.isEmpty(irVar.B) && !irVar.I && ax.o().b()) {
            jm.b("Sending troubleshooting signals to the server.");
            ax.o().b(this.e.c, this.e.e.f3528a, irVar.B, this.e.f1990b);
            irVar.I = true;
        }
    }

    public String D() {
        return this.e.f1990b;
    }

    public final aqe E() {
        return this.e.o;
    }

    public final apk F() {
        return this.e.n;
    }

    /* access modifiers changed from: protected */
    public final void G() {
        if (this.h != null) {
            ax.u().b(this.h);
            this.h = null;
        }
    }

    /* access modifiers changed from: protected */
    public final String H() {
        is isVar = this.e.k;
        if (isVar == null) {
            return "javascript";
        }
        if (isVar.f3398b == null) {
            return "javascript";
        }
        String str = isVar.f3398b.T;
        if (TextUtils.isEmpty(str)) {
            return "javascript";
        }
        try {
            if (new JSONObject(str).optInt("media_type", -1) == 0) {
                return null;
            }
            return "javascript";
        } catch (JSONException e2) {
            ms.c("", e2);
            return "javascript";
        }
    }

    /* access modifiers changed from: protected */
    public final List<String> a(List<String> list) {
        ArrayList arrayList = new ArrayList(list.size());
        for (String b2 : list) {
            arrayList.add(il.b(b2, this.e.c));
        }
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public void a(int i2) {
        a(i2, false);
    }

    /* access modifiers changed from: protected */
    public void a(int i2, boolean z) {
        jm.e("Failed to load ad: " + i2);
        this.c = z;
        if (this.e.n != null) {
            try {
                this.e.n.a(i2);
            } catch (RemoteException e2) {
                jm.d("#007 Could not call remote method.", e2);
            }
        }
        if (this.e.C != null) {
            try {
                this.e.C.a(i2);
            } catch (RemoteException e3) {
                jm.d("#007 Could not call remote method.", e3);
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void a(View view) {
        az azVar = this.e.f;
        if (azVar != null) {
            azVar.addView(view, ax.g().d());
        }
    }

    public final void a(af afVar, String str) {
        jm.e("#006 Unexpected call to a deprecated method.");
    }

    public final void a(aot aot) {
        aa.b("#008 Must be called on the main UI thread.: setAdSize");
        this.e.i = aot;
        if (!(this.e.j == null || this.e.j.f3396b == null || this.e.I != 0)) {
            this.e.j.f3396b.a(sb.a(aot));
        }
        if (this.e.f != null) {
            if (this.e.f.getChildCount() > 1) {
                this.e.f.removeView(this.e.f.getNextView());
            }
            this.e.f.setMinimumWidth(aot.f);
            this.e.f.setMinimumHeight(aot.c);
            this.e.f.requestLayout();
        }
    }

    public final void a(aph aph) {
        aa.b("#008 Must be called on the main UI thread.: setAdClickListener");
        this.e.m = aph;
    }

    public final void a(apk apk) {
        aa.b("#008 Must be called on the main UI thread.: setAdListener");
        this.e.n = apk;
    }

    public final void a(aqa aqa) {
        this.e.p = aqa;
    }

    public final void a(aqe aqe) {
        aa.b("#008 Must be called on the main UI thread.: setAppEventListener");
        this.e.o = aqe;
    }

    public final void a(aqk aqk) {
        aa.b("#008 Must be called on the main UI thread.: setCorrelationIdProvider");
        this.e.q = aqk;
    }

    public final void a(aqy aqy) {
        aa.b("#008 Must be called on the main UI thread.: setIconAdOptions");
        this.e.y = aqy;
    }

    public final void a(arr arr) {
        aa.b("#008 Must be called on the main UI thread.: setVideoOptions");
        this.e.x = arr;
    }

    public final void a(ast ast) {
        this.f1950a = new asv(((Boolean) ape.f().a(asi.N)).booleanValue(), "load_ad", this.e.i.f2814a);
        this.j = new ast(-1, null, null);
        if (ast == null) {
            this.f1951b = new ast(-1, null, null);
        } else {
            this.f1951b = new ast(ast.a(), ast.b(), ast.c());
        }
    }

    public void a(atc atc) {
        throw new IllegalStateException("#005 Unexpected call to an abstract (unimplemented) method.");
    }

    public final void a(gf gfVar) {
        aa.b("#008 Must be called on the main UI thread.: setRewardedAdSkuListener");
        this.e.D = gfVar;
    }

    public final void a(gn gnVar) {
        aa.b("#008 Must be called on the main UI thread.: setRewardedVideoAdListener");
        this.e.C = gnVar;
    }

    public final void a(is isVar) {
        if (isVar.f3398b.m != -1 && !TextUtils.isEmpty(isVar.f3398b.w)) {
            long b2 = b(isVar.f3398b.w);
            if (b2 != -1) {
                ast a2 = this.f1950a.a(b2 + isVar.f3398b.m);
                this.f1950a.a(a2, "stc");
            }
        }
        this.f1950a.a(isVar.f3398b.w);
        this.f1950a.a(this.f1951b, "arf");
        this.j = this.f1950a.a();
        this.f1950a.a("gqi", isVar.f3398b.x);
        this.e.g = null;
        this.e.k = isVar;
        isVar.i.a((amx) new ba(this, isVar));
        isVar.i.a(b.AD_LOADED);
        a(isVar, this.f1950a);
    }

    /* access modifiers changed from: protected */
    public abstract void a(is isVar, asv asv);

    public void a(y yVar) {
        jm.e("#006 Unexpected call to a deprecated method.");
    }

    public final void a(String str) {
        aa.b("#008 Must be called on the main UI thread.: setUserId");
        this.e.E = str;
    }

    public final void a(String str, Bundle bundle) {
        this.k.putAll(bundle);
        if (this.l && this.e.p != null) {
            try {
                this.e.p.a();
            } catch (RemoteException e2) {
                jm.d("#007 Could not call remote method.", e2);
            }
        }
    }

    public final void a(String str, String str2) {
        if (this.e.o != null) {
            try {
                this.e.o.a(str, str2);
            } catch (RemoteException e2) {
                jm.d("#007 Could not call remote method.", e2);
            }
        }
    }

    public final void a(HashSet<it> hashSet) {
        this.e.a(hashSet);
    }

    /* access modifiers changed from: protected */
    public abstract boolean a(aop aop, asv asv);

    /* access modifiers changed from: 0000 */
    public boolean a(ir irVar) {
        return false;
    }

    /* access modifiers changed from: protected */
    public abstract boolean a(ir irVar, ir irVar2);

    /* access modifiers changed from: protected */
    public final List<String> b(List<String> list) {
        ArrayList arrayList = new ArrayList(list.size());
        for (String a2 : list) {
            arrayList.add(il.a(a2, this.e.c));
        }
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public final void b(hp hpVar) {
        if (this.e.C != null) {
            String str = "";
            int i2 = 1;
            if (hpVar != null) {
                try {
                    str = hpVar.f3369a;
                    i2 = hpVar.f3370b;
                } catch (RemoteException e2) {
                    jm.d("#007 Could not call remote method.", e2);
                    return;
                }
            }
            fx fxVar = new fx(str, i2);
            this.e.C.a((gc) fxVar);
            if (this.e.D != null) {
                this.e.D.a(fxVar, this.e.k.f3397a.v);
            }
        }
    }

    public void b(ir irVar) {
        this.f1950a.a(this.j, "awr");
        this.e.h = null;
        if (!(irVar.d == -2 || irVar.d == 3 || this.e.a() == null)) {
            ax.j().a(this.e.a());
        }
        if (irVar.d == -1) {
            this.c = false;
            return;
        }
        if (a(irVar)) {
            jm.b("Ad refresh scheduled.");
        }
        if (irVar.d != -2) {
            if (irVar.d == 3) {
                irVar.K.a(b.AD_FAILED_TO_LOAD_NO_FILL);
            } else {
                irVar.K.a(b.AD_FAILED_TO_LOAD);
            }
            a(irVar.d);
            return;
        }
        if (this.e.G == null) {
            this.e.G = new je(this.e.f1990b);
        }
        if (this.e.f != null) {
            this.e.f.a().d(irVar.B);
        }
        this.g.a(this.e.j);
        if (a(this.e.j, irVar)) {
            this.e.j = irVar;
            ay ayVar = this.e;
            if (ayVar.l != null) {
                if (ayVar.j != null) {
                    ayVar.l.a(ayVar.j.y);
                    ayVar.l.b(ayVar.j.z);
                    ayVar.l.b(ayVar.j.n);
                }
                ayVar.l.a(ayVar.i.d);
            }
            this.f1950a.a("is_mraid", this.e.j.a() ? "1" : Config.NOTIFICATION_DEFAULT_PRIORITY);
            this.f1950a.a("is_mediation", this.e.j.n ? "1" : Config.NOTIFICATION_DEFAULT_PRIORITY);
            if (!(this.e.j.f3396b == null || this.e.j.f3396b.v() == null)) {
                this.f1950a.a("is_delay_pl", this.e.j.f3396b.v().f() ? "1" : Config.NOTIFICATION_DEFAULT_PRIORITY);
            }
            this.f1950a.a(this.f1951b, "ttc");
            if (ax.i().b() != null) {
                ax.i().b().a(this.f1950a);
            }
            C();
            if (this.e.d()) {
                x();
            }
        }
        if (irVar.J != null) {
            ax.e().a(this.e.c, irVar.J);
        }
    }

    public void b(boolean z) {
        jm.e("Attempt to call setManualImpressionsEnabled for an unsupported ad type.");
    }

    public boolean b(aop aop) {
        aa.b("#008 Must be called on the main UI thread.: loadAd");
        ax.k().a();
        this.k.clear();
        this.l = false;
        if (((Boolean) ape.f().a(asi.aN)).booleanValue()) {
            aop = aop.a();
            if (((Boolean) ape.f().a(asi.aO)).booleanValue()) {
                aop.c.putBoolean(AdMobAdapter.NEW_BUNDLE, true);
            }
        }
        if (i.c(this.e.c) && aop.k != null) {
            aop = new aoq(aop).a(null).a();
        }
        if (this.e.g == null && this.e.h == null) {
            jm.d("Starting ad request.");
            a((ast) null);
            this.f1951b = this.f1950a.a();
            if (aop.f) {
                jm.d("This request is sent from a test device.");
            } else {
                ape.a();
                String a2 = mh.a(this.e.c);
                jm.d(new StringBuilder(String.valueOf(a2).length() + 71).append("Use AdRequest.Builder.addTestDevice(\"").append(a2).append("\") to get test ads on this device.").toString());
            }
            this.d.a(aop);
            this.c = a(aop, this.f1950a);
            return this.c;
        }
        if (this.f != null) {
            jm.e("Aborting last ad request since another ad request is already in progress. The current request object will still be cached for future refreshes.");
        } else {
            jm.e("Loading already in progress, saving this object for future refreshes.");
        }
        this.f = aop;
        return false;
    }

    public void c(boolean z) {
        throw new IllegalStateException("#005 Unexpected call to an abstract (unimplemented) method.");
    }

    /* access modifiers changed from: protected */
    public boolean c(aop aop) {
        if (this.e.f == null) {
            return false;
        }
        ViewParent parent = this.e.f.getParent();
        if (!(parent instanceof View)) {
            return false;
        }
        View view = (View) parent;
        return ax.e().a(view, view.getContext());
    }

    /* access modifiers changed from: protected */
    public void d(boolean z) {
        jm.a("Ad finished loading.");
        this.c = z;
        this.l = true;
        if (this.e.n != null) {
            try {
                this.e.n.c();
            } catch (RemoteException e2) {
                jm.d("#007 Could not call remote method.", e2);
            }
        }
        if (this.e.C != null) {
            try {
                this.e.C.a();
            } catch (RemoteException e3) {
                jm.d("#007 Could not call remote method.", e3);
            }
        }
        if (this.e.p != null) {
            try {
                this.e.p.a();
            } catch (RemoteException e4) {
                jm.d("#007 Could not call remote method.", e4);
            }
        }
    }

    public void e() {
        if (this.e.j == null) {
            jm.e("Ad state was null when trying to ping click URLs.");
            return;
        }
        jm.b("Pinging click URLs.");
        if (this.e.l != null) {
            this.e.l.b();
        }
        if (this.e.j.c != null) {
            ax.e();
            jv.a(this.e.c, this.e.e.f3528a, b(this.e.j.c));
        }
        if (this.e.m != null) {
            try {
                this.e.m.a();
            } catch (RemoteException e2) {
                jm.d("#007 Could not call remote method.", e2);
            }
        }
    }

    public final void h() {
        v();
    }

    public final bu i() {
        return this.i;
    }

    public void j() {
        aa.b("#008 Must be called on the main UI thread.: destroy");
        this.d.a();
        this.g.b(this.e.j);
        ay ayVar = this.e;
        if (ayVar.f != null) {
            ayVar.f.b();
        }
        ayVar.n = null;
        ayVar.p = null;
        ayVar.o = null;
        ayVar.B = null;
        ayVar.q = null;
        ayVar.a(false);
        if (ayVar.f != null) {
            ayVar.f.removeAllViews();
        }
        ayVar.b();
        ayVar.c();
        ayVar.j = null;
    }

    public final com.google.android.gms.b.a k() {
        aa.b("#008 Must be called on the main UI thread.: getAdFrame");
        return com.google.android.gms.b.b.a(this.e.f);
    }

    public final aot l() {
        aa.b("#008 Must be called on the main UI thread.: getAdSize");
        if (this.e.i == null) {
            return null;
        }
        return new arp(this.e.i);
    }

    public final boolean m() {
        aa.b("#008 Must be called on the main UI thread.: isLoaded");
        return this.e.g == null && this.e.h == null && this.e.j != null;
    }

    public final void n() {
        aa.b("#008 Must be called on the main UI thread.: recordManualImpression");
        if (this.e.j == null) {
            jm.e("Ad state was null when trying to ping manual tracking URLs.");
            return;
        }
        jm.b("Pinging manual tracking URLs.");
        if (!this.e.j.H) {
            ArrayList arrayList = new ArrayList();
            if (this.e.j.g != null) {
                arrayList.addAll(this.e.j.g);
            }
            if (!(this.e.j.o == null || this.e.j.o.i == null)) {
                arrayList.addAll(this.e.j.o.i);
            }
            if (!arrayList.isEmpty()) {
                ax.e();
                jv.a(this.e.c, this.e.e.f3528a, (List<String>) arrayList);
                this.e.j.H = true;
            }
        }
    }

    public void o() {
        aa.b("#008 Must be called on the main UI thread.: pause");
    }

    public void p() {
        aa.b("#008 Must be called on the main UI thread.: resume");
    }

    public final Bundle q() {
        return this.l ? this.k : new Bundle();
    }

    public final void r() {
        aa.b("#008 Must be called on the main UI thread.: stopLoading");
        this.c = false;
        this.e.a(true);
    }

    public final boolean s() {
        return this.c;
    }

    public aqs t() {
        return null;
    }

    /* access modifiers changed from: protected */
    public void u() {
        jm.a("Ad closing.");
        if (this.e.n != null) {
            try {
                this.e.n.a();
            } catch (RemoteException e2) {
                jm.d("#007 Could not call remote method.", e2);
            }
        }
        if (this.e.C != null) {
            try {
                this.e.C.d();
            } catch (RemoteException e3) {
                jm.d("#007 Could not call remote method.", e3);
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void v() {
        jm.a("Ad leaving application.");
        if (this.e.n != null) {
            try {
                this.e.n.b();
            } catch (RemoteException e2) {
                jm.d("#007 Could not call remote method.", e2);
            }
        }
        if (this.e.C != null) {
            try {
                this.e.C.e();
            } catch (RemoteException e3) {
                jm.d("#007 Could not call remote method.", e3);
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void w() {
        jm.a("Ad opening.");
        if (this.e.n != null) {
            try {
                this.e.n.d();
            } catch (RemoteException e2) {
                jm.d("#007 Could not call remote method.", e2);
            }
        }
        if (this.e.C != null) {
            try {
                this.e.C.b();
            } catch (RemoteException e3) {
                jm.d("#007 Could not call remote method.", e3);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void x() {
        d(false);
    }

    public final void y() {
        jm.d("Ad impression.");
        if (this.e.n != null) {
            try {
                this.e.n.f();
            } catch (RemoteException e2) {
                jm.d("#007 Could not call remote method.", e2);
            }
        }
    }

    public final void z() {
        jm.d("Ad clicked.");
        if (this.e.n != null) {
            try {
                this.e.n.e();
            } catch (RemoteException e2) {
                jm.d("#007 Could not call remote method.", e2);
            }
        }
    }
}
