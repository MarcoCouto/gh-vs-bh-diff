package com.google.android.gms.ads.internal;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.RemoteException;
import android.text.TextUtils;
import android.view.Window;
import com.google.ads.mediation.AbstractAdViewAdapter;
import com.google.android.gms.ads.internal.gmsg.ae;
import com.google.android.gms.ads.internal.gmsg.ai;
import com.google.android.gms.ads.internal.gmsg.i;
import com.google.android.gms.ads.internal.gmsg.j;
import com.google.android.gms.ads.internal.overlay.AdOverlayInfoParcel;
import com.google.android.gms.ads.internal.overlay.d;
import com.google.android.gms.ads.internal.overlay.l;
import com.google.android.gms.common.internal.aa;
import com.google.android.gms.common.util.n;
import com.google.android.gms.internal.ads.akr;
import com.google.android.gms.internal.ads.akv;
import com.google.android.gms.internal.ads.aop;
import com.google.android.gms.internal.ads.aot;
import com.google.android.gms.internal.ads.ape;
import com.google.android.gms.internal.ads.asi;
import com.google.android.gms.internal.ads.asv;
import com.google.android.gms.internal.ads.bca;
import com.google.android.gms.internal.ads.bcb;
import com.google.android.gms.internal.ads.bcr;
import com.google.android.gms.internal.ads.cm;
import com.google.android.gms.internal.ads.dp;
import com.google.android.gms.internal.ads.ez;
import com.google.android.gms.internal.ads.fw;
import com.google.android.gms.internal.ads.hp;
import com.google.android.gms.internal.ads.ic;
import com.google.android.gms.internal.ads.ig;
import com.google.android.gms.internal.ads.ir;
import com.google.android.gms.internal.ads.is;
import com.google.android.gms.internal.ads.jm;
import com.google.android.gms.internal.ads.jv;
import com.google.android.gms.internal.ads.kb;
import com.google.android.gms.internal.ads.mu;
import com.google.android.gms.internal.ads.qn;
import com.google.android.gms.internal.ads.qu;
import com.google.android.gms.internal.ads.qy;
import com.google.android.gms.internal.ads.rv;
import com.google.android.gms.internal.ads.ry;
import com.google.android.gms.internal.ads.sb;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

@cm
public final class m extends bg implements ai, j {
    private transient boolean k = false;
    private int l = -1;
    /* access modifiers changed from: private */
    public boolean m;
    /* access modifiers changed from: private */
    public float n;
    /* access modifiers changed from: private */
    public boolean o;
    private ig p;
    private String q;
    private final String r;
    private final fw s;

    public m(Context context, aot aot, String str, bcr bcr, mu muVar, bu buVar) {
        super(context, aot, str, bcr, muVar, buVar);
        boolean z = aot != null && "reward_mb".equals(aot.f2814a);
        this.r = z ? "/Rewarded" : "/Interstitial";
        this.s = z ? new fw(this.e, this.j, new o(this), this, this) : null;
    }

    private final void a(Bundle bundle) {
        ax.e().b(this.e.c, this.e.e.f3528a, "gmob-apps", bundle, false);
    }

    private static is b(is isVar) {
        try {
            String jSONObject = ez.a(isVar.f3398b).toString();
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put(AbstractAdViewAdapter.AD_UNIT_ID_PARAMETER, isVar.f3397a.e);
            bca bca = new bca(jSONObject, null, Collections.singletonList("com.google.ads.mediation.admob.AdMobAdapter"), null, null, Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), jSONObject2.toString(), null, Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), null, null, null, null, null, Collections.emptyList(), null, -1);
            dp dpVar = isVar.f3398b;
            bcb bcb = new bcb(Collections.singletonList(bca), ((Long) ape.f().a(asi.bB)).longValue(), Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), dpVar.H, dpVar.I, "", -1, 0, 1, null, 0, -1, -1, false);
            return new is(isVar.f3397a, new dp(isVar.f3397a, dpVar.f3265a, dpVar.f3266b, Collections.emptyList(), Collections.emptyList(), dpVar.f, true, dpVar.h, Collections.emptyList(), dpVar.j, dpVar.k, dpVar.l, dpVar.m, dpVar.n, dpVar.o, dpVar.p, null, dpVar.r, dpVar.s, dpVar.t, dpVar.u, dpVar.v, dpVar.x, dpVar.y, dpVar.z, null, Collections.emptyList(), Collections.emptyList(), dpVar.D, dpVar.E, dpVar.F, dpVar.G, dpVar.H, dpVar.I, dpVar.J, null, dpVar.L, dpVar.M, dpVar.N, dpVar.O, 0, dpVar.Q, Collections.emptyList(), dpVar.S, dpVar.T), bcb, isVar.d, isVar.e, isVar.f, isVar.g, null, isVar.i, null);
        } catch (JSONException e) {
            jm.b("Unable to generate ad state for an interstitial ad with pooling.", e);
            return isVar;
        }
    }

    private final boolean e(boolean z) {
        return this.s != null && z;
    }

    public final void I() {
        Bitmap bitmap;
        aa.b("showInterstitial must be called on the main UI thread.");
        if (e(this.e.j != null && this.e.j.n)) {
            this.s.a(this.o);
            return;
        }
        if (ax.B().d(this.e.c)) {
            this.q = ax.B().g(this.e.c);
            String valueOf = String.valueOf(this.q);
            String valueOf2 = String.valueOf(this.r);
            this.q = valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf);
        }
        if (this.e.j == null) {
            jm.e("The interstitial has not loaded.");
            return;
        }
        if (((Boolean) ape.f().a(asi.br)).booleanValue()) {
            String packageName = this.e.c.getApplicationContext() != null ? this.e.c.getApplicationContext().getPackageName() : this.e.c.getPackageName();
            if (!this.k) {
                jm.e("It is not recommended to show an interstitial before onAdLoaded completes.");
                Bundle bundle = new Bundle();
                bundle.putString("appid", packageName);
                bundle.putString("action", "show_interstitial_before_load_finish");
                a(bundle);
            }
            ax.e();
            if (!jv.g(this.e.c)) {
                jm.e("It is not recommended to show an interstitial when app is not in foreground.");
                Bundle bundle2 = new Bundle();
                bundle2.putString("appid", packageName);
                bundle2.putString("action", "show_interstitial_app_not_in_foreground");
                a(bundle2);
            }
        }
        if (this.e.e()) {
            return;
        }
        if (this.e.j.n && this.e.j.p != null) {
            try {
                if (((Boolean) ape.f().a(asi.aQ)).booleanValue()) {
                    this.e.j.p.a(this.o);
                }
                this.e.j.p.b();
            } catch (RemoteException e) {
                jm.c("Could not show interstitial.", e);
                K();
            }
        } else if (this.e.j.f3396b == null) {
            jm.e("The interstitial failed to load.");
        } else if (this.e.j.f3396b.z()) {
            jm.e("The interstitial is already showing.");
        } else {
            this.e.j.f3396b.b(true);
            this.e.a(this.e.j.f3396b.getView());
            if (this.e.j.k != null) {
                this.g.a(this.e.i, this.e.j);
            }
            if (n.b()) {
                ir irVar = this.e.j;
                if (irVar.a()) {
                    new akr(this.e.c, irVar.f3396b.getView()).a((akv) irVar.f3396b);
                } else {
                    irVar.f3396b.v().a((ry) new n(this, irVar));
                }
            }
            if (this.e.J) {
                ax.e();
                bitmap = jv.h(this.e.c);
            } else {
                bitmap = null;
            }
            this.l = ax.y().a(bitmap);
            if (!((Boolean) ape.f().a(asi.bR)).booleanValue() || bitmap == null) {
                r rVar = new r(this.e.J, J(), false, 0.0f, -1, this.o, this.e.j.L, this.e.j.O);
                int requestedOrientation = this.e.j.f3396b.getRequestedOrientation();
                if (requestedOrientation == -1) {
                    requestedOrientation = this.e.j.h;
                }
                AdOverlayInfoParcel adOverlayInfoParcel = new AdOverlayInfoParcel(this, this, this, this.e.j.f3396b, requestedOrientation, this.e.e, this.e.j.A, rVar);
                ax.c();
                l.a(this.e.c, adOverlayInfoParcel, true);
                return;
            }
            new p(this, this.l).h();
        }
    }

    /* access modifiers changed from: protected */
    public final boolean J() {
        if (!(this.e.c instanceof Activity)) {
            return false;
        }
        Window window = ((Activity) this.e.c).getWindow();
        if (window == null || window.getDecorView() == null) {
            return false;
        }
        Rect rect = new Rect();
        Rect rect2 = new Rect();
        window.getDecorView().getGlobalVisibleRect(rect, null);
        window.getDecorView().getWindowVisibleDisplayFrame(rect2);
        return (rect.bottom == 0 || rect2.bottom == 0 || rect.top != rect2.top) ? false : true;
    }

    public final void K() {
        ax.y().b(Integer.valueOf(this.l));
        if (this.e.d()) {
            this.e.b();
            this.e.j = null;
            this.e.J = false;
            this.k = false;
        }
    }

    public final void L() {
        d r2 = this.e.j.f3396b.r();
        if (r2 != null) {
            r2.a();
        }
    }

    /* access modifiers changed from: protected */
    public final qn a(is isVar, bv bvVar, ic icVar) throws qy {
        ax.f();
        qn a2 = qu.a(this.e.c, sb.a(this.e.i), this.e.i.f2814a, false, false, this.e.d, this.e.e, this.f1950a, this, this.i, isVar.i);
        a2.v().a(this, this, null, this, this, ((Boolean) ape.f().a(asi.ai)).booleanValue(), this, bvVar, this, icVar);
        a(a2);
        a2.a(isVar.f3397a.v);
        a2.a("/reward", (ae<? super qn>) new i<Object>(this));
        return a2;
    }

    public final void a(hp hpVar) {
        if (e(this.e.j != null && this.e.j.n)) {
            b(this.s.a(hpVar));
            return;
        }
        if (this.e.j != null) {
            if (this.e.j.x != null) {
                ax.e();
                jv.a(this.e.c, this.e.e.f3528a, this.e.j.x);
            }
            if (this.e.j.v != null) {
                hpVar = this.e.j.v;
            }
        }
        b(hpVar);
    }

    public final void a(is isVar, asv asv) {
        boolean z = true;
        if (isVar.e != -2) {
            super.a(isVar, asv);
            return;
        }
        if (e(isVar.c != null)) {
            this.s.c();
            return;
        }
        if (!((Boolean) ape.f().a(asi.aT)).booleanValue()) {
            super.a(isVar, asv);
            return;
        }
        if (isVar.f3398b.g) {
            z = false;
        }
        if (a(isVar.f3397a.c) && z) {
            this.e.k = b(isVar);
        }
        super.a(this.e.k, asv);
    }

    public final void a(boolean z) {
        this.e.J = z;
    }

    public final void a(boolean z, float f) {
        this.m = z;
        this.n = f;
    }

    public final boolean a(aop aop, asv asv) {
        if (this.e.j != null) {
            jm.e("An interstitial is already loading. Aborting.");
            return false;
        }
        if (this.p == null && a(aop) && ax.B().d(this.e.c) && !TextUtils.isEmpty(this.e.f1990b)) {
            this.p = new ig(this.e.c, this.e.f1990b);
        }
        return super.a(aop, asv);
    }

    /* access modifiers changed from: protected */
    public final boolean a(aop aop, ir irVar, boolean z) {
        if (this.e.d() && irVar.f3396b != null) {
            ax.g();
            kb.a(irVar.f3396b);
        }
        return this.d.e();
    }

    public final boolean a(ir irVar, ir irVar2) {
        if (e(irVar2.n)) {
            return fw.a(irVar, irVar2);
        }
        if (!super.a(irVar, irVar2)) {
            return false;
        }
        if (!(this.e.d() || this.e.H == null || irVar2.k == null)) {
            this.g.a(this.e.i, irVar2, this.e.H);
        }
        b(irVar2, false);
        return true;
    }

    public final void c(boolean z) {
        aa.b("setImmersiveMode must be called on the main UI thread.");
        this.o = z;
    }

    public final void d_() {
        if (e(this.e.j != null && this.e.j.n)) {
            this.s.g();
            A();
            return;
        }
        if (!(this.e.j == null || this.e.j.w == null)) {
            ax.e();
            jv.a(this.e.c, this.e.e.f3528a, this.e.j.w);
        }
        A();
    }

    public final void e_() {
        if (e(this.e.j != null && this.e.j.n)) {
            this.s.h();
        }
        B();
    }

    public final void g() {
        ac();
        super.g();
        if (!(this.e.j == null || this.e.j.f3396b == null)) {
            rv v = this.e.j.f3396b.v();
            if (v != null) {
                v.g();
            }
        }
        if (!(!ax.B().d(this.e.c) || this.e.j == null || this.e.j.f3396b == null)) {
            ax.B().c(this.e.j.f3396b.getContext(), this.q);
        }
        if (this.p != null) {
            this.p.a(true);
        }
        if (this.h != null && this.e.j != null && this.e.j.f3396b != null) {
            this.e.j.f3396b.a("onSdkImpression", (Map<String, ?>) new HashMap<String,Object>());
        }
    }

    public final void p_() {
        super.p_();
        this.g.a(this.e.j);
        if (this.p != null) {
            this.p.a(false);
        }
        G();
    }

    /* access modifiers changed from: protected */
    public final void u() {
        K();
        super.u();
    }

    /* access modifiers changed from: protected */
    public final void x() {
        qn qnVar = this.e.j != null ? this.e.j.f3396b : null;
        is isVar = this.e.k;
        if (!(isVar == null || isVar.f3398b == null || !isVar.f3398b.Q || qnVar == null || !ax.u().a(this.e.c))) {
            this.h = ax.u().a(this.e.e.f3529b + "." + this.e.e.c, qnVar.getWebView(), "", "javascript", H());
            if (!(this.h == null || qnVar.getView() == null)) {
                ax.u().a(this.h, qnVar.getView());
                ax.u().a(this.h);
            }
        }
        super.x();
        this.k = true;
    }
}
