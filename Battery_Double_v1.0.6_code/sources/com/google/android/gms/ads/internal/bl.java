package com.google.android.gms.ads.internal;

import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

final class bl implements OnTouchListener {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ bv f2006a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ bj f2007b;

    bl(bj bjVar, bv bvVar) {
        this.f2007b = bjVar;
        this.f2006a = bvVar;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        this.f2006a.a();
        if (this.f2007b.f2004b != null) {
            this.f2007b.f2004b.c();
        }
        return false;
    }
}
