package com.google.android.gms.ads.internal;

import android.os.Debug;
import com.google.android.gms.internal.ads.ape;
import com.google.android.gms.internal.ads.asi;
import com.google.android.gms.internal.ads.jm;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CountDownLatch;

final class ab extends TimerTask {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ CountDownLatch f1954a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ Timer f1955b;
    private final /* synthetic */ a c;

    ab(a aVar, CountDownLatch countDownLatch, Timer timer) {
        this.c = aVar;
        this.f1954a = countDownLatch;
        this.f1955b = timer;
    }

    public final void run() {
        if (((long) ((Integer) ape.f().a(asi.cp)).intValue()) != this.f1954a.getCount()) {
            jm.b("Stopping method tracing");
            Debug.stopMethodTracing();
            if (this.f1954a.getCount() == 0) {
                this.f1955b.cancel();
                return;
            }
        }
        String concat = String.valueOf(this.c.e.c.getPackageName()).concat("_adsTrace_");
        try {
            jm.b("Starting method tracing");
            this.f1954a.countDown();
            Debug.startMethodTracing(new StringBuilder(String.valueOf(concat).length() + 20).append(concat).append(ax.l().a()).toString(), ((Integer) ape.f().a(asi.cq)).intValue());
        } catch (Exception e) {
            jm.d("#007 Could not call remote method.", e);
        }
    }
}
