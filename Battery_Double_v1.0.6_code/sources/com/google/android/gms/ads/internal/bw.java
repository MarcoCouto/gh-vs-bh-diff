package com.google.android.gms.ads.internal;

import android.content.Context;
import android.graphics.Rect;
import android.os.RemoteException;
import android.support.v4.h.a;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.ViewTreeObserver.OnScrollChangedListener;
import android.webkit.WebView;
import com.google.android.gms.ads.d;
import com.google.android.gms.b.b;
import com.google.android.gms.common.internal.aa;
import com.google.android.gms.common.util.n;
import com.google.android.gms.internal.ads.akr;
import com.google.android.gms.internal.ads.akv;
import com.google.android.gms.internal.ads.amy;
import com.google.android.gms.internal.ads.aop;
import com.google.android.gms.internal.ads.aot;
import com.google.android.gms.internal.ads.ape;
import com.google.android.gms.internal.ads.aqs;
import com.google.android.gms.internal.ads.bcr;
import com.google.android.gms.internal.ads.bdd;
import com.google.android.gms.internal.ads.bdh;
import com.google.android.gms.internal.ads.cm;
import com.google.android.gms.internal.ads.ic;
import com.google.android.gms.internal.ads.ig;
import com.google.android.gms.internal.ads.ir;
import com.google.android.gms.internal.ads.is;
import com.google.android.gms.internal.ads.jm;
import com.google.android.gms.internal.ads.jv;
import com.google.android.gms.internal.ads.mu;
import com.google.android.gms.internal.ads.og;
import com.google.android.gms.internal.ads.qn;
import com.google.android.gms.internal.ads.qy;
import com.google.android.gms.internal.ads.rd;
import com.google.android.gms.internal.ads.rv;
import com.google.android.gms.internal.ads.ry;
import com.google.android.gms.internal.ads.rz;
import com.google.android.gms.internal.ads.sb;
import java.lang.ref.WeakReference;
import java.util.List;
import java.util.Map;

@cm
public final class bw extends bg implements OnGlobalLayoutListener, OnScrollChangedListener {
    private boolean k;
    private boolean l;
    private WeakReference<Object> m = new WeakReference<>(null);

    public bw(Context context, aot aot, String str, bcr bcr, mu muVar, bu buVar) {
        super(context, aot, str, bcr, muVar, buVar);
    }

    private final boolean b(ir irVar, ir irVar2) {
        if (irVar2.n) {
            View a2 = t.a(irVar2);
            if (a2 == null) {
                jm.e("Could not get mediation view");
                return false;
            }
            View nextView = this.e.f.getNextView();
            if (nextView != null) {
                if (nextView instanceof qn) {
                    ((qn) nextView).destroy();
                }
                this.e.f.removeView(nextView);
            }
            if (!t.b(irVar2)) {
                try {
                    if (ax.B().b(this.e.c)) {
                        new akr(this.e.c, a2).a((akv) new ig(this.e.c, this.e.f1990b));
                    }
                    if (irVar2.u != null) {
                        this.e.f.setMinimumWidth(irVar2.u.f);
                        this.e.f.setMinimumHeight(irVar2.u.c);
                    }
                    a(a2);
                } catch (Exception e) {
                    ax.i().a((Throwable) e, "BannerAdManager.swapViews");
                    jm.c("Could not add mediation view to view hierarchy.", e);
                    return false;
                }
            }
        } else if (!(irVar2.u == null || irVar2.f3396b == null)) {
            irVar2.f3396b.a(sb.a(irVar2.u));
            this.e.f.removeAllViews();
            this.e.f.setMinimumWidth(irVar2.u.f);
            this.e.f.setMinimumHeight(irVar2.u.c);
            a(irVar2.f3396b.getView());
        }
        if (this.e.f.getChildCount() > 1) {
            this.e.f.showNext();
        }
        if (irVar != null) {
            View nextView2 = this.e.f.getNextView();
            if (nextView2 instanceof qn) {
                ((qn) nextView2).destroy();
            } else if (nextView2 != null) {
                this.e.f.removeView(nextView2);
            }
            this.e.c();
        }
        this.e.f.setVisibility(0);
        return true;
    }

    private final void c(qn qnVar) {
        if (M()) {
            WebView webView = qnVar.getWebView();
            if (webView != null) {
                View view = qnVar.getView();
                if (view != null && ax.u().a(this.e.c)) {
                    int i = this.e.e.f3529b;
                    this.h = ax.u().a(i + "." + this.e.e.c, webView, "", "javascript", H());
                    if (this.h != null) {
                        ax.u().a(this.h, view);
                        ax.u().a(this.h);
                        this.l = true;
                    }
                }
            }
        }
    }

    public final void I() {
        throw new IllegalStateException("Interstitial is NOT supported by BannerAdManager.");
    }

    public final void L() {
        this.d.d();
    }

    /* access modifiers changed from: protected */
    public final boolean X() {
        boolean z = true;
        ax.e();
        if (!jv.a(this.e.c, "android.permission.INTERNET")) {
            ape.a().a(this.e.f, this.e.i, "Missing internet permission in AndroidManifest.xml.", "Missing internet permission in AndroidManifest.xml. You must have the following declaration: <uses-permission android:name=\"android.permission.INTERNET\" />");
            z = false;
        }
        ax.e();
        if (!jv.a(this.e.c)) {
            ape.a().a(this.e.f, this.e.i, "Missing AdActivity with android:configChanges in AndroidManifest.xml.", "Missing AdActivity with android:configChanges in AndroidManifest.xml. You must have the following declaration within the <application> element: <activity android:name=\"com.google.android.gms.ads.AdActivity\" android:configChanges=\"keyboard|keyboardHidden|orientation|screenLayout|uiMode|screenSize|smallestScreenSize\" />");
            z = false;
        }
        if (!z && this.e.f != null) {
            this.e.f.setVisibility(0);
        }
        return z;
    }

    /* access modifiers changed from: protected */
    public final qn a(is isVar, bv bvVar, ic icVar) throws qy {
        d b2;
        aot aot;
        if (this.e.i.g == null && this.e.i.i) {
            ay ayVar = this.e;
            if (isVar.f3398b.y) {
                aot = this.e.i;
            } else {
                String str = isVar.f3398b.l;
                if (str != null) {
                    String[] split = str.split("[xX]");
                    split[0] = split[0].trim();
                    split[1] = split[1].trim();
                    b2 = new d(Integer.parseInt(split[0]), Integer.parseInt(split[1]));
                } else {
                    b2 = this.e.i.b();
                }
                aot = new aot(this.e.c, b2);
            }
            ayVar.i = aot;
        }
        return super.a(isVar, bvVar, icVar);
    }

    /* access modifiers changed from: protected */
    public final void a(ir irVar, boolean z) {
        bdh bdh = null;
        if (M()) {
            qn qnVar = irVar != null ? irVar.f3396b : null;
            if (qnVar != null) {
                if (!this.l) {
                    c(qnVar);
                }
                if (this.h != null) {
                    qnVar.a("onSdkImpression", (Map) new a());
                }
            }
        }
        super.a(irVar, z);
        if (t.b(irVar)) {
            d dVar = new d(this);
            if (irVar != null && t.b(irVar)) {
                qn qnVar2 = irVar.f3396b;
                Object obj = qnVar2 != null ? qnVar2.getView() : null;
                if (obj == null) {
                    jm.e("AdWebView is null");
                    return;
                }
                try {
                    List list = irVar.o != null ? irVar.o.r : null;
                    if (list == null || list.isEmpty()) {
                        jm.e("No template ids present in mediation response");
                        return;
                    }
                    bdd bdd = irVar.p != null ? irVar.p.h() : null;
                    if (irVar.p != null) {
                        bdh = irVar.p.i();
                    }
                    if (list.contains("2") && bdd != null) {
                        bdd.b(b.a(obj));
                        if (!bdd.j()) {
                            bdd.i();
                        }
                        qnVar2.a("/nativeExpressViewClicked", t.a(bdd, (bdh) null, dVar));
                    } else if (!list.contains("1") || bdh == null) {
                        jm.e("No matching template id and mapper");
                    } else {
                        bdh.b(b.a(obj));
                        if (!bdh.h()) {
                            bdh.g();
                        }
                        qnVar2.a("/nativeExpressViewClicked", t.a((bdd) null, bdh, dVar));
                    }
                } catch (RemoteException e) {
                    jm.c("Error occurred while recording impression and registering for clicks", e);
                }
            }
        }
    }

    /* JADX INFO: used method not loaded: com.google.android.gms.internal.ads.asg.a(com.google.android.gms.internal.ads.ary):null, types can be incorrect */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x0117, code lost:
        if (((java.lang.Boolean) com.google.android.gms.internal.ads.ape.f().a((com.google.android.gms.internal.ads.ary) com.google.android.gms.internal.ads.asi.bW)).booleanValue() != false) goto L_0x0119;
     */
    public final boolean a(ir irVar, ir irVar2) {
        View view;
        if (!super.a(irVar, irVar2)) {
            return false;
        }
        if (!this.e.d() || b(irVar, irVar2)) {
            b(irVar2, false);
            if (irVar2.l) {
                d(irVar2);
                ax.A();
                og.a((View) this.e.f, (OnGlobalLayoutListener) this);
                ax.A();
                og.a((View) this.e.f, (OnScrollChangedListener) this);
                if (!irVar2.m) {
                    c cVar = new c(this);
                    rv rvVar = irVar2.f3396b != null ? irVar2.f3396b.v() : null;
                    if (rvVar != null) {
                        rvVar.a((rz) new bx(irVar2, cVar));
                    }
                }
            } else {
                if (this.e.e()) {
                }
                a(irVar2, false);
            }
            if (irVar2.f3396b != null) {
                rd b2 = irVar2.f3396b.b();
                rv v = irVar2.f3396b.v();
                if (v != null) {
                    v.g();
                }
                if (!(this.e.x == null || b2 == null)) {
                    b2.a(this.e.x);
                }
            }
            if (n.b()) {
                if (this.e.d()) {
                    if (irVar2.f3396b != null) {
                        if (irVar2.k != null) {
                            this.g.a(this.e.i, irVar2);
                        }
                        view = irVar2.f3396b.getView();
                        akr akr = new akr(this.e.c, view);
                        if (ax.B().b(this.e.c) && a(irVar2.f3395a) && !TextUtils.isEmpty(this.e.f1990b)) {
                            akr.a((akv) new ig(this.e.c, this.e.f1990b));
                        }
                        if (irVar2.a()) {
                            akr.a((akv) irVar2.f3396b);
                        } else {
                            irVar2.f3396b.v().a((ry) new b(akr, irVar2));
                        }
                    }
                    view = null;
                } else {
                    if (!(this.e.H == null || irVar2.k == null)) {
                        this.g.a(this.e.i, irVar2, this.e.H);
                        view = this.e.H;
                    }
                    view = null;
                }
                if (!irVar2.n) {
                    this.e.a(view);
                }
            }
            return true;
        }
        if (irVar2.K != null) {
            irVar2.K.a(amy.a.b.AD_FAILED_TO_LOAD);
        }
        a(0);
        return false;
    }

    public final void b(boolean z) {
        aa.b("setManualImpressionsEnabled must be called from the main thread.");
        this.k = z;
    }

    public final boolean b(aop aop) {
        if (aop.h != this.k) {
            aop = new aop(aop.f2809a, aop.f2810b, aop.c, aop.d, aop.e, aop.f, aop.g, aop.h || this.k, aop.i, aop.j, aop.k, aop.l, aop.m, aop.n, aop.o, aop.p, aop.q, aop.r);
        }
        return super.b(aop);
    }

    /* access modifiers changed from: 0000 */
    public final void d(ir irVar) {
        if (irVar != null && !irVar.m && this.e.f != null && ax.e().a((View) this.e.f, this.e.c) && this.e.f.getGlobalVisibleRect(new Rect(), null)) {
            if (!(irVar == null || irVar.f3396b == null || irVar.f3396b.v() == null)) {
                irVar.f3396b.v().a((rz) null);
            }
            a(irVar, false);
            irVar.m = true;
        }
    }

    public final void onGlobalLayout() {
        d(this.e.j);
    }

    public final void onScrollChanged() {
        d(this.e.j);
    }

    public final aqs t() {
        aa.b("getVideoController must be called from the main thread.");
        if (this.e.j == null || this.e.j.f3396b == null) {
            return null;
        }
        return this.e.j.f3396b.b();
    }

    /* access modifiers changed from: protected */
    public final void x() {
        qn qnVar = this.e.j != null ? this.e.j.f3396b : null;
        if (!this.l && qnVar != null) {
            c(qnVar);
        }
        super.x();
    }
}
