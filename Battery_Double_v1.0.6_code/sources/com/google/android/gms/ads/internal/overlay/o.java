package com.google.android.gms.ads.internal.overlay;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageButton;
import com.google.android.gms.internal.ads.ape;
import com.google.android.gms.internal.ads.cm;
import com.google.android.gms.internal.ads.mh;

@cm
public final class o extends FrameLayout implements OnClickListener {

    /* renamed from: a reason: collision with root package name */
    private final ImageButton f2091a;

    /* renamed from: b reason: collision with root package name */
    private final w f2092b;

    public o(Context context, p pVar, w wVar) {
        super(context);
        this.f2092b = wVar;
        setOnClickListener(this);
        this.f2091a = new ImageButton(context);
        this.f2091a.setImageResource(17301527);
        this.f2091a.setBackgroundColor(0);
        this.f2091a.setOnClickListener(this);
        ImageButton imageButton = this.f2091a;
        ape.a();
        int a2 = mh.a(context, pVar.f2093a);
        ape.a();
        int a3 = mh.a(context, 0);
        ape.a();
        int a4 = mh.a(context, pVar.f2094b);
        ape.a();
        imageButton.setPadding(a2, a3, a4, mh.a(context, pVar.d));
        this.f2091a.setContentDescription("Interstitial close button");
        ape.a();
        mh.a(context, pVar.e);
        ImageButton imageButton2 = this.f2091a;
        ape.a();
        int a5 = mh.a(context, pVar.e + pVar.f2093a + pVar.f2094b);
        ape.a();
        addView(imageButton2, new LayoutParams(a5, mh.a(context, pVar.e + pVar.d), 17));
    }

    public final void a(boolean z) {
        if (z) {
            this.f2091a.setVisibility(8);
        } else {
            this.f2091a.setVisibility(0);
        }
    }

    public final void onClick(View view) {
        if (this.f2092b != null) {
            this.f2092b.c();
        }
    }
}
