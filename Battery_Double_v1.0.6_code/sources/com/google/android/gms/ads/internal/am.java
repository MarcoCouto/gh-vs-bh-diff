package com.google.android.gms.ads.internal;

import android.os.RemoteException;
import com.google.android.gms.internal.ads.avt;
import com.google.android.gms.internal.ads.awh;
import com.google.android.gms.internal.ads.jm;

final class am implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ avt f1972a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ ae f1973b;

    am(ae aeVar, avt avt) {
        this.f1973b = aeVar;
        this.f1972a = avt;
    }

    public final void run() {
        try {
            ((awh) this.f1973b.e.v.get(this.f1972a.l())).a(this.f1972a);
        } catch (RemoteException e) {
            jm.d("#007 Could not call remote method.", e);
        }
    }
}
