package com.google.android.gms.ads.internal;

import android.os.RemoteException;
import com.google.android.gms.internal.ads.atp;
import com.google.android.gms.internal.ads.jm;

final class bs implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ atp f2015a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ bo f2016b;

    bs(bo boVar, atp atp) {
        this.f2016b = boVar;
        this.f2015a = atp;
    }

    public final void run() {
        try {
            if (this.f2016b.e.s != null) {
                this.f2016b.e.s.a(this.f2015a);
                this.f2016b.a(this.f2015a.j());
            }
        } catch (RemoteException e) {
            jm.d("#007 Could not call remote method.", e);
        }
    }
}
