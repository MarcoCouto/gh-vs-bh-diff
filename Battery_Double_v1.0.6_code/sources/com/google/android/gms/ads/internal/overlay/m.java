package com.google.android.gms.ads.internal.overlay;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.ads.internal.r;
import com.google.android.gms.common.internal.a.b;
import com.google.android.gms.internal.ads.mu;

public final class m implements Creator<AdOverlayInfoParcel> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b2 = b.b(parcel);
        c cVar = null;
        IBinder iBinder = null;
        IBinder iBinder2 = null;
        IBinder iBinder3 = null;
        IBinder iBinder4 = null;
        String str = null;
        boolean z = false;
        String str2 = null;
        IBinder iBinder5 = null;
        int i = 0;
        int i2 = 0;
        String str3 = null;
        mu muVar = null;
        String str4 = null;
        r rVar = null;
        IBinder iBinder6 = null;
        while (parcel.dataPosition() < b2) {
            int a2 = b.a(parcel);
            switch (b.a(a2)) {
                case 2:
                    cVar = (c) b.a(parcel, a2, c.CREATOR);
                    break;
                case 3:
                    iBinder = b.l(parcel, a2);
                    break;
                case 4:
                    iBinder2 = b.l(parcel, a2);
                    break;
                case 5:
                    iBinder3 = b.l(parcel, a2);
                    break;
                case 6:
                    iBinder4 = b.l(parcel, a2);
                    break;
                case 7:
                    str = b.k(parcel, a2);
                    break;
                case 8:
                    z = b.c(parcel, a2);
                    break;
                case 9:
                    str2 = b.k(parcel, a2);
                    break;
                case 10:
                    iBinder5 = b.l(parcel, a2);
                    break;
                case 11:
                    i = b.d(parcel, a2);
                    break;
                case 12:
                    i2 = b.d(parcel, a2);
                    break;
                case 13:
                    str3 = b.k(parcel, a2);
                    break;
                case 14:
                    muVar = (mu) b.a(parcel, a2, mu.CREATOR);
                    break;
                case 16:
                    str4 = b.k(parcel, a2);
                    break;
                case 17:
                    rVar = (r) b.a(parcel, a2, r.CREATOR);
                    break;
                case 18:
                    iBinder6 = b.l(parcel, a2);
                    break;
                default:
                    b.b(parcel, a2);
                    break;
            }
        }
        b.r(parcel, b2);
        return new AdOverlayInfoParcel(cVar, iBinder, iBinder2, iBinder3, iBinder4, str, z, str2, iBinder5, i, i2, str3, muVar, str4, rVar, iBinder6);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new AdOverlayInfoParcel[i];
    }
}
