package com.google.android.gms.ads.internal.overlay;

import android.app.Activity;
import android.os.Bundle;
import com.google.android.gms.internal.ads.cm;
import com.google.android.gms.internal.ads.jm;

@cm
public final class r extends d {
    public r(Activity activity) {
        super(activity);
    }

    public final void a(Bundle bundle) {
        jm.a("AdOverlayParcel is null or does not contain valid overlay type.");
        this.d = 3;
        this.f2080a.finish();
    }
}
