package com.google.android.gms.ads.internal;

import android.app.Activity;
import android.content.Context;
import android.os.RemoteException;
import android.support.annotation.Keep;
import android.view.View;
import android.widget.FrameLayout;
import com.google.android.gms.ads.internal.overlay.AdOverlayInfoParcel;
import com.google.android.gms.ads.internal.overlay.q;
import com.google.android.gms.ads.internal.overlay.s;
import com.google.android.gms.ads.internal.overlay.x;
import com.google.android.gms.ads.internal.overlay.y;
import com.google.android.gms.b.a;
import com.google.android.gms.b.b;
import com.google.android.gms.common.util.DynamiteApi;
import com.google.android.gms.internal.ads.ab;
import com.google.android.gms.internal.ads.aot;
import com.google.android.gms.internal.ads.apq;
import com.google.android.gms.internal.ads.apv;
import com.google.android.gms.internal.ads.aqi;
import com.google.android.gms.internal.ads.aqn;
import com.google.android.gms.internal.ads.asi;
import com.google.android.gms.internal.ads.aun;
import com.google.android.gms.internal.ads.aup;
import com.google.android.gms.internal.ads.avb;
import com.google.android.gms.internal.ads.avg;
import com.google.android.gms.internal.ads.azc;
import com.google.android.gms.internal.ads.bcr;
import com.google.android.gms.internal.ads.cm;
import com.google.android.gms.internal.ads.fy;
import com.google.android.gms.internal.ads.gh;
import com.google.android.gms.internal.ads.jv;
import com.google.android.gms.internal.ads.mu;
import com.google.android.gms.internal.ads.r;
import java.util.HashMap;

@Keep
@DynamiteApi
@cm
public class ClientApi extends aqi {
    public apq createAdLoaderBuilder(a aVar, String str, bcr bcr, int i) {
        Context context = (Context) b.a(aVar);
        ax.e();
        return new l(context, str, bcr, new mu(12451000, i, true, jv.k(context)), bu.a(context));
    }

    public r createAdOverlay(a aVar) {
        Activity activity = (Activity) b.a(aVar);
        AdOverlayInfoParcel a2 = AdOverlayInfoParcel.a(activity.getIntent());
        if (a2 == null) {
            return new com.google.android.gms.ads.internal.overlay.r(activity);
        }
        switch (a2.k) {
            case 1:
                return new q(activity);
            case 2:
                return new x(activity);
            case 3:
                return new y(activity);
            case 4:
                return new s(activity, a2);
            default:
                return new com.google.android.gms.ads.internal.overlay.r(activity);
        }
    }

    public apv createBannerAdManager(a aVar, aot aot, String str, bcr bcr, int i) throws RemoteException {
        Context context = (Context) b.a(aVar);
        ax.e();
        return new bw(context, aot, str, bcr, new mu(12451000, i, true, jv.k(context)), bu.a(context));
    }

    public ab createInAppPurchaseManager(a aVar) {
        return null;
    }

    /* JADX INFO: used method not loaded: com.google.android.gms.internal.ads.asg.a(com.google.android.gms.internal.ads.ary):null, types can be incorrect */
    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0035, code lost:
        if (((java.lang.Boolean) com.google.android.gms.internal.ads.ape.f().a((com.google.android.gms.internal.ads.ary) com.google.android.gms.internal.ads.asi.aT)).booleanValue() == false) goto L_0x0037;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0049, code lost:
        if (((java.lang.Boolean) com.google.android.gms.internal.ads.ape.f().a((com.google.android.gms.internal.ads.ary) com.google.android.gms.internal.ads.asi.aU)).booleanValue() != false) goto L_0x004b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x004b, code lost:
        r1 = true;
     */
    public apv createInterstitialAdManager(a aVar, aot aot, String str, bcr bcr, int i) throws RemoteException {
        Context context = (Context) b.a(aVar);
        asi.a(context);
        ax.e();
        mu muVar = new mu(12451000, i, true, jv.k(context));
        boolean equals = "reward_mb".equals(aot.f2814a);
        if (!equals) {
        }
        if (equals) {
        }
        boolean z = false;
        if (z) {
            return new azc(context, str, bcr, muVar, bu.a(context));
        }
        return new m(context, aot, str, bcr, muVar, bu.a(context));
    }

    public avb createNativeAdViewDelegate(a aVar, a aVar2) {
        return new aun((FrameLayout) b.a(aVar), (FrameLayout) b.a(aVar2));
    }

    public avg createNativeAdViewHolderDelegate(a aVar, a aVar2, a aVar3) {
        return new aup((View) b.a(aVar), (HashMap) b.a(aVar2), (HashMap) b.a(aVar3));
    }

    public gh createRewardedVideoAd(a aVar, bcr bcr, int i) {
        Context context = (Context) b.a(aVar);
        ax.e();
        return new fy(context, bu.a(context), bcr, new mu(12451000, i, true, jv.k(context)));
    }

    public apv createSearchAdManager(a aVar, aot aot, String str, int i) throws RemoteException {
        Context context = (Context) b.a(aVar);
        ax.e();
        return new ar(context, aot, str, new mu(12451000, i, true, jv.k(context)));
    }

    public aqn getMobileAdsSettingsManager(a aVar) {
        return null;
    }

    public aqn getMobileAdsSettingsManagerWithClientJarVersion(a aVar, int i) {
        Context context = (Context) b.a(aVar);
        ax.e();
        return z.a(context, new mu(12451000, i, true, jv.k(context)));
    }
}
