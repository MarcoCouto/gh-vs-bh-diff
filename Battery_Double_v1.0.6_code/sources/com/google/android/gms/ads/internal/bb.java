package com.google.android.gms.ads.internal;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.DisplayMetrics;
import android.view.View;
import com.google.android.gms.ads.internal.overlay.n;
import com.google.android.gms.common.b.c;
import com.google.android.gms.common.internal.aa;
import com.google.android.gms.internal.ads.aln;
import com.google.android.gms.internal.ads.amy.a.b;
import com.google.android.gms.internal.ads.anb;
import com.google.android.gms.internal.ads.aop;
import com.google.android.gms.internal.ads.aot;
import com.google.android.gms.internal.ads.ape;
import com.google.android.gms.internal.ads.aqy;
import com.google.android.gms.internal.ads.asi;
import com.google.android.gms.internal.ads.asv;
import com.google.android.gms.internal.ads.aul;
import com.google.android.gms.internal.ads.avt;
import com.google.android.gms.internal.ads.awe;
import com.google.android.gms.internal.ads.azb;
import com.google.android.gms.internal.ads.bcc;
import com.google.android.gms.internal.ads.bck;
import com.google.android.gms.internal.ads.bcr;
import com.google.android.gms.internal.ads.cm;
import com.google.android.gms.internal.ads.cp;
import com.google.android.gms.internal.ads.dm;
import com.google.android.gms.internal.ads.eh;
import com.google.android.gms.internal.ads.ir;
import com.google.android.gms.internal.ads.it;
import com.google.android.gms.internal.ads.iv;
import com.google.android.gms.internal.ads.jh;
import com.google.android.gms.internal.ads.jm;
import com.google.android.gms.internal.ads.jt;
import com.google.android.gms.internal.ads.jv;
import com.google.android.gms.internal.ads.kb;
import com.google.android.gms.internal.ads.mu;
import com.google.android.gms.internal.ads.nc;
import com.google.android.gms.internal.ads.nn;
import com.google.android.gms.internal.ads.nt;
import com.google.android.gms.internal.ads.qn;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@cm
public abstract class bb extends a implements aq, n, bcc {
    protected final bcr j;
    private transient boolean k;

    public bb(Context context, aot aot, String str, bcr bcr, mu muVar, bu buVar) {
        this(new ay(context, aot, str, muVar), bcr, null, buVar);
    }

    private bb(ay ayVar, bcr bcr, an anVar, bu buVar) {
        super(ayVar, null, buVar);
        this.j = bcr;
        this.k = false;
    }

    private final dm a(aop aop, Bundle bundle, iv ivVar, int i) {
        PackageInfo packageInfo;
        ApplicationInfo applicationInfo = this.e.c.getApplicationInfo();
        try {
            packageInfo = c.b(this.e.c).b(applicationInfo.packageName, 0);
        } catch (NameNotFoundException e) {
            packageInfo = null;
        }
        DisplayMetrics displayMetrics = this.e.c.getResources().getDisplayMetrics();
        Bundle bundle2 = null;
        if (!(this.e.f == null || this.e.f.getParent() == null)) {
            int[] iArr = new int[2];
            this.e.f.getLocationOnScreen(iArr);
            int i2 = iArr[0];
            int i3 = iArr[1];
            int width = this.e.f.getWidth();
            int height = this.e.f.getHeight();
            int i4 = 0;
            if (this.e.f.isShown() && i2 + width > 0 && i3 + height > 0 && i2 <= displayMetrics.widthPixels && i3 <= displayMetrics.heightPixels) {
                i4 = 1;
            }
            bundle2 = new Bundle(5);
            bundle2.putInt("x", i2);
            bundle2.putInt("y", i3);
            bundle2.putInt("width", width);
            bundle2.putInt("height", height);
            bundle2.putInt("visible", i4);
        }
        String a2 = ax.i().a().a();
        this.e.l = new it(a2, this.e.f1990b);
        this.e.l.a(aop);
        ax.e();
        String a3 = jv.a(this.e.c, (View) this.e.f, this.e.i);
        long j2 = 0;
        if (this.e.q != null) {
            try {
                j2 = this.e.q.a();
            } catch (RemoteException e2) {
                jm.e("Cannot get correlation id, default to 0.");
            }
        }
        String uuid = UUID.randomUUID().toString();
        Bundle a4 = ax.j().a(this.e.c, this, a2);
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        int i5 = 0;
        while (true) {
            int i6 = i5;
            if (i6 >= this.e.v.size()) {
                break;
            }
            String str = (String) this.e.v.b(i6);
            arrayList.add(str);
            if (this.e.u.containsKey(str) && this.e.u.get(str) != null) {
                arrayList2.add(str);
            }
            i5 = i6 + 1;
        }
        nn a5 = jt.a((Callable<T>) new be<T>(this));
        nn a6 = jt.a((Callable<T>) new bf<T>(this));
        String str2 = null;
        if (ivVar != null) {
            str2 = ivVar.c();
        }
        String str3 = null;
        if (this.e.F != null && this.e.F.size() > 0) {
            int i7 = 0;
            if (packageInfo != null) {
                i7 = packageInfo.versionCode;
            }
            if (i7 > ax.i().l().g()) {
                ax.i().l().m();
                ax.i().l().a(i7);
            } else {
                JSONObject l = ax.i().l().l();
                if (l != null) {
                    JSONArray optJSONArray = l.optJSONArray(this.e.f1990b);
                    if (optJSONArray != null) {
                        str3 = optJSONArray.toString();
                    }
                }
            }
        }
        aot aot = this.e.i;
        String str4 = this.e.f1990b;
        String c = ape.c();
        mu muVar = this.e.e;
        List<String> list = this.e.F;
        boolean a7 = ax.i().l().a();
        int i8 = displayMetrics.widthPixels;
        int i9 = displayMetrics.heightPixels;
        float f = displayMetrics.density;
        List a8 = asi.a();
        String str5 = this.e.f1989a;
        aul aul = this.e.w;
        String f2 = this.e.f();
        float a9 = ax.D().a();
        boolean b2 = ax.D().b();
        ax.e();
        int i10 = jv.i(this.e.c);
        ax.e();
        int d = jv.d((View) this.e.f);
        boolean z = this.e.c instanceof Activity;
        boolean f3 = ax.i().l().f();
        boolean d2 = ax.i().d();
        int a10 = ax.z().a();
        ax.e();
        Bundle c2 = jv.c();
        String a11 = ax.o().a();
        aqy aqy = this.e.y;
        boolean b3 = ax.o().b();
        Bundle j3 = azb.a().j();
        boolean e3 = ax.i().l().e(this.e.f1990b);
        List<Integer> list2 = this.e.A;
        boolean a12 = c.b(this.e.c).a();
        boolean e4 = ax.i().e();
        ax.g();
        return new dm(bundle2, aop, aot, str4, applicationInfo, packageInfo, a2, c, muVar, a4, list, arrayList, bundle, a7, i8, i9, f, a3, j2, uuid, a8, str5, aul, f2, a9, b2, i10, d, z, f3, a5, str2, d2, a10, c2, a11, aqy, b3, j3, e3, a6, list2, str3, arrayList2, i, a12, e4, kb.e(), (ArrayList) nc.a((Future<T>) ax.i().n(), null, 1000, TimeUnit.MILLISECONDS));
    }

    static String c(ir irVar) {
        if (irVar == null) {
            return null;
        }
        String str = irVar.q;
        if (!("com.google.android.gms.ads.mediation.customevent.CustomEventAdapter".equals(str) || "com.google.ads.mediation.customevent.CustomEventAdapter".equals(str)) || irVar.o == null) {
            return str;
        }
        try {
            return new JSONObject(irVar.o.k).getString("class_name");
        } catch (NullPointerException | JSONException e) {
            return str;
        }
    }

    public void I() {
        jm.e("showInterstitial is not supported for current ad type");
    }

    public void U() {
        e();
    }

    public void V() {
        ac();
    }

    public void W() {
        jm.e("Mediated ad does not support onVideoEnd callback");
    }

    /* access modifiers changed from: protected */
    public boolean X() {
        ax.e();
        if (jv.a(this.e.c, "android.permission.INTERNET")) {
            ax.e();
            if (jv.a(this.e.c)) {
                return true;
            }
        }
        return false;
    }

    public final void Y() {
        p_();
    }

    public final void Z() {
        v();
    }

    public final String a() {
        if (this.e.j == null) {
            return null;
        }
        return this.e.j.q;
    }

    public final void a(avt avt, String str) {
        String str2;
        awe awe = null;
        if (avt != null) {
            try {
                str2 = avt.l();
            } catch (RemoteException e) {
                jm.c("Unable to call onCustomClick.", e);
                return;
            }
        } else {
            str2 = null;
        }
        if (!(this.e.u == null || str2 == null)) {
            awe = (awe) this.e.u.get(str2);
        }
        if (awe == null) {
            jm.e("Mediation adapter invoked onCustomClick but no listeners were set.");
        } else {
            awe.a(avt, str);
        }
    }

    /* access modifiers changed from: protected */
    public void a(ir irVar, boolean z) {
        if (irVar == null) {
            jm.e("Ad state was null when trying to ping impression URLs.");
            return;
        }
        if (irVar == null) {
            jm.e("Ad state was null when trying to ping impression URLs.");
        } else {
            jm.b("Pinging Impression URLs.");
            if (this.e.l != null) {
                this.e.l.a();
            }
            irVar.K.a(b.AD_IMPRESSION);
            if (irVar.e != null && !irVar.D) {
                ax.e();
                jv.a(this.e.c, this.e.e.f3528a, b(irVar.e));
                irVar.D = true;
            }
        }
        if (!irVar.F || z) {
            if (!(irVar.r == null || irVar.r.d == null)) {
                ax.x();
                bck.a(this.e.c, this.e.e.f3528a, irVar, this.e.f1990b, z, b(irVar.r.d));
            }
            if (!(irVar.o == null || irVar.o.g == null)) {
                ax.x();
                bck.a(this.e.c, this.e.e.f3528a, irVar, this.e.f1990b, z, irVar.o.g);
            }
            irVar.F = true;
        }
    }

    public boolean a(aop aop, asv asv) {
        return a(aop, asv, 1);
    }

    public final boolean a(aop aop, asv asv, int i) {
        iv ivVar;
        if (!X()) {
            return false;
        }
        ax.e();
        aln a2 = ax.i().a(this.e.c);
        Bundle a3 = a2 == null ? null : jv.a(a2);
        this.d.a();
        this.e.I = 0;
        if (((Boolean) ape.f().a(asi.cs)).booleanValue()) {
            ivVar = ax.i().l().h();
            ax.m().a(this.e.c, this.e.e, false, ivVar, ivVar != null ? ivVar.d() : null, this.e.f1990b, null);
        } else {
            ivVar = null;
        }
        return a(a(aop, a3, ivVar, i), asv);
    }

    /* access modifiers changed from: protected */
    public boolean a(aop aop, ir irVar, boolean z) {
        if (!z && this.e.d()) {
            if (irVar.i > 0) {
                this.d.a(aop, irVar.i);
            } else if (irVar.r != null && irVar.r.j > 0) {
                this.d.a(aop, irVar.r.j);
            } else if (!irVar.n && irVar.d == 2) {
                this.d.b(aop);
            }
        }
        return this.d.e();
    }

    public final boolean a(dm dmVar, asv asv) {
        this.f1950a = asv;
        asv.a("seq_num", dmVar.g);
        asv.a("request_id", dmVar.v);
        asv.a("session_id", dmVar.h);
        if (dmVar.f != null) {
            asv.a("app_version", String.valueOf(dmVar.f.versionCode));
        }
        ay ayVar = this.e;
        ax.a();
        Context context = this.e.c;
        anb anb = this.i.d;
        jh cpVar = dmVar.f3263b.c.getBundle("sdk_less_server_data") != null ? new eh(context, dmVar, this, anb) : new cp(context, dmVar, this, anb);
        cpVar.h();
        ayVar.g = cpVar;
        return true;
    }

    /* access modifiers changed from: 0000 */
    public final boolean a(ir irVar) {
        aop aop;
        boolean z = false;
        if (this.f != null) {
            aop = this.f;
            this.f = null;
        } else {
            aop = irVar.f3395a;
            if (aop.c != null) {
                z = aop.c.getBoolean("_noRefresh", false);
            }
        }
        return a(aop, irVar, z);
    }

    /* access modifiers changed from: protected */
    public boolean a(ir irVar, ir irVar2) {
        int i;
        int i2 = 0;
        if (!(irVar == null || irVar.s == null)) {
            irVar.s.a((bcc) null);
        }
        if (irVar2.s != null) {
            irVar2.s.a((bcc) this);
        }
        if (irVar2.r != null) {
            i = irVar2.r.r;
            i2 = irVar2.r.s;
        } else {
            i = 0;
        }
        this.e.G.a(i, i2);
        return true;
    }

    public final void aa() {
        g();
    }

    public final void ab() {
        if (this.e.j != null) {
            String str = this.e.j.q;
            jm.e(new StringBuilder(String.valueOf(str).length() + 74).append("Mediation adapter ").append(str).append(" refreshed, but mediation adapters should never refresh.").toString());
        }
        a(this.e.j, true);
        b(this.e.j, true);
        x();
    }

    public final void ac() {
        a(this.e.j, false);
    }

    public final void b(ir irVar) {
        super.b(irVar);
        if (irVar.o != null) {
            jm.b("Disable the debug gesture detector on the mediation ad frame.");
            if (this.e.f != null) {
                this.e.f.d();
            }
            jm.b("Pinging network fill URLs.");
            ax.x();
            bck.a(this.e.c, this.e.e.f3528a, irVar, this.e.f1990b, false, irVar.o.j);
            if (!(irVar.r == null || irVar.r.g == null || irVar.r.g.size() <= 0)) {
                jm.b("Pinging urls remotely");
                ax.e().a(this.e.c, irVar.r.g);
            }
        } else {
            jm.b("Enable the debug gesture detector on the admob ad frame.");
            if (this.e.f != null) {
                this.e.f.c();
            }
        }
        if (irVar.d == 3 && irVar.r != null && irVar.r.f != null) {
            jm.b("Pinging no fill URLs.");
            ax.x();
            bck.a(this.e.c, this.e.e.f3528a, irVar, this.e.f1990b, false, irVar.r.f);
        }
    }

    /* access modifiers changed from: protected */
    public final void b(ir irVar, boolean z) {
        if (irVar != null) {
            if (!(irVar == null || irVar.f == null || irVar.E)) {
                ax.e();
                jv.a(this.e.c, this.e.e.f3528a, a(irVar.f));
                irVar.E = true;
            }
            if (!irVar.G || z) {
                if (!(irVar.r == null || irVar.r.e == null)) {
                    ax.x();
                    bck.a(this.e.c, this.e.e.f3528a, irVar, this.e.f1990b, z, a(irVar.r.e));
                }
                if (!(irVar.o == null || irVar.o.h == null)) {
                    ax.x();
                    bck.a(this.e.c, this.e.e.f3528a, irVar, this.e.f1990b, z, irVar.o.h);
                }
                irVar.G = true;
            }
        }
    }

    public final void b(String str, String str2) {
        a(str, str2);
    }

    /* access modifiers changed from: protected */
    public final boolean c(aop aop) {
        return super.c(aop) && !this.k;
    }

    public final void d() {
        this.g.c(this.e.j);
    }

    public void e() {
        if (this.e.j == null) {
            jm.e("Ad state was null when trying to ping click URLs.");
            return;
        }
        if (!(this.e.j.r == null || this.e.j.r.c == null)) {
            ax.x();
            bck.a(this.e.c, this.e.e.f3528a, this.e.j, this.e.f1990b, false, b(this.e.j.r.c));
        }
        if (!(this.e.j.o == null || this.e.j.o.f == null)) {
            ax.x();
            bck.a(this.e.c, this.e.e.f3528a, this.e.j, this.e.f1990b, false, this.e.j.o.f);
        }
        super.e();
    }

    public final void f() {
        this.g.d(this.e.j);
    }

    public void g() {
        this.k = true;
        w();
    }

    public final void h_() {
        Executor executor = nt.f3558a;
        an anVar = this.d;
        anVar.getClass();
        executor.execute(bc.a(anVar));
    }

    public final void i_() {
        Executor executor = nt.f3558a;
        an anVar = this.d;
        anVar.getClass();
        executor.execute(bd.a(anVar));
    }

    public void o() {
        aa.b("pause must be called on the main UI thread.");
        if (!(this.e.j == null || this.e.j.f3396b == null || !this.e.d())) {
            ax.g();
            kb.a(this.e.j.f3396b);
        }
        if (!(this.e.j == null || this.e.j.p == null)) {
            try {
                this.e.j.p.d();
            } catch (RemoteException e) {
                jm.e("Could not pause mediation adapter.");
            }
        }
        this.g.c(this.e.j);
        this.d.b();
    }

    public void p() {
        aa.b("resume must be called on the main UI thread.");
        qn qnVar = null;
        if (!(this.e.j == null || this.e.j.f3396b == null)) {
            qnVar = this.e.j.f3396b;
        }
        if (qnVar != null && this.e.d()) {
            ax.g();
            kb.b(this.e.j.f3396b);
        }
        if (!(this.e.j == null || this.e.j.p == null)) {
            try {
                this.e.j.p.e();
            } catch (RemoteException e) {
                jm.e("Could not resume mediation adapter.");
            }
        }
        if (qnVar == null || !qnVar.D()) {
            this.d.c();
        }
        this.g.d(this.e.j);
    }

    public void p_() {
        this.k = false;
        u();
        this.e.l.c();
    }

    public final String q_() {
        if (this.e.j == null) {
            return null;
        }
        return c(this.e.j);
    }
}
