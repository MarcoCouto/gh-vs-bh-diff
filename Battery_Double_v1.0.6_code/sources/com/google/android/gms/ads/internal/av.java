package com.google.android.gms.ads.internal;

import android.os.AsyncTask;
import com.google.android.gms.internal.ads.ahh;
import com.google.android.gms.internal.ads.ape;
import com.google.android.gms.internal.ads.asi;
import com.google.android.gms.internal.ads.ms;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

final class av extends AsyncTask<Void, Void, String> {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ ar f1984a;

    private av(ar arVar) {
        this.f1984a = arVar;
    }

    /* synthetic */ av(ar arVar, as asVar) {
        this(arVar);
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public final String doInBackground(Void... voidArr) {
        try {
            this.f1984a.h = (ahh) this.f1984a.c.get(((Long) ape.f().a(asi.cz)).longValue(), TimeUnit.MILLISECONDS);
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            ms.c("", e);
        }
        return this.f1984a.c();
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void onPostExecute(Object obj) {
        String str = (String) obj;
        if (this.f1984a.f != null && str != null) {
            this.f1984a.f.loadUrl(str);
        }
    }
}
