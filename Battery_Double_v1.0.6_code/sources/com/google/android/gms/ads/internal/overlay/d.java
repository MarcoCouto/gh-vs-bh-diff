package com.google.android.gms.ads.internal.overlay;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.Window;
import android.webkit.WebChromeClient.CustomViewCallback;
import android.widget.FrameLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.google.android.gms.ads.internal.ax;
import com.google.android.gms.b.a;
import com.google.android.gms.b.b;
import com.google.android.gms.common.util.n;
import com.google.android.gms.internal.ads.amw;
import com.google.android.gms.internal.ads.ape;
import com.google.android.gms.internal.ads.asi;
import com.google.android.gms.internal.ads.cm;
import com.google.android.gms.internal.ads.jm;
import com.google.android.gms.internal.ads.jv;
import com.google.android.gms.internal.ads.kb;
import com.google.android.gms.internal.ads.qn;
import com.google.android.gms.internal.ads.qu;
import com.google.android.gms.internal.ads.rv;
import com.google.android.gms.internal.ads.rw;
import com.google.android.gms.internal.ads.s;
import java.util.Collections;

@cm
public class d extends s implements w {
    private static final int e = Color.argb(0, 0, 0, 0);

    /* renamed from: a reason: collision with root package name */
    protected final Activity f2080a;

    /* renamed from: b reason: collision with root package name */
    AdOverlayInfoParcel f2081b;
    qn c;
    int d = 0;
    private i f;
    private o g;
    private boolean h = false;
    private FrameLayout i;
    private CustomViewCallback j;
    private boolean k = false;
    private boolean l = false;
    private h m;
    private boolean n = false;
    private final Object o = new Object();
    private Runnable p;
    private boolean q;
    private boolean r;
    private boolean s = false;
    private boolean t = false;
    private boolean u = true;

    public d(Activity activity) {
        this.f2080a = activity;
    }

    private final void a(boolean z) {
        int intValue = ((Integer) ape.f().a(asi.da)).intValue();
        p pVar = new p();
        pVar.e = 50;
        pVar.f2093a = z ? intValue : 0;
        pVar.f2094b = z ? 0 : intValue;
        pVar.c = 0;
        pVar.d = intValue;
        this.g = new o(this.f2080a, pVar, this);
        LayoutParams layoutParams = new LayoutParams(-2, -2);
        layoutParams.addRule(10);
        layoutParams.addRule(z ? 11 : 9);
        a(z, this.f2081b.g);
        this.m.addView(this.g, layoutParams);
    }

    /* JADX WARNING: Removed duplicated region for block: B:111:0x02f2  */
    /* JADX WARNING: Removed duplicated region for block: B:114:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00af  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00bb  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00c6  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x011d  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0128  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x0143 A[SYNTHETIC, Splitter:B:51:0x0143] */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x0234  */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x0266  */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x0270  */
    /* JADX WARNING: Removed duplicated region for block: B:89:0x0273  */
    /* JADX WARNING: Removed duplicated region for block: B:98:0x02a3  */
    private final void b(boolean z) throws g {
        boolean z2;
        boolean z3;
        ViewParent parent;
        if (!this.r) {
            this.f2080a.requestWindowFeature(1);
        }
        Window window = this.f2080a.getWindow();
        if (window == null) {
            throw new g("Invalid activity, no window available.");
        }
        if (n.k()) {
            if (((Boolean) ape.f().a(asi.cY)).booleanValue()) {
                ax.e();
                z2 = jv.a(this.f2080a, this.f2080a.getResources().getConfiguration());
                boolean z4 = this.f2081b.o == null && this.f2081b.o.f2102b;
                if ((!this.l || z4) && z2) {
                    window.setFlags(1024, 1024);
                    if (((Boolean) ape.f().a(asi.aQ)).booleanValue() && n.g() && this.f2081b.o != null && this.f2081b.o.f) {
                        window.getDecorView().setSystemUiVisibility(4098);
                    }
                }
                rv rvVar = this.f2081b.d == null ? this.f2081b.d.v() : null;
                z3 = rvVar == null ? rvVar.b() : false;
                this.n = false;
                if (z3) {
                    if (this.f2081b.j == ax.g().a()) {
                        this.n = this.f2080a.getResources().getConfiguration().orientation == 1;
                    } else if (this.f2081b.j == ax.g().b()) {
                        this.n = this.f2080a.getResources().getConfiguration().orientation == 2;
                    }
                }
                jm.b("Delay onShow to next orientation change: " + this.n);
                a(this.f2081b.j);
                if (ax.g().a(window)) {
                    jm.b("Hardware acceleration on the AdActivity window enabled.");
                }
                if (this.l) {
                    this.m.setBackgroundColor(-16777216);
                } else {
                    this.m.setBackgroundColor(e);
                }
                this.f2080a.setContentView(this.m);
                this.r = true;
                if (!z) {
                    try {
                        ax.f();
                        this.c = qu.a(this.f2080a, this.f2081b.d != null ? this.f2081b.d.t() : null, this.f2081b.d != null ? this.f2081b.d.u() : null, true, z3, null, this.f2081b.m, null, null, this.f2081b.d != null ? this.f2081b.d.e() : null, amw.a());
                        this.c.v().a(null, this.f2081b.p, null, this.f2081b.e, this.f2081b.i, true, null, this.f2081b.d != null ? this.f2081b.d.v().a() : null, null, null);
                        this.c.v().a((rw) new e(this));
                        if (this.f2081b.l != null) {
                            this.c.loadUrl(this.f2081b.l);
                        } else if (this.f2081b.h != null) {
                            this.c.loadDataWithBaseURL(this.f2081b.f, this.f2081b.h, "text/html", "UTF-8", null);
                        } else {
                            throw new g("No URL or HTML to display in ad overlay.");
                        }
                        if (this.f2081b.d != null) {
                            this.f2081b.d.b(this);
                        }
                    } catch (Exception e2) {
                        jm.b("Error obtaining webview.", e2);
                        throw new g("Could not obtain webview for the overlay.");
                    }
                } else {
                    this.c = this.f2081b.d;
                    this.c.a((Context) this.f2080a);
                }
                this.c.a(this);
                parent = this.c.getParent();
                if (parent != null && (parent instanceof ViewGroup)) {
                    ((ViewGroup) parent).removeView(this.c.getView());
                }
                if (this.l) {
                    this.c.I();
                }
                this.m.addView(this.c.getView(), -1, -1);
                if (!z && !this.n) {
                    s();
                }
                a(z3);
                if (!this.c.x()) {
                    a(z3, true);
                    return;
                }
                return;
            }
        }
        z2 = true;
        if (this.f2081b.o == null) {
        }
        window.setFlags(1024, 1024);
        window.getDecorView().setSystemUiVisibility(4098);
        if (this.f2081b.d == null) {
        }
        if (rvVar == null) {
        }
        this.n = false;
        if (z3) {
        }
        jm.b("Delay onShow to next orientation change: " + this.n);
        a(this.f2081b.j);
        if (ax.g().a(window)) {
        }
        if (this.l) {
        }
        this.f2080a.setContentView(this.m);
        this.r = true;
        if (!z) {
        }
        this.c.a(this);
        parent = this.c.getParent();
        ((ViewGroup) parent).removeView(this.c.getView());
        if (this.l) {
        }
        this.m.addView(this.c.getView(), -1, -1);
        s();
        a(z3);
        if (!this.c.x()) {
        }
    }

    private final void r() {
        if (this.f2080a.isFinishing() && !this.s) {
            this.s = true;
            if (this.c != null) {
                this.c.a(this.d);
                synchronized (this.o) {
                    if (!this.q && this.c.E()) {
                        this.p = new f(this);
                        jv.f3440a.postDelayed(this.p, ((Long) ape.f().a(asi.aP)).longValue());
                        return;
                    }
                }
            }
            n();
        }
    }

    private final void s() {
        this.c.o();
    }

    public final void a() {
        this.d = 2;
        this.f2080a.finish();
    }

    public final void a(int i2) {
        if (this.f2080a.getApplicationInfo().targetSdkVersion >= ((Integer) ape.f().a(asi.dn)).intValue()) {
            if (this.f2080a.getApplicationInfo().targetSdkVersion <= ((Integer) ape.f().a(asi.f0do)).intValue()) {
                if (VERSION.SDK_INT >= ((Integer) ape.f().a(asi.dp)).intValue()) {
                    if (VERSION.SDK_INT <= ((Integer) ape.f().a(asi.dq)).intValue()) {
                        return;
                    }
                }
            }
        }
        this.f2080a.setRequestedOrientation(i2);
    }

    public final void a(int i2, int i3, Intent intent) {
    }

    public void a(Bundle bundle) {
        boolean z = false;
        this.f2080a.requestWindowFeature(1);
        if (bundle != null) {
            z = bundle.getBoolean("com.google.android.gms.ads.internal.overlay.hasResumed", false);
        }
        this.k = z;
        try {
            this.f2081b = AdOverlayInfoParcel.a(this.f2080a.getIntent());
            if (this.f2081b == null) {
                throw new g("Could not get info for ad overlay.");
            }
            if (this.f2081b.m.c > 7500000) {
                this.d = 3;
            }
            if (this.f2080a.getIntent() != null) {
                this.u = this.f2080a.getIntent().getBooleanExtra("shouldCallOnOverlayOpened", true);
            }
            if (this.f2081b.o != null) {
                this.l = this.f2081b.o.f2101a;
            } else {
                this.l = false;
            }
            if (((Boolean) ape.f().a(asi.bR)).booleanValue() && this.l && this.f2081b.o.e != -1) {
                new j(this, null).h();
            }
            if (bundle == null) {
                if (this.f2081b.c != null && this.u) {
                    this.f2081b.c.g();
                }
                if (!(this.f2081b.k == 1 || this.f2081b.f2077b == null)) {
                    this.f2081b.f2077b.e();
                }
            }
            this.m = new h(this.f2080a, this.f2081b.n, this.f2081b.m.f3528a);
            this.m.setId(1000);
            switch (this.f2081b.k) {
                case 1:
                    b(false);
                    return;
                case 2:
                    this.f = new i(this.f2081b.d);
                    b(false);
                    return;
                case 3:
                    b(true);
                    return;
                default:
                    throw new g("Could not determine ad overlay type.");
            }
        } catch (g e2) {
            jm.e(e2.getMessage());
            this.d = 3;
            this.f2080a.finish();
        }
    }

    public final void a(View view, CustomViewCallback customViewCallback) {
        this.i = new FrameLayout(this.f2080a);
        this.i.setBackgroundColor(-16777216);
        this.i.addView(view, -1, -1);
        this.f2080a.setContentView(this.i);
        this.r = true;
        this.j = customViewCallback;
        this.h = true;
    }

    public final void a(a aVar) {
        if (((Boolean) ape.f().a(asi.cY)).booleanValue() && n.k()) {
            Configuration configuration = (Configuration) b.a(aVar);
            ax.e();
            if (jv.a(this.f2080a, configuration)) {
                this.f2080a.getWindow().addFlags(1024);
                this.f2080a.getWindow().clearFlags(2048);
                return;
            }
            this.f2080a.getWindow().addFlags(2048);
            this.f2080a.getWindow().clearFlags(1024);
        }
    }

    public final void a(boolean z, boolean z2) {
        boolean z3 = false;
        boolean z4 = ((Boolean) ape.f().a(asi.aR)).booleanValue() && this.f2081b != null && this.f2081b.o != null && this.f2081b.o.g;
        boolean z5 = ((Boolean) ape.f().a(asi.aS)).booleanValue() && this.f2081b != null && this.f2081b.o != null && this.f2081b.o.h;
        if (z && z2 && z4 && !z5) {
            new com.google.android.gms.internal.ads.n(this.c, "useCustomClose").a("Custom close has been disabled for interstitial ads in this ad slot.");
        }
        if (this.g != null) {
            o oVar = this.g;
            if (z5 || (z2 && !z4)) {
                z3 = true;
            }
            oVar.a(z3);
        }
    }

    public final void b() {
        if (this.f2081b != null && this.h) {
            a(this.f2081b.j);
        }
        if (this.i != null) {
            this.f2080a.setContentView(this.m);
            this.r = true;
            this.i.removeAllViews();
            this.i = null;
        }
        if (this.j != null) {
            this.j.onCustomViewHidden();
            this.j = null;
        }
        this.h = false;
    }

    public final void b(Bundle bundle) {
        bundle.putBoolean("com.google.android.gms.ads.internal.overlay.hasResumed", this.k);
    }

    public final void c() {
        this.d = 1;
        this.f2080a.finish();
    }

    public final void d() {
        this.d = 0;
    }

    public final boolean e() {
        this.d = 0;
        if (this.c == null) {
            return true;
        }
        boolean C = this.c.C();
        if (C) {
            return C;
        }
        this.c.a("onbackblocked", Collections.emptyMap());
        return C;
    }

    public final void f() {
    }

    public final void g() {
        if (!((Boolean) ape.f().a(asi.cZ)).booleanValue()) {
            return;
        }
        if (this.c == null || this.c.A()) {
            jm.e("The webview does not exist. Ignoring action.");
            return;
        }
        ax.g();
        kb.b(this.c);
    }

    public final void h() {
        if (this.f2081b.c != null) {
            this.f2081b.c.f();
        }
        if (((Boolean) ape.f().a(asi.cZ)).booleanValue()) {
            return;
        }
        if (this.c == null || this.c.A()) {
            jm.e("The webview does not exist. Ignoring action.");
            return;
        }
        ax.g();
        kb.b(this.c);
    }

    public final void i() {
        b();
        if (this.f2081b.c != null) {
            this.f2081b.c.d();
        }
        if (!((Boolean) ape.f().a(asi.cZ)).booleanValue() && this.c != null && (!this.f2080a.isFinishing() || this.f == null)) {
            ax.g();
            kb.a(this.c);
        }
        r();
    }

    public final void j() {
        if (((Boolean) ape.f().a(asi.cZ)).booleanValue() && this.c != null && (!this.f2080a.isFinishing() || this.f == null)) {
            ax.g();
            kb.a(this.c);
        }
        r();
    }

    public final void k() {
        if (this.c != null) {
            this.m.removeView(this.c.getView());
        }
        r();
    }

    public final void l() {
        this.r = true;
    }

    public final void m() {
        this.m.removeView(this.g);
        a(true);
    }

    /* access modifiers changed from: 0000 */
    public final void n() {
        if (!this.t) {
            this.t = true;
            if (this.c != null) {
                this.m.removeView(this.c.getView());
                if (this.f != null) {
                    this.c.a(this.f.d);
                    this.c.b(false);
                    this.f.c.addView(this.c.getView(), this.f.f2086a, this.f.f2087b);
                    this.f = null;
                } else if (this.f2080a.getApplicationContext() != null) {
                    this.c.a(this.f2080a.getApplicationContext());
                }
                this.c = null;
            }
            if (this.f2081b != null && this.f2081b.c != null) {
                this.f2081b.c.p_();
            }
        }
    }

    public final void o() {
        if (this.n) {
            this.n = false;
            s();
        }
    }

    public final void p() {
        this.m.f2084a = true;
    }

    public final void q() {
        synchronized (this.o) {
            this.q = true;
            if (this.p != null) {
                jv.f3440a.removeCallbacks(this.p);
                jv.f3440a.post(this.p);
            }
        }
    }
}
