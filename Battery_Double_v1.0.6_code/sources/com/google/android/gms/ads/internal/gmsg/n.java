package com.google.android.gms.ads.internal.gmsg;

import android.text.TextUtils;
import com.google.android.gms.ads.internal.ax;
import com.google.android.gms.internal.ads.asv;
import com.google.android.gms.internal.ads.cm;
import com.google.android.gms.internal.ads.jm;
import com.google.android.gms.internal.ads.qn;
import java.util.Map;

@cm
public final class n implements ae<qn> {
    public final /* synthetic */ void zza(Object obj, Map map) {
        qn qnVar = (qn) obj;
        String str = (String) map.get("action");
        if ("tick".equals(str)) {
            String str2 = (String) map.get("label");
            String str3 = (String) map.get("start_label");
            String str4 = (String) map.get("timestamp");
            if (TextUtils.isEmpty(str2)) {
                jm.e("No label given for CSI tick.");
            } else if (TextUtils.isEmpty(str4)) {
                jm.e("No timestamp given for CSI tick.");
            } else {
                try {
                    long parseLong = (Long.parseLong(str4) - ax.l().a()) + ax.l().b();
                    if (TextUtils.isEmpty(str3)) {
                        str3 = "native:view_load";
                    }
                    qnVar.j().a(str2, str3, parseLong);
                } catch (NumberFormatException e) {
                    jm.c("Malformed timestamp for CSI tick.", e);
                }
            }
        } else if ("experiment".equals(str)) {
            String str5 = (String) map.get("value");
            if (TextUtils.isEmpty(str5)) {
                jm.e("No value given for CSI experiment.");
                return;
            }
            asv a2 = qnVar.j().a();
            if (a2 == null) {
                jm.e("No ticker for WebView, dropping experiment ID.");
            } else {
                a2.a("e", str5);
            }
        } else if ("extra".equals(str)) {
            String str6 = (String) map.get("name");
            String str7 = (String) map.get("value");
            if (TextUtils.isEmpty(str7)) {
                jm.e("No value given for CSI extra.");
            } else if (TextUtils.isEmpty(str6)) {
                jm.e("No name given for CSI extra.");
            } else {
                asv a3 = qnVar.j().a();
                if (a3 == null) {
                    jm.e("No ticker for WebView, dropping extra parameter.");
                } else {
                    a3.a(str6, str7);
                }
            }
        }
    }
}
