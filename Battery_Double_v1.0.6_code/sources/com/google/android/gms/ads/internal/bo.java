package com.google.android.gms.ads.internal;

import android.content.Context;
import android.os.RemoteException;
import android.view.View;
import com.google.android.gms.b.a;
import com.google.android.gms.b.b;
import com.google.android.gms.common.internal.aa;
import com.google.android.gms.internal.ads.ao;
import com.google.android.gms.internal.ads.aop;
import com.google.android.gms.internal.ads.aot;
import com.google.android.gms.internal.ads.aqs;
import com.google.android.gms.internal.ads.asv;
import com.google.android.gms.internal.ads.atc;
import com.google.android.gms.internal.ads.atn;
import com.google.android.gms.internal.ads.atp;
import com.google.android.gms.internal.ads.atu;
import com.google.android.gms.internal.ads.atw;
import com.google.android.gms.internal.ads.atx;
import com.google.android.gms.internal.ads.aty;
import com.google.android.gms.internal.ads.aua;
import com.google.android.gms.internal.ads.aub;
import com.google.android.gms.internal.ads.avt;
import com.google.android.gms.internal.ads.awe;
import com.google.android.gms.internal.ads.bcb;
import com.google.android.gms.internal.ads.bcr;
import com.google.android.gms.internal.ads.bdd;
import com.google.android.gms.internal.ads.bdh;
import com.google.android.gms.internal.ads.bdk;
import com.google.android.gms.internal.ads.cm;
import com.google.android.gms.internal.ads.ir;
import com.google.android.gms.internal.ads.is;
import com.google.android.gms.internal.ads.jm;
import com.google.android.gms.internal.ads.jv;
import com.google.android.gms.internal.ads.ms;
import com.google.android.gms.internal.ads.mu;
import com.google.android.gms.internal.ads.qn;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@cm
public final class bo extends bb implements aua {
    private boolean k;
    /* access modifiers changed from: private */
    public ir l;
    private boolean m = false;

    public bo(Context context, bu buVar, aot aot, String str, bcr bcr, mu muVar) {
        super(context, aot, str, bcr, muVar, buVar);
    }

    private final bcb J() {
        if (this.e.j == null || !this.e.j.n) {
            return null;
        }
        return this.e.j.r;
    }

    private static ir a(is isVar, int i) {
        return new ir(isVar.f3397a.c, null, isVar.f3398b.c, i, isVar.f3398b.e, isVar.f3398b.i, isVar.f3398b.k, isVar.f3398b.j, isVar.f3397a.i, isVar.f3398b.g, null, null, null, isVar.c, null, isVar.f3398b.h, isVar.d, isVar.f3398b.f, isVar.f, isVar.g, isVar.f3398b.n, isVar.h, null, isVar.f3398b.A, isVar.f3398b.B, isVar.f3398b.B, isVar.f3398b.D, isVar.f3398b.E, null, isVar.f3398b.H, isVar.f3398b.L, isVar.i, isVar.f3398b.O, isVar.j, isVar.f3398b.Q, isVar.f3398b.R, isVar.f3398b.S, isVar.f3398b.T);
    }

    private final void a(atu atu) {
        jv.f3440a.post(new bq(this, atu));
    }

    private final boolean b(ir irVar, ir irVar2) {
        c(null);
        if (!this.e.d()) {
            jm.e("Native ad does not have custom rendering mode.");
            a(0);
            return false;
        }
        try {
            bdk bdk = irVar2.p != null ? irVar2.p.p() : null;
            bdd bdd = irVar2.p != null ? irVar2.p.h() : null;
            bdh bdh = irVar2.p != null ? irVar2.p.i() : null;
            avt avt = irVar2.p != null ? irVar2.p.n() : null;
            String c = c(irVar2);
            if (bdk == null || this.e.t == null) {
                if (bdd != null) {
                    if (this.e.t != null) {
                        atu atu = new atu(bdd.a(), bdd.b(), bdd.c(), bdd.d() != null ? bdd.d() : null, bdd.e(), null, bdd.f(), bdd.g(), bdd.h(), null, bdd.m(), bdd.p() != null ? (View) b.a(bdd.p()) : null, bdd.q(), c, bdd.l());
                        atu.a((aty) new atx(this.e.c, (aua) this, this.e.d, bdd, (aub) atu));
                        a(atu);
                    }
                }
                if (bdd != null) {
                    if (this.e.r != null) {
                        atn atn = new atn(bdd.a(), bdd.b(), bdd.c(), bdd.d() != null ? bdd.d() : null, bdd.e(), bdd.f(), bdd.g(), bdd.h(), null, bdd.l(), bdd.m(), bdd.p() != null ? (View) b.a(bdd.p()) : null, bdd.q(), c);
                        atn.a((aty) new atx(this.e.c, (aua) this, this.e.d, bdd, (aub) atn));
                        jv.f3440a.post(new br(this, atn));
                    }
                }
                if (bdh != null && this.e.t != null) {
                    atu atu2 = new atu(bdh.a(), bdh.b(), bdh.c(), bdh.d() != null ? bdh.d() : null, bdh.e(), bdh.f(), -1.0d, null, null, null, bdh.l(), bdh.n() != null ? (View) b.a(bdh.n()) : null, bdh.o(), c, bdh.j());
                    atu2.a((aty) new atx(this.e.c, (aua) this, this.e.d, bdh, (aub) atu2));
                    a(atu2);
                } else if (bdh != null && this.e.s != null) {
                    atp atp = new atp(bdh.a(), bdh.b(), bdh.c(), bdh.d() != null ? bdh.d() : null, bdh.e(), bdh.f(), null, bdh.j(), bdh.l(), bdh.n() != null ? (View) b.a(bdh.n()) : null, bdh.o(), c);
                    atp.a((aty) new atx(this.e.c, (aua) this, this.e.d, bdh, (aub) atp));
                    jv.f3440a.post(new bs(this, atp));
                } else if (avt == null || this.e.v == null || this.e.v.get(avt.l()) == null) {
                    jm.e("No matching mapper/listener for retrieved native ad template.");
                    a(0);
                    return false;
                } else {
                    jv.f3440a.post(new bt(this, avt));
                }
            } else {
                atu atu3 = new atu(bdk.a(), bdk.b(), bdk.c(), bdk.d() != null ? bdk.d() : null, bdk.e(), bdk.f(), bdk.g(), bdk.h(), bdk.i(), null, bdk.j(), bdk.m() != null ? (View) b.a(bdk.m()) : null, bdk.n(), c, bdk.o());
                atu3.a((aty) new atx(this.e.c, (aua) this, this.e.d, bdk, (aub) atu3));
                a(atu3);
            }
            return super.a(irVar, irVar2);
        } catch (RemoteException e) {
            jm.d("#007 Could not call remote method.", e);
            a(0);
            return false;
        }
    }

    private final boolean c(ir irVar, ir irVar2) {
        View a2 = t.a(irVar2);
        if (a2 == null) {
            return false;
        }
        View nextView = this.e.f.getNextView();
        if (nextView != null) {
            if (nextView instanceof qn) {
                ((qn) nextView).destroy();
            }
            this.e.f.removeView(nextView);
        }
        if (!t.b(irVar2)) {
            try {
                a(a2);
            } catch (Throwable th) {
                ax.i().a(th, "AdLoaderManager.swapBannerViews");
                jm.c("Could not add mediation view to view hierarchy.", th);
                return false;
            }
        }
        if (this.e.f.getChildCount() > 1) {
            this.e.f.showNext();
        }
        if (irVar != null) {
            View nextView2 = this.e.f.getNextView();
            if (nextView2 != null) {
                this.e.f.removeView(nextView2);
            }
            this.e.c();
        }
        this.e.f.setMinimumWidth(l().f);
        this.e.f.setMinimumHeight(l().c);
        this.e.f.requestLayout();
        this.e.f.setVisibility(0);
        return true;
    }

    public final void I() {
        throw new IllegalStateException("Interstitial is not supported by AdLoaderManager.");
    }

    public final void L() {
        ms.d("#005 Unexpected call to an abstract (unimplemented) method.", null);
    }

    public final void P() {
        ms.d("#005 Unexpected call to an abstract (unimplemented) method.", null);
    }

    public final void Q() {
        ms.d("#005 Unexpected call to an abstract (unimplemented) method.", null);
    }

    public final boolean S() {
        if (J() != null) {
            return J().p;
        }
        return false;
    }

    public final boolean T() {
        if (J() != null) {
            return J().q;
        }
        return false;
    }

    public final void U() {
        if (this.e.j == null || !"com.google.ads.mediation.admob.AdMobAdapter".equals(this.e.j.q) || this.e.j.o == null || !this.e.j.o.b()) {
            super.U();
        } else {
            z();
        }
    }

    public final void V() {
        if (this.e.j == null || !"com.google.ads.mediation.admob.AdMobAdapter".equals(this.e.j.q) || this.e.j.o == null || !this.e.j.o.b()) {
            super.V();
        } else {
            y();
        }
    }

    /* access modifiers changed from: protected */
    public final void a(a aVar) {
        Object obj = aVar != null ? b.a(aVar) : null;
        if (obj instanceof aty) {
            ((aty) obj).d();
        }
        super.b(this.e.j, false);
    }

    public final void a(atc atc) {
        throw new IllegalStateException("CustomRendering is not supported by AdLoaderManager.");
    }

    public final void a(atw atw) {
        ms.d("#005 Unexpected call to an abstract (unimplemented) method.", null);
    }

    public final void a(aty aty) {
        ms.d("#005 Unexpected call to an abstract (unimplemented) method.", null);
    }

    public final void a(is isVar, asv asv) {
        this.l = null;
        if (isVar.e != -2) {
            this.l = a(isVar, isVar.e);
        } else if (!isVar.f3398b.g) {
            jm.e("partialAdState is not mediation");
            this.l = a(isVar, 0);
        }
        if (this.l != null) {
            jv.f3440a.post(new bp(this));
            return;
        }
        if (isVar.d != null) {
            this.e.i = isVar.d;
        }
        this.e.I = 0;
        ay ayVar = this.e;
        ax.d();
        ayVar.h = ao.a(this.e.c, this, isVar, this.e.d, null, this.j, this, asv);
    }

    /* access modifiers changed from: protected */
    public final boolean a(aop aop, ir irVar, boolean z) {
        return false;
    }

    /* access modifiers changed from: protected */
    public final boolean a(ir irVar, ir irVar2) {
        boolean z;
        if (!this.e.d()) {
            throw new IllegalStateException("AdLoader API does not support custom rendering.");
        } else if (!irVar2.n) {
            a(0);
            jm.e("newState is not mediation.");
            return false;
        } else {
            if (irVar2.o != null && irVar2.o.a()) {
                if (this.e.d() && this.e.f != null) {
                    this.e.f.a().c(irVar2.A);
                }
                if (!super.a(irVar, irVar2)) {
                    z = false;
                } else if (!this.e.d() || c(irVar, irVar2)) {
                    if (!this.e.e()) {
                        super.a(irVar2, false);
                    }
                    z = true;
                } else {
                    a(0);
                    z = false;
                }
                if (!z) {
                    return false;
                }
                this.m = true;
            } else if (irVar2.o == null || !irVar2.o.b()) {
                a(0);
                jm.e("Response is neither banner nor native.");
                return false;
            } else if (!b(irVar, irVar2)) {
                return false;
            }
            d(new ArrayList(Arrays.asList(new Integer[]{Integer.valueOf(2)})));
            return true;
        }
    }

    public final awe b(String str) {
        aa.b("getOnCustomClickListener must be called on the main UI thread.");
        return (awe) this.e.u.get(str);
    }

    public final void b(View view) {
        ms.d("#005 Unexpected call to an abstract (unimplemented) method.", null);
    }

    public final void b(boolean z) {
        aa.b("setManualImpressionsEnabled must be called from the main thread.");
        this.k = z;
    }

    public final boolean b(aop aop) {
        if (this.e.A != null && this.e.A.size() == 1 && ((Integer) this.e.A.get(0)).intValue() == 2) {
            jm.c("Requesting only banner Ad from AdLoader or calling loadAd on returned banner is not yet supported");
            a(0);
            return false;
        } else if (this.e.z == null) {
            return super.b(aop);
        } else {
            if (aop.h != this.k) {
                aop = new aop(aop.f2809a, aop.f2810b, aop.c, aop.d, aop.e, aop.f, aop.g, aop.h || this.k, aop.i, aop.j, aop.k, aop.l, aop.m, aop.n, aop.o, aop.p, aop.q, aop.r);
            }
            return super.b(aop);
        }
    }

    public final void c(List<String> list) {
        aa.b("setNativeTemplates must be called on the main UI thread.");
        this.e.F = list;
    }

    public final void d(List<Integer> list) {
        aa.b("setAllowedAdTypes must be called on the main UI thread.");
        this.e.A = list;
    }

    public final void o() {
        if (!this.m) {
            throw new IllegalStateException("Native Ad does not support pause().");
        }
        super.o();
    }

    public final void p() {
        if (!this.m) {
            throw new IllegalStateException("Native Ad does not support resume().");
        }
        super.p();
    }

    public final aqs t() {
        return null;
    }

    /* access modifiers changed from: protected */
    public final void x() {
        super.x();
        ir irVar = this.e.j;
        if (irVar != null && irVar.o != null && irVar.o.a() && this.e.z != null) {
            try {
                this.e.z.a(this, b.a(this.e.c));
                super.b(this.e.j, false);
            } catch (RemoteException e) {
                jm.d("#007 Could not call remote method.", e);
            }
        }
    }
}
