package com.google.android.gms.ads.internal.overlay;

import com.google.android.gms.internal.ads.cm;

@cm
final class g extends Exception {
    public g(String str) {
        super(str);
    }
}
