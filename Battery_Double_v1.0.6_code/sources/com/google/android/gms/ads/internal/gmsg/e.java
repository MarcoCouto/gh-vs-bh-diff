package com.google.android.gms.ads.internal.gmsg;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.view.View;
import com.google.android.gms.ads.internal.ax;
import com.google.android.gms.ads.internal.bv;
import com.google.android.gms.ads.internal.overlay.c;
import com.google.android.gms.ads.internal.overlay.n;
import com.google.android.gms.ads.internal.overlay.t;
import com.google.android.gms.internal.ads.ahh;
import com.google.android.gms.internal.ads.ahi;
import com.google.android.gms.internal.ads.aoj;
import com.google.android.gms.internal.ads.cm;
import com.google.android.gms.internal.ads.d;
import com.google.android.gms.internal.ads.il;
import com.google.android.gms.internal.ads.jm;
import com.google.android.gms.internal.ads.mu;
import com.google.android.gms.internal.ads.qn;
import com.google.android.gms.internal.ads.rj;
import com.google.android.gms.internal.ads.rk;
import com.google.android.gms.internal.ads.ro;
import com.google.android.gms.internal.ads.rr;
import com.google.android.gms.internal.ads.ru;
import java.net.URISyntaxException;
import java.util.Map;

@cm
public final class e<T extends rj & rk & ro & rr & ru> implements ae<T> {

    /* renamed from: a reason: collision with root package name */
    private final Context f2048a;

    /* renamed from: b reason: collision with root package name */
    private final ahh f2049b;
    private final mu c;
    private final t d;
    private final aoj e;
    private final n f;
    private final k g;
    private final m h;
    private final bv i;
    private final d j;
    private final qn k = null;

    public e(Context context, mu muVar, ahh ahh, t tVar, aoj aoj, k kVar, m mVar, n nVar, bv bvVar, d dVar) {
        this.f2048a = context;
        this.c = muVar;
        this.f2049b = ahh;
        this.d = tVar;
        this.e = aoj;
        this.g = kVar;
        this.h = mVar;
        this.i = bvVar;
        this.j = dVar;
        this.f = nVar;
    }

    static String a(Context context, ahh ahh, String str, View view, Activity activity) {
        if (ahh == null) {
            return str;
        }
        try {
            Uri parse = Uri.parse(str);
            if (ahh.b(parse)) {
                parse = ahh.a(parse, context, view, activity);
            }
            return parse.toString();
        } catch (ahi e2) {
            return str;
        } catch (Exception e3) {
            ax.i().a((Throwable) e3, "OpenGmsgHandler.maybeAddClickSignalsToUrl");
            return str;
        }
    }

    private final void a(boolean z) {
        if (this.j != null) {
            this.j.a(z);
        }
    }

    private static boolean a(Map<String, String> map) {
        return "1".equals(map.get("custom_close"));
    }

    private static int b(Map<String, String> map) {
        String str = (String) map.get("o");
        if (str != null) {
            if ("p".equalsIgnoreCase(str)) {
                return ax.g().b();
            }
            if ("l".equalsIgnoreCase(str)) {
                return ax.g().a();
            }
            if ("c".equalsIgnoreCase(str)) {
                return ax.g().c();
            }
        }
        return -1;
    }

    /* JADX WARNING: Removed duplicated region for block: B:43:0x0121 A[SYNTHETIC, Splitter:B:43:0x0121] */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x0146  */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x01a4  */
    public final /* synthetic */ void zza(Object obj, Map map) {
        Intent intent;
        String uri;
        Uri uri2;
        String str;
        rj rjVar = (rj) obj;
        String a2 = il.a((String) map.get("u"), rjVar.getContext());
        String str2 = (String) map.get("a");
        if (str2 == null) {
            jm.e("Action missing from an open GMSG.");
        } else if (this.i != null && !this.i.b()) {
            this.i.a(a2);
        } else if ("expand".equalsIgnoreCase(str2)) {
            if (((rk) rjVar).z()) {
                jm.e("Cannot expand WebView that is already expanded.");
                return;
            }
            a(false);
            ((ro) rjVar).a(a(map), b(map));
        } else if ("webapp".equalsIgnoreCase(str2)) {
            a(false);
            if (a2 != null) {
                ((ro) rjVar).a(a(map), b(map), a2);
            } else {
                ((ro) rjVar).a(a(map), b(map), (String) map.get("html"), (String) map.get("baseurl"));
            }
        } else if (!"app".equalsIgnoreCase(str2) || !"true".equalsIgnoreCase((String) map.get("system_browser"))) {
            a(true);
            String str3 = (String) map.get("intent_url");
            if (!TextUtils.isEmpty(str3)) {
                try {
                    intent = Intent.parseUri(str3, 0);
                } catch (URISyntaxException e2) {
                    String str4 = "Error parsing the url: ";
                    String valueOf = String.valueOf(str3);
                    jm.b(valueOf.length() != 0 ? str4.concat(valueOf) : new String(str4), e2);
                }
                if (!(intent == null || intent.getData() == null)) {
                    Uri data = intent.getData();
                    uri = data.toString();
                    if (!TextUtils.isEmpty(uri)) {
                        try {
                            str = a(rjVar.getContext(), ((rr) rjVar).y(), uri, ((ru) rjVar).getView(), rjVar.d());
                        } catch (Exception e3) {
                            jm.b("Error occurred while adding signals.", e3);
                            ax.i().a((Throwable) e3, "OpenGmsgHandler.onGmsg");
                            str = uri;
                        }
                        try {
                            uri2 = Uri.parse(str);
                        } catch (Exception e4) {
                            String str5 = "Error parsing the uri: ";
                            String valueOf2 = String.valueOf(str);
                            jm.b(valueOf2.length() != 0 ? str5.concat(valueOf2) : new String(str5), e4);
                            ax.i().a((Throwable) e4, "OpenGmsgHandler.onGmsg");
                        }
                        intent.setData(uri2);
                    }
                    uri2 = data;
                    intent.setData(uri2);
                }
                if (intent == null) {
                    ((ro) rjVar).a(new c(intent));
                    return;
                }
                if (!TextUtils.isEmpty(a2)) {
                    a2 = a(rjVar.getContext(), ((rr) rjVar).y(), a2, ((ru) rjVar).getView(), rjVar.d());
                }
                ((ro) rjVar).a(new c((String) map.get("i"), a2, (String) map.get("m"), (String) map.get("p"), (String) map.get("c"), (String) map.get("f"), (String) map.get("e")));
                return;
            }
            intent = null;
            Uri data2 = intent.getData();
            uri = data2.toString();
            if (!TextUtils.isEmpty(uri)) {
            }
            uri2 = data2;
            intent.setData(uri2);
            if (intent == null) {
            }
        } else {
            a(true);
            rjVar.getContext();
            if (TextUtils.isEmpty(a2)) {
                jm.e("Destination url cannot be empty.");
                return;
            }
            try {
                ((ro) rjVar).a(new c(new f(rjVar.getContext(), ((rr) rjVar).y(), ((ru) rjVar).getView()).a(map)));
            } catch (ActivityNotFoundException e5) {
                jm.e(e5.getMessage());
            }
        }
    }
}
