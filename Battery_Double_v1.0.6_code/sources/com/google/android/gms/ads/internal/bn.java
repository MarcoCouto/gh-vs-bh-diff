package com.google.android.gms.ads.internal;

import android.os.Bundle;
import com.google.android.gms.internal.ads.aop;
import com.google.android.gms.internal.ads.aot;
import com.google.android.gms.internal.ads.cm;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.TreeSet;

@cm
public final class bn {
    private static String a(Bundle bundle) {
        if (bundle == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        Iterator it = new TreeSet(bundle.keySet()).iterator();
        while (it.hasNext()) {
            Object obj = bundle.get((String) it.next());
            String obj2 = obj == null ? "null" : obj instanceof Bundle ? a((Bundle) obj) : obj.toString();
            sb.append(obj2);
        }
        return sb.toString();
    }

    public static Object[] a(String str, aop aop, String str2, int i, aot aot) {
        HashSet hashSet = new HashSet(Arrays.asList(str.split(",")));
        ArrayList arrayList = new ArrayList();
        arrayList.add(str);
        arrayList.add(str2);
        if (hashSet.contains("formatString")) {
            arrayList.add(null);
        }
        if (hashSet.contains("networkType")) {
            arrayList.add(Integer.valueOf(i));
        }
        if (hashSet.contains("birthday")) {
            arrayList.add(Long.valueOf(aop.f2810b));
        }
        if (hashSet.contains("extras")) {
            arrayList.add(a(aop.c));
        }
        if (hashSet.contains("gender")) {
            arrayList.add(Integer.valueOf(aop.d));
        }
        if (hashSet.contains("keywords")) {
            if (aop.e != null) {
                arrayList.add(aop.e.toString());
            } else {
                arrayList.add(null);
            }
        }
        if (hashSet.contains("isTestDevice")) {
            arrayList.add(Boolean.valueOf(aop.f));
        }
        if (hashSet.contains("tagForChildDirectedTreatment")) {
            arrayList.add(Integer.valueOf(aop.g));
        }
        if (hashSet.contains("manualImpressionsEnabled")) {
            arrayList.add(Boolean.valueOf(aop.h));
        }
        if (hashSet.contains("publisherProvidedId")) {
            arrayList.add(aop.i);
        }
        if (hashSet.contains("location")) {
            if (aop.k != null) {
                arrayList.add(aop.k.toString());
            } else {
                arrayList.add(null);
            }
        }
        if (hashSet.contains("contentUrl")) {
            arrayList.add(aop.l);
        }
        if (hashSet.contains("networkExtras")) {
            arrayList.add(a(aop.m));
        }
        if (hashSet.contains("customTargeting")) {
            arrayList.add(a(aop.n));
        }
        if (hashSet.contains("categoryExclusions")) {
            if (aop.o != null) {
                arrayList.add(aop.o.toString());
            } else {
                arrayList.add(null);
            }
        }
        if (hashSet.contains("requestAgent")) {
            arrayList.add(aop.p);
        }
        if (hashSet.contains("requestPackage")) {
            arrayList.add(aop.q);
        }
        if (hashSet.contains("isDesignedForFamilies")) {
            arrayList.add(Boolean.valueOf(aop.r));
        }
        return arrayList.toArray();
    }
}
