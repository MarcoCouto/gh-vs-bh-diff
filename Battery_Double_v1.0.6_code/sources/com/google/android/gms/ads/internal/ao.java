package com.google.android.gms.ads.internal;

import com.google.android.gms.internal.ads.aop;
import com.google.android.gms.internal.ads.jm;
import java.lang.ref.WeakReference;

final class ao implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ WeakReference f1976a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ an f1977b;

    ao(an anVar, WeakReference weakReference) {
        this.f1977b = anVar;
        this.f1976a = weakReference;
    }

    public final void run() {
        this.f1977b.d = false;
        a aVar = (a) this.f1976a.get();
        if (aVar != null) {
            aop a2 = this.f1977b.c;
            if (aVar.c(a2)) {
                aVar.b(a2);
                return;
            }
            jm.d("Ad is not visible. Not refreshing ad.");
            aVar.d.b(a2);
        }
    }
}
