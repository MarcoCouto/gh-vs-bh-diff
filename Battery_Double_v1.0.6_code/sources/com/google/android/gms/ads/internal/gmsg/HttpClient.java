package com.google.android.gms.ads.internal.gmsg;

import android.content.Context;
import android.support.annotation.Keep;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.ax;
import com.google.android.gms.common.annotation.KeepName;
import com.google.android.gms.internal.ads.azf;
import com.google.android.gms.internal.ads.cm;
import com.google.android.gms.internal.ads.jm;
import com.google.android.gms.internal.ads.jt;
import com.google.android.gms.internal.ads.jv;
import com.google.android.gms.internal.ads.ml;
import com.google.android.gms.internal.ads.mu;
import java.io.BufferedOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@Keep
@KeepName
@cm
public class HttpClient implements ae<azf> {
    private final Context mContext;
    private final mu zzyf;

    @cm
    static class a {

        /* renamed from: a reason: collision with root package name */
        private final String f2030a;

        /* renamed from: b reason: collision with root package name */
        private final String f2031b;

        public a(String str, String str2) {
            this.f2030a = str;
            this.f2031b = str2;
        }

        public final String a() {
            return this.f2030a;
        }

        public final String b() {
            return this.f2031b;
        }
    }

    @cm
    static class b {

        /* renamed from: a reason: collision with root package name */
        private final String f2032a;

        /* renamed from: b reason: collision with root package name */
        private final URL f2033b;
        private final ArrayList<a> c;
        private final String d;

        b(String str, URL url, ArrayList<a> arrayList, String str2) {
            this.f2032a = str;
            this.f2033b = url;
            this.c = arrayList;
            this.d = str2;
        }

        public final String a() {
            return this.f2032a;
        }

        public final URL b() {
            return this.f2033b;
        }

        public final ArrayList<a> c() {
            return this.c;
        }

        public final String d() {
            return this.d;
        }
    }

    @cm
    class c {

        /* renamed from: a reason: collision with root package name */
        private final d f2034a;

        /* renamed from: b reason: collision with root package name */
        private final boolean f2035b;
        private final String c;

        public c(HttpClient httpClient, boolean z, d dVar, String str) {
            this.f2035b = z;
            this.f2034a = dVar;
            this.c = str;
        }

        public final String a() {
            return this.c;
        }

        public final d b() {
            return this.f2034a;
        }

        public final boolean c() {
            return this.f2035b;
        }
    }

    @cm
    static class d {

        /* renamed from: a reason: collision with root package name */
        private final String f2036a;

        /* renamed from: b reason: collision with root package name */
        private final int f2037b;
        private final List<a> c;
        private final String d;

        d(String str, int i, List<a> list, String str2) {
            this.f2036a = str;
            this.f2037b = i;
            this.c = list;
            this.d = str2;
        }

        public final String a() {
            return this.f2036a;
        }

        public final int b() {
            return this.f2037b;
        }

        public final Iterable<a> c() {
            return this.c;
        }

        public final String d() {
            return this.d;
        }
    }

    public HttpClient(Context context, mu muVar) {
        this.mContext = context;
        this.zzyf = muVar;
    }

    /* JADX WARNING: type inference failed for: r2v1 */
    /* JADX WARNING: type inference failed for: r3v1, types: [java.net.HttpURLConnection] */
    /* JADX WARNING: type inference failed for: r2v2, types: [java.net.HttpURLConnection] */
    /* JADX WARNING: type inference failed for: r3v2 */
    /* JADX WARNING: type inference failed for: r3v4 */
    /* JADX WARNING: type inference failed for: r2v3 */
    /* JADX WARNING: type inference failed for: r3v11 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x004a  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00cc  */
    /* JADX WARNING: Removed duplicated region for block: B:43:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Unknown variable types count: 2 */
    private final c zza(b bVar) {
        Exception e;
        ? r2;
        ? r3;
        byte[] bArr = 0;
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection) bVar.b().openConnection();
            try {
                ax.e().a(this.mContext, this.zzyf.f3528a, false, httpURLConnection);
                ArrayList c2 = bVar.c();
                int size = c2.size();
                int i = 0;
                while (i < size) {
                    Object obj = c2.get(i);
                    i++;
                    a aVar = (a) obj;
                    httpURLConnection.addRequestProperty(aVar.a(), aVar.b());
                }
                if (!TextUtils.isEmpty(bVar.d())) {
                    httpURLConnection.setDoOutput(true);
                    byte[] bytes = bVar.d().getBytes();
                    httpURLConnection.setFixedLengthStreamingMode(bytes.length);
                    BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(httpURLConnection.getOutputStream());
                    bufferedOutputStream.write(bytes);
                    bufferedOutputStream.close();
                    bArr = bytes;
                }
                ml mlVar = new ml();
                mlVar.a(httpURLConnection, bArr);
                ArrayList arrayList = new ArrayList();
                if (httpURLConnection.getHeaderFields() != null) {
                    for (Entry entry : httpURLConnection.getHeaderFields().entrySet()) {
                        for (String aVar2 : (List) entry.getValue()) {
                            arrayList.add(new a((String) entry.getKey(), aVar2));
                        }
                    }
                }
                String a2 = bVar.a();
                int responseCode = httpURLConnection.getResponseCode();
                ax.e();
                d dVar = new d(a2, responseCode, arrayList, jv.a(new InputStreamReader(httpURLConnection.getInputStream())));
                mlVar.a(httpURLConnection, dVar.b());
                mlVar.a(dVar.d());
                c cVar = new c(this, true, dVar, null);
                if (httpURLConnection != 0) {
                    httpURLConnection.disconnect();
                }
                return cVar;
            } catch (Exception e2) {
                e = e2;
                r2 = httpURLConnection;
                try {
                    c cVar2 = new c(this, false, null, e.toString());
                    if (r2 != 0) {
                    }
                } catch (Throwable th) {
                    th = th;
                    r3 = r2;
                    if (r3 != 0) {
                    }
                    throw th;
                }
            } catch (Throwable th2) {
                r3 = httpURLConnection;
                th = th2;
                if (r3 != 0) {
                }
                throw th;
            }
        } catch (Exception e3) {
            e = e3;
            r2 = 0;
            c cVar22 = new c(this, false, null, e.toString());
            if (r2 != 0) {
                return cVar22;
            }
            r2.disconnect();
            return cVar22;
        } catch (Throwable th3) {
            th = th3;
            r3 = bArr;
            if (r3 != 0) {
                r3.disconnect();
            }
            throw th;
        }
    }

    private static JSONObject zza(d dVar) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("http_request_id", dVar.a());
            if (dVar.d() != null) {
                jSONObject.put("body", dVar.d());
            }
            JSONArray jSONArray = new JSONArray();
            for (a aVar : dVar.c()) {
                jSONArray.put(new JSONObject().put("key", aVar.a()).put("value", aVar.b()));
            }
            jSONObject.put("headers", jSONArray);
            jSONObject.put("response_code", dVar.b());
        } catch (JSONException e) {
            jm.b("Error constructing JSON for http response.", e);
        }
        return jSONObject;
    }

    private static b zzc(JSONObject jSONObject) {
        URL url;
        String optString = jSONObject.optString("http_request_id");
        String optString2 = jSONObject.optString("url");
        String optString3 = jSONObject.optString("post_body", null);
        try {
            url = new URL(optString2);
        } catch (MalformedURLException e) {
            jm.b("Error constructing http request.", e);
            url = null;
        }
        ArrayList arrayList = new ArrayList();
        JSONArray optJSONArray = jSONObject.optJSONArray("headers");
        if (optJSONArray == null) {
            optJSONArray = new JSONArray();
        }
        for (int i = 0; i < optJSONArray.length(); i++) {
            JSONObject optJSONObject = optJSONArray.optJSONObject(i);
            if (optJSONObject != null) {
                arrayList.add(new a(optJSONObject.optString("key"), optJSONObject.optString("value")));
            }
        }
        return new b(optString, url, arrayList, optString3);
    }

    @Keep
    @KeepName
    public JSONObject send(JSONObject jSONObject) {
        JSONObject jSONObject2 = new JSONObject();
        String str = "";
        try {
            String optString = jSONObject.optString("http_request_id");
            c zza = zza(zzc(jSONObject));
            if (zza.c()) {
                jSONObject2.put("response", zza(zza.b()));
                jSONObject2.put("success", true);
            } else {
                jSONObject2.put("response", new JSONObject().put("http_request_id", optString));
                jSONObject2.put("success", false);
                jSONObject2.put("reason", zza.a());
            }
        } catch (Exception e) {
            jm.b("Error executing http request.", e);
            try {
                jSONObject2.put("response", new JSONObject().put("http_request_id", str));
                jSONObject2.put("success", false);
                jSONObject2.put("reason", e.toString());
            } catch (JSONException e2) {
                jm.b("Error executing http request.", e2);
            }
        }
        return jSONObject2;
    }

    public final /* synthetic */ void zza(Object obj, Map map) {
        jt.a((Runnable) new af(this, map, (azf) obj));
    }
}
