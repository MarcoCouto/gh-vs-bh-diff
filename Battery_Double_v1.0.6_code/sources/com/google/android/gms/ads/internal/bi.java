package com.google.android.gms.ads.internal;

import com.google.android.gms.internal.ads.ir;
import com.google.android.gms.internal.ads.is;

final class bi implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ is f2001a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ bg f2002b;

    bi(bg bgVar, is isVar) {
        this.f2002b = bgVar;
        this.f2001a = isVar;
    }

    public final void run() {
        this.f2002b.b(new ir(this.f2001a, null, null, null, null, null, null, null));
    }
}
