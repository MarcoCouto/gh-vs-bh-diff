package com.google.android.gms.ads.internal;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import com.google.android.gms.internal.ads.cm;
import com.google.android.gms.internal.ads.dr;
import com.google.android.gms.internal.ads.ic;
import com.google.android.gms.internal.ads.jv;

@cm
public final class bv {

    /* renamed from: a reason: collision with root package name */
    private final Context f2021a;

    /* renamed from: b reason: collision with root package name */
    private boolean f2022b;
    private ic c;
    private dr d;

    public bv(Context context, ic icVar, dr drVar) {
        this.f2021a = context;
        this.c = icVar;
        this.d = drVar;
        if (this.d == null) {
            this.d = new dr();
        }
    }

    private final boolean c() {
        return (this.c != null && this.c.a().f) || this.d.f3267a;
    }

    public final void a() {
        this.f2022b = true;
    }

    public final void a(String str) {
        if (c()) {
            if (str == null) {
                str = "";
            }
            if (this.c != null) {
                this.c.a(str, null, 3);
            } else if (this.d.f3267a && this.d.f3268b != null) {
                for (String str2 : this.d.f3268b) {
                    if (!TextUtils.isEmpty(str2)) {
                        String replace = str2.replace("{NAVIGATION_URL}", Uri.encode(str));
                        ax.e();
                        jv.a(this.f2021a, "", replace);
                    }
                }
            }
        }
    }

    public final boolean b() {
        return !c() || this.f2022b;
    }
}
