package com.google.android.gms.ads.internal.gmsg;

import android.text.TextUtils;
import com.google.android.gms.internal.ads.ape;
import com.google.android.gms.internal.ads.asi;
import com.google.android.gms.internal.ads.cm;
import com.google.android.gms.internal.ads.hp;
import com.google.android.gms.internal.ads.jm;
import java.util.Map;

@cm
public final class i implements ae<Object> {

    /* renamed from: a reason: collision with root package name */
    private final j f2054a;

    public i(j jVar) {
        this.f2054a = jVar;
    }

    public final void zza(Object obj, Map<String, String> map) {
        hp hpVar;
        String str = (String) map.get("action");
        if ("grant".equals(str)) {
            try {
                int parseInt = Integer.parseInt((String) map.get("amount"));
                String str2 = (String) map.get("type");
                if (!TextUtils.isEmpty(str2)) {
                    hpVar = new hp(str2, parseInt);
                    this.f2054a.a(hpVar);
                }
            } catch (NumberFormatException e) {
                jm.c("Unable to parse reward amount.", e);
            }
            hpVar = null;
            this.f2054a.a(hpVar);
        } else if ("video_start".equals(str)) {
            this.f2054a.d_();
        } else if ("video_complete".equals(str)) {
            if (((Boolean) ape.f().a(asi.ax)).booleanValue()) {
                if (((Boolean) ape.f().a(asi.ax)).booleanValue()) {
                    this.f2054a.e_();
                }
            }
        }
    }
}
