package com.google.android.gms.ads.internal.gmsg;

import com.google.android.gms.internal.ads.azf;
import com.google.android.gms.internal.ads.jm;
import com.google.android.gms.internal.ads.jv;
import java.util.Map;
import org.json.JSONObject;

final class af implements Runnable {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ azf f2039a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ Map f2040b;
    private final /* synthetic */ HttpClient c;

    af(HttpClient httpClient, Map map, azf azf) {
        this.c = httpClient;
        this.f2040b = map;
        this.f2039a = azf;
    }

    public final void run() {
        jm.b("Received Http request.");
        try {
            JSONObject send = this.c.send(new JSONObject((String) this.f2040b.get("http_request")));
            if (send == null) {
                jm.c("Response should not be null.");
            } else {
                jv.f3440a.post(new ag(this, send));
            }
        } catch (Exception e) {
            jm.b("Error converting request to json.", e);
        }
    }
}
