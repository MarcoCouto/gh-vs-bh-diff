package com.google.android.gms.ads.internal;

import com.google.android.gms.ads.internal.overlay.AdOverlayInfoParcel;
import com.google.android.gms.internal.ads.cm;
import com.google.android.gms.internal.ads.jh;
import com.google.android.gms.internal.ads.jv;

@cm
final class p extends jh {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ m f2097a;

    /* renamed from: b reason: collision with root package name */
    private final int f2098b;

    public p(m mVar, int i) {
        this.f2097a = mVar;
        this.f2098b = i;
    }

    public final void a() {
        r rVar = new r(this.f2097a.e.J, this.f2097a.J(), this.f2097a.m, this.f2097a.n, this.f2097a.e.J ? this.f2098b : -1, this.f2097a.o, this.f2097a.e.j.L, this.f2097a.e.j.O);
        int requestedOrientation = this.f2097a.e.j.f3396b.getRequestedOrientation();
        if (requestedOrientation == -1) {
            requestedOrientation = this.f2097a.e.j.h;
        }
        jv.f3440a.post(new q(this, new AdOverlayInfoParcel(this.f2097a, this.f2097a, this.f2097a, this.f2097a.e.j.f3396b, requestedOrientation, this.f2097a.e.e, this.f2097a.e.j.A, rVar)));
    }

    public final void c_() {
    }
}
