package com.google.android.gms.ads.internal;

import com.google.android.gms.internal.ads.mx;
import com.google.android.gms.internal.ads.nc;
import com.google.android.gms.internal.ads.nn;
import org.json.JSONObject;

final /* synthetic */ class f implements mx {

    /* renamed from: a reason: collision with root package name */
    static final mx f2029a = new f();

    private f() {
    }

    public final nn a(Object obj) {
        JSONObject jSONObject = (JSONObject) obj;
        if (jSONObject.optBoolean("isSuccessful", false)) {
            ax.i().l().f(jSONObject.getString("appSettingsJson"));
        }
        return nc.a(null);
    }
}
