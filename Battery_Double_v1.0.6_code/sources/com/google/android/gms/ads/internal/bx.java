package com.google.android.gms.ads.internal;

import com.google.android.gms.internal.ads.ir;
import com.google.android.gms.internal.ads.jv;
import com.google.android.gms.internal.ads.rz;

final /* synthetic */ class bx implements rz {

    /* renamed from: a reason: collision with root package name */
    private final ir f2023a;

    /* renamed from: b reason: collision with root package name */
    private final Runnable f2024b;

    bx(ir irVar, Runnable runnable) {
        this.f2023a = irVar;
        this.f2024b = runnable;
    }

    public final void a() {
        ir irVar = this.f2023a;
        Runnable runnable = this.f2024b;
        if (!irVar.m) {
            ax.e();
            jv.a(runnable);
        }
    }
}
