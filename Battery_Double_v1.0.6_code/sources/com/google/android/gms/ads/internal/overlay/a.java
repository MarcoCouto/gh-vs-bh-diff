package com.google.android.gms.ads.internal.overlay;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.ax;
import com.google.android.gms.internal.ads.ape;
import com.google.android.gms.internal.ads.asi;
import com.google.android.gms.internal.ads.cm;
import com.google.android.gms.internal.ads.jm;
import com.google.android.gms.internal.ads.jv;

@cm
public final class a {
    private static boolean a(Context context, Intent intent, t tVar) {
        String str = "Launching an intent: ";
        try {
            String valueOf = String.valueOf(intent.toURI());
            jm.a(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
            ax.e();
            jv.a(context, intent);
            if (tVar != null) {
                tVar.h();
            }
            return true;
        } catch (ActivityNotFoundException e) {
            jm.e(e.getMessage());
            return false;
        }
    }

    public static boolean a(Context context, c cVar, t tVar) {
        int i;
        if (cVar == null) {
            jm.e("No intent data for launcher overlay.");
            return false;
        }
        asi.a(context);
        if (cVar.f != null) {
            return a(context, cVar.f, tVar);
        }
        Intent intent = new Intent();
        if (TextUtils.isEmpty(cVar.f2078a)) {
            jm.e("Open GMSG did not contain a URL.");
            return false;
        }
        if (!TextUtils.isEmpty(cVar.f2079b)) {
            intent.setDataAndType(Uri.parse(cVar.f2078a), cVar.f2079b);
        } else {
            intent.setData(Uri.parse(cVar.f2078a));
        }
        intent.setAction("android.intent.action.VIEW");
        if (!TextUtils.isEmpty(cVar.c)) {
            intent.setPackage(cVar.c);
        }
        if (!TextUtils.isEmpty(cVar.d)) {
            String[] split = cVar.d.split("/", 2);
            if (split.length < 2) {
                String str = "Could not parse component name from open GMSG: ";
                String valueOf = String.valueOf(cVar.d);
                jm.e(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
                return false;
            }
            intent.setClassName(split[0], split[1]);
        }
        String str2 = cVar.e;
        if (!TextUtils.isEmpty(str2)) {
            try {
                i = Integer.parseInt(str2);
            } catch (NumberFormatException e) {
                jm.e("Could not parse intent flags.");
                i = 0;
            }
            intent.addFlags(i);
        }
        if (((Boolean) ape.f().a(asi.cN)).booleanValue()) {
            intent.addFlags(268435456);
            intent.putExtra("android.support.customtabs.extra.user_opt_out", true);
        } else {
            if (((Boolean) ape.f().a(asi.cM)).booleanValue()) {
                ax.e();
                jv.b(context, intent);
            }
        }
        return a(context, intent, tVar);
    }
}
