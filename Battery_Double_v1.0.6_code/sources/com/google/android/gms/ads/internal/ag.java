package com.google.android.gms.ads.internal;

import android.os.Bundle;
import com.google.android.gms.internal.ads.aop;
import com.google.android.gms.internal.ads.asv;
import com.google.android.gms.internal.ads.aub;
import com.google.android.gms.internal.ads.dl;
import com.google.android.gms.internal.ads.dm;
import com.google.android.gms.internal.ads.is;
import com.google.android.gms.internal.ads.nc;
import java.util.concurrent.Callable;
import org.json.JSONArray;
import org.json.JSONObject;

final class ag implements Callable<aub> {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ int f1960a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ JSONArray f1961b;
    private final /* synthetic */ int c;
    private final /* synthetic */ is d;
    private final /* synthetic */ ae e;

    ag(ae aeVar, int i, JSONArray jSONArray, int i2, is isVar) {
        this.e = aeVar;
        this.f1960a = i;
        this.f1961b = jSONArray;
        this.c = i2;
        this.d = isVar;
    }

    public final /* synthetic */ Object call() throws Exception {
        if (this.f1960a >= this.f1961b.length()) {
            return null;
        }
        JSONArray jSONArray = new JSONArray();
        jSONArray.put(this.f1961b.get(this.f1960a));
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("ads", jSONArray);
        ae aeVar = new ae(this.e.e.c, this.e.i, this.e.e.i, this.e.e.f1990b, this.e.j, this.e.e.e, true);
        ae.a(this.e.e, aeVar.e);
        aeVar.k_();
        aeVar.a(this.e.f1951b);
        asv asv = aeVar.f1950a;
        int i = this.f1960a;
        asv.a("num_ads_requested", String.valueOf(this.c));
        asv.a("ad_index", String.valueOf(i));
        dl dlVar = this.d.f3397a;
        String jSONObject2 = jSONObject.toString();
        Bundle bundle = dlVar.c.c != null ? new Bundle(dlVar.c.c) : new Bundle();
        bundle.putString("_ad", jSONObject2);
        aeVar.a(new dm(dlVar.f3261b, new aop(dlVar.c.f2809a, dlVar.c.f2810b, bundle, dlVar.c.d, dlVar.c.e, dlVar.c.f, dlVar.c.g, dlVar.c.h, dlVar.c.i, dlVar.c.j, dlVar.c.k, dlVar.c.l, dlVar.c.m, dlVar.c.n, dlVar.c.o, dlVar.c.p, dlVar.c.q, dlVar.c.r), dlVar.d, dlVar.e, dlVar.f, dlVar.g, dlVar.i, dlVar.j, dlVar.k, dlVar.l, dlVar.n, dlVar.z, dlVar.o, dlVar.p, dlVar.q, dlVar.r, dlVar.s, dlVar.t, dlVar.u, dlVar.v, dlVar.w, dlVar.x, dlVar.y, dlVar.B, dlVar.C, dlVar.I, dlVar.D, dlVar.E, dlVar.F, dlVar.G, nc.a(dlVar.H), dlVar.J, dlVar.K, dlVar.L, dlVar.M, dlVar.N, dlVar.O, dlVar.P, dlVar.Q, dlVar.U, nc.a(dlVar.h), dlVar.V, dlVar.W, dlVar.X, 1, dlVar.Z, dlVar.aa, dlVar.ab, dlVar.ac), aeVar.f1950a);
        return (aub) aeVar.K().get();
    }
}
