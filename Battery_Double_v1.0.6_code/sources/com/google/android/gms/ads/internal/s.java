package com.google.android.gms.ads.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.a.b;

public final class s implements Creator<r> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        boolean z = false;
        int b2 = b.b(parcel);
        String str = null;
        float f = 0.0f;
        boolean z2 = false;
        boolean z3 = false;
        int i = 0;
        boolean z4 = false;
        boolean z5 = false;
        boolean z6 = false;
        while (parcel.dataPosition() < b2) {
            int a2 = b.a(parcel);
            switch (b.a(a2)) {
                case 2:
                    z6 = b.c(parcel, a2);
                    break;
                case 3:
                    z5 = b.c(parcel, a2);
                    break;
                case 4:
                    str = b.k(parcel, a2);
                    break;
                case 5:
                    z4 = b.c(parcel, a2);
                    break;
                case 6:
                    f = b.h(parcel, a2);
                    break;
                case 7:
                    i = b.d(parcel, a2);
                    break;
                case 8:
                    z3 = b.c(parcel, a2);
                    break;
                case 9:
                    z2 = b.c(parcel, a2);
                    break;
                case 10:
                    z = b.c(parcel, a2);
                    break;
                default:
                    b.b(parcel, a2);
                    break;
            }
        }
        b.r(parcel, b2);
        return new r(z6, z5, str, z4, f, i, z3, z2, z);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new r[i];
    }
}
