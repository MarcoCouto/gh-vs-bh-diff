package com.google.android.gms.ads.internal.overlay;

import android.content.Context;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewParent;
import com.google.android.gms.internal.ads.cm;
import com.google.android.gms.internal.ads.qn;

@cm
public final class i {

    /* renamed from: a reason: collision with root package name */
    public final int f2086a;

    /* renamed from: b reason: collision with root package name */
    public final LayoutParams f2087b;
    public final ViewGroup c;
    public final Context d;

    public i(qn qnVar) throws g {
        this.f2087b = qnVar.getLayoutParams();
        ViewParent parent = qnVar.getParent();
        this.d = qnVar.q();
        if (parent == null || !(parent instanceof ViewGroup)) {
            throw new g("Could not get the parent of the WebView for an overlay.");
        }
        this.c = (ViewGroup) parent;
        this.f2086a = this.c.indexOfChild(qnVar.getView());
        this.c.removeView(qnVar.getView());
        qnVar.b(true);
    }
}
