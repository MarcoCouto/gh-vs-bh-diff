package com.google.android.gms.ads.internal;

import android.content.Context;
import android.text.TextUtils;
import com.google.android.gms.internal.ads.ape;
import com.google.android.gms.internal.ads.asi;
import com.google.android.gms.internal.ads.bbi;
import com.google.android.gms.internal.ads.bbn;
import com.google.android.gms.internal.ads.cm;
import com.google.android.gms.internal.ads.iv;
import com.google.android.gms.internal.ads.jm;
import com.google.android.gms.internal.ads.mu;
import com.google.android.gms.internal.ads.na;
import com.google.android.gms.internal.ads.nc;
import com.google.android.gms.internal.ads.nn;
import com.google.android.gms.internal.ads.nt;
import org.json.JSONObject;

@cm
public final class e {

    /* renamed from: a reason: collision with root package name */
    private final Object f2027a = new Object();

    /* renamed from: b reason: collision with root package name */
    private Context f2028b;
    private long c = 0;

    public final void a(Context context, mu muVar, String str, Runnable runnable) {
        a(context, muVar, true, null, str, null, runnable);
    }

    /* access modifiers changed from: 0000 */
    public final void a(Context context, mu muVar, boolean z, iv ivVar, String str, String str2, Runnable runnable) {
        boolean z2;
        if (ax.l().b() - this.c < 5000) {
            jm.e("Not retrying to fetch app settings");
            return;
        }
        this.c = ax.l().b();
        if (ivVar == null) {
            z2 = true;
        } else {
            z2 = (((ax.l().a() - ivVar.a()) > ((Long) ape.f().a(asi.ct)).longValue() ? 1 : ((ax.l().a() - ivVar.a()) == ((Long) ape.f().a(asi.ct)).longValue() ? 0 : -1)) > 0) || !ivVar.b();
        }
        if (!z2) {
            return;
        }
        if (context == null) {
            jm.e("Context not provided to fetch application settings");
        } else if (!TextUtils.isEmpty(str) || !TextUtils.isEmpty(str2)) {
            Context applicationContext = context.getApplicationContext();
            if (applicationContext == null) {
                applicationContext = context;
            }
            this.f2028b = applicationContext;
            bbi a2 = ax.s().a(this.f2028b, muVar).a("google.afma.config.fetchAppSettings", bbn.f3104a, bbn.f3104a);
            try {
                JSONObject jSONObject = new JSONObject();
                if (!TextUtils.isEmpty(str)) {
                    jSONObject.put("app_id", str);
                } else if (!TextUtils.isEmpty(str2)) {
                    jSONObject.put("ad_unit_id", str2);
                }
                jSONObject.put("is_init", z);
                jSONObject.put("pn", context.getPackageName());
                nn b2 = a2.b(jSONObject);
                nn a3 = nc.a(b2, f.f2029a, nt.f3559b);
                if (runnable != null) {
                    b2.a(runnable, nt.f3559b);
                }
                na.a(a3, "ConfigLoader.maybeFetchNewAppSettings");
            } catch (Exception e) {
                jm.b("Error requesting application settings", e);
            }
        } else {
            jm.e("App settings could not be fetched. Required parameters missing");
        }
    }
}
