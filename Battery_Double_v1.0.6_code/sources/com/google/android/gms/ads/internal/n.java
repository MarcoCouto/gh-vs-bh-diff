package com.google.android.gms.ads.internal;

import com.google.android.gms.internal.ads.akr;
import com.google.android.gms.internal.ads.akv;
import com.google.android.gms.internal.ads.ir;
import com.google.android.gms.internal.ads.ry;

final /* synthetic */ class n implements ry {

    /* renamed from: a reason: collision with root package name */
    private final m f2073a;

    /* renamed from: b reason: collision with root package name */
    private final ir f2074b;

    n(m mVar, ir irVar) {
        this.f2073a = mVar;
        this.f2074b = irVar;
    }

    public final void a() {
        m mVar = this.f2073a;
        ir irVar = this.f2074b;
        new akr(mVar.e.c, irVar.f3396b.getView()).a((akv) irVar.f3396b);
    }
}
