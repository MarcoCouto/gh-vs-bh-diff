package com.google.android.gms.ads.internal.gmsg;

import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import com.google.android.gms.ads.internal.ax;
import com.google.android.gms.internal.ads.jm;
import com.google.android.gms.internal.ads.jv;
import com.google.android.gms.internal.ads.qn;
import java.util.HashMap;
import java.util.Map;

final class v implements ae<qn> {
    v() {
    }

    public final /* synthetic */ void zza(Object obj, Map map) {
        qn qnVar = (qn) obj;
        WindowManager windowManager = (WindowManager) qnVar.getContext().getSystemService("window");
        ax.e();
        View view = (View) qnVar;
        DisplayMetrics a2 = jv.a(windowManager);
        int i = a2.widthPixels;
        int i2 = a2.heightPixels;
        int[] iArr = new int[2];
        HashMap hashMap = new HashMap();
        view.getLocationInWindow(iArr);
        hashMap.put("xInPixels", Integer.valueOf(iArr[0]));
        hashMap.put("yInPixels", Integer.valueOf(iArr[1]));
        hashMap.put("windowWidthInPixels", Integer.valueOf(i));
        hashMap.put("windowHeightInPixels", Integer.valueOf(i2));
        qnVar.a("locationReady", (Map<String, ?>) hashMap);
        jm.e("GET LOCATION COMPILED");
    }
}
