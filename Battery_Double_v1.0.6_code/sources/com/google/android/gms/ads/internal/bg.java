package com.google.android.gms.ads.internal;

import android.content.Context;
import android.view.View;
import com.google.android.gms.ads.internal.gmsg.ae;
import com.google.android.gms.common.internal.aa;
import com.google.android.gms.internal.ads.ao;
import com.google.android.gms.internal.ads.aot;
import com.google.android.gms.internal.ads.ape;
import com.google.android.gms.internal.ads.asi;
import com.google.android.gms.internal.ads.asv;
import com.google.android.gms.internal.ads.atc;
import com.google.android.gms.internal.ads.bcr;
import com.google.android.gms.internal.ads.cm;
import com.google.android.gms.internal.ads.ic;
import com.google.android.gms.internal.ads.ir;
import com.google.android.gms.internal.ads.is;
import com.google.android.gms.internal.ads.jm;
import com.google.android.gms.internal.ads.jv;
import com.google.android.gms.internal.ads.mu;
import com.google.android.gms.internal.ads.o;
import com.google.android.gms.internal.ads.qn;
import com.google.android.gms.internal.ads.qu;
import com.google.android.gms.internal.ads.qy;
import com.google.android.gms.internal.ads.sb;

@cm
public abstract class bg extends bb implements g, o {
    private boolean k;

    public bg(Context context, aot aot, String str, bcr bcr, mu muVar, bu buVar) {
        super(context, aot, str, bcr, muVar, buVar);
    }

    /* access modifiers changed from: protected */
    public final boolean M() {
        return (this.e.k == null || this.e.k.f3398b == null || !this.e.k.f3398b.Q) ? false : true;
    }

    public final void N() {
        u();
    }

    /* access modifiers changed from: protected */
    public qn a(is isVar, bv bvVar, ic icVar) throws qy {
        View nextView = this.e.f.getNextView();
        if (nextView instanceof qn) {
            ((qn) nextView).destroy();
        }
        if (nextView != null) {
            this.e.f.removeView(nextView);
        }
        ax.f();
        qn a2 = qu.a(this.e.c, sb.a(this.e.i), this.e.i.f2814a, false, false, this.e.d, this.e.e, this.f1950a, this, this.i, isVar.i);
        if (this.e.i.g == null) {
            a(a2.getView());
        }
        a2.v().a(this, this, this, this, this, false, null, bvVar, this, icVar);
        a(a2);
        a2.a(isVar.f3397a.v);
        return a2;
    }

    public final void a(int i, int i2, int i3, int i4) {
        w();
    }

    public final void a(atc atc) {
        aa.b("setOnCustomRenderedAdLoadedListener must be called on the main UI thread.");
        this.e.B = atc;
    }

    /* access modifiers changed from: protected */
    public void a(is isVar, asv asv) {
        if (isVar.e != -2) {
            jv.f3440a.post(new bi(this, isVar));
            return;
        }
        if (isVar.d != null) {
            this.e.i = isVar.d;
        }
        if (!isVar.f3398b.g || isVar.f3398b.z) {
            jv.f3440a.post(new bj(this, isVar, this.i.c.a(this.e.c, this.e.e, isVar.f3398b), asv));
            return;
        }
        this.e.I = 0;
        ay ayVar = this.e;
        ax.d();
        ayVar.h = ao.a(this.e.c, this, isVar, this.e.d, null, this.j, this, asv);
    }

    /* access modifiers changed from: protected */
    public final void a(qn qnVar) {
        qnVar.a("/trackActiveViewUnit", (ae<? super qn>) new bh<Object>(this));
    }

    /* access modifiers changed from: protected */
    public boolean a(ir irVar, ir irVar2) {
        if (this.e.d() && this.e.f != null) {
            this.e.f.a().c(irVar2.A);
        }
        try {
            if (irVar2.f3396b != null && !irVar2.n && irVar2.M) {
                if (((Boolean) ape.f().a(asi.dl)).booleanValue() && !irVar2.f3395a.c.containsKey("sdk_less_server_data")) {
                    try {
                        irVar2.f3396b.J();
                    } catch (Throwable th) {
                        jm.a("Could not render test Ad label.");
                    }
                }
            }
        } catch (RuntimeException e) {
            jm.a("Could not render test AdLabel.");
        }
        return super.a(irVar, irVar2);
    }

    public final void a_(View view) {
        this.e.H = view;
        b(new ir(this.e.k, null, null, null, null, null, null, null));
    }

    /* access modifiers changed from: 0000 */
    public final void b(qn qnVar) {
        if (this.e.j != null) {
            this.g.a(this.e.i, this.e.j, qnVar.getView(), qnVar);
            this.k = false;
            return;
        }
        this.k = true;
        jm.e("Request to enable ActiveView before adState is available.");
    }

    public final void f_() {
        e();
    }

    public final void g_() {
        ac();
        n();
    }

    /* access modifiers changed from: protected */
    public void x() {
        super.x();
        if (this.k) {
            if (((Boolean) ape.f().a(asi.cg)).booleanValue()) {
                b(this.e.j.f3396b);
            }
        }
    }
}
