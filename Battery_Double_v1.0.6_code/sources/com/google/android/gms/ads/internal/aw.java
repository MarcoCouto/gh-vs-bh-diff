package com.google.android.gms.ads.internal;

import android.os.Bundle;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.internal.ads.aop;
import com.google.android.gms.internal.ads.ape;
import com.google.android.gms.internal.ads.asi;
import com.google.android.gms.internal.ads.mu;
import java.util.Map;
import java.util.TreeMap;

final class aw {

    /* renamed from: a reason: collision with root package name */
    private final String f1985a;

    /* renamed from: b reason: collision with root package name */
    private final Map<String, String> f1986b = new TreeMap();
    private String c;
    private String d;

    public aw(String str) {
        this.f1985a = str;
    }

    public final String a() {
        return this.d;
    }

    public final void a(aop aop, mu muVar) {
        this.c = aop.j.f2858a;
        Bundle bundle = aop.m != null ? aop.m.getBundle(AdMobAdapter.class.getName()) : null;
        if (bundle != null) {
            String str = (String) ape.f().a(asi.cy);
            for (String str2 : bundle.keySet()) {
                if (str.equals(str2)) {
                    this.d = bundle.getString(str2);
                } else if (str2.startsWith("csa_")) {
                    this.f1986b.put(str2.substring(4), bundle.getString(str2));
                }
            }
            this.f1986b.put("SDKVersion", muVar.f3528a);
        }
    }

    public final String b() {
        return this.c;
    }

    public final String c() {
        return this.f1985a;
    }

    public final Map<String, String> d() {
        return this.f1986b;
    }
}
