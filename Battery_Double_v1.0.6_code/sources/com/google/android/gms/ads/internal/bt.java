package com.google.android.gms.ads.internal;

import android.os.RemoteException;
import com.google.android.gms.internal.ads.avt;
import com.google.android.gms.internal.ads.awh;
import com.google.android.gms.internal.ads.jm;

final class bt implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ avt f2017a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ bo f2018b;

    bt(bo boVar, avt avt) {
        this.f2018b = boVar;
        this.f2017a = avt;
    }

    public final void run() {
        try {
            ((awh) this.f2018b.e.v.get(this.f2017a.l())).a(this.f2017a);
        } catch (RemoteException e) {
            jm.d("#007 Could not call remote method.", e);
        }
    }
}
