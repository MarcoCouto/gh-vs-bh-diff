package com.google.android.gms.ads.internal;

import android.webkit.CookieManager;
import com.google.android.gms.internal.ads.ape;
import com.google.android.gms.internal.ads.asi;
import java.util.concurrent.Callable;

final class be implements Callable<String> {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ bb f1998a;

    be(bb bbVar) {
        this.f1998a = bbVar;
    }

    public final /* synthetic */ Object call() throws Exception {
        String str = "";
        if (((Boolean) ape.f().a(asi.cC)).booleanValue()) {
            CookieManager c = ax.g().c(this.f1998a.e.c);
            if (c != null) {
                return c.getCookie("googleads.g.doubleclick.net");
            }
        }
        return str;
    }
}
