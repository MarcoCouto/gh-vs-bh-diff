package com.google.android.gms.ads.internal;

import android.os.RemoteException;
import com.google.android.gms.internal.ads.ao;
import com.google.android.gms.internal.ads.asv;
import com.google.android.gms.internal.ads.asw;
import com.google.android.gms.internal.ads.ic;
import com.google.android.gms.internal.ads.is;
import com.google.android.gms.internal.ads.jm;
import com.google.android.gms.internal.ads.jv;
import com.google.android.gms.internal.ads.qn;
import com.google.android.gms.internal.ads.qy;

final class bj implements Runnable {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ is f2003a;

    /* renamed from: b reason: collision with root package name */
    final /* synthetic */ ic f2004b;
    final /* synthetic */ bg c;
    private final /* synthetic */ asv d;

    bj(bg bgVar, is isVar, ic icVar, asv asv) {
        this.c = bgVar;
        this.f2003a = isVar;
        this.f2004b = icVar;
        this.d = asv;
    }

    public final void run() {
        if (this.f2003a.f3398b.r && this.c.e.B != null) {
            String str = null;
            if (this.f2003a.f3398b.f3265a != null) {
                ax.e();
                str = jv.a(this.f2003a.f3398b.f3265a);
            }
            asw asw = new asw(this.c, str, this.f2003a.f3398b.f3266b);
            this.c.e.I = 1;
            try {
                this.c.c = false;
                this.c.e.B.a(asw);
                return;
            } catch (RemoteException e) {
                jm.d("#007 Could not call remote method.", e);
                this.c.c = true;
            }
        }
        bv bvVar = new bv(this.c.e.c, this.f2004b, this.f2003a.f3398b.E);
        try {
            qn a2 = this.c.a(this.f2003a, bvVar, this.f2004b);
            a2.setOnTouchListener(new bl(this, bvVar));
            a2.setOnClickListener(new bm(this, bvVar));
            this.c.e.I = 0;
            ay ayVar = this.c.e;
            ax.d();
            ayVar.h = ao.a(this.c.e.c, this.c, this.f2003a, this.c.e.d, a2, this.c.j, this.c, this.d);
        } catch (qy e2) {
            jm.b("Could not obtain webview.", e2);
            jv.f3440a.post(new bk(this));
        }
    }
}
