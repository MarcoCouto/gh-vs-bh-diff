package com.google.android.gms.ads.internal;

import android.app.Activity;
import android.content.Context;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.ViewTreeObserver.OnScrollChangedListener;
import android.widget.ViewSwitcher;
import com.google.android.gms.internal.ads.jm;
import com.google.android.gms.internal.ads.kp;
import com.google.android.gms.internal.ads.mg;
import com.google.android.gms.internal.ads.qn;
import java.util.ArrayList;

public final class az extends ViewSwitcher {

    /* renamed from: a reason: collision with root package name */
    private final kp f1991a;

    /* renamed from: b reason: collision with root package name */
    private final mg f1992b;
    private boolean c = true;

    public az(Context context, String str, String str2, OnGlobalLayoutListener onGlobalLayoutListener, OnScrollChangedListener onScrollChangedListener) {
        super(context);
        this.f1991a = new kp(context);
        this.f1991a.a(str);
        this.f1991a.b(str2);
        if (context instanceof Activity) {
            this.f1992b = new mg((Activity) context, this, onGlobalLayoutListener, onScrollChangedListener);
        } else {
            this.f1992b = new mg(null, this, onGlobalLayoutListener, onScrollChangedListener);
        }
        this.f1992b.a();
    }

    public final kp a() {
        return this.f1991a;
    }

    public final void b() {
        jm.a("Disable position monitoring on adFrame.");
        if (this.f1992b != null) {
            this.f1992b.b();
        }
    }

    public final void c() {
        jm.a("Enable debug gesture detector on adFrame.");
        this.c = true;
    }

    public final void d() {
        jm.a("Disable debug gesture detector on adFrame.");
        this.c = false;
    }

    /* access modifiers changed from: protected */
    public final void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.f1992b != null) {
            this.f1992b.c();
        }
    }

    /* access modifiers changed from: protected */
    public final void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.f1992b != null) {
            this.f1992b.d();
        }
    }

    public final boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        if (this.c) {
            this.f1991a.a(motionEvent);
        }
        return false;
    }

    public final void removeAllViews() {
        int i = 0;
        ArrayList arrayList = new ArrayList();
        for (int i2 = 0; i2 < getChildCount(); i2++) {
            View childAt = getChildAt(i2);
            if (childAt != null && (childAt instanceof qn)) {
                arrayList.add((qn) childAt);
            }
        }
        super.removeAllViews();
        ArrayList arrayList2 = arrayList;
        int size = arrayList2.size();
        while (i < size) {
            Object obj = arrayList2.get(i);
            i++;
            ((qn) obj).destroy();
        }
    }
}
