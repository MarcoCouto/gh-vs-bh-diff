package com.google.android.gms.ads.internal.gmsg;

import com.google.android.gms.ads.internal.overlay.d;
import com.google.android.gms.internal.ads.jm;
import com.google.android.gms.internal.ads.qn;
import java.util.Map;

final class w implements ae<qn> {
    w() {
    }

    public final /* synthetic */ void zza(Object obj, Map map) {
        qn qnVar = (qn) obj;
        d r = qnVar.r();
        if (r != null) {
            r.a();
            return;
        }
        d s = qnVar.s();
        if (s != null) {
            s.a();
        } else {
            jm.e("A GMSG tried to close something that wasn't an overlay.");
        }
    }
}
