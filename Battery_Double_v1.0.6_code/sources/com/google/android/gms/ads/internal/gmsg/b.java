package com.google.android.gms.ads.internal.gmsg;

import com.google.android.gms.internal.ads.cm;
import com.google.android.gms.internal.ads.jm;
import com.google.android.gms.internal.ads.ny;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Future;
import org.json.JSONException;
import org.json.JSONObject;

@cm
public final class b implements ae<Object> {

    /* renamed from: a reason: collision with root package name */
    private final HashMap<String, ny<JSONObject>> f2044a = new HashMap<>();

    public final Future<JSONObject> a(String str) {
        ny nyVar = new ny();
        this.f2044a.put(str, nyVar);
        return nyVar;
    }

    public final void b(String str) {
        ny nyVar = (ny) this.f2044a.get(str);
        if (nyVar == null) {
            jm.c("Could not find the ad request for the corresponding ad response.");
            return;
        }
        if (!nyVar.isDone()) {
            nyVar.cancel(true);
        }
        this.f2044a.remove(str);
    }

    public final void zza(Object obj, Map<String, String> map) {
        String str = (String) map.get("request_id");
        String str2 = (String) map.get("fetched_ad");
        jm.b("Received ad from the cache.");
        ny nyVar = (ny) this.f2044a.get(str);
        if (nyVar == null) {
            jm.c("Could not find the ad request for the corresponding ad response.");
            return;
        }
        try {
            nyVar.b(new JSONObject(str2));
        } catch (JSONException e) {
            jm.b("Failed constructing JSON object from value passed from javascript", e);
            nyVar.b(null);
        } finally {
            this.f2044a.remove(str);
        }
    }
}
