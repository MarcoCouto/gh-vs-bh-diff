package com.google.android.gms.ads.internal.overlay;

import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.ads.internal.gmsg.k;
import com.google.android.gms.ads.internal.gmsg.m;
import com.google.android.gms.ads.internal.r;
import com.google.android.gms.b.a.C0046a;
import com.google.android.gms.b.b;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.a.a;
import com.google.android.gms.common.internal.a.c;
import com.google.android.gms.internal.ads.aoj;
import com.google.android.gms.internal.ads.cm;
import com.google.android.gms.internal.ads.mu;
import com.google.android.gms.internal.ads.qn;

@cm
public final class AdOverlayInfoParcel extends a implements ReflectedParcelable {
    public static final Creator<AdOverlayInfoParcel> CREATOR = new m();

    /* renamed from: a reason: collision with root package name */
    public final c f2076a;

    /* renamed from: b reason: collision with root package name */
    public final aoj f2077b;
    public final n c;
    public final qn d;
    public final m e;
    public final String f;
    public final boolean g;
    public final String h;
    public final t i;
    public final int j;
    public final int k;
    public final String l;
    public final mu m;
    public final String n;
    public final r o;
    public final k p;

    AdOverlayInfoParcel(c cVar, IBinder iBinder, IBinder iBinder2, IBinder iBinder3, IBinder iBinder4, String str, boolean z, String str2, IBinder iBinder5, int i2, int i3, String str3, mu muVar, String str4, r rVar, IBinder iBinder6) {
        this.f2076a = cVar;
        this.f2077b = (aoj) b.a(C0046a.a(iBinder));
        this.c = (n) b.a(C0046a.a(iBinder2));
        this.d = (qn) b.a(C0046a.a(iBinder3));
        this.p = (k) b.a(C0046a.a(iBinder6));
        this.e = (m) b.a(C0046a.a(iBinder4));
        this.f = str;
        this.g = z;
        this.h = str2;
        this.i = (t) b.a(C0046a.a(iBinder5));
        this.j = i2;
        this.k = i3;
        this.l = str3;
        this.m = muVar;
        this.n = str4;
        this.o = rVar;
    }

    public AdOverlayInfoParcel(c cVar, aoj aoj, n nVar, t tVar, mu muVar) {
        this.f2076a = cVar;
        this.f2077b = aoj;
        this.c = nVar;
        this.d = null;
        this.p = null;
        this.e = null;
        this.f = null;
        this.g = false;
        this.h = null;
        this.i = tVar;
        this.j = -1;
        this.k = 4;
        this.l = null;
        this.m = muVar;
        this.n = null;
        this.o = null;
    }

    public AdOverlayInfoParcel(aoj aoj, n nVar, k kVar, m mVar, t tVar, qn qnVar, boolean z, int i2, String str, mu muVar) {
        this.f2076a = null;
        this.f2077b = aoj;
        this.c = nVar;
        this.d = qnVar;
        this.p = kVar;
        this.e = mVar;
        this.f = null;
        this.g = z;
        this.h = null;
        this.i = tVar;
        this.j = i2;
        this.k = 3;
        this.l = str;
        this.m = muVar;
        this.n = null;
        this.o = null;
    }

    public AdOverlayInfoParcel(aoj aoj, n nVar, k kVar, m mVar, t tVar, qn qnVar, boolean z, int i2, String str, String str2, mu muVar) {
        this.f2076a = null;
        this.f2077b = aoj;
        this.c = nVar;
        this.d = qnVar;
        this.p = kVar;
        this.e = mVar;
        this.f = str2;
        this.g = z;
        this.h = str;
        this.i = tVar;
        this.j = i2;
        this.k = 3;
        this.l = null;
        this.m = muVar;
        this.n = null;
        this.o = null;
    }

    public AdOverlayInfoParcel(aoj aoj, n nVar, t tVar, qn qnVar, int i2, mu muVar, String str, r rVar) {
        this.f2076a = null;
        this.f2077b = aoj;
        this.c = nVar;
        this.d = qnVar;
        this.p = null;
        this.e = null;
        this.f = null;
        this.g = false;
        this.h = null;
        this.i = tVar;
        this.j = i2;
        this.k = 1;
        this.l = null;
        this.m = muVar;
        this.n = str;
        this.o = rVar;
    }

    public AdOverlayInfoParcel(aoj aoj, n nVar, t tVar, qn qnVar, boolean z, int i2, mu muVar) {
        this.f2076a = null;
        this.f2077b = aoj;
        this.c = nVar;
        this.d = qnVar;
        this.p = null;
        this.e = null;
        this.f = null;
        this.g = z;
        this.h = null;
        this.i = tVar;
        this.j = i2;
        this.k = 2;
        this.l = null;
        this.m = muVar;
        this.n = null;
        this.o = null;
    }

    public static AdOverlayInfoParcel a(Intent intent) {
        try {
            Bundle bundleExtra = intent.getBundleExtra("com.google.android.gms.ads.inernal.overlay.AdOverlayInfo");
            bundleExtra.setClassLoader(AdOverlayInfoParcel.class.getClassLoader());
            return (AdOverlayInfoParcel) bundleExtra.getParcelable("com.google.android.gms.ads.inernal.overlay.AdOverlayInfo");
        } catch (Exception e2) {
            return null;
        }
    }

    public static void a(Intent intent, AdOverlayInfoParcel adOverlayInfoParcel) {
        Bundle bundle = new Bundle(1);
        bundle.putParcelable("com.google.android.gms.ads.inernal.overlay.AdOverlayInfo", adOverlayInfoParcel);
        intent.putExtra("com.google.android.gms.ads.inernal.overlay.AdOverlayInfo", bundle);
    }

    public final void writeToParcel(Parcel parcel, int i2) {
        int a2 = c.a(parcel);
        c.a(parcel, 2, (Parcelable) this.f2076a, i2, false);
        c.a(parcel, 3, b.a(this.f2077b).asBinder(), false);
        c.a(parcel, 4, b.a(this.c).asBinder(), false);
        c.a(parcel, 5, b.a(this.d).asBinder(), false);
        c.a(parcel, 6, b.a(this.e).asBinder(), false);
        c.a(parcel, 7, this.f, false);
        c.a(parcel, 8, this.g);
        c.a(parcel, 9, this.h, false);
        c.a(parcel, 10, b.a(this.i).asBinder(), false);
        c.a(parcel, 11, this.j);
        c.a(parcel, 12, this.k);
        c.a(parcel, 13, this.l, false);
        c.a(parcel, 14, (Parcelable) this.m, i2, false);
        c.a(parcel, 16, this.n, false);
        c.a(parcel, 17, (Parcelable) this.o, i2, false);
        c.a(parcel, 18, b.a(this.p).asBinder(), false);
        c.a(parcel, a2);
    }
}
