package com.google.android.gms.ads.internal.gmsg;

import com.google.android.gms.internal.ads.qn;
import java.util.Map;

final class ac implements ae<qn> {
    ac() {
    }

    public final /* synthetic */ void zza(Object obj, Map map) {
        qn qnVar = (qn) obj;
        if (map.keySet().contains("start")) {
            qnVar.v().h();
        } else if (map.keySet().contains("stop")) {
            qnVar.v().i();
        } else if (map.keySet().contains("cancel")) {
            qnVar.v().j();
        }
    }
}
