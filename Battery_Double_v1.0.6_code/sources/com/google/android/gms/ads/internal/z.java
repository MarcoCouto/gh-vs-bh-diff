package com.google.android.gms.ads.internal;

import android.content.Context;
import android.text.TextUtils;
import com.google.android.gms.b.a;
import com.google.android.gms.b.b;
import com.google.android.gms.common.internal.aa;
import com.google.android.gms.internal.ads.ape;
import com.google.android.gms.internal.ads.aqo;
import com.google.android.gms.internal.ads.asi;
import com.google.android.gms.internal.ads.bca;
import com.google.android.gms.internal.ads.bcb;
import com.google.android.gms.internal.ads.bcu;
import com.google.android.gms.internal.ads.cm;
import com.google.android.gms.internal.ads.fz;
import com.google.android.gms.internal.ads.hk;
import com.google.android.gms.internal.ads.hl;
import com.google.android.gms.internal.ads.jm;
import com.google.android.gms.internal.ads.kp;
import com.google.android.gms.internal.ads.mu;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

@cm
public final class z extends aqo {

    /* renamed from: b reason: collision with root package name */
    private static final Object f2111b = new Object();
    private static z c;

    /* renamed from: a reason: collision with root package name */
    private final Context f2112a;
    private final Object d = new Object();
    private boolean e;
    private mu f;

    private z(Context context, mu muVar) {
        this.f2112a = context;
        this.f = muVar;
        this.e = false;
    }

    public static z a(Context context, mu muVar) {
        z zVar;
        synchronized (f2111b) {
            if (c == null) {
                c = new z(context.getApplicationContext(), muVar);
            }
            zVar = c;
        }
        return zVar;
    }

    public final void a() {
        synchronized (f2111b) {
            if (this.e) {
                jm.e("Mobile ads is initialized already.");
                return;
            }
            this.e = true;
            asi.a(this.f2112a);
            ax.i().a(this.f2112a, this.f);
            ax.k().a(this.f2112a);
        }
    }

    public final void a(float f2) {
        ax.D().a(f2);
    }

    public final void a(a aVar, String str) {
        if (aVar == null) {
            jm.c("Wrapped context is null. Failed to open debug menu.");
            return;
        }
        Context context = (Context) b.a(aVar);
        if (context == null) {
            jm.c("Context is null. Failed to open debug menu.");
            return;
        }
        kp kpVar = new kp(context);
        kpVar.a(str);
        kpVar.b(this.f.f3528a);
        kpVar.a();
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ void a(Runnable runnable) {
        Context context = this.f2112a;
        aa.b("Adapters must be initialized on the main thread.");
        Map e2 = ax.i().l().h().e();
        if (e2 != null && !e2.isEmpty()) {
            if (runnable != null) {
                try {
                    runnable.run();
                } catch (Throwable th) {
                    jm.c("Could not initialize rewarded ads.", th);
                    return;
                }
            }
            fz J = fz.J();
            if (J != null) {
                Collection<bcb> values = e2.values();
                HashMap hashMap = new HashMap();
                a a2 = b.a(context);
                for (bcb bcb : values) {
                    for (bca bca : bcb.f3121a) {
                        String str = bca.k;
                        for (String str2 : bca.c) {
                            if (!hashMap.containsKey(str2)) {
                                hashMap.put(str2, new ArrayList());
                            }
                            if (str != null) {
                                ((Collection) hashMap.get(str2)).add(str);
                            }
                        }
                    }
                }
                for (Entry entry : hashMap.entrySet()) {
                    String str3 = (String) entry.getKey();
                    try {
                        hk b2 = J.b(str3);
                        if (b2 != null) {
                            bcu a3 = b2.a();
                            if (!a3.g() && a3.m()) {
                                a3.a(a2, (hl) b2.b(), (List) entry.getValue());
                                String str4 = "Initialized rewarded video mediation adapter ";
                                String valueOf = String.valueOf(str3);
                                jm.b(valueOf.length() != 0 ? str4.concat(valueOf) : new String(str4));
                            }
                        }
                    } catch (Throwable th2) {
                        jm.c(new StringBuilder(String.valueOf(str3).length() + 56).append("Failed to initialize rewarded video mediation adapter \"").append(str3).append("\"").toString(), th2);
                    }
                }
            }
        }
    }

    public final void a(String str) {
        asi.a(this.f2112a);
        if (!TextUtils.isEmpty(str)) {
            if (((Boolean) ape.f().a(asi.cs)).booleanValue()) {
                ax.m().a(this.f2112a, this.f, str, null);
            }
        }
    }

    public final void a(String str, a aVar) {
        Runnable runnable;
        boolean z;
        if (!TextUtils.isEmpty(str)) {
            asi.a(this.f2112a);
            boolean booleanValue = ((Boolean) ape.f().a(asi.cs)).booleanValue() | ((Boolean) ape.f().a(asi.aD)).booleanValue();
            if (((Boolean) ape.f().a(asi.aD)).booleanValue()) {
                runnable = new aa(this, (Runnable) b.a(aVar));
                z = true;
            } else {
                runnable = null;
                z = booleanValue;
            }
            if (z) {
                ax.m().a(this.f2112a, this.f, str, runnable);
            }
        }
    }

    public final void a(boolean z) {
        ax.D().a(z);
    }

    public final float b() {
        return ax.D().a();
    }

    public final boolean c() {
        return ax.D().b();
    }
}
