package com.google.android.gms.ads.internal.gmsg;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.text.TextUtils;
import android.view.View;
import com.google.android.gms.ads.internal.ax;
import com.google.android.gms.internal.ads.ahh;
import com.google.android.gms.internal.ads.ape;
import com.google.android.gms.internal.ads.asi;
import com.google.android.gms.internal.ads.jv;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public final class f {

    /* renamed from: a reason: collision with root package name */
    private final Context f2050a;

    /* renamed from: b reason: collision with root package name */
    private final ahh f2051b;
    private final View c;

    public f(Context context, ahh ahh, View view) {
        this.f2050a = context;
        this.f2051b = ahh;
        this.c = view;
    }

    private static Intent a(Intent intent, ResolveInfo resolveInfo) {
        Intent intent2 = new Intent(intent);
        intent2.setClassName(resolveInfo.activityInfo.packageName, resolveInfo.activityInfo.name);
        return intent2;
    }

    private static Intent a(Uri uri) {
        if (uri == null) {
            return null;
        }
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.addFlags(268435456);
        intent.setData(uri);
        intent.setAction("android.intent.action.VIEW");
        return intent;
    }

    private final ResolveInfo a(Intent intent) {
        return a(intent, new ArrayList<>());
    }

    private final ResolveInfo a(Intent intent, ArrayList<ResolveInfo> arrayList) {
        ResolveInfo resolveInfo;
        Throwable th;
        try {
            PackageManager packageManager = this.f2050a.getPackageManager();
            if (packageManager == null) {
                return null;
            }
            List queryIntentActivities = packageManager.queryIntentActivities(intent, 65536);
            ResolveInfo resolveActivity = packageManager.resolveActivity(intent, 65536);
            if (queryIntentActivities != null && resolveActivity != null) {
                int i = 0;
                while (true) {
                    int i2 = i;
                    if (i2 >= queryIntentActivities.size()) {
                        break;
                    }
                    ResolveInfo resolveInfo2 = (ResolveInfo) queryIntentActivities.get(i2);
                    if (resolveActivity != null && resolveActivity.activityInfo.name.equals(resolveInfo2.activityInfo.name)) {
                        resolveInfo = resolveActivity;
                        break;
                    }
                    i = i2 + 1;
                }
                arrayList.addAll(queryIntentActivities);
                return resolveInfo;
            }
            resolveInfo = null;
            try {
                arrayList.addAll(queryIntentActivities);
                return resolveInfo;
            } catch (Throwable th2) {
                th = th2;
            }
        } catch (Throwable th3) {
            Throwable th4 = th3;
            resolveInfo = null;
            th = th4;
            ax.i().a(th, "OpenSystemBrowserHandler.getDefaultBrowserResolverForIntent");
            return resolveInfo;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x008d  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x00a3  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x00ab  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x00c7  */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x006f  */
    public final Intent a(Map<String, String> map) {
        boolean z;
        ResolveInfo a2;
        Uri uri = null;
        ActivityManager activityManager = (ActivityManager) this.f2050a.getSystemService("activity");
        String str = (String) map.get("u");
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        Uri parse = Uri.parse(e.a(this.f2050a, this.f2051b, str, this.c, null));
        boolean parseBoolean = Boolean.parseBoolean((String) map.get("use_first_package"));
        boolean parseBoolean2 = Boolean.parseBoolean((String) map.get("use_running_process"));
        if (!Boolean.parseBoolean((String) map.get("use_custom_tabs"))) {
            if (!((Boolean) ape.f().a(asi.cM)).booleanValue()) {
                z = false;
                if (!"http".equalsIgnoreCase(parse.getScheme())) {
                    uri = parse.buildUpon().scheme("https").build();
                } else if ("https".equalsIgnoreCase(parse.getScheme())) {
                    uri = parse.buildUpon().scheme("http").build();
                }
                ArrayList arrayList = new ArrayList();
                Intent a3 = a(parse);
                Intent a4 = a(uri);
                if (z) {
                    ax.e();
                    jv.b(this.f2050a, a3);
                    ax.e();
                    jv.b(this.f2050a, a4);
                }
                a2 = a(a3, arrayList);
                if (a2 == null) {
                    return a(a3, a2);
                }
                if (a4 != null) {
                    ResolveInfo a5 = a(a4);
                    if (a5 != null) {
                        Intent a6 = a(a3, a5);
                        if (a(a6) != null) {
                            return a6;
                        }
                    }
                }
                if (arrayList.size() == 0) {
                    return a3;
                }
                if (parseBoolean2 && activityManager != null) {
                    List<RunningAppProcessInfo> runningAppProcesses = activityManager.getRunningAppProcesses();
                    if (runningAppProcesses != null) {
                        ArrayList arrayList2 = arrayList;
                        int size = arrayList2.size();
                        int i = 0;
                        while (i < size) {
                            int i2 = i + 1;
                            ResolveInfo resolveInfo = (ResolveInfo) arrayList2.get(i);
                            for (RunningAppProcessInfo runningAppProcessInfo : runningAppProcesses) {
                                if (runningAppProcessInfo.processName.equals(resolveInfo.activityInfo.packageName)) {
                                    return a(a3, resolveInfo);
                                }
                            }
                            i = i2;
                        }
                    }
                }
                return parseBoolean ? a(a3, (ResolveInfo) arrayList.get(0)) : a3;
            }
        }
        z = true;
        if (!"http".equalsIgnoreCase(parse.getScheme())) {
        }
        ArrayList arrayList3 = new ArrayList();
        Intent a32 = a(parse);
        Intent a42 = a(uri);
        if (z) {
        }
        a2 = a(a32, arrayList3);
        if (a2 == null) {
        }
    }
}
