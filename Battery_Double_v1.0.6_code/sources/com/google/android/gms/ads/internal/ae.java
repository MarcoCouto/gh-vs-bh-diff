package com.google.android.gms.ads.internal;

import android.content.Context;
import android.os.RemoteException;
import android.support.v4.h.m;
import android.util.Log;
import android.view.View;
import com.google.android.gms.b.a;
import com.google.android.gms.b.b;
import com.google.android.gms.common.internal.aa;
import com.google.android.gms.internal.ads.ajw;
import com.google.android.gms.internal.ads.alg;
import com.google.android.gms.internal.ads.ao;
import com.google.android.gms.internal.ads.aop;
import com.google.android.gms.internal.ads.aot;
import com.google.android.gms.internal.ads.ape;
import com.google.android.gms.internal.ads.aqs;
import com.google.android.gms.internal.ads.aqv;
import com.google.android.gms.internal.ads.asi;
import com.google.android.gms.internal.ads.asv;
import com.google.android.gms.internal.ads.atc;
import com.google.android.gms.internal.ads.atn;
import com.google.android.gms.internal.ads.atp;
import com.google.android.gms.internal.ads.atr;
import com.google.android.gms.internal.ads.atu;
import com.google.android.gms.internal.ads.atw;
import com.google.android.gms.internal.ads.atx;
import com.google.android.gms.internal.ads.aty;
import com.google.android.gms.internal.ads.aua;
import com.google.android.gms.internal.ads.aub;
import com.google.android.gms.internal.ads.aud;
import com.google.android.gms.internal.ads.avt;
import com.google.android.gms.internal.ads.awe;
import com.google.android.gms.internal.ads.awh;
import com.google.android.gms.internal.ads.bcb;
import com.google.android.gms.internal.ads.bcr;
import com.google.android.gms.internal.ads.bcu;
import com.google.android.gms.internal.ads.bdd;
import com.google.android.gms.internal.ads.bdh;
import com.google.android.gms.internal.ads.bdk;
import com.google.android.gms.internal.ads.bq;
import com.google.android.gms.internal.ads.bu;
import com.google.android.gms.internal.ads.cm;
import com.google.android.gms.internal.ads.ir;
import com.google.android.gms.internal.ads.is;
import com.google.android.gms.internal.ads.jm;
import com.google.android.gms.internal.ads.jt;
import com.google.android.gms.internal.ads.jv;
import com.google.android.gms.internal.ads.ms;
import com.google.android.gms.internal.ads.mu;
import com.google.android.gms.internal.ads.nn;
import com.google.android.gms.internal.ads.ny;
import com.google.android.gms.internal.ads.qn;
import com.google.android.gms.internal.ads.qy;
import com.google.android.gms.internal.ads.y;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@cm
public final class ae extends bb implements aua {
    private final Object k;
    private boolean l;
    private ny<aub> m;
    private qn n;
    private qn o;
    private boolean p;
    private int q;
    private bq r;
    private final String s;

    public ae(Context context, bu buVar, aot aot, String str, bcr bcr, mu muVar) {
        this(context, buVar, aot, str, bcr, muVar, false);
    }

    public ae(Context context, bu buVar, aot aot, String str, bcr bcr, mu muVar, boolean z) {
        super(context, aot, str, bcr, muVar, buVar);
        this.k = new Object();
        this.m = new ny<>();
        this.q = 1;
        this.s = UUID.randomUUID().toString();
        this.l = z;
    }

    /* access modifiers changed from: private */
    public static void a(ay ayVar, ay ayVar2) {
        if (ayVar2.r == null) {
            ayVar2.r = ayVar.r;
        }
        if (ayVar2.s == null) {
            ayVar2.s = ayVar.s;
        }
        if (ayVar2.u == null) {
            ayVar2.u = ayVar.u;
        }
        if (ayVar2.v == null) {
            ayVar2.v = ayVar.v;
        }
        if (ayVar2.x == null) {
            ayVar2.x = ayVar.x;
        }
        if (ayVar2.w == null) {
            ayVar2.w = ayVar.w;
        }
        if (ayVar2.F == null) {
            ayVar2.F = ayVar.F;
        }
        if (ayVar2.l == null) {
            ayVar2.l = ayVar.l;
        }
        if (ayVar2.G == null) {
            ayVar2.G = ayVar.G;
        }
        if (ayVar2.m == null) {
            ayVar2.m = ayVar.m;
        }
        if (ayVar2.n == null) {
            ayVar2.n = ayVar.n;
        }
        if (ayVar2.i == null) {
            ayVar2.i = ayVar.i;
        }
        if (ayVar2.j == null) {
            ayVar2.j = ayVar.j;
        }
        if (ayVar2.k == null) {
            ayVar2.k = ayVar.k;
        }
    }

    private final void a(atn atn) {
        jv.f3440a.post(new ai(this, atn));
    }

    private final void a(atp atp) {
        jv.f3440a.post(new ak(this, atp));
    }

    private final void a(atu atu) {
        jv.f3440a.post(new aj(this, atu));
    }

    private final boolean ad() {
        return this.e.j != null && this.e.j.N;
    }

    private final bcb ae() {
        if (this.e.j == null || !this.e.j.n) {
            return null;
        }
        return this.e.j.r;
    }

    private final void af() {
        bq J = J();
        if (J != null) {
            J.a();
        }
    }

    /* access modifiers changed from: private */
    public static atu b(aub aub) {
        atu atu = null;
        Object obj = null;
        if (aub instanceof atp) {
            atp atp = (atp) aub;
            atu = new atu(atp.a(), atp.b(), atp.e(), atp.f(), atp.g(), atp.h(), -1.0d, null, null, atp.m(), atp.i(), atp.o(), atp.c(), atp.d(), atp.n());
            obj = atp.j() != null ? b.a(atp.j()) : null;
        } else if (aub instanceof atn) {
            atn atn = (atn) aub;
            atu = new atu(atn.a(), atn.b(), atn.c(), atn.d(), atn.e(), null, atn.f(), atn.g(), atn.h(), atn.m(), atn.i(), atn.o(), atn.p(), atn.q(), atn.n());
            obj = atn.j() != null ? b.a(atn.j()) : null;
        }
        if (obj instanceof aud) {
            atu.a((aty) (aud) obj);
        }
        return atu;
    }

    public final String D() {
        return this.e.f1990b;
    }

    public final void I() {
        throw new IllegalStateException("Interstitial is NOT supported by NativeAdManager.");
    }

    public final bq J() {
        bq bqVar;
        synchronized (this.k) {
            bqVar = this.r;
        }
        return bqVar;
    }

    /* access modifiers changed from: protected */
    public final Future<aub> K() {
        return this.m;
    }

    public final void L() {
        if (ad() && this.h != null) {
            qn qnVar = null;
            if (this.o != null) {
                qnVar = this.o;
            } else if (this.n != null) {
                qnVar = this.n;
            }
            if (qnVar != null) {
                qnVar.a("onSdkImpression", (Map) new HashMap());
            }
        }
    }

    public final void M() {
        if (this.e.j == null || this.n == null) {
            this.p = true;
            jm.e("Request to enable ActiveView before adState is available.");
            return;
        }
        ax.i().g().a(this.e.i, this.e.j, this.n.getView(), this.n);
        this.p = false;
    }

    public final void N() {
        this.p = false;
        if (this.e.j == null || this.n == null) {
            jm.e("Request to enable ActiveView before adState is available.");
        } else {
            ax.i().g().a(this.e.j);
        }
    }

    public final m<String, awh> O() {
        aa.b("getOnCustomTemplateAdLoadedListeners must be called on the main UI thread.");
        return this.e.v;
    }

    public final void P() {
        if (this.n != null) {
            this.n.destroy();
            this.n = null;
        }
    }

    public final void Q() {
        super.G();
        if (this.o != null) {
            this.o.destroy();
            this.o = null;
        }
    }

    public final void R() {
        if (this.n != null && this.n.b() != null && this.e.w != null && this.e.w.f != null) {
            this.n.b().a(this.e.w.f);
        }
    }

    public final boolean S() {
        if (ae() != null) {
            return ae().p;
        }
        return false;
    }

    public final boolean T() {
        if (ae() != null) {
            return ae().q;
        }
        return false;
    }

    public final void U() {
        if (this.e.j == null || !"com.google.ads.mediation.admob.AdMobAdapter".equals(this.e.j.q)) {
            super.U();
        } else {
            z();
        }
    }

    public final void V() {
        if (this.e.j == null || !"com.google.ads.mediation.admob.AdMobAdapter".equals(this.e.j.q)) {
            super.V();
        } else {
            y();
        }
    }

    public final void W() {
        ir irVar = this.e.j;
        if (irVar.p == null) {
            super.W();
            return;
        }
        try {
            bcu bcu = irVar.p;
            aqs aqs = null;
            bdd h = bcu.h();
            if (h != null) {
                aqs = h.m();
            } else {
                bdh i = bcu.i();
                if (i != null) {
                    aqs = i.l();
                } else {
                    avt n2 = bcu.n();
                    if (n2 != null) {
                        aqs = n2.c();
                    }
                }
            }
            if (aqs != null) {
                aqv h2 = aqs.h();
                if (h2 != null) {
                    h2.d();
                }
            }
        } catch (RemoteException e) {
            jm.d("#007 Could not call remote method.", e);
        }
    }

    /* access modifiers changed from: protected */
    public final void a(int i) {
        a(i, false);
    }

    /* access modifiers changed from: protected */
    public final void a(int i, boolean z) {
        af();
        super.a(i, z);
    }

    /* access modifiers changed from: protected */
    public final void a(a aVar) {
        Object obj = aVar != null ? b.a(aVar) : null;
        if (obj instanceof aty) {
            ((aty) obj).d();
        }
        super.b(this.e.j, false);
    }

    public final void a(atc atc) {
        throw new IllegalStateException("CustomRendering is NOT supported by NativeAdManager.");
    }

    public final void a(atw atw) {
        if (this.n != null) {
            this.n.a(atw);
        }
    }

    public final void a(aty aty) {
        if (this.e.j.k != null) {
            ax.i().g().a(this.e.i, this.e.j, (alg) new ajw(aty), (qn) null);
        }
    }

    public final void a(is isVar, asv asv) {
        if (isVar.d != null) {
            this.e.i = isVar.d;
        }
        if (isVar.e != -2) {
            jv.f3440a.post(new af(this, isVar));
            return;
        }
        int i = isVar.f3397a.Y;
        if (i == 1) {
            this.e.I = 0;
            ay ayVar = this.e;
            ax.d();
            ayVar.h = ao.a(this.e.c, this, isVar, this.e.d, null, this.j, this, asv);
            String str = "AdRenderer: ";
            String valueOf = String.valueOf(this.e.h.getClass().getName());
            jm.b(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
            return;
        }
        JSONArray jSONArray = new JSONArray();
        try {
            JSONArray jSONArray2 = new JSONObject(isVar.f3398b.f3266b).getJSONArray("slots");
            for (int i2 = 0; i2 < jSONArray2.length(); i2++) {
                JSONArray jSONArray3 = jSONArray2.getJSONObject(i2).getJSONArray("ads");
                for (int i3 = 0; i3 < jSONArray3.length(); i3++) {
                    jSONArray.put(jSONArray3.get(i3));
                }
            }
            af();
            ArrayList arrayList = new ArrayList();
            for (int i4 = 0; i4 < i; i4++) {
                arrayList.add(jt.a((Callable<T>) new ag<T>(this, i4, jSONArray, i, isVar)));
            }
            for (int i5 = 0; i5 < arrayList.size(); i5++) {
                try {
                    jv.f3440a.post(new ah(this, (aub) ((nn) arrayList.get(i5)).get(((Long) ape.f().a(asi.bB)).longValue(), TimeUnit.MILLISECONDS), i5, arrayList));
                } catch (InterruptedException e) {
                    ms.c("", e);
                    Thread.currentThread().interrupt();
                } catch (CancellationException | ExecutionException | TimeoutException e2) {
                    ms.c("", e2);
                }
            }
        } catch (JSONException e3) {
            jm.c("Malformed native ad response", e3);
            a(0);
        }
    }

    public final void a(qn qnVar) {
        this.n = qnVar;
    }

    public final void a(y yVar) {
        throw new IllegalStateException("In App Purchase is NOT supported by NativeAdManager.");
    }

    public final boolean a(aop aop, asv asv) {
        try {
            k_();
            return super.a(aop, asv, this.q);
        } catch (Exception e) {
            String str = "Error initializing webview.";
            if (ms.a(4)) {
                Log.i("Ads", str, e);
            }
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public final boolean a(aop aop, ir irVar, boolean z) {
        return this.d.e();
    }

    /* access modifiers changed from: protected */
    public final boolean a(ir irVar, ir irVar2) {
        c(null);
        if (!this.e.d()) {
            throw new IllegalStateException("Native ad DOES NOT have custom rendering mode.");
        }
        if (irVar2.n) {
            af();
            try {
                bdk bdk = irVar2.p != null ? irVar2.p.p() : null;
                bdd bdd = irVar2.p != null ? irVar2.p.h() : null;
                bdh bdh = irVar2.p != null ? irVar2.p.i() : null;
                avt avt = irVar2.p != null ? irVar2.p.n() : null;
                String c = c(irVar2);
                if (bdk == null || this.e.t == null) {
                    if (bdd != null) {
                        if (this.e.t != null) {
                            atu atu = new atu(bdd.a(), bdd.b(), bdd.c(), bdd.d() != null ? bdd.d() : null, bdd.e(), null, bdd.f(), bdd.g(), bdd.h(), null, bdd.m(), bdd.p() != null ? (View) b.a(bdd.p()) : null, bdd.q(), c, bdd.l());
                            atu.a((aty) new atx(this.e.c, (aua) this, this.e.d, bdd, (aub) atu));
                            a(atu);
                        }
                    }
                    if (bdd != null) {
                        if (this.e.r != null) {
                            atn atn = new atn(bdd.a(), bdd.b(), bdd.c(), bdd.d() != null ? bdd.d() : null, bdd.e(), bdd.f(), bdd.g(), bdd.h(), null, bdd.l(), bdd.m(), bdd.p() != null ? (View) b.a(bdd.p()) : null, bdd.q(), c);
                            atn.a((aty) new atx(this.e.c, (aua) this, this.e.d, bdd, (aub) atn));
                            a(atn);
                        }
                    }
                    if (bdh != null && this.e.t != null) {
                        atu atu2 = new atu(bdh.a(), bdh.b(), bdh.c(), bdh.d() != null ? bdh.d() : null, bdh.e(), bdh.f(), -1.0d, null, null, null, bdh.l(), bdh.n() != null ? (View) b.a(bdh.n()) : null, bdh.o(), c, bdh.j());
                        atu2.a((aty) new atx(this.e.c, (aua) this, this.e.d, bdh, (aub) atu2));
                        a(atu2);
                    } else if (bdh != null && this.e.s != null) {
                        atp atp = new atp(bdh.a(), bdh.b(), bdh.c(), bdh.d() != null ? bdh.d() : null, bdh.e(), bdh.f(), null, bdh.j(), bdh.l(), bdh.n() != null ? (View) b.a(bdh.n()) : null, bdh.o(), c);
                        atp.a((aty) new atx(this.e.c, (aua) this, this.e.d, bdh, (aub) atp));
                        a(atp);
                    } else if (avt == null || this.e.v == null || this.e.v.get(avt.l()) == null) {
                        jm.e("No matching mapper/listener for retrieved native ad template.");
                        a(0);
                        return false;
                    } else {
                        jv.f3440a.post(new am(this, avt));
                    }
                } else {
                    atu atu3 = new atu(bdk.a(), bdk.b(), bdk.c(), bdk.d() != null ? bdk.d() : null, bdk.e(), bdk.f(), bdk.g(), bdk.h(), bdk.i(), null, bdk.j(), bdk.m() != null ? (View) b.a(bdk.m()) : null, bdk.n(), c, bdk.o());
                    atu3.a((aty) new atx(this.e.c, (aua) this, this.e.d, bdk, (aub) atu3));
                    a(atu3);
                }
            } catch (RemoteException e) {
                jm.d("#007 Could not call remote method.", e);
            }
        } else {
            aub aub = irVar2.C;
            if (this.l) {
                this.m.b(aub);
            } else if ((aub instanceof atp) && this.e.t != null) {
                a(b(irVar2.C));
            } else if ((aub instanceof atp) && this.e.s != null) {
                a((atp) irVar2.C);
            } else if ((aub instanceof atn) && this.e.t != null) {
                a(b(irVar2.C));
            } else if ((aub instanceof atn) && this.e.r != null) {
                a((atn) irVar2.C);
            } else if (!(aub instanceof atr) || this.e.v == null || this.e.v.get(((atr) aub).l()) == null) {
                jm.e("No matching listener for retrieved native ad template.");
                a(0);
                return false;
            } else {
                jv.f3440a.post(new al(this, ((atr) aub).l(), irVar2));
            }
        }
        return super.a(irVar, irVar2);
    }

    public final awe b(String str) {
        aa.b("getOnCustomClickListener must be called on the main UI thread.");
        if (this.e.u == null) {
            return null;
        }
        return (awe) this.e.u.get(str);
    }

    public final void b(int i) {
        aa.b("setMaxNumberOfAds must be called on the main UI thread.");
        this.q = i;
    }

    public final void b(View view) {
        if (this.h != null) {
            ax.u().a(this.h, view);
        }
    }

    public final void b(qn qnVar) {
        this.o = qnVar;
    }

    public final void c(List<String> list) {
        aa.b("setNativeTemplates must be called on the main UI thread.");
        this.e.F = list;
    }

    /* access modifiers changed from: protected */
    public final void d(boolean z) {
        qn qnVar;
        String str = null;
        super.d(z);
        if (this.p) {
            if (((Boolean) ape.f().a(asi.cg)).booleanValue()) {
                M();
            }
        }
        if (!ad()) {
            return;
        }
        if (this.o != null || this.n != null) {
            if (this.o != null) {
                qnVar = this.o;
            } else if (this.n != null) {
                str = "javascript";
                qnVar = this.n;
            } else {
                qnVar = null;
            }
            if (qnVar.getWebView() != null && ax.u().a(this.e.c)) {
                int i = this.e.e.f3529b;
                this.h = ax.u().a(i + "." + this.e.e.c, qnVar.getWebView(), "", "javascript", str);
                if (this.h != null) {
                    ax.u().a(this.h);
                }
            }
        }
    }

    public final String j_() {
        return this.s;
    }

    /* access modifiers changed from: 0000 */
    public final void k_() throws qy {
        synchronized (this.k) {
            jm.a("Initializing webview native ads utills");
            this.r = new bu(this.e.c, this, this.s, this.e.d, this.e.e);
        }
    }

    public final void o() {
        throw new IllegalStateException("Native Ad DOES NOT support pause().");
    }

    public final void p() {
        throw new IllegalStateException("Native Ad DOES NOT support resume().");
    }

    /* access modifiers changed from: protected */
    public final void x() {
        d(false);
    }
}
