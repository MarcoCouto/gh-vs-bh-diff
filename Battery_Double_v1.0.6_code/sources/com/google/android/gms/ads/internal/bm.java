package com.google.android.gms.ads.internal;

import android.view.View;
import android.view.View.OnClickListener;

final class bm implements OnClickListener {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ bv f2008a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ bj f2009b;

    bm(bj bjVar, bv bvVar) {
        this.f2009b = bjVar;
        this.f2008a = bvVar;
    }

    public final void onClick(View view) {
        this.f2008a.a();
        if (this.f2009b.f2004b != null) {
            this.f2009b.f2004b.c();
        }
    }
}
