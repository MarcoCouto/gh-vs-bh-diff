package com.google.android.gms.ads.internal;

import android.os.RemoteException;
import com.google.android.gms.internal.ads.atn;
import com.google.android.gms.internal.ads.jm;

final class br implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ atn f2013a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ bo f2014b;

    br(bo boVar, atn atn) {
        this.f2014b = boVar;
        this.f2013a = atn;
    }

    public final void run() {
        try {
            if (this.f2014b.e.r != null) {
                this.f2014b.e.r.a(this.f2013a);
                this.f2014b.a(this.f2013a.j());
            }
        } catch (RemoteException e) {
            jm.d("#007 Could not call remote method.", e);
        }
    }
}
