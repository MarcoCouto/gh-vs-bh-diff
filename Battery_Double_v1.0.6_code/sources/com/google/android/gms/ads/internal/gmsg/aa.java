package com.google.android.gms.ads.internal.gmsg;

import com.google.android.gms.internal.ads.ape;
import com.google.android.gms.internal.ads.asi;
import com.google.android.gms.internal.ads.qn;
import com.hmatalonga.greenhub.Config;
import java.util.Map;

final class aa implements ae<qn> {
    aa() {
    }

    public final /* synthetic */ void zza(Object obj, Map map) {
        qn qnVar = (qn) obj;
        if (((Boolean) ape.f().a(asi.bt)).booleanValue()) {
            qnVar.d(!Boolean.parseBoolean((String) map.get(Config.IMPORTANCE_DISABLED)));
        }
    }
}
