package com.google.android.gms.ads.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.a.a;
import com.google.android.gms.common.internal.a.c;
import com.google.android.gms.internal.ads.cm;

@cm
public final class r extends a {
    public static final Creator<r> CREATOR = new s();

    /* renamed from: a reason: collision with root package name */
    public final boolean f2101a;

    /* renamed from: b reason: collision with root package name */
    public final boolean f2102b;
    public final boolean c;
    public final float d;
    public final int e;
    public final boolean f;
    public final boolean g;
    public final boolean h;
    private final String i;

    r(boolean z, boolean z2, String str, boolean z3, float f2, int i2, boolean z4, boolean z5, boolean z6) {
        this.f2101a = z;
        this.f2102b = z2;
        this.i = str;
        this.c = z3;
        this.d = f2;
        this.e = i2;
        this.f = z4;
        this.g = z5;
        this.h = z6;
    }

    public r(boolean z, boolean z2, boolean z3, float f2, int i2, boolean z4, boolean z5, boolean z6) {
        this(z, z2, null, z3, f2, i2, z4, z5, z6);
    }

    public final void writeToParcel(Parcel parcel, int i2) {
        int a2 = c.a(parcel);
        c.a(parcel, 2, this.f2101a);
        c.a(parcel, 3, this.f2102b);
        c.a(parcel, 4, this.i, false);
        c.a(parcel, 5, this.c);
        c.a(parcel, 6, this.d);
        c.a(parcel, 7, this.e);
        c.a(parcel, 8, this.f);
        c.a(parcel, 9, this.g);
        c.a(parcel, 10, this.h);
        c.a(parcel, a2);
    }
}
