package com.google.android.gms.ads.internal;

final /* synthetic */ class ac implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final z f1956a;

    /* renamed from: b reason: collision with root package name */
    private final Runnable f1957b;

    ac(z zVar, Runnable runnable) {
        this.f1956a = zVar;
        this.f1957b = runnable;
    }

    public final void run() {
        this.f1956a.a(this.f1957b);
    }
}
