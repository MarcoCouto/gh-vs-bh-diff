package com.google.android.gms.ads.internal.gmsg;

import com.google.android.gms.internal.ads.jm;
import org.json.JSONObject;

final class ag implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ JSONObject f2041a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ af f2042b;

    ag(af afVar, JSONObject jSONObject) {
        this.f2042b = afVar;
        this.f2041a = jSONObject;
    }

    public final void run() {
        this.f2042b.f2039a.a("fetchHttpRequestCompleted", this.f2041a);
        jm.b("Dispatched http response.");
    }
}
