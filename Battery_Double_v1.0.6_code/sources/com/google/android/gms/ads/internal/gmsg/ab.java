package com.google.android.gms.ads.internal.gmsg;

import com.google.android.gms.internal.ads.qn;
import java.util.Map;

final class ab implements ae<qn> {
    ab() {
    }

    public final /* synthetic */ void zza(Object obj, Map map) {
        qn qnVar = (qn) obj;
        String str = (String) map.get("action");
        if ("pause".equals(str)) {
            qnVar.h_();
        } else if ("resume".equals(str)) {
            qnVar.i_();
        }
    }
}
