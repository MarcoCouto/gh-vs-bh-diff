package com.google.android.gms.ads.internal;

import android.content.Context;
import android.support.v4.h.m;
import android.text.TextUtils;
import com.google.android.gms.ads.b.j;
import com.google.android.gms.internal.ads.aot;
import com.google.android.gms.internal.ads.apk;
import com.google.android.gms.internal.ads.apn;
import com.google.android.gms.internal.ads.apr;
import com.google.android.gms.internal.ads.aqk;
import com.google.android.gms.internal.ads.aul;
import com.google.android.gms.internal.ads.avx;
import com.google.android.gms.internal.ads.awa;
import com.google.android.gms.internal.ads.awe;
import com.google.android.gms.internal.ads.awh;
import com.google.android.gms.internal.ads.awk;
import com.google.android.gms.internal.ads.awn;
import com.google.android.gms.internal.ads.bcr;
import com.google.android.gms.internal.ads.cm;
import com.google.android.gms.internal.ads.mu;

@cm
public final class l extends apr {

    /* renamed from: a reason: collision with root package name */
    private apk f2071a;

    /* renamed from: b reason: collision with root package name */
    private avx f2072b;
    private awn c;
    private awa d;
    private m<String, awe> e = new m<>();
    private m<String, awh> f = new m<>();
    private awk g;
    private aot h;
    private j i;
    private aul j;
    private aqk k;
    private final Context l;
    private final bcr m;
    private final String n;
    private final mu o;
    private final bu p;

    public l(Context context, String str, bcr bcr, mu muVar, bu buVar) {
        this.l = context;
        this.n = str;
        this.m = bcr;
        this.o = muVar;
        this.p = buVar;
    }

    public final apn a() {
        return new i(this.l, this.n, this.m, this.o, this.f2071a, this.f2072b, this.c, this.d, this.f, this.e, this.j, this.k, this.p, this.g, this.h, this.i);
    }

    public final void a(j jVar) {
        this.i = jVar;
    }

    public final void a(apk apk) {
        this.f2071a = apk;
    }

    public final void a(aqk aqk) {
        this.k = aqk;
    }

    public final void a(aul aul) {
        this.j = aul;
    }

    public final void a(avx avx) {
        this.f2072b = avx;
    }

    public final void a(awa awa) {
        this.d = awa;
    }

    public final void a(awk awk, aot aot) {
        this.g = awk;
        this.h = aot;
    }

    public final void a(awn awn) {
        this.c = awn;
    }

    public final void a(String str, awh awh, awe awe) {
        if (TextUtils.isEmpty(str)) {
            throw new IllegalArgumentException("Custom template ID for native custom template ad is empty. Please provide a valid template id.");
        }
        this.f.put(str, awh);
        this.e.put(str, awe);
    }
}
