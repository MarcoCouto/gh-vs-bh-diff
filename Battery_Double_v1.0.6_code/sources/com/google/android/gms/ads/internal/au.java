package com.google.android.gms.ads.internal;

import com.google.android.gms.internal.ads.ahg;
import com.google.android.gms.internal.ads.ahh;
import java.util.concurrent.Callable;

final class au implements Callable<ahh> {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ ar f1983a;

    au(ar arVar) {
        this.f1983a = arVar;
    }

    public final /* synthetic */ Object call() throws Exception {
        return new ahh(ahg.a(this.f1983a.f1979a.f3528a, this.f1983a.d, false));
    }
}
