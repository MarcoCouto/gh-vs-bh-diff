package com.google.android.gms.ads;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.RemoteException;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import com.google.android.gms.b.b;
import com.google.android.gms.internal.ads.ape;
import com.google.android.gms.internal.ads.ms;
import com.google.android.gms.internal.ads.r;

public class AdActivity extends Activity {

    /* renamed from: a reason: collision with root package name */
    private r f1917a;

    private final void a() {
        if (this.f1917a != null) {
            try {
                this.f1917a.l();
            } catch (RemoteException e) {
                ms.d("#007 Could not call remote method.", e);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        try {
            this.f1917a.a(i, i2, intent);
        } catch (Exception e) {
            ms.d("#007 Could not call remote method.", e);
        }
        super.onActivityResult(i, i2, intent);
    }

    public void onBackPressed() {
        boolean z = true;
        try {
            if (this.f1917a != null) {
                z = this.f1917a.e();
            }
        } catch (RemoteException e) {
            ms.d("#007 Could not call remote method.", e);
        }
        if (z) {
            super.onBackPressed();
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        try {
            this.f1917a.a(b.a(configuration));
        } catch (RemoteException e) {
            ms.d("#007 Could not call remote method.", e);
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f1917a = ape.b().a((Activity) this);
        if (this.f1917a == null) {
            ms.d("#007 Could not call remote method.", null);
            finish();
            return;
        }
        try {
            this.f1917a.a(bundle);
        } catch (RemoteException e) {
            ms.d("#007 Could not call remote method.", e);
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        try {
            if (this.f1917a != null) {
                this.f1917a.k();
            }
        } catch (RemoteException e) {
            ms.d("#007 Could not call remote method.", e);
        }
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        try {
            if (this.f1917a != null) {
                this.f1917a.i();
            }
        } catch (RemoteException e) {
            ms.d("#007 Could not call remote method.", e);
            finish();
        }
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
        try {
            if (this.f1917a != null) {
                this.f1917a.f();
            }
        } catch (RemoteException e) {
            ms.d("#007 Could not call remote method.", e);
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        try {
            if (this.f1917a != null) {
                this.f1917a.h();
            }
        } catch (RemoteException e) {
            ms.d("#007 Could not call remote method.", e);
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        try {
            if (this.f1917a != null) {
                this.f1917a.b(bundle);
            }
        } catch (RemoteException e) {
            ms.d("#007 Could not call remote method.", e);
            finish();
        }
        super.onSaveInstanceState(bundle);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        try {
            if (this.f1917a != null) {
                this.f1917a.g();
            }
        } catch (RemoteException e) {
            ms.d("#007 Could not call remote method.", e);
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        try {
            if (this.f1917a != null) {
                this.f1917a.j();
            }
        } catch (RemoteException e) {
            ms.d("#007 Could not call remote method.", e);
            finish();
        }
        super.onStop();
    }

    public void setContentView(int i) {
        super.setContentView(i);
        a();
    }

    public void setContentView(View view) {
        super.setContentView(view);
        a();
    }

    public void setContentView(View view, LayoutParams layoutParams) {
        super.setContentView(view, layoutParams);
        a();
    }
}
