package com.google.android.gms.ads;

import android.content.Context;
import com.google.android.gms.internal.ads.aot;
import com.google.android.gms.internal.ads.ape;
import com.google.android.gms.internal.ads.mh;

public final class d {

    /* renamed from: a reason: collision with root package name */
    public static final d f1937a = new d(320, 50, "320x50_mb");

    /* renamed from: b reason: collision with root package name */
    public static final d f1938b = new d(468, 60, "468x60_as");
    public static final d c = new d(320, 100, "320x100_as");
    public static final d d = new d(728, 90, "728x90_as");
    public static final d e = new d(300, 250, "300x250_as");
    public static final d f = new d(160, 600, "160x600_as");
    public static final d g = new d(-1, -2, "smart_banner");
    public static final d h = new d(-3, -4, "fluid");
    public static final d i = new d(50, 50, "50x50_mb");
    public static final d j = new d(-3, 0, "search_v2");
    private final int k;
    private final int l;
    private final String m;

    public d(int i2, int i3) {
        String valueOf = i2 == -1 ? "FULL" : String.valueOf(i2);
        String valueOf2 = i3 == -2 ? "AUTO" : String.valueOf(i3);
        this(i2, i3, new StringBuilder(String.valueOf(valueOf).length() + 4 + String.valueOf(valueOf2).length()).append(valueOf).append("x").append(valueOf2).append("_as").toString());
    }

    d(int i2, int i3, String str) {
        if (i2 < 0 && i2 != -1 && i2 != -3) {
            throw new IllegalArgumentException("Invalid width for AdSize: " + i2);
        } else if (i3 >= 0 || i3 == -2 || i3 == -4) {
            this.k = i2;
            this.l = i3;
            this.m = str;
        } else {
            throw new IllegalArgumentException("Invalid height for AdSize: " + i3);
        }
    }

    public final int a() {
        return this.l;
    }

    public final int a(Context context) {
        switch (this.l) {
            case -4:
            case -3:
                return -1;
            case -2:
                return aot.b(context.getResources().getDisplayMetrics());
            default:
                ape.a();
                return mh.a(context, this.l);
        }
    }

    public final int b() {
        return this.k;
    }

    public final int b(Context context) {
        switch (this.k) {
            case -4:
            case -3:
                return -1;
            case -1:
                return aot.a(context.getResources().getDisplayMetrics());
            default:
                ape.a();
                return mh.a(context, this.k);
        }
    }

    public final boolean c() {
        return this.k == -3 && this.l == -4;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof d)) {
            return false;
        }
        d dVar = (d) obj;
        return this.k == dVar.k && this.l == dVar.l && this.m.equals(dVar.m);
    }

    public final int hashCode() {
        return this.m.hashCode();
    }

    public final String toString() {
        return this.m;
    }
}
