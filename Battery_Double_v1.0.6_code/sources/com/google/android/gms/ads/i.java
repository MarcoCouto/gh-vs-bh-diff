package com.google.android.gms.ads;

import android.os.RemoteException;
import com.google.android.gms.common.internal.aa;
import com.google.android.gms.internal.ads.aqs;
import com.google.android.gms.internal.ads.aqv;
import com.google.android.gms.internal.ads.arq;
import com.google.android.gms.internal.ads.cm;
import com.google.android.gms.internal.ads.ms;

@cm
public final class i {

    /* renamed from: a reason: collision with root package name */
    private final Object f1944a = new Object();

    /* renamed from: b reason: collision with root package name */
    private aqs f1945b;
    private a c;

    public static abstract class a {
        public void a() {
        }

        public void a(boolean z) {
        }

        public void b() {
        }

        public void c() {
        }

        public void d() {
        }
    }

    public final aqs a() {
        aqs aqs;
        synchronized (this.f1944a) {
            aqs = this.f1945b;
        }
        return aqs;
    }

    public final void a(a aVar) {
        aa.a(aVar, (Object) "VideoLifecycleCallbacks may not be null.");
        synchronized (this.f1944a) {
            this.c = aVar;
            if (this.f1945b != null) {
                try {
                    this.f1945b.a((aqv) new arq(aVar));
                } catch (RemoteException e) {
                    ms.b("Unable to call setVideoLifecycleCallbacks on video controller.", e);
                }
                return;
            }
            return;
        }
    }

    public final void a(aqs aqs) {
        synchronized (this.f1944a) {
            this.f1945b = aqs;
            if (this.c != null) {
                a(this.c);
            }
        }
    }
}
