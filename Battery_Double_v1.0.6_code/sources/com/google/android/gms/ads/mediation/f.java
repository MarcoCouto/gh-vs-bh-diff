package com.google.android.gms.ads.mediation;

import android.os.Bundle;
import android.view.View;
import com.google.android.gms.ads.i;
import com.google.android.gms.internal.ads.cm;
import java.util.Map;

@cm
public class f {

    /* renamed from: a reason: collision with root package name */
    protected boolean f2125a;

    /* renamed from: b reason: collision with root package name */
    protected boolean f2126b;
    protected Bundle c = new Bundle();
    protected View d;
    private View e;
    private i f;
    private boolean g;

    @Deprecated
    public void a(View view) {
    }

    public void a(View view, Map<String, View> map, Map<String, View> map2) {
    }

    public final void a(i iVar) {
        this.f = iVar;
    }

    public final void a(boolean z) {
        this.f2125a = z;
    }

    public final boolean a() {
        return this.f2125a;
    }

    public void b(View view) {
    }

    public final void b(boolean z) {
        this.f2126b = z;
    }

    public final boolean b() {
        return this.f2126b;
    }

    public final Bundle c() {
        return this.c;
    }

    public void c(View view) {
    }

    public View d() {
        return this.d;
    }

    public void e() {
    }

    public final View f() {
        return this.e;
    }

    public final i g() {
        return this.f;
    }

    public boolean h() {
        return this.g;
    }
}
