package com.google.android.gms.ads.mediation.customevent;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import com.google.android.gms.ads.mediation.MediationBannerAdapter;
import com.google.android.gms.ads.mediation.MediationInterstitialAdapter;
import com.google.android.gms.ads.mediation.MediationNativeAdapter;
import com.google.android.gms.ads.mediation.d;
import com.google.android.gms.ads.mediation.e;
import com.google.android.gms.ads.mediation.i;
import com.google.android.gms.ads.reward.mediation.MediationRewardedVideoAdAdapter;
import com.google.android.gms.common.annotation.KeepName;
import com.google.android.gms.internal.ads.ms;

@KeepName
public final class CustomEventAdapter implements MediationBannerAdapter, MediationInterstitialAdapter, MediationNativeAdapter {

    /* renamed from: a reason: collision with root package name */
    private View f2116a;

    /* renamed from: b reason: collision with root package name */
    private CustomEventBanner f2117b;
    private CustomEventInterstitial c;
    private CustomEventNative d;

    static final class a implements b {

        /* renamed from: a reason: collision with root package name */
        private final CustomEventAdapter f2118a;

        /* renamed from: b reason: collision with root package name */
        private final com.google.android.gms.ads.mediation.c f2119b;

        public a(CustomEventAdapter customEventAdapter, com.google.android.gms.ads.mediation.c cVar) {
            this.f2118a = customEventAdapter;
            this.f2119b = cVar;
        }
    }

    class b implements d {

        /* renamed from: a reason: collision with root package name */
        private final CustomEventAdapter f2120a;

        /* renamed from: b reason: collision with root package name */
        private final d f2121b;

        public b(CustomEventAdapter customEventAdapter, d dVar) {
            this.f2120a = customEventAdapter;
            this.f2121b = dVar;
        }
    }

    static class c implements e {

        /* renamed from: a reason: collision with root package name */
        private final CustomEventAdapter f2122a;

        /* renamed from: b reason: collision with root package name */
        private final e f2123b;

        public c(CustomEventAdapter customEventAdapter, e eVar) {
            this.f2122a = customEventAdapter;
            this.f2123b = eVar;
        }
    }

    private static <T> T a(String str) {
        try {
            return Class.forName(str).newInstance();
        } catch (Throwable th) {
            String message = th.getMessage();
            ms.e(new StringBuilder(String.valueOf(str).length() + 46 + String.valueOf(message).length()).append("Could not instantiate custom event adapter: ").append(str).append(". ").append(message).toString());
            return null;
        }
    }

    public final View getBannerView() {
        return this.f2116a;
    }

    public final void onDestroy() {
        if (this.f2117b != null) {
            this.f2117b.a();
        }
        if (this.c != null) {
            this.c.a();
        }
        if (this.d != null) {
            this.d.a();
        }
    }

    public final void onPause() {
        if (this.f2117b != null) {
            this.f2117b.b();
        }
        if (this.c != null) {
            this.c.b();
        }
        if (this.d != null) {
            this.d.b();
        }
    }

    public final void onResume() {
        if (this.f2117b != null) {
            this.f2117b.c();
        }
        if (this.c != null) {
            this.c.c();
        }
        if (this.d != null) {
            this.d.c();
        }
    }

    public final void requestBannerAd(Context context, com.google.android.gms.ads.mediation.c cVar, Bundle bundle, com.google.android.gms.ads.d dVar, com.google.android.gms.ads.mediation.a aVar, Bundle bundle2) {
        this.f2117b = (CustomEventBanner) a(bundle.getString("class_name"));
        if (this.f2117b == null) {
            cVar.a(this, 0);
            return;
        }
        this.f2117b.requestBannerAd(context, new a(this, cVar), bundle.getString(MediationRewardedVideoAdAdapter.CUSTOM_EVENT_SERVER_PARAMETER_FIELD), dVar, aVar, bundle2 == null ? null : bundle2.getBundle(bundle.getString("class_name")));
    }

    public final void requestInterstitialAd(Context context, d dVar, Bundle bundle, com.google.android.gms.ads.mediation.a aVar, Bundle bundle2) {
        this.c = (CustomEventInterstitial) a(bundle.getString("class_name"));
        if (this.c == null) {
            dVar.a(this, 0);
            return;
        }
        this.c.requestInterstitialAd(context, new b(this, dVar), bundle.getString(MediationRewardedVideoAdAdapter.CUSTOM_EVENT_SERVER_PARAMETER_FIELD), aVar, bundle2 == null ? null : bundle2.getBundle(bundle.getString("class_name")));
    }

    public final void requestNativeAd(Context context, e eVar, Bundle bundle, i iVar, Bundle bundle2) {
        this.d = (CustomEventNative) a(bundle.getString("class_name"));
        if (this.d == null) {
            eVar.a((MediationNativeAdapter) this, 0);
            return;
        }
        this.d.requestNativeAd(context, new c(this, eVar), bundle.getString(MediationRewardedVideoAdAdapter.CUSTOM_EVENT_SERVER_PARAMETER_FIELD), iVar, bundle2 == null ? null : bundle2.getBundle(bundle.getString("class_name")));
    }

    public final void showInterstitial() {
        this.c.showInterstitial();
    }
}
