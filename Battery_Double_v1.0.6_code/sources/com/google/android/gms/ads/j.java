package com.google.android.gms.ads;

import com.google.android.gms.internal.ads.arr;
import com.google.android.gms.internal.ads.cm;

@cm
public final class j {

    /* renamed from: a reason: collision with root package name */
    private final boolean f2113a;

    /* renamed from: b reason: collision with root package name */
    private final boolean f2114b;
    private final boolean c;

    public j(arr arr) {
        this.f2113a = arr.f2860a;
        this.f2114b = arr.f2861b;
        this.c = arr.c;
    }

    public final boolean a() {
        return this.f2113a;
    }

    public final boolean b() {
        return this.f2114b;
    }

    public final boolean c() {
        return this.c;
    }
}
