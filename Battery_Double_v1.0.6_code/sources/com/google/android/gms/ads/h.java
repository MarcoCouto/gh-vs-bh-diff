package com.google.android.gms.ads;

import com.mansoon.BatteryDouble.R;

public final class h {

    public static final class a {
        public static final int[] AdsAttrs = {R.attr.adSize, R.attr.adSizes, R.attr.adUnitId};
        public static final int AdsAttrs_adSize = 0;
        public static final int AdsAttrs_adSizes = 1;
        public static final int AdsAttrs_adUnitId = 2;
    }
}
