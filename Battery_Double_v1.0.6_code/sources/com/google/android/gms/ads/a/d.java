package com.google.android.gms.ads.a;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import com.google.android.gms.ads.a;
import com.google.android.gms.ads.f;
import com.google.android.gms.ads.i;
import com.google.android.gms.ads.j;
import com.google.android.gms.internal.ads.arc;
import com.google.android.gms.internal.ads.ms;

public final class d extends ViewGroup {

    /* renamed from: a reason: collision with root package name */
    private final arc f1918a;

    public final a getAdListener() {
        return this.f1918a.b();
    }

    public final com.google.android.gms.ads.d getAdSize() {
        return this.f1918a.c();
    }

    public final com.google.android.gms.ads.d[] getAdSizes() {
        return this.f1918a.d();
    }

    public final String getAdUnitId() {
        return this.f1918a.e();
    }

    public final a getAppEventListener() {
        return this.f1918a.f();
    }

    public final String getMediationAdapterClassName() {
        return this.f1918a.j();
    }

    public final c getOnCustomRenderedAdLoadedListener() {
        return this.f1918a.g();
    }

    public final i getVideoController() {
        return this.f1918a.k();
    }

    public final j getVideoOptions() {
        return this.f1918a.m();
    }

    /* access modifiers changed from: protected */
    public final void onLayout(boolean z, int i, int i2, int i3, int i4) {
        View childAt = getChildAt(0);
        if (childAt != null && childAt.getVisibility() != 8) {
            int measuredWidth = childAt.getMeasuredWidth();
            int measuredHeight = childAt.getMeasuredHeight();
            int i5 = ((i3 - i) - measuredWidth) / 2;
            int i6 = ((i4 - i2) - measuredHeight) / 2;
            childAt.layout(i5, i6, measuredWidth + i5, measuredHeight + i6);
        }
    }

    /* access modifiers changed from: protected */
    public final void onMeasure(int i, int i2) {
        com.google.android.gms.ads.d dVar;
        int i3;
        int i4 = 0;
        View childAt = getChildAt(0);
        if (childAt == null || childAt.getVisibility() == 8) {
            com.google.android.gms.ads.d dVar2 = null;
            try {
                dVar = getAdSize();
            } catch (NullPointerException e) {
                ms.b("Unable to retrieve ad size.", e);
                dVar = dVar2;
            }
            if (dVar != null) {
                Context context = getContext();
                i3 = dVar.b(context);
                i4 = dVar.a(context);
            } else {
                i3 = 0;
            }
        } else {
            measureChild(childAt, i, i2);
            i3 = childAt.getMeasuredWidth();
            i4 = childAt.getMeasuredHeight();
        }
        setMeasuredDimension(View.resolveSize(Math.max(i3, getSuggestedMinimumWidth()), i), View.resolveSize(Math.max(i4, getSuggestedMinimumHeight()), i2));
    }

    public final void setAdListener(a aVar) {
        this.f1918a.a(aVar);
    }

    public final void setAdSizes(com.google.android.gms.ads.d... dVarArr) {
        if (dVarArr == null || dVarArr.length <= 0) {
            throw new IllegalArgumentException("The supported ad sizes must contain at least one valid ad size.");
        }
        this.f1918a.b(dVarArr);
    }

    public final void setAdUnitId(String str) {
        this.f1918a.a(str);
    }

    public final void setAppEventListener(a aVar) {
        this.f1918a.a(aVar);
    }

    public final void setCorrelator(f fVar) {
        this.f1918a.a(fVar);
    }

    public final void setManualImpressionsEnabled(boolean z) {
        this.f1918a.a(z);
    }

    public final void setOnCustomRenderedAdLoadedListener(c cVar) {
        this.f1918a.a(cVar);
    }

    public final void setVideoOptions(j jVar) {
        this.f1918a.a(jVar);
    }
}
