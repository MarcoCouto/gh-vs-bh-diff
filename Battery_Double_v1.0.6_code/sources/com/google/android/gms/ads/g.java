package com.google.android.gms.ads;

import android.content.Context;
import android.os.Bundle;
import com.google.android.gms.ads.reward.b;
import com.google.android.gms.ads.reward.c;
import com.google.android.gms.common.internal.aa;
import com.google.android.gms.internal.ads.aoj;
import com.google.android.gms.internal.ads.arf;

public final class g {

    /* renamed from: a reason: collision with root package name */
    private final arf f1943a;

    public g(Context context) {
        this.f1943a = new arf(context);
        aa.a(context, (Object) "Context cannot be null");
    }

    public final void a(a aVar) {
        this.f1943a.a(aVar);
        if (aVar != null && (aVar instanceof aoj)) {
            this.f1943a.a((aoj) aVar);
        } else if (aVar == null) {
            this.f1943a.a((aoj) null);
        }
    }

    public final void a(c cVar) {
        this.f1943a.a(cVar.a());
    }

    public final void a(b bVar) {
        this.f1943a.a(bVar);
    }

    public final void a(c cVar) {
        this.f1943a.a(cVar);
    }

    public final void a(String str) {
        this.f1943a.a(str);
    }

    public final void a(boolean z) {
        this.f1943a.a(true);
    }

    public final boolean a() {
        return this.f1943a.a();
    }

    public final void b() {
        this.f1943a.c();
    }

    public final void b(boolean z) {
        this.f1943a.b(z);
    }

    public final Bundle c() {
        return this.f1943a.b();
    }
}
