package com.google.android.gms.ads.b;

import android.os.RemoteException;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.FrameLayout;
import com.google.android.gms.b.a;
import com.google.android.gms.b.b;
import com.google.android.gms.internal.ads.avb;
import com.google.android.gms.internal.ads.ms;

public final class l extends FrameLayout {

    /* renamed from: a reason: collision with root package name */
    private final FrameLayout f1933a;

    /* renamed from: b reason: collision with root package name */
    private final avb f1934b;

    private final View a(String str) {
        try {
            a a2 = this.f1934b.a(str);
            if (a2 != null) {
                return (View) b.a(a2);
            }
        } catch (RemoteException e) {
            ms.b("Unable to call getAssetView on delegate", e);
        }
        return null;
    }

    private final void a(String str, View view) {
        try {
            this.f1934b.a(str, b.a(view));
        } catch (RemoteException e) {
            ms.b("Unable to call setAssetView on delegate", e);
        }
    }

    public final void addView(View view, int i, LayoutParams layoutParams) {
        super.addView(view, i, layoutParams);
        super.bringChildToFront(this.f1933a);
    }

    public final void bringChildToFront(View view) {
        super.bringChildToFront(view);
        if (this.f1933a != view) {
            super.bringChildToFront(this.f1933a);
        }
    }

    public final a getAdChoicesView() {
        View a2 = a("3011");
        if (a2 instanceof a) {
            return (a) a2;
        }
        return null;
    }

    public final View getAdvertiserView() {
        return a("3005");
    }

    public final View getBodyView() {
        return a("3004");
    }

    public final View getCallToActionView() {
        return a("3002");
    }

    public final View getHeadlineView() {
        return a("3001");
    }

    public final View getIconView() {
        return a("3003");
    }

    public final View getImageView() {
        return a("3008");
    }

    public final b getMediaView() {
        View a2 = a("3010");
        if (a2 instanceof b) {
            return (b) a2;
        }
        if (a2 != null) {
            ms.b("View is not an instance of MediaView");
        }
        return null;
    }

    public final View getPriceView() {
        return a("3007");
    }

    public final View getStarRatingView() {
        return a("3009");
    }

    public final View getStoreView() {
        return a("3006");
    }

    public final void onVisibilityChanged(View view, int i) {
        super.onVisibilityChanged(view, i);
        if (this.f1934b != null) {
            try {
                this.f1934b.a(b.a(view), i);
            } catch (RemoteException e) {
                ms.b("Unable to call onVisibilityChanged on delegate", e);
            }
        }
    }

    public final void removeAllViews() {
        super.removeAllViews();
        super.addView(this.f1933a);
    }

    public final void removeView(View view) {
        if (this.f1933a != view) {
            super.removeView(view);
        }
    }

    public final void setAdChoicesView(a aVar) {
        a("3011", aVar);
    }

    public final void setAdvertiserView(View view) {
        a("3005", view);
    }

    public final void setBodyView(View view) {
        a("3004", view);
    }

    public final void setCallToActionView(View view) {
        a("3002", view);
    }

    public final void setClickConfirmingView(View view) {
        try {
            this.f1934b.b(b.a(view));
        } catch (RemoteException e) {
            ms.b("Unable to call setClickConfirmingView on delegate", e);
        }
    }

    public final void setHeadlineView(View view) {
        a("3001", view);
    }

    public final void setIconView(View view) {
        a("3003", view);
    }

    public final void setImageView(View view) {
        a("3008", view);
    }

    public final void setMediaView(b bVar) {
        a("3010", bVar);
    }

    public final void setNativeAd(k kVar) {
        try {
            this.f1934b.a((a) kVar.k());
        } catch (RemoteException e) {
            ms.b("Unable to call setNativeAd on delegate", e);
        }
    }

    public final void setPriceView(View view) {
        a("3007", view);
    }

    public final void setStarRatingView(View view) {
        a("3009", view);
    }

    public final void setStoreView(View view) {
        a("3006", view);
    }
}
