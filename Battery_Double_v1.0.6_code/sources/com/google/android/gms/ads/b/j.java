package com.google.android.gms.ads.b;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.a.a;
import com.google.android.gms.common.internal.a.c;
import com.google.android.gms.internal.ads.aqe;
import com.google.android.gms.internal.ads.aqf;
import com.google.android.gms.internal.ads.cm;

@cm
public final class j extends a {
    public static final Creator<j> CREATOR = new n();

    /* renamed from: a reason: collision with root package name */
    private final boolean f1931a;

    /* renamed from: b reason: collision with root package name */
    private final aqe f1932b;

    j(boolean z, IBinder iBinder) {
        this.f1931a = z;
        this.f1932b = iBinder != null ? aqf.a(iBinder) : null;
    }

    public final boolean a() {
        return this.f1931a;
    }

    public final aqe b() {
        return this.f1932b;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = c.a(parcel);
        c.a(parcel, 1, a());
        c.a(parcel, 2, this.f1932b == null ? null : this.f1932b.asBinder(), false);
        c.a(parcel, a2);
    }
}
