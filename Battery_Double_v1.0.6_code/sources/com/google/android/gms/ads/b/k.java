package com.google.android.gms.ads.b;

import com.google.android.gms.ads.b.c.b;
import com.google.android.gms.ads.i;
import java.util.List;

public abstract class k {

    public interface a {
        void a(k kVar);
    }

    public abstract String a();

    public abstract List<b> b();

    public abstract String c();

    public abstract b d();

    public abstract String e();

    public abstract String f();

    public abstract Double g();

    public abstract String h();

    public abstract String i();

    public abstract i j();

    /* access modifiers changed from: protected */
    public abstract Object k();

    public abstract Object l();
}
