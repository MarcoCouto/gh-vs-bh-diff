package com.google.android.gms.ads.b;

import com.google.android.gms.ads.j;
import com.google.android.gms.internal.ads.cm;

@cm
public final class d {

    /* renamed from: a reason: collision with root package name */
    private final boolean f1923a;

    /* renamed from: b reason: collision with root package name */
    private final int f1924b;
    private final boolean c;
    private final int d;
    private final j e;

    public static final class a {
        /* access modifiers changed from: private */

        /* renamed from: a reason: collision with root package name */
        public boolean f1925a = false;
        /* access modifiers changed from: private */

        /* renamed from: b reason: collision with root package name */
        public int f1926b = -1;
        /* access modifiers changed from: private */
        public boolean c = false;
        /* access modifiers changed from: private */
        public j d;
        /* access modifiers changed from: private */
        public int e = 1;

        public final a a(int i) {
            this.f1926b = i;
            return this;
        }

        public final a a(j jVar) {
            this.d = jVar;
            return this;
        }

        public final a a(boolean z) {
            this.f1925a = z;
            return this;
        }

        public final d a() {
            return new d(this);
        }

        public final a b(int i) {
            this.e = i;
            return this;
        }

        public final a b(boolean z) {
            this.c = z;
            return this;
        }
    }

    private d(a aVar) {
        this.f1923a = aVar.f1925a;
        this.f1924b = aVar.f1926b;
        this.c = aVar.c;
        this.d = aVar.e;
        this.e = aVar.d;
    }

    public final boolean a() {
        return this.f1923a;
    }

    public final int b() {
        return this.f1924b;
    }

    public final boolean c() {
        return this.c;
    }

    public final int d() {
        return this.d;
    }

    public final j e() {
        return this.e;
    }
}
