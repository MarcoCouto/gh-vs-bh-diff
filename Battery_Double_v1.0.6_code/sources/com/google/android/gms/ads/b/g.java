package com.google.android.gms.ads.b;

import com.google.android.gms.ads.b.c.b;
import com.google.android.gms.ads.i;
import java.util.List;

public abstract class g extends c {

    public interface a {
        void a(g gVar);
    }

    public abstract CharSequence b();

    public abstract List<b> c();

    public abstract CharSequence d();

    public abstract b e();

    public abstract CharSequence f();

    public abstract Double g();

    public abstract CharSequence h();

    public abstract CharSequence i();

    public abstract i j();
}
