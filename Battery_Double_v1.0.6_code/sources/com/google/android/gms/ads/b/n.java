package com.google.android.gms.ads.b;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.a.b;

public final class n implements Creator<j> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b2 = b.b(parcel);
        boolean z = false;
        IBinder iBinder = null;
        while (parcel.dataPosition() < b2) {
            int a2 = b.a(parcel);
            switch (b.a(a2)) {
                case 1:
                    z = b.c(parcel, a2);
                    break;
                case 2:
                    iBinder = b.l(parcel, a2);
                    break;
                default:
                    b.b(parcel, a2);
                    break;
            }
        }
        b.r(parcel, b2);
        return new j(z, iBinder);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new j[i];
    }
}
