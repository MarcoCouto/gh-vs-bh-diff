package com.google.android.gms.ads.b;

import android.os.RemoteException;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.FrameLayout;
import com.google.android.gms.b.a;
import com.google.android.gms.b.b;
import com.google.android.gms.internal.ads.avb;
import com.google.android.gms.internal.ads.ms;

public class e extends FrameLayout {

    /* renamed from: a reason: collision with root package name */
    private final FrameLayout f1927a;

    /* renamed from: b reason: collision with root package name */
    private final avb f1928b;

    /* access modifiers changed from: protected */
    public final View a(String str) {
        try {
            a a2 = this.f1928b.a(str);
            if (a2 != null) {
                return (View) b.a(a2);
            }
        } catch (RemoteException e) {
            ms.b("Unable to call getAssetView on delegate", e);
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public final void a(String str, View view) {
        try {
            this.f1928b.a(str, b.a(view));
        } catch (RemoteException e) {
            ms.b("Unable to call setAssetView on delegate", e);
        }
    }

    public void addView(View view, int i, LayoutParams layoutParams) {
        super.addView(view, i, layoutParams);
        super.bringChildToFront(this.f1927a);
    }

    public void bringChildToFront(View view) {
        super.bringChildToFront(view);
        if (this.f1927a != view) {
            super.bringChildToFront(this.f1927a);
        }
    }

    public a getAdChoicesView() {
        View a2 = a("1098");
        if (a2 instanceof a) {
            return (a) a2;
        }
        return null;
    }

    public void onVisibilityChanged(View view, int i) {
        super.onVisibilityChanged(view, i);
        if (this.f1928b != null) {
            try {
                this.f1928b.a(b.a(view), i);
            } catch (RemoteException e) {
                ms.b("Unable to call onVisibilityChanged on delegate", e);
            }
        }
    }

    public void removeAllViews() {
        super.removeAllViews();
        super.addView(this.f1927a);
    }

    public void removeView(View view) {
        if (this.f1927a != view) {
            super.removeView(view);
        }
    }

    public void setAdChoicesView(a aVar) {
        a("1098", aVar);
    }

    public void setNativeAd(c cVar) {
        try {
            this.f1928b.a((a) cVar.a());
        } catch (RemoteException e) {
            ms.b("Unable to call setNativeAd on delegate", e);
        }
    }
}
