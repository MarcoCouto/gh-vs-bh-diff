package com.google.android.gms.ads.b;

import android.os.RemoteException;
import android.view.View;
import com.google.android.gms.b.a;
import com.google.android.gms.internal.ads.avg;
import com.google.android.gms.internal.ads.ms;
import java.lang.ref.WeakReference;
import java.util.WeakHashMap;

public final class f {

    /* renamed from: a reason: collision with root package name */
    public static WeakHashMap<View, f> f1929a = new WeakHashMap<>();

    /* renamed from: b reason: collision with root package name */
    private avg f1930b;
    private WeakReference<View> c;

    private final void a(a aVar) {
        View view = this.c != null ? (View) this.c.get() : null;
        if (view == null) {
            ms.e("NativeAdViewHolder.setNativeAd containerView doesn't exist, returning");
            return;
        }
        if (!f1929a.containsKey(view)) {
            f1929a.put(view, this);
        }
        if (this.f1930b != null) {
            try {
                this.f1930b.a(aVar);
            } catch (RemoteException e) {
                ms.b("Unable to call setNativeAd on delegate", e);
            }
        }
    }

    public final void a(c cVar) {
        a((a) cVar.a());
    }

    public final void a(k kVar) {
        a((a) kVar.k());
    }
}
