package com.google.android.gms.ads;

import android.location.Location;
import android.os.Bundle;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.mediation.b;
import com.google.android.gms.internal.ads.ara;
import com.google.android.gms.internal.ads.arb;
import java.util.Date;

public final class c {

    /* renamed from: a reason: collision with root package name */
    private final ara f1935a;

    public static final class a {
        /* access modifiers changed from: private */

        /* renamed from: a reason: collision with root package name */
        public final arb f1936a = new arb();

        public a() {
            this.f1936a.b("B3EEABB8EE11C2BE770B684D95219ECB");
        }

        public final a a(int i) {
            this.f1936a.a(i);
            return this;
        }

        public final a a(Location location) {
            this.f1936a.a(location);
            return this;
        }

        public final a a(Class<? extends b> cls, Bundle bundle) {
            this.f1936a.a(cls, bundle);
            if (cls.equals(AdMobAdapter.class) && bundle.getBoolean("_emulatorLiveAds")) {
                this.f1936a.c("B3EEABB8EE11C2BE770B684D95219ECB");
            }
            return this;
        }

        public final a a(String str) {
            this.f1936a.a(str);
            return this;
        }

        public final a a(Date date) {
            this.f1936a.a(date);
            return this;
        }

        public final a a(boolean z) {
            this.f1936a.a(z);
            return this;
        }

        public final c a() {
            return new c(this);
        }

        public final a b(String str) {
            this.f1936a.b(str);
            return this;
        }

        public final a b(boolean z) {
            this.f1936a.b(z);
            return this;
        }
    }

    private c(a aVar) {
        this.f1935a = new ara(aVar.f1936a);
    }

    public final ara a() {
        return this.f1935a;
    }
}
