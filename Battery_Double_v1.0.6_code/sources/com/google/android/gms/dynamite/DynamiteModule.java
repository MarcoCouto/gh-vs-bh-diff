package com.google.android.gms.dynamite;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.common.h;
import com.google.android.gms.common.internal.aa;
import com.google.android.gms.common.util.DynamiteApi;
import com.google.android.gms.dynamite.a.C0064a;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

public final class DynamiteModule {

    /* renamed from: a reason: collision with root package name */
    public static final b f2374a = new d();

    /* renamed from: b reason: collision with root package name */
    public static final b f2375b = new e();
    public static final b c = new f();
    public static final b d = new g();
    public static final b e = new h();
    public static final b f = new i();
    private static Boolean g;
    private static a h;
    private static b i;
    private static String j;
    private static final ThreadLocal<c> k = new ThreadLocal<>();
    private static final a l = new c();
    private final Context m;

    @DynamiteApi
    public static class DynamiteLoaderClassLoader {
        public static ClassLoader sClassLoader;
    }

    public static class a extends Exception {
        private a(String str) {
            super(str);
        }

        /* synthetic */ a(String str, c cVar) {
            this(str);
        }

        private a(String str, Throwable th) {
            super(str, th);
        }

        /* synthetic */ a(String str, Throwable th, c cVar) {
            this(str, th);
        }
    }

    public interface b {

        public interface a {
            int a(Context context, String str);

            int a(Context context, String str, boolean z) throws a;
        }

        /* renamed from: com.google.android.gms.dynamite.DynamiteModule$b$b reason: collision with other inner class name */
        public static class C0063b {

            /* renamed from: a reason: collision with root package name */
            public int f2376a = 0;

            /* renamed from: b reason: collision with root package name */
            public int f2377b = 0;
            public int c = 0;
        }

        C0063b a(Context context, String str, a aVar) throws a;
    }

    private static class c {

        /* renamed from: a reason: collision with root package name */
        public Cursor f2378a;

        private c() {
        }

        /* synthetic */ c(c cVar) {
            this();
        }
    }

    private static class d implements a {

        /* renamed from: a reason: collision with root package name */
        private final int f2379a;

        /* renamed from: b reason: collision with root package name */
        private final int f2380b = 0;

        public d(int i, int i2) {
            this.f2379a = i;
        }

        public final int a(Context context, String str) {
            return this.f2379a;
        }

        public final int a(Context context, String str, boolean z) {
            return 0;
        }
    }

    private DynamiteModule(Context context) {
        this.m = (Context) aa.a(context);
    }

    public static int a(Context context, String str) {
        try {
            Class loadClass = context.getApplicationContext().getClassLoader().loadClass(new StringBuilder(String.valueOf(str).length() + 61).append("com.google.android.gms.dynamite.descriptors.").append(str).append(".ModuleDescriptor").toString());
            Field declaredField = loadClass.getDeclaredField("MODULE_ID");
            Field declaredField2 = loadClass.getDeclaredField("MODULE_VERSION");
            if (declaredField.get(null).equals(str)) {
                return declaredField2.getInt(null);
            }
            String valueOf = String.valueOf(declaredField.get(null));
            Log.e("DynamiteModule", new StringBuilder(String.valueOf(valueOf).length() + 51 + String.valueOf(str).length()).append("Module descriptor id '").append(valueOf).append("' didn't match expected id '").append(str).append("'").toString());
            return 0;
        } catch (ClassNotFoundException e2) {
            Log.w("DynamiteModule", new StringBuilder(String.valueOf(str).length() + 45).append("Local module descriptor class for ").append(str).append(" not found.").toString());
            return 0;
        } catch (Exception e3) {
            String str2 = "DynamiteModule";
            String str3 = "Failed to load module descriptor class: ";
            String valueOf2 = String.valueOf(e3.getMessage());
            Log.e(str2, valueOf2.length() != 0 ? str3.concat(valueOf2) : new String(str3));
            return 0;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x003b A[SYNTHETIC, Splitter:B:21:0x003b] */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x00ed  */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:35:0x0071=Splitter:B:35:0x0071, B:25:0x0043=Splitter:B:25:0x0043} */
    public static int a(Context context, String str, boolean z) {
        synchronized (DynamiteModule.class) {
            Boolean bool = g;
            if (bool == null) {
                try {
                    Class loadClass = context.getApplicationContext().getClassLoader().loadClass(DynamiteLoaderClassLoader.class.getName());
                    Field declaredField = loadClass.getDeclaredField("sClassLoader");
                    synchronized (loadClass) {
                        ClassLoader classLoader = (ClassLoader) declaredField.get(null);
                        if (classLoader != null) {
                            if (classLoader == ClassLoader.getSystemClassLoader()) {
                                bool = Boolean.FALSE;
                            } else {
                                try {
                                    a(classLoader);
                                } catch (a e2) {
                                }
                                bool = Boolean.TRUE;
                            }
                        } else if ("com.google.android.gms".equals(context.getApplicationContext().getPackageName())) {
                            declaredField.set(null, ClassLoader.getSystemClassLoader());
                            bool = Boolean.FALSE;
                        } else {
                            try {
                                int d2 = d(context, str, z);
                                if (j == null || j.isEmpty()) {
                                    return d2;
                                }
                                j jVar = new j(j, ClassLoader.getSystemClassLoader());
                                a((ClassLoader) jVar);
                                declaredField.set(null, jVar);
                                g = Boolean.TRUE;
                                return d2;
                            } catch (a e3) {
                                declaredField.set(null, ClassLoader.getSystemClassLoader());
                                bool = Boolean.FALSE;
                                g = bool;
                                if (bool.booleanValue()) {
                                }
                            }
                        }
                    }
                } catch (ClassNotFoundException | IllegalAccessException | NoSuchFieldException e4) {
                    String str2 = "DynamiteModule";
                    String valueOf = String.valueOf(e4);
                    Log.w(str2, new StringBuilder(String.valueOf(valueOf).length() + 30).append("Failed to load module via V2: ").append(valueOf).toString());
                    bool = Boolean.FALSE;
                    g = bool;
                    if (bool.booleanValue()) {
                        return c(context, str, z);
                    }
                    try {
                        return d(context, str, z);
                    } catch (a e5) {
                        String str3 = "DynamiteModule";
                        String str4 = "Failed to retrieve remote module version: ";
                        String valueOf2 = String.valueOf(e5.getMessage());
                        Log.w(str3, valueOf2.length() != 0 ? str4.concat(valueOf2) : new String(str4));
                        return 0;
                    }
                }
            }
        }
    }

    private static Context a(Context context, String str, int i2, Cursor cursor, b bVar) {
        try {
            return (Context) com.google.android.gms.b.b.a(bVar.a(com.google.android.gms.b.b.a(context), str, i2, com.google.android.gms.b.b.a(cursor)));
        } catch (Exception e2) {
            String str2 = "DynamiteModule";
            String str3 = "Failed to load DynamiteLoader: ";
            String valueOf = String.valueOf(e2.toString());
            Log.e(str2, valueOf.length() != 0 ? str3.concat(valueOf) : new String(str3));
            return null;
        }
    }

    public static Uri a(String str, boolean z) {
        String str2 = z ? "api_force_staging" : "api";
        return Uri.parse(new StringBuilder(String.valueOf(str2).length() + 42 + String.valueOf(str).length()).append("content://com.google.android.gms.chimera/").append(str2).append("/").append(str).toString());
    }

    public static DynamiteModule a(Context context, b bVar, String str) throws a {
        C0063b a2;
        c cVar = (c) k.get();
        c cVar2 = new c(null);
        k.set(cVar2);
        try {
            a2 = bVar.a(context, str, l);
            Log.i("DynamiteModule", new StringBuilder(String.valueOf(str).length() + 68 + String.valueOf(str).length()).append("Considering local module ").append(str).append(":").append(a2.f2376a).append(" and remote module ").append(str).append(":").append(a2.f2377b).toString());
            if (a2.c == 0 || ((a2.c == -1 && a2.f2376a == 0) || (a2.c == 1 && a2.f2377b == 0))) {
                throw new a("No acceptable module found. Local version is " + a2.f2376a + " and remote version is " + a2.f2377b + ".", (c) null);
            } else if (a2.c == -1) {
                DynamiteModule c2 = c(context, str);
                if (cVar2.f2378a != null) {
                    cVar2.f2378a.close();
                }
                k.set(cVar);
                return c2;
            } else if (a2.c == 1) {
                DynamiteModule a3 = a(context, str, a2.f2377b);
                if (cVar2.f2378a != null) {
                    cVar2.f2378a.close();
                }
                k.set(cVar);
                return a3;
            } else {
                throw new a("VersionPolicy returned invalid code:" + a2.c, (c) null);
            }
        } catch (a e2) {
            String str2 = "DynamiteModule";
            String str3 = "Failed to load remote module: ";
            String valueOf = String.valueOf(e2.getMessage());
            Log.w(str2, valueOf.length() != 0 ? str3.concat(valueOf) : new String(str3));
            if (a2.f2376a == 0 || bVar.a(context, str, new d(a2.f2376a, 0)).c != -1) {
                throw new a("Remote load failed. No local fallback found.", e2, null);
            }
            DynamiteModule c3 = c(context, str);
            if (cVar2.f2378a != null) {
                cVar2.f2378a.close();
            }
            k.set(cVar);
            return c3;
        } catch (Throwable th) {
            if (cVar2.f2378a != null) {
                cVar2.f2378a.close();
            }
            k.set(cVar);
            throw th;
        }
    }

    private static DynamiteModule a(Context context, String str, int i2) throws a {
        Boolean bool;
        synchronized (DynamiteModule.class) {
            bool = g;
        }
        if (bool != null) {
            return bool.booleanValue() ? c(context, str, i2) : b(context, str, i2);
        }
        throw new a("Failed to determine which loading route to use.", (c) null);
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    private static a a(Context context) {
        synchronized (DynamiteModule.class) {
            if (h != null) {
                a aVar = h;
                return aVar;
            } else if (h.b().a(context) != 0) {
                return null;
            } else {
                try {
                    a a2 = C0064a.a((IBinder) context.createPackageContext("com.google.android.gms", 3).getClassLoader().loadClass("com.google.android.gms.chimera.container.DynamiteLoaderImpl").newInstance());
                    if (a2 != null) {
                        h = a2;
                        return a2;
                    }
                } catch (Exception e2) {
                    String str = "DynamiteModule";
                    String str2 = "Failed to load IDynamiteLoader from GmsCore: ";
                    String valueOf = String.valueOf(e2.getMessage());
                    Log.e(str, valueOf.length() != 0 ? str2.concat(valueOf) : new String(str2));
                    return null;
                }
            }
        }
    }

    private static void a(ClassLoader classLoader) throws a {
        try {
            i = com.google.android.gms.dynamite.b.a.a((IBinder) classLoader.loadClass("com.google.android.gms.dynamiteloader.DynamiteLoaderV2").getConstructor(new Class[0]).newInstance(new Object[0]));
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException e2) {
            throw new a("Failed to instantiate dynamite loader", e2, null);
        }
    }

    public static int b(Context context, String str) {
        return a(context, str, false);
    }

    public static Cursor b(Context context, String str, boolean z) {
        return context.getContentResolver().query(a(str, z), null, null, null, null);
    }

    private static DynamiteModule b(Context context, String str, int i2) throws a {
        Log.i("DynamiteModule", new StringBuilder(String.valueOf(str).length() + 51).append("Selected remote version of ").append(str).append(", version >= ").append(i2).toString());
        a a2 = a(context);
        if (a2 == null) {
            throw new a("Failed to create IDynamiteLoader.", (c) null);
        }
        try {
            com.google.android.gms.b.a a3 = a2.a(com.google.android.gms.b.b.a(context), str, i2);
            if (com.google.android.gms.b.b.a(a3) != null) {
                return new DynamiteModule((Context) com.google.android.gms.b.b.a(a3));
            }
            throw new a("Failed to load remote module.", (c) null);
        } catch (RemoteException e2) {
            throw new a("Failed to load remote module.", e2, null);
        }
    }

    private static int c(Context context, String str, boolean z) {
        a a2 = a(context);
        if (a2 == null) {
            return 0;
        }
        try {
            return a2.a(com.google.android.gms.b.b.a(context), str, z);
        } catch (RemoteException e2) {
            String str2 = "DynamiteModule";
            String str3 = "Failed to retrieve remote module version: ";
            String valueOf = String.valueOf(e2.getMessage());
            Log.w(str2, valueOf.length() != 0 ? str3.concat(valueOf) : new String(str3));
            return 0;
        }
    }

    private static DynamiteModule c(Context context, String str) {
        String str2 = "DynamiteModule";
        String str3 = "Selected local version of ";
        String valueOf = String.valueOf(str);
        Log.i(str2, valueOf.length() != 0 ? str3.concat(valueOf) : new String(str3));
        return new DynamiteModule(context.getApplicationContext());
    }

    private static DynamiteModule c(Context context, String str, int i2) throws a {
        b bVar;
        Log.i("DynamiteModule", new StringBuilder(String.valueOf(str).length() + 51).append("Selected remote version of ").append(str).append(", version >= ").append(i2).toString());
        synchronized (DynamiteModule.class) {
            bVar = i;
        }
        if (bVar == null) {
            throw new a("DynamiteLoaderV2 was not cached.", (c) null);
        }
        c cVar = (c) k.get();
        if (cVar == null || cVar.f2378a == null) {
            throw new a("No result cursor", (c) null);
        }
        Context a2 = a(context.getApplicationContext(), str, i2, cVar.f2378a, bVar);
        if (a2 != null) {
            return new DynamiteModule(a2);
        }
        throw new a("Failed to get module context", (c) null);
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0026  */
    private static int d(Context context, String str, boolean z) throws a {
        Cursor cursor;
        try {
            cursor = b(context, str, z);
            if (cursor != null) {
                try {
                    if (cursor.moveToFirst()) {
                        int i2 = cursor.getInt(0);
                        if (i2 > 0) {
                            synchronized (DynamiteModule.class) {
                                j = cursor.getString(2);
                            }
                            c cVar = (c) k.get();
                            if (cVar != null && cVar.f2378a == null) {
                                cVar.f2378a = cursor;
                                cursor = null;
                            }
                        }
                        if (cursor != null) {
                            cursor.close();
                        }
                        return i2;
                    }
                } catch (Exception e2) {
                    e = e2;
                }
            }
            Log.w("DynamiteModule", "Failed to retrieve remote module version.");
            throw new a("Failed to connect to dynamite module ContentResolver.", (c) null);
        } catch (Exception e3) {
            e = e3;
            cursor = null;
        } catch (Throwable th) {
            th = th;
            cursor = null;
            if (cursor != null) {
            }
            throw th;
        }
        try {
            if (e instanceof a) {
                throw e;
            }
            throw new a("V2 version check failed", e, null);
        } catch (Throwable th2) {
            th = th2;
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    public final Context a() {
        return this.m;
    }

    public final IBinder a(String str) throws a {
        try {
            return (IBinder) this.m.getClassLoader().loadClass(str).newInstance();
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e2) {
            String str2 = "Failed to instantiate module class: ";
            String valueOf = String.valueOf(str);
            throw new a(valueOf.length() != 0 ? str2.concat(valueOf) : new String(str2), e2, null);
        }
    }
}
