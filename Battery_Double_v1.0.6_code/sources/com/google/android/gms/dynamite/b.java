package com.google.android.gms.dynamite;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.b.a.C0046a;
import com.google.android.gms.internal.d.c;

public interface b extends IInterface {

    public static abstract class a extends com.google.android.gms.internal.d.b implements b {

        /* renamed from: com.google.android.gms.dynamite.b$a$a reason: collision with other inner class name */
        public static class C0066a extends com.google.android.gms.internal.d.a implements b {
            C0066a(IBinder iBinder) {
                super(iBinder, "com.google.android.gms.dynamite.IDynamiteLoaderV2");
            }

            public com.google.android.gms.b.a a(com.google.android.gms.b.a aVar, String str, int i, com.google.android.gms.b.a aVar2) throws RemoteException {
                Parcel d = d();
                c.a(d, (IInterface) aVar);
                d.writeString(str);
                d.writeInt(i);
                c.a(d, (IInterface) aVar2);
                Parcel a2 = a(2, d);
                com.google.android.gms.b.a a3 = C0046a.a(a2.readStrongBinder());
                a2.recycle();
                return a3;
            }

            public com.google.android.gms.b.a a(com.google.android.gms.b.a aVar, String str, byte[] bArr) throws RemoteException {
                Parcel d = d();
                c.a(d, (IInterface) aVar);
                d.writeString(str);
                d.writeByteArray(bArr);
                Parcel a2 = a(1, d);
                com.google.android.gms.b.a a3 = C0046a.a(a2.readStrongBinder());
                a2.recycle();
                return a3;
            }
        }

        public static b a(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.dynamite.IDynamiteLoaderV2");
            return queryLocalInterface instanceof b ? (b) queryLocalInterface : new C0066a(iBinder);
        }

        /* access modifiers changed from: protected */
        public boolean a(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
            switch (i) {
                case 1:
                    com.google.android.gms.b.a a2 = a(C0046a.a(parcel.readStrongBinder()), parcel.readString(), parcel.createByteArray());
                    parcel2.writeNoException();
                    c.a(parcel2, (IInterface) a2);
                    break;
                case 2:
                    com.google.android.gms.b.a a3 = a(C0046a.a(parcel.readStrongBinder()), parcel.readString(), parcel.readInt(), C0046a.a(parcel.readStrongBinder()));
                    parcel2.writeNoException();
                    c.a(parcel2, (IInterface) a3);
                    break;
                default:
                    return false;
            }
            return true;
        }
    }

    com.google.android.gms.b.a a(com.google.android.gms.b.a aVar, String str, int i, com.google.android.gms.b.a aVar2) throws RemoteException;

    com.google.android.gms.b.a a(com.google.android.gms.b.a aVar, String str, byte[] bArr) throws RemoteException;
}
