package com.google.android.gms.dynamite;

import android.content.Context;
import com.google.android.gms.dynamite.DynamiteModule.b;
import com.google.android.gms.dynamite.DynamiteModule.b.C0063b;
import com.google.android.gms.dynamite.DynamiteModule.b.a;

final class d implements b {
    d() {
    }

    public final C0063b a(Context context, String str, a aVar) throws DynamiteModule.a {
        C0063b bVar = new C0063b();
        bVar.f2377b = aVar.a(context, str, true);
        if (bVar.f2377b != 0) {
            bVar.c = 1;
        } else {
            bVar.f2376a = aVar.a(context, str);
            if (bVar.f2376a != 0) {
                bVar.c = -1;
            }
        }
        return bVar;
    }
}
