package com.google.android.gms.dynamite;

import android.content.Context;
import com.google.android.gms.dynamite.DynamiteModule.b;
import com.google.android.gms.dynamite.DynamiteModule.b.C0063b;
import com.google.android.gms.dynamite.DynamiteModule.b.a;

final class h implements b {
    h() {
    }

    public final C0063b a(Context context, String str, a aVar) throws DynamiteModule.a {
        C0063b bVar = new C0063b();
        bVar.f2376a = aVar.a(context, str);
        bVar.f2377b = aVar.a(context, str, true);
        if (bVar.f2376a == 0 && bVar.f2377b == 0) {
            bVar.c = 0;
        } else if (bVar.f2377b >= bVar.f2376a) {
            bVar.c = 1;
        } else {
            bVar.c = -1;
        }
        return bVar;
    }
}
