package com.google.android.gms.dynamite;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.b.a.C0046a;
import com.google.android.gms.internal.d.b;
import com.google.android.gms.internal.d.c;

public interface a extends IInterface {

    /* renamed from: com.google.android.gms.dynamite.a$a reason: collision with other inner class name */
    public static abstract class C0064a extends b implements a {

        /* renamed from: com.google.android.gms.dynamite.a$a$a reason: collision with other inner class name */
        public static class C0065a extends com.google.android.gms.internal.d.a implements a {
            C0065a(IBinder iBinder) {
                super(iBinder, "com.google.android.gms.dynamite.IDynamiteLoader");
            }

            public int a(com.google.android.gms.b.a aVar, String str) throws RemoteException {
                Parcel d = d();
                c.a(d, (IInterface) aVar);
                d.writeString(str);
                Parcel a2 = a(1, d);
                int readInt = a2.readInt();
                a2.recycle();
                return readInt;
            }

            public int a(com.google.android.gms.b.a aVar, String str, boolean z) throws RemoteException {
                Parcel d = d();
                c.a(d, (IInterface) aVar);
                d.writeString(str);
                c.a(d, z);
                Parcel a2 = a(3, d);
                int readInt = a2.readInt();
                a2.recycle();
                return readInt;
            }

            public com.google.android.gms.b.a a(com.google.android.gms.b.a aVar, String str, int i) throws RemoteException {
                Parcel d = d();
                c.a(d, (IInterface) aVar);
                d.writeString(str);
                d.writeInt(i);
                Parcel a2 = a(2, d);
                com.google.android.gms.b.a a3 = C0046a.a(a2.readStrongBinder());
                a2.recycle();
                return a3;
            }
        }

        public static a a(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.dynamite.IDynamiteLoader");
            return queryLocalInterface instanceof a ? (a) queryLocalInterface : new C0065a(iBinder);
        }

        /* access modifiers changed from: protected */
        public boolean a(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
            switch (i) {
                case 1:
                    int a2 = a(C0046a.a(parcel.readStrongBinder()), parcel.readString());
                    parcel2.writeNoException();
                    parcel2.writeInt(a2);
                    break;
                case 2:
                    com.google.android.gms.b.a a3 = a(C0046a.a(parcel.readStrongBinder()), parcel.readString(), parcel.readInt());
                    parcel2.writeNoException();
                    c.a(parcel2, (IInterface) a3);
                    break;
                case 3:
                    int a4 = a(C0046a.a(parcel.readStrongBinder()), parcel.readString(), c.a(parcel));
                    parcel2.writeNoException();
                    parcel2.writeInt(a4);
                    break;
                default:
                    return false;
            }
            return true;
        }
    }

    int a(com.google.android.gms.b.a aVar, String str) throws RemoteException;

    int a(com.google.android.gms.b.a aVar, String str, boolean z) throws RemoteException;

    com.google.android.gms.b.a a(com.google.android.gms.b.a aVar, String str, int i) throws RemoteException;
}
