package com.google.android.gms.a;

public final class a {

    /* renamed from: com.google.android.gms.a.a$a reason: collision with other inner class name */
    public static final class C0044a {
        public static final int common_full_open_on_phone = 2131230830;
        public static final int common_google_signin_btn_icon_dark = 2131230831;
        public static final int common_google_signin_btn_icon_dark_focused = 2131230832;
        public static final int common_google_signin_btn_icon_dark_normal = 2131230833;
        public static final int common_google_signin_btn_icon_dark_normal_background = 2131230834;
        public static final int common_google_signin_btn_icon_disabled = 2131230835;
        public static final int common_google_signin_btn_icon_light = 2131230836;
        public static final int common_google_signin_btn_icon_light_focused = 2131230837;
        public static final int common_google_signin_btn_icon_light_normal = 2131230838;
        public static final int common_google_signin_btn_icon_light_normal_background = 2131230839;
        public static final int common_google_signin_btn_text_dark = 2131230840;
        public static final int common_google_signin_btn_text_dark_focused = 2131230841;
        public static final int common_google_signin_btn_text_dark_normal = 2131230842;
        public static final int common_google_signin_btn_text_dark_normal_background = 2131230843;
        public static final int common_google_signin_btn_text_disabled = 2131230844;
        public static final int common_google_signin_btn_text_light = 2131230845;
        public static final int common_google_signin_btn_text_light_focused = 2131230846;
        public static final int common_google_signin_btn_text_light_normal = 2131230847;
        public static final int common_google_signin_btn_text_light_normal_background = 2131230848;
        public static final int googleg_disabled_color_18 = 2131230858;
        public static final int googleg_standard_color_18 = 2131230859;
    }

    public static final class b {
        public static final int common_google_play_services_enable_button = 2131624009;
        public static final int common_google_play_services_enable_text = 2131624010;
        public static final int common_google_play_services_enable_title = 2131624011;
        public static final int common_google_play_services_install_button = 2131624012;
        public static final int common_google_play_services_install_text = 2131624013;
        public static final int common_google_play_services_install_title = 2131624014;
        public static final int common_google_play_services_notification_channel_name = 2131624015;
        public static final int common_google_play_services_notification_ticker = 2131624016;
        public static final int common_google_play_services_unsupported_text = 2131624018;
        public static final int common_google_play_services_update_button = 2131624019;
        public static final int common_google_play_services_update_text = 2131624020;
        public static final int common_google_play_services_update_title = 2131624021;
        public static final int common_google_play_services_updating_text = 2131624022;
        public static final int common_google_play_services_wear_update_text = 2131624023;
        public static final int common_open_on_phone = 2131624024;
        public static final int common_signin_button_text = 2131624025;
        public static final int common_signin_button_text_long = 2131624026;
    }
}
