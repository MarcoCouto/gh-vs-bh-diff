package com.google.android.gms.common;

import android.content.Context;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.b.b;
import com.google.android.gms.common.internal.aa;
import com.google.android.gms.common.internal.t;
import com.google.android.gms.common.internal.w;
import com.google.android.gms.dynamite.DynamiteModule;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;

final class i {

    /* renamed from: a reason: collision with root package name */
    private static volatile w f2250a;

    /* renamed from: b reason: collision with root package name */
    private static final Object f2251b = new Object();
    private static Context c;

    static abstract class a extends com.google.android.gms.common.internal.t.a {

        /* renamed from: a reason: collision with root package name */
        private int f2252a;

        protected a(byte[] bArr) {
            aa.b(bArr.length == 25);
            this.f2252a = Arrays.hashCode(bArr);
        }

        protected static byte[] a(String str) {
            try {
                return str.getBytes("ISO-8859-1");
            } catch (UnsupportedEncodingException e) {
                throw new AssertionError(e);
            }
        }

        /* access modifiers changed from: 0000 */
        public abstract byte[] a();

        public com.google.android.gms.b.a b() {
            return b.a(a());
        }

        public int c() {
            return hashCode();
        }

        public boolean equals(Object obj) {
            if (obj == null || !(obj instanceof t)) {
                return false;
            }
            try {
                t tVar = (t) obj;
                if (tVar.c() != hashCode()) {
                    return false;
                }
                com.google.android.gms.b.a b2 = tVar.b();
                if (b2 == null) {
                    return false;
                }
                return Arrays.equals(a(), (byte[]) b.a(b2));
            } catch (RemoteException e) {
                Log.e("GoogleCertificates", "Failed to get Google certificates from remote", e);
                return false;
            }
        }

        public int hashCode() {
            return this.f2252a;
        }
    }

    static x a(String str, a aVar, boolean z) {
        boolean z2 = true;
        try {
            a();
            aa.a(c);
            try {
                if (f2250a.a(new j(str, aVar, z), b.a(c.getPackageManager()))) {
                    return x.a();
                }
                if (z || !a(str, aVar, true).f2342a) {
                    z2 = false;
                }
                return x.a(str, aVar, z, z2);
            } catch (RemoteException e) {
                Log.e("GoogleCertificates", "Failed to get Google certificates from remote", e);
                return x.a("module call", e);
            }
        } catch (com.google.android.gms.dynamite.DynamiteModule.a e2) {
            return x.a("module init", e2);
        }
    }

    private static void a() throws com.google.android.gms.dynamite.DynamiteModule.a {
        if (f2250a == null) {
            aa.a(c);
            synchronized (f2251b) {
                if (f2250a == null) {
                    f2250a = com.google.android.gms.common.internal.w.a.a(DynamiteModule.a(c, DynamiteModule.d, "com.google.android.gms.googlecertificates").a("com.google.android.gms.common.GoogleCertificatesImpl"));
                }
            }
        }
    }

    static synchronized void a(Context context) {
        synchronized (i.class) {
            if (c != null) {
                Log.w("GoogleCertificates", "GoogleCertificates has been initialized already");
            } else if (context != null) {
                c = context.getApplicationContext();
            }
        }
    }
}
