package com.google.android.gms.common;

import com.google.android.gms.common.util.a;
import com.google.android.gms.common.util.k;

final class z extends x {

    /* renamed from: b reason: collision with root package name */
    private final String f2343b;
    private final a c;
    private final boolean d;
    private final boolean e;

    private z(String str, a aVar, boolean z, boolean z2) {
        super(false, null, null);
        this.f2343b = str;
        this.c = aVar;
        this.d = z;
        this.e = z2;
    }

    /* access modifiers changed from: 0000 */
    public final String b() {
        String str = this.e ? "debug cert rejected" : "not whitelisted";
        String str2 = this.f2343b;
        String a2 = k.a(a.a("SHA-1").digest(this.c.a()));
        return new StringBuilder(String.valueOf(str).length() + 44 + String.valueOf(str2).length() + String.valueOf(a2).length()).append(str).append(": pkg=").append(str2).append(", sha1=").append(a2).append(", atk=").append(this.d).append(", ver=12451009.false").toString();
    }
}
