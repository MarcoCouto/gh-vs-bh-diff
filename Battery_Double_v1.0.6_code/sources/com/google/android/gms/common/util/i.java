package com.google.android.gms.common.util;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import com.google.android.gms.common.n;

public final class i {

    /* renamed from: a reason: collision with root package name */
    private static Boolean f2333a;

    /* renamed from: b reason: collision with root package name */
    private static Boolean f2334b;
    private static Boolean c;

    public static boolean a() {
        return n.sIsTestMode ? n.sTestIsUserBuild : "user".equals(Build.TYPE);
    }

    @TargetApi(20)
    public static boolean a(Context context) {
        if (f2333a == null) {
            f2333a = Boolean.valueOf(n.h() && context.getPackageManager().hasSystemFeature("android.hardware.type.watch"));
        }
        return f2333a.booleanValue();
    }

    @TargetApi(24)
    public static boolean b(Context context) {
        return (!n.k() || c(context)) && a(context);
    }

    @TargetApi(21)
    public static boolean c(Context context) {
        if (f2334b == null) {
            f2334b = Boolean.valueOf(n.i() && context.getPackageManager().hasSystemFeature(n.FEATURE_SIDEWINDER));
        }
        return f2334b.booleanValue();
    }

    public static boolean d(Context context) {
        if (c == null) {
            c = Boolean.valueOf(context.getPackageManager().hasSystemFeature("android.hardware.type.iot") || context.getPackageManager().hasSystemFeature("android.hardware.type.embedded"));
        }
        return c.booleanValue();
    }
}
