package com.google.android.gms.common.util;

public class k {

    /* renamed from: a reason: collision with root package name */
    private static final char[] f2336a = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

    /* renamed from: b reason: collision with root package name */
    private static final char[] f2337b = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

    public static String a(byte[] bArr) {
        char[] cArr = new char[(bArr.length << 1)];
        int i = 0;
        for (byte b2 : bArr) {
            byte b3 = b2 & 255;
            int i2 = i + 1;
            cArr[i] = f2337b[b3 >>> 4];
            i = i2 + 1;
            cArr[i2] = f2337b[b3 & 15];
        }
        return new String(cArr);
    }
}
