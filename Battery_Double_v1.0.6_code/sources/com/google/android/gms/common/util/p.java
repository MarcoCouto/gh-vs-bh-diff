package com.google.android.gms.common.util;

import android.os.Process;
import android.os.StrictMode;
import android.os.StrictMode.ThreadPolicy;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class p {

    /* renamed from: a reason: collision with root package name */
    private static String f2338a = null;

    /* renamed from: b reason: collision with root package name */
    private static int f2339b = 0;

    private static BufferedReader a(String str) throws IOException {
        ThreadPolicy allowThreadDiskReads = StrictMode.allowThreadDiskReads();
        try {
            return new BufferedReader(new FileReader(str));
        } finally {
            StrictMode.setThreadPolicy(allowThreadDiskReads);
        }
    }

    public static String a() {
        if (f2338a == null) {
            f2338a = a(b());
        }
        return f2338a;
    }

    /* JADX WARNING: type inference failed for: r1v1, types: [java.io.Closeable] */
    /* JADX WARNING: type inference failed for: r1v3 */
    /* JADX WARNING: type inference failed for: r1v4, types: [java.io.Closeable] */
    /* JADX WARNING: type inference failed for: r1v6 */
    /* JADX WARNING: type inference failed for: r1v13 */
    /* JADX WARNING: type inference failed for: r1v14 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 2 */
    private static String a(int i) {
        ? r1;
        Throwable th;
        ? r12;
        String str = 0;
        if (i > 0) {
            try {
                BufferedReader a2 = a("/proc/" + i + "/cmdline");
                try {
                    String trim = a2.readLine().trim();
                    l.a(a2);
                    str = trim;
                } catch (IOException e) {
                    r12 = a2;
                    l.a(r12);
                    return str;
                } catch (Throwable th2) {
                    th = th2;
                    r1 = a2;
                    l.a(r1);
                    throw th;
                }
            } catch (IOException e2) {
                r12 = str;
            } catch (Throwable th3) {
                Throwable th4 = th3;
                r1 = str;
                th = th4;
                l.a(r1);
                throw th;
            }
        }
        return str;
    }

    private static int b() {
        if (f2339b == 0) {
            f2339b = Process.myPid();
        }
        return f2339b;
    }
}
