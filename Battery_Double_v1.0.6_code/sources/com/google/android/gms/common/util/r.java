package com.google.android.gms.common.util;

import java.util.regex.Pattern;

public class r {

    /* renamed from: a reason: collision with root package name */
    private static final Pattern f2340a = Pattern.compile("\\$\\{(.*?)\\}");

    public static String a(String str) {
        return str == null ? "" : str;
    }

    public static boolean b(String str) {
        return str == null || str.trim().isEmpty();
    }
}
