package com.google.android.gms.common.util.a;

import android.os.Process;

final class b implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final Runnable f2328a;

    /* renamed from: b reason: collision with root package name */
    private final int f2329b;

    public b(Runnable runnable, int i) {
        this.f2328a = runnable;
        this.f2329b = i;
    }

    public final void run() {
        Process.setThreadPriority(this.f2329b);
        this.f2328a.run();
    }
}
