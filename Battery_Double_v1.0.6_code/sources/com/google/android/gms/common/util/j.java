package com.google.android.gms.common.util;

import java.util.regex.Pattern;

public final class j {

    /* renamed from: a reason: collision with root package name */
    private static Pattern f2335a = null;

    public static int a(int i) {
        if (i == -1) {
            return -1;
        }
        return i / 1000;
    }
}
