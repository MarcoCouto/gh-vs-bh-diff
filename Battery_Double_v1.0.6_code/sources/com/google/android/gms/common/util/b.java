package com.google.android.gms.common.util;

import com.google.android.gms.common.internal.z;
import java.lang.reflect.Array;
import java.util.Arrays;

public final class b {
    public static <T> int a(T[] tArr, T t) {
        int length = tArr != null ? tArr.length : 0;
        for (int i = 0; i < length; i++) {
            if (z.a(tArr[i], t)) {
                return i;
            }
        }
        return -1;
    }

    public static <T> T[] a(T[] tArr, int i) {
        if (tArr == null) {
            return null;
        }
        return i != tArr.length ? Arrays.copyOf(tArr, i) : tArr;
    }

    public static <T> T[] a(T[] tArr, T... tArr2) {
        int i;
        int i2;
        int i3;
        int i4 = 0;
        if (tArr == null) {
            return null;
        }
        if (tArr2 == null || tArr2.length == 0) {
            return Arrays.copyOf(tArr, tArr.length);
        }
        Object[] objArr = (Object[]) Array.newInstance(tArr2.getClass().getComponentType(), tArr.length);
        if (tArr2.length == 1) {
            int length = tArr.length;
            int i5 = 0;
            i = 0;
            while (i5 < length) {
                T t = tArr[i5];
                if (!z.a(tArr2[0], t)) {
                    i3 = i + 1;
                    objArr[i] = t;
                } else {
                    i3 = i;
                }
                i5++;
                i = i3;
            }
        } else {
            int length2 = tArr.length;
            int i6 = 0;
            while (i4 < length2) {
                T t2 = tArr[i4];
                if (!b(tArr2, t2)) {
                    i2 = i + 1;
                    objArr[i] = t2;
                } else {
                    i2 = i;
                }
                i4++;
                i6 = i2;
            }
        }
        return a((T[]) objArr, i);
    }

    public static <T> boolean b(T[] tArr, T t) {
        return a(tArr, t) >= 0;
    }
}
