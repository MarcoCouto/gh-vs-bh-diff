package com.google.android.gms.common.util.a;

import com.google.android.gms.common.internal.aa;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

public class a implements ThreadFactory {

    /* renamed from: a reason: collision with root package name */
    private final String f2326a;

    /* renamed from: b reason: collision with root package name */
    private final int f2327b;
    private final ThreadFactory c;

    public a(String str) {
        this(str, 0);
    }

    public a(String str, int i) {
        this.c = Executors.defaultThreadFactory();
        this.f2326a = (String) aa.a(str, (Object) "Name must not be null");
        this.f2327b = i;
    }

    public Thread newThread(Runnable runnable) {
        Thread newThread = this.c.newThread(new b(runnable, this.f2327b));
        newThread.setName(this.f2326a);
        return newThread;
    }
}
