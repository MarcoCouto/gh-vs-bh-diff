package com.google.android.gms.common.util;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.os.Build;
import android.os.Debug;
import android.os.DropBoxManager;
import android.provider.Settings.Secure;
import android.text.TextUtils;
import android.util.Log;
import com.google.android.gms.common.b.c;
import com.google.android.gms.common.internal.aa;
import com.google.android.gms.measurement.AppMeasurement;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedList;

public final class g {

    /* renamed from: a reason: collision with root package name */
    private static final String[] f2330a = {"android.", "com.android.", "dalvik.", "java.", "javax."};

    /* renamed from: b reason: collision with root package name */
    private static DropBoxManager f2331b = null;
    private static boolean c = false;
    private static boolean d;
    private static boolean e;
    private static int f = -1;
    private static int g = 0;
    private static int h = 0;

    /* JADX WARNING: Code restructure failed: missing block: B:75:0x0194, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x0194 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:42:0x0144] */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x0197 A[SYNTHETIC, Splitter:B:77:0x0197] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:59:0x0170=Splitter:B:59:0x0170, B:79:0x019a=Splitter:B:79:0x019a} */
    private static synchronized String a(Context context, String str, String str2, int i) {
        int i2;
        InputStreamReader inputStreamReader;
        synchronized (g.class) {
            StringBuilder sb = new StringBuilder(1024);
            sb.append("Process: ").append(r.a(str2)).append("\n");
            sb.append("Package: com.google.android.gms");
            int i3 = 12451009;
            String str3 = "12.4.51 (020308-{{cl}})";
            if (b()) {
                try {
                    PackageInfo b2 = c.b(context).b(context.getPackageName(), 0);
                    i3 = b2.versionCode;
                    if (b2.versionName != null) {
                        str3 = b2.versionName;
                    }
                } catch (Exception e2) {
                    Log.w("CrashUtils", "Error while trying to get the package information! Using static version.", e2);
                }
            }
            sb.append(" v").append(i3);
            if (!TextUtils.isEmpty(str3)) {
                if (str3.contains("(") && !str3.contains(")")) {
                    if (str3.endsWith("-")) {
                        str3 = String.valueOf(str3).concat("111111111");
                    }
                    str3 = String.valueOf(str3).concat(")");
                }
                sb.append(" (").append(str3).append(")");
            }
            sb.append("\n");
            sb.append("Build: ").append(Build.FINGERPRINT).append("\n");
            if (Debug.isDebuggerConnected()) {
                sb.append("Debugger: Connected\n");
            }
            if (i != 0) {
                sb.append("DD-EDD: ").append(i).append("\n");
            }
            sb.append("\n");
            if (!TextUtils.isEmpty(str)) {
                sb.append(str);
            }
            if (b()) {
                i2 = f >= 0 ? f : Secure.getInt(context.getContentResolver(), "logcat_for_system_app_crash", 0);
            } else {
                i2 = 0;
            }
            if (i2 > 0) {
                sb.append("\n");
                InputStreamReader inputStreamReader2 = null;
                try {
                    Process start = new ProcessBuilder(new String[]{"/system/bin/logcat", "-v", "time", "-b", "events", "-b", "system", "-b", "main", "-b", AppMeasurement.CRASH_ORIGIN, "-t", String.valueOf(i2)}).redirectErrorStream(true).start();
                    try {
                        start.getOutputStream().close();
                    } catch (IOException e3) {
                    } catch (Throwable th) {
                    }
                    try {
                        start.getErrorStream().close();
                    } catch (IOException e4) {
                    } catch (Throwable th2) {
                    }
                    inputStreamReader = new InputStreamReader(start.getInputStream());
                    try {
                        char[] cArr = new char[8192];
                        while (true) {
                            int read = inputStreamReader.read(cArr);
                            if (read > 0) {
                                sb.append(cArr, 0, read);
                            } else {
                                try {
                                    break;
                                } catch (IOException e5) {
                                }
                            }
                        }
                        inputStreamReader.close();
                    } catch (IOException e6) {
                        e = e6;
                        try {
                            Log.e("CrashUtils", "Error running logcat", e);
                            if (inputStreamReader != null) {
                                try {
                                    inputStreamReader.close();
                                } catch (IOException e7) {
                                }
                            }
                            String sb2 = sb.toString();
                            return sb2;
                        } catch (Throwable th3) {
                            th = th3;
                            inputStreamReader2 = inputStreamReader;
                            if (inputStreamReader2 != null) {
                            }
                            throw th;
                        }
                    }
                } catch (IOException e8) {
                    e = e8;
                    inputStreamReader = null;
                } catch (Throwable th22) {
                }
            }
            String sb22 = sb.toString();
        }
        return sb22;
    }

    private static synchronized Throwable a(Throwable th) {
        Throwable th2;
        synchronized (g.class) {
            LinkedList linkedList = new LinkedList();
            while (th != null) {
                linkedList.push(th);
                th = th.getCause();
            }
            Throwable th3 = null;
            boolean z = false;
            while (!linkedList.isEmpty()) {
                Throwable th4 = (Throwable) linkedList.pop();
                StackTraceElement[] stackTrace = th4.getStackTrace();
                ArrayList arrayList = new ArrayList();
                arrayList.add(new StackTraceElement(th4.getClass().getName(), "<filtered>", "<filtered>", 1));
                boolean z2 = z;
                for (StackTraceElement stackTraceElement : stackTrace) {
                    String className = stackTraceElement.getClassName();
                    String fileName = stackTraceElement.getFileName();
                    boolean z3 = !TextUtils.isEmpty(fileName) && fileName.startsWith(":com.google.android.gms");
                    z2 |= z3;
                    if (!z3 && !a(className)) {
                        stackTraceElement = new StackTraceElement("<filtered>", "<filtered>", "<filtered>", 1);
                    }
                    arrayList.add(stackTraceElement);
                }
                th3 = th3 == null ? new Throwable("<filtered>") : new Throwable("<filtered>", th3);
                th3.setStackTrace((StackTraceElement[]) arrayList.toArray(new StackTraceElement[0]));
                z = z2;
            }
            th2 = !z ? null : th3;
        }
        return th2;
    }

    private static boolean a() {
        if (c) {
            return d;
        }
        return false;
    }

    private static synchronized boolean a(Context context, String str, String str2, int i, Throwable th) {
        boolean z;
        synchronized (g.class) {
            aa.a(context);
            if (!a() || r.b(str)) {
                z = false;
            } else {
                int hashCode = str.hashCode();
                int hashCode2 = th == null ? h : th.hashCode();
                if (g == hashCode && h == hashCode2) {
                    z = false;
                } else {
                    g = hashCode;
                    h = hashCode2;
                    DropBoxManager dropBoxManager = f2331b != null ? f2331b : (DropBoxManager) context.getSystemService("dropbox");
                    if (dropBoxManager == null || !dropBoxManager.isTagEnabled("system_app_crash")) {
                        z = false;
                    } else {
                        dropBoxManager.addText("system_app_crash", a(context, str, str2, i));
                        z = true;
                    }
                }
            }
        }
        return z;
    }

    public static boolean a(Context context, Throwable th) {
        return a(context, th, 536870912);
    }

    public static boolean a(Context context, Throwable th, int i) {
        boolean z;
        try {
            aa.a(context);
            aa.a(th);
            if (!a()) {
                return false;
            }
            if (!b()) {
                th = a(th);
                if (th == null) {
                    return false;
                }
            }
            return a(context, Log.getStackTraceString(th), p.a(), i, th);
        } catch (Exception e2) {
            try {
                z = b();
            } catch (Exception e3) {
                Log.e("CrashUtils", "Error determining which process we're running in!", e3);
                z = false;
            }
            if (z) {
                throw e2;
            }
            Log.e("CrashUtils", "Error adding exception to DropBox!", e2);
            return false;
        }
    }

    public static boolean a(String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        for (String startsWith : f2330a) {
            if (str.startsWith(startsWith)) {
                return true;
            }
        }
        return false;
    }

    private static boolean b() {
        if (c) {
            return e;
        }
        return false;
    }
}
