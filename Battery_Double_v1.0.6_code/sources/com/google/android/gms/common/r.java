package com.google.android.gms.common;

import android.content.Intent;

public class r extends Exception {

    /* renamed from: a reason: collision with root package name */
    private final Intent f2321a;

    public r(String str, Intent intent) {
        super(str);
        this.f2321a = intent;
    }
}
