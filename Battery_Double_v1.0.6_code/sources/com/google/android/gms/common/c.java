package com.google.android.gms.common;

import android.app.PendingIntent;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.a.b;

public class c implements Creator<b> {
    /* renamed from: a */
    public b createFromParcel(Parcel parcel) {
        int b2 = b.b(parcel);
        PendingIntent pendingIntent = null;
        int i = 0;
        int i2 = 0;
        String str = null;
        while (parcel.dataPosition() < b2) {
            int a2 = b.a(parcel);
            switch (b.a(a2)) {
                case 1:
                    i2 = b.d(parcel, a2);
                    break;
                case 2:
                    i = b.d(parcel, a2);
                    break;
                case 3:
                    pendingIntent = (PendingIntent) b.a(parcel, a2, PendingIntent.CREATOR);
                    break;
                case 4:
                    str = b.k(parcel, a2);
                    break;
                default:
                    b.b(parcel, a2);
                    break;
            }
        }
        b.r(parcel, b2);
        return new b(i2, i, pendingIntent, str);
    }

    /* renamed from: a */
    public b[] newArray(int i) {
        return new b[i];
    }
}
