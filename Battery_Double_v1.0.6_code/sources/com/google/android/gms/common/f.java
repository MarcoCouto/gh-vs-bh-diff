package com.google.android.gms.common;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.a.b;

public class f implements Creator<e> {
    /* renamed from: a */
    public e createFromParcel(Parcel parcel) {
        int b2 = b.b(parcel);
        String str = null;
        int i = 0;
        long j = -1;
        while (parcel.dataPosition() < b2) {
            int a2 = b.a(parcel);
            switch (b.a(a2)) {
                case 1:
                    str = b.k(parcel, a2);
                    break;
                case 2:
                    i = b.d(parcel, a2);
                    break;
                case 3:
                    j = b.f(parcel, a2);
                    break;
                default:
                    b.b(parcel, a2);
                    break;
            }
        }
        b.r(parcel, b2);
        return new e(str, i, j);
    }

    /* renamed from: a */
    public e[] newArray(int i) {
        return new e[i];
    }
}
