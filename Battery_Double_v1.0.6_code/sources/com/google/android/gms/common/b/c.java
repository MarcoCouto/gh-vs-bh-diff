package com.google.android.gms.common.b;

import android.content.Context;

public class c {

    /* renamed from: b reason: collision with root package name */
    private static c f2239b = new c();

    /* renamed from: a reason: collision with root package name */
    private b f2240a = null;

    public static b b(Context context) {
        return f2239b.a(context);
    }

    public synchronized b a(Context context) {
        if (this.f2240a == null) {
            if (context.getApplicationContext() != null) {
                context = context.getApplicationContext();
            }
            this.f2240a = new b(context);
        }
        return this.f2240a;
    }
}
