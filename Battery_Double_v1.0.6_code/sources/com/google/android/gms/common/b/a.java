package com.google.android.gms.common.b;

import android.content.Context;
import com.google.android.gms.common.util.n;

public class a {

    /* renamed from: a reason: collision with root package name */
    private static Context f2236a;

    /* renamed from: b reason: collision with root package name */
    private static Boolean f2237b;

    public static synchronized boolean a(Context context) {
        boolean booleanValue;
        synchronized (a.class) {
            Context applicationContext = context.getApplicationContext();
            if (f2236a == null || f2237b == null || f2236a != applicationContext) {
                f2237b = null;
                if (n.l()) {
                    f2237b = Boolean.valueOf(applicationContext.getPackageManager().isInstantApp());
                } else {
                    try {
                        context.getClassLoader().loadClass("com.google.android.instantapps.supervisor.InstantAppsRuntime");
                        f2237b = Boolean.valueOf(true);
                    } catch (ClassNotFoundException e) {
                        f2237b = Boolean.valueOf(false);
                    }
                }
                f2236a = applicationContext;
                booleanValue = f2237b.booleanValue();
            } else {
                booleanValue = f2237b.booleanValue();
            }
        }
        return booleanValue;
    }
}
