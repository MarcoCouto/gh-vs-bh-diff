package com.google.android.gms.common.b;

import android.annotation.TargetApi;
import android.app.AppOpsManager;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Binder;
import android.os.Process;
import com.google.android.gms.common.util.n;

public class b {

    /* renamed from: a reason: collision with root package name */
    private final Context f2238a;

    public b(Context context) {
        this.f2238a = context;
    }

    public int a(String str) {
        return this.f2238a.checkCallingOrSelfPermission(str);
    }

    public int a(String str, String str2) {
        return this.f2238a.getPackageManager().checkPermission(str, str2);
    }

    public ApplicationInfo a(String str, int i) throws NameNotFoundException {
        return this.f2238a.getPackageManager().getApplicationInfo(str, i);
    }

    public boolean a() {
        if (Binder.getCallingUid() == Process.myUid()) {
            return a.a(this.f2238a);
        }
        if (n.l()) {
            String nameForUid = this.f2238a.getPackageManager().getNameForUid(Binder.getCallingUid());
            if (nameForUid != null) {
                return this.f2238a.getPackageManager().isInstantApp(nameForUid);
            }
        }
        return false;
    }

    @TargetApi(19)
    public boolean a(int i, String str) {
        if (n.g()) {
            try {
                ((AppOpsManager) this.f2238a.getSystemService("appops")).checkPackage(i, str);
                return true;
            } catch (SecurityException e) {
                return false;
            }
        } else {
            String[] packagesForUid = this.f2238a.getPackageManager().getPackagesForUid(i);
            if (str == null || packagesForUid == null) {
                return false;
            }
            for (String equals : packagesForUid) {
                if (str.equals(equals)) {
                    return true;
                }
            }
            return false;
        }
    }

    public String[] a(int i) {
        return this.f2238a.getPackageManager().getPackagesForUid(i);
    }

    public PackageInfo b(String str, int i) throws NameNotFoundException {
        return this.f2238a.getPackageManager().getPackageInfo(str, i);
    }

    public CharSequence b(String str) throws NameNotFoundException {
        return this.f2238a.getPackageManager().getApplicationLabel(this.f2238a.getPackageManager().getApplicationInfo(str, 0));
    }
}
