package com.google.android.gms.common;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import com.google.android.gms.common.internal.aa;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class a implements ServiceConnection {

    /* renamed from: a reason: collision with root package name */
    private boolean f2153a = false;

    /* renamed from: b reason: collision with root package name */
    private final BlockingQueue<IBinder> f2154b = new LinkedBlockingQueue();

    public IBinder a(long j, TimeUnit timeUnit) throws InterruptedException, TimeoutException {
        aa.c("BlockingServiceConnection.getServiceWithTimeout() called on main thread");
        if (this.f2153a) {
            throw new IllegalStateException("Cannot call get on this connection more than once");
        }
        this.f2153a = true;
        IBinder iBinder = (IBinder) this.f2154b.poll(j, timeUnit);
        if (iBinder != null) {
            return iBinder;
        }
        throw new TimeoutException("Timed out waiting for the service connection");
    }

    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        this.f2154b.add(iBinder);
    }

    public void onServiceDisconnected(ComponentName componentName) {
    }
}
