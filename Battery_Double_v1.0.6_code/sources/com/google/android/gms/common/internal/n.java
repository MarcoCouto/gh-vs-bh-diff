package com.google.android.gms.common.internal;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;

public abstract class n {

    /* renamed from: a reason: collision with root package name */
    private static final Object f2300a = new Object();

    /* renamed from: b reason: collision with root package name */
    private static n f2301b;

    protected static final class a {

        /* renamed from: a reason: collision with root package name */
        private final String f2302a;

        /* renamed from: b reason: collision with root package name */
        private final String f2303b;
        private final ComponentName c = null;
        private final int d;

        public a(String str, String str2, int i) {
            this.f2302a = aa.a(str);
            this.f2303b = aa.a(str2);
            this.d = i;
        }

        public final Intent a(Context context) {
            return this.f2302a != null ? new Intent(this.f2302a).setPackage(this.f2303b) : new Intent().setComponent(this.c);
        }

        public final String a() {
            return this.f2303b;
        }

        public final ComponentName b() {
            return this.c;
        }

        public final int c() {
            return this.d;
        }

        public final boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof a)) {
                return false;
            }
            a aVar = (a) obj;
            return z.a(this.f2302a, aVar.f2302a) && z.a(this.f2303b, aVar.f2303b) && z.a(this.c, aVar.c) && this.d == aVar.d;
        }

        public final int hashCode() {
            return z.a(this.f2302a, this.f2303b, this.c, Integer.valueOf(this.d));
        }

        public final String toString() {
            return this.f2302a == null ? this.c.flattenToString() : this.f2302a;
        }
    }

    public static n a(Context context) {
        synchronized (f2300a) {
            if (f2301b == null) {
                f2301b = new ao(context.getApplicationContext());
            }
        }
        return f2301b;
    }

    /* access modifiers changed from: protected */
    public abstract boolean a(a aVar, ServiceConnection serviceConnection, String str);

    public boolean a(String str, String str2, int i, ServiceConnection serviceConnection, String str3) {
        return a(new a(str, str2, i), serviceConnection, str3);
    }

    /* access modifiers changed from: protected */
    public abstract void b(a aVar, ServiceConnection serviceConnection, String str);

    public void b(String str, String str2, int i, ServiceConnection serviceConnection, String str3) {
        b(new a(str, str2, i), serviceConnection, str3);
    }
}
