package com.google.android.gms.common.internal;

import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.util.Log;
import com.google.android.gms.common.b.c;
import com.google.android.gms.common.n;

public class y {

    /* renamed from: a reason: collision with root package name */
    private static Object f2311a = new Object();

    /* renamed from: b reason: collision with root package name */
    private static boolean f2312b;
    private static String c;
    private static int d;

    public static String a(Context context) {
        c(context);
        return c;
    }

    public static int b(Context context) {
        c(context);
        return d;
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    private static void c(Context context) {
        synchronized (f2311a) {
            if (!f2312b) {
                f2312b = true;
                try {
                    Bundle bundle = c.b(context).a(context.getPackageName(), 128).metaData;
                    if (bundle != null) {
                        c = bundle.getString("com.google.app.id");
                        d = bundle.getInt(n.KEY_METADATA_GOOGLE_PLAY_SERVICES_VERSION);
                    }
                } catch (NameNotFoundException e) {
                    Log.wtf("MetadataValueReader", "This should never happen.", e);
                }
            }
        }
    }
}
