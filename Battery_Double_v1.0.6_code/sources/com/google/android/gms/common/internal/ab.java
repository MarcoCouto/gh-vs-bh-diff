package com.google.android.gms.common.internal;

import android.accounts.Account;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.internal.a.a;
import com.google.android.gms.common.internal.a.c;

public class ab extends a {
    public static final Creator<ab> CREATOR = new ac();

    /* renamed from: a reason: collision with root package name */
    private final int f2255a;

    /* renamed from: b reason: collision with root package name */
    private final Account f2256b;
    private final int c;
    private final GoogleSignInAccount d;

    ab(int i, Account account, int i2, GoogleSignInAccount googleSignInAccount) {
        this.f2255a = i;
        this.f2256b = account;
        this.c = i2;
        this.d = googleSignInAccount;
    }

    public ab(Account account, int i, GoogleSignInAccount googleSignInAccount) {
        this(2, account, i, googleSignInAccount);
    }

    public Account a() {
        return this.f2256b;
    }

    public int b() {
        return this.c;
    }

    public GoogleSignInAccount c() {
        return this.d;
    }

    public void writeToParcel(Parcel parcel, int i) {
        int a2 = c.a(parcel);
        c.a(parcel, 1, this.f2255a);
        c.a(parcel, 2, (Parcelable) a(), i, false);
        c.a(parcel, 3, b());
        c.a(parcel, 4, (Parcelable) c(), i, false);
        c.a(parcel, a2);
    }
}
