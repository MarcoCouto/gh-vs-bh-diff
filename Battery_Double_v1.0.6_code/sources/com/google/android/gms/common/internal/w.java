package com.google.android.gms.common.internal;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.google.android.gms.b.a.C0046a;
import com.google.android.gms.common.j;
import com.google.android.gms.internal.d.b;
import com.google.android.gms.internal.d.c;

public interface w extends IInterface {

    public static abstract class a extends b implements w {

        /* renamed from: com.google.android.gms.common.internal.w$a$a reason: collision with other inner class name */
        public static class C0061a extends com.google.android.gms.internal.d.a implements w {
            C0061a(IBinder iBinder) {
                super(iBinder, "com.google.android.gms.common.internal.IGoogleCertificatesApi");
            }

            public com.google.android.gms.b.a a() throws RemoteException {
                Parcel a2 = a(1, d());
                com.google.android.gms.b.a a3 = C0046a.a(a2.readStrongBinder());
                a2.recycle();
                return a3;
            }

            public boolean a(j jVar, com.google.android.gms.b.a aVar) throws RemoteException {
                Parcel d = d();
                c.a(d, (Parcelable) jVar);
                c.a(d, (IInterface) aVar);
                Parcel a2 = a(5, d);
                boolean a3 = c.a(a2);
                a2.recycle();
                return a3;
            }

            public boolean a(String str, com.google.android.gms.b.a aVar) throws RemoteException {
                Parcel d = d();
                d.writeString(str);
                c.a(d, (IInterface) aVar);
                Parcel a2 = a(3, d);
                boolean a3 = c.a(a2);
                a2.recycle();
                return a3;
            }

            public com.google.android.gms.b.a b() throws RemoteException {
                Parcel a2 = a(2, d());
                com.google.android.gms.b.a a3 = C0046a.a(a2.readStrongBinder());
                a2.recycle();
                return a3;
            }

            public boolean b(String str, com.google.android.gms.b.a aVar) throws RemoteException {
                Parcel d = d();
                d.writeString(str);
                c.a(d, (IInterface) aVar);
                Parcel a2 = a(4, d);
                boolean a3 = c.a(a2);
                a2.recycle();
                return a3;
            }
        }

        public static w a(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.common.internal.IGoogleCertificatesApi");
            return queryLocalInterface instanceof w ? (w) queryLocalInterface : new C0061a(iBinder);
        }

        /* access modifiers changed from: protected */
        public boolean a(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
            switch (i) {
                case 1:
                    com.google.android.gms.b.a a2 = a();
                    parcel2.writeNoException();
                    c.a(parcel2, (IInterface) a2);
                    break;
                case 2:
                    com.google.android.gms.b.a b2 = b();
                    parcel2.writeNoException();
                    c.a(parcel2, (IInterface) b2);
                    break;
                case 3:
                    boolean a3 = a(parcel.readString(), C0046a.a(parcel.readStrongBinder()));
                    parcel2.writeNoException();
                    c.a(parcel2, a3);
                    break;
                case 4:
                    boolean b3 = b(parcel.readString(), C0046a.a(parcel.readStrongBinder()));
                    parcel2.writeNoException();
                    c.a(parcel2, b3);
                    break;
                case 5:
                    boolean a4 = a((j) c.a(parcel, j.CREATOR), C0046a.a(parcel.readStrongBinder()));
                    parcel2.writeNoException();
                    c.a(parcel2, a4);
                    break;
                default:
                    return false;
            }
            return true;
        }
    }

    com.google.android.gms.b.a a() throws RemoteException;

    boolean a(j jVar, com.google.android.gms.b.a aVar) throws RemoteException;

    boolean a(String str, com.google.android.gms.b.a aVar) throws RemoteException;

    com.google.android.gms.b.a b() throws RemoteException;

    boolean b(String str, com.google.android.gms.b.a aVar) throws RemoteException;
}
