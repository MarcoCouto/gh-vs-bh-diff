package com.google.android.gms.common.internal;

import android.accounts.Account;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.Handler;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.text.TextUtils;
import android.util.Log;
import com.google.android.gms.common.api.Scope;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class e<T extends IInterface> {
    public static final String[] d = {"service_esmobile", "service_googleme"};
    private static final com.google.android.gms.common.e[] e = new com.google.android.gms.common.e[0];
    /* access modifiers changed from: private */
    public com.google.android.gms.common.b A;
    /* access modifiers changed from: private */
    public boolean B;
    private volatile h C;

    /* renamed from: a reason: collision with root package name */
    final Handler f2276a;

    /* renamed from: b reason: collision with root package name */
    protected d f2277b;
    protected AtomicInteger c;
    private int f;
    private long g;
    private long h;
    private int i;
    private long j;
    private p k;
    private final Context l;
    private final Looper m;
    private final n n;
    private final com.google.android.gms.common.h o;
    private final Object p;
    /* access modifiers changed from: private */
    public final Object q;
    /* access modifiers changed from: private */
    public v r;
    private T s;
    /* access modifiers changed from: private */
    public final ArrayList<c<?>> t;
    private f u;
    private int v;
    /* access modifiers changed from: private */
    public final a w;
    /* access modifiers changed from: private */
    public final b x;
    private final int y;
    private final String z;

    public interface a {
        void a(int i);

        void a(Bundle bundle);
    }

    public interface b {
        void a(com.google.android.gms.common.b bVar);
    }

    protected abstract class c<TListener> {

        /* renamed from: a reason: collision with root package name */
        private TListener f2278a;

        /* renamed from: b reason: collision with root package name */
        private boolean f2279b = false;

        public c(TListener tlistener) {
            this.f2278a = tlistener;
        }

        /* access modifiers changed from: protected */
        public abstract void a();

        /* access modifiers changed from: protected */
        public abstract void a(TListener tlistener);

        public void b() {
            TListener tlistener;
            synchronized (this) {
                tlistener = this.f2278a;
                if (this.f2279b) {
                    String valueOf = String.valueOf(this);
                    Log.w("GmsClient", new StringBuilder(String.valueOf(valueOf).length() + 47).append("Callback proxy ").append(valueOf).append(" being reused. This is not safe.").toString());
                }
            }
            if (tlistener != null) {
                try {
                    a(tlistener);
                } catch (RuntimeException e) {
                    a();
                    throw e;
                }
            } else {
                a();
            }
            synchronized (this) {
                this.f2279b = true;
            }
            c();
        }

        public void c() {
            d();
            synchronized (e.this.t) {
                e.this.t.remove(this);
            }
        }

        public void d() {
            synchronized (this) {
                this.f2278a = null;
            }
        }
    }

    public interface d {
        void a(com.google.android.gms.common.b bVar);
    }

    /* renamed from: com.google.android.gms.common.internal.e$e reason: collision with other inner class name */
    public static final class C0056e extends com.google.android.gms.common.internal.u.a {

        /* renamed from: a reason: collision with root package name */
        private e f2280a;

        /* renamed from: b reason: collision with root package name */
        private final int f2281b;

        public C0056e(e eVar, int i) {
            this.f2280a = eVar;
            this.f2281b = i;
        }

        public final void a(int i, Bundle bundle) {
            Log.wtf("GmsClient", "received deprecated onAccountValidationComplete callback, ignoring", new Exception());
        }

        public final void a(int i, IBinder iBinder, Bundle bundle) {
            aa.a(this.f2280a, (Object) "onPostInitComplete can be called only once per call to getRemoteService");
            this.f2280a.a(i, iBinder, bundle, this.f2281b);
            this.f2280a = null;
        }

        public final void a(int i, IBinder iBinder, h hVar) {
            aa.a(this.f2280a, (Object) "onPostInitCompleteWithConnectionInfo can be called only once per call togetRemoteService");
            aa.a(hVar);
            this.f2280a.a(hVar);
            a(i, iBinder, hVar.a());
        }
    }

    public final class f implements ServiceConnection {

        /* renamed from: a reason: collision with root package name */
        private final int f2282a;

        public f(int i) {
            this.f2282a = i;
        }

        public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            if (iBinder == null) {
                e.this.c(16);
                return;
            }
            synchronized (e.this.q) {
                e.this.r = com.google.android.gms.common.internal.v.a.a(iBinder);
            }
            e.this.a(0, (Bundle) null, this.f2282a);
        }

        public final void onServiceDisconnected(ComponentName componentName) {
            synchronized (e.this.q) {
                e.this.r = null;
            }
            e.this.f2276a.sendMessage(e.this.f2276a.obtainMessage(6, this.f2282a, 1));
        }
    }

    protected class g implements d {
        public g() {
        }

        public void a(com.google.android.gms.common.b bVar) {
            if (bVar.b()) {
                e.this.a((r) null, e.this.z());
            } else if (e.this.x != null) {
                e.this.x.a(bVar);
            }
        }
    }

    protected final class h extends k {

        /* renamed from: a reason: collision with root package name */
        public final IBinder f2285a;

        public h(int i, IBinder iBinder, Bundle bundle) {
            super(i, bundle);
            this.f2285a = iBinder;
        }

        /* access modifiers changed from: protected */
        public final void a(com.google.android.gms.common.b bVar) {
            if (e.this.x != null) {
                e.this.x.a(bVar);
            }
            e.this.a(bVar);
        }

        /* access modifiers changed from: protected */
        public final boolean e() {
            try {
                String interfaceDescriptor = this.f2285a.getInterfaceDescriptor();
                if (!e.this.l().equals(interfaceDescriptor)) {
                    String l = e.this.l();
                    Log.e("GmsClient", new StringBuilder(String.valueOf(l).length() + 34 + String.valueOf(interfaceDescriptor).length()).append("service descriptor mismatch: ").append(l).append(" vs. ").append(interfaceDescriptor).toString());
                    return false;
                }
                IInterface a2 = e.this.a(this.f2285a);
                if (a2 == null) {
                    return false;
                }
                if (!e.this.a(2, 4, a2) && !e.this.a(3, 4, a2)) {
                    return false;
                }
                e.this.A = null;
                Bundle w = e.this.w();
                if (e.this.w != null) {
                    e.this.w.a(w);
                }
                return true;
            } catch (RemoteException e) {
                Log.w("GmsClient", "service probably died");
                return false;
            }
        }
    }

    protected final class i extends k {
        public i(int i, Bundle bundle) {
            super(i, bundle);
        }

        /* access modifiers changed from: protected */
        public final void a(com.google.android.gms.common.b bVar) {
            e.this.f2277b.a(bVar);
            e.this.a(bVar);
        }

        /* access modifiers changed from: protected */
        public final boolean e() {
            e.this.f2277b.a(com.google.android.gms.common.b.f2234a);
            return true;
        }
    }

    public interface j {
        void a();
    }

    private abstract class k extends c<Boolean> {

        /* renamed from: b reason: collision with root package name */
        public final int f2288b;
        public final Bundle c;

        protected k(int i, Bundle bundle) {
            super(Boolean.valueOf(true));
            this.f2288b = i;
            this.c = bundle;
        }

        /* access modifiers changed from: protected */
        public void a() {
        }

        /* access modifiers changed from: protected */
        public abstract void a(com.google.android.gms.common.b bVar);

        /* access modifiers changed from: protected */
        public void a(Boolean bool) {
            PendingIntent pendingIntent = null;
            if (bool == null) {
                e.this.b(1, null);
                return;
            }
            switch (this.f2288b) {
                case 0:
                    if (!e()) {
                        e.this.b(1, null);
                        a(new com.google.android.gms.common.b(8, null));
                        return;
                    }
                    return;
                case 10:
                    e.this.b(1, null);
                    throw new IllegalStateException("A fatal developer error has occurred. Check the logs for further information.");
                default:
                    e.this.b(1, null);
                    if (this.c != null) {
                        pendingIntent = (PendingIntent) this.c.getParcelable("pendingIntent");
                    }
                    a(new com.google.android.gms.common.b(this.f2288b, pendingIntent));
                    return;
            }
        }

        /* access modifiers changed from: protected */
        public abstract boolean e();
    }

    final class l extends Handler {
        public l(Looper looper) {
            super(looper);
        }

        private static void a(Message message) {
            c cVar = (c) message.obj;
            cVar.a();
            cVar.c();
        }

        private static boolean b(Message message) {
            return message.what == 2 || message.what == 1 || message.what == 7;
        }

        public final void handleMessage(Message message) {
            PendingIntent pendingIntent = null;
            if (e.this.c.get() != message.arg1) {
                if (b(message)) {
                    a(message);
                }
            } else if ((message.what == 1 || message.what == 7 || message.what == 4 || message.what == 5) && !e.this.c()) {
                a(message);
            } else if (message.what == 4) {
                e.this.A = new com.google.android.gms.common.b(message.arg2);
                if (!e.this.B() || e.this.B) {
                    com.google.android.gms.common.b bVar = e.this.A != null ? e.this.A : new com.google.android.gms.common.b(8);
                    e.this.f2277b.a(bVar);
                    e.this.a(bVar);
                    return;
                }
                e.this.b(3, null);
            } else if (message.what == 5) {
                com.google.android.gms.common.b bVar2 = e.this.A != null ? e.this.A : new com.google.android.gms.common.b(8);
                e.this.f2277b.a(bVar2);
                e.this.a(bVar2);
            } else if (message.what == 3) {
                if (message.obj instanceof PendingIntent) {
                    pendingIntent = (PendingIntent) message.obj;
                }
                com.google.android.gms.common.b bVar3 = new com.google.android.gms.common.b(message.arg2, pendingIntent);
                e.this.f2277b.a(bVar3);
                e.this.a(bVar3);
            } else if (message.what == 6) {
                e.this.b(5, null);
                if (e.this.w != null) {
                    e.this.w.a(message.arg2);
                }
                e.this.a(message.arg2);
                e.this.a(5, 1, null);
            } else if (message.what == 2 && !e.this.b()) {
                a(message);
            } else if (b(message)) {
                ((c) message.obj).b();
            } else {
                Log.wtf("GmsClient", "Don't know how to handle message: " + message.what, new Exception());
            }
        }
    }

    protected e(Context context, Looper looper, int i2, a aVar, b bVar, String str) {
        this(context, looper, n.a(context), com.google.android.gms.common.h.b(), i2, (a) aa.a(aVar), (b) aa.a(bVar), str);
    }

    protected e(Context context, Looper looper, n nVar, com.google.android.gms.common.h hVar, int i2, a aVar, b bVar, String str) {
        this.p = new Object();
        this.q = new Object();
        this.t = new ArrayList<>();
        this.v = 1;
        this.A = null;
        this.B = false;
        this.C = null;
        this.c = new AtomicInteger(0);
        this.l = (Context) aa.a(context, (Object) "Context must not be null");
        this.m = (Looper) aa.a(looper, (Object) "Looper must not be null");
        this.n = (n) aa.a(nVar, (Object) "Supervisor must not be null");
        this.o = (com.google.android.gms.common.h) aa.a(hVar, (Object) "API availability must not be null");
        this.f2276a = new l(looper);
        this.y = i2;
        this.w = aVar;
        this.x = bVar;
        this.z = str;
    }

    private final boolean A() {
        boolean z2;
        synchronized (this.p) {
            z2 = this.v == 3;
        }
        return z2;
    }

    /* access modifiers changed from: private */
    public final boolean B() {
        if (this.B || TextUtils.isEmpty(l()) || TextUtils.isEmpty(n())) {
            return false;
        }
        try {
            Class.forName(l());
            return true;
        } catch (ClassNotFoundException e2) {
            return false;
        }
    }

    /* access modifiers changed from: private */
    public final void a(h hVar) {
        this.C = hVar;
    }

    /* access modifiers changed from: private */
    public final boolean a(int i2, int i3, T t2) {
        boolean z2;
        synchronized (this.p) {
            if (this.v != i2) {
                z2 = false;
            } else {
                b(i3, t2);
                z2 = true;
            }
        }
        return z2;
    }

    /* access modifiers changed from: private */
    public final void b(int i2, T t2) {
        boolean z2 = true;
        if ((i2 == 4) != (t2 != null)) {
            z2 = false;
        }
        aa.b(z2);
        synchronized (this.p) {
            this.v = i2;
            this.s = t2;
            a(i2, t2);
            switch (i2) {
                case 1:
                    if (this.u != null) {
                        this.n.b(i(), w_(), k(), this.u, m());
                        this.u = null;
                        break;
                    }
                    break;
                case 2:
                case 3:
                    if (!(this.u == null || this.k == null)) {
                        String a2 = this.k.a();
                        String b2 = this.k.b();
                        Log.e("GmsClient", new StringBuilder(String.valueOf(a2).length() + 70 + String.valueOf(b2).length()).append("Calling connect() while still connected, missing disconnect() for ").append(a2).append(" on ").append(b2).toString());
                        this.n.b(this.k.a(), this.k.b(), this.k.c(), this.u, m());
                        this.c.incrementAndGet();
                    }
                    this.u = new f<>(this.c.get());
                    this.k = (this.v != 3 || n() == null) ? new p(w_(), i(), false, k()) : new p(p().getPackageName(), n(), true, k());
                    if (!this.n.a(this.k.a(), this.k.b(), this.k.c(), this.u, m())) {
                        String a3 = this.k.a();
                        String b3 = this.k.b();
                        Log.e("GmsClient", new StringBuilder(String.valueOf(a3).length() + 34 + String.valueOf(b3).length()).append("unable to connect to service: ").append(a3).append(" on ").append(b3).toString());
                        a(16, (Bundle) null, this.c.get());
                        break;
                    }
                    break;
                case 4:
                    a(t2);
                    break;
            }
        }
    }

    /* access modifiers changed from: private */
    public final void c(int i2) {
        int i3;
        if (A()) {
            i3 = 5;
            this.B = true;
        } else {
            i3 = 4;
        }
        this.f2276a.sendMessage(this.f2276a.obtainMessage(i3, this.c.get(), 16));
    }

    /* access modifiers changed from: protected */
    public abstract T a(IBinder iBinder);

    public void a() {
        this.c.incrementAndGet();
        synchronized (this.t) {
            int size = this.t.size();
            for (int i2 = 0; i2 < size; i2++) {
                ((c) this.t.get(i2)).d();
            }
            this.t.clear();
        }
        synchronized (this.q) {
            this.r = null;
        }
        b(1, null);
    }

    /* access modifiers changed from: protected */
    public void a(int i2) {
        this.f = i2;
        this.g = System.currentTimeMillis();
    }

    /* access modifiers changed from: protected */
    public void a(int i2, Bundle bundle, int i3) {
        this.f2276a.sendMessage(this.f2276a.obtainMessage(7, i3, -1, new i(i2, bundle)));
    }

    /* access modifiers changed from: protected */
    public void a(int i2, IBinder iBinder, Bundle bundle, int i3) {
        this.f2276a.sendMessage(this.f2276a.obtainMessage(1, i3, -1, new h(i2, iBinder, bundle)));
    }

    /* access modifiers changed from: 0000 */
    public void a(int i2, T t2) {
    }

    /* access modifiers changed from: protected */
    public void a(T t2) {
        this.h = System.currentTimeMillis();
    }

    /* access modifiers changed from: protected */
    public void a(com.google.android.gms.common.b bVar) {
        this.i = bVar.c();
        this.j = System.currentTimeMillis();
    }

    public void a(d dVar) {
        this.f2277b = (d) aa.a(dVar, (Object) "Connection progress callbacks cannot be null.");
        b(2, null);
    }

    /* access modifiers changed from: protected */
    public void a(d dVar, int i2, PendingIntent pendingIntent) {
        this.f2277b = (d) aa.a(dVar, (Object) "Connection progress callbacks cannot be null.");
        this.f2276a.sendMessage(this.f2276a.obtainMessage(3, this.c.get(), i2, pendingIntent));
    }

    public void a(j jVar) {
        jVar.a();
    }

    public void a(r rVar, Set<Scope> set) {
        k a2 = new k(this.y).a(this.l.getPackageName()).a(u());
        if (set != null) {
            a2.a((Collection<Scope>) set);
        }
        if (d()) {
            a2.a(t()).a(rVar);
        } else if (y()) {
            a2.a(q());
        }
        a2.a(r());
        a2.b(s());
        try {
            synchronized (this.q) {
                if (this.r != null) {
                    this.r.a(new C0056e(this, this.c.get()), a2);
                } else {
                    Log.w("GmsClient", "mServiceBroker is null, client disconnected");
                }
            }
        } catch (DeadObjectException e2) {
            Log.w("GmsClient", "IGmsServiceBroker.getService failed", e2);
            b(1);
        } catch (SecurityException e3) {
            throw e3;
        } catch (RemoteException | RuntimeException e4) {
            Log.w("GmsClient", "IGmsServiceBroker.getService failed", e4);
            a(8, (IBinder) null, (Bundle) null, this.c.get());
        }
    }

    public void b(int i2) {
        this.f2276a.sendMessage(this.f2276a.obtainMessage(6, this.c.get(), i2));
    }

    public boolean b() {
        boolean z2;
        synchronized (this.p) {
            z2 = this.v == 4;
        }
        return z2;
    }

    public boolean c() {
        boolean z2;
        synchronized (this.p) {
            z2 = this.v == 2 || this.v == 3;
        }
        return z2;
    }

    public boolean d() {
        return false;
    }

    public boolean e() {
        return true;
    }

    public String f() {
        if (b() && this.k != null) {
            return this.k.b();
        }
        throw new RuntimeException("Failed to connect when checking package");
    }

    public int g() {
        return com.google.android.gms.common.h.f2249b;
    }

    public final com.google.android.gms.common.e[] h() {
        h hVar = this.C;
        if (hVar == null) {
            return null;
        }
        return hVar.b();
    }

    /* access modifiers changed from: protected */
    public abstract String i();

    /* access modifiers changed from: protected */
    public int k() {
        return 129;
    }

    /* access modifiers changed from: protected */
    public abstract String l();

    /* access modifiers changed from: protected */
    public final String m() {
        return this.z == null ? this.l.getClass().getName() : this.z;
    }

    /* access modifiers changed from: protected */
    public String n() {
        return null;
    }

    public void o() {
        int b2 = this.o.b(this.l, g());
        if (b2 != 0) {
            b(1, null);
            a((d) new g(), b2, (PendingIntent) null);
            return;
        }
        a((d) new g());
    }

    public final Context p() {
        return this.l;
    }

    public Account q() {
        return null;
    }

    public com.google.android.gms.common.e[] r() {
        return e;
    }

    public com.google.android.gms.common.e[] s() {
        return e;
    }

    public final Account t() {
        return q() != null ? q() : new Account("<<default account>>", "com.google");
    }

    /* access modifiers changed from: protected */
    public Bundle u() {
        return new Bundle();
    }

    /* access modifiers changed from: protected */
    public final void v() {
        if (!b()) {
            throw new IllegalStateException("Not connected. Call connect() and wait for onConnected() to be called.");
        }
    }

    public Bundle w() {
        return null;
    }

    /* access modifiers changed from: protected */
    public String w_() {
        return "com.google.android.gms";
    }

    public final T x() throws DeadObjectException {
        T t2;
        synchronized (this.p) {
            if (this.v == 5) {
                throw new DeadObjectException();
            }
            v();
            aa.a(this.s != null, (Object) "Client is connected but service is null");
            t2 = this.s;
        }
        return t2;
    }

    public boolean y() {
        return false;
    }

    /* access modifiers changed from: protected */
    public Set<Scope> z() {
        return Collections.EMPTY_SET;
    }
}
