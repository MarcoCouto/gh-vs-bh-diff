package com.google.android.gms.common.internal;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.support.v4.a.i;
import android.util.Log;
import com.google.android.gms.common.api.internal.f;

public abstract class j implements OnClickListener {
    public static j a(Activity activity, Intent intent, int i) {
        return new aj(intent, activity, i);
    }

    public static j a(i iVar, Intent intent, int i) {
        return new ak(intent, iVar, i);
    }

    public static j a(f fVar, Intent intent, int i) {
        return new al(intent, fVar, i);
    }

    /* access modifiers changed from: protected */
    public abstract void a();

    public void onClick(DialogInterface dialogInterface, int i) {
        try {
            a();
        } catch (ActivityNotFoundException e) {
            Log.e("DialogRedirect", "Failed to start resolution intent", e);
        } finally {
            dialogInterface.dismiss();
        }
    }
}
