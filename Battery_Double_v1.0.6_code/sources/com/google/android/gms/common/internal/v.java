package com.google.android.gms.common.internal;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface v extends IInterface {

    public static abstract class a extends Binder implements v {

        /* renamed from: com.google.android.gms.common.internal.v$a$a reason: collision with other inner class name */
        private static class C0060a implements v {

            /* renamed from: a reason: collision with root package name */
            private final IBinder f2310a;

            C0060a(IBinder iBinder) {
                this.f2310a = iBinder;
            }

            public final void a(u uVar, k kVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.common.internal.IGmsServiceBroker");
                    obtain.writeStrongBinder(uVar != null ? uVar.asBinder() : null);
                    if (kVar != null) {
                        obtain.writeInt(1);
                        kVar.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.f2310a.transact(46, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public final IBinder asBinder() {
                return this.f2310a;
            }
        }

        public static v a(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
            return (queryLocalInterface == null || !(queryLocalInterface instanceof v)) ? new C0060a(iBinder) : (v) queryLocalInterface;
        }

        /* access modifiers changed from: protected */
        public void a(int i, u uVar, int i2, String str, String str2, String[] strArr, Bundle bundle, IBinder iBinder, String str3, String str4) throws RemoteException {
            throw new UnsupportedOperationException();
        }

        /* access modifiers changed from: protected */
        public void a(u uVar, ah ahVar) throws RemoteException {
            throw new UnsupportedOperationException();
        }

        public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
            String str;
            String str2;
            IBinder iBinder;
            Bundle bundle;
            String[] strArr;
            String str3;
            if (i > 16777215) {
                return super.onTransact(i, parcel, parcel2, i2);
            }
            parcel.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
            u a2 = com.google.android.gms.common.internal.u.a.a(parcel.readStrongBinder());
            if (i == 46) {
                a(a2, parcel.readInt() != 0 ? (k) k.CREATOR.createFromParcel(parcel) : null);
            } else if (i == 47) {
                a(a2, parcel.readInt() != 0 ? (ah) ah.CREATOR.createFromParcel(parcel) : null);
            } else {
                int readInt = parcel.readInt();
                String str4 = i != 4 ? parcel.readString() : null;
                switch (i) {
                    case 1:
                        str2 = parcel.readString();
                        strArr = parcel.createStringArray();
                        str3 = parcel.readString();
                        if (parcel.readInt() == 0) {
                            str = null;
                            iBinder = null;
                            bundle = null;
                            break;
                        } else {
                            str = null;
                            iBinder = null;
                            bundle = (Bundle) Bundle.CREATOR.createFromParcel(parcel);
                            break;
                        }
                    case 2:
                    case 5:
                    case 6:
                    case 7:
                    case 8:
                    case 11:
                    case 12:
                    case 13:
                    case 14:
                    case 15:
                    case 16:
                    case 17:
                    case 18:
                    case 23:
                    case 25:
                    case 27:
                    case 37:
                    case 38:
                    case 41:
                    case 43:
                        if (parcel.readInt() != 0) {
                            str = null;
                            str2 = null;
                            iBinder = null;
                            bundle = (Bundle) Bundle.CREATOR.createFromParcel(parcel);
                            strArr = null;
                            str3 = null;
                            break;
                        }
                    case 9:
                        str3 = parcel.readString();
                        strArr = parcel.createStringArray();
                        str2 = parcel.readString();
                        iBinder = parcel.readStrongBinder();
                        str = parcel.readString();
                        if (parcel.readInt() == 0) {
                            bundle = null;
                            break;
                        } else {
                            bundle = (Bundle) Bundle.CREATOR.createFromParcel(parcel);
                            break;
                        }
                    case 10:
                        str3 = parcel.readString();
                        strArr = parcel.createStringArray();
                        str = null;
                        str2 = null;
                        iBinder = null;
                        bundle = null;
                        break;
                    case 19:
                        iBinder = parcel.readStrongBinder();
                        if (parcel.readInt() == 0) {
                            str = null;
                            str2 = null;
                            bundle = null;
                            strArr = null;
                            str3 = null;
                            break;
                        } else {
                            str = null;
                            str2 = null;
                            bundle = (Bundle) Bundle.CREATOR.createFromParcel(parcel);
                            strArr = null;
                            str3 = null;
                            break;
                        }
                    case 20:
                    case 30:
                        strArr = parcel.createStringArray();
                        str3 = parcel.readString();
                        if (parcel.readInt() == 0) {
                            str = null;
                            str2 = null;
                            iBinder = null;
                            bundle = null;
                            break;
                        } else {
                            str = null;
                            str2 = null;
                            iBinder = null;
                            bundle = (Bundle) Bundle.CREATOR.createFromParcel(parcel);
                            break;
                        }
                    case 34:
                        str3 = parcel.readString();
                        str = null;
                        str2 = null;
                        iBinder = null;
                        bundle = null;
                        strArr = null;
                        break;
                    default:
                        str = null;
                        str2 = null;
                        iBinder = null;
                        bundle = null;
                        strArr = null;
                        str3 = null;
                        break;
                }
                a(i, a2, readInt, str4, str3, strArr, bundle, iBinder, str2, str);
            }
            parcel2.writeNoException();
            return true;
        }
    }

    void a(u uVar, k kVar) throws RemoteException;
}
