package com.google.android.gms.common.internal;

import android.os.IBinder;
import android.os.IInterface;
import com.google.android.gms.common.api.a.h;

public class af<T extends IInterface> extends m<T> {
    private final h<T> e;

    public h<T> A() {
        return this.e;
    }

    /* access modifiers changed from: protected */
    public T a(IBinder iBinder) {
        return this.e.a(iBinder);
    }

    /* access modifiers changed from: protected */
    public void a(int i, T t) {
        this.e.a(i, t);
    }

    public int g() {
        return super.g();
    }

    /* access modifiers changed from: protected */
    public String i() {
        return this.e.a();
    }

    /* access modifiers changed from: protected */
    public String l() {
        return this.e.b();
    }
}
