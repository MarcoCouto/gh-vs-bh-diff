package com.google.android.gms.common.internal;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public final class z {

    public static final class a {

        /* renamed from: a reason: collision with root package name */
        private final List<String> f2313a;

        /* renamed from: b reason: collision with root package name */
        private final Object f2314b;

        private a(Object obj) {
            this.f2314b = aa.a(obj);
            this.f2313a = new ArrayList();
        }

        public final a a(String str, Object obj) {
            List<String> list = this.f2313a;
            String str2 = (String) aa.a(str);
            String valueOf = String.valueOf(obj);
            list.add(new StringBuilder(String.valueOf(str2).length() + 1 + String.valueOf(valueOf).length()).append(str2).append("=").append(valueOf).toString());
            return this;
        }

        public final String toString() {
            StringBuilder append = new StringBuilder(100).append(this.f2314b.getClass().getSimpleName()).append('{');
            int size = this.f2313a.size();
            for (int i = 0; i < size; i++) {
                append.append((String) this.f2313a.get(i));
                if (i < size - 1) {
                    append.append(", ");
                }
            }
            return append.append('}').toString();
        }
    }

    public static int a(Object... objArr) {
        return Arrays.hashCode(objArr);
    }

    public static a a(Object obj) {
        return new a(obj);
    }

    public static boolean a(Object obj, Object obj2) {
        return obj == obj2 || (obj != null && obj.equals(obj2));
    }
}
