package com.google.android.gms.common.internal;

import android.accounts.Account;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.internal.d.b;
import com.google.android.gms.internal.d.c;

public interface r extends IInterface {

    public static abstract class a extends b implements r {

        /* renamed from: com.google.android.gms.common.internal.r$a$a reason: collision with other inner class name */
        public static class C0057a extends com.google.android.gms.internal.d.a implements r {
            C0057a(IBinder iBinder) {
                super(iBinder, "com.google.android.gms.common.internal.IAccountAccessor");
            }

            public Account a() throws RemoteException {
                Parcel a2 = a(2, d());
                Account account = (Account) c.a(a2, Account.CREATOR);
                a2.recycle();
                return account;
            }
        }

        public static r a(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.common.internal.IAccountAccessor");
            return queryLocalInterface instanceof r ? (r) queryLocalInterface : new C0057a(iBinder);
        }

        /* access modifiers changed from: protected */
        public boolean a(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
            if (i != 2) {
                return false;
            }
            Account a2 = a();
            parcel2.writeNoException();
            c.b(parcel2, a2);
            return true;
        }
    }

    Account a() throws RemoteException;
}
