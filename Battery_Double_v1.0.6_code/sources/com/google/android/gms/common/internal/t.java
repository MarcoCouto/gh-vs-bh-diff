package com.google.android.gms.common.internal;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.b.a.C0046a;
import com.google.android.gms.internal.d.b;
import com.google.android.gms.internal.d.c;

public interface t extends IInterface {

    public static abstract class a extends b implements t {

        /* renamed from: com.google.android.gms.common.internal.t$a$a reason: collision with other inner class name */
        public static class C0058a extends com.google.android.gms.internal.d.a implements t {
            C0058a(IBinder iBinder) {
                super(iBinder, "com.google.android.gms.common.internal.ICertData");
            }

            public com.google.android.gms.b.a b() throws RemoteException {
                Parcel a2 = a(1, d());
                com.google.android.gms.b.a a3 = C0046a.a(a2.readStrongBinder());
                a2.recycle();
                return a3;
            }

            public int c() throws RemoteException {
                Parcel a2 = a(2, d());
                int readInt = a2.readInt();
                a2.recycle();
                return readInt;
            }
        }

        public a() {
            super("com.google.android.gms.common.internal.ICertData");
        }

        public static t a(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.common.internal.ICertData");
            return queryLocalInterface instanceof t ? (t) queryLocalInterface : new C0058a(iBinder);
        }

        /* access modifiers changed from: protected */
        public boolean a(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
            switch (i) {
                case 1:
                    com.google.android.gms.b.a b2 = b();
                    parcel2.writeNoException();
                    c.a(parcel2, (IInterface) b2);
                    break;
                case 2:
                    int c = c();
                    parcel2.writeNoException();
                    parcel2.writeInt(c);
                    break;
                default:
                    return false;
            }
            return true;
        }
    }

    com.google.android.gms.b.a b() throws RemoteException;

    int c() throws RemoteException;
}
