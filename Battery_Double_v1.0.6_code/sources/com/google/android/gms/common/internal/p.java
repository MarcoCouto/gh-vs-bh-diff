package com.google.android.gms.common.internal;

public class p {

    /* renamed from: a reason: collision with root package name */
    private final String f2306a;

    /* renamed from: b reason: collision with root package name */
    private final String f2307b;
    private final int c;
    private final boolean d;

    public p(String str, String str2, boolean z, int i) {
        this.f2307b = str;
        this.f2306a = str2;
        this.d = z;
        this.c = i;
    }

    /* access modifiers changed from: 0000 */
    public final String a() {
        return this.f2306a;
    }

    /* access modifiers changed from: 0000 */
    public final String b() {
        return this.f2307b;
    }

    /* access modifiers changed from: 0000 */
    public final int c() {
        return this.c;
    }
}
