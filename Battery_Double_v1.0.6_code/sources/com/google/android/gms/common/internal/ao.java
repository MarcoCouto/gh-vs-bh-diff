package com.google.android.gms.common.internal;

import android.content.ComponentName;
import android.content.Context;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import android.util.Log;
import com.google.android.gms.common.a.a;
import com.hmatalonga.greenhub.models.Network;
import java.util.HashMap;

final class ao extends n implements Callback {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public final HashMap<a, ap> f2270a = new HashMap<>();
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public final Context f2271b;
    /* access modifiers changed from: private */
    public final Handler c;
    /* access modifiers changed from: private */
    public final a d;
    private final long e;
    /* access modifiers changed from: private */
    public final long f;

    ao(Context context) {
        this.f2271b = context.getApplicationContext();
        this.c = new Handler(context.getMainLooper(), this);
        this.d = a.a();
        this.e = 5000;
        this.f = 300000;
    }

    /* access modifiers changed from: protected */
    public final boolean a(a aVar, ServiceConnection serviceConnection, String str) {
        boolean a2;
        aa.a(serviceConnection, (Object) "ServiceConnection must not be null");
        synchronized (this.f2270a) {
            ap apVar = (ap) this.f2270a.get(aVar);
            if (apVar != null) {
                this.c.removeMessages(0, aVar);
                if (!apVar.a(serviceConnection)) {
                    apVar.a(serviceConnection, str);
                    switch (apVar.b()) {
                        case 1:
                            serviceConnection.onServiceConnected(apVar.e(), apVar.d());
                            break;
                        case 2:
                            apVar.a(str);
                            break;
                    }
                } else {
                    String valueOf = String.valueOf(aVar);
                    throw new IllegalStateException(new StringBuilder(String.valueOf(valueOf).length() + 81).append("Trying to bind a GmsServiceConnection that was already connected before.  config=").append(valueOf).toString());
                }
            } else {
                apVar = new ap(this, aVar);
                apVar.a(serviceConnection, str);
                apVar.a(str);
                this.f2270a.put(aVar, apVar);
            }
            a2 = apVar.a();
        }
        return a2;
    }

    /* access modifiers changed from: protected */
    public final void b(a aVar, ServiceConnection serviceConnection, String str) {
        aa.a(serviceConnection, (Object) "ServiceConnection must not be null");
        synchronized (this.f2270a) {
            ap apVar = (ap) this.f2270a.get(aVar);
            if (apVar == null) {
                String valueOf = String.valueOf(aVar);
                throw new IllegalStateException(new StringBuilder(String.valueOf(valueOf).length() + 50).append("Nonexistent connection status for service config: ").append(valueOf).toString());
            } else if (!apVar.a(serviceConnection)) {
                String valueOf2 = String.valueOf(aVar);
                throw new IllegalStateException(new StringBuilder(String.valueOf(valueOf2).length() + 76).append("Trying to unbind a GmsServiceConnection  that was not bound before.  config=").append(valueOf2).toString());
            } else {
                apVar.b(serviceConnection, str);
                if (apVar.c()) {
                    this.c.sendMessageDelayed(this.c.obtainMessage(0, aVar), this.e);
                }
            }
        }
    }

    public final boolean handleMessage(Message message) {
        switch (message.what) {
            case 0:
                synchronized (this.f2270a) {
                    a aVar = (a) message.obj;
                    ap apVar = (ap) this.f2270a.get(aVar);
                    if (apVar != null && apVar.c()) {
                        if (apVar.a()) {
                            apVar.b("GmsClientSupervisor");
                        }
                        this.f2270a.remove(aVar);
                    }
                }
                return true;
            case 1:
                synchronized (this.f2270a) {
                    a aVar2 = (a) message.obj;
                    ap apVar2 = (ap) this.f2270a.get(aVar2);
                    if (apVar2 != null && apVar2.b() == 3) {
                        String valueOf = String.valueOf(aVar2);
                        Log.wtf("GmsClientSupervisor", new StringBuilder(String.valueOf(valueOf).length() + 47).append("Timeout waiting for ServiceConnection callback ").append(valueOf).toString(), new Exception());
                        ComponentName e2 = apVar2.e();
                        if (e2 == null) {
                            e2 = aVar2.b();
                        }
                        apVar2.onServiceDisconnected(e2 == null ? new ComponentName(aVar2.a(), Network.TYPE_UNKNOWN) : e2);
                    }
                }
                return true;
            default:
                return false;
        }
    }
}
