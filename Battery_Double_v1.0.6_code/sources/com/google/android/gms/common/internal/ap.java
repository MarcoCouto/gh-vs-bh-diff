package com.google.android.gms.common.internal;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import java.util.HashSet;
import java.util.Set;

final class ap implements ServiceConnection {

    /* renamed from: a reason: collision with root package name */
    private final Set<ServiceConnection> f2272a = new HashSet();

    /* renamed from: b reason: collision with root package name */
    private int f2273b = 2;
    private boolean c;
    private IBinder d;
    private final a e;
    private ComponentName f;
    private final /* synthetic */ ao g;

    public ap(ao aoVar, a aVar) {
        this.g = aoVar;
        this.e = aVar;
    }

    public final void a(ServiceConnection serviceConnection, String str) {
        this.g.d.a(this.g.f2271b, serviceConnection, str, this.e.a(this.g.f2271b));
        this.f2272a.add(serviceConnection);
    }

    public final void a(String str) {
        this.f2273b = 3;
        this.c = this.g.d.a(this.g.f2271b, str, this.e.a(this.g.f2271b), this, this.e.c());
        if (this.c) {
            this.g.c.sendMessageDelayed(this.g.c.obtainMessage(1, this.e), this.g.f);
            return;
        }
        this.f2273b = 2;
        try {
            this.g.d.a(this.g.f2271b, this);
        } catch (IllegalArgumentException e2) {
        }
    }

    public final boolean a() {
        return this.c;
    }

    public final boolean a(ServiceConnection serviceConnection) {
        return this.f2272a.contains(serviceConnection);
    }

    public final int b() {
        return this.f2273b;
    }

    public final void b(ServiceConnection serviceConnection, String str) {
        this.g.d.b(this.g.f2271b, serviceConnection);
        this.f2272a.remove(serviceConnection);
    }

    public final void b(String str) {
        this.g.c.removeMessages(1, this.e);
        this.g.d.a(this.g.f2271b, this);
        this.c = false;
        this.f2273b = 2;
    }

    public final boolean c() {
        return this.f2272a.isEmpty();
    }

    public final IBinder d() {
        return this.d;
    }

    public final ComponentName e() {
        return this.f;
    }

    public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        synchronized (this.g.f2270a) {
            this.g.c.removeMessages(1, this.e);
            this.d = iBinder;
            this.f = componentName;
            for (ServiceConnection onServiceConnected : this.f2272a) {
                onServiceConnected.onServiceConnected(componentName, iBinder);
            }
            this.f2273b = 1;
        }
    }

    public final void onServiceDisconnected(ComponentName componentName) {
        synchronized (this.g.f2270a) {
            this.g.c.removeMessages(1, this.e);
            this.d = null;
            this.f = componentName;
            for (ServiceConnection onServiceDisconnected : this.f2272a) {
                onServiceDisconnected.onServiceDisconnected(componentName);
            }
            this.f2273b = 2;
        }
    }
}
