package com.google.android.gms.common.internal;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.google.android.gms.internal.d.b;
import com.google.android.gms.internal.d.c;

public interface x extends IInterface {

    public static abstract class a extends b implements x {

        /* renamed from: com.google.android.gms.common.internal.x$a$a reason: collision with other inner class name */
        public static class C0062a extends com.google.android.gms.internal.d.a implements x {
            C0062a(IBinder iBinder) {
                super(iBinder, "com.google.android.gms.common.internal.IResolveAccountCallbacks");
            }

            public void a(ad adVar) throws RemoteException {
                Parcel d = d();
                c.a(d, (Parcelable) adVar);
                b(2, d);
            }
        }

        public static x a(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.common.internal.IResolveAccountCallbacks");
            return queryLocalInterface instanceof x ? (x) queryLocalInterface : new C0062a(iBinder);
        }

        /* access modifiers changed from: protected */
        public boolean a(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
            if (i != 2) {
                return false;
            }
            a((ad) c.a(parcel, ad.CREATOR));
            parcel2.writeNoException();
            return true;
        }
    }

    void a(ad adVar) throws RemoteException;
}
