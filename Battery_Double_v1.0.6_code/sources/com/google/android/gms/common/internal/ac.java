package com.google.android.gms.common.internal;

import android.accounts.Account;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.internal.a.b;

public class ac implements Creator<ab> {
    /* renamed from: a */
    public ab createFromParcel(Parcel parcel) {
        int b2 = b.b(parcel);
        int i = 0;
        Account account = null;
        int i2 = 0;
        GoogleSignInAccount googleSignInAccount = null;
        while (parcel.dataPosition() < b2) {
            int a2 = b.a(parcel);
            switch (b.a(a2)) {
                case 1:
                    i2 = b.d(parcel, a2);
                    break;
                case 2:
                    account = (Account) b.a(parcel, a2, Account.CREATOR);
                    break;
                case 3:
                    i = b.d(parcel, a2);
                    break;
                case 4:
                    googleSignInAccount = (GoogleSignInAccount) b.a(parcel, a2, GoogleSignInAccount.CREATOR);
                    break;
                default:
                    b.b(parcel, a2);
                    break;
            }
        }
        b.r(parcel, b2);
        return new ab(i2, account, i, googleSignInAccount);
    }

    /* renamed from: a */
    public ab[] newArray(int i) {
        return new ab[i];
    }
}
