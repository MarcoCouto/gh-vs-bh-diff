package com.google.android.gms.common.internal;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.b;
import com.google.android.gms.common.internal.a.a;
import com.google.android.gms.common.internal.a.c;

public class ad extends a {
    public static final Creator<ad> CREATOR = new ae();

    /* renamed from: a reason: collision with root package name */
    private final int f2257a;

    /* renamed from: b reason: collision with root package name */
    private IBinder f2258b;
    private b c;
    private boolean d;
    private boolean e;

    ad(int i, IBinder iBinder, b bVar, boolean z, boolean z2) {
        this.f2257a = i;
        this.f2258b = iBinder;
        this.c = bVar;
        this.d = z;
        this.e = z2;
    }

    public r a() {
        return r.a.a(this.f2258b);
    }

    public b b() {
        return this.c;
    }

    public boolean c() {
        return this.d;
    }

    public boolean d() {
        return this.e;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ad)) {
            return false;
        }
        ad adVar = (ad) obj;
        return this.c.equals(adVar.c) && a().equals(adVar.a());
    }

    public void writeToParcel(Parcel parcel, int i) {
        int a2 = c.a(parcel);
        c.a(parcel, 1, this.f2257a);
        c.a(parcel, 2, this.f2258b, false);
        c.a(parcel, 3, (Parcelable) b(), i, false);
        c.a(parcel, 4, c());
        c.a(parcel, 5, d());
        c.a(parcel, a2);
    }
}
