package com.google.android.gms.common.internal;

import android.app.Activity;
import android.content.Intent;

final class aj extends j {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ Intent f2262a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ Activity f2263b;
    private final /* synthetic */ int c;

    aj(Intent intent, Activity activity, int i) {
        this.f2262a = intent;
        this.f2263b = activity;
        this.c = i;
    }

    public final void a() {
        if (this.f2262a != null) {
            this.f2263b.startActivityForResult(this.f2262a, this.c);
        }
    }
}
