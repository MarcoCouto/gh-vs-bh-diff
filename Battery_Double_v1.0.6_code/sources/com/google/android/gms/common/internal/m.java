package com.google.android.gms.common.internal;

import android.accounts.Account;
import android.content.Context;
import android.os.IInterface;
import android.os.Looper;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.a.f;
import com.google.android.gms.common.api.f.a;
import com.google.android.gms.common.api.f.b;
import com.google.android.gms.common.e;
import com.google.android.gms.common.g;
import java.util.Set;

public abstract class m<T extends IInterface> extends e<T> implements f {
    private final f e;
    private final Set<Scope> f;
    private final Account g;

    protected m(Context context, Looper looper, int i, f fVar, a aVar, b bVar) {
        this(context, looper, n.a(context), g.a(), i, fVar, (a) aa.a(aVar), (b) aa.a(bVar));
    }

    protected m(Context context, Looper looper, n nVar, g gVar, int i, f fVar, a aVar, b bVar) {
        super(context, looper, nVar, gVar, i, a(aVar), a(bVar), fVar.f());
        this.e = fVar;
        this.g = fVar.a();
        this.f = b(fVar.d());
    }

    private static e.a a(a aVar) {
        if (aVar == null) {
            return null;
        }
        return new am(aVar);
    }

    private static e.b a(b bVar) {
        if (bVar == null) {
            return null;
        }
        return new an(bVar);
    }

    private final Set<Scope> b(Set<Scope> set) {
        Set<Scope> a2 = a(set);
        for (Scope contains : a2) {
            if (!set.contains(contains)) {
                throw new IllegalStateException("Expanding scopes is not permitted, use implied scopes instead");
            }
        }
        return a2;
    }

    /* access modifiers changed from: protected */
    public Set<Scope> a(Set<Scope> set) {
        return set;
    }

    public int g() {
        return super.g();
    }

    public final Account q() {
        return this.g;
    }

    public e[] r() {
        return new e[0];
    }

    /* access modifiers changed from: protected */
    public final Set<Scope> z() {
        return this.f;
    }
}
