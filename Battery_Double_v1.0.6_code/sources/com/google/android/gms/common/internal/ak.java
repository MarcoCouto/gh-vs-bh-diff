package com.google.android.gms.common.internal;

import android.content.Intent;
import android.support.v4.a.i;

final class ak extends j {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ Intent f2264a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ i f2265b;
    private final /* synthetic */ int c;

    ak(Intent intent, i iVar, int i) {
        this.f2264a = intent;
        this.f2265b = iVar;
        this.c = i;
    }

    public final void a() {
        if (this.f2264a != null) {
            this.f2265b.a(this.f2264a, this.c);
        }
    }
}
