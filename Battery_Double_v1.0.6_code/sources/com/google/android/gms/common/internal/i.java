package com.google.android.gms.common.internal;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.e;
import com.google.android.gms.common.internal.a.b;

public class i implements Creator<h> {
    /* renamed from: a */
    public h createFromParcel(Parcel parcel) {
        e[] eVarArr = null;
        int b2 = b.b(parcel);
        Bundle bundle = null;
        while (parcel.dataPosition() < b2) {
            int a2 = b.a(parcel);
            switch (b.a(a2)) {
                case 1:
                    bundle = b.m(parcel, a2);
                    break;
                case 2:
                    eVarArr = (e[]) b.b(parcel, a2, e.CREATOR);
                    break;
                default:
                    b.b(parcel, a2);
                    break;
            }
        }
        b.r(parcel, b2);
        return new h(bundle, eVarArr);
    }

    /* renamed from: a */
    public h[] newArray(int i) {
        return new h[i];
    }
}
