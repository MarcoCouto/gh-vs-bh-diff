package com.google.android.gms.common.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.a.b;

public class ai implements Creator<ah> {
    /* renamed from: a */
    public ah createFromParcel(Parcel parcel) {
        int b2 = b.b(parcel);
        int i = 0;
        while (parcel.dataPosition() < b2) {
            int a2 = b.a(parcel);
            switch (b.a(a2)) {
                case 1:
                    i = b.d(parcel, a2);
                    break;
                default:
                    b.b(parcel, a2);
                    break;
            }
        }
        b.r(parcel, b2);
        return new ah(i);
    }

    /* renamed from: a */
    public ah[] newArray(int i) {
        return new ah[i];
    }
}
