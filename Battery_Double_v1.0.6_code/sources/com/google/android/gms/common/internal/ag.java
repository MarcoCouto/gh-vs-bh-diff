package com.google.android.gms.common.internal;

import android.content.Context;
import android.content.res.Resources;
import com.google.android.gms.common.p.a;

public class ag {

    /* renamed from: a reason: collision with root package name */
    private final Resources f2259a;

    /* renamed from: b reason: collision with root package name */
    private final String f2260b = this.f2259a.getResourcePackageName(a.common_google_play_services_unknown_issue);

    public ag(Context context) {
        aa.a(context);
        this.f2259a = context.getResources();
    }

    public String a(String str) {
        int identifier = this.f2259a.getIdentifier(str, "string", this.f2260b);
        if (identifier == 0) {
            return null;
        }
        return this.f2259a.getString(identifier);
    }
}
