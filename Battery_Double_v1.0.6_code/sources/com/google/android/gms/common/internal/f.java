package com.google.android.gms.common.internal;

import android.accounts.Account;
import android.view.View;
import com.google.android.gms.c.c;
import com.google.android.gms.common.api.Scope;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public final class f {

    /* renamed from: a reason: collision with root package name */
    private final Account f2290a;

    /* renamed from: b reason: collision with root package name */
    private final Set<Scope> f2291b;
    private final Set<Scope> c;
    private final Map<com.google.android.gms.common.api.a<?>, b> d;
    private final int e;
    private final View f;
    private final String g;
    private final String h;
    private final c i;
    private Integer j;

    public static final class a {

        /* renamed from: a reason: collision with root package name */
        private Account f2292a;

        /* renamed from: b reason: collision with root package name */
        private android.support.v4.h.b<Scope> f2293b;
        private Map<com.google.android.gms.common.api.a<?>, b> c;
        private int d = 0;
        private View e;
        private String f;
        private String g;
        private c h = c.f2149a;

        public final a a(Account account) {
            this.f2292a = account;
            return this;
        }

        public final a a(String str) {
            this.f = str;
            return this;
        }

        public final a a(Collection<Scope> collection) {
            if (this.f2293b == null) {
                this.f2293b = new android.support.v4.h.b<>();
            }
            this.f2293b.addAll(collection);
            return this;
        }

        public final f a() {
            return new f(this.f2292a, this.f2293b, this.c, this.d, this.e, this.f, this.g, this.h);
        }

        public final a b(String str) {
            this.g = str;
            return this;
        }
    }

    public static final class b {

        /* renamed from: a reason: collision with root package name */
        public final Set<Scope> f2294a;
    }

    public f(Account account, Set<Scope> set, Map<com.google.android.gms.common.api.a<?>, b> map, int i2, View view, String str, String str2, c cVar) {
        this.f2290a = account;
        this.f2291b = set == null ? Collections.EMPTY_SET : Collections.unmodifiableSet(set);
        if (map == null) {
            map = Collections.EMPTY_MAP;
        }
        this.d = map;
        this.f = view;
        this.e = i2;
        this.g = str;
        this.h = str2;
        this.i = cVar;
        HashSet hashSet = new HashSet(this.f2291b);
        for (b bVar : this.d.values()) {
            hashSet.addAll(bVar.f2294a);
        }
        this.c = Collections.unmodifiableSet(hashSet);
    }

    public final Account a() {
        return this.f2290a;
    }

    public final void a(Integer num) {
        this.j = num;
    }

    public final Account b() {
        return this.f2290a != null ? this.f2290a : new Account("<<default account>>", "com.google");
    }

    public final Set<Scope> c() {
        return this.f2291b;
    }

    public final Set<Scope> d() {
        return this.c;
    }

    public final String e() {
        return this.g;
    }

    public final String f() {
        return this.h;
    }

    public final c g() {
        return this.i;
    }

    public final Integer h() {
        return this.j;
    }
}
