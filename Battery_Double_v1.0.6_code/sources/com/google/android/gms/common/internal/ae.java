package com.google.android.gms.common.internal;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.b;

public class ae implements Creator<ad> {
    /* renamed from: a */
    public ad createFromParcel(Parcel parcel) {
        b bVar = null;
        boolean z = false;
        int b2 = com.google.android.gms.common.internal.a.b.b(parcel);
        boolean z2 = false;
        IBinder iBinder = null;
        int i = 0;
        while (parcel.dataPosition() < b2) {
            int a2 = com.google.android.gms.common.internal.a.b.a(parcel);
            switch (com.google.android.gms.common.internal.a.b.a(a2)) {
                case 1:
                    i = com.google.android.gms.common.internal.a.b.d(parcel, a2);
                    break;
                case 2:
                    iBinder = com.google.android.gms.common.internal.a.b.l(parcel, a2);
                    break;
                case 3:
                    bVar = (b) com.google.android.gms.common.internal.a.b.a(parcel, a2, b.CREATOR);
                    break;
                case 4:
                    z2 = com.google.android.gms.common.internal.a.b.c(parcel, a2);
                    break;
                case 5:
                    z = com.google.android.gms.common.internal.a.b.c(parcel, a2);
                    break;
                default:
                    com.google.android.gms.common.internal.a.b.b(parcel, a2);
                    break;
            }
        }
        com.google.android.gms.common.internal.a.b.r(parcel, b2);
        return new ad(i, iBinder, bVar, z2, z);
    }

    /* renamed from: a */
    public ad[] newArray(int i) {
        return new ad[i];
    }
}
