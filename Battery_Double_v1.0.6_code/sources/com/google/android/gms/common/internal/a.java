package com.google.android.gms.common.internal;

import android.accounts.Account;
import android.content.Context;
import android.os.Binder;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.common.n;

public class a extends com.google.android.gms.common.internal.r.a {

    /* renamed from: a reason: collision with root package name */
    private Account f2253a;

    /* renamed from: b reason: collision with root package name */
    private Context f2254b;
    private int c;

    public static Account a(r rVar) {
        Account account = null;
        if (rVar != null) {
            long clearCallingIdentity = Binder.clearCallingIdentity();
            try {
                account = rVar.a();
            } catch (RemoteException e) {
                Log.w("AccountAccessor", "Remote account accessor probably died");
            } finally {
                Binder.restoreCallingIdentity(clearCallingIdentity);
            }
        }
        return account;
    }

    public Account a() {
        int callingUid = Binder.getCallingUid();
        if (callingUid == this.c) {
            return this.f2253a;
        }
        if (n.isGooglePlayServicesUid(this.f2254b, callingUid)) {
            this.c = callingUid;
            return this.f2253a;
        }
        throw new SecurityException("Caller is not GooglePlayServices");
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof a)) {
            return false;
        }
        return this.f2253a.equals(((a) obj).f2253a);
    }
}
