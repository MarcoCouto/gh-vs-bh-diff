package com.google.android.gms.common.internal;

import android.content.Context;
import android.util.SparseIntArray;
import com.google.android.gms.common.api.a.f;
import com.google.android.gms.common.g;
import com.google.android.gms.common.h;

public class q {

    /* renamed from: a reason: collision with root package name */
    private final SparseIntArray f2308a;

    /* renamed from: b reason: collision with root package name */
    private h f2309b;

    public q() {
        this(g.a());
    }

    public q(h hVar) {
        this.f2308a = new SparseIntArray();
        aa.a(hVar);
        this.f2309b = hVar;
    }

    public int a(Context context, f fVar) {
        int i;
        aa.a(context);
        aa.a(fVar);
        if (!fVar.e()) {
            return 0;
        }
        int g = fVar.g();
        int i2 = this.f2308a.get(g, -1);
        if (i2 != -1) {
            return i2;
        }
        int i3 = 0;
        while (true) {
            if (i3 >= this.f2308a.size()) {
                i = i2;
                break;
            }
            int keyAt = this.f2308a.keyAt(i3);
            if (keyAt > g && this.f2308a.get(keyAt) == 0) {
                i = 0;
                break;
            }
            i3++;
        }
        if (i == -1) {
            i = this.f2309b.b(context, g);
        }
        this.f2308a.put(g, i);
        return i;
    }

    public void a() {
        this.f2308a.clear();
    }
}
