package com.google.android.gms.common.internal;

import android.accounts.Account;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.a.b;

public class d implements Creator<c> {
    /* renamed from: a */
    public c createFromParcel(Parcel parcel) {
        Account account = null;
        int b2 = b.b(parcel);
        int i = 0;
        Integer num = null;
        Integer num2 = null;
        Scope[] scopeArr = null;
        IBinder iBinder = null;
        while (parcel.dataPosition() < b2) {
            int a2 = b.a(parcel);
            switch (b.a(a2)) {
                case 1:
                    i = b.d(parcel, a2);
                    break;
                case 2:
                    iBinder = b.l(parcel, a2);
                    break;
                case 3:
                    scopeArr = (Scope[]) b.b(parcel, a2, Scope.CREATOR);
                    break;
                case 4:
                    num2 = b.e(parcel, a2);
                    break;
                case 5:
                    num = b.e(parcel, a2);
                    break;
                case 6:
                    account = (Account) b.a(parcel, a2, Account.CREATOR);
                    break;
                default:
                    b.b(parcel, a2);
                    break;
            }
        }
        b.r(parcel, b2);
        return new c(i, iBinder, scopeArr, num2, num, account);
    }

    /* renamed from: a */
    public c[] newArray(int i) {
        return new c[i];
    }
}
