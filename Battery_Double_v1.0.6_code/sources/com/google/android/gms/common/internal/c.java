package com.google.android.gms.common.internal;

import android.accounts.Account;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.a.a;

public class c extends a {
    public static final Creator<c> CREATOR = new d();

    /* renamed from: a reason: collision with root package name */
    private final int f2274a;
    @Deprecated

    /* renamed from: b reason: collision with root package name */
    private final IBinder f2275b;
    private final Scope[] c;
    private Integer d;
    private Integer e;
    private Account f;

    c(int i, IBinder iBinder, Scope[] scopeArr, Integer num, Integer num2, Account account) {
        this.f2274a = i;
        this.f2275b = iBinder;
        this.c = scopeArr;
        this.d = num;
        this.e = num2;
        this.f = account;
    }

    public void writeToParcel(Parcel parcel, int i) {
        int a2 = com.google.android.gms.common.internal.a.c.a(parcel);
        com.google.android.gms.common.internal.a.c.a(parcel, 1, this.f2274a);
        com.google.android.gms.common.internal.a.c.a(parcel, 2, this.f2275b, false);
        com.google.android.gms.common.internal.a.c.a(parcel, 3, (T[]) this.c, i, false);
        com.google.android.gms.common.internal.a.c.a(parcel, 4, this.d, false);
        com.google.android.gms.common.internal.a.c.a(parcel, 5, this.e, false);
        com.google.android.gms.common.internal.a.c.a(parcel, 6, (Parcelable) this.f, i, false);
        com.google.android.gms.common.internal.a.c.a(parcel, a2);
    }
}
