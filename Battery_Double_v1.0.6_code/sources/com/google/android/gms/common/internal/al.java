package com.google.android.gms.common.internal;

import android.content.Intent;
import com.google.android.gms.common.api.internal.f;

final class al extends j {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ Intent f2266a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ f f2267b;
    private final /* synthetic */ int c;

    al(Intent intent, f fVar, int i) {
        this.f2266a = intent;
        this.f2267b = fVar;
        this.c = i;
    }

    public final void a() {
        if (this.f2266a != null) {
            this.f2267b.a(this.f2266a, this.c);
        }
    }
}
