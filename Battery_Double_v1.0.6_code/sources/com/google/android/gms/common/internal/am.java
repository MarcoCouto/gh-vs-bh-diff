package com.google.android.gms.common.internal;

import android.os.Bundle;
import com.google.android.gms.common.api.f;
import com.google.android.gms.common.internal.e.a;

final class am implements a {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ f.a f2268a;

    am(f.a aVar) {
        this.f2268a = aVar;
    }

    public final void a(int i) {
        this.f2268a.a(i);
    }

    public final void a(Bundle bundle) {
        this.f2268a.a(bundle);
    }
}
