package com.google.android.gms.common.internal;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.e;
import com.google.android.gms.common.internal.a.a;
import com.google.android.gms.common.internal.a.c;

public class h extends a {
    public static final Creator<h> CREATOR = new i();

    /* renamed from: a reason: collision with root package name */
    private Bundle f2296a;

    /* renamed from: b reason: collision with root package name */
    private e[] f2297b;

    public h() {
    }

    h(Bundle bundle, e[] eVarArr) {
        this.f2296a = bundle;
        this.f2297b = eVarArr;
    }

    public Bundle a() {
        return this.f2296a;
    }

    public e[] b() {
        return this.f2297b;
    }

    public void writeToParcel(Parcel parcel, int i) {
        int a2 = c.a(parcel);
        c.a(parcel, 1, this.f2296a, false);
        c.a(parcel, 2, (T[]) this.f2297b, i, false);
        c.a(parcel, a2);
    }
}
