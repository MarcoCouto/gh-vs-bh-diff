package com.google.android.gms.common.internal;

import android.accounts.Account;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.e;
import com.google.android.gms.common.internal.a.b;

public class l implements Creator<k> {
    /* renamed from: a */
    public k createFromParcel(Parcel parcel) {
        boolean z = false;
        e[] eVarArr = null;
        int b2 = b.b(parcel);
        e[] eVarArr2 = null;
        Account account = null;
        Bundle bundle = null;
        Scope[] scopeArr = null;
        IBinder iBinder = null;
        String str = null;
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        while (parcel.dataPosition() < b2) {
            int a2 = b.a(parcel);
            switch (b.a(a2)) {
                case 1:
                    i3 = b.d(parcel, a2);
                    break;
                case 2:
                    i2 = b.d(parcel, a2);
                    break;
                case 3:
                    i = b.d(parcel, a2);
                    break;
                case 4:
                    str = b.k(parcel, a2);
                    break;
                case 5:
                    iBinder = b.l(parcel, a2);
                    break;
                case 6:
                    scopeArr = (Scope[]) b.b(parcel, a2, Scope.CREATOR);
                    break;
                case 7:
                    bundle = b.m(parcel, a2);
                    break;
                case 8:
                    account = (Account) b.a(parcel, a2, Account.CREATOR);
                    break;
                case 10:
                    eVarArr2 = (e[]) b.b(parcel, a2, e.CREATOR);
                    break;
                case 11:
                    eVarArr = (e[]) b.b(parcel, a2, e.CREATOR);
                    break;
                case 12:
                    z = b.c(parcel, a2);
                    break;
                default:
                    b.b(parcel, a2);
                    break;
            }
        }
        b.r(parcel, b2);
        return new k(i3, i2, i, str, iBinder, scopeArr, bundle, account, eVarArr2, eVarArr, z);
    }

    /* renamed from: a */
    public k[] newArray(int i) {
        return new k[i];
    }
}
