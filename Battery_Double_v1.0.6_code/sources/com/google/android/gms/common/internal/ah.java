package com.google.android.gms.common.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.a.a;
import com.google.android.gms.common.internal.a.c;

@Deprecated
public class ah extends a {
    public static final Creator<ah> CREATOR = new ai();

    /* renamed from: a reason: collision with root package name */
    private final int f2261a;

    ah(int i) {
        this.f2261a = i;
    }

    public void writeToParcel(Parcel parcel, int i) {
        int a2 = c.a(parcel);
        c.a(parcel, 1, this.f2261a);
        c.a(parcel, a2);
    }
}
