package com.google.android.gms.common.internal;

import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.google.android.gms.internal.d.b;
import com.google.android.gms.internal.d.c;

public interface u extends IInterface {

    public static abstract class a extends b implements u {

        /* renamed from: com.google.android.gms.common.internal.u$a$a reason: collision with other inner class name */
        public static class C0059a extends com.google.android.gms.internal.d.a implements u {
            C0059a(IBinder iBinder) {
                super(iBinder, "com.google.android.gms.common.internal.IGmsCallbacks");
            }

            public void a(int i, Bundle bundle) throws RemoteException {
                Parcel d = d();
                d.writeInt(i);
                c.a(d, (Parcelable) bundle);
                b(2, d);
            }

            public void a(int i, IBinder iBinder, Bundle bundle) throws RemoteException {
                Parcel d = d();
                d.writeInt(i);
                d.writeStrongBinder(iBinder);
                c.a(d, (Parcelable) bundle);
                b(1, d);
            }

            public void a(int i, IBinder iBinder, h hVar) throws RemoteException {
                Parcel d = d();
                d.writeInt(i);
                d.writeStrongBinder(iBinder);
                c.a(d, (Parcelable) hVar);
                b(3, d);
            }
        }

        public a() {
            super("com.google.android.gms.common.internal.IGmsCallbacks");
        }

        public static u a(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.common.internal.IGmsCallbacks");
            return queryLocalInterface instanceof u ? (u) queryLocalInterface : new C0059a(iBinder);
        }

        /* access modifiers changed from: protected */
        public boolean a(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
            switch (i) {
                case 1:
                    a(parcel.readInt(), parcel.readStrongBinder(), (Bundle) c.a(parcel, Bundle.CREATOR));
                    break;
                case 2:
                    a(parcel.readInt(), (Bundle) c.a(parcel, Bundle.CREATOR));
                    break;
                case 3:
                    a(parcel.readInt(), parcel.readStrongBinder(), (h) c.a(parcel, h.CREATOR));
                    break;
                default:
                    return false;
            }
            parcel2.writeNoException();
            return true;
        }
    }

    void a(int i, Bundle bundle) throws RemoteException;

    void a(int i, IBinder iBinder, Bundle bundle) throws RemoteException;

    void a(int i, IBinder iBinder, h hVar) throws RemoteException;
}
