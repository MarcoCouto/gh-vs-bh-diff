package com.google.android.gms.common.internal;

import android.accounts.Account;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.e;
import com.google.android.gms.common.h;
import com.google.android.gms.common.internal.a.a;
import com.google.android.gms.common.internal.a.c;
import java.util.Collection;

public class k extends a {
    public static final Creator<k> CREATOR = new l();

    /* renamed from: a reason: collision with root package name */
    private final int f2298a;

    /* renamed from: b reason: collision with root package name */
    private final int f2299b;
    private int c;
    private String d;
    private IBinder e;
    private Scope[] f;
    private Bundle g;
    private Account h;
    private e[] i;
    private e[] j;
    private boolean k;

    public k(int i2) {
        this.f2298a = 4;
        this.c = h.f2249b;
        this.f2299b = i2;
        this.k = true;
    }

    k(int i2, int i3, int i4, String str, IBinder iBinder, Scope[] scopeArr, Bundle bundle, Account account, e[] eVarArr, e[] eVarArr2, boolean z) {
        this.f2298a = i2;
        this.f2299b = i3;
        this.c = i4;
        if ("com.google.android.gms".equals(str)) {
            this.d = "com.google.android.gms";
        } else {
            this.d = str;
        }
        if (i2 < 2) {
            this.h = a(iBinder);
        } else {
            this.e = iBinder;
            this.h = account;
        }
        this.f = scopeArr;
        this.g = bundle;
        this.i = eVarArr;
        this.j = eVarArr2;
        this.k = z;
    }

    private static Account a(IBinder iBinder) {
        if (iBinder != null) {
            return a.a(r.a.a(iBinder));
        }
        return null;
    }

    public k a(Account account) {
        this.h = account;
        return this;
    }

    public k a(Bundle bundle) {
        this.g = bundle;
        return this;
    }

    public k a(r rVar) {
        if (rVar != null) {
            this.e = rVar.asBinder();
        }
        return this;
    }

    public k a(String str) {
        this.d = str;
        return this;
    }

    public k a(Collection<Scope> collection) {
        this.f = (Scope[]) collection.toArray(new Scope[collection.size()]);
        return this;
    }

    public k a(e[] eVarArr) {
        this.i = eVarArr;
        return this;
    }

    public k b(e[] eVarArr) {
        this.j = eVarArr;
        return this;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        int a2 = c.a(parcel);
        c.a(parcel, 1, this.f2298a);
        c.a(parcel, 2, this.f2299b);
        c.a(parcel, 3, this.c);
        c.a(parcel, 4, this.d, false);
        c.a(parcel, 5, this.e, false);
        c.a(parcel, 6, (T[]) this.f, i2, false);
        c.a(parcel, 7, this.g, false);
        c.a(parcel, 8, (Parcelable) this.h, i2, false);
        c.a(parcel, 10, (T[]) this.i, i2, false);
        c.a(parcel, 11, (T[]) this.j, i2, false);
        c.a(parcel, 12, this.k);
        c.a(parcel, a2);
    }
}
