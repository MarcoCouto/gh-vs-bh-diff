package com.google.android.gms.common;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.os.Bundle;
import android.support.v4.a.h;
import android.support.v4.a.n;
import com.google.android.gms.common.internal.aa;

public class q extends h {
    private Dialog ae = null;
    private OnCancelListener af = null;

    public static q a(Dialog dialog, OnCancelListener onCancelListener) {
        q qVar = new q();
        Dialog dialog2 = (Dialog) aa.a(dialog, (Object) "Cannot display null dialog");
        dialog2.setOnCancelListener(null);
        dialog2.setOnDismissListener(null);
        qVar.ae = dialog2;
        if (onCancelListener != null) {
            qVar.af = onCancelListener;
        }
        return qVar;
    }

    public void a(n nVar, String str) {
        super.a(nVar, str);
    }

    public Dialog c(Bundle bundle) {
        if (this.ae == null) {
            b(false);
        }
        return this.ae;
    }

    public void onCancel(DialogInterface dialogInterface) {
        if (this.af != null) {
            this.af.onCancel(dialogInterface);
        }
    }
}
