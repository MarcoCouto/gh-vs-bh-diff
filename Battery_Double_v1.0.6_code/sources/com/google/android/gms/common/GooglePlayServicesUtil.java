package com.google.android.gms.common;

import android.app.Activity;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface.OnCancelListener;
import android.content.res.Resources;
import android.support.v4.a.i;
import com.google.android.gms.common.internal.j;

public final class GooglePlayServicesUtil extends n {
    public static final String GMS_ERROR_DIALOG = "GooglePlayServicesErrorDialog";
    @Deprecated
    public static final String GOOGLE_PLAY_SERVICES_PACKAGE = "com.google.android.gms";
    @Deprecated
    public static final int GOOGLE_PLAY_SERVICES_VERSION_CODE = n.GOOGLE_PLAY_SERVICES_VERSION_CODE;
    public static final String GOOGLE_PLAY_STORE_PACKAGE = "com.android.vending";

    private GooglePlayServicesUtil() {
    }

    @Deprecated
    public static Dialog getErrorDialog(int i, Activity activity, int i2) {
        return getErrorDialog(i, activity, i2, null);
    }

    @Deprecated
    public static Dialog getErrorDialog(int i, Activity activity, int i2, OnCancelListener onCancelListener) {
        if (n.isPlayServicesPossiblyUpdating(activity, i)) {
            i = 18;
        }
        return g.a().a(activity, i, i2, onCancelListener);
    }

    @Deprecated
    public static PendingIntent getErrorPendingIntent(int i, Context context, int i2) {
        return n.getErrorPendingIntent(i, context, i2);
    }

    @Deprecated
    public static String getErrorString(int i) {
        return n.getErrorString(i);
    }

    public static Context getRemoteContext(Context context) {
        return n.getRemoteContext(context);
    }

    public static Resources getRemoteResource(Context context) {
        return n.getRemoteResource(context);
    }

    @Deprecated
    public static int isGooglePlayServicesAvailable(Context context) {
        return n.isGooglePlayServicesAvailable(context);
    }

    @Deprecated
    public static boolean isUserRecoverableError(int i) {
        return n.isUserRecoverableError(i);
    }

    @Deprecated
    public static boolean showErrorDialogFragment(int i, Activity activity, int i2) {
        return showErrorDialogFragment(i, activity, i2, null);
    }

    @Deprecated
    public static boolean showErrorDialogFragment(int i, Activity activity, int i2, OnCancelListener onCancelListener) {
        return showErrorDialogFragment(i, activity, null, i2, onCancelListener);
    }

    public static boolean showErrorDialogFragment(int i, Activity activity, i iVar, int i2, OnCancelListener onCancelListener) {
        if (n.isPlayServicesPossiblyUpdating(activity, i)) {
            i = 18;
        }
        g a2 = g.a();
        if (iVar == null) {
            return a2.b(activity, i, i2, onCancelListener);
        }
        Dialog a3 = g.a((Context) activity, i, j.a(iVar, g.a().b(activity, i, "d"), i2), onCancelListener);
        if (a3 == null) {
            return false;
        }
        g.a(activity, a3, GMS_ERROR_DIALOG, onCancelListener);
        return true;
    }

    @Deprecated
    public static void showErrorNotification(int i, Context context) {
        g a2 = g.a();
        if (n.isPlayServicesPossiblyUpdating(context, i) || n.isPlayStorePossiblyUpdating(context, i)) {
            a2.c(context);
        } else {
            a2.a(context, i);
        }
    }
}
