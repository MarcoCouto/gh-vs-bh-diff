package com.google.android.gms.common;

import java.lang.ref.WeakReference;

abstract class t extends a {

    /* renamed from: b reason: collision with root package name */
    private static final WeakReference<byte[]> f2323b = new WeakReference<>(null);

    /* renamed from: a reason: collision with root package name */
    private WeakReference<byte[]> f2324a = f2323b;

    t(byte[] bArr) {
        super(bArr);
    }

    /* access modifiers changed from: 0000 */
    public final byte[] a() {
        byte[] bArr;
        synchronized (this) {
            bArr = (byte[]) this.f2324a.get();
            if (bArr == null) {
                bArr = d();
                this.f2324a = new WeakReference<>(bArr);
            }
        }
        return bArr;
    }

    /* access modifiers changed from: protected */
    public abstract byte[] d();
}
