package com.google.android.gms.common;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.b.b;
import com.google.android.gms.common.internal.a.a;
import com.google.android.gms.common.internal.a.c;
import com.google.android.gms.common.internal.t;

public class j extends a {
    public static final Creator<j> CREATOR = new k();

    /* renamed from: a reason: collision with root package name */
    private final String f2315a;

    /* renamed from: b reason: collision with root package name */
    private final a f2316b;
    private final boolean c;

    j(String str, IBinder iBinder, boolean z) {
        this.f2315a = str;
        this.f2316b = a(iBinder);
        this.c = z;
    }

    j(String str, a aVar, boolean z) {
        this.f2315a = str;
        this.f2316b = aVar;
        this.c = z;
    }

    private static a a(IBinder iBinder) {
        s sVar;
        if (iBinder == null) {
            return null;
        }
        try {
            com.google.android.gms.b.a b2 = t.a.a(iBinder).b();
            byte[] bArr = b2 == null ? null : (byte[]) b.a(b2);
            if (bArr != null) {
                sVar = new s(bArr);
            } else {
                Log.e("GoogleCertificatesQuery", "Could not unwrap certificate");
                sVar = null;
            }
            return sVar;
        } catch (RemoteException e) {
            Log.e("GoogleCertificatesQuery", "Could not unwrap certificate", e);
            return null;
        }
    }

    public String a() {
        return this.f2315a;
    }

    public IBinder b() {
        if (this.f2316b != null) {
            return this.f2316b.asBinder();
        }
        Log.w("GoogleCertificatesQuery", "certificate binder is null");
        return null;
    }

    public boolean c() {
        return this.c;
    }

    public void writeToParcel(Parcel parcel, int i) {
        int a2 = c.a(parcel);
        c.a(parcel, 1, a(), false);
        c.a(parcel, 2, b(), false);
        c.a(parcel, 3, c());
        c.a(parcel, a2);
    }
}
