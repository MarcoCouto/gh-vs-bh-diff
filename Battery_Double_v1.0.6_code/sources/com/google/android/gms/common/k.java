package com.google.android.gms.common;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.a.b;

public class k implements Creator<j> {
    /* renamed from: a */
    public j createFromParcel(Parcel parcel) {
        IBinder iBinder = null;
        int b2 = b.b(parcel);
        boolean z = false;
        String str = null;
        while (parcel.dataPosition() < b2) {
            int a2 = b.a(parcel);
            switch (b.a(a2)) {
                case 1:
                    str = b.k(parcel, a2);
                    break;
                case 2:
                    iBinder = b.l(parcel, a2);
                    break;
                case 3:
                    z = b.c(parcel, a2);
                    break;
                default:
                    b.b(parcel, a2);
                    break;
            }
        }
        b.r(parcel, b2);
        return new j(str, iBinder, z);
    }

    /* renamed from: a */
    public j[] newArray(int i) {
        return new j[i];
    }
}
