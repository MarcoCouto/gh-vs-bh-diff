package com.google.android.gms.common;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.os.Bundle;
import com.google.android.gms.common.internal.aa;

public class d extends DialogFragment {

    /* renamed from: a reason: collision with root package name */
    private Dialog f2241a = null;

    /* renamed from: b reason: collision with root package name */
    private OnCancelListener f2242b = null;

    public static d a(Dialog dialog, OnCancelListener onCancelListener) {
        d dVar = new d();
        Dialog dialog2 = (Dialog) aa.a(dialog, (Object) "Cannot display null dialog");
        dialog2.setOnCancelListener(null);
        dialog2.setOnDismissListener(null);
        dVar.f2241a = dialog2;
        if (onCancelListener != null) {
            dVar.f2242b = onCancelListener;
        }
        return dVar;
    }

    public void onCancel(DialogInterface dialogInterface) {
        if (this.f2242b != null) {
            this.f2242b.onCancel(dialogInterface);
        }
    }

    public Dialog onCreateDialog(Bundle bundle) {
        if (this.f2241a == null) {
            setShowsDialog(false);
        }
        return this.f2241a;
    }

    public void show(FragmentManager fragmentManager, String str) {
        super.show(fragmentManager, str);
    }
}
