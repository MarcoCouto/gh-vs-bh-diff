package com.google.android.gms.common;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.util.Log;
import com.google.android.gms.common.b.c;
import com.google.android.gms.common.internal.aa;

public class o {

    /* renamed from: a reason: collision with root package name */
    private static o f2319a;

    /* renamed from: b reason: collision with root package name */
    private final Context f2320b;

    private o(Context context) {
        this.f2320b = context.getApplicationContext();
    }

    private static a a(PackageInfo packageInfo, a... aVarArr) {
        if (packageInfo.signatures == null) {
            return null;
        }
        if (packageInfo.signatures.length != 1) {
            Log.w("GoogleSignatureVerifier", "Package has more than one signature.");
            return null;
        }
        s sVar = new s(packageInfo.signatures[0].toByteArray());
        for (int i = 0; i < aVarArr.length; i++) {
            if (aVarArr[i].equals(sVar)) {
                return aVarArr[i];
            }
        }
        return null;
    }

    public static o a(Context context) {
        aa.a(context);
        synchronized (o.class) {
            if (f2319a == null) {
                i.a(context);
                f2319a = new o(context);
            }
        }
        return f2319a;
    }

    private final x a(String str) {
        try {
            return b(c.b(this.f2320b).b(str, 64));
        } catch (NameNotFoundException e) {
            String str2 = "no pkg ";
            String valueOf = String.valueOf(str);
            return x.a(valueOf.length() != 0 ? str2.concat(valueOf) : new String(str2));
        }
    }

    private final x b(int i) {
        String[] a2 = c.b(this.f2320b).a(i);
        if (a2 == null || a2.length == 0) {
            return x.a("no pkgs");
        }
        x xVar = null;
        for (String a3 : a2) {
            xVar = a(a3);
            if (xVar.f2342a) {
                return xVar;
            }
        }
        return xVar;
    }

    private final x b(PackageInfo packageInfo) {
        boolean honorsDebugCertificates = n.honorsDebugCertificates(this.f2320b);
        if (packageInfo == null) {
            return x.a("null pkg");
        }
        if (packageInfo.signatures.length != 1) {
            return x.a("single cert required");
        }
        s sVar = new s(packageInfo.signatures[0].toByteArray());
        String str = packageInfo.packageName;
        x a2 = i.a(str, sVar, honorsDebugCertificates);
        return (!a2.f2342a || packageInfo.applicationInfo == null || (packageInfo.applicationInfo.flags & 2) == 0) ? a2 : (!honorsDebugCertificates || i.a(str, sVar, false).f2342a) ? x.a("debuggable release cert app rejected") : a2;
    }

    public boolean a(int i) {
        x b2 = b(i);
        b2.c();
        return b2.f2342a;
    }

    public boolean a(PackageInfo packageInfo) {
        if (packageInfo == null) {
            return false;
        }
        if (a(packageInfo, false)) {
            return true;
        }
        if (!a(packageInfo, true)) {
            return false;
        }
        if (n.honorsDebugCertificates(this.f2320b)) {
            return true;
        }
        Log.w("GoogleSignatureVerifier", "Test-keys aren't accepted on this build.");
        return false;
    }

    public boolean a(PackageInfo packageInfo, boolean z) {
        a a2;
        if (!(packageInfo == null || packageInfo.signatures == null)) {
            if (z) {
                a2 = a(packageInfo, u.f2325a);
            } else {
                a2 = a(packageInfo, u.f2325a[0]);
            }
            if (a2 != null) {
                return true;
            }
        }
        return false;
    }
}
