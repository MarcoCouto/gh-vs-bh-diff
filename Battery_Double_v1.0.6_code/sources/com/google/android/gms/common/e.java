package com.google.android.gms.common;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.a.a;
import com.google.android.gms.common.internal.a.c;
import com.google.android.gms.common.internal.z;

public class e extends a {
    public static final Creator<e> CREATOR = new f();

    /* renamed from: a reason: collision with root package name */
    private final String f2243a;
    @Deprecated

    /* renamed from: b reason: collision with root package name */
    private final int f2244b;
    private final long c;

    public e(String str, int i, long j) {
        this.f2243a = str;
        this.f2244b = i;
        this.c = j;
    }

    public String a() {
        return this.f2243a;
    }

    public long b() {
        return this.c == -1 ? (long) this.f2244b : this.c;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof e)) {
            return false;
        }
        e eVar = (e) obj;
        return ((a() != null && a().equals(eVar.a())) || (a() == null && eVar.a() == null)) && b() == eVar.b();
    }

    public int hashCode() {
        return z.a(a(), Long.valueOf(b()));
    }

    public String toString() {
        return z.a((Object) this).a("name", a()).a("version", Long.valueOf(b())).toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        int a2 = c.a(parcel);
        c.a(parcel, 1, a(), false);
        c.a(parcel, 2, this.f2244b);
        c.a(parcel, 3, b());
        c.a(parcel, a2);
    }
}
