package com.google.android.gms.common.api.internal;

import android.support.v4.h.a;
import com.google.android.gms.common.api.c;
import com.google.android.gms.common.b;
import com.google.android.gms.d.h;
import java.util.Map;
import java.util.Set;

public final class ai {

    /* renamed from: a reason: collision with root package name */
    private final a<ah<?>, b> f2185a;

    /* renamed from: b reason: collision with root package name */
    private final a<ah<?>, String> f2186b;
    private final h<Map<ah<?>, String>> c;
    private int d;
    private boolean e;

    public final Set<ah<?>> a() {
        return this.f2185a.keySet();
    }

    public final void a(ah<?> ahVar, b bVar, String str) {
        this.f2185a.put(ahVar, bVar);
        this.f2186b.put(ahVar, str);
        this.d--;
        if (!bVar.b()) {
            this.e = true;
        }
        if (this.d != 0) {
            return;
        }
        if (this.e) {
            this.c.a((Exception) new c(this.f2185a));
            return;
        }
        this.c.a(this.f2186b);
    }
}
