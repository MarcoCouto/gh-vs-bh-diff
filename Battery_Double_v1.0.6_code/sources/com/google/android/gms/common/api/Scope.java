package com.google.android.gms.common.api;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.a.a;
import com.google.android.gms.common.internal.a.c;
import com.google.android.gms.common.internal.aa;

public final class Scope extends a implements ReflectedParcelable {
    public static final Creator<Scope> CREATOR = new l();

    /* renamed from: a reason: collision with root package name */
    private final int f2158a;

    /* renamed from: b reason: collision with root package name */
    private final String f2159b;

    Scope(int i, String str) {
        aa.a(str, (Object) "scopeUri must not be null or empty");
        this.f2158a = i;
        this.f2159b = str;
    }

    public Scope(String str) {
        this(1, str);
    }

    public final String a() {
        return this.f2159b;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Scope)) {
            return false;
        }
        return this.f2159b.equals(((Scope) obj).f2159b);
    }

    public final int hashCode() {
        return this.f2159b.hashCode();
    }

    public final String toString() {
        return this.f2159b;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = c.a(parcel);
        c.a(parcel, 1, this.f2158a);
        c.a(parcel, 2, a(), false);
        c.a(parcel, a2);
    }
}
