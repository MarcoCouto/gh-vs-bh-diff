package com.google.android.gms.common.api;

import android.accounts.Account;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.a.d;
import com.google.android.gms.common.api.a.d.C0052a;
import com.google.android.gms.common.api.a.d.b;
import com.google.android.gms.common.api.a.f;
import com.google.android.gms.common.api.internal.ah;
import com.google.android.gms.common.api.internal.b.a;
import com.google.android.gms.common.api.internal.y;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;

public class e<O extends d> {

    /* renamed from: a reason: collision with root package name */
    private final Context f2166a;

    /* renamed from: b reason: collision with root package name */
    private final a<O> f2167b;
    private final O c;
    private final ah<O> d;
    private final int e;

    public f a(Looper looper, a<O> aVar) {
        return this.f2167b.a().a(this.f2166a, looper, c().a(), this.c, aVar, aVar);
    }

    public final ah<O> a() {
        return this.d;
    }

    public y a(Context context, Handler handler) {
        return new y(context, handler, c().a());
    }

    public final int b() {
        return this.e;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0023  */
    public com.google.android.gms.common.internal.f.a c() {
        Account account;
        Set emptySet;
        com.google.android.gms.common.internal.f.a aVar = new com.google.android.gms.common.internal.f.a();
        if (this.c instanceof b) {
            GoogleSignInAccount a2 = ((b) this.c).a();
            if (a2 != null) {
                account = a2.d();
                com.google.android.gms.common.internal.f.a a3 = aVar.a(account);
                if (this.c instanceof b) {
                    GoogleSignInAccount a4 = ((b) this.c).a();
                    if (a4 != null) {
                        emptySet = a4.l();
                        return a3.a((Collection<Scope>) emptySet).b(this.f2166a.getClass().getName()).a(this.f2166a.getPackageName());
                    }
                }
                emptySet = Collections.emptySet();
                return a3.a((Collection<Scope>) emptySet).b(this.f2166a.getClass().getName()).a(this.f2166a.getPackageName());
            }
        }
        account = this.c instanceof C0052a ? ((C0052a) this.c).a() : null;
        com.google.android.gms.common.internal.f.a a32 = aVar.a(account);
        if (this.c instanceof b) {
        }
        emptySet = Collections.emptySet();
        return a32.a((Collection<Scope>) emptySet).b(this.f2166a.getClass().getName()).a(this.f2166a.getPackageName());
    }
}
