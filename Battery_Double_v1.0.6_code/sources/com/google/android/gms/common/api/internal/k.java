package com.google.android.gms.common.api.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.a.b;
import com.google.android.gms.d.h;

public abstract class k<A extends b, L> {
    /* access modifiers changed from: protected */
    public abstract void a(A a2, h<Boolean> hVar) throws RemoteException;
}
