package com.google.android.gms.common.api.internal;

import android.os.DeadObjectException;
import android.os.RemoteException;
import android.os.TransactionTooLargeException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.b.a;
import com.google.android.gms.common.util.n;

public abstract class p {

    /* renamed from: a reason: collision with root package name */
    private final int f2218a;

    public p(int i) {
        this.f2218a = i;
    }

    /* access modifiers changed from: private */
    public static Status b(RemoteException remoteException) {
        StringBuilder sb = new StringBuilder();
        if (n.c() && (remoteException instanceof TransactionTooLargeException)) {
            sb.append("TransactionTooLargeException: ");
        }
        sb.append(remoteException.getLocalizedMessage());
        return new Status(8, sb.toString());
    }

    public abstract void a(Status status);

    public abstract void a(a<?> aVar) throws DeadObjectException;

    public abstract void a(l lVar, boolean z);

    public abstract void a(RuntimeException runtimeException);
}
