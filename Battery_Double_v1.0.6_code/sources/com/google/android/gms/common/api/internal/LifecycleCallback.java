package com.google.android.gms.common.api.internal;

import android.app.Activity;
import android.support.annotation.Keep;

public class LifecycleCallback {

    /* renamed from: a reason: collision with root package name */
    protected final f f2172a;

    @Keep
    private static f getChimeraLifecycleFragmentImpl(e eVar) {
        throw new IllegalStateException("Method not available in SDK.");
    }

    public final Activity a() {
        return this.f2172a.a();
    }
}
