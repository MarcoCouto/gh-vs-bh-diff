package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.a;
import com.google.android.gms.common.api.a.d;
import com.google.android.gms.common.internal.z;

public final class ah<O extends d> {

    /* renamed from: a reason: collision with root package name */
    private final boolean f2183a;

    /* renamed from: b reason: collision with root package name */
    private final int f2184b;
    private final a<O> c;
    private final O d;

    public final String a() {
        return this.c.b();
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ah)) {
            return false;
        }
        ah ahVar = (ah) obj;
        return !this.f2183a && !ahVar.f2183a && z.a(this.c, ahVar.c) && z.a(this.d, ahVar.d);
    }

    public final int hashCode() {
        return this.f2184b;
    }
}
