package com.google.android.gms.common.api.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.g.a;
import com.google.android.gms.d.h;

public final class ag extends aa<Boolean> {

    /* renamed from: b reason: collision with root package name */
    private final a<?> f2182b;

    public ag(a<?> aVar, h<Boolean> hVar) {
        super(4, hVar);
        this.f2182b = aVar;
    }

    public final /* bridge */ /* synthetic */ void a(Status status) {
        super.a(status);
    }

    public final /* bridge */ /* synthetic */ void a(l lVar, boolean z) {
    }

    public final /* bridge */ /* synthetic */ void a(RuntimeException runtimeException) {
        super.a(runtimeException);
    }

    public final void b(b.a<?> aVar) throws RemoteException {
        x xVar = (x) aVar.c().remove(this.f2182b);
        if (xVar != null) {
            xVar.f2229b.a(aVar.b(), this.f2175a);
            xVar.f2228a.a();
            return;
        }
        this.f2175a.b(Boolean.valueOf(false));
    }
}
