package com.google.android.gms.common.api.internal;

import android.os.DeadObjectException;
import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.a.b;
import com.google.android.gms.common.api.internal.b.a;
import com.google.android.gms.common.e;
import com.google.android.gms.d.h;

public final class af<ResultT> extends p {

    /* renamed from: a reason: collision with root package name */
    private final j<b, ResultT> f2180a;

    /* renamed from: b reason: collision with root package name */
    private final h<ResultT> f2181b;
    private final i c;

    public final void a(Status status) {
        this.f2181b.b(this.c.a(status));
    }

    public final void a(a<?> aVar) throws DeadObjectException {
        try {
            this.f2180a.a(aVar.b(), this.f2181b);
        } catch (DeadObjectException e) {
            throw e;
        } catch (RemoteException e2) {
            a(p.b(e2));
        } catch (RuntimeException e3) {
            a(e3);
        }
    }

    public final void a(l lVar, boolean z) {
        lVar.a(this.f2181b, z);
    }

    public final void a(RuntimeException runtimeException) {
        this.f2181b.b((Exception) runtimeException);
    }

    public final e[] a() {
        return this.f2180a.a();
    }

    public final boolean b() {
        return this.f2180a.b();
    }
}
