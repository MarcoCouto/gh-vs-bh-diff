package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.b;
import com.google.android.gms.common.internal.aa;

final class ak {

    /* renamed from: a reason: collision with root package name */
    private final int f2188a;

    /* renamed from: b reason: collision with root package name */
    private final b f2189b;

    ak(b bVar, int i) {
        aa.a(bVar);
        this.f2189b = bVar;
        this.f2188a = i;
    }

    /* access modifiers changed from: 0000 */
    public final int a() {
        return this.f2188a;
    }

    /* access modifiers changed from: 0000 */
    public final b b() {
        return this.f2189b;
    }
}
