package com.google.android.gms.common.api;

import android.support.v4.h.a;
import android.text.TextUtils;
import com.google.android.gms.common.api.internal.ah;
import com.google.android.gms.common.b;
import java.util.ArrayList;

public class c extends Exception {

    /* renamed from: a reason: collision with root package name */
    private final a<ah<?>, b> f2165a;

    public c(a<ah<?>, b> aVar) {
        this.f2165a = aVar;
    }

    public String getMessage() {
        ArrayList arrayList = new ArrayList();
        boolean z = true;
        for (ah ahVar : this.f2165a.keySet()) {
            b bVar = (b) this.f2165a.get(ahVar);
            if (bVar.b()) {
                z = false;
            }
            String a2 = ahVar.a();
            String valueOf = String.valueOf(bVar);
            arrayList.add(new StringBuilder(String.valueOf(a2).length() + 2 + String.valueOf(valueOf).length()).append(a2).append(": ").append(valueOf).toString());
        }
        StringBuilder sb = new StringBuilder();
        if (z) {
            sb.append("None of the queried APIs are available. ");
        } else {
            sb.append("Some of the queried APIs are unavailable. ");
        }
        sb.append(TextUtils.join("; ", arrayList));
        return sb.toString();
    }
}
