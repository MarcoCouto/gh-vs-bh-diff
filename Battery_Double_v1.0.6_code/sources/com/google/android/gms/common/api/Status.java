package com.google.android.gms.common.api;

import android.app.PendingIntent;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.a.a;
import com.google.android.gms.common.internal.a.c;
import com.google.android.gms.common.internal.z;

public final class Status extends a implements i, ReflectedParcelable {
    public static final Creator<Status> CREATOR = new m();

    /* renamed from: a reason: collision with root package name */
    public static final Status f2160a = new Status(0);

    /* renamed from: b reason: collision with root package name */
    public static final Status f2161b = new Status(14);
    public static final Status c = new Status(8);
    public static final Status d = new Status(15);
    public static final Status e = new Status(16);
    public static final Status f = new Status(18);
    private static final Status g = new Status(17);
    private final int h;
    private final int i;
    private final String j;
    private final PendingIntent k;

    public Status(int i2) {
        this(i2, null);
    }

    Status(int i2, int i3, String str, PendingIntent pendingIntent) {
        this.h = i2;
        this.i = i3;
        this.j = str;
        this.k = pendingIntent;
    }

    public Status(int i2, String str) {
        this(1, i2, str, null);
    }

    public final Status a() {
        return this;
    }

    public final String b() {
        return this.j;
    }

    public final boolean c() {
        return this.i <= 0;
    }

    public final int d() {
        return this.i;
    }

    public final String e() {
        return this.j != null ? this.j : d.a(this.i);
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof Status)) {
            return false;
        }
        Status status = (Status) obj;
        return this.h == status.h && this.i == status.i && z.a(this.j, status.j) && z.a(this.k, status.k);
    }

    public final int hashCode() {
        return z.a(Integer.valueOf(this.h), Integer.valueOf(this.i), this.j, this.k);
    }

    public final String toString() {
        return z.a((Object) this).a("statusCode", e()).a("resolution", this.k).toString();
    }

    public final void writeToParcel(Parcel parcel, int i2) {
        int a2 = c.a(parcel);
        c.a(parcel, 1, d());
        c.a(parcel, 2, b(), false);
        c.a(parcel, 3, (Parcelable) this.k, i2, false);
        c.a(parcel, 1000, this.h);
        c.a(parcel, a2);
    }
}
