package com.google.android.gms.common.api.internal;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.app.Application;
import android.app.Application.ActivityLifecycleCallbacks;
import android.content.ComponentCallbacks2;
import android.content.res.Configuration;
import android.os.Bundle;
import com.google.android.gms.common.util.n;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

public final class a implements ActivityLifecycleCallbacks, ComponentCallbacks2 {

    /* renamed from: a reason: collision with root package name */
    private static final a f2173a = new a();

    /* renamed from: b reason: collision with root package name */
    private final AtomicBoolean f2174b = new AtomicBoolean();
    private final AtomicBoolean c = new AtomicBoolean();
    private final ArrayList<C0054a> d = new ArrayList<>();
    private boolean e = false;

    /* renamed from: com.google.android.gms.common.api.internal.a$a reason: collision with other inner class name */
    public interface C0054a {
        void a(boolean z);
    }

    private a() {
    }

    public static a a() {
        return f2173a;
    }

    public static void a(Application application) {
        synchronized (f2173a) {
            if (!f2173a.e) {
                application.registerActivityLifecycleCallbacks(f2173a);
                application.registerComponentCallbacks(f2173a);
                f2173a.e = true;
            }
        }
    }

    private final void b(boolean z) {
        synchronized (f2173a) {
            ArrayList arrayList = this.d;
            int size = arrayList.size();
            int i = 0;
            while (i < size) {
                Object obj = arrayList.get(i);
                i++;
                ((C0054a) obj).a(z);
            }
        }
    }

    public final void a(C0054a aVar) {
        synchronized (f2173a) {
            this.d.add(aVar);
        }
    }

    @TargetApi(16)
    public final boolean a(boolean z) {
        if (!this.c.get()) {
            if (!n.d()) {
                return z;
            }
            RunningAppProcessInfo runningAppProcessInfo = new RunningAppProcessInfo();
            ActivityManager.getMyMemoryState(runningAppProcessInfo);
            if (!this.c.getAndSet(true) && runningAppProcessInfo.importance > 100) {
                this.f2174b.set(true);
            }
        }
        return b();
    }

    public final boolean b() {
        return this.f2174b.get();
    }

    public final void onActivityCreated(Activity activity, Bundle bundle) {
        boolean compareAndSet = this.f2174b.compareAndSet(true, false);
        this.c.set(true);
        if (compareAndSet) {
            b(false);
        }
    }

    public final void onActivityDestroyed(Activity activity) {
    }

    public final void onActivityPaused(Activity activity) {
    }

    public final void onActivityResumed(Activity activity) {
        boolean compareAndSet = this.f2174b.compareAndSet(true, false);
        this.c.set(true);
        if (compareAndSet) {
            b(false);
        }
    }

    public final void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
    }

    public final void onActivityStarted(Activity activity) {
    }

    public final void onActivityStopped(Activity activity) {
    }

    public final void onConfigurationChanged(Configuration configuration) {
    }

    public final void onLowMemory() {
    }

    public final void onTrimMemory(int i) {
        if (i == 20 && this.f2174b.compareAndSet(false, true)) {
            this.c.set(true);
            b(true);
        }
    }
}
