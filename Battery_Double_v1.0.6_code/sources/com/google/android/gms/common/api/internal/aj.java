package com.google.android.gms.common.api.internal;

import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.os.Handler;
import com.google.android.gms.common.b;
import com.google.android.gms.common.g;
import java.util.concurrent.atomic.AtomicReference;

public abstract class aj extends LifecycleCallback implements OnCancelListener {

    /* renamed from: b reason: collision with root package name */
    protected volatile boolean f2187b;
    protected final AtomicReference<ak> c;
    protected final g d;
    private final Handler e;

    private static int a(ak akVar) {
        if (akVar == null) {
            return -1;
        }
        return akVar.a();
    }

    /* access modifiers changed from: protected */
    public abstract void a(b bVar, int i);

    /* access modifiers changed from: protected */
    public abstract void b();

    public final void b(b bVar, int i) {
        ak akVar = new ak(bVar, i);
        if (this.c.compareAndSet(null, akVar)) {
            this.e.post(new al(this, akVar));
        }
    }

    /* access modifiers changed from: protected */
    public final void c() {
        this.c.set(null);
        b();
    }

    public void onCancel(DialogInterface dialogInterface) {
        a(new b(13, null), a((ak) this.c.get()));
        c();
    }
}
