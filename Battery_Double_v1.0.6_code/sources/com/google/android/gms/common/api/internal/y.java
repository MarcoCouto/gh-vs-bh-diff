package com.google.android.gms.common.api.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import com.google.android.gms.c.a.c;
import com.google.android.gms.c.a.m;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.a.C0051a;
import com.google.android.gms.common.api.f.a;
import com.google.android.gms.common.api.f.b;
import com.google.android.gms.common.internal.aa;
import com.google.android.gms.common.internal.ad;
import com.google.android.gms.common.internal.f;
import java.util.Set;

public final class y extends c implements a, b {

    /* renamed from: a reason: collision with root package name */
    private static C0051a<? extends com.google.android.gms.c.b, com.google.android.gms.c.c> f2230a = com.google.android.gms.c.a.c;

    /* renamed from: b reason: collision with root package name */
    private final Context f2231b;
    private final Handler c;
    private final C0051a<? extends com.google.android.gms.c.b, com.google.android.gms.c.c> d;
    private Set<Scope> e;
    private f f;
    private com.google.android.gms.c.b g;
    /* access modifiers changed from: private */
    public ac h;

    public y(Context context, Handler handler, f fVar) {
        this(context, handler, fVar, f2230a);
    }

    public y(Context context, Handler handler, f fVar, C0051a<? extends com.google.android.gms.c.b, com.google.android.gms.c.c> aVar) {
        this.f2231b = context;
        this.c = handler;
        this.f = (f) aa.a(fVar, (Object) "ClientSettings must not be null");
        this.e = fVar.c();
        this.d = aVar;
    }

    /* access modifiers changed from: private */
    public final void b(m mVar) {
        com.google.android.gms.common.b a2 = mVar.a();
        if (a2.b()) {
            ad b2 = mVar.b();
            com.google.android.gms.common.b b3 = b2.b();
            if (!b3.b()) {
                String valueOf = String.valueOf(b3);
                Log.wtf("SignInCoordinator", new StringBuilder(String.valueOf(valueOf).length() + 48).append("Sign-in succeeded with resolve account failure: ").append(valueOf).toString(), new Exception());
                this.h.b(b3);
                this.g.a();
                return;
            }
            this.h.a(b2.a(), this.e);
        } else {
            this.h.b(a2);
        }
        this.g.a();
    }

    public final void a() {
        if (this.g != null) {
            this.g.a();
        }
    }

    public final void a(int i) {
        this.g.a();
    }

    public final void a(Bundle bundle) {
        this.g.a(this);
    }

    public final void a(m mVar) {
        this.c.post(new ab(this, mVar));
    }

    public final void a(ac acVar) {
        if (this.g != null) {
            this.g.a();
        }
        this.f.a(Integer.valueOf(System.identityHashCode(this)));
        this.g = (com.google.android.gms.c.b) this.d.a(this.f2231b, this.c.getLooper(), this.f, this.f.g(), this, this);
        this.h = acVar;
        if (this.e == null || this.e.isEmpty()) {
            this.c.post(new z(this));
        } else {
            this.g.j();
        }
    }

    public final void a(com.google.android.gms.common.b bVar) {
        this.h.b(bVar);
    }
}
