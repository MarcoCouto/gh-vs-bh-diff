package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.internal.b.a;
import com.google.android.gms.common.b;
import java.util.Collections;

final class v implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ b f2224a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ c f2225b;

    v(c cVar, b bVar) {
        this.f2225b = cVar;
        this.f2224a = bVar;
    }

    public final void run() {
        if (this.f2224a.b()) {
            this.f2225b.f = true;
            if (this.f2225b.f2201b.d()) {
                this.f2225b.a();
            } else {
                this.f2225b.f2201b.a(null, Collections.emptySet());
            }
        } else {
            ((a) b.this.m.get(this.f2225b.c)).a(this.f2224a);
        }
    }
}
