package com.google.android.gms.common.api;

import android.os.Bundle;
import java.util.Collections;
import java.util.Set;
import java.util.WeakHashMap;

public abstract class f {

    /* renamed from: a reason: collision with root package name */
    private static final Set<f> f2168a = Collections.newSetFromMap(new WeakHashMap());

    public interface a {
        void a(int i);

        void a(Bundle bundle);
    }

    public interface b {
        void a(com.google.android.gms.common.b bVar);
    }
}
