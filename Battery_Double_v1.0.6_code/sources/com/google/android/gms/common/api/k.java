package com.google.android.gms.common.api;

import com.google.android.gms.common.e;

public final class k extends UnsupportedOperationException {

    /* renamed from: a reason: collision with root package name */
    private final e f2233a;

    public k(e eVar) {
        this.f2233a = eVar;
    }

    public final String getMessage() {
        String valueOf = String.valueOf(this.f2233a);
        return new StringBuilder(String.valueOf(valueOf).length() + 8).append("Missing ").append(valueOf).toString();
    }
}
