package com.google.android.gms.common.api.internal;

public final class g<L> {

    /* renamed from: a reason: collision with root package name */
    private volatile L f2206a;

    public static final class a<L> {

        /* renamed from: a reason: collision with root package name */
        private final L f2207a;

        /* renamed from: b reason: collision with root package name */
        private final String f2208b;

        public final boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof a)) {
                return false;
            }
            a aVar = (a) obj;
            return this.f2207a == aVar.f2207a && this.f2208b.equals(aVar.f2208b);
        }

        public final int hashCode() {
            return (System.identityHashCode(this.f2207a) * 31) + this.f2208b.hashCode();
        }
    }

    public final void a() {
        this.f2206a = null;
    }
}
