package com.google.android.gms.common.api.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.a.b;
import com.google.android.gms.common.e;
import com.google.android.gms.d.h;

public abstract class j<A extends b, ResultT> {

    /* renamed from: a reason: collision with root package name */
    private final e[] f2210a;

    /* renamed from: b reason: collision with root package name */
    private final boolean f2211b;

    /* access modifiers changed from: protected */
    public abstract void a(A a2, h<ResultT> hVar) throws RemoteException;

    public final e[] a() {
        return this.f2210a;
    }

    public boolean b() {
        return this.f2211b;
    }
}
