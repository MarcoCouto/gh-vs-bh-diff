package com.google.android.gms.common.api.internal;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.util.Pair;
import com.google.android.gms.common.annotation.KeepName;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.f;
import com.google.android.gms.common.api.g;
import com.google.android.gms.common.api.h;
import com.google.android.gms.common.api.i;
import com.google.android.gms.common.api.j;
import com.google.android.gms.common.internal.aa;
import com.google.android.gms.common.internal.s;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicReference;

@KeepName
public abstract class BasePendingResult<R extends i> extends g<R> {

    /* renamed from: a reason: collision with root package name */
    static final ThreadLocal<Boolean> f2169a = new an();

    /* renamed from: b reason: collision with root package name */
    private final Object f2170b = new Object();
    private final a<R> c = new a<>(Looper.getMainLooper());
    private final WeakReference<f> d = new WeakReference<>(null);
    private final CountDownLatch e = new CountDownLatch(1);
    private final ArrayList<com.google.android.gms.common.api.g.a> f = new ArrayList<>();
    private j<? super R> g;
    private final AtomicReference<ae> h = new AtomicReference<>();
    /* access modifiers changed from: private */
    public R i;
    private Status j;
    private volatile boolean k;
    private boolean l;
    private boolean m;
    @KeepName
    private b mResultGuardian;
    private s n;
    private boolean o = false;

    public static class a<R extends i> extends Handler {
        public a() {
            this(Looper.getMainLooper());
        }

        public a(Looper looper) {
            super(looper);
        }

        public final void a(j<? super R> jVar, R r) {
            sendMessage(obtainMessage(1, new Pair(jVar, r)));
        }

        public void handleMessage(Message message) {
            switch (message.what) {
                case 1:
                    Pair pair = (Pair) message.obj;
                    j jVar = (j) pair.first;
                    i iVar = (i) pair.second;
                    try {
                        jVar.a(iVar);
                        return;
                    } catch (RuntimeException e) {
                        BasePendingResult.b(iVar);
                        throw e;
                    }
                case 2:
                    ((BasePendingResult) message.obj).a(Status.d);
                    return;
                default:
                    Log.wtf("BasePendingResult", "Don't know how to handle message: " + message.what, new Exception());
                    return;
            }
        }
    }

    private final class b {
        private b() {
        }

        /* synthetic */ b(BasePendingResult basePendingResult, an anVar) {
            this();
        }

        /* access modifiers changed from: protected */
        public final void finalize() throws Throwable {
            BasePendingResult.b(BasePendingResult.this.i);
            super.finalize();
        }
    }

    @Deprecated
    BasePendingResult() {
    }

    private final R b() {
        R r;
        boolean z = true;
        synchronized (this.f2170b) {
            if (this.k) {
                z = false;
            }
            aa.a(z, (Object) "Result has already been consumed.");
            aa.a(a(), (Object) "Result is not ready.");
            r = this.i;
            this.i = null;
            this.g = null;
            this.k = true;
        }
        ae aeVar = (ae) this.h.getAndSet(null);
        if (aeVar != null) {
            aeVar.a(this);
        }
        return r;
    }

    public static void b(i iVar) {
        if (iVar instanceof h) {
            try {
                ((h) iVar).a();
            } catch (RuntimeException e2) {
                String valueOf = String.valueOf(iVar);
                Log.w("BasePendingResult", new StringBuilder(String.valueOf(valueOf).length() + 18).append("Unable to release ").append(valueOf).toString(), e2);
            }
        }
    }

    private final void c(R r) {
        this.i = r;
        this.n = null;
        this.e.countDown();
        this.j = this.i.a();
        if (this.l) {
            this.g = null;
        } else if (this.g != null) {
            this.c.removeMessages(2);
            this.c.a(this.g, b());
        } else if (this.i instanceof h) {
            this.mResultGuardian = new b(this, null);
        }
        ArrayList arrayList = this.f;
        int size = arrayList.size();
        int i2 = 0;
        while (i2 < size) {
            Object obj = arrayList.get(i2);
            i2++;
            ((com.google.android.gms.common.api.g.a) obj).a(this.j);
        }
        this.f.clear();
    }

    public final void a(Status status) {
        synchronized (this.f2170b) {
            if (!a()) {
                a((R) b(status));
                this.m = true;
            }
        }
    }

    public final void a(R r) {
        boolean z = true;
        synchronized (this.f2170b) {
            if (this.m || this.l) {
                b((i) r);
                return;
            }
            if (a()) {
            }
            aa.a(!a(), (Object) "Results have already been set");
            if (this.k) {
                z = false;
            }
            aa.a(z, (Object) "Result has already been consumed");
            c(r);
        }
    }

    public final boolean a() {
        return this.e.getCount() == 0;
    }

    /* access modifiers changed from: protected */
    public abstract R b(Status status);
}
