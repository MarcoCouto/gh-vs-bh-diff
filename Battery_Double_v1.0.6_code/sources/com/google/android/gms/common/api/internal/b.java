package com.google.android.gms.common.api.internal;

import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.a.d;
import com.google.android.gms.common.api.a.f;
import com.google.android.gms.common.api.e;
import com.google.android.gms.common.api.internal.a.C0054a;
import com.google.android.gms.common.api.k;
import com.google.android.gms.common.g;
import com.google.android.gms.common.internal.aa;
import com.google.android.gms.common.internal.af;
import com.google.android.gms.common.internal.e.j;
import com.google.android.gms.common.internal.q;
import com.google.android.gms.common.internal.r;
import com.google.android.gms.common.internal.z;
import com.google.android.gms.common.util.n;
import com.google.android.gms.d.h;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

public class b implements Callback {

    /* renamed from: a reason: collision with root package name */
    public static final Status f2194a = new Status(4, "Sign-out occurred while this API call was in progress.");
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public static final Status f2195b = new Status(4, "The user must be signed in to make this API call.");
    /* access modifiers changed from: private */
    public static final Object f = new Object();
    private static b g;
    /* access modifiers changed from: private */
    public long c = 5000;
    /* access modifiers changed from: private */
    public long d = 120000;
    /* access modifiers changed from: private */
    public long e = 10000;
    /* access modifiers changed from: private */
    public final Context h;
    /* access modifiers changed from: private */
    public final g i;
    /* access modifiers changed from: private */
    public final q j;
    private final AtomicInteger k = new AtomicInteger(1);
    private final AtomicInteger l = new AtomicInteger(0);
    /* access modifiers changed from: private */
    public final Map<ah<?>, a<?>> m = new ConcurrentHashMap(5, 0.75f, 1);
    /* access modifiers changed from: private */
    public n n = null;
    /* access modifiers changed from: private */
    public final Set<ah<?>> o = new android.support.v4.h.b();
    private final Set<ah<?>> p = new android.support.v4.h.b();
    /* access modifiers changed from: private */
    public final Handler q;

    public class a<O extends d> implements com.google.android.gms.common.api.f.a, com.google.android.gms.common.api.f.b, ao {

        /* renamed from: b reason: collision with root package name */
        private final Queue<p> f2197b = new LinkedList();
        /* access modifiers changed from: private */
        public final f c;
        private final com.google.android.gms.common.api.a.b d;
        private final ah<O> e;
        private final l f;
        private final Set<ai> g = new HashSet();
        private final Map<com.google.android.gms.common.api.internal.g.a<?>, x> h = new HashMap();
        private final int i;
        private final y j;
        private boolean k;
        private final List<C0055b> l = new ArrayList();
        private com.google.android.gms.common.b m = null;

        public a(e<O> eVar) {
            this.c = eVar.a(b.this.q.getLooper(), this);
            if (this.c instanceof af) {
                this.d = ((af) this.c).A();
            } else {
                this.d = this.c;
            }
            this.e = eVar.a();
            this.f = new l();
            this.i = eVar.b();
            if (this.c.d()) {
                this.j = eVar.a(b.this.h, b.this.q);
            } else {
                this.j = null;
            }
        }

        /* access modifiers changed from: private */
        public final void a(C0055b bVar) {
            if (!this.l.contains(bVar) || this.k) {
                return;
            }
            if (!this.c.b()) {
                i();
            } else {
                o();
            }
        }

        /* access modifiers changed from: private */
        public final boolean a(boolean z) {
            aa.a(b.this.q);
            if (!this.c.b() || this.h.size() != 0) {
                return false;
            }
            if (!this.f.a()) {
                this.c.a();
                return true;
            } else if (!z) {
                return false;
            } else {
                q();
                return false;
            }
        }

        /* access modifiers changed from: private */
        public final void b(C0055b bVar) {
            if (this.l.remove(bVar)) {
                b.this.q.removeMessages(15, bVar);
                b.this.q.removeMessages(16, bVar);
                com.google.android.gms.common.e b2 = bVar.f2199b;
                ArrayList arrayList = new ArrayList(this.f2197b.size());
                for (p pVar : this.f2197b) {
                    if (pVar instanceof af) {
                        com.google.android.gms.common.e[] a2 = ((af) pVar).a();
                        if (a2 != null && com.google.android.gms.common.util.b.b(a2, b2)) {
                            arrayList.add(pVar);
                        }
                    }
                }
                ArrayList arrayList2 = arrayList;
                int size = arrayList2.size();
                int i2 = 0;
                while (i2 < size) {
                    Object obj = arrayList2.get(i2);
                    i2++;
                    p pVar2 = (p) obj;
                    this.f2197b.remove(pVar2);
                    pVar2.a((RuntimeException) new k(b2));
                }
            }
        }

        private final boolean b(p pVar) {
            if (!(pVar instanceof af)) {
                c(pVar);
                return true;
            }
            af afVar = (af) pVar;
            com.google.android.gms.common.e[] a2 = afVar.a();
            if (a2 == null || a2.length == 0) {
                c(pVar);
                return true;
            }
            com.google.android.gms.common.e[] h2 = this.c.h();
            if (h2 == null) {
                h2 = new com.google.android.gms.common.e[0];
            }
            android.support.v4.h.a aVar = new android.support.v4.h.a(h2.length);
            for (com.google.android.gms.common.e eVar : h2) {
                aVar.put(eVar.a(), Long.valueOf(eVar.b()));
            }
            for (com.google.android.gms.common.e eVar2 : a2) {
                if (!aVar.containsKey(eVar2.a()) || ((Long) aVar.get(eVar2.a())).longValue() < eVar2.b()) {
                    if (afVar.b()) {
                        C0055b bVar = new C0055b(this.e, eVar2, null);
                        int indexOf = this.l.indexOf(bVar);
                        if (indexOf >= 0) {
                            C0055b bVar2 = (C0055b) this.l.get(indexOf);
                            b.this.q.removeMessages(15, bVar2);
                            b.this.q.sendMessageDelayed(Message.obtain(b.this.q, 15, bVar2), b.this.c);
                        } else {
                            this.l.add(bVar);
                            b.this.q.sendMessageDelayed(Message.obtain(b.this.q, 15, bVar), b.this.c);
                            b.this.q.sendMessageDelayed(Message.obtain(b.this.q, 16, bVar), b.this.d);
                            com.google.android.gms.common.b bVar3 = new com.google.android.gms.common.b(2, null);
                            if (!c(bVar3)) {
                                b.this.a(bVar3, this.i);
                            }
                        }
                    } else {
                        afVar.a((RuntimeException) new k(eVar2));
                    }
                    return false;
                }
                this.l.remove(new C0055b(this.e, eVar2, null));
            }
            c(pVar);
            return true;
        }

        private final void c(p pVar) {
            pVar.a(this.f, k());
            try {
                pVar.a(this);
            } catch (DeadObjectException e2) {
                a(1);
                this.c.a();
            }
        }

        private final boolean c(com.google.android.gms.common.b bVar) {
            boolean z;
            synchronized (b.f) {
                if (b.this.n == null || !b.this.o.contains(this.e)) {
                    z = false;
                } else {
                    b.this.n.b(bVar, this.i);
                    z = true;
                }
            }
            return z;
        }

        private final void d(com.google.android.gms.common.b bVar) {
            for (ai aiVar : this.g) {
                String str = null;
                if (z.a(bVar, com.google.android.gms.common.b.f2234a)) {
                    str = this.c.f();
                }
                aiVar.a(this.e, bVar, str);
            }
            this.g.clear();
        }

        /* access modifiers changed from: private */
        public final void m() {
            d();
            d(com.google.android.gms.common.b.f2234a);
            p();
            for (x xVar : this.h.values()) {
                try {
                    xVar.f2228a.a(this.d, new h());
                } catch (DeadObjectException e2) {
                    a(1);
                    this.c.a();
                } catch (RemoteException e3) {
                }
            }
            o();
            q();
        }

        /* access modifiers changed from: private */
        public final void n() {
            d();
            this.k = true;
            this.f.c();
            b.this.q.sendMessageDelayed(Message.obtain(b.this.q, 9, this.e), b.this.c);
            b.this.q.sendMessageDelayed(Message.obtain(b.this.q, 11, this.e), b.this.d);
            b.this.j.a();
        }

        private final void o() {
            ArrayList arrayList = new ArrayList(this.f2197b);
            int size = arrayList.size();
            int i2 = 0;
            while (i2 < size) {
                Object obj = arrayList.get(i2);
                i2++;
                p pVar = (p) obj;
                if (!this.c.b()) {
                    return;
                }
                if (b(pVar)) {
                    this.f2197b.remove(pVar);
                }
            }
        }

        private final void p() {
            if (this.k) {
                b.this.q.removeMessages(11, this.e);
                b.this.q.removeMessages(9, this.e);
                this.k = false;
            }
        }

        private final void q() {
            b.this.q.removeMessages(12, this.e);
            b.this.q.sendMessageDelayed(b.this.q.obtainMessage(12, this.e), b.this.e);
        }

        public final void a() {
            aa.a(b.this.q);
            a(b.f2194a);
            this.f.b();
            for (com.google.android.gms.common.api.internal.g.a agVar : (com.google.android.gms.common.api.internal.g.a[]) this.h.keySet().toArray(new com.google.android.gms.common.api.internal.g.a[this.h.size()])) {
                a((p) new ag(agVar, new h()));
            }
            d(new com.google.android.gms.common.b(4));
            if (this.c.b()) {
                this.c.a((j) new t(this));
            }
        }

        public final void a(int i2) {
            if (Looper.myLooper() == b.this.q.getLooper()) {
                n();
            } else {
                b.this.q.post(new s(this));
            }
        }

        public final void a(Bundle bundle) {
            if (Looper.myLooper() == b.this.q.getLooper()) {
                m();
            } else {
                b.this.q.post(new r(this));
            }
        }

        public final void a(Status status) {
            aa.a(b.this.q);
            for (p a2 : this.f2197b) {
                a2.a(status);
            }
            this.f2197b.clear();
        }

        public final void a(ai aiVar) {
            aa.a(b.this.q);
            this.g.add(aiVar);
        }

        public final void a(p pVar) {
            aa.a(b.this.q);
            if (!this.c.b()) {
                this.f2197b.add(pVar);
                if (this.m == null || !this.m.a()) {
                    i();
                } else {
                    a(this.m);
                }
            } else if (b(pVar)) {
                q();
            } else {
                this.f2197b.add(pVar);
            }
        }

        public final void a(com.google.android.gms.common.b bVar) {
            aa.a(b.this.q);
            if (this.j != null) {
                this.j.a();
            }
            d();
            b.this.j.a();
            d(bVar);
            if (bVar.c() == 4) {
                a(b.f2195b);
            } else if (this.f2197b.isEmpty()) {
                this.m = bVar;
            } else if (!c(bVar) && !b.this.a(bVar, this.i)) {
                if (bVar.c() == 18) {
                    this.k = true;
                }
                if (this.k) {
                    b.this.q.sendMessageDelayed(Message.obtain(b.this.q, 9, this.e), b.this.c);
                    return;
                }
                String a2 = this.e.a();
                a(new Status(17, new StringBuilder(String.valueOf(a2).length() + 38).append("API: ").append(a2).append(" is not available on this device.").toString()));
            }
        }

        public final f b() {
            return this.c;
        }

        public final void b(com.google.android.gms.common.b bVar) {
            aa.a(b.this.q);
            this.c.a();
            a(bVar);
        }

        public final Map<com.google.android.gms.common.api.internal.g.a<?>, x> c() {
            return this.h;
        }

        public final void d() {
            aa.a(b.this.q);
            this.m = null;
        }

        public final com.google.android.gms.common.b e() {
            aa.a(b.this.q);
            return this.m;
        }

        public final void f() {
            aa.a(b.this.q);
            if (this.k) {
                i();
            }
        }

        public final void g() {
            aa.a(b.this.q);
            if (this.k) {
                p();
                a(b.this.i.a(b.this.h) == 18 ? new Status(8, "Connection timed out while waiting for Google Play services update to complete.") : new Status(8, "API failed to connect while resuming due to an unknown error."));
                this.c.a();
            }
        }

        public final boolean h() {
            return a(true);
        }

        public final void i() {
            aa.a(b.this.q);
            if (!this.c.b() && !this.c.c()) {
                int a2 = b.this.j.a(b.this.h, this.c);
                if (a2 != 0) {
                    a(new com.google.android.gms.common.b(a2, null));
                    return;
                }
                c cVar = new c(this.c, this.e);
                if (this.c.d()) {
                    this.j.a((ac) cVar);
                }
                this.c.a((com.google.android.gms.common.internal.e.d) cVar);
            }
        }

        /* access modifiers changed from: 0000 */
        public final boolean j() {
            return this.c.b();
        }

        public final boolean k() {
            return this.c.d();
        }

        public final int l() {
            return this.i;
        }
    }

    /* renamed from: com.google.android.gms.common.api.internal.b$b reason: collision with other inner class name */
    private static class C0055b {
        /* access modifiers changed from: private */

        /* renamed from: a reason: collision with root package name */
        public final ah<?> f2198a;
        /* access modifiers changed from: private */

        /* renamed from: b reason: collision with root package name */
        public final com.google.android.gms.common.e f2199b;

        private C0055b(ah<?> ahVar, com.google.android.gms.common.e eVar) {
            this.f2198a = ahVar;
            this.f2199b = eVar;
        }

        /* synthetic */ C0055b(ah ahVar, com.google.android.gms.common.e eVar, q qVar) {
            this(ahVar, eVar);
        }

        public final boolean equals(Object obj) {
            if (obj == null || !(obj instanceof C0055b)) {
                return false;
            }
            C0055b bVar = (C0055b) obj;
            return z.a(this.f2198a, bVar.f2198a) && z.a(this.f2199b, bVar.f2199b);
        }

        public final int hashCode() {
            return z.a(this.f2198a, this.f2199b);
        }

        public final String toString() {
            return z.a((Object) this).a("key", this.f2198a).a("feature", this.f2199b).toString();
        }
    }

    private class c implements ac, com.google.android.gms.common.internal.e.d {
        /* access modifiers changed from: private */

        /* renamed from: b reason: collision with root package name */
        public final f f2201b;
        /* access modifiers changed from: private */
        public final ah<?> c;
        private r d = null;
        private Set<Scope> e = null;
        /* access modifiers changed from: private */
        public boolean f = false;

        public c(f fVar, ah<?> ahVar) {
            this.f2201b = fVar;
            this.c = ahVar;
        }

        /* access modifiers changed from: private */
        public final void a() {
            if (this.f && this.d != null) {
                this.f2201b.a(this.d, this.e);
            }
        }

        public final void a(com.google.android.gms.common.b bVar) {
            b.this.q.post(new v(this, bVar));
        }

        public final void a(r rVar, Set<Scope> set) {
            if (rVar == null || set == null) {
                Log.wtf("GoogleApiManager", "Received null response from onSignInSuccess", new Exception());
                b(new com.google.android.gms.common.b(4));
                return;
            }
            this.d = rVar;
            this.e = set;
            a();
        }

        public final void b(com.google.android.gms.common.b bVar) {
            ((a) b.this.m.get(this.c)).b(bVar);
        }
    }

    private b(Context context, Looper looper, g gVar) {
        this.h = context;
        this.q = new Handler(looper, this);
        this.i = gVar;
        this.j = new q(gVar);
        this.q.sendMessage(this.q.obtainMessage(6));
    }

    public static b a(Context context) {
        b bVar;
        synchronized (f) {
            if (g == null) {
                HandlerThread handlerThread = new HandlerThread("GoogleApiHandler", 9);
                handlerThread.start();
                g = new b(context.getApplicationContext(), handlerThread.getLooper(), g.a());
            }
            bVar = g;
        }
        return bVar;
    }

    private final void a(e<?> eVar) {
        ah a2 = eVar.a();
        a aVar = (a) this.m.get(a2);
        if (aVar == null) {
            aVar = new a(eVar);
            this.m.put(a2, aVar);
        }
        if (aVar.k()) {
            this.p.add(a2);
        }
        aVar.i();
    }

    public final void a() {
        this.q.sendMessage(this.q.obtainMessage(3));
    }

    /* access modifiers changed from: 0000 */
    public final boolean a(com.google.android.gms.common.b bVar, int i2) {
        return this.i.a(this.h, bVar, i2);
    }

    public final void b(com.google.android.gms.common.b bVar, int i2) {
        if (!a(bVar, i2)) {
            this.q.sendMessage(this.q.obtainMessage(5, i2, 0, bVar));
        }
    }

    public boolean handleMessage(Message message) {
        a aVar;
        switch (message.what) {
            case 1:
                this.e = ((Boolean) message.obj).booleanValue() ? 10000 : 300000;
                this.q.removeMessages(12);
                for (ah obtainMessage : this.m.keySet()) {
                    this.q.sendMessageDelayed(this.q.obtainMessage(12, obtainMessage), this.e);
                }
                break;
            case 2:
                ai aiVar = (ai) message.obj;
                Iterator it = aiVar.a().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        break;
                    } else {
                        ah ahVar = (ah) it.next();
                        a aVar2 = (a) this.m.get(ahVar);
                        if (aVar2 == null) {
                            aiVar.a(ahVar, new com.google.android.gms.common.b(13), null);
                            break;
                        } else if (aVar2.j()) {
                            aiVar.a(ahVar, com.google.android.gms.common.b.f2234a, aVar2.b().f());
                        } else if (aVar2.e() != null) {
                            aiVar.a(ahVar, aVar2.e(), null);
                        } else {
                            aVar2.a(aiVar);
                        }
                    }
                }
            case 3:
                for (a aVar3 : this.m.values()) {
                    aVar3.d();
                    aVar3.i();
                }
                break;
            case 4:
            case 8:
            case 13:
                w wVar = (w) message.obj;
                a aVar4 = (a) this.m.get(wVar.c.a());
                if (aVar4 == null) {
                    a(wVar.c);
                    aVar4 = (a) this.m.get(wVar.c.a());
                }
                if (aVar4.k() && this.l.get() != wVar.f2227b) {
                    wVar.f2226a.a(f2194a);
                    aVar4.a();
                    break;
                } else {
                    aVar4.a(wVar.f2226a);
                    break;
                }
                break;
            case 5:
                int i2 = message.arg1;
                com.google.android.gms.common.b bVar = (com.google.android.gms.common.b) message.obj;
                Iterator it2 = this.m.values().iterator();
                while (true) {
                    if (it2.hasNext()) {
                        aVar = (a) it2.next();
                        if (aVar.l() == i2) {
                        }
                    } else {
                        aVar = null;
                    }
                }
                if (aVar == null) {
                    Log.wtf("GoogleApiManager", "Could not find API instance " + i2 + " while trying to fail enqueued calls.", new Exception());
                    break;
                } else {
                    String b2 = this.i.b(bVar.c());
                    String e2 = bVar.e();
                    aVar.a(new Status(17, new StringBuilder(String.valueOf(b2).length() + 69 + String.valueOf(e2).length()).append("Error resolution was canceled by the user, original error message: ").append(b2).append(": ").append(e2).toString()));
                    break;
                }
            case 6:
                if (n.b() && (this.h.getApplicationContext() instanceof Application)) {
                    a.a((Application) this.h.getApplicationContext());
                    a.a().a((C0054a) new q(this));
                    if (!a.a().a(true)) {
                        this.e = 300000;
                        break;
                    }
                }
                break;
            case 7:
                a((e) message.obj);
                break;
            case 9:
                if (this.m.containsKey(message.obj)) {
                    ((a) this.m.get(message.obj)).f();
                    break;
                }
                break;
            case 10:
                for (ah remove : this.p) {
                    ((a) this.m.remove(remove)).a();
                }
                this.p.clear();
                break;
            case 11:
                if (this.m.containsKey(message.obj)) {
                    ((a) this.m.get(message.obj)).g();
                    break;
                }
                break;
            case 12:
                if (this.m.containsKey(message.obj)) {
                    ((a) this.m.get(message.obj)).h();
                    break;
                }
                break;
            case 14:
                o oVar = (o) message.obj;
                ah a2 = oVar.a();
                if (this.m.containsKey(a2)) {
                    oVar.b().a(Boolean.valueOf(((a) this.m.get(a2)).a(false)));
                    break;
                } else {
                    oVar.b().a(Boolean.valueOf(false));
                    break;
                }
            case 15:
                C0055b bVar2 = (C0055b) message.obj;
                if (this.m.containsKey(bVar2.f2198a)) {
                    ((a) this.m.get(bVar2.f2198a)).a(bVar2);
                    break;
                }
                break;
            case 16:
                C0055b bVar3 = (C0055b) message.obj;
                if (this.m.containsKey(bVar3.f2198a)) {
                    ((a) this.m.get(bVar3.f2198a)).b(bVar3);
                    break;
                }
                break;
            default:
                Log.w("GoogleApiManager", "Unknown message id: " + message.what);
                return false;
        }
        return true;
    }
}
