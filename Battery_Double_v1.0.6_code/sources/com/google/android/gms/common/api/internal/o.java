package com.google.android.gms.common.api.internal;

import com.google.android.gms.d.h;

final class o {

    /* renamed from: a reason: collision with root package name */
    private final ah<?> f2216a;

    /* renamed from: b reason: collision with root package name */
    private final h<Boolean> f2217b;

    public final ah<?> a() {
        return this.f2216a;
    }

    public final h<Boolean> b() {
        return this.f2217b;
    }
}
