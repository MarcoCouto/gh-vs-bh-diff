package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.internal.a.C0054a;

final class q implements C0054a {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ b f2219a;

    q(b bVar) {
        this.f2219a = bVar;
    }

    public final void a(boolean z) {
        this.f2219a.q.sendMessage(this.f2219a.q.obtainMessage(1, Boolean.valueOf(z)));
    }
}
