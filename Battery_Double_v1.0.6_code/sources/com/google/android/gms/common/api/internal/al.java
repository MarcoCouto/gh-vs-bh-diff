package com.google.android.gms.common.api.internal;

import android.content.DialogInterface.OnCancelListener;
import com.google.android.gms.common.api.GoogleApiActivity;
import com.google.android.gms.common.api.internal.c.a;
import com.google.android.gms.common.b;

final class al implements Runnable {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ aj f2190a;

    /* renamed from: b reason: collision with root package name */
    private final ak f2191b;

    al(aj ajVar, ak akVar) {
        this.f2190a = ajVar;
        this.f2191b = akVar;
    }

    public final void run() {
        if (this.f2190a.f2187b) {
            b b2 = this.f2191b.b();
            if (b2.a()) {
                this.f2190a.f2172a.a(GoogleApiActivity.a(this.f2190a.a(), b2.d(), this.f2191b.a(), false), 1);
            } else if (this.f2190a.d.a(b2.c())) {
                this.f2190a.d.a(this.f2190a.a(), this.f2190a.f2172a, b2.c(), 2, this.f2190a);
            } else if (b2.c() == 18) {
                this.f2190a.d.a(this.f2190a.a().getApplicationContext(), (a) new am(this, this.f2190a.d.a(this.f2190a.a(), (OnCancelListener) this.f2190a)));
            } else {
                this.f2190a.a(b2, this.f2191b.a());
            }
        }
    }
}
