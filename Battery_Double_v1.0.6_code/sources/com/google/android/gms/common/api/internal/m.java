package com.google.android.gms.common.api.internal;

import com.google.android.gms.d.c;
import com.google.android.gms.d.g;
import com.google.android.gms.d.h;

final class m implements c<TResult> {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ h f2214a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ l f2215b;

    m(l lVar, h hVar) {
        this.f2215b = lVar;
        this.f2214a = hVar;
    }

    public final void a(g<TResult> gVar) {
        this.f2215b.f2213b.remove(this.f2214a);
    }
}
