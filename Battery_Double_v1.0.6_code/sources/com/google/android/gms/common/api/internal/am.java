package com.google.android.gms.common.api.internal;

import android.app.Dialog;
import com.google.android.gms.common.api.internal.c.a;

final class am extends a {

    /* renamed from: a reason: collision with root package name */
    private final /* synthetic */ Dialog f2192a;

    /* renamed from: b reason: collision with root package name */
    private final /* synthetic */ al f2193b;

    am(al alVar, Dialog dialog) {
        this.f2193b = alVar;
        this.f2192a = dialog;
    }

    public final void a() {
        this.f2193b.f2190a.c();
        if (this.f2192a.isShowing()) {
            this.f2192a.dismiss();
        }
    }
}
