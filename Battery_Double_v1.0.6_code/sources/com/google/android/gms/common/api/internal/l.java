package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.b;
import com.google.android.gms.d.c;
import com.google.android.gms.d.h;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.WeakHashMap;

public final class l {

    /* renamed from: a reason: collision with root package name */
    private final Map<BasePendingResult<?>, Boolean> f2212a = Collections.synchronizedMap(new WeakHashMap());
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public final Map<h<?>, Boolean> f2213b = Collections.synchronizedMap(new WeakHashMap());

    private final void a(boolean z, Status status) {
        HashMap hashMap;
        HashMap hashMap2;
        synchronized (this.f2212a) {
            hashMap = new HashMap(this.f2212a);
        }
        synchronized (this.f2213b) {
            hashMap2 = new HashMap(this.f2213b);
        }
        for (Entry entry : hashMap.entrySet()) {
            if (z || ((Boolean) entry.getValue()).booleanValue()) {
                ((BasePendingResult) entry.getKey()).a(status);
            }
        }
        for (Entry entry2 : hashMap2.entrySet()) {
            if (z || ((Boolean) entry2.getValue()).booleanValue()) {
                ((h) entry2.getKey()).b((Exception) new b(status));
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public final <TResult> void a(h<TResult> hVar, boolean z) {
        this.f2213b.put(hVar, Boolean.valueOf(z));
        hVar.a().a((c<TResult>) new m<TResult>(this, hVar));
    }

    /* access modifiers changed from: 0000 */
    public final boolean a() {
        return !this.f2212a.isEmpty() || !this.f2213b.isEmpty();
    }

    public final void b() {
        a(false, b.f2194a);
    }

    public final void c() {
        a(true, ad.f2178a);
    }
}
