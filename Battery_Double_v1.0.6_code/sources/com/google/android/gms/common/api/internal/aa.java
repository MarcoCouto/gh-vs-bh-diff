package com.google.android.gms.common.api.internal;

import android.os.DeadObjectException;
import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.b;
import com.google.android.gms.common.api.internal.b.a;
import com.google.android.gms.d.h;

abstract class aa<T> extends p {

    /* renamed from: a reason: collision with root package name */
    protected final h<T> f2175a;

    public aa(int i, h<T> hVar) {
        super(i);
        this.f2175a = hVar;
    }

    public void a(Status status) {
        this.f2175a.b((Exception) new b(status));
    }

    public final void a(a<?> aVar) throws DeadObjectException {
        try {
            b(aVar);
        } catch (DeadObjectException e) {
            a(p.b(e));
            throw e;
        } catch (RemoteException e2) {
            a(p.b(e2));
        } catch (RuntimeException e3) {
            a(e3);
        }
    }

    public void a(l lVar, boolean z) {
    }

    public void a(RuntimeException runtimeException) {
        this.f2175a.b((Exception) runtimeException);
    }

    /* access modifiers changed from: protected */
    public abstract void b(a<?> aVar) throws RemoteException;
}
