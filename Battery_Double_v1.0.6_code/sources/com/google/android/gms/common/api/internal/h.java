package com.google.android.gms.common.api.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.a.b;

public abstract class h<A extends b, L> {

    /* renamed from: a reason: collision with root package name */
    private final g<L> f2209a;

    public void a() {
        this.f2209a.a();
    }

    /* access modifiers changed from: protected */
    public abstract void a(A a2, com.google.android.gms.d.h<Void> hVar) throws RemoteException;
}
