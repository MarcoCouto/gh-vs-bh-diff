package com.google.android.gms.common.api;

public class b extends Exception {

    /* renamed from: a reason: collision with root package name */
    protected final Status f2164a;

    public b(Status status) {
        int d = status.d();
        String str = status.b() != null ? status.b() : "";
        super(new StringBuilder(String.valueOf(str).length() + 13).append(d).append(": ").append(str).toString());
        this.f2164a = status;
    }
}
