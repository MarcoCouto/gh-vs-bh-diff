package com.google.android.gms.common.api.internal;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

public final class c extends BroadcastReceiver {

    /* renamed from: a reason: collision with root package name */
    private Context f2202a;

    /* renamed from: b reason: collision with root package name */
    private final a f2203b;

    public static abstract class a {
        public abstract void a();
    }

    public c(a aVar) {
        this.f2203b = aVar;
    }

    public final synchronized void a() {
        if (this.f2202a != null) {
            this.f2202a.unregisterReceiver(this);
        }
        this.f2202a = null;
    }

    public final void a(Context context) {
        this.f2202a = context;
    }

    public final void onReceive(Context context, Intent intent) {
        Uri data = intent.getData();
        String str = null;
        if (data != null) {
            str = data.getSchemeSpecificPart();
        }
        if ("com.google.android.gms".equals(str)) {
            this.f2203b.a();
            a();
        }
    }
}
