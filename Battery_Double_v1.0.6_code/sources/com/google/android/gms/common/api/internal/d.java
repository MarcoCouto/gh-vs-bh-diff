package com.google.android.gms.common.api.internal;

import android.content.Context;
import android.content.res.Resources;
import android.text.TextUtils;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.aa;
import com.google.android.gms.common.internal.ag;
import com.google.android.gms.common.internal.y;
import com.google.android.gms.common.p.a;

@Deprecated
public final class d {

    /* renamed from: a reason: collision with root package name */
    private static final Object f2204a = new Object();

    /* renamed from: b reason: collision with root package name */
    private static d f2205b;
    private final String c;
    private final Status d;
    private final boolean e;
    private final boolean f;

    d(Context context) {
        boolean z = true;
        Resources resources = context.getResources();
        int identifier = resources.getIdentifier("google_app_measurement_enable", "integer", resources.getResourcePackageName(a.common_google_play_services_unknown_issue));
        if (identifier != 0) {
            boolean z2 = resources.getInteger(identifier) != 0;
            if (z2) {
                z = false;
            }
            this.f = z;
            z = z2;
        } else {
            this.f = false;
        }
        this.e = z;
        String a2 = y.a(context);
        if (a2 == null) {
            a2 = new ag(context).a("google_app_id");
        }
        if (TextUtils.isEmpty(a2)) {
            this.d = new Status(10, "Missing google app id value from from string resources with name google_app_id.");
            this.c = null;
            return;
        }
        this.c = a2;
        this.d = Status.f2160a;
    }

    public static Status a(Context context) {
        Status status;
        aa.a(context, (Object) "Context must not be null.");
        synchronized (f2204a) {
            if (f2205b == null) {
                f2205b = new d(context);
            }
            status = f2205b.d;
        }
        return status;
    }

    private static d a(String str) {
        d dVar;
        synchronized (f2204a) {
            if (f2205b == null) {
                throw new IllegalStateException(new StringBuilder(String.valueOf(str).length() + 34).append("Initialize must be called before ").append(str).append(".").toString());
            }
            dVar = f2205b;
        }
        return dVar;
    }

    public static String a() {
        return a("getGoogleAppId").c;
    }

    public static boolean b() {
        return a("isMeasurementExplicitlyDisabled").f;
    }
}
