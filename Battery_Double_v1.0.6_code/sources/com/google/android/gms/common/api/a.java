package com.google.android.gms.common.api;

import android.accounts.Account;
import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.a.d;
import com.google.android.gms.common.internal.aa;
import com.google.android.gms.common.internal.r;
import java.util.Set;

public final class a<O extends d> {

    /* renamed from: a reason: collision with root package name */
    private final C0051a<?, O> f2162a;

    /* renamed from: b reason: collision with root package name */
    private final i<?, O> f2163b = null;
    private final g<?> c;
    private final j<?> d;
    private final String e;

    /* renamed from: com.google.android.gms.common.api.a$a reason: collision with other inner class name */
    public static abstract class C0051a<T extends f, O> extends e<T, O> {
        public abstract T a(Context context, Looper looper, com.google.android.gms.common.internal.f fVar, O o, com.google.android.gms.common.api.f.a aVar, com.google.android.gms.common.api.f.b bVar);
    }

    public interface b {
    }

    public static class c<C extends b> {
    }

    public interface d {

        /* renamed from: com.google.android.gms.common.api.a$d$a reason: collision with other inner class name */
        public interface C0052a extends c, C0053d {
            Account a();
        }

        public interface b extends c {
            GoogleSignInAccount a();
        }

        public interface c extends d {
        }

        /* renamed from: com.google.android.gms.common.api.a$d$d reason: collision with other inner class name */
        public interface C0053d extends d {
        }

        public interface e extends c, C0053d {
        }
    }

    public static abstract class e<T extends b, O> {
    }

    public interface f extends b {
        void a();

        void a(com.google.android.gms.common.internal.e.d dVar);

        void a(com.google.android.gms.common.internal.e.j jVar);

        void a(r rVar, Set<Scope> set);

        boolean b();

        boolean c();

        boolean d();

        boolean e();

        String f();

        int g();

        com.google.android.gms.common.e[] h();
    }

    public static final class g<C extends f> extends c<C> {
    }

    public interface h<T extends IInterface> extends b {
        T a(IBinder iBinder);

        String a();

        void a(int i, T t);

        String b();
    }

    public static abstract class i<T extends h, O> extends e<T, O> {
    }

    public static final class j<C extends h> extends c<C> {
    }

    public <C extends f> a(String str, C0051a<C, O> aVar, g<C> gVar) {
        aa.a(aVar, (Object) "Cannot construct an Api with a null ClientBuilder");
        aa.a(gVar, (Object) "Cannot construct an Api with a null ClientKey");
        this.e = str;
        this.f2162a = aVar;
        this.c = gVar;
        this.d = null;
    }

    public final C0051a<?, O> a() {
        aa.a(this.f2162a != null, (Object) "This API was constructed with a SimpleClientBuilder. Use getSimpleClientBuilder");
        return this.f2162a;
    }

    public final String b() {
        return this.e;
    }
}
