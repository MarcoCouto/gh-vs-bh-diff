package com.google.android.gms.common;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.Notification;
import android.app.Notification.BigTextStyle;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v4.a.aa.c;
import android.support.v4.a.aa.d;
import android.util.Log;
import android.util.TypedValue;
import android.widget.ProgressBar;
import com.google.android.gms.a.a.C0044a;
import com.google.android.gms.a.a.b;
import com.google.android.gms.common.api.GoogleApiActivity;
import com.google.android.gms.common.api.internal.f;
import com.google.android.gms.common.internal.aa;
import com.google.android.gms.common.internal.j;
import com.google.android.gms.common.util.i;
import com.google.android.gms.common.util.n;

public class g extends h {

    /* renamed from: a reason: collision with root package name */
    public static final int f2245a = h.f2249b;
    private static final Object c = new Object();
    private static final g d = new g();
    private String e;

    @SuppressLint({"HandlerLeak"})
    private class a extends Handler {

        /* renamed from: a reason: collision with root package name */
        private final Context f2246a;

        public a(Context context) {
            super(Looper.myLooper() == null ? Looper.getMainLooper() : Looper.myLooper());
            this.f2246a = context.getApplicationContext();
        }

        public final void handleMessage(Message message) {
            switch (message.what) {
                case 1:
                    int a2 = g.this.a(this.f2246a);
                    if (g.this.a(a2)) {
                        g.this.a(this.f2246a, a2);
                        return;
                    }
                    return;
                default:
                    Log.w("GoogleApiAvailability", "Don't know how to handle this message: " + message.what);
                    return;
            }
        }
    }

    g() {
    }

    static Dialog a(Context context, int i, j jVar, OnCancelListener onCancelListener) {
        Builder builder = null;
        if (i == 0) {
            return null;
        }
        TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(16843529, typedValue, true);
        if ("Theme.Dialog.Alert".equals(context.getResources().getResourceEntryName(typedValue.resourceId))) {
            builder = new Builder(context, 5);
        }
        if (builder == null) {
            builder = new Builder(context);
        }
        builder.setMessage(com.google.android.gms.common.internal.g.c(context, i));
        if (onCancelListener != null) {
            builder.setOnCancelListener(onCancelListener);
        }
        String e2 = com.google.android.gms.common.internal.g.e(context, i);
        if (e2 != null) {
            builder.setPositiveButton(e2, jVar);
        }
        String a2 = com.google.android.gms.common.internal.g.a(context, i);
        if (a2 != null) {
            builder.setTitle(a2);
        }
        return builder.create();
    }

    public static g a() {
        return d;
    }

    @TargetApi(26)
    private final String a(Context context, NotificationManager notificationManager) {
        aa.a(n.l());
        String c2 = c();
        if (c2 == null) {
            c2 = "com.google.android.gms.availability";
            NotificationChannel notificationChannel = notificationManager.getNotificationChannel(c2);
            String b2 = com.google.android.gms.common.internal.g.b(context);
            if (notificationChannel == null) {
                notificationManager.createNotificationChannel(new NotificationChannel(c2, b2, 4));
            } else if (!b2.equals(notificationChannel.getName())) {
                notificationChannel.setName(b2);
                notificationManager.createNotificationChannel(notificationChannel);
            }
        }
        return c2;
    }

    static void a(Activity activity, Dialog dialog, String str, OnCancelListener onCancelListener) {
        if (activity instanceof android.support.v4.a.j) {
            q.a(dialog, onCancelListener).a(((android.support.v4.a.j) activity).f(), str);
            return;
        }
        d.a(dialog, onCancelListener).show(activity.getFragmentManager(), str);
    }

    @TargetApi(20)
    private final void a(Context context, int i, String str, PendingIntent pendingIntent) {
        Notification a2;
        int i2;
        if (i == 18) {
            c(context);
        } else if (pendingIntent != null) {
            String b2 = com.google.android.gms.common.internal.g.b(context, i);
            String d2 = com.google.android.gms.common.internal.g.d(context, i);
            Resources resources = context.getResources();
            NotificationManager notificationManager = (NotificationManager) context.getSystemService("notification");
            if (i.a(context)) {
                aa.a(n.h());
                Notification.Builder style = new Notification.Builder(context).setSmallIcon(context.getApplicationInfo().icon).setPriority(2).setAutoCancel(true).setContentTitle(b2).setStyle(new BigTextStyle().bigText(d2));
                if (i.b(context)) {
                    style.addAction(C0044a.common_full_open_on_phone, resources.getString(b.common_open_on_phone), pendingIntent);
                } else {
                    style.setContentIntent(pendingIntent);
                }
                if (n.l() && n.l()) {
                    style.setChannelId(a(context, notificationManager));
                }
                a2 = style.build();
            } else {
                c a3 = new c(context).a(17301642).c((CharSequence) resources.getString(b.common_google_play_services_notification_ticker)).a(System.currentTimeMillis()).b(true).a(pendingIntent).a((CharSequence) b2).b((CharSequence) d2).c(true).a((d) new android.support.v4.a.aa.b().a((CharSequence) d2));
                if (n.l() && n.l()) {
                    a3.a(a(context, notificationManager));
                }
                a2 = a3.a();
            }
            switch (i) {
                case 1:
                case 2:
                case 3:
                    i2 = 10436;
                    n.zzbt.set(false);
                    break;
                default:
                    i2 = 39789;
                    break;
            }
            if (str == null) {
                notificationManager.notify(i2, a2);
            } else {
                notificationManager.notify(str, i2, a2);
            }
        } else if (i == 6) {
            Log.w("GoogleApiAvailability", "Missing resolution for ConnectionResult.RESOLUTION_REQUIRED. Call GoogleApiAvailability#showErrorNotification(Context, ConnectionResult) instead.");
        }
    }

    private final String c() {
        String str;
        synchronized (c) {
            str = this.e;
        }
        return str;
    }

    public int a(Context context) {
        return super.a(context);
    }

    public Dialog a(Activity activity, int i, int i2, OnCancelListener onCancelListener) {
        return a((Context) activity, i, j.a(activity, b(activity, i, "d"), i2), onCancelListener);
    }

    public Dialog a(Activity activity, OnCancelListener onCancelListener) {
        ProgressBar progressBar = new ProgressBar(activity, null, 16842874);
        progressBar.setIndeterminate(true);
        progressBar.setVisibility(0);
        Builder builder = new Builder(activity);
        builder.setView(progressBar);
        builder.setMessage(com.google.android.gms.common.internal.g.c(activity, 18));
        builder.setPositiveButton("", null);
        AlertDialog create = builder.create();
        a(activity, (Dialog) create, "GooglePlayServicesUpdatingDialog", onCancelListener);
        return create;
    }

    public PendingIntent a(Context context, int i, int i2) {
        return super.a(context, i, i2);
    }

    public PendingIntent a(Context context, int i, int i2, String str) {
        return super.a(context, i, i2, str);
    }

    public PendingIntent a(Context context, b bVar) {
        return bVar.a() ? bVar.d() : a(context, bVar.c(), 0);
    }

    public com.google.android.gms.common.api.internal.c a(Context context, com.google.android.gms.common.api.internal.c.a aVar) {
        IntentFilter intentFilter = new IntentFilter("android.intent.action.PACKAGE_ADDED");
        intentFilter.addDataScheme("package");
        com.google.android.gms.common.api.internal.c cVar = new com.google.android.gms.common.api.internal.c(aVar);
        context.registerReceiver(cVar, intentFilter);
        cVar.a(context);
        if (a(context, "com.google.android.gms")) {
            return cVar;
        }
        aVar.a();
        cVar.a();
        return null;
    }

    public void a(Context context, int i) {
        a(context, i, (String) null);
    }

    public void a(Context context, int i, String str) {
        a(context, i, str, a(context, i, 0, "n"));
    }

    public final boolean a(int i) {
        return super.a(i);
    }

    public boolean a(Activity activity, f fVar, int i, int i2, OnCancelListener onCancelListener) {
        Dialog a2 = a((Context) activity, i, j.a(fVar, b(activity, i, "d"), i2), onCancelListener);
        if (a2 == null) {
            return false;
        }
        a(activity, a2, GooglePlayServicesUtil.GMS_ERROR_DIALOG, onCancelListener);
        return true;
    }

    public boolean a(Context context, b bVar, int i) {
        PendingIntent a2 = a(context, bVar);
        if (a2 == null) {
            return false;
        }
        a(context, bVar.c(), (String) null, GoogleApiActivity.a(context, a2, i));
        return true;
    }

    public int b(Context context) {
        return super.b(context);
    }

    public int b(Context context, int i) {
        return super.b(context, i);
    }

    public Intent b(Context context, int i, String str) {
        return super.b(context, i, str);
    }

    public final String b(int i) {
        return super.b(i);
    }

    public boolean b(Activity activity, int i, int i2, OnCancelListener onCancelListener) {
        Dialog a2 = a(activity, i, i2, onCancelListener);
        if (a2 == null) {
            return false;
        }
        a(activity, a2, GooglePlayServicesUtil.GMS_ERROR_DIALOG, onCancelListener);
        return true;
    }

    /* access modifiers changed from: 0000 */
    public final void c(Context context) {
        new a(context).sendEmptyMessageDelayed(1, 120000);
    }
}
