package com.google.android.gms.common;

import java.util.Arrays;

final class s extends a {

    /* renamed from: a reason: collision with root package name */
    private final byte[] f2322a;

    s(byte[] bArr) {
        super(Arrays.copyOfRange(bArr, 0, 25));
        this.f2322a = bArr;
    }

    /* access modifiers changed from: 0000 */
    public final byte[] a() {
        return this.f2322a;
    }
}
