package com.google.android.gms.common;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.text.TextUtils;
import com.google.android.gms.common.b.c;
import com.google.android.gms.common.internal.o;
import com.google.android.gms.common.util.i;

public class h {

    /* renamed from: a reason: collision with root package name */
    private static final h f2248a = new h();

    /* renamed from: b reason: collision with root package name */
    public static final int f2249b = n.GOOGLE_PLAY_SERVICES_VERSION_CODE;

    h() {
    }

    public static h b() {
        return f2248a;
    }

    private static String b(Context context, String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("gcore_");
        sb.append(f2249b);
        sb.append("-");
        if (!TextUtils.isEmpty(str)) {
            sb.append(str);
        }
        sb.append("-");
        if (context != null) {
            sb.append(context.getPackageName());
        }
        sb.append("-");
        if (context != null) {
            try {
                sb.append(c.b(context).b(context.getPackageName(), 0).versionCode);
            } catch (NameNotFoundException e) {
            }
        }
        return sb.toString();
    }

    public int a(Context context) {
        return b(context, f2249b);
    }

    public PendingIntent a(Context context, int i, int i2) {
        return a(context, i, i2, null);
    }

    public PendingIntent a(Context context, int i, int i2, String str) {
        Intent b2 = b(context, i, str);
        if (b2 == null) {
            return null;
        }
        return PendingIntent.getActivity(context, i2, b2, 134217728);
    }

    public boolean a(int i) {
        return n.isUserRecoverableError(i);
    }

    public boolean a(Context context, String str) {
        return n.isUninstalledAppPossiblyUpdating(context, str);
    }

    public int b(Context context) {
        return n.getApkVersion(context);
    }

    public int b(Context context, int i) {
        int isGooglePlayServicesAvailable = n.isGooglePlayServicesAvailable(context, i);
        if (n.isPlayServicesPossiblyUpdating(context, isGooglePlayServicesAvailable)) {
            return 18;
        }
        return isGooglePlayServicesAvailable;
    }

    public Intent b(Context context, int i, String str) {
        switch (i) {
            case 1:
            case 2:
                return (context == null || !i.b(context)) ? o.a("com.google.android.gms", b(context, str)) : o.a();
            case 3:
                return o.a("com.google.android.gms");
            default:
                return null;
        }
    }

    public String b(int i) {
        return n.getErrorString(i);
    }
}
