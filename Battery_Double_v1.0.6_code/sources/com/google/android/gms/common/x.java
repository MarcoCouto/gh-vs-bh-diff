package com.google.android.gms.common;

import android.util.Log;

class x {

    /* renamed from: b reason: collision with root package name */
    private static final x f2341b = new x(true, null, null);

    /* renamed from: a reason: collision with root package name */
    final boolean f2342a;
    private final String c;
    private final Throwable d;

    x(boolean z, String str, Throwable th) {
        this.f2342a = z;
        this.c = str;
        this.d = th;
    }

    static x a() {
        return f2341b;
    }

    static x a(String str) {
        return new x(false, str, null);
    }

    static x a(String str, a aVar, boolean z, boolean z2) {
        return new z(str, aVar, z, z2);
    }

    static x a(String str, Throwable th) {
        return new x(false, str, th);
    }

    /* access modifiers changed from: 0000 */
    public String b() {
        return this.c;
    }

    /* access modifiers changed from: 0000 */
    public final void c() {
        if (this.f2342a) {
            return;
        }
        if (this.d != null) {
            Log.d("GoogleCertificatesRslt", b(), this.d);
        } else {
            Log.d("GoogleCertificatesRslt", b());
        }
    }
}
