package com.google.a;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public final class i extends l implements Iterable<l> {

    /* renamed from: a reason: collision with root package name */
    private final List<l> f1884a = new ArrayList();

    public Number a() {
        if (this.f1884a.size() == 1) {
            return ((l) this.f1884a.get(0)).a();
        }
        throw new IllegalStateException();
    }

    public void a(l lVar) {
        if (lVar == null) {
            lVar = n.f1885a;
        }
        this.f1884a.add(lVar);
    }

    public String b() {
        if (this.f1884a.size() == 1) {
            return ((l) this.f1884a.get(0)).b();
        }
        throw new IllegalStateException();
    }

    public double c() {
        if (this.f1884a.size() == 1) {
            return ((l) this.f1884a.get(0)).c();
        }
        throw new IllegalStateException();
    }

    public long d() {
        if (this.f1884a.size() == 1) {
            return ((l) this.f1884a.get(0)).d();
        }
        throw new IllegalStateException();
    }

    public int e() {
        if (this.f1884a.size() == 1) {
            return ((l) this.f1884a.get(0)).e();
        }
        throw new IllegalStateException();
    }

    public boolean equals(Object obj) {
        return obj == this || ((obj instanceof i) && ((i) obj).f1884a.equals(this.f1884a));
    }

    public boolean f() {
        if (this.f1884a.size() == 1) {
            return ((l) this.f1884a.get(0)).f();
        }
        throw new IllegalStateException();
    }

    public int hashCode() {
        return this.f1884a.hashCode();
    }

    public Iterator<l> iterator() {
        return this.f1884a.iterator();
    }
}
