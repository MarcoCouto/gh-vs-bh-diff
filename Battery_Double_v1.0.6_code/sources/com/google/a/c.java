package com.google.a;

import com.google.a.b.a;
import java.lang.reflect.Field;

public final class c {

    /* renamed from: a reason: collision with root package name */
    private final Field f1864a;

    public c(Field field) {
        a.a(field);
        this.f1864a = field;
    }

    public Class<?> a() {
        return this.f1864a.getDeclaringClass();
    }
}
