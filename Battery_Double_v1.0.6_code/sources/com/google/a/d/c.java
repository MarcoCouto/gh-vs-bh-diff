package com.google.a.d;

import java.io.Closeable;
import java.io.Flushable;
import java.io.IOException;
import java.io.Writer;

public class c implements Closeable, Flushable {

    /* renamed from: a reason: collision with root package name */
    private static final String[] f1873a = new String[128];

    /* renamed from: b reason: collision with root package name */
    private static final String[] f1874b = ((String[]) f1873a.clone());
    private final Writer c;
    private int[] d = new int[32];
    private int e = 0;
    private String f;
    private String g;
    private boolean h;
    private boolean i;
    private String j;
    private boolean k;

    static {
        for (int i2 = 0; i2 <= 31; i2++) {
            f1873a[i2] = String.format("\\u%04x", new Object[]{Integer.valueOf(i2)});
        }
        f1873a[34] = "\\\"";
        f1873a[92] = "\\\\";
        f1873a[9] = "\\t";
        f1873a[8] = "\\b";
        f1873a[10] = "\\n";
        f1873a[13] = "\\r";
        f1873a[12] = "\\f";
        f1874b[60] = "\\u003c";
        f1874b[62] = "\\u003e";
        f1874b[38] = "\\u0026";
        f1874b[61] = "\\u003d";
        f1874b[39] = "\\u0027";
    }

    public c(Writer writer) {
        a(6);
        this.g = ":";
        this.k = true;
        if (writer == null) {
            throw new NullPointerException("out == null");
        }
        this.c = writer;
    }

    private int a() {
        if (this.e != 0) {
            return this.d[this.e - 1];
        }
        throw new IllegalStateException("JsonWriter is closed.");
    }

    private c a(int i2, int i3, String str) throws IOException {
        int a2 = a();
        if (a2 != i3 && a2 != i2) {
            throw new IllegalStateException("Nesting problem.");
        } else if (this.j != null) {
            throw new IllegalStateException("Dangling name: " + this.j);
        } else {
            this.e--;
            if (a2 == i3) {
                j();
            }
            this.c.write(str);
            return this;
        }
    }

    private c a(int i2, String str) throws IOException {
        l();
        a(i2);
        this.c.write(str);
        return this;
    }

    private void a(int i2) {
        if (this.e == this.d.length) {
            int[] iArr = new int[(this.e * 2)];
            System.arraycopy(this.d, 0, iArr, 0, this.e);
            this.d = iArr;
        }
        int[] iArr2 = this.d;
        int i3 = this.e;
        this.e = i3 + 1;
        iArr2[i3] = i2;
    }

    private void b(int i2) {
        this.d[this.e - 1] = i2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0030  */
    private void d(String str) throws IOException {
        String str2;
        int i2 = 0;
        String[] strArr = this.i ? f1874b : f1873a;
        this.c.write("\"");
        int length = str.length();
        for (int i3 = 0; i3 < length; i3++) {
            char charAt = str.charAt(i3);
            if (charAt < 128) {
                str2 = strArr[charAt];
                if (str2 == null) {
                }
                if (i2 < i3) {
                    this.c.write(str, i2, i3 - i2);
                }
                this.c.write(str2);
                i2 = i3 + 1;
            } else {
                if (charAt == 8232) {
                    str2 = "\\u2028";
                } else if (charAt == 8233) {
                    str2 = "\\u2029";
                }
                if (i2 < i3) {
                }
                this.c.write(str2);
                i2 = i3 + 1;
            }
        }
        if (i2 < length) {
            this.c.write(str, i2, length - i2);
        }
        this.c.write("\"");
    }

    private void i() throws IOException {
        if (this.j != null) {
            k();
            d(this.j);
            this.j = null;
        }
    }

    private void j() throws IOException {
        if (this.f != null) {
            this.c.write("\n");
            int i2 = this.e;
            for (int i3 = 1; i3 < i2; i3++) {
                this.c.write(this.f);
            }
        }
    }

    private void k() throws IOException {
        int a2 = a();
        if (a2 == 5) {
            this.c.write(44);
        } else if (a2 != 3) {
            throw new IllegalStateException("Nesting problem.");
        }
        j();
        b(4);
    }

    private void l() throws IOException {
        switch (a()) {
            case 1:
                b(2);
                j();
                return;
            case 2:
                this.c.append(',');
                j();
                return;
            case 4:
                this.c.append(this.g);
                b(5);
                return;
            case 6:
                break;
            case 7:
                if (!this.h) {
                    throw new IllegalStateException("JSON must have only one top-level value.");
                }
                break;
            default:
                throw new IllegalStateException("Nesting problem.");
        }
        b(7);
    }

    public c a(long j2) throws IOException {
        i();
        l();
        this.c.write(Long.toString(j2));
        return this;
    }

    public c a(Boolean bool) throws IOException {
        if (bool == null) {
            return f();
        }
        i();
        l();
        this.c.write(bool.booleanValue() ? "true" : "false");
        return this;
    }

    public c a(Number number) throws IOException {
        if (number == null) {
            return f();
        }
        i();
        String obj = number.toString();
        if (this.h || (!obj.equals("-Infinity") && !obj.equals("Infinity") && !obj.equals("NaN"))) {
            l();
            this.c.append(obj);
            return this;
        }
        throw new IllegalArgumentException("Numeric values must be finite, but was " + number);
    }

    public c a(String str) throws IOException {
        if (str == null) {
            throw new NullPointerException("name == null");
        } else if (this.j != null) {
            throw new IllegalStateException();
        } else if (this.e == 0) {
            throw new IllegalStateException("JsonWriter is closed.");
        } else {
            this.j = str;
            return this;
        }
    }

    public c a(boolean z) throws IOException {
        i();
        l();
        this.c.write(z ? "true" : "false");
        return this;
    }

    public c b() throws IOException {
        i();
        return a(1, "[");
    }

    public c b(String str) throws IOException {
        if (str == null) {
            return f();
        }
        i();
        l();
        d(str);
        return this;
    }

    public final void b(boolean z) {
        this.h = z;
    }

    public c c() throws IOException {
        return a(1, 2, "]");
    }

    public final void c(String str) {
        if (str.length() == 0) {
            this.f = null;
            this.g = ":";
            return;
        }
        this.f = str;
        this.g = ": ";
    }

    public final void c(boolean z) {
        this.k = z;
    }

    public void close() throws IOException {
        this.c.close();
        int i2 = this.e;
        if (i2 > 1 || (i2 == 1 && this.d[i2 - 1] != 7)) {
            throw new IOException("Incomplete document");
        }
        this.e = 0;
    }

    public c d() throws IOException {
        i();
        return a(3, "{");
    }

    public c e() throws IOException {
        return a(3, 5, "}");
    }

    public c f() throws IOException {
        if (this.j != null) {
            if (this.k) {
                i();
            } else {
                this.j = null;
                return this;
            }
        }
        l();
        this.c.write("null");
        return this;
    }

    public void flush() throws IOException {
        if (this.e == 0) {
            throw new IllegalStateException("JsonWriter is closed.");
        }
        this.c.flush();
    }

    public boolean g() {
        return this.h;
    }

    public final boolean h() {
        return this.k;
    }
}
