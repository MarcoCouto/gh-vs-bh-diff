package com.google.a;

import com.google.a.b.a.l;
import com.google.a.b.d;
import com.google.a.c.a;
import java.lang.reflect.Type;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class g {

    /* renamed from: a reason: collision with root package name */
    private d f1882a = d.f1842a;

    /* renamed from: b reason: collision with root package name */
    private u f1883b = u.DEFAULT;
    private e c = d.IDENTITY;
    private final Map<Type, h<?>> d = new HashMap();
    private final List<w> e = new ArrayList();
    private final List<w> f = new ArrayList();
    private boolean g = false;
    private String h;
    private int i = 2;
    private int j = 2;
    private boolean k = false;
    private boolean l = false;
    private boolean m = true;
    private boolean n = false;
    private boolean o = false;
    private boolean p = false;

    private void a(String str, int i2, int i3, List<w> list) {
        a aVar;
        if (str != null && !"".equals(str.trim())) {
            aVar = new a(str);
        } else if (i2 != 2 && i3 != 2) {
            aVar = new a(i2, i3);
        } else {
            return;
        }
        list.add(l.a(a.b(Date.class), (Object) aVar));
        list.add(l.a(a.b(Timestamp.class), (Object) aVar));
        list.add(l.a(a.b(java.sql.Date.class), (Object) aVar));
    }

    public f a() {
        ArrayList arrayList = new ArrayList();
        arrayList.addAll(this.e);
        Collections.reverse(arrayList);
        arrayList.addAll(this.f);
        a(this.h, this.i, this.j, arrayList);
        return new f(this.f1882a, this.c, this.d, this.g, this.k, this.o, this.m, this.n, this.p, this.l, this.f1883b, arrayList);
    }

    public g a(b... bVarArr) {
        for (b a2 : bVarArr) {
            this.f1882a = this.f1882a.a(a2, true, true);
        }
        return this;
    }
}
