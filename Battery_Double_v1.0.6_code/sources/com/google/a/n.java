package com.google.a;

public final class n extends l {

    /* renamed from: a reason: collision with root package name */
    public static final n f1885a = new n();

    public boolean equals(Object obj) {
        return this == obj || (obj instanceof n);
    }

    public int hashCode() {
        return n.class.hashCode();
    }
}
