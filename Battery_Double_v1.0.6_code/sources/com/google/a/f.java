package com.google.a;

import com.google.a.b.a.b;
import com.google.a.b.a.g;
import com.google.a.b.a.h;
import com.google.a.b.a.i;
import com.google.a.b.a.j;
import com.google.a.b.a.k;
import com.google.a.b.a.n;
import com.google.a.b.c;
import com.google.a.b.d;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicLongArray;

public final class f {

    /* renamed from: a reason: collision with root package name */
    private static final com.google.a.c.a<?> f1875a = new com.google.a.c.a<Object>() {
    };

    /* renamed from: b reason: collision with root package name */
    private final ThreadLocal<Map<com.google.a.c.a<?>, a<?>>> f1876b;
    private final Map<com.google.a.c.a<?>, v<?>> c;
    private final List<w> d;
    private final c e;
    private final d f;
    private final e g;
    private final boolean h;
    private final boolean i;
    private final boolean j;
    private final boolean k;
    private final boolean l;
    private final com.google.a.b.a.d m;

    static class a<T> extends v<T> {

        /* renamed from: a reason: collision with root package name */
        private v<T> f1881a;

        a() {
        }

        public void a(com.google.a.d.c cVar, T t) throws IOException {
            if (this.f1881a == null) {
                throw new IllegalStateException();
            }
            this.f1881a.a(cVar, t);
        }

        public void a(v<T> vVar) {
            if (this.f1881a != null) {
                throw new AssertionError();
            }
            this.f1881a = vVar;
        }

        public T b(com.google.a.d.a aVar) throws IOException {
            if (this.f1881a != null) {
                return this.f1881a.b(aVar);
            }
            throw new IllegalStateException();
        }
    }

    public f() {
        this(d.f1842a, d.IDENTITY, Collections.emptyMap(), false, false, false, true, false, false, false, u.DEFAULT, Collections.emptyList());
    }

    f(d dVar, e eVar, Map<Type, h<?>> map, boolean z, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7, u uVar, List<w> list) {
        this.f1876b = new ThreadLocal<>();
        this.c = new ConcurrentHashMap();
        this.e = new c(map);
        this.f = dVar;
        this.g = eVar;
        this.h = z;
        this.j = z3;
        this.i = z4;
        this.k = z5;
        this.l = z6;
        ArrayList arrayList = new ArrayList();
        arrayList.add(n.Y);
        arrayList.add(h.f1779a);
        arrayList.add(dVar);
        arrayList.addAll(list);
        arrayList.add(n.D);
        arrayList.add(n.m);
        arrayList.add(n.g);
        arrayList.add(n.i);
        arrayList.add(n.k);
        v a2 = a(uVar);
        arrayList.add(n.a(Long.TYPE, Long.class, a2));
        arrayList.add(n.a(Double.TYPE, Double.class, a(z7)));
        arrayList.add(n.a(Float.TYPE, Float.class, b(z7)));
        arrayList.add(n.x);
        arrayList.add(n.o);
        arrayList.add(n.q);
        arrayList.add(n.a(AtomicLong.class, a(a2)));
        arrayList.add(n.a(AtomicLongArray.class, b(a2)));
        arrayList.add(n.s);
        arrayList.add(n.z);
        arrayList.add(n.F);
        arrayList.add(n.H);
        arrayList.add(n.a(BigDecimal.class, n.B));
        arrayList.add(n.a(BigInteger.class, n.C));
        arrayList.add(n.J);
        arrayList.add(n.L);
        arrayList.add(n.P);
        arrayList.add(n.R);
        arrayList.add(n.W);
        arrayList.add(n.N);
        arrayList.add(n.d);
        arrayList.add(com.google.a.b.a.c.f1769a);
        arrayList.add(n.U);
        arrayList.add(k.f1790a);
        arrayList.add(j.f1788a);
        arrayList.add(n.S);
        arrayList.add(com.google.a.b.a.a.f1763a);
        arrayList.add(n.f1800b);
        arrayList.add(new b(this.e));
        arrayList.add(new g(this.e, z2));
        this.m = new com.google.a.b.a.d(this.e);
        arrayList.add(this.m);
        arrayList.add(n.Z);
        arrayList.add(new i(this.e, eVar, dVar, this.m));
        this.d = Collections.unmodifiableList(arrayList);
    }

    private static v<Number> a(u uVar) {
        return uVar == u.DEFAULT ? n.t : new v<Number>() {
            /* renamed from: a */
            public Number b(com.google.a.d.a aVar) throws IOException {
                if (aVar.f() != com.google.a.d.b.NULL) {
                    return Long.valueOf(aVar.l());
                }
                aVar.j();
                return null;
            }

            public void a(com.google.a.d.c cVar, Number number) throws IOException {
                if (number == null) {
                    cVar.f();
                } else {
                    cVar.b(number.toString());
                }
            }
        };
    }

    private static v<AtomicLong> a(final v<Number> vVar) {
        return new v<AtomicLong>() {
            /* renamed from: a */
            public AtomicLong b(com.google.a.d.a aVar) throws IOException {
                return new AtomicLong(((Number) vVar.b(aVar)).longValue());
            }

            public void a(com.google.a.d.c cVar, AtomicLong atomicLong) throws IOException {
                vVar.a(cVar, Long.valueOf(atomicLong.get()));
            }
        }.a();
    }

    private v<Number> a(boolean z) {
        return z ? n.v : new v<Number>() {
            /* renamed from: a */
            public Double b(com.google.a.d.a aVar) throws IOException {
                if (aVar.f() != com.google.a.d.b.NULL) {
                    return Double.valueOf(aVar.k());
                }
                aVar.j();
                return null;
            }

            public void a(com.google.a.d.c cVar, Number number) throws IOException {
                if (number == null) {
                    cVar.f();
                    return;
                }
                f.a(number.doubleValue());
                cVar.a(number);
            }
        };
    }

    static void a(double d2) {
        if (Double.isNaN(d2) || Double.isInfinite(d2)) {
            throw new IllegalArgumentException(d2 + " is not a valid double value as per JSON specification. To override this behavior, use GsonBuilder.serializeSpecialFloatingPointValues() method.");
        }
    }

    private static v<AtomicLongArray> b(final v<Number> vVar) {
        return new v<AtomicLongArray>() {
            /* renamed from: a */
            public AtomicLongArray b(com.google.a.d.a aVar) throws IOException {
                ArrayList arrayList = new ArrayList();
                aVar.a();
                while (aVar.e()) {
                    arrayList.add(Long.valueOf(((Number) vVar.b(aVar)).longValue()));
                }
                aVar.b();
                int size = arrayList.size();
                AtomicLongArray atomicLongArray = new AtomicLongArray(size);
                for (int i = 0; i < size; i++) {
                    atomicLongArray.set(i, ((Long) arrayList.get(i)).longValue());
                }
                return atomicLongArray;
            }

            public void a(com.google.a.d.c cVar, AtomicLongArray atomicLongArray) throws IOException {
                cVar.b();
                int length = atomicLongArray.length();
                for (int i = 0; i < length; i++) {
                    vVar.a(cVar, Long.valueOf(atomicLongArray.get(i)));
                }
                cVar.c();
            }
        }.a();
    }

    private v<Number> b(boolean z) {
        return z ? n.u : new v<Number>() {
            /* renamed from: a */
            public Float b(com.google.a.d.a aVar) throws IOException {
                if (aVar.f() != com.google.a.d.b.NULL) {
                    return Float.valueOf((float) aVar.k());
                }
                aVar.j();
                return null;
            }

            public void a(com.google.a.d.c cVar, Number number) throws IOException {
                if (number == null) {
                    cVar.f();
                    return;
                }
                f.a((double) number.floatValue());
                cVar.a(number);
            }
        };
    }

    public com.google.a.d.a a(Reader reader) {
        com.google.a.d.a aVar = new com.google.a.d.a(reader);
        aVar.a(this.l);
        return aVar;
    }

    public com.google.a.d.c a(Writer writer) throws IOException {
        if (this.j) {
            writer.write(")]}'\n");
        }
        com.google.a.d.c cVar = new com.google.a.d.c(writer);
        if (this.k) {
            cVar.c("  ");
        }
        cVar.c(this.h);
        return cVar;
    }

    /* JADX WARNING: Incorrect type for immutable var: ssa=com.google.a.c.a<T>, code=com.google.a.c.a, for r6v0, types: [com.google.a.c.a<T>, java.lang.Object, com.google.a.c.a] */
    public <T> v<T> a(com.google.a.c.a aVar) {
        Map map;
        v<T> vVar = (v) this.c.get(aVar == null ? f1875a : aVar);
        if (vVar == null) {
            Map map2 = (Map) this.f1876b.get();
            boolean z = false;
            if (map2 == null) {
                HashMap hashMap = new HashMap();
                this.f1876b.set(hashMap);
                map = hashMap;
                z = true;
            } else {
                map = map2;
            }
            vVar = (a) map.get(aVar);
            if (vVar == null) {
                try {
                    a aVar2 = new a();
                    map.put(aVar, aVar2);
                    for (w a2 : this.d) {
                        vVar = a2.a(this, aVar);
                        if (vVar != null) {
                            aVar2.a(vVar);
                            this.c.put(aVar, vVar);
                            map.remove(aVar);
                            if (z) {
                                this.f1876b.remove();
                            }
                        }
                    }
                    throw new IllegalArgumentException("GSON cannot handle " + aVar);
                } catch (Throwable th) {
                    map.remove(aVar);
                    if (z) {
                        this.f1876b.remove();
                    }
                    throw th;
                }
            }
        }
        return vVar;
    }

    public <T> v<T> a(w wVar, com.google.a.c.a<T> aVar) {
        if (!this.d.contains(wVar)) {
            wVar = this.m;
        }
        boolean z = false;
        for (w wVar2 : this.d) {
            if (z) {
                v<T> a2 = wVar2.a(this, aVar);
                if (a2 != null) {
                    return a2;
                }
            } else if (wVar2 == wVar) {
                z = true;
            }
        }
        throw new IllegalArgumentException("GSON cannot serialize " + aVar);
    }

    public <T> v<T> a(Class<T> cls) {
        return a(com.google.a.c.a.b(cls));
    }

    public String toString() {
        return "{serializeNulls:" + this.h + "factories:" + this.d + ",instanceCreators:" + this.e + "}";
    }
}
