package com.google.a.c;

import com.google.a.b.b;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

public class a<T> {

    /* renamed from: a reason: collision with root package name */
    final Class<? super T> f1865a;

    /* renamed from: b reason: collision with root package name */
    final Type f1866b;
    final int c;

    protected a() {
        this.f1866b = a(getClass());
        this.f1865a = b.e(this.f1866b);
        this.c = this.f1866b.hashCode();
    }

    a(Type type) {
        this.f1866b = b.d((Type) com.google.a.b.a.a(type));
        this.f1865a = b.e(this.f1866b);
        this.c = this.f1866b.hashCode();
    }

    public static a<?> a(Type type) {
        return new a<>(type);
    }

    static Type a(Class<?> cls) {
        Type genericSuperclass = cls.getGenericSuperclass();
        if (!(genericSuperclass instanceof Class)) {
            return b.d(((ParameterizedType) genericSuperclass).getActualTypeArguments()[0]);
        }
        throw new RuntimeException("Missing type parameter.");
    }

    public static <T> a<T> b(Class<T> cls) {
        return new a<>(cls);
    }

    public final Class<? super T> a() {
        return this.f1865a;
    }

    public final Type b() {
        return this.f1866b;
    }

    public final boolean equals(Object obj) {
        return (obj instanceof a) && b.a(this.f1866b, ((a) obj).f1866b);
    }

    public final int hashCode() {
        return this.c;
    }

    public final String toString() {
        return b.f(this.f1866b);
    }
}
