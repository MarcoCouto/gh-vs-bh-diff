package com.google.a;

import com.google.a.b.a;
import com.google.a.b.f;
import java.math.BigInteger;

public final class q extends l {

    /* renamed from: a reason: collision with root package name */
    private static final Class<?>[] f1887a = {Integer.TYPE, Long.TYPE, Short.TYPE, Float.TYPE, Double.TYPE, Byte.TYPE, Boolean.TYPE, Character.TYPE, Integer.class, Long.class, Short.class, Float.class, Double.class, Byte.class, Boolean.class, Character.class};

    /* renamed from: b reason: collision with root package name */
    private Object f1888b;

    public q(Boolean bool) {
        a((Object) bool);
    }

    public q(Number number) {
        a((Object) number);
    }

    q(Object obj) {
        a(obj);
    }

    public q(String str) {
        a((Object) str);
    }

    private static boolean a(q qVar) {
        if (!(qVar.f1888b instanceof Number)) {
            return false;
        }
        Number number = (Number) qVar.f1888b;
        return (number instanceof BigInteger) || (number instanceof Long) || (number instanceof Integer) || (number instanceof Short) || (number instanceof Byte);
    }

    private static boolean b(Object obj) {
        if (obj instanceof String) {
            return true;
        }
        Class cls = obj.getClass();
        for (Class<?> isAssignableFrom : f1887a) {
            if (isAssignableFrom.isAssignableFrom(cls)) {
                return true;
            }
        }
        return false;
    }

    public Number a() {
        return this.f1888b instanceof String ? new f((String) this.f1888b) : (Number) this.f1888b;
    }

    /* access modifiers changed from: 0000 */
    public void a(Object obj) {
        if (obj instanceof Character) {
            this.f1888b = String.valueOf(((Character) obj).charValue());
            return;
        }
        a.a((obj instanceof Number) || b(obj));
        this.f1888b = obj;
    }

    public String b() {
        return p() ? a().toString() : o() ? n().toString() : (String) this.f1888b;
    }

    public double c() {
        return p() ? a().doubleValue() : Double.parseDouble(b());
    }

    public long d() {
        return p() ? a().longValue() : Long.parseLong(b());
    }

    public int e() {
        return p() ? a().intValue() : Integer.parseInt(b());
    }

    public boolean equals(Object obj) {
        boolean z = false;
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        q qVar = (q) obj;
        if (this.f1888b == null) {
            return qVar.f1888b == null;
        }
        if (a(this) && a(qVar)) {
            return a().longValue() == qVar.a().longValue();
        }
        if (!(this.f1888b instanceof Number) || !(qVar.f1888b instanceof Number)) {
            return this.f1888b.equals(qVar.f1888b);
        }
        double doubleValue = a().doubleValue();
        double doubleValue2 = qVar.a().doubleValue();
        if (doubleValue == doubleValue2 || (Double.isNaN(doubleValue) && Double.isNaN(doubleValue2))) {
            z = true;
        }
        return z;
    }

    public boolean f() {
        return o() ? n().booleanValue() : Boolean.parseBoolean(b());
    }

    public int hashCode() {
        if (this.f1888b == null) {
            return 31;
        }
        if (a(this)) {
            long longValue = a().longValue();
            return (int) (longValue ^ (longValue >>> 32));
        } else if (!(this.f1888b instanceof Number)) {
            return this.f1888b.hashCode();
        } else {
            long doubleToLongBits = Double.doubleToLongBits(a().doubleValue());
            return (int) (doubleToLongBits ^ (doubleToLongBits >>> 32));
        }
    }

    /* access modifiers changed from: 0000 */
    public Boolean n() {
        return (Boolean) this.f1888b;
    }

    public boolean o() {
        return this.f1888b instanceof Boolean;
    }

    public boolean p() {
        return this.f1888b instanceof Number;
    }

    public boolean q() {
        return this.f1888b instanceof String;
    }
}
