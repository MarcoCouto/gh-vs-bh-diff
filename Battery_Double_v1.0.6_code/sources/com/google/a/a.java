package com.google.a;

import java.lang.reflect.Type;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

final class a implements k<Date>, s<Date> {

    /* renamed from: a reason: collision with root package name */
    private final DateFormat f1761a;

    /* renamed from: b reason: collision with root package name */
    private final DateFormat f1762b;

    a() {
        this(DateFormat.getDateTimeInstance(2, 2, Locale.US), DateFormat.getDateTimeInstance(2, 2));
    }

    public a(int i, int i2) {
        this(DateFormat.getDateTimeInstance(i, i2, Locale.US), DateFormat.getDateTimeInstance(i, i2));
    }

    a(String str) {
        this((DateFormat) new SimpleDateFormat(str, Locale.US), (DateFormat) new SimpleDateFormat(str));
    }

    a(DateFormat dateFormat, DateFormat dateFormat2) {
        this.f1761a = dateFormat;
        this.f1762b = dateFormat2;
    }

    private Date a(l lVar) {
        Date a2;
        synchronized (this.f1762b) {
            try {
                a2 = this.f1762b.parse(lVar.b());
            } catch (ParseException e) {
                throw new t(lVar.b(), e);
            } catch (ParseException e2) {
                try {
                    a2 = this.f1761a.parse(lVar.b());
                } catch (ParseException e3) {
                    a2 = com.google.a.b.a.a.a.a(lVar.b(), new ParsePosition(0));
                }
            }
        }
        return a2;
    }

    public l a(Date date, Type type, r rVar) {
        q qVar;
        synchronized (this.f1762b) {
            qVar = new q(this.f1761a.format(date));
        }
        return qVar;
    }

    /* renamed from: a */
    public Date b(l lVar, Type type, j jVar) throws p {
        if (!(lVar instanceof q)) {
            throw new p("The date should be a string value");
        }
        Date a2 = a(lVar);
        if (type == Date.class) {
            return a2;
        }
        if (type == Timestamp.class) {
            return new Timestamp(a2.getTime());
        }
        if (type == java.sql.Date.class) {
            return new java.sql.Date(a2.getTime());
        }
        throw new IllegalArgumentException(getClass() + " cannot deserialize to " + type);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(a.class.getSimpleName());
        sb.append('(').append(this.f1762b.getClass().getSimpleName()).append(')');
        return sb.toString();
    }
}
