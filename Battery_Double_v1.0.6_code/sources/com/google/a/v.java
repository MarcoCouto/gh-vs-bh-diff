package com.google.a;

import com.google.a.b.a.f;
import com.google.a.d.a;
import com.google.a.d.b;
import com.google.a.d.c;
import java.io.IOException;

public abstract class v<T> {
    public final l a(T t) {
        try {
            f fVar = new f();
            a(fVar, t);
            return fVar.a();
        } catch (IOException e) {
            throw new m((Throwable) e);
        }
    }

    public final v<T> a() {
        return new v<T>() {
            public void a(c cVar, T t) throws IOException {
                if (t == null) {
                    cVar.f();
                } else {
                    v.this.a(cVar, t);
                }
            }

            public T b(a aVar) throws IOException {
                if (aVar.f() != b.NULL) {
                    return v.this.b(aVar);
                }
                aVar.j();
                return null;
            }
        };
    }

    public abstract void a(c cVar, T t) throws IOException;

    public abstract T b(a aVar) throws IOException;
}
