package com.google.a.b.a;

import com.google.a.d.c;
import com.google.a.i;
import com.google.a.l;
import com.google.a.n;
import com.google.a.o;
import com.google.a.q;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

public final class f extends c {

    /* renamed from: a reason: collision with root package name */
    private static final Writer f1773a = new Writer() {
        public void close() throws IOException {
            throw new AssertionError();
        }

        public void flush() throws IOException {
            throw new AssertionError();
        }

        public void write(char[] cArr, int i, int i2) {
            throw new AssertionError();
        }
    };

    /* renamed from: b reason: collision with root package name */
    private static final q f1774b = new q("closed");
    private final List<l> c = new ArrayList();
    private String d;
    private l e = n.f1885a;

    public f() {
        super(f1773a);
    }

    private void a(l lVar) {
        if (this.d != null) {
            if (!lVar.j() || h()) {
                ((o) i()).a(this.d, lVar);
            }
            this.d = null;
        } else if (this.c.isEmpty()) {
            this.e = lVar;
        } else {
            l i = i();
            if (i instanceof i) {
                ((i) i).a(lVar);
                return;
            }
            throw new IllegalStateException();
        }
    }

    private l i() {
        return (l) this.c.get(this.c.size() - 1);
    }

    public c a(long j) throws IOException {
        a((l) new q((Number) Long.valueOf(j)));
        return this;
    }

    public c a(Boolean bool) throws IOException {
        if (bool == null) {
            return f();
        }
        a((l) new q(bool));
        return this;
    }

    public c a(Number number) throws IOException {
        if (number == null) {
            return f();
        }
        if (!g()) {
            double doubleValue = number.doubleValue();
            if (Double.isNaN(doubleValue) || Double.isInfinite(doubleValue)) {
                throw new IllegalArgumentException("JSON forbids NaN and infinities: " + number);
            }
        }
        a((l) new q(number));
        return this;
    }

    public c a(String str) throws IOException {
        if (this.c.isEmpty() || this.d != null) {
            throw new IllegalStateException();
        } else if (i() instanceof o) {
            this.d = str;
            return this;
        } else {
            throw new IllegalStateException();
        }
    }

    public c a(boolean z) throws IOException {
        a((l) new q(Boolean.valueOf(z)));
        return this;
    }

    public l a() {
        if (this.c.isEmpty()) {
            return this.e;
        }
        throw new IllegalStateException("Expected one JSON element but was " + this.c);
    }

    public c b() throws IOException {
        i iVar = new i();
        a((l) iVar);
        this.c.add(iVar);
        return this;
    }

    public c b(String str) throws IOException {
        if (str == null) {
            return f();
        }
        a((l) new q(str));
        return this;
    }

    public c c() throws IOException {
        if (this.c.isEmpty() || this.d != null) {
            throw new IllegalStateException();
        } else if (i() instanceof i) {
            this.c.remove(this.c.size() - 1);
            return this;
        } else {
            throw new IllegalStateException();
        }
    }

    public void close() throws IOException {
        if (!this.c.isEmpty()) {
            throw new IOException("Incomplete document");
        }
        this.c.add(f1774b);
    }

    public c d() throws IOException {
        o oVar = new o();
        a((l) oVar);
        this.c.add(oVar);
        return this;
    }

    public c e() throws IOException {
        if (this.c.isEmpty() || this.d != null) {
            throw new IllegalStateException();
        } else if (i() instanceof o) {
            this.c.remove(this.c.size() - 1);
            return this;
        } else {
            throw new IllegalStateException();
        }
    }

    public c f() throws IOException {
        a((l) n.f1885a);
        return this;
    }

    public void flush() throws IOException {
    }
}
