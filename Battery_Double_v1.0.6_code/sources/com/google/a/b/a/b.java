package com.google.a.b.a;

import com.google.a.b.c;
import com.google.a.b.h;
import com.google.a.f;
import com.google.a.v;
import com.google.a.w;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Collection;

public final class b implements w {

    /* renamed from: a reason: collision with root package name */
    private final c f1766a;

    private static final class a<E> extends v<Collection<E>> {

        /* renamed from: a reason: collision with root package name */
        private final v<E> f1767a;

        /* renamed from: b reason: collision with root package name */
        private final h<? extends Collection<E>> f1768b;

        public a(f fVar, Type type, v<E> vVar, h<? extends Collection<E>> hVar) {
            this.f1767a = new m(fVar, vVar, type);
            this.f1768b = hVar;
        }

        /* renamed from: a */
        public Collection<E> b(com.google.a.d.a aVar) throws IOException {
            if (aVar.f() == com.google.a.d.b.NULL) {
                aVar.j();
                return null;
            }
            Collection<E> collection = (Collection) this.f1768b.a();
            aVar.a();
            while (aVar.e()) {
                collection.add(this.f1767a.b(aVar));
            }
            aVar.b();
            return collection;
        }

        public void a(com.google.a.d.c cVar, Collection<E> collection) throws IOException {
            if (collection == null) {
                cVar.f();
                return;
            }
            cVar.b();
            for (E a2 : collection) {
                this.f1767a.a(cVar, a2);
            }
            cVar.c();
        }
    }

    public b(c cVar) {
        this.f1766a = cVar;
    }

    public <T> v<T> a(f fVar, com.google.a.c.a<T> aVar) {
        Type b2 = aVar.b();
        Class a2 = aVar.a();
        if (!Collection.class.isAssignableFrom(a2)) {
            return null;
        }
        Type a3 = com.google.a.b.b.a(b2, a2);
        return new a(fVar, a3, fVar.a(com.google.a.c.a.a(a3)), this.f1766a.a(aVar));
    }
}
