package com.google.a.b.a;

import com.google.a.c.a;
import com.google.a.d.b;
import com.google.a.d.c;
import com.google.a.f;
import com.google.a.t;
import com.google.a.v;
import com.google.a.w;
import java.io.IOException;
import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public final class k extends v<Time> {

    /* renamed from: a reason: collision with root package name */
    public static final w f1790a = new w() {
        public <T> v<T> a(f fVar, a<T> aVar) {
            if (aVar.a() == Time.class) {
                return new k();
            }
            return null;
        }
    };

    /* renamed from: b reason: collision with root package name */
    private final DateFormat f1791b = new SimpleDateFormat("hh:mm:ss a");

    /* renamed from: a */
    public synchronized Time b(com.google.a.d.a aVar) throws IOException {
        Time time;
        if (aVar.f() == b.NULL) {
            aVar.j();
            time = null;
        } else {
            try {
                time = new Time(this.f1791b.parse(aVar.h()).getTime());
            } catch (ParseException e) {
                throw new t((Throwable) e);
            }
        }
        return time;
    }

    public synchronized void a(c cVar, Time time) throws IOException {
        cVar.b(time == null ? null : this.f1791b.format(time));
    }
}
