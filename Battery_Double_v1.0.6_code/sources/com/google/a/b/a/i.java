package com.google.a.b.a;

import com.google.a.b.c;
import com.google.a.b.d;
import com.google.a.b.h;
import com.google.a.e;
import com.google.a.f;
import com.google.a.t;
import com.google.a.v;
import com.google.a.w;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public final class i implements w {

    /* renamed from: a reason: collision with root package name */
    private final c f1782a;

    /* renamed from: b reason: collision with root package name */
    private final e f1783b;
    private final d c;
    private final d d;

    public static final class a<T> extends v<T> {

        /* renamed from: a reason: collision with root package name */
        private final h<T> f1786a;

        /* renamed from: b reason: collision with root package name */
        private final Map<String, b> f1787b;

        a(h<T> hVar, Map<String, b> map) {
            this.f1786a = hVar;
            this.f1787b = map;
        }

        public void a(com.google.a.d.c cVar, T t) throws IOException {
            if (t == null) {
                cVar.f();
                return;
            }
            cVar.d();
            try {
                for (b bVar : this.f1787b.values()) {
                    if (bVar.a(t)) {
                        cVar.a(bVar.h);
                        bVar.a(cVar, (Object) t);
                    }
                }
                cVar.e();
            } catch (IllegalAccessException e) {
                throw new AssertionError(e);
            }
        }

        public T b(com.google.a.d.a aVar) throws IOException {
            if (aVar.f() == com.google.a.d.b.NULL) {
                aVar.j();
                return null;
            }
            Object a2 = this.f1786a.a();
            try {
                aVar.c();
                while (aVar.e()) {
                    b bVar = (b) this.f1787b.get(aVar.g());
                    if (bVar == null || !bVar.j) {
                        aVar.n();
                    } else {
                        bVar.a(aVar, a2);
                    }
                }
                aVar.d();
                return a2;
            } catch (IllegalStateException e) {
                throw new t((Throwable) e);
            } catch (IllegalAccessException e2) {
                throw new AssertionError(e2);
            }
        }
    }

    static abstract class b {
        final String h;
        final boolean i;
        final boolean j;

        protected b(String str, boolean z, boolean z2) {
            this.h = str;
            this.i = z;
            this.j = z2;
        }

        /* access modifiers changed from: 0000 */
        public abstract void a(com.google.a.d.a aVar, Object obj) throws IOException, IllegalAccessException;

        /* access modifiers changed from: 0000 */
        public abstract void a(com.google.a.d.c cVar, Object obj) throws IOException, IllegalAccessException;

        /* access modifiers changed from: 0000 */
        public abstract boolean a(Object obj) throws IOException, IllegalAccessException;
    }

    public i(c cVar, e eVar, d dVar, d dVar2) {
        this.f1782a = cVar;
        this.f1783b = eVar;
        this.c = dVar;
        this.d = dVar2;
    }

    private b a(f fVar, Field field, String str, com.google.a.c.a<?> aVar, boolean z, boolean z2) {
        final boolean a2 = com.google.a.b.i.a(aVar.a());
        com.google.a.a.b bVar = (com.google.a.a.b) field.getAnnotation(com.google.a.a.b.class);
        final v vVar = null;
        if (bVar != null) {
            vVar = this.d.a(this.f1782a, fVar, aVar, bVar);
        }
        final boolean z3 = vVar != null;
        if (vVar == null) {
            vVar = fVar.a(aVar);
        }
        final Field field2 = field;
        final f fVar2 = fVar;
        final com.google.a.c.a<?> aVar2 = aVar;
        return new b(str, z, z2) {
            /* access modifiers changed from: 0000 */
            public void a(com.google.a.d.a aVar, Object obj) throws IOException, IllegalAccessException {
                Object b2 = vVar.b(aVar);
                if (b2 != null || !a2) {
                    field2.set(obj, b2);
                }
            }

            /* access modifiers changed from: 0000 */
            public void a(com.google.a.d.c cVar, Object obj) throws IOException, IllegalAccessException {
                (z3 ? vVar : new m(fVar2, vVar, aVar2.b())).a(cVar, field2.get(obj));
            }

            public boolean a(Object obj) throws IOException, IllegalAccessException {
                return this.i && field2.get(obj) != obj;
            }
        };
    }

    private List<String> a(Field field) {
        com.google.a.a.c cVar = (com.google.a.a.c) field.getAnnotation(com.google.a.a.c.class);
        if (cVar == null) {
            return Collections.singletonList(this.f1783b.a(field));
        }
        String a2 = cVar.a();
        String[] b2 = cVar.b();
        if (b2.length == 0) {
            return Collections.singletonList(a2);
        }
        ArrayList arrayList = new ArrayList(b2.length + 1);
        arrayList.add(a2);
        for (String add : b2) {
            arrayList.add(add);
        }
        return arrayList;
    }

    private Map<String, b> a(f fVar, com.google.a.c.a<?> aVar, Class<?> cls) {
        Field[] declaredFields;
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        if (cls.isInterface()) {
            return linkedHashMap;
        }
        Type b2 = aVar.b();
        while (cls != Object.class) {
            for (Field field : cls.getDeclaredFields()) {
                boolean a2 = a(field, true);
                boolean a3 = a(field, false);
                if (a2 || a3) {
                    field.setAccessible(true);
                    Type a4 = com.google.a.b.b.a(aVar.b(), cls, field.getGenericType());
                    List a5 = a(field);
                    b bVar = null;
                    int i = 0;
                    while (i < a5.size()) {
                        String str = (String) a5.get(i);
                        if (i != 0) {
                            a2 = false;
                        }
                        b bVar2 = (b) linkedHashMap.put(str, a(fVar, field, str, com.google.a.c.a.a(a4), a2, a3));
                        if (bVar != null) {
                            bVar2 = bVar;
                        }
                        i++;
                        bVar = bVar2;
                    }
                    if (bVar != null) {
                        throw new IllegalArgumentException(b2 + " declares multiple JSON fields named " + bVar.h);
                    }
                }
            }
            aVar = com.google.a.c.a.a(com.google.a.b.b.a(aVar.b(), cls, cls.getGenericSuperclass()));
            cls = aVar.a();
        }
        return linkedHashMap;
    }

    static boolean a(Field field, boolean z, d dVar) {
        return !dVar.a(field.getType(), z) && !dVar.a(field, z);
    }

    public <T> v<T> a(f fVar, com.google.a.c.a<T> aVar) {
        Class a2 = aVar.a();
        if (!Object.class.isAssignableFrom(a2)) {
            return null;
        }
        return new a(this.f1782a.a(aVar), a(fVar, aVar, a2));
    }

    public boolean a(Field field, boolean z) {
        return a(field, z, this.c);
    }
}
