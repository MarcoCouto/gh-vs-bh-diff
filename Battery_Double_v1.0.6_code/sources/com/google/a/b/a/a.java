package com.google.a.b.a;

import com.google.a.b.b;
import com.google.a.d.c;
import com.google.a.f;
import com.google.a.v;
import com.google.a.w;
import java.io.IOException;
import java.lang.reflect.Array;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.Type;
import java.util.ArrayList;

public final class a<E> extends v<Object> {

    /* renamed from: a reason: collision with root package name */
    public static final w f1763a = new w() {
        public <T> v<T> a(f fVar, com.google.a.c.a<T> aVar) {
            Type b2 = aVar.b();
            if (!(b2 instanceof GenericArrayType) && (!(b2 instanceof Class) || !((Class) b2).isArray())) {
                return null;
            }
            Type g = b.g(b2);
            return new a(fVar, fVar.a(com.google.a.c.a.a(g)), b.e(g));
        }
    };

    /* renamed from: b reason: collision with root package name */
    private final Class<E> f1764b;
    private final v<E> c;

    public a(f fVar, v<E> vVar, Class<E> cls) {
        this.c = new m(fVar, vVar, cls);
        this.f1764b = cls;
    }

    public void a(c cVar, Object obj) throws IOException {
        if (obj == null) {
            cVar.f();
            return;
        }
        cVar.b();
        int length = Array.getLength(obj);
        for (int i = 0; i < length; i++) {
            this.c.a(cVar, Array.get(obj, i));
        }
        cVar.c();
    }

    public Object b(com.google.a.d.a aVar) throws IOException {
        if (aVar.f() == com.google.a.d.b.NULL) {
            aVar.j();
            return null;
        }
        ArrayList arrayList = new ArrayList();
        aVar.a();
        while (aVar.e()) {
            arrayList.add(this.c.b(aVar));
        }
        aVar.b();
        Object newInstance = Array.newInstance(this.f1764b, arrayList.size());
        for (int i = 0; i < arrayList.size(); i++) {
            Array.set(newInstance, i, arrayList.get(i));
        }
        return newInstance;
    }
}
