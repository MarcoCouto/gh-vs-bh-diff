package com.google.a.b.a;

import com.google.a.c.a;
import com.google.a.d.b;
import com.google.a.d.c;
import com.google.a.f;
import com.google.a.t;
import com.google.a.v;
import com.google.a.w;
import java.io.IOException;
import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public final class j extends v<Date> {

    /* renamed from: a reason: collision with root package name */
    public static final w f1788a = new w() {
        public <T> v<T> a(f fVar, a<T> aVar) {
            if (aVar.a() == Date.class) {
                return new j();
            }
            return null;
        }
    };

    /* renamed from: b reason: collision with root package name */
    private final DateFormat f1789b = new SimpleDateFormat("MMM d, yyyy");

    /* renamed from: a */
    public synchronized Date b(com.google.a.d.a aVar) throws IOException {
        Date date;
        if (aVar.f() == b.NULL) {
            aVar.j();
            date = null;
        } else {
            try {
                date = new Date(this.f1789b.parse(aVar.h()).getTime());
            } catch (ParseException e) {
                throw new t((Throwable) e);
            }
        }
        return date;
    }

    public synchronized void a(c cVar, Date date) throws IOException {
        cVar.b(date == null ? null : this.f1789b.format(date));
    }
}
