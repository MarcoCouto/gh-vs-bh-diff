package com.google.a.b.a;

import com.google.a.d.b;
import com.google.a.d.c;
import com.google.a.f;
import com.google.a.i;
import com.google.a.l;
import com.google.a.m;
import com.google.a.o;
import com.google.a.q;
import com.google.a.t;
import com.google.a.v;
import com.google.a.w;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Calendar;
import java.util.Currency;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.StringTokenizer;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicIntegerArray;

public final class n {
    public static final v<String> A = new v<String>() {
        /* renamed from: a */
        public String b(com.google.a.d.a aVar) throws IOException {
            b f = aVar.f();
            if (f != b.NULL) {
                return f == b.BOOLEAN ? Boolean.toString(aVar.i()) : aVar.h();
            }
            aVar.j();
            return null;
        }

        public void a(c cVar, String str) throws IOException {
            cVar.b(str);
        }
    };
    public static final v<BigDecimal> B = new v<BigDecimal>() {
        /* renamed from: a */
        public BigDecimal b(com.google.a.d.a aVar) throws IOException {
            if (aVar.f() == b.NULL) {
                aVar.j();
                return null;
            }
            try {
                return new BigDecimal(aVar.h());
            } catch (NumberFormatException e) {
                throw new t((Throwable) e);
            }
        }

        public void a(c cVar, BigDecimal bigDecimal) throws IOException {
            cVar.a((Number) bigDecimal);
        }
    };
    public static final v<BigInteger> C = new v<BigInteger>() {
        /* renamed from: a */
        public BigInteger b(com.google.a.d.a aVar) throws IOException {
            if (aVar.f() == b.NULL) {
                aVar.j();
                return null;
            }
            try {
                return new BigInteger(aVar.h());
            } catch (NumberFormatException e) {
                throw new t((Throwable) e);
            }
        }

        public void a(c cVar, BigInteger bigInteger) throws IOException {
            cVar.a((Number) bigInteger);
        }
    };
    public static final w D = a(String.class, A);
    public static final v<StringBuilder> E = new v<StringBuilder>() {
        /* renamed from: a */
        public StringBuilder b(com.google.a.d.a aVar) throws IOException {
            if (aVar.f() != b.NULL) {
                return new StringBuilder(aVar.h());
            }
            aVar.j();
            return null;
        }

        public void a(c cVar, StringBuilder sb) throws IOException {
            cVar.b(sb == null ? null : sb.toString());
        }
    };
    public static final w F = a(StringBuilder.class, E);
    public static final v<StringBuffer> G = new v<StringBuffer>() {
        /* renamed from: a */
        public StringBuffer b(com.google.a.d.a aVar) throws IOException {
            if (aVar.f() != b.NULL) {
                return new StringBuffer(aVar.h());
            }
            aVar.j();
            return null;
        }

        public void a(c cVar, StringBuffer stringBuffer) throws IOException {
            cVar.b(stringBuffer == null ? null : stringBuffer.toString());
        }
    };
    public static final w H = a(StringBuffer.class, G);
    public static final v<URL> I = new v<URL>() {
        /* renamed from: a */
        public URL b(com.google.a.d.a aVar) throws IOException {
            if (aVar.f() == b.NULL) {
                aVar.j();
                return null;
            }
            String h = aVar.h();
            if (!"null".equals(h)) {
                return new URL(h);
            }
            return null;
        }

        public void a(c cVar, URL url) throws IOException {
            cVar.b(url == null ? null : url.toExternalForm());
        }
    };
    public static final w J = a(URL.class, I);
    public static final v<URI> K = new v<URI>() {
        /* renamed from: a */
        public URI b(com.google.a.d.a aVar) throws IOException {
            if (aVar.f() == b.NULL) {
                aVar.j();
                return null;
            }
            try {
                String h = aVar.h();
                if (!"null".equals(h)) {
                    return new URI(h);
                }
                return null;
            } catch (URISyntaxException e) {
                throw new m((Throwable) e);
            }
        }

        public void a(c cVar, URI uri) throws IOException {
            cVar.b(uri == null ? null : uri.toASCIIString());
        }
    };
    public static final w L = a(URI.class, K);
    public static final v<InetAddress> M = new v<InetAddress>() {
        /* renamed from: a */
        public InetAddress b(com.google.a.d.a aVar) throws IOException {
            if (aVar.f() != b.NULL) {
                return InetAddress.getByName(aVar.h());
            }
            aVar.j();
            return null;
        }

        public void a(c cVar, InetAddress inetAddress) throws IOException {
            cVar.b(inetAddress == null ? null : inetAddress.getHostAddress());
        }
    };
    public static final w N = b(InetAddress.class, M);
    public static final v<UUID> O = new v<UUID>() {
        /* renamed from: a */
        public UUID b(com.google.a.d.a aVar) throws IOException {
            if (aVar.f() != b.NULL) {
                return UUID.fromString(aVar.h());
            }
            aVar.j();
            return null;
        }

        public void a(c cVar, UUID uuid) throws IOException {
            cVar.b(uuid == null ? null : uuid.toString());
        }
    };
    public static final w P = a(UUID.class, O);
    public static final v<Currency> Q = new v<Currency>() {
        /* renamed from: a */
        public Currency b(com.google.a.d.a aVar) throws IOException {
            return Currency.getInstance(aVar.h());
        }

        public void a(c cVar, Currency currency) throws IOException {
            cVar.b(currency.getCurrencyCode());
        }
    }.a();
    public static final w R = a(Currency.class, Q);
    public static final w S = new w() {
        public <T> v<T> a(f fVar, com.google.a.c.a<T> aVar) {
            if (aVar.a() != Timestamp.class) {
                return null;
            }
            final v a2 = fVar.a(Date.class);
            return new v<Timestamp>() {
                /* renamed from: a */
                public Timestamp b(com.google.a.d.a aVar) throws IOException {
                    Date date = (Date) a2.b(aVar);
                    if (date != null) {
                        return new Timestamp(date.getTime());
                    }
                    return null;
                }

                public void a(c cVar, Timestamp timestamp) throws IOException {
                    a2.a(cVar, timestamp);
                }
            };
        }
    };
    public static final v<Calendar> T = new v<Calendar>() {
        /* renamed from: a */
        public Calendar b(com.google.a.d.a aVar) throws IOException {
            int i = 0;
            if (aVar.f() == b.NULL) {
                aVar.j();
                return null;
            }
            aVar.c();
            int i2 = 0;
            int i3 = 0;
            int i4 = 0;
            int i5 = 0;
            int i6 = 0;
            while (aVar.f() != b.END_OBJECT) {
                String g = aVar.g();
                int m = aVar.m();
                if ("year".equals(g)) {
                    i6 = m;
                } else if ("month".equals(g)) {
                    i5 = m;
                } else if ("dayOfMonth".equals(g)) {
                    i4 = m;
                } else if ("hourOfDay".equals(g)) {
                    i3 = m;
                } else if ("minute".equals(g)) {
                    i2 = m;
                } else if ("second".equals(g)) {
                    i = m;
                }
            }
            aVar.d();
            return new GregorianCalendar(i6, i5, i4, i3, i2, i);
        }

        public void a(c cVar, Calendar calendar) throws IOException {
            if (calendar == null) {
                cVar.f();
                return;
            }
            cVar.d();
            cVar.a("year");
            cVar.a((long) calendar.get(1));
            cVar.a("month");
            cVar.a((long) calendar.get(2));
            cVar.a("dayOfMonth");
            cVar.a((long) calendar.get(5));
            cVar.a("hourOfDay");
            cVar.a((long) calendar.get(11));
            cVar.a("minute");
            cVar.a((long) calendar.get(12));
            cVar.a("second");
            cVar.a((long) calendar.get(13));
            cVar.e();
        }
    };
    public static final w U = b(Calendar.class, GregorianCalendar.class, T);
    public static final v<Locale> V = new v<Locale>() {
        /* renamed from: a */
        public Locale b(com.google.a.d.a aVar) throws IOException {
            if (aVar.f() == b.NULL) {
                aVar.j();
                return null;
            }
            StringTokenizer stringTokenizer = new StringTokenizer(aVar.h(), "_");
            String str = stringTokenizer.hasMoreElements() ? stringTokenizer.nextToken() : null;
            String str2 = stringTokenizer.hasMoreElements() ? stringTokenizer.nextToken() : null;
            String str3 = stringTokenizer.hasMoreElements() ? stringTokenizer.nextToken() : null;
            return (str2 == null && str3 == null) ? new Locale(str) : str3 == null ? new Locale(str, str2) : new Locale(str, str2, str3);
        }

        public void a(c cVar, Locale locale) throws IOException {
            cVar.b(locale == null ? null : locale.toString());
        }
    };
    public static final w W = a(Locale.class, V);
    public static final v<l> X = new v<l>() {
        /* renamed from: a */
        public l b(com.google.a.d.a aVar) throws IOException {
            switch (AnonymousClass29.f1813a[aVar.f().ordinal()]) {
                case 1:
                    return new q((Number) new com.google.a.b.f(aVar.h()));
                case 2:
                    return new q(Boolean.valueOf(aVar.i()));
                case 3:
                    return new q(aVar.h());
                case 4:
                    aVar.j();
                    return com.google.a.n.f1885a;
                case 5:
                    i iVar = new i();
                    aVar.a();
                    while (aVar.e()) {
                        iVar.a(b(aVar));
                    }
                    aVar.b();
                    return iVar;
                case 6:
                    o oVar = new o();
                    aVar.c();
                    while (aVar.e()) {
                        oVar.a(aVar.g(), b(aVar));
                    }
                    aVar.d();
                    return oVar;
                default:
                    throw new IllegalArgumentException();
            }
        }

        public void a(c cVar, l lVar) throws IOException {
            if (lVar == null || lVar.j()) {
                cVar.f();
            } else if (lVar.i()) {
                q m = lVar.m();
                if (m.p()) {
                    cVar.a(m.a());
                } else if (m.o()) {
                    cVar.a(m.f());
                } else {
                    cVar.b(m.b());
                }
            } else if (lVar.g()) {
                cVar.b();
                Iterator it = lVar.l().iterator();
                while (it.hasNext()) {
                    a(cVar, (l) it.next());
                }
                cVar.c();
            } else if (lVar.h()) {
                cVar.d();
                for (Entry entry : lVar.k().o()) {
                    cVar.a((String) entry.getKey());
                    a(cVar, (l) entry.getValue());
                }
                cVar.e();
            } else {
                throw new IllegalArgumentException("Couldn't write " + lVar.getClass());
            }
        }
    };
    public static final w Y = b(l.class, X);
    public static final w Z = new w() {
        public <T> v<T> a(f fVar, com.google.a.c.a<T> aVar) {
            Class<Enum> a2 = aVar.a();
            if (!Enum.class.isAssignableFrom(a2) || a2 == Enum.class) {
                return null;
            }
            if (!a2.isEnum()) {
                a2 = a2.getSuperclass();
            }
            return new a(a2);
        }
    };

    /* renamed from: a reason: collision with root package name */
    public static final v<Class> f1799a = new v<Class>() {
        /* renamed from: a */
        public Class b(com.google.a.d.a aVar) throws IOException {
            if (aVar.f() == b.NULL) {
                aVar.j();
                return null;
            }
            throw new UnsupportedOperationException("Attempted to deserialize a java.lang.Class. Forgot to register a type adapter?");
        }

        public void a(c cVar, Class cls) throws IOException {
            if (cls == null) {
                cVar.f();
                return;
            }
            throw new UnsupportedOperationException("Attempted to serialize java.lang.Class: " + cls.getName() + ". Forgot to register a type adapter?");
        }
    };

    /* renamed from: b reason: collision with root package name */
    public static final w f1800b = a(Class.class, f1799a);
    public static final v<BitSet> c = new v<BitSet>() {
        /* renamed from: a */
        public BitSet b(com.google.a.d.a aVar) throws IOException {
            boolean z;
            if (aVar.f() == b.NULL) {
                aVar.j();
                return null;
            }
            BitSet bitSet = new BitSet();
            aVar.a();
            b f = aVar.f();
            int i = 0;
            while (f != b.END_ARRAY) {
                switch (AnonymousClass29.f1813a[f.ordinal()]) {
                    case 1:
                        if (aVar.m() == 0) {
                            z = false;
                            break;
                        } else {
                            z = true;
                            break;
                        }
                    case 2:
                        z = aVar.i();
                        break;
                    case 3:
                        String h = aVar.h();
                        try {
                            if (Integer.parseInt(h) == 0) {
                                z = false;
                                break;
                            } else {
                                z = true;
                                break;
                            }
                        } catch (NumberFormatException e) {
                            throw new t("Error: Expecting: bitset number value (1, 0), Found: " + h);
                        }
                    default:
                        throw new t("Invalid bitset value type: " + f);
                }
                if (z) {
                    bitSet.set(i);
                }
                i++;
                f = aVar.f();
            }
            aVar.b();
            return bitSet;
        }

        public void a(c cVar, BitSet bitSet) throws IOException {
            if (bitSet == null) {
                cVar.f();
                return;
            }
            cVar.b();
            for (int i = 0; i < bitSet.length(); i++) {
                cVar.a((long) (bitSet.get(i) ? 1 : 0));
            }
            cVar.c();
        }
    };
    public static final w d = a(BitSet.class, c);
    public static final v<Boolean> e = new v<Boolean>() {
        /* renamed from: a */
        public Boolean b(com.google.a.d.a aVar) throws IOException {
            if (aVar.f() != b.NULL) {
                return aVar.f() == b.STRING ? Boolean.valueOf(Boolean.parseBoolean(aVar.h())) : Boolean.valueOf(aVar.i());
            }
            aVar.j();
            return null;
        }

        public void a(c cVar, Boolean bool) throws IOException {
            cVar.a(bool);
        }
    };
    public static final v<Boolean> f = new v<Boolean>() {
        /* renamed from: a */
        public Boolean b(com.google.a.d.a aVar) throws IOException {
            if (aVar.f() != b.NULL) {
                return Boolean.valueOf(aVar.h());
            }
            aVar.j();
            return null;
        }

        public void a(c cVar, Boolean bool) throws IOException {
            cVar.b(bool == null ? "null" : bool.toString());
        }
    };
    public static final w g = a(Boolean.TYPE, Boolean.class, e);
    public static final v<Number> h = new v<Number>() {
        /* renamed from: a */
        public Number b(com.google.a.d.a aVar) throws IOException {
            if (aVar.f() == b.NULL) {
                aVar.j();
                return null;
            }
            try {
                return Byte.valueOf((byte) aVar.m());
            } catch (NumberFormatException e) {
                throw new t((Throwable) e);
            }
        }

        public void a(c cVar, Number number) throws IOException {
            cVar.a(number);
        }
    };
    public static final w i = a(Byte.TYPE, Byte.class, h);
    public static final v<Number> j = new v<Number>() {
        /* renamed from: a */
        public Number b(com.google.a.d.a aVar) throws IOException {
            if (aVar.f() == b.NULL) {
                aVar.j();
                return null;
            }
            try {
                return Short.valueOf((short) aVar.m());
            } catch (NumberFormatException e) {
                throw new t((Throwable) e);
            }
        }

        public void a(c cVar, Number number) throws IOException {
            cVar.a(number);
        }
    };
    public static final w k = a(Short.TYPE, Short.class, j);
    public static final v<Number> l = new v<Number>() {
        /* renamed from: a */
        public Number b(com.google.a.d.a aVar) throws IOException {
            if (aVar.f() == b.NULL) {
                aVar.j();
                return null;
            }
            try {
                return Integer.valueOf(aVar.m());
            } catch (NumberFormatException e) {
                throw new t((Throwable) e);
            }
        }

        public void a(c cVar, Number number) throws IOException {
            cVar.a(number);
        }
    };
    public static final w m = a(Integer.TYPE, Integer.class, l);
    public static final v<AtomicInteger> n = new v<AtomicInteger>() {
        /* renamed from: a */
        public AtomicInteger b(com.google.a.d.a aVar) throws IOException {
            try {
                return new AtomicInteger(aVar.m());
            } catch (NumberFormatException e) {
                throw new t((Throwable) e);
            }
        }

        public void a(c cVar, AtomicInteger atomicInteger) throws IOException {
            cVar.a((long) atomicInteger.get());
        }
    }.a();
    public static final w o = a(AtomicInteger.class, n);
    public static final v<AtomicBoolean> p = new v<AtomicBoolean>() {
        /* renamed from: a */
        public AtomicBoolean b(com.google.a.d.a aVar) throws IOException {
            return new AtomicBoolean(aVar.i());
        }

        public void a(c cVar, AtomicBoolean atomicBoolean) throws IOException {
            cVar.a(atomicBoolean.get());
        }
    }.a();
    public static final w q = a(AtomicBoolean.class, p);
    public static final v<AtomicIntegerArray> r = new v<AtomicIntegerArray>() {
        /* renamed from: a */
        public AtomicIntegerArray b(com.google.a.d.a aVar) throws IOException {
            ArrayList arrayList = new ArrayList();
            aVar.a();
            while (aVar.e()) {
                try {
                    arrayList.add(Integer.valueOf(aVar.m()));
                } catch (NumberFormatException e) {
                    throw new t((Throwable) e);
                }
            }
            aVar.b();
            int size = arrayList.size();
            AtomicIntegerArray atomicIntegerArray = new AtomicIntegerArray(size);
            for (int i = 0; i < size; i++) {
                atomicIntegerArray.set(i, ((Integer) arrayList.get(i)).intValue());
            }
            return atomicIntegerArray;
        }

        public void a(c cVar, AtomicIntegerArray atomicIntegerArray) throws IOException {
            cVar.b();
            int length = atomicIntegerArray.length();
            for (int i = 0; i < length; i++) {
                cVar.a((long) atomicIntegerArray.get(i));
            }
            cVar.c();
        }
    }.a();
    public static final w s = a(AtomicIntegerArray.class, r);
    public static final v<Number> t = new v<Number>() {
        /* renamed from: a */
        public Number b(com.google.a.d.a aVar) throws IOException {
            if (aVar.f() == b.NULL) {
                aVar.j();
                return null;
            }
            try {
                return Long.valueOf(aVar.l());
            } catch (NumberFormatException e) {
                throw new t((Throwable) e);
            }
        }

        public void a(c cVar, Number number) throws IOException {
            cVar.a(number);
        }
    };
    public static final v<Number> u = new v<Number>() {
        /* renamed from: a */
        public Number b(com.google.a.d.a aVar) throws IOException {
            if (aVar.f() != b.NULL) {
                return Float.valueOf((float) aVar.k());
            }
            aVar.j();
            return null;
        }

        public void a(c cVar, Number number) throws IOException {
            cVar.a(number);
        }
    };
    public static final v<Number> v = new v<Number>() {
        /* renamed from: a */
        public Number b(com.google.a.d.a aVar) throws IOException {
            if (aVar.f() != b.NULL) {
                return Double.valueOf(aVar.k());
            }
            aVar.j();
            return null;
        }

        public void a(c cVar, Number number) throws IOException {
            cVar.a(number);
        }
    };
    public static final v<Number> w = new v<Number>() {
        /* renamed from: a */
        public Number b(com.google.a.d.a aVar) throws IOException {
            b f = aVar.f();
            switch (f) {
                case NUMBER:
                    return new com.google.a.b.f(aVar.h());
                case NULL:
                    aVar.j();
                    return null;
                default:
                    throw new t("Expecting number, got: " + f);
            }
        }

        public void a(c cVar, Number number) throws IOException {
            cVar.a(number);
        }
    };
    public static final w x = a(Number.class, w);
    public static final v<Character> y = new v<Character>() {
        /* renamed from: a */
        public Character b(com.google.a.d.a aVar) throws IOException {
            if (aVar.f() == b.NULL) {
                aVar.j();
                return null;
            }
            String h = aVar.h();
            if (h.length() == 1) {
                return Character.valueOf(h.charAt(0));
            }
            throw new t("Expecting character, got: " + h);
        }

        public void a(c cVar, Character ch) throws IOException {
            cVar.b(ch == null ? null : String.valueOf(ch));
        }
    };
    public static final w z = a(Character.TYPE, Character.class, y);

    private static final class a<T extends Enum<T>> extends v<T> {

        /* renamed from: a reason: collision with root package name */
        private final Map<String, T> f1814a = new HashMap();

        /* renamed from: b reason: collision with root package name */
        private final Map<T, String> f1815b = new HashMap();

        public a(Class<T> cls) {
            Enum[] enumArr;
            try {
                for (Enum enumR : (Enum[]) cls.getEnumConstants()) {
                    String name = enumR.name();
                    com.google.a.a.c cVar = (com.google.a.a.c) cls.getField(name).getAnnotation(com.google.a.a.c.class);
                    if (cVar != null) {
                        name = cVar.a();
                        for (String put : cVar.b()) {
                            this.f1814a.put(put, enumR);
                        }
                    }
                    String str = name;
                    this.f1814a.put(str, enumR);
                    this.f1815b.put(enumR, str);
                }
            } catch (NoSuchFieldException e) {
                throw new AssertionError(e);
            }
        }

        /* renamed from: a */
        public T b(com.google.a.d.a aVar) throws IOException {
            if (aVar.f() != b.NULL) {
                return (Enum) this.f1814a.get(aVar.h());
            }
            aVar.j();
            return null;
        }

        public void a(c cVar, T t) throws IOException {
            cVar.b(t == null ? null : (String) this.f1815b.get(t));
        }
    }

    public static <TT> w a(final Class<TT> cls, final v<TT> vVar) {
        return new w() {
            public <T> v<T> a(f fVar, com.google.a.c.a<T> aVar) {
                if (aVar.a() == cls) {
                    return vVar;
                }
                return null;
            }

            public String toString() {
                return "Factory[type=" + cls.getName() + ",adapter=" + vVar + "]";
            }
        };
    }

    public static <TT> w a(final Class<TT> cls, final Class<TT> cls2, final v<? super TT> vVar) {
        return new w() {
            public <T> v<T> a(f fVar, com.google.a.c.a<T> aVar) {
                Class a2 = aVar.a();
                if (a2 == cls || a2 == cls2) {
                    return vVar;
                }
                return null;
            }

            public String toString() {
                return "Factory[type=" + cls2.getName() + "+" + cls.getName() + ",adapter=" + vVar + "]";
            }
        };
    }

    public static <T1> w b(final Class<T1> cls, final v<T1> vVar) {
        return new w() {
            public <T2> v<T2> a(f fVar, com.google.a.c.a<T2> aVar) {
                final Class a2 = aVar.a();
                if (!cls.isAssignableFrom(a2)) {
                    return null;
                }
                return new v<T1>() {
                    public void a(c cVar, T1 t1) throws IOException {
                        vVar.a(cVar, t1);
                    }

                    public T1 b(com.google.a.d.a aVar) throws IOException {
                        T1 b2 = vVar.b(aVar);
                        if (b2 == null || a2.isInstance(b2)) {
                            return b2;
                        }
                        throw new t("Expected a " + a2.getName() + " but was " + b2.getClass().getName());
                    }
                };
            }

            public String toString() {
                return "Factory[typeHierarchy=" + cls.getName() + ",adapter=" + vVar + "]";
            }
        };
    }

    public static <TT> w b(final Class<TT> cls, final Class<? extends TT> cls2, final v<? super TT> vVar) {
        return new w() {
            public <T> v<T> a(f fVar, com.google.a.c.a<T> aVar) {
                Class a2 = aVar.a();
                if (a2 == cls || a2 == cls2) {
                    return vVar;
                }
                return null;
            }

            public String toString() {
                return "Factory[type=" + cls.getName() + "+" + cls2.getName() + ",adapter=" + vVar + "]";
            }
        };
    }
}
