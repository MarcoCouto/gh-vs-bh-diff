package com.google.a.b.a;

import com.google.a.b.c;
import com.google.a.b.e;
import com.google.a.b.h;
import com.google.a.b.j;
import com.google.a.d.b;
import com.google.a.f;
import com.google.a.l;
import com.google.a.q;
import com.google.a.t;
import com.google.a.v;
import com.google.a.w;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Map;
import java.util.Map.Entry;

public final class g implements w {

    /* renamed from: a reason: collision with root package name */
    final boolean f1775a;

    /* renamed from: b reason: collision with root package name */
    private final c f1776b;

    private final class a<K, V> extends v<Map<K, V>> {

        /* renamed from: b reason: collision with root package name */
        private final v<K> f1778b;
        private final v<V> c;
        private final h<? extends Map<K, V>> d;

        public a(f fVar, Type type, v<K> vVar, Type type2, v<V> vVar2, h<? extends Map<K, V>> hVar) {
            this.f1778b = new m(fVar, vVar, type);
            this.c = new m(fVar, vVar2, type2);
            this.d = hVar;
        }

        private String a(l lVar) {
            if (lVar.i()) {
                q m = lVar.m();
                if (m.p()) {
                    return String.valueOf(m.a());
                }
                if (m.o()) {
                    return Boolean.toString(m.f());
                }
                if (m.q()) {
                    return m.b();
                }
                throw new AssertionError();
            } else if (lVar.j()) {
                return "null";
            } else {
                throw new AssertionError();
            }
        }

        /* renamed from: a */
        public Map<K, V> b(com.google.a.d.a aVar) throws IOException {
            b f = aVar.f();
            if (f == b.NULL) {
                aVar.j();
                return null;
            }
            Map<K, V> map = (Map) this.d.a();
            if (f == b.BEGIN_ARRAY) {
                aVar.a();
                while (aVar.e()) {
                    aVar.a();
                    Object b2 = this.f1778b.b(aVar);
                    if (map.put(b2, this.c.b(aVar)) != null) {
                        throw new t("duplicate key: " + b2);
                    }
                    aVar.b();
                }
                aVar.b();
                return map;
            }
            aVar.c();
            while (aVar.e()) {
                e.f1846a.a(aVar);
                Object b3 = this.f1778b.b(aVar);
                if (map.put(b3, this.c.b(aVar)) != null) {
                    throw new t("duplicate key: " + b3);
                }
            }
            aVar.d();
            return map;
        }

        public void a(com.google.a.d.c cVar, Map<K, V> map) throws IOException {
            int i = 0;
            if (map == null) {
                cVar.f();
            } else if (!g.this.f1775a) {
                cVar.d();
                for (Entry entry : map.entrySet()) {
                    cVar.a(String.valueOf(entry.getKey()));
                    this.c.a(cVar, entry.getValue());
                }
                cVar.e();
            } else {
                ArrayList arrayList = new ArrayList(map.size());
                ArrayList arrayList2 = new ArrayList(map.size());
                boolean z = false;
                for (Entry entry2 : map.entrySet()) {
                    l a2 = this.f1778b.a(entry2.getKey());
                    arrayList.add(a2);
                    arrayList2.add(entry2.getValue());
                    z = (a2.g() || a2.h()) | z;
                }
                if (z) {
                    cVar.b();
                    while (i < arrayList.size()) {
                        cVar.b();
                        j.a((l) arrayList.get(i), cVar);
                        this.c.a(cVar, arrayList2.get(i));
                        cVar.c();
                        i++;
                    }
                    cVar.c();
                    return;
                }
                cVar.d();
                while (i < arrayList.size()) {
                    cVar.a(a((l) arrayList.get(i)));
                    this.c.a(cVar, arrayList2.get(i));
                    i++;
                }
                cVar.e();
            }
        }
    }

    public g(c cVar, boolean z) {
        this.f1776b = cVar;
        this.f1775a = z;
    }

    private v<?> a(f fVar, Type type) {
        return (type == Boolean.TYPE || type == Boolean.class) ? n.f : fVar.a(com.google.a.c.a.a(type));
    }

    public <T> v<T> a(f fVar, com.google.a.c.a<T> aVar) {
        Type b2 = aVar.b();
        if (!Map.class.isAssignableFrom(aVar.a())) {
            return null;
        }
        Type[] b3 = com.google.a.b.b.b(b2, com.google.a.b.b.e(b2));
        v a2 = a(fVar, b3[0]);
        v a3 = fVar.a(com.google.a.c.a.a(b3[1]));
        h a4 = this.f1776b.a(aVar);
        return new a(fVar, b3[0], a2, b3[1], a3, a4);
    }
}
