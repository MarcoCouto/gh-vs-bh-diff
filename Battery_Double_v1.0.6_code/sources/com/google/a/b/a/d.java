package com.google.a.b.a;

import com.google.a.a.b;
import com.google.a.b.c;
import com.google.a.c.a;
import com.google.a.f;
import com.google.a.k;
import com.google.a.s;
import com.google.a.v;
import com.google.a.w;

public final class d implements w {

    /* renamed from: a reason: collision with root package name */
    private final c f1771a;

    public d(c cVar) {
        this.f1771a = cVar;
    }

    /* access modifiers changed from: 0000 */
    public v<?> a(c cVar, f fVar, a<?> aVar, b bVar) {
        v<?> lVar;
        Object a2 = cVar.a(a.b(bVar.a())).a();
        if (a2 instanceof v) {
            lVar = (v) a2;
        } else if (a2 instanceof w) {
            lVar = ((w) a2).a(fVar, aVar);
        } else if ((a2 instanceof s) || (a2 instanceof k)) {
            lVar = new l<>(a2 instanceof s ? (s) a2 : null, a2 instanceof k ? (k) a2 : null, fVar, aVar, null);
        } else {
            throw new IllegalArgumentException("@JsonAdapter value must be TypeAdapter, TypeAdapterFactory, JsonSerializer or JsonDeserializer reference.");
        }
        return (lVar == null || !bVar.b()) ? lVar : lVar.a();
    }

    public <T> v<T> a(f fVar, a<T> aVar) {
        b bVar = (b) aVar.a().getAnnotation(b.class);
        if (bVar == null) {
            return null;
        }
        return a(this.f1771a, fVar, aVar, bVar);
    }
}
