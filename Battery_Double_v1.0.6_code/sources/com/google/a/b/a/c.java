package com.google.a.b.a;

import com.google.a.c.a;
import com.google.a.d.b;
import com.google.a.f;
import com.google.a.t;
import com.google.a.v;
import com.google.a.w;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.util.Date;
import java.util.Locale;

public final class c extends v<Date> {

    /* renamed from: a reason: collision with root package name */
    public static final w f1769a = new w() {
        public <T> v<T> a(f fVar, a<T> aVar) {
            if (aVar.a() == Date.class) {
                return new c();
            }
            return null;
        }
    };

    /* renamed from: b reason: collision with root package name */
    private final DateFormat f1770b = DateFormat.getDateTimeInstance(2, 2, Locale.US);
    private final DateFormat c = DateFormat.getDateTimeInstance(2, 2);

    private synchronized Date a(String str) {
        Date a2;
        try {
            a2 = this.c.parse(str);
        } catch (ParseException e) {
            try {
                a2 = this.f1770b.parse(str);
            } catch (ParseException e2) {
                try {
                    a2 = com.google.a.b.a.a.a.a(str, new ParsePosition(0));
                } catch (ParseException e3) {
                    throw new t(str, e3);
                }
            }
        }
        return a2;
    }

    /* renamed from: a */
    public Date b(com.google.a.d.a aVar) throws IOException {
        if (aVar.f() != b.NULL) {
            return a(aVar.h());
        }
        aVar.j();
        return null;
    }

    public synchronized void a(com.google.a.d.c cVar, Date date) throws IOException {
        if (date == null) {
            cVar.f();
        } else {
            cVar.b(this.f1770b.format(date));
        }
    }
}
