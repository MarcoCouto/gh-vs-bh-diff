package com.google.a.b.a;

import com.google.a.c.a;
import com.google.a.d.c;
import com.google.a.f;
import com.google.a.v;
import java.io.IOException;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;

final class m<T> extends v<T> {

    /* renamed from: a reason: collision with root package name */
    private final f f1797a;

    /* renamed from: b reason: collision with root package name */
    private final v<T> f1798b;
    private final Type c;

    m(f fVar, v<T> vVar, Type type) {
        this.f1797a = fVar;
        this.f1798b = vVar;
        this.c = type;
    }

    private Type a(Type type, Object obj) {
        return obj != null ? (type == Object.class || (type instanceof TypeVariable) || (type instanceof Class)) ? obj.getClass() : type : type;
    }

    public void a(c cVar, T t) throws IOException {
        v<T> vVar = this.f1798b;
        Type a2 = a(this.c, (Object) t);
        if (a2 != this.c) {
            vVar = this.f1797a.a(a.a(a2));
            if ((vVar instanceof i.a) && !(this.f1798b instanceof i.a)) {
                vVar = this.f1798b;
            }
        }
        vVar.a(cVar, t);
    }

    public T b(com.google.a.d.a aVar) throws IOException {
        return this.f1798b.b(aVar);
    }
}
