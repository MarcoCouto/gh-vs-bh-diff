package com.google.a.b.a;

import com.google.a.d.c;
import com.google.a.f;
import com.google.a.j;
import com.google.a.k;
import com.google.a.r;
import com.google.a.s;
import com.google.a.v;
import com.google.a.w;
import java.io.IOException;

public final class l<T> extends v<T> {

    /* renamed from: a reason: collision with root package name */
    private final s<T> f1792a;

    /* renamed from: b reason: collision with root package name */
    private final k<T> f1793b;
    private final f c;
    private final com.google.a.c.a<T> d;
    private final w e;
    private final a f = new a<>();
    private v<T> g;

    private final class a implements j, r {
        private a() {
        }
    }

    private static final class b implements w {

        /* renamed from: a reason: collision with root package name */
        private final com.google.a.c.a<?> f1795a;

        /* renamed from: b reason: collision with root package name */
        private final boolean f1796b;
        private final Class<?> c;
        private final s<?> d;
        private final k<?> e;

        b(Object obj, com.google.a.c.a<?> aVar, boolean z, Class<?> cls) {
            this.d = obj instanceof s ? (s) obj : null;
            this.e = obj instanceof k ? (k) obj : null;
            com.google.a.b.a.a((this.d == null && this.e == null) ? false : true);
            this.f1795a = aVar;
            this.f1796b = z;
            this.c = cls;
        }

        public <T> v<T> a(f fVar, com.google.a.c.a<T> aVar) {
            boolean isAssignableFrom = this.f1795a != null ? this.f1795a.equals(aVar) || (this.f1796b && this.f1795a.b() == aVar.a()) : this.c.isAssignableFrom(aVar.a());
            if (isAssignableFrom) {
                return new l(this.d, this.e, fVar, aVar, this);
            }
            return null;
        }
    }

    public l(s<T> sVar, k<T> kVar, f fVar, com.google.a.c.a<T> aVar, w wVar) {
        this.f1792a = sVar;
        this.f1793b = kVar;
        this.c = fVar;
        this.d = aVar;
        this.e = wVar;
    }

    public static w a(com.google.a.c.a<?> aVar, Object obj) {
        return new b(obj, aVar, false, null);
    }

    private v<T> b() {
        v<T> vVar = this.g;
        if (vVar != null) {
            return vVar;
        }
        v<T> a2 = this.c.a(this.e, this.d);
        this.g = a2;
        return a2;
    }

    public void a(c cVar, T t) throws IOException {
        if (this.f1792a == null) {
            b().a(cVar, t);
        } else if (t == null) {
            cVar.f();
        } else {
            com.google.a.b.j.a(this.f1792a.a(t, this.d.b(), this.f), cVar);
        }
    }

    public T b(com.google.a.d.a aVar) throws IOException {
        if (this.f1793b == null) {
            return b().b(aVar);
        }
        com.google.a.l a2 = com.google.a.b.j.a(aVar);
        if (a2.j()) {
            return null;
        }
        return this.f1793b.b(a2, this.d.b(), this.f);
    }
}
