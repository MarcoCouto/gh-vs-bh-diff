package com.google.a.b;

import com.google.a.a.e;
import com.google.a.b;
import com.google.a.c.a;
import com.google.a.d.c;
import com.google.a.f;
import com.google.a.v;
import com.google.a.w;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class d implements w, Cloneable {

    /* renamed from: a reason: collision with root package name */
    public static final d f1842a = new d();

    /* renamed from: b reason: collision with root package name */
    private double f1843b = -1.0d;
    private int c = 136;
    private boolean d = true;
    private boolean e;
    private List<b> f = Collections.emptyList();
    private List<b> g = Collections.emptyList();

    private boolean a(com.google.a.a.d dVar) {
        return dVar == null || dVar.a() <= this.f1843b;
    }

    private boolean a(com.google.a.a.d dVar, e eVar) {
        return a(dVar) && a(eVar);
    }

    private boolean a(e eVar) {
        return eVar == null || eVar.a() > this.f1843b;
    }

    private boolean a(Class<?> cls) {
        return !Enum.class.isAssignableFrom(cls) && (cls.isAnonymousClass() || cls.isLocalClass());
    }

    private boolean b(Class<?> cls) {
        return cls.isMemberClass() && !c(cls);
    }

    private boolean c(Class<?> cls) {
        return (cls.getModifiers() & 8) != 0;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public d clone() {
        try {
            return (d) super.clone();
        } catch (CloneNotSupportedException e2) {
            throw new AssertionError(e2);
        }
    }

    public d a(b bVar, boolean z, boolean z2) {
        d a2 = clone();
        if (z) {
            a2.f = new ArrayList(this.f);
            a2.f.add(bVar);
        }
        if (z2) {
            a2.g = new ArrayList(this.g);
            a2.g.add(bVar);
        }
        return a2;
    }

    public <T> v<T> a(f fVar, a<T> aVar) {
        Class a2 = aVar.a();
        final boolean a3 = a(a2, true);
        final boolean a4 = a(a2, false);
        if (!a3 && !a4) {
            return null;
        }
        final f fVar2 = fVar;
        final a<T> aVar2 = aVar;
        return new v<T>() {
            private v<T> f;

            private v<T> b() {
                v<T> vVar = this.f;
                if (vVar != null) {
                    return vVar;
                }
                v<T> a2 = fVar2.a(d.this, aVar2);
                this.f = a2;
                return a2;
            }

            public void a(c cVar, T t) throws IOException {
                if (a3) {
                    cVar.f();
                } else {
                    b().a(cVar, t);
                }
            }

            public T b(com.google.a.d.a aVar) throws IOException {
                if (!a4) {
                    return b().b(aVar);
                }
                aVar.n();
                return null;
            }
        };
    }

    public boolean a(Class<?> cls, boolean z) {
        if (this.f1843b != -1.0d && !a((com.google.a.a.d) cls.getAnnotation(com.google.a.a.d.class), (e) cls.getAnnotation(e.class))) {
            return true;
        }
        if (!this.d && b(cls)) {
            return true;
        }
        if (a(cls)) {
            return true;
        }
        for (b a2 : z ? this.f : this.g) {
            if (a2.a(cls)) {
                return true;
            }
        }
        return false;
    }

    public boolean a(Field field, boolean z) {
        if ((this.c & field.getModifiers()) != 0) {
            return true;
        }
        if (this.f1843b != -1.0d && !a((com.google.a.a.d) field.getAnnotation(com.google.a.a.d.class), (e) field.getAnnotation(e.class))) {
            return true;
        }
        if (field.isSynthetic()) {
            return true;
        }
        if (this.e) {
            com.google.a.a.a aVar = (com.google.a.a.a) field.getAnnotation(com.google.a.a.a.class);
            if (aVar == null || (!z ? !aVar.b() : !aVar.a())) {
                return true;
            }
        }
        if (!this.d && b(field.getType())) {
            return true;
        }
        if (a(field.getType())) {
            return true;
        }
        List<b> list = z ? this.f : this.g;
        if (!list.isEmpty()) {
            com.google.a.c cVar = new com.google.a.c(field);
            for (b a2 : list) {
                if (a2.a(cVar)) {
                    return true;
                }
            }
        }
        return false;
    }
}
