package com.google.a.b;

import com.google.a.c.a;
import com.google.a.h;
import com.google.a.m;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ConcurrentNavigableMap;
import java.util.concurrent.ConcurrentSkipListMap;

public final class c {

    /* renamed from: a reason: collision with root package name */
    private final Map<Type, h<?>> f1822a;

    public c(Map<Type, h<?>> map) {
        this.f1822a = map;
    }

    private <T> h<T> a(Class<? super T> cls) {
        try {
            final Constructor declaredConstructor = cls.getDeclaredConstructor(new Class[0]);
            if (!declaredConstructor.isAccessible()) {
                declaredConstructor.setAccessible(true);
            }
            return new h<T>() {
                public T a() {
                    try {
                        return declaredConstructor.newInstance(null);
                    } catch (InstantiationException e) {
                        throw new RuntimeException("Failed to invoke " + declaredConstructor + " with no args", e);
                    } catch (InvocationTargetException e2) {
                        throw new RuntimeException("Failed to invoke " + declaredConstructor + " with no args", e2.getTargetException());
                    } catch (IllegalAccessException e3) {
                        throw new AssertionError(e3);
                    }
                }
            };
        } catch (NoSuchMethodException e) {
            return null;
        }
    }

    private <T> h<T> a(final Type type, Class<? super T> cls) {
        if (Collection.class.isAssignableFrom(cls)) {
            return SortedSet.class.isAssignableFrom(cls) ? new h<T>() {
                public T a() {
                    return new TreeSet();
                }
            } : EnumSet.class.isAssignableFrom(cls) ? new h<T>() {
                public T a() {
                    if (type instanceof ParameterizedType) {
                        Type type = ((ParameterizedType) type).getActualTypeArguments()[0];
                        if (type instanceof Class) {
                            return EnumSet.noneOf((Class) type);
                        }
                        throw new m("Invalid EnumSet type: " + type.toString());
                    }
                    throw new m("Invalid EnumSet type: " + type.toString());
                }
            } : Set.class.isAssignableFrom(cls) ? new h<T>() {
                public T a() {
                    return new LinkedHashSet();
                }
            } : Queue.class.isAssignableFrom(cls) ? new h<T>() {
                public T a() {
                    return new ArrayDeque();
                }
            } : new h<T>() {
                public T a() {
                    return new ArrayList();
                }
            };
        }
        if (Map.class.isAssignableFrom(cls)) {
            return ConcurrentNavigableMap.class.isAssignableFrom(cls) ? new h<T>() {
                public T a() {
                    return new ConcurrentSkipListMap();
                }
            } : ConcurrentMap.class.isAssignableFrom(cls) ? new h<T>() {
                public T a() {
                    return new ConcurrentHashMap();
                }
            } : SortedMap.class.isAssignableFrom(cls) ? new h<T>() {
                public T a() {
                    return new TreeMap();
                }
            } : (!(type instanceof ParameterizedType) || String.class.isAssignableFrom(a.a(((ParameterizedType) type).getActualTypeArguments()[0]).a())) ? new h<T>() {
                public T a() {
                    return new g();
                }
            } : new h<T>() {
                public T a() {
                    return new LinkedHashMap();
                }
            };
        }
        return null;
    }

    private <T> h<T> b(final Type type, final Class<? super T> cls) {
        return new h<T>() {
            private final k d = k.a();

            public T a() {
                try {
                    return this.d.a(cls);
                } catch (Exception e) {
                    throw new RuntimeException("Unable to invoke no-args constructor for " + type + ". Register an InstanceCreator with Gson for this type may fix this problem.", e);
                }
            }
        };
    }

    public <T> h<T> a(a<T> aVar) {
        final Type b2 = aVar.b();
        Class a2 = aVar.a();
        final h hVar = (h) this.f1822a.get(b2);
        if (hVar != null) {
            return new h<T>() {
                public T a() {
                    return hVar.a(b2);
                }
            };
        }
        final h hVar2 = (h) this.f1822a.get(a2);
        if (hVar2 != null) {
            return new h<T>() {
                public T a() {
                    return hVar2.a(b2);
                }
            };
        }
        h<T> a3 = a(a2);
        if (a3 != null) {
            return a3;
        }
        h<T> a4 = a(b2, a2);
        return a4 == null ? b(b2, a2) : a4;
    }

    public String toString() {
        return this.f1822a.toString();
    }
}
