package com.google.a.b;

import com.google.a.b.a.n;
import com.google.a.d.a;
import com.google.a.d.c;
import com.google.a.d.d;
import com.google.a.l;
import com.google.a.m;
import com.google.a.p;
import com.google.a.t;
import java.io.EOFException;
import java.io.IOException;

public final class j {
    public static l a(a aVar) throws p {
        boolean z = true;
        try {
            aVar.f();
            z = false;
            return (l) n.X.b(aVar);
        } catch (EOFException e) {
            if (z) {
                return com.google.a.n.f1885a;
            }
            throw new t((Throwable) e);
        } catch (d e2) {
            throw new t((Throwable) e2);
        } catch (IOException e3) {
            throw new m((Throwable) e3);
        } catch (NumberFormatException e4) {
            throw new t((Throwable) e4);
        }
    }

    public static void a(l lVar, c cVar) throws IOException {
        n.X.a(cVar, lVar);
    }
}
