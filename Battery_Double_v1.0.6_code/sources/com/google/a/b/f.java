package com.google.a.b;

import java.math.BigDecimal;

public final class f extends Number {

    /* renamed from: a reason: collision with root package name */
    private final String f1847a;

    public f(String str) {
        this.f1847a = str;
    }

    public double doubleValue() {
        return Double.parseDouble(this.f1847a);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof f)) {
            return false;
        }
        f fVar = (f) obj;
        return this.f1847a == fVar.f1847a || this.f1847a.equals(fVar.f1847a);
    }

    public float floatValue() {
        return Float.parseFloat(this.f1847a);
    }

    public int hashCode() {
        return this.f1847a.hashCode();
    }

    public int intValue() {
        try {
            return Integer.parseInt(this.f1847a);
        } catch (NumberFormatException e) {
            try {
                return (int) Long.parseLong(this.f1847a);
            } catch (NumberFormatException e2) {
                return new BigDecimal(this.f1847a).intValue();
            }
        }
    }

    public long longValue() {
        try {
            return Long.parseLong(this.f1847a);
        } catch (NumberFormatException e) {
            return new BigDecimal(this.f1847a).longValue();
        }
    }

    public String toString() {
        return this.f1847a;
    }
}
