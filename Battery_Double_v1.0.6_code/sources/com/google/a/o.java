package com.google.a;

import com.google.a.b.g;
import java.util.Map.Entry;
import java.util.Set;

public final class o extends l {

    /* renamed from: a reason: collision with root package name */
    private final g<String, l> f1886a = new g<>();

    private l a(Object obj) {
        return obj == null ? n.f1885a : new q(obj);
    }

    public l a(String str) {
        return (l) this.f1886a.get(str);
    }

    public void a(String str, l lVar) {
        if (lVar == null) {
            lVar = n.f1885a;
        }
        this.f1886a.put(str, lVar);
    }

    public void a(String str, Boolean bool) {
        a(str, a((Object) bool));
    }

    public void a(String str, Number number) {
        a(str, a((Object) number));
    }

    public void a(String str, String str2) {
        a(str, a((Object) str2));
    }

    public boolean equals(Object obj) {
        return obj == this || ((obj instanceof o) && ((o) obj).f1886a.equals(this.f1886a));
    }

    public int hashCode() {
        return this.f1886a.hashCode();
    }

    public Set<Entry<String, l>> o() {
        return this.f1886a.entrySet();
    }
}
