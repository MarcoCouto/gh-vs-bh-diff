package com.hmatalonga.greenhub;

import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.SystemClock;
import com.crashlytics.android.a;
import com.hmatalonga.greenhub.b.d;
import com.hmatalonga.greenhub.c.c;
import com.hmatalonga.greenhub.c.g;
import com.hmatalonga.greenhub.managers.a.b;
import com.hmatalonga.greenhub.managers.sampling.DataEstimator;
import com.hmatalonga.greenhub.receivers.NotificationReceiver;
import io.realm.bb;
import io.realm.be;
import io.realm.bg;

public class GreenHubApp extends Application {
    private static final String TAG = c.a(GreenHubApp.class);
    public static boolean isServiceRunning = false;
    public DataEstimator estimator;
    private AlarmManager mAlarmManager;
    private PendingIntent mNotificationIntent;

    public void onCreate() {
        super.onCreate();
        c.b(TAG, "onCreate() called");
        io.a.a.a.c.a((Context) this, new a());
        bb.a((Context) this);
        bb.b(new be.a().a(3).a((bg) new b()).a());
        c.b(TAG, "Estimator new instance");
        this.estimator = new DataEstimator();
        Context applicationContext = getApplicationContext();
        if (g.a(applicationContext)) {
            c.b(TAG, "startGreenHubService() called");
            startGreenHubService();
            int g = g.g(applicationContext);
            new d().execute(new Integer[]{Integer.valueOf(g)});
            new com.hmatalonga.greenhub.b.c().execute(new Integer[]{Integer.valueOf(g)});
            if (g.l(applicationContext)) {
                startStatusBarUpdater();
            }
        }
    }

    public void startGreenHubService() {
        if (!isServiceRunning) {
            c.b(TAG, "GreenHubService starting...");
            final Context applicationContext = getApplicationContext();
            isServiceRunning = true;
            new Thread() {
                private IntentFilter c;

                public void run() {
                    this.c = new IntentFilter();
                    this.c.addAction("android.intent.action.BATTERY_CHANGED");
                    GreenHubApp.this.registerReceiver(GreenHubApp.this.estimator, this.c);
                    if (g.f(applicationContext)) {
                        this.c.addAction("android.intent.action.SCREEN_ON");
                        GreenHubApp.this.registerReceiver(GreenHubApp.this.estimator, this.c);
                        this.c.addAction("android.intent.action.SCREEN_OFF");
                        GreenHubApp.this.registerReceiver(GreenHubApp.this.estimator, this.c);
                    }
                }
            }.start();
            return;
        }
        c.b(TAG, "GreenHubService is already running...");
    }

    public void startStatusBarUpdater() {
        this.mNotificationIntent = PendingIntent.getBroadcast(this, 0, new Intent(this, NotificationReceiver.class), 134217728);
        if (this.mAlarmManager == null) {
            this.mAlarmManager = (AlarmManager) getSystemService("alarm");
        }
        this.mAlarmManager.setInexactRepeating(3, SystemClock.elapsedRealtime() + 30000, 30000, this.mNotificationIntent);
    }

    public void stopGreenHubService() {
        try {
            if (this.estimator != null) {
                unregisterReceiver(this.estimator);
                isServiceRunning = false;
            }
        } catch (IllegalArgumentException e) {
            c.c(TAG, "Estimator receiver is not registered!");
            e.printStackTrace();
        }
    }

    public void stopStatusBarUpdater() {
        if (this.mAlarmManager != null) {
            this.mAlarmManager.cancel(this.mNotificationIntent);
        }
    }
}
