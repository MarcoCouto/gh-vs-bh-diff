package com.hmatalonga.greenhub.network.services;

import com.google.a.o;
import com.hmatalonga.greenhub.models.data.Device;
import com.hmatalonga.greenhub.models.data.Upload;
import java.util.List;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface GreenHubAPIService {
    @POST("api/mobile/register")
    Call<Integer> createDevice(@Body Device device);

    @POST("api/mobile/upload")
    Call<Integer> createSample(@Body Upload upload);

    @GET("api/mobile/messages")
    Call<List<o>> getMessages(@Query("uuid") String str, @Query("message") int i);
}
