package com.hmatalonga.greenhub.network.services;

import com.hmatalonga.greenhub.models.ServerStatus;
import retrofit2.Call;
import retrofit2.http.GET;

public interface GreenHubStatusService {
    @GET("status.json")
    Call<ServerStatus> getStatus();
}
