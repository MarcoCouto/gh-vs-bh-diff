package com.hmatalonga.greenhub.network.a;

import android.content.Context;
import com.google.a.f;
import com.hmatalonga.greenhub.c.b;
import com.hmatalonga.greenhub.c.c;
import com.hmatalonga.greenhub.c.g;
import com.hmatalonga.greenhub.events.StatusEvent;
import com.hmatalonga.greenhub.models.Specifications;
import com.hmatalonga.greenhub.models.data.Device;
import com.hmatalonga.greenhub.network.services.GreenHubAPIService;
import com.mansoon.BatteryDouble.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit.Builder;
import retrofit2.converter.gson.GsonConverterFactory;

public class a {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public static final String f4294a = c.a(a.class);

    /* renamed from: b reason: collision with root package name */
    private GreenHubAPIService f4295b;
    /* access modifiers changed from: private */
    public Context c;

    public a(Context context) {
        this.c = context;
        f a2 = b.a();
        this.f4295b = (GreenHubAPIService) new Builder().baseUrl(g.c(context)).addConverterFactory(GsonConverterFactory.create(a2)).build().create(GreenHubAPIService.class);
    }

    private void a(Device device) {
        c.b(f4294a, "callRegistration()");
        this.f4295b.createDevice(device).enqueue(new Callback<Integer>() {
            public void onFailure(Call<Integer> call, Throwable th) {
                g.b(a.this.c, false);
                org.greenrobot.eventbus.c.a().c(new StatusEvent(a.this.c.getString(R.string.event_registration_failed)));
                c.b(a.f4294a, th.getMessage());
            }

            public void onResponse(Call<Integer> call, Response<Integer> response) {
                if (response == null || response.body() == null) {
                    if (response == null) {
                        c.b(a.f4294a, "response is null");
                    } else {
                        c.b(a.f4294a, "response body is null");
                    }
                    g.b(a.this.c, false);
                    org.greenrobot.eventbus.c.a().c(new StatusEvent(a.this.c.getString(R.string.event_registration_failed)));
                    return;
                }
                if (((Integer) response.body()).intValue() > 0) {
                    org.greenrobot.eventbus.c.a().c(new StatusEvent(a.this.c.getString(R.string.event_device_registered)));
                } else if (((Integer) response.body()).intValue() == 0) {
                    org.greenrobot.eventbus.c.a().c(new StatusEvent(a.this.c.getString(R.string.event_already_registered)));
                }
                g.b(a.this.c, true);
                new com.hmatalonga.greenhub.b.a().execute(new Context[]{a.this.c});
            }
        });
    }

    public Device a() {
        Device device = new Device();
        device.realmSet$uuId(Specifications.getAndroidId(this.c));
        device.realmSet$model(Specifications.getModel());
        device.realmSet$manufacturer(Specifications.getManufacturer());
        device.realmSet$brand(Specifications.getBrand());
        device.realmSet$product(Specifications.getProductName());
        device.realmSet$osVersion(Specifications.getOsVersion());
        device.realmSet$kernelVersion(Specifications.getKernelVersion());
        device.realmSet$isRoot(Specifications.isRooted() ? 1 : 0);
        a(device);
        return device;
    }
}
