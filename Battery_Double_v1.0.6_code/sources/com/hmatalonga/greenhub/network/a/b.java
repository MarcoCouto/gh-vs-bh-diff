package com.hmatalonga.greenhub.network.a;

import android.content.Context;
import com.hmatalonga.greenhub.Config;
import com.hmatalonga.greenhub.b.e;
import com.hmatalonga.greenhub.c.c;
import com.hmatalonga.greenhub.c.g;
import com.hmatalonga.greenhub.models.ServerStatus;
import com.hmatalonga.greenhub.network.services.GreenHubStatusService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit.Builder;
import retrofit2.converter.gson.GsonConverterFactory;

public class b {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public static final String f4297a = c.a(b.class);

    /* renamed from: b reason: collision with root package name */
    private GreenHubStatusService f4298b = ((GreenHubStatusService) new Builder().baseUrl(Config.SERVER_STATUS_URL).addConverterFactory(GsonConverterFactory.create()).build().create(GreenHubStatusService.class));

    public void a(final Context context) {
        c.b(f4297a, "callGetStatus()");
        this.f4298b.getStatus().enqueue(new Callback<ServerStatus>() {
            public void onFailure(Call<ServerStatus> call, Throwable th) {
                th.printStackTrace();
            }

            public void onResponse(Call<ServerStatus> call, Response<ServerStatus> response) {
                if (response != null && response.body() != null) {
                    c.b(b.f4297a, "Server Status: { server: " + ((ServerStatus) response.body()).server + ", version: " + ((ServerStatus) response.body()).version + " }");
                    if (!g.c(context).equals(((ServerStatus) response.body()).server)) {
                        g.b(context, false);
                    }
                    g.a(context, ((ServerStatus) response.body()).server);
                    g.b(context, ((ServerStatus) response.body()).version);
                    if (!g.b(context)) {
                        new e().execute(new Context[]{context});
                    }
                }
            }
        });
    }
}
