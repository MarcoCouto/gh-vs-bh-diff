package com.hmatalonga.greenhub.network;

import android.content.Context;
import android.os.Handler;
import com.google.a.i;
import com.google.a.l;
import com.google.a.o;
import com.hmatalonga.greenhub.b.b;
import com.hmatalonga.greenhub.c.c;
import com.hmatalonga.greenhub.c.d;
import com.hmatalonga.greenhub.c.g;
import com.hmatalonga.greenhub.events.StatusEvent;
import com.hmatalonga.greenhub.models.data.AppPermission;
import com.hmatalonga.greenhub.models.data.AppSignature;
import com.hmatalonga.greenhub.models.data.Feature;
import com.hmatalonga.greenhub.models.data.LocationProvider;
import com.hmatalonga.greenhub.models.data.ProcessInfo;
import com.hmatalonga.greenhub.models.data.Sample;
import com.hmatalonga.greenhub.models.data.Upload;
import com.hmatalonga.greenhub.network.services.GreenHubAPIService;
import com.mansoon.BatteryDouble.R;
import io.realm.bb;
import java.util.Iterator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit.Builder;
import retrofit2.converter.gson.GsonConverterFactory;

public class a {

    /* renamed from: a reason: collision with root package name */
    public static boolean f4289a = false;

    /* renamed from: b reason: collision with root package name */
    public static boolean f4290b = false;
    public static int c = 0;
    private static final String d = c.a(a.class);
    /* access modifiers changed from: private */
    public Context e;
    private GreenHubAPIService f;
    private Iterator<Integer> g;
    /* access modifiers changed from: private */
    public boolean h;

    public a(Context context, boolean z) {
        this.e = context;
        this.h = z;
        this.f = (GreenHubAPIService) new Builder().baseUrl(g.c(context)).addConverterFactory(GsonConverterFactory.create()).build().create(GreenHubAPIService.class);
    }

    private o a(Sample sample) {
        if (sample == null) {
            return null;
        }
        o oVar = new o();
        oVar.a("uuId", sample.realmGet$uuId());
        oVar.a("timestamp", (Number) Long.valueOf(sample.realmGet$timestamp()));
        oVar.a("version", (Number) Integer.valueOf(sample.realmGet$version()));
        oVar.a("database", (Number) Integer.valueOf(sample.realmGet$database()));
        oVar.a("batteryState", sample.realmGet$batteryState());
        oVar.a("batteryLevel", (Number) Double.valueOf(sample.realmGet$batteryLevel()));
        oVar.a("memoryWired", (Number) Integer.valueOf(sample.realmGet$memoryWired()));
        oVar.a("memoryActive", (Number) Integer.valueOf(sample.realmGet$memoryActive()));
        oVar.a("memoryInactive", (Number) Integer.valueOf(sample.realmGet$memoryInactive()));
        oVar.a("memoryFree", (Number) Integer.valueOf(sample.realmGet$memoryFree()));
        oVar.a("memoryUser", (Number) Integer.valueOf(sample.realmGet$memoryUser()));
        oVar.a("triggeredBy", sample.realmGet$triggeredBy());
        oVar.a("networkStatus", sample.realmGet$networkStatus());
        oVar.a("distanceTraveled", (Number) Double.valueOf(sample.realmGet$distanceTraveled()));
        oVar.a("screenBrightness", (Number) Integer.valueOf(sample.realmGet$screenBrightness()));
        o oVar2 = new o();
        oVar2.a("networkType", sample.realmGet$networkDetails().realmGet$networkType());
        oVar2.a("mobileNetworkType", sample.realmGet$networkDetails().realmGet$mobileNetworkType());
        oVar2.a("mobileDataStatus", sample.realmGet$networkDetails().realmGet$mobileDataStatus());
        oVar2.a("mobileDataActivity", sample.realmGet$networkDetails().realmGet$mobileDataActivity());
        oVar2.a("roamingEnabled", (Number) Integer.valueOf(sample.realmGet$networkDetails().realmGet$roamingEnabled()));
        oVar2.a("wifiStatus", sample.realmGet$networkDetails().realmGet$wifiStatus());
        oVar2.a("wifiSignalStrength", (Number) Integer.valueOf(sample.realmGet$networkDetails().realmGet$wifiSignalStrength()));
        oVar2.a("wifiLinkSpeed", (Number) Integer.valueOf(sample.realmGet$networkDetails().realmGet$wifiLinkSpeed()));
        oVar2.a("wifiApStatus", sample.realmGet$networkDetails().realmGet$wifiApStatus());
        oVar2.a("networkOperator", sample.realmGet$networkDetails().realmGet$networkOperator());
        oVar2.a("simOperator", sample.realmGet$networkDetails().realmGet$simOperator());
        oVar2.a("mcc", sample.realmGet$networkDetails().realmGet$mcc());
        oVar2.a("mnc", sample.realmGet$networkDetails().realmGet$mnc());
        if (sample.realmGet$networkDetails().realmGet$networkStatistics() != null) {
            o oVar3 = new o();
            oVar3.a("wifiReceived", (Number) Double.valueOf(sample.realmGet$networkDetails().realmGet$networkStatistics().realmGet$wifiReceived()));
            oVar3.a("wifiSent", (Number) Double.valueOf(sample.realmGet$networkDetails().realmGet$networkStatistics().realmGet$wifiSent()));
            oVar3.a("mobileReceived", (Number) Double.valueOf(sample.realmGet$networkDetails().realmGet$networkStatistics().realmGet$mobileReceived()));
            oVar3.a("mobileSent", (Number) Double.valueOf(sample.realmGet$networkDetails().realmGet$networkStatistics().realmGet$mobileSent()));
            oVar2.a("networkStatistics", (l) oVar3);
        }
        oVar.a("networkDetails", (l) oVar2);
        o oVar4 = new o();
        oVar4.a("charger", sample.realmGet$batteryDetails().realmGet$charger());
        oVar4.a("health", sample.realmGet$batteryDetails().realmGet$health());
        oVar4.a("voltage", (Number) Double.valueOf(sample.realmGet$batteryDetails().realmGet$voltage()));
        oVar4.a("temperature", (Number) Double.valueOf(sample.realmGet$batteryDetails().realmGet$temperature()));
        oVar4.a("technology", sample.realmGet$batteryDetails().realmGet$technology());
        oVar4.a("capacity", (Number) Integer.valueOf(sample.realmGet$batteryDetails().realmGet$capacity()));
        oVar4.a("chargeCounter", (Number) Integer.valueOf(sample.realmGet$batteryDetails().realmGet$chargeCounter()));
        oVar4.a("currentAverage", (Number) Integer.valueOf(sample.realmGet$batteryDetails().realmGet$currentAverage()));
        oVar4.a("currentNow", (Number) Integer.valueOf(sample.realmGet$batteryDetails().realmGet$currentNow()));
        oVar4.a("energyCounter", (Number) Long.valueOf(sample.realmGet$batteryDetails().realmGet$energyCounter()));
        oVar.a("batteryDetails", (l) oVar4);
        o oVar5 = new o();
        oVar5.a("cpuUsage", (Number) Double.valueOf(sample.realmGet$cpuStatus().realmGet$cpuUsage()));
        oVar5.a("upTime", (Number) Long.valueOf(sample.realmGet$cpuStatus().realmGet$upTime()));
        oVar5.a("sleepTime", (Number) Long.valueOf(sample.realmGet$cpuStatus().realmGet$sleepTime()));
        oVar.a("cpuStatus", (l) oVar5);
        oVar.a("screenOn", (Number) Integer.valueOf(sample.realmGet$screenOn()));
        oVar.a("timeZone", sample.realmGet$timeZone());
        o oVar6 = new o();
        oVar6.a("bluetoothEnabled", Boolean.valueOf(sample.realmGet$settings().realmGet$bluetoothEnabled()));
        oVar6.a("locationEnabled", Boolean.valueOf(sample.realmGet$settings().realmGet$locationEnabled()));
        oVar6.a("powersaverEnabled", Boolean.valueOf(sample.realmGet$settings().realmGet$powersaverEnabled()));
        oVar6.a("flashlightEnabled", Boolean.valueOf(sample.realmGet$settings().realmGet$flashlightEnabled()));
        oVar6.a("nfcEnabled", Boolean.valueOf(sample.realmGet$settings().realmGet$nfcEnabled()));
        oVar6.a("unknownSources", (Number) Integer.valueOf(sample.realmGet$settings().realmGet$unknownSources()));
        oVar6.a("developerMode", (Number) Integer.valueOf(sample.realmGet$settings().realmGet$developerMode()));
        oVar.a("settings", (l) oVar6);
        o oVar7 = new o();
        oVar7.a("free", (Number) Integer.valueOf(sample.realmGet$storageDetails().realmGet$free()));
        oVar7.a("total", (Number) Integer.valueOf(sample.realmGet$storageDetails().realmGet$total()));
        oVar7.a("freeExternal", (Number) Integer.valueOf(sample.realmGet$storageDetails().realmGet$freeExternal()));
        oVar7.a("totalExternal", (Number) Integer.valueOf(sample.realmGet$storageDetails().realmGet$totalExternal()));
        oVar7.a("freeSystem", (Number) Integer.valueOf(sample.realmGet$storageDetails().realmGet$freeSystem()));
        oVar7.a("totalSystem", (Number) Integer.valueOf(sample.realmGet$storageDetails().realmGet$totalSystem()));
        oVar7.a("freeSecondary", (Number) Integer.valueOf(sample.realmGet$storageDetails().realmGet$freeSecondary()));
        oVar7.a("totalSecondary", (Number) Integer.valueOf(sample.realmGet$storageDetails().realmGet$totalSecondary()));
        oVar.a("storageDetails", (l) oVar7);
        oVar.a("countryCode", sample.realmGet$countryCode());
        if (sample.realmGet$locationProviders() != null && !sample.realmGet$locationProviders().isEmpty()) {
            i iVar = new i();
            Iterator it = sample.realmGet$locationProviders().iterator();
            while (it.hasNext()) {
                LocationProvider locationProvider = (LocationProvider) it.next();
                o oVar8 = new o();
                oVar8.a("provider", locationProvider.realmGet$provider());
                iVar.a(oVar8);
            }
            oVar.a("locationProviders", (l) iVar);
        }
        if (sample.realmGet$features() != null && !sample.realmGet$features().isEmpty()) {
            i iVar2 = new i();
            Iterator it2 = sample.realmGet$features().iterator();
            while (it2.hasNext()) {
                Feature feature = (Feature) it2.next();
                o oVar9 = new o();
                oVar9.a("key", feature.realmGet$key());
                oVar9.a("value", feature.realmGet$value());
                iVar2.a(oVar9);
            }
            oVar.a("features", (l) iVar2);
        }
        if (sample.realmGet$processInfos() != null && !sample.realmGet$processInfos().isEmpty()) {
            i iVar3 = new i();
            Iterator it3 = sample.realmGet$processInfos().iterator();
            while (it3.hasNext()) {
                ProcessInfo processInfo = (ProcessInfo) it3.next();
                o oVar10 = new o();
                oVar10.a("processId", (Number) Integer.valueOf(processInfo.realmGet$processId()));
                oVar10.a("name", processInfo.realmGet$name());
                oVar10.a("applicationLabel", processInfo.realmGet$applicationLabel() == null ? "" : processInfo.realmGet$applicationLabel());
                oVar10.a("isSystemApp", Boolean.valueOf(processInfo.realmGet$isSystemApp()));
                oVar10.a("importance", processInfo.realmGet$importance());
                oVar10.a("versionName", processInfo.realmGet$versionName() == null ? "" : processInfo.realmGet$versionName());
                oVar10.a("versionCode", (Number) Integer.valueOf(processInfo.realmGet$versionCode()));
                oVar10.a("installationPkg", processInfo.realmGet$installationPkg() == null ? "" : processInfo.realmGet$installationPkg());
                if (processInfo.realmGet$appPermissions() != null && !processInfo.realmGet$appPermissions().isEmpty()) {
                    i iVar4 = new i();
                    Iterator it4 = processInfo.realmGet$appPermissions().iterator();
                    while (it4.hasNext()) {
                        AppPermission appPermission = (AppPermission) it4.next();
                        o oVar11 = new o();
                        oVar11.a("permission", appPermission.realmGet$permission());
                        iVar4.a(oVar11);
                    }
                    oVar10.a("appPermissions", (l) iVar4);
                }
                if (processInfo.realmGet$appSignatures() != null && !processInfo.realmGet$appSignatures().isEmpty()) {
                    i iVar5 = new i();
                    Iterator it5 = processInfo.realmGet$appSignatures().iterator();
                    while (it5.hasNext()) {
                        AppSignature appSignature = (AppSignature) it5.next();
                        o oVar12 = new o();
                        oVar12.a("signature", appSignature.realmGet$signature());
                        iVar5.a(oVar12);
                    }
                    oVar10.a("appSignatures", (l) iVar5);
                }
                iVar3.a(oVar10);
            }
            oVar.a("processInfos", (l) iVar3);
        }
        return oVar;
    }

    private String a(long j) {
        return this.e.getString(R.string.event_uploading) + " " + j + " " + this.e.getString(R.string.event_samples);
    }

    private void a(final int i) {
        c.b(d, "Uploading Sample => " + i);
        bb m = bb.m();
        Upload upload = new Upload(a((Sample) m.a(Sample.class).a("id", Integer.valueOf(i)).c()));
        m.close();
        this.f.createSample(upload).enqueue(new Callback<Integer>() {
            public void onFailure(Call<Integer> call, Throwable th) {
                th.printStackTrace();
                org.greenrobot.eventbus.c.a().c(new StatusEvent(a.this.e.getString(R.string.event_server_not_responding)));
                if (a.this.h) {
                    a.c++;
                }
                a.f4289a = false;
                a.this.b();
            }

            public void onResponse(Call<Integer> call, Response<Integer> response) {
                if (response == null) {
                    org.greenrobot.eventbus.c.a().c(new StatusEvent(a.this.e.getString(R.string.event_server_response_failed)));
                } else if (response.body() != null) {
                    a.this.a(((Integer) response.body()).intValue(), i);
                } else {
                    a.this.a(0, -1);
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(int i, int i2) {
        if (i == 1) {
            c.b(d, "Sample => " + i2 + " uploaded successfully! Deleting uploaded sample...");
            new b().execute(new Integer[]{Integer.valueOf(i2)});
            if (!this.g.hasNext()) {
                org.greenrobot.eventbus.c.a().c(new StatusEvent(this.e.getString(R.string.event_upload_finished)));
                if (this.h) {
                    c = 0;
                }
                f4289a = false;
                b();
                return;
            }
            a(((Integer) this.g.next()).intValue());
        } else if (i == 0) {
            org.greenrobot.eventbus.c.a().c(new StatusEvent(this.e.getString(R.string.event_error_uploading_sample)));
            f4289a = false;
            b();
        }
    }

    /* access modifiers changed from: private */
    public void b() {
        new Handler().postDelayed(new Runnable() {
            public void run() {
                org.greenrobot.eventbus.c.a().c(new StatusEvent(a.this.e.getString(R.string.event_idle)));
            }
        }, 10000);
    }

    public void a() {
        if (!d.a(this.e, 2)) {
            org.greenrobot.eventbus.c.a().c(new StatusEvent(this.e.getString(R.string.event_no_connectivity)));
            f4289a = false;
            f4290b = true;
            return;
        }
        com.hmatalonga.greenhub.managers.a.a aVar = new com.hmatalonga.greenhub.managers.a.a();
        long a2 = aVar.a(Sample.class);
        this.g = aVar.e();
        aVar.b();
        if (!this.g.hasNext()) {
            org.greenrobot.eventbus.c.a().c(new StatusEvent(this.e.getString(R.string.event_no_samples)));
            f4289a = false;
            b();
            return;
        }
        org.greenrobot.eventbus.c.a().c(new StatusEvent(a(a2)));
        f4289a = true;
        a(((Integer) this.g.next()).intValue());
    }
}
