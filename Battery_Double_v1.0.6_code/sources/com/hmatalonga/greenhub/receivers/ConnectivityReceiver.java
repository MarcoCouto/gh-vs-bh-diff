package com.hmatalonga.greenhub.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.hmatalonga.greenhub.b.f;
import com.hmatalonga.greenhub.c.c;
import com.hmatalonga.greenhub.c.g;
import com.hmatalonga.greenhub.events.RefreshEvent;
import com.hmatalonga.greenhub.network.a;

public class ConnectivityReceiver extends BroadcastReceiver {

    /* renamed from: a reason: collision with root package name */
    private static final String f4302a = c.a("ConnectivityReceiver");

    public void onReceive(Context context, Intent intent) {
        boolean z;
        String action = intent.getAction();
        switch (action.hashCode()) {
            case -1530327060:
                if (action.equals("android.bluetooth.adapter.action.STATE_CHANGED")) {
                    z = true;
                    break;
                }
            case -1172645946:
                if (action.equals("android.net.conn.CONNECTIVITY_CHANGE")) {
                    z = false;
                    break;
                }
            default:
                z = true;
                break;
        }
        switch (z) {
            case false:
                NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
                if (!(activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting())) {
                    org.greenrobot.eventbus.c.a().c(new RefreshEvent("wifi", false));
                    org.greenrobot.eventbus.c.a().c(new RefreshEvent("mobile", false));
                    return;
                }
                a.c = 0;
                new f().execute(new Context[]{context});
                if (g.b(context)) {
                    new com.hmatalonga.greenhub.b.a().execute(new Context[]{context});
                }
                if (a.f4290b && g.d(context)) {
                    new a(context, true).a();
                    a.f4290b = false;
                }
                if (activeNetworkInfo.getType() == 1) {
                    org.greenrobot.eventbus.c.a().c(new RefreshEvent("wifi", true));
                    return;
                } else if (activeNetworkInfo.getType() == 0) {
                    org.greenrobot.eventbus.c.a().c(new RefreshEvent("mobile", true));
                    return;
                } else {
                    return;
                }
            case true:
                if (intent.getIntExtra("android.bluetooth.adapter.extra.STATE", -1) == 12) {
                    org.greenrobot.eventbus.c.a().c(new RefreshEvent("bluetooth", true));
                    return;
                } else {
                    org.greenrobot.eventbus.c.a().c(new RefreshEvent("bluetooth", false));
                    return;
                }
            default:
                return;
        }
    }
}
