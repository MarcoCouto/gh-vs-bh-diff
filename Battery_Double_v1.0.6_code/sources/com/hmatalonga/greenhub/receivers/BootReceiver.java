package com.hmatalonga.greenhub.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.hmatalonga.greenhub.c.c;
import com.hmatalonga.greenhub.c.e;
import com.hmatalonga.greenhub.c.g;

public class BootReceiver extends BroadcastReceiver {

    /* renamed from: a reason: collision with root package name */
    private static final String f4301a = c.a(BootReceiver.class);

    public void onReceive(Context context, Intent intent) {
        c.b(f4301a, "BOOT_COMPLETED onReceive()");
        if (g.a(context) && g.l(context)) {
            e.a(context);
        }
    }
}
