package com.hmatalonga.greenhub.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.ReceiverCallNotAllowedException;
import com.hmatalonga.greenhub.c.c;
import com.hmatalonga.greenhub.c.e;

public class NotificationReceiver extends BroadcastReceiver {

    /* renamed from: a reason: collision with root package name */
    private static final String f4303a = c.a(NotificationReceiver.class);

    public void onReceive(Context context, Intent intent) {
        c.b(f4303a, "onReceive called!");
        try {
            e.b(context);
        } catch (ReceiverCallNotAllowedException e) {
            e.printStackTrace();
        }
    }
}
