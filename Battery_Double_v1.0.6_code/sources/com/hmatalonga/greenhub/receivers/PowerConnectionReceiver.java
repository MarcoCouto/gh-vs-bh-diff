package com.hmatalonga.greenhub.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import com.hmatalonga.greenhub.c.c;
import com.hmatalonga.greenhub.events.PowerSourceEvent;
import com.hmatalonga.greenhub.managers.a.a;

public class PowerConnectionReceiver extends BroadcastReceiver {

    /* renamed from: a reason: collision with root package name */
    private static final String f4304a = c.a(PowerConnectionReceiver.class);

    public void onReceive(Context context, Intent intent) {
        boolean z = true;
        if (intent.getAction().equals("android.intent.action.ACTION_POWER_CONNECTED")) {
            Intent registerReceiver = context.getApplicationContext().registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
            if (registerReceiver != null) {
                int intExtra = registerReceiver.getIntExtra("plugged", -1);
                boolean z2 = intExtra == 2;
                boolean z3 = intExtra == 1;
                if (intExtra != 4) {
                    z = false;
                }
                if (z3) {
                    org.greenrobot.eventbus.c.a().c(new PowerSourceEvent("ac"));
                } else if (z2) {
                    org.greenrobot.eventbus.c.a().c(new PowerSourceEvent("usb"));
                } else if (z) {
                    org.greenrobot.eventbus.c.a().c(new PowerSourceEvent("wireless"));
                }
            } else {
                return;
            }
        } else if (intent.getAction().equals("android.intent.action.ACTION_POWER_DISCONNECTED")) {
            org.greenrobot.eventbus.c.a().c(new PowerSourceEvent("unplugged"));
        }
        a aVar = new a();
        c.b(f4304a, "Getting new session");
        aVar.a(com.hmatalonga.greenhub.managers.sampling.a.b(context, intent));
        aVar.b();
    }
}
