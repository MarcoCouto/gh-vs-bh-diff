package com.hmatalonga.greenhub.c;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class d {
    public static boolean a(Context context, int i) {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo != null) {
            if (activeNetworkInfo.getType() == 1) {
                return activeNetworkInfo.isConnected();
            }
            if (activeNetworkInfo.getType() == 0) {
                if (i == 1) {
                    return activeNetworkInfo.isConnected();
                }
                if (i == 2) {
                    return g.h(context) && activeNetworkInfo.isConnected();
                }
            }
        }
        return false;
    }
}
