package com.hmatalonga.greenhub.c;

import android.content.Context;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.preference.PreferenceManager;
import com.hmatalonga.greenhub.Config;

public class g {
    public static void a(Context context, int i) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putInt("pref_message_last", i).apply();
    }

    public static void a(Context context, long j) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putLong("pref_last_temperature_alert", j).apply();
    }

    public static void a(Context context, OnSharedPreferenceChangeListener onSharedPreferenceChangeListener) {
        PreferenceManager.getDefaultSharedPreferences(context).registerOnSharedPreferenceChangeListener(onSharedPreferenceChangeListener);
    }

    public static void a(Context context, String str) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("pref_server_url", str).apply();
    }

    public static void a(Context context, boolean z) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean("pref_tos_accepted", z).apply();
    }

    public static boolean a(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("pref_tos_accepted", false);
    }

    public static void b(Context context, int i) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putInt("pref_app_version", i).apply();
    }

    public static void b(Context context, OnSharedPreferenceChangeListener onSharedPreferenceChangeListener) {
        PreferenceManager.getDefaultSharedPreferences(context).unregisterOnSharedPreferenceChangeListener(onSharedPreferenceChangeListener);
    }

    public static void b(Context context, boolean z) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean("pref_device_registered", z).apply();
    }

    public static boolean b(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("pref_device_registered", false);
    }

    public static String c(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString("pref_server_url", Config.SERVER_URL_DEFAULT);
    }

    public static void c(Context context, boolean z) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean("pref_send_installed", z).apply();
    }

    public static boolean d(Context context) {
        return !PreferenceManager.getDefaultSharedPreferences(context).getString("pref_server_url", Config.SERVER_URL_DEFAULT).equals(Config.SERVER_URL_DEFAULT);
    }

    public static boolean e(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("pref_send_installed", false);
    }

    public static boolean f(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("pref_sampling_screen", false);
    }

    public static int g(Context context) {
        return Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(context).getString("pref_data_history", Config.DATA_HISTORY_DEFAULT));
    }

    public static boolean h(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("pref_mobile_data", false);
    }

    public static boolean i(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("pref_auto_upload", true);
    }

    public static int j(Context context) {
        return Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(context).getString("pref_upload_rate", Config.UPLOAD_DEFAULT_RATE));
    }

    public static int k(Context context) {
        return Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(context).getString("pref_notifications_priority", Config.NOTIFICATION_DEFAULT_PRIORITY));
    }

    public static boolean l(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("pref_power_indicator", false);
    }

    public static boolean m(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("pref_battery_alerts", true);
    }

    public static boolean n(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("pref_charge_alerts", true);
    }

    public static boolean o(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("pref_temperature_alerts", true);
    }

    public static int p(Context context) {
        return Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(context).getString("pref_temperature_rate", Config.NOTIFICATION_DEFAULT_TEMPERATURE_RATE));
    }

    public static int q(Context context) {
        return Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(context).getString("pref_temperature_warning", Config.NOTIFICATION_DEFAULT_TEMPERATURE_WARNING).replaceFirst("^0+(?!$)", ""));
    }

    public static int r(Context context) {
        return Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(context).getString("pref_temperature_high", Config.NOTIFICATION_DEFAULT_TEMPERATURE_HIGH).replaceFirst("^0+(?!$)", ""));
    }

    public static long s(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getLong("pref_last_temperature_alert", 0);
    }

    public static boolean t(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("pref_message_alerts", true);
    }

    public static int u(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt("pref_message_last", 0);
    }

    public static boolean v(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("pref_system_apps", true);
    }
}
