package com.hmatalonga.greenhub.c;

import android.content.Context;
import android.util.SparseArray;
import com.mansoon.BatteryDouble.R;
import java.text.NumberFormat;

public class h {

    /* renamed from: a reason: collision with root package name */
    private static final SparseArray<String> f4277a = new SparseArray<>();

    static {
        f4277a.put(500, "Not running");
        f4277a.put(400, "Background process");
        f4277a.put(300, "Service");
        f4277a.put(200, "Visible task");
        f4277a.put(100, "Foreground app");
    }

    public static String a(double d) {
        NumberFormat numberInstance = NumberFormat.getNumberInstance();
        numberInstance.setMaximumFractionDigits(2);
        return numberInstance.format(d);
    }

    public static String a(float f) {
        NumberFormat percentInstance = NumberFormat.getPercentInstance();
        percentInstance.setMinimumFractionDigits(0);
        return percentInstance.format((double) f);
    }

    public static String a(int i) {
        String str = (String) f4277a.get(i);
        if (str != null && str.length() != 0) {
            return str;
        }
        c.c("Importance not found: ", "" + i);
        return "Unknown";
    }

    public static String a(Context context, String str) {
        if (str == null) {
            return context.getString(R.string.priorityDefault);
        }
        char c = 65535;
        switch (str.hashCode()) {
            case -1577917980:
                if (str.equals("Foreground app")) {
                    c = 4;
                    break;
                }
                break;
            case -1103780316:
                if (str.equals("Suggestion")) {
                    c = 6;
                    break;
                }
                break;
            case -786595502:
                if (str.equals("Not running")) {
                    c = 0;
                    break;
                }
                break;
            case -646160747:
                if (str.equals("Service")) {
                    c = 2;
                    break;
                }
                break;
            case -131006317:
                if (str.equals("Visible task")) {
                    c = 3;
                    break;
                }
                break;
            case 884577168:
                if (str.equals("Perceptible task")) {
                    c = 5;
                    break;
                }
                break;
            case 1295367581:
                if (str.equals("Background process")) {
                    c = 1;
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
                return context.getString(R.string.prioritynotrunning);
            case 1:
                return context.getString(R.string.prioritybackground);
            case 2:
                return context.getString(R.string.priorityservice);
            case 3:
                return context.getString(R.string.priorityvisible);
            case 4:
                return context.getString(R.string.priorityforeground);
            case 5:
                return context.getString(R.string.priorityperceptible);
            case 6:
                return context.getString(R.string.prioritysuggestion);
            default:
                return context.getString(R.string.priorityDefault);
        }
    }

    public static String a(String str) {
        int lastIndexOf = str.lastIndexOf(58);
        if (lastIndexOf <= 0) {
            lastIndexOf = str.length();
        }
        return str.substring(0, lastIndexOf);
    }

    public static String a(String str, int i) {
        return str.length() > i ? str.substring(0, i) + "..." : str;
    }

    public static String a(byte[] bArr) {
        StringBuilder sb = new StringBuilder();
        for (byte b2 : bArr) {
            byte b3 = (b2 >>> 4) & 15;
            int i = 0;
            while (true) {
                sb.append((b3 < 0 || b3 > 9) ? (char) ((b3 - 10) + 97) : (char) (b3 + 48));
                byte b4 = b2 & 15;
                int i2 = i + 1;
                if (i >= 1) {
                    break;
                }
                i = i2;
                b3 = b4;
            }
        }
        return sb.toString();
    }

    public static String b(float f) {
        NumberFormat numberInstance = NumberFormat.getNumberInstance();
        numberInstance.setMinimumFractionDigits(1);
        return numberInstance.format((double) f);
    }
}
