package com.hmatalonga.greenhub.c;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.os.Build.VERSION;
import android.support.v4.a.aa.c;
import com.hmatalonga.greenhub.Config;
import com.hmatalonga.greenhub.managers.sampling.DataEstimator;
import com.hmatalonga.greenhub.managers.sampling.a;
import com.hmatalonga.greenhub.models.Battery;
import com.hmatalonga.greenhub.ui.InboxActivity;
import com.hmatalonga.greenhub.ui.MainActivity;
import com.mansoon.BatteryDouble.R;

public class e {

    /* renamed from: a reason: collision with root package name */
    private static final String f4274a = c.a(e.class);

    /* renamed from: b reason: collision with root package name */
    private static boolean f4275b = false;
    private static c c = null;
    private static NotificationManager d = null;

    public static void a() {
        d.cancel(Config.NOTIFICATION_BATTERY_STATUS);
        f4275b = false;
    }

    public static void a(Context context) {
        if (!f4275b) {
            DataEstimator dataEstimator = new DataEstimator();
            dataEstimator.getCurrentStatus(context);
            int batteryCurrentNow = Battery.getBatteryCurrentNow(context);
            int level = dataEstimator.getLevel();
            String str = context.getString(R.string.now) + ": " + batteryCurrentNow + " mA";
            c = new c(context).a((CharSequence) str).b((CharSequence) context.getString(R.string.notif_batteryhub_running)).b(false).a(true).b(g.k(context));
            if (VERSION.SDK_INT >= 21) {
                c.c(1);
            }
            if (level < 100) {
                c.a(level + R.drawable.ic_stat_00_pct_charged);
            } else {
                c.a((int) R.drawable.ic_stat_z100_pct_charged);
            }
            Intent intent = new Intent(context, MainActivity.class);
            TaskStackBuilder create = TaskStackBuilder.create(context);
            create.addParentStack(MainActivity.class);
            create.addNextIntent(intent);
            c.a(create.getPendingIntent(0, 134217728));
            d = (NotificationManager) context.getSystemService("notification");
            d.notify(Config.NOTIFICATION_BATTERY_STATUS, c.a());
            f4275b = true;
        }
    }

    public static void b(Context context) {
        if (!f4275b) {
            a(context);
            return;
        }
        int b2 = (int) (a.b() * 100.0d);
        String str = context.getString(R.string.now) + ": " + Battery.getBatteryCurrentNow(context) + " mA";
        c.a((CharSequence) str).b((CharSequence) context.getString(R.string.notif_batteryhub_running)).b(false).a(true).b(g.k(context));
        if (VERSION.SDK_INT >= 21) {
            c.c(1);
        }
        if (b2 < 100) {
            c.a(b2 + R.drawable.ic_stat_00_pct_charged);
        } else {
            c.a((int) R.drawable.ic_stat_z100_pct_charged);
        }
        c.b(f4274a, "Updating value of notification");
        d.notify(Config.NOTIFICATION_BATTERY_STATUS, c.a());
    }

    public static void c(Context context) {
        if (d == null) {
            d = (NotificationManager) context.getSystemService("notification");
        }
        c b2 = new c(context).a((int) R.drawable.ic_email_white_24dp).a((CharSequence) context.getString(R.string.notif_new_message)).b((CharSequence) context.getString(R.string.notif_open_inbox)).b(true).a(false).a(-16711936, 500, Config.STARTUP_CURRENT_INTERVAL).a(new long[]{0, 800, 1500}).b(g.k(context));
        if (VERSION.SDK_INT >= 21) {
            b2.c(1);
        }
        Intent intent = new Intent(context, InboxActivity.class);
        TaskStackBuilder create = TaskStackBuilder.create(context);
        create.addParentStack(InboxActivity.class);
        create.addNextIntent(intent);
        b2.a(create.getPendingIntent(0, 134217728));
        Notification a2 = b2.a();
        a2.flags |= 8;
        d.notify(Config.NOTIFICATION_MESSAGE_NEW, a2);
    }

    public static void d(Context context) {
        if (d == null) {
            d = (NotificationManager) context.getSystemService("notification");
        }
        c b2 = new c(context).a((int) R.drawable.ic_information_white_24dp).a((CharSequence) context.getString(R.string.notif_battery_full)).b((CharSequence) context.getString(R.string.notif_remove_charger)).b(true).a(false).a(-16711936, 500, Config.STARTUP_CURRENT_INTERVAL).a(new long[]{0, 400, 1000}).b(g.k(context));
        if (VERSION.SDK_INT >= 21) {
            b2.c(1);
        }
        Intent intent = new Intent(context, MainActivity.class);
        TaskStackBuilder create = TaskStackBuilder.create(context);
        create.addParentStack(InboxActivity.class);
        create.addNextIntent(intent);
        b2.a(create.getPendingIntent(0, 134217728));
        d.notify(Config.NOTIFICATION_BATTERY_FULL, b2.a());
    }

    public static void e(Context context) {
        if (d == null) {
            d = (NotificationManager) context.getSystemService("notification");
        }
        c b2 = new c(context).a((int) R.drawable.ic_alert_circle_white_24dp).a((CharSequence) context.getString(R.string.notif_battery_low)).b((CharSequence) context.getString(R.string.notif_connect_power)).b(true).a(false).a(-65536, 500, Config.STARTUP_CURRENT_INTERVAL).a(new long[]{0, 400, 1000}).b(g.k(context));
        if (VERSION.SDK_INT >= 21) {
            b2.c(1);
        }
        Intent intent = new Intent(context, MainActivity.class);
        TaskStackBuilder create = TaskStackBuilder.create(context);
        create.addParentStack(InboxActivity.class);
        create.addNextIntent(intent);
        b2.a(create.getPendingIntent(0, 134217728));
        d.notify(Config.NOTIFICATION_BATTERY_LOW, b2.a());
    }

    public static void f(Context context) {
        if (d == null) {
            d = (NotificationManager) context.getSystemService("notification");
        }
        c b2 = new c(context).a((int) R.drawable.ic_alert_circle_white_24dp).a((CharSequence) context.getString(R.string.notif_battery_warning)).b((CharSequence) context.getString(R.string.notif_battery_warm)).b(true).a(false).a(-256, 500, Config.STARTUP_CURRENT_INTERVAL).a(new long[]{0, 400, 1000}).b(g.k(context));
        if (VERSION.SDK_INT >= 21) {
            b2.c(1);
        }
        Intent intent = new Intent(context, MainActivity.class);
        intent.putExtra("tab", 2);
        TaskStackBuilder create = TaskStackBuilder.create(context);
        create.addParentStack(InboxActivity.class);
        create.addNextIntent(intent);
        b2.a(create.getPendingIntent(0, 134217728));
        Notification a2 = b2.a();
        a2.flags |= 8;
        d.notify(Config.NOTIFICATION_TEMPERATURE_WARNING, a2);
    }

    public static void g(Context context) {
        if (d == null) {
            d = (NotificationManager) context.getSystemService("notification");
        }
        c b2 = new c(context).a((int) R.drawable.ic_alert_circle_white_24dp).a((CharSequence) context.getString(R.string.notif_battery_hot)).b((CharSequence) context.getString(R.string.notif_battery_cooldown)).b(true).a(false).a(-65536, 500, Config.STARTUP_CURRENT_INTERVAL).a(new long[]{0, 800, 1500}).b(g.k(context));
        if (VERSION.SDK_INT >= 21) {
            b2.c(1);
        }
        Intent intent = new Intent(context, MainActivity.class);
        intent.putExtra("tab", 2);
        TaskStackBuilder create = TaskStackBuilder.create(context);
        create.addParentStack(InboxActivity.class);
        create.addNextIntent(intent);
        b2.a(create.getPendingIntent(0, 134217728));
        Notification a2 = b2.a();
        a2.flags |= 8;
        d.notify(Config.NOTIFICATION_TEMPERATURE_HIGH, a2);
    }
}
