package com.hmatalonga.greenhub.c;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class a {

    /* renamed from: a reason: collision with root package name */
    private static SimpleDateFormat f4271a = new SimpleDateFormat("dd-MM HH:mm", Locale.UK);

    public static long a(int i) {
        long currentTimeMillis = System.currentTimeMillis();
        return i == 1 ? currentTimeMillis - 86400000 : i == 2 ? currentTimeMillis - 259200000 : i == 3 ? currentTimeMillis - 432000000 : i == 4 ? currentTimeMillis - 864000000 : i == 5 ? currentTimeMillis - 1296000000 : currentTimeMillis;
    }

    public static String a(Long l) {
        Calendar instance = Calendar.getInstance();
        instance.setTimeInMillis(l.longValue());
        return f4271a.format(instance.getTime());
    }
}
