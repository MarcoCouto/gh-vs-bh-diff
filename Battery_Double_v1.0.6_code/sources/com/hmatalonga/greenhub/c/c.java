package com.hmatalonga.greenhub.c;

import android.util.Log;

public class c {

    /* renamed from: a reason: collision with root package name */
    public static boolean f4272a = false;

    /* renamed from: b reason: collision with root package name */
    private static final int f4273b = "greenhub_".length();

    public static String a(Class cls) {
        return a(cls.getSimpleName());
    }

    public static String a(String str) {
        return str.length() > 23 - f4273b ? "greenhub_" + str.substring(0, (23 - f4273b) - 1) : "greenhub_" + str;
    }

    public static void a(String str, String str2) {
        if (f4272a && Log.isLoggable(str, 3)) {
            Log.d(str, str2);
        }
    }

    public static void b(String str, String str2) {
        if (f4272a) {
            Log.i(str, str2);
        }
    }

    public static void c(String str, String str2) {
        if (f4272a) {
            Log.e(str, str2);
        }
    }
}
