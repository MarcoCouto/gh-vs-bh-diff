package com.hmatalonga.greenhub.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;
import com.hmatalonga.greenhub.events.OpenMessageEvent;
import com.hmatalonga.greenhub.managers.a.a;
import com.hmatalonga.greenhub.models.data.Message;
import com.hmatalonga.greenhub.ui.a.c;
import com.mansoon.BatteryDouble.R;
import java.util.ArrayList;
import java.util.Iterator;
import org.greenrobot.eventbus.ThreadMode;
import org.greenrobot.eventbus.j;

public class InboxActivity extends a {
    private RecyclerView m;
    private TextView n;
    private c o;
    private ArrayList<Message> p;

    private void l() {
        this.p = new ArrayList<>();
        a aVar = new a();
        Iterator it = aVar.f().iterator();
        while (it.hasNext()) {
            Message message = (Message) it.next();
            this.p.add(new Message(message.realmGet$id(), message.realmGet$title(), message.realmGet$body(), message.realmGet$date(), message.realmGet$read()));
        }
        aVar.b();
        m();
    }

    private void m() {
        if (this.o == null) {
            this.o = new c(this.p);
            this.m.setAdapter(this.o);
        } else {
            this.o.a(this.p);
        }
        this.m.invalidate();
        if (this.p.isEmpty()) {
            this.m.setVisibility(8);
            this.n.setVisibility(0);
            return;
        }
        this.m.setVisibility(0);
        this.n.setVisibility(8);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_inbox);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        if (toolbar != null) {
            a(toolbar);
        }
        android.support.v7.app.a g = g();
        if (g != null) {
            g.a(true);
        }
        this.n = (TextView) findViewById(R.id.no_messages_view);
        this.m = (RecyclerView) findViewById(R.id.rv);
        this.o = null;
        this.m.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        this.m.setHasFixedSize(true);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        l();
    }

    public void onStart() {
        super.onStart();
        org.greenrobot.eventbus.c.a().a((Object) this);
    }

    public void onStop() {
        org.greenrobot.eventbus.c.a().b(this);
        super.onStop();
    }

    @j(a = ThreadMode.MAIN)
    public void openMessage(OpenMessageEvent openMessageEvent) {
        Message message = (Message) this.p.get(openMessageEvent.index);
        if (!message.realmGet$read()) {
            a aVar = new a();
            aVar.a(message.realmGet$id());
            aVar.b();
        }
        Intent intent = new Intent(this, MessageActivity.class);
        intent.putExtra("id", message.realmGet$id());
        intent.putExtra("title", message.realmGet$title());
        intent.putExtra("body", message.realmGet$body());
        intent.putExtra("date", message.realmGet$date());
        startActivity(intent);
    }
}
