package com.hmatalonga.greenhub.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.c;

public class SplashActivity extends c {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }
}
