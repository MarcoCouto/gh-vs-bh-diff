package com.hmatalonga.greenhub.ui;

import android.content.Context;
import android.support.v4.i.w;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class LockableViewPager extends w {
    private boolean d;

    public LockableViewPager(Context context) {
        super(context);
    }

    public LockableViewPager(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.d = true;
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        if (this.d) {
            return super.onInterceptTouchEvent(motionEvent);
        }
        return false;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (this.d) {
            return super.onTouchEvent(motionEvent);
        }
        return false;
    }

    public void setSwipeable(boolean z) {
        this.d = z;
    }
}
