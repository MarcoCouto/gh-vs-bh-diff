package com.hmatalonga.greenhub.ui;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.design.widget.o;
import android.support.design.widget.o.b;
import android.support.design.widget.o.e;
import android.support.v4.a.a.C0011a;
import android.support.v4.i.w;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.Toolbar.c;
import android.view.DragEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnDragListener;
import android.widget.ImageButton;
import com.crashlytics.android.a.m;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.g;
import com.hmatalonga.greenhub.GreenHubApp;
import com.hmatalonga.greenhub.b.f;
import com.hmatalonga.greenhub.c.d;
import com.hmatalonga.greenhub.events.RefreshChartEvent;
import com.hmatalonga.greenhub.managers.a.a;
import com.hmatalonga.greenhub.managers.sampling.DataEstimator;
import com.hmatalonga.greenhub.ui.layouts.MainTabLayout;
import com.mansoon.BatteryDouble.R;

public class MainActivity extends a implements C0011a, c {
    private static final String o = com.hmatalonga.greenhub.c.c.a(MainActivity.class);
    public a m;
    g n;
    private GreenHubApp p;
    /* access modifiers changed from: private */
    public LockableViewPager q;
    private AdView r;

    private void a(String str, int i) {
        if (android.support.v4.b.a.a((Context) this, str) != 0) {
            android.support.v4.a.a.a(this, new String[]{str}, i);
        }
    }

    /* access modifiers changed from: private */
    public void m() {
        this.n.a(new com.google.android.gms.ads.c.a().b("2F4FCE3D2D008DBE17DAF1B46F033D6B").a());
    }

    private void n() {
        Context applicationContext = getApplicationContext();
        this.m = new a();
        this.p = (GreenHubApp) getApplication();
        if (com.hmatalonga.greenhub.c.g.a(applicationContext)) {
            this.p.startGreenHubService();
            if (d.a(applicationContext, 1)) {
                new f().execute(new Context[]{applicationContext});
                if (com.hmatalonga.greenhub.c.g.b(applicationContext)) {
                    new com.hmatalonga.greenhub.b.a().execute(new Context[]{applicationContext});
                }
            }
        }
        o();
    }

    private void o() {
        this.q = (LockableViewPager) findViewById(R.id.viewpager);
        this.q.setOffscreenPageLimit(2);
        final com.hmatalonga.greenhub.ui.a.d dVar = new com.hmatalonga.greenhub.ui.a.d(getFragmentManager());
        this.q.setAdapter(dVar);
        MainTabLayout mainTabLayout = (MainTabLayout) findViewById(R.id.tab_layout);
        mainTabLayout.d();
        final ImageButton imageButton = (ImageButton) findViewById(R.id.fabSendSample);
        if (imageButton != null) {
            imageButton.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    MainActivity.this.getApplicationContext();
                    Intent intent = new Intent("android.intent.action.VIEW");
                    intent.setData(Uri.parse("http://smarturl.it/bdoublemore"));
                    MainActivity.this.startActivity(intent);
                }
            });
            mainTabLayout.a((b) new b() {
                public void a(e eVar) {
                    MainActivity.this.q.setCurrentItem(eVar.c());
                    MainActivity.this.k().setTitle(eVar.g());
                    if (eVar.c() == 0) {
                        imageButton.setVisibility(0);
                    } else {
                        imageButton.setVisibility(4);
                    }
                    if (eVar.c() == 2) {
                        org.greenrobot.eventbus.c.a().c(new RefreshChartEvent());
                    }
                    com.crashlytics.android.a.b.c().a(new m().b("Visits Tab " + dVar.d(eVar.c())).c("Tab navigation").a("page-tab"));
                    if (MainActivity.this.n.a()) {
                        MainActivity.this.n.b();
                    }
                }

                public void b(e eVar) {
                }

                public void c(e eVar) {
                }
            });
            this.q.a((w.f) new o.f(mainTabLayout));
            if (VERSION.SDK_INT >= 23) {
                a("android.permission.READ_PHONE_STATE", 1);
            }
        }
    }

    public boolean a(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.action_rating /*2131296282*/:
                com.crashlytics.android.a.b.c().a(new m().b("Enters Google Play Store to rate").c("Page visit").a("page-store"));
                try {
                    startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=com.mansoon.BatteryDouble")));
                    return true;
                } catch (ActivityNotFoundException e) {
                    startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://play.google.com/store/apps/details?id=com.mansoon.BatteryDouble")));
                    return true;
                }
            case R.id.action_settings /*2131296283*/:
                com.crashlytics.android.a.b.c().a(new m().b("Enters settings page").c("Page visit").a("page-settings"));
                startActivity(new Intent(this, SettingsActivity.class));
                return true;
            case R.id.action_summary /*2131296286*/:
                try {
                    Intent intent = new Intent("android.intent.action.POWER_USAGE_SUMMARY");
                    if (getPackageManager().resolveActivity(intent, 0) == null) {
                        return true;
                    }
                    startActivity(intent);
                    return true;
                } catch (ActivityNotFoundException e2) {
                    e2.printStackTrace();
                    return true;
                }
            default:
                return false;
        }
    }

    public DataEstimator l() {
        return this.p.estimator;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_main);
        this.r = (AdView) findViewById(R.id.adView);
        this.r.a(new com.google.android.gms.ads.c.a().b("2F4FCE3D2D008DBE17DAF1B46F033D6B").a());
        this.n = new g(this);
        this.n.a("ca-app-pub-7372783568928829/2035331486");
        this.n.a((com.google.android.gms.ads.a) new com.google.android.gms.ads.a() {
            public void c() {
                MainActivity.this.m();
            }
        });
        m();
        com.hmatalonga.greenhub.c.c.b(o, "onCreate() called");
        n();
        Intent intent = getIntent();
        if (intent != null) {
            int intExtra = intent.getIntExtra("tab", -1);
            if (intExtra != -1) {
                this.q.setCurrentItem(intExtra);
            }
        }
        this.q.setOnDragListener(new OnDragListener() {
            public boolean onDrag(View view, DragEvent dragEvent) {
                return false;
            }
        });
        this.q.setSwipeable(false);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        Toolbar k = k();
        k.a((int) R.menu.menu_main);
        k.setOnMenuItemClickListener(this);
        return true;
    }

    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        switch (i) {
            case 1:
                a("android.permission.ACCESS_COARSE_LOCATION", 2);
                return;
            case 2:
                a("android.permission.ACCESS_FINE_LOCATION", 3);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        this.m.a();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        this.m.b();
        super.onStop();
    }
}
