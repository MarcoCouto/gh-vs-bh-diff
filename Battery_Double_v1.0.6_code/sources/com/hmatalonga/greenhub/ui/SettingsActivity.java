package com.hmatalonga.greenhub.ui;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.support.v7.app.a;
import android.support.v7.widget.Toolbar;
import com.crashlytics.android.a.b;
import com.crashlytics.android.a.n;
import com.hmatalonga.greenhub.GreenHubApp;
import com.hmatalonga.greenhub.b.d;
import com.hmatalonga.greenhub.c.c;
import com.hmatalonga.greenhub.c.e;
import com.hmatalonga.greenhub.c.g;
import com.mansoon.BatteryDouble.R;

public class SettingsActivity extends a {
    /* access modifiers changed from: private */
    public static final String m = c.a(SettingsActivity.class);

    public static class SettingsFragment extends PreferenceFragment implements OnSharedPreferenceChangeListener {
        private void a(Preference preference) {
            String string = PreferenceManager.getDefaultSharedPreferences(preference.getContext()).getString(preference.getKey(), "");
            if (preference instanceof ListPreference) {
                ListPreference listPreference = (ListPreference) preference;
                int findIndexOfValue = listPreference.findIndexOfValue(string);
                preference.setSummary(findIndexOfValue >= 0 ? listPreference.getEntries()[findIndexOfValue] : null);
            } else if (preference instanceof EditTextPreference) {
                EditTextPreference editTextPreference = (EditTextPreference) preference;
                String replaceFirst = string.replaceFirst("^0+(?!$)", "");
                editTextPreference.setText(replaceFirst);
                preference.setSummary(replaceFirst.replaceFirst("^0+(?!$)", ""));
            }
        }

        public void onCreate(Bundle bundle) {
            super.onCreate(bundle);
            addPreferencesFromResource(R.xml.preferences);
            getActivity().getApplicationContext();
            String str = "1.0.6";
            findPreference("pref_app_version").setSummary("1.0.6");
            a(findPreference("pref_data_history"));
            a(findPreference("pref_temperature_rate"));
            a(findPreference("pref_temperature_warning"));
            a(findPreference("pref_temperature_high"));
            a(findPreference("pref_notifications_priority"));
            g.a((Context) getActivity(), (OnSharedPreferenceChangeListener) this);
        }

        public void onDestroy() {
            super.onDestroy();
            g.b((Context) getActivity(), (OnSharedPreferenceChangeListener) this);
        }

        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String str) {
            Context applicationContext = getActivity().getApplicationContext();
            GreenHubApp greenHubApp = (GreenHubApp) getActivity().getApplication();
            Preference findPreference = findPreference(str);
            char c = 65535;
            switch (str.hashCode()) {
                case -1058682469:
                    if (str.equals("pref_data_history")) {
                        c = 1;
                        break;
                    }
                    break;
                case -224299966:
                    if (str.equals("pref_upload_rate")) {
                        c = 2;
                        break;
                    }
                    break;
                case 214107177:
                    if (str.equals("pref_temperature_high")) {
                        c = 5;
                        break;
                    }
                    break;
                case 214397799:
                    if (str.equals("pref_temperature_rate")) {
                        c = 6;
                        break;
                    }
                    break;
                case 538500520:
                    if (str.equals("pref_sampling_screen")) {
                        c = 0;
                        break;
                    }
                    break;
                case 649537461:
                    if (str.equals("pref_temperature_warning")) {
                        c = 4;
                        break;
                    }
                    break;
                case 991928503:
                    if (str.equals("pref_notifications_priority")) {
                        c = 7;
                        break;
                    }
                    break;
                case 1155191097:
                    if (str.equals("pref_power_indicator")) {
                        c = 3;
                        break;
                    }
                    break;
            }
            switch (c) {
                case 0:
                    c.b(SettingsActivity.m, "Restarting GreenHub Service because of preference changes");
                    greenHubApp.stopGreenHubService();
                    greenHubApp.startGreenHubService();
                    b.c().a((n) new n("Preference Change").a("Sampling on Screen", String.valueOf(g.f(applicationContext))));
                    return;
                case 1:
                    a(findPreference);
                    int g = g.g(applicationContext);
                    new d().execute(new Integer[]{Integer.valueOf(g)});
                    new com.hmatalonga.greenhub.b.c().execute(new Integer[]{Integer.valueOf(g)});
                    return;
                case 2:
                    a(findPreference);
                    return;
                case 3:
                    if (g.l(applicationContext)) {
                        e.a(applicationContext);
                        greenHubApp.startStatusBarUpdater();
                    } else {
                        e.a();
                        greenHubApp.stopStatusBarUpdater();
                    }
                    b.c().a((n) new n("Preference Change").a("Power Indicator", String.valueOf(g.l(applicationContext))));
                    return;
                case 4:
                    a(findPreference);
                    return;
                case 5:
                    a(findPreference);
                    return;
                case 6:
                    a(findPreference);
                    return;
                case 7:
                    a(findPreference);
                    return;
                default:
                    return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_settings);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        if (toolbar != null) {
            a(toolbar);
        }
        a g = g();
        if (g != null) {
            g.a(true);
        }
    }
}
