package com.hmatalonga.greenhub.ui.a;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.w;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.github.mikephil.charting.a.b.C0040b;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.d.j;
import com.github.mikephil.charting.d.k;
import com.github.mikephil.charting.e.d;
import com.hmatalonga.greenhub.c.h;
import com.hmatalonga.greenhub.models.ui.ChartCard;
import com.mansoon.BatteryDouble.R;
import java.util.ArrayList;

public class b extends android.support.v7.widget.RecyclerView.a<a> {

    /* renamed from: a reason: collision with root package name */
    private ArrayList<ChartCard> f4330a;

    /* renamed from: b reason: collision with root package name */
    private int f4331b;
    private Context c;

    static class a extends w {
        CardView n;
        TextView o;
        TextView p;
        LineChart q;
        RelativeLayout r;
        TextView s;
        TextView t;
        TextView u;

        a(View view) {
            super(view);
            this.n = (CardView) view.findViewById(R.id.cv);
            this.o = (TextView) view.findViewById(R.id.label);
            this.p = (TextView) view.findViewById(R.id.interval);
            this.q = (LineChart) view.findViewById(R.id.chart);
            this.r = (RelativeLayout) view.findViewById(R.id.extra_details);
            this.s = (TextView) view.findViewById(R.id.minValue);
            this.t = (TextView) view.findViewById(R.id.avgValue);
            this.u = (TextView) view.findViewById(R.id.maxValue);
        }
    }

    public b(ArrayList<ChartCard> arrayList, int i, Context context) {
        this.f4330a = arrayList;
        this.f4331b = i;
        this.c = context;
    }

    private j a(ChartCard chartCard) {
        k kVar = new k(chartCard.entries, null);
        kVar.a(com.github.mikephil.charting.d.k.a.LINEAR);
        kVar.a(false);
        kVar.b(false);
        kVar.b(chartCard.color);
        kVar.f(chartCard.color);
        kVar.b(1.8f);
        kVar.c(true);
        kVar.g(chartCard.color);
        return new j(kVar);
    }

    private void a(a aVar, final ChartCard chartCard) {
        AnonymousClass1 r0 = new d() {
            public String a(float f, com.github.mikephil.charting.c.a aVar) {
                return com.hmatalonga.greenhub.c.a.a(Long.valueOf((long) f));
            }
        };
        AnonymousClass2 r1 = new d() {
            public String a(float f, com.github.mikephil.charting.c.a aVar) {
                switch (chartCard.type) {
                    case 1:
                        return h.a(f);
                    case 2:
                        return h.b(f) + " ºC";
                    case 3:
                        return h.b(f) + " V";
                    default:
                        return String.valueOf(f);
                }
            }
        };
        aVar.q.getXAxis().a((d) r0);
        if (chartCard.type == 1) {
            aVar.q.getAxisLeft().b(1.0f);
        }
        aVar.q.setExtraBottomOffset(5.0f);
        aVar.q.getAxisLeft().a(false);
        aVar.q.getAxisLeft().a((d) r1);
        aVar.q.getAxisRight().a(false);
        aVar.q.getAxisRight().b(false);
        aVar.q.getXAxis().a(false);
        aVar.q.getXAxis().a(3);
        aVar.q.getXAxis().a(1.0f);
        aVar.q.getLegend().c(false);
        aVar.q.getDescription().c(false);
        aVar.q.setNoDataText(this.c.getString(R.string.chart_loading_data));
        aVar.q.setMarker(new com.hmatalonga.greenhub.ui.b.a(aVar.f1102a.getContext(), R.layout.item_marker, chartCard.type));
        aVar.q.a(600, C0040b.Linear);
    }

    public int a() {
        return this.f4330a.size();
    }

    public void a(RecyclerView recyclerView) {
        super.a(recyclerView);
    }

    public void a(a aVar, int i) {
        ChartCard chartCard = (ChartCard) this.f4330a.get(i);
        a(aVar, chartCard);
        aVar.q.setData(a(chartCard));
        aVar.q.invalidate();
        aVar.o.setText(chartCard.label);
        if (chartCard.extras != null) {
            if (chartCard.type == 2) {
                aVar.s.setText("Min: " + h.a(chartCard.extras[0]) + " ºC");
                aVar.t.setText(this.c.getString(R.string.chart_average_title) + ": " + h.a(chartCard.extras[1]) + " ºC");
                aVar.u.setText("Max: " + h.a(chartCard.extras[2]) + " ºC");
            } else if (chartCard.type == 3) {
                aVar.s.setText("Min: " + h.a(chartCard.extras[0]) + " V");
                aVar.t.setText(this.c.getString(R.string.chart_average_title) + ": " + h.a(chartCard.extras[1]) + " V");
                aVar.u.setText("Max: " + h.a(chartCard.extras[2]) + " V");
            }
            aVar.r.setVisibility(0);
        }
        if (this.f4331b == 1) {
            aVar.p.setText("Last 24h");
        } else if (this.f4331b == 2) {
            aVar.p.setText("Last 3 days");
        } else if (this.f4331b == 3) {
            aVar.p.setText("Last 5 days");
        }
    }

    public void a(ArrayList<ChartCard> arrayList) {
        if (this.f4330a != null) {
            this.f4330a.clear();
            this.f4330a.addAll(arrayList);
        } else {
            this.f4330a = arrayList;
        }
        c();
    }

    /* renamed from: c */
    public a a(ViewGroup viewGroup, int i) {
        return new a(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.chart_card_view, viewGroup, false));
    }

    public void e(int i) {
        this.f4331b = i;
    }
}
