package com.hmatalonga.greenhub.ui.a;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.w;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import com.hmatalonga.greenhub.c.h;
import com.hmatalonga.greenhub.events.OpenMessageEvent;
import com.hmatalonga.greenhub.models.data.Message;
import com.mansoon.BatteryDouble.R;
import java.util.ArrayList;

public class c extends android.support.v7.widget.RecyclerView.a<a> {

    /* renamed from: a reason: collision with root package name */
    private ArrayList<Message> f4335a;

    static class a extends w {
        public TextView n;
        public TextView o;
        public TextView p;

        a(View view) {
            super(view);
            this.n = (TextView) view.findViewById(R.id.message_title);
            this.o = (TextView) view.findViewById(R.id.message_body);
            this.p = (TextView) view.findViewById(R.id.message_date);
        }
    }

    public c(ArrayList<Message> arrayList) {
        this.f4335a = arrayList;
    }

    public int a() {
        return this.f4335a.size();
    }

    public void a(RecyclerView recyclerView) {
        super.a(recyclerView);
    }

    public void a(final a aVar, int i) {
        aVar.n.setText(h.a(((Message) this.f4335a.get(i)).realmGet$title(), 25));
        aVar.o.setText(h.a(((Message) this.f4335a.get(i)).realmGet$body(), 30));
        aVar.p.setText(((Message) this.f4335a.get(i)).realmGet$date().substring(0, 10));
        if (!((Message) this.f4335a.get(i)).realmGet$read()) {
            aVar.n.setTypeface(null, 1);
        }
        aVar.f1102a.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                org.greenrobot.eventbus.c.a().c(new OpenMessageEvent(aVar.e()));
            }
        });
    }

    public void a(ArrayList<Message> arrayList) {
        if (this.f4335a != null) {
            this.f4335a.clear();
            this.f4335a.addAll(arrayList);
        } else {
            this.f4335a = arrayList;
        }
        c();
    }

    /* renamed from: c */
    public a a(ViewGroup viewGroup, int i) {
        return new a(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.message_item_view, viewGroup, false));
    }
}
