package com.hmatalonga.greenhub.ui.a;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Parcelable;
import android.support.d.a.b;
import android.util.SparseArray;
import android.view.ViewGroup;
import com.hmatalonga.greenhub.a.a;
import com.hmatalonga.greenhub.a.c;

public class d extends b {

    /* renamed from: a reason: collision with root package name */
    private final SparseArray<Fragment> f4338a = new SparseArray<>(3);

    public d(FragmentManager fragmentManager) {
        super(fragmentManager);
    }

    public Fragment a(int i) {
        switch (i) {
            case 0:
                return com.hmatalonga.greenhub.a.b.a();
            case 1:
                return a.a();
            case 2:
                return c.a();
            default:
                return null;
        }
    }

    public Object a(ViewGroup viewGroup, int i) {
        Object a2 = super.a(viewGroup, i);
        if (a2 instanceof Fragment) {
            this.f4338a.put(i, (Fragment) a2);
        }
        return a2;
    }

    public void a(Parcelable parcelable, ClassLoader classLoader) {
        try {
            super.a(parcelable, classLoader);
        } catch (IllegalStateException e) {
        }
    }

    public void a(ViewGroup viewGroup, int i, Object obj) {
        this.f4338a.remove(i);
        super.a(viewGroup, i, obj);
    }

    public int b() {
        return 3;
    }

    public String d(int i) {
        switch (i) {
            case 0:
                return "Home";
            case 1:
                return "My Device";
            case 2:
                return "Statistics";
            default:
                return "default";
        }
    }
}
