package com.hmatalonga.greenhub.ui.a;

import android.content.Context;
import android.os.Handler;
import android.support.v7.widget.RecyclerView.w;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.hmatalonga.greenhub.events.OpenTaskDetailsEvent;
import com.hmatalonga.greenhub.events.TaskRemovedEvent;
import com.hmatalonga.greenhub.models.ui.Task;
import com.mansoon.BatteryDouble.R;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.greenrobot.eventbus.c;

public class e extends android.support.v7.widget.RecyclerView.a<a> {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public List<Task> f4339a;
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public List<Task> f4340b;
    private boolean c;
    /* access modifiers changed from: private */
    public Handler d;
    /* access modifiers changed from: private */
    public HashMap<Task, Runnable> e = new HashMap<>();
    private com.hmatalonga.greenhub.managers.a f;

    static class a extends w {
        TextView n;
        TextView o;
        RelativeLayout p;
        TextView q;
        TextView r;
        TextView s;
        TextView t;
        ImageView u;
        ImageView v;
        Button w;
        CheckBox x;

        a(View view) {
            super(view);
            this.n = (TextView) view.findViewById(R.id.taskName);
            this.o = (TextView) view.findViewById(R.id.taskMemory);
            this.p = (RelativeLayout) view.findViewById(R.id.taskDetailsContainer);
            this.q = (TextView) view.findViewById(R.id.taskAutoStart);
            this.r = (TextView) view.findViewById(R.id.taskBackgroundService);
            this.s = (TextView) view.findViewById(R.id.taskPackage);
            this.t = (TextView) view.findViewById(R.id.taskAppVersion);
            this.u = (ImageView) view.findViewById(R.id.taskIcon);
            this.v = (ImageView) view.findViewById(R.id.taskShowDetails);
            this.w = (Button) view.findViewById(R.id.undo_button);
            this.x = (CheckBox) view.findViewById(R.id.checkBox);
        }
    }

    public e(Context context, List<Task> list) {
        this.f4339a = list;
        this.f = new com.hmatalonga.greenhub.managers.a(context);
        this.f4340b = new ArrayList();
        this.c = true;
        this.d = new Handler();
    }

    private void e() {
        if (this.f4339a != null) {
            this.f4339a.clear();
        }
        if (this.f4340b != null) {
            this.f4340b.clear();
        }
        this.e.clear();
    }

    public int a() {
        return this.f4339a.size();
    }

    public void a(final a aVar, int i) {
        final Task task = (Task) this.f4339a.get(i);
        if (this.f4340b.contains(task)) {
            aVar.f1102a.setBackgroundColor(-12303292);
            aVar.f1102a.setOnClickListener(null);
            aVar.f1102a.setOnLongClickListener(null);
            aVar.n.setVisibility(8);
            aVar.o.setVisibility(8);
            aVar.p.setVisibility(8);
            aVar.q.setVisibility(8);
            aVar.r.setVisibility(8);
            aVar.s.setVisibility(8);
            aVar.t.setVisibility(8);
            aVar.u.setVisibility(8);
            aVar.v.setVisibility(8);
            aVar.x.setVisibility(8);
            aVar.x.setOnCheckedChangeListener(null);
            aVar.w.setVisibility(0);
            aVar.w.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    Runnable runnable = (Runnable) e.this.e.get(task);
                    e.this.e.remove(task);
                    if (runnable != null) {
                        e.this.d.removeCallbacks(runnable);
                    }
                    e.this.f4340b.remove(task);
                    e.this.c(e.this.f4339a.indexOf(task));
                }
            });
            return;
        }
        aVar.f1102a.setBackgroundColor(0);
        aVar.f1102a.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                if (aVar.p.getVisibility() == 8) {
                    aVar.v.setImageResource(R.drawable.ic_chevron_up_grey600_18dp);
                    aVar.p.setVisibility(0);
                } else if (aVar.p.getVisibility() == 0) {
                    aVar.v.setImageResource(R.drawable.ic_chevron_down_grey600_18dp);
                    aVar.p.setVisibility(8);
                }
            }
        });
        aVar.f1102a.setOnLongClickListener(new OnLongClickListener() {
            public boolean onLongClick(View view) {
                c.a().c(new OpenTaskDetailsEvent(task));
                return true;
            }
        });
        aVar.n.setText(task.getLabel());
        aVar.o.setText(task.getMemory() > 0.0d ? task.getMemory() + " MB" : "N/A");
        aVar.u.setImageDrawable(this.f.a(task.getPackageInfo()));
        if (task.isAutoStart()) {
            aVar.q.setVisibility(0);
        }
        if (task.hasBackgroundService()) {
            aVar.r.setVisibility(0);
        }
        aVar.s.setText("Package: " + task.getPackageInfo().packageName);
        aVar.t.setText("Version: " + (task.getPackageInfo().versionName == null ? "Not available" : task.getPackageInfo().versionName));
        aVar.n.setVisibility(0);
        aVar.o.setVisibility(0);
        aVar.s.setVisibility(0);
        aVar.t.setVisibility(0);
        aVar.u.setVisibility(0);
        aVar.v.setVisibility(0);
        aVar.x.setVisibility(0);
        aVar.x.setChecked(task.isChecked());
        aVar.x.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                task.setIsChecked(z);
            }
        });
        aVar.w.setVisibility(8);
        aVar.w.setOnClickListener(null);
    }

    public void a(List<Task> list) {
        e();
        if (this.f4339a != null) {
            this.f4339a.addAll(list);
        } else {
            this.f4339a = list;
        }
        c();
    }

    /* renamed from: c */
    public a a(ViewGroup viewGroup, int i) {
        return new a(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_task, viewGroup, false));
    }

    public boolean d() {
        return this.c;
    }

    public void e(int i) {
        final Task task = (Task) this.f4339a.get(i);
        if (!this.f4340b.contains(task)) {
            this.f4340b.add(task);
            c(i);
            AnonymousClass5 r1 = new Runnable() {
                public void run() {
                    e.this.f(e.this.f4339a.indexOf(task));
                }
            };
            this.d.postDelayed(r1, 1500);
            this.e.put(task, r1);
        }
    }

    public void f(int i) {
        Task task = (Task) this.f4339a.get(i);
        if (this.f4340b.contains(task)) {
            this.f4340b.remove(task);
        }
        if (this.f4339a.contains(task)) {
            this.f4339a.remove(i);
            d(i);
            this.f.a(task);
            c.a().c(new TaskRemovedEvent(i, task));
        }
    }

    public boolean g(int i) {
        Task task;
        try {
            task = (Task) this.f4339a.get(i);
        } catch (ArrayIndexOutOfBoundsException e2) {
            task = null;
        }
        return this.f4340b.contains(task);
    }
}
