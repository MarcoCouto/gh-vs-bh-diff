package com.hmatalonga.greenhub.ui.a;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.w;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.hmatalonga.greenhub.models.ui.BatteryCard;
import com.mansoon.BatteryDouble.R;
import java.util.ArrayList;

public class a extends android.support.v7.widget.RecyclerView.a<C0070a> {

    /* renamed from: a reason: collision with root package name */
    private ArrayList<BatteryCard> f4329a;

    /* renamed from: com.hmatalonga.greenhub.ui.a.a$a reason: collision with other inner class name */
    static class C0070a extends w {
        CardView n;
        public ImageView o;
        public TextView p;
        public TextView q;
        public View r;

        C0070a(View view) {
            super(view);
            this.n = (CardView) view.findViewById(R.id.cv);
            this.o = (ImageView) view.findViewById(R.id.icon);
            this.p = (TextView) view.findViewById(R.id.label);
            this.q = (TextView) view.findViewById(R.id.value);
            this.r = view.findViewById(R.id.indicator);
        }
    }

    public a(ArrayList<BatteryCard> arrayList) {
        this.f4329a = arrayList;
    }

    public int a() {
        return this.f4329a.size();
    }

    public void a(RecyclerView recyclerView) {
        super.a(recyclerView);
    }

    public void a(C0070a aVar, int i) {
        aVar.o.setImageResource(((BatteryCard) this.f4329a.get(i)).icon);
        aVar.p.setText(((BatteryCard) this.f4329a.get(i)).label);
        aVar.q.setText(((BatteryCard) this.f4329a.get(i)).value);
        aVar.r.setBackgroundColor(((BatteryCard) this.f4329a.get(i)).indicator);
    }

    public void a(ArrayList<BatteryCard> arrayList) {
        if (this.f4329a != null) {
            this.f4329a.clear();
            this.f4329a.addAll(arrayList);
        } else {
            this.f4329a = arrayList;
        }
        c();
    }

    /* renamed from: c */
    public C0070a a(ViewGroup viewGroup, int i) {
        return new C0070a(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.battery_item_card_view, viewGroup, false));
    }
}
