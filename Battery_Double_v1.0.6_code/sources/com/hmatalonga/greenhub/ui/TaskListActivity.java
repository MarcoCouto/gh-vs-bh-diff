package com.hmatalonga.greenhub.ui;

import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.app.ActivityManager.MemoryInfo;
import android.app.AppOpsManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Process;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.b;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.g;
import android.support.v7.widget.RecyclerView.t;
import android.support.v7.widget.RecyclerView.w;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.a.a.d;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.hmatalonga.greenhub.events.OpenTaskDetailsEvent;
import com.hmatalonga.greenhub.events.TaskRemovedEvent;
import com.hmatalonga.greenhub.models.ui.Task;
import com.hmatalonga.greenhub.ui.a.e;
import com.mansoon.BatteryDouble.R;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import org.greenrobot.eventbus.ThreadMode;
import org.greenrobot.eventbus.c;
import org.greenrobot.eventbus.j;

public class TaskListActivity extends a {
    /* access modifiers changed from: private */
    public ArrayList<Task> m;
    /* access modifiers changed from: private */
    public RecyclerView n;
    private e o;
    private SwipeRefreshLayout p;
    /* access modifiers changed from: private */
    public ProgressBar q;
    private Task r;
    private long s;
    /* access modifiers changed from: private */
    public boolean t;
    private int u;
    private int v;

    private class a extends AsyncTask<Context, Void, List<Task>> {
        private a() {
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public List<Task> doInBackground(Context... contextArr) {
            return new com.hmatalonga.greenhub.managers.a(contextArr[0]).a();
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void onPostExecute(List<Task> list) {
            super.onPostExecute(list);
            TaskListActivity.this.a(list);
            TaskListActivity.this.u();
        }
    }

    /* access modifiers changed from: private */
    public String a(int i, double d) {
        return getString(R.string.task_killed) + " " + i + " apps! " + getString(R.string.task_cleared) + " " + d + " MB";
    }

    private void a(int i, final int i2) {
        if (i == 1) {
            Collections.sort(this.m, new Comparator<Task>() {
                /* renamed from: a */
                public int compare(Task task, Task task2) {
                    int i = task.getMemory() < task2.getMemory() ? -1 : task.getMemory() == task2.getMemory() ? 0 : 1;
                    return i * i2;
                }
            });
        } else if (i == 2) {
            Collections.sort(this.m, new Comparator<Task>() {
                /* renamed from: a */
                public int compare(Task task, Task task2) {
                    return i2 * task.getLabel().compareTo(task2.getLabel());
                }
            });
        }
        this.o.c();
    }

    /* access modifiers changed from: private */
    public void a(List<Task> list) {
        if (this.q.getVisibility() == 0) {
            this.q.setVisibility(8);
            this.n.setVisibility(0);
        }
        this.o.a(list);
        this.t = false;
        r();
        this.p.setRefreshing(false);
    }

    @TargetApi(21)
    private boolean a(Context context) {
        return ((AppOpsManager) context.getSystemService("appops")).checkOpNoThrow("android:get_usage_stats", Process.myUid(), context.getPackageName()) == 0;
    }

    private boolean a(String str) {
        if (this.s < System.currentTimeMillis() - 15000) {
            this.r = null;
            return false;
        }
        Iterator it = this.m.iterator();
        while (it.hasNext()) {
            if (((Task) it.next()).getLabel().equals(str)) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: private */
    public String c(int i) {
        return getString(R.string.task_killed) + " " + i + " apps!";
    }

    private void l() {
        a((Toolbar) findViewById(R.id.toolbar_actionbar));
        this.q = (ProgressBar) findViewById(R.id.loader);
        this.r = null;
        this.u = 1;
        this.v = 1;
        ((FloatingActionButton) findViewById(R.id.fab)).setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                if (TaskListActivity.this.m.isEmpty()) {
                    Snackbar.a(view, TaskListActivity.this.getString(R.string.task_no_apps_running), 0).a();
                    return;
                }
                com.hmatalonga.greenhub.managers.a aVar = new com.hmatalonga.greenhub.managers.a(TaskListActivity.this.getApplicationContext());
                Iterator it = TaskListActivity.this.m.iterator();
                double d = 0.0d;
                int i = 0;
                while (it.hasNext()) {
                    Task task = (Task) it.next();
                    if (task.isChecked()) {
                        aVar.a(task);
                        i++;
                        d = task.getMemory() + d;
                    }
                }
                double round = ((double) Math.round(d * 100.0d)) / 100.0d;
                TaskListActivity.this.n.setVisibility(8);
                TaskListActivity.this.q.setVisibility(0);
                TaskListActivity.this.q();
                String string = VERSION.SDK_INT >= 24 ? i > 0 ? TaskListActivity.this.c(i) : TaskListActivity.this.getString(R.string.task_no_apps_killed) : i > 0 ? TaskListActivity.this.a(i, round) : TaskListActivity.this.getString(R.string.task_no_apps_killed);
                Snackbar.a(view, string, 0).a();
            }
        });
        if (VERSION.SDK_INT >= 24 && !a(getApplicationContext())) {
            v();
        }
        this.m = new ArrayList<>();
        this.t = false;
        n();
        m();
    }

    private void m() {
        this.n = (RecyclerView) findViewById(R.id.rv);
        this.n.setHasFixedSize(true);
        this.n.setLayoutManager(new LinearLayoutManager(this));
        this.o = new e(getApplicationContext(), this.m);
        this.n.setAdapter(this.o);
        o();
        p();
    }

    private void n() {
        this.p = (SwipeRefreshLayout) findViewById(R.id.swipe_layout);
        if (VERSION.SDK_INT >= 23) {
            this.p.setColorSchemeColors(getColor(R.color.color_accent), getColor(R.color.color_primary_dark));
        } else {
            Context applicationContext = getApplicationContext();
            this.p.setColorSchemeColors(android.support.v4.b.a.c(applicationContext, R.color.color_accent), android.support.v4.b.a.c(applicationContext, R.color.color_primary_dark));
        }
        this.p.setOnRefreshListener(new b() {
            public void a() {
                if (!TaskListActivity.this.t) {
                    TaskListActivity.this.q();
                }
            }
        });
    }

    private void o() {
        new android.support.v7.widget.a.a(new d(0, 4) {

            /* renamed from: a reason: collision with root package name */
            Drawable f4321a;

            /* renamed from: b reason: collision with root package name */
            Drawable f4322b;
            int c;
            boolean d;

            private void d() {
                this.f4321a = new ColorDrawable(-12303292);
                this.f4322b = android.support.v4.b.a.a((Context) TaskListActivity.this, (int) R.drawable.ic_delete_white_24dp);
                this.f4322b.setColorFilter(-1, Mode.SRC_ATOP);
                this.c = (int) TaskListActivity.this.getResources().getDimension(R.dimen.fab_margin);
                this.d = true;
            }

            public void a(Canvas canvas, RecyclerView recyclerView, w wVar, float f, float f2, int i, boolean z) {
                View view = wVar.f1102a;
                if (wVar.e() != -1) {
                    if (!this.d) {
                        d();
                    }
                    this.f4321a.setBounds(view.getRight() + ((int) f), view.getTop(), view.getRight(), view.getBottom());
                    this.f4321a.draw(canvas);
                    int bottom = view.getBottom() - view.getTop();
                    int intrinsicWidth = this.f4322b.getIntrinsicWidth();
                    int intrinsicWidth2 = this.f4322b.getIntrinsicWidth();
                    int right = (view.getRight() - this.c) - intrinsicWidth;
                    int right2 = view.getRight() - this.c;
                    int top = view.getTop() + ((bottom - intrinsicWidth2) / 2);
                    this.f4322b.setBounds(right, top, right2, top + intrinsicWidth2);
                    this.f4322b.draw(canvas);
                    super.a(canvas, recyclerView, wVar, f, f2, i, z);
                }
            }

            public void a(w wVar, int i) {
                int e2 = wVar.e();
                e eVar = (e) TaskListActivity.this.n.getAdapter();
                if (eVar.d()) {
                    eVar.e(e2);
                } else {
                    eVar.f(e2);
                }
            }

            public boolean b(RecyclerView recyclerView, w wVar, w wVar2) {
                return false;
            }

            public int e(RecyclerView recyclerView, w wVar) {
                int e2 = wVar.e();
                e eVar = (e) recyclerView.getAdapter();
                if (!eVar.d() || !eVar.g(e2)) {
                    return super.e(recyclerView, wVar);
                }
                return 0;
            }
        }).a(this.n);
    }

    private void p() {
        this.n.a((g) new g() {

            /* renamed from: a reason: collision with root package name */
            Drawable f4323a;

            /* renamed from: b reason: collision with root package name */
            boolean f4324b;

            private void a() {
                this.f4323a = new ColorDrawable(-12303292);
                this.f4324b = true;
            }

            public void b(Canvas canvas, RecyclerView recyclerView, t tVar) {
                int i;
                int i2;
                View view = null;
                if (!this.f4324b) {
                    a();
                }
                if (recyclerView.getItemAnimator().b()) {
                    int width = recyclerView.getWidth();
                    int u = recyclerView.getLayoutManager().u();
                    int i3 = 0;
                    View view2 = null;
                    while (i3 < u) {
                        View h = recyclerView.getLayoutManager().h(i3);
                        if (h.getTranslationY() >= 0.0f) {
                            if (h.getTranslationY() <= 0.0f || view != null) {
                                h = view2;
                            } else {
                                view = h;
                                h = view2;
                            }
                        }
                        i3++;
                        view2 = h;
                    }
                    if (view2 != null && view != null) {
                        i2 = view2.getBottom() + ((int) view2.getTranslationY());
                        i = ((int) view.getTranslationY()) + view.getTop();
                    } else if (view2 != null) {
                        i2 = ((int) view2.getTranslationY()) + view2.getBottom();
                        i = view2.getBottom();
                    } else if (view != null) {
                        i2 = view.getTop();
                        i = ((int) view.getTranslationY()) + view.getTop();
                    } else {
                        i = 0;
                        i2 = 0;
                    }
                    this.f4323a.setBounds(0, i2, width, i);
                    this.f4323a.draw(canvas);
                }
                super.b(canvas, recyclerView, tVar);
            }
        });
    }

    /* access modifiers changed from: private */
    public void q() {
        this.t = true;
        s();
        new a().execute(new Context[]{getApplicationContext()});
    }

    private void r() {
        ((TextView) findViewById(R.id.count)).setText("Apps " + this.m.size());
        TextView textView = (TextView) findViewById(R.id.usage);
        double t2 = t();
        textView.setText(t2 > 1000.0d ? getString(R.string.task_free_ram) + " " + (((double) Math.round((t2 / 1024.0d) * 100.0d)) / 100.0d) + " GB" : getString(R.string.task_free_ram) + " " + t2 + " MB");
    }

    private void s() {
        ((TextView) findViewById(R.id.count)).setText(getString(R.string.header_status_loading));
        ((TextView) findViewById(R.id.usage)).setText("");
    }

    private double t() {
        MemoryInfo memoryInfo = new MemoryInfo();
        ((ActivityManager) getApplicationContext().getSystemService("activity")).getMemoryInfo(memoryInfo);
        return ((double) Math.round((((double) memoryInfo.availMem) / 1048576.0d) * 100.0d)) / 100.0d;
    }

    /* access modifiers changed from: private */
    public void u() {
        if (this.r != null && a(this.r.getLabel())) {
            final String str = this.r.getPackageInfo().packageName;
            android.support.v7.app.b.a aVar = new android.support.v7.app.b.a(this);
            aVar.b((CharSequence) getString(R.string.kill_app_dialog_text)).a((CharSequence) this.r.getLabel());
            aVar.a((int) R.string.force_close, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    TaskListActivity.this.startActivity(new Intent("android.settings.APPLICATION_DETAILS_SETTINGS", Uri.parse("package:" + str)));
                }
            });
            aVar.b(R.string.cancel, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.cancel();
                }
            });
            aVar.b().show();
        }
        this.r = null;
    }

    @TargetApi(21)
    private void v() {
        android.support.v7.app.b.a aVar = new android.support.v7.app.b.a(this);
        aVar.b((CharSequence) getString(R.string.package_usage_permission_text)).a((CharSequence) getString(R.string.package_usage_permission_title));
        aVar.a((int) R.string.open_settings, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                TaskListActivity.this.startActivity(new Intent("android.settings.USAGE_ACCESS_SETTINGS"));
            }
        });
        aVar.b(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        aVar.b().show();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (!com.hmatalonga.greenhub.c.g.a(getApplicationContext())) {
            startActivity(new Intent(this, WelcomeActivity.class));
            finish();
            return;
        }
        setContentView(R.layout.activity_task_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        if (toolbar != null) {
            a(toolbar);
        }
        android.support.v7.app.a g = g();
        if (g != null) {
            g.a(true);
        }
        l();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_task_list, menu);
        return true;
    }

    @j(a = ThreadMode.MAIN)
    public void onOpenTaskDetailsEvent(OpenTaskDetailsEvent openTaskDetailsEvent) {
        startActivity(new Intent("android.settings.APPLICATION_DETAILS_SETTINGS", Uri.parse("package:" + openTaskDetailsEvent.task.getPackageInfo().packageName)));
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        int itemId = menuItem.getItemId();
        if (itemId == R.id.action_settings) {
            startActivity(new Intent(this, SettingsActivity.class));
            return true;
        } else if (itemId == R.id.action_sort_memory) {
            a(1, this.v);
            this.v = -this.v;
            return true;
        } else if (itemId != R.id.action_sort_name) {
            return super.onOptionsItemSelected(menuItem);
        } else {
            a(2, this.u);
            this.u = -this.u;
            return true;
        }
    }

    public void onResume() {
        super.onResume();
        if (!this.t) {
            q();
        }
    }

    public void onStart() {
        super.onStart();
        c.a().a((Object) this);
    }

    public void onStop() {
        c.a().b(this);
        super.onStop();
    }

    @j(a = ThreadMode.MAIN)
    public void onTaskRemovedEvent(TaskRemovedEvent taskRemovedEvent) {
        r();
        this.r = taskRemovedEvent.task;
        this.s = System.currentTimeMillis();
    }
}
