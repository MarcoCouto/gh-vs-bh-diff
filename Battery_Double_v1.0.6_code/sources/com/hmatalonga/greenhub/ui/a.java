package com.hmatalonga.greenhub.ui;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.c;
import android.support.v7.widget.Toolbar;
import com.hmatalonga.greenhub.c.g;
import com.mansoon.BatteryDouble.R;

public abstract class a extends c implements OnSharedPreferenceChangeListener {
    private static final String m = com.hmatalonga.greenhub.c.c.a(a.class);
    private Toolbar n;

    /* access modifiers changed from: protected */
    public Toolbar k() {
        if (this.n == null) {
            this.n = (Toolbar) findViewById(R.id.toolbar_actionbar);
            if (this.n != null) {
                a(this.n);
            }
        }
        return this.n;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        g.a((Context) this, true);
        if (WelcomeActivity.a(this)) {
            startActivity(new Intent(this, WelcomeActivity.class));
            finish();
            return;
        }
        PreferenceManager.getDefaultSharedPreferences(this).registerOnSharedPreferenceChangeListener(this);
        android.support.v7.app.a g = g();
        if (g != null) {
            g.a(true);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        PreferenceManager.getDefaultSharedPreferences(this).unregisterOnSharedPreferenceChangeListener(this);
    }

    /* access modifiers changed from: protected */
    public void onPostCreate(Bundle bundle) {
        super.onPostCreate(bundle);
    }

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String str) {
    }

    public void setContentView(int i) {
        super.setContentView(i);
        k();
    }
}
