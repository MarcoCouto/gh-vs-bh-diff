package com.hmatalonga.greenhub.ui;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.c;
import android.widget.Button;
import com.hmatalonga.greenhub.a.d;
import com.mansoon.BatteryDouble.R;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class WelcomeActivity extends c implements com.hmatalonga.greenhub.a.e.a {
    private static final String n = com.hmatalonga.greenhub.c.c.a(WelcomeActivity.class);
    a m;

    public interface a {
        boolean a(Context context);
    }

    public static boolean a(Context context) {
        return b(context) != null;
    }

    private static a b(Context context) {
        for (a aVar : k()) {
            if (aVar.a(context)) {
                return aVar;
            }
        }
        return null;
    }

    private static List<a> k() {
        return new ArrayList(Collections.singletonList(new d()));
    }

    public Button b() {
        return (Button) findViewById(R.id.button_decline);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_welcome);
        this.m = b(this);
        if (this.m == null) {
            finish();
        }
        FragmentTransaction beginTransaction = getFragmentManager().beginTransaction();
        beginTransaction.add(R.id.welcome_content, (Fragment) this.m);
        beginTransaction.commit();
        com.hmatalonga.greenhub.c.c.a(n, "Inside Create View.");
    }

    public Button x_() {
        return (Button) findViewById(R.id.button_accept);
    }
}
