package com.hmatalonga.greenhub.ui.layouts;

import android.content.Context;
import android.support.design.widget.o;
import android.util.AttributeSet;
import com.mansoon.BatteryDouble.R;

public class MainTabLayout extends o {
    public MainTabLayout(Context context) {
        super(context);
    }

    public MainTabLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public MainTabLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    private void a(int i, int i2) {
        a(a().c(i).d(i2));
    }

    public void d() {
        a(R.drawable.ic_home_white_24dp, R.string.title_fragment_home);
        a(R.drawable.ic_cellphone_android_white_24dp, R.string.title_fragment_device);
        a(R.drawable.ic_chart_areaspline_white_24dp, R.string.title_fragment_stats);
    }
}
