package com.hmatalonga.greenhub.ui.b;

import android.content.Context;
import android.widget.TextView;
import com.github.mikephil.charting.c.h;
import com.github.mikephil.charting.d.i;
import com.github.mikephil.charting.f.c;
import com.github.mikephil.charting.j.e;
import com.mansoon.BatteryDouble.R;

public class a extends h {

    /* renamed from: a reason: collision with root package name */
    private TextView f4351a = ((TextView) findViewById(R.id.tvContent));

    /* renamed from: b reason: collision with root package name */
    private e f4352b;
    private int c;

    public a(Context context, int i, int i2) {
        super(context, i);
        this.c = i2;
    }

    public void a(i iVar, c cVar) {
        String str = "";
        switch (this.c) {
            case 1:
                str = com.hmatalonga.greenhub.c.h.a(iVar.b());
                break;
            case 2:
                str = com.hmatalonga.greenhub.c.h.b(iVar.b()) + " ºC";
                break;
            case 3:
                str = com.hmatalonga.greenhub.c.h.b(iVar.b()) + " V";
                break;
        }
        this.f4351a.setText(str);
        super.a(iVar, cVar);
    }

    public e getOffset() {
        if (this.f4352b == null) {
            this.f4352b = new e((float) (-(getWidth() / 2)), (float) (-getHeight()));
        }
        return this.f4352b;
    }
}
