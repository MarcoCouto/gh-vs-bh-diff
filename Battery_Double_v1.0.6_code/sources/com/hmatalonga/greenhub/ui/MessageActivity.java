package com.hmatalonga.greenhub.ui;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.a;
import android.support.v7.app.b;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import com.mansoon.BatteryDouble.R;

public class MessageActivity extends a {
    /* access modifiers changed from: private */
    public int m;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_message);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        if (toolbar != null) {
            a(toolbar);
        }
        a g = g();
        if (g != null) {
            g.a(true);
        }
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.m = extras.getInt("id");
            ((TextView) findViewById(R.id.message_title)).setText(extras.getString("title"));
            ((TextView) findViewById(R.id.message_body)).setText(extras.getString("body"));
            TextView textView = (TextView) findViewById(R.id.message_date);
            try {
                String string = extras.getString("date");
                if (string != null) {
                    string = string.substring(0, 16);
                }
                if (string == null) {
                    string = getString(R.string.not_available);
                }
                textView.setText(string);
            } catch (NullPointerException e) {
                textView.setText(getString(R.string.not_available));
                e.printStackTrace();
            }
            FloatingActionButton floatingActionButton = (FloatingActionButton) findViewById(R.id.fabDeleteMessage);
            if (floatingActionButton != null) {
                floatingActionButton.setOnClickListener(new OnClickListener() {
                    public void onClick(View view) {
                        b.a aVar = new b.a(view.getContext());
                        aVar.b((int) R.string.dialog_message_text).a((int) R.string.dialog_message_title);
                        aVar.a((int) R.string.ok, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogInterface, int i) {
                                com.hmatalonga.greenhub.managers.a.a aVar = new com.hmatalonga.greenhub.managers.a.a();
                                aVar.b(MessageActivity.this.m);
                                aVar.b();
                                dialogInterface.dismiss();
                                MessageActivity.this.finish();
                            }
                        });
                        aVar.b(R.string.cancel, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                        aVar.b().show();
                    }
                });
            }
        }
    }
}
