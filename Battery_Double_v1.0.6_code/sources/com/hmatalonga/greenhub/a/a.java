package com.hmatalonga.greenhub.a;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.crashlytics.android.a.b;
import com.crashlytics.android.a.m;
import com.hmatalonga.greenhub.events.RefreshEvent;
import com.hmatalonga.greenhub.models.Bluetooth;
import com.hmatalonga.greenhub.models.Memory;
import com.hmatalonga.greenhub.models.Network;
import com.hmatalonga.greenhub.models.Phone;
import com.hmatalonga.greenhub.models.Specifications;
import com.hmatalonga.greenhub.models.Storage;
import com.hmatalonga.greenhub.models.Wifi;
import com.hmatalonga.greenhub.models.data.StorageDetails;
import com.hmatalonga.greenhub.ui.TaskListActivity;
import com.mansoon.BatteryDouble.R;
import org.greenrobot.eventbus.ThreadMode;
import org.greenrobot.eventbus.c;
import org.greenrobot.eventbus.j;

public class a extends Fragment {

    /* renamed from: a reason: collision with root package name */
    private Context f4240a = null;

    /* renamed from: b reason: collision with root package name */
    private View f4241b = null;
    /* access modifiers changed from: private */
    public Handler c;
    /* access modifiers changed from: private */
    public ProgressBar d;
    /* access modifiers changed from: private */
    public TextView e;
    /* access modifiers changed from: private */
    public TextView f;
    /* access modifiers changed from: private */
    public ProgressBar g;
    /* access modifiers changed from: private */
    public TextView h;
    /* access modifiers changed from: private */
    public TextView i;
    private Runnable j = new Runnable() {
        public void run() {
            int[] readMemoryInfo = Memory.readMemoryInfo();
            StorageDetails storageDetails = Storage.getStorageDetails();
            a.this.d.setMax(readMemoryInfo[1]);
            a.this.d.setProgress(readMemoryInfo[1] - readMemoryInfo[2]);
            a.this.e.setText(((readMemoryInfo[1] - readMemoryInfo[2]) / 1024) + " MB");
            a.this.f.setText((readMemoryInfo[2] / 1024) + " MB");
            a.this.g.setMax(storageDetails.realmGet$total());
            a.this.g.setProgress(storageDetails.realmGet$total() - storageDetails.realmGet$free());
            a.this.h.setText((storageDetails.realmGet$total() - storageDetails.realmGet$free()) + " MB");
            a.this.i.setText(storageDetails.realmGet$free() + " MB");
            a.this.c.postDelayed(this, 10000);
        }
    };

    public static a a() {
        return new a();
    }

    private void a(View view) {
        StringBuilder sb = new StringBuilder();
        sb.append(Specifications.getBrand());
        sb.append(" ");
        sb.append(Specifications.getModel());
        ((TextView) view.findViewById(R.id.androidVersion)).setText(Specifications.getOsVersion());
        ((TextView) view.findViewById(R.id.androidModel)).setText(sb);
        TextView textView = (TextView) view.findViewById(R.id.androidImei);
        String deviceId = Phone.getDeviceId(this.f4240a);
        if (deviceId == null) {
            deviceId = getString(R.string.not_available);
        }
        textView.setText(deviceId);
        ((TextView) view.findViewById(R.id.androidRoot)).setText(Specifications.isRooted() ? getString(R.string.yes) : getString(R.string.no));
        a(view, Wifi.isEnabled(this.f4240a));
        b(view, Bluetooth.isEnabled());
        c(view, Network.isMobileDataEnabled(this.f4240a));
        this.d = (ProgressBar) view.findViewById(R.id.memoryBar);
        this.e = (TextView) view.findViewById(R.id.memoryUsed);
        this.f = (TextView) view.findViewById(R.id.memoryFree);
        ((Button) view.findViewById(R.id.buttonViewMore)).setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                b.c().a(new m().b("Enters Task Manager").c("Page visit").a("page-task-manager"));
                a.this.startActivity(new Intent(view.getContext(), TaskListActivity.class));
            }
        });
        this.g = (ProgressBar) view.findViewById(R.id.storageBar);
        this.h = (TextView) view.findViewById(R.id.storageUsed);
        this.i = (TextView) view.findViewById(R.id.storageFree);
        this.c.post(this.j);
    }

    private void a(View view, boolean z) {
        int i2 = 0;
        ((TextView) view.findViewById(R.id.wifi)).setText(z ? getString(R.string.yes) : getString(R.string.no));
        ((TextView) view.findViewById(R.id.ipAddressLabel)).setVisibility(z ? 0 : 8);
        TextView textView = (TextView) view.findViewById(R.id.ipAddress);
        if (z) {
            textView.setText(Wifi.getIpAddress(this.f4240a));
        }
        textView.setVisibility(z ? 0 : 8);
        ((TextView) view.findViewById(R.id.macAddressLabel)).setVisibility(z ? 0 : 8);
        TextView textView2 = (TextView) view.findViewById(R.id.macAddress);
        if (z) {
            textView2.setText(Wifi.getMacAddress(this.f4240a));
        }
        textView2.setVisibility(z ? 0 : 8);
        ((TextView) view.findViewById(R.id.ssidLabel)).setVisibility(z ? 0 : 8);
        TextView textView3 = (TextView) view.findViewById(R.id.ssid);
        if (z) {
            textView3.setText(Wifi.getInfo(this.f4240a).getSSID());
        }
        if (!z) {
            i2 = 8;
        }
        textView3.setVisibility(i2);
    }

    private void b(View view, boolean z) {
        int i2 = 0;
        ((TextView) view.findViewById(R.id.bluetooth)).setText(z ? getString(R.string.yes) : getString(R.string.no));
        TextView textView = (TextView) view.findViewById(R.id.bluetoothAddress);
        if (z) {
            textView.setText(Bluetooth.getAddress());
        }
        textView.setVisibility(z ? 0 : 8);
        TextView textView2 = (TextView) view.findViewById(R.id.bluetoothAddressLabel);
        if (!z) {
            i2 = 8;
        }
        textView2.setVisibility(i2);
    }

    private void c(View view, boolean z) {
        int i2 = 0;
        ((TextView) view.findViewById(R.id.mobileData)).setText(z ? getString(R.string.yes) : getString(R.string.no));
        TextView textView = (TextView) view.findViewById(R.id.networkType);
        if (z) {
            textView.setText(Network.getMobileNetworkType(this.f4240a));
        }
        textView.setVisibility(z ? 0 : 8);
        TextView textView2 = (TextView) view.findViewById(R.id.networkTypeLabel);
        if (!z) {
            i2 = 8;
        }
        textView2.setVisibility(i2);
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(R.layout.fragment_device, viewGroup, false);
        this.f4241b = inflate;
        this.f4240a = inflate.getContext();
        this.c = new Handler();
        a(inflate);
        return inflate;
    }

    public void onStart() {
        super.onStart();
        c.a().a((Object) this);
    }

    public void onStop() {
        c.a().b(this);
        super.onStop();
    }

    @j(a = ThreadMode.MAIN)
    public void refreshData(RefreshEvent refreshEvent) {
        if (this.f4241b != null) {
            String str = refreshEvent.field;
            char c2 = 65535;
            switch (str.hashCode()) {
                case -1068855134:
                    if (str.equals("mobile")) {
                        c2 = 2;
                        break;
                    }
                    break;
                case 3649301:
                    if (str.equals("wifi")) {
                        c2 = 0;
                        break;
                    }
                    break;
                case 1968882350:
                    if (str.equals("bluetooth")) {
                        c2 = 1;
                        break;
                    }
                    break;
            }
            switch (c2) {
                case 0:
                    a(this.f4241b, refreshEvent.value);
                    return;
                case 1:
                    b(this.f4241b, refreshEvent.value);
                    return;
                case 2:
                    c(this.f4241b, refreshEvent.value);
                    return;
                default:
                    return;
            }
        }
    }
}
