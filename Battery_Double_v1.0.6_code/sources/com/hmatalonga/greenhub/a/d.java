package com.hmatalonga.greenhub.a;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import com.hmatalonga.greenhub.c.c;
import com.hmatalonga.greenhub.c.g;
import com.hmatalonga.greenhub.ui.WelcomeActivity.a;
import com.mansoon.BatteryDouble.R;

public class d extends e implements a {
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public static final String f4255b = c.a("TosFragment");

    /* access modifiers changed from: protected */
    public OnClickListener a() {
        return new b(this.f4259a) {
            public void onClick(View view) {
                c.a(d.f4255b, "Marking TOS flag.");
                g.a((Context) this.f4260b, true);
                a();
            }
        };
    }

    public boolean a(Context context) {
        return !g.a(context);
    }

    /* access modifiers changed from: protected */
    public OnClickListener b() {
        return new b(this.f4259a) {
            public void onClick(View view) {
                c.a(d.f4255b, "Need to accept Tos.");
                b();
            }
        };
    }

    /* access modifiers changed from: protected */
    public String c() {
        c.a(f4255b, "Getting Accept string.");
        return a((int) R.string.accept);
    }

    /* access modifiers changed from: protected */
    public String d() {
        c.a(f4255b, "Getting Decline string.");
        return a((int) R.string.decline);
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        super.onCreateView(layoutInflater, viewGroup, bundle);
        return layoutInflater.inflate(R.layout.welcome_tos_fragment, viewGroup, false);
    }
}
