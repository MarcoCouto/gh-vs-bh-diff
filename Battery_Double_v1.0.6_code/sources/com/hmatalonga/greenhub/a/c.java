package com.hmatalonga.greenhub.a;

import android.app.Fragment;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import com.github.mikephil.charting.d.i;
import com.hmatalonga.greenhub.c.a;
import com.hmatalonga.greenhub.events.RefreshChartEvent;
import com.hmatalonga.greenhub.models.data.BatteryUsage;
import com.hmatalonga.greenhub.models.ui.ChartCard;
import com.hmatalonga.greenhub.ui.MainActivity;
import com.hmatalonga.greenhub.ui.a.b;
import com.mansoon.BatteryDouble.R;
import io.realm.bn;
import java.util.ArrayList;
import java.util.Iterator;
import org.greenrobot.eventbus.ThreadMode;
import org.greenrobot.eventbus.j;

public class c extends Fragment {

    /* renamed from: a reason: collision with root package name */
    private static final String f4252a = com.hmatalonga.greenhub.c.c.a(c.class);

    /* renamed from: b reason: collision with root package name */
    private MainActivity f4253b;
    private RecyclerView c;
    private b d;
    private ArrayList<ChartCard> e;
    /* access modifiers changed from: private */
    public int f;

    public static c a() {
        return new c();
    }

    /* access modifiers changed from: private */
    public void a(int i) {
        long currentTimeMillis = System.currentTimeMillis();
        this.e = new ArrayList<>();
        if (this.f4253b.m.c()) {
            this.f4253b.m.a();
        }
        bn a2 = i == 1 ? this.f4253b.m.a(a.a(1), currentTimeMillis) : i == 2 ? this.f4253b.m.a(a.a(2), currentTimeMillis) : i == 3 ? this.f4253b.m.a(a.a(3), currentTimeMillis) : this.f4253b.m.a(a.a(1), currentTimeMillis);
        a(a2);
        b(this.f);
    }

    private void a(bn<BatteryUsage> bnVar) {
        double d2;
        double d3;
        double d4;
        ChartCard chartCard = new ChartCard(1, getString(R.string.chart_battery_level), com.github.mikephil.charting.j.a.a("#E84813"));
        Iterator it = bnVar.iterator();
        while (it.hasNext()) {
            BatteryUsage batteryUsage = (BatteryUsage) it.next();
            chartCard.entries.add(new i((float) batteryUsage.realmGet$timestamp(), batteryUsage.realmGet$level()));
        }
        this.e.add(chartCard);
        if (!bnVar.isEmpty()) {
            d4 = Double.MAX_VALUE;
            d3 = 0.0d;
            d2 = Double.MIN_VALUE;
        } else {
            d2 = 0.0d;
            d3 = 0.0d;
            d4 = 0.0d;
        }
        ChartCard chartCard2 = new ChartCard(2, getString(R.string.chart_battery_temperature), com.github.mikephil.charting.j.a.a("#E81332"));
        Iterator it2 = bnVar.iterator();
        double d5 = d4;
        double d6 = d3;
        double d7 = d2;
        while (it2.hasNext()) {
            BatteryUsage batteryUsage2 = (BatteryUsage) it2.next();
            chartCard2.entries.add(new i((float) batteryUsage2.realmGet$timestamp(), (float) batteryUsage2.realmGet$details().realmGet$temperature()));
            if (batteryUsage2.realmGet$details().realmGet$temperature() < d5) {
                d5 = batteryUsage2.realmGet$details().realmGet$temperature();
            }
            if (batteryUsage2.realmGet$details().realmGet$temperature() > d7) {
                d7 = batteryUsage2.realmGet$details().realmGet$temperature();
            }
            d6 = batteryUsage2.realmGet$details().realmGet$temperature() + d6;
        }
        double size = d6 / ((double) bnVar.size());
        chartCard2.extras = new double[]{d5, size, d7};
        this.e.add(chartCard2);
        if (!bnVar.isEmpty()) {
            d5 = Double.MAX_VALUE;
            size = 0.0d;
            d7 = Double.MIN_VALUE;
        }
        ChartCard chartCard3 = new ChartCard(3, getString(R.string.chart_battery_voltage), com.github.mikephil.charting.j.a.a("#FF15AC"));
        Iterator it3 = bnVar.iterator();
        while (true) {
            double d8 = size;
            if (it3.hasNext()) {
                BatteryUsage batteryUsage3 = (BatteryUsage) it3.next();
                chartCard3.entries.add(new i((float) batteryUsage3.realmGet$timestamp(), (float) batteryUsage3.realmGet$details().realmGet$voltage()));
                if (batteryUsage3.realmGet$details().realmGet$voltage() < d5) {
                    d5 = batteryUsage3.realmGet$details().realmGet$voltage();
                }
                if (batteryUsage3.realmGet$details().realmGet$voltage() > d7) {
                    d7 = batteryUsage3.realmGet$details().realmGet$voltage();
                }
                size = batteryUsage3.realmGet$details().realmGet$voltage() + d8;
            } else {
                chartCard3.extras = new double[]{d5, d8 / ((double) bnVar.size()), d7};
                this.e.add(chartCard3);
                return;
            }
        }
    }

    private void b(int i) {
        if (this.d == null) {
            this.d = new b(this.e, i, getActivity().getApplicationContext());
            this.c.setAdapter(this.d);
        } else {
            this.d.e(i);
            this.d.a(this.e);
        }
        this.c.invalidate();
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(R.layout.fragment_statistics, viewGroup, false);
        this.f4253b = (MainActivity) getActivity();
        this.c = (RecyclerView) inflate.findViewById(R.id.rv);
        this.d = null;
        this.c.setLayoutManager(new LinearLayoutManager(inflate.getContext()));
        this.c.setHasFixedSize(true);
        ((BottomNavigationView) inflate.findViewById(R.id.bottom_navigation)).setOnNavigationItemSelectedListener(new BottomNavigationView.b() {
            public boolean a(MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.action_24h /*2131296263*/:
                        c.this.f = 1;
                        c.this.a(1);
                        return true;
                    case R.id.action_3days /*2131296264*/:
                        c.this.f = 2;
                        c.this.a(2);
                        return true;
                    case R.id.action_5days /*2131296265*/:
                        c.this.f = 3;
                        c.this.a(3);
                        return true;
                    default:
                        return false;
                }
            }
        });
        this.f = 1;
        return inflate;
    }

    public void onResume() {
        super.onResume();
        a(this.f);
    }

    public void onStart() {
        super.onStart();
        org.greenrobot.eventbus.c.a().a((Object) this);
    }

    public void onStop() {
        org.greenrobot.eventbus.c.a().b(this);
        super.onStop();
    }

    @j(a = ThreadMode.MAIN)
    public void refreshChartsData(RefreshChartEvent refreshChartEvent) {
        a(this.f);
    }
}
