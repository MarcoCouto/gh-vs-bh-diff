package com.hmatalonga.greenhub.a;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import com.hmatalonga.greenhub.c.c;
import com.hmatalonga.greenhub.ui.MainActivity;

public abstract class e extends Fragment {
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public static final String f4258b = c.a(e.class);

    /* renamed from: a reason: collision with root package name */
    protected Activity f4259a;

    public interface a {
        Button b();

        Button x_();
    }

    protected abstract class b implements OnClickListener {

        /* renamed from: b reason: collision with root package name */
        Activity f4260b;

        b(Activity activity) {
            this.f4260b = activity;
        }

        /* access modifiers changed from: 0000 */
        public void a() {
            c.a(e.f4258b, "Proceeding to next activity");
            e.this.startActivity(new Intent(this.f4260b, MainActivity.class));
            this.f4260b.finish();
        }

        /* access modifiers changed from: 0000 */
        public void b() {
            c.a(e.f4258b, "Closing app");
            this.f4260b.finish();
        }
    }

    /* access modifiers changed from: protected */
    public abstract OnClickListener a();

    /* access modifiers changed from: protected */
    public String a(int i) {
        if (this.f4259a != null) {
            return this.f4259a.getResources().getString(i);
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public void a(Button button) {
        button.setText(c());
        button.setOnClickListener(a());
    }

    /* access modifiers changed from: protected */
    public abstract OnClickListener b();

    /* access modifiers changed from: protected */
    public void b(Button button) {
        button.setText(d());
        button.setOnClickListener(b());
    }

    /* access modifiers changed from: protected */
    public abstract String c();

    /* access modifiers changed from: protected */
    public abstract String d();

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        c.a(f4258b, "Attaching to activity");
        this.f4259a = activity;
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View onCreateView = super.onCreateView(layoutInflater, viewGroup, bundle);
        c.a(f4258b, "Creating View");
        if (this.f4259a instanceof a) {
            a aVar = (a) this.f4259a;
            a(aVar.x_());
            b(aVar.b());
        }
        return onCreateView;
    }

    public void onDetach() {
        super.onDetach();
        this.f4259a = null;
    }
}
