package com.hmatalonga.greenhub.a;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import com.hmatalonga.greenhub.Config;
import com.hmatalonga.greenhub.c.c;
import com.hmatalonga.greenhub.events.BatteryLevelEvent;
import com.hmatalonga.greenhub.events.PowerSourceEvent;
import com.hmatalonga.greenhub.events.StatusEvent;
import com.hmatalonga.greenhub.managers.sampling.DataEstimator;
import com.hmatalonga.greenhub.managers.sampling.a;
import com.hmatalonga.greenhub.models.Battery;
import com.hmatalonga.greenhub.models.ui.BatteryCard;
import com.hmatalonga.greenhub.ui.MainActivity;
import com.mansoon.BatteryDouble.R;
import java.util.ArrayList;
import org.greenrobot.eventbus.ThreadMode;
import org.greenrobot.eventbus.j;

public class b extends Fragment {
    private static final String d = c.a("HomeFragment");
    private ImageView A;
    /* access modifiers changed from: private */
    public RelativeLayout B;
    private TextView C;
    private Runnable D = new Runnable() {
        public void run() {
            int batteryCurrentNow = Battery.getBatteryCurrentNow(b.this.e);
            double b2 = a.b();
            if (b.this.x.equals("unplugged") || b2 != 1.0d) {
                if (Math.abs(batteryCurrentNow) < Math.abs(b.this.v)) {
                    b.this.v = batteryCurrentNow;
                    b.this.i.setText("min: " + b.this.v + " mA");
                }
                if (Math.abs(batteryCurrentNow) > Math.abs(b.this.w)) {
                    b.this.w = batteryCurrentNow;
                    b.this.j.setText("max: " + b.this.w + " mA");
                }
                b.this.h.setText(batteryCurrentNow + " mA");
                b.this.u.postDelayed(this, 5000);
                return;
            }
            b.this.i.setText("min: --");
            b.this.j.setText("max: --");
            b.this.h.setText(b.this.e.getString(R.string.battery_full));
            b.this.u.postDelayed(this, 5000);
        }
    };

    /* renamed from: a reason: collision with root package name */
    String f4244a = Config.NOTIFICATION_DEFAULT_PRIORITY;

    /* renamed from: b reason: collision with root package name */
    int f4245b = 0;
    int c = 0;
    /* access modifiers changed from: private */
    public Context e;
    private MainActivity f;
    private TextView g;
    /* access modifiers changed from: private */
    public TextView h;
    /* access modifiers changed from: private */
    public TextView i;
    /* access modifiers changed from: private */
    public TextView j;
    private ImageView k;
    private ImageView l;
    private ImageView m;
    private ImageView n;
    private TextView o;
    private ProgressBar p;
    private RecyclerView q;
    private com.hmatalonga.greenhub.ui.a.a r;
    /* access modifiers changed from: private */
    public ArrayList<BatteryCard> s;
    private Thread t;
    /* access modifiers changed from: private */
    public Handler u;
    /* access modifiers changed from: private */
    public int v;
    /* access modifiers changed from: private */
    public int w;
    /* access modifiers changed from: private */
    public String x;
    /* access modifiers changed from: private */
    public RelativeLayout y;
    private TextView z;

    public static b a() {
        return new b();
    }

    private void a(final DataEstimator dataEstimator) {
        this.t = new Thread(new Runnable() {
            public void run() {
                String technology;
                int i = -65536;
                int i2 = -7829368;
                int i3 = -16711936;
                b.this.s = new ArrayList();
                float temperature = dataEstimator.getTemperature();
                String valueOf = String.valueOf(temperature + " ºC");
                int i4 = temperature > 45.0f ? -65536 : (temperature > 45.0f || temperature <= 35.0f) ? -16711936 : -256;
                b.this.s.add(new BatteryCard(R.drawable.ic_thermometer_black_18dp, b.this.getString(R.string.battery_summary_temperature), valueOf, i4));
                b.this.s.add(new BatteryCard(R.drawable.ic_flash_black_18dp, b.this.getString(R.string.battery_summary_voltage), String.valueOf(dataEstimator.getVoltage() + " V")));
                String healthStatus = dataEstimator.getHealthStatus(b.this.e);
                if (healthStatus.equals(b.this.e.getString(R.string.battery_health_good))) {
                    i = -16711936;
                }
                b.this.s.add(new BatteryCard(R.drawable.ic_heart_black_18dp, b.this.getString(R.string.battery_summary_health), healthStatus, i));
                if (dataEstimator.getTechnology() == null) {
                    technology = b.this.getString(R.string.not_available);
                } else {
                    if (dataEstimator.getTechnology().equals("Li-ion")) {
                        i3 = -7829368;
                    }
                    technology = dataEstimator.getTechnology();
                    i2 = i3;
                }
                b.this.s.add(new BatteryCard(R.drawable.ic_wrench_black_18dp, b.this.getString(R.string.battery_summary_technology), technology, i2));
            }
        });
        this.t.start();
        b();
    }

    private void a(String str) {
        this.f4244a = str;
        int parseInt = Integer.parseInt(this.f4244a);
        this.f4245b = Math.max((parseInt - 50) * 2, 0);
        this.c = Math.min(parseInt * 2, 100);
        this.y.setVisibility(0);
        this.z.setVisibility(0);
        this.B.setVisibility(0);
        this.C.setVisibility(0);
        this.z.setText(this.c + "%");
        this.C.setText(this.f4245b + "%");
        if (this.f4245b == 0) {
            this.B.setVisibility(4);
        }
        if (this.c == 0) {
            this.y.setVisibility(4);
        }
        this.y.post(new Runnable() {
            public void run() {
                LayoutParams layoutParams = (LayoutParams) b.this.y.getLayoutParams();
                int i = b.this.c * 4;
                if (i >= 23) {
                    i = (b.this.c * 4) - 23;
                }
                layoutParams.height = i;
                b.this.y.setLayoutParams(layoutParams);
            }
        });
        this.B.post(new Runnable() {
            public void run() {
                LayoutParams layoutParams = (LayoutParams) b.this.B.getLayoutParams();
                layoutParams.height = (b.this.f4245b * 4) - 23;
                b.this.B.setLayoutParams(layoutParams);
            }
        });
    }

    private void b() {
        try {
            this.t.join();
            if (this.r == null) {
                this.r = new com.hmatalonga.greenhub.ui.a.a(this.s);
                this.q.setAdapter(this.r);
            } else {
                this.r.a(this.s);
            }
            this.q.invalidate();
        } catch (InterruptedException e2) {
            e2.printStackTrace();
        }
    }

    private void b(String str) {
        this.v = Integer.MAX_VALUE;
        this.w = Integer.MIN_VALUE;
        String str2 = "unplugged";
        if (str.equals("home")) {
            if (this.f.l() != null) {
                switch (this.f.l().getPlugged()) {
                    case 1:
                        str2 = "ac";
                        break;
                    case 2:
                        str2 = "usb";
                        break;
                }
            } else {
                return;
            }
        } else {
            str2 = str;
        }
        this.A.setVisibility(4);
        String str3 = this.x;
        char c2 = 65535;
        switch (str3.hashCode()) {
            case -1000044642:
                if (str3.equals("wireless")) {
                    c2 = 3;
                    break;
                }
                break;
            case 3106:
                if (str3.equals("ac")) {
                    c2 = 1;
                    break;
                }
                break;
            case 116100:
                if (str3.equals("usb")) {
                    c2 = 2;
                    break;
                }
                break;
            case 1236169279:
                if (str3.equals("unplugged")) {
                    c2 = 0;
                    break;
                }
                break;
        }
        switch (c2) {
            case 0:
                this.k.setImageResource(R.drawable.ic_battery_50_grey600_24dp);
                break;
            case 1:
                this.l.setImageResource(R.drawable.ic_power_plug_grey600_24dp);
                break;
            case 2:
                this.m.setImageResource(R.drawable.ic_usb_grey600_24dp);
                break;
            case 3:
                this.n.setImageResource(R.drawable.ic_access_point_grey600_24dp);
                break;
        }
        if (str2.equals("unplugged")) {
            this.k.setImageResource(R.drawable.ic_battery_50_white_24dp);
        } else if (str2.equals("ac")) {
            this.A.setVisibility(0);
            this.l.setImageResource(R.drawable.ic_power_plug_white_24dp);
        } else if (str2.equals("usb")) {
            this.A.setVisibility(0);
            this.m.setImageResource(R.drawable.ic_usb_white_24dp);
        } else if (str2.equals("wireless")) {
            this.n.setImageResource(R.drawable.ic_access_point_white_24dp);
        }
        this.x = str2;
    }

    private void c() {
        this.v = Integer.MAX_VALUE;
        this.w = 0;
        this.i.setText("min: --");
        this.j.setText("max: --");
        this.h.setText(getString(R.string.battery_measure));
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(R.layout.fragment_home, viewGroup, false);
        this.e = inflate.getContext();
        this.f = (MainActivity) getActivity();
        this.q = (RecyclerView) inflate.findViewById(R.id.rv);
        this.r = null;
        this.q.setLayoutManager(new LinearLayoutManager(this.e) {
            public boolean e() {
                return false;
            }
        });
        this.q.setHasFixedSize(true);
        this.g = (TextView) inflate.findViewById(R.id.batteryCurrentValue);
        this.p = (ProgressBar) inflate.findViewById(R.id.batteryProgressbar);
        this.o = (TextView) inflate.findViewById(R.id.status);
        this.h = (TextView) inflate.findViewById(R.id.batteryCurrentNow);
        this.i = (TextView) inflate.findViewById(R.id.batteryCurrentMin);
        this.j = (TextView) inflate.findViewById(R.id.batteryCurrentMax);
        this.k = (ImageView) inflate.findViewById(R.id.imgPowerDischarging);
        this.l = (ImageView) inflate.findViewById(R.id.imgPowerAc);
        this.m = (ImageView) inflate.findViewById(R.id.imgPowerUsb);
        this.n = (ImageView) inflate.findViewById(R.id.imgPowerWireless);
        this.x = "";
        this.y = (RelativeLayout) inflate.findViewById(R.id.levelContainer);
        this.z = (TextView) inflate.findViewById(R.id.batteryPercentage);
        this.B = (RelativeLayout) inflate.findViewById(R.id.levelContainer1);
        this.C = (TextView) inflate.findViewById(R.id.batteryPercentage1);
        this.A = (ImageView) inflate.findViewById(R.id.charging);
        this.y.setVisibility(4);
        this.z.setVisibility(4);
        this.B.setVisibility(4);
        this.C.setVisibility(4);
        this.v = Integer.MAX_VALUE;
        this.w = 0;
        this.u = new Handler();
        this.u.postDelayed(this.D, 5000);
        return inflate;
    }

    public void onResume() {
        super.onResume();
        if (this.f.l() != null) {
            String num = Integer.toString(this.f.l().getLevel());
            this.g.setText(num);
            a(num);
            this.p.setProgress(this.f.l().getLevel());
            a(this.f.l());
            b("home");
        }
    }

    public void onStart() {
        super.onStart();
        org.greenrobot.eventbus.c.a().a((Object) this);
    }

    public void onStop() {
        org.greenrobot.eventbus.c.a().b(this);
        super.onStop();
    }

    @j(a = ThreadMode.MAIN)
    public void updateBatteryLevelUI(BatteryLevelEvent batteryLevelEvent) {
        String str = "" + batteryLevelEvent.level;
        this.g.setText(str);
        this.p.setProgress(batteryLevelEvent.level);
        a(str);
        a(this.f.l());
    }

    @j(a = ThreadMode.MAIN)
    public void updatePowerSource(PowerSourceEvent powerSourceEvent) {
        b(powerSourceEvent.status);
        if (this.x.equals("unplugged")) {
            c();
        }
    }

    @j(a = ThreadMode.MAIN)
    public void updateStatus(StatusEvent statusEvent) {
        this.o.setText(statusEvent.status);
    }
}
