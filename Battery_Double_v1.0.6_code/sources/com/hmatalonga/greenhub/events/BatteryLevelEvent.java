package com.hmatalonga.greenhub.events;

public class BatteryLevelEvent {
    public final int level;

    public BatteryLevelEvent(int i) {
        this.level = i;
    }
}
