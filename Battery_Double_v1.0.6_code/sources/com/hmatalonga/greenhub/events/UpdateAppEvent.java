package com.hmatalonga.greenhub.events;

public class UpdateAppEvent {
    public final int version;

    public UpdateAppEvent(int i) {
        this.version = i;
    }
}
