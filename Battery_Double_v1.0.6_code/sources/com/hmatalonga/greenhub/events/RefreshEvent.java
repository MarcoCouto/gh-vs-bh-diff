package com.hmatalonga.greenhub.events;

public class RefreshEvent {
    public final String field;
    public final boolean value;

    public RefreshEvent(String str, boolean z) {
        this.field = str;
        this.value = z;
    }
}
