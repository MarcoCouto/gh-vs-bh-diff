package com.hmatalonga.greenhub.events;

import com.hmatalonga.greenhub.models.ui.Task;

public class OpenTaskDetailsEvent {
    public final Task task;

    public OpenTaskDetailsEvent(Task task2) {
        this.task = task2;
    }
}
