package com.hmatalonga.greenhub.events;

public class StatusEvent {
    public final String status;

    public StatusEvent(String str) {
        this.status = str;
    }
}
