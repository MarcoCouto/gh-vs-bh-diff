package com.hmatalonga.greenhub.models;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Build.VERSION;
import android.preference.PreferenceManager;
import com.hmatalonga.greenhub.c.c;
import com.hmatalonga.greenhub.models.data.Feature;
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.util.UUID;

public class Specifications {
    private static final String PREF_UNIQUE_ID = "PREF_UNIQUE_ID";
    private static final String TAG = c.a(Specifications.class);
    private static final String TYPE_UNKNOWN = "unknown";
    private static final int UUID_LENGTH = 16;

    private static boolean checkRootMethod1() {
        String str = Build.TAGS;
        return str != null && str.contains("test-keys");
    }

    private static boolean checkRootMethod2() {
        for (String file : new String[]{"/system/app/Superuser.apk", "/sbin/su", "/system/bin/su", "/system/xbin/su", "/data/local/xbin/su", "/data/local/bin/su", "/system/sd/xbin/su", "/system/bin/failsafe/su", "/data/local/su"}) {
            if (new File(file).exists()) {
                return true;
            }
        }
        return false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0035, code lost:
        r0 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x003d, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x003e, code lost:
        if (r2 != null) goto L_0x0040;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0040, code lost:
        r2.destroy();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0043, code lost:
        throw r0;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x003d A[ExcHandler: all (r0v1 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:1:0x0003] */
    private static boolean checkRootMethod3() {
        Process process;
        Process exec;
        boolean z = true;
        Process process2 = null;
        try {
            exec = Runtime.getRuntime().exec(new String[]{"/system/xbin/which", "su"});
            if (new BufferedReader(new InputStreamReader(exec.getInputStream())).readLine() == null) {
                z = false;
            }
            if (exec == null) {
                return z;
            }
            exec.destroy();
            return z;
        } catch (Throwable th) {
        }
        if (process != null) {
            process.destroy();
        }
        return false;
    }

    public static synchronized String getAndroidId(Context context) {
        String string;
        synchronized (Specifications.class) {
            string = getSharedPreferences(context).getString(PREF_UNIQUE_ID, null);
            if (string == null) {
                string = UUID.randomUUID().toString();
                c.a(TAG, "No Android ID Key on this device. Generating random one: " + string);
                setAndroidId(context, string);
            }
        }
        return string;
    }

    public static String getBrand() {
        return Build.BRAND;
    }

    public static String getBuildSerial() {
        return Build.SERIAL;
    }

    public static String getKernelVersion() {
        return System.getProperty("os.version", "unknown");
    }

    public static String getManufacturer() {
        return Build.MANUFACTURER;
    }

    public static String getModel() {
        return Build.MODEL;
    }

    public static String getOsVersion() {
        return VERSION.RELEASE;
    }

    public static String getProductName() {
        return Build.PRODUCT;
    }

    private static SharedPreferences getSharedPreferences(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static String getStringFromSystemProperty(Context context, String str) {
        try {
            String systemProperty = getSystemProperty(context, str);
            if (systemProperty != null && systemProperty.length() > 0) {
                return systemProperty;
            }
        } catch (Exception e) {
            if (!(e == null || e.getLocalizedMessage() == null)) {
                c.a(TAG, "Failed getting service provider: " + e.getLocalizedMessage());
            }
        }
        return null;
    }

    public static String getSystemProperty(Context context, String str) throws Exception {
        Method method = Class.forName("android.os.SystemProperties").getMethod("get", new Class[]{String.class});
        method.setAccessible(true);
        return (String) method.invoke(context, new Object[]{str});
    }

    public static Feature getVmVersion() {
        Feature feature = new Feature();
        String property = System.getProperty("java.vm.version");
        if (property == null) {
            property = "";
        }
        feature.realmSet$key("vm");
        feature.realmSet$value(property);
        return feature;
    }

    public static boolean isRooted() {
        return checkRootMethod1() || checkRootMethod2() || checkRootMethod3();
    }

    private static void setAndroidId(Context context, String str) {
        getSharedPreferences(context).edit().putString(PREF_UNIQUE_ID, str).apply();
        c.a(TAG, "Android ID Key of device set to: " + str);
    }
}
