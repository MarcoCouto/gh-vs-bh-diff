package com.hmatalonga.greenhub.models.data;

import io.realm.ad;
import io.realm.bj;
import io.realm.internal.m;

public class Feature extends bj implements ad {
    public String key;
    public String value;

    public Feature() {
        if (this instanceof m) {
            ((m) this).a();
        }
    }

    public String realmGet$key() {
        return this.key;
    }

    public String realmGet$value() {
        return this.value;
    }

    public void realmSet$key(String str) {
        this.key = str;
    }

    public void realmSet$value(String str) {
        this.value = str;
    }
}
