package com.hmatalonga.greenhub.models.data;

import io.realm.bj;
import io.realm.internal.m;
import io.realm.o;

public class CallInfo extends bj implements o {
    public String callStatus;
    public double incomingCallTime;
    public double nonCallTime;
    public double outgoingCallTime;

    public CallInfo() {
        if (this instanceof m) {
            ((m) this).a();
        }
    }

    public String realmGet$callStatus() {
        return this.callStatus;
    }

    public double realmGet$incomingCallTime() {
        return this.incomingCallTime;
    }

    public double realmGet$nonCallTime() {
        return this.nonCallTime;
    }

    public double realmGet$outgoingCallTime() {
        return this.outgoingCallTime;
    }

    public void realmSet$callStatus(String str) {
        this.callStatus = str;
    }

    public void realmSet$incomingCallTime(double d) {
        this.incomingCallTime = d;
    }

    public void realmSet$nonCallTime(double d) {
        this.nonCallTime = d;
    }

    public void realmSet$outgoingCallTime(double d) {
        this.outgoingCallTime = d;
    }
}
