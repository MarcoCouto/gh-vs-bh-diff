package com.hmatalonga.greenhub.models.data;

import io.realm.bj;
import io.realm.internal.m;
import io.realm.q;

public class CallMonth extends bj implements q {
    public long totalCallInDur;
    public int totalCallInNum;
    public long totalCallOutDur;
    public int totalCallOutNum;
    public int totalMissedCallNum;

    public CallMonth() {
        if (this instanceof m) {
            ((m) this).a();
        }
        realmSet$totalCallInNum(0);
        realmSet$totalCallOutNum(0);
        realmSet$totalMissedCallNum(0);
        realmSet$totalCallInDur(0);
        realmSet$totalCallOutDur(0);
    }

    public long realmGet$totalCallInDur() {
        return this.totalCallInDur;
    }

    public int realmGet$totalCallInNum() {
        return this.totalCallInNum;
    }

    public long realmGet$totalCallOutDur() {
        return this.totalCallOutDur;
    }

    public int realmGet$totalCallOutNum() {
        return this.totalCallOutNum;
    }

    public int realmGet$totalMissedCallNum() {
        return this.totalMissedCallNum;
    }

    public void realmSet$totalCallInDur(long j) {
        this.totalCallInDur = j;
    }

    public void realmSet$totalCallInNum(int i) {
        this.totalCallInNum = i;
    }

    public void realmSet$totalCallOutDur(long j) {
        this.totalCallOutDur = j;
    }

    public void realmSet$totalCallOutNum(int i) {
        this.totalCallOutNum = i;
    }

    public void realmSet$totalMissedCallNum(int i) {
        this.totalMissedCallNum = i;
    }
}
