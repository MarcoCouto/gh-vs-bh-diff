package com.hmatalonga.greenhub.models.data;

import io.realm.bj;
import io.realm.internal.m;
import io.realm.y;

public class Device extends bj implements y {
    public String brand;
    public int isRoot;
    public String kernelVersion;
    public String manufacturer;
    public String model;
    public String osVersion;
    public String product;
    public String uuId;

    public Device() {
        if (this instanceof m) {
            ((m) this).a();
        }
    }

    public String realmGet$brand() {
        return this.brand;
    }

    public int realmGet$isRoot() {
        return this.isRoot;
    }

    public String realmGet$kernelVersion() {
        return this.kernelVersion;
    }

    public String realmGet$manufacturer() {
        return this.manufacturer;
    }

    public String realmGet$model() {
        return this.model;
    }

    public String realmGet$osVersion() {
        return this.osVersion;
    }

    public String realmGet$product() {
        return this.product;
    }

    public String realmGet$uuId() {
        return this.uuId;
    }

    public void realmSet$brand(String str) {
        this.brand = str;
    }

    public void realmSet$isRoot(int i) {
        this.isRoot = i;
    }

    public void realmSet$kernelVersion(String str) {
        this.kernelVersion = str;
    }

    public void realmSet$manufacturer(String str) {
        this.manufacturer = str;
    }

    public void realmSet$model(String str) {
        this.model = str;
    }

    public void realmSet$osVersion(String str) {
        this.osVersion = str;
    }

    public void realmSet$product(String str) {
        this.product = str;
    }

    public void realmSet$uuId(String str) {
        this.uuId = str;
    }
}
