package com.hmatalonga.greenhub.models.data;

import io.realm.bj;
import io.realm.internal.m;
import io.realm.k;

public class BatteryUsage extends bj implements k {
    public BatteryDetails details;
    public int id;
    public float level;
    public int screenOn;
    public String state;
    public long timestamp;
    public String triggeredBy;

    public BatteryUsage() {
        if (this instanceof m) {
            ((m) this).a();
        }
    }

    public BatteryDetails realmGet$details() {
        return this.details;
    }

    public int realmGet$id() {
        return this.id;
    }

    public float realmGet$level() {
        return this.level;
    }

    public int realmGet$screenOn() {
        return this.screenOn;
    }

    public String realmGet$state() {
        return this.state;
    }

    public long realmGet$timestamp() {
        return this.timestamp;
    }

    public String realmGet$triggeredBy() {
        return this.triggeredBy;
    }

    public void realmSet$details(BatteryDetails batteryDetails) {
        this.details = batteryDetails;
    }

    public void realmSet$id(int i) {
        this.id = i;
    }

    public void realmSet$level(float f) {
        this.level = f;
    }

    public void realmSet$screenOn(int i) {
        this.screenOn = i;
    }

    public void realmSet$state(String str) {
        this.state = str;
    }

    public void realmSet$timestamp(long j) {
        this.timestamp = j;
    }

    public void realmSet$triggeredBy(String str) {
        this.triggeredBy = str;
    }
}
