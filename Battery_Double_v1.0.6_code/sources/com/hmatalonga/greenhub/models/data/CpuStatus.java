package com.hmatalonga.greenhub.models.data;

import io.realm.bj;
import io.realm.internal.m;
import io.realm.v;

public class CpuStatus extends bj implements v {
    public double cpuUsage;
    public long sleepTime;
    public long upTime;

    public CpuStatus() {
        if (this instanceof m) {
            ((m) this).a();
        }
    }

    public double realmGet$cpuUsage() {
        return this.cpuUsage;
    }

    public long realmGet$sleepTime() {
        return this.sleepTime;
    }

    public long realmGet$upTime() {
        return this.upTime;
    }

    public void realmSet$cpuUsage(double d) {
        this.cpuUsage = d;
    }

    public void realmSet$sleepTime(long j) {
        this.sleepTime = j;
    }

    public void realmSet$upTime(long j) {
        this.upTime = j;
    }
}
