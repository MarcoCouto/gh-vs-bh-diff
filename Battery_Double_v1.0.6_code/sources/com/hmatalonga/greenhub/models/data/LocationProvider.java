package com.hmatalonga.greenhub.models.data;

import io.realm.aj;
import io.realm.bj;
import io.realm.internal.m;

public class LocationProvider extends bj implements aj {
    public String provider;

    public LocationProvider() {
        if (this instanceof m) {
            ((m) this).a();
        }
    }

    public LocationProvider(String str) {
        if (this instanceof m) {
            ((m) this).a();
        }
        realmSet$provider(str);
    }

    public String realmGet$provider() {
        return this.provider;
    }

    public void realmSet$provider(String str) {
        this.provider = str;
    }
}
