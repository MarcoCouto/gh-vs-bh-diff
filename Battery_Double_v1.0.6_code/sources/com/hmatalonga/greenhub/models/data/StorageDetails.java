package com.hmatalonga.greenhub.models.data;

import io.realm.bj;
import io.realm.bw;
import io.realm.internal.m;

public class StorageDetails extends bj implements bw {
    public int free;
    public int freeExternal;
    public int freeSecondary;
    public int freeSystem;
    public int total;
    public int totalExternal;
    public int totalSecondary;
    public int totalSystem;

    public StorageDetails() {
        if (this instanceof m) {
            ((m) this).a();
        }
    }

    public int realmGet$free() {
        return this.free;
    }

    public int realmGet$freeExternal() {
        return this.freeExternal;
    }

    public int realmGet$freeSecondary() {
        return this.freeSecondary;
    }

    public int realmGet$freeSystem() {
        return this.freeSystem;
    }

    public int realmGet$total() {
        return this.total;
    }

    public int realmGet$totalExternal() {
        return this.totalExternal;
    }

    public int realmGet$totalSecondary() {
        return this.totalSecondary;
    }

    public int realmGet$totalSystem() {
        return this.totalSystem;
    }

    public void realmSet$free(int i) {
        this.free = i;
    }

    public void realmSet$freeExternal(int i) {
        this.freeExternal = i;
    }

    public void realmSet$freeSecondary(int i) {
        this.freeSecondary = i;
    }

    public void realmSet$freeSystem(int i) {
        this.freeSystem = i;
    }

    public void realmSet$total(int i) {
        this.total = i;
    }

    public void realmSet$totalExternal(int i) {
        this.totalExternal = i;
    }

    public void realmSet$totalSecondary(int i) {
        this.totalSecondary = i;
    }

    public void realmSet$totalSystem(int i) {
        this.totalSystem = i;
    }
}
