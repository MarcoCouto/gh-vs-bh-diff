package com.hmatalonga.greenhub.models.data;

import io.realm.bj;
import io.realm.d;
import io.realm.internal.m;

public class AppSignature extends bj implements d {
    public String signature;

    public AppSignature() {
        if (this instanceof m) {
            ((m) this).a();
        }
    }

    public AppSignature(String str) {
        if (this instanceof m) {
            ((m) this).a();
        }
        realmSet$signature(str);
    }

    public String realmGet$signature() {
        return this.signature;
    }

    public void realmSet$signature(String str) {
        this.signature = str;
    }
}
