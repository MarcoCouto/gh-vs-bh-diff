package com.hmatalonga.greenhub.models.data;

import io.realm.b;
import io.realm.bj;
import io.realm.internal.m;

public class AppPermission extends bj implements b {
    public String permission;

    public AppPermission() {
        if (this instanceof m) {
            ((m) this).a();
        }
    }

    public AppPermission(String str) {
        if (this instanceof m) {
            ((m) this).a();
        }
        realmSet$permission(str);
    }

    public String realmGet$permission() {
        return this.permission;
    }

    public void realmSet$permission(String str) {
        this.permission = str;
    }
}
