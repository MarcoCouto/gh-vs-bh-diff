package com.hmatalonga.greenhub.models.data;

import io.realm.bj;
import io.realm.bt;
import io.realm.internal.m;

public class Settings extends bj implements bt {
    public boolean bluetoothEnabled;
    public int developerMode;
    public boolean flashlightEnabled;
    public boolean locationEnabled;
    public boolean nfcEnabled;
    public boolean powersaverEnabled;
    public int unknownSources;

    public Settings() {
        if (this instanceof m) {
            ((m) this).a();
        }
    }

    public boolean realmGet$bluetoothEnabled() {
        return this.bluetoothEnabled;
    }

    public int realmGet$developerMode() {
        return this.developerMode;
    }

    public boolean realmGet$flashlightEnabled() {
        return this.flashlightEnabled;
    }

    public boolean realmGet$locationEnabled() {
        return this.locationEnabled;
    }

    public boolean realmGet$nfcEnabled() {
        return this.nfcEnabled;
    }

    public boolean realmGet$powersaverEnabled() {
        return this.powersaverEnabled;
    }

    public int realmGet$unknownSources() {
        return this.unknownSources;
    }

    public void realmSet$bluetoothEnabled(boolean z) {
        this.bluetoothEnabled = z;
    }

    public void realmSet$developerMode(int i) {
        this.developerMode = i;
    }

    public void realmSet$flashlightEnabled(boolean z) {
        this.flashlightEnabled = z;
    }

    public void realmSet$locationEnabled(boolean z) {
        this.locationEnabled = z;
    }

    public void realmSet$nfcEnabled(boolean z) {
        this.nfcEnabled = z;
    }

    public void realmSet$powersaverEnabled(boolean z) {
        this.powersaverEnabled = z;
    }

    public void realmSet$unknownSources(int i) {
        this.unknownSources = i;
    }
}
