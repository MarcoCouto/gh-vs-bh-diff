package com.hmatalonga.greenhub.models.data;

import io.realm.az;
import io.realm.bf;
import io.realm.bj;
import io.realm.internal.m;

public class ProcessInfo extends bj implements az {
    public bf<AppPermission> appPermissions;
    public bf<AppSignature> appSignatures;
    public String applicationLabel;
    public String importance;
    public String installationPkg;
    public boolean isSystemApp;
    public String name;
    public int processId;
    public int versionCode;
    public String versionName;

    public ProcessInfo() {
        if (this instanceof m) {
            ((m) this).a();
        }
    }

    public bf realmGet$appPermissions() {
        return this.appPermissions;
    }

    public bf realmGet$appSignatures() {
        return this.appSignatures;
    }

    public String realmGet$applicationLabel() {
        return this.applicationLabel;
    }

    public String realmGet$importance() {
        return this.importance;
    }

    public String realmGet$installationPkg() {
        return this.installationPkg;
    }

    public boolean realmGet$isSystemApp() {
        return this.isSystemApp;
    }

    public String realmGet$name() {
        return this.name;
    }

    public int realmGet$processId() {
        return this.processId;
    }

    public int realmGet$versionCode() {
        return this.versionCode;
    }

    public String realmGet$versionName() {
        return this.versionName;
    }

    public void realmSet$appPermissions(bf bfVar) {
        this.appPermissions = bfVar;
    }

    public void realmSet$appSignatures(bf bfVar) {
        this.appSignatures = bfVar;
    }

    public void realmSet$applicationLabel(String str) {
        this.applicationLabel = str;
    }

    public void realmSet$importance(String str) {
        this.importance = str;
    }

    public void realmSet$installationPkg(String str) {
        this.installationPkg = str;
    }

    public void realmSet$isSystemApp(boolean z) {
        this.isSystemApp = z;
    }

    public void realmSet$name(String str) {
        this.name = str;
    }

    public void realmSet$processId(int i) {
        this.processId = i;
    }

    public void realmSet$versionCode(int i) {
        this.versionCode = i;
    }

    public void realmSet$versionName(String str) {
        this.versionName = str;
    }
}
