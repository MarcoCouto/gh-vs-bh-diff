package com.hmatalonga.greenhub.models.data;

import io.realm.at;
import io.realm.bj;
import io.realm.internal.m;

public class NetworkStatistics extends bj implements at {
    public double mobileReceived;
    public double mobileSent;
    public double wifiReceived;
    public double wifiSent;

    public NetworkStatistics() {
        if (this instanceof m) {
            ((m) this).a();
        }
    }

    public double realmGet$mobileReceived() {
        return this.mobileReceived;
    }

    public double realmGet$mobileSent() {
        return this.mobileSent;
    }

    public double realmGet$wifiReceived() {
        return this.wifiReceived;
    }

    public double realmGet$wifiSent() {
        return this.wifiSent;
    }

    public void realmSet$mobileReceived(double d) {
        this.mobileReceived = d;
    }

    public void realmSet$mobileSent(double d) {
        this.mobileSent = d;
    }

    public void realmSet$wifiReceived(double d) {
        this.wifiReceived = d;
    }

    public void realmSet$wifiSent(double d) {
        this.wifiSent = d;
    }
}
