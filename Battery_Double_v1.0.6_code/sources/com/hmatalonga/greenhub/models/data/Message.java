package com.hmatalonga.greenhub.models.data;

import io.realm.an;
import io.realm.bj;
import io.realm.internal.m;

public class Message extends bj implements an {
    public String body;
    public String date;
    public int id;
    public boolean read;
    public String title;
    public String type;

    public Message() {
        if (this instanceof m) {
            ((m) this).a();
        }
        realmSet$type("info");
        realmSet$read(false);
    }

    public Message(int i, String str, String str2, String str3) {
        if (this instanceof m) {
            ((m) this).a();
        }
        realmSet$id(i);
        realmSet$type("info");
        realmSet$title(str);
        realmSet$body(str2);
        realmSet$date(str3);
        realmSet$read(false);
    }

    public Message(int i, String str, String str2, String str3, boolean z) {
        if (this instanceof m) {
            ((m) this).a();
        }
        realmSet$id(i);
        realmSet$type("info");
        realmSet$title(str);
        realmSet$body(str2);
        realmSet$date(str3);
        realmSet$read(z);
    }

    public String realmGet$body() {
        return this.body;
    }

    public String realmGet$date() {
        return this.date;
    }

    public int realmGet$id() {
        return this.id;
    }

    public boolean realmGet$read() {
        return this.read;
    }

    public String realmGet$title() {
        return this.title;
    }

    public String realmGet$type() {
        return this.type;
    }

    public void realmSet$body(String str) {
        this.body = str;
    }

    public void realmSet$date(String str) {
        this.date = str;
    }

    public void realmSet$id(int i) {
        this.id = i;
    }

    public void realmSet$read(boolean z) {
        this.read = z;
    }

    public void realmSet$title(String str) {
        this.title = str;
    }

    public void realmSet$type(String str) {
        this.type = str;
    }
}
