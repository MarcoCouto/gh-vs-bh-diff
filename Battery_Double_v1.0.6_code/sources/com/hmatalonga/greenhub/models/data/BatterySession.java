package com.hmatalonga.greenhub.models.data;

import io.realm.bj;
import io.realm.i;
import io.realm.internal.m;

public class BatterySession extends bj implements i {
    public int id;
    public float level;
    public int screenOn;
    public long timestamp;
    public String triggeredBy;

    public BatterySession() {
        if (this instanceof m) {
            ((m) this).a();
        }
    }

    public int realmGet$id() {
        return this.id;
    }

    public float realmGet$level() {
        return this.level;
    }

    public int realmGet$screenOn() {
        return this.screenOn;
    }

    public long realmGet$timestamp() {
        return this.timestamp;
    }

    public String realmGet$triggeredBy() {
        return this.triggeredBy;
    }

    public void realmSet$id(int i) {
        this.id = i;
    }

    public void realmSet$level(float f) {
        this.level = f;
    }

    public void realmSet$screenOn(int i) {
        this.screenOn = i;
    }

    public void realmSet$timestamp(long j) {
        this.timestamp = j;
    }

    public void realmSet$triggeredBy(String str) {
        this.triggeredBy = str;
    }
}
