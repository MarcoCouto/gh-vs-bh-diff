package com.hmatalonga.greenhub.models;

import android.bluetooth.BluetoothAdapter;

public class Bluetooth {
    private static final String TAG = "Bluetooth";

    public static String getAddress() {
        BluetoothAdapter defaultAdapter = BluetoothAdapter.getDefaultAdapter();
        return defaultAdapter != null ? defaultAdapter.getAddress() : "not available";
    }

    public static boolean isEnabled() {
        BluetoothAdapter defaultAdapter = BluetoothAdapter.getDefaultAdapter();
        return defaultAdapter != null && defaultAdapter.isEnabled();
    }
}
