package com.hmatalonga.greenhub.models;

import android.content.pm.PackageInfo;
import android.content.pm.Signature;
import com.hmatalonga.greenhub.c.c;
import com.hmatalonga.greenhub.c.h;
import com.hmatalonga.greenhub.models.data.AppSignature;
import java.io.ByteArrayInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.interfaces.DSAPublicKey;
import java.security.interfaces.RSAPublicKey;
import java.util.LinkedList;
import java.util.List;

public class Signatures {
    private static final String TAG = "Signatures";

    public static List<AppSignature> getSignatureList(PackageInfo packageInfo) {
        Signature[] signatureArr;
        LinkedList linkedList = new LinkedList();
        String[] strArr = packageInfo.requestedPermissions;
        if (strArr != null) {
            linkedList.add(new AppSignature(h.a(Permissions.getPermissionBytes(strArr))));
        }
        for (Signature signature : packageInfo.signatures) {
            try {
                MessageDigest instance = MessageDigest.getInstance("SHA-1");
                instance.update(signature.toByteArray());
                linkedList.add(new AppSignature(h.a(instance.digest())));
                CertificateFactory instance2 = CertificateFactory.getInstance("X.509");
                if (instance2 != null) {
                    X509Certificate x509Certificate = (X509Certificate) instance2.generateCertificate(new ByteArrayInputStream(signature.toByteArray()));
                    if (x509Certificate != null) {
                        PublicKey publicKey = x509Certificate.getPublicKey();
                        if (publicKey != null) {
                            String algorithm = publicKey.getAlgorithm();
                            char c = 65535;
                            switch (algorithm.hashCode()) {
                                case 67986:
                                    if (algorithm.equals("DSA")) {
                                        c = 1;
                                        break;
                                    }
                                    break;
                                case 81440:
                                    if (algorithm.equals("RSA")) {
                                        c = 0;
                                        break;
                                    }
                                    break;
                            }
                            switch (c) {
                                case 0:
                                    MessageDigest instance3 = MessageDigest.getInstance("SHA-256");
                                    byte[] byteArray = ((RSAPublicKey) publicKey).getModulus().toByteArray();
                                    if (byteArray[0] == 0) {
                                        byte[] bArr = new byte[(byteArray.length - 1)];
                                        System.arraycopy(byteArray, 1, bArr, 0, byteArray.length - 1);
                                        instance3.update(bArr);
                                    } else {
                                        instance3.update(byteArray);
                                    }
                                    linkedList.add(new AppSignature(h.a(instance3.digest())));
                                    break;
                                case 1:
                                    DSAPublicKey dSAPublicKey = (DSAPublicKey) publicKey;
                                    MessageDigest instance4 = MessageDigest.getInstance("SHA-256");
                                    byte[] byteArray2 = dSAPublicKey.getY().toByteArray();
                                    if (byteArray2[0] == 0) {
                                        byte[] bArr2 = new byte[(byteArray2.length - 1)];
                                        System.arraycopy(byteArray2, 1, bArr2, 0, byteArray2.length - 1);
                                        instance4.update(bArr2);
                                    } else {
                                        instance4.update(byteArray2);
                                    }
                                    linkedList.add(new AppSignature(h.a(instance4.digest())));
                                    break;
                                default:
                                    c.c(TAG, "Weird algorithm: " + algorithm + " for " + packageInfo.packageName);
                                    break;
                            }
                        }
                    }
                }
            } catch (NoSuchAlgorithmException e) {
                e = e;
                e.printStackTrace();
            } catch (CertificateException e2) {
                e = e2;
                e.printStackTrace();
            }
        }
        return linkedList;
    }
}
