package com.hmatalonga.greenhub.models;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.Context;
import com.hmatalonga.greenhub.c.c;
import java.lang.ref.WeakReference;
import java.util.List;

public class Service {
    private static final String TAG = c.a(Service.class);
    private static WeakReference<List<RunningServiceInfo>> runningServiceInfo = null;

    public static void clear() {
        runningServiceInfo = null;
    }

    public static List<RunningServiceInfo> getRunningServiceInfo(Context context) {
        if (runningServiceInfo != null && runningServiceInfo.get() != null) {
            return (List) runningServiceInfo.get();
        }
        List<RunningServiceInfo> runningServices = ((ActivityManager) context.getSystemService("activity")).getRunningServices(255);
        runningServiceInfo = new WeakReference<>(runningServices);
        return runningServices;
    }
}
