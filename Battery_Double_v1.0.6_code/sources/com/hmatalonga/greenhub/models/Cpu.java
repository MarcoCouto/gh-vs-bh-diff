package com.hmatalonga.greenhub.models;

import android.os.SystemClock;
import com.hmatalonga.greenhub.c.c;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.concurrent.TimeUnit;

public class Cpu {
    private static final String TAG = c.a(Cpu.class);

    public static long getSleepTime() {
        long elapsedRealtime = SystemClock.elapsedRealtime() - SystemClock.uptimeMillis();
        if (elapsedRealtime < 0) {
            return 0;
        }
        return TimeUnit.MILLISECONDS.toSeconds(elapsedRealtime);
    }

    public static long getUptime() {
        return TimeUnit.MILLISECONDS.toSeconds(SystemClock.elapsedRealtime());
    }

    public static double getUsage(long[] jArr, long[] jArr2) {
        if (jArr == null || jArr2 == null || jArr.length < 2 || jArr2.length < 2) {
            return 0.0d;
        }
        return ((double) (jArr2[1] - jArr[1])) / ((double) ((jArr2[0] + jArr2[1]) - (jArr[0] + jArr[1])));
    }

    public static synchronized long[] readUsagePoint() {
        long j;
        long j2;
        long j3 = 0;
        synchronized (Cpu.class) {
            try {
                RandomAccessFile randomAccessFile = new RandomAccessFile("/proc/stat", "r");
                String[] split = randomAccessFile.readLine().split(" ");
                j2 = 0;
                for (int i = 2; i <= 8; i++) {
                    if (i == 5) {
                        try {
                            j3 = Long.parseLong(split[i]);
                        } catch (IOException e) {
                            IOException iOException = e;
                            j = j3;
                            j3 = j2;
                            e = iOException;
                            e.printStackTrace();
                            j2 = j3;
                            j3 = j;
                            long[] jArr = {j3, j2};
                            return jArr;
                        }
                    } else {
                        j2 += Long.parseLong(split[i]);
                    }
                }
                randomAccessFile.close();
            } catch (IOException e2) {
                e = e2;
                j = 0;
                e.printStackTrace();
                j2 = j3;
                j3 = j;
                long[] jArr2 = {j3, j2};
                return jArr2;
            }
            long[] jArr22 = {j3, j2};
        }
        return jArr22;
    }
}
