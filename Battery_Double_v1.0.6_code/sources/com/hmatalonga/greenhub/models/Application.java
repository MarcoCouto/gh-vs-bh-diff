package com.hmatalonga.greenhub.models;

import android.app.ActivityManager.RunningAppProcessInfo;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.Context;
import com.hmatalonga.greenhub.c.c;
import com.hmatalonga.greenhub.c.h;
import com.hmatalonga.greenhub.models.data.ProcessInfo;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

public class Application {
    private static final String TAG = c.a(Application.class);

    public static String getAppPriority(Context context, String str) {
        int i;
        List<RunningAppProcessInfo> runningProcessInfo = Process.getRunningProcessInfo(context);
        int i2 = Integer.MAX_VALUE;
        Iterator it = Service.getRunningServiceInfo(context).iterator();
        while (true) {
            i = i2;
            if (!it.hasNext()) {
                break;
            }
            i2 = ((RunningServiceInfo) it.next()).service.getPackageName().equals(str) ? 300 : i;
        }
        for (RunningAppProcessInfo runningAppProcessInfo : runningProcessInfo) {
            if (Arrays.asList(runningAppProcessInfo.pkgList).contains(str) && runningAppProcessInfo.importance < i) {
                i = runningAppProcessInfo.importance;
            }
        }
        return h.a(context, h.a(i));
    }

    public static ArrayList<ProcessInfo> getRunningAppInfo(Context context) {
        List<RunningAppProcessInfo> runningProcessInfo = Process.getRunningProcessInfo(context);
        List<RunningServiceInfo> runningServiceInfo = Service.getRunningServiceInfo(context);
        HashSet hashSet = new HashSet();
        ArrayList<ProcessInfo> arrayList = new ArrayList<>();
        if (runningProcessInfo != null) {
            for (RunningAppProcessInfo runningAppProcessInfo : runningProcessInfo) {
                if (runningAppProcessInfo != null && !hashSet.contains(runningAppProcessInfo.processName)) {
                    hashSet.add(runningAppProcessInfo.processName);
                    ProcessInfo processInfo = new ProcessInfo();
                    processInfo.realmSet$importance(h.a(runningAppProcessInfo.importance));
                    processInfo.realmSet$processId(runningAppProcessInfo.pid);
                    processInfo.realmSet$name(runningAppProcessInfo.processName);
                    arrayList.add(processInfo);
                }
            }
        }
        if (runningServiceInfo != null) {
            for (RunningServiceInfo runningServiceInfo2 : runningServiceInfo) {
                if (runningServiceInfo2 != null && !hashSet.contains(runningServiceInfo2.process)) {
                    hashSet.add(runningServiceInfo2.process);
                    ProcessInfo processInfo2 = new ProcessInfo();
                    processInfo2.realmSet$importance(runningServiceInfo2.foreground ? "Foreground app" : "Service");
                    processInfo2.realmSet$processId(runningServiceInfo2.pid);
                    processInfo2.realmSet$applicationLabel(runningServiceInfo2.service.flattenToString());
                    processInfo2.realmSet$name(runningServiceInfo2.process);
                    arrayList.add(processInfo2);
                }
            }
        }
        return arrayList;
    }

    public static boolean isRunning(Context context, String str) {
        List<RunningAppProcessInfo> runningProcessInfo = Process.getRunningProcessInfo(context);
        List<RunningServiceInfo> runningServiceInfo = Service.getRunningServiceInfo(context);
        for (RunningAppProcessInfo runningAppProcessInfo : runningProcessInfo) {
            if (runningAppProcessInfo.processName.equals(str) && runningAppProcessInfo.importance != 500) {
                return true;
            }
        }
        for (RunningServiceInfo runningServiceInfo2 : runningServiceInfo) {
            if (h.a(runningServiceInfo2.process).equals(str)) {
                return true;
            }
        }
        return false;
    }
}
