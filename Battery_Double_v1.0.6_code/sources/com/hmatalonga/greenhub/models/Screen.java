package com.hmatalonga.greenhub.models;

import android.content.Context;
import android.os.Build.VERSION;
import android.os.PowerManager;
import android.provider.Settings.SettingNotFoundException;
import android.provider.Settings.System;

public class Screen {
    private static final String TAG = "Screen";

    public static int getBrightness(Context context) {
        boolean z = false;
        try {
            return System.getInt(context.getContentResolver(), "screen_brightness");
        } catch (SettingNotFoundException e) {
            e.printStackTrace();
            return z;
        }
    }

    public static boolean isAutoBrightness(Context context) {
        try {
            return System.getInt(context.getContentResolver(), "screen_brightness_mode") == 1;
        } catch (SettingNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static int isOn(Context context) {
        int i = 1;
        PowerManager powerManager = (PowerManager) context.getSystemService("power");
        if (powerManager == null) {
            return 0;
        }
        if (VERSION.SDK_INT >= 20) {
            return powerManager.isInteractive() ? 1 : 0;
        }
        if (!powerManager.isScreenOn()) {
            i = 0;
        }
        return i;
    }
}
