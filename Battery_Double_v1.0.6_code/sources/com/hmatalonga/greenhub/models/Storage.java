package com.hmatalonga.greenhub.models;

import android.os.Build.VERSION;
import android.os.Environment;
import android.os.StatFs;
import com.hmatalonga.greenhub.models.data.StorageDetails;
import java.io.File;

public class Storage {
    private static final String TAG = "Storage";

    private static long[] getExternalStorageDetails() {
        File storagePathFromEnv = getStoragePathFromEnv("EXTERNAL_STORAGE");
        if (storagePathFromEnv != null && storagePathFromEnv.exists()) {
            long[] storageDetailsForPath = getStorageDetailsForPath(storagePathFromEnv);
            if (storageDetailsForPath.length == 2) {
                return storageDetailsForPath;
            }
        }
        if (!isExternalStorageRemovable() || isExternalStorageEmulated()) {
            File externalStorageDirectory = Environment.getExternalStorageDirectory();
            if (externalStorageDirectory != null && externalStorageDirectory.exists()) {
                return getStorageDetailsForPath(externalStorageDirectory);
            }
        }
        return new long[0];
    }

    private static long[] getSecondaryStorageDetails() {
        File storagePathFromEnv = getStoragePathFromEnv("SECONDARY_STORAGE");
        if (storagePathFromEnv != null && storagePathFromEnv.exists()) {
            return getStorageDetailsForPath(storagePathFromEnv);
        }
        if (isExternalStorageRemovable() && !isExternalStorageEmulated()) {
            File externalStorageDirectory = Environment.getExternalStorageDirectory();
            if (externalStorageDirectory != null && externalStorageDirectory.exists()) {
                return getStorageDetailsForPath(externalStorageDirectory);
            }
        }
        return new long[0];
    }

    public static StorageDetails getStorageDetails() {
        StorageDetails storageDetails = new StorageDetails();
        long[] storageDetailsForPath = getStorageDetailsForPath(Environment.getDataDirectory());
        if (storageDetailsForPath.length == 2) {
            storageDetails.realmSet$free((int) storageDetailsForPath[0]);
            storageDetails.realmSet$total((int) storageDetailsForPath[1]);
        }
        long[] externalStorageDetails = getExternalStorageDetails();
        if (externalStorageDetails.length == 2) {
            storageDetails.realmSet$freeExternal((int) externalStorageDetails[0]);
            storageDetails.realmSet$totalExternal((int) externalStorageDetails[1]);
        }
        long[] secondaryStorageDetails = getSecondaryStorageDetails();
        if (secondaryStorageDetails.length == 2) {
            storageDetails.realmSet$freeSecondary((int) secondaryStorageDetails[0]);
            storageDetails.realmSet$totalSecondary((int) secondaryStorageDetails[1]);
        }
        long[] storageDetailsForPath2 = getStorageDetailsForPath(Environment.getRootDirectory());
        if (storageDetailsForPath2.length == 2) {
            storageDetails.realmSet$freeSystem((int) storageDetailsForPath2[0]);
            storageDetails.realmSet$totalSystem((int) storageDetailsForPath2[1]);
        }
        return storageDetails;
    }

    @Deprecated
    private static long[] getStorageDetailsForPath(File file) {
        if (file == null) {
            return new long[0];
        }
        try {
            StatFs statFs = new StatFs(file.getAbsolutePath());
            if (VERSION.SDK_INT >= 18) {
                return new long[]{statFs.getAvailableBytes() / 1048576, statFs.getTotalBytes() / 1048576};
            }
            long blockSize = (long) statFs.getBlockSize();
            long availableBlocks = (((long) statFs.getAvailableBlocks()) * blockSize) / 1048576;
            long blockCount = (((long) statFs.getBlockCount()) * blockSize) / 1048576;
            if (availableBlocks < 0 || blockCount < 0) {
                return new long[0];
            }
            return new long[]{availableBlocks, blockCount};
        } catch (Exception e) {
            return new long[0];
        }
    }

    private static File getStoragePathFromEnv(String str) {
        try {
            return new File(System.getenv(str));
        } catch (Exception e) {
            return null;
        }
    }

    private static boolean isExternalStorageEmulated() {
        return VERSION.SDK_INT >= 11 && Environment.isExternalStorageEmulated();
    }

    private static boolean isExternalStorageRemovable() {
        return VERSION.SDK_INT >= 9 && Environment.isExternalStorageRemovable();
    }
}
