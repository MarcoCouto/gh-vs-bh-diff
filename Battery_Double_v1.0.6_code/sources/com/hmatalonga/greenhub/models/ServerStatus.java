package com.hmatalonga.greenhub.models;

public class ServerStatus {
    private static final String TAG = "ServerStatus";
    public final String server;
    public final int version;

    public ServerStatus(String str, int i) {
        this.server = str;
        this.version = i;
    }
}
