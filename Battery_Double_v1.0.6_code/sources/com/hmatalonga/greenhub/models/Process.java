package com.hmatalonga.greenhub.models;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.Context;
import android.content.SharedPreferences.Editor;
import com.hmatalonga.greenhub.Config;
import com.hmatalonga.greenhub.c.c;
import com.hmatalonga.greenhub.c.h;
import com.hmatalonga.greenhub.models.data.AppSignature;
import com.hmatalonga.greenhub.models.data.ProcessInfo;
import java.lang.ref.WeakReference;
import java.util.List;

public class Process {
    private static final String TAG = c.a(Process.class);
    private static WeakReference<List<RunningAppProcessInfo>> runningAppInfo = null;

    public static void clear() {
        runningAppInfo = null;
    }

    public static ProcessInfo disabledItem(String str, String str2, Editor editor) {
        ProcessInfo processInfo = new ProcessInfo();
        processInfo.realmSet$name(str);
        processInfo.realmSet$processId(-1);
        processInfo.realmSet$importance(Config.IMPORTANCE_DISABLED);
        editor.remove(str2);
        return processInfo;
    }

    public static List<RunningAppProcessInfo> getRunningProcessInfo(Context context) {
        if (runningAppInfo != null && runningAppInfo.get() != null) {
            return (List) runningAppInfo.get();
        }
        List<RunningAppProcessInfo> runningAppProcesses = ((ActivityManager) context.getSystemService("activity")).getRunningAppProcesses();
        for (RunningAppProcessInfo runningAppProcessInfo : runningAppProcesses) {
            if (!(runningAppProcessInfo == null || runningAppProcessInfo.processName == null)) {
                runningAppProcessInfo.processName = h.a(runningAppProcessInfo.processName);
            }
        }
        runningAppInfo = new WeakReference<>(runningAppProcesses);
        return runningAppProcesses;
    }

    public static ProcessInfo uninstalledItem(String str, String str2, Editor editor) {
        ProcessInfo processInfo = new ProcessInfo();
        processInfo.realmSet$name(str);
        processInfo.realmGet$appSignatures().add(new AppSignature(Config.IMPORTANCE_UNINSTALLED));
        processInfo.realmSet$processId(-1);
        processInfo.realmSet$importance(Config.IMPORTANCE_UNINSTALLED);
        editor.remove(str2);
        return processInfo;
    }
}
