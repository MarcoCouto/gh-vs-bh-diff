package com.hmatalonga.greenhub.models;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.Build.VERSION;
import com.hmatalonga.greenhub.Config;
import com.hmatalonga.greenhub.c.c;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.lang.reflect.Method;

public class Battery {
    private static final String TAG = c.a(Battery.class);

    public static int getActualBatteryCapacity(Context context) {
        Object obj;
        Exception e;
        int i;
        String str = "com.android.internal.os.PowerProfile";
        try {
            obj = Class.forName("com.android.internal.os.PowerProfile").getConstructor(new Class[]{Context.class}).newInstance(new Object[]{context});
        } catch (Exception e2) {
            e2.printStackTrace();
            obj = null;
        }
        try {
            i = ((Integer) Class.forName("com.android.internal.os.PowerProfile").getMethod("getAveragePower", new Class[]{String.class}).invoke(obj, new Object[]{"battery.capacity"})).intValue();
            try {
                c.b(TAG, i + " mah");
            } catch (Exception e3) {
                e = e3;
                e.printStackTrace();
                return i;
            }
        } catch (Exception e4) {
            Exception exc = e4;
            i = 0;
            e = exc;
        }
        return i;
    }

    public static int getBatteryAveragePower(Context context) {
        int i = 0;
        Intent registerReceiver = context.registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
        if (registerReceiver == null) {
            return -1;
        }
        int intExtra = registerReceiver.getIntExtra("voltage", 0);
        if (VERSION.SDK_INT >= 21) {
            i = ((BatteryManager) context.getSystemService("batterymanager")).getIntProperty(3);
        }
        return (i * intExtra) / 1000000000;
    }

    public static int getBatteryCapacity(Context context) {
        if (VERSION.SDK_INT >= 21) {
            int intProperty = ((BatteryManager) context.getSystemService("batterymanager")).getIntProperty(4);
            if (intProperty != Integer.MIN_VALUE) {
                return intProperty;
            }
            return -1;
        }
        try {
            Class cls = Class.forName("com.android.internal.os.PowerProfile");
            Object newInstance = cls.getConstructor(new Class[]{Context.class}).newInstance(new Object[]{context});
            Method method = cls.getMethod("getAveragePower", new Class[]{String.class});
            method.setAccessible(true);
            return (int) Double.parseDouble(method.invoke(newInstance, new Object[]{"battery.capacity"}).toString());
        } catch (Throwable th) {
            th.printStackTrace();
            return -1;
        }
    }

    public static double getBatteryCapacityConsumed(double d, Context context) {
        int i = 0;
        if (VERSION.SDK_INT >= 21) {
            i = ((BatteryManager) context.getSystemService("batterymanager")).getIntProperty(3);
        }
        return (((double) i) * d) / 1000.0d;
    }

    public static int getBatteryChargeCounter(Context context) {
        if (VERSION.SDK_INT >= 21) {
            return ((BatteryManager) context.getSystemService("batterymanager")).getIntProperty(1);
        }
        return -1;
    }

    public static int getBatteryCurrentAverage(Context context) {
        int i = VERSION.SDK_INT >= 21 ? ((BatteryManager) context.getSystemService("batterymanager")).getIntProperty(3) : -1;
        if (i != Integer.MIN_VALUE) {
            return i;
        }
        return -1;
    }

    public static int getBatteryCurrentNow(Context context) {
        int batteryCurrentNowLegacy = VERSION.SDK_INT >= 21 ? ((BatteryManager) context.getSystemService("batterymanager")).getIntProperty(2) : getBatteryCurrentNowLegacy();
        if (batteryCurrentNowLegacy != Integer.MIN_VALUE) {
            return (-batteryCurrentNowLegacy) / 1000;
        }
        return -1;
    }

    private static int getBatteryCurrentNowLegacy() {
        int i = -1;
        try {
            RandomAccessFile randomAccessFile = new RandomAccessFile(Config.BATTERY_SOURCE_DEFAULT, "r");
            i = Integer.parseInt(randomAccessFile.readLine());
            randomAccessFile.close();
        } catch (IOException e) {
            c.b(TAG, "Device has no current_avg file available");
        }
        return -i;
    }

    public static long getBatteryEnergyCounter(Context context) {
        long j = VERSION.SDK_INT >= 21 ? ((BatteryManager) context.getSystemService("batterymanager")).getLongProperty(5) : -1;
        if (j != Long.MIN_VALUE) {
            return j;
        }
        return -1;
    }

    public static double getBatteryVoltage(Context context) {
        Intent registerReceiver = context.registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
        if (registerReceiver == null) {
            return -1.0d;
        }
        double intExtra = (double) registerReceiver.getIntExtra("voltage", 0);
        return intExtra != -1.0d ? intExtra / 1000.0d : intExtra;
    }
}
