package com.hmatalonga.greenhub.models;

import android.content.Context;
import android.os.Build.VERSION;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import com.hmatalonga.greenhub.c.c;
import com.hmatalonga.greenhub.c.f;
import java.lang.reflect.Method;
import java.util.List;

public class SimCard {
    private static final String TAG = "SimCard";

    public static String getSIMOperator(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        String sIMOperators = getSIMOperators(context);
        if (sIMOperators != null && sIMOperators.length() > 0) {
            return sIMOperators;
        }
        String simOperatorName = telephonyManager.getSimOperatorName();
        return (simOperatorName == null || simOperatorName.length() <= 0) ? Network.TYPE_UNKNOWN : simOperatorName;
    }

    private static String getSIMOperators(Context context) {
        String str = "";
        if (!f.a(context, "android.permission.READ_PHONE_STATE") || VERSION.SDK_INT <= 22) {
            return str;
        }
        List<SubscriptionInfo> activeSubscriptionInfoList = SubscriptionManager.from(context).getActiveSubscriptionInfoList();
        if (activeSubscriptionInfoList == null || activeSubscriptionInfoList.size() <= 0) {
            return str;
        }
        for (SubscriptionInfo subscriptionId : activeSubscriptionInfoList) {
            String simOperatorNameForSubscription = getSimOperatorNameForSubscription(context, subscriptionId.getSubscriptionId());
            str = (simOperatorNameForSubscription == null || simOperatorNameForSubscription.length() <= 0) ? str : str + simOperatorNameForSubscription + ";";
        }
        return str.length() > 1 ? str.substring(0, str.length() - 1) : str;
    }

    private static String getSimOperatorNameForSubscription(Context context, int i) {
        try {
            Method method = Class.forName(((TelephonyManager) context.getSystemService("phone")).getClass().getName()).getMethod("getSimOperatorNameForSubscription", new Class[]{Integer.TYPE});
            method.setAccessible(true);
            return (String) method.invoke(context, new Object[]{Integer.valueOf(i)});
        } catch (Exception e) {
            if (!(e == null || e.getLocalizedMessage() == null)) {
                c.a(TAG, "Failed getting sim operator with subid: " + e.getLocalizedMessage());
            }
            return null;
        }
    }
}
