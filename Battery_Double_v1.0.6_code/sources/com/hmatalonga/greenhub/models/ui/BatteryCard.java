package com.hmatalonga.greenhub.models.ui;

public class BatteryCard {
    public int icon;
    public int indicator;
    public String label;
    public String value;

    public BatteryCard(int i, String str, String str2) {
        this.icon = i;
        this.label = str;
        this.value = str2;
        this.indicator = -16711936;
    }

    public BatteryCard(int i, String str, String str2, int i2) {
        this.icon = i;
        this.label = str;
        this.value = str2;
        this.indicator = i2;
    }
}
