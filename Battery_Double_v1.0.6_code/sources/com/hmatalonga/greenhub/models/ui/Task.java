package com.hmatalonga.greenhub.models.ui;

import android.content.pm.PackageInfo;
import java.util.TreeSet;

public class Task {
    private boolean mHasBackgroundService;
    private boolean mIsAutoStart;
    private boolean mIsChecked = true;
    private String mLabel;
    private double mMemory;
    private String mName;
    private PackageInfo mPackageInfo;
    private TreeSet<Integer> mProcesses = new TreeSet<>();
    private int mUid;

    public Task(int i, String str) {
        this.mUid = i;
        this.mName = str;
    }

    public String getLabel() {
        return this.mLabel;
    }

    public double getMemory() {
        return this.mMemory;
    }

    public String getName() {
        return this.mName;
    }

    public PackageInfo getPackageInfo() {
        return this.mPackageInfo;
    }

    public TreeSet<Integer> getProcesses() {
        return this.mProcesses;
    }

    public int getUid() {
        return this.mUid;
    }

    public boolean hasBackgroundService() {
        return this.mHasBackgroundService;
    }

    public boolean isAutoStart() {
        return this.mIsAutoStart;
    }

    public boolean isChecked() {
        return this.mIsChecked;
    }

    public void setHasBackgroundService(boolean z) {
        this.mHasBackgroundService = z;
    }

    public void setIsAutoStart(boolean z) {
        this.mIsAutoStart = z;
    }

    public void setIsChecked(boolean z) {
        this.mIsChecked = z;
    }

    public void setLabel(String str) {
        this.mLabel = str;
    }

    public void setMemory(double d) {
        this.mMemory = ((double) Math.round(d * 100.0d)) / 100.0d;
    }

    public void setName(String str) {
        this.mName = str;
    }

    public void setPackageInfo(PackageInfo packageInfo) {
        this.mPackageInfo = packageInfo;
    }
}
