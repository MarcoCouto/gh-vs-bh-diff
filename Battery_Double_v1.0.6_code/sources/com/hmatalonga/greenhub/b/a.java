package com.hmatalonga.greenhub.b;

import android.content.Context;
import android.os.AsyncTask;
import com.google.a.o;
import com.hmatalonga.greenhub.c.e;
import com.hmatalonga.greenhub.c.g;
import com.hmatalonga.greenhub.events.StatusEvent;
import com.hmatalonga.greenhub.models.Specifications;
import com.hmatalonga.greenhub.models.data.Message;
import com.hmatalonga.greenhub.network.services.GreenHubAPIService;
import com.mansoon.BatteryDouble.R;
import io.realm.bb;
import io.realm.exceptions.RealmPrimaryKeyConstraintException;
import java.util.Iterator;
import java.util.List;
import org.greenrobot.eventbus.c;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit.Builder;
import retrofit2.converter.gson.GsonConverterFactory;

public class a extends AsyncTask<Context, Void, Void> {
    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Void doInBackground(final Context... contextArr) {
        ((GreenHubAPIService) new Builder().baseUrl(g.c(contextArr[0])).addConverterFactory(GsonConverterFactory.create()).build().create(GreenHubAPIService.class)).getMessages(Specifications.getAndroidId(contextArr[0]), g.u(contextArr[0])).enqueue(new Callback<List<o>>() {
            public void onFailure(Call<List<o>> call, Throwable th) {
                th.printStackTrace();
                c.a().c(new StatusEvent(contextArr[0].getString(R.string.event_server_not_responding)));
            }

            public void onResponse(Call<List<o>> call, Response<List<o>> response) {
                Message message;
                if (response == null) {
                    c.a().c(new StatusEvent(contextArr[0].getString(R.string.event_server_response_failed)));
                } else if (response.body() != null && ((List) response.body()).size() > 0) {
                    bb m = bb.m();
                    Message message2 = null;
                    Iterator it = ((List) response.body()).iterator();
                    while (true) {
                        message = message2;
                        if (!it.hasNext()) {
                            break;
                        }
                        o oVar = (o) it.next();
                        message2 = new Message(oVar.a("id").e(), oVar.a("title").b(), oVar.a("body").b(), oVar.a("created_at").b());
                        if (m.a(Message.class).a("id", Integer.valueOf(message2.realmGet$id())).a() == 0) {
                            try {
                                m.b();
                                m.a(message2);
                                m.c();
                            } catch (RealmPrimaryKeyConstraintException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    m.close();
                    if (message != null) {
                        g.a(contextArr[0], message.realmGet$id());
                    }
                    if (g.t(contextArr[0])) {
                        e.c(contextArr[0]);
                    }
                }
            }
        });
        return null;
    }
}
