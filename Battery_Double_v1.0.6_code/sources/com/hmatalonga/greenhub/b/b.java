package com.hmatalonga.greenhub.b;

import android.os.AsyncTask;
import com.hmatalonga.greenhub.models.data.Sample;
import io.realm.bb;
import io.realm.bb.a;

public class b extends AsyncTask<Integer, Void, Void> {
    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Void doInBackground(Integer... numArr) {
        bb m = bb.m();
        try {
            final int intValue = numArr[0].intValue();
            m.a((a) new a() {
                public void a(bb bbVar) {
                    Sample sample = (Sample) bbVar.a(Sample.class).a("id", Integer.valueOf(intValue)).c();
                    if (sample != null) {
                        sample.deleteFromRealm();
                    }
                }
            });
            m.close();
            return null;
        } catch (Throwable th) {
            m.close();
            throw th;
        }
    }
}
