package com.hmatalonga.greenhub.b;

import android.os.AsyncTask;
import com.hmatalonga.greenhub.models.data.BatterySession;
import io.realm.bb;
import io.realm.bb.a;
import io.realm.bn;

public class c extends AsyncTask<Integer, Void, Boolean> {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public boolean f4265a;

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Boolean doInBackground(Integer... numArr) {
        this.f4265a = false;
        bb m = bb.m();
        try {
            final int intValue = numArr[0].intValue();
            m.a((a) new a() {
                public void a(bb bbVar) {
                    bn b2 = bbVar.a(BatterySession.class).a("timestamp", com.hmatalonga.greenhub.c.a.a(intValue)).b();
                    if (b2 != null && !b2.isEmpty()) {
                        c.this.f4265a = b2.b();
                    }
                }
            });
            m.close();
            return Boolean.valueOf(this.f4265a);
        } catch (Throwable th) {
            m.close();
            throw th;
        }
    }
}
