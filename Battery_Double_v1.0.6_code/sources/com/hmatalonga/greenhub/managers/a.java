package com.hmatalonga.greenhub.managers;

import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.app.AppOpsManager;
import android.app.usage.UsageStats;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ServiceInfo;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.os.Process;
import com.b.a.a.a.g;
import com.hmatalonga.greenhub.managers.sampling.b;
import com.hmatalonga.greenhub.models.ui.Task;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public class a {

    /* renamed from: a reason: collision with root package name */
    private Context f4278a;

    /* renamed from: b reason: collision with root package name */
    private PackageManager f4279b;

    public a(Context context) {
        this.f4279b = context.getPackageManager();
        this.f4278a = context;
    }

    private double a(com.b.a.a.a.a aVar) {
        double d = 0.0d;
        try {
            g d2 = aVar.d();
            if (d2 != null) {
                d = (((double) d2.a()) / 1024.0d) / 1024.0d;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ((double) Math.round(d * 100.0d)) / 100.0d;
    }

    private PackageInfo a(com.b.a.a.a.a aVar, int i) {
        try {
            return aVar.a(this.f4278a, i);
        } catch (NameNotFoundException e) {
            return null;
        }
    }

    @TargetApi(21)
    private PackageInfo a(String str, int i) {
        try {
            return this.f4279b.getPackageInfo(str, i);
        } catch (NameNotFoundException e) {
            return null;
        }
    }

    private Task a(List<Task> list, int i) {
        for (Task task : list) {
            if (task.getUid() == i) {
                return task;
            }
        }
        return null;
    }

    private boolean a(String str) {
        try {
            PackageInfo packageInfo = this.f4279b.getPackageInfo(str, 4096);
            if (packageInfo.requestedPermissions == null) {
                return false;
            }
            for (String equals : packageInfo.requestedPermissions) {
                if (equals.equals("android.permission.RECEIVE_BOOT_COMPLETED")) {
                    return true;
                }
            }
            return false;
        } catch (NameNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }

    private List<Task> b() {
        ArrayList arrayList = new ArrayList();
        List<com.b.a.a.a.a> a2 = com.b.a.a.a.a();
        if (a2 == null) {
            return arrayList;
        }
        for (com.b.a.a.a.a aVar : a2) {
            if (!aVar.c.equals("com.mansoon.BatteryDouble")) {
                PackageInfo a3 = a(aVar, 0);
                if (a3 != null && ((!b(a3) || !com.hmatalonga.greenhub.c.g.v(this.f4278a)) && a3.applicationInfo != null)) {
                    String charSequence = a3.applicationInfo.loadLabel(this.f4279b).toString();
                    if (!charSequence.isEmpty()) {
                        Task a4 = a((List<Task>) arrayList, aVar.f1414b);
                        if (a4 == null) {
                            Task task = new Task(aVar.f1414b, aVar.c);
                            task.setPackageInfo(a3);
                            task.setLabel(charSequence);
                            task.setMemory(a(aVar));
                            task.setIsAutoStart(a(aVar.a()));
                            task.setHasBackgroundService(b(aVar.a()));
                            task.getProcesses().add(Integer.valueOf(aVar.d));
                            arrayList.add(task);
                        } else {
                            a4.getProcesses().add(Integer.valueOf(aVar.d));
                            a4.setMemory(a4.getMemory() + a(aVar));
                        }
                    }
                }
            }
        }
        if (!arrayList.isEmpty()) {
            Collections.sort(arrayList, new Comparator<Task>() {
                /* renamed from: a */
                public int compare(Task task, Task task2) {
                    return task.getLabel().compareTo(task2.getLabel());
                }
            });
        }
        return arrayList;
    }

    private boolean b(PackageInfo packageInfo) {
        return (packageInfo == null || (packageInfo.applicationInfo.flags & 1) == 0) ? false : true;
    }

    private boolean b(String str) {
        try {
            PackageInfo packageInfo = this.f4279b.getPackageInfo(str, 4);
            if (packageInfo.services == null) {
                return false;
            }
            for (ServiceInfo serviceInfo : packageInfo.services) {
                if (serviceInfo.exported) {
                    return true;
                }
            }
            return false;
        } catch (NameNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }

    @TargetApi(21)
    private List<Task> c() {
        ArrayList arrayList = new ArrayList();
        if (!(((AppOpsManager) this.f4278a.getSystemService("appops")).checkOpNoThrow("android:get_usage_stats", Process.myUid(), this.f4278a.getPackageName()) == 0)) {
            return arrayList;
        }
        for (UsageStats packageName : b.a(this.f4278a)) {
            String packageName2 = packageName.getPackageName();
            if (!packageName2.equals("com.mansoon.BatteryDouble")) {
                PackageInfo a2 = a(packageName2, 0);
                if (a2 != null && (!b(a2) || !com.hmatalonga.greenhub.c.g.v(this.f4278a))) {
                    String charSequence = a2.applicationInfo.loadLabel(this.f4279b).toString();
                    if (!charSequence.isEmpty()) {
                        int i = a2.applicationInfo.uid;
                        if (a((List<Task>) arrayList, i) == null) {
                            Task task = new Task(i, a2.applicationInfo.processName);
                            task.setPackageInfo(a2);
                            task.setLabel(charSequence);
                            task.setIsAutoStart(a(packageName2));
                            task.setHasBackgroundService(b(packageName2));
                            arrayList.add(task);
                        }
                    }
                }
            }
        }
        Collections.sort(arrayList, new Comparator<Task>() {
            /* renamed from: a */
            public int compare(Task task, Task task2) {
                return task.getLabel().compareTo(task2.getLabel());
            }
        });
        return arrayList;
    }

    public Drawable a(PackageInfo packageInfo) {
        try {
            return this.f4279b.getApplicationIcon(packageInfo.packageName);
        } catch (NameNotFoundException e) {
            return packageInfo.applicationInfo.loadIcon(this.f4279b);
        }
    }

    public List<Task> a() {
        return VERSION.SDK_INT < 24 ? b() : c();
    }

    public void a(Task task) {
        ((ActivityManager) this.f4278a.getSystemService("activity")).killBackgroundProcesses(task.getPackageInfo().packageName);
        Iterator it = task.getProcesses().iterator();
        while (it.hasNext()) {
            Process.killProcess(((Integer) it.next()).intValue());
        }
    }
}
