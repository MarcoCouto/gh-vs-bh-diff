package com.hmatalonga.greenhub.managers.sampling;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import com.hmatalonga.greenhub.b.f;
import com.hmatalonga.greenhub.c.c;
import com.hmatalonga.greenhub.c.e;
import com.hmatalonga.greenhub.c.g;
import com.hmatalonga.greenhub.events.StatusEvent;
import com.hmatalonga.greenhub.managers.a.a;
import com.hmatalonga.greenhub.models.data.BatteryUsage;
import com.hmatalonga.greenhub.models.data.Sample;
import com.mansoon.BatteryDouble.R;

public class DataEstimatorService extends IntentService {

    /* renamed from: a reason: collision with root package name */
    private static final String f4285a = c.a(DataEstimatorService.class);

    /* renamed from: b reason: collision with root package name */
    private double f4286b;

    public DataEstimatorService() {
        super(f4285a);
    }

    private void a(Context context, Intent intent, a aVar, boolean z) {
        if (z) {
            intent.putExtras(DataEstimator.getBatteryChangedIntent(context).getExtras());
        }
        BatteryUsage a2 = a.a(context, intent);
        if (a2 != null && !a2.realmGet$state().equals("Unknown") && a2.realmGet$level() >= 0.0f) {
            aVar.a(a2);
            c.b(f4285a, "Took usage details " + a2.realmGet$id() + " for " + intent.getAction());
        }
    }

    private void a(Context context, Intent intent, Sample sample, a aVar) {
        Sample a2 = a.a(context, intent, sample != null ? sample.realmGet$batteryState() : "Unknown");
        if (a2 != null) {
            a2.realmSet$distanceTraveled(this.f4286b);
            this.f4286b = 0.0d;
        }
        if (a2 != null && !a2.realmGet$batteryState().equals("Unknown") && a2.realmGet$batteryLevel() >= 0.0d) {
            aVar.a(a2);
            c.b(f4285a, "Took sample " + a2.realmGet$id() + " for " + intent.getAction());
        }
        org.greenrobot.eventbus.c.a().c(new StatusEvent(context.getString(R.string.event_idle)));
    }

    private void a(Intent intent, Context context) {
        this.f4286b = intent.getDoubleExtra("distance", 0.0d);
        if (a.b() > 0.0d) {
            a aVar = new a();
            Sample d = aVar.d();
            if (intent.getAction().equals("android.intent.action.SCREEN_ON") || intent.getAction().equals("android.intent.action.SCREEN_OFF")) {
                c.b(f4285a, "Getting new usage details");
                a(context, intent, aVar, true);
                aVar.b();
                return;
            }
            if (d != null) {
                a.a(d.realmGet$batteryLevel());
            }
            boolean z = a.a() != a.b();
            if (z) {
                c.b(f4285a, "The battery percentage changed. About to take a new sample (currentBatteryLevel=" + a.b() + ", lastBatteryLevel=" + a.a() + ")");
                org.greenrobot.eventbus.c.a().c(new StatusEvent(getString(R.string.event_new_sample)));
                a(context, intent, d, aVar);
                a(context, intent, aVar, false);
                boolean z2 = intent.getIntExtra("plugged", 0) != 0;
                if (g.m(context) && g.n(context)) {
                    if (a.b() == 1.0d && z2) {
                        e.d(context);
                    } else if (a.b() == 0.2d) {
                        e.e(context);
                    }
                }
                if (a.a() == 0.0d) {
                    c.b(f4285a, "Last Battery Level = 0. Updating to BatteryLevel => " + a.b());
                    a.a(a.b());
                }
            } else {
                c.b(f4285a, "No battery percentage change. BatteryLevel => " + a.b());
            }
            if (com.hmatalonga.greenhub.network.a.c >= 3 || !g.i(context) || !g.d(context) || !g.b(context) || !z) {
                aVar.b();
                return;
            }
            new f().execute(new Context[]{context});
            new com.hmatalonga.greenhub.b.a().execute(new Context[]{context});
            if (aVar.a(Sample.class) >= ((long) g.j(context)) && !com.hmatalonga.greenhub.network.a.f4289a) {
                new com.hmatalonga.greenhub.network.a(context, true).a();
            }
            aVar.b();
            return;
        }
        c.b(f4285a, "current battery level = 0");
    }

    /* access modifiers changed from: protected */
    public void onHandleIntent(Intent intent) {
        WakeLock newWakeLock = ((PowerManager) getSystemService("power")).newWakeLock(1, f4285a);
        newWakeLock.acquire();
        String str = null;
        Context applicationContext = getApplicationContext();
        if (intent != null) {
            str = intent.getStringExtra("OriginalAction");
        }
        if (str != null) {
            a(intent, applicationContext);
        }
        newWakeLock.release();
        if (intent != null) {
            DataEstimator.completeWakefulIntent(intent);
        }
    }
}
