package com.hmatalonga.greenhub.managers.sampling;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PermissionInfo;
import android.preference.PreferenceManager;
import android.util.Log;
import com.hmatalonga.greenhub.Config;
import com.hmatalonga.greenhub.c.c;
import com.hmatalonga.greenhub.c.g;
import com.hmatalonga.greenhub.events.StatusEvent;
import com.hmatalonga.greenhub.models.Application;
import com.hmatalonga.greenhub.models.Battery;
import com.hmatalonga.greenhub.models.Bluetooth;
import com.hmatalonga.greenhub.models.Cpu;
import com.hmatalonga.greenhub.models.Gps;
import com.hmatalonga.greenhub.models.LocationInfo;
import com.hmatalonga.greenhub.models.Memory;
import com.hmatalonga.greenhub.models.Network;
import com.hmatalonga.greenhub.models.Package;
import com.hmatalonga.greenhub.models.Phone;
import com.hmatalonga.greenhub.models.Process;
import com.hmatalonga.greenhub.models.Screen;
import com.hmatalonga.greenhub.models.SettingsInfo;
import com.hmatalonga.greenhub.models.SimCard;
import com.hmatalonga.greenhub.models.Specifications;
import com.hmatalonga.greenhub.models.Storage;
import com.hmatalonga.greenhub.models.Wifi;
import com.hmatalonga.greenhub.models.data.AppPermission;
import com.hmatalonga.greenhub.models.data.BatteryDetails;
import com.hmatalonga.greenhub.models.data.BatterySession;
import com.hmatalonga.greenhub.models.data.BatteryUsage;
import com.hmatalonga.greenhub.models.data.CpuStatus;
import com.hmatalonga.greenhub.models.data.Feature;
import com.hmatalonga.greenhub.models.data.NetworkDetails;
import com.hmatalonga.greenhub.models.data.ProcessInfo;
import com.hmatalonga.greenhub.models.data.Sample;
import com.hmatalonga.greenhub.models.data.Settings;
import com.mansoon.BatteryDouble.R;
import io.realm.bf;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public final class a {

    /* renamed from: a reason: collision with root package name */
    private static final String f4287a = c.a(a.class);

    /* renamed from: b reason: collision with root package name */
    private static double f4288b = 0.0d;
    private static double c = 0.0d;

    private a() {
    }

    public static double a() {
        return f4288b;
    }

    static BatteryUsage a(Context context, Intent intent) {
        String str;
        String str2;
        BatteryUsage batteryUsage = new BatteryUsage();
        BatteryDetails batteryDetails = new BatteryDetails();
        int intExtra = intent.getIntExtra("health", 0);
        int intExtra2 = intent.getIntExtra("status", 0);
        int intExtra3 = intent.getIntExtra("plugged", 0);
        String string = intent.getExtras().getString("technology");
        String str3 = "Unknown";
        String str4 = "unplugged";
        batteryUsage.realmSet$timestamp(System.currentTimeMillis());
        batteryUsage.realmSet$id(String.valueOf(batteryUsage.realmGet$timestamp()).hashCode());
        switch (intExtra) {
            case 1:
                str = "Unknown";
                break;
            case 2:
                str = "Good";
                break;
            case 3:
                str = "Overheat";
                break;
            case 4:
                str = "Dead";
                break;
            case 5:
                str = "Over voltage";
                break;
            case 6:
                str = "Unspecified failure";
                break;
            default:
                str = str3;
                break;
        }
        switch (intExtra2) {
            case 1:
                str2 = "Unknown";
                break;
            case 2:
                str2 = "Charging";
                break;
            case 3:
                str2 = "Discharging";
                break;
            case 4:
                str2 = "Not charging";
                break;
            case 5:
                str2 = "Full";
                break;
            default:
                str2 = "Unknown";
                break;
        }
        switch (intExtra3) {
            case 1:
                str4 = "ac";
                break;
            case 2:
                str4 = "usb";
                break;
        }
        batteryDetails.realmSet$temperature((double) (((float) intent.getIntExtra("temperature", 0)) / 10.0f));
        batteryDetails.realmSet$voltage((double) (((float) intent.getIntExtra("voltage", 0)) / 1000.0f));
        batteryDetails.realmSet$charger(str4);
        batteryDetails.realmSet$health(str);
        batteryDetails.realmSet$technology(string);
        batteryDetails.realmSet$capacity(Battery.getActualBatteryCapacity(context));
        batteryDetails.realmSet$chargeCounter(Battery.getBatteryChargeCounter(context));
        batteryDetails.realmSet$currentAverage(Battery.getBatteryCurrentAverage(context));
        batteryDetails.realmSet$currentNow(Battery.getBatteryCurrentNow(context));
        batteryDetails.realmSet$energyCounter(Battery.getBatteryEnergyCounter(context));
        batteryUsage.realmSet$level((float) c);
        batteryUsage.realmSet$state(str2);
        batteryUsage.realmSet$screenOn(Screen.isOn(context));
        batteryUsage.realmSet$triggeredBy(intent.getAction());
        batteryUsage.realmSet$details(batteryDetails);
        return batteryUsage;
    }

    static Sample a(Context context, Intent intent, String str) {
        String str2;
        Sample sample = new Sample();
        NetworkDetails networkDetails = new NetworkDetails();
        BatteryDetails batteryDetails = new BatteryDetails();
        CpuStatus cpuStatus = new CpuStatus();
        Settings settings = new Settings();
        sample.realmSet$processInfos(new bf());
        sample.realmSet$locationProviders(new bf());
        sample.realmSet$features(new bf());
        c.b(f4287a, "getSample() was invoked.");
        String action = intent.getAction();
        c.b(f4287a, "action = " + action);
        sample.realmSet$uuId(Specifications.getAndroidId(context));
        sample.realmSet$triggeredBy(action);
        sample.realmSet$timestamp(System.currentTimeMillis());
        sample.realmSet$id(String.valueOf(sample.realmGet$timestamp()).hashCode());
        sample.realmSet$version(13);
        sample.realmSet$database(3);
        long[] readUsagePoint = Cpu.readUsagePoint();
        org.greenrobot.eventbus.c.a().c(new StatusEvent(context.getString(R.string.event_get_processes)));
        sample.realmGet$processInfos().addAll(a(context));
        sample.realmSet$screenBrightness(Screen.getBrightness(context));
        if (Screen.isAutoBrightness(context)) {
            sample.realmSet$screenBrightness(-1);
        }
        sample.realmGet$locationProviders().addAll(LocationInfo.getEnabledLocationProviders(context));
        String status = Network.getStatus(context);
        String type = Network.getType(context);
        String mobileNetworkType = Network.getMobileNetworkType(context);
        if (!status.equals(Network.NETWORKSTATUS_CONNECTED)) {
            sample.realmSet$networkStatus(status);
        } else if (type.equals("WIFI")) {
            sample.realmSet$networkStatus(type);
        } else {
            sample.realmSet$networkStatus(mobileNetworkType);
        }
        networkDetails.realmSet$networkType(type);
        networkDetails.realmSet$mobileNetworkType(mobileNetworkType);
        networkDetails.realmSet$roamingEnabled(Network.getRoamingStatus(context));
        networkDetails.realmSet$mobileDataStatus(Network.getDataState(context));
        networkDetails.realmSet$mobileDataActivity(Network.getDataActivity(context));
        networkDetails.realmSet$simOperator(SimCard.getSIMOperator(context));
        networkDetails.realmSet$networkOperator(Phone.getNetworkOperator(context));
        networkDetails.realmSet$mcc(Phone.getMcc(context));
        networkDetails.realmSet$mnc(Phone.getMnc(context));
        networkDetails.realmSet$wifiStatus(Wifi.getState(context));
        networkDetails.realmSet$wifiSignalStrength(Wifi.getSignalStrength(context));
        networkDetails.realmSet$wifiLinkSpeed(Wifi.getLinkSpeed(context));
        networkDetails.realmSet$wifiApStatus(Wifi.getHotspotState(context));
        sample.realmSet$networkDetails(networkDetails);
        sample.realmSet$callInfo(null);
        int intExtra = intent.getIntExtra("health", 0);
        int intExtra2 = intent.getIntExtra("status", 0);
        int intExtra3 = intent.getIntExtra("plugged", 0);
        String string = intent.getExtras().getString("technology");
        String str3 = "Unknown";
        switch (intExtra) {
            case 1:
                str2 = "Unknown";
                break;
            case 2:
                str2 = "Good";
                break;
            case 3:
                str2 = "Overheat";
                break;
            case 4:
                str2 = "Dead";
                break;
            case 5:
                str2 = "Over voltage";
                break;
            case 6:
                str2 = "Unspecified failure";
                break;
            default:
                str2 = str3;
                break;
        }
        switch (intExtra2) {
            case 1:
                str = "Unknown";
                break;
            case 2:
                str = "Charging";
                break;
            case 3:
                str = "Discharging";
                break;
            case 4:
                str = "Not charging";
                break;
            case 5:
                str = "Full";
                break;
            default:
                if (str == null) {
                    str = "Unknown";
                    break;
                }
                break;
        }
        String str4 = "unplugged";
        switch (intExtra3) {
            case 1:
                str4 = "ac";
                break;
            case 2:
                str4 = "usb";
                break;
        }
        batteryDetails.realmSet$temperature((double) (((float) intent.getIntExtra("temperature", 0)) / 10.0f));
        batteryDetails.realmSet$voltage((double) (((float) intent.getIntExtra("voltage", 0)) / 1000.0f));
        batteryDetails.realmSet$charger(str4);
        batteryDetails.realmSet$health(str2);
        batteryDetails.realmSet$technology(string);
        batteryDetails.realmSet$capacity(Battery.getActualBatteryCapacity(context));
        batteryDetails.realmSet$chargeCounter(Battery.getBatteryChargeCounter(context));
        batteryDetails.realmSet$currentAverage(Battery.getBatteryCurrentAverage(context));
        batteryDetails.realmSet$currentNow(Battery.getBatteryCurrentNow(context));
        batteryDetails.realmSet$energyCounter(Battery.getBatteryEnergyCounter(context));
        sample.realmSet$batteryDetails(batteryDetails);
        sample.realmSet$batteryLevel(c);
        sample.realmSet$batteryState(str);
        int[] readMemoryInfo = Memory.readMemoryInfo();
        if (readMemoryInfo != null && readMemoryInfo.length == 4) {
            sample.realmSet$memoryUser(readMemoryInfo[0]);
            sample.realmSet$memoryFree(readMemoryInfo[1]);
            sample.realmSet$memoryActive(readMemoryInfo[2]);
            sample.realmSet$memoryInactive(readMemoryInfo[3]);
        }
        long[] readUsagePoint2 = Cpu.readUsagePoint();
        long uptime = Cpu.getUptime();
        long sleepTime = Cpu.getSleepTime();
        cpuStatus.realmSet$cpuUsage(Cpu.getUsage(readUsagePoint, readUsagePoint2));
        cpuStatus.realmSet$upTime(uptime);
        cpuStatus.realmSet$sleepTime(sleepTime);
        sample.realmSet$cpuStatus(cpuStatus);
        sample.realmSet$storageDetails(Storage.getStorageDetails());
        settings.realmSet$bluetoothEnabled(Bluetooth.isEnabled());
        settings.realmSet$locationEnabled(Gps.isEnabled(context));
        settings.realmSet$powersaverEnabled(SettingsInfo.isPowerSaveEnabled(context));
        settings.realmSet$flashlightEnabled(false);
        settings.realmSet$nfcEnabled(SettingsInfo.isNfcEnabled(context));
        settings.realmSet$developerMode(SettingsInfo.isDeveloperModeOn(context));
        settings.realmSet$unknownSources(SettingsInfo.allowUnknownSources(context));
        sample.realmSet$settings(settings);
        sample.realmSet$screenOn(Screen.isOn(context));
        sample.realmSet$timeZone(SettingsInfo.getTimeZone());
        sample.realmSet$countryCode(LocationInfo.getCountryCode(context));
        List c2 = c();
        if (c2 != null && c2.size() > 0) {
            sample.realmGet$features().addAll(c2);
        }
        return sample;
    }

    /* JADX WARNING: Removed duplicated region for block: B:36:0x00db  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x00dd A[SYNTHETIC] */
    private static List<ProcessInfo> a(Context context) {
        String str;
        Process.clear();
        ArrayList<ProcessInfo> runningAppInfo = Application.getRunningAppInfo(context);
        ArrayList arrayList = new ArrayList();
        PackageManager packageManager = context.getPackageManager();
        int[] iArr = new int[runningAppInfo.size()];
        HashSet hashSet = new HashSet();
        Map map = g.e(context) ? Package.getInstalledPackages(context, false) : null;
        for (ProcessInfo processInfo : runningAppInfo) {
            String realmGet$name = processInfo.realmGet$name();
            if (map != null && map.containsKey(realmGet$name)) {
                map.remove(realmGet$name);
            }
            hashSet.add(realmGet$name);
            ProcessInfo processInfo2 = new ProcessInfo();
            processInfo2.realmSet$appPermissions(new bf());
            processInfo2.realmSet$appSignatures(new bf());
            PackageInfo packageInfo = Package.getPackageInfo(context, realmGet$name);
            if (packageInfo != null) {
                processInfo2.realmSet$versionName(packageInfo.versionName);
                processInfo2.realmSet$versionCode(packageInfo.versionCode);
                String charSequence = packageManager.getApplicationLabel(packageInfo.applicationInfo).toString();
                if (charSequence.length() > 0) {
                    processInfo2.realmSet$applicationLabel(charSequence);
                }
                int i = packageInfo.applicationInfo.flags;
                processInfo2.realmSet$isSystemApp(((i & 1) > 0) || (i & 128) > 0);
                if (packageInfo.permissions != null) {
                    for (PermissionInfo permissionInfo : packageInfo.permissions) {
                        processInfo2.realmGet$appPermissions().add(new AppPermission(permissionInfo.name));
                    }
                }
            }
            processInfo2.realmSet$importance(processInfo.realmGet$importance());
            processInfo2.realmSet$processId(processInfo.realmGet$processId());
            processInfo2.realmSet$name(processInfo.realmGet$name());
            if (!processInfo.realmGet$isSystemApp()) {
                try {
                    str = packageManager.getInstallerPackageName(realmGet$name);
                } catch (IllegalArgumentException e) {
                    Log.e(f4287a, "Could not get installer for " + realmGet$name);
                }
                if (str != null) {
                    str = "null";
                }
                processInfo2.realmSet$installationPkg(str);
                arrayList.add(processInfo2);
            }
            str = null;
            if (str != null) {
            }
            processInfo2.realmSet$installationPkg(str);
            arrayList.add(processInfo2);
        }
        if (map != null && map.size() > 0) {
            arrayList.addAll(map.values());
            g.c(context, false);
        }
        a(context, (List<ProcessInfo>) arrayList);
        return arrayList;
    }

    public static void a(double d) {
        f4288b = d;
    }

    public static void a(double d, double d2) {
        double d3 = d / d2;
        if (d3 != c) {
            c = d3;
        }
    }

    private static void a(Context context, List<ProcessInfo> list) {
        boolean z;
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        Set<String> keySet = defaultSharedPreferences.getAll().keySet();
        Editor edit = defaultSharedPreferences.edit();
        boolean z2 = false;
        for (String str : keySet) {
            if (str.startsWith("installed:")) {
                String substring = str.substring("installed:".length());
                if (defaultSharedPreferences.getBoolean(str, false)) {
                    Log.i(f4287a, "Installed:" + substring);
                    ProcessInfo installedPackage = Package.getInstalledPackage(context, substring);
                    if (installedPackage != null) {
                        installedPackage.realmSet$importance(Config.IMPORTANCE_INSTALLED);
                        list.add(installedPackage);
                        edit.remove(str);
                        z2 = true;
                    }
                }
                z = z2;
            } else if (str.startsWith("replaced:")) {
                String substring2 = str.substring("replaced:".length());
                if (defaultSharedPreferences.getBoolean(str, false)) {
                    Log.i(f4287a, "Replaced:" + substring2);
                    ProcessInfo installedPackage2 = Package.getInstalledPackage(context, substring2);
                    if (installedPackage2 != null) {
                        installedPackage2.realmSet$importance(Config.IMPORTANCE_REPLACED);
                        list.add(installedPackage2);
                        edit.remove(str);
                        z2 = true;
                    }
                }
                z = z2;
            } else if (str.startsWith("uninstalled:")) {
                String substring3 = str.substring("uninstalled:".length());
                if (defaultSharedPreferences.getBoolean(str, false)) {
                    Log.i(f4287a, "Uninstalled:" + substring3);
                    list.add(Process.uninstalledItem(substring3, str, edit));
                    z2 = true;
                }
                z = z2;
            } else {
                if (str.startsWith("disabled:")) {
                    String substring4 = str.substring("disabled:".length());
                    if (defaultSharedPreferences.getBoolean(str, false)) {
                        Log.i(f4287a, "Disabled app:" + substring4);
                        list.add(Process.disabledItem(substring4, str, edit));
                        z = true;
                    }
                }
                z = z2;
            }
            z2 = z;
        }
        if (z2) {
            edit.apply();
        }
    }

    public static double b() {
        return c;
    }

    public static BatterySession b(Context context, Intent intent) {
        BatterySession batterySession = new BatterySession();
        batterySession.realmSet$timestamp(System.currentTimeMillis());
        batterySession.realmSet$id(String.valueOf(batterySession.realmGet$timestamp()).hashCode());
        batterySession.realmSet$level((float) c);
        batterySession.realmSet$screenOn(Screen.isOn(context));
        batterySession.realmSet$triggeredBy(intent.getAction());
        return batterySession;
    }

    private static List<Feature> c() {
        LinkedList linkedList = new LinkedList();
        linkedList.add(Specifications.getVmVersion());
        return linkedList;
    }
}
