package com.hmatalonga.greenhub.managers.sampling;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ReceiverCallNotAllowedException;
import android.support.v4.b.d;
import com.hmatalonga.greenhub.c.c;
import com.hmatalonga.greenhub.c.e;
import com.hmatalonga.greenhub.c.g;
import com.hmatalonga.greenhub.events.BatteryLevelEvent;
import com.mansoon.BatteryDouble.R;
import java.util.Calendar;

public class DataEstimator extends d {
    private static final String TAG = c.a(DataEstimator.class);
    private long lastNotify;
    private int level;
    private int mHealth;
    private int plugged;
    private boolean present;
    private int scale;
    private int status;
    private String technology;
    private float temperature;
    private float voltage;

    public static Intent getBatteryChangedIntent(Context context) {
        return context.registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
    }

    public void getCurrentStatus(Context context) {
        try {
            Intent registerReceiver = context.registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
            if (registerReceiver != null) {
                this.level = registerReceiver.getIntExtra("level", -1);
                this.scale = registerReceiver.getIntExtra("scale", -1);
                this.mHealth = registerReceiver.getIntExtra("health", 0);
                this.plugged = registerReceiver.getIntExtra("plugged", 0);
                this.present = registerReceiver.getExtras().getBoolean("present");
                this.status = registerReceiver.getIntExtra("status", 0);
                this.technology = registerReceiver.getExtras().getString("technology");
                this.temperature = (float) (registerReceiver.getIntExtra("temperature", 0) / 10);
                this.voltage = (float) (registerReceiver.getIntExtra("voltage", 0) / 1000);
            }
        } catch (ReceiverCallNotAllowedException e) {
            c.c(TAG, "ReceiverCallNotAllowedException from Notification Receiver?");
            e.printStackTrace();
        }
    }

    public int getHealth() {
        return this.mHealth;
    }

    public String getHealthStatus() {
        String str = "";
        switch (this.mHealth) {
            case 1:
                return "Unknown";
            case 2:
                return "Good";
            case 3:
                return "Overheat";
            case 4:
                return "Dead";
            case 5:
                return "Over Voltage";
            case 6:
                return "Unspecified Failure";
            default:
                return str;
        }
    }

    public String getHealthStatus(Context context) {
        String str = "";
        switch (this.mHealth) {
            case 1:
                return context.getString(R.string.battery_health_unknown);
            case 2:
                return context.getString(R.string.battery_health_good);
            case 3:
                return context.getString(R.string.battery_health_overheat);
            case 4:
                return context.getString(R.string.battery_health_dead);
            case 5:
                return context.getString(R.string.battery_health_over_voltage);
            case 6:
                return context.getString(R.string.battery_health_failure);
            default:
                return str;
        }
    }

    public long getLastNotify() {
        return this.lastNotify;
    }

    public int getLevel() {
        return this.level;
    }

    public int getPlugged() {
        return this.plugged;
    }

    public int getScale() {
        return this.scale;
    }

    public int getStatus() {
        return this.status;
    }

    public String getTechnology() {
        return this.technology;
    }

    public float getTemperature() {
        return this.temperature;
    }

    public float getVoltage() {
        return this.voltage;
    }

    public boolean isPresent() {
        return this.present;
    }

    public void onReceive(Context context, Intent intent) {
        if (context == null) {
            c.c(TAG, "Error, context is null");
        } else if (intent == null) {
            c.c(TAG, "Data Estimator error, received intent is null");
        } else {
            c.b(TAG, "onReceive action => " + intent.getAction());
            if (intent.getAction().equals("android.intent.action.BATTERY_CHANGED")) {
                try {
                    this.level = intent.getIntExtra("level", -1);
                    this.scale = intent.getIntExtra("scale", -1);
                    this.mHealth = intent.getIntExtra("health", 0);
                    this.plugged = intent.getIntExtra("plugged", 0);
                    this.present = intent.getExtras().getBoolean("present");
                    this.status = intent.getIntExtra("status", 0);
                    this.technology = intent.getExtras().getString("technology");
                    this.temperature = ((float) intent.getIntExtra("temperature", 0)) / 10.0f;
                    this.voltage = ((float) intent.getIntExtra("voltage", 0)) / 1000.0f;
                } catch (RuntimeException e) {
                    e.printStackTrace();
                }
                if (this.temperature > ((float) g.q(context)) && g.m(context) && g.o(context)) {
                    Calendar instance = Calendar.getInstance();
                    long s = g.s(context);
                    if (s != 0) {
                        instance.setTimeInMillis(s);
                    }
                    instance.add(12, g.p(context));
                    if (s == 0 || Calendar.getInstance().after(instance)) {
                        if (this.temperature > ((float) g.r(context))) {
                            e.g(context);
                            g.a(context, System.currentTimeMillis());
                        } else if (this.temperature <= ((float) g.r(context)) && this.temperature > ((float) g.q(context))) {
                            e.f(context);
                            g.a(context, System.currentTimeMillis());
                        }
                    }
                }
            }
            if (this.scale == 0) {
                this.scale = 100;
            }
            if (this.level > 0) {
                a.a((double) this.level, (double) this.scale);
                Intent intent2 = new Intent(context, DataEstimatorService.class);
                intent2.putExtra("OriginalAction", intent.getAction());
                intent2.fillIn(intent, 0);
                if (g.l(context)) {
                    c.b(TAG, "Updating notification status bar");
                    e.b(context);
                }
                org.greenrobot.eventbus.c.a().c(new BatteryLevelEvent(this.level));
                startWakefulService(context, intent2);
            }
        }
    }

    public void setLastNotify(long j) {
        this.lastNotify = j;
    }
}
