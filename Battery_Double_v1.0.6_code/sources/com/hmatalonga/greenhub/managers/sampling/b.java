package com.hmatalonga.greenhub.managers.sampling;

import android.annotation.TargetApi;
import android.app.usage.UsageStats;
import android.app.usage.UsageStatsManager;
import android.content.Context;
import java.util.Calendar;
import java.util.List;

public class b {
    @TargetApi(21)
    public static List<UsageStats> a(Context context) {
        UsageStatsManager b2 = b(context);
        Calendar instance = Calendar.getInstance();
        instance.add(2, -1);
        return b2.queryUsageStats(0, instance.getTimeInMillis(), System.currentTimeMillis());
    }

    private static UsageStatsManager b(Context context) {
        return (UsageStatsManager) context.getSystemService("usagestats");
    }
}
