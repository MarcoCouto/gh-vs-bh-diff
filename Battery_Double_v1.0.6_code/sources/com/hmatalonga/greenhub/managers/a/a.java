package com.hmatalonga.greenhub.managers.a;

import com.hmatalonga.greenhub.c.c;
import com.hmatalonga.greenhub.models.data.BatterySession;
import com.hmatalonga.greenhub.models.data.BatteryUsage;
import com.hmatalonga.greenhub.models.data.Message;
import com.hmatalonga.greenhub.models.data.Sample;
import io.realm.bb;
import io.realm.bn;
import io.realm.bu;
import io.realm.exceptions.RealmMigrationNeededException;
import java.util.ArrayList;
import java.util.Iterator;

public class a {

    /* renamed from: a reason: collision with root package name */
    private static final String f4282a = c.a(a.class);

    /* renamed from: b reason: collision with root package name */
    private bb f4283b;

    public a() {
        try {
            this.f4283b = bb.m();
        } catch (RealmMigrationNeededException e) {
            e.printStackTrace();
        }
    }

    public long a(Class cls) {
        if (cls.equals(Sample.class)) {
            return this.f4283b.a(Sample.class).a();
        }
        if (cls.equals(BatteryUsage.class)) {
            return this.f4283b.a(BatteryUsage.class).a();
        }
        if (cls.equals(BatterySession.class)) {
            return this.f4283b.a(BatterySession.class).a();
        }
        if (cls.equals(Message.class)) {
            return this.f4283b.a(Message.class).a();
        }
        return -1;
    }

    public bn<BatteryUsage> a(long j, long j2) {
        return this.f4283b.a(BatteryUsage.class).a("triggeredBy", "android.intent.action.BATTERY_CHANGED").a("timestamp", j, j2).a("timestamp");
    }

    public void a() {
        if (this.f4283b.j()) {
            this.f4283b = bb.m();
        }
    }

    public void a(int i) {
        this.f4283b.b();
        Message message = (Message) this.f4283b.a(Message.class).a("id", Integer.valueOf(i)).c();
        if (message != null) {
            message.realmSet$read(true);
        }
        this.f4283b.c();
    }

    public void a(BatterySession batterySession) {
        this.f4283b.b();
        this.f4283b.a(batterySession);
        this.f4283b.c();
    }

    public void a(BatteryUsage batteryUsage) {
        this.f4283b.b();
        this.f4283b.a(batteryUsage);
        this.f4283b.c();
    }

    public void a(Sample sample) {
        this.f4283b.b();
        this.f4283b.a(sample);
        this.f4283b.c();
    }

    public void b() {
        this.f4283b.close();
    }

    public void b(int i) {
        this.f4283b.b();
        Message message = (Message) this.f4283b.a(Message.class).a("id", Integer.valueOf(i)).c();
        if (message != null) {
            message.deleteFromRealm();
        }
        this.f4283b.c();
    }

    public boolean c() {
        return this.f4283b.j();
    }

    public Sample d() {
        if (this.f4283b.a(Sample.class).a() > 0) {
            return (Sample) this.f4283b.a(Sample.class).b().a();
        }
        return null;
    }

    public Iterator<Integer> e() {
        ArrayList arrayList = new ArrayList();
        bn b2 = this.f4283b.a(Sample.class).b();
        if (!b2.isEmpty()) {
            Iterator it = b2.iterator();
            while (it.hasNext()) {
                arrayList.add(Integer.valueOf(((Sample) it.next()).realmGet$id()));
            }
        }
        return arrayList.iterator();
    }

    public bn<Message> f() {
        return this.f4283b.a(Message.class).a("id", bu.DESCENDING);
    }
}
