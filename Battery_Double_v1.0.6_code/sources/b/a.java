package b;

import java.io.IOException;
import java.io.InterruptedIOException;

public class a extends t {

    /* renamed from: a reason: collision with root package name */
    private static a f1359a;
    private boolean c;
    private a d;
    private long e;

    /* renamed from: b.a$a reason: collision with other inner class name */
    private static final class C0035a extends Thread {
        public C0035a() {
            super("Okio Watchdog");
            setDaemon(true);
        }

        public void run() {
            while (true) {
                try {
                    a e = a.e();
                    if (e != null) {
                        e.a();
                    }
                } catch (InterruptedException e2) {
                }
            }
        }
    }

    private static synchronized void a(a aVar, long j, boolean z) {
        synchronized (a.class) {
            if (f1359a == null) {
                f1359a = new a();
                new C0035a().start();
            }
            long nanoTime = System.nanoTime();
            if (j != 0 && z) {
                aVar.e = Math.min(j, aVar.d() - nanoTime) + nanoTime;
            } else if (j != 0) {
                aVar.e = nanoTime + j;
            } else if (z) {
                aVar.e = aVar.d();
            } else {
                throw new AssertionError();
            }
            long b2 = aVar.b(nanoTime);
            a aVar2 = f1359a;
            while (aVar2.d != null && b2 >= aVar2.d.b(nanoTime)) {
                aVar2 = aVar2.d;
            }
            aVar.d = aVar2.d;
            aVar2.d = aVar;
            if (aVar2 == f1359a) {
                a.class.notify();
            }
        }
    }

    private static synchronized boolean a(a aVar) {
        boolean z;
        synchronized (a.class) {
            a aVar2 = f1359a;
            while (true) {
                if (aVar2 == null) {
                    z = true;
                    break;
                } else if (aVar2.d == aVar) {
                    aVar2.d = aVar.d;
                    aVar.d = null;
                    z = false;
                    break;
                } else {
                    aVar2 = aVar2.d;
                }
            }
        }
        return z;
    }

    private long b(long j) {
        return this.e - j;
    }

    static synchronized a e() throws InterruptedException {
        a aVar = null;
        synchronized (a.class) {
            a aVar2 = f1359a.d;
            if (aVar2 == null) {
                a.class.wait();
            } else {
                long b2 = aVar2.b(System.nanoTime());
                if (b2 > 0) {
                    long j = b2 / 1000000;
                    a.class.wait(j, (int) (b2 - (1000000 * j)));
                } else {
                    f1359a.d = aVar2.d;
                    aVar2.d = null;
                    aVar = aVar2;
                }
            }
        }
        return aVar;
    }

    public final boolean B_() {
        if (!this.c) {
            return false;
        }
        this.c = false;
        return a(this);
    }

    public final r a(final r rVar) {
        return new r() {
            public void a(c cVar, long j) throws IOException {
                u.a(cVar.f1367b, 0, j);
                long j2 = j;
                while (j2 > 0) {
                    o oVar = cVar.f1366a;
                    long j3 = 0;
                    while (true) {
                        if (j3 >= 65536) {
                            break;
                        }
                        long j4 = ((long) (cVar.f1366a.c - cVar.f1366a.f1391b)) + j3;
                        if (j4 >= j2) {
                            j3 = j2;
                            break;
                        } else {
                            oVar = oVar.f;
                            j3 = j4;
                        }
                    }
                    a.this.c();
                    try {
                        rVar.a(cVar, j3);
                        j2 -= j3;
                        a.this.a(true);
                    } catch (IOException e) {
                        throw a.this.b(e);
                    } catch (Throwable th) {
                        a.this.a(false);
                        throw th;
                    }
                }
            }

            public void close() throws IOException {
                a.this.c();
                try {
                    rVar.close();
                    a.this.a(true);
                } catch (IOException e) {
                    throw a.this.b(e);
                } catch (Throwable th) {
                    a.this.a(false);
                    throw th;
                }
            }

            public void flush() throws IOException {
                a.this.c();
                try {
                    rVar.flush();
                    a.this.a(true);
                } catch (IOException e) {
                    throw a.this.b(e);
                } catch (Throwable th) {
                    a.this.a(false);
                    throw th;
                }
            }

            public t timeout() {
                return a.this;
            }

            public String toString() {
                return "AsyncTimeout.sink(" + rVar + ")";
            }
        };
    }

    public final s a(final s sVar) {
        return new s() {
            public void close() throws IOException {
                try {
                    sVar.close();
                    a.this.a(true);
                } catch (IOException e) {
                    throw a.this.b(e);
                } catch (Throwable th) {
                    a.this.a(false);
                    throw th;
                }
            }

            public long read(c cVar, long j) throws IOException {
                a.this.c();
                try {
                    long read = sVar.read(cVar, j);
                    a.this.a(true);
                    return read;
                } catch (IOException e) {
                    throw a.this.b(e);
                } catch (Throwable th) {
                    a.this.a(false);
                    throw th;
                }
            }

            public t timeout() {
                return a.this;
            }

            public String toString() {
                return "AsyncTimeout.source(" + sVar + ")";
            }
        };
    }

    /* access modifiers changed from: protected */
    public IOException a(IOException iOException) {
        InterruptedIOException interruptedIOException = new InterruptedIOException("timeout");
        if (iOException != null) {
            interruptedIOException.initCause(iOException);
        }
        return interruptedIOException;
    }

    /* access modifiers changed from: protected */
    public void a() {
    }

    /* access modifiers changed from: 0000 */
    public final void a(boolean z) throws IOException {
        if (B_() && z) {
            throw a((IOException) null);
        }
    }

    /* access modifiers changed from: 0000 */
    public final IOException b(IOException iOException) throws IOException {
        return !B_() ? iOException : a(iOException);
    }

    public final void c() {
        if (this.c) {
            throw new IllegalStateException("Unbalanced enter/exit");
        }
        long C_ = C_();
        boolean D_ = D_();
        if (C_ != 0 || D_) {
            this.c = true;
            a(this, C_, D_);
        }
    }
}
