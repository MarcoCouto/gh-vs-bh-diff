package b;

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;

public final class c implements d, e, Cloneable {
    private static final byte[] c = {48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 97, 98, 99, 100, 101, 102};

    /* renamed from: a reason: collision with root package name */
    o f1366a;

    /* renamed from: b reason: collision with root package name */
    long f1367b;

    public int a(byte[] bArr, int i, int i2) {
        u.a((long) bArr.length, (long) i, (long) i2);
        o oVar = this.f1366a;
        if (oVar == null) {
            return -1;
        }
        int min = Math.min(i2, oVar.c - oVar.f1391b);
        System.arraycopy(oVar.f1390a, oVar.f1391b, bArr, i, min);
        oVar.f1391b += min;
        this.f1367b -= (long) min;
        if (oVar.f1391b != oVar.c) {
            return min;
        }
        this.f1366a = oVar.a();
        p.a(oVar);
        return min;
    }

    public long a() {
        return this.f1367b;
    }

    public long a(byte b2) {
        return a(b2, 0);
    }

    public long a(byte b2, long j) {
        o oVar;
        long j2;
        long j3 = 0;
        if (j < 0) {
            throw new IllegalArgumentException("fromIndex < 0");
        }
        o oVar2 = this.f1366a;
        if (oVar2 == null) {
            return -1;
        }
        if (this.f1367b - j >= j) {
            o oVar3 = oVar2;
            while (true) {
                long j4 = ((long) (oVar.c - oVar.f1391b)) + j2;
                if (j4 >= j) {
                    break;
                }
                oVar3 = oVar.f;
                j3 = j4;
            }
        } else {
            j2 = this.f1367b;
            oVar = oVar2;
            while (j2 > j) {
                oVar = oVar.g;
                j2 -= (long) (oVar.c - oVar.f1391b);
            }
        }
        while (j2 < this.f1367b) {
            byte[] bArr = oVar.f1390a;
            int i = oVar.c;
            for (int i2 = (int) ((((long) oVar.f1391b) + j) - j2); i2 < i; i2++) {
                if (bArr[i2] == b2) {
                    return j2 + ((long) (i2 - oVar.f1391b));
                }
            }
            j2 += (long) (oVar.c - oVar.f1391b);
            oVar = oVar.f;
            j = j2;
        }
        return -1;
    }

    public long a(r rVar) throws IOException {
        long j = this.f1367b;
        if (j > 0) {
            rVar.a(this, j);
        }
        return j;
    }

    public long a(s sVar) throws IOException {
        if (sVar == null) {
            throw new IllegalArgumentException("source == null");
        }
        long j = 0;
        while (true) {
            long read = sVar.read(this, 8192);
            if (read == -1) {
                return j;
            }
            j += read;
        }
    }

    public c a(int i) {
        if (i < 128) {
            i(i);
        } else if (i < 2048) {
            i((i >> 6) | 192);
            i((i & 63) | 128);
        } else if (i < 65536) {
            if (i < 55296 || i > 57343) {
                i((i >> 12) | 224);
                i(((i >> 6) & 63) | 128);
                i((i & 63) | 128);
            } else {
                throw new IllegalArgumentException("Unexpected code point: " + Integer.toHexString(i));
            }
        } else if (i <= 1114111) {
            i((i >> 18) | 240);
            i(((i >> 12) & 63) | 128);
            i(((i >> 6) & 63) | 128);
            i((i & 63) | 128);
        } else {
            throw new IllegalArgumentException("Unexpected code point: " + Integer.toHexString(i));
        }
        return this;
    }

    public c a(c cVar, long j, long j2) {
        if (cVar == null) {
            throw new IllegalArgumentException("out == null");
        }
        u.a(this.f1367b, j, j2);
        if (j2 != 0) {
            cVar.f1367b += j2;
            o oVar = this.f1366a;
            while (j >= ((long) (oVar.c - oVar.f1391b))) {
                j -= (long) (oVar.c - oVar.f1391b);
                oVar = oVar.f;
            }
            while (j2 > 0) {
                o oVar2 = new o(oVar);
                oVar2.f1391b = (int) (((long) oVar2.f1391b) + j);
                oVar2.c = Math.min(oVar2.f1391b + ((int) j2), oVar2.c);
                if (cVar.f1366a == null) {
                    oVar2.g = oVar2;
                    oVar2.f = oVar2;
                    cVar.f1366a = oVar2;
                } else {
                    cVar.f1366a.g.a(oVar2);
                }
                j2 -= (long) (oVar2.c - oVar2.f1391b);
                oVar = oVar.f;
                j = 0;
            }
        }
        return this;
    }

    /* renamed from: a */
    public c b(f fVar) {
        if (fVar == null) {
            throw new IllegalArgumentException("byteString == null");
        }
        fVar.a(this);
        return this;
    }

    /* renamed from: a */
    public c b(String str) {
        return a(str, 0, str.length());
    }

    /* JADX WARNING: CFG modification limit reached, blocks count: 152 */
    public c a(String str, int i, int i2) {
        int i3;
        if (str == null) {
            throw new IllegalArgumentException("string == null");
        } else if (i < 0) {
            throw new IllegalAccessError("beginIndex < 0: " + i);
        } else if (i2 < i) {
            throw new IllegalArgumentException("endIndex < beginIndex: " + i2 + " < " + i);
        } else if (i2 > str.length()) {
            throw new IllegalArgumentException("endIndex > string.length: " + i2 + " > " + str.length());
        } else {
            while (i < i2) {
                char charAt = str.charAt(i);
                if (charAt < 128) {
                    o e = e(1);
                    byte[] bArr = e.f1390a;
                    int i4 = e.c - i;
                    int min = Math.min(i2, 8192 - i4);
                    i3 = i + 1;
                    bArr[i4 + i] = (byte) charAt;
                    while (i3 < min) {
                        char charAt2 = str.charAt(i3);
                        if (charAt2 < 128) {
                            int i5 = i3 + 1;
                            bArr[i3 + i4] = (byte) charAt2;
                            i3 = i5;
                        }
                    }
                    int i6 = (i3 + i4) - e.c;
                    e.c += i6;
                    this.f1367b += (long) i6;
                    break;
                } else if (charAt < 2048) {
                    i((charAt >> 6) | 192);
                    i((int) (charAt & '?') | 128);
                    i3 = i + 1;
                } else if (charAt < 55296 || charAt > 57343) {
                    i((charAt >> 12) | 224);
                    i(((charAt >> 6) & 63) | 128);
                    i((int) (charAt & '?') | 128);
                    i3 = i + 1;
                } else {
                    char charAt3 = i + 1 < i2 ? str.charAt(i + 1) : 0;
                    if (charAt > 56319 || charAt3 < 56320 || charAt3 > 57343) {
                        i(63);
                        i++;
                    } else {
                        int i7 = ((charAt3 & 9215) | ((charAt & 10239) << 10)) + 0;
                        i((i7 >> 18) | 240);
                        i(((i7 >> 12) & 63) | 128);
                        i(((i7 >> 6) & 63) | 128);
                        i((i7 & 63) | 128);
                        i3 = i + 2;
                    }
                }
                i = i3;
            }
            return this;
        }
    }

    public c a(String str, int i, int i2, Charset charset) {
        if (str == null) {
            throw new IllegalArgumentException("string == null");
        } else if (i < 0) {
            throw new IllegalAccessError("beginIndex < 0: " + i);
        } else if (i2 < i) {
            throw new IllegalArgumentException("endIndex < beginIndex: " + i2 + " < " + i);
        } else if (i2 > str.length()) {
            throw new IllegalArgumentException("endIndex > string.length: " + i2 + " > " + str.length());
        } else if (charset == null) {
            throw new IllegalArgumentException("charset == null");
        } else if (charset.equals(u.f1396a)) {
            return b(str);
        } else {
            byte[] bytes = str.substring(i, i2).getBytes(charset);
            return c(bytes, 0, bytes.length);
        }
    }

    public c a(String str, Charset charset) {
        return a(str, 0, str.length(), charset);
    }

    public String a(long j, Charset charset) throws EOFException {
        u.a(this.f1367b, 0, j);
        if (charset == null) {
            throw new IllegalArgumentException("charset == null");
        } else if (j > 2147483647L) {
            throw new IllegalArgumentException("byteCount > Integer.MAX_VALUE: " + j);
        } else if (j == 0) {
            return "";
        } else {
            o oVar = this.f1366a;
            if (((long) oVar.f1391b) + j > ((long) oVar.c)) {
                return new String(f(j), charset);
            }
            String str = new String(oVar.f1390a, oVar.f1391b, (int) j, charset);
            oVar.f1391b = (int) (((long) oVar.f1391b) + j);
            this.f1367b -= j;
            if (oVar.f1391b != oVar.c) {
                return str;
            }
            this.f1366a = oVar.a();
            p.a(oVar);
            return str;
        }
    }

    public void a(long j) throws EOFException {
        if (this.f1367b < j) {
            throw new EOFException();
        }
    }

    public void a(c cVar, long j) {
        if (cVar == null) {
            throw new IllegalArgumentException("source == null");
        } else if (cVar == this) {
            throw new IllegalArgumentException("source == this");
        } else {
            u.a(cVar.f1367b, 0, j);
            while (j > 0) {
                if (j < ((long) (cVar.f1366a.c - cVar.f1366a.f1391b))) {
                    o oVar = this.f1366a != null ? this.f1366a.g : null;
                    if (oVar != null && oVar.e) {
                        if ((((long) oVar.c) + j) - ((long) (oVar.d ? 0 : oVar.f1391b)) <= 8192) {
                            cVar.f1366a.a(oVar, (int) j);
                            cVar.f1367b -= j;
                            this.f1367b += j;
                            return;
                        }
                    }
                    cVar.f1366a = cVar.f1366a.a((int) j);
                }
                o oVar2 = cVar.f1366a;
                long j2 = (long) (oVar2.c - oVar2.f1391b);
                cVar.f1366a = oVar2.a();
                if (this.f1366a == null) {
                    this.f1366a = oVar2;
                    o oVar3 = this.f1366a;
                    o oVar4 = this.f1366a;
                    o oVar5 = this.f1366a;
                    oVar4.g = oVar5;
                    oVar3.f = oVar5;
                } else {
                    this.f1366a.g.a(oVar2).b();
                }
                cVar.f1367b -= j2;
                this.f1367b += j2;
                j -= j2;
            }
        }
    }

    public void a(byte[] bArr) throws EOFException {
        int i = 0;
        while (i < bArr.length) {
            int a2 = a(bArr, i, bArr.length - i);
            if (a2 == -1) {
                throw new EOFException();
            }
            i += a2;
        }
    }

    public byte b(long j) {
        u.a(this.f1367b, j, 1);
        o oVar = this.f1366a;
        while (true) {
            int i = oVar.c - oVar.f1391b;
            if (j < ((long) i)) {
                return oVar.f1390a[oVar.f1391b + ((int) j)];
            }
            j -= (long) i;
            oVar = oVar.f;
        }
    }

    public c b() {
        return this;
    }

    /* renamed from: b */
    public c i(int i) {
        o e = e(1);
        byte[] bArr = e.f1390a;
        int i2 = e.c;
        e.c = i2 + 1;
        bArr[i2] = (byte) i;
        this.f1367b++;
        return this;
    }

    /* renamed from: b */
    public c c(byte[] bArr) {
        if (bArr != null) {
            return c(bArr, 0, bArr.length);
        }
        throw new IllegalArgumentException("source == null");
    }

    /* renamed from: b */
    public c c(byte[] bArr, int i, int i2) {
        if (bArr == null) {
            throw new IllegalArgumentException("source == null");
        }
        u.a((long) bArr.length, (long) i, (long) i2);
        int i3 = i + i2;
        while (i < i3) {
            o e = e(1);
            int min = Math.min(i3 - i, 8192 - e.c);
            System.arraycopy(bArr, i, e.f1390a, e.c, min);
            i += min;
            e.c = min + e.c;
        }
        this.f1367b += (long) i2;
        return this;
    }

    /* renamed from: c */
    public c h(int i) {
        o e = e(2);
        byte[] bArr = e.f1390a;
        int i2 = e.c;
        int i3 = i2 + 1;
        bArr[i2] = (byte) ((i >>> 8) & 255);
        int i4 = i3 + 1;
        bArr[i3] = (byte) (i & 255);
        e.c = i4;
        this.f1367b += 2;
        return this;
    }

    public f c(long j) throws EOFException {
        return new f(f(j));
    }

    public OutputStream c() {
        return new OutputStream() {
            public void close() {
            }

            public void flush() {
            }

            public String toString() {
                return this + ".outputStream()";
            }

            public void write(int i) {
                c.this.i((int) (byte) i);
            }

            public void write(byte[] bArr, int i, int i2) {
                c.this.c(bArr, i, i2);
            }
        };
    }

    public void close() {
    }

    /* renamed from: d */
    public c v() {
        return this;
    }

    /* renamed from: d */
    public c g(int i) {
        o e = e(4);
        byte[] bArr = e.f1390a;
        int i2 = e.c;
        int i3 = i2 + 1;
        bArr[i2] = (byte) ((i >>> 24) & 255);
        int i4 = i3 + 1;
        bArr[i3] = (byte) ((i >>> 16) & 255);
        int i5 = i4 + 1;
        bArr[i4] = (byte) ((i >>> 8) & 255);
        int i6 = i5 + 1;
        bArr[i5] = (byte) (i & 255);
        e.c = i6;
        this.f1367b += 4;
        return this;
    }

    public String d(long j) throws EOFException {
        return a(j, u.f1396a);
    }

    public d e() {
        return this;
    }

    /* access modifiers changed from: 0000 */
    public o e(int i) {
        if (i < 1 || i > 8192) {
            throw new IllegalArgumentException();
        } else if (this.f1366a == null) {
            this.f1366a = p.a();
            o oVar = this.f1366a;
            o oVar2 = this.f1366a;
            o oVar3 = this.f1366a;
            oVar2.g = oVar3;
            oVar.f = oVar3;
            return oVar3;
        } else {
            o oVar4 = this.f1366a.g;
            return (oVar4.c + i > 8192 || !oVar4.e) ? oVar4.a(p.a()) : oVar4;
        }
    }

    /* access modifiers changed from: 0000 */
    public String e(long j) throws EOFException {
        if (j <= 0 || b(j - 1) != 13) {
            String d = d(j);
            g(1);
            return d;
        }
        String d2 = d(j - 1);
        g(2);
        return d2;
    }

    public boolean equals(Object obj) {
        long j = 0;
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof c)) {
            return false;
        }
        c cVar = (c) obj;
        if (this.f1367b != cVar.f1367b) {
            return false;
        }
        if (this.f1367b == 0) {
            return true;
        }
        o oVar = this.f1366a;
        o oVar2 = cVar.f1366a;
        int i = oVar.f1391b;
        int i2 = oVar2.f1391b;
        while (j < this.f1367b) {
            long min = (long) Math.min(oVar.c - i, oVar2.c - i2);
            int i3 = 0;
            while (((long) i3) < min) {
                int i4 = i + 1;
                byte b2 = oVar.f1390a[i];
                int i5 = i2 + 1;
                if (b2 != oVar2.f1390a[i2]) {
                    return false;
                }
                i3++;
                i2 = i5;
                i = i4;
            }
            if (i == oVar.c) {
                oVar = oVar.f;
                i = oVar.f1391b;
            }
            if (i2 == oVar2.c) {
                oVar2 = oVar2.f;
                i2 = oVar2.f1391b;
            }
            j += min;
        }
        return true;
    }

    public f f(int i) {
        return i == 0 ? f.f1371b : new q(this, i);
    }

    public boolean f() {
        return this.f1367b == 0;
    }

    public byte[] f(long j) throws EOFException {
        u.a(this.f1367b, 0, j);
        if (j > 2147483647L) {
            throw new IllegalArgumentException("byteCount > Integer.MAX_VALUE: " + j);
        }
        byte[] bArr = new byte[((int) j)];
        a(bArr);
        return bArr;
    }

    public void flush() {
    }

    public InputStream g() {
        return new InputStream() {
            public int available() {
                return (int) Math.min(c.this.f1367b, 2147483647L);
            }

            public void close() {
            }

            public int read() {
                if (c.this.f1367b > 0) {
                    return c.this.i() & 255;
                }
                return -1;
            }

            public int read(byte[] bArr, int i, int i2) {
                return c.this.a(bArr, i, i2);
            }

            public String toString() {
                return c.this + ".inputStream()";
            }
        };
    }

    public void g(long j) throws EOFException {
        while (j > 0) {
            if (this.f1366a == null) {
                throw new EOFException();
            }
            int min = (int) Math.min(j, (long) (this.f1366a.c - this.f1366a.f1391b));
            this.f1367b -= (long) min;
            j -= (long) min;
            o oVar = this.f1366a;
            oVar.f1391b = min + oVar.f1391b;
            if (this.f1366a.f1391b == this.f1366a.c) {
                o oVar2 = this.f1366a;
                this.f1366a = oVar2.a();
                p.a(oVar2);
            }
        }
    }

    public long h() {
        long j = this.f1367b;
        if (j == 0) {
            return 0;
        }
        o oVar = this.f1366a.g;
        return (oVar.c >= 8192 || !oVar.e) ? j : j - ((long) (oVar.c - oVar.f1391b));
    }

    /* renamed from: h */
    public c k(long j) {
        boolean z;
        long j2;
        if (j == 0) {
            return i(48);
        }
        if (j < 0) {
            j2 = -j;
            if (j2 < 0) {
                return b("-9223372036854775808");
            }
            z = true;
        } else {
            z = false;
            j2 = j;
        }
        int i = j2 < 100000000 ? j2 < 10000 ? j2 < 100 ? j2 < 10 ? 1 : 2 : j2 < 1000 ? 3 : 4 : j2 < 1000000 ? j2 < 100000 ? 5 : 6 : j2 < 10000000 ? 7 : 8 : j2 < 1000000000000L ? j2 < 10000000000L ? j2 < 1000000000 ? 9 : 10 : j2 < 100000000000L ? 11 : 12 : j2 < 1000000000000000L ? j2 < 10000000000000L ? 13 : j2 < 100000000000000L ? 14 : 15 : j2 < 100000000000000000L ? j2 < 10000000000000000L ? 16 : 17 : j2 < 1000000000000000000L ? 18 : 19;
        if (z) {
            i++;
        }
        o e = e(i);
        byte[] bArr = e.f1390a;
        int i2 = e.c + i;
        while (j2 != 0) {
            i2--;
            bArr[i2] = c[(int) (j2 % 10)];
            j2 /= 10;
        }
        if (z) {
            bArr[i2 - 1] = 45;
        }
        e.c += i;
        this.f1367b = ((long) i) + this.f1367b;
        return this;
    }

    public int hashCode() {
        o oVar = this.f1366a;
        if (oVar == null) {
            return 0;
        }
        int i = 1;
        do {
            int i2 = oVar.f1391b;
            while (i2 < oVar.c) {
                int i3 = oVar.f1390a[i2] + (i * 31);
                i2++;
                i = i3;
            }
            oVar = oVar.f;
        } while (oVar != this.f1366a);
        return i;
    }

    public byte i() {
        if (this.f1367b == 0) {
            throw new IllegalStateException("size == 0");
        }
        o oVar = this.f1366a;
        int i = oVar.f1391b;
        int i2 = oVar.c;
        int i3 = i + 1;
        byte b2 = oVar.f1390a[i];
        this.f1367b--;
        if (i3 == i2) {
            this.f1366a = oVar.a();
            p.a(oVar);
        } else {
            oVar.f1391b = i3;
        }
        return b2;
    }

    /* renamed from: i */
    public c j(long j) {
        if (j == 0) {
            return i(48);
        }
        int numberOfTrailingZeros = (Long.numberOfTrailingZeros(Long.highestOneBit(j)) / 4) + 1;
        o e = e(numberOfTrailingZeros);
        byte[] bArr = e.f1390a;
        int i = e.c;
        for (int i2 = (e.c + numberOfTrailingZeros) - 1; i2 >= i; i2--) {
            bArr[i2] = c[(int) (15 & j)];
            j >>>= 4;
        }
        e.c += numberOfTrailingZeros;
        this.f1367b = ((long) numberOfTrailingZeros) + this.f1367b;
        return this;
    }

    public short j() {
        if (this.f1367b < 2) {
            throw new IllegalStateException("size < 2: " + this.f1367b);
        }
        o oVar = this.f1366a;
        int i = oVar.f1391b;
        int i2 = oVar.c;
        if (i2 - i < 2) {
            return (short) (((i() & 255) << 8) | (i() & 255));
        }
        byte[] bArr = oVar.f1390a;
        int i3 = i + 1;
        int i4 = i3 + 1;
        byte b2 = ((bArr[i] & 255) << 8) | (bArr[i3] & 255);
        this.f1367b -= 2;
        if (i4 == i2) {
            this.f1366a = oVar.a();
            p.a(oVar);
        } else {
            oVar.f1391b = i4;
        }
        return (short) b2;
    }

    public int k() {
        if (this.f1367b < 4) {
            throw new IllegalStateException("size < 4: " + this.f1367b);
        }
        o oVar = this.f1366a;
        int i = oVar.f1391b;
        int i2 = oVar.c;
        if (i2 - i < 4) {
            return ((i() & 255) << 24) | ((i() & 255) << 16) | ((i() & 255) << 8) | (i() & 255);
        }
        byte[] bArr = oVar.f1390a;
        int i3 = i + 1;
        int i4 = i3 + 1;
        byte b2 = ((bArr[i] & 255) << 24) | ((bArr[i3] & 255) << 16);
        int i5 = i4 + 1;
        byte b3 = b2 | ((bArr[i4] & 255) << 8);
        int i6 = i5 + 1;
        byte b4 = b3 | (bArr[i5] & 255);
        this.f1367b -= 4;
        if (i6 == i2) {
            this.f1366a = oVar.a();
            p.a(oVar);
            return b4;
        }
        oVar.f1391b = i6;
        return b4;
    }

    public short l() {
        return u.a(j());
    }

    public int m() {
        return u.a(k());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:30:0x009c, code lost:
        if (r7 != r12) goto L_0x00c9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x009e, code lost:
        r18.f1366a = r10.a();
        b.p.a(r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00a9, code lost:
        if (r2 != false) goto L_0x00b1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00c9, code lost:
        r10.f1391b = r7;
     */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x009b  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x007e A[SYNTHETIC] */
    public long n() {
        int i;
        if (this.f1367b == 0) {
            throw new IllegalStateException("size == 0");
        }
        long j = 0;
        int i2 = 0;
        boolean z = false;
        do {
            o oVar = this.f1366a;
            byte[] bArr = oVar.f1390a;
            int i3 = oVar.f1391b;
            int i4 = oVar.c;
            int i5 = i3;
            while (true) {
                if (i5 >= i4) {
                    break;
                }
                byte b2 = bArr[i5];
                if (b2 >= 48 && b2 <= 57) {
                    i = b2 - 48;
                } else if (b2 >= 97 && b2 <= 102) {
                    i = (b2 - 97) + 10;
                } else if (b2 >= 65 && b2 <= 70) {
                    i = (b2 - 65) + 10;
                } else if (i2 != 0) {
                    throw new NumberFormatException("Expected leading [0-9a-fA-F] character but was 0x" + Integer.toHexString(b2));
                } else {
                    z = true;
                }
                if ((-1152921504606846976L & j) != 0) {
                    throw new NumberFormatException("Number too large: " + new c().j(j).i((int) b2).p());
                }
                i2++;
                i5++;
                j = ((long) i) | (j << 4);
            }
            if (i2 != 0) {
            }
        } while (this.f1366a != null);
        this.f1367b -= (long) i2;
        return j;
    }

    public f o() {
        return new f(r());
    }

    public String p() {
        try {
            return a(this.f1367b, u.f1396a);
        } catch (EOFException e) {
            throw new AssertionError(e);
        }
    }

    public String q() throws EOFException {
        long a2 = a(10);
        if (a2 != -1) {
            return e(a2);
        }
        c cVar = new c();
        a(cVar, 0, Math.min(32, this.f1367b));
        throw new EOFException("\\n not found: size=" + a() + " content=" + cVar.o().c() + "…");
    }

    public byte[] r() {
        try {
            return f(this.f1367b);
        } catch (EOFException e) {
            throw new AssertionError(e);
        }
    }

    public long read(c cVar, long j) {
        if (cVar == null) {
            throw new IllegalArgumentException("sink == null");
        } else if (j < 0) {
            throw new IllegalArgumentException("byteCount < 0: " + j);
        } else if (this.f1367b == 0) {
            return -1;
        } else {
            if (j > this.f1367b) {
                j = this.f1367b;
            }
            cVar.a(this, j);
            return j;
        }
    }

    public void s() {
        try {
            g(this.f1367b);
        } catch (EOFException e) {
            throw new AssertionError(e);
        }
    }

    /* renamed from: t */
    public c clone() {
        c cVar = new c();
        if (this.f1367b == 0) {
            return cVar;
        }
        cVar.f1366a = new o(this.f1366a);
        o oVar = cVar.f1366a;
        o oVar2 = cVar.f1366a;
        o oVar3 = cVar.f1366a;
        oVar2.g = oVar3;
        oVar.f = oVar3;
        for (o oVar4 = this.f1366a.f; oVar4 != this.f1366a; oVar4 = oVar4.f) {
            cVar.f1366a.g.a(new o(oVar4));
        }
        cVar.f1367b = this.f1367b;
        return cVar;
    }

    public t timeout() {
        return t.f1394b;
    }

    public String toString() {
        return u().toString();
    }

    public f u() {
        if (this.f1367b <= 2147483647L) {
            return f((int) this.f1367b);
        }
        throw new IllegalArgumentException("size > Integer.MAX_VALUE: " + this.f1367b);
    }
}
