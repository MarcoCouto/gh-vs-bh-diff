package b;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class l {

    /* renamed from: a reason: collision with root package name */
    static final Logger f1379a = Logger.getLogger(l.class.getName());

    private l() {
    }

    public static d a(r rVar) {
        if (rVar != null) {
            return new m(rVar);
        }
        throw new IllegalArgumentException("sink == null");
    }

    public static e a(s sVar) {
        if (sVar != null) {
            return new n(sVar);
        }
        throw new IllegalArgumentException("source == null");
    }

    private static r a(final OutputStream outputStream, final t tVar) {
        if (outputStream == null) {
            throw new IllegalArgumentException("out == null");
        } else if (tVar != null) {
            return new r() {
                public void a(c cVar, long j) throws IOException {
                    u.a(cVar.f1367b, 0, j);
                    while (j > 0) {
                        tVar.g();
                        o oVar = cVar.f1366a;
                        int min = (int) Math.min(j, (long) (oVar.c - oVar.f1391b));
                        outputStream.write(oVar.f1390a, oVar.f1391b, min);
                        oVar.f1391b += min;
                        j -= (long) min;
                        cVar.f1367b -= (long) min;
                        if (oVar.f1391b == oVar.c) {
                            cVar.f1366a = oVar.a();
                            p.a(oVar);
                        }
                    }
                }

                public void close() throws IOException {
                    outputStream.close();
                }

                public void flush() throws IOException {
                    outputStream.flush();
                }

                public t timeout() {
                    return tVar;
                }

                public String toString() {
                    return "sink(" + outputStream + ")";
                }
            };
        } else {
            throw new IllegalArgumentException("timeout == null");
        }
    }

    public static r a(Socket socket) throws IOException {
        if (socket == null) {
            throw new IllegalArgumentException("socket == null");
        }
        a c = c(socket);
        return c.a(a(socket.getOutputStream(), (t) c));
    }

    public static s a(File file) throws FileNotFoundException {
        if (file != null) {
            return a((InputStream) new FileInputStream(file));
        }
        throw new IllegalArgumentException("file == null");
    }

    public static s a(InputStream inputStream) {
        return a(inputStream, new t());
    }

    private static s a(final InputStream inputStream, final t tVar) {
        if (inputStream == null) {
            throw new IllegalArgumentException("in == null");
        } else if (tVar != null) {
            return new s() {
                public void close() throws IOException {
                    inputStream.close();
                }

                public long read(c cVar, long j) throws IOException {
                    if (j < 0) {
                        throw new IllegalArgumentException("byteCount < 0: " + j);
                    } else if (j == 0) {
                        return 0;
                    } else {
                        try {
                            tVar.g();
                            o e = cVar.e(1);
                            int read = inputStream.read(e.f1390a, e.c, (int) Math.min(j, (long) (8192 - e.c)));
                            if (read == -1) {
                                return -1;
                            }
                            e.c += read;
                            cVar.f1367b += (long) read;
                            return (long) read;
                        } catch (AssertionError e2) {
                            if (l.a(e2)) {
                                throw new IOException(e2);
                            }
                            throw e2;
                        }
                    }
                }

                public t timeout() {
                    return tVar;
                }

                public String toString() {
                    return "source(" + inputStream + ")";
                }
            };
        } else {
            throw new IllegalArgumentException("timeout == null");
        }
    }

    static boolean a(AssertionError assertionError) {
        return (assertionError.getCause() == null || assertionError.getMessage() == null || !assertionError.getMessage().contains("getsockname failed")) ? false : true;
    }

    public static s b(Socket socket) throws IOException {
        if (socket == null) {
            throw new IllegalArgumentException("socket == null");
        }
        a c = c(socket);
        return c.a(a(socket.getInputStream(), (t) c));
    }

    private static a c(final Socket socket) {
        return new a() {
            /* access modifiers changed from: protected */
            public IOException a(IOException iOException) {
                SocketTimeoutException socketTimeoutException = new SocketTimeoutException("timeout");
                if (iOException != null) {
                    socketTimeoutException.initCause(iOException);
                }
                return socketTimeoutException;
            }

            /* access modifiers changed from: protected */
            public void a() {
                try {
                    socket.close();
                } catch (Exception e) {
                    l.f1379a.log(Level.WARNING, "Failed to close timed out socket " + socket, e);
                } catch (AssertionError e2) {
                    if (l.a(e2)) {
                        l.f1379a.log(Level.WARNING, "Failed to close timed out socket " + socket, e2);
                        return;
                    }
                    throw e2;
                }
            }
        };
    }
}
