package b;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.util.concurrent.TimeUnit;

public class t {

    /* renamed from: b reason: collision with root package name */
    public static final t f1394b = new t() {
        public t a(long j) {
            return this;
        }

        public t a(long j, TimeUnit timeUnit) {
            return this;
        }

        public void g() throws IOException {
        }
    };

    /* renamed from: a reason: collision with root package name */
    private boolean f1395a;
    private long c;
    private long d;

    public long C_() {
        return this.d;
    }

    public boolean D_() {
        return this.f1395a;
    }

    public t E_() {
        this.d = 0;
        return this;
    }

    public t a(long j) {
        this.f1395a = true;
        this.c = j;
        return this;
    }

    public t a(long j, TimeUnit timeUnit) {
        if (j < 0) {
            throw new IllegalArgumentException("timeout < 0: " + j);
        } else if (timeUnit == null) {
            throw new IllegalArgumentException("unit == null");
        } else {
            this.d = timeUnit.toNanos(j);
            return this;
        }
    }

    public long d() {
        if (this.f1395a) {
            return this.c;
        }
        throw new IllegalStateException("No deadline");
    }

    public t f() {
        this.f1395a = false;
        return this;
    }

    public void g() throws IOException {
        if (Thread.interrupted()) {
            throw new InterruptedIOException("thread interrupted");
        } else if (this.f1395a && this.c - System.nanoTime() <= 0) {
            throw new InterruptedIOException("deadline reached");
        }
    }
}
