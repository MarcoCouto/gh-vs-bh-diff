package b;

import java.io.IOException;

public interface d extends r {
    long a(s sVar) throws IOException;

    c b();

    d b(f fVar) throws IOException;

    d b(String str) throws IOException;

    d c(byte[] bArr) throws IOException;

    d c(byte[] bArr, int i, int i2) throws IOException;

    d e() throws IOException;

    void flush() throws IOException;

    d g(int i) throws IOException;

    d h(int i) throws IOException;

    d i(int i) throws IOException;

    d j(long j) throws IOException;

    d k(long j) throws IOException;

    d v() throws IOException;
}
