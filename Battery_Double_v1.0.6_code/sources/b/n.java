package b;

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;

final class n implements e {

    /* renamed from: a reason: collision with root package name */
    public final c f1387a = new c();

    /* renamed from: b reason: collision with root package name */
    public final s f1388b;
    boolean c;

    n(s sVar) {
        if (sVar == null) {
            throw new IllegalArgumentException("source == null");
        }
        this.f1388b = sVar;
    }

    public long a(byte b2) throws IOException {
        return a(b2, 0);
    }

    public long a(byte b2, long j) throws IOException {
        if (this.c) {
            throw new IllegalStateException("closed");
        }
        while (true) {
            long a2 = this.f1387a.a(b2, j);
            if (a2 != -1) {
                return a2;
            }
            long j2 = this.f1387a.f1367b;
            if (this.f1388b.read(this.f1387a, 8192) == -1) {
                return -1;
            }
            j = Math.max(j, j2);
        }
    }

    public long a(r rVar) throws IOException {
        if (rVar == null) {
            throw new IllegalArgumentException("sink == null");
        }
        long j = 0;
        while (this.f1388b.read(this.f1387a, 8192) != -1) {
            long h = this.f1387a.h();
            if (h > 0) {
                j += h;
                rVar.a(this.f1387a, h);
            }
        }
        if (this.f1387a.a() <= 0) {
            return j;
        }
        long a2 = j + this.f1387a.a();
        rVar.a(this.f1387a, this.f1387a.a());
        return a2;
    }

    public void a(long j) throws IOException {
        if (!b(j)) {
            throw new EOFException();
        }
    }

    public c b() {
        return this.f1387a;
    }

    public boolean b(long j) throws IOException {
        if (j < 0) {
            throw new IllegalArgumentException("byteCount < 0: " + j);
        } else if (this.c) {
            throw new IllegalStateException("closed");
        } else {
            while (this.f1387a.f1367b < j) {
                if (this.f1388b.read(this.f1387a, 8192) == -1) {
                    return false;
                }
            }
            return true;
        }
    }

    public f c(long j) throws IOException {
        a(j);
        return this.f1387a.c(j);
    }

    public void close() throws IOException {
        if (!this.c) {
            this.c = true;
            this.f1388b.close();
            this.f1387a.s();
        }
    }

    public boolean f() throws IOException {
        if (!this.c) {
            return this.f1387a.f() && this.f1388b.read(this.f1387a, 8192) == -1;
        }
        throw new IllegalStateException("closed");
    }

    public byte[] f(long j) throws IOException {
        a(j);
        return this.f1387a.f(j);
    }

    public InputStream g() {
        return new InputStream() {
            public int available() throws IOException {
                if (!n.this.c) {
                    return (int) Math.min(n.this.f1387a.f1367b, 2147483647L);
                }
                throw new IOException("closed");
            }

            public void close() throws IOException {
                n.this.close();
            }

            public int read() throws IOException {
                if (n.this.c) {
                    throw new IOException("closed");
                } else if (n.this.f1387a.f1367b == 0 && n.this.f1388b.read(n.this.f1387a, 8192) == -1) {
                    return -1;
                } else {
                    return n.this.f1387a.i() & 255;
                }
            }

            public int read(byte[] bArr, int i, int i2) throws IOException {
                if (n.this.c) {
                    throw new IOException("closed");
                }
                u.a((long) bArr.length, (long) i, (long) i2);
                if (n.this.f1387a.f1367b == 0 && n.this.f1388b.read(n.this.f1387a, 8192) == -1) {
                    return -1;
                }
                return n.this.f1387a.a(bArr, i, i2);
            }

            public String toString() {
                return n.this + ".inputStream()";
            }
        };
    }

    public void g(long j) throws IOException {
        if (this.c) {
            throw new IllegalStateException("closed");
        }
        while (j > 0) {
            if (this.f1387a.f1367b == 0 && this.f1388b.read(this.f1387a, 8192) == -1) {
                throw new EOFException();
            }
            long min = Math.min(j, this.f1387a.a());
            this.f1387a.g(min);
            j -= min;
        }
    }

    public byte i() throws IOException {
        a(1);
        return this.f1387a.i();
    }

    public short j() throws IOException {
        a(2);
        return this.f1387a.j();
    }

    public int k() throws IOException {
        a(4);
        return this.f1387a.k();
    }

    public short l() throws IOException {
        a(2);
        return this.f1387a.l();
    }

    public int m() throws IOException {
        a(4);
        return this.f1387a.m();
    }

    public long n() throws IOException {
        a(1);
        int i = 0;
        while (true) {
            if (!b((long) (i + 1))) {
                break;
            }
            byte b2 = this.f1387a.b((long) i);
            if ((b2 >= 48 && b2 <= 57) || ((b2 >= 97 && b2 <= 102) || (b2 >= 65 && b2 <= 70))) {
                i++;
            } else if (i == 0) {
                throw new NumberFormatException(String.format("Expected leading [0-9a-fA-F] character but was %#x", new Object[]{Byte.valueOf(b2)}));
            }
        }
        return this.f1387a.n();
    }

    public String q() throws IOException {
        long a2 = a(10);
        if (a2 != -1) {
            return this.f1387a.e(a2);
        }
        c cVar = new c();
        this.f1387a.a(cVar, 0, Math.min(32, this.f1387a.a()));
        throw new EOFException("\\n not found: size=" + this.f1387a.a() + " content=" + cVar.o().c() + "…");
    }

    public byte[] r() throws IOException {
        this.f1387a.a(this.f1388b);
        return this.f1387a.r();
    }

    public long read(c cVar, long j) throws IOException {
        if (cVar == null) {
            throw new IllegalArgumentException("sink == null");
        } else if (j < 0) {
            throw new IllegalArgumentException("byteCount < 0: " + j);
        } else if (this.c) {
            throw new IllegalStateException("closed");
        } else if (this.f1387a.f1367b == 0 && this.f1388b.read(this.f1387a, 8192) == -1) {
            return -1;
        } else {
            return this.f1387a.read(cVar, Math.min(j, this.f1387a.f1367b));
        }
    }

    public t timeout() {
        return this.f1388b.timeout();
    }

    public String toString() {
        return "buffer(" + this.f1388b + ")";
    }
}
