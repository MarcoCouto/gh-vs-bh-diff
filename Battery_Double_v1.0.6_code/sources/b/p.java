package b;

final class p {

    /* renamed from: a reason: collision with root package name */
    static o f1392a;

    /* renamed from: b reason: collision with root package name */
    static long f1393b;

    private p() {
    }

    static o a() {
        synchronized (p.class) {
            if (f1392a == null) {
                return new o();
            }
            o oVar = f1392a;
            f1392a = oVar.f;
            oVar.f = null;
            f1393b -= 8192;
            return oVar;
        }
    }

    static void a(o oVar) {
        if (oVar.f != null || oVar.g != null) {
            throw new IllegalArgumentException();
        } else if (!oVar.d) {
            synchronized (p.class) {
                if (f1393b + 8192 <= 65536) {
                    f1393b += 8192;
                    oVar.f = f1392a;
                    oVar.c = 0;
                    oVar.f1391b = 0;
                    f1392a = oVar;
                }
            }
        }
    }
}
