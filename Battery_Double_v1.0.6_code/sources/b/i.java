package b;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class i extends t {

    /* renamed from: a reason: collision with root package name */
    private t f1374a;

    public i(t tVar) {
        if (tVar == null) {
            throw new IllegalArgumentException("delegate == null");
        }
        this.f1374a = tVar;
    }

    public long C_() {
        return this.f1374a.C_();
    }

    public boolean D_() {
        return this.f1374a.D_();
    }

    public t E_() {
        return this.f1374a.E_();
    }

    public final i a(t tVar) {
        if (tVar == null) {
            throw new IllegalArgumentException("delegate == null");
        }
        this.f1374a = tVar;
        return this;
    }

    public final t a() {
        return this.f1374a;
    }

    public t a(long j) {
        return this.f1374a.a(j);
    }

    public t a(long j, TimeUnit timeUnit) {
        return this.f1374a.a(j, timeUnit);
    }

    public long d() {
        return this.f1374a.d();
    }

    public t f() {
        return this.f1374a.f();
    }

    public void g() throws IOException {
        this.f1374a.g();
    }
}
