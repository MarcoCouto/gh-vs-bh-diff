package b;

import java.io.Serializable;
import java.util.Arrays;

public class f implements Serializable, Comparable<f> {

    /* renamed from: a reason: collision with root package name */
    static final char[] f1370a = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

    /* renamed from: b reason: collision with root package name */
    public static final f f1371b = a(new byte[0]);
    final byte[] c;
    transient int d;
    transient String e;

    f(byte[] bArr) {
        this.c = bArr;
    }

    static int a(String str, int i) {
        int i2 = 0;
        int length = str.length();
        int i3 = 0;
        while (i2 < length) {
            if (i3 == i) {
                return i2;
            }
            int codePointAt = str.codePointAt(i2);
            if ((Character.isISOControl(codePointAt) && codePointAt != 10 && codePointAt != 13) || codePointAt == 65533) {
                return -1;
            }
            i3++;
            i2 += Character.charCount(codePointAt);
        }
        return str.length();
    }

    public static f a(String str) {
        if (str == null) {
            throw new IllegalArgumentException("s == null");
        }
        f fVar = new f(str.getBytes(u.f1396a));
        fVar.e = str;
        return fVar;
    }

    public static f a(byte... bArr) {
        if (bArr != null) {
            return new f((byte[]) bArr.clone());
        }
        throw new IllegalArgumentException("data == null");
    }

    public byte a(int i) {
        return this.c[i];
    }

    /* renamed from: a */
    public int compareTo(f fVar) {
        int e2 = e();
        int e3 = fVar.e();
        int min = Math.min(e2, e3);
        for (int i = 0; i < min; i++) {
            byte a2 = a(i) & 255;
            byte a3 = fVar.a(i) & 255;
            if (a2 != a3) {
                return a2 < a3 ? -1 : 1;
            }
        }
        if (e2 == e3) {
            return 0;
        }
        return e2 >= e3 ? 1 : -1;
    }

    public f a(int i, int i2) {
        if (i < 0) {
            throw new IllegalArgumentException("beginIndex < 0");
        } else if (i2 > this.c.length) {
            throw new IllegalArgumentException("endIndex > length(" + this.c.length + ")");
        } else {
            int i3 = i2 - i;
            if (i3 < 0) {
                throw new IllegalArgumentException("endIndex < beginIndex");
            } else if (i == 0 && i2 == this.c.length) {
                return this;
            } else {
                byte[] bArr = new byte[i3];
                System.arraycopy(this.c, i, bArr, 0, i3);
                return new f(bArr);
            }
        }
    }

    public String a() {
        String str = this.e;
        if (str != null) {
            return str;
        }
        String str2 = new String(this.c, u.f1396a);
        this.e = str2;
        return str2;
    }

    /* access modifiers changed from: 0000 */
    public void a(c cVar) {
        cVar.c(this.c, 0, this.c.length);
    }

    public boolean a(int i, f fVar, int i2, int i3) {
        return fVar.a(i2, this.c, i, i3);
    }

    public boolean a(int i, byte[] bArr, int i2, int i3) {
        return i >= 0 && i <= this.c.length - i3 && i2 >= 0 && i2 <= bArr.length - i3 && u.a(this.c, i, bArr, i2, i3);
    }

    public String b() {
        return b.a(this.c);
    }

    public String c() {
        byte[] bArr;
        char[] cArr = new char[(this.c.length * 2)];
        int i = 0;
        for (byte b2 : this.c) {
            int i2 = i + 1;
            cArr[i] = f1370a[(b2 >> 4) & 15];
            i = i2 + 1;
            cArr[i2] = f1370a[b2 & 15];
        }
        return new String(cArr);
    }

    public f d() {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.c.length) {
                return this;
            }
            byte b2 = this.c[i2];
            if (b2 < 65 || b2 > 90) {
                i = i2 + 1;
            } else {
                byte[] bArr = (byte[]) this.c.clone();
                int i3 = i2 + 1;
                bArr[i2] = (byte) (b2 + 32);
                for (int i4 = i3; i4 < bArr.length; i4++) {
                    byte b3 = bArr[i4];
                    if (b3 >= 65 && b3 <= 90) {
                        bArr[i4] = (byte) (b3 + 32);
                    }
                }
                return new f(bArr);
            }
        }
    }

    public int e() {
        return this.c.length;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        return (obj instanceof f) && ((f) obj).e() == this.c.length && ((f) obj).a(0, this.c, 0, this.c.length);
    }

    public byte[] f() {
        return (byte[]) this.c.clone();
    }

    public int hashCode() {
        int i = this.d;
        if (i != 0) {
            return i;
        }
        int hashCode = Arrays.hashCode(this.c);
        this.d = hashCode;
        return hashCode;
    }

    public String toString() {
        if (this.c.length == 0) {
            return "[size=0]";
        }
        String a2 = a();
        int a3 = a(a2, 64);
        if (a3 == -1) {
            return this.c.length <= 64 ? "[hex=" + c() + "]" : "[size=" + this.c.length + " hex=" + a(0, 64).c() + "…]";
        }
        String replace = a2.substring(0, a3).replace("\\", "\\\\").replace("\n", "\\n").replace("\r", "\\r");
        return a3 < a2.length() ? "[size=" + this.c.length + " text=" + replace + "…]" : "[text=" + replace + "]";
    }
}
