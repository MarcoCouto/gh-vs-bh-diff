package b;

final class o {

    /* renamed from: a reason: collision with root package name */
    final byte[] f1390a;

    /* renamed from: b reason: collision with root package name */
    int f1391b;
    int c;
    boolean d;
    boolean e;
    o f;
    o g;

    o() {
        this.f1390a = new byte[8192];
        this.e = true;
        this.d = false;
    }

    o(o oVar) {
        this(oVar.f1390a, oVar.f1391b, oVar.c);
        oVar.d = true;
    }

    o(byte[] bArr, int i, int i2) {
        this.f1390a = bArr;
        this.f1391b = i;
        this.c = i2;
        this.e = false;
        this.d = true;
    }

    public o a() {
        o oVar = this.f != this ? this.f : null;
        this.g.f = this.f;
        this.f.g = this.g;
        this.f = null;
        this.g = null;
        return oVar;
    }

    public o a(int i) {
        o a2;
        if (i <= 0 || i > this.c - this.f1391b) {
            throw new IllegalArgumentException();
        }
        if (i >= 1024) {
            a2 = new o(this);
        } else {
            a2 = p.a();
            System.arraycopy(this.f1390a, this.f1391b, a2.f1390a, 0, i);
        }
        a2.c = a2.f1391b + i;
        this.f1391b += i;
        this.g.a(a2);
        return a2;
    }

    public o a(o oVar) {
        oVar.g = this;
        oVar.f = this.f;
        this.f.g = oVar;
        this.f = oVar;
        return oVar;
    }

    public void a(o oVar, int i) {
        if (!oVar.e) {
            throw new IllegalArgumentException();
        }
        if (oVar.c + i > 8192) {
            if (oVar.d) {
                throw new IllegalArgumentException();
            } else if ((oVar.c + i) - oVar.f1391b > 8192) {
                throw new IllegalArgumentException();
            } else {
                System.arraycopy(oVar.f1390a, oVar.f1391b, oVar.f1390a, 0, oVar.c - oVar.f1391b);
                oVar.c -= oVar.f1391b;
                oVar.f1391b = 0;
            }
        }
        System.arraycopy(this.f1390a, this.f1391b, oVar.f1390a, oVar.c, i);
        oVar.c += i;
        this.f1391b += i;
    }

    public void b() {
        if (this.g == this) {
            throw new IllegalStateException();
        } else if (this.g.e) {
            int i = this.c - this.f1391b;
            if (i <= (this.g.d ? 0 : this.g.f1391b) + (8192 - this.g.c)) {
                a(this.g, i);
                a();
                p.a(this);
            }
        }
    }
}
