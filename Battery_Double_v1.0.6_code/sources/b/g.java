package b;

import java.io.IOException;
import java.util.zip.Deflater;
import org.codehaus.mojo.animal_sniffer.IgnoreJRERequirement;

public final class g implements r {

    /* renamed from: a reason: collision with root package name */
    private final d f1372a;

    /* renamed from: b reason: collision with root package name */
    private final Deflater f1373b;
    private boolean c;

    g(d dVar, Deflater deflater) {
        if (dVar == null) {
            throw new IllegalArgumentException("source == null");
        } else if (deflater == null) {
            throw new IllegalArgumentException("inflater == null");
        } else {
            this.f1372a = dVar;
            this.f1373b = deflater;
        }
    }

    public g(r rVar, Deflater deflater) {
        this(l.a(rVar), deflater);
    }

    @IgnoreJRERequirement
    private void a(boolean z) throws IOException {
        o e;
        c b2 = this.f1372a.b();
        while (true) {
            e = b2.e(1);
            int deflate = z ? this.f1373b.deflate(e.f1390a, e.c, 8192 - e.c, 2) : this.f1373b.deflate(e.f1390a, e.c, 8192 - e.c);
            if (deflate > 0) {
                e.c += deflate;
                b2.f1367b += (long) deflate;
                this.f1372a.v();
            } else if (this.f1373b.needsInput()) {
                break;
            }
        }
        if (e.f1391b == e.c) {
            b2.f1366a = e.a();
            p.a(e);
        }
    }

    /* access modifiers changed from: 0000 */
    public void a() throws IOException {
        this.f1373b.finish();
        a(false);
    }

    public void a(c cVar, long j) throws IOException {
        u.a(cVar.f1367b, 0, j);
        while (j > 0) {
            o oVar = cVar.f1366a;
            int min = (int) Math.min(j, (long) (oVar.c - oVar.f1391b));
            this.f1373b.setInput(oVar.f1390a, oVar.f1391b, min);
            a(false);
            cVar.f1367b -= (long) min;
            oVar.f1391b += min;
            if (oVar.f1391b == oVar.c) {
                cVar.f1366a = oVar.a();
                p.a(oVar);
            }
            j -= (long) min;
        }
    }

    public void close() throws IOException {
        if (!this.c) {
            Throwable th = null;
            try {
                a();
            } catch (Throwable th2) {
                th = th2;
            }
            try {
                this.f1373b.end();
                th = th;
            } catch (Throwable th3) {
                th = th3;
                if (th != null) {
                    th = th;
                }
            }
            try {
                this.f1372a.close();
            } catch (Throwable th4) {
                if (th == null) {
                    th = th4;
                }
            }
            this.c = true;
            if (th != null) {
                u.a(th);
            }
        }
    }

    public void flush() throws IOException {
        a(true);
        this.f1372a.flush();
    }

    public t timeout() {
        return this.f1372a.timeout();
    }

    public String toString() {
        return "DeflaterSink(" + this.f1372a + ")";
    }
}
