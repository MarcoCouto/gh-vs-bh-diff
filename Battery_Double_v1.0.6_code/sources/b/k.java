package b;

import java.io.EOFException;
import java.io.IOException;
import java.util.zip.DataFormatException;
import java.util.zip.Inflater;

public final class k implements s {

    /* renamed from: a reason: collision with root package name */
    private final e f1377a;

    /* renamed from: b reason: collision with root package name */
    private final Inflater f1378b;
    private int c;
    private boolean d;

    k(e eVar, Inflater inflater) {
        if (eVar == null) {
            throw new IllegalArgumentException("source == null");
        } else if (inflater == null) {
            throw new IllegalArgumentException("inflater == null");
        } else {
            this.f1377a = eVar;
            this.f1378b = inflater;
        }
    }

    public k(s sVar, Inflater inflater) {
        this(l.a(sVar), inflater);
    }

    private void b() throws IOException {
        if (this.c != 0) {
            int remaining = this.c - this.f1378b.getRemaining();
            this.c -= remaining;
            this.f1377a.g((long) remaining);
        }
    }

    public boolean a() throws IOException {
        if (!this.f1378b.needsInput()) {
            return false;
        }
        b();
        if (this.f1378b.getRemaining() != 0) {
            throw new IllegalStateException("?");
        } else if (this.f1377a.f()) {
            return true;
        } else {
            o oVar = this.f1377a.b().f1366a;
            this.c = oVar.c - oVar.f1391b;
            this.f1378b.setInput(oVar.f1390a, oVar.f1391b, this.c);
            return false;
        }
    }

    public void close() throws IOException {
        if (!this.d) {
            this.f1378b.end();
            this.d = true;
            this.f1377a.close();
        }
    }

    public long read(c cVar, long j) throws IOException {
        boolean a2;
        if (j < 0) {
            throw new IllegalArgumentException("byteCount < 0: " + j);
        } else if (this.d) {
            throw new IllegalStateException("closed");
        } else if (j == 0) {
            return 0;
        } else {
            do {
                a2 = a();
                try {
                    o e = cVar.e(1);
                    int inflate = this.f1378b.inflate(e.f1390a, e.c, 8192 - e.c);
                    if (inflate > 0) {
                        e.c += inflate;
                        cVar.f1367b += (long) inflate;
                        return (long) inflate;
                    } else if (this.f1378b.finished() || this.f1378b.needsDictionary()) {
                        b();
                        if (e.f1391b == e.c) {
                            cVar.f1366a = e.a();
                            p.a(e);
                        }
                        return -1;
                    }
                } catch (DataFormatException e2) {
                    throw new IOException(e2);
                }
            } while (!a2);
            throw new EOFException("source exhausted prematurely");
        }
    }

    public t timeout() {
        return this.f1377a.timeout();
    }
}
