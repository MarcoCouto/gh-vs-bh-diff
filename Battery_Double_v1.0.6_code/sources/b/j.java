package b;

import java.io.EOFException;
import java.io.IOException;
import java.util.zip.CRC32;
import java.util.zip.Inflater;

public final class j implements s {

    /* renamed from: a reason: collision with root package name */
    private int f1375a = 0;

    /* renamed from: b reason: collision with root package name */
    private final e f1376b;
    private final Inflater c;
    private final k d;
    private final CRC32 e = new CRC32();

    public j(s sVar) {
        if (sVar == null) {
            throw new IllegalArgumentException("source == null");
        }
        this.c = new Inflater(true);
        this.f1376b = l.a(sVar);
        this.d = new k(this.f1376b, this.c);
    }

    private void a() throws IOException {
        this.f1376b.a(10);
        byte b2 = this.f1376b.b().b(3);
        boolean z = ((b2 >> 1) & 1) == 1;
        if (z) {
            a(this.f1376b.b(), 0, 10);
        }
        a("ID1ID2", 8075, (int) this.f1376b.j());
        this.f1376b.g(8);
        if (((b2 >> 2) & 1) == 1) {
            this.f1376b.a(2);
            if (z) {
                a(this.f1376b.b(), 0, 2);
            }
            short l = this.f1376b.b().l();
            this.f1376b.a((long) l);
            if (z) {
                a(this.f1376b.b(), 0, (long) l);
            }
            this.f1376b.g((long) l);
        }
        if (((b2 >> 3) & 1) == 1) {
            long a2 = this.f1376b.a(0);
            if (a2 == -1) {
                throw new EOFException();
            }
            if (z) {
                a(this.f1376b.b(), 0, 1 + a2);
            }
            this.f1376b.g(1 + a2);
        }
        if (((b2 >> 4) & 1) == 1) {
            long a3 = this.f1376b.a(0);
            if (a3 == -1) {
                throw new EOFException();
            }
            if (z) {
                a(this.f1376b.b(), 0, 1 + a3);
            }
            this.f1376b.g(1 + a3);
        }
        if (z) {
            a("FHCRC", (int) this.f1376b.l(), (int) (short) ((int) this.e.getValue()));
            this.e.reset();
        }
    }

    private void a(c cVar, long j, long j2) {
        o oVar = cVar.f1366a;
        while (j >= ((long) (oVar.c - oVar.f1391b))) {
            j -= (long) (oVar.c - oVar.f1391b);
            oVar = oVar.f;
        }
        while (j2 > 0) {
            int i = (int) (((long) oVar.f1391b) + j);
            int min = (int) Math.min((long) (oVar.c - i), j2);
            this.e.update(oVar.f1390a, i, min);
            j2 -= (long) min;
            oVar = oVar.f;
            j = 0;
        }
    }

    private void a(String str, int i, int i2) throws IOException {
        if (i2 != i) {
            throw new IOException(String.format("%s: actual 0x%08x != expected 0x%08x", new Object[]{str, Integer.valueOf(i2), Integer.valueOf(i)}));
        }
    }

    private void b() throws IOException {
        a("CRC", this.f1376b.m(), (int) this.e.getValue());
        a("ISIZE", this.f1376b.m(), this.c.getTotalOut());
    }

    public void close() throws IOException {
        this.d.close();
    }

    public long read(c cVar, long j) throws IOException {
        if (j < 0) {
            throw new IllegalArgumentException("byteCount < 0: " + j);
        } else if (j == 0) {
            return 0;
        } else {
            if (this.f1375a == 0) {
                a();
                this.f1375a = 1;
            }
            if (this.f1375a == 1) {
                long j2 = cVar.f1367b;
                long read = this.d.read(cVar, j);
                if (read != -1) {
                    a(cVar, j2, read);
                    return read;
                }
                this.f1375a = 2;
            }
            if (this.f1375a == 2) {
                b();
                this.f1375a = 3;
                if (!this.f1376b.f()) {
                    throw new IOException("gzip finished without exhausting source");
                }
            }
            return -1;
        }
    }

    public t timeout() {
        return this.f1376b.timeout();
    }
}
