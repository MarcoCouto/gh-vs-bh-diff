package b;

import java.io.IOException;

final class m implements d {

    /* renamed from: a reason: collision with root package name */
    public final c f1385a = new c();

    /* renamed from: b reason: collision with root package name */
    public final r f1386b;
    boolean c;

    m(r rVar) {
        if (rVar == null) {
            throw new IllegalArgumentException("sink == null");
        }
        this.f1386b = rVar;
    }

    public long a(s sVar) throws IOException {
        if (sVar == null) {
            throw new IllegalArgumentException("source == null");
        }
        long j = 0;
        while (true) {
            long read = sVar.read(this.f1385a, 8192);
            if (read == -1) {
                return j;
            }
            j += read;
            v();
        }
    }

    public void a(c cVar, long j) throws IOException {
        if (this.c) {
            throw new IllegalStateException("closed");
        }
        this.f1385a.a(cVar, j);
        v();
    }

    public c b() {
        return this.f1385a;
    }

    public d b(f fVar) throws IOException {
        if (this.c) {
            throw new IllegalStateException("closed");
        }
        this.f1385a.b(fVar);
        return v();
    }

    public d b(String str) throws IOException {
        if (this.c) {
            throw new IllegalStateException("closed");
        }
        this.f1385a.b(str);
        return v();
    }

    public d c(byte[] bArr) throws IOException {
        if (this.c) {
            throw new IllegalStateException("closed");
        }
        this.f1385a.c(bArr);
        return v();
    }

    public d c(byte[] bArr, int i, int i2) throws IOException {
        if (this.c) {
            throw new IllegalStateException("closed");
        }
        this.f1385a.c(bArr, i, i2);
        return v();
    }

    public void close() throws IOException {
        if (!this.c) {
            Throwable th = null;
            try {
                if (this.f1385a.f1367b > 0) {
                    this.f1386b.a(this.f1385a, this.f1385a.f1367b);
                }
            } catch (Throwable th2) {
                th = th2;
            }
            try {
                this.f1386b.close();
            } catch (Throwable th3) {
                if (th == null) {
                    th = th3;
                }
            }
            this.c = true;
            if (th != null) {
                u.a(th);
            }
        }
    }

    public d e() throws IOException {
        if (this.c) {
            throw new IllegalStateException("closed");
        }
        long a2 = this.f1385a.a();
        if (a2 > 0) {
            this.f1386b.a(this.f1385a, a2);
        }
        return this;
    }

    public void flush() throws IOException {
        if (this.c) {
            throw new IllegalStateException("closed");
        }
        if (this.f1385a.f1367b > 0) {
            this.f1386b.a(this.f1385a, this.f1385a.f1367b);
        }
        this.f1386b.flush();
    }

    public d g(int i) throws IOException {
        if (this.c) {
            throw new IllegalStateException("closed");
        }
        this.f1385a.g(i);
        return v();
    }

    public d h(int i) throws IOException {
        if (this.c) {
            throw new IllegalStateException("closed");
        }
        this.f1385a.h(i);
        return v();
    }

    public d i(int i) throws IOException {
        if (this.c) {
            throw new IllegalStateException("closed");
        }
        this.f1385a.i(i);
        return v();
    }

    public d j(long j) throws IOException {
        if (this.c) {
            throw new IllegalStateException("closed");
        }
        this.f1385a.j(j);
        return v();
    }

    public d k(long j) throws IOException {
        if (this.c) {
            throw new IllegalStateException("closed");
        }
        this.f1385a.k(j);
        return v();
    }

    public t timeout() {
        return this.f1386b.timeout();
    }

    public String toString() {
        return "buffer(" + this.f1386b + ")";
    }

    public d v() throws IOException {
        if (this.c) {
            throw new IllegalStateException("closed");
        }
        long h = this.f1385a.h();
        if (h > 0) {
            this.f1386b.a(this.f1385a, h);
        }
        return this;
    }
}
