package b;

import java.util.Arrays;

final class q extends f {
    final transient byte[][] f;
    final transient int[] g;

    q(c cVar, int i) {
        int i2 = 0;
        super(null);
        u.a(cVar.f1367b, 0, (long) i);
        o oVar = cVar.f1366a;
        int i3 = 0;
        int i4 = 0;
        while (i4 < i) {
            if (oVar.c == oVar.f1391b) {
                throw new AssertionError("s.limit == s.pos");
            }
            i4 += oVar.c - oVar.f1391b;
            i3++;
            oVar = oVar.f;
        }
        this.f = new byte[i3][];
        this.g = new int[(i3 * 2)];
        o oVar2 = cVar.f1366a;
        int i5 = 0;
        while (i2 < i) {
            this.f[i5] = oVar2.f1390a;
            int i6 = (oVar2.c - oVar2.f1391b) + i2;
            if (i6 > i) {
                i6 = i;
            }
            this.g[i5] = i6;
            this.g[this.f.length + i5] = oVar2.f1391b;
            oVar2.d = true;
            i5++;
            oVar2 = oVar2.f;
            i2 = i6;
        }
    }

    private int b(int i) {
        int binarySearch = Arrays.binarySearch(this.g, 0, this.f.length, i + 1);
        return binarySearch >= 0 ? binarySearch : binarySearch ^ -1;
    }

    private f g() {
        return new f(f());
    }

    public byte a(int i) {
        u.a((long) this.g[this.f.length - 1], (long) i, 1);
        int b2 = b(i);
        return this.f[b2][(i - (b2 == 0 ? 0 : this.g[b2 - 1])) + this.g[this.f.length + b2]];
    }

    public f a(int i, int i2) {
        return g().a(i, i2);
    }

    public String a() {
        return g().a();
    }

    /* access modifiers changed from: 0000 */
    public void a(c cVar) {
        int i = 0;
        int length = this.f.length;
        int i2 = 0;
        while (i < length) {
            int i3 = this.g[length + i];
            int i4 = this.g[i];
            o oVar = new o(this.f[i], i3, (i3 + i4) - i2);
            if (cVar.f1366a == null) {
                oVar.g = oVar;
                oVar.f = oVar;
                cVar.f1366a = oVar;
            } else {
                cVar.f1366a.g.a(oVar);
            }
            i++;
            i2 = i4;
        }
        cVar.f1367b = ((long) i2) + cVar.f1367b;
    }

    public boolean a(int i, f fVar, int i2, int i3) {
        if (i < 0 || i > e() - i3) {
            return false;
        }
        int b2 = b(i);
        while (i3 > 0) {
            int i4 = b2 == 0 ? 0 : this.g[b2 - 1];
            int min = Math.min(i3, ((this.g[b2] - i4) + i4) - i);
            if (!fVar.a(i2, this.f[b2], (i - i4) + this.g[this.f.length + b2], min)) {
                return false;
            }
            i += min;
            i2 += min;
            i3 -= min;
            b2++;
        }
        return true;
    }

    public boolean a(int i, byte[] bArr, int i2, int i3) {
        if (i < 0 || i > e() - i3 || i2 < 0 || i2 > bArr.length - i3) {
            return false;
        }
        int b2 = b(i);
        while (i3 > 0) {
            int i4 = b2 == 0 ? 0 : this.g[b2 - 1];
            int min = Math.min(i3, ((this.g[b2] - i4) + i4) - i);
            if (!u.a(this.f[b2], (i - i4) + this.g[this.f.length + b2], bArr, i2, min)) {
                return false;
            }
            i += min;
            i2 += min;
            i3 -= min;
            b2++;
        }
        return true;
    }

    public String b() {
        return g().b();
    }

    public String c() {
        return g().c();
    }

    public f d() {
        return g().d();
    }

    public int e() {
        return this.g[this.f.length - 1];
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        return (obj instanceof f) && ((f) obj).e() == e() && a(0, (f) obj, 0, e());
    }

    public byte[] f() {
        int i = 0;
        byte[] bArr = new byte[this.g[this.f.length - 1]];
        int length = this.f.length;
        int i2 = 0;
        while (i < length) {
            int i3 = this.g[length + i];
            int i4 = this.g[i];
            System.arraycopy(this.f[i], i3, bArr, i2, i4 - i2);
            i++;
            i2 = i4;
        }
        return bArr;
    }

    public int hashCode() {
        int i = this.d;
        if (i == 0) {
            i = 1;
            int length = this.f.length;
            int i2 = 0;
            int i3 = 0;
            while (i2 < length) {
                byte[] bArr = this.f[i2];
                int i4 = this.g[length + i2];
                int i5 = this.g[i2];
                int i6 = (i5 - i3) + i4;
                int i7 = i4;
                int i8 = i;
                for (int i9 = i7; i9 < i6; i9++) {
                    i8 = (i8 * 31) + bArr[i9];
                }
                i2++;
                i3 = i5;
                i = i8;
            }
            this.d = i;
        }
        return i;
    }

    public String toString() {
        return g().toString();
    }
}
