package io.a.a.a;

public interface f<T> {
    public static final f d = new a();

    public static class a implements f<Object> {
        private a() {
        }

        public void a(Exception exc) {
        }

        public void a(Object obj) {
        }
    }

    void a(Exception exc);

    void a(T t);
}
