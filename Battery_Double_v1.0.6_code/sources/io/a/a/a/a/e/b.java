package io.a.a.a.a.e;

import io.a.a.a.l;
import java.util.Locale;
import java.util.Map;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSocketFactory;

public class b implements e {

    /* renamed from: a reason: collision with root package name */
    private final l f4448a;

    /* renamed from: b reason: collision with root package name */
    private g f4449b;
    private SSLSocketFactory c;
    private boolean d;

    public b() {
        this(new io.a.a.a.b());
    }

    public b(l lVar) {
        this.f4448a = lVar;
    }

    private synchronized void a() {
        this.d = false;
        this.c = null;
    }

    private boolean a(String str) {
        return str != null && str.toLowerCase(Locale.US).startsWith("https");
    }

    private synchronized SSLSocketFactory b() {
        if (this.c == null && !this.d) {
            this.c = c();
        }
        return this.c;
    }

    private synchronized SSLSocketFactory c() {
        SSLSocketFactory sSLSocketFactory;
        this.d = true;
        try {
            sSLSocketFactory = f.a(this.f4449b);
            this.f4448a.a("Fabric", "Custom SSL pinning enabled");
        } catch (Exception e) {
            this.f4448a.e("Fabric", "Exception while validating pinned certs", e);
            sSLSocketFactory = null;
        }
        return sSLSocketFactory;
    }

    public d a(c cVar, String str, Map<String, String> map) {
        d e;
        switch (cVar) {
            case GET:
                e = d.a((CharSequence) str, map, true);
                break;
            case POST:
                e = d.b((CharSequence) str, map, true);
                break;
            case PUT:
                e = d.d((CharSequence) str);
                break;
            case DELETE:
                e = d.e((CharSequence) str);
                break;
            default:
                throw new IllegalArgumentException("Unsupported HTTP method!");
        }
        if (a(str) && this.f4449b != null) {
            SSLSocketFactory b2 = b();
            if (b2 != null) {
                ((HttpsURLConnection) e.a()).setSSLSocketFactory(b2);
            }
        }
        return e;
    }

    public void a(g gVar) {
        if (this.f4449b != gVar) {
            this.f4449b = gVar;
            a();
        }
    }
}
