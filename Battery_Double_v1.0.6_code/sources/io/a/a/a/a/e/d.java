package io.a.a.a.a.e;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.Flushable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.Proxy.Type;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.Callable;
import java.util.zip.GZIPInputStream;

public class d {

    /* renamed from: b reason: collision with root package name */
    private static final String[] f4453b = new String[0];
    private static b c = b.f4459a;

    /* renamed from: a reason: collision with root package name */
    public final URL f4454a;
    private HttpURLConnection d = null;
    private final String e;
    private e f;
    private boolean g;
    private boolean h = true;
    private boolean i = false;
    /* access modifiers changed from: private */
    public int j = 8192;
    private String k;
    private int l;

    protected static abstract class a<V> extends C0073d<V> {

        /* renamed from: a reason: collision with root package name */
        private final Closeable f4457a;

        /* renamed from: b reason: collision with root package name */
        private final boolean f4458b;

        protected a(Closeable closeable, boolean z) {
            this.f4457a = closeable;
            this.f4458b = z;
        }

        /* access modifiers changed from: protected */
        public void c() throws IOException {
            if (this.f4457a instanceof Flushable) {
                ((Flushable) this.f4457a).flush();
            }
            if (this.f4458b) {
                try {
                    this.f4457a.close();
                } catch (IOException e) {
                }
            } else {
                this.f4457a.close();
            }
        }
    }

    public interface b {

        /* renamed from: a reason: collision with root package name */
        public static final b f4459a = new b() {
            public HttpURLConnection a(URL url) throws IOException {
                return (HttpURLConnection) url.openConnection();
            }

            public HttpURLConnection a(URL url, Proxy proxy) throws IOException {
                return (HttpURLConnection) url.openConnection(proxy);
            }
        };

        HttpURLConnection a(URL url) throws IOException;

        HttpURLConnection a(URL url, Proxy proxy) throws IOException;
    }

    public static class c extends RuntimeException {
        protected c(IOException iOException) {
            super(iOException);
        }

        /* renamed from: a */
        public IOException getCause() {
            return (IOException) super.getCause();
        }
    }

    /* renamed from: io.a.a.a.a.e.d$d reason: collision with other inner class name */
    protected static abstract class C0073d<V> implements Callable<V> {
        protected C0073d() {
        }

        /* access modifiers changed from: protected */
        public abstract V b() throws c, IOException;

        /* access modifiers changed from: protected */
        public abstract void c() throws IOException;

        public V call() throws c {
            boolean z = true;
            try {
                V b2 = b();
                try {
                    c();
                    return b2;
                } catch (IOException e) {
                    throw new c(e);
                }
            } catch (c e2) {
                throw e2;
            } catch (IOException e3) {
                throw new c(e3);
            } catch (Throwable th) {
                th = th;
            }
            try {
                c();
            } catch (IOException e4) {
                if (!z) {
                    throw new c(e4);
                }
            }
            throw th;
        }
    }

    public static class e extends BufferedOutputStream {

        /* renamed from: a reason: collision with root package name */
        private final CharsetEncoder f4460a;

        public e(OutputStream outputStream, String str, int i) {
            super(outputStream, i);
            this.f4460a = Charset.forName(d.f(str)).newEncoder();
        }

        public e a(String str) throws IOException {
            ByteBuffer encode = this.f4460a.encode(CharBuffer.wrap(str));
            super.write(encode.array(), 0, encode.limit());
            return this;
        }
    }

    public d(CharSequence charSequence, String str) throws c {
        try {
            this.f4454a = new URL(charSequence.toString());
            this.e = str;
        } catch (MalformedURLException e2) {
            throw new c(e2);
        }
    }

    public static d a(CharSequence charSequence, Map<?, ?> map, boolean z) {
        String a2 = a(charSequence, map);
        if (z) {
            a2 = a((CharSequence) a2);
        }
        return b((CharSequence) a2);
    }

    public static String a(CharSequence charSequence) throws c {
        try {
            URL url = new URL(charSequence.toString());
            String host = url.getHost();
            int port = url.getPort();
            if (port != -1) {
                host = host + ':' + Integer.toString(port);
            }
            try {
                String aSCIIString = new URI(url.getProtocol(), host, url.getPath(), url.getQuery(), null).toASCIIString();
                int indexOf = aSCIIString.indexOf(63);
                return (indexOf <= 0 || indexOf + 1 >= aSCIIString.length()) ? aSCIIString : aSCIIString.substring(0, indexOf + 1) + aSCIIString.substring(indexOf + 1).replace("+", "%2B");
            } catch (URISyntaxException e2) {
                IOException iOException = new IOException("Parsing URI failed");
                iOException.initCause(e2);
                throw new c(iOException);
            }
        } catch (IOException e3) {
            throw new c(e3);
        }
    }

    public static String a(CharSequence charSequence, Map<?, ?> map) {
        String charSequence2 = charSequence.toString();
        if (map == null || map.isEmpty()) {
            return charSequence2;
        }
        StringBuilder sb = new StringBuilder(charSequence2);
        a(charSequence2, sb);
        b(charSequence2, sb);
        Iterator it = map.entrySet().iterator();
        Entry entry = (Entry) it.next();
        sb.append(entry.getKey().toString());
        sb.append('=');
        Object value = entry.getValue();
        if (value != null) {
            sb.append(value);
        }
        while (it.hasNext()) {
            sb.append('&');
            Entry entry2 = (Entry) it.next();
            sb.append(entry2.getKey().toString());
            sb.append('=');
            Object value2 = entry2.getValue();
            if (value2 != null) {
                sb.append(value2);
            }
        }
        return sb.toString();
    }

    private static StringBuilder a(String str, StringBuilder sb) {
        if (str.indexOf(58) + 2 == str.lastIndexOf(47)) {
            sb.append('/');
        }
        return sb;
    }

    public static d b(CharSequence charSequence) throws c {
        return new d(charSequence, "GET");
    }

    public static d b(CharSequence charSequence, Map<?, ?> map, boolean z) {
        String a2 = a(charSequence, map);
        if (z) {
            a2 = a((CharSequence) a2);
        }
        return c((CharSequence) a2);
    }

    private static StringBuilder b(String str, StringBuilder sb) {
        int indexOf = str.indexOf(63);
        int length = sb.length() - 1;
        if (indexOf == -1) {
            sb.append('?');
        } else if (indexOf < length && str.charAt(length) != '&') {
            sb.append('&');
        }
        return sb;
    }

    public static d c(CharSequence charSequence) throws c {
        return new d(charSequence, "POST");
    }

    public static d d(CharSequence charSequence) throws c {
        return new d(charSequence, "PUT");
    }

    public static d e(CharSequence charSequence) throws c {
        return new d(charSequence, "DELETE");
    }

    /* access modifiers changed from: private */
    public static String f(String str) {
        return (str == null || str.length() <= 0) ? "UTF-8" : str;
    }

    private Proxy q() {
        return new Proxy(Type.HTTP, new InetSocketAddress(this.k, this.l));
    }

    private HttpURLConnection r() {
        try {
            HttpURLConnection a2 = this.k != null ? c.a(this.f4454a, q()) : c.a(this.f4454a);
            a2.setRequestMethod(this.e);
            return a2;
        } catch (IOException e2) {
            throw new c(e2);
        }
    }

    public int a(String str, int i2) throws c {
        l();
        return a().getHeaderFieldInt(str, i2);
    }

    public d a(int i2) {
        a().setConnectTimeout(i2);
        return this;
    }

    /* access modifiers changed from: protected */
    public d a(InputStream inputStream, OutputStream outputStream) throws IOException {
        final InputStream inputStream2 = inputStream;
        final OutputStream outputStream2 = outputStream;
        return (d) new a<d>(inputStream, this.h) {
            /* renamed from: a */
            public d b() throws IOException {
                byte[] bArr = new byte[d.this.j];
                while (true) {
                    int read = inputStream2.read(bArr);
                    if (read == -1) {
                        return d.this;
                    }
                    outputStream2.write(bArr, 0, read);
                }
            }
        }.call();
    }

    public d a(String str, Number number) throws c {
        return a(str, (String) null, number);
    }

    public d a(String str, String str2) {
        a().setRequestProperty(str, str2);
        return this;
    }

    public d a(String str, String str2, Number number) throws c {
        return b(str, str2, number != null ? number.toString() : null);
    }

    /* access modifiers changed from: protected */
    public d a(String str, String str2, String str3) throws IOException {
        StringBuilder sb = new StringBuilder();
        sb.append("form-data; name=\"").append(str);
        if (str2 != null) {
            sb.append("\"; filename=\"").append(str2);
        }
        sb.append('\"');
        f("Content-Disposition", sb.toString());
        if (str3 != null) {
            f("Content-Type", str3);
        }
        return f((CharSequence) "\r\n");
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0020 A[SYNTHETIC, Splitter:B:16:0x0020] */
    public d a(String str, String str2, String str3, File file) throws c {
        InputStream inputStream;
        try {
            inputStream = new BufferedInputStream(new FileInputStream(file));
            try {
                d a2 = a(str, str2, str3, inputStream);
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException e2) {
                    }
                }
                return a2;
            } catch (IOException e3) {
                e = e3;
            }
        } catch (IOException e4) {
            e = e4;
            inputStream = null;
            try {
                throw new c(e);
            } catch (Throwable th) {
                th = th;
                if (inputStream != null) {
                }
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            inputStream = null;
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e5) {
                }
            }
            throw th;
        }
    }

    public d a(String str, String str2, String str3, InputStream inputStream) throws c {
        try {
            n();
            a(str, str2, str3);
            a(inputStream, (OutputStream) this.f);
            return this;
        } catch (IOException e2) {
            throw new c(e2);
        }
    }

    public d a(String str, String str2, String str3, String str4) throws c {
        try {
            n();
            a(str, str2, str3);
            this.f.a(str4);
            return this;
        } catch (IOException e2) {
            throw new c(e2);
        }
    }

    public d a(Entry<String, String> entry) {
        return a((String) entry.getKey(), (String) entry.getValue());
    }

    public d a(boolean z) {
        a().setUseCaches(z);
        return this;
    }

    public String a(String str) throws c {
        ByteArrayOutputStream d2 = d();
        try {
            a((InputStream) f(), (OutputStream) d2);
            return d2.toString(f(str));
        } catch (IOException e2) {
            throw new c(e2);
        }
    }

    public HttpURLConnection a() {
        if (this.d == null) {
            this.d = r();
        }
        return this.d;
    }

    public int b() throws c {
        try {
            k();
            return a().getResponseCode();
        } catch (IOException e2) {
            throw new c(e2);
        }
    }

    public d b(String str, String str2, String str3) throws c {
        return a(str, str2, (String) null, str3);
    }

    public String b(String str) throws c {
        l();
        return a().getHeaderField(str);
    }

    public String b(String str, String str2) {
        return c(b(str), str2);
    }

    public int c(String str) throws c {
        return a(str, -1);
    }

    /* access modifiers changed from: protected */
    public String c(String str, String str2) {
        int i2;
        int i3;
        if (str == null || str.length() == 0) {
            return null;
        }
        int length = str.length();
        int indexOf = str.indexOf(59) + 1;
        if (indexOf == 0 || indexOf == length) {
            return null;
        }
        int indexOf2 = str.indexOf(59, indexOf);
        if (indexOf2 == -1) {
            i3 = indexOf;
            i2 = length;
        } else {
            int i4 = indexOf2;
            i3 = indexOf;
            i2 = i4;
        }
        while (i3 < i2) {
            int indexOf3 = str.indexOf(61, i3);
            if (indexOf3 != -1 && indexOf3 < i2 && str2.equals(str.substring(i3, indexOf3).trim())) {
                String trim = str.substring(indexOf3 + 1, i2).trim();
                int length2 = trim.length();
                if (length2 != 0) {
                    return (length2 > 2 && '\"' == trim.charAt(0) && '\"' == trim.charAt(length2 + -1)) ? trim.substring(1, length2 - 1) : trim;
                }
            }
            int i5 = i2 + 1;
            int indexOf4 = str.indexOf(59, i5);
            if (indexOf4 == -1) {
                indexOf4 = length;
            }
            int i6 = indexOf4;
            i3 = i5;
            i2 = i6;
        }
        return null;
    }

    public boolean c() throws c {
        return 200 == b();
    }

    public d d(String str) {
        return d(str, null);
    }

    public d d(String str, String str2) {
        if (str2 == null || str2.length() <= 0) {
            return a("Content-Type", str);
        }
        String str3 = "; charset=";
        return a("Content-Type", str + "; charset=" + str2);
    }

    /* access modifiers changed from: protected */
    public ByteArrayOutputStream d() {
        int j2 = j();
        return j2 > 0 ? new ByteArrayOutputStream(j2) : new ByteArrayOutputStream();
    }

    public d e(String str, String str2) {
        return b(str, (String) null, str2);
    }

    public String e() throws c {
        return a(h());
    }

    public d f(CharSequence charSequence) throws c {
        try {
            m();
            this.f.a(charSequence.toString());
            return this;
        } catch (IOException e2) {
            throw new c(e2);
        }
    }

    public d f(String str, String str2) throws c {
        return f((CharSequence) str).f((CharSequence) ": ").f((CharSequence) str2).f((CharSequence) "\r\n");
    }

    public BufferedInputStream f() throws c {
        return new BufferedInputStream(g(), this.j);
    }

    public InputStream g() throws c {
        InputStream errorStream;
        if (b() < 400) {
            try {
                errorStream = a().getInputStream();
            } catch (IOException e2) {
                throw new c(e2);
            }
        } else {
            errorStream = a().getErrorStream();
            if (errorStream == null) {
                try {
                    errorStream = a().getInputStream();
                } catch (IOException e3) {
                    throw new c(e3);
                }
            }
        }
        if (!this.i || !"gzip".equals(i())) {
            return errorStream;
        }
        try {
            return new GZIPInputStream(errorStream);
        } catch (IOException e4) {
            throw new c(e4);
        }
    }

    public String h() {
        return b("Content-Type", "charset");
    }

    public String i() {
        return b("Content-Encoding");
    }

    public int j() {
        return c("Content-Length");
    }

    /* access modifiers changed from: protected */
    public d k() throws IOException {
        if (this.f != null) {
            if (this.g) {
                this.f.a("\r\n--00content0boundary00--\r\n");
            }
            if (this.h) {
                try {
                    this.f.close();
                } catch (IOException e2) {
                }
            } else {
                this.f.close();
            }
            this.f = null;
        }
        return this;
    }

    /* access modifiers changed from: protected */
    public d l() throws c {
        try {
            return k();
        } catch (IOException e2) {
            throw new c(e2);
        }
    }

    /* access modifiers changed from: protected */
    public d m() throws IOException {
        if (this.f == null) {
            a().setDoOutput(true);
            this.f = new e(a().getOutputStream(), c(a().getRequestProperty("Content-Type"), "charset"), this.j);
        }
        return this;
    }

    /* access modifiers changed from: protected */
    public d n() throws IOException {
        if (!this.g) {
            this.g = true;
            d("multipart/form-data; boundary=00content0boundary00").m();
            this.f.a("--00content0boundary00\r\n");
        } else {
            this.f.a("\r\n--00content0boundary00\r\n");
        }
        return this;
    }

    public URL o() {
        return a().getURL();
    }

    public String p() {
        return a().getRequestMethod();
    }

    public String toString() {
        return p() + ' ' + o();
    }
}
