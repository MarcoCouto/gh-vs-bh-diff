package io.a.a.a.a.g;

public class t {

    /* renamed from: a reason: collision with root package name */
    public final e f4495a;

    /* renamed from: b reason: collision with root package name */
    public final p f4496b;
    public final o c;
    public final m d;
    public final b e;
    public final f f;
    public final long g;
    public final int h;
    public final int i;

    public t(long j, e eVar, p pVar, o oVar, m mVar, b bVar, f fVar, int i2, int i3) {
        this.g = j;
        this.f4495a = eVar;
        this.f4496b = pVar;
        this.c = oVar;
        this.d = mVar;
        this.h = i2;
        this.i = i3;
        this.e = bVar;
        this.f = fVar;
    }

    public boolean a(long j) {
        return this.g < j;
    }
}
