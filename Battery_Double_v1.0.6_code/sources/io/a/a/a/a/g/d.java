package io.a.a.a.a.g;

import io.a.a.a.k;
import java.util.Collection;

public class d {

    /* renamed from: a reason: collision with root package name */
    public final String f4473a;

    /* renamed from: b reason: collision with root package name */
    public final String f4474b;
    public final String c;
    public final String d;
    public final String e;
    public final String f;
    public final int g;
    public final String h;
    public final String i;
    public final n j;
    public final Collection<k> k;

    public d(String str, String str2, String str3, String str4, String str5, String str6, int i2, String str7, String str8, n nVar, Collection<k> collection) {
        this.f4473a = str;
        this.f4474b = str2;
        this.c = str3;
        this.d = str4;
        this.e = str5;
        this.f = str6;
        this.g = i2;
        this.h = str7;
        this.i = str8;
        this.j = nVar;
        this.k = collection;
    }
}
