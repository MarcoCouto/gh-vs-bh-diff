package io.a.a.a.a.g;

import android.content.Context;
import io.a.a.a.a.b.g;
import io.a.a.a.a.b.l;
import io.a.a.a.a.b.p;
import io.a.a.a.a.b.t;
import io.a.a.a.a.e.e;
import io.a.a.a.c;
import io.a.a.a.i;
import java.util.Locale;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicReference;

public class q {

    /* renamed from: a reason: collision with root package name */
    private final AtomicReference<t> f4490a;

    /* renamed from: b reason: collision with root package name */
    private final CountDownLatch f4491b;
    private s c;
    private boolean d;

    static class a {
        /* access modifiers changed from: private */

        /* renamed from: a reason: collision with root package name */
        public static final q f4492a = new q();
    }

    private q() {
        this.f4490a = new AtomicReference<>();
        this.f4491b = new CountDownLatch(1);
        this.d = false;
    }

    public static q a() {
        return a.f4492a;
    }

    private void a(t tVar) {
        this.f4490a.set(tVar);
        this.f4491b.countDown();
    }

    public synchronized q a(i iVar, p pVar, e eVar, String str, String str2, String str3) {
        q qVar;
        if (this.d) {
            qVar = this;
        } else {
            if (this.c == null) {
                Context q = iVar.q();
                String c2 = pVar.c();
                String a2 = new g().a(q);
                String i = pVar.i();
                t tVar = new t();
                k kVar = new k();
                i iVar2 = new i(iVar);
                String k = io.a.a.a.a.b.i.k(q);
                i iVar3 = iVar;
                String str4 = str3;
                l lVar = new l(iVar3, str4, String.format(Locale.US, "https://settings.crashlytics.com/spi/v2/platforms/android/apps/%s/settings", new Object[]{c2}), eVar);
                String str5 = str2;
                String str6 = str;
                this.c = new j(iVar, new w(a2, pVar.g(), pVar.f(), pVar.e(), pVar.k(), pVar.b(), pVar.l(), io.a.a.a.a.b.i.a(io.a.a.a.a.b.i.m(q)), str5, str6, l.a(i).a(), k), tVar, kVar, iVar2, lVar);
            }
            this.d = true;
            qVar = this;
        }
        return qVar;
    }

    public t b() {
        try {
            this.f4491b.await();
            return (t) this.f4490a.get();
        } catch (InterruptedException e) {
            c.h().e("Fabric", "Interrupted while waiting for settings data.");
            return null;
        }
    }

    public synchronized boolean c() {
        t a2;
        a2 = this.c.a();
        a(a2);
        return a2 != null;
    }

    public synchronized boolean d() {
        t a2;
        a2 = this.c.a(r.SKIP_CACHE_LOOKUP);
        a(a2);
        if (a2 == null) {
            c.h().e("Fabric", "Failed to force reload of settings from Crashlytics.", null);
        }
        return a2 != null;
    }
}
