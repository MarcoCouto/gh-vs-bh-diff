package io.a.a.a.a.g;

import android.text.TextUtils;
import io.a.a.a.a.b.a;
import io.a.a.a.a.e.c;
import io.a.a.a.a.e.d;
import io.a.a.a.a.e.e;
import io.a.a.a.i;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;

class l extends a implements x {
    public l(i iVar, String str, String str2, e eVar) {
        this(iVar, str, str2, eVar, c.GET);
    }

    l(i iVar, String str, String str2, e eVar, c cVar) {
        super(iVar, str, str2, eVar, cVar);
    }

    private d a(d dVar, w wVar) {
        a(dVar, "X-CRASHLYTICS-API-KEY", wVar.f4498a);
        a(dVar, "X-CRASHLYTICS-API-CLIENT-TYPE", "android");
        a(dVar, "X-CRASHLYTICS-API-CLIENT-VERSION", this.f4362a.a());
        a(dVar, "Accept", "application/json");
        a(dVar, "X-CRASHLYTICS-DEVICE-MODEL", wVar.f4499b);
        a(dVar, "X-CRASHLYTICS-OS-BUILD-VERSION", wVar.c);
        a(dVar, "X-CRASHLYTICS-OS-DISPLAY-VERSION", wVar.d);
        a(dVar, "X-CRASHLYTICS-INSTALLATION-ID", wVar.f);
        if (TextUtils.isEmpty(wVar.e)) {
            a(dVar, "X-CRASHLYTICS-ANDROID-ID", wVar.g);
        } else {
            a(dVar, "X-CRASHLYTICS-ADVERTISING-TOKEN", wVar.e);
        }
        return dVar;
    }

    private JSONObject a(String str) {
        try {
            return new JSONObject(str);
        } catch (Exception e) {
            io.a.a.a.c.h().a("Fabric", "Failed to parse settings JSON from " + a(), (Throwable) e);
            io.a.a.a.c.h().a("Fabric", "Settings response " + str);
            return null;
        }
    }

    private void a(d dVar, String str, String str2) {
        if (str2 != null) {
            dVar.a(str, str2);
        }
    }

    private Map<String, String> b(w wVar) {
        HashMap hashMap = new HashMap();
        hashMap.put("build_version", wVar.j);
        hashMap.put("display_version", wVar.i);
        hashMap.put("source", Integer.toString(wVar.k));
        if (wVar.l != null) {
            hashMap.put("icon_hash", wVar.l);
        }
        String str = wVar.h;
        if (!io.a.a.a.a.b.i.d(str)) {
            hashMap.put("instance", str);
        }
        return hashMap;
    }

    /* access modifiers changed from: 0000 */
    public JSONObject a(d dVar) {
        int b2 = dVar.b();
        io.a.a.a.c.h().a("Fabric", "Settings result was: " + b2);
        if (a(b2)) {
            return a(dVar.e());
        }
        io.a.a.a.c.h().e("Fabric", "Failed to retrieve settings from " + a());
        return null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x00a9  */
    public JSONObject a(w wVar) {
        d dVar;
        Throwable th;
        d dVar2;
        d a2;
        JSONObject jSONObject = 0;
        try {
            Map b2 = b(wVar);
            try {
                a2 = a(a(b2), wVar);
                io.a.a.a.c.h().a("Fabric", "Requesting settings from " + a());
                io.a.a.a.c.h().a("Fabric", "Settings query params were: " + b2);
                jSONObject = a(a2);
                if (a2 != 0) {
                    io.a.a.a.c.h().a("Fabric", "Settings request ID: " + a2.b("X-REQUEST-ID"));
                }
            } catch (d.c e) {
                e = e;
                dVar2 = a2;
            }
        } catch (d.c e2) {
            e = e2;
            dVar2 = jSONObject;
        } catch (Throwable th2) {
            dVar = jSONObject;
            th = th2;
            if (dVar != 0) {
                io.a.a.a.c.h().a("Fabric", "Settings request ID: " + dVar.b("X-REQUEST-ID"));
            }
            throw th;
        }
        return jSONObject;
        try {
            io.a.a.a.c.h().e("Fabric", "Settings request failed.", e);
            if (dVar2 != 0) {
                io.a.a.a.c.h().a("Fabric", "Settings request ID: " + dVar2.b("X-REQUEST-ID"));
            }
            return jSONObject;
        } catch (Throwable th3) {
            th = th3;
            dVar = dVar2;
            if (dVar != 0) {
            }
            throw th;
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean a(int i) {
        return i == 200 || i == 201 || i == 202 || i == 203;
    }
}
