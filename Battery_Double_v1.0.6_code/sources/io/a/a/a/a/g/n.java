package io.a.a.a.a.g;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import io.a.a.a.a.b.i;
import io.a.a.a.c;

public class n {

    /* renamed from: a reason: collision with root package name */
    public final String f4484a;

    /* renamed from: b reason: collision with root package name */
    public final int f4485b;
    public final int c;
    public final int d;

    public n(String str, int i, int i2, int i3) {
        this.f4484a = str;
        this.f4485b = i;
        this.c = i2;
        this.d = i3;
    }

    public static n a(Context context, String str) {
        if (str != null) {
            try {
                int l = i.l(context);
                c.h().a("Fabric", "App icon resource ID is " + l);
                Options options = new Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeResource(context.getResources(), l, options);
                return new n(str, l, options.outWidth, options.outHeight);
            } catch (Exception e) {
                c.h().e("Fabric", "Failed to load icon", e);
            }
        }
        return null;
    }
}
