package io.a.a.a.a.g;

import android.content.res.Resources.NotFoundException;
import io.a.a.a.a.b.s;
import io.a.a.a.a.e.c;
import io.a.a.a.a.e.d;
import io.a.a.a.a.e.e;
import io.a.a.a.i;
import io.a.a.a.k;
import java.io.Closeable;
import java.io.InputStream;
import java.util.Locale;

abstract class a extends io.a.a.a.a.b.a {
    public a(i iVar, String str, String str2, e eVar, c cVar) {
        super(iVar, str, str2, eVar, cVar);
    }

    private d a(d dVar, d dVar2) {
        return dVar.a("X-CRASHLYTICS-API-KEY", dVar2.f4473a).a("X-CRASHLYTICS-API-CLIENT-TYPE", "android").a("X-CRASHLYTICS-API-CLIENT-VERSION", this.f4362a.a());
    }

    private d b(d dVar, d dVar2) {
        d e = dVar.e("app[identifier]", dVar2.f4474b).e("app[name]", dVar2.f).e("app[display_version]", dVar2.c).e("app[build_version]", dVar2.d).a("app[source]", (Number) Integer.valueOf(dVar2.g)).e("app[minimum_sdk_version]", dVar2.h).e("app[built_sdk_version]", dVar2.i);
        if (!io.a.a.a.a.b.i.d(dVar2.e)) {
            e.e("app[instance_identifier]", dVar2.e);
        }
        if (dVar2.j != null) {
            InputStream inputStream = null;
            try {
                inputStream = this.f4362a.q().getResources().openRawResource(dVar2.j.f4485b);
                e.e("app[icon][hash]", dVar2.j.f4484a).a("app[icon][data]", "icon.png", "application/octet-stream", inputStream).a("app[icon][width]", (Number) Integer.valueOf(dVar2.j.c)).a("app[icon][height]", (Number) Integer.valueOf(dVar2.j.d));
            } catch (NotFoundException e2) {
                io.a.a.a.c.h().e("Fabric", "Failed to find app icon with resource ID: " + dVar2.j.f4485b, e2);
            } finally {
                io.a.a.a.a.b.i.a((Closeable) inputStream, "Failed to close app icon InputStream.");
            }
        }
        if (dVar2.k != null) {
            for (k kVar : dVar2.k) {
                e.e(a(kVar), kVar.b());
                e.e(b(kVar), kVar.c());
            }
        }
        return e;
    }

    /* access modifiers changed from: 0000 */
    public String a(k kVar) {
        return String.format(Locale.US, "app[build][libraries][%s][version]", new Object[]{kVar.a()});
    }

    public boolean a(d dVar) {
        d b2 = b(a(b(), dVar), dVar);
        io.a.a.a.c.h().a("Fabric", "Sending app info to " + a());
        if (dVar.j != null) {
            io.a.a.a.c.h().a("Fabric", "App icon hash is " + dVar.j.f4484a);
            io.a.a.a.c.h().a("Fabric", "App icon size is " + dVar.j.c + "x" + dVar.j.d);
        }
        int b3 = b2.b();
        io.a.a.a.c.h().a("Fabric", ("POST".equals(b2.p()) ? "Create" : "Update") + " app request ID: " + b2.b("X-REQUEST-ID"));
        io.a.a.a.c.h().a("Fabric", "Result was " + b3);
        return s.a(b3) == 0;
    }

    /* access modifiers changed from: 0000 */
    public String b(k kVar) {
        return String.format(Locale.US, "app[build][libraries][%s][type]", new Object[]{kVar.a()});
    }
}
