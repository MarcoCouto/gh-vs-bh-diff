package io.a.a.a.a.g;

import android.annotation.SuppressLint;
import android.content.SharedPreferences.Editor;
import io.a.a.a.a.b.k;
import io.a.a.a.a.f.c;
import io.a.a.a.a.f.d;
import io.a.a.a.i;
import org.json.JSONException;
import org.json.JSONObject;

class j implements s {

    /* renamed from: a reason: collision with root package name */
    private final w f4480a;

    /* renamed from: b reason: collision with root package name */
    private final v f4481b;
    private final k c;
    private final g d;
    private final x e;
    private final i f;
    private final c g = new d(this.f);

    public j(i iVar, w wVar, k kVar, v vVar, g gVar, x xVar) {
        this.f = iVar;
        this.f4480a = wVar;
        this.c = kVar;
        this.f4481b = vVar;
        this.d = gVar;
        this.e = xVar;
    }

    private void a(JSONObject jSONObject, String str) throws JSONException {
        io.a.a.a.c.h().a("Fabric", str + jSONObject.toString());
    }

    private t b(r rVar) {
        t tVar = null;
        try {
            if (r.SKIP_CACHE_LOOKUP.equals(rVar)) {
                return null;
            }
            JSONObject a2 = this.d.a();
            if (a2 != null) {
                t a3 = this.f4481b.a(this.c, a2);
                if (a3 != null) {
                    a(a2, "Loaded cached settings: ");
                    long a4 = this.c.a();
                    if (r.IGNORE_CACHE_EXPIRATION.equals(rVar) || !a3.a(a4)) {
                        try {
                            io.a.a.a.c.h().a("Fabric", "Returning cached settings.");
                            return a3;
                        } catch (Exception e2) {
                            Throwable th = e2;
                            tVar = a3;
                            e = th;
                            io.a.a.a.c.h().e("Fabric", "Failed to get cached settings", e);
                            return tVar;
                        }
                    } else {
                        io.a.a.a.c.h().a("Fabric", "Cached settings have expired.");
                        return null;
                    }
                } else {
                    io.a.a.a.c.h().e("Fabric", "Failed to transform cached settings data.", null);
                    return null;
                }
            } else {
                io.a.a.a.c.h().a("Fabric", "No cached settings data found.");
                return null;
            }
        } catch (Exception e3) {
            e = e3;
        }
    }

    public t a() {
        return a(r.USE_CACHE);
    }

    public t a(r rVar) {
        Throwable e2;
        t tVar;
        t tVar2 = null;
        try {
            if (!io.a.a.a.c.i() && !d()) {
                tVar2 = b(rVar);
            }
            if (tVar2 == null) {
                try {
                    JSONObject a2 = this.e.a(this.f4480a);
                    if (a2 != null) {
                        tVar2 = this.f4481b.a(this.c, a2);
                        this.d.a(tVar2.g, a2);
                        a(a2, "Loaded settings: ");
                        a(b());
                    }
                } catch (Exception e3) {
                    Throwable th = e3;
                    tVar = tVar2;
                    e2 = th;
                    io.a.a.a.c.h().e("Fabric", "Unknown error while loading Crashlytics settings. Crashes will be cached until settings can be retrieved.", e2);
                    return tVar;
                }
            }
            tVar = tVar2;
            if (tVar != null) {
                return tVar;
            }
            try {
                return b(r.IGNORE_CACHE_EXPIRATION);
            } catch (Exception e4) {
                e2 = e4;
            }
        } catch (Exception e5) {
            Throwable th2 = e5;
            tVar = null;
            e2 = th2;
            io.a.a.a.c.h().e("Fabric", "Unknown error while loading Crashlytics settings. Crashes will be cached until settings can be retrieved.", e2);
            return tVar;
        }
    }

    /* access modifiers changed from: 0000 */
    @SuppressLint({"CommitPrefEdits"})
    public boolean a(String str) {
        Editor b2 = this.g.b();
        b2.putString("existing_instance_identifier", str);
        return this.g.a(b2);
    }

    /* access modifiers changed from: 0000 */
    public String b() {
        return io.a.a.a.a.b.i.a(io.a.a.a.a.b.i.m(this.f.q()));
    }

    /* access modifiers changed from: 0000 */
    public String c() {
        return this.g.a().getString("existing_instance_identifier", "");
    }

    /* access modifiers changed from: 0000 */
    public boolean d() {
        return !c().equals(b());
    }
}
