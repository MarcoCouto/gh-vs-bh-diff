package io.a.a.a.a.a;

import android.content.Context;

public class b<T> extends a<T> {

    /* renamed from: a reason: collision with root package name */
    private T f4360a;

    public b() {
        this(null);
    }

    public b(c<T> cVar) {
        super(cVar);
    }

    /* access modifiers changed from: protected */
    public T a(Context context) {
        return this.f4360a;
    }

    /* access modifiers changed from: protected */
    public void a(Context context, T t) {
        this.f4360a = t;
    }
}
