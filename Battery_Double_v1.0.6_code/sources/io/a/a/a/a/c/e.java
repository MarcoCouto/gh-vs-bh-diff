package io.a.a.a.a.c;

public enum e {
    LOW,
    NORMAL,
    HIGH,
    IMMEDIATE;

    static <Y> int a(i iVar, Y y) {
        return (y instanceof i ? ((i) y).b() : NORMAL).ordinal() - iVar.b().ordinal();
    }
}
