package io.a.a.a.a.c;

import io.a.a.a.a.c.a.d;
import java.util.Collection;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;

public abstract class f<Params, Progress, Result> extends a<Params, Progress, Result> implements b<l>, i, l {

    /* renamed from: a reason: collision with root package name */
    private final j f4429a = new j();

    private static class a<Result> implements Executor {

        /* renamed from: a reason: collision with root package name */
        private final Executor f4430a;
        /* access modifiers changed from: private */

        /* renamed from: b reason: collision with root package name */
        public final f f4431b;

        public a(Executor executor, f fVar) {
            this.f4430a = executor;
            this.f4431b = fVar;
        }

        public void execute(Runnable runnable) {
            this.f4430a.execute(new h<Result>(runnable, null) {
                public <T extends b<l> & i & l> T a() {
                    return a.this.f4431b;
                }
            });
        }
    }

    /* renamed from: a */
    public void c(l lVar) {
        if (z_() != d.PENDING) {
            throw new IllegalStateException("Must not add Dependency after task is running");
        }
        ((b) ((i) g())).c(lVar);
    }

    public void a(Throwable th) {
        ((l) ((i) g())).a(th);
    }

    public final void a(ExecutorService executorService, Params... paramsArr) {
        super.a((Executor) new a(executorService, this), paramsArr);
    }

    public e b() {
        return ((i) g()).b();
    }

    public void b(boolean z) {
        ((l) ((i) g())).b(z);
    }

    public Collection<l> c() {
        return ((b) ((i) g())).c();
    }

    public int compareTo(Object obj) {
        return e.a(this, obj);
    }

    public boolean d() {
        return ((b) ((i) g())).d();
    }

    public boolean f() {
        return ((l) ((i) g())).f();
    }

    public <T extends b<l> & i & l> T g() {
        return this.f4429a;
    }
}
