package io.a.a.a.a.c;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.util.Log;
import java.util.LinkedList;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.FutureTask;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class a<Params, Progress, Result> {

    /* renamed from: a reason: collision with root package name */
    private static final int f4405a = Runtime.getRuntime().availableProcessors();

    /* renamed from: b reason: collision with root package name */
    public static final Executor f4406b = new ThreadPoolExecutor(d, e, 1, TimeUnit.SECONDS, g, f);
    public static final Executor c = new c();
    private static final int d = (f4405a + 1);
    private static final int e = ((f4405a * 2) + 1);
    private static final ThreadFactory f = new ThreadFactory() {

        /* renamed from: a reason: collision with root package name */
        private final AtomicInteger f4407a = new AtomicInteger(1);

        public Thread newThread(Runnable runnable) {
            return new Thread(runnable, "AsyncTask #" + this.f4407a.getAndIncrement());
        }
    };
    private static final BlockingQueue<Runnable> g = new LinkedBlockingQueue(128);
    private static final b h = new b();
    private static volatile Executor i = c;
    private final e<Params, Result> j = new e<Params, Result>() {
        public Result call() throws Exception {
            a.this.n.set(true);
            Process.setThreadPriority(10);
            return a.this.e(a.this.a((Params[]) this.f4422b));
        }
    };
    private final FutureTask<Result> k = new FutureTask<Result>(this.j) {
        /* access modifiers changed from: protected */
        public void done() {
            try {
                a.this.d(get());
            } catch (InterruptedException e) {
                Log.w("AsyncTask", e);
            } catch (ExecutionException e2) {
                throw new RuntimeException("An error occured while executing doInBackground()", e2.getCause());
            } catch (CancellationException e3) {
                a.this.d(null);
            }
        }
    };
    private volatile d l = d.PENDING;
    private final AtomicBoolean m = new AtomicBoolean();
    /* access modifiers changed from: private */
    public final AtomicBoolean n = new AtomicBoolean();

    /* renamed from: io.a.a.a.a.c.a$a reason: collision with other inner class name */
    private static class C0072a<Data> {

        /* renamed from: a reason: collision with root package name */
        final a f4411a;

        /* renamed from: b reason: collision with root package name */
        final Data[] f4412b;

        C0072a(a aVar, Data... dataArr) {
            this.f4411a = aVar;
            this.f4412b = dataArr;
        }
    }

    private static class b extends Handler {
        public b() {
            super(Looper.getMainLooper());
        }

        public void handleMessage(Message message) {
            C0072a aVar = (C0072a) message.obj;
            switch (message.what) {
                case 1:
                    aVar.f4411a.f(aVar.f4412b[0]);
                    return;
                case 2:
                    aVar.f4411a.b((Progress[]) aVar.f4412b);
                    return;
                default:
                    return;
            }
        }
    }

    private static class c implements Executor {

        /* renamed from: a reason: collision with root package name */
        final LinkedList<Runnable> f4414a;

        /* renamed from: b reason: collision with root package name */
        Runnable f4415b;

        private c() {
            this.f4414a = new LinkedList<>();
        }

        /* access modifiers changed from: protected */
        public synchronized void a() {
            Runnable runnable = (Runnable) this.f4414a.poll();
            this.f4415b = runnable;
            if (runnable != null) {
                a.f4406b.execute(this.f4415b);
            }
        }

        public synchronized void execute(final Runnable runnable) {
            this.f4414a.offer(new Runnable() {
                public void run() {
                    try {
                        runnable.run();
                    } finally {
                        c.this.a();
                    }
                }
            });
            if (this.f4415b == null) {
                a();
            }
        }
    }

    public enum d {
        PENDING,
        RUNNING,
        FINISHED
    }

    private static abstract class e<Params, Result> implements Callable<Result> {

        /* renamed from: b reason: collision with root package name */
        Params[] f4422b;

        private e() {
        }
    }

    /* access modifiers changed from: private */
    public void d(Result result) {
        if (!this.n.get()) {
            e(result);
        }
    }

    /* access modifiers changed from: private */
    public Result e(Result result) {
        h.obtainMessage(1, new C0072a(this, result)).sendToTarget();
        return result;
    }

    /* access modifiers changed from: private */
    public void f(Result result) {
        if (e()) {
            b(result);
        } else {
            a(result);
        }
        this.l = d.FINISHED;
    }

    /* access modifiers changed from: protected */
    public void A_() {
    }

    public final a<Params, Progress, Result> a(Executor executor, Params... paramsArr) {
        if (this.l != d.PENDING) {
            switch (this.l) {
                case RUNNING:
                    throw new IllegalStateException("Cannot execute task: the task is already running.");
                case FINISHED:
                    throw new IllegalStateException("Cannot execute task: the task has already been executed (a task can be executed only once)");
            }
        }
        this.l = d.RUNNING;
        a();
        this.j.f4422b = paramsArr;
        executor.execute(this.k);
        return this;
    }

    /* access modifiers changed from: protected */
    public abstract Result a(Params... paramsArr);

    /* access modifiers changed from: protected */
    public void a() {
    }

    /* access modifiers changed from: protected */
    public void a(Result result) {
    }

    public final boolean a(boolean z) {
        this.m.set(true);
        return this.k.cancel(z);
    }

    /* access modifiers changed from: protected */
    public void b(Result result) {
        A_();
    }

    /* access modifiers changed from: protected */
    public void b(Progress... progressArr) {
    }

    public final boolean e() {
        return this.m.get();
    }

    public final d z_() {
        return this.l;
    }
}
