package io.a.a.a.a.c;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

public class j implements b<l>, i, l {

    /* renamed from: a reason: collision with root package name */
    private final List<l> f4434a = new ArrayList();

    /* renamed from: b reason: collision with root package name */
    private final AtomicBoolean f4435b = new AtomicBoolean(false);
    private final AtomicReference<Throwable> c = new AtomicReference<>(null);

    public static boolean a(Object obj) {
        try {
            return (((b) obj) == null || ((l) obj) == null || ((i) obj) == null) ? false : true;
        } catch (ClassCastException e) {
            return false;
        }
    }

    /* renamed from: a */
    public synchronized void c(l lVar) {
        this.f4434a.add(lVar);
    }

    public void a(Throwable th) {
        this.c.set(th);
    }

    public e b() {
        return e.NORMAL;
    }

    public synchronized void b(boolean z) {
        this.f4435b.set(z);
    }

    public synchronized Collection<l> c() {
        return Collections.unmodifiableCollection(this.f4434a);
    }

    public int compareTo(Object obj) {
        return e.a(this, obj);
    }

    public boolean d() {
        for (l f : c()) {
            if (!f.f()) {
                return false;
            }
        }
        return true;
    }

    public boolean f() {
        return this.f4435b.get();
    }
}
