package io.a.a.a.a.c;

import java.util.Collection;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

public class h<V> extends FutureTask<V> implements b<l>, i, l {

    /* renamed from: b reason: collision with root package name */
    final Object f4433b;

    public h(Runnable runnable, V v) {
        super(runnable, v);
        this.f4433b = a((Object) runnable);
    }

    public h(Callable<V> callable) {
        super(callable);
        this.f4433b = a((Object) callable);
    }

    public <T extends b<l> & i & l> T a() {
        return (b) this.f4433b;
    }

    /* access modifiers changed from: protected */
    public <T extends b<l> & i & l> T a(Object obj) {
        return j.a(obj) ? (b) obj : new j();
    }

    /* renamed from: a */
    public void c(l lVar) {
        ((b) ((i) a())).c(lVar);
    }

    public void a(Throwable th) {
        ((l) ((i) a())).a(th);
    }

    public e b() {
        return ((i) a()).b();
    }

    public void b(boolean z) {
        ((l) ((i) a())).b(z);
    }

    public Collection<l> c() {
        return ((b) ((i) a())).c();
    }

    public int compareTo(Object obj) {
        return ((i) a()).compareTo(obj);
    }

    public boolean d() {
        return ((b) ((i) a())).d();
    }

    public boolean f() {
        return ((l) ((i) a())).f();
    }
}
