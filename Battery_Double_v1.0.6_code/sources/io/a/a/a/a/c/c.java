package io.a.a.a.a.c;

import io.a.a.a.a.c.b;
import io.a.a.a.a.c.i;
import io.a.a.a.a.c.l;
import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

public class c<E extends b & l & i> extends PriorityBlockingQueue<E> {

    /* renamed from: a reason: collision with root package name */
    final Queue<E> f4425a = new LinkedList();

    /* renamed from: b reason: collision with root package name */
    private final ReentrantLock f4426b = new ReentrantLock();

    /* renamed from: a */
    public E take() throws InterruptedException {
        return b(0, null, null);
    }

    /* access modifiers changed from: 0000 */
    public E a(int i, Long l, TimeUnit timeUnit) throws InterruptedException {
        switch (i) {
            case 0:
                return (b) super.take();
            case 1:
                return (b) super.peek();
            case 2:
                return (b) super.poll();
            case 3:
                return (b) super.poll(l.longValue(), timeUnit);
            default:
                return null;
        }
    }

    /* renamed from: a */
    public E poll(long j, TimeUnit timeUnit) throws InterruptedException {
        return b(3, Long.valueOf(j), timeUnit);
    }

    /* access modifiers changed from: 0000 */
    public boolean a(int i, E e) {
        try {
            this.f4426b.lock();
            if (i == 1) {
                super.remove(e);
            }
            return this.f4425a.offer(e);
        } finally {
            this.f4426b.unlock();
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean a(E e) {
        return e.d();
    }

    /* access modifiers changed from: 0000 */
    public <T> T[] a(T[] tArr, T[] tArr2) {
        int length = tArr.length;
        int length2 = tArr2.length;
        T[] tArr3 = (Object[]) Array.newInstance(tArr.getClass().getComponentType(), length + length2);
        System.arraycopy(tArr, 0, tArr3, 0, length);
        System.arraycopy(tArr2, 0, tArr3, length, length2);
        return tArr3;
    }

    /* renamed from: b */
    public E peek() {
        boolean z = false;
        try {
            return b(1, null, null);
        } catch (InterruptedException e) {
            return z;
        }
    }

    /* access modifiers changed from: 0000 */
    public E b(int i, Long l, TimeUnit timeUnit) throws InterruptedException {
        E a2;
        while (true) {
            a2 = a(i, l, timeUnit);
            if (a2 == null || a(a2)) {
                return a2;
            }
            a(i, a2);
        }
        return a2;
    }

    /* renamed from: c */
    public E poll() {
        boolean z = false;
        try {
            return b(2, null, null);
        } catch (InterruptedException e) {
            return z;
        }
    }

    public void clear() {
        try {
            this.f4426b.lock();
            this.f4425a.clear();
            super.clear();
        } finally {
            this.f4426b.unlock();
        }
    }

    public boolean contains(Object obj) {
        try {
            this.f4426b.lock();
            return super.contains(obj) || this.f4425a.contains(obj);
        } finally {
            this.f4426b.unlock();
        }
    }

    public void d() {
        try {
            this.f4426b.lock();
            Iterator it = this.f4425a.iterator();
            while (it.hasNext()) {
                b bVar = (b) it.next();
                if (a(bVar)) {
                    super.offer(bVar);
                    it.remove();
                }
            }
        } finally {
            this.f4426b.unlock();
        }
    }

    public int drainTo(Collection<? super E> collection) {
        try {
            this.f4426b.lock();
            int drainTo = super.drainTo(collection) + this.f4425a.size();
            while (!this.f4425a.isEmpty()) {
                collection.add(this.f4425a.poll());
            }
            return drainTo;
        } finally {
            this.f4426b.unlock();
        }
    }

    public int drainTo(Collection<? super E> collection, int i) {
        try {
            this.f4426b.lock();
            int drainTo = super.drainTo(collection, i);
            while (!this.f4425a.isEmpty() && drainTo <= i) {
                collection.add(this.f4425a.poll());
                drainTo++;
            }
            return drainTo;
        } finally {
            this.f4426b.unlock();
        }
    }

    public boolean remove(Object obj) {
        try {
            this.f4426b.lock();
            return super.remove(obj) || this.f4425a.remove(obj);
        } finally {
            this.f4426b.unlock();
        }
    }

    public boolean removeAll(Collection<?> collection) {
        try {
            this.f4426b.lock();
            return super.removeAll(collection) | this.f4425a.removeAll(collection);
        } finally {
            this.f4426b.unlock();
        }
    }

    public int size() {
        try {
            this.f4426b.lock();
            return this.f4425a.size() + super.size();
        } finally {
            this.f4426b.unlock();
        }
    }

    public Object[] toArray() {
        try {
            this.f4426b.lock();
            return a((T[]) super.toArray(), (T[]) this.f4425a.toArray());
        } finally {
            this.f4426b.unlock();
        }
    }

    public <T> T[] toArray(T[] tArr) {
        try {
            this.f4426b.lock();
            return a((T[]) super.toArray(tArr), (T[]) this.f4425a.toArray(tArr));
        } finally {
            this.f4426b.unlock();
        }
    }
}
