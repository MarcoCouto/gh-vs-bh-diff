package io.a.a.a.a.c.a;

public class e {

    /* renamed from: a reason: collision with root package name */
    private final int f4423a;

    /* renamed from: b reason: collision with root package name */
    private final a f4424b;
    private final d c;

    public e(int i, a aVar, d dVar) {
        this.f4423a = i;
        this.f4424b = aVar;
        this.c = dVar;
    }

    public e(a aVar, d dVar) {
        this(0, aVar, dVar);
    }

    public long a() {
        return this.f4424b.a(this.f4423a);
    }

    public e b() {
        return new e(this.f4423a + 1, this.f4424b, this.c);
    }

    public e c() {
        return new e(this.f4424b, this.c);
    }
}
