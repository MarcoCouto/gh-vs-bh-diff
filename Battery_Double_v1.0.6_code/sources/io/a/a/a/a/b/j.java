package io.a.a.a.a.b;

public abstract class j {

    /* renamed from: a reason: collision with root package name */
    private final String f4378a;

    /* renamed from: b reason: collision with root package name */
    private final String f4379b;

    public static class a extends j {
        public a(String str, String str2) {
            super(str, str2);
        }
    }

    public j(String str, String str2) {
        this.f4378a = str;
        this.f4379b = str2;
    }

    public String a() {
        return this.f4378a;
    }

    public String b() {
        return this.f4379b;
    }
}
