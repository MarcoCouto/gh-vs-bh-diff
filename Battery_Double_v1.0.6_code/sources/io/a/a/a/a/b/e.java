package io.a.a.a.a.b;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.os.Parcel;
import android.os.RemoteException;
import io.a.a.a.c;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

class e implements f {

    /* renamed from: a reason: collision with root package name */
    private final Context f4370a;

    private static final class a implements ServiceConnection {

        /* renamed from: a reason: collision with root package name */
        private boolean f4371a;

        /* renamed from: b reason: collision with root package name */
        private final LinkedBlockingQueue<IBinder> f4372b;

        private a() {
            this.f4371a = false;
            this.f4372b = new LinkedBlockingQueue<>(1);
        }

        public IBinder a() {
            if (this.f4371a) {
                c.h().e("Fabric", "getBinder already called");
            }
            this.f4371a = true;
            try {
                return (IBinder) this.f4372b.poll(200, TimeUnit.MILLISECONDS);
            } catch (InterruptedException e) {
                return null;
            }
        }

        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            try {
                this.f4372b.put(iBinder);
            } catch (InterruptedException e) {
            }
        }

        public void onServiceDisconnected(ComponentName componentName) {
            this.f4372b.clear();
        }
    }

    private static final class b implements IInterface {

        /* renamed from: a reason: collision with root package name */
        private final IBinder f4373a;

        public b(IBinder iBinder) {
            this.f4373a = iBinder;
        }

        public String a() throws RemoteException {
            Parcel obtain = Parcel.obtain();
            Parcel obtain2 = Parcel.obtain();
            String str = null;
            try {
                obtain.writeInterfaceToken("com.google.android.gms.ads.identifier.internal.IAdvertisingIdService");
                this.f4373a.transact(1, obtain, obtain2, 0);
                obtain2.readException();
                str = obtain2.readString();
            } catch (Exception e) {
                c.h().a("Fabric", "Could not get parcel from Google Play Service to capture AdvertisingId");
            } finally {
                obtain2.recycle();
                obtain.recycle();
            }
            return str;
        }

        public IBinder asBinder() {
            return this.f4373a;
        }

        /* JADX INFO: finally extract failed */
        public boolean b() throws RemoteException {
            boolean z = true;
            Parcel obtain = Parcel.obtain();
            Parcel obtain2 = Parcel.obtain();
            try {
                obtain.writeInterfaceToken("com.google.android.gms.ads.identifier.internal.IAdvertisingIdService");
                obtain.writeInt(1);
                this.f4373a.transact(2, obtain, obtain2, 0);
                obtain2.readException();
                if (obtain2.readInt() == 0) {
                    z = false;
                }
                obtain2.recycle();
                obtain.recycle();
                return z;
            } catch (Exception e) {
                c.h().a("Fabric", "Could not get parcel from Google Play Service to capture Advertising limitAdTracking");
                obtain2.recycle();
                obtain.recycle();
                return false;
            } catch (Throwable th) {
                obtain2.recycle();
                obtain.recycle();
                throw th;
            }
        }
    }

    public e(Context context) {
        this.f4370a = context.getApplicationContext();
    }

    public b a() {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            c.h().a("Fabric", "AdvertisingInfoServiceStrategy cannot be called on the main thread");
            return null;
        }
        try {
            this.f4370a.getPackageManager().getPackageInfo("com.android.vending", 0);
            a aVar = new a();
            Intent intent = new Intent("com.google.android.gms.ads.identifier.service.START");
            intent.setPackage("com.google.android.gms");
            try {
                if (this.f4370a.bindService(intent, aVar, 1)) {
                    b bVar = new b(aVar.a());
                    b bVar2 = new b(bVar.a(), bVar.b());
                    this.f4370a.unbindService(aVar);
                    return bVar2;
                }
                c.h().a("Fabric", "Could not bind to Google Play Service to capture AdvertisingId");
                return null;
            } catch (Exception e) {
                c.h().d("Fabric", "Exception in binding to Google Play Service to capture AdvertisingId", e);
                this.f4370a.unbindService(aVar);
                return null;
            } catch (Throwable th) {
                c.h().a("Fabric", "Could not bind to Google Play Service to capture AdvertisingId", th);
                return null;
            }
        } catch (NameNotFoundException e2) {
            c.h().a("Fabric", "Unable to find Google Play Services package name");
            return null;
        } catch (Exception e3) {
            c.h().a("Fabric", "Unable to determine if Google Play Services is available", (Throwable) e3);
            return null;
        }
    }
}
