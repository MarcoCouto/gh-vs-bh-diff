package io.a.a.a.a.b;

class b {

    /* renamed from: a reason: collision with root package name */
    public final String f4363a;

    /* renamed from: b reason: collision with root package name */
    public final boolean f4364b;

    b(String str, boolean z) {
        this.f4363a = str;
        this.f4364b = z;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        b bVar = (b) obj;
        if (this.f4364b != bVar.f4364b) {
            return false;
        }
        if (this.f4363a != null) {
            if (this.f4363a.equals(bVar.f4363a)) {
                return true;
            }
        } else if (bVar.f4363a == null) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int hashCode = (this.f4363a != null ? this.f4363a.hashCode() : 0) * 31;
        if (this.f4364b) {
            i = 1;
        }
        return hashCode + i;
    }
}
