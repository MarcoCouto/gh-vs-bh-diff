package io.a.a.a.a.b;

import android.content.Context;
import io.a.a.a.a.a.b;
import io.a.a.a.a.a.d;
import io.a.a.a.c;

public class q {

    /* renamed from: a reason: collision with root package name */
    private final d<String> f4392a = new d<String>() {
        /* renamed from: a */
        public String b(Context context) throws Exception {
            String installerPackageName = context.getPackageManager().getInstallerPackageName(context.getPackageName());
            return installerPackageName == null ? "" : installerPackageName;
        }
    };

    /* renamed from: b reason: collision with root package name */
    private final b<String> f4393b = new b<>();

    public String a(Context context) {
        try {
            String str = (String) this.f4393b.a(context, this.f4392a);
            if ("".equals(str)) {
                return null;
            }
            return str;
        } catch (Exception e) {
            c.h().e("Fabric", "Failed to determine installer package name", e);
            return null;
        }
    }
}
