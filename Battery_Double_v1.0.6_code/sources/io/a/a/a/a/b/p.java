package io.a.a.a.a.b;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Build.VERSION;
import android.provider.Settings.Secure;
import android.text.TextUtils;
import io.a.a.a.c;
import io.a.a.a.i;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;
import java.util.concurrent.locks.ReentrantLock;
import java.util.regex.Pattern;

public class p {
    private static final Pattern e = Pattern.compile("[^\\p{Alnum}]");
    private static final String f = Pattern.quote("/");

    /* renamed from: a reason: collision with root package name */
    c f4388a;

    /* renamed from: b reason: collision with root package name */
    b f4389b;
    boolean c;
    o d;
    private final ReentrantLock g = new ReentrantLock();
    private final q h;
    private final boolean i;
    private final boolean j;
    private final Context k;
    private final String l;
    private final String m;
    private final Collection<i> n;

    public enum a {
        WIFI_MAC_ADDRESS(1),
        BLUETOOTH_MAC_ADDRESS(2),
        FONT_TOKEN(53),
        ANDROID_ID(100),
        ANDROID_DEVICE_ID(101),
        ANDROID_SERIAL(102),
        ANDROID_ADVERTISING_ID(103);
        
        public final int h;

        private a(int i2) {
            this.h = i2;
        }
    }

    public p(Context context, String str, String str2, Collection<i> collection) {
        if (context == null) {
            throw new IllegalArgumentException("appContext must not be null");
        } else if (str == null) {
            throw new IllegalArgumentException("appIdentifier must not be null");
        } else if (collection == null) {
            throw new IllegalArgumentException("kits must not be null");
        } else {
            this.k = context;
            this.l = str;
            this.m = str2;
            this.n = collection;
            this.h = new q();
            this.f4388a = new c(context);
            this.d = new o();
            this.i = i.a(context, "com.crashlytics.CollectDeviceIdentifiers", true);
            if (!this.i) {
                c.h().a("Fabric", "Device ID collection disabled for " + context.getPackageName());
            }
            this.j = i.a(context, "com.crashlytics.CollectUserIdentifiers", true);
            if (!this.j) {
                c.h().a("Fabric", "User information collection disabled for " + context.getPackageName());
            }
        }
    }

    @SuppressLint({"CommitPrefEdits"})
    private String a(SharedPreferences sharedPreferences) {
        this.g.lock();
        try {
            String string = sharedPreferences.getString("crashlytics.installation.id", null);
            if (string == null) {
                string = a(UUID.randomUUID().toString());
                sharedPreferences.edit().putString("crashlytics.installation.id", string).commit();
            }
            return string;
        } finally {
            this.g.unlock();
        }
    }

    private String a(String str) {
        if (str == null) {
            return null;
        }
        return e.matcher(str).replaceAll("").toLowerCase(Locale.US);
    }

    @SuppressLint({"CommitPrefEdits"})
    private void a(SharedPreferences sharedPreferences, String str) {
        this.g.lock();
        try {
            if (!TextUtils.isEmpty(str)) {
                String string = sharedPreferences.getString("crashlytics.advertising.id", null);
                if (TextUtils.isEmpty(string)) {
                    sharedPreferences.edit().putString("crashlytics.advertising.id", str).commit();
                } else if (!string.equals(str)) {
                    sharedPreferences.edit().remove("crashlytics.installation.id").putString("crashlytics.advertising.id", str).commit();
                }
                this.g.unlock();
            }
        } finally {
            this.g.unlock();
        }
    }

    private void a(Map<a, String> map, a aVar, String str) {
        if (str != null) {
            map.put(aVar, str);
        }
    }

    private String b(String str) {
        return str.replaceAll(f, "");
    }

    private void b(SharedPreferences sharedPreferences) {
        b n2 = n();
        if (n2 != null) {
            a(sharedPreferences, n2.f4363a);
        }
    }

    private Boolean o() {
        b n2 = n();
        if (n2 != null) {
            return Boolean.valueOf(n2.f4364b);
        }
        return null;
    }

    public boolean a() {
        return this.j;
    }

    public String b() {
        String str = this.m;
        if (str != null) {
            return str;
        }
        SharedPreferences a2 = i.a(this.k);
        b(a2);
        String string = a2.getString("crashlytics.installation.id", null);
        return string == null ? a(a2) : string;
    }

    public String c() {
        return this.l;
    }

    public String d() {
        return e() + "/" + f();
    }

    public String e() {
        return b(VERSION.RELEASE);
    }

    public String f() {
        return b(VERSION.INCREMENTAL);
    }

    public String g() {
        return String.format(Locale.US, "%s/%s", new Object[]{b(Build.MANUFACTURER), b(Build.MODEL)});
    }

    public Map<a, String> h() {
        HashMap hashMap = new HashMap();
        for (i iVar : this.n) {
            if (iVar instanceof m) {
                for (Entry entry : ((m) iVar).f().entrySet()) {
                    a(hashMap, (a) entry.getKey(), (String) entry.getValue());
                }
            }
        }
        String k2 = k();
        if (TextUtils.isEmpty(k2)) {
            a(hashMap, a.ANDROID_ID, l());
        } else {
            a(hashMap, a.ANDROID_ADVERTISING_ID, k2);
        }
        return Collections.unmodifiableMap(hashMap);
    }

    public String i() {
        return this.h.a(this.k);
    }

    public Boolean j() {
        if (m()) {
            return o();
        }
        return null;
    }

    public String k() {
        if (!m()) {
            return null;
        }
        b n2 = n();
        if (n2 == null || n2.f4364b) {
            return null;
        }
        return n2.f4363a;
    }

    public String l() {
        boolean equals = Boolean.TRUE.equals(o());
        if (!m() || equals) {
            return null;
        }
        String string = Secure.getString(this.k.getContentResolver(), "android_id");
        if (!"9774d56d682e549c".equals(string)) {
            return a(string);
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public boolean m() {
        return this.i && !this.d.b(this.k);
    }

    /* access modifiers changed from: 0000 */
    public synchronized b n() {
        if (!this.c) {
            this.f4389b = this.f4388a.a();
            this.c = true;
        }
        return this.f4389b;
    }
}
