package io.a.a.a.a.b;

public enum l {
    DEVELOPER(1),
    USER_SIDELOAD(2),
    TEST_DISTRIBUTION(3),
    APP_STORE(4);
    
    private final int e;

    private l(int i) {
        this.e = i;
    }

    public static l a(String str) {
        return "io.crash.air".equals(str) ? TEST_DISTRIBUTION : str != null ? APP_STORE : DEVELOPER;
    }

    public int a() {
        return this.e;
    }

    public String toString() {
        return Integer.toString(this.e);
    }
}
