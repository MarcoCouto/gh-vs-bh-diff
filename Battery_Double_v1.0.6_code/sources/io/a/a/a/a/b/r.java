package io.a.a.a.a.b;

import java.io.Closeable;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.util.logging.Level;
import java.util.logging.Logger;

public class r implements Closeable {

    /* renamed from: b reason: collision with root package name */
    private static final Logger f4395b = Logger.getLogger(r.class.getName());

    /* renamed from: a reason: collision with root package name */
    int f4396a;
    /* access modifiers changed from: private */
    public final RandomAccessFile c;
    private int d;
    private a e;
    private a f;
    private final byte[] g = new byte[16];

    static class a {

        /* renamed from: a reason: collision with root package name */
        static final a f4399a = new a(0, 0);

        /* renamed from: b reason: collision with root package name */
        final int f4400b;
        final int c;

        a(int i, int i2) {
            this.f4400b = i;
            this.c = i2;
        }

        public String toString() {
            return getClass().getSimpleName() + "[position = " + this.f4400b + ", length = " + this.c + "]";
        }
    }

    private final class b extends InputStream {

        /* renamed from: b reason: collision with root package name */
        private int f4402b;
        private int c;

        private b(a aVar) {
            this.f4402b = r.this.b(aVar.f4400b + 4);
            this.c = aVar.c;
        }

        public int read() throws IOException {
            if (this.c == 0) {
                return -1;
            }
            r.this.c.seek((long) this.f4402b);
            int read = r.this.c.read();
            this.f4402b = r.this.b(this.f4402b + 1);
            this.c--;
            return read;
        }

        public int read(byte[] bArr, int i, int i2) throws IOException {
            r.b(bArr, "buffer");
            if ((i | i2) < 0 || i2 > bArr.length - i) {
                throw new ArrayIndexOutOfBoundsException();
            } else if (this.c <= 0) {
                return -1;
            } else {
                if (i2 > this.c) {
                    i2 = this.c;
                }
                r.this.b(this.f4402b, bArr, i, i2);
                this.f4402b = r.this.b(this.f4402b + i2);
                this.c -= i2;
                return i2;
            }
        }
    }

    public interface c {
        void a(InputStream inputStream, int i) throws IOException;
    }

    public r(File file) throws IOException {
        if (!file.exists()) {
            a(file);
        }
        this.c = b(file);
        c();
    }

    private static int a(byte[] bArr, int i) {
        return ((bArr[i] & 255) << 24) + ((bArr[i + 1] & 255) << 16) + ((bArr[i + 2] & 255) << 8) + (bArr[i + 3] & 255);
    }

    private a a(int i) throws IOException {
        if (i == 0) {
            return a.f4399a;
        }
        this.c.seek((long) i);
        return new a(i, this.c.readInt());
    }

    private void a(int i, int i2, int i3, int i4) throws IOException {
        a(this.g, i, i2, i3, i4);
        this.c.seek(0);
        this.c.write(this.g);
    }

    private void a(int i, byte[] bArr, int i2, int i3) throws IOException {
        int b2 = b(i);
        if (b2 + i3 <= this.f4396a) {
            this.c.seek((long) b2);
            this.c.write(bArr, i2, i3);
            return;
        }
        int i4 = this.f4396a - b2;
        this.c.seek((long) b2);
        this.c.write(bArr, i2, i4);
        this.c.seek(16);
        this.c.write(bArr, i2 + i4, i3 - i4);
    }

    /* JADX INFO: finally extract failed */
    private static void a(File file) throws IOException {
        File file2 = new File(file.getPath() + ".tmp");
        RandomAccessFile b2 = b(file2);
        try {
            b2.setLength(4096);
            b2.seek(0);
            byte[] bArr = new byte[16];
            a(bArr, 4096, 0, 0, 0);
            b2.write(bArr);
            b2.close();
            if (!file2.renameTo(file)) {
                throw new IOException("Rename failed!");
            }
        } catch (Throwable th) {
            b2.close();
            throw th;
        }
    }

    private static void a(byte[] bArr, int... iArr) {
        int i = 0;
        for (int b2 : iArr) {
            b(bArr, i, b2);
            i += 4;
        }
    }

    /* access modifiers changed from: private */
    public int b(int i) {
        return i < this.f4396a ? i : (i + 16) - this.f4396a;
    }

    private static RandomAccessFile b(File file) throws FileNotFoundException {
        return new RandomAccessFile(file, "rwd");
    }

    /* access modifiers changed from: private */
    public static <T> T b(T t, String str) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException(str);
    }

    /* access modifiers changed from: private */
    public void b(int i, byte[] bArr, int i2, int i3) throws IOException {
        int b2 = b(i);
        if (b2 + i3 <= this.f4396a) {
            this.c.seek((long) b2);
            this.c.readFully(bArr, i2, i3);
            return;
        }
        int i4 = this.f4396a - b2;
        this.c.seek((long) b2);
        this.c.readFully(bArr, i2, i4);
        this.c.seek(16);
        this.c.readFully(bArr, i2 + i4, i3 - i4);
    }

    private static void b(byte[] bArr, int i, int i2) {
        bArr[i] = (byte) (i2 >> 24);
        bArr[i + 1] = (byte) (i2 >> 16);
        bArr[i + 2] = (byte) (i2 >> 8);
        bArr[i + 3] = (byte) i2;
    }

    private void c() throws IOException {
        this.c.seek(0);
        this.c.readFully(this.g);
        this.f4396a = a(this.g, 0);
        if (((long) this.f4396a) > this.c.length()) {
            throw new IOException("File is truncated. Expected length: " + this.f4396a + ", Actual length: " + this.c.length());
        }
        this.d = a(this.g, 4);
        int a2 = a(this.g, 8);
        int a3 = a(this.g, 12);
        this.e = a(a2);
        this.f = a(a3);
    }

    private void c(int i) throws IOException {
        int i2 = i + 4;
        int d2 = d();
        if (d2 < i2) {
            int i3 = this.f4396a;
            do {
                d2 += i3;
                i3 <<= 1;
            } while (d2 < i2);
            d(i3);
            int b2 = b(this.f.f4400b + 4 + this.f.c);
            if (b2 < this.e.f4400b) {
                FileChannel channel = this.c.getChannel();
                channel.position((long) this.f4396a);
                int i4 = b2 - 4;
                if (channel.transferTo(16, (long) i4, channel) != ((long) i4)) {
                    throw new AssertionError("Copied insufficient number of bytes!");
                }
            }
            if (this.f.f4400b < this.e.f4400b) {
                int i5 = (this.f4396a + this.f.f4400b) - 16;
                a(i3, this.d, this.e.f4400b, i5);
                this.f = new a(i5, this.f.c);
            } else {
                a(i3, this.d, this.e.f4400b, this.f.f4400b);
            }
            this.f4396a = i3;
        }
    }

    private int d() {
        return this.f4396a - a();
    }

    private void d(int i) throws IOException {
        this.c.setLength((long) i);
        this.c.getChannel().force(true);
    }

    public int a() {
        if (this.d == 0) {
            return 16;
        }
        return this.f.f4400b >= this.e.f4400b ? (this.f.f4400b - this.e.f4400b) + 4 + this.f.c + 16 : (((this.f.f4400b + 4) + this.f.c) + this.f4396a) - this.e.f4400b;
    }

    public synchronized void a(c cVar) throws IOException {
        int i = this.e.f4400b;
        for (int i2 = 0; i2 < this.d; i2++) {
            a a2 = a(i);
            cVar.a(new b(a2), a2.c);
            i = b(a2.c + a2.f4400b + 4);
        }
    }

    public void a(byte[] bArr) throws IOException {
        a(bArr, 0, bArr.length);
    }

    public synchronized void a(byte[] bArr, int i, int i2) throws IOException {
        b(bArr, "buffer");
        if ((i | i2) < 0 || i2 > bArr.length - i) {
            throw new IndexOutOfBoundsException();
        }
        c(i2);
        boolean b2 = b();
        a aVar = new a(b2 ? 16 : b(this.f.f4400b + 4 + this.f.c), i2);
        b(this.g, 0, i2);
        a(aVar.f4400b, this.g, 0, 4);
        a(aVar.f4400b + 4, bArr, i, i2);
        a(this.f4396a, this.d + 1, b2 ? aVar.f4400b : this.e.f4400b, aVar.f4400b);
        this.f = aVar;
        this.d++;
        if (b2) {
            this.e = this.f;
        }
    }

    public boolean a(int i, int i2) {
        return (a() + 4) + i <= i2;
    }

    public synchronized boolean b() {
        return this.d == 0;
    }

    public synchronized void close() throws IOException {
        this.c.close();
    }

    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName()).append('[');
        sb.append("fileLength=").append(this.f4396a);
        sb.append(", size=").append(this.d);
        sb.append(", first=").append(this.e);
        sb.append(", last=").append(this.f);
        sb.append(", element lengths=[");
        try {
            a((c) new c() {

                /* renamed from: a reason: collision with root package name */
                boolean f4397a = true;

                public void a(InputStream inputStream, int i) throws IOException {
                    if (this.f4397a) {
                        this.f4397a = false;
                    } else {
                        sb.append(", ");
                    }
                    sb.append(i);
                }
            });
        } catch (IOException e2) {
            f4395b.log(Level.WARNING, "read error", e2);
        }
        sb.append("]]");
        return sb.toString();
    }
}
