package io.a.a.a.a.b;

import android.os.SystemClock;
import android.util.Log;

public class u {

    /* renamed from: a reason: collision with root package name */
    private final String f4403a;

    /* renamed from: b reason: collision with root package name */
    private final String f4404b;
    private final boolean c;
    private long d;
    private long e;

    public u(String str, String str2) {
        this.f4403a = str;
        this.f4404b = str2;
        this.c = !Log.isLoggable(str2, 2);
    }

    private void c() {
        Log.v(this.f4404b, this.f4403a + ": " + this.e + "ms");
    }

    public synchronized void a() {
        if (!this.c) {
            this.d = SystemClock.elapsedRealtime();
            this.e = 0;
        }
    }

    public synchronized void b() {
        if (!this.c) {
            if (this.e == 0) {
                this.e = SystemClock.elapsedRealtime() - this.d;
                c();
            }
        }
    }
}
