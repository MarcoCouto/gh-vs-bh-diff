package io.a.a.a.a.b;

import io.a.a.a.a.e.c;
import io.a.a.a.a.e.d;
import io.a.a.a.a.e.e;
import io.a.a.a.i;
import java.util.Collections;
import java.util.Map;
import java.util.regex.Pattern;

public abstract class a {

    /* renamed from: b reason: collision with root package name */
    private static final Pattern f4361b = Pattern.compile("http(s?)://[^\\/]+", 2);

    /* renamed from: a reason: collision with root package name */
    protected final i f4362a;
    private final String c;
    private final e d;
    private final c e;
    private final String f;

    public a(i iVar, String str, String str2, e eVar, c cVar) {
        if (str2 == null) {
            throw new IllegalArgumentException("url must not be null.");
        } else if (eVar == null) {
            throw new IllegalArgumentException("requestFactory must not be null.");
        } else {
            this.f4362a = iVar;
            this.f = str;
            this.c = a(str2);
            this.d = eVar;
            this.e = cVar;
        }
    }

    private String a(String str) {
        return !i.d(this.f) ? f4361b.matcher(str).replaceFirst(this.f) : str;
    }

    /* access modifiers changed from: protected */
    public d a(Map<String, String> map) {
        return this.d.a(this.e, a(), map).a(false).a(10000).a("User-Agent", "Crashlytics Android SDK/" + this.f4362a.a()).a("X-CRASHLYTICS-DEVELOPER-TOKEN", "470fa2b4ae81cd56ecbcda9735803434cec591fa");
    }

    /* access modifiers changed from: protected */
    public String a() {
        return this.c;
    }

    /* access modifiers changed from: protected */
    public d b() {
        return a(Collections.emptyMap());
    }
}
