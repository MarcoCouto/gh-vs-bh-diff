package io.a.a.a.a.b;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.TextUtils;
import io.a.a.a.a.f.d;

class c {

    /* renamed from: a reason: collision with root package name */
    private final Context f4365a;

    /* renamed from: b reason: collision with root package name */
    private final io.a.a.a.a.f.c f4366b;

    public c(Context context) {
        this.f4365a = context.getApplicationContext();
        this.f4366b = new d(context, "TwitterAdvertisingInfoPreferences");
    }

    private void a(final b bVar) {
        new Thread(new h() {
            public void a() {
                b a2 = c.this.e();
                if (!bVar.equals(a2)) {
                    io.a.a.a.c.h().a("Fabric", "Asychronously getting Advertising Info and storing it to preferences");
                    c.this.b(a2);
                }
            }
        }).start();
    }

    /* access modifiers changed from: private */
    @SuppressLint({"CommitPrefEdits"})
    public void b(b bVar) {
        if (c(bVar)) {
            this.f4366b.a(this.f4366b.b().putString("advertising_id", bVar.f4363a).putBoolean("limit_ad_tracking_enabled", bVar.f4364b));
        } else {
            this.f4366b.a(this.f4366b.b().remove("advertising_id").remove("limit_ad_tracking_enabled"));
        }
    }

    private boolean c(b bVar) {
        return bVar != null && !TextUtils.isEmpty(bVar.f4363a);
    }

    /* access modifiers changed from: private */
    public b e() {
        b a2 = c().a();
        if (!c(a2)) {
            a2 = d().a();
            if (!c(a2)) {
                io.a.a.a.c.h().a("Fabric", "AdvertisingInfo not present");
            } else {
                io.a.a.a.c.h().a("Fabric", "Using AdvertisingInfo from Service Provider");
            }
        } else {
            io.a.a.a.c.h().a("Fabric", "Using AdvertisingInfo from Reflection Provider");
        }
        return a2;
    }

    public b a() {
        b b2 = b();
        if (c(b2)) {
            io.a.a.a.c.h().a("Fabric", "Using AdvertisingInfo from Preference Store");
            a(b2);
            return b2;
        }
        b e = e();
        b(e);
        return e;
    }

    /* access modifiers changed from: protected */
    public b b() {
        return new b(this.f4366b.a().getString("advertising_id", ""), this.f4366b.a().getBoolean("limit_ad_tracking_enabled", false));
    }

    public f c() {
        return new d(this.f4365a);
    }

    public f d() {
        return new e(this.f4365a);
    }
}
