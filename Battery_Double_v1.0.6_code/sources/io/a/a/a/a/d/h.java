package io.a.a.a.a.d;

import android.content.Context;
import io.a.a.a.a.b.i;
import io.a.a.a.a.b.r;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class h implements c {

    /* renamed from: a reason: collision with root package name */
    private final Context f4444a;

    /* renamed from: b reason: collision with root package name */
    private final File f4445b;
    private final String c;
    private final File d;
    private r e = new r(this.d);
    private File f;

    public h(Context context, File file, String str, String str2) throws IOException {
        this.f4444a = context;
        this.f4445b = file;
        this.c = str2;
        this.d = new File(this.f4445b, str);
        e();
    }

    private void a(File file, File file2) throws IOException {
        FileInputStream fileInputStream;
        OutputStream outputStream = null;
        try {
            fileInputStream = new FileInputStream(file);
            try {
                outputStream = a(file2);
                i.a((InputStream) fileInputStream, outputStream, new byte[1024]);
                i.a((Closeable) fileInputStream, "Failed to close file input stream");
                i.a((Closeable) outputStream, "Failed to close output stream");
                file.delete();
            } catch (Throwable th) {
                th = th;
                i.a((Closeable) fileInputStream, "Failed to close file input stream");
                i.a((Closeable) outputStream, "Failed to close output stream");
                file.delete();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            fileInputStream = null;
            i.a((Closeable) fileInputStream, "Failed to close file input stream");
            i.a((Closeable) outputStream, "Failed to close output stream");
            file.delete();
            throw th;
        }
    }

    private void e() {
        this.f = new File(this.f4445b, this.c);
        if (!this.f.exists()) {
            this.f.mkdirs();
        }
    }

    public int a() {
        return this.e.a();
    }

    public OutputStream a(File file) throws IOException {
        return new FileOutputStream(file);
    }

    public List<File> a(int i) {
        ArrayList arrayList = new ArrayList();
        for (File add : this.f.listFiles()) {
            arrayList.add(add);
            if (arrayList.size() >= i) {
                break;
            }
        }
        return arrayList;
    }

    public void a(String str) throws IOException {
        this.e.close();
        a(this.d, new File(this.f, str));
        this.e = new r(this.d);
    }

    public void a(List<File> list) {
        for (File file : list) {
            i.a(this.f4444a, String.format("deleting sent analytics file %s", new Object[]{file.getName()}));
            file.delete();
        }
    }

    public void a(byte[] bArr) throws IOException {
        this.e.a(bArr);
    }

    public boolean a(int i, int i2) {
        return this.e.a(i, i2);
    }

    public boolean b() {
        return this.e.b();
    }

    public List<File> c() {
        return Arrays.asList(this.f.listFiles());
    }

    public void d() {
        try {
            this.e.close();
        } catch (IOException e2) {
        }
        this.d.delete();
    }
}
