package io.a.a.a.a.d;

import android.content.Context;

public class i implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final Context f4446a;

    /* renamed from: b reason: collision with root package name */
    private final e f4447b;

    public i(Context context, e eVar) {
        this.f4446a = context;
        this.f4447b = eVar;
    }

    public void run() {
        try {
            io.a.a.a.a.b.i.a(this.f4446a, "Performing time based file roll over.");
            if (!this.f4447b.c()) {
                this.f4447b.d();
            }
        } catch (Exception e) {
            io.a.a.a.a.b.i.a(this.f4446a, "Failed to roll over file", (Throwable) e);
        }
    }
}
