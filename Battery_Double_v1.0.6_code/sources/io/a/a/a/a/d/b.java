package io.a.a.a.a.d;

import android.content.Context;
import io.a.a.a.a.b.i;
import io.a.a.a.a.b.k;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.TreeSet;
import java.util.concurrent.CopyOnWriteArrayList;

public abstract class b<T> {

    /* renamed from: a reason: collision with root package name */
    protected final Context f4439a;

    /* renamed from: b reason: collision with root package name */
    protected final a<T> f4440b;
    protected final k c;
    protected final c d;
    protected volatile long e;
    protected final List<d> f = new CopyOnWriteArrayList();
    private final int g;

    static class a {

        /* renamed from: a reason: collision with root package name */
        final File f4442a;

        /* renamed from: b reason: collision with root package name */
        final long f4443b;

        public a(File file, long j) {
            this.f4442a = file;
            this.f4443b = j;
        }
    }

    public b(Context context, a<T> aVar, k kVar, c cVar, int i) throws IOException {
        this.f4439a = context.getApplicationContext();
        this.f4440b = aVar;
        this.d = cVar;
        this.c = kVar;
        this.e = this.c.a();
        this.g = i;
    }

    private void a(int i) throws IOException {
        if (!this.d.a(i, c())) {
            i.a(this.f4439a, 4, "Fabric", String.format(Locale.US, "session analytics events file is %d bytes, new event is %d bytes, this is over flush limit of %d, rolling it over", new Object[]{Integer.valueOf(this.d.a()), Integer.valueOf(i), Integer.valueOf(c())}));
            d();
        }
    }

    private void b(String str) {
        for (d a2 : this.f) {
            try {
                a2.a(str);
            } catch (Exception e2) {
                i.a(this.f4439a, "One of the roll over listeners threw an exception", (Throwable) e2);
            }
        }
    }

    public long a(String str) {
        long j = 0;
        String[] split = str.split("_");
        if (split.length != 3) {
            return j;
        }
        try {
            return Long.valueOf(split[2]).longValue();
        } catch (NumberFormatException e2) {
            return j;
        }
    }

    /* access modifiers changed from: protected */
    public abstract String a();

    public void a(d dVar) {
        if (dVar != null) {
            this.f.add(dVar);
        }
    }

    public void a(T t) throws IOException {
        byte[] a2 = this.f4440b.a(t);
        a(a2.length);
        this.d.a(a2);
    }

    public void a(List<File> list) {
        this.d.a(list);
    }

    /* access modifiers changed from: protected */
    public int b() {
        return this.g;
    }

    /* access modifiers changed from: protected */
    public int c() {
        return 8000;
    }

    public boolean d() throws IOException {
        boolean z = true;
        String str = null;
        if (!this.d.b()) {
            str = a();
            this.d.a(str);
            i.a(this.f4439a, 4, "Fabric", String.format(Locale.US, "generated new file %s", new Object[]{str}));
            this.e = this.c.a();
        } else {
            z = false;
        }
        b(str);
        return z;
    }

    public List<File> e() {
        return this.d.a(1);
    }

    public void f() {
        this.d.a(this.d.c());
        this.d.d();
    }

    public void g() {
        List<File> c2 = this.d.c();
        int b2 = b();
        if (c2.size() > b2) {
            int size = c2.size() - b2;
            i.a(this.f4439a, String.format(Locale.US, "Found %d files in  roll over directory, this is greater than %d, deleting %d oldest files", new Object[]{Integer.valueOf(c2.size()), Integer.valueOf(b2), Integer.valueOf(size)}));
            TreeSet treeSet = new TreeSet(new Comparator<a>() {
                /* renamed from: a */
                public int compare(a aVar, a aVar2) {
                    return (int) (aVar.f4443b - aVar2.f4443b);
                }
            });
            for (File file : c2) {
                treeSet.add(new a(file, a(file.getName())));
            }
            ArrayList arrayList = new ArrayList();
            Iterator it = treeSet.iterator();
            while (it.hasNext()) {
                arrayList.add(((a) it.next()).f4442a);
                if (arrayList.size() == size) {
                    break;
                }
            }
            this.d.a((List<File>) arrayList);
        }
    }
}
