package io.a.a.a.a.f;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Build.VERSION;
import io.a.a.a.i;

public class d implements c {

    /* renamed from: a reason: collision with root package name */
    private final SharedPreferences f4467a;

    /* renamed from: b reason: collision with root package name */
    private final String f4468b;
    private final Context c;

    public d(Context context, String str) {
        if (context == null) {
            throw new IllegalStateException("Cannot get directory before context has been set. Call Fabric.with() first");
        }
        this.c = context;
        this.f4468b = str;
        this.f4467a = this.c.getSharedPreferences(this.f4468b, 0);
    }

    @Deprecated
    public d(i iVar) {
        this(iVar.q(), iVar.getClass().getName());
    }

    public SharedPreferences a() {
        return this.f4467a;
    }

    @TargetApi(9)
    public boolean a(Editor editor) {
        if (VERSION.SDK_INT < 9) {
            return editor.commit();
        }
        editor.apply();
        return true;
    }

    public Editor b() {
        return this.f4467a.edit();
    }
}
