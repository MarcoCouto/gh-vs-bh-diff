package io.a.a.a.a.f;

import android.content.Context;
import io.a.a.a.c;
import io.a.a.a.i;
import java.io.File;

public class b implements a {

    /* renamed from: a reason: collision with root package name */
    private final Context f4465a;

    /* renamed from: b reason: collision with root package name */
    private final String f4466b;
    private final String c;

    public b(i iVar) {
        if (iVar.q() == null) {
            throw new IllegalStateException("Cannot get directory before context has been set. Call Fabric.with() first");
        }
        this.f4465a = iVar.q();
        this.f4466b = iVar.s();
        this.c = "Android/" + this.f4465a.getPackageName();
    }

    public File a() {
        return a(this.f4465a.getFilesDir());
    }

    /* access modifiers changed from: 0000 */
    public File a(File file) {
        if (file == null) {
            c.h().a("Fabric", "Null File");
        } else if (file.exists() || file.mkdirs()) {
            return file;
        } else {
            c.h().d("Fabric", "Couldn't create file");
        }
        return null;
    }
}
