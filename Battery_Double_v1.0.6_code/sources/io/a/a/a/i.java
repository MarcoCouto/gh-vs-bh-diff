package io.a.a.a;

import android.content.Context;
import io.a.a.a.a.b.p;
import io.a.a.a.a.c.d;
import io.a.a.a.a.c.l;
import java.io.File;
import java.util.Collection;

public abstract class i<Result> implements Comparable<i> {
    c e;
    h<Result> f = new h<>(this);
    Context g;
    f<Result> h;
    p i;
    final d j = ((d) getClass().getAnnotation(d.class));

    /* renamed from: a */
    public int compareTo(i iVar) {
        if (b(iVar)) {
            return 1;
        }
        if (iVar.b(this)) {
            return -1;
        }
        if (!t() || iVar.t()) {
            return (t() || !iVar.t()) ? 0 : -1;
        }
        return 1;
    }

    public abstract String a();

    /* access modifiers changed from: 0000 */
    public void a(Context context, c cVar, f<Result> fVar, p pVar) {
        this.e = cVar;
        this.g = new d(context, b(), s());
        this.h = fVar;
        this.i = pVar;
    }

    /* access modifiers changed from: protected */
    public void a(Result result) {
    }

    public abstract String b();

    /* access modifiers changed from: protected */
    public void b(Result result) {
    }

    /* access modifiers changed from: 0000 */
    public boolean b(i iVar) {
        if (!t()) {
            return false;
        }
        for (Class isAssignableFrom : this.j.a()) {
            if (isAssignableFrom.isAssignableFrom(iVar.getClass())) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean b_() {
        return true;
    }

    /* access modifiers changed from: protected */
    public abstract Result e();

    /* access modifiers changed from: 0000 */
    public final void o() {
        this.f.a(this.e.f(), null);
    }

    /* access modifiers changed from: protected */
    public p p() {
        return this.i;
    }

    public Context q() {
        return this.g;
    }

    public c r() {
        return this.e;
    }

    public String s() {
        return ".Fabric" + File.separator + b();
    }

    /* access modifiers changed from: 0000 */
    public boolean t() {
        return this.j != null;
    }

    /* access modifiers changed from: protected */
    public Collection<l> u() {
        return this.f.c();
    }
}
