package io.a.a.a;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import com.hmatalonga.greenhub.Config;
import io.a.a.a.a.b.g;
import io.a.a.a.a.b.i;
import io.a.a.a.a.b.l;
import io.a.a.a.a.e.b;
import io.a.a.a.a.e.e;
import io.a.a.a.a.g.d;
import io.a.a.a.a.g.h;
import io.a.a.a.a.g.n;
import io.a.a.a.a.g.q;
import io.a.a.a.a.g.t;
import io.a.a.a.a.g.y;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Future;

class m extends i<Boolean> {

    /* renamed from: a reason: collision with root package name */
    private final e f4514a = new b();

    /* renamed from: b reason: collision with root package name */
    private PackageManager f4515b;
    private String c;
    private PackageInfo d;
    private String k;
    private String l;
    private String m;
    private String n;
    private String o;
    private final Future<Map<String, k>> p;
    private final Collection<i> q;

    public m(Future<Map<String, k>> future, Collection<i> collection) {
        this.p = future;
        this.q = collection;
    }

    private d a(n nVar, Collection<k> collection) {
        Context q2 = q();
        return new d(new g().a(q2), p().c(), this.l, this.k, i.a(i.m(q2)), this.n, l.a(this.m).a(), this.o, Config.NOTIFICATION_DEFAULT_PRIORITY, nVar, collection);
    }

    private boolean a(io.a.a.a.a.g.e eVar, n nVar, Collection<k> collection) {
        return new y(this, f(), eVar.c, this.f4514a).a(a(nVar, collection));
    }

    private boolean a(String str, io.a.a.a.a.g.e eVar, Collection<k> collection) {
        if ("new".equals(eVar.f4476b)) {
            if (b(str, eVar, collection)) {
                return q.a().d();
            }
            c.h().e("Fabric", "Failed to create app with Crashlytics service.", null);
            return false;
        } else if ("configured".equals(eVar.f4476b)) {
            return q.a().d();
        } else {
            if (!eVar.f) {
                return true;
            }
            c.h().a("Fabric", "Server says an update is required - forcing a full App update.");
            c(str, eVar, collection);
            return true;
        }
    }

    private boolean b(String str, io.a.a.a.a.g.e eVar, Collection<k> collection) {
        return new h(this, f(), eVar.c, this.f4514a).a(a(n.a(q(), str), collection));
    }

    private boolean c(String str, io.a.a.a.a.g.e eVar, Collection<k> collection) {
        return a(eVar, n.a(q(), str), collection);
    }

    private t g() {
        try {
            q.a().a(this, this.i, this.f4514a, this.k, this.l, f()).c();
            return q.a().b();
        } catch (Exception e) {
            c.h().e("Fabric", "Error dealing with settings", e);
            return null;
        }
    }

    public String a() {
        return "1.4.1.19";
    }

    /* access modifiers changed from: 0000 */
    public Map<String, k> a(Map<String, k> map, Collection<i> collection) {
        for (i iVar : collection) {
            if (!map.containsKey(iVar.b())) {
                map.put(iVar.b(), new k(iVar.b(), iVar.a(), "binary"));
            }
        }
        return map;
    }

    public String b() {
        return "io.fabric.sdk.android:fabric";
    }

    /* access modifiers changed from: protected */
    public boolean b_() {
        try {
            this.m = p().i();
            this.f4515b = q().getPackageManager();
            this.c = q().getPackageName();
            this.d = this.f4515b.getPackageInfo(this.c, 0);
            this.k = Integer.toString(this.d.versionCode);
            this.l = this.d.versionName == null ? "0.0" : this.d.versionName;
            this.n = this.f4515b.getApplicationLabel(q().getApplicationInfo()).toString();
            this.o = Integer.toString(q().getApplicationInfo().targetSdkVersion);
            return true;
        } catch (NameNotFoundException e) {
            c.h().e("Fabric", "Failed init", e);
            return false;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: c */
    public Boolean e() {
        boolean z;
        String k2 = i.k(q());
        t g = g();
        if (g != null) {
            try {
                z = a(k2, g.f4495a, a(this.p != null ? (Map) this.p.get() : new HashMap<>(), this.q).values());
            } catch (Exception e) {
                c.h().e("Fabric", "Error performing auto configuration.", e);
            }
            return Boolean.valueOf(z);
        }
        z = false;
        return Boolean.valueOf(z);
    }

    /* access modifiers changed from: 0000 */
    public String f() {
        return i.b(q(), "com.crashlytics.ApiEndpoint");
    }
}
