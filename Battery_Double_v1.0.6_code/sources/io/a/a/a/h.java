package io.a.a.a;

import io.a.a.a.a.b.u;
import io.a.a.a.a.c.e;
import io.a.a.a.a.c.f;
import io.a.a.a.a.c.m;

class h<Result> extends f<Void, Void, Result> {

    /* renamed from: a reason: collision with root package name */
    final i<Result> f4511a;

    public h(i<Result> iVar) {
        this.f4511a = iVar;
    }

    private u a(String str) {
        u uVar = new u(this.f4511a.b() + "." + str, "KitInitialization");
        uVar.a();
        return uVar;
    }

    /* access modifiers changed from: protected */
    public Result a(Void... voidArr) {
        u a2 = a("doInBackground");
        Result result = null;
        if (!e()) {
            result = this.f4511a.e();
        }
        a2.b();
        return result;
    }

    /* access modifiers changed from: protected */
    public void a() {
        super.a();
        u a2 = a("onPreExecute");
        try {
            boolean b_ = this.f4511a.b_();
            a2.b();
            if (!b_) {
                a(true);
            }
        } catch (m e) {
            throw e;
        } catch (Exception e2) {
            c.h().e("Fabric", "Failure onPreExecute()", e2);
            a2.b();
            a(true);
        } catch (Throwable th) {
            a2.b();
            a(true);
            throw th;
        }
    }

    /* access modifiers changed from: protected */
    public void a(Result result) {
        this.f4511a.a(result);
        this.f4511a.h.a(result);
    }

    public e b() {
        return e.HIGH;
    }

    /* access modifiers changed from: protected */
    public void b(Result result) {
        this.f4511a.b(result);
        this.f4511a.h.a((Exception) new g(this.f4511a.b() + " Initialization was cancelled"));
    }
}
