package io.a.a.a;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import io.a.a.a.a.b;
import io.a.a.a.a.b.p;
import io.a.a.a.a.c.d;
import io.a.a.a.a.c.k;
import io.a.a.a.a.c.l;
import io.a.a.a.a.c.m;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicBoolean;

public class c {

    /* renamed from: a reason: collision with root package name */
    static volatile c f4501a;

    /* renamed from: b reason: collision with root package name */
    static final l f4502b = new b();
    final l c;
    final boolean d;
    private final Context e;
    private final Map<Class<? extends i>, i> f;
    private final ExecutorService g;
    private final Handler h;
    /* access modifiers changed from: private */
    public final f<c> i;
    private final f<?> j;
    private final p k;
    private a l;
    private WeakReference<Activity> m;
    /* access modifiers changed from: private */
    public AtomicBoolean n = new AtomicBoolean(false);

    public static class a {

        /* renamed from: a reason: collision with root package name */
        private final Context f4506a;

        /* renamed from: b reason: collision with root package name */
        private i[] f4507b;
        private k c;
        private Handler d;
        private l e;
        private boolean f;
        private String g;
        private String h;
        private f<c> i;

        public a(Context context) {
            if (context == null) {
                throw new IllegalArgumentException("Context must not be null.");
            }
            this.f4506a = context;
        }

        public a a(i... iVarArr) {
            if (this.f4507b != null) {
                throw new IllegalStateException("Kits already set.");
            }
            this.f4507b = iVarArr;
            return this;
        }

        public c a() {
            if (this.c == null) {
                this.c = k.a();
            }
            if (this.d == null) {
                this.d = new Handler(Looper.getMainLooper());
            }
            if (this.e == null) {
                if (this.f) {
                    this.e = new b(3);
                } else {
                    this.e = new b();
                }
            }
            if (this.h == null) {
                this.h = this.f4506a.getPackageName();
            }
            if (this.i == null) {
                this.i = f.d;
            }
            Map a2 = this.f4507b == null ? new HashMap() : c.b((Collection<? extends i>) Arrays.asList(this.f4507b));
            Context applicationContext = this.f4506a.getApplicationContext();
            return new c(applicationContext, a2, this.c, this.d, this.e, this.f, this.i, new p(applicationContext, this.h, this.g, a2.values()), c.d(this.f4506a));
        }
    }

    c(Context context, Map<Class<? extends i>, i> map, k kVar, Handler handler, l lVar, boolean z, f fVar, p pVar, Activity activity) {
        this.e = context;
        this.f = map;
        this.g = kVar;
        this.h = handler;
        this.c = lVar;
        this.d = z;
        this.i = fVar;
        this.j = a(map.size());
        this.k = pVar;
        a(activity);
    }

    static c a() {
        if (f4501a != null) {
            return f4501a;
        }
        throw new IllegalStateException("Must Initialize Fabric before using singleton()");
    }

    public static c a(Context context, i... iVarArr) {
        if (f4501a == null) {
            synchronized (c.class) {
                if (f4501a == null) {
                    c(new a(context).a(iVarArr).a());
                }
            }
        }
        return f4501a;
    }

    public static <T extends i> T a(Class<T> cls) {
        return (i) a().f.get(cls);
    }

    private static void a(Map<Class<? extends i>, i> map, Collection<? extends i> collection) {
        for (i iVar : collection) {
            map.put(iVar.getClass(), iVar);
            if (iVar instanceof j) {
                a(map, ((j) iVar).c());
            }
        }
    }

    /* access modifiers changed from: private */
    public static Map<Class<? extends i>, i> b(Collection<? extends i> collection) {
        HashMap hashMap = new HashMap(collection.size());
        a((Map<Class<? extends i>, i>) hashMap, collection);
        return hashMap;
    }

    private static void c(c cVar) {
        f4501a = cVar;
        cVar.j();
    }

    /* access modifiers changed from: private */
    public static Activity d(Context context) {
        if (context instanceof Activity) {
            return (Activity) context;
        }
        return null;
    }

    public static l h() {
        return f4501a == null ? f4502b : f4501a.c;
    }

    public static boolean i() {
        if (f4501a == null) {
            return false;
        }
        return f4501a.d;
    }

    private void j() {
        this.l = new a(this.e);
        this.l.a(new b() {
            public void a(Activity activity) {
                c.this.a(activity);
            }

            public void a(Activity activity, Bundle bundle) {
                c.this.a(activity);
            }

            public void b(Activity activity) {
                c.this.a(activity);
            }
        });
        a(this.e);
    }

    public c a(Activity activity) {
        this.m = new WeakReference<>(activity);
        return this;
    }

    /* access modifiers changed from: 0000 */
    public f<?> a(final int i2) {
        return new f() {

            /* renamed from: a reason: collision with root package name */
            final CountDownLatch f4504a = new CountDownLatch(i2);

            public void a(Exception exc) {
                c.this.i.a(exc);
            }

            public void a(Object obj) {
                this.f4504a.countDown();
                if (this.f4504a.getCount() == 0) {
                    c.this.n.set(true);
                    c.this.i.a(c.this);
                }
            }
        };
    }

    /* access modifiers changed from: 0000 */
    public void a(Context context) {
        Future b2 = b(context);
        Collection g2 = g();
        m mVar = new m(b2, g2);
        ArrayList<i> arrayList = new ArrayList<>(g2);
        Collections.sort(arrayList);
        mVar.a(context, this, f.d, this.k);
        for (i a2 : arrayList) {
            a2.a(context, this, this.j, this.k);
        }
        mVar.o();
        StringBuilder sb = h().a("Fabric", 3) ? new StringBuilder("Initializing ").append(d()).append(" [Version: ").append(c()).append("], with the following kits:\n") : null;
        for (i iVar : arrayList) {
            iVar.f.c((l) mVar.f);
            a(this.f, iVar);
            iVar.o();
            if (sb != null) {
                sb.append(iVar.b()).append(" [Version: ").append(iVar.a()).append("]\n");
            }
        }
        if (sb != null) {
            h().a("Fabric", sb.toString());
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(Map<Class<? extends i>, i> map, i iVar) {
        Class[] a2;
        d dVar = iVar.j;
        if (dVar != null) {
            for (Class cls : dVar.a()) {
                if (cls.isInterface()) {
                    for (i iVar2 : map.values()) {
                        if (cls.isAssignableFrom(iVar2.getClass())) {
                            iVar.f.c((l) iVar2.f);
                        }
                    }
                } else if (((i) map.get(cls)) == null) {
                    throw new m("Referenced Kit was null, does the kit exist?");
                } else {
                    iVar.f.c((l) ((i) map.get(cls)).f);
                }
            }
        }
    }

    public Activity b() {
        if (this.m != null) {
            return (Activity) this.m.get();
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    public Future<Map<String, k>> b(Context context) {
        return f().submit(new e(context.getPackageCodePath()));
    }

    public String c() {
        return "1.4.1.19";
    }

    public String d() {
        return "io.fabric.sdk.android:fabric";
    }

    public a e() {
        return this.l;
    }

    public ExecutorService f() {
        return this.g;
    }

    public Collection<i> g() {
        return this.f.values();
    }
}
