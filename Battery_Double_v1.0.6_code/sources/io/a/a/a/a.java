package io.a.a.a;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application;
import android.app.Application.ActivityLifecycleCallbacks;
import android.content.Context;
import android.os.Build.VERSION;
import android.os.Bundle;
import java.util.HashSet;
import java.util.Set;

public class a {

    /* renamed from: a reason: collision with root package name */
    private final Application f4353a;

    /* renamed from: b reason: collision with root package name */
    private C0071a f4354b;

    /* renamed from: io.a.a.a.a$a reason: collision with other inner class name */
    private static class C0071a {

        /* renamed from: a reason: collision with root package name */
        private final Set<ActivityLifecycleCallbacks> f4355a = new HashSet();

        /* renamed from: b reason: collision with root package name */
        private final Application f4356b;

        C0071a(Application application) {
            this.f4356b = application;
        }

        /* access modifiers changed from: private */
        @TargetApi(14)
        public void a() {
            for (ActivityLifecycleCallbacks unregisterActivityLifecycleCallbacks : this.f4355a) {
                this.f4356b.unregisterActivityLifecycleCallbacks(unregisterActivityLifecycleCallbacks);
            }
        }

        /* access modifiers changed from: private */
        @TargetApi(14)
        public boolean a(final b bVar) {
            if (this.f4356b == null) {
                return false;
            }
            AnonymousClass1 r0 = new ActivityLifecycleCallbacks() {
                public void onActivityCreated(Activity activity, Bundle bundle) {
                    bVar.a(activity, bundle);
                }

                public void onActivityDestroyed(Activity activity) {
                    bVar.e(activity);
                }

                public void onActivityPaused(Activity activity) {
                    bVar.c(activity);
                }

                public void onActivityResumed(Activity activity) {
                    bVar.b(activity);
                }

                public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
                    bVar.b(activity, bundle);
                }

                public void onActivityStarted(Activity activity) {
                    bVar.a(activity);
                }

                public void onActivityStopped(Activity activity) {
                    bVar.d(activity);
                }
            };
            this.f4356b.registerActivityLifecycleCallbacks(r0);
            this.f4355a.add(r0);
            return true;
        }
    }

    public static abstract class b {
        public void a(Activity activity) {
        }

        public void a(Activity activity, Bundle bundle) {
        }

        public void b(Activity activity) {
        }

        public void b(Activity activity, Bundle bundle) {
        }

        public void c(Activity activity) {
        }

        public void d(Activity activity) {
        }

        public void e(Activity activity) {
        }
    }

    public a(Context context) {
        this.f4353a = (Application) context.getApplicationContext();
        if (VERSION.SDK_INT >= 14) {
            this.f4354b = new C0071a(this.f4353a);
        }
    }

    public void a() {
        if (this.f4354b != null) {
            this.f4354b.a();
        }
    }

    public boolean a(b bVar) {
        return this.f4354b != null && this.f4354b.a(bVar);
    }
}
