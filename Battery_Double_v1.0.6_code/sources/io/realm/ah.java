package io.realm;

import io.realm.internal.Table;
import io.realm.internal.b;

class ah extends bo {
    ah(e eVar, b bVar) {
        super(eVar, bVar);
    }

    public bl a(String str) {
        a(str, "Null or empty class names are not allowed");
        String c = Table.c(str);
        if (!this.f4595a.l().hasTable(c)) {
            return null;
        }
        return new ag(this.f4595a, this, this.f4595a.l().getTable(c), c(str));
    }
}
