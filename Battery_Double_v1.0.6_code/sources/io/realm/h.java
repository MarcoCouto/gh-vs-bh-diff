package io.realm;

import com.hmatalonga.greenhub.models.data.BatterySession;
import io.realm.exceptions.RealmException;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.Table;
import io.realm.internal.c;
import io.realm.internal.m;
import io.realm.internal.o;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class h extends BatterySession implements i, m {

    /* renamed from: a reason: collision with root package name */
    private static final OsObjectSchemaInfo f4629a = e();

    /* renamed from: b reason: collision with root package name */
    private static final List<String> f4630b;
    private a c;
    private ba<BatterySession> d;

    static final class a extends c {

        /* renamed from: a reason: collision with root package name */
        long f4631a;

        /* renamed from: b reason: collision with root package name */
        long f4632b;
        long c;
        long d;
        long e;

        a(OsSchemaInfo osSchemaInfo) {
            super(5);
            OsObjectSchemaInfo a2 = osSchemaInfo.a("BatterySession");
            this.f4631a = a("id", a2);
            this.f4632b = a("timestamp", a2);
            this.c = a("level", a2);
            this.d = a("screenOn", a2);
            this.e = a("triggeredBy", a2);
        }

        /* access modifiers changed from: protected */
        public final void a(c cVar, c cVar2) {
            a aVar = (a) cVar;
            a aVar2 = (a) cVar2;
            aVar2.f4631a = aVar.f4631a;
            aVar2.f4632b = aVar.f4632b;
            aVar2.c = aVar.c;
            aVar2.d = aVar.d;
            aVar2.e = aVar.e;
        }
    }

    static {
        ArrayList arrayList = new ArrayList(5);
        arrayList.add("id");
        arrayList.add("timestamp");
        arrayList.add("level");
        arrayList.add("screenOn");
        arrayList.add("triggeredBy");
        f4630b = Collections.unmodifiableList(arrayList);
    }

    h() {
        this.d.g();
    }

    static BatterySession a(bb bbVar, BatterySession batterySession, BatterySession batterySession2, Map<bh, m> map) {
        i iVar = batterySession;
        i iVar2 = batterySession2;
        iVar.realmSet$timestamp(iVar2.realmGet$timestamp());
        iVar.realmSet$level(iVar2.realmGet$level());
        iVar.realmSet$screenOn(iVar2.realmGet$screenOn());
        iVar.realmSet$triggeredBy(iVar2.realmGet$triggeredBy());
        return batterySession;
    }

    /* JADX INFO: finally extract failed */
    public static BatterySession a(bb bbVar, BatterySession batterySession, boolean z, Map<bh, m> map) {
        boolean z2;
        h hVar;
        if ((batterySession instanceof m) && ((m) batterySession).d().a() != null) {
            e a2 = ((m) batterySession).d().a();
            if (a2.c != bbVar.c) {
                throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
            } else if (a2.g().equals(bbVar.g())) {
                return batterySession;
            }
        }
        io.realm.e.a aVar = (io.realm.e.a) e.f.get();
        m mVar = (m) map.get(batterySession);
        if (mVar != null) {
            return (BatterySession) mVar;
        }
        if (z) {
            Table b2 = bbVar.b(BatterySession.class);
            long a3 = b2.a(((a) bbVar.k().c(BatterySession.class)).f4631a, (long) batterySession.realmGet$id());
            if (a3 == -1) {
                z2 = false;
                hVar = null;
            } else {
                try {
                    aVar.a(bbVar, b2.f(a3), bbVar.k().c(BatterySession.class), false, Collections.emptyList());
                    hVar = new h();
                    map.put(batterySession, hVar);
                    aVar.f();
                    z2 = z;
                } catch (Throwable th) {
                    aVar.f();
                    throw th;
                }
            }
        } else {
            z2 = z;
            hVar = null;
        }
        return z2 ? a(bbVar, (BatterySession) hVar, batterySession, map) : b(bbVar, batterySession, z, map);
    }

    public static a a(OsSchemaInfo osSchemaInfo) {
        return new a(osSchemaInfo);
    }

    public static BatterySession b(bb bbVar, BatterySession batterySession, boolean z, Map<bh, m> map) {
        m mVar = (m) map.get(batterySession);
        if (mVar != null) {
            return (BatterySession) mVar;
        }
        BatterySession batterySession2 = (BatterySession) bbVar.a(BatterySession.class, Integer.valueOf(batterySession.realmGet$id()), false, Collections.emptyList());
        map.put(batterySession, (m) batterySession2);
        i iVar = batterySession;
        i iVar2 = batterySession2;
        iVar2.realmSet$timestamp(iVar.realmGet$timestamp());
        iVar2.realmSet$level(iVar.realmGet$level());
        iVar2.realmSet$screenOn(iVar.realmGet$screenOn());
        iVar2.realmSet$triggeredBy(iVar.realmGet$triggeredBy());
        return batterySession2;
    }

    public static OsObjectSchemaInfo b() {
        return f4629a;
    }

    public static String c() {
        return "class_BatterySession";
    }

    private static OsObjectSchemaInfo e() {
        io.realm.internal.OsObjectSchemaInfo.a aVar = new io.realm.internal.OsObjectSchemaInfo.a("BatterySession", 5, 0);
        aVar.a("id", RealmFieldType.INTEGER, true, true, true);
        aVar.a("timestamp", RealmFieldType.INTEGER, false, true, true);
        aVar.a("level", RealmFieldType.FLOAT, false, false, true);
        aVar.a("screenOn", RealmFieldType.INTEGER, false, false, true);
        aVar.a("triggeredBy", RealmFieldType.STRING, false, false, false);
        return aVar.a();
    }

    public void a() {
        if (this.d == null) {
            io.realm.e.a aVar = (io.realm.e.a) e.f.get();
            this.c = (a) aVar.c();
            this.d = new ba<>(this);
            this.d.a(aVar.a());
            this.d.a(aVar.b());
            this.d.a(aVar.d());
            this.d.a(aVar.e());
        }
    }

    public ba<?> d() {
        return this.d;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        h hVar = (h) obj;
        String g = this.d.a().g();
        String g2 = hVar.d.a().g();
        if (g == null ? g2 != null : !g.equals(g2)) {
            return false;
        }
        String g3 = this.d.b().b().g();
        String g4 = hVar.d.b().b().g();
        if (g3 == null ? g4 != null : !g3.equals(g4)) {
            return false;
        }
        return this.d.b().c() == hVar.d.b().c();
    }

    public int hashCode() {
        int i = 0;
        String g = this.d.a().g();
        String g2 = this.d.b().b().g();
        long c2 = this.d.b().c();
        int hashCode = ((g != null ? g.hashCode() : 0) + 527) * 31;
        if (g2 != null) {
            i = g2.hashCode();
        }
        return ((i + hashCode) * 31) + ((int) ((c2 >>> 32) ^ c2));
    }

    public int realmGet$id() {
        this.d.a().e();
        return (int) this.d.b().g(this.c.f4631a);
    }

    public float realmGet$level() {
        this.d.a().e();
        return this.d.b().i(this.c.c);
    }

    public int realmGet$screenOn() {
        this.d.a().e();
        return (int) this.d.b().g(this.c.d);
    }

    public long realmGet$timestamp() {
        this.d.a().e();
        return this.d.b().g(this.c.f4632b);
    }

    public String realmGet$triggeredBy() {
        this.d.a().e();
        return this.d.b().l(this.c.e);
    }

    public void realmSet$id(int i) {
        if (!this.d.f()) {
            this.d.a().e();
            throw new RealmException("Primary key field 'id' cannot be changed after object was created.");
        }
    }

    public void realmSet$level(float f) {
        if (!this.d.f()) {
            this.d.a().e();
            this.d.b().a(this.c.c, f);
        } else if (this.d.c()) {
            o b2 = this.d.b();
            b2.b().a(this.c.c, b2.c(), f, true);
        }
    }

    public void realmSet$screenOn(int i) {
        if (!this.d.f()) {
            this.d.a().e();
            this.d.b().a(this.c.d, (long) i);
        } else if (this.d.c()) {
            o b2 = this.d.b();
            b2.b().a(this.c.d, b2.c(), (long) i, true);
        }
    }

    public void realmSet$timestamp(long j) {
        if (!this.d.f()) {
            this.d.a().e();
            this.d.b().a(this.c.f4632b, j);
        } else if (this.d.c()) {
            o b2 = this.d.b();
            b2.b().a(this.c.f4632b, b2.c(), j, true);
        }
    }

    public void realmSet$triggeredBy(String str) {
        if (!this.d.f()) {
            this.d.a().e();
            if (str == null) {
                this.d.b().c(this.c.e);
            } else {
                this.d.b().a(this.c.e, str);
            }
        } else if (this.d.c()) {
            o b2 = this.d.b();
            if (str == null) {
                b2.b().a(this.c.e, b2.c(), true);
            } else {
                b2.b().a(this.c.e, b2.c(), str, true);
            }
        }
    }

    public String toString() {
        if (!bj.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder sb = new StringBuilder("BatterySession = proxy[");
        sb.append("{id:");
        sb.append(realmGet$id());
        sb.append("}");
        sb.append(",");
        sb.append("{timestamp:");
        sb.append(realmGet$timestamp());
        sb.append("}");
        sb.append(",");
        sb.append("{level:");
        sb.append(realmGet$level());
        sb.append("}");
        sb.append(",");
        sb.append("{screenOn:");
        sb.append(realmGet$screenOn());
        sb.append("}");
        sb.append(",");
        sb.append("{triggeredBy:");
        sb.append(realmGet$triggeredBy() != null ? realmGet$triggeredBy() : "null");
        sb.append("}");
        sb.append("]");
        return sb.toString();
    }
}
