package io.realm;

import android.content.Context;
import io.realm.internal.CheckedRow;
import io.realm.internal.OsObjectStore;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.SharedRealm;
import io.realm.internal.SharedRealm.InitializationCallback;
import io.realm.internal.SharedRealm.MigrationCallback;
import io.realm.internal.SharedRealm.SchemaChangedCallback;
import io.realm.internal.Table;
import io.realm.internal.UncheckedRow;
import io.realm.internal.Util;
import io.realm.internal.c;
import io.realm.internal.f;
import io.realm.internal.o;
import io.realm.log.RealmLog;
import java.io.Closeable;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

abstract class e implements Closeable {

    /* renamed from: a reason: collision with root package name */
    static volatile Context f4615a;

    /* renamed from: b reason: collision with root package name */
    static final io.realm.internal.async.a f4616b = io.realm.internal.async.a.a();
    public static final b f = new b();
    final long c;
    protected final be d;
    public SharedRealm e;
    private bc g;
    private boolean h;
    private SchemaChangedCallback i;

    public static final class a {

        /* renamed from: a reason: collision with root package name */
        private e f4623a;

        /* renamed from: b reason: collision with root package name */
        private o f4624b;
        private c c;
        private boolean d;
        private List<String> e;

        /* access modifiers changed from: 0000 */
        public e a() {
            return this.f4623a;
        }

        public void a(e eVar, o oVar, c cVar, boolean z, List<String> list) {
            this.f4623a = eVar;
            this.f4624b = oVar;
            this.c = cVar;
            this.d = z;
            this.e = list;
        }

        public o b() {
            return this.f4624b;
        }

        public c c() {
            return this.c;
        }

        public boolean d() {
            return this.d;
        }

        public List<String> e() {
            return this.e;
        }

        public void f() {
            this.f4623a = null;
            this.f4624b = null;
            this.c = null;
            this.d = false;
            this.e = null;
        }
    }

    static final class b extends ThreadLocal<a> {
        b() {
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public a initialValue() {
            return new a();
        }
    }

    e(bc bcVar, OsSchemaInfo osSchemaInfo) {
        this(bcVar.a(), osSchemaInfo);
        this.g = bcVar;
    }

    e(be beVar, OsSchemaInfo osSchemaInfo) {
        AnonymousClass2 r1 = null;
        this.i = new SchemaChangedCallback() {
            public void onSchemaChanged() {
                bo k = e.this.k();
                if (k != null) {
                    k.b();
                }
            }
        };
        this.c = Thread.currentThread().getId();
        this.d = beVar;
        this.g = null;
        MigrationCallback migrationCallback = (osSchemaInfo == null || beVar.e() == null) ? null : a(beVar.e());
        final io.realm.bb.a i2 = beVar.i();
        if (i2 != null) {
            r1 = new InitializationCallback() {
                public void onInit(SharedRealm sharedRealm) {
                    i2.a(bb.a(sharedRealm));
                }
            };
        }
        this.e = SharedRealm.getInstance(new io.realm.internal.OsRealmConfig.a(beVar).a(true).a(migrationCallback).a(osSchemaInfo).a((InitializationCallback) r1));
        this.h = true;
        this.e.registerSchemaChangedCallback(this.i);
    }

    e(SharedRealm sharedRealm) {
        this.i = new SchemaChangedCallback() {
            public void onSchemaChanged() {
                bo k = e.this.k();
                if (k != null) {
                    k.b();
                }
            }
        };
        this.c = Thread.currentThread().getId();
        this.d = sharedRealm.getConfiguration();
        this.g = null;
        this.e = sharedRealm;
        this.h = false;
    }

    private static MigrationCallback a(final bg bgVar) {
        return new MigrationCallback() {
            public void onMigrationNeeded(SharedRealm sharedRealm, long j, long j2) {
                bg.this.a(aa.a(sharedRealm), j, j2);
            }
        };
    }

    static boolean a(final be beVar) {
        final AtomicBoolean atomicBoolean = new AtomicBoolean(true);
        if (OsObjectStore.a(beVar, (Runnable) new Runnable() {
            public void run() {
                atomicBoolean.set(Util.a(be.this.m(), be.this.a(), be.this.b()));
            }
        })) {
            return atomicBoolean.get();
        }
        throw new IllegalStateException("It's not allowed to delete the file associated with an open Realm. Remember to close() all the instances of the Realm before deleting its file: " + beVar.m());
    }

    /* access modifiers changed from: 0000 */
    public <E extends bh> E a(Class<E> cls, long j, boolean z, List<String> list) {
        UncheckedRow f2 = k().a(cls).f(j);
        return this.d.h().a(cls, this, f2, k().c(cls), z, list);
    }

    /* access modifiers changed from: 0000 */
    public <E extends bh> E a(Class<E> cls, String str, long j) {
        boolean z = str != null;
        Table a2 = z ? k().b(str) : k().a(cls);
        if (z) {
            return new ab(this, j != -1 ? a2.h(j) : f.INSTANCE);
        }
        return this.d.h().a(cls, this, j != -1 ? a2.f(j) : f.INSTANCE, k().c(cls), false, Collections.emptyList());
    }

    /* access modifiers changed from: 0000 */
    public <E extends bh> E a(Class<E> cls, String str, UncheckedRow uncheckedRow) {
        if (str != null) {
            return new ab(this, CheckedRow.a(uncheckedRow));
        }
        return this.d.h().a(cls, this, uncheckedRow, k().c(cls), false, Collections.emptyList());
    }

    public boolean a() {
        e();
        return this.e.isInTransaction();
    }

    public void b() {
        e();
        this.e.beginTransaction();
    }

    public void c() {
        e();
        this.e.commitTransaction();
    }

    public void close() {
        if (this.c != Thread.currentThread().getId()) {
            throw new IllegalStateException("Realm access from incorrect thread. Realm instance can only be closed on the thread it was created.");
        } else if (this.g != null) {
            this.g.a(this);
        } else {
            i();
        }
    }

    public void d() {
        e();
        this.e.cancelTransaction();
    }

    /* access modifiers changed from: protected */
    public void e() {
        if (this.e == null || this.e.isClosed()) {
            throw new IllegalStateException("This Realm instance has already been closed, making it unusable.");
        } else if (this.c != Thread.currentThread().getId()) {
            throw new IllegalStateException("Realm access from incorrect thread. Realm objects can only be accessed on the thread they were created.");
        }
    }

    /* access modifiers changed from: 0000 */
    public void f() {
        if (this.d.s()) {
            throw new IllegalArgumentException("You cannot perform changes to a schema. Please update app and restart.");
        }
    }

    /* access modifiers changed from: protected */
    public void finalize() throws Throwable {
        if (this.h && this.e != null && !this.e.isClosed()) {
            RealmLog.a("Remember to call close() on all Realm instances. Realm %s is being finalized without being closed, this can lead to running out of native memory.", this.d.m());
            if (this.g != null) {
                this.g.b();
            }
        }
        super.finalize();
    }

    public String g() {
        return this.d.m();
    }

    public be h() {
        return this.d;
    }

    /* access modifiers changed from: 0000 */
    public void i() {
        this.g = null;
        if (this.e != null && this.h) {
            this.e.close();
            this.e = null;
        }
    }

    public boolean j() {
        if (this.c == Thread.currentThread().getId()) {
            return this.e == null || this.e.isClosed();
        }
        throw new IllegalStateException("Realm access from incorrect thread. Realm objects can only be accessed on the thread they were created.");
    }

    public abstract bo k();

    /* access modifiers changed from: 0000 */
    public SharedRealm l() {
        return this.e;
    }
}
