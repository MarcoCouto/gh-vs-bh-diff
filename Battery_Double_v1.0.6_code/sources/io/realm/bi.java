package io.realm;

import io.realm.internal.OsList;
import io.realm.internal.OsObjectStore;
import io.realm.internal.m;
import java.util.Locale;

final class bi<T> extends al<T> {
    private final String d;

    bi(e eVar, OsList osList, Class<T> cls, String str) {
        super(eVar, osList, cls);
        this.d = str;
    }

    private <E extends bh> E a(E e) {
        if (e instanceof m) {
            m mVar = (m) e;
            if (mVar instanceof ab) {
                String str = this.d;
                if (mVar.d().a() == this.f4547a) {
                    String c = ((ab) e).c();
                    if (str.equals(c)) {
                        return e;
                    }
                    throw new IllegalArgumentException(String.format(Locale.US, "The object has a different type from list's. Type of the list is '%s', type of object is '%s'.", new Object[]{str, c}));
                } else if (this.f4547a.c == mVar.d().a().c) {
                    throw new IllegalArgumentException("Cannot copy DynamicRealmObject between Realm instances.");
                } else {
                    throw new IllegalStateException("Cannot copy an object to a Realm instance created in another thread.");
                }
            } else if (mVar.d().b() != null && mVar.d().a().g().equals(this.f4547a.g())) {
                if (this.f4547a == mVar.d().a()) {
                    return e;
                }
                throw new IllegalArgumentException("Cannot copy an object from another Realm instance.");
            }
        }
        bb bbVar = (bb) this.f4547a;
        return OsObjectStore.a(bbVar.l(), bbVar.h().h().b(e.getClass())) != null ? bbVar.b(e) : bbVar.a(e);
    }

    private void a(int i) {
        int b2 = b();
        if (i < 0 || b2 < i) {
            throw new IndexOutOfBoundsException("Invalid index " + i + ", size is " + this.f4548b.c());
        }
    }

    public void a(int i, Object obj) {
        a(i);
        this.f4548b.a((long) i, ((m) a((E) (bh) obj)).d().b().c());
    }

    /* access modifiers changed from: protected */
    public void a(Object obj) {
        if (obj == null) {
            throw new IllegalArgumentException("RealmList does not accept null values.");
        } else if (!(obj instanceof bh)) {
            throw new IllegalArgumentException(String.format(Locale.ENGLISH, "Unacceptable value type. Acceptable: %1$s, actual: %2$s .", new Object[]{"java.lang.String", obj.getClass().getName()}));
        }
    }

    public T b(int i) {
        return this.f4547a.a(this.c, this.d, this.f4548b.a((long) i));
    }

    /* access modifiers changed from: protected */
    public void b(int i, Object obj) {
        this.f4548b.b((long) i, ((m) a((E) (bh) obj)).d().b().c());
    }

    public void b(Object obj) {
        this.f4548b.b(((m) a((E) (bh) obj)).d().b().c());
    }

    /* access modifiers changed from: protected */
    public void c(int i) {
        throw new RuntimeException("Should not reach here.");
    }

    /* access modifiers changed from: protected */
    public void d(int i) {
        throw new RuntimeException("Should not reach here.");
    }
}
