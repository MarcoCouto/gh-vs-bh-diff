package io.realm;

import com.hmatalonga.greenhub.models.data.AppPermission;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.c;
import io.realm.internal.m;
import io.realm.internal.o;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class a extends AppPermission implements b, m {

    /* renamed from: a reason: collision with root package name */
    private static final OsObjectSchemaInfo f4517a = e();

    /* renamed from: b reason: collision with root package name */
    private static final List<String> f4518b;
    private C0074a c;
    private ba<AppPermission> d;

    /* renamed from: io.realm.a$a reason: collision with other inner class name */
    static final class C0074a extends c {

        /* renamed from: a reason: collision with root package name */
        long f4519a;

        C0074a(OsSchemaInfo osSchemaInfo) {
            super(1);
            this.f4519a = a("permission", osSchemaInfo.a("AppPermission"));
        }

        /* access modifiers changed from: protected */
        public final void a(c cVar, c cVar2) {
            ((C0074a) cVar2).f4519a = ((C0074a) cVar).f4519a;
        }
    }

    static {
        ArrayList arrayList = new ArrayList(1);
        arrayList.add("permission");
        f4518b = Collections.unmodifiableList(arrayList);
    }

    a() {
        this.d.g();
    }

    public static AppPermission a(bb bbVar, AppPermission appPermission, boolean z, Map<bh, m> map) {
        if ((appPermission instanceof m) && ((m) appPermission).d().a() != null) {
            e a2 = ((m) appPermission).d().a();
            if (a2.c != bbVar.c) {
                throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
            } else if (a2.g().equals(bbVar.g())) {
                return appPermission;
            }
        }
        io.realm.e.a aVar = (io.realm.e.a) e.f.get();
        m mVar = (m) map.get(appPermission);
        return mVar != null ? (AppPermission) mVar : b(bbVar, appPermission, z, map);
    }

    public static C0074a a(OsSchemaInfo osSchemaInfo) {
        return new C0074a(osSchemaInfo);
    }

    public static AppPermission b(bb bbVar, AppPermission appPermission, boolean z, Map<bh, m> map) {
        m mVar = (m) map.get(appPermission);
        if (mVar != null) {
            return (AppPermission) mVar;
        }
        AppPermission appPermission2 = (AppPermission) bbVar.a(AppPermission.class, false, Collections.emptyList());
        map.put(appPermission, (m) appPermission2);
        appPermission2.realmSet$permission(appPermission.realmGet$permission());
        return appPermission2;
    }

    public static OsObjectSchemaInfo b() {
        return f4517a;
    }

    public static String c() {
        return "class_AppPermission";
    }

    private static OsObjectSchemaInfo e() {
        io.realm.internal.OsObjectSchemaInfo.a aVar = new io.realm.internal.OsObjectSchemaInfo.a("AppPermission", 1, 0);
        aVar.a("permission", RealmFieldType.STRING, false, false, false);
        return aVar.a();
    }

    public void a() {
        if (this.d == null) {
            io.realm.e.a aVar = (io.realm.e.a) e.f.get();
            this.c = (C0074a) aVar.c();
            this.d = new ba<>(this);
            this.d.a(aVar.a());
            this.d.a(aVar.b());
            this.d.a(aVar.d());
            this.d.a(aVar.e());
        }
    }

    public ba<?> d() {
        return this.d;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        a aVar = (a) obj;
        String g = this.d.a().g();
        String g2 = aVar.d.a().g();
        if (g == null ? g2 != null : !g.equals(g2)) {
            return false;
        }
        String g3 = this.d.b().b().g();
        String g4 = aVar.d.b().b().g();
        if (g3 == null ? g4 != null : !g3.equals(g4)) {
            return false;
        }
        return this.d.b().c() == aVar.d.b().c();
    }

    public int hashCode() {
        int i = 0;
        String g = this.d.a().g();
        String g2 = this.d.b().b().g();
        long c2 = this.d.b().c();
        int hashCode = ((g != null ? g.hashCode() : 0) + 527) * 31;
        if (g2 != null) {
            i = g2.hashCode();
        }
        return ((i + hashCode) * 31) + ((int) ((c2 >>> 32) ^ c2));
    }

    public String realmGet$permission() {
        this.d.a().e();
        return this.d.b().l(this.c.f4519a);
    }

    public void realmSet$permission(String str) {
        if (!this.d.f()) {
            this.d.a().e();
            if (str == null) {
                this.d.b().c(this.c.f4519a);
            } else {
                this.d.b().a(this.c.f4519a, str);
            }
        } else if (this.d.c()) {
            o b2 = this.d.b();
            if (str == null) {
                b2.b().a(this.c.f4519a, b2.c(), true);
            } else {
                b2.b().a(this.c.f4519a, b2.c(), str, true);
            }
        }
    }

    public String toString() {
        if (!bj.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder sb = new StringBuilder("AppPermission = proxy[");
        sb.append("{permission:");
        sb.append(realmGet$permission() != null ? realmGet$permission() : "null");
        sb.append("}");
        sb.append("]");
        return sb.toString();
    }
}
