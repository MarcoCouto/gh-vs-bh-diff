package io.realm;

import com.hmatalonga.greenhub.models.data.AppPermission;
import com.hmatalonga.greenhub.models.data.AppSignature;
import com.hmatalonga.greenhub.models.data.BatteryDetails;
import com.hmatalonga.greenhub.models.data.BatterySession;
import com.hmatalonga.greenhub.models.data.BatteryUsage;
import com.hmatalonga.greenhub.models.data.CallInfo;
import com.hmatalonga.greenhub.models.data.CallMonth;
import com.hmatalonga.greenhub.models.data.CellInfo;
import com.hmatalonga.greenhub.models.data.CpuStatus;
import com.hmatalonga.greenhub.models.data.Device;
import com.hmatalonga.greenhub.models.data.Feature;
import com.hmatalonga.greenhub.models.data.LocationProvider;
import com.hmatalonga.greenhub.models.data.Message;
import com.hmatalonga.greenhub.models.data.NetworkDetails;
import com.hmatalonga.greenhub.models.data.NetworkStatistics;
import com.hmatalonga.greenhub.models.data.ProcessInfo;
import com.hmatalonga.greenhub.models.data.Sample;
import com.hmatalonga.greenhub.models.data.Settings;
import com.hmatalonga.greenhub.models.data.StorageDetails;
import io.realm.annotations.RealmModule;
import io.realm.e.a;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.c;
import io.realm.internal.m;
import io.realm.internal.n;
import io.realm.internal.o;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@RealmModule
class DefaultRealmModuleMediator extends n {

    /* renamed from: a reason: collision with root package name */
    private static final Set<Class<? extends bh>> f4516a;

    static {
        HashSet hashSet = new HashSet(19);
        hashSet.add(NetworkDetails.class);
        hashSet.add(BatteryDetails.class);
        hashSet.add(Sample.class);
        hashSet.add(StorageDetails.class);
        hashSet.add(CpuStatus.class);
        hashSet.add(LocationProvider.class);
        hashSet.add(AppPermission.class);
        hashSet.add(BatteryUsage.class);
        hashSet.add(Message.class);
        hashSet.add(ProcessInfo.class);
        hashSet.add(BatterySession.class);
        hashSet.add(Feature.class);
        hashSet.add(CellInfo.class);
        hashSet.add(AppSignature.class);
        hashSet.add(Settings.class);
        hashSet.add(Device.class);
        hashSet.add(CallInfo.class);
        hashSet.add(CallMonth.class);
        hashSet.add(NetworkStatistics.class);
        f4516a = Collections.unmodifiableSet(hashSet);
    }

    DefaultRealmModuleMediator() {
    }

    public <E extends bh> E a(bb bbVar, E e, boolean z, Map<bh, m> map) {
        Class cls = e instanceof m ? e.getClass().getSuperclass() : e.getClass();
        if (cls.equals(NetworkDetails.class)) {
            return (bh) cls.cast(aq.a(bbVar, (NetworkDetails) e, z, map));
        }
        if (cls.equals(BatteryDetails.class)) {
            return (bh) cls.cast(f.a(bbVar, (BatteryDetails) e, z, map));
        }
        if (cls.equals(Sample.class)) {
            return (bh) cls.cast(bp.a(bbVar, (Sample) e, z, map));
        }
        if (cls.equals(StorageDetails.class)) {
            return (bh) cls.cast(bv.a(bbVar, (StorageDetails) e, z, map));
        }
        if (cls.equals(CpuStatus.class)) {
            return (bh) cls.cast(u.a(bbVar, (CpuStatus) e, z, map));
        }
        if (cls.equals(LocationProvider.class)) {
            return (bh) cls.cast(ai.a(bbVar, (LocationProvider) e, z, map));
        }
        if (cls.equals(AppPermission.class)) {
            return (bh) cls.cast(a.a(bbVar, (AppPermission) e, z, map));
        }
        if (cls.equals(BatteryUsage.class)) {
            return (bh) cls.cast(j.a(bbVar, (BatteryUsage) e, z, map));
        }
        if (cls.equals(Message.class)) {
            return (bh) cls.cast(am.a(bbVar, (Message) e, z, map));
        }
        if (cls.equals(ProcessInfo.class)) {
            return (bh) cls.cast(ay.a(bbVar, (ProcessInfo) e, z, map));
        }
        if (cls.equals(BatterySession.class)) {
            return (bh) cls.cast(h.a(bbVar, (BatterySession) e, z, map));
        }
        if (cls.equals(Feature.class)) {
            return (bh) cls.cast(ac.a(bbVar, (Feature) e, z, map));
        }
        if (cls.equals(CellInfo.class)) {
            return (bh) cls.cast(s.a(bbVar, (CellInfo) e, z, map));
        }
        if (cls.equals(AppSignature.class)) {
            return (bh) cls.cast(c.a(bbVar, (AppSignature) e, z, map));
        }
        if (cls.equals(Settings.class)) {
            return (bh) cls.cast(bs.a(bbVar, (Settings) e, z, map));
        }
        if (cls.equals(Device.class)) {
            return (bh) cls.cast(x.a(bbVar, (Device) e, z, map));
        }
        if (cls.equals(CallInfo.class)) {
            return (bh) cls.cast(n.a(bbVar, (CallInfo) e, z, map));
        }
        if (cls.equals(CallMonth.class)) {
            return (bh) cls.cast(p.a(bbVar, (CallMonth) e, z, map));
        }
        if (cls.equals(NetworkStatistics.class)) {
            return (bh) cls.cast(as.a(bbVar, (NetworkStatistics) e, z, map));
        }
        throw d(cls);
    }

    public <E extends bh> E a(Class<E> cls, Object obj, o oVar, c cVar, boolean z, List<String> list) {
        E e;
        a aVar = (a) e.f.get();
        try {
            aVar.a((e) obj, oVar, cVar, z, list);
            c(cls);
            if (cls.equals(NetworkDetails.class)) {
                e = (bh) cls.cast(new aq());
            } else if (cls.equals(BatteryDetails.class)) {
                e = (bh) cls.cast(new f());
                aVar.f();
            } else if (cls.equals(Sample.class)) {
                e = (bh) cls.cast(new bp());
                aVar.f();
            } else if (cls.equals(StorageDetails.class)) {
                e = (bh) cls.cast(new bv());
                aVar.f();
            } else if (cls.equals(CpuStatus.class)) {
                e = (bh) cls.cast(new u());
                aVar.f();
            } else if (cls.equals(LocationProvider.class)) {
                e = (bh) cls.cast(new ai());
                aVar.f();
            } else if (cls.equals(AppPermission.class)) {
                e = (bh) cls.cast(new a());
                aVar.f();
            } else if (cls.equals(BatteryUsage.class)) {
                e = (bh) cls.cast(new j());
                aVar.f();
            } else if (cls.equals(Message.class)) {
                e = (bh) cls.cast(new am());
                aVar.f();
            } else if (cls.equals(ProcessInfo.class)) {
                e = (bh) cls.cast(new ay());
                aVar.f();
            } else if (cls.equals(BatterySession.class)) {
                e = (bh) cls.cast(new h());
                aVar.f();
            } else if (cls.equals(Feature.class)) {
                e = (bh) cls.cast(new ac());
                aVar.f();
            } else if (cls.equals(CellInfo.class)) {
                e = (bh) cls.cast(new s());
                aVar.f();
            } else if (cls.equals(AppSignature.class)) {
                e = (bh) cls.cast(new c());
                aVar.f();
            } else if (cls.equals(Settings.class)) {
                e = (bh) cls.cast(new bs());
                aVar.f();
            } else if (cls.equals(Device.class)) {
                e = (bh) cls.cast(new x());
                aVar.f();
            } else if (cls.equals(CallInfo.class)) {
                e = (bh) cls.cast(new n());
                aVar.f();
            } else if (cls.equals(CallMonth.class)) {
                e = (bh) cls.cast(new p());
                aVar.f();
            } else if (cls.equals(NetworkStatistics.class)) {
                e = (bh) cls.cast(new as());
                aVar.f();
            } else {
                throw d(cls);
            }
            return e;
        } finally {
            aVar.f();
        }
    }

    public c a(Class<? extends bh> cls, OsSchemaInfo osSchemaInfo) {
        c(cls);
        if (cls.equals(NetworkDetails.class)) {
            return aq.a(osSchemaInfo);
        }
        if (cls.equals(BatteryDetails.class)) {
            return f.a(osSchemaInfo);
        }
        if (cls.equals(Sample.class)) {
            return bp.a(osSchemaInfo);
        }
        if (cls.equals(StorageDetails.class)) {
            return bv.a(osSchemaInfo);
        }
        if (cls.equals(CpuStatus.class)) {
            return u.a(osSchemaInfo);
        }
        if (cls.equals(LocationProvider.class)) {
            return ai.a(osSchemaInfo);
        }
        if (cls.equals(AppPermission.class)) {
            return a.a(osSchemaInfo);
        }
        if (cls.equals(BatteryUsage.class)) {
            return j.a(osSchemaInfo);
        }
        if (cls.equals(Message.class)) {
            return am.a(osSchemaInfo);
        }
        if (cls.equals(ProcessInfo.class)) {
            return ay.a(osSchemaInfo);
        }
        if (cls.equals(BatterySession.class)) {
            return h.a(osSchemaInfo);
        }
        if (cls.equals(Feature.class)) {
            return ac.a(osSchemaInfo);
        }
        if (cls.equals(CellInfo.class)) {
            return s.a(osSchemaInfo);
        }
        if (cls.equals(AppSignature.class)) {
            return c.a(osSchemaInfo);
        }
        if (cls.equals(Settings.class)) {
            return bs.a(osSchemaInfo);
        }
        if (cls.equals(Device.class)) {
            return x.a(osSchemaInfo);
        }
        if (cls.equals(CallInfo.class)) {
            return n.a(osSchemaInfo);
        }
        if (cls.equals(CallMonth.class)) {
            return p.a(osSchemaInfo);
        }
        if (cls.equals(NetworkStatistics.class)) {
            return as.a(osSchemaInfo);
        }
        throw d(cls);
    }

    public String a(Class<? extends bh> cls) {
        c(cls);
        if (cls.equals(NetworkDetails.class)) {
            return aq.c();
        }
        if (cls.equals(BatteryDetails.class)) {
            return f.c();
        }
        if (cls.equals(Sample.class)) {
            return bp.c();
        }
        if (cls.equals(StorageDetails.class)) {
            return bv.c();
        }
        if (cls.equals(CpuStatus.class)) {
            return u.c();
        }
        if (cls.equals(LocationProvider.class)) {
            return ai.c();
        }
        if (cls.equals(AppPermission.class)) {
            return a.c();
        }
        if (cls.equals(BatteryUsage.class)) {
            return j.c();
        }
        if (cls.equals(Message.class)) {
            return am.c();
        }
        if (cls.equals(ProcessInfo.class)) {
            return ay.c();
        }
        if (cls.equals(BatterySession.class)) {
            return h.c();
        }
        if (cls.equals(Feature.class)) {
            return ac.c();
        }
        if (cls.equals(CellInfo.class)) {
            return s.c();
        }
        if (cls.equals(AppSignature.class)) {
            return c.c();
        }
        if (cls.equals(Settings.class)) {
            return bs.c();
        }
        if (cls.equals(Device.class)) {
            return x.c();
        }
        if (cls.equals(CallInfo.class)) {
            return n.c();
        }
        if (cls.equals(CallMonth.class)) {
            return p.c();
        }
        if (cls.equals(NetworkStatistics.class)) {
            return as.c();
        }
        throw d(cls);
    }

    public Map<Class<? extends bh>, OsObjectSchemaInfo> a() {
        HashMap hashMap = new HashMap(19);
        hashMap.put(NetworkDetails.class, aq.b());
        hashMap.put(BatteryDetails.class, f.b());
        hashMap.put(Sample.class, bp.b());
        hashMap.put(StorageDetails.class, bv.b());
        hashMap.put(CpuStatus.class, u.b());
        hashMap.put(LocationProvider.class, ai.b());
        hashMap.put(AppPermission.class, a.b());
        hashMap.put(BatteryUsage.class, j.b());
        hashMap.put(Message.class, am.b());
        hashMap.put(ProcessInfo.class, ay.b());
        hashMap.put(BatterySession.class, h.b());
        hashMap.put(Feature.class, ac.b());
        hashMap.put(CellInfo.class, s.b());
        hashMap.put(AppSignature.class, c.b());
        hashMap.put(Settings.class, bs.b());
        hashMap.put(Device.class, x.b());
        hashMap.put(CallInfo.class, n.b());
        hashMap.put(CallMonth.class, p.b());
        hashMap.put(NetworkStatistics.class, as.b());
        return hashMap;
    }

    public Set<Class<? extends bh>> b() {
        return f4516a;
    }

    public boolean c() {
        return true;
    }
}
