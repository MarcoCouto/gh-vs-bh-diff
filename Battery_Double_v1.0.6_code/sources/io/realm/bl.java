package io.realm;

import io.realm.internal.Table;
import io.realm.internal.c;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public abstract class bl {

    /* renamed from: a reason: collision with root package name */
    static final Map<Class<?>, b> f4588a;

    /* renamed from: b reason: collision with root package name */
    static final Map<Class<?>, b> f4589b;
    final bo c;
    final e d;
    final Table e;
    private final c f;

    static final class a extends c {

        /* renamed from: a reason: collision with root package name */
        private final Table f4590a;

        a(Table table) {
            super((c) null, false);
            this.f4590a = table;
        }

        public io.realm.internal.c.a a(String str) {
            throw new UnsupportedOperationException("DynamicColumnIndices do not support 'getColumnDetails'");
        }

        public void a(c cVar) {
            throw new UnsupportedOperationException("DynamicColumnIndices cannot be copied");
        }

        /* access modifiers changed from: protected */
        public void a(c cVar, c cVar2) {
            throw new UnsupportedOperationException("DynamicColumnIndices cannot copy");
        }
    }

    static final class b {

        /* renamed from: a reason: collision with root package name */
        final RealmFieldType f4591a;

        /* renamed from: b reason: collision with root package name */
        final RealmFieldType f4592b;
        final boolean c;

        b(RealmFieldType realmFieldType, RealmFieldType realmFieldType2, boolean z) {
            this.f4591a = realmFieldType;
            this.f4592b = realmFieldType2;
            this.c = z;
        }
    }

    static {
        HashMap hashMap = new HashMap();
        hashMap.put(String.class, new b(RealmFieldType.STRING, RealmFieldType.STRING_LIST, true));
        hashMap.put(Short.TYPE, new b(RealmFieldType.INTEGER, RealmFieldType.INTEGER_LIST, false));
        hashMap.put(Short.class, new b(RealmFieldType.INTEGER, RealmFieldType.INTEGER_LIST, true));
        hashMap.put(Integer.TYPE, new b(RealmFieldType.INTEGER, RealmFieldType.INTEGER_LIST, false));
        hashMap.put(Integer.class, new b(RealmFieldType.INTEGER, RealmFieldType.INTEGER_LIST, true));
        hashMap.put(Long.TYPE, new b(RealmFieldType.INTEGER, RealmFieldType.INTEGER_LIST, false));
        hashMap.put(Long.class, new b(RealmFieldType.INTEGER, RealmFieldType.INTEGER_LIST, true));
        hashMap.put(Float.TYPE, new b(RealmFieldType.FLOAT, RealmFieldType.FLOAT_LIST, false));
        hashMap.put(Float.class, new b(RealmFieldType.FLOAT, RealmFieldType.FLOAT_LIST, true));
        hashMap.put(Double.TYPE, new b(RealmFieldType.DOUBLE, RealmFieldType.DOUBLE_LIST, false));
        hashMap.put(Double.class, new b(RealmFieldType.DOUBLE, RealmFieldType.DOUBLE_LIST, true));
        hashMap.put(Boolean.TYPE, new b(RealmFieldType.BOOLEAN, RealmFieldType.BOOLEAN_LIST, false));
        hashMap.put(Boolean.class, new b(RealmFieldType.BOOLEAN, RealmFieldType.BOOLEAN_LIST, true));
        hashMap.put(Byte.TYPE, new b(RealmFieldType.INTEGER, RealmFieldType.INTEGER_LIST, false));
        hashMap.put(Byte.class, new b(RealmFieldType.INTEGER, RealmFieldType.INTEGER_LIST, true));
        hashMap.put(byte[].class, new b(RealmFieldType.BINARY, RealmFieldType.BINARY_LIST, true));
        hashMap.put(Date.class, new b(RealmFieldType.DATE, RealmFieldType.DATE_LIST, true));
        f4588a = Collections.unmodifiableMap(hashMap);
        HashMap hashMap2 = new HashMap();
        hashMap2.put(bj.class, new b(RealmFieldType.OBJECT, null, false));
        hashMap2.put(bf.class, new b(RealmFieldType.LIST, null, false));
        f4589b = Collections.unmodifiableMap(hashMap2);
    }

    bl(e eVar, bo boVar, Table table, c cVar) {
        this.c = boVar;
        this.d = eVar;
        this.e = table;
        this.f = cVar;
    }

    private br c() {
        return new br(this.c);
    }

    static void e(String str) {
        if (str == null || str.isEmpty()) {
            throw new IllegalArgumentException("Field name can not be null or empty");
        } else if (str.contains(".")) {
            throw new IllegalArgumentException("Field name can not contain '.'");
        } else if (str.length() > 63) {
            throw new IllegalArgumentException("Field name is currently limited to max 63 characters.");
        }
    }

    public abstract bl a(String str);

    public abstract bl a(String str, Class<?> cls, ae... aeVarArr);

    /* access modifiers changed from: protected */
    public final io.realm.internal.a.c a(String str, RealmFieldType... realmFieldTypeArr) {
        return io.realm.internal.a.c.a((io.realm.internal.a.c.a) c(), b(), str, realmFieldTypeArr);
    }

    public String a() {
        return this.e.h();
    }

    /* access modifiers changed from: 0000 */
    public Table b() {
        return this.e;
    }

    public boolean d(String str) {
        return this.e.a(str) != -1;
    }

    /* access modifiers changed from: 0000 */
    public void f(String str) {
        if (this.e.a(str) == -1) {
            throw new IllegalArgumentException("Field name doesn't exist on object '" + a() + "': " + str);
        }
    }

    /* access modifiers changed from: 0000 */
    public long g(String str) {
        long a2 = this.e.a(str);
        if (a2 != -1) {
            return a2;
        }
        throw new IllegalArgumentException(String.format(Locale.US, "Field name '%s' does not exist on schema for '%s'", new Object[]{str, a()}));
    }
}
