package io.realm;

import com.hmatalonga.greenhub.models.data.BatteryDetails;

public interface k {
    BatteryDetails realmGet$details();

    int realmGet$id();

    float realmGet$level();

    int realmGet$screenOn();

    String realmGet$state();

    long realmGet$timestamp();

    String realmGet$triggeredBy();

    void realmSet$details(BatteryDetails batteryDetails);

    void realmSet$level(float f);

    void realmSet$screenOn(int i);

    void realmSet$state(String str);

    void realmSet$timestamp(long j);

    void realmSet$triggeredBy(String str);
}
