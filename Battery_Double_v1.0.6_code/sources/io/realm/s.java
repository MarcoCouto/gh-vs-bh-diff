package io.realm;

import com.hmatalonga.greenhub.models.data.CellInfo;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.c;
import io.realm.internal.m;
import io.realm.internal.o;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class s extends CellInfo implements m, t {

    /* renamed from: a reason: collision with root package name */
    private static final OsObjectSchemaInfo f4722a = e();

    /* renamed from: b reason: collision with root package name */
    private static final List<String> f4723b;
    private a c;
    private ba<CellInfo> d;

    static final class a extends c {

        /* renamed from: a reason: collision with root package name */
        long f4724a;

        /* renamed from: b reason: collision with root package name */
        long f4725b;
        long c;
        long d;
        long e;

        a(OsSchemaInfo osSchemaInfo) {
            super(5);
            OsObjectSchemaInfo a2 = osSchemaInfo.a("CellInfo");
            this.f4724a = a("mcc", a2);
            this.f4725b = a("mnc", a2);
            this.c = a("lac", a2);
            this.d = a("cid", a2);
            this.e = a("radioType", a2);
        }

        /* access modifiers changed from: protected */
        public final void a(c cVar, c cVar2) {
            a aVar = (a) cVar;
            a aVar2 = (a) cVar2;
            aVar2.f4724a = aVar.f4724a;
            aVar2.f4725b = aVar.f4725b;
            aVar2.c = aVar.c;
            aVar2.d = aVar.d;
            aVar2.e = aVar.e;
        }
    }

    static {
        ArrayList arrayList = new ArrayList(5);
        arrayList.add("mcc");
        arrayList.add("mnc");
        arrayList.add("lac");
        arrayList.add("cid");
        arrayList.add("radioType");
        f4723b = Collections.unmodifiableList(arrayList);
    }

    s() {
        this.d.g();
    }

    public static CellInfo a(bb bbVar, CellInfo cellInfo, boolean z, Map<bh, m> map) {
        if ((cellInfo instanceof m) && ((m) cellInfo).d().a() != null) {
            e a2 = ((m) cellInfo).d().a();
            if (a2.c != bbVar.c) {
                throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
            } else if (a2.g().equals(bbVar.g())) {
                return cellInfo;
            }
        }
        io.realm.e.a aVar = (io.realm.e.a) e.f.get();
        m mVar = (m) map.get(cellInfo);
        return mVar != null ? (CellInfo) mVar : b(bbVar, cellInfo, z, map);
    }

    public static a a(OsSchemaInfo osSchemaInfo) {
        return new a(osSchemaInfo);
    }

    public static CellInfo b(bb bbVar, CellInfo cellInfo, boolean z, Map<bh, m> map) {
        m mVar = (m) map.get(cellInfo);
        if (mVar != null) {
            return (CellInfo) mVar;
        }
        CellInfo cellInfo2 = (CellInfo) bbVar.a(CellInfo.class, false, Collections.emptyList());
        map.put(cellInfo, (m) cellInfo2);
        t tVar = cellInfo;
        t tVar2 = cellInfo2;
        tVar2.realmSet$mcc(tVar.realmGet$mcc());
        tVar2.realmSet$mnc(tVar.realmGet$mnc());
        tVar2.realmSet$lac(tVar.realmGet$lac());
        tVar2.realmSet$cid(tVar.realmGet$cid());
        tVar2.realmSet$radioType(tVar.realmGet$radioType());
        return cellInfo2;
    }

    public static OsObjectSchemaInfo b() {
        return f4722a;
    }

    public static String c() {
        return "class_CellInfo";
    }

    private static OsObjectSchemaInfo e() {
        io.realm.internal.OsObjectSchemaInfo.a aVar = new io.realm.internal.OsObjectSchemaInfo.a("CellInfo", 5, 0);
        aVar.a("mcc", RealmFieldType.INTEGER, false, false, true);
        aVar.a("mnc", RealmFieldType.INTEGER, false, false, true);
        aVar.a("lac", RealmFieldType.INTEGER, false, false, true);
        aVar.a("cid", RealmFieldType.INTEGER, false, false, true);
        aVar.a("radioType", RealmFieldType.STRING, false, false, false);
        return aVar.a();
    }

    public void a() {
        if (this.d == null) {
            io.realm.e.a aVar = (io.realm.e.a) e.f.get();
            this.c = (a) aVar.c();
            this.d = new ba<>(this);
            this.d.a(aVar.a());
            this.d.a(aVar.b());
            this.d.a(aVar.d());
            this.d.a(aVar.e());
        }
    }

    public ba<?> d() {
        return this.d;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        s sVar = (s) obj;
        String g = this.d.a().g();
        String g2 = sVar.d.a().g();
        if (g == null ? g2 != null : !g.equals(g2)) {
            return false;
        }
        String g3 = this.d.b().b().g();
        String g4 = sVar.d.b().b().g();
        if (g3 == null ? g4 != null : !g3.equals(g4)) {
            return false;
        }
        return this.d.b().c() == sVar.d.b().c();
    }

    public int hashCode() {
        int i = 0;
        String g = this.d.a().g();
        String g2 = this.d.b().b().g();
        long c2 = this.d.b().c();
        int hashCode = ((g != null ? g.hashCode() : 0) + 527) * 31;
        if (g2 != null) {
            i = g2.hashCode();
        }
        return ((i + hashCode) * 31) + ((int) ((c2 >>> 32) ^ c2));
    }

    public int realmGet$cid() {
        this.d.a().e();
        return (int) this.d.b().g(this.c.d);
    }

    public int realmGet$lac() {
        this.d.a().e();
        return (int) this.d.b().g(this.c.c);
    }

    public int realmGet$mcc() {
        this.d.a().e();
        return (int) this.d.b().g(this.c.f4724a);
    }

    public int realmGet$mnc() {
        this.d.a().e();
        return (int) this.d.b().g(this.c.f4725b);
    }

    public String realmGet$radioType() {
        this.d.a().e();
        return this.d.b().l(this.c.e);
    }

    public void realmSet$cid(int i) {
        if (!this.d.f()) {
            this.d.a().e();
            this.d.b().a(this.c.d, (long) i);
        } else if (this.d.c()) {
            o b2 = this.d.b();
            b2.b().a(this.c.d, b2.c(), (long) i, true);
        }
    }

    public void realmSet$lac(int i) {
        if (!this.d.f()) {
            this.d.a().e();
            this.d.b().a(this.c.c, (long) i);
        } else if (this.d.c()) {
            o b2 = this.d.b();
            b2.b().a(this.c.c, b2.c(), (long) i, true);
        }
    }

    public void realmSet$mcc(int i) {
        if (!this.d.f()) {
            this.d.a().e();
            this.d.b().a(this.c.f4724a, (long) i);
        } else if (this.d.c()) {
            o b2 = this.d.b();
            b2.b().a(this.c.f4724a, b2.c(), (long) i, true);
        }
    }

    public void realmSet$mnc(int i) {
        if (!this.d.f()) {
            this.d.a().e();
            this.d.b().a(this.c.f4725b, (long) i);
        } else if (this.d.c()) {
            o b2 = this.d.b();
            b2.b().a(this.c.f4725b, b2.c(), (long) i, true);
        }
    }

    public void realmSet$radioType(String str) {
        if (!this.d.f()) {
            this.d.a().e();
            if (str == null) {
                this.d.b().c(this.c.e);
            } else {
                this.d.b().a(this.c.e, str);
            }
        } else if (this.d.c()) {
            o b2 = this.d.b();
            if (str == null) {
                b2.b().a(this.c.e, b2.c(), true);
            } else {
                b2.b().a(this.c.e, b2.c(), str, true);
            }
        }
    }

    public String toString() {
        if (!bj.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder sb = new StringBuilder("CellInfo = proxy[");
        sb.append("{mcc:");
        sb.append(realmGet$mcc());
        sb.append("}");
        sb.append(",");
        sb.append("{mnc:");
        sb.append(realmGet$mnc());
        sb.append("}");
        sb.append(",");
        sb.append("{lac:");
        sb.append(realmGet$lac());
        sb.append("}");
        sb.append(",");
        sb.append("{cid:");
        sb.append(realmGet$cid());
        sb.append("}");
        sb.append(",");
        sb.append("{radioType:");
        sb.append(realmGet$radioType() != null ? realmGet$radioType() : "null");
        sb.append("}");
        sb.append("]");
        return sb.toString();
    }
}
