package io.realm;

import com.hmatalonga.greenhub.models.data.BatteryDetails;
import com.hmatalonga.greenhub.models.data.BatteryUsage;
import io.realm.exceptions.RealmException;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.Table;
import io.realm.internal.c;
import io.realm.internal.m;
import io.realm.internal.o;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class j extends BatteryUsage implements m, k {

    /* renamed from: a reason: collision with root package name */
    private static final OsObjectSchemaInfo f4707a = e();

    /* renamed from: b reason: collision with root package name */
    private static final List<String> f4708b;
    private a c;
    private ba<BatteryUsage> d;

    static final class a extends c {

        /* renamed from: a reason: collision with root package name */
        long f4709a;

        /* renamed from: b reason: collision with root package name */
        long f4710b;
        long c;
        long d;
        long e;
        long f;
        long g;

        a(OsSchemaInfo osSchemaInfo) {
            super(7);
            OsObjectSchemaInfo a2 = osSchemaInfo.a("BatteryUsage");
            this.f4709a = a("id", a2);
            this.f4710b = a("timestamp", a2);
            this.c = a("state", a2);
            this.d = a("level", a2);
            this.e = a("screenOn", a2);
            this.f = a("triggeredBy", a2);
            this.g = a("details", a2);
        }

        /* access modifiers changed from: protected */
        public final void a(c cVar, c cVar2) {
            a aVar = (a) cVar;
            a aVar2 = (a) cVar2;
            aVar2.f4709a = aVar.f4709a;
            aVar2.f4710b = aVar.f4710b;
            aVar2.c = aVar.c;
            aVar2.d = aVar.d;
            aVar2.e = aVar.e;
            aVar2.f = aVar.f;
            aVar2.g = aVar.g;
        }
    }

    static {
        ArrayList arrayList = new ArrayList(7);
        arrayList.add("id");
        arrayList.add("timestamp");
        arrayList.add("state");
        arrayList.add("level");
        arrayList.add("screenOn");
        arrayList.add("triggeredBy");
        arrayList.add("details");
        f4708b = Collections.unmodifiableList(arrayList);
    }

    j() {
        this.d.g();
    }

    static BatteryUsage a(bb bbVar, BatteryUsage batteryUsage, BatteryUsage batteryUsage2, Map<bh, m> map) {
        k kVar = batteryUsage;
        k kVar2 = batteryUsage2;
        kVar.realmSet$timestamp(kVar2.realmGet$timestamp());
        kVar.realmSet$state(kVar2.realmGet$state());
        kVar.realmSet$level(kVar2.realmGet$level());
        kVar.realmSet$screenOn(kVar2.realmGet$screenOn());
        kVar.realmSet$triggeredBy(kVar2.realmGet$triggeredBy());
        BatteryDetails realmGet$details = kVar2.realmGet$details();
        if (realmGet$details == null) {
            kVar.realmSet$details(null);
        } else {
            BatteryDetails batteryDetails = (BatteryDetails) map.get(realmGet$details);
            if (batteryDetails != null) {
                kVar.realmSet$details(batteryDetails);
            } else {
                kVar.realmSet$details(f.a(bbVar, realmGet$details, true, map));
            }
        }
        return batteryUsage;
    }

    /* JADX INFO: finally extract failed */
    public static BatteryUsage a(bb bbVar, BatteryUsage batteryUsage, boolean z, Map<bh, m> map) {
        boolean z2;
        j jVar;
        if ((batteryUsage instanceof m) && ((m) batteryUsage).d().a() != null) {
            e a2 = ((m) batteryUsage).d().a();
            if (a2.c != bbVar.c) {
                throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
            } else if (a2.g().equals(bbVar.g())) {
                return batteryUsage;
            }
        }
        io.realm.e.a aVar = (io.realm.e.a) e.f.get();
        m mVar = (m) map.get(batteryUsage);
        if (mVar != null) {
            return (BatteryUsage) mVar;
        }
        if (z) {
            Table b2 = bbVar.b(BatteryUsage.class);
            long a3 = b2.a(((a) bbVar.k().c(BatteryUsage.class)).f4709a, (long) batteryUsage.realmGet$id());
            if (a3 == -1) {
                z2 = false;
                jVar = null;
            } else {
                try {
                    aVar.a(bbVar, b2.f(a3), bbVar.k().c(BatteryUsage.class), false, Collections.emptyList());
                    jVar = new j();
                    map.put(batteryUsage, jVar);
                    aVar.f();
                    z2 = z;
                } catch (Throwable th) {
                    aVar.f();
                    throw th;
                }
            }
        } else {
            z2 = z;
            jVar = null;
        }
        return z2 ? a(bbVar, (BatteryUsage) jVar, batteryUsage, map) : b(bbVar, batteryUsage, z, map);
    }

    public static a a(OsSchemaInfo osSchemaInfo) {
        return new a(osSchemaInfo);
    }

    public static BatteryUsage b(bb bbVar, BatteryUsage batteryUsage, boolean z, Map<bh, m> map) {
        m mVar = (m) map.get(batteryUsage);
        if (mVar != null) {
            return (BatteryUsage) mVar;
        }
        BatteryUsage batteryUsage2 = (BatteryUsage) bbVar.a(BatteryUsage.class, Integer.valueOf(batteryUsage.realmGet$id()), false, Collections.emptyList());
        map.put(batteryUsage, (m) batteryUsage2);
        k kVar = batteryUsage;
        k kVar2 = batteryUsage2;
        kVar2.realmSet$timestamp(kVar.realmGet$timestamp());
        kVar2.realmSet$state(kVar.realmGet$state());
        kVar2.realmSet$level(kVar.realmGet$level());
        kVar2.realmSet$screenOn(kVar.realmGet$screenOn());
        kVar2.realmSet$triggeredBy(kVar.realmGet$triggeredBy());
        BatteryDetails realmGet$details = kVar.realmGet$details();
        if (realmGet$details == null) {
            kVar2.realmSet$details(null);
            return batteryUsage2;
        }
        BatteryDetails batteryDetails = (BatteryDetails) map.get(realmGet$details);
        if (batteryDetails != null) {
            kVar2.realmSet$details(batteryDetails);
            return batteryUsage2;
        }
        kVar2.realmSet$details(f.a(bbVar, realmGet$details, z, map));
        return batteryUsage2;
    }

    public static OsObjectSchemaInfo b() {
        return f4707a;
    }

    public static String c() {
        return "class_BatteryUsage";
    }

    private static OsObjectSchemaInfo e() {
        io.realm.internal.OsObjectSchemaInfo.a aVar = new io.realm.internal.OsObjectSchemaInfo.a("BatteryUsage", 7, 0);
        aVar.a("id", RealmFieldType.INTEGER, true, true, true);
        aVar.a("timestamp", RealmFieldType.INTEGER, false, true, true);
        aVar.a("state", RealmFieldType.STRING, false, false, false);
        aVar.a("level", RealmFieldType.FLOAT, false, false, true);
        aVar.a("screenOn", RealmFieldType.INTEGER, false, false, true);
        aVar.a("triggeredBy", RealmFieldType.STRING, false, false, false);
        aVar.a("details", RealmFieldType.OBJECT, "BatteryDetails");
        return aVar.a();
    }

    public void a() {
        if (this.d == null) {
            io.realm.e.a aVar = (io.realm.e.a) e.f.get();
            this.c = (a) aVar.c();
            this.d = new ba<>(this);
            this.d.a(aVar.a());
            this.d.a(aVar.b());
            this.d.a(aVar.d());
            this.d.a(aVar.e());
        }
    }

    public ba<?> d() {
        return this.d;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        j jVar = (j) obj;
        String g = this.d.a().g();
        String g2 = jVar.d.a().g();
        if (g == null ? g2 != null : !g.equals(g2)) {
            return false;
        }
        String g3 = this.d.b().b().g();
        String g4 = jVar.d.b().b().g();
        if (g3 == null ? g4 != null : !g3.equals(g4)) {
            return false;
        }
        return this.d.b().c() == jVar.d.b().c();
    }

    public int hashCode() {
        int i = 0;
        String g = this.d.a().g();
        String g2 = this.d.b().b().g();
        long c2 = this.d.b().c();
        int hashCode = ((g != null ? g.hashCode() : 0) + 527) * 31;
        if (g2 != null) {
            i = g2.hashCode();
        }
        return ((i + hashCode) * 31) + ((int) ((c2 >>> 32) ^ c2));
    }

    public BatteryDetails realmGet$details() {
        this.d.a().e();
        if (this.d.b().a(this.c.g)) {
            return null;
        }
        return (BatteryDetails) this.d.a().a(BatteryDetails.class, this.d.b().n(this.c.g), false, Collections.emptyList());
    }

    public int realmGet$id() {
        this.d.a().e();
        return (int) this.d.b().g(this.c.f4709a);
    }

    public float realmGet$level() {
        this.d.a().e();
        return this.d.b().i(this.c.d);
    }

    public int realmGet$screenOn() {
        this.d.a().e();
        return (int) this.d.b().g(this.c.e);
    }

    public String realmGet$state() {
        this.d.a().e();
        return this.d.b().l(this.c.c);
    }

    public long realmGet$timestamp() {
        this.d.a().e();
        return this.d.b().g(this.c.f4710b);
    }

    public String realmGet$triggeredBy() {
        this.d.a().e();
        return this.d.b().l(this.c.f);
    }

    public void realmSet$details(BatteryDetails batteryDetails) {
        if (!this.d.f()) {
            this.d.a().e();
            if (batteryDetails == null) {
                this.d.b().o(this.c.g);
                return;
            }
            this.d.a((bh) batteryDetails);
            this.d.b().b(this.c.g, ((m) batteryDetails).d().b().c());
        } else if (this.d.c() && !this.d.d().contains("details")) {
            BatteryDetails batteryDetails2 = (batteryDetails == null || bj.isManaged(batteryDetails)) ? batteryDetails : (BatteryDetails) ((bb) this.d.a()).a(batteryDetails);
            o b2 = this.d.b();
            if (batteryDetails2 == null) {
                b2.o(this.c.g);
                return;
            }
            this.d.a((bh) batteryDetails2);
            b2.b().b(this.c.g, b2.c(), ((m) batteryDetails2).d().b().c(), true);
        }
    }

    public void realmSet$id(int i) {
        if (!this.d.f()) {
            this.d.a().e();
            throw new RealmException("Primary key field 'id' cannot be changed after object was created.");
        }
    }

    public void realmSet$level(float f) {
        if (!this.d.f()) {
            this.d.a().e();
            this.d.b().a(this.c.d, f);
        } else if (this.d.c()) {
            o b2 = this.d.b();
            b2.b().a(this.c.d, b2.c(), f, true);
        }
    }

    public void realmSet$screenOn(int i) {
        if (!this.d.f()) {
            this.d.a().e();
            this.d.b().a(this.c.e, (long) i);
        } else if (this.d.c()) {
            o b2 = this.d.b();
            b2.b().a(this.c.e, b2.c(), (long) i, true);
        }
    }

    public void realmSet$state(String str) {
        if (!this.d.f()) {
            this.d.a().e();
            if (str == null) {
                this.d.b().c(this.c.c);
            } else {
                this.d.b().a(this.c.c, str);
            }
        } else if (this.d.c()) {
            o b2 = this.d.b();
            if (str == null) {
                b2.b().a(this.c.c, b2.c(), true);
            } else {
                b2.b().a(this.c.c, b2.c(), str, true);
            }
        }
    }

    public void realmSet$timestamp(long j) {
        if (!this.d.f()) {
            this.d.a().e();
            this.d.b().a(this.c.f4710b, j);
        } else if (this.d.c()) {
            o b2 = this.d.b();
            b2.b().a(this.c.f4710b, b2.c(), j, true);
        }
    }

    public void realmSet$triggeredBy(String str) {
        if (!this.d.f()) {
            this.d.a().e();
            if (str == null) {
                this.d.b().c(this.c.f);
            } else {
                this.d.b().a(this.c.f, str);
            }
        } else if (this.d.c()) {
            o b2 = this.d.b();
            if (str == null) {
                b2.b().a(this.c.f, b2.c(), true);
            } else {
                b2.b().a(this.c.f, b2.c(), str, true);
            }
        }
    }

    public String toString() {
        if (!bj.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder sb = new StringBuilder("BatteryUsage = proxy[");
        sb.append("{id:");
        sb.append(realmGet$id());
        sb.append("}");
        sb.append(",");
        sb.append("{timestamp:");
        sb.append(realmGet$timestamp());
        sb.append("}");
        sb.append(",");
        sb.append("{state:");
        sb.append(realmGet$state() != null ? realmGet$state() : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{level:");
        sb.append(realmGet$level());
        sb.append("}");
        sb.append(",");
        sb.append("{screenOn:");
        sb.append(realmGet$screenOn());
        sb.append("}");
        sb.append(",");
        sb.append("{triggeredBy:");
        sb.append(realmGet$triggeredBy() != null ? realmGet$triggeredBy() : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{details:");
        sb.append(realmGet$details() != null ? "BatteryDetails" : "null");
        sb.append("}");
        sb.append("]");
        return sb.toString();
    }
}
