package io.realm;

import io.realm.internal.Collection;
import io.realm.internal.OsList;
import io.realm.internal.SortDescriptor;
import io.realm.internal.Table;
import io.realm.internal.TableQuery;
import io.realm.internal.a.c;
import io.realm.internal.a.c.a;

public class bm<E> {

    /* renamed from: a reason: collision with root package name */
    private final Table f4593a;

    /* renamed from: b reason: collision with root package name */
    private final e f4594b;
    private final TableQuery c;
    private final bl d;
    private Class<E> e;
    private String f;
    private final boolean g;
    private final OsList h;

    private bm(bb bbVar, Class<E> cls) {
        this.f4594b = bbVar;
        this.e = cls;
        this.g = !a(cls);
        if (this.g) {
            this.d = null;
            this.f4593a = null;
            this.h = null;
            this.c = null;
            return;
        }
        this.d = bbVar.k().b(cls);
        this.f4593a = this.d.b();
        this.h = null;
        this.c = this.f4593a.f();
    }

    static <E extends bh> bm<E> a(bb bbVar, Class<E> cls) {
        return new bm<>(bbVar, cls);
    }

    private bn<E> a(TableQuery tableQuery, SortDescriptor sortDescriptor, SortDescriptor sortDescriptor2, boolean z) {
        Collection collection = new Collection(this.f4594b.e, tableQuery, sortDescriptor, sortDescriptor2);
        bn<E> bnVar = d() ? new bn<>(this.f4594b, collection, this.f) : new bn<>(this.f4594b, collection, this.e);
        if (z) {
            bnVar.d();
        }
        return bnVar;
    }

    private static boolean a(Class<?> cls) {
        return bh.class.isAssignableFrom(cls);
    }

    private bm<E> b(String str, Integer num) {
        c a2 = this.d.a(str, RealmFieldType.INTEGER);
        if (num == null) {
            this.c.a(a2.a(), a2.b());
        } else {
            this.c.a(a2.a(), a2.b(), (long) num.intValue());
        }
        return this;
    }

    private bm<E> b(String str, String str2, r rVar) {
        c a2 = this.d.a(str, RealmFieldType.STRING);
        this.c.a(a2.a(), a2.b(), str2, rVar);
        return this;
    }

    private boolean d() {
        return this.f != null;
    }

    private long e() {
        return this.c.c();
    }

    private br f() {
        return new br(this.f4594b.k());
    }

    public long a() {
        this.f4594b.e();
        return this.c.d();
    }

    public bm<E> a(String str, long j) {
        this.f4594b.e();
        c a2 = this.d.a(str, RealmFieldType.INTEGER);
        this.c.b(a2.a(), a2.b(), j);
        return this;
    }

    public bm<E> a(String str, long j, long j2) {
        this.f4594b.e();
        this.c.a(this.d.a(str, RealmFieldType.INTEGER).a(), j, j2);
        return this;
    }

    public bm<E> a(String str, Integer num) {
        this.f4594b.e();
        return b(str, num);
    }

    public bm<E> a(String str, String str2) {
        return a(str, str2, r.SENSITIVE);
    }

    public bm<E> a(String str, String str2, r rVar) {
        this.f4594b.e();
        return b(str, str2, rVar);
    }

    public bn<E> a(String str) {
        return a(str, bu.ASCENDING);
    }

    public bn<E> a(String str, bu buVar) {
        this.f4594b.e();
        return a(this.c, SortDescriptor.getInstanceForSort((a) f(), this.c.a(), str, buVar), null, true);
    }

    public bn<E> b() {
        this.f4594b.e();
        return a(this.c, null, null, true);
    }

    public E c() {
        this.f4594b.e();
        if (this.g) {
            return null;
        }
        long e2 = e();
        if (e2 >= 0) {
            return this.f4594b.a(this.e, this.f, e2);
        }
        return null;
    }
}
