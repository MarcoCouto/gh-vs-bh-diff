package io.realm;

import io.realm.internal.Table;

class ap extends bo {
    ap(e eVar) {
        super(eVar, null);
    }

    public bl a(String str) {
        a(str, "Null or empty class names are not allowed");
        String c = Table.c(str);
        if (!this.f4595a.l().hasTable(c)) {
            return null;
        }
        return new ao(this.f4595a, this, this.f4595a.l().getTable(c));
    }
}
