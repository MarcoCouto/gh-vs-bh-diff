package io.realm;

import com.hmatalonga.greenhub.models.data.NetworkStatistics;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.c;
import io.realm.internal.m;
import io.realm.internal.o;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class as extends NetworkStatistics implements at, m {

    /* renamed from: a reason: collision with root package name */
    private static final OsObjectSchemaInfo f4557a = e();

    /* renamed from: b reason: collision with root package name */
    private static final List<String> f4558b;
    private a c;
    private ba<NetworkStatistics> d;

    static final class a extends c {

        /* renamed from: a reason: collision with root package name */
        long f4559a;

        /* renamed from: b reason: collision with root package name */
        long f4560b;
        long c;
        long d;

        a(OsSchemaInfo osSchemaInfo) {
            super(4);
            OsObjectSchemaInfo a2 = osSchemaInfo.a("NetworkStatistics");
            this.f4559a = a("wifiReceived", a2);
            this.f4560b = a("wifiSent", a2);
            this.c = a("mobileReceived", a2);
            this.d = a("mobileSent", a2);
        }

        /* access modifiers changed from: protected */
        public final void a(c cVar, c cVar2) {
            a aVar = (a) cVar;
            a aVar2 = (a) cVar2;
            aVar2.f4559a = aVar.f4559a;
            aVar2.f4560b = aVar.f4560b;
            aVar2.c = aVar.c;
            aVar2.d = aVar.d;
        }
    }

    static {
        ArrayList arrayList = new ArrayList(4);
        arrayList.add("wifiReceived");
        arrayList.add("wifiSent");
        arrayList.add("mobileReceived");
        arrayList.add("mobileSent");
        f4558b = Collections.unmodifiableList(arrayList);
    }

    as() {
        this.d.g();
    }

    public static NetworkStatistics a(bb bbVar, NetworkStatistics networkStatistics, boolean z, Map<bh, m> map) {
        if ((networkStatistics instanceof m) && ((m) networkStatistics).d().a() != null) {
            e a2 = ((m) networkStatistics).d().a();
            if (a2.c != bbVar.c) {
                throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
            } else if (a2.g().equals(bbVar.g())) {
                return networkStatistics;
            }
        }
        io.realm.e.a aVar = (io.realm.e.a) e.f.get();
        m mVar = (m) map.get(networkStatistics);
        return mVar != null ? (NetworkStatistics) mVar : b(bbVar, networkStatistics, z, map);
    }

    public static a a(OsSchemaInfo osSchemaInfo) {
        return new a(osSchemaInfo);
    }

    public static NetworkStatistics b(bb bbVar, NetworkStatistics networkStatistics, boolean z, Map<bh, m> map) {
        m mVar = (m) map.get(networkStatistics);
        if (mVar != null) {
            return (NetworkStatistics) mVar;
        }
        NetworkStatistics networkStatistics2 = (NetworkStatistics) bbVar.a(NetworkStatistics.class, false, Collections.emptyList());
        map.put(networkStatistics, (m) networkStatistics2);
        at atVar = networkStatistics;
        at atVar2 = networkStatistics2;
        atVar2.realmSet$wifiReceived(atVar.realmGet$wifiReceived());
        atVar2.realmSet$wifiSent(atVar.realmGet$wifiSent());
        atVar2.realmSet$mobileReceived(atVar.realmGet$mobileReceived());
        atVar2.realmSet$mobileSent(atVar.realmGet$mobileSent());
        return networkStatistics2;
    }

    public static OsObjectSchemaInfo b() {
        return f4557a;
    }

    public static String c() {
        return "class_NetworkStatistics";
    }

    private static OsObjectSchemaInfo e() {
        io.realm.internal.OsObjectSchemaInfo.a aVar = new io.realm.internal.OsObjectSchemaInfo.a("NetworkStatistics", 4, 0);
        aVar.a("wifiReceived", RealmFieldType.DOUBLE, false, false, true);
        aVar.a("wifiSent", RealmFieldType.DOUBLE, false, false, true);
        aVar.a("mobileReceived", RealmFieldType.DOUBLE, false, false, true);
        aVar.a("mobileSent", RealmFieldType.DOUBLE, false, false, true);
        return aVar.a();
    }

    public void a() {
        if (this.d == null) {
            io.realm.e.a aVar = (io.realm.e.a) e.f.get();
            this.c = (a) aVar.c();
            this.d = new ba<>(this);
            this.d.a(aVar.a());
            this.d.a(aVar.b());
            this.d.a(aVar.d());
            this.d.a(aVar.e());
        }
    }

    public ba<?> d() {
        return this.d;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        as asVar = (as) obj;
        String g = this.d.a().g();
        String g2 = asVar.d.a().g();
        if (g == null ? g2 != null : !g.equals(g2)) {
            return false;
        }
        String g3 = this.d.b().b().g();
        String g4 = asVar.d.b().b().g();
        if (g3 == null ? g4 != null : !g3.equals(g4)) {
            return false;
        }
        return this.d.b().c() == asVar.d.b().c();
    }

    public int hashCode() {
        int i = 0;
        String g = this.d.a().g();
        String g2 = this.d.b().b().g();
        long c2 = this.d.b().c();
        int hashCode = ((g != null ? g.hashCode() : 0) + 527) * 31;
        if (g2 != null) {
            i = g2.hashCode();
        }
        return ((i + hashCode) * 31) + ((int) ((c2 >>> 32) ^ c2));
    }

    public double realmGet$mobileReceived() {
        this.d.a().e();
        return this.d.b().j(this.c.c);
    }

    public double realmGet$mobileSent() {
        this.d.a().e();
        return this.d.b().j(this.c.d);
    }

    public double realmGet$wifiReceived() {
        this.d.a().e();
        return this.d.b().j(this.c.f4559a);
    }

    public double realmGet$wifiSent() {
        this.d.a().e();
        return this.d.b().j(this.c.f4560b);
    }

    public void realmSet$mobileReceived(double d2) {
        if (!this.d.f()) {
            this.d.a().e();
            this.d.b().a(this.c.c, d2);
        } else if (this.d.c()) {
            o b2 = this.d.b();
            b2.b().a(this.c.c, b2.c(), d2, true);
        }
    }

    public void realmSet$mobileSent(double d2) {
        if (!this.d.f()) {
            this.d.a().e();
            this.d.b().a(this.c.d, d2);
        } else if (this.d.c()) {
            o b2 = this.d.b();
            b2.b().a(this.c.d, b2.c(), d2, true);
        }
    }

    public void realmSet$wifiReceived(double d2) {
        if (!this.d.f()) {
            this.d.a().e();
            this.d.b().a(this.c.f4559a, d2);
        } else if (this.d.c()) {
            o b2 = this.d.b();
            b2.b().a(this.c.f4559a, b2.c(), d2, true);
        }
    }

    public void realmSet$wifiSent(double d2) {
        if (!this.d.f()) {
            this.d.a().e();
            this.d.b().a(this.c.f4560b, d2);
        } else if (this.d.c()) {
            o b2 = this.d.b();
            b2.b().a(this.c.f4560b, b2.c(), d2, true);
        }
    }

    public String toString() {
        if (!bj.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder sb = new StringBuilder("NetworkStatistics = proxy[");
        sb.append("{wifiReceived:");
        sb.append(realmGet$wifiReceived());
        sb.append("}");
        sb.append(",");
        sb.append("{wifiSent:");
        sb.append(realmGet$wifiSent());
        sb.append("}");
        sb.append(",");
        sb.append("{mobileReceived:");
        sb.append(realmGet$mobileReceived());
        sb.append("}");
        sb.append(",");
        sb.append("{mobileSent:");
        sb.append(realmGet$mobileSent());
        sb.append("}");
        sb.append("]");
        return sb.toString();
    }
}
