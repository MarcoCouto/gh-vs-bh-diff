package io.realm.internal;

import io.realm.CompactOnLaunchCallback;
import io.realm.be;
import io.realm.internal.SharedRealm.InitializationCallback;
import io.realm.internal.SharedRealm.MigrationCallback;
import io.realm.log.RealmLog;
import java.net.URI;
import java.net.URISyntaxException;

public class OsRealmConfig implements h {

    /* renamed from: a reason: collision with root package name */
    private static final long f4653a = nativeGetFinalizerPtr();

    /* renamed from: b reason: collision with root package name */
    private final be f4654b;
    private final URI c;
    private final long d;
    private final g e;
    private final CompactOnLaunchCallback f;
    private final MigrationCallback g;
    private final InitializationCallback h;

    public static class a {

        /* renamed from: a reason: collision with root package name */
        private be f4655a;

        /* renamed from: b reason: collision with root package name */
        private OsSchemaInfo f4656b = null;
        private MigrationCallback c = null;
        private InitializationCallback d = null;
        private boolean e = false;

        public a(be beVar) {
            this.f4655a = beVar;
        }

        public a a(OsSchemaInfo osSchemaInfo) {
            this.f4656b = osSchemaInfo;
            return this;
        }

        public a a(InitializationCallback initializationCallback) {
            this.d = initializationCallback;
            return this;
        }

        public a a(MigrationCallback migrationCallback) {
            this.c = migrationCallback;
            return this;
        }

        public a a(boolean z) {
            this.e = z;
            return this;
        }

        /* access modifiers changed from: 0000 */
        public OsRealmConfig a() {
            return new OsRealmConfig(this.f4655a, this.e, this.f4656b, this.c, this.d);
        }
    }

    public enum b {
        FULL(0),
        MEM_ONLY(1);
        
        final int c;

        private b(int i) {
            this.c = i;
        }
    }

    public enum c {
        SCHEMA_MODE_AUTOMATIC(0),
        SCHEMA_MODE_IMMUTABLE(1),
        SCHEMA_MODE_READONLY(2),
        SCHEMA_MODE_RESET_FILE(3),
        SCHEMA_MODE_ADDITIVE(4),
        SCHEMA_MODE_MANUAL(5);
        
        final byte g;

        private c(byte b2) {
            this.g = b2;
        }

        public byte a() {
            return this.g;
        }
    }

    private OsRealmConfig(be beVar, boolean z, OsSchemaInfo osSchemaInfo, MigrationCallback migrationCallback, InitializationCallback initializationCallback) {
        URI uri;
        this.e = new g();
        this.f4654b = beVar;
        this.d = nativeCreate(beVar.m(), false, true);
        g.f4695a.a(this);
        Object[] b2 = i.a().b(this.f4654b);
        String str = (String) b2[0];
        String str2 = (String) b2[1];
        String str3 = (String) b2[2];
        String str4 = (String) b2[3];
        boolean equals = Boolean.TRUE.equals(b2[4]);
        String str5 = (String) b2[5];
        Byte b3 = (Byte) b2[6];
        boolean equals2 = Boolean.TRUE.equals(b2[7]);
        byte[] c2 = beVar.c();
        if (c2 != null) {
            nativeSetEncryptionKey(this.d, c2);
        }
        nativeSetInMemory(this.d, beVar.g() == b.MEM_ONLY);
        nativeEnableChangeNotification(this.d, z);
        c cVar = c.SCHEMA_MODE_MANUAL;
        if (beVar.q()) {
            cVar = c.SCHEMA_MODE_IMMUTABLE;
        } else if (beVar.p()) {
            cVar = c.SCHEMA_MODE_READONLY;
        } else if (str2 != null) {
            cVar = c.SCHEMA_MODE_ADDITIVE;
        } else if (beVar.f()) {
            cVar = c.SCHEMA_MODE_RESET_FILE;
        }
        long d2 = beVar.d();
        long nativePtr = osSchemaInfo == null ? 0 : osSchemaInfo.getNativePtr();
        this.g = migrationCallback;
        nativeSetSchemaConfig(this.d, cVar.a(), d2, nativePtr, migrationCallback);
        this.f = beVar.l();
        if (this.f != null) {
            nativeSetCompactOnLaunchCallback(this.d, this.f);
        }
        this.h = initializationCallback;
        if (initializationCallback != null) {
            nativeSetInitializationCallback(this.d, initializationCallback);
        }
        if (str2 != null) {
            try {
                uri = new URI(nativeCreateAndSetSyncConfig(this.d, str2, str3, str, str4, equals2, b3.byteValue()));
            } catch (URISyntaxException e2) {
                RealmLog.b(e2, "Cannot create a URI from the Realm URL address", new Object[0]);
                uri = null;
            }
            nativeSetSyncConfigSslSettings(this.d, equals, str5);
        } else {
            uri = null;
        }
        this.c = uri;
    }

    private static native long nativeCreate(String str, boolean z, boolean z2);

    private static native String nativeCreateAndSetSyncConfig(long j, String str, String str2, String str3, String str4, boolean z, byte b2);

    private static native void nativeEnableChangeNotification(long j, boolean z);

    private static native long nativeGetFinalizerPtr();

    private static native void nativeSetCompactOnLaunchCallback(long j, CompactOnLaunchCallback compactOnLaunchCallback);

    private static native void nativeSetEncryptionKey(long j, byte[] bArr);

    private static native void nativeSetInMemory(long j, boolean z);

    private native void nativeSetInitializationCallback(long j, InitializationCallback initializationCallback);

    private native void nativeSetSchemaConfig(long j, byte b2, long j2, long j3, MigrationCallback migrationCallback);

    private static native void nativeSetSyncConfigSslSettings(long j, boolean z, String str);

    public be a() {
        return this.f4654b;
    }

    /* access modifiers changed from: 0000 */
    public g b() {
        return this.e;
    }

    public long getNativeFinalizerPtr() {
        return f4653a;
    }

    public long getNativePtr() {
        return this.d;
    }
}
