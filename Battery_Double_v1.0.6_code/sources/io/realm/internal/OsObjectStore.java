package io.realm.internal;

import io.realm.be;

public class OsObjectStore {
    public static long a(SharedRealm sharedRealm) {
        return nativeGetSchemaVersion(sharedRealm.getNativePtr());
    }

    public static String a(SharedRealm sharedRealm, String str) {
        return nativeGetPrimaryKeyForObject(sharedRealm.getNativePtr(), str);
    }

    public static void a(SharedRealm sharedRealm, long j) {
        nativeSetSchemaVersion(sharedRealm.getNativePtr(), j);
    }

    public static void a(SharedRealm sharedRealm, String str, String str2) {
        nativeSetPrimaryKeyForObject(sharedRealm.getNativePtr(), str, str2);
    }

    public static boolean a(be beVar, Runnable runnable) {
        return nativeCallWithLock(beVar.m(), runnable);
    }

    private static native boolean nativeCallWithLock(String str, Runnable runnable);

    private static native String nativeGetPrimaryKeyForObject(long j, String str);

    private static native long nativeGetSchemaVersion(long j);

    private static native void nativeSetPrimaryKeyForObject(long j, String str, String str2);

    private static native void nativeSetSchemaVersion(long j, long j2);
}
