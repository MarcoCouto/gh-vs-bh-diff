package io.realm.internal;

import io.realm.RealmFieldType;
import io.realm.bu;
import io.realm.internal.a.c;
import io.realm.internal.a.c.a;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

@Keep
public class SortDescriptor {
    static final Set<RealmFieldType> DISTINCT_VALID_FIELD_TYPES = Collections.unmodifiableSet(new HashSet(Arrays.asList(new RealmFieldType[]{RealmFieldType.BOOLEAN, RealmFieldType.INTEGER, RealmFieldType.STRING, RealmFieldType.DATE})));
    static final Set<RealmFieldType> SORT_VALID_FIELD_TYPES = Collections.unmodifiableSet(new HashSet(Arrays.asList(new RealmFieldType[]{RealmFieldType.BOOLEAN, RealmFieldType.INTEGER, RealmFieldType.FLOAT, RealmFieldType.DOUBLE, RealmFieldType.STRING, RealmFieldType.DATE})));
    private final boolean[] ascendings;
    private final long[][] columnIndices;
    private final Table table;

    private SortDescriptor(Table table2, long[][] jArr, bu[] buVarArr) {
        this.table = table2;
        this.columnIndices = jArr;
        if (buVarArr != null) {
            this.ascendings = new boolean[buVarArr.length];
            for (int i = 0; i < buVarArr.length; i++) {
                this.ascendings[i] = buVarArr[i].a();
            }
            return;
        }
        this.ascendings = null;
    }

    private static void checkFieldType(c cVar, Set<RealmFieldType> set, String str, String str2) {
        if (!set.contains(cVar.d())) {
            throw new IllegalArgumentException(String.format(Locale.US, "%s on '%s' field '%s' in '%s'.", new Object[]{str, cVar.d(), cVar.c(), str2}));
        }
    }

    private static SortDescriptor getInstance(a aVar, Table table2, String[] strArr, bu[] buVarArr, Set<RealmFieldType> set, Set<RealmFieldType> set2, String str) {
        if (strArr == null || strArr.length == 0) {
            throw new IllegalArgumentException("You must provide at least one field name.");
        }
        long[][] jArr = new long[strArr.length][];
        for (int i = 0; i < strArr.length; i++) {
            c a2 = c.a(aVar, table2, strArr[i], set, null);
            checkFieldType(a2, set2, str, strArr[i]);
            jArr[i] = a2.a();
        }
        return new SortDescriptor(table2, jArr, buVarArr);
    }

    public static SortDescriptor getInstanceForDistinct(a aVar, Table table2, String str) {
        return getInstanceForDistinct(aVar, table2, new String[]{str});
    }

    public static SortDescriptor getInstanceForDistinct(a aVar, Table table2, String[] strArr) {
        return getInstance(aVar, table2, strArr, null, c.e, DISTINCT_VALID_FIELD_TYPES, "Distinct is not supported");
    }

    public static SortDescriptor getInstanceForSort(a aVar, Table table2, String str, bu buVar) {
        return getInstanceForSort(aVar, table2, new String[]{str}, new bu[]{buVar});
    }

    public static SortDescriptor getInstanceForSort(a aVar, Table table2, String[] strArr, bu[] buVarArr) {
        if (buVarArr == null || buVarArr.length == 0) {
            throw new IllegalArgumentException("You must provide at least one sort order.");
        } else if (strArr.length != buVarArr.length) {
            throw new IllegalArgumentException("Number of fields and sort orders do not match.");
        } else {
            return getInstance(aVar, table2, strArr, buVarArr, c.d, SORT_VALID_FIELD_TYPES, "Sort is not supported");
        }
    }

    private long getTablePtr() {
        return this.table.getNativePtr();
    }

    static SortDescriptor getTestInstance(Table table2, long[] jArr) {
        return new SortDescriptor(table2, new long[][]{jArr}, null);
    }

    /* access modifiers changed from: 0000 */
    public boolean[] getAscendings() {
        return this.ascendings;
    }

    /* access modifiers changed from: 0000 */
    public long[][] getColumnIndices() {
        return this.columnIndices;
    }
}
