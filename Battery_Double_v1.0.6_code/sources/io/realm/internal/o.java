package io.realm.internal;

import io.realm.RealmFieldType;
import java.util.Date;

public interface o {
    long a();

    long a(String str);

    OsList a(long j, RealmFieldType realmFieldType);

    void a(long j, double d);

    void a(long j, float f);

    void a(long j, long j2);

    void a(long j, String str);

    void a(long j, boolean z);

    boolean a(long j);

    Table b();

    void b(long j, long j2);

    boolean b(long j);

    long c();

    void c(long j);

    OsList d(long j);

    boolean d();

    String e(long j);

    RealmFieldType f(long j);

    long g(long j);

    boolean h(long j);

    float i(long j);

    double j(long j);

    Date k(long j);

    String l(long j);

    byte[] m(long j);

    long n(long j);

    void o(long j);
}
