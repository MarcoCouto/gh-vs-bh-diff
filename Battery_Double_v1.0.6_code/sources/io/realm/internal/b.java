package io.realm.internal;

import io.realm.bh;
import io.realm.exceptions.RealmException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

public final class b {

    /* renamed from: a reason: collision with root package name */
    private final Map<Class<? extends bh>, c> f4683a = new HashMap();

    /* renamed from: b reason: collision with root package name */
    private final Map<String, c> f4684b = new HashMap();
    private final n c;
    private final OsSchemaInfo d;

    public b(n nVar, OsSchemaInfo osSchemaInfo) {
        this.c = nVar;
        this.d = osSchemaInfo;
    }

    public c a(Class<? extends bh> cls) {
        c cVar = (c) this.f4683a.get(cls);
        if (cVar != null) {
            return cVar;
        }
        c a2 = this.c.a(cls, this.d);
        this.f4683a.put(cls, a2);
        return a2;
    }

    public c a(String str) {
        c cVar = (c) this.f4684b.get(str);
        if (cVar == null) {
            Iterator it = this.c.b().iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                Class cls = (Class) it.next();
                if (Table.b(this.c.a(cls)).equals(str)) {
                    cVar = a(cls);
                    this.f4684b.put(str, cVar);
                    break;
                }
            }
        }
        if (cVar != null) {
            return cVar;
        }
        throw new RealmException(String.format(Locale.US, "'%s' doesn't exist in current schema.", new Object[]{str}));
    }

    public void a() {
        for (Entry entry : this.f4683a.entrySet()) {
            ((c) entry.getValue()).a(this.c.a((Class) entry.getKey(), this.d));
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("ColumnIndices[");
        boolean z = false;
        Iterator it = this.f4683a.entrySet().iterator();
        while (true) {
            boolean z2 = z;
            if (!it.hasNext()) {
                return sb.append("]").toString();
            }
            Entry entry = (Entry) it.next();
            if (z2) {
                sb.append(",");
            }
            sb.append(((Class) entry.getKey()).getSimpleName()).append("->").append(entry.getValue());
            z = true;
        }
    }
}
