package io.realm.internal;

import io.realm.RealmFieldType;
import java.util.Date;

public class UncheckedRow implements h, o {

    /* renamed from: a reason: collision with root package name */
    private static final long f4674a = nativeGetFinalizerPtr();

    /* renamed from: b reason: collision with root package name */
    private final g f4675b;
    private final Table c;
    private final long d;

    UncheckedRow(UncheckedRow uncheckedRow) {
        this.f4675b = uncheckedRow.f4675b;
        this.c = uncheckedRow.c;
        this.d = uncheckedRow.d;
    }

    UncheckedRow(g gVar, Table table, long j) {
        this.f4675b = gVar;
        this.c = table;
        this.d = j;
        gVar.a(this);
    }

    static UncheckedRow b(g gVar, Table table, long j) {
        return new UncheckedRow(gVar, table, table.nativeGetRowPtr(table.getNativePtr(), j));
    }

    static UncheckedRow c(g gVar, Table table, long j) {
        return new UncheckedRow(gVar, table, j);
    }

    private static native long nativeGetFinalizerPtr();

    public long a() {
        return nativeGetColumnCount(this.d);
    }

    public long a(String str) {
        if (str != null) {
            return nativeGetColumnIndex(this.d, str);
        }
        throw new IllegalArgumentException("Column name can not be null.");
    }

    public OsList a(long j, RealmFieldType realmFieldType) {
        return new OsList(this, j);
    }

    public void a(long j, double d2) {
        this.c.e();
        nativeSetDouble(this.d, j, d2);
    }

    public void a(long j, float f) {
        this.c.e();
        nativeSetFloat(this.d, j, f);
    }

    public void a(long j, long j2) {
        this.c.e();
        nativeSetLong(this.d, j, j2);
    }

    public void a(long j, String str) {
        this.c.e();
        if (str == null) {
            nativeSetNull(this.d, j);
            return;
        }
        nativeSetString(this.d, j, str);
    }

    public void a(long j, boolean z) {
        this.c.e();
        nativeSetBoolean(this.d, j, z);
    }

    public void a(long j, byte[] bArr) {
        this.c.e();
        nativeSetByteArray(this.d, j, bArr);
    }

    public boolean a(long j) {
        return nativeIsNullLink(this.d, j);
    }

    public Table b() {
        return this.c;
    }

    public void b(long j, long j2) {
        this.c.e();
        nativeSetLink(this.d, j, j2);
    }

    public boolean b(long j) {
        return nativeIsNull(this.d, j);
    }

    public long c() {
        return nativeGetIndex(this.d);
    }

    public void c(long j) {
        this.c.e();
        nativeSetNull(this.d, j);
    }

    public OsList d(long j) {
        return new OsList(this, j);
    }

    public boolean d() {
        return this.d != 0 && nativeIsAttached(this.d);
    }

    public String e(long j) {
        return nativeGetColumnName(this.d, j);
    }

    public RealmFieldType f(long j) {
        return RealmFieldType.fromNativeValue(nativeGetColumnType(this.d, j));
    }

    public long g(long j) {
        return nativeGetLong(this.d, j);
    }

    public long getNativeFinalizerPtr() {
        return f4674a;
    }

    public long getNativePtr() {
        return this.d;
    }

    public boolean h(long j) {
        return nativeGetBoolean(this.d, j);
    }

    public float i(long j) {
        return nativeGetFloat(this.d, j);
    }

    public double j(long j) {
        return nativeGetDouble(this.d, j);
    }

    public Date k(long j) {
        return new Date(nativeGetTimestamp(this.d, j));
    }

    public String l(long j) {
        return nativeGetString(this.d, j);
    }

    public byte[] m(long j) {
        return nativeGetByteArray(this.d, j);
    }

    public long n(long j) {
        return nativeGetLink(this.d, j);
    }

    /* access modifiers changed from: protected */
    public native boolean nativeGetBoolean(long j, long j2);

    /* access modifiers changed from: protected */
    public native byte[] nativeGetByteArray(long j, long j2);

    /* access modifiers changed from: protected */
    public native long nativeGetColumnCount(long j);

    /* access modifiers changed from: protected */
    public native long nativeGetColumnIndex(long j, String str);

    /* access modifiers changed from: protected */
    public native String nativeGetColumnName(long j, long j2);

    /* access modifiers changed from: protected */
    public native int nativeGetColumnType(long j, long j2);

    /* access modifiers changed from: protected */
    public native double nativeGetDouble(long j, long j2);

    /* access modifiers changed from: protected */
    public native float nativeGetFloat(long j, long j2);

    /* access modifiers changed from: protected */
    public native long nativeGetIndex(long j);

    /* access modifiers changed from: protected */
    public native long nativeGetLink(long j, long j2);

    /* access modifiers changed from: protected */
    public native long nativeGetLong(long j, long j2);

    /* access modifiers changed from: protected */
    public native String nativeGetString(long j, long j2);

    /* access modifiers changed from: protected */
    public native long nativeGetTimestamp(long j, long j2);

    /* access modifiers changed from: protected */
    public native boolean nativeIsAttached(long j);

    /* access modifiers changed from: protected */
    public native boolean nativeIsNull(long j, long j2);

    /* access modifiers changed from: protected */
    public native boolean nativeIsNullLink(long j, long j2);

    /* access modifiers changed from: protected */
    public native void nativeNullifyLink(long j, long j2);

    /* access modifiers changed from: protected */
    public native void nativeSetBoolean(long j, long j2, boolean z);

    /* access modifiers changed from: protected */
    public native void nativeSetByteArray(long j, long j2, byte[] bArr);

    /* access modifiers changed from: protected */
    public native void nativeSetDouble(long j, long j2, double d2);

    /* access modifiers changed from: protected */
    public native void nativeSetFloat(long j, long j2, float f);

    /* access modifiers changed from: protected */
    public native void nativeSetLink(long j, long j2, long j3);

    /* access modifiers changed from: protected */
    public native void nativeSetLong(long j, long j2, long j3);

    /* access modifiers changed from: protected */
    public native void nativeSetNull(long j, long j2);

    /* access modifiers changed from: protected */
    public native void nativeSetString(long j, long j2, String str);

    public void o(long j) {
        this.c.e();
        nativeNullifyLink(this.d, j);
    }
}
