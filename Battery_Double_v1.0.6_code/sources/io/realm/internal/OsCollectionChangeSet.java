package io.realm.internal;

import io.realm.av;

public class OsCollectionChangeSet implements av, h {

    /* renamed from: a reason: collision with root package name */
    private static long f4642a = nativeGetFinalizerPtr();

    /* renamed from: b reason: collision with root package name */
    private final long f4643b;

    public OsCollectionChangeSet(long j) {
        this.f4643b = j;
        g.f4695a.a(this);
    }

    private static native long nativeGetFinalizerPtr();

    public long getNativeFinalizerPtr() {
        return f4642a;
    }

    public long getNativePtr() {
        return this.f4643b;
    }
}
