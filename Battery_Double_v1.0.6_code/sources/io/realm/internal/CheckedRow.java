package io.realm.internal;

import io.realm.RealmFieldType;
import java.util.Locale;

public class CheckedRow extends UncheckedRow {

    /* renamed from: a reason: collision with root package name */
    private UncheckedRow f4633a;

    private CheckedRow(UncheckedRow uncheckedRow) {
        super(uncheckedRow);
        this.f4633a = uncheckedRow;
    }

    private CheckedRow(g gVar, Table table, long j) {
        super(gVar, table, j);
    }

    public static CheckedRow a(UncheckedRow uncheckedRow) {
        return new CheckedRow(uncheckedRow);
    }

    public static CheckedRow a(g gVar, Table table, long j) {
        return new CheckedRow(gVar, table, table.nativeGetRowPtr(table.getNativePtr(), j));
    }

    public OsList a(long j, RealmFieldType realmFieldType) {
        if (realmFieldType == b().c(j)) {
            return super.a(j, realmFieldType);
        }
        throw new IllegalArgumentException(String.format(Locale.US, "The type of field '%1$s' is not 'RealmFieldType.%2$s'.", new Object[]{b().b(j), realmFieldType.name()}));
    }

    public boolean a(long j) {
        RealmFieldType f = f(j);
        if (f == RealmFieldType.OBJECT || f == RealmFieldType.LIST) {
            return super.a(j);
        }
        return false;
    }

    public boolean b(long j) {
        return super.b(j);
    }

    public void c(long j) {
        if (f(j) == RealmFieldType.BINARY) {
            super.a(j, (byte[]) null);
        } else {
            super.c(j);
        }
    }

    public OsList d(long j) {
        if (b().c(j) == RealmFieldType.LIST) {
            return super.d(j);
        }
        throw new IllegalArgumentException(String.format(Locale.US, "Field '%s' is not a 'RealmList'.", new Object[]{b().b(j)}));
    }

    /* access modifiers changed from: protected */
    public native boolean nativeGetBoolean(long j, long j2);

    /* access modifiers changed from: protected */
    public native byte[] nativeGetByteArray(long j, long j2);

    /* access modifiers changed from: protected */
    public native long nativeGetColumnCount(long j);

    /* access modifiers changed from: protected */
    public native long nativeGetColumnIndex(long j, String str);

    /* access modifiers changed from: protected */
    public native String nativeGetColumnName(long j, long j2);

    /* access modifiers changed from: protected */
    public native int nativeGetColumnType(long j, long j2);

    /* access modifiers changed from: protected */
    public native double nativeGetDouble(long j, long j2);

    /* access modifiers changed from: protected */
    public native float nativeGetFloat(long j, long j2);

    /* access modifiers changed from: protected */
    public native long nativeGetLink(long j, long j2);

    /* access modifiers changed from: protected */
    public native long nativeGetLong(long j, long j2);

    /* access modifiers changed from: protected */
    public native String nativeGetString(long j, long j2);

    /* access modifiers changed from: protected */
    public native long nativeGetTimestamp(long j, long j2);

    /* access modifiers changed from: protected */
    public native boolean nativeIsNullLink(long j, long j2);

    /* access modifiers changed from: protected */
    public native void nativeNullifyLink(long j, long j2);

    /* access modifiers changed from: protected */
    public native void nativeSetBoolean(long j, long j2, boolean z);

    /* access modifiers changed from: protected */
    public native void nativeSetByteArray(long j, long j2, byte[] bArr);

    /* access modifiers changed from: protected */
    public native void nativeSetDouble(long j, long j2, double d);

    /* access modifiers changed from: protected */
    public native void nativeSetFloat(long j, long j2, float f);

    /* access modifiers changed from: protected */
    public native void nativeSetLink(long j, long j2, long j3);

    /* access modifiers changed from: protected */
    public native void nativeSetLong(long j, long j2, long j3);

    /* access modifiers changed from: protected */
    public native void nativeSetString(long j, long j2, String str);
}
