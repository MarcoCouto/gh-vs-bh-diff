package io.realm.internal;

import io.realm.log.RealmLog;
import java.lang.ref.ReferenceQueue;

class d implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final ReferenceQueue<h> f4692a;

    d(ReferenceQueue<h> referenceQueue) {
        this.f4692a = referenceQueue;
    }

    public void run() {
        while (true) {
            try {
                ((NativeObjectReference) this.f4692a.remove()).a();
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                RealmLog.b("The FinalizerRunnable thread has been interrupted. Native resources cannot be freed anymore", new Object[0]);
                return;
            }
        }
    }
}
