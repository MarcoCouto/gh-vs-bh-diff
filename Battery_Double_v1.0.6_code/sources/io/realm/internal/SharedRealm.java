package io.realm.internal;

import io.realm.be;
import io.realm.exceptions.RealmException;
import io.realm.internal.android.AndroidRealmNotifier;
import java.io.Closeable;
import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

@Keep
public final class SharedRealm implements h, Closeable {
    public static final byte FILE_EXCEPTION_INCOMPATIBLE_SYNC_FILE = 7;
    public static final byte FILE_EXCEPTION_KIND_ACCESS_ERROR = 0;
    public static final byte FILE_EXCEPTION_KIND_BAD_HISTORY = 1;
    public static final byte FILE_EXCEPTION_KIND_EXISTS = 3;
    public static final byte FILE_EXCEPTION_KIND_FORMAT_UPGRADE_REQUIRED = 6;
    public static final byte FILE_EXCEPTION_KIND_INCOMPATIBLE_LOCK_FILE = 5;
    public static final byte FILE_EXCEPTION_KIND_NOT_FOUND = 4;
    public static final byte FILE_EXCEPTION_KIND_PERMISSION_DENIED = 2;
    private static final long nativeFinalizerPtr = nativeGetFinalizerPtr();
    private static volatile File temporaryDirectory;
    public final a capabilities;
    public final List<WeakReference<Collection>> collections = new CopyOnWriteArrayList();
    final g context;
    public final List<WeakReference<io.realm.internal.Collection.a>> iterators = new ArrayList();
    private final long nativePtr;
    private final OsRealmConfig osRealmConfig;
    private final List<WeakReference<k>> pendingRows = new CopyOnWriteArrayList();
    public final RealmNotifier realmNotifier;
    private final OsSchemaInfo schemaInfo;

    @Keep
    public interface InitializationCallback {
        void onInit(SharedRealm sharedRealm);
    }

    @Keep
    public interface MigrationCallback {
        void onMigrationNeeded(SharedRealm sharedRealm, long j, long j2);
    }

    @Keep
    public static abstract class PartialSyncCallback {
        /* access modifiers changed from: private */
        public final String className;

        protected PartialSyncCallback(String str) {
            this.className = str;
        }

        public abstract void onError(RealmException realmException);

        public abstract void onSuccess(Collection collection);
    }

    @Keep
    public interface SchemaChangedCallback {
        void onSchemaChanged();
    }

    public static class a implements Comparable<a> {

        /* renamed from: a reason: collision with root package name */
        public final long f4667a;

        /* renamed from: b reason: collision with root package name */
        public final long f4668b;

        a(long j, long j2) {
            this.f4667a = j;
            this.f4668b = j2;
        }

        /* renamed from: a */
        public int compareTo(a aVar) {
            if (aVar == null) {
                throw new IllegalArgumentException("Version cannot be compared to a null value.");
            } else if (this.f4667a > aVar.f4667a) {
                return 1;
            } else {
                return this.f4667a < aVar.f4667a ? -1 : 0;
            }
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            a aVar = (a) obj;
            return this.f4667a == aVar.f4667a && this.f4668b == aVar.f4668b;
        }

        public int hashCode() {
            return (((super.hashCode() * 31) + ((int) (this.f4667a ^ (this.f4667a >>> 32)))) * 31) + ((int) (this.f4668b ^ (this.f4668b >>> 32)));
        }

        public String toString() {
            return "VersionID{version=" + this.f4667a + ", index=" + this.f4668b + '}';
        }
    }

    private SharedRealm(long j, OsRealmConfig osRealmConfig2) {
        this.nativePtr = j;
        this.osRealmConfig = osRealmConfig2;
        this.schemaInfo = new OsSchemaInfo(nativeGetSchemaInfo(this.nativePtr), this);
        this.context = osRealmConfig2.b();
        this.context.a(this);
        this.capabilities = new io.realm.internal.android.a();
        this.realmNotifier = null;
        nativeSetAutoRefresh(this.nativePtr, false);
    }

    private SharedRealm(OsRealmConfig osRealmConfig2) {
        io.realm.internal.android.a aVar = new io.realm.internal.android.a();
        AndroidRealmNotifier androidRealmNotifier = new AndroidRealmNotifier(this, aVar);
        this.nativePtr = nativeGetSharedRealm(osRealmConfig2.getNativePtr(), androidRealmNotifier);
        this.osRealmConfig = osRealmConfig2;
        this.schemaInfo = new OsSchemaInfo(nativeGetSchemaInfo(this.nativePtr), this);
        this.context = osRealmConfig2.b();
        this.context.a(this);
        this.capabilities = aVar;
        this.realmNotifier = androidRealmNotifier;
        nativeSetAutoRefresh(this.nativePtr, aVar.a());
    }

    private void executePendingRowQueries() {
        for (WeakReference weakReference : this.pendingRows) {
            k kVar = (k) weakReference.get();
            if (kVar != null) {
                kVar.e();
            }
        }
        this.pendingRows.clear();
    }

    public static SharedRealm getInstance(be beVar) {
        return getInstance(new io.realm.internal.OsRealmConfig.a(beVar));
    }

    public static SharedRealm getInstance(io.realm.internal.OsRealmConfig.a aVar) {
        OsRealmConfig a2 = aVar.a();
        i.a().a(a2);
        return new SharedRealm(a2);
    }

    public static File getTemporaryDirectory() {
        return temporaryDirectory;
    }

    public static void initialize(File file) {
        if (temporaryDirectory == null) {
            String absolutePath = file.getAbsolutePath();
            if (file.isDirectory() || file.mkdirs() || file.isDirectory()) {
                if (!absolutePath.endsWith("/")) {
                    absolutePath = absolutePath + "/";
                }
                nativeInit(absolutePath);
                temporaryDirectory = file;
                return;
            }
            throw new e("failed to create temporary directory: " + absolutePath);
        }
    }

    private static native void nativeBeginTransaction(long j);

    private static native void nativeCancelTransaction(long j);

    private static native void nativeCloseSharedRealm(long j);

    private static native void nativeCommitTransaction(long j);

    private static native boolean nativeCompact(long j);

    private static native long nativeCreateTable(long j, String str);

    private static native long nativeCreateTableWithPrimaryKeyField(long j, String str, String str2, boolean z, boolean z2);

    private static native long nativeGetFinalizerPtr();

    private static native long nativeGetSchemaInfo(long j);

    private static native long nativeGetSharedRealm(long j, RealmNotifier realmNotifier2);

    private static native long nativeGetTable(long j, String str);

    private static native String nativeGetTableName(long j, int i);

    private static native long[] nativeGetVersionID(long j);

    private static native boolean nativeHasTable(long j, String str);

    private static native void nativeInit(String str);

    private static native boolean nativeIsAutoRefresh(long j);

    private static native boolean nativeIsClosed(long j);

    private static native boolean nativeIsEmpty(long j);

    private static native boolean nativeIsInTransaction(long j);

    private static native void nativeRefresh(long j);

    private native void nativeRegisterPartialSyncQuery(long j, String str, String str2, PartialSyncCallback partialSyncCallback);

    private static native void nativeRegisterSchemaChangedCallback(long j, SchemaChangedCallback schemaChangedCallback);

    private static native void nativeRenameTable(long j, String str, String str2);

    private static native void nativeSetAutoRefresh(long j, boolean z);

    private static native long nativeSize(long j);

    private static native void nativeStopWaitForChange(long j);

    private static native boolean nativeWaitForChange(long j);

    private static native void nativeWriteCopy(long j, String str, byte[] bArr);

    private static void runInitializationCallback(long j, OsRealmConfig osRealmConfig2, InitializationCallback initializationCallback) {
        initializationCallback.onInit(new SharedRealm(j, osRealmConfig2));
    }

    private static void runMigrationCallback(long j, OsRealmConfig osRealmConfig2, MigrationCallback migrationCallback, long j2) {
        migrationCallback.onMigrationNeeded(new SharedRealm(j, osRealmConfig2), j2, osRealmConfig2.a().d());
    }

    private void runPartialSyncRegistrationCallback(String str, long j, PartialSyncCallback partialSyncCallback) {
        if (str != null) {
            partialSyncCallback.onError(new RealmException(str));
            return;
        }
        partialSyncCallback.onSuccess(new Collection(this, getTable(Table.c(partialSyncCallback.className)), j, true));
    }

    /* access modifiers changed from: 0000 */
    public void addIterator(io.realm.internal.Collection.a aVar) {
        this.iterators.add(new WeakReference(aVar));
    }

    /* access modifiers changed from: 0000 */
    public void addPendingRow(k kVar) {
        this.pendingRows.add(new WeakReference(kVar));
    }

    public void beginTransaction() {
        detachIterators();
        executePendingRowQueries();
        nativeBeginTransaction(this.nativePtr);
    }

    public void cancelTransaction() {
        nativeCancelTransaction(this.nativePtr);
    }

    public void close() {
        if (this.realmNotifier != null) {
            this.realmNotifier.close();
        }
        synchronized (this.context) {
            nativeCloseSharedRealm(this.nativePtr);
        }
    }

    public void commitTransaction() {
        nativeCommitTransaction(this.nativePtr);
    }

    public boolean compact() {
        return nativeCompact(this.nativePtr);
    }

    public Table createTable(String str) {
        return new Table(this, nativeCreateTable(this.nativePtr, str));
    }

    public Table createTableWithPrimaryKey(String str, String str2, boolean z, boolean z2) {
        return new Table(this, nativeCreateTableWithPrimaryKeyField(this.nativePtr, str, str2, z, z2));
    }

    /* access modifiers changed from: 0000 */
    public void detachIterators() {
        for (WeakReference weakReference : this.iterators) {
            io.realm.internal.Collection.a aVar = (io.realm.internal.Collection.a) weakReference.get();
            if (aVar != null) {
                aVar.a();
            }
        }
        this.iterators.clear();
    }

    public be getConfiguration() {
        return this.osRealmConfig.a();
    }

    public long getNativeFinalizerPtr() {
        return nativeFinalizerPtr;
    }

    public long getNativePtr() {
        return this.nativePtr;
    }

    public String getPath() {
        return this.osRealmConfig.a().m();
    }

    public OsSchemaInfo getSchemaInfo() {
        return this.schemaInfo;
    }

    public Table getTable(String str) {
        return new Table(this, nativeGetTable(this.nativePtr, str));
    }

    public String getTableName(int i) {
        return nativeGetTableName(this.nativePtr, i);
    }

    public a getVersionID() {
        long[] nativeGetVersionID = nativeGetVersionID(this.nativePtr);
        return new a(nativeGetVersionID[0], nativeGetVersionID[1]);
    }

    public boolean hasTable(String str) {
        return nativeHasTable(this.nativePtr, str);
    }

    /* access modifiers changed from: 0000 */
    public void invalidateIterators() {
        for (WeakReference weakReference : this.iterators) {
            io.realm.internal.Collection.a aVar = (io.realm.internal.Collection.a) weakReference.get();
            if (aVar != null) {
                aVar.b();
            }
        }
        this.iterators.clear();
    }

    public boolean isAutoRefresh() {
        return nativeIsAutoRefresh(this.nativePtr);
    }

    public boolean isClosed() {
        return nativeIsClosed(this.nativePtr);
    }

    public boolean isEmpty() {
        return nativeIsEmpty(this.nativePtr);
    }

    public boolean isInTransaction() {
        return nativeIsInTransaction(this.nativePtr);
    }

    public void refresh() {
        nativeRefresh(this.nativePtr);
    }

    public void registerPartialSyncQuery(String str, PartialSyncCallback partialSyncCallback) {
        nativeRegisterPartialSyncQuery(this.nativePtr, partialSyncCallback.className, str, partialSyncCallback);
    }

    public void registerSchemaChangedCallback(SchemaChangedCallback schemaChangedCallback) {
        nativeRegisterSchemaChangedCallback(this.nativePtr, schemaChangedCallback);
    }

    /* access modifiers changed from: 0000 */
    public void removePendingRow(k kVar) {
        for (WeakReference weakReference : this.pendingRows) {
            k kVar2 = (k) weakReference.get();
            if (kVar2 == null || kVar2 == kVar) {
                this.pendingRows.remove(weakReference);
            }
        }
    }

    public void renameTable(String str, String str2) {
        nativeRenameTable(this.nativePtr, str, str2);
    }

    public void setAutoRefresh(boolean z) {
        this.capabilities.a(null);
        nativeSetAutoRefresh(this.nativePtr, z);
    }

    public long size() {
        return nativeSize(this.nativePtr);
    }

    public void stopWaitForChange() {
        nativeStopWaitForChange(this.nativePtr);
    }

    public boolean waitForChange() {
        return nativeWaitForChange(this.nativePtr);
    }

    public void writeCopy(File file, byte[] bArr) {
        if (!file.isFile() || !file.exists()) {
            nativeWriteCopy(this.nativePtr, file.getAbsolutePath(), bArr);
            return;
        }
        throw new IllegalArgumentException("The destination file must not exist");
    }
}
