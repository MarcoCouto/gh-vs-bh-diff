package io.realm.internal.android;

import android.os.Looper;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

public class a implements io.realm.internal.a {
    @SuppressFBWarnings({"MS_SHOULD_BE_FINAL"})

    /* renamed from: a reason: collision with root package name */
    public static boolean f4678a = false;

    /* renamed from: b reason: collision with root package name */
    private final Looper f4679b = Looper.myLooper();
    private final boolean c = c();

    private boolean b() {
        return this.f4679b != null;
    }

    private static boolean c() {
        String name = Thread.currentThread().getName();
        return name != null && name.startsWith("IntentService[");
    }

    public void a(String str) {
        if (!b()) {
            throw new IllegalStateException(str == null ? "" : str + " " + "Realm cannot be automatically updated on a thread without a looper.");
        } else if (this.c) {
            throw new IllegalStateException(str == null ? "" : str + " " + "Realm cannot be automatically updated on an IntentService thread.");
        }
    }

    public boolean a() {
        return b() && !this.c;
    }
}
