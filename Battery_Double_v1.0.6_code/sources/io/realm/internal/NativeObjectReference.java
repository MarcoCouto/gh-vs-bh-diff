package io.realm.internal;

import java.lang.ref.PhantomReference;
import java.lang.ref.ReferenceQueue;

final class NativeObjectReference extends PhantomReference<h> {
    private static a f = new a();

    /* renamed from: a reason: collision with root package name */
    private final long f4637a;

    /* renamed from: b reason: collision with root package name */
    private final long f4638b;
    private final g c;
    /* access modifiers changed from: private */
    public NativeObjectReference d;
    /* access modifiers changed from: private */
    public NativeObjectReference e;

    private static class a {

        /* renamed from: a reason: collision with root package name */
        NativeObjectReference f4639a;

        private a() {
        }

        /* access modifiers changed from: 0000 */
        public synchronized void a(NativeObjectReference nativeObjectReference) {
            nativeObjectReference.d = null;
            nativeObjectReference.e = this.f4639a;
            if (this.f4639a != null) {
                this.f4639a.d = nativeObjectReference;
            }
            this.f4639a = nativeObjectReference;
        }

        /* access modifiers changed from: 0000 */
        public synchronized void b(NativeObjectReference nativeObjectReference) {
            NativeObjectReference a2 = nativeObjectReference.e;
            NativeObjectReference b2 = nativeObjectReference.d;
            nativeObjectReference.e = null;
            nativeObjectReference.d = null;
            if (b2 != null) {
                b2.e = a2;
            } else {
                this.f4639a = a2;
            }
            if (a2 != null) {
                a2.d = b2;
            }
        }
    }

    NativeObjectReference(g gVar, h hVar, ReferenceQueue<? super h> referenceQueue) {
        super(hVar, referenceQueue);
        this.f4637a = hVar.getNativePtr();
        this.f4638b = hVar.getNativeFinalizerPtr();
        this.c = gVar;
        f.a(this);
    }

    private static native void nativeCleanUp(long j, long j2);

    /* access modifiers changed from: 0000 */
    public void a() {
        synchronized (this.c) {
            nativeCleanUp(this.f4638b, this.f4637a);
        }
        f.b(this);
    }
}
