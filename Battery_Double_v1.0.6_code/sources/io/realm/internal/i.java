package io.realm.internal;

import android.content.Context;
import io.realm.be;
import io.realm.exceptions.RealmException;
import java.lang.reflect.InvocationTargetException;

public class i {

    /* renamed from: a reason: collision with root package name */
    private static final i f4697a = new i();

    /* renamed from: b reason: collision with root package name */
    private static i f4698b;

    static {
        f4698b = null;
        try {
            f4698b = (i) Class.forName("io.realm.internal.SyncObjectServerFacade").getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (ClassNotFoundException e) {
        } catch (InstantiationException e2) {
            throw new RealmException("Failed to init SyncObjectServerFacade", e2);
        } catch (IllegalAccessException e3) {
            throw new RealmException("Failed to init SyncObjectServerFacade", e3);
        } catch (NoSuchMethodException e4) {
            throw new RealmException("Failed to init SyncObjectServerFacade", e4);
        } catch (InvocationTargetException e5) {
            throw new RealmException("Failed to init SyncObjectServerFacade", e5.getTargetException());
        }
    }

    public static i a() {
        return f4698b != null ? f4698b : f4697a;
    }

    public static i a(boolean z) {
        return z ? f4698b : f4697a;
    }

    public void a(Context context) {
    }

    public void a(be beVar) {
    }

    public void a(OsRealmConfig osRealmConfig) {
    }

    public Object[] b(be beVar) {
        return new Object[8];
    }

    public String c(be beVar) {
        return null;
    }

    public String d(be beVar) {
        return null;
    }

    public void e(be beVar) {
    }
}
