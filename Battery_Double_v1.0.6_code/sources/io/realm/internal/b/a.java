package io.realm.internal.b;

import io.realm.bb;
import io.realm.bh;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.Util;
import io.realm.internal.c;
import io.realm.internal.m;
import io.realm.internal.n;
import io.realm.internal.o;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class a extends n {

    /* renamed from: a reason: collision with root package name */
    private final Map<Class<? extends bh>, n> f4685a;

    public a(n... nVarArr) {
        HashMap hashMap = new HashMap();
        if (nVarArr != null) {
            for (n nVar : nVarArr) {
                for (Class put : nVar.b()) {
                    hashMap.put(put, nVar);
                }
            }
        }
        this.f4685a = Collections.unmodifiableMap(hashMap);
    }

    private n e(Class<? extends bh> cls) {
        n nVar = (n) this.f4685a.get(cls);
        if (nVar != null) {
            return nVar;
        }
        throw new IllegalArgumentException(cls.getSimpleName() + " is not part of the schema for this Realm");
    }

    public <E extends bh> E a(bb bbVar, E e, boolean z, Map<bh, m> map) {
        return e(Util.a(e.getClass())).a(bbVar, e, z, map);
    }

    public <E extends bh> E a(Class<E> cls, Object obj, o oVar, c cVar, boolean z, List<String> list) {
        return e(cls).a(cls, obj, oVar, cVar, z, list);
    }

    public c a(Class<? extends bh> cls, OsSchemaInfo osSchemaInfo) {
        return e(cls).a(cls, osSchemaInfo);
    }

    public String a(Class<? extends bh> cls) {
        return e(cls).a(cls);
    }

    public Map<Class<? extends bh>, OsObjectSchemaInfo> a() {
        HashMap hashMap = new HashMap();
        for (n a2 : this.f4685a.values()) {
            hashMap.putAll(a2.a());
        }
        return hashMap;
    }

    public Set<Class<? extends bh>> b() {
        return this.f4685a.keySet();
    }

    public boolean c() {
        for (Entry value : this.f4685a.entrySet()) {
            if (!((n) value.getValue()).c()) {
                return false;
            }
        }
        return true;
    }
}
