package io.realm.internal.b;

import io.realm.bb;
import io.realm.bh;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.Util;
import io.realm.internal.c;
import io.realm.internal.m;
import io.realm.internal.n;
import io.realm.internal.o;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class b extends n {

    /* renamed from: a reason: collision with root package name */
    private final n f4686a;

    /* renamed from: b reason: collision with root package name */
    private final Set<Class<? extends bh>> f4687b;

    public b(n nVar, Collection<Class<? extends bh>> collection) {
        this.f4686a = nVar;
        HashSet hashSet = new HashSet();
        if (nVar != null) {
            Set b2 = nVar.b();
            for (Class cls : collection) {
                if (b2.contains(cls)) {
                    hashSet.add(cls);
                }
            }
        }
        this.f4687b = Collections.unmodifiableSet(hashSet);
    }

    private void e(Class<? extends bh> cls) {
        if (!this.f4687b.contains(cls)) {
            throw new IllegalArgumentException(cls.getSimpleName() + " is not part of the schema for this Realm");
        }
    }

    public <E extends bh> E a(bb bbVar, E e, boolean z, Map<bh, m> map) {
        e(Util.a(e.getClass()));
        return this.f4686a.a(bbVar, e, z, map);
    }

    public <E extends bh> E a(Class<E> cls, Object obj, o oVar, c cVar, boolean z, List<String> list) {
        e(cls);
        return this.f4686a.a(cls, obj, oVar, cVar, z, list);
    }

    public c a(Class<? extends bh> cls, OsSchemaInfo osSchemaInfo) {
        e(cls);
        return this.f4686a.a(cls, osSchemaInfo);
    }

    public String a(Class<? extends bh> cls) {
        e(cls);
        return this.f4686a.a(cls);
    }

    public Map<Class<? extends bh>, OsObjectSchemaInfo> a() {
        HashMap hashMap = new HashMap();
        for (Entry entry : this.f4686a.a().entrySet()) {
            if (this.f4687b.contains(entry.getKey())) {
                hashMap.put(entry.getKey(), entry.getValue());
            }
        }
        return hashMap;
    }

    public Set<Class<? extends bh>> b() {
        return this.f4687b;
    }

    public boolean c() {
        if (this.f4686a == null) {
            return true;
        }
        return this.f4686a.c();
    }
}
