package io.realm.internal;

import io.realm.RealmFieldType;
import io.realm.bd;
import java.lang.ref.WeakReference;
import java.util.Date;

public class k implements o {

    /* renamed from: a reason: collision with root package name */
    private SharedRealm f4703a;

    /* renamed from: b reason: collision with root package name */
    private Collection f4704b;
    private bd<k> c;
    private WeakReference<a> d;
    private boolean e;

    public interface a {
        void b(o oVar);
    }

    private void f() {
        this.f4704b.a(this, this.c);
        this.f4704b = null;
        this.c = null;
        this.f4703a.removePendingRow(this);
    }

    private void g() {
        if (this.d == null) {
            throw new IllegalStateException("The 'frontEnd' has not been set.");
        }
        a aVar = (a) this.d.get();
        if (aVar == null) {
            f();
        } else if (this.f4704b.f()) {
            UncheckedRow b2 = this.f4704b.b();
            f();
            if (b2 != null) {
                if (this.e) {
                    b2 = CheckedRow.a(b2);
                }
                aVar.b(b2);
                return;
            }
            aVar.b(f.INSTANCE);
        } else {
            f();
        }
    }

    public long a() {
        throw new IllegalStateException("The pending query has not been executed.");
    }

    public long a(String str) {
        throw new IllegalStateException("The pending query has not been executed.");
    }

    public OsList a(long j, RealmFieldType realmFieldType) {
        throw new IllegalStateException("The pending query has not been executed.");
    }

    public void a(long j, double d2) {
        throw new IllegalStateException("The pending query has not been executed.");
    }

    public void a(long j, float f) {
        throw new IllegalStateException("The pending query has not been executed.");
    }

    public void a(long j, long j2) {
        throw new IllegalStateException("The pending query has not been executed.");
    }

    public void a(long j, String str) {
        throw new IllegalStateException("The pending query has not been executed.");
    }

    public void a(long j, boolean z) {
        throw new IllegalStateException("The pending query has not been executed.");
    }

    public boolean a(long j) {
        throw new IllegalStateException("The pending query has not been executed.");
    }

    public Table b() {
        throw new IllegalStateException("The pending query has not been executed.");
    }

    public void b(long j, long j2) {
        throw new IllegalStateException("The pending query has not been executed.");
    }

    public boolean b(long j) {
        throw new IllegalStateException("The pending query has not been executed.");
    }

    public long c() {
        throw new IllegalStateException("The pending query has not been executed.");
    }

    public void c(long j) {
        throw new IllegalStateException("The pending query has not been executed.");
    }

    public OsList d(long j) {
        throw new IllegalStateException("The pending query has not been executed.");
    }

    public boolean d() {
        return false;
    }

    public String e(long j) {
        throw new IllegalStateException("The pending query has not been executed.");
    }

    public void e() {
        if (this.f4704b == null) {
            throw new IllegalStateException("The query has been executed. This 'PendingRow' is not valid anymore.");
        }
        g();
    }

    public RealmFieldType f(long j) {
        throw new IllegalStateException("The pending query has not been executed.");
    }

    public long g(long j) {
        throw new IllegalStateException("The pending query has not been executed.");
    }

    public boolean h(long j) {
        throw new IllegalStateException("The pending query has not been executed.");
    }

    public float i(long j) {
        throw new IllegalStateException("The pending query has not been executed.");
    }

    public double j(long j) {
        throw new IllegalStateException("The pending query has not been executed.");
    }

    public Date k(long j) {
        throw new IllegalStateException("The pending query has not been executed.");
    }

    public String l(long j) {
        throw new IllegalStateException("The pending query has not been executed.");
    }

    public byte[] m(long j) {
        throw new IllegalStateException("The pending query has not been executed.");
    }

    public long n(long j) {
        throw new IllegalStateException("The pending query has not been executed.");
    }

    public void o(long j) {
        throw new IllegalStateException("The pending query has not been executed.");
    }
}
