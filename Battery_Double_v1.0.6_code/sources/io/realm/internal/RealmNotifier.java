package io.realm.internal;

import io.realm.bd;
import io.realm.internal.j.b;
import java.io.Closeable;
import java.util.ArrayList;
import java.util.List;

@Keep
public abstract class RealmNotifier implements Closeable {
    private final io.realm.internal.j.a<a> onChangeCallBack = new io.realm.internal.j.a<a>() {
        public void a(a aVar, Object obj) {
            if (RealmNotifier.this.sharedRealm != null && !RealmNotifier.this.sharedRealm.isClosed()) {
                aVar.a(obj);
            }
        }
    };
    private j<a> realmObserverPairs = new j<>();
    /* access modifiers changed from: private */
    public SharedRealm sharedRealm;
    private List<Runnable> transactionCallbacks = new ArrayList();

    private static class a<T> extends b<T, bd<T>> {
        public a(T t, bd<T> bdVar) {
            super(t, bdVar);
        }

        /* access modifiers changed from: private */
        public void a(T t) {
            if (t != null) {
                ((bd) this.f4702b).a(t);
            }
        }
    }

    protected RealmNotifier(SharedRealm sharedRealm2) {
        this.sharedRealm = sharedRealm2;
    }

    private void removeAllChangeListeners() {
        this.realmObserverPairs.b();
    }

    public <T> void addChangeListener(T t, bd<T> bdVar) {
        this.realmObserverPairs.a(new a(t, bdVar));
    }

    public void addTransactionCallback(Runnable runnable) {
        this.transactionCallbacks.add(runnable);
    }

    /* access modifiers changed from: 0000 */
    public void beforeNotify() {
        this.sharedRealm.invalidateIterators();
    }

    public void close() {
        removeAllChangeListeners();
    }

    /* access modifiers changed from: 0000 */
    public void didChange() {
        this.realmObserverPairs.a(this.onChangeCallBack);
        if (!this.transactionCallbacks.isEmpty()) {
            List<Runnable> list = this.transactionCallbacks;
            this.transactionCallbacks = new ArrayList();
            for (Runnable run : list) {
                run.run();
            }
        }
    }

    public int getListenersListSize() {
        return this.realmObserverPairs.c();
    }

    public abstract boolean post(Runnable runnable);

    public <E> void removeChangeListener(E e, bd<E> bdVar) {
        this.realmObserverPairs.a(e, bdVar);
    }

    public <E> void removeChangeListeners(E e) {
        this.realmObserverPairs.a((Object) e);
    }
}
