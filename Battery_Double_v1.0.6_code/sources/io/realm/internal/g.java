package io.realm.internal;

import java.lang.ref.ReferenceQueue;

public class g {

    /* renamed from: a reason: collision with root package name */
    static final g f4695a = new g();

    /* renamed from: b reason: collision with root package name */
    private static final ReferenceQueue<h> f4696b = new ReferenceQueue<>();
    private static final Thread c = new Thread(new d(f4696b));

    static {
        c.setName("RealmFinalizingDaemon");
        c.start();
    }

    /* access modifiers changed from: 0000 */
    public void a(h hVar) {
        new NativeObjectReference(this, hVar, f4696b);
    }
}
