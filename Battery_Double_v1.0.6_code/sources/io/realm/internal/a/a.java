package io.realm.internal.a;

import io.realm.RealmFieldType;
import io.realm.internal.c;
import java.util.List;
import java.util.Locale;
import java.util.Set;

class a extends c {
    private final io.realm.internal.a.c.a f;
    private final String g;

    a(io.realm.internal.a.c.a aVar, String str, String str2, Set<RealmFieldType> set, Set<RealmFieldType> set2) {
        super(str2, set, set2);
        this.g = str;
        this.f = aVar;
    }

    /* access modifiers changed from: protected */
    public void a(List<String> list) {
        RealmFieldType realmFieldType = null;
        int size = list.size();
        long[] jArr = new long[size];
        long[] jArr2 = new long[size];
        String str = this.g;
        int i = 0;
        String str2 = null;
        while (i < size) {
            String str3 = (String) list.get(i);
            if (str3 == null || str3.length() <= 0) {
                throw new IllegalArgumentException("Invalid query: Field descriptor contains an empty field.  A field description may not begin with or contain adjacent periods ('.').");
            }
            c a2 = this.f.a(str);
            if (a2 == null) {
                throw new IllegalArgumentException(String.format(Locale.US, "Invalid query: class '%s' not found in this schema.", new Object[]{str}));
            }
            io.realm.internal.c.a a3 = a2.a(str3);
            if (a3 == null) {
                throw new IllegalArgumentException(String.format(Locale.US, "Invalid query: field '%s' not found in class '%s'.", new Object[]{str3, str}));
            }
            RealmFieldType realmFieldType2 = a3.f4691b;
            if (i < size - 1) {
                a(str, str3, realmFieldType2);
                str = a3.c;
            }
            jArr[i] = a3.f4690a;
            jArr2[i] = realmFieldType2 != RealmFieldType.LINKING_OBJECTS ? 0 : this.f.b(a3.c);
            i++;
            realmFieldType = realmFieldType2;
            str2 = str3;
        }
        a(str, str2, realmFieldType, jArr, jArr2);
    }
}
