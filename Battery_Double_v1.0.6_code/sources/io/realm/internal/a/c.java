package io.realm.internal.a;

import io.realm.RealmFieldType;
import io.realm.internal.Table;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

public abstract class c {

    /* renamed from: a reason: collision with root package name */
    public static final Set<RealmFieldType> f4676a;

    /* renamed from: b reason: collision with root package name */
    public static final Set<RealmFieldType> f4677b;
    public static final Set<RealmFieldType> c;
    public static final Set<RealmFieldType> d;
    public static final Set<RealmFieldType> e = Collections.emptySet();
    private final List<String> f;
    private final Set<RealmFieldType> g;
    private final Set<RealmFieldType> h;
    private String i;
    private RealmFieldType j;
    private long[] k;
    private long[] l;

    public interface a {
        io.realm.internal.c a(String str);

        boolean a();

        long b(String str);
    }

    static {
        HashSet hashSet = new HashSet(3);
        hashSet.add(RealmFieldType.OBJECT);
        hashSet.add(RealmFieldType.LIST);
        hashSet.add(RealmFieldType.LINKING_OBJECTS);
        f4676a = Collections.unmodifiableSet(hashSet);
        HashSet hashSet2 = new HashSet(2);
        hashSet2.add(RealmFieldType.OBJECT);
        hashSet2.add(RealmFieldType.LIST);
        f4677b = Collections.unmodifiableSet(hashSet2);
        HashSet hashSet3 = new HashSet(1);
        hashSet3.add(RealmFieldType.LIST);
        c = Collections.unmodifiableSet(hashSet3);
        HashSet hashSet4 = new HashSet(1);
        hashSet4.add(RealmFieldType.OBJECT);
        d = Collections.unmodifiableSet(hashSet4);
    }

    protected c(String str, Set<RealmFieldType> set, Set<RealmFieldType> set2) {
        this.f = a(str);
        if (this.f.size() <= 0) {
            throw new IllegalArgumentException("Invalid query: Empty field descriptor");
        }
        this.g = set;
        this.h = set2;
    }

    public static c a(a aVar, Table table, String str, Set<RealmFieldType> set, Set<RealmFieldType> set2) {
        c cVar;
        if (aVar == null || !aVar.a()) {
            if (set == null) {
                set = f4677b;
            }
            cVar = new b(table, str, set, set2);
        } else {
            cVar = new a(aVar, table.h(), str, set != null ? set : f4676a, set2);
        }
        return cVar;
    }

    public static c a(a aVar, Table table, String str, RealmFieldType... realmFieldTypeArr) {
        return a(aVar, table, str, null, (Set<RealmFieldType>) new HashSet<RealmFieldType>(Arrays.asList(realmFieldTypeArr)));
    }

    private List<String> a(String str) {
        if (str == null || str.equals("")) {
            throw new IllegalArgumentException("Invalid query: field name is empty");
        } else if (!str.endsWith(".")) {
            return Arrays.asList(str.split("\\."));
        } else {
            throw new IllegalArgumentException("Invalid query: field name must not end with a period ('.')");
        }
    }

    private void a(String str, String str2, RealmFieldType realmFieldType, Set<RealmFieldType> set) {
        if (!set.contains(realmFieldType)) {
            throw new IllegalArgumentException(String.format(Locale.US, "Invalid query: field '%s' in class '%s' is of invalid type '%s'.", new Object[]{str2, str, realmFieldType.toString()}));
        }
    }

    private void e() {
        if (this.j == null) {
            a(this.f);
        }
    }

    /* access modifiers changed from: protected */
    public final void a(String str, String str2, RealmFieldType realmFieldType) {
        a(str, str2, realmFieldType, this.g);
    }

    /* access modifiers changed from: protected */
    public final void a(String str, String str2, RealmFieldType realmFieldType, long[] jArr, long[] jArr2) {
        if (this.h != null && this.h.size() > 0) {
            a(str, str2, realmFieldType, this.h);
        }
        this.i = str2;
        this.j = realmFieldType;
        this.k = jArr;
        this.l = jArr2;
    }

    /* access modifiers changed from: protected */
    public abstract void a(List<String> list);

    public final long[] a() {
        e();
        return Arrays.copyOf(this.k, this.k.length);
    }

    public final long[] b() {
        e();
        return Arrays.copyOf(this.l, this.l.length);
    }

    public final String c() {
        e();
        return this.i;
    }

    public final RealmFieldType d() {
        e();
        return this.j;
    }
}
