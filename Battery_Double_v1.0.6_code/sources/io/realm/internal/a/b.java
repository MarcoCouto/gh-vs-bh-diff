package io.realm.internal.a;

import io.realm.RealmFieldType;
import io.realm.internal.Table;
import java.util.List;
import java.util.Locale;
import java.util.Set;

class b extends c {
    private final Table f;

    b(Table table, String str, Set<RealmFieldType> set, Set<RealmFieldType> set2) {
        super(str, set, set2);
        this.f = table;
    }

    /* access modifiers changed from: protected */
    public void a(List<String> list) {
        Table table;
        RealmFieldType realmFieldType = null;
        int size = list.size();
        long[] jArr = new long[size];
        Table table2 = this.f;
        int i = 0;
        String str = null;
        String str2 = null;
        while (i < size) {
            String str3 = (String) list.get(i);
            if (str3 == null || str3.length() <= 0) {
                throw new IllegalArgumentException("Invalid query: Field descriptor contains an empty field.  A field description may not begin with or contain adjacent periods ('.').");
            }
            String h = table2.h();
            long a2 = table2.a(str3);
            if (a2 < 0) {
                throw new IllegalArgumentException(String.format(Locale.US, "Invalid query: field '%s' not found in table '%s'.", new Object[]{str3, h}));
            }
            realmFieldType = table2.c(a2);
            if (i < size - 1) {
                a(h, str3, realmFieldType);
                table = table2.e(a2);
            } else {
                table = table2;
            }
            jArr[i] = a2;
            i++;
            table2 = table;
            str2 = h;
            str = str3;
        }
        a(str2, str, realmFieldType, jArr, new long[size]);
    }
}
