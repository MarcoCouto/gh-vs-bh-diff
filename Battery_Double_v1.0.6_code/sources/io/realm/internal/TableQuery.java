package io.realm.internal;

import io.realm.r;

public class TableQuery implements h {

    /* renamed from: a reason: collision with root package name */
    private static final long f4672a = nativeGetFinalizerPtr();

    /* renamed from: b reason: collision with root package name */
    private final g f4673b;
    private final Table c;
    private final long d;
    private boolean e = true;

    public TableQuery(g gVar, Table table, long j) {
        this.f4673b = gVar;
        this.c = table;
        this.d = j;
        gVar.a(this);
    }

    private native void nativeBetween(long j, long[] jArr, long j2, long j3);

    private native long nativeCount(long j, long j2, long j3, long j4);

    private native void nativeEqual(long j, long[] jArr, long[] jArr2, long j2);

    private native void nativeEqual(long j, long[] jArr, long[] jArr2, String str, boolean z);

    private native long nativeFind(long j, long j2);

    private static native long nativeGetFinalizerPtr();

    private native void nativeIsNull(long j, long[] jArr, long[] jArr2);

    private native void nativeLess(long j, long[] jArr, long[] jArr2, long j2);

    private native String nativeValidateQuery(long j);

    public Table a() {
        return this.c;
    }

    public TableQuery a(long[] jArr, long j, long j2) {
        nativeBetween(this.d, jArr, j, j2);
        this.e = false;
        return this;
    }

    public TableQuery a(long[] jArr, long[] jArr2) {
        nativeIsNull(this.d, jArr, jArr2);
        this.e = false;
        return this;
    }

    public TableQuery a(long[] jArr, long[] jArr2, long j) {
        nativeEqual(this.d, jArr, jArr2, j);
        this.e = false;
        return this;
    }

    public TableQuery a(long[] jArr, long[] jArr2, String str, r rVar) {
        nativeEqual(this.d, jArr, jArr2, str, rVar.a());
        this.e = false;
        return this;
    }

    public TableQuery b(long[] jArr, long[] jArr2, long j) {
        nativeLess(this.d, jArr, jArr2, j);
        this.e = false;
        return this;
    }

    /* access modifiers changed from: 0000 */
    public void b() {
        if (!this.e) {
            String nativeValidateQuery = nativeValidateQuery(this.d);
            if (nativeValidateQuery.equals("")) {
                this.e = true;
                return;
            }
            throw new UnsupportedOperationException(nativeValidateQuery);
        }
    }

    public long c() {
        b();
        return nativeFind(this.d, 0);
    }

    public long d() {
        b();
        return nativeCount(this.d, 0, -1, -1);
    }

    public long getNativeFinalizerPtr() {
        return f4672a;
    }

    public long getNativePtr() {
        return this.d;
    }
}
