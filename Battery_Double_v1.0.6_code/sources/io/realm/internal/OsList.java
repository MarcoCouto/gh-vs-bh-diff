package io.realm.internal;

import io.realm.internal.ObservableCollection.b;
import io.realm.internal.j.a;
import java.util.Date;

public class OsList implements ObservableCollection, h {
    private static final long d = nativeGetFinalizerPtr();

    /* renamed from: a reason: collision with root package name */
    private final long f4644a;

    /* renamed from: b reason: collision with root package name */
    private final g f4645b;
    private final Table c;
    private final j<b> e = new j<>();

    public OsList(UncheckedRow uncheckedRow, long j) {
        SharedRealm c2 = uncheckedRow.b().c();
        long[] nativeCreate = nativeCreate(c2.getNativePtr(), uncheckedRow.getNativePtr(), j);
        this.f4644a = nativeCreate[0];
        this.f4645b = c2.context;
        this.f4645b.a(this);
        if (nativeCreate[1] != 0) {
            this.c = new Table(c2, nativeCreate[1]);
        } else {
            this.c = null;
        }
    }

    private static native void nativeAddBinary(long j, byte[] bArr);

    private static native void nativeAddBoolean(long j, boolean z);

    private static native void nativeAddDate(long j, long j2);

    private static native void nativeAddDouble(long j, double d2);

    private static native void nativeAddFloat(long j, float f);

    private static native void nativeAddLong(long j, long j2);

    private static native void nativeAddNull(long j);

    private static native void nativeAddRow(long j, long j2);

    private static native void nativeAddString(long j, String str);

    private static native long[] nativeCreate(long j, long j2, long j3);

    private static native long nativeGetFinalizerPtr();

    private static native long nativeGetRow(long j, long j2);

    private static native Object nativeGetValue(long j, long j2);

    private static native void nativeInsertBinary(long j, long j2, byte[] bArr);

    private static native void nativeInsertBoolean(long j, long j2, boolean z);

    private static native void nativeInsertDate(long j, long j2, long j3);

    private static native void nativeInsertDouble(long j, long j2, double d2);

    private static native void nativeInsertFloat(long j, long j2, float f);

    private static native void nativeInsertLong(long j, long j2, long j3);

    private static native void nativeInsertNull(long j, long j2);

    private static native void nativeInsertRow(long j, long j2, long j3);

    private static native void nativeInsertString(long j, long j2, String str);

    private static native boolean nativeIsValid(long j);

    private static native void nativeRemove(long j, long j2);

    private static native void nativeRemoveAll(long j);

    private static native void nativeSetBinary(long j, long j2, byte[] bArr);

    private static native void nativeSetBoolean(long j, long j2, boolean z);

    private static native void nativeSetDate(long j, long j2, long j3);

    private static native void nativeSetDouble(long j, long j2, double d2);

    private static native void nativeSetFloat(long j, long j2, float f);

    private static native void nativeSetLong(long j, long j2, long j3);

    private static native void nativeSetNull(long j, long j2);

    private static native void nativeSetRow(long j, long j2, long j3);

    private static native void nativeSetString(long j, long j2, String str);

    private static native long nativeSize(long j);

    public UncheckedRow a(long j) {
        return this.c.g(nativeGetRow(this.f4644a, j));
    }

    public void a() {
        nativeAddNull(this.f4644a);
    }

    public void a(double d2) {
        nativeAddDouble(this.f4644a, d2);
    }

    public void a(float f) {
        nativeAddFloat(this.f4644a, f);
    }

    public void a(long j, double d2) {
        nativeInsertDouble(this.f4644a, j, d2);
    }

    public void a(long j, float f) {
        nativeInsertFloat(this.f4644a, j, f);
    }

    public void a(long j, long j2) {
        nativeInsertRow(this.f4644a, j, j2);
    }

    public void a(long j, String str) {
        nativeInsertString(this.f4644a, j, str);
    }

    public void a(long j, Date date) {
        if (date == null) {
            nativeInsertNull(this.f4644a, j);
            return;
        }
        nativeInsertDate(this.f4644a, j, date.getTime());
    }

    public void a(long j, boolean z) {
        nativeInsertBoolean(this.f4644a, j, z);
    }

    public void a(long j, byte[] bArr) {
        nativeInsertBinary(this.f4644a, j, bArr);
    }

    public void a(String str) {
        nativeAddString(this.f4644a, str);
    }

    public void a(Date date) {
        if (date == null) {
            nativeAddNull(this.f4644a);
        } else {
            nativeAddDate(this.f4644a, date.getTime());
        }
    }

    public void a(boolean z) {
        nativeAddBoolean(this.f4644a, z);
    }

    public void a(byte[] bArr) {
        nativeAddBinary(this.f4644a, bArr);
    }

    public void b() {
        nativeRemoveAll(this.f4644a);
    }

    public void b(long j) {
        nativeAddRow(this.f4644a, j);
    }

    public void b(long j, double d2) {
        nativeSetDouble(this.f4644a, j, d2);
    }

    public void b(long j, float f) {
        nativeSetFloat(this.f4644a, j, f);
    }

    public void b(long j, long j2) {
        nativeSetRow(this.f4644a, j, j2);
    }

    public void b(long j, String str) {
        nativeSetString(this.f4644a, j, str);
    }

    public void b(long j, Date date) {
        if (date == null) {
            nativeSetNull(this.f4644a, j);
            return;
        }
        nativeSetDate(this.f4644a, j, date.getTime());
    }

    public void b(long j, boolean z) {
        nativeSetBoolean(this.f4644a, j, z);
    }

    public void b(long j, byte[] bArr) {
        nativeSetBinary(this.f4644a, j, bArr);
    }

    public long c() {
        return nativeSize(this.f4644a);
    }

    public void c(long j) {
        nativeInsertNull(this.f4644a, j);
    }

    public void c(long j, long j2) {
        nativeInsertLong(this.f4644a, j, j2);
    }

    public void d(long j) {
        nativeSetNull(this.f4644a, j);
    }

    public void d(long j, long j2) {
        nativeSetLong(this.f4644a, j, j2);
    }

    public boolean d() {
        return nativeIsValid(this.f4644a);
    }

    public void e(long j) {
        nativeAddLong(this.f4644a, j);
    }

    public Object f(long j) {
        return nativeGetValue(this.f4644a, j);
    }

    public void g(long j) {
        nativeRemove(this.f4644a, j);
    }

    public long getNativeFinalizerPtr() {
        return d;
    }

    public long getNativePtr() {
        return this.f4644a;
    }

    public void notifyChangeListeners(long j) {
        if (j != 0) {
            this.e.a((a<T>) new ObservableCollection.a<T>(new OsCollectionChangeSet(j)));
        }
    }
}
