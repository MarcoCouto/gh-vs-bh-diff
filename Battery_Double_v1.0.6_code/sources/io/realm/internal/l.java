package io.realm.internal;

import android.content.Context;
import com.a.a.c;
import java.io.File;

public class l {

    /* renamed from: a reason: collision with root package name */
    private static final String f4705a = File.separator;

    /* renamed from: b reason: collision with root package name */
    private static final String f4706b = File.pathSeparator;
    private static final String c = ("lib" + f4706b + ".." + f4705a + "lib");
    private static volatile boolean d = false;

    public static synchronized void a(Context context) {
        synchronized (l.class) {
            if (!d) {
                c.a(context, "realm-jni", "4.1.0");
                d = true;
            }
        }
    }
}
