package io.realm.internal;

import io.realm.bh;
import io.realm.bj;
import io.realm.log.RealmLog;
import java.io.File;
import java.util.Locale;

public class Util {
    public static Class<? extends bh> a(Class<? extends bh> cls) {
        Class superclass = cls.getSuperclass();
        return (superclass.equals(Object.class) || superclass.equals(bj.class)) ? cls : superclass;
    }

    public static String a() {
        return nativeGetTablePrefix();
    }

    public static boolean a(String str) {
        return str == null || str.length() == 0;
    }

    public static boolean a(String str, File file, String str2) {
        String str3 = ".management";
        File file2 = new File(file, str2 + ".management");
        File file3 = new File(str);
        File[] listFiles = file2.listFiles();
        if (listFiles != null) {
            for (File file4 : listFiles) {
                if (!file4.delete()) {
                    RealmLog.a(String.format(Locale.ENGLISH, "Realm temporary file at %s cannot be deleted", new Object[]{file4.getAbsolutePath()}), new Object[0]);
                }
            }
        }
        if (file2.exists() && !file2.delete()) {
            RealmLog.a(String.format(Locale.ENGLISH, "Realm temporary folder at %s cannot be deleted", new Object[]{file2.getAbsolutePath()}), new Object[0]);
        }
        if (!file3.exists()) {
            return true;
        }
        boolean delete = file3.delete();
        if (delete) {
            return delete;
        }
        RealmLog.a(String.format(Locale.ENGLISH, "Realm file at %s cannot be deleted", new Object[]{file3.getAbsolutePath()}), new Object[0]);
        return delete;
    }

    static native String nativeGetTablePrefix();
}
