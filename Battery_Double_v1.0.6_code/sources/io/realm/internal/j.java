package io.realm.internal;

import io.realm.internal.j.b;
import java.lang.ref.WeakReference;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class j<T extends b> {

    /* renamed from: a reason: collision with root package name */
    private List<T> f4699a = new CopyOnWriteArrayList();

    /* renamed from: b reason: collision with root package name */
    private boolean f4700b = false;

    public interface a<T extends b> {
        void a(T t, Object obj);
    }

    public static abstract class b<T, S> {

        /* renamed from: a reason: collision with root package name */
        final WeakReference<T> f4701a;

        /* renamed from: b reason: collision with root package name */
        protected final S f4702b;
        boolean c = false;

        b(T t, S s) {
            this.f4702b = s;
            this.f4701a = new WeakReference<>(t);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof b)) {
                return false;
            }
            b bVar = (b) obj;
            return this.f4702b.equals(bVar.f4702b) && this.f4701a.get() == bVar.f4701a.get();
        }

        public int hashCode() {
            int i = 0;
            Object obj = this.f4701a.get();
            int hashCode = ((obj != null ? obj.hashCode() : 0) + 527) * 31;
            if (this.f4702b != null) {
                i = this.f4702b.hashCode();
            }
            return hashCode + i;
        }
    }

    public void a(a<T> aVar) {
        for (T t : this.f4699a) {
            if (!this.f4700b) {
                Object obj = t.f4701a.get();
                if (obj == null) {
                    this.f4699a.remove(t);
                } else if (!t.c) {
                    aVar.a(t, obj);
                }
            } else {
                return;
            }
        }
    }

    public void a(T t) {
        if (!this.f4699a.contains(t)) {
            this.f4699a.add(t);
            t.c = false;
        }
        if (this.f4700b) {
            this.f4700b = false;
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(Object obj) {
        for (T t : this.f4699a) {
            Object obj2 = t.f4701a.get();
            if (obj2 == null || obj2 == obj) {
                t.c = true;
                this.f4699a.remove(t);
            }
        }
    }

    public <S, U> void a(S s, U u) {
        for (T t : this.f4699a) {
            if (s == t.f4701a.get() && u.equals(t.f4702b)) {
                t.c = true;
                this.f4699a.remove(t);
                return;
            }
        }
    }

    public boolean a() {
        return this.f4699a.isEmpty();
    }

    public void b() {
        this.f4700b = true;
        this.f4699a.clear();
    }

    public int c() {
        return this.f4699a.size();
    }
}
