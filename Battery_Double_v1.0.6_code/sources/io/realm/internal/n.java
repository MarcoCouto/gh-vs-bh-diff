package io.realm.internal;

import io.realm.bb;
import io.realm.bh;
import io.realm.exceptions.RealmException;
import java.util.List;
import java.util.Map;
import java.util.Set;

public abstract class n {
    protected static void c(Class<? extends bh> cls) {
        if (cls == null) {
            throw new NullPointerException("A class extending RealmObject must be provided");
        }
    }

    protected static RealmException d(Class<? extends bh> cls) {
        return new RealmException(String.format("'%s' is not part of the schema for this Realm.", new Object[]{cls.toString()}));
    }

    public abstract <E extends bh> E a(bb bbVar, E e, boolean z, Map<bh, m> map);

    public abstract <E extends bh> E a(Class<E> cls, Object obj, o oVar, c cVar, boolean z, List<String> list);

    public abstract c a(Class<? extends bh> cls, OsSchemaInfo osSchemaInfo);

    @Deprecated
    public abstract String a(Class<? extends bh> cls);

    public abstract Map<Class<? extends bh>, OsObjectSchemaInfo> a();

    public String b(Class<? extends bh> cls) {
        return Table.b(a(Util.a(cls)));
    }

    public abstract Set<Class<? extends bh>> b();

    public boolean c() {
        return false;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof n)) {
            return false;
        }
        return b().equals(((n) obj).b());
    }

    public int hashCode() {
        return b().hashCode();
    }
}
