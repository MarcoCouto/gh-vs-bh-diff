package io.realm.internal;

import java.util.Collection;
import java.util.Iterator;

public class OsSchemaInfo implements h {

    /* renamed from: b reason: collision with root package name */
    private static final long f4661b = nativeGetFinalizerPtr();

    /* renamed from: a reason: collision with root package name */
    private long f4662a;
    private final SharedRealm c;

    OsSchemaInfo(long j, SharedRealm sharedRealm) {
        this.f4662a = j;
        this.c = sharedRealm;
    }

    public OsSchemaInfo(Collection<OsObjectSchemaInfo> collection) {
        this.f4662a = nativeCreateFromList(a(collection));
        g.f4695a.a(this);
        this.c = null;
    }

    private static long[] a(Collection<OsObjectSchemaInfo> collection) {
        long[] jArr = new long[collection.size()];
        int i = 0;
        Iterator it = collection.iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                return jArr;
            }
            jArr[i2] = ((OsObjectSchemaInfo) it.next()).getNativePtr();
            i = i2 + 1;
        }
    }

    private static native long nativeCreateFromList(long[] jArr);

    private static native long nativeGetFinalizerPtr();

    private static native long nativeGetObjectSchemaInfo(long j, String str);

    public OsObjectSchemaInfo a(String str) {
        return new OsObjectSchemaInfo(nativeGetObjectSchemaInfo(this.f4662a, str));
    }

    public long getNativeFinalizerPtr() {
        return f4661b;
    }

    public long getNativePtr() {
        return this.f4662a;
    }
}
