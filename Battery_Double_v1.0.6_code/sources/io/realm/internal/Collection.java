package io.realm.internal;

import io.realm.aw;
import io.realm.bd;
import io.realm.internal.ObservableCollection.c;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.NoSuchElementException;

public class Collection implements ObservableCollection, h {

    /* renamed from: b reason: collision with root package name */
    private static final long f4634b = nativeGetFinalizerPtr();

    /* renamed from: a reason: collision with root package name */
    private final long f4635a;
    /* access modifiers changed from: private */
    public final SharedRealm c;
    private final g d;
    private final Table e;
    private boolean f;
    /* access modifiers changed from: private */
    public boolean g;
    private final j<io.realm.internal.ObservableCollection.b> h;

    public static abstract class a<T> implements Iterator<T> {

        /* renamed from: b reason: collision with root package name */
        Collection f4636b;
        protected int c = -1;

        public a(Collection collection) {
            if (collection.c.isClosed()) {
                throw new IllegalStateException("This Realm instance has already been closed, making it unusable.");
            }
            this.f4636b = collection;
            if (!collection.g) {
                if (collection.c.isInTransaction()) {
                    a();
                } else {
                    this.f4636b.c.addIterator(this);
                }
            }
        }

        /* access modifiers changed from: 0000 */
        public T a(int i) {
            return a(this.f4636b.a(i));
        }

        /* access modifiers changed from: protected */
        public abstract T a(UncheckedRow uncheckedRow);

        /* access modifiers changed from: 0000 */
        public void a() {
            this.f4636b = this.f4636b.a();
        }

        /* access modifiers changed from: 0000 */
        public void b() {
            this.f4636b = null;
        }

        /* access modifiers changed from: 0000 */
        public void c() {
            if (this.f4636b == null) {
                throw new ConcurrentModificationException("No outside changes to a Realm is allowed while iterating a living Realm collection.");
            }
        }

        public boolean hasNext() {
            c();
            return ((long) (this.c + 1)) < this.f4636b.d();
        }

        public T next() {
            c();
            this.c++;
            if (((long) this.c) < this.f4636b.d()) {
                return a(this.c);
            }
            throw new NoSuchElementException("Cannot access index " + this.c + " when size is " + this.f4636b.d() + ". Remember to check hasNext() before using next().");
        }

        @Deprecated
        public void remove() {
            throw new UnsupportedOperationException("remove() is not supported by RealmResults iterators.");
        }
    }

    public static abstract class b<T> extends a<T> implements ListIterator<T> {
        public b(Collection collection, int i) {
            super(collection);
            if (i < 0 || ((long) i) > this.f4636b.d()) {
                throw new IndexOutOfBoundsException("Starting location must be a valid index: [0, " + (this.f4636b.d() - 1) + "]. Yours was " + i);
            }
            this.c = i - 1;
        }

        @Deprecated
        public void add(T t) {
            throw new UnsupportedOperationException("Adding an element is not supported. Use Realm.createObject() instead.");
        }

        public boolean hasPrevious() {
            c();
            return this.c >= 0;
        }

        public int nextIndex() {
            c();
            return this.c + 1;
        }

        public T previous() {
            c();
            try {
                T a2 = a(this.c);
                this.c--;
                return a2;
            } catch (IndexOutOfBoundsException e) {
                throw new NoSuchElementException("Cannot access index less than zero. This was " + this.c + ". Remember to check hasPrevious() before using previous().");
            }
        }

        public int previousIndex() {
            c();
            return this.c;
        }

        @Deprecated
        public void set(T t) {
            throw new UnsupportedOperationException("Replacing and element is not supported.");
        }
    }

    private Collection(SharedRealm sharedRealm, Table table, long j) {
        this(sharedRealm, table, j, false);
    }

    Collection(SharedRealm sharedRealm, Table table, long j, boolean z) {
        this.g = false;
        this.h = new j<>();
        this.c = sharedRealm;
        this.d = sharedRealm.context;
        this.e = table;
        this.f4635a = j;
        this.d.a(this);
        this.f = z;
    }

    public Collection(SharedRealm sharedRealm, TableQuery tableQuery, SortDescriptor sortDescriptor, SortDescriptor sortDescriptor2) {
        this.g = false;
        this.h = new j<>();
        tableQuery.b();
        this.f4635a = nativeCreateResults(sharedRealm.getNativePtr(), tableQuery.getNativePtr(), sortDescriptor, sortDescriptor2);
        this.c = sharedRealm;
        this.d = sharedRealm.context;
        this.e = tableQuery.a();
        this.d.a(this);
        this.f = false;
    }

    private static native void nativeClear(long j);

    private static native long nativeCreateResults(long j, long j2, SortDescriptor sortDescriptor, SortDescriptor sortDescriptor2);

    private static native long nativeCreateSnapshot(long j);

    private static native long nativeFirstRow(long j);

    private static native long nativeGetFinalizerPtr();

    private static native long nativeGetRow(long j, int i);

    private static native boolean nativeIsValid(long j);

    private static native long nativeLastRow(long j);

    private static native long nativeSize(long j);

    private native void nativeStopListening(long j);

    public Collection a() {
        if (this.g) {
            return this;
        }
        Collection collection = new Collection(this.c, this.e, nativeCreateSnapshot(this.f4635a));
        collection.g = true;
        return collection;
    }

    public UncheckedRow a(int i) {
        return this.e.g(nativeGetRow(this.f4635a, i));
    }

    public <T> void a(T t, aw<T> awVar) {
        this.h.a(t, awVar);
        if (this.h.a()) {
            nativeStopListening(this.f4635a);
        }
    }

    public <T> void a(T t, bd<T> bdVar) {
        a(t, (aw<T>) new c<T>(bdVar));
    }

    public UncheckedRow b() {
        long nativeFirstRow = nativeFirstRow(this.f4635a);
        if (nativeFirstRow != 0) {
            return this.e.g(nativeFirstRow);
        }
        return null;
    }

    public UncheckedRow c() {
        long nativeLastRow = nativeLastRow(this.f4635a);
        if (nativeLastRow != 0) {
            return this.e.g(nativeLastRow);
        }
        return null;
    }

    public long d() {
        return nativeSize(this.f4635a);
    }

    public void e() {
        nativeClear(this.f4635a);
    }

    public boolean f() {
        return nativeIsValid(this.f4635a);
    }

    public boolean g() {
        return this.f;
    }

    public long getNativeFinalizerPtr() {
        return f4634b;
    }

    public long getNativePtr() {
        return this.f4635a;
    }

    public void h() {
        if (!this.f) {
            notifyChangeListeners(0);
        }
    }

    public void notifyChangeListeners(long j) {
        if (j != 0 || !g()) {
            boolean z = this.f;
            this.f = true;
            this.h.a((io.realm.internal.j.a<T>) new io.realm.internal.ObservableCollection.a<T>((j == 0 || !z) ? null : new OsCollectionChangeSet(j)));
        }
    }
}
