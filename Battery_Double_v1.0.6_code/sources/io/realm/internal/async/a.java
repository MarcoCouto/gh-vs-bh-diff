package io.realm.internal.async;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import java.io.File;
import java.io.FileFilter;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;
import java.util.regex.Pattern;

public class a extends ThreadPoolExecutor {

    /* renamed from: a reason: collision with root package name */
    private static final int f4680a = b();

    /* renamed from: b reason: collision with root package name */
    private boolean f4681b;
    private ReentrantLock c = new ReentrantLock();
    private Condition d = this.c.newCondition();

    private a(int i, int i2) {
        super(i, i2, 0, TimeUnit.MILLISECONDS, new ArrayBlockingQueue(100));
    }

    private static int a(String str, String str2) {
        final Pattern compile = Pattern.compile(str2);
        try {
            File[] listFiles = new File(str).listFiles(new FileFilter() {
                public boolean accept(File file) {
                    return compile.matcher(file.getName()).matches();
                }
            });
            if (listFiles == null) {
                return 0;
            }
            return listFiles.length;
        } catch (SecurityException e) {
            return 0;
        }
    }

    public static a a() {
        return new a(f4680a, f4680a);
    }

    @SuppressFBWarnings({"DMI_HARDCODED_ABSOLUTE_FILENAME"})
    private static int b() {
        int a2 = a("/sys/devices/system/cpu/", "cpu[0-9]+");
        if (a2 <= 0) {
            a2 = Runtime.getRuntime().availableProcessors();
        }
        if (a2 <= 0) {
            return 1;
        }
        return (a2 * 2) + 1;
    }

    /* access modifiers changed from: protected */
    public void beforeExecute(Thread thread, Runnable runnable) {
        super.beforeExecute(thread, runnable);
        this.c.lock();
        while (this.f4681b) {
            try {
                this.d.await();
            } catch (InterruptedException e) {
                thread.interrupt();
                return;
            } finally {
                this.c.unlock();
            }
        }
    }
}
