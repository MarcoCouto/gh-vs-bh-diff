package io.realm.internal;

import io.realm.RealmFieldType;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public abstract class c {

    /* renamed from: a reason: collision with root package name */
    private final Map<String, a> f4688a;

    /* renamed from: b reason: collision with root package name */
    private final boolean f4689b;

    public static final class a {

        /* renamed from: a reason: collision with root package name */
        public final long f4690a;

        /* renamed from: b reason: collision with root package name */
        public final RealmFieldType f4691b;
        public final String c;

        private a(long j, RealmFieldType realmFieldType, String str) {
            this.f4690a = j;
            this.f4691b = realmFieldType;
            this.c = str;
        }

        a(Property property) {
            this(property.c(), property.a(), property.b());
        }

        public String toString() {
            StringBuilder sb = new StringBuilder("ColumnDetails[");
            sb.append(this.f4690a);
            sb.append(", ").append(this.f4691b);
            sb.append(", ").append(this.c);
            return sb.append("]").toString();
        }
    }

    protected c(int i) {
        this(i, true);
    }

    private c(int i, boolean z) {
        this.f4688a = new HashMap(i);
        this.f4689b = z;
    }

    protected c(c cVar, boolean z) {
        this(cVar == null ? 0 : cVar.f4688a.size(), z);
        if (cVar != null) {
            this.f4688a.putAll(cVar.f4688a);
        }
    }

    /* access modifiers changed from: protected */
    public final long a(String str, OsObjectSchemaInfo osObjectSchemaInfo) {
        Property a2 = osObjectSchemaInfo.a(str);
        this.f4688a.put(str, new a(a2));
        return a2.c();
    }

    public a a(String str) {
        return (a) this.f4688a.get(str);
    }

    public void a(c cVar) {
        if (!this.f4689b) {
            throw new UnsupportedOperationException("Attempt to modify an immutable ColumnInfo");
        } else if (cVar == null) {
            throw new NullPointerException("Attempt to copy null ColumnInfo");
        } else {
            this.f4688a.clear();
            this.f4688a.putAll(cVar.f4688a);
            a(cVar, this);
        }
    }

    /* access modifiers changed from: protected */
    public abstract void a(c cVar, c cVar2);

    public String toString() {
        StringBuilder sb = new StringBuilder("ColumnInfo[");
        sb.append(this.f4689b).append(",");
        if (this.f4688a != null) {
            boolean z = false;
            Iterator it = this.f4688a.entrySet().iterator();
            while (true) {
                boolean z2 = z;
                if (!it.hasNext()) {
                    break;
                }
                Entry entry = (Entry) it.next();
                if (z2) {
                    sb.append(",");
                }
                sb.append((String) entry.getKey()).append("->").append(entry.getValue());
                z = true;
            }
        }
        return sb.append("]").toString();
    }
}
