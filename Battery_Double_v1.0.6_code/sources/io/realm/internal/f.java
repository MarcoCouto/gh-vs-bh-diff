package io.realm.internal;

import io.realm.RealmFieldType;
import java.util.Date;

public enum f implements o {
    INSTANCE;

    private RuntimeException e() {
        return new IllegalStateException("Object is no longer managed by Realm. Has it been deleted?");
    }

    public long a() {
        throw e();
    }

    public long a(String str) {
        throw e();
    }

    public OsList a(long j, RealmFieldType realmFieldType) {
        throw e();
    }

    public void a(long j, double d) {
        throw e();
    }

    public void a(long j, float f) {
        throw e();
    }

    public void a(long j, long j2) {
        throw e();
    }

    public void a(long j, String str) {
        throw e();
    }

    public void a(long j, boolean z) {
        throw e();
    }

    public boolean a(long j) {
        throw e();
    }

    public Table b() {
        throw e();
    }

    public void b(long j, long j2) {
        throw e();
    }

    public boolean b(long j) {
        throw e();
    }

    public long c() {
        throw e();
    }

    public void c(long j) {
        throw e();
    }

    public OsList d(long j) {
        throw e();
    }

    public boolean d() {
        return false;
    }

    public String e(long j) {
        throw e();
    }

    public RealmFieldType f(long j) {
        throw e();
    }

    public long g(long j) {
        throw e();
    }

    public boolean h(long j) {
        throw e();
    }

    public float i(long j) {
        throw e();
    }

    public double j(long j) {
        throw e();
    }

    public Date k(long j) {
        throw e();
    }

    public String l(long j) {
        throw e();
    }

    public byte[] m(long j) {
        throw e();
    }

    public long n(long j) {
        throw e();
    }

    public void o(long j) {
        throw e();
    }
}
