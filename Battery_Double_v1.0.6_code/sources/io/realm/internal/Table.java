package io.realm.internal;

import io.realm.RealmFieldType;

public class Table implements h {

    /* renamed from: a reason: collision with root package name */
    public static final int f4669a = (63 - f4670b.length());

    /* renamed from: b reason: collision with root package name */
    private static final String f4670b = Util.a();
    private static final long c = nativeGetFinalizerPtr();
    private final long d;
    private final g e;
    private final SharedRealm f;

    Table(SharedRealm sharedRealm, long j) {
        this.e = sharedRealm.context;
        this.f = sharedRealm;
        this.d = j;
        this.e.a(this);
    }

    public static void a(SharedRealm sharedRealm) {
        nativeMigratePrimaryKeyTableIfNeeded(sharedRealm.getNativePtr());
    }

    public static String b(String str) {
        if (str == null) {
            return null;
        }
        return str.startsWith(f4670b) ? str.substring(f4670b.length()) : str;
    }

    public static String c(String str) {
        if (str == null) {
            return null;
        }
        return !str.startsWith(f4670b) ? f4670b + str : str;
    }

    private void d(String str) {
        if (str.length() > 63) {
            throw new IllegalArgumentException("Column names are currently limited to max 63 characters.");
        }
    }

    private static void i() {
        throw new IllegalStateException("Cannot modify managed objects outside of a write transaction.");
    }

    private native long nativeAddColumn(long j, int i, String str, boolean z);

    private native long nativeAddPrimitiveListColumn(long j, int i, String str, boolean z);

    private native void nativeAddSearchIndex(long j, long j2);

    public static native long nativeFindFirstInt(long j, long j2, long j3);

    private native long nativeGetColumnCount(long j);

    private native long nativeGetColumnIndex(long j, String str);

    private native String nativeGetColumnName(long j, long j2);

    private native int nativeGetColumnType(long j, long j2);

    private static native long nativeGetFinalizerPtr();

    private native long nativeGetLinkTarget(long j, long j2);

    private native String nativeGetName(long j);

    private native boolean nativeHasSearchIndex(long j, long j2);

    private static native void nativeMigratePrimaryKeyTableIfNeeded(long j);

    private native void nativeMoveLastOver(long j, long j2);

    private native void nativeRemoveColumn(long j, long j2);

    private native void nativeRemoveSearchIndex(long j, long j2);

    public static native void nativeSetBoolean(long j, long j2, long j3, boolean z, boolean z2);

    public static native void nativeSetDouble(long j, long j2, long j3, double d2, boolean z);

    public static native void nativeSetFloat(long j, long j2, long j3, float f2, boolean z);

    public static native void nativeSetLink(long j, long j2, long j3, long j4, boolean z);

    public static native void nativeSetLong(long j, long j2, long j3, long j4, boolean z);

    public static native void nativeSetNull(long j, long j2, long j3, boolean z);

    public static native void nativeSetString(long j, long j2, long j3, String str, boolean z);

    private native long nativeSize(long j);

    private native long nativeWhere(long j);

    public long a() {
        return nativeSize(this.d);
    }

    public long a(long j, long j2) {
        return nativeFindFirstInt(this.d, j, j2);
    }

    public long a(RealmFieldType realmFieldType, String str, boolean z) {
        d(str);
        switch (realmFieldType) {
            case INTEGER:
            case BOOLEAN:
            case STRING:
            case BINARY:
            case DATE:
            case FLOAT:
            case DOUBLE:
                return nativeAddColumn(this.d, realmFieldType.getNativeValue(), str, z);
            case INTEGER_LIST:
            case BOOLEAN_LIST:
            case STRING_LIST:
            case BINARY_LIST:
            case DATE_LIST:
            case FLOAT_LIST:
            case DOUBLE_LIST:
                return nativeAddPrimitiveListColumn(this.d, realmFieldType.getNativeValue() - 128, str, z);
            default:
                throw new IllegalArgumentException("Unsupported type: " + realmFieldType);
        }
    }

    public long a(String str) {
        if (str != null) {
            return nativeGetColumnIndex(this.d, str);
        }
        throw new IllegalArgumentException("Column name can not be null.");
    }

    public void a(long j) {
        String h = h();
        String b2 = b(j);
        String a2 = OsObjectStore.a(this.f, h());
        nativeRemoveColumn(this.d, j);
        if (b2.equals(a2)) {
            OsObjectStore.a(this.f, h, null);
        }
    }

    public void a(long j, long j2, double d2, boolean z) {
        e();
        nativeSetDouble(this.d, j, j2, d2, z);
    }

    public void a(long j, long j2, float f2, boolean z) {
        e();
        nativeSetFloat(this.d, j, j2, f2, z);
    }

    public void a(long j, long j2, long j3, boolean z) {
        e();
        nativeSetLong(this.d, j, j2, j3, z);
    }

    public void a(long j, long j2, String str, boolean z) {
        e();
        if (str == null) {
            nativeSetNull(this.d, j, j2, z);
        } else {
            nativeSetString(this.d, j, j2, str, z);
        }
    }

    public void a(long j, long j2, boolean z) {
        e();
        nativeSetNull(this.d, j, j2, z);
    }

    public void a(long j, long j2, boolean z, boolean z2) {
        e();
        nativeSetBoolean(this.d, j, j2, z, z2);
    }

    public long b() {
        return nativeGetColumnCount(this.d);
    }

    public String b(long j) {
        return nativeGetColumnName(this.d, j);
    }

    public void b(long j, long j2, long j3, boolean z) {
        e();
        nativeSetLink(this.d, j, j2, j3, z);
    }

    public RealmFieldType c(long j) {
        return RealmFieldType.fromNativeValue(nativeGetColumnType(this.d, j));
    }

    /* access modifiers changed from: 0000 */
    public SharedRealm c() {
        return this.f;
    }

    public void d(long j) {
        e();
        nativeMoveLastOver(this.d, j);
    }

    /* access modifiers changed from: 0000 */
    public boolean d() {
        return this.f != null && !this.f.isInTransaction();
    }

    public Table e(long j) {
        return new Table(this.f, nativeGetLinkTarget(this.d, j));
    }

    /* access modifiers changed from: 0000 */
    public void e() {
        if (d()) {
            i();
        }
    }

    public TableQuery f() {
        return new TableQuery(this.e, this, nativeWhere(this.d));
    }

    public UncheckedRow f(long j) {
        return UncheckedRow.b(this.e, this, j);
    }

    public UncheckedRow g(long j) {
        return UncheckedRow.c(this.e, this, j);
    }

    public String g() {
        return nativeGetName(this.d);
    }

    public long getNativeFinalizerPtr() {
        return c;
    }

    public long getNativePtr() {
        return this.d;
    }

    public CheckedRow h(long j) {
        return CheckedRow.a(this.e, this, j);
    }

    public String h() {
        return b(g());
    }

    public void i(long j) {
        e();
        nativeAddSearchIndex(this.d, j);
    }

    public void j(long j) {
        e();
        nativeRemoveSearchIndex(this.d, j);
    }

    public boolean k(long j) {
        return nativeHasSearchIndex(this.d, j);
    }

    /* access modifiers changed from: 0000 */
    public native long nativeGetRowPtr(long j, long j2);

    public String toString() {
        long b2 = b();
        String g = g();
        StringBuilder sb = new StringBuilder("The Table ");
        if (g != null && !g.isEmpty()) {
            sb.append(g());
            sb.append(" ");
        }
        sb.append("contains ");
        sb.append(b2);
        sb.append(" columns: ");
        for (int i = 0; ((long) i) < b2; i++) {
            if (i != 0) {
                sb.append(", ");
            }
            sb.append(b((long) i));
        }
        sb.append(".");
        sb.append(" And ");
        sb.append(a());
        sb.append(" rows.");
        return sb.toString();
    }
}
