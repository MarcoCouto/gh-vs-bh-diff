package io.realm.internal;

import io.realm.RealmFieldType;

public class OsObjectSchemaInfo implements h {

    /* renamed from: b reason: collision with root package name */
    private static final long f4649b = nativeGetFinalizerPtr();
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public long f4650a;

    public static class a {

        /* renamed from: a reason: collision with root package name */
        private final String f4651a;

        /* renamed from: b reason: collision with root package name */
        private final long[] f4652b;
        private int c = 0;
        private final long[] d;
        private int e = 0;

        public a(String str, int i, int i2) {
            this.f4651a = str;
            this.f4652b = new long[i];
            this.d = new long[i2];
        }

        public a a(String str, RealmFieldType realmFieldType, String str2) {
            this.f4652b[this.c] = Property.nativeCreatePersistedLinkProperty(str, Property.a(realmFieldType, false), str2);
            this.c++;
            return this;
        }

        public a a(String str, RealmFieldType realmFieldType, boolean z, boolean z2, boolean z3) {
            this.f4652b[this.c] = Property.nativeCreatePersistedProperty(str, Property.a(realmFieldType, z3), z, z2);
            this.c++;
            return this;
        }

        public OsObjectSchemaInfo a() {
            if (this.c == -1 || this.e == -1) {
                throw new IllegalStateException("'OsObjectSchemaInfo.build()' has been called before on this object.");
            }
            OsObjectSchemaInfo osObjectSchemaInfo = new OsObjectSchemaInfo(this.f4651a);
            OsObjectSchemaInfo.nativeAddProperties(osObjectSchemaInfo.f4650a, this.f4652b, this.d);
            this.c = -1;
            this.e = -1;
            return osObjectSchemaInfo;
        }
    }

    OsObjectSchemaInfo(long j) {
        this.f4650a = j;
        g.f4695a.a(this);
    }

    private OsObjectSchemaInfo(String str) {
        this(nativeCreateRealmObjectSchema(str));
    }

    /* access modifiers changed from: private */
    public static native void nativeAddProperties(long j, long[] jArr, long[] jArr2);

    private static native long nativeCreateRealmObjectSchema(String str);

    private static native long nativeGetFinalizerPtr();

    private static native long nativeGetPrimaryKeyProperty(long j);

    private static native long nativeGetProperty(long j, String str);

    public Property a() {
        if (nativeGetPrimaryKeyProperty(this.f4650a) == 0) {
            return null;
        }
        return new Property(nativeGetPrimaryKeyProperty(this.f4650a));
    }

    public Property a(String str) {
        return new Property(nativeGetProperty(this.f4650a, str));
    }

    public long getNativeFinalizerPtr() {
        return f4649b;
    }

    public long getNativePtr() {
        return this.f4650a;
    }
}
