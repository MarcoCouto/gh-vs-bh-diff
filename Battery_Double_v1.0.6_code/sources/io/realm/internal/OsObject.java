package io.realm.internal;

import io.realm.RealmFieldType;
import io.realm.au;
import io.realm.bh;
import io.realm.bk;
import io.realm.exceptions.RealmException;

@Keep
public class OsObject implements h {
    private static final String OBJECT_ID_COLUMN_NAME = nativeGetObjectIdColumName();
    private static final long nativeFinalizerPtr = nativeGetFinalizerPtr();
    private final long nativePtr;
    private j<b> observerPairs = new j<>();

    private static class a implements io.realm.internal.j.a<b> {

        /* renamed from: a reason: collision with root package name */
        private final String[] f4646a;

        a(String[] strArr) {
            this.f4646a = strArr;
        }

        private au a() {
            boolean z = this.f4646a == null;
            return new c(z ? new String[0] : this.f4646a, z);
        }

        public void a(b bVar, Object obj) {
            bVar.a((bh) obj, a());
        }
    }

    public static class b<T extends bh> extends io.realm.internal.j.b<T, bk<T>> {
        public b(T t, bk<T> bkVar) {
            super(t, bkVar);
        }

        public void a(T t, au auVar) {
            ((bk) this.f4702b).a(t, auVar);
        }
    }

    private static class c implements au {

        /* renamed from: a reason: collision with root package name */
        final String[] f4647a;

        /* renamed from: b reason: collision with root package name */
        final boolean f4648b;

        c(String[] strArr, boolean z) {
            this.f4647a = strArr;
            this.f4648b = z;
        }
    }

    public OsObject(SharedRealm sharedRealm, UncheckedRow uncheckedRow) {
        this.nativePtr = nativeCreate(sharedRealm.getNativePtr(), uncheckedRow.getNativePtr());
        sharedRealm.context.a(this);
    }

    public static UncheckedRow create(Table table) {
        SharedRealm c2 = table.c();
        return new UncheckedRow(c2.context, table, nativeCreateNewObject(c2.getNativePtr(), table.getNativePtr()));
    }

    public static long createRow(Table table) {
        return nativeCreateRow(table.c().getNativePtr(), table.getNativePtr());
    }

    public static long createRowWithPrimaryKey(Table table, long j, Object obj) {
        RealmFieldType c2 = table.c(j);
        SharedRealm c3 = table.c();
        if (c2 == RealmFieldType.STRING) {
            if (obj == null || (obj instanceof String)) {
                return nativeCreateRowWithStringPrimaryKey(c3.getNativePtr(), table.getNativePtr(), j, (String) obj);
            }
            throw new IllegalArgumentException("Primary key value is not a String: " + obj);
        } else if (c2 == RealmFieldType.INTEGER) {
            return nativeCreateRowWithLongPrimaryKey(c3.getNativePtr(), table.getNativePtr(), j, obj == null ? 0 : Long.parseLong(obj.toString()), obj == null);
        } else {
            throw new RealmException("Cannot check for duplicate rows for unsupported primary key type: " + c2);
        }
    }

    public static UncheckedRow createWithPrimaryKey(Table table, Object obj) {
        long andVerifyPrimaryKeyColumnIndex = getAndVerifyPrimaryKeyColumnIndex(table);
        RealmFieldType c2 = table.c(andVerifyPrimaryKeyColumnIndex);
        SharedRealm c3 = table.c();
        if (c2 == RealmFieldType.STRING) {
            if (obj == null || (obj instanceof String)) {
                return new UncheckedRow(c3.context, table, nativeCreateNewObjectWithStringPrimaryKey(c3.getNativePtr(), table.getNativePtr(), andVerifyPrimaryKeyColumnIndex, (String) obj));
            }
            throw new IllegalArgumentException("Primary key value is not a String: " + obj);
        } else if (c2 == RealmFieldType.INTEGER) {
            return new UncheckedRow(c3.context, table, nativeCreateNewObjectWithLongPrimaryKey(c3.getNativePtr(), table.getNativePtr(), andVerifyPrimaryKeyColumnIndex, obj == null ? 0 : Long.parseLong(obj.toString()), obj == null));
        } else {
            throw new RealmException("Cannot check for duplicate rows for unsupported primary key type: " + c2);
        }
    }

    private static long getAndVerifyPrimaryKeyColumnIndex(Table table) {
        String a2 = OsObjectStore.a(table.c(), table.h());
        if (a2 != null) {
            return table.a(a2);
        }
        throw new IllegalStateException(table.g() + " has no primary key defined.");
    }

    public static boolean isObjectIdColumn(String str) {
        return OBJECT_ID_COLUMN_NAME.equals(str);
    }

    private static native long nativeCreate(long j, long j2);

    private static native long nativeCreateNewObject(long j, long j2);

    private static native long nativeCreateNewObjectWithLongPrimaryKey(long j, long j2, long j3, long j4, boolean z);

    private static native long nativeCreateNewObjectWithStringPrimaryKey(long j, long j2, long j3, String str);

    private static native long nativeCreateRow(long j, long j2);

    private static native long nativeCreateRowWithLongPrimaryKey(long j, long j2, long j3, long j4, boolean z);

    private static native long nativeCreateRowWithStringPrimaryKey(long j, long j2, long j3, String str);

    private static native long nativeGetFinalizerPtr();

    private static native String nativeGetObjectIdColumName();

    private native void nativeStartListening(long j);

    private native void nativeStopListening(long j);

    private void notifyChangeListeners(String[] strArr) {
        this.observerPairs.a((io.realm.internal.j.a<T>) new a<T>(strArr));
    }

    public <T extends bh> void addListener(T t, bk<T> bkVar) {
        if (this.observerPairs.a()) {
            nativeStartListening(this.nativePtr);
        }
        this.observerPairs.a(new b(t, bkVar));
    }

    public long getNativeFinalizerPtr() {
        return nativeFinalizerPtr;
    }

    public long getNativePtr() {
        return this.nativePtr;
    }

    public <T extends bh> void removeListener(T t) {
        this.observerPairs.a((Object) t);
        if (this.observerPairs.a()) {
            nativeStopListening(this.nativePtr);
        }
    }

    public <T extends bh> void removeListener(T t, bk<T> bkVar) {
        this.observerPairs.a(t, bkVar);
        if (this.observerPairs.a()) {
            nativeStopListening(this.nativePtr);
        }
    }

    public void setObserverPairs(j<b> jVar) {
        if (!this.observerPairs.a()) {
            throw new IllegalStateException("'observerPairs' is not empty. Listeners have been added before.");
        }
        this.observerPairs = jVar;
        if (!jVar.a()) {
            nativeStartListening(this.nativePtr);
        }
    }
}
