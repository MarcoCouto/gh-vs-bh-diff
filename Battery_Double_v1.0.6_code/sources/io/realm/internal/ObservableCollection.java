package io.realm.internal;

import io.realm.av;
import io.realm.aw;
import io.realm.bd;

@Keep
interface ObservableCollection {

    public static class a implements io.realm.internal.j.a<b> {

        /* renamed from: a reason: collision with root package name */
        private final av f4640a;

        a(av avVar) {
            this.f4640a = avVar;
        }

        public void a(b bVar, Object obj) {
            bVar.a(obj, this.f4640a);
        }
    }

    public static class b<T> extends io.realm.internal.j.b<T, Object> {
        public void a(T t, av avVar) {
            if (this.f4702b instanceof aw) {
                ((aw) this.f4702b).a(t, avVar);
            } else if (this.f4702b instanceof bd) {
                ((bd) this.f4702b).a(t);
            } else {
                throw new RuntimeException("Unsupported listener type: " + this.f4702b);
            }
        }
    }

    public static class c<T> implements aw<T> {

        /* renamed from: a reason: collision with root package name */
        private final bd<T> f4641a;

        c(bd<T> bdVar) {
            this.f4641a = bdVar;
        }

        public void a(T t, av avVar) {
            this.f4641a.a(t);
        }

        public boolean equals(Object obj) {
            return (obj instanceof c) && this.f4641a == ((c) obj).f4641a;
        }

        public int hashCode() {
            return this.f4641a.hashCode();
        }
    }

    void notifyChangeListeners(long j);
}
