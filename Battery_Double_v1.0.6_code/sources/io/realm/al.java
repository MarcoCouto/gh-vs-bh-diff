package io.realm;

import io.realm.internal.OsList;

abstract class al<T> {

    /* renamed from: a reason: collision with root package name */
    final e f4547a;

    /* renamed from: b reason: collision with root package name */
    final OsList f4548b;
    final Class<T> c;

    al(e eVar, OsList osList, Class<T> cls) {
        this.f4547a = eVar;
        this.c = cls;
        this.f4548b = osList;
    }

    private void d() {
        this.f4548b.a();
    }

    /* access modifiers changed from: protected */
    public abstract void a(int i, Object obj);

    /* access modifiers changed from: protected */
    public abstract void a(Object obj);

    public final boolean a() {
        return this.f4548b.d();
    }

    public final int b() {
        long c2 = this.f4548b.c();
        if (c2 < 2147483647L) {
            return (int) c2;
        }
        return Integer.MAX_VALUE;
    }

    public abstract T b(int i);

    /* access modifiers changed from: protected */
    public abstract void b(int i, Object obj);

    /* access modifiers changed from: protected */
    public abstract void b(Object obj);

    /* access modifiers changed from: 0000 */
    public final void c() {
        this.f4548b.b();
    }

    /* access modifiers changed from: protected */
    public void c(int i) {
        this.f4548b.c((long) i);
    }

    public final void c(int i, Object obj) {
        a(obj);
        if (obj == null) {
            c(i);
        } else {
            a(i, obj);
        }
    }

    public final void c(Object obj) {
        a(obj);
        if (obj == null) {
            d();
        } else {
            b(obj);
        }
    }

    public final T d(int i, Object obj) {
        a(obj);
        T b2 = b(i);
        if (obj == null) {
            d(i);
        } else {
            b(i, obj);
        }
        return b2;
    }

    /* access modifiers changed from: protected */
    public void d(int i) {
        this.f4548b.d((long) i);
    }

    /* access modifiers changed from: 0000 */
    public final void e(int i) {
        this.f4548b.g((long) i);
    }
}
