package io.realm;

import io.realm.internal.Keep;
import java.nio.ByteBuffer;
import java.util.Date;

@Keep
public enum RealmFieldType {
    INTEGER(0),
    BOOLEAN(1),
    STRING(2),
    BINARY(4),
    DATE(8),
    FLOAT(9),
    DOUBLE(10),
    OBJECT(12),
    LIST(13),
    LINKING_OBJECTS(14),
    INTEGER_LIST(128),
    BOOLEAN_LIST(129),
    STRING_LIST(130),
    BINARY_LIST(132),
    DATE_LIST(136),
    FLOAT_LIST(137),
    DOUBLE_LIST(138);
    
    private static final RealmFieldType[] basicTypes = null;
    private static final RealmFieldType[] listTypes = null;
    private final int nativeValue;

    static {
        int i;
        RealmFieldType[] values;
        basicTypes = new RealmFieldType[15];
        listTypes = new RealmFieldType[15];
        for (RealmFieldType realmFieldType : values()) {
            int i2 = realmFieldType.nativeValue;
            if (i2 < 128) {
                basicTypes[i2] = realmFieldType;
            } else {
                listTypes[i2 - 128] = realmFieldType;
            }
        }
    }

    private RealmFieldType(int i) {
        this.nativeValue = i;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001d, code lost:
        if (r0 == null) goto L_0x001f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:4:0x000b, code lost:
        if (r0 != null) goto L_0x000d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000d, code lost:
        return r0;
     */
    public static RealmFieldType fromNativeValue(int i) {
        RealmFieldType realmFieldType;
        if (i >= 0 && i < basicTypes.length) {
            realmFieldType = basicTypes[i];
        }
        if (128 <= i) {
            int i2 = i - 128;
            if (i2 < listTypes.length) {
                realmFieldType = listTypes[i2];
            }
        }
        throw new IllegalArgumentException("Invalid native Realm type: " + i);
    }

    public int getNativeValue() {
        return this.nativeValue;
    }

    public boolean isValid(Object obj) {
        switch (this.nativeValue) {
            case 0:
                return (obj instanceof Long) || (obj instanceof Integer) || (obj instanceof Short) || (obj instanceof Byte);
            case 1:
                return obj instanceof Boolean;
            case 2:
                return obj instanceof String;
            case 4:
                return (obj instanceof byte[]) || (obj instanceof ByteBuffer);
            case 8:
                return obj instanceof Date;
            case 9:
                return obj instanceof Float;
            case 10:
                return obj instanceof Double;
            case 12:
            case 13:
            case 14:
            case 128:
            case 129:
            case 130:
            case 132:
            case 136:
            case 137:
            case 138:
                return false;
            default:
                throw new RuntimeException("Unsupported Realm type:  " + this);
        }
    }
}
