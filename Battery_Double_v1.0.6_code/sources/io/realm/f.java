package io.realm;

import com.hmatalonga.greenhub.models.data.BatteryDetails;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.c;
import io.realm.internal.m;
import io.realm.internal.o;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class f extends BatteryDetails implements g, m {

    /* renamed from: a reason: collision with root package name */
    private static final OsObjectSchemaInfo f4625a = e();

    /* renamed from: b reason: collision with root package name */
    private static final List<String> f4626b;
    private a c;
    private ba<BatteryDetails> d;

    static final class a extends c {

        /* renamed from: a reason: collision with root package name */
        long f4627a;

        /* renamed from: b reason: collision with root package name */
        long f4628b;
        long c;
        long d;
        long e;
        long f;
        long g;
        long h;
        long i;
        long j;

        a(OsSchemaInfo osSchemaInfo) {
            super(10);
            OsObjectSchemaInfo a2 = osSchemaInfo.a("BatteryDetails");
            this.f4627a = a("charger", a2);
            this.f4628b = a("health", a2);
            this.c = a("voltage", a2);
            this.d = a("temperature", a2);
            this.e = a("technology", a2);
            this.f = a("capacity", a2);
            this.g = a("chargeCounter", a2);
            this.h = a("currentAverage", a2);
            this.i = a("currentNow", a2);
            this.j = a("energyCounter", a2);
        }

        /* access modifiers changed from: protected */
        public final void a(c cVar, c cVar2) {
            a aVar = (a) cVar;
            a aVar2 = (a) cVar2;
            aVar2.f4627a = aVar.f4627a;
            aVar2.f4628b = aVar.f4628b;
            aVar2.c = aVar.c;
            aVar2.d = aVar.d;
            aVar2.e = aVar.e;
            aVar2.f = aVar.f;
            aVar2.g = aVar.g;
            aVar2.h = aVar.h;
            aVar2.i = aVar.i;
            aVar2.j = aVar.j;
        }
    }

    static {
        ArrayList arrayList = new ArrayList(10);
        arrayList.add("charger");
        arrayList.add("health");
        arrayList.add("voltage");
        arrayList.add("temperature");
        arrayList.add("technology");
        arrayList.add("capacity");
        arrayList.add("chargeCounter");
        arrayList.add("currentAverage");
        arrayList.add("currentNow");
        arrayList.add("energyCounter");
        f4626b = Collections.unmodifiableList(arrayList);
    }

    f() {
        this.d.g();
    }

    public static BatteryDetails a(bb bbVar, BatteryDetails batteryDetails, boolean z, Map<bh, m> map) {
        if ((batteryDetails instanceof m) && ((m) batteryDetails).d().a() != null) {
            e a2 = ((m) batteryDetails).d().a();
            if (a2.c != bbVar.c) {
                throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
            } else if (a2.g().equals(bbVar.g())) {
                return batteryDetails;
            }
        }
        io.realm.e.a aVar = (io.realm.e.a) e.f.get();
        m mVar = (m) map.get(batteryDetails);
        return mVar != null ? (BatteryDetails) mVar : b(bbVar, batteryDetails, z, map);
    }

    public static a a(OsSchemaInfo osSchemaInfo) {
        return new a(osSchemaInfo);
    }

    public static BatteryDetails b(bb bbVar, BatteryDetails batteryDetails, boolean z, Map<bh, m> map) {
        m mVar = (m) map.get(batteryDetails);
        if (mVar != null) {
            return (BatteryDetails) mVar;
        }
        BatteryDetails batteryDetails2 = (BatteryDetails) bbVar.a(BatteryDetails.class, false, Collections.emptyList());
        map.put(batteryDetails, (m) batteryDetails2);
        g gVar = batteryDetails;
        g gVar2 = batteryDetails2;
        gVar2.realmSet$charger(gVar.realmGet$charger());
        gVar2.realmSet$health(gVar.realmGet$health());
        gVar2.realmSet$voltage(gVar.realmGet$voltage());
        gVar2.realmSet$temperature(gVar.realmGet$temperature());
        gVar2.realmSet$technology(gVar.realmGet$technology());
        gVar2.realmSet$capacity(gVar.realmGet$capacity());
        gVar2.realmSet$chargeCounter(gVar.realmGet$chargeCounter());
        gVar2.realmSet$currentAverage(gVar.realmGet$currentAverage());
        gVar2.realmSet$currentNow(gVar.realmGet$currentNow());
        gVar2.realmSet$energyCounter(gVar.realmGet$energyCounter());
        return batteryDetails2;
    }

    public static OsObjectSchemaInfo b() {
        return f4625a;
    }

    public static String c() {
        return "class_BatteryDetails";
    }

    private static OsObjectSchemaInfo e() {
        io.realm.internal.OsObjectSchemaInfo.a aVar = new io.realm.internal.OsObjectSchemaInfo.a("BatteryDetails", 10, 0);
        aVar.a("charger", RealmFieldType.STRING, false, false, false);
        aVar.a("health", RealmFieldType.STRING, false, false, false);
        aVar.a("voltage", RealmFieldType.DOUBLE, false, false, true);
        aVar.a("temperature", RealmFieldType.DOUBLE, false, false, true);
        aVar.a("technology", RealmFieldType.STRING, false, false, false);
        aVar.a("capacity", RealmFieldType.INTEGER, false, false, true);
        aVar.a("chargeCounter", RealmFieldType.INTEGER, false, false, true);
        aVar.a("currentAverage", RealmFieldType.INTEGER, false, false, true);
        aVar.a("currentNow", RealmFieldType.INTEGER, false, false, true);
        aVar.a("energyCounter", RealmFieldType.INTEGER, false, false, true);
        return aVar.a();
    }

    public void a() {
        if (this.d == null) {
            io.realm.e.a aVar = (io.realm.e.a) e.f.get();
            this.c = (a) aVar.c();
            this.d = new ba<>(this);
            this.d.a(aVar.a());
            this.d.a(aVar.b());
            this.d.a(aVar.d());
            this.d.a(aVar.e());
        }
    }

    public ba<?> d() {
        return this.d;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        f fVar = (f) obj;
        String g = this.d.a().g();
        String g2 = fVar.d.a().g();
        if (g == null ? g2 != null : !g.equals(g2)) {
            return false;
        }
        String g3 = this.d.b().b().g();
        String g4 = fVar.d.b().b().g();
        if (g3 == null ? g4 != null : !g3.equals(g4)) {
            return false;
        }
        return this.d.b().c() == fVar.d.b().c();
    }

    public int hashCode() {
        int i = 0;
        String g = this.d.a().g();
        String g2 = this.d.b().b().g();
        long c2 = this.d.b().c();
        int hashCode = ((g != null ? g.hashCode() : 0) + 527) * 31;
        if (g2 != null) {
            i = g2.hashCode();
        }
        return ((i + hashCode) * 31) + ((int) ((c2 >>> 32) ^ c2));
    }

    public int realmGet$capacity() {
        this.d.a().e();
        return (int) this.d.b().g(this.c.f);
    }

    public int realmGet$chargeCounter() {
        this.d.a().e();
        return (int) this.d.b().g(this.c.g);
    }

    public String realmGet$charger() {
        this.d.a().e();
        return this.d.b().l(this.c.f4627a);
    }

    public int realmGet$currentAverage() {
        this.d.a().e();
        return (int) this.d.b().g(this.c.h);
    }

    public int realmGet$currentNow() {
        this.d.a().e();
        return (int) this.d.b().g(this.c.i);
    }

    public long realmGet$energyCounter() {
        this.d.a().e();
        return this.d.b().g(this.c.j);
    }

    public String realmGet$health() {
        this.d.a().e();
        return this.d.b().l(this.c.f4628b);
    }

    public String realmGet$technology() {
        this.d.a().e();
        return this.d.b().l(this.c.e);
    }

    public double realmGet$temperature() {
        this.d.a().e();
        return this.d.b().j(this.c.d);
    }

    public double realmGet$voltage() {
        this.d.a().e();
        return this.d.b().j(this.c.c);
    }

    public void realmSet$capacity(int i) {
        if (!this.d.f()) {
            this.d.a().e();
            this.d.b().a(this.c.f, (long) i);
        } else if (this.d.c()) {
            o b2 = this.d.b();
            b2.b().a(this.c.f, b2.c(), (long) i, true);
        }
    }

    public void realmSet$chargeCounter(int i) {
        if (!this.d.f()) {
            this.d.a().e();
            this.d.b().a(this.c.g, (long) i);
        } else if (this.d.c()) {
            o b2 = this.d.b();
            b2.b().a(this.c.g, b2.c(), (long) i, true);
        }
    }

    public void realmSet$charger(String str) {
        if (!this.d.f()) {
            this.d.a().e();
            if (str == null) {
                this.d.b().c(this.c.f4627a);
            } else {
                this.d.b().a(this.c.f4627a, str);
            }
        } else if (this.d.c()) {
            o b2 = this.d.b();
            if (str == null) {
                b2.b().a(this.c.f4627a, b2.c(), true);
            } else {
                b2.b().a(this.c.f4627a, b2.c(), str, true);
            }
        }
    }

    public void realmSet$currentAverage(int i) {
        if (!this.d.f()) {
            this.d.a().e();
            this.d.b().a(this.c.h, (long) i);
        } else if (this.d.c()) {
            o b2 = this.d.b();
            b2.b().a(this.c.h, b2.c(), (long) i, true);
        }
    }

    public void realmSet$currentNow(int i) {
        if (!this.d.f()) {
            this.d.a().e();
            this.d.b().a(this.c.i, (long) i);
        } else if (this.d.c()) {
            o b2 = this.d.b();
            b2.b().a(this.c.i, b2.c(), (long) i, true);
        }
    }

    public void realmSet$energyCounter(long j) {
        if (!this.d.f()) {
            this.d.a().e();
            this.d.b().a(this.c.j, j);
        } else if (this.d.c()) {
            o b2 = this.d.b();
            b2.b().a(this.c.j, b2.c(), j, true);
        }
    }

    public void realmSet$health(String str) {
        if (!this.d.f()) {
            this.d.a().e();
            if (str == null) {
                this.d.b().c(this.c.f4628b);
            } else {
                this.d.b().a(this.c.f4628b, str);
            }
        } else if (this.d.c()) {
            o b2 = this.d.b();
            if (str == null) {
                b2.b().a(this.c.f4628b, b2.c(), true);
            } else {
                b2.b().a(this.c.f4628b, b2.c(), str, true);
            }
        }
    }

    public void realmSet$technology(String str) {
        if (!this.d.f()) {
            this.d.a().e();
            if (str == null) {
                this.d.b().c(this.c.e);
            } else {
                this.d.b().a(this.c.e, str);
            }
        } else if (this.d.c()) {
            o b2 = this.d.b();
            if (str == null) {
                b2.b().a(this.c.e, b2.c(), true);
            } else {
                b2.b().a(this.c.e, b2.c(), str, true);
            }
        }
    }

    public void realmSet$temperature(double d2) {
        if (!this.d.f()) {
            this.d.a().e();
            this.d.b().a(this.c.d, d2);
        } else if (this.d.c()) {
            o b2 = this.d.b();
            b2.b().a(this.c.d, b2.c(), d2, true);
        }
    }

    public void realmSet$voltage(double d2) {
        if (!this.d.f()) {
            this.d.a().e();
            this.d.b().a(this.c.c, d2);
        } else if (this.d.c()) {
            o b2 = this.d.b();
            b2.b().a(this.c.c, b2.c(), d2, true);
        }
    }

    public String toString() {
        if (!bj.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder sb = new StringBuilder("BatteryDetails = proxy[");
        sb.append("{charger:");
        sb.append(realmGet$charger() != null ? realmGet$charger() : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{health:");
        sb.append(realmGet$health() != null ? realmGet$health() : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{voltage:");
        sb.append(realmGet$voltage());
        sb.append("}");
        sb.append(",");
        sb.append("{temperature:");
        sb.append(realmGet$temperature());
        sb.append("}");
        sb.append(",");
        sb.append("{technology:");
        sb.append(realmGet$technology() != null ? realmGet$technology() : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{capacity:");
        sb.append(realmGet$capacity());
        sb.append("}");
        sb.append(",");
        sb.append("{chargeCounter:");
        sb.append(realmGet$chargeCounter());
        sb.append("}");
        sb.append(",");
        sb.append("{currentAverage:");
        sb.append(realmGet$currentAverage());
        sb.append("}");
        sb.append(",");
        sb.append("{currentNow:");
        sb.append(realmGet$currentNow());
        sb.append("}");
        sb.append(",");
        sb.append("{energyCounter:");
        sb.append(realmGet$energyCounter());
        sb.append("}");
        sb.append("]");
        return sb.toString();
    }
}
