package io.realm;

public enum r {
    SENSITIVE(true),
    INSENSITIVE(false);
    
    private final boolean c;

    private r(boolean z) {
        this.c = z;
    }

    public boolean a() {
        return this.c;
    }
}
