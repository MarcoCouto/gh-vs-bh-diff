package io.realm;

import android.content.Context;
import io.realm.exceptions.RealmException;
import io.realm.exceptions.RealmFileException;
import io.realm.exceptions.RealmFileException.Kind;
import io.realm.internal.OsRealmConfig.b;
import io.realm.internal.Util;
import io.realm.internal.l;
import io.realm.internal.n;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

public class be {

    /* renamed from: a reason: collision with root package name */
    protected static final n f4580a;
    /* access modifiers changed from: private */

    /* renamed from: b reason: collision with root package name */
    public static final Object f4581b = bb.o();
    private static Boolean c;
    private final File d;
    private final String e;
    private final String f;
    private final String g;
    private final byte[] h;
    private final long i;
    private final bg j;
    private final boolean k;
    private final b l;
    private final n m;
    private final io.realm.a.b n;
    private final io.realm.bb.a o;
    private final boolean p;
    private final CompactOnLaunchCallback q;
    private final boolean r;

    public static class a {

        /* renamed from: a reason: collision with root package name */
        private File f4582a;

        /* renamed from: b reason: collision with root package name */
        private String f4583b;
        private String c;
        private byte[] d;
        private long e;
        private bg f;
        private boolean g;
        private b h;
        private HashSet<Object> i;
        private HashSet<Class<? extends bh>> j;
        private io.realm.a.b k;
        private io.realm.bb.a l;
        private boolean m;
        private CompactOnLaunchCallback n;

        public a() {
            this(e.f4615a);
        }

        a(Context context) {
            this.i = new HashSet<>();
            this.j = new HashSet<>();
            if (context == null) {
                throw new IllegalStateException("Call `Realm.init(Context)` before creating a RealmConfiguration");
            }
            l.a(context);
            a(context);
        }

        private void a(Context context) {
            this.f4582a = context.getFilesDir();
            this.f4583b = "default.realm";
            this.d = null;
            this.e = 0;
            this.f = null;
            this.g = false;
            this.h = b.FULL;
            this.m = false;
            this.n = null;
            if (be.f4581b != null) {
                this.i.add(be.f4581b);
            }
        }

        public a a(long j2) {
            if (j2 < 0) {
                throw new IllegalArgumentException("Realm schema version numbers must be 0 (zero) or higher. Yours was: " + j2);
            }
            this.e = j2;
            return this;
        }

        public a a(bg bgVar) {
            if (bgVar == null) {
                throw new IllegalArgumentException("A non-null migration must be provided");
            }
            this.f = bgVar;
            return this;
        }

        public be a() {
            if (this.m) {
                if (this.l != null) {
                    throw new IllegalStateException("This Realm is marked as read-only. Read-only Realms cannot use initialData(Realm.Transaction).");
                } else if (this.c == null) {
                    throw new IllegalStateException("Only Realms provided using 'assetFile(path)' can be marked read-only. No such Realm was provided.");
                } else if (this.g) {
                    throw new IllegalStateException("'deleteRealmIfMigrationNeeded()' and read-only Realms cannot be combined");
                } else if (this.n != null) {
                    throw new IllegalStateException("'compactOnLaunch()' and read-only Realms cannot be combined");
                }
            }
            if (this.k == null && be.r()) {
                this.k = new io.realm.a.a();
            }
            return new be(this.f4582a, this.f4583b, be.a(new File(this.f4582a, this.f4583b)), this.c, this.d, this.e, this.f, this.g, this.h, be.a(this.i, this.j), this.k, this.l, this.m, this.n, false);
        }
    }

    static {
        if (f4581b != null) {
            n a2 = a(f4581b.getClass().getCanonicalName());
            if (!a2.c()) {
                throw new ExceptionInInitializerError("RealmTransformer doesn't seem to be applied. Please update the project configuration to use the Realm Gradle plugin. See https://realm.io/news/android-installation-change/");
            }
            f4580a = a2;
            return;
        }
        f4580a = null;
    }

    protected be(File file, String str, String str2, String str3, byte[] bArr, long j2, bg bgVar, boolean z, b bVar, n nVar, io.realm.a.b bVar2, io.realm.bb.a aVar, boolean z2, CompactOnLaunchCallback compactOnLaunchCallback, boolean z3) {
        this.d = file;
        this.e = str;
        this.f = str2;
        this.g = str3;
        this.h = bArr;
        this.i = j2;
        this.j = bgVar;
        this.k = z;
        this.l = bVar;
        this.m = nVar;
        this.n = bVar2;
        this.o = aVar;
        this.p = z2;
        this.q = compactOnLaunchCallback;
        this.r = z3;
    }

    private static n a(String str) {
        String[] split = str.split("\\.");
        String format = String.format(Locale.US, "io.realm.%s%s", new Object[]{split[split.length - 1], "Mediator"});
        try {
            Constructor constructor = Class.forName(format).getDeclaredConstructors()[0];
            constructor.setAccessible(true);
            return (n) constructor.newInstance(new Object[0]);
        } catch (ClassNotFoundException e2) {
            throw new RealmException("Could not find " + format, e2);
        } catch (InvocationTargetException e3) {
            throw new RealmException("Could not create an instance of " + format, e3);
        } catch (InstantiationException e4) {
            throw new RealmException("Could not create an instance of " + format, e4);
        } catch (IllegalAccessException e5) {
            throw new RealmException("Could not create an instance of " + format, e5);
        }
    }

    protected static n a(Set<Object> set, Set<Class<? extends bh>> set2) {
        if (set2.size() > 0) {
            return new io.realm.internal.b.b(f4580a, set2);
        }
        if (set.size() == 1) {
            return a(set.iterator().next().getClass().getCanonicalName());
        }
        n[] nVarArr = new n[set.size()];
        int i2 = 0;
        for (Object obj : set) {
            nVarArr[i2] = a(obj.getClass().getCanonicalName());
            i2++;
        }
        return new io.realm.internal.b.a(nVarArr);
    }

    protected static String a(File file) {
        try {
            return file.getCanonicalPath();
        } catch (IOException e2) {
            throw new RealmFileException(Kind.ACCESS_ERROR, "Could not resolve the canonical path to the Realm file: " + file.getAbsolutePath(), e2);
        }
    }

    static synchronized boolean r() {
        boolean booleanValue;
        synchronized (be.class) {
            if (c == null) {
                try {
                    Class.forName("io.reactivex.Flowable");
                    c = Boolean.valueOf(true);
                } catch (ClassNotFoundException e2) {
                    c = Boolean.valueOf(false);
                }
            }
            booleanValue = c.booleanValue();
        }
        return booleanValue;
    }

    public File a() {
        return this.d;
    }

    public String b() {
        return this.e;
    }

    public byte[] c() {
        if (this.h == null) {
            return null;
        }
        return Arrays.copyOf(this.h, this.h.length);
    }

    public long d() {
        return this.i;
    }

    public bg e() {
        return this.j;
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        be beVar = (be) obj;
        if (this.i != beVar.i || this.k != beVar.k || this.p != beVar.p || this.r != beVar.r) {
            return false;
        }
        if (this.d != null) {
            if (!this.d.equals(beVar.d)) {
                return false;
            }
        } else if (beVar.d != null) {
            return false;
        }
        if (this.e != null) {
            if (!this.e.equals(beVar.e)) {
                return false;
            }
        } else if (beVar.e != null) {
            return false;
        }
        if (!this.f.equals(beVar.f)) {
            return false;
        }
        if (this.g != null) {
            if (!this.g.equals(beVar.g)) {
                return false;
            }
        } else if (beVar.g != null) {
            return false;
        }
        if (!Arrays.equals(this.h, beVar.h)) {
            return false;
        }
        if (this.j != null) {
            if (!this.j.equals(beVar.j)) {
                return false;
            }
        } else if (beVar.j != null) {
            return false;
        }
        if (this.l != beVar.l || !this.m.equals(beVar.m)) {
            return false;
        }
        if (this.n != null) {
            if (!this.n.equals(beVar.n)) {
                return false;
            }
        } else if (beVar.n != null) {
            return false;
        }
        if (this.o != null) {
            if (!this.o.equals(beVar.o)) {
                return false;
            }
        } else if (beVar.o != null) {
            return false;
        }
        if (this.q != null) {
            z = this.q.equals(beVar.q);
        } else if (beVar.q != null) {
            z = false;
        }
        return z;
    }

    public boolean f() {
        return this.k;
    }

    public b g() {
        return this.l;
    }

    /* access modifiers changed from: protected */
    public n h() {
        return this.m;
    }

    public int hashCode() {
        int i2 = 1;
        int hashCode = ((this.q != null ? this.q.hashCode() : 0) + (((this.p ? 1 : 0) + (((this.o != null ? this.o.hashCode() : 0) + (((this.n != null ? this.n.hashCode() : 0) + (((((((this.k ? 1 : 0) + (((this.j != null ? this.j.hashCode() : 0) + (((((((this.g != null ? this.g.hashCode() : 0) + (((((this.e != null ? this.e.hashCode() : 0) + ((this.d != null ? this.d.hashCode() : 0) * 31)) * 31) + this.f.hashCode()) * 31)) * 31) + Arrays.hashCode(this.h)) * 31) + ((int) (this.i ^ (this.i >>> 32)))) * 31)) * 31)) * 31) + this.l.hashCode()) * 31) + this.m.hashCode()) * 31)) * 31)) * 31)) * 31)) * 31;
        if (!this.r) {
            i2 = 0;
        }
        return hashCode + i2;
    }

    /* access modifiers changed from: 0000 */
    public io.realm.bb.a i() {
        return this.o;
    }

    /* access modifiers changed from: 0000 */
    public boolean j() {
        return !Util.a(this.g);
    }

    /* access modifiers changed from: 0000 */
    public String k() {
        return this.g;
    }

    public CompactOnLaunchCallback l() {
        return this.q;
    }

    public String m() {
        return this.f;
    }

    /* access modifiers changed from: 0000 */
    public boolean n() {
        return new File(this.f).exists();
    }

    public io.realm.a.b o() {
        if (this.n != null) {
            return this.n;
        }
        throw new UnsupportedOperationException("RxJava seems to be missing from the classpath. Remember to add it as a compile dependency. See https://realm.io/docs/java/latest/#rxjava for more details.");
    }

    public boolean p() {
        return this.p;
    }

    public boolean q() {
        return this.r;
    }

    /* access modifiers changed from: 0000 */
    public boolean s() {
        return false;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("realmDirectory: ").append(this.d != null ? this.d.toString() : "");
        sb.append("\n");
        sb.append("realmFileName : ").append(this.e);
        sb.append("\n");
        sb.append("canonicalPath: ").append(this.f);
        sb.append("\n");
        sb.append("key: ").append("[length: ").append(this.h == null ? 0 : 64).append("]");
        sb.append("\n");
        sb.append("schemaVersion: ").append(Long.toString(this.i));
        sb.append("\n");
        sb.append("migration: ").append(this.j);
        sb.append("\n");
        sb.append("deleteRealmIfMigrationNeeded: ").append(this.k);
        sb.append("\n");
        sb.append("durability: ").append(this.l);
        sb.append("\n");
        sb.append("schemaMediator: ").append(this.m);
        sb.append("\n");
        sb.append("readOnly: ").append(this.p);
        sb.append("\n");
        sb.append("compactOnLaunch: ").append(this.q);
        return sb.toString();
    }
}
