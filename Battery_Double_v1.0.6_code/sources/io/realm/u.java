package io.realm;

import com.hmatalonga.greenhub.models.data.CpuStatus;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.c;
import io.realm.internal.m;
import io.realm.internal.o;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class u extends CpuStatus implements m, v {

    /* renamed from: a reason: collision with root package name */
    private static final OsObjectSchemaInfo f4726a = e();

    /* renamed from: b reason: collision with root package name */
    private static final List<String> f4727b;
    private a c;
    private ba<CpuStatus> d;

    static final class a extends c {

        /* renamed from: a reason: collision with root package name */
        long f4728a;

        /* renamed from: b reason: collision with root package name */
        long f4729b;
        long c;

        a(OsSchemaInfo osSchemaInfo) {
            super(3);
            OsObjectSchemaInfo a2 = osSchemaInfo.a("CpuStatus");
            this.f4728a = a("cpuUsage", a2);
            this.f4729b = a("upTime", a2);
            this.c = a("sleepTime", a2);
        }

        /* access modifiers changed from: protected */
        public final void a(c cVar, c cVar2) {
            a aVar = (a) cVar;
            a aVar2 = (a) cVar2;
            aVar2.f4728a = aVar.f4728a;
            aVar2.f4729b = aVar.f4729b;
            aVar2.c = aVar.c;
        }
    }

    static {
        ArrayList arrayList = new ArrayList(3);
        arrayList.add("cpuUsage");
        arrayList.add("upTime");
        arrayList.add("sleepTime");
        f4727b = Collections.unmodifiableList(arrayList);
    }

    u() {
        this.d.g();
    }

    public static CpuStatus a(bb bbVar, CpuStatus cpuStatus, boolean z, Map<bh, m> map) {
        if ((cpuStatus instanceof m) && ((m) cpuStatus).d().a() != null) {
            e a2 = ((m) cpuStatus).d().a();
            if (a2.c != bbVar.c) {
                throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
            } else if (a2.g().equals(bbVar.g())) {
                return cpuStatus;
            }
        }
        io.realm.e.a aVar = (io.realm.e.a) e.f.get();
        m mVar = (m) map.get(cpuStatus);
        return mVar != null ? (CpuStatus) mVar : b(bbVar, cpuStatus, z, map);
    }

    public static a a(OsSchemaInfo osSchemaInfo) {
        return new a(osSchemaInfo);
    }

    public static CpuStatus b(bb bbVar, CpuStatus cpuStatus, boolean z, Map<bh, m> map) {
        m mVar = (m) map.get(cpuStatus);
        if (mVar != null) {
            return (CpuStatus) mVar;
        }
        CpuStatus cpuStatus2 = (CpuStatus) bbVar.a(CpuStatus.class, false, Collections.emptyList());
        map.put(cpuStatus, (m) cpuStatus2);
        v vVar = cpuStatus;
        v vVar2 = cpuStatus2;
        vVar2.realmSet$cpuUsage(vVar.realmGet$cpuUsage());
        vVar2.realmSet$upTime(vVar.realmGet$upTime());
        vVar2.realmSet$sleepTime(vVar.realmGet$sleepTime());
        return cpuStatus2;
    }

    public static OsObjectSchemaInfo b() {
        return f4726a;
    }

    public static String c() {
        return "class_CpuStatus";
    }

    private static OsObjectSchemaInfo e() {
        io.realm.internal.OsObjectSchemaInfo.a aVar = new io.realm.internal.OsObjectSchemaInfo.a("CpuStatus", 3, 0);
        aVar.a("cpuUsage", RealmFieldType.DOUBLE, false, false, true);
        aVar.a("upTime", RealmFieldType.INTEGER, false, false, true);
        aVar.a("sleepTime", RealmFieldType.INTEGER, false, false, true);
        return aVar.a();
    }

    public void a() {
        if (this.d == null) {
            io.realm.e.a aVar = (io.realm.e.a) e.f.get();
            this.c = (a) aVar.c();
            this.d = new ba<>(this);
            this.d.a(aVar.a());
            this.d.a(aVar.b());
            this.d.a(aVar.d());
            this.d.a(aVar.e());
        }
    }

    public ba<?> d() {
        return this.d;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        u uVar = (u) obj;
        String g = this.d.a().g();
        String g2 = uVar.d.a().g();
        if (g == null ? g2 != null : !g.equals(g2)) {
            return false;
        }
        String g3 = this.d.b().b().g();
        String g4 = uVar.d.b().b().g();
        if (g3 == null ? g4 != null : !g3.equals(g4)) {
            return false;
        }
        return this.d.b().c() == uVar.d.b().c();
    }

    public int hashCode() {
        int i = 0;
        String g = this.d.a().g();
        String g2 = this.d.b().b().g();
        long c2 = this.d.b().c();
        int hashCode = ((g != null ? g.hashCode() : 0) + 527) * 31;
        if (g2 != null) {
            i = g2.hashCode();
        }
        return ((i + hashCode) * 31) + ((int) ((c2 >>> 32) ^ c2));
    }

    public double realmGet$cpuUsage() {
        this.d.a().e();
        return this.d.b().j(this.c.f4728a);
    }

    public long realmGet$sleepTime() {
        this.d.a().e();
        return this.d.b().g(this.c.c);
    }

    public long realmGet$upTime() {
        this.d.a().e();
        return this.d.b().g(this.c.f4729b);
    }

    public void realmSet$cpuUsage(double d2) {
        if (!this.d.f()) {
            this.d.a().e();
            this.d.b().a(this.c.f4728a, d2);
        } else if (this.d.c()) {
            o b2 = this.d.b();
            b2.b().a(this.c.f4728a, b2.c(), d2, true);
        }
    }

    public void realmSet$sleepTime(long j) {
        if (!this.d.f()) {
            this.d.a().e();
            this.d.b().a(this.c.c, j);
        } else if (this.d.c()) {
            o b2 = this.d.b();
            b2.b().a(this.c.c, b2.c(), j, true);
        }
    }

    public void realmSet$upTime(long j) {
        if (!this.d.f()) {
            this.d.a().e();
            this.d.b().a(this.c.f4729b, j);
        } else if (this.d.c()) {
            o b2 = this.d.b();
            b2.b().a(this.c.f4729b, b2.c(), j, true);
        }
    }

    public String toString() {
        if (!bj.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder sb = new StringBuilder("CpuStatus = proxy[");
        sb.append("{cpuUsage:");
        sb.append(realmGet$cpuUsage());
        sb.append("}");
        sb.append(",");
        sb.append("{upTime:");
        sb.append(realmGet$upTime());
        sb.append("}");
        sb.append(",");
        sb.append("{sleepTime:");
        sb.append(realmGet$sleepTime());
        sb.append("}");
        sb.append("]");
        return sb.toString();
    }
}
