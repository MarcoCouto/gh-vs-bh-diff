package io.realm;

import com.hmatalonga.greenhub.models.data.AppPermission;
import com.hmatalonga.greenhub.models.data.AppSignature;

public interface az {
    bf<AppPermission> realmGet$appPermissions();

    bf<AppSignature> realmGet$appSignatures();

    String realmGet$applicationLabel();

    String realmGet$importance();

    String realmGet$installationPkg();

    boolean realmGet$isSystemApp();

    String realmGet$name();

    int realmGet$processId();

    int realmGet$versionCode();

    String realmGet$versionName();

    void realmSet$applicationLabel(String str);

    void realmSet$importance(String str);

    void realmSet$installationPkg(String str);

    void realmSet$isSystemApp(boolean z);

    void realmSet$name(String str);

    void realmSet$processId(int i);

    void realmSet$versionCode(int i);

    void realmSet$versionName(String str);
}
