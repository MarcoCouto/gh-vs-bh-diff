package io.realm;

import io.realm.internal.Collection;
import java.util.Iterator;
import java.util.ListIterator;

public class bn<E> extends ax<E> {
    bn(e eVar, Collection collection, Class<E> cls) {
        super(eVar, collection, cls);
    }

    bn(e eVar, Collection collection, String str) {
        super(eVar, collection, str);
    }

    public /* bridge */ /* synthetic */ Object a() {
        return super.a();
    }

    @Deprecated
    public /* bridge */ /* synthetic */ void add(int i, Object obj) {
        super.add(i, obj);
    }

    @Deprecated
    public /* bridge */ /* synthetic */ boolean add(Object obj) {
        return super.add(obj);
    }

    @Deprecated
    public /* bridge */ /* synthetic */ boolean addAll(int i, java.util.Collection collection) {
        return super.addAll(i, collection);
    }

    @Deprecated
    public /* bridge */ /* synthetic */ boolean addAll(java.util.Collection collection) {
        return super.addAll(collection);
    }

    public /* bridge */ /* synthetic */ boolean b() {
        return super.b();
    }

    public boolean c() {
        this.f4561a.e();
        return this.e.g();
    }

    @Deprecated
    public /* bridge */ /* synthetic */ void clear() {
        super.clear();
    }

    public /* bridge */ /* synthetic */ boolean contains(Object obj) {
        return super.contains(obj);
    }

    public boolean d() {
        this.f4561a.e();
        this.e.h();
        return true;
    }

    public /* bridge */ /* synthetic */ Object get(int i) {
        return super.get(i);
    }

    public /* bridge */ /* synthetic */ Iterator iterator() {
        return super.iterator();
    }

    public /* bridge */ /* synthetic */ ListIterator listIterator() {
        return super.listIterator();
    }

    public /* bridge */ /* synthetic */ ListIterator listIterator(int i) {
        return super.listIterator(i);
    }

    @Deprecated
    public /* bridge */ /* synthetic */ Object remove(int i) {
        return super.remove(i);
    }

    @Deprecated
    public /* bridge */ /* synthetic */ boolean remove(Object obj) {
        return super.remove(obj);
    }

    @Deprecated
    public /* bridge */ /* synthetic */ boolean removeAll(java.util.Collection collection) {
        return super.removeAll(collection);
    }

    @Deprecated
    public /* bridge */ /* synthetic */ boolean retainAll(java.util.Collection collection) {
        return super.retainAll(collection);
    }

    @Deprecated
    public /* bridge */ /* synthetic */ Object set(int i, Object obj) {
        return super.set(i, obj);
    }

    public /* bridge */ /* synthetic */ int size() {
        return super.size();
    }
}
