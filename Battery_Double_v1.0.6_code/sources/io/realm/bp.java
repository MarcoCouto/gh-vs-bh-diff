package io.realm;

import com.hmatalonga.greenhub.models.data.BatteryDetails;
import com.hmatalonga.greenhub.models.data.CallInfo;
import com.hmatalonga.greenhub.models.data.CpuStatus;
import com.hmatalonga.greenhub.models.data.Feature;
import com.hmatalonga.greenhub.models.data.LocationProvider;
import com.hmatalonga.greenhub.models.data.NetworkDetails;
import com.hmatalonga.greenhub.models.data.ProcessInfo;
import com.hmatalonga.greenhub.models.data.Sample;
import com.hmatalonga.greenhub.models.data.Settings;
import com.hmatalonga.greenhub.models.data.StorageDetails;
import io.realm.exceptions.RealmException;
import io.realm.internal.OsList;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.Table;
import io.realm.internal.c;
import io.realm.internal.m;
import io.realm.internal.o;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class bp extends Sample implements bq, m {

    /* renamed from: a reason: collision with root package name */
    private static final OsObjectSchemaInfo f4597a = e();

    /* renamed from: b reason: collision with root package name */
    private static final List<String> f4598b;
    private a c;
    private ba<Sample> d;
    private bf<LocationProvider> e;
    private bf<ProcessInfo> f;
    private bf<Feature> g;

    static final class a extends c {
        long A;
        long B;

        /* renamed from: a reason: collision with root package name */
        long f4599a;

        /* renamed from: b reason: collision with root package name */
        long f4600b;
        long c;
        long d;
        long e;
        long f;
        long g;
        long h;
        long i;
        long j;
        long k;
        long l;
        long m;
        long n;
        long o;
        long p;
        long q;
        long r;
        long s;
        long t;
        long u;
        long v;
        long w;
        long x;
        long y;
        long z;

        a(OsSchemaInfo osSchemaInfo) {
            super(28);
            OsObjectSchemaInfo a2 = osSchemaInfo.a("Sample");
            this.f4599a = a("id", a2);
            this.f4600b = a("uuId", a2);
            this.c = a("timestamp", a2);
            this.d = a("version", a2);
            this.e = a("database", a2);
            this.f = a("batteryState", a2);
            this.g = a("batteryLevel", a2);
            this.h = a("memoryWired", a2);
            this.i = a("memoryActive", a2);
            this.j = a("memoryInactive", a2);
            this.k = a("memoryFree", a2);
            this.l = a("memoryUser", a2);
            this.m = a("triggeredBy", a2);
            this.n = a("networkStatus", a2);
            this.o = a("distanceTraveled", a2);
            this.p = a("screenBrightness", a2);
            this.q = a("networkDetails", a2);
            this.r = a("batteryDetails", a2);
            this.s = a("cpuStatus", a2);
            this.t = a("callInfo", a2);
            this.u = a("screenOn", a2);
            this.v = a("timeZone", a2);
            this.w = a("settings", a2);
            this.x = a("storageDetails", a2);
            this.y = a("countryCode", a2);
            this.z = a("locationProviders", a2);
            this.A = a("processInfos", a2);
            this.B = a("features", a2);
        }

        /* access modifiers changed from: protected */
        public final void a(c cVar, c cVar2) {
            a aVar = (a) cVar;
            a aVar2 = (a) cVar2;
            aVar2.f4599a = aVar.f4599a;
            aVar2.f4600b = aVar.f4600b;
            aVar2.c = aVar.c;
            aVar2.d = aVar.d;
            aVar2.e = aVar.e;
            aVar2.f = aVar.f;
            aVar2.g = aVar.g;
            aVar2.h = aVar.h;
            aVar2.i = aVar.i;
            aVar2.j = aVar.j;
            aVar2.k = aVar.k;
            aVar2.l = aVar.l;
            aVar2.m = aVar.m;
            aVar2.n = aVar.n;
            aVar2.o = aVar.o;
            aVar2.p = aVar.p;
            aVar2.q = aVar.q;
            aVar2.r = aVar.r;
            aVar2.s = aVar.s;
            aVar2.t = aVar.t;
            aVar2.u = aVar.u;
            aVar2.v = aVar.v;
            aVar2.w = aVar.w;
            aVar2.x = aVar.x;
            aVar2.y = aVar.y;
            aVar2.z = aVar.z;
            aVar2.A = aVar.A;
            aVar2.B = aVar.B;
        }
    }

    static {
        ArrayList arrayList = new ArrayList(28);
        arrayList.add("id");
        arrayList.add("uuId");
        arrayList.add("timestamp");
        arrayList.add("version");
        arrayList.add("database");
        arrayList.add("batteryState");
        arrayList.add("batteryLevel");
        arrayList.add("memoryWired");
        arrayList.add("memoryActive");
        arrayList.add("memoryInactive");
        arrayList.add("memoryFree");
        arrayList.add("memoryUser");
        arrayList.add("triggeredBy");
        arrayList.add("networkStatus");
        arrayList.add("distanceTraveled");
        arrayList.add("screenBrightness");
        arrayList.add("networkDetails");
        arrayList.add("batteryDetails");
        arrayList.add("cpuStatus");
        arrayList.add("callInfo");
        arrayList.add("screenOn");
        arrayList.add("timeZone");
        arrayList.add("settings");
        arrayList.add("storageDetails");
        arrayList.add("countryCode");
        arrayList.add("locationProviders");
        arrayList.add("processInfos");
        arrayList.add("features");
        f4598b = Collections.unmodifiableList(arrayList);
    }

    bp() {
        this.d.g();
    }

    static Sample a(bb bbVar, Sample sample, Sample sample2, Map<bh, m> map) {
        int i = 0;
        bq bqVar = sample;
        bq bqVar2 = sample2;
        bqVar.realmSet$uuId(bqVar2.realmGet$uuId());
        bqVar.realmSet$timestamp(bqVar2.realmGet$timestamp());
        bqVar.realmSet$version(bqVar2.realmGet$version());
        bqVar.realmSet$database(bqVar2.realmGet$database());
        bqVar.realmSet$batteryState(bqVar2.realmGet$batteryState());
        bqVar.realmSet$batteryLevel(bqVar2.realmGet$batteryLevel());
        bqVar.realmSet$memoryWired(bqVar2.realmGet$memoryWired());
        bqVar.realmSet$memoryActive(bqVar2.realmGet$memoryActive());
        bqVar.realmSet$memoryInactive(bqVar2.realmGet$memoryInactive());
        bqVar.realmSet$memoryFree(bqVar2.realmGet$memoryFree());
        bqVar.realmSet$memoryUser(bqVar2.realmGet$memoryUser());
        bqVar.realmSet$triggeredBy(bqVar2.realmGet$triggeredBy());
        bqVar.realmSet$networkStatus(bqVar2.realmGet$networkStatus());
        bqVar.realmSet$distanceTraveled(bqVar2.realmGet$distanceTraveled());
        bqVar.realmSet$screenBrightness(bqVar2.realmGet$screenBrightness());
        NetworkDetails realmGet$networkDetails = bqVar2.realmGet$networkDetails();
        if (realmGet$networkDetails == null) {
            bqVar.realmSet$networkDetails(null);
        } else {
            NetworkDetails networkDetails = (NetworkDetails) map.get(realmGet$networkDetails);
            if (networkDetails != null) {
                bqVar.realmSet$networkDetails(networkDetails);
            } else {
                bqVar.realmSet$networkDetails(aq.a(bbVar, realmGet$networkDetails, true, map));
            }
        }
        BatteryDetails realmGet$batteryDetails = bqVar2.realmGet$batteryDetails();
        if (realmGet$batteryDetails == null) {
            bqVar.realmSet$batteryDetails(null);
        } else {
            BatteryDetails batteryDetails = (BatteryDetails) map.get(realmGet$batteryDetails);
            if (batteryDetails != null) {
                bqVar.realmSet$batteryDetails(batteryDetails);
            } else {
                bqVar.realmSet$batteryDetails(f.a(bbVar, realmGet$batteryDetails, true, map));
            }
        }
        CpuStatus realmGet$cpuStatus = bqVar2.realmGet$cpuStatus();
        if (realmGet$cpuStatus == null) {
            bqVar.realmSet$cpuStatus(null);
        } else {
            CpuStatus cpuStatus = (CpuStatus) map.get(realmGet$cpuStatus);
            if (cpuStatus != null) {
                bqVar.realmSet$cpuStatus(cpuStatus);
            } else {
                bqVar.realmSet$cpuStatus(u.a(bbVar, realmGet$cpuStatus, true, map));
            }
        }
        CallInfo realmGet$callInfo = bqVar2.realmGet$callInfo();
        if (realmGet$callInfo == null) {
            bqVar.realmSet$callInfo(null);
        } else {
            CallInfo callInfo = (CallInfo) map.get(realmGet$callInfo);
            if (callInfo != null) {
                bqVar.realmSet$callInfo(callInfo);
            } else {
                bqVar.realmSet$callInfo(n.a(bbVar, realmGet$callInfo, true, map));
            }
        }
        bqVar.realmSet$screenOn(bqVar2.realmGet$screenOn());
        bqVar.realmSet$timeZone(bqVar2.realmGet$timeZone());
        Settings realmGet$settings = bqVar2.realmGet$settings();
        if (realmGet$settings == null) {
            bqVar.realmSet$settings(null);
        } else {
            Settings settings = (Settings) map.get(realmGet$settings);
            if (settings != null) {
                bqVar.realmSet$settings(settings);
            } else {
                bqVar.realmSet$settings(bs.a(bbVar, realmGet$settings, true, map));
            }
        }
        StorageDetails realmGet$storageDetails = bqVar2.realmGet$storageDetails();
        if (realmGet$storageDetails == null) {
            bqVar.realmSet$storageDetails(null);
        } else {
            StorageDetails storageDetails = (StorageDetails) map.get(realmGet$storageDetails);
            if (storageDetails != null) {
                bqVar.realmSet$storageDetails(storageDetails);
            } else {
                bqVar.realmSet$storageDetails(bv.a(bbVar, realmGet$storageDetails, true, map));
            }
        }
        bqVar.realmSet$countryCode(bqVar2.realmGet$countryCode());
        bf realmGet$locationProviders = bqVar2.realmGet$locationProviders();
        bf realmGet$locationProviders2 = bqVar.realmGet$locationProviders();
        if (realmGet$locationProviders == null || realmGet$locationProviders.size() != realmGet$locationProviders2.size()) {
            realmGet$locationProviders2.clear();
            if (realmGet$locationProviders != null) {
                for (int i2 = 0; i2 < realmGet$locationProviders.size(); i2++) {
                    LocationProvider locationProvider = (LocationProvider) realmGet$locationProviders.get(i2);
                    LocationProvider locationProvider2 = (LocationProvider) map.get(locationProvider);
                    if (locationProvider2 != null) {
                        realmGet$locationProviders2.add(locationProvider2);
                    } else {
                        realmGet$locationProviders2.add(ai.a(bbVar, locationProvider, true, map));
                    }
                }
            }
        } else {
            int size = realmGet$locationProviders.size();
            for (int i3 = 0; i3 < size; i3++) {
                LocationProvider locationProvider3 = (LocationProvider) realmGet$locationProviders.get(i3);
                LocationProvider locationProvider4 = (LocationProvider) map.get(locationProvider3);
                if (locationProvider4 != null) {
                    realmGet$locationProviders2.set(i3, locationProvider4);
                } else {
                    realmGet$locationProviders2.set(i3, ai.a(bbVar, locationProvider3, true, map));
                }
            }
        }
        bf realmGet$processInfos = bqVar2.realmGet$processInfos();
        bf realmGet$processInfos2 = bqVar.realmGet$processInfos();
        if (realmGet$processInfos == null || realmGet$processInfos.size() != realmGet$processInfos2.size()) {
            realmGet$processInfos2.clear();
            if (realmGet$processInfos != null) {
                for (int i4 = 0; i4 < realmGet$processInfos.size(); i4++) {
                    ProcessInfo processInfo = (ProcessInfo) realmGet$processInfos.get(i4);
                    ProcessInfo processInfo2 = (ProcessInfo) map.get(processInfo);
                    if (processInfo2 != null) {
                        realmGet$processInfos2.add(processInfo2);
                    } else {
                        realmGet$processInfos2.add(ay.a(bbVar, processInfo, true, map));
                    }
                }
            }
        } else {
            int size2 = realmGet$processInfos.size();
            for (int i5 = 0; i5 < size2; i5++) {
                ProcessInfo processInfo3 = (ProcessInfo) realmGet$processInfos.get(i5);
                ProcessInfo processInfo4 = (ProcessInfo) map.get(processInfo3);
                if (processInfo4 != null) {
                    realmGet$processInfos2.set(i5, processInfo4);
                } else {
                    realmGet$processInfos2.set(i5, ay.a(bbVar, processInfo3, true, map));
                }
            }
        }
        bf realmGet$features = bqVar2.realmGet$features();
        bf realmGet$features2 = bqVar.realmGet$features();
        if (realmGet$features == null || realmGet$features.size() != realmGet$features2.size()) {
            realmGet$features2.clear();
            if (realmGet$features != null) {
                while (i < realmGet$features.size()) {
                    Feature feature = (Feature) realmGet$features.get(i);
                    Feature feature2 = (Feature) map.get(feature);
                    if (feature2 != null) {
                        realmGet$features2.add(feature2);
                    } else {
                        realmGet$features2.add(ac.a(bbVar, feature, true, map));
                    }
                    i++;
                }
            }
        } else {
            int size3 = realmGet$features.size();
            while (i < size3) {
                Feature feature3 = (Feature) realmGet$features.get(i);
                Feature feature4 = (Feature) map.get(feature3);
                if (feature4 != null) {
                    realmGet$features2.set(i, feature4);
                } else {
                    realmGet$features2.set(i, ac.a(bbVar, feature3, true, map));
                }
                i++;
            }
        }
        return sample;
    }

    /* JADX INFO: finally extract failed */
    public static Sample a(bb bbVar, Sample sample, boolean z, Map<bh, m> map) {
        boolean z2;
        bp bpVar;
        if ((sample instanceof m) && ((m) sample).d().a() != null) {
            e a2 = ((m) sample).d().a();
            if (a2.c != bbVar.c) {
                throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
            } else if (a2.g().equals(bbVar.g())) {
                return sample;
            }
        }
        io.realm.e.a aVar = (io.realm.e.a) e.f.get();
        m mVar = (m) map.get(sample);
        if (mVar != null) {
            return (Sample) mVar;
        }
        if (z) {
            Table b2 = bbVar.b(Sample.class);
            long a3 = b2.a(((a) bbVar.k().c(Sample.class)).f4599a, (long) sample.realmGet$id());
            if (a3 == -1) {
                z2 = false;
                bpVar = null;
            } else {
                try {
                    aVar.a(bbVar, b2.f(a3), bbVar.k().c(Sample.class), false, Collections.emptyList());
                    bpVar = new bp();
                    map.put(sample, bpVar);
                    aVar.f();
                    z2 = z;
                } catch (Throwable th) {
                    aVar.f();
                    throw th;
                }
            }
        } else {
            z2 = z;
            bpVar = null;
        }
        return z2 ? a(bbVar, (Sample) bpVar, sample, map) : b(bbVar, sample, z, map);
    }

    public static a a(OsSchemaInfo osSchemaInfo) {
        return new a(osSchemaInfo);
    }

    public static Sample b(bb bbVar, Sample sample, boolean z, Map<bh, m> map) {
        m mVar = (m) map.get(sample);
        if (mVar != null) {
            return (Sample) mVar;
        }
        Sample sample2 = (Sample) bbVar.a(Sample.class, Integer.valueOf(sample.realmGet$id()), false, Collections.emptyList());
        map.put(sample, (m) sample2);
        bq bqVar = sample;
        bq bqVar2 = sample2;
        bqVar2.realmSet$uuId(bqVar.realmGet$uuId());
        bqVar2.realmSet$timestamp(bqVar.realmGet$timestamp());
        bqVar2.realmSet$version(bqVar.realmGet$version());
        bqVar2.realmSet$database(bqVar.realmGet$database());
        bqVar2.realmSet$batteryState(bqVar.realmGet$batteryState());
        bqVar2.realmSet$batteryLevel(bqVar.realmGet$batteryLevel());
        bqVar2.realmSet$memoryWired(bqVar.realmGet$memoryWired());
        bqVar2.realmSet$memoryActive(bqVar.realmGet$memoryActive());
        bqVar2.realmSet$memoryInactive(bqVar.realmGet$memoryInactive());
        bqVar2.realmSet$memoryFree(bqVar.realmGet$memoryFree());
        bqVar2.realmSet$memoryUser(bqVar.realmGet$memoryUser());
        bqVar2.realmSet$triggeredBy(bqVar.realmGet$triggeredBy());
        bqVar2.realmSet$networkStatus(bqVar.realmGet$networkStatus());
        bqVar2.realmSet$distanceTraveled(bqVar.realmGet$distanceTraveled());
        bqVar2.realmSet$screenBrightness(bqVar.realmGet$screenBrightness());
        NetworkDetails realmGet$networkDetails = bqVar.realmGet$networkDetails();
        if (realmGet$networkDetails == null) {
            bqVar2.realmSet$networkDetails(null);
        } else {
            NetworkDetails networkDetails = (NetworkDetails) map.get(realmGet$networkDetails);
            if (networkDetails != null) {
                bqVar2.realmSet$networkDetails(networkDetails);
            } else {
                bqVar2.realmSet$networkDetails(aq.a(bbVar, realmGet$networkDetails, z, map));
            }
        }
        BatteryDetails realmGet$batteryDetails = bqVar.realmGet$batteryDetails();
        if (realmGet$batteryDetails == null) {
            bqVar2.realmSet$batteryDetails(null);
        } else {
            BatteryDetails batteryDetails = (BatteryDetails) map.get(realmGet$batteryDetails);
            if (batteryDetails != null) {
                bqVar2.realmSet$batteryDetails(batteryDetails);
            } else {
                bqVar2.realmSet$batteryDetails(f.a(bbVar, realmGet$batteryDetails, z, map));
            }
        }
        CpuStatus realmGet$cpuStatus = bqVar.realmGet$cpuStatus();
        if (realmGet$cpuStatus == null) {
            bqVar2.realmSet$cpuStatus(null);
        } else {
            CpuStatus cpuStatus = (CpuStatus) map.get(realmGet$cpuStatus);
            if (cpuStatus != null) {
                bqVar2.realmSet$cpuStatus(cpuStatus);
            } else {
                bqVar2.realmSet$cpuStatus(u.a(bbVar, realmGet$cpuStatus, z, map));
            }
        }
        CallInfo realmGet$callInfo = bqVar.realmGet$callInfo();
        if (realmGet$callInfo == null) {
            bqVar2.realmSet$callInfo(null);
        } else {
            CallInfo callInfo = (CallInfo) map.get(realmGet$callInfo);
            if (callInfo != null) {
                bqVar2.realmSet$callInfo(callInfo);
            } else {
                bqVar2.realmSet$callInfo(n.a(bbVar, realmGet$callInfo, z, map));
            }
        }
        bqVar2.realmSet$screenOn(bqVar.realmGet$screenOn());
        bqVar2.realmSet$timeZone(bqVar.realmGet$timeZone());
        Settings realmGet$settings = bqVar.realmGet$settings();
        if (realmGet$settings == null) {
            bqVar2.realmSet$settings(null);
        } else {
            Settings settings = (Settings) map.get(realmGet$settings);
            if (settings != null) {
                bqVar2.realmSet$settings(settings);
            } else {
                bqVar2.realmSet$settings(bs.a(bbVar, realmGet$settings, z, map));
            }
        }
        StorageDetails realmGet$storageDetails = bqVar.realmGet$storageDetails();
        if (realmGet$storageDetails == null) {
            bqVar2.realmSet$storageDetails(null);
        } else {
            StorageDetails storageDetails = (StorageDetails) map.get(realmGet$storageDetails);
            if (storageDetails != null) {
                bqVar2.realmSet$storageDetails(storageDetails);
            } else {
                bqVar2.realmSet$storageDetails(bv.a(bbVar, realmGet$storageDetails, z, map));
            }
        }
        bqVar2.realmSet$countryCode(bqVar.realmGet$countryCode());
        bf realmGet$locationProviders = bqVar.realmGet$locationProviders();
        if (realmGet$locationProviders != null) {
            bf realmGet$locationProviders2 = bqVar2.realmGet$locationProviders();
            realmGet$locationProviders2.clear();
            for (int i = 0; i < realmGet$locationProviders.size(); i++) {
                LocationProvider locationProvider = (LocationProvider) realmGet$locationProviders.get(i);
                LocationProvider locationProvider2 = (LocationProvider) map.get(locationProvider);
                if (locationProvider2 != null) {
                    realmGet$locationProviders2.add(locationProvider2);
                } else {
                    realmGet$locationProviders2.add(ai.a(bbVar, locationProvider, z, map));
                }
            }
        }
        bf realmGet$processInfos = bqVar.realmGet$processInfos();
        if (realmGet$processInfos != null) {
            bf realmGet$processInfos2 = bqVar2.realmGet$processInfos();
            realmGet$processInfos2.clear();
            for (int i2 = 0; i2 < realmGet$processInfos.size(); i2++) {
                ProcessInfo processInfo = (ProcessInfo) realmGet$processInfos.get(i2);
                ProcessInfo processInfo2 = (ProcessInfo) map.get(processInfo);
                if (processInfo2 != null) {
                    realmGet$processInfos2.add(processInfo2);
                } else {
                    realmGet$processInfos2.add(ay.a(bbVar, processInfo, z, map));
                }
            }
        }
        bf realmGet$features = bqVar.realmGet$features();
        if (realmGet$features == null) {
            return sample2;
        }
        bf realmGet$features2 = bqVar2.realmGet$features();
        realmGet$features2.clear();
        for (int i3 = 0; i3 < realmGet$features.size(); i3++) {
            Feature feature = (Feature) realmGet$features.get(i3);
            Feature feature2 = (Feature) map.get(feature);
            if (feature2 != null) {
                realmGet$features2.add(feature2);
            } else {
                realmGet$features2.add(ac.a(bbVar, feature, z, map));
            }
        }
        return sample2;
    }

    public static OsObjectSchemaInfo b() {
        return f4597a;
    }

    public static String c() {
        return "class_Sample";
    }

    private static OsObjectSchemaInfo e() {
        io.realm.internal.OsObjectSchemaInfo.a aVar = new io.realm.internal.OsObjectSchemaInfo.a("Sample", 28, 0);
        aVar.a("id", RealmFieldType.INTEGER, true, true, true);
        aVar.a("uuId", RealmFieldType.STRING, false, false, false);
        aVar.a("timestamp", RealmFieldType.INTEGER, false, true, true);
        aVar.a("version", RealmFieldType.INTEGER, false, false, true);
        aVar.a("database", RealmFieldType.INTEGER, false, false, true);
        aVar.a("batteryState", RealmFieldType.STRING, false, false, false);
        aVar.a("batteryLevel", RealmFieldType.DOUBLE, false, false, true);
        aVar.a("memoryWired", RealmFieldType.INTEGER, false, false, true);
        aVar.a("memoryActive", RealmFieldType.INTEGER, false, false, true);
        aVar.a("memoryInactive", RealmFieldType.INTEGER, false, false, true);
        aVar.a("memoryFree", RealmFieldType.INTEGER, false, false, true);
        aVar.a("memoryUser", RealmFieldType.INTEGER, false, false, true);
        aVar.a("triggeredBy", RealmFieldType.STRING, false, false, false);
        aVar.a("networkStatus", RealmFieldType.STRING, false, false, false);
        aVar.a("distanceTraveled", RealmFieldType.DOUBLE, false, false, true);
        aVar.a("screenBrightness", RealmFieldType.INTEGER, false, false, true);
        aVar.a("networkDetails", RealmFieldType.OBJECT, "NetworkDetails");
        aVar.a("batteryDetails", RealmFieldType.OBJECT, "BatteryDetails");
        aVar.a("cpuStatus", RealmFieldType.OBJECT, "CpuStatus");
        aVar.a("callInfo", RealmFieldType.OBJECT, "CallInfo");
        aVar.a("screenOn", RealmFieldType.INTEGER, false, false, true);
        aVar.a("timeZone", RealmFieldType.STRING, false, false, false);
        aVar.a("settings", RealmFieldType.OBJECT, "Settings");
        aVar.a("storageDetails", RealmFieldType.OBJECT, "StorageDetails");
        aVar.a("countryCode", RealmFieldType.STRING, false, false, false);
        aVar.a("locationProviders", RealmFieldType.LIST, "LocationProvider");
        aVar.a("processInfos", RealmFieldType.LIST, "ProcessInfo");
        aVar.a("features", RealmFieldType.LIST, "Feature");
        return aVar.a();
    }

    public void a() {
        if (this.d == null) {
            io.realm.e.a aVar = (io.realm.e.a) e.f.get();
            this.c = (a) aVar.c();
            this.d = new ba<>(this);
            this.d.a(aVar.a());
            this.d.a(aVar.b());
            this.d.a(aVar.d());
            this.d.a(aVar.e());
        }
    }

    public ba<?> d() {
        return this.d;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        bp bpVar = (bp) obj;
        String g2 = this.d.a().g();
        String g3 = bpVar.d.a().g();
        if (g2 == null ? g3 != null : !g2.equals(g3)) {
            return false;
        }
        String g4 = this.d.b().b().g();
        String g5 = bpVar.d.b().b().g();
        if (g4 == null ? g5 != null : !g4.equals(g5)) {
            return false;
        }
        return this.d.b().c() == bpVar.d.b().c();
    }

    public int hashCode() {
        int i = 0;
        String g2 = this.d.a().g();
        String g3 = this.d.b().b().g();
        long c2 = this.d.b().c();
        int hashCode = ((g2 != null ? g2.hashCode() : 0) + 527) * 31;
        if (g3 != null) {
            i = g3.hashCode();
        }
        return ((i + hashCode) * 31) + ((int) ((c2 >>> 32) ^ c2));
    }

    public BatteryDetails realmGet$batteryDetails() {
        this.d.a().e();
        if (this.d.b().a(this.c.r)) {
            return null;
        }
        return (BatteryDetails) this.d.a().a(BatteryDetails.class, this.d.b().n(this.c.r), false, Collections.emptyList());
    }

    public double realmGet$batteryLevel() {
        this.d.a().e();
        return this.d.b().j(this.c.g);
    }

    public String realmGet$batteryState() {
        this.d.a().e();
        return this.d.b().l(this.c.f);
    }

    public CallInfo realmGet$callInfo() {
        this.d.a().e();
        if (this.d.b().a(this.c.t)) {
            return null;
        }
        return (CallInfo) this.d.a().a(CallInfo.class, this.d.b().n(this.c.t), false, Collections.emptyList());
    }

    public String realmGet$countryCode() {
        this.d.a().e();
        return this.d.b().l(this.c.y);
    }

    public CpuStatus realmGet$cpuStatus() {
        this.d.a().e();
        if (this.d.b().a(this.c.s)) {
            return null;
        }
        return (CpuStatus) this.d.a().a(CpuStatus.class, this.d.b().n(this.c.s), false, Collections.emptyList());
    }

    public int realmGet$database() {
        this.d.a().e();
        return (int) this.d.b().g(this.c.e);
    }

    public double realmGet$distanceTraveled() {
        this.d.a().e();
        return this.d.b().j(this.c.o);
    }

    public bf<Feature> realmGet$features() {
        this.d.a().e();
        if (this.g != null) {
            return this.g;
        }
        this.g = new bf<>(Feature.class, this.d.b().d(this.c.B), this.d.a());
        return this.g;
    }

    public int realmGet$id() {
        this.d.a().e();
        return (int) this.d.b().g(this.c.f4599a);
    }

    public bf<LocationProvider> realmGet$locationProviders() {
        this.d.a().e();
        if (this.e != null) {
            return this.e;
        }
        this.e = new bf<>(LocationProvider.class, this.d.b().d(this.c.z), this.d.a());
        return this.e;
    }

    public int realmGet$memoryActive() {
        this.d.a().e();
        return (int) this.d.b().g(this.c.i);
    }

    public int realmGet$memoryFree() {
        this.d.a().e();
        return (int) this.d.b().g(this.c.k);
    }

    public int realmGet$memoryInactive() {
        this.d.a().e();
        return (int) this.d.b().g(this.c.j);
    }

    public int realmGet$memoryUser() {
        this.d.a().e();
        return (int) this.d.b().g(this.c.l);
    }

    public int realmGet$memoryWired() {
        this.d.a().e();
        return (int) this.d.b().g(this.c.h);
    }

    public NetworkDetails realmGet$networkDetails() {
        this.d.a().e();
        if (this.d.b().a(this.c.q)) {
            return null;
        }
        return (NetworkDetails) this.d.a().a(NetworkDetails.class, this.d.b().n(this.c.q), false, Collections.emptyList());
    }

    public String realmGet$networkStatus() {
        this.d.a().e();
        return this.d.b().l(this.c.n);
    }

    public bf<ProcessInfo> realmGet$processInfos() {
        this.d.a().e();
        if (this.f != null) {
            return this.f;
        }
        this.f = new bf<>(ProcessInfo.class, this.d.b().d(this.c.A), this.d.a());
        return this.f;
    }

    public int realmGet$screenBrightness() {
        this.d.a().e();
        return (int) this.d.b().g(this.c.p);
    }

    public int realmGet$screenOn() {
        this.d.a().e();
        return (int) this.d.b().g(this.c.u);
    }

    public Settings realmGet$settings() {
        this.d.a().e();
        if (this.d.b().a(this.c.w)) {
            return null;
        }
        return (Settings) this.d.a().a(Settings.class, this.d.b().n(this.c.w), false, Collections.emptyList());
    }

    public StorageDetails realmGet$storageDetails() {
        this.d.a().e();
        if (this.d.b().a(this.c.x)) {
            return null;
        }
        return (StorageDetails) this.d.a().a(StorageDetails.class, this.d.b().n(this.c.x), false, Collections.emptyList());
    }

    public String realmGet$timeZone() {
        this.d.a().e();
        return this.d.b().l(this.c.v);
    }

    public long realmGet$timestamp() {
        this.d.a().e();
        return this.d.b().g(this.c.c);
    }

    public String realmGet$triggeredBy() {
        this.d.a().e();
        return this.d.b().l(this.c.m);
    }

    public String realmGet$uuId() {
        this.d.a().e();
        return this.d.b().l(this.c.f4600b);
    }

    public int realmGet$version() {
        this.d.a().e();
        return (int) this.d.b().g(this.c.d);
    }

    public void realmSet$batteryDetails(BatteryDetails batteryDetails) {
        if (!this.d.f()) {
            this.d.a().e();
            if (batteryDetails == null) {
                this.d.b().o(this.c.r);
                return;
            }
            this.d.a((bh) batteryDetails);
            this.d.b().b(this.c.r, ((m) batteryDetails).d().b().c());
        } else if (this.d.c() && !this.d.d().contains("batteryDetails")) {
            BatteryDetails batteryDetails2 = (batteryDetails == null || bj.isManaged(batteryDetails)) ? batteryDetails : (BatteryDetails) ((bb) this.d.a()).a(batteryDetails);
            o b2 = this.d.b();
            if (batteryDetails2 == null) {
                b2.o(this.c.r);
                return;
            }
            this.d.a((bh) batteryDetails2);
            b2.b().b(this.c.r, b2.c(), ((m) batteryDetails2).d().b().c(), true);
        }
    }

    public void realmSet$batteryLevel(double d2) {
        if (!this.d.f()) {
            this.d.a().e();
            this.d.b().a(this.c.g, d2);
        } else if (this.d.c()) {
            o b2 = this.d.b();
            b2.b().a(this.c.g, b2.c(), d2, true);
        }
    }

    public void realmSet$batteryState(String str) {
        if (!this.d.f()) {
            this.d.a().e();
            if (str == null) {
                this.d.b().c(this.c.f);
            } else {
                this.d.b().a(this.c.f, str);
            }
        } else if (this.d.c()) {
            o b2 = this.d.b();
            if (str == null) {
                b2.b().a(this.c.f, b2.c(), true);
            } else {
                b2.b().a(this.c.f, b2.c(), str, true);
            }
        }
    }

    public void realmSet$callInfo(CallInfo callInfo) {
        if (!this.d.f()) {
            this.d.a().e();
            if (callInfo == null) {
                this.d.b().o(this.c.t);
                return;
            }
            this.d.a((bh) callInfo);
            this.d.b().b(this.c.t, ((m) callInfo).d().b().c());
        } else if (this.d.c() && !this.d.d().contains("callInfo")) {
            CallInfo callInfo2 = (callInfo == null || bj.isManaged(callInfo)) ? callInfo : (CallInfo) ((bb) this.d.a()).a(callInfo);
            o b2 = this.d.b();
            if (callInfo2 == null) {
                b2.o(this.c.t);
                return;
            }
            this.d.a((bh) callInfo2);
            b2.b().b(this.c.t, b2.c(), ((m) callInfo2).d().b().c(), true);
        }
    }

    public void realmSet$countryCode(String str) {
        if (!this.d.f()) {
            this.d.a().e();
            if (str == null) {
                this.d.b().c(this.c.y);
            } else {
                this.d.b().a(this.c.y, str);
            }
        } else if (this.d.c()) {
            o b2 = this.d.b();
            if (str == null) {
                b2.b().a(this.c.y, b2.c(), true);
            } else {
                b2.b().a(this.c.y, b2.c(), str, true);
            }
        }
    }

    public void realmSet$cpuStatus(CpuStatus cpuStatus) {
        if (!this.d.f()) {
            this.d.a().e();
            if (cpuStatus == null) {
                this.d.b().o(this.c.s);
                return;
            }
            this.d.a((bh) cpuStatus);
            this.d.b().b(this.c.s, ((m) cpuStatus).d().b().c());
        } else if (this.d.c() && !this.d.d().contains("cpuStatus")) {
            CpuStatus cpuStatus2 = (cpuStatus == null || bj.isManaged(cpuStatus)) ? cpuStatus : (CpuStatus) ((bb) this.d.a()).a(cpuStatus);
            o b2 = this.d.b();
            if (cpuStatus2 == null) {
                b2.o(this.c.s);
                return;
            }
            this.d.a((bh) cpuStatus2);
            b2.b().b(this.c.s, b2.c(), ((m) cpuStatus2).d().b().c(), true);
        }
    }

    public void realmSet$database(int i) {
        if (!this.d.f()) {
            this.d.a().e();
            this.d.b().a(this.c.e, (long) i);
        } else if (this.d.c()) {
            o b2 = this.d.b();
            b2.b().a(this.c.e, b2.c(), (long) i, true);
        }
    }

    public void realmSet$distanceTraveled(double d2) {
        if (!this.d.f()) {
            this.d.a().e();
            this.d.b().a(this.c.o, d2);
        } else if (this.d.c()) {
            o b2 = this.d.b();
            b2.b().a(this.c.o, b2.c(), d2, true);
        }
    }

    public void realmSet$features(bf<Feature> bfVar) {
        if (this.d.f()) {
            if (!this.d.c() || this.d.d().contains("features")) {
                return;
            }
            if (bfVar != null && !bfVar.a()) {
                bb bbVar = (bb) this.d.a();
                bf<Feature> bfVar2 = new bf<>();
                Iterator it = bfVar.iterator();
                while (it.hasNext()) {
                    Feature feature = (Feature) it.next();
                    if (feature == null || bj.isManaged(feature)) {
                        bfVar2.add(feature);
                    } else {
                        bfVar2.add(bbVar.a(feature));
                    }
                }
                bfVar = bfVar2;
            }
        }
        this.d.a().e();
        OsList d2 = this.d.b().d(this.c.B);
        if (bfVar == null || ((long) bfVar.size()) != d2.c()) {
            d2.b();
            if (bfVar != null) {
                int size = bfVar.size();
                for (int i = 0; i < size; i++) {
                    Feature feature2 = (Feature) bfVar.get(i);
                    this.d.a((bh) feature2);
                    d2.b(((m) feature2).d().b().c());
                }
                return;
            }
            return;
        }
        int size2 = bfVar.size();
        for (int i2 = 0; i2 < size2; i2++) {
            Feature feature3 = (Feature) bfVar.get(i2);
            this.d.a((bh) feature3);
            d2.b((long) i2, ((m) feature3).d().b().c());
        }
    }

    public void realmSet$id(int i) {
        if (!this.d.f()) {
            this.d.a().e();
            throw new RealmException("Primary key field 'id' cannot be changed after object was created.");
        }
    }

    public void realmSet$locationProviders(bf<LocationProvider> bfVar) {
        if (this.d.f()) {
            if (!this.d.c() || this.d.d().contains("locationProviders")) {
                return;
            }
            if (bfVar != null && !bfVar.a()) {
                bb bbVar = (bb) this.d.a();
                bf<LocationProvider> bfVar2 = new bf<>();
                Iterator it = bfVar.iterator();
                while (it.hasNext()) {
                    LocationProvider locationProvider = (LocationProvider) it.next();
                    if (locationProvider == null || bj.isManaged(locationProvider)) {
                        bfVar2.add(locationProvider);
                    } else {
                        bfVar2.add(bbVar.a(locationProvider));
                    }
                }
                bfVar = bfVar2;
            }
        }
        this.d.a().e();
        OsList d2 = this.d.b().d(this.c.z);
        if (bfVar == null || ((long) bfVar.size()) != d2.c()) {
            d2.b();
            if (bfVar != null) {
                int size = bfVar.size();
                for (int i = 0; i < size; i++) {
                    LocationProvider locationProvider2 = (LocationProvider) bfVar.get(i);
                    this.d.a((bh) locationProvider2);
                    d2.b(((m) locationProvider2).d().b().c());
                }
                return;
            }
            return;
        }
        int size2 = bfVar.size();
        for (int i2 = 0; i2 < size2; i2++) {
            LocationProvider locationProvider3 = (LocationProvider) bfVar.get(i2);
            this.d.a((bh) locationProvider3);
            d2.b((long) i2, ((m) locationProvider3).d().b().c());
        }
    }

    public void realmSet$memoryActive(int i) {
        if (!this.d.f()) {
            this.d.a().e();
            this.d.b().a(this.c.i, (long) i);
        } else if (this.d.c()) {
            o b2 = this.d.b();
            b2.b().a(this.c.i, b2.c(), (long) i, true);
        }
    }

    public void realmSet$memoryFree(int i) {
        if (!this.d.f()) {
            this.d.a().e();
            this.d.b().a(this.c.k, (long) i);
        } else if (this.d.c()) {
            o b2 = this.d.b();
            b2.b().a(this.c.k, b2.c(), (long) i, true);
        }
    }

    public void realmSet$memoryInactive(int i) {
        if (!this.d.f()) {
            this.d.a().e();
            this.d.b().a(this.c.j, (long) i);
        } else if (this.d.c()) {
            o b2 = this.d.b();
            b2.b().a(this.c.j, b2.c(), (long) i, true);
        }
    }

    public void realmSet$memoryUser(int i) {
        if (!this.d.f()) {
            this.d.a().e();
            this.d.b().a(this.c.l, (long) i);
        } else if (this.d.c()) {
            o b2 = this.d.b();
            b2.b().a(this.c.l, b2.c(), (long) i, true);
        }
    }

    public void realmSet$memoryWired(int i) {
        if (!this.d.f()) {
            this.d.a().e();
            this.d.b().a(this.c.h, (long) i);
        } else if (this.d.c()) {
            o b2 = this.d.b();
            b2.b().a(this.c.h, b2.c(), (long) i, true);
        }
    }

    public void realmSet$networkDetails(NetworkDetails networkDetails) {
        if (!this.d.f()) {
            this.d.a().e();
            if (networkDetails == null) {
                this.d.b().o(this.c.q);
                return;
            }
            this.d.a((bh) networkDetails);
            this.d.b().b(this.c.q, ((m) networkDetails).d().b().c());
        } else if (this.d.c() && !this.d.d().contains("networkDetails")) {
            NetworkDetails networkDetails2 = (networkDetails == null || bj.isManaged(networkDetails)) ? networkDetails : (NetworkDetails) ((bb) this.d.a()).a(networkDetails);
            o b2 = this.d.b();
            if (networkDetails2 == null) {
                b2.o(this.c.q);
                return;
            }
            this.d.a((bh) networkDetails2);
            b2.b().b(this.c.q, b2.c(), ((m) networkDetails2).d().b().c(), true);
        }
    }

    public void realmSet$networkStatus(String str) {
        if (!this.d.f()) {
            this.d.a().e();
            if (str == null) {
                this.d.b().c(this.c.n);
            } else {
                this.d.b().a(this.c.n, str);
            }
        } else if (this.d.c()) {
            o b2 = this.d.b();
            if (str == null) {
                b2.b().a(this.c.n, b2.c(), true);
            } else {
                b2.b().a(this.c.n, b2.c(), str, true);
            }
        }
    }

    public void realmSet$processInfos(bf<ProcessInfo> bfVar) {
        if (this.d.f()) {
            if (!this.d.c() || this.d.d().contains("processInfos")) {
                return;
            }
            if (bfVar != null && !bfVar.a()) {
                bb bbVar = (bb) this.d.a();
                bf<ProcessInfo> bfVar2 = new bf<>();
                Iterator it = bfVar.iterator();
                while (it.hasNext()) {
                    ProcessInfo processInfo = (ProcessInfo) it.next();
                    if (processInfo == null || bj.isManaged(processInfo)) {
                        bfVar2.add(processInfo);
                    } else {
                        bfVar2.add(bbVar.a(processInfo));
                    }
                }
                bfVar = bfVar2;
            }
        }
        this.d.a().e();
        OsList d2 = this.d.b().d(this.c.A);
        if (bfVar == null || ((long) bfVar.size()) != d2.c()) {
            d2.b();
            if (bfVar != null) {
                int size = bfVar.size();
                for (int i = 0; i < size; i++) {
                    ProcessInfo processInfo2 = (ProcessInfo) bfVar.get(i);
                    this.d.a((bh) processInfo2);
                    d2.b(((m) processInfo2).d().b().c());
                }
                return;
            }
            return;
        }
        int size2 = bfVar.size();
        for (int i2 = 0; i2 < size2; i2++) {
            ProcessInfo processInfo3 = (ProcessInfo) bfVar.get(i2);
            this.d.a((bh) processInfo3);
            d2.b((long) i2, ((m) processInfo3).d().b().c());
        }
    }

    public void realmSet$screenBrightness(int i) {
        if (!this.d.f()) {
            this.d.a().e();
            this.d.b().a(this.c.p, (long) i);
        } else if (this.d.c()) {
            o b2 = this.d.b();
            b2.b().a(this.c.p, b2.c(), (long) i, true);
        }
    }

    public void realmSet$screenOn(int i) {
        if (!this.d.f()) {
            this.d.a().e();
            this.d.b().a(this.c.u, (long) i);
        } else if (this.d.c()) {
            o b2 = this.d.b();
            b2.b().a(this.c.u, b2.c(), (long) i, true);
        }
    }

    public void realmSet$settings(Settings settings) {
        if (!this.d.f()) {
            this.d.a().e();
            if (settings == null) {
                this.d.b().o(this.c.w);
                return;
            }
            this.d.a((bh) settings);
            this.d.b().b(this.c.w, ((m) settings).d().b().c());
        } else if (this.d.c() && !this.d.d().contains("settings")) {
            Settings settings2 = (settings == null || bj.isManaged(settings)) ? settings : (Settings) ((bb) this.d.a()).a(settings);
            o b2 = this.d.b();
            if (settings2 == null) {
                b2.o(this.c.w);
                return;
            }
            this.d.a((bh) settings2);
            b2.b().b(this.c.w, b2.c(), ((m) settings2).d().b().c(), true);
        }
    }

    public void realmSet$storageDetails(StorageDetails storageDetails) {
        if (!this.d.f()) {
            this.d.a().e();
            if (storageDetails == null) {
                this.d.b().o(this.c.x);
                return;
            }
            this.d.a((bh) storageDetails);
            this.d.b().b(this.c.x, ((m) storageDetails).d().b().c());
        } else if (this.d.c() && !this.d.d().contains("storageDetails")) {
            StorageDetails storageDetails2 = (storageDetails == null || bj.isManaged(storageDetails)) ? storageDetails : (StorageDetails) ((bb) this.d.a()).a(storageDetails);
            o b2 = this.d.b();
            if (storageDetails2 == null) {
                b2.o(this.c.x);
                return;
            }
            this.d.a((bh) storageDetails2);
            b2.b().b(this.c.x, b2.c(), ((m) storageDetails2).d().b().c(), true);
        }
    }

    public void realmSet$timeZone(String str) {
        if (!this.d.f()) {
            this.d.a().e();
            if (str == null) {
                this.d.b().c(this.c.v);
            } else {
                this.d.b().a(this.c.v, str);
            }
        } else if (this.d.c()) {
            o b2 = this.d.b();
            if (str == null) {
                b2.b().a(this.c.v, b2.c(), true);
            } else {
                b2.b().a(this.c.v, b2.c(), str, true);
            }
        }
    }

    public void realmSet$timestamp(long j) {
        if (!this.d.f()) {
            this.d.a().e();
            this.d.b().a(this.c.c, j);
        } else if (this.d.c()) {
            o b2 = this.d.b();
            b2.b().a(this.c.c, b2.c(), j, true);
        }
    }

    public void realmSet$triggeredBy(String str) {
        if (!this.d.f()) {
            this.d.a().e();
            if (str == null) {
                this.d.b().c(this.c.m);
            } else {
                this.d.b().a(this.c.m, str);
            }
        } else if (this.d.c()) {
            o b2 = this.d.b();
            if (str == null) {
                b2.b().a(this.c.m, b2.c(), true);
            } else {
                b2.b().a(this.c.m, b2.c(), str, true);
            }
        }
    }

    public void realmSet$uuId(String str) {
        if (!this.d.f()) {
            this.d.a().e();
            if (str == null) {
                this.d.b().c(this.c.f4600b);
            } else {
                this.d.b().a(this.c.f4600b, str);
            }
        } else if (this.d.c()) {
            o b2 = this.d.b();
            if (str == null) {
                b2.b().a(this.c.f4600b, b2.c(), true);
            } else {
                b2.b().a(this.c.f4600b, b2.c(), str, true);
            }
        }
    }

    public void realmSet$version(int i) {
        if (!this.d.f()) {
            this.d.a().e();
            this.d.b().a(this.c.d, (long) i);
        } else if (this.d.c()) {
            o b2 = this.d.b();
            b2.b().a(this.c.d, b2.c(), (long) i, true);
        }
    }

    public String toString() {
        if (!bj.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder sb = new StringBuilder("Sample = proxy[");
        sb.append("{id:");
        sb.append(realmGet$id());
        sb.append("}");
        sb.append(",");
        sb.append("{uuId:");
        sb.append(realmGet$uuId() != null ? realmGet$uuId() : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{timestamp:");
        sb.append(realmGet$timestamp());
        sb.append("}");
        sb.append(",");
        sb.append("{version:");
        sb.append(realmGet$version());
        sb.append("}");
        sb.append(",");
        sb.append("{database:");
        sb.append(realmGet$database());
        sb.append("}");
        sb.append(",");
        sb.append("{batteryState:");
        sb.append(realmGet$batteryState() != null ? realmGet$batteryState() : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{batteryLevel:");
        sb.append(realmGet$batteryLevel());
        sb.append("}");
        sb.append(",");
        sb.append("{memoryWired:");
        sb.append(realmGet$memoryWired());
        sb.append("}");
        sb.append(",");
        sb.append("{memoryActive:");
        sb.append(realmGet$memoryActive());
        sb.append("}");
        sb.append(",");
        sb.append("{memoryInactive:");
        sb.append(realmGet$memoryInactive());
        sb.append("}");
        sb.append(",");
        sb.append("{memoryFree:");
        sb.append(realmGet$memoryFree());
        sb.append("}");
        sb.append(",");
        sb.append("{memoryUser:");
        sb.append(realmGet$memoryUser());
        sb.append("}");
        sb.append(",");
        sb.append("{triggeredBy:");
        sb.append(realmGet$triggeredBy() != null ? realmGet$triggeredBy() : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{networkStatus:");
        sb.append(realmGet$networkStatus() != null ? realmGet$networkStatus() : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{distanceTraveled:");
        sb.append(realmGet$distanceTraveled());
        sb.append("}");
        sb.append(",");
        sb.append("{screenBrightness:");
        sb.append(realmGet$screenBrightness());
        sb.append("}");
        sb.append(",");
        sb.append("{networkDetails:");
        sb.append(realmGet$networkDetails() != null ? "NetworkDetails" : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{batteryDetails:");
        sb.append(realmGet$batteryDetails() != null ? "BatteryDetails" : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{cpuStatus:");
        sb.append(realmGet$cpuStatus() != null ? "CpuStatus" : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{callInfo:");
        sb.append(realmGet$callInfo() != null ? "CallInfo" : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{screenOn:");
        sb.append(realmGet$screenOn());
        sb.append("}");
        sb.append(",");
        sb.append("{timeZone:");
        sb.append(realmGet$timeZone() != null ? realmGet$timeZone() : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{settings:");
        sb.append(realmGet$settings() != null ? "Settings" : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{storageDetails:");
        sb.append(realmGet$storageDetails() != null ? "StorageDetails" : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{countryCode:");
        sb.append(realmGet$countryCode() != null ? realmGet$countryCode() : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{locationProviders:");
        sb.append("RealmList<LocationProvider>[").append(realmGet$locationProviders().size()).append("]");
        sb.append("}");
        sb.append(",");
        sb.append("{processInfos:");
        sb.append("RealmList<ProcessInfo>[").append(realmGet$processInfos().size()).append("]");
        sb.append("}");
        sb.append(",");
        sb.append("{features:");
        sb.append("RealmList<Feature>[").append(realmGet$features().size()).append("]");
        sb.append("}");
        sb.append("]");
        return sb.toString();
    }
}
