package io.realm;

import io.realm.exceptions.RealmFileException;
import io.realm.exceptions.RealmFileException.Kind;
import io.realm.internal.OsObjectStore;
import io.realm.internal.SharedRealm;
import io.realm.internal.Table;
import io.realm.internal.Util;
import io.realm.internal.i;
import io.realm.log.RealmLog;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.util.Arrays;
import java.util.Collection;
import java.util.EnumMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicBoolean;

final class bc {
    private static final List<WeakReference<bc>> d = new LinkedList();
    private static final Collection<bc> f = new ConcurrentLinkedQueue();

    /* renamed from: a reason: collision with root package name */
    private final EnumMap<b, c> f4572a;

    /* renamed from: b reason: collision with root package name */
    private final String f4573b;
    private be c;
    private final AtomicBoolean e = new AtomicBoolean(false);

    interface a {
        void a(int i);
    }

    private enum b {
        TYPED_REALM,
        DYNAMIC_REALM;

        static b a(Class<? extends e> cls) {
            if (cls == bb.class) {
                return TYPED_REALM;
            }
            if (cls == aa.class) {
                return DYNAMIC_REALM;
            }
            throw new IllegalArgumentException("The type of Realm class must be Realm or DynamicRealm.");
        }
    }

    private static class c {
        /* access modifiers changed from: private */

        /* renamed from: a reason: collision with root package name */
        public final ThreadLocal<e> f4578a;
        /* access modifiers changed from: private */

        /* renamed from: b reason: collision with root package name */
        public final ThreadLocal<Integer> f4579b;
        /* access modifiers changed from: private */
        public int c;

        private c() {
            this.f4578a = new ThreadLocal<>();
            this.f4579b = new ThreadLocal<>();
            this.c = 0;
        }
    }

    private bc(String str) {
        this.f4573b = str;
        this.f4572a = new EnumMap<>(b.class);
        for (b put : b.values()) {
            this.f4572a.put(put, new c());
        }
    }

    private static bc a(String str, boolean z) {
        bc bcVar = null;
        synchronized (d) {
            Iterator it = d.iterator();
            while (it.hasNext()) {
                bc bcVar2 = (bc) ((WeakReference) it.next()).get();
                if (bcVar2 == null) {
                    it.remove();
                    bcVar2 = bcVar;
                } else if (!bcVar2.f4573b.equals(str)) {
                    bcVar2 = bcVar;
                }
                bcVar = bcVar2;
            }
            if (bcVar == null && z) {
                bcVar = new bc(str);
                d.add(new WeakReference(bcVar));
            }
        }
        return bcVar;
    }

    static <E extends e> E a(be beVar, Class<E> cls) {
        return a(beVar.m(), true).b(beVar, cls);
    }

    private synchronized void a(a aVar) {
        aVar.a(c());
    }

    private void a(be beVar) {
        if (!this.c.equals(beVar)) {
            if (!Arrays.equals(this.c.c(), beVar.c())) {
                throw new IllegalArgumentException("Wrong key used to decrypt Realm.");
            }
            bg e2 = beVar.e();
            bg e3 = this.c.e();
            if (e3 == null || e2 == null || !e3.getClass().equals(e2.getClass()) || e2.equals(e3)) {
                throw new IllegalArgumentException("Configurations cannot be different if used to open the same file. \nCached configuration: \n" + this.c + "\n\nNew configuration: \n" + beVar);
            }
            throw new IllegalArgumentException("Configurations cannot be different if used to open the same file. The most likely cause is that equals() and hashCode() are not overridden in the migration class: " + beVar.e().getClass().getCanonicalName());
        }
    }

    static void a(be beVar, a aVar) {
        synchronized (d) {
            bc a2 = a(beVar.m(), false);
            if (a2 == null) {
                aVar.a(0);
            } else {
                a2.a(aVar);
            }
        }
    }

    private synchronized <E extends e> E b(be beVar, Class<E> cls) {
        c cVar;
        Object a2;
        SharedRealm sharedRealm;
        SharedRealm sharedRealm2;
        cVar = (c) this.f4572a.get(b.a(cls));
        if (c() == 0) {
            b(beVar);
            boolean n = beVar.n();
            try {
                if (beVar.s()) {
                    if (!n) {
                        sharedRealm2 = SharedRealm.getInstance(beVar);
                        i.a().e(beVar);
                    }
                    sharedRealm2 = null;
                } else {
                    if (n) {
                        sharedRealm2 = SharedRealm.getInstance(beVar);
                        Table.a(sharedRealm2);
                    }
                    sharedRealm2 = null;
                }
                if (sharedRealm2 != null) {
                    sharedRealm2.close();
                }
                this.c = beVar;
            } catch (Throwable th) {
                th = th;
                sharedRealm = null;
            }
        } else {
            a(beVar);
        }
        if (cVar.f4578a.get() == null) {
            if (cls == bb.class) {
                a2 = bb.a(this);
            } else if (cls == aa.class) {
                a2 = aa.a(this);
            } else {
                throw new IllegalArgumentException("The type of Realm class must be Realm or DynamicRealm.");
            }
            cVar.f4578a.set(a2);
            cVar.f4579b.set(Integer.valueOf(0));
            cVar.c = cVar.c + 1;
        }
        cVar.f4579b.set(Integer.valueOf(((Integer) cVar.f4579b.get()).intValue() + 1));
        return (e) cVar.f4578a.get();
        if (sharedRealm != null) {
            sharedRealm.close();
        }
        throw th;
    }

    private static void b(final be beVar) {
        final File file = beVar.j() ? new File(beVar.a(), beVar.b()) : null;
        final String c2 = i.a(beVar.s()).c(beVar);
        final boolean z = !Util.a(c2);
        if (file != null || z) {
            OsObjectStore.a(beVar, (Runnable) new Runnable() {
                public void run() {
                    if (file != null) {
                        bc.b(beVar.k(), file);
                    }
                    if (z) {
                        bc.b(c2, new File(i.a(beVar.s()).d(beVar)));
                    }
                }
            });
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0050 A[SYNTHETIC, Splitter:B:15:0x0050] */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0055 A[SYNTHETIC, Splitter:B:18:0x0055] */
    public static void b(String str, File file) {
        FileOutputStream fileOutputStream;
        InputStream inputStream;
        Throwable th;
        IOException e2 = null;
        if (!file.exists()) {
            try {
                InputStream open = e.f4615a.getAssets().open(str);
                if (open == null) {
                    try {
                        throw new RealmFileException(Kind.ACCESS_ERROR, "Invalid input stream to the asset file: " + str);
                    } catch (IOException e3) {
                        e = e3;
                        inputStream = open;
                        fileOutputStream = null;
                        try {
                            throw new RealmFileException(Kind.ACCESS_ERROR, "Could not resolve the path to the asset file: " + str, e);
                        } catch (Throwable th2) {
                            th = th2;
                            if (inputStream != null) {
                            }
                            if (fileOutputStream != null) {
                            }
                            throw th;
                        }
                    } catch (Throwable th3) {
                        th = th3;
                        inputStream = open;
                        fileOutputStream = null;
                        if (inputStream != null) {
                        }
                        if (fileOutputStream != null) {
                        }
                        throw th;
                    }
                } else {
                    FileOutputStream fileOutputStream2 = new FileOutputStream(file);
                    try {
                        byte[] bArr = new byte[4096];
                        while (true) {
                            int read = open.read(bArr);
                            if (read <= -1) {
                                break;
                            }
                            fileOutputStream2.write(bArr, 0, read);
                        }
                        if (open != null) {
                            try {
                                open.close();
                                th = null;
                            } catch (IOException e4) {
                                th = e4;
                            }
                        } else {
                            th = null;
                        }
                        if (fileOutputStream2 != null) {
                            try {
                                fileOutputStream2.close();
                            } catch (IOException e5) {
                                e = e5;
                                if (th != null) {
                                    e = th;
                                }
                                th = e;
                            }
                        }
                        if (th != null) {
                            throw new RealmFileException(Kind.ACCESS_ERROR, th);
                        }
                    } catch (IOException e6) {
                        e = e6;
                        FileOutputStream fileOutputStream3 = fileOutputStream2;
                        inputStream = open;
                        fileOutputStream = fileOutputStream3;
                        throw new RealmFileException(Kind.ACCESS_ERROR, "Could not resolve the path to the asset file: " + str, e);
                    } catch (Throwable th4) {
                        th = th4;
                        FileOutputStream fileOutputStream4 = fileOutputStream2;
                        inputStream = open;
                        fileOutputStream = fileOutputStream4;
                        if (inputStream != null) {
                        }
                        if (fileOutputStream != null) {
                        }
                        throw th;
                    }
                }
            } catch (IOException e7) {
                e = e7;
                fileOutputStream = null;
                inputStream = null;
            } catch (Throwable th5) {
                th = th5;
                fileOutputStream = null;
                inputStream = null;
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException e8) {
                        e2 = e8;
                    }
                }
                if (fileOutputStream != null) {
                    try {
                        fileOutputStream.close();
                    } catch (IOException e9) {
                        if (e2 == null) {
                        }
                    }
                }
                throw th;
            }
        }
    }

    private int c() {
        int i = 0;
        Iterator it = this.f4572a.values().iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                return i2;
            }
            i = ((c) it.next()).c + i2;
        }
    }

    public be a() {
        return this.c;
    }

    /* access modifiers changed from: 0000 */
    public synchronized void a(e eVar) {
        String g = eVar.g();
        c cVar = (c) this.f4572a.get(b.a(eVar.getClass()));
        Integer num = (Integer) cVar.f4579b.get();
        if (num == null) {
            num = Integer.valueOf(0);
        }
        if (num.intValue() <= 0) {
            RealmLog.a("%s has been closed already. refCount is %s", g, num);
        } else {
            Integer valueOf = Integer.valueOf(num.intValue() - 1);
            if (valueOf.intValue() == 0) {
                cVar.f4579b.set(null);
                cVar.f4578a.set(null);
                cVar.c = cVar.c - 1;
                if (cVar.c < 0) {
                    throw new IllegalStateException("Global reference counter of Realm" + g + " got corrupted.");
                }
                eVar.i();
                if (c() == 0) {
                    this.c = null;
                    i.a(eVar.h().s()).a(eVar.h());
                }
            } else {
                cVar.f4579b.set(valueOf);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void b() {
        if (!this.e.getAndSet(true)) {
            f.add(this);
        }
    }
}
