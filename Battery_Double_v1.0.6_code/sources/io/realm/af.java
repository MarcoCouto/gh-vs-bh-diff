package io.realm;

import io.realm.internal.OsList;
import java.util.Locale;

final class af extends al<Float> {
    af(e eVar, OsList osList, Class<Float> cls) {
        super(eVar, osList, cls);
    }

    /* renamed from: a */
    public Float b(int i) {
        return (Float) this.f4548b.f((long) i);
    }

    public void a(int i, Object obj) {
        this.f4548b.a((long) i, ((Number) obj).floatValue());
    }

    /* access modifiers changed from: protected */
    public void a(Object obj) {
        if (obj != null && !(obj instanceof Number)) {
            throw new IllegalArgumentException(String.format(Locale.ENGLISH, "Unacceptable value type. Acceptable: %1$s, actual: %2$s .", new Object[]{"java.lang.Number", obj.getClass().getName()}));
        }
    }

    /* access modifiers changed from: protected */
    public void b(int i, Object obj) {
        this.f4548b.b((long) i, ((Number) obj).floatValue());
    }

    public void b(Object obj) {
        this.f4548b.a(((Number) obj).floatValue());
    }
}
