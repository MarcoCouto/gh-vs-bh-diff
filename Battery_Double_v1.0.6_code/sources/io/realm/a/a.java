package io.realm.a;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.realm.aa;
import io.realm.ab;
import io.realm.bb;
import io.realm.be;
import io.realm.bf;
import io.realm.bh;
import io.realm.bn;
import java.util.IdentityHashMap;
import java.util.Map;

public class a implements b {
    private static final BackpressureStrategy d = BackpressureStrategy.LATEST;

    /* renamed from: a reason: collision with root package name */
    private ThreadLocal<C0075a<bn>> f4520a = new ThreadLocal<C0075a<bn>>() {
        /* access modifiers changed from: protected */
        /* renamed from: a */
        public C0075a<bn> initialValue() {
            return new C0075a<>();
        }
    };

    /* renamed from: b reason: collision with root package name */
    private ThreadLocal<C0075a<bf>> f4521b = new ThreadLocal<C0075a<bf>>() {
        /* access modifiers changed from: protected */
        /* renamed from: a */
        public C0075a<bf> initialValue() {
            return new C0075a<>();
        }
    };
    private ThreadLocal<C0075a<bh>> c = new ThreadLocal<C0075a<bh>>() {
        /* access modifiers changed from: protected */
        /* renamed from: a */
        public C0075a<bh> initialValue() {
            return new C0075a<>();
        }
    };

    /* renamed from: io.realm.a.a$a reason: collision with other inner class name */
    private static class C0075a<K> {

        /* renamed from: a reason: collision with root package name */
        private final Map<K, Integer> f4533a;

        private C0075a() {
            this.f4533a = new IdentityHashMap();
        }
    }

    public Flowable<ab> a(aa aaVar, final ab abVar) {
        final be h = aaVar.h();
        return Flowable.create(new Object() {
        }, d);
    }

    public <E extends bh> Flowable<E> a(bb bbVar, final E e) {
        final be h = bbVar.h();
        return Flowable.create(new Object() {
        }, d);
    }

    public Observable<Object<ab>> b(aa aaVar, final ab abVar) {
        final be h = aaVar.h();
        return Observable.create(new Object() {
        });
    }

    public <E extends bh> Observable<Object<E>> b(bb bbVar, final E e) {
        final be h = bbVar.h();
        return Observable.create(new Object() {
        });
    }

    public boolean equals(Object obj) {
        return obj instanceof a;
    }

    public int hashCode() {
        return 37;
    }
}
