package io.realm.a;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.realm.aa;
import io.realm.ab;
import io.realm.bb;
import io.realm.bh;

public interface b {
    Flowable<ab> a(aa aaVar, ab abVar);

    <E extends bh> Flowable<E> a(bb bbVar, E e);

    Observable<Object<ab>> b(aa aaVar, ab abVar);

    <E extends bh> Observable<Object<E>> b(bb bbVar, E e);
}
