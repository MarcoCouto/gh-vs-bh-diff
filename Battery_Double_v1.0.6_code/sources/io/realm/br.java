package io.realm;

import io.realm.internal.a.c.a;
import io.realm.internal.c;

class br implements a {

    /* renamed from: a reason: collision with root package name */
    private final bo f4601a;

    public br(bo boVar) {
        this.f4601a = boVar;
    }

    public c a(String str) {
        return this.f4601a.c(str);
    }

    public boolean a() {
        return this.f4601a.a();
    }

    public long b(String str) {
        return this.f4601a.b(str).getNativePtr();
    }
}
