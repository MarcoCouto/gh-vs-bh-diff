package io.realm;

public interface bw {
    int realmGet$free();

    int realmGet$freeExternal();

    int realmGet$freeSecondary();

    int realmGet$freeSystem();

    int realmGet$total();

    int realmGet$totalExternal();

    int realmGet$totalSecondary();

    int realmGet$totalSystem();

    void realmSet$free(int i);

    void realmSet$freeExternal(int i);

    void realmSet$freeSecondary(int i);

    void realmSet$freeSystem(int i);

    void realmSet$total(int i);

    void realmSet$totalExternal(int i);

    void realmSet$totalSecondary(int i);

    void realmSet$totalSystem(int i);
}
