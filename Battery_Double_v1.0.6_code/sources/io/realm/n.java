package io.realm;

import com.hmatalonga.greenhub.models.data.CallInfo;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.c;
import io.realm.internal.m;
import io.realm.internal.o;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class n extends CallInfo implements m, o {

    /* renamed from: a reason: collision with root package name */
    private static final OsObjectSchemaInfo f4712a = e();

    /* renamed from: b reason: collision with root package name */
    private static final List<String> f4713b;
    private a c;
    private ba<CallInfo> d;

    static final class a extends c {

        /* renamed from: a reason: collision with root package name */
        long f4714a;

        /* renamed from: b reason: collision with root package name */
        long f4715b;
        long c;
        long d;

        a(OsSchemaInfo osSchemaInfo) {
            super(4);
            OsObjectSchemaInfo a2 = osSchemaInfo.a("CallInfo");
            this.f4714a = a("incomingCallTime", a2);
            this.f4715b = a("outgoingCallTime", a2);
            this.c = a("nonCallTime", a2);
            this.d = a("callStatus", a2);
        }

        /* access modifiers changed from: protected */
        public final void a(c cVar, c cVar2) {
            a aVar = (a) cVar;
            a aVar2 = (a) cVar2;
            aVar2.f4714a = aVar.f4714a;
            aVar2.f4715b = aVar.f4715b;
            aVar2.c = aVar.c;
            aVar2.d = aVar.d;
        }
    }

    static {
        ArrayList arrayList = new ArrayList(4);
        arrayList.add("incomingCallTime");
        arrayList.add("outgoingCallTime");
        arrayList.add("nonCallTime");
        arrayList.add("callStatus");
        f4713b = Collections.unmodifiableList(arrayList);
    }

    n() {
        this.d.g();
    }

    public static CallInfo a(bb bbVar, CallInfo callInfo, boolean z, Map<bh, m> map) {
        if ((callInfo instanceof m) && ((m) callInfo).d().a() != null) {
            e a2 = ((m) callInfo).d().a();
            if (a2.c != bbVar.c) {
                throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
            } else if (a2.g().equals(bbVar.g())) {
                return callInfo;
            }
        }
        io.realm.e.a aVar = (io.realm.e.a) e.f.get();
        m mVar = (m) map.get(callInfo);
        return mVar != null ? (CallInfo) mVar : b(bbVar, callInfo, z, map);
    }

    public static a a(OsSchemaInfo osSchemaInfo) {
        return new a(osSchemaInfo);
    }

    public static CallInfo b(bb bbVar, CallInfo callInfo, boolean z, Map<bh, m> map) {
        m mVar = (m) map.get(callInfo);
        if (mVar != null) {
            return (CallInfo) mVar;
        }
        CallInfo callInfo2 = (CallInfo) bbVar.a(CallInfo.class, false, Collections.emptyList());
        map.put(callInfo, (m) callInfo2);
        o oVar = callInfo;
        o oVar2 = callInfo2;
        oVar2.realmSet$incomingCallTime(oVar.realmGet$incomingCallTime());
        oVar2.realmSet$outgoingCallTime(oVar.realmGet$outgoingCallTime());
        oVar2.realmSet$nonCallTime(oVar.realmGet$nonCallTime());
        oVar2.realmSet$callStatus(oVar.realmGet$callStatus());
        return callInfo2;
    }

    public static OsObjectSchemaInfo b() {
        return f4712a;
    }

    public static String c() {
        return "class_CallInfo";
    }

    private static OsObjectSchemaInfo e() {
        io.realm.internal.OsObjectSchemaInfo.a aVar = new io.realm.internal.OsObjectSchemaInfo.a("CallInfo", 4, 0);
        aVar.a("incomingCallTime", RealmFieldType.DOUBLE, false, false, true);
        aVar.a("outgoingCallTime", RealmFieldType.DOUBLE, false, false, true);
        aVar.a("nonCallTime", RealmFieldType.DOUBLE, false, false, true);
        aVar.a("callStatus", RealmFieldType.STRING, false, false, false);
        return aVar.a();
    }

    public void a() {
        if (this.d == null) {
            io.realm.e.a aVar = (io.realm.e.a) e.f.get();
            this.c = (a) aVar.c();
            this.d = new ba<>(this);
            this.d.a(aVar.a());
            this.d.a(aVar.b());
            this.d.a(aVar.d());
            this.d.a(aVar.e());
        }
    }

    public ba<?> d() {
        return this.d;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        n nVar = (n) obj;
        String g = this.d.a().g();
        String g2 = nVar.d.a().g();
        if (g == null ? g2 != null : !g.equals(g2)) {
            return false;
        }
        String g3 = this.d.b().b().g();
        String g4 = nVar.d.b().b().g();
        if (g3 == null ? g4 != null : !g3.equals(g4)) {
            return false;
        }
        return this.d.b().c() == nVar.d.b().c();
    }

    public int hashCode() {
        int i = 0;
        String g = this.d.a().g();
        String g2 = this.d.b().b().g();
        long c2 = this.d.b().c();
        int hashCode = ((g != null ? g.hashCode() : 0) + 527) * 31;
        if (g2 != null) {
            i = g2.hashCode();
        }
        return ((i + hashCode) * 31) + ((int) ((c2 >>> 32) ^ c2));
    }

    public String realmGet$callStatus() {
        this.d.a().e();
        return this.d.b().l(this.c.d);
    }

    public double realmGet$incomingCallTime() {
        this.d.a().e();
        return this.d.b().j(this.c.f4714a);
    }

    public double realmGet$nonCallTime() {
        this.d.a().e();
        return this.d.b().j(this.c.c);
    }

    public double realmGet$outgoingCallTime() {
        this.d.a().e();
        return this.d.b().j(this.c.f4715b);
    }

    public void realmSet$callStatus(String str) {
        if (!this.d.f()) {
            this.d.a().e();
            if (str == null) {
                this.d.b().c(this.c.d);
            } else {
                this.d.b().a(this.c.d, str);
            }
        } else if (this.d.c()) {
            o b2 = this.d.b();
            if (str == null) {
                b2.b().a(this.c.d, b2.c(), true);
            } else {
                b2.b().a(this.c.d, b2.c(), str, true);
            }
        }
    }

    public void realmSet$incomingCallTime(double d2) {
        if (!this.d.f()) {
            this.d.a().e();
            this.d.b().a(this.c.f4714a, d2);
        } else if (this.d.c()) {
            o b2 = this.d.b();
            b2.b().a(this.c.f4714a, b2.c(), d2, true);
        }
    }

    public void realmSet$nonCallTime(double d2) {
        if (!this.d.f()) {
            this.d.a().e();
            this.d.b().a(this.c.c, d2);
        } else if (this.d.c()) {
            o b2 = this.d.b();
            b2.b().a(this.c.c, b2.c(), d2, true);
        }
    }

    public void realmSet$outgoingCallTime(double d2) {
        if (!this.d.f()) {
            this.d.a().e();
            this.d.b().a(this.c.f4715b, d2);
        } else if (this.d.c()) {
            o b2 = this.d.b();
            b2.b().a(this.c.f4715b, b2.c(), d2, true);
        }
    }

    public String toString() {
        if (!bj.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder sb = new StringBuilder("CallInfo = proxy[");
        sb.append("{incomingCallTime:");
        sb.append(realmGet$incomingCallTime());
        sb.append("}");
        sb.append(",");
        sb.append("{outgoingCallTime:");
        sb.append(realmGet$outgoingCallTime());
        sb.append("}");
        sb.append(",");
        sb.append("{nonCallTime:");
        sb.append(realmGet$nonCallTime());
        sb.append("}");
        sb.append(",");
        sb.append("{callStatus:");
        sb.append(realmGet$callStatus() != null ? realmGet$callStatus() : "null");
        sb.append("}");
        sb.append("]");
        return sb.toString();
    }
}
