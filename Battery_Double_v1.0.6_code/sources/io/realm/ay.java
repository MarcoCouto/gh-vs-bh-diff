package io.realm;

import com.hmatalonga.greenhub.models.data.AppPermission;
import com.hmatalonga.greenhub.models.data.AppSignature;
import com.hmatalonga.greenhub.models.data.ProcessInfo;
import io.realm.internal.OsList;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.c;
import io.realm.internal.m;
import io.realm.internal.o;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class ay extends ProcessInfo implements az, m {

    /* renamed from: a reason: collision with root package name */
    private static final OsObjectSchemaInfo f4565a = e();

    /* renamed from: b reason: collision with root package name */
    private static final List<String> f4566b;
    private a c;
    private ba<ProcessInfo> d;
    private bf<AppPermission> e;
    private bf<AppSignature> f;

    static final class a extends c {

        /* renamed from: a reason: collision with root package name */
        long f4567a;

        /* renamed from: b reason: collision with root package name */
        long f4568b;
        long c;
        long d;
        long e;
        long f;
        long g;
        long h;
        long i;
        long j;

        a(OsSchemaInfo osSchemaInfo) {
            super(10);
            OsObjectSchemaInfo a2 = osSchemaInfo.a("ProcessInfo");
            this.f4567a = a("processId", a2);
            this.f4568b = a("name", a2);
            this.c = a("applicationLabel", a2);
            this.d = a("isSystemApp", a2);
            this.e = a("importance", a2);
            this.f = a("versionName", a2);
            this.g = a("versionCode", a2);
            this.h = a("installationPkg", a2);
            this.i = a("appPermissions", a2);
            this.j = a("appSignatures", a2);
        }

        /* access modifiers changed from: protected */
        public final void a(c cVar, c cVar2) {
            a aVar = (a) cVar;
            a aVar2 = (a) cVar2;
            aVar2.f4567a = aVar.f4567a;
            aVar2.f4568b = aVar.f4568b;
            aVar2.c = aVar.c;
            aVar2.d = aVar.d;
            aVar2.e = aVar.e;
            aVar2.f = aVar.f;
            aVar2.g = aVar.g;
            aVar2.h = aVar.h;
            aVar2.i = aVar.i;
            aVar2.j = aVar.j;
        }
    }

    static {
        ArrayList arrayList = new ArrayList(10);
        arrayList.add("processId");
        arrayList.add("name");
        arrayList.add("applicationLabel");
        arrayList.add("isSystemApp");
        arrayList.add("importance");
        arrayList.add("versionName");
        arrayList.add("versionCode");
        arrayList.add("installationPkg");
        arrayList.add("appPermissions");
        arrayList.add("appSignatures");
        f4566b = Collections.unmodifiableList(arrayList);
    }

    ay() {
        this.d.g();
    }

    public static ProcessInfo a(bb bbVar, ProcessInfo processInfo, boolean z, Map<bh, m> map) {
        if ((processInfo instanceof m) && ((m) processInfo).d().a() != null) {
            e a2 = ((m) processInfo).d().a();
            if (a2.c != bbVar.c) {
                throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
            } else if (a2.g().equals(bbVar.g())) {
                return processInfo;
            }
        }
        io.realm.e.a aVar = (io.realm.e.a) e.f.get();
        m mVar = (m) map.get(processInfo);
        return mVar != null ? (ProcessInfo) mVar : b(bbVar, processInfo, z, map);
    }

    public static a a(OsSchemaInfo osSchemaInfo) {
        return new a(osSchemaInfo);
    }

    public static ProcessInfo b(bb bbVar, ProcessInfo processInfo, boolean z, Map<bh, m> map) {
        m mVar = (m) map.get(processInfo);
        if (mVar != null) {
            return (ProcessInfo) mVar;
        }
        ProcessInfo processInfo2 = (ProcessInfo) bbVar.a(ProcessInfo.class, false, Collections.emptyList());
        map.put(processInfo, (m) processInfo2);
        az azVar = processInfo;
        az azVar2 = processInfo2;
        azVar2.realmSet$processId(azVar.realmGet$processId());
        azVar2.realmSet$name(azVar.realmGet$name());
        azVar2.realmSet$applicationLabel(azVar.realmGet$applicationLabel());
        azVar2.realmSet$isSystemApp(azVar.realmGet$isSystemApp());
        azVar2.realmSet$importance(azVar.realmGet$importance());
        azVar2.realmSet$versionName(azVar.realmGet$versionName());
        azVar2.realmSet$versionCode(azVar.realmGet$versionCode());
        azVar2.realmSet$installationPkg(azVar.realmGet$installationPkg());
        bf realmGet$appPermissions = azVar.realmGet$appPermissions();
        if (realmGet$appPermissions != null) {
            bf realmGet$appPermissions2 = azVar2.realmGet$appPermissions();
            realmGet$appPermissions2.clear();
            for (int i = 0; i < realmGet$appPermissions.size(); i++) {
                AppPermission appPermission = (AppPermission) realmGet$appPermissions.get(i);
                AppPermission appPermission2 = (AppPermission) map.get(appPermission);
                if (appPermission2 != null) {
                    realmGet$appPermissions2.add(appPermission2);
                } else {
                    realmGet$appPermissions2.add(a.a(bbVar, appPermission, z, map));
                }
            }
        }
        bf realmGet$appSignatures = azVar.realmGet$appSignatures();
        if (realmGet$appSignatures == null) {
            return processInfo2;
        }
        bf realmGet$appSignatures2 = azVar2.realmGet$appSignatures();
        realmGet$appSignatures2.clear();
        for (int i2 = 0; i2 < realmGet$appSignatures.size(); i2++) {
            AppSignature appSignature = (AppSignature) realmGet$appSignatures.get(i2);
            AppSignature appSignature2 = (AppSignature) map.get(appSignature);
            if (appSignature2 != null) {
                realmGet$appSignatures2.add(appSignature2);
            } else {
                realmGet$appSignatures2.add(c.a(bbVar, appSignature, z, map));
            }
        }
        return processInfo2;
    }

    public static OsObjectSchemaInfo b() {
        return f4565a;
    }

    public static String c() {
        return "class_ProcessInfo";
    }

    private static OsObjectSchemaInfo e() {
        io.realm.internal.OsObjectSchemaInfo.a aVar = new io.realm.internal.OsObjectSchemaInfo.a("ProcessInfo", 10, 0);
        aVar.a("processId", RealmFieldType.INTEGER, false, false, true);
        aVar.a("name", RealmFieldType.STRING, false, false, false);
        aVar.a("applicationLabel", RealmFieldType.STRING, false, false, false);
        aVar.a("isSystemApp", RealmFieldType.BOOLEAN, false, false, true);
        aVar.a("importance", RealmFieldType.STRING, false, false, false);
        aVar.a("versionName", RealmFieldType.STRING, false, false, false);
        aVar.a("versionCode", RealmFieldType.INTEGER, false, false, true);
        aVar.a("installationPkg", RealmFieldType.STRING, false, false, false);
        aVar.a("appPermissions", RealmFieldType.LIST, "AppPermission");
        aVar.a("appSignatures", RealmFieldType.LIST, "AppSignature");
        return aVar.a();
    }

    public void a() {
        if (this.d == null) {
            io.realm.e.a aVar = (io.realm.e.a) e.f.get();
            this.c = (a) aVar.c();
            this.d = new ba<>(this);
            this.d.a(aVar.a());
            this.d.a(aVar.b());
            this.d.a(aVar.d());
            this.d.a(aVar.e());
        }
    }

    public ba<?> d() {
        return this.d;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        ay ayVar = (ay) obj;
        String g = this.d.a().g();
        String g2 = ayVar.d.a().g();
        if (g == null ? g2 != null : !g.equals(g2)) {
            return false;
        }
        String g3 = this.d.b().b().g();
        String g4 = ayVar.d.b().b().g();
        if (g3 == null ? g4 != null : !g3.equals(g4)) {
            return false;
        }
        return this.d.b().c() == ayVar.d.b().c();
    }

    public int hashCode() {
        int i = 0;
        String g = this.d.a().g();
        String g2 = this.d.b().b().g();
        long c2 = this.d.b().c();
        int hashCode = ((g != null ? g.hashCode() : 0) + 527) * 31;
        if (g2 != null) {
            i = g2.hashCode();
        }
        return ((i + hashCode) * 31) + ((int) ((c2 >>> 32) ^ c2));
    }

    public bf<AppPermission> realmGet$appPermissions() {
        this.d.a().e();
        if (this.e != null) {
            return this.e;
        }
        this.e = new bf<>(AppPermission.class, this.d.b().d(this.c.i), this.d.a());
        return this.e;
    }

    public bf<AppSignature> realmGet$appSignatures() {
        this.d.a().e();
        if (this.f != null) {
            return this.f;
        }
        this.f = new bf<>(AppSignature.class, this.d.b().d(this.c.j), this.d.a());
        return this.f;
    }

    public String realmGet$applicationLabel() {
        this.d.a().e();
        return this.d.b().l(this.c.c);
    }

    public String realmGet$importance() {
        this.d.a().e();
        return this.d.b().l(this.c.e);
    }

    public String realmGet$installationPkg() {
        this.d.a().e();
        return this.d.b().l(this.c.h);
    }

    public boolean realmGet$isSystemApp() {
        this.d.a().e();
        return this.d.b().h(this.c.d);
    }

    public String realmGet$name() {
        this.d.a().e();
        return this.d.b().l(this.c.f4568b);
    }

    public int realmGet$processId() {
        this.d.a().e();
        return (int) this.d.b().g(this.c.f4567a);
    }

    public int realmGet$versionCode() {
        this.d.a().e();
        return (int) this.d.b().g(this.c.g);
    }

    public String realmGet$versionName() {
        this.d.a().e();
        return this.d.b().l(this.c.f);
    }

    public void realmSet$appPermissions(bf<AppPermission> bfVar) {
        if (this.d.f()) {
            if (!this.d.c() || this.d.d().contains("appPermissions")) {
                return;
            }
            if (bfVar != null && !bfVar.a()) {
                bb bbVar = (bb) this.d.a();
                bf<AppPermission> bfVar2 = new bf<>();
                Iterator it = bfVar.iterator();
                while (it.hasNext()) {
                    AppPermission appPermission = (AppPermission) it.next();
                    if (appPermission == null || bj.isManaged(appPermission)) {
                        bfVar2.add(appPermission);
                    } else {
                        bfVar2.add(bbVar.a(appPermission));
                    }
                }
                bfVar = bfVar2;
            }
        }
        this.d.a().e();
        OsList d2 = this.d.b().d(this.c.i);
        if (bfVar == null || ((long) bfVar.size()) != d2.c()) {
            d2.b();
            if (bfVar != null) {
                int size = bfVar.size();
                for (int i = 0; i < size; i++) {
                    AppPermission appPermission2 = (AppPermission) bfVar.get(i);
                    this.d.a((bh) appPermission2);
                    d2.b(((m) appPermission2).d().b().c());
                }
                return;
            }
            return;
        }
        int size2 = bfVar.size();
        for (int i2 = 0; i2 < size2; i2++) {
            AppPermission appPermission3 = (AppPermission) bfVar.get(i2);
            this.d.a((bh) appPermission3);
            d2.b((long) i2, ((m) appPermission3).d().b().c());
        }
    }

    public void realmSet$appSignatures(bf<AppSignature> bfVar) {
        if (this.d.f()) {
            if (!this.d.c() || this.d.d().contains("appSignatures")) {
                return;
            }
            if (bfVar != null && !bfVar.a()) {
                bb bbVar = (bb) this.d.a();
                bf<AppSignature> bfVar2 = new bf<>();
                Iterator it = bfVar.iterator();
                while (it.hasNext()) {
                    AppSignature appSignature = (AppSignature) it.next();
                    if (appSignature == null || bj.isManaged(appSignature)) {
                        bfVar2.add(appSignature);
                    } else {
                        bfVar2.add(bbVar.a(appSignature));
                    }
                }
                bfVar = bfVar2;
            }
        }
        this.d.a().e();
        OsList d2 = this.d.b().d(this.c.j);
        if (bfVar == null || ((long) bfVar.size()) != d2.c()) {
            d2.b();
            if (bfVar != null) {
                int size = bfVar.size();
                for (int i = 0; i < size; i++) {
                    AppSignature appSignature2 = (AppSignature) bfVar.get(i);
                    this.d.a((bh) appSignature2);
                    d2.b(((m) appSignature2).d().b().c());
                }
                return;
            }
            return;
        }
        int size2 = bfVar.size();
        for (int i2 = 0; i2 < size2; i2++) {
            AppSignature appSignature3 = (AppSignature) bfVar.get(i2);
            this.d.a((bh) appSignature3);
            d2.b((long) i2, ((m) appSignature3).d().b().c());
        }
    }

    public void realmSet$applicationLabel(String str) {
        if (!this.d.f()) {
            this.d.a().e();
            if (str == null) {
                this.d.b().c(this.c.c);
            } else {
                this.d.b().a(this.c.c, str);
            }
        } else if (this.d.c()) {
            o b2 = this.d.b();
            if (str == null) {
                b2.b().a(this.c.c, b2.c(), true);
            } else {
                b2.b().a(this.c.c, b2.c(), str, true);
            }
        }
    }

    public void realmSet$importance(String str) {
        if (!this.d.f()) {
            this.d.a().e();
            if (str == null) {
                this.d.b().c(this.c.e);
            } else {
                this.d.b().a(this.c.e, str);
            }
        } else if (this.d.c()) {
            o b2 = this.d.b();
            if (str == null) {
                b2.b().a(this.c.e, b2.c(), true);
            } else {
                b2.b().a(this.c.e, b2.c(), str, true);
            }
        }
    }

    public void realmSet$installationPkg(String str) {
        if (!this.d.f()) {
            this.d.a().e();
            if (str == null) {
                this.d.b().c(this.c.h);
            } else {
                this.d.b().a(this.c.h, str);
            }
        } else if (this.d.c()) {
            o b2 = this.d.b();
            if (str == null) {
                b2.b().a(this.c.h, b2.c(), true);
            } else {
                b2.b().a(this.c.h, b2.c(), str, true);
            }
        }
    }

    public void realmSet$isSystemApp(boolean z) {
        if (!this.d.f()) {
            this.d.a().e();
            this.d.b().a(this.c.d, z);
        } else if (this.d.c()) {
            o b2 = this.d.b();
            b2.b().a(this.c.d, b2.c(), z, true);
        }
    }

    public void realmSet$name(String str) {
        if (!this.d.f()) {
            this.d.a().e();
            if (str == null) {
                this.d.b().c(this.c.f4568b);
            } else {
                this.d.b().a(this.c.f4568b, str);
            }
        } else if (this.d.c()) {
            o b2 = this.d.b();
            if (str == null) {
                b2.b().a(this.c.f4568b, b2.c(), true);
            } else {
                b2.b().a(this.c.f4568b, b2.c(), str, true);
            }
        }
    }

    public void realmSet$processId(int i) {
        if (!this.d.f()) {
            this.d.a().e();
            this.d.b().a(this.c.f4567a, (long) i);
        } else if (this.d.c()) {
            o b2 = this.d.b();
            b2.b().a(this.c.f4567a, b2.c(), (long) i, true);
        }
    }

    public void realmSet$versionCode(int i) {
        if (!this.d.f()) {
            this.d.a().e();
            this.d.b().a(this.c.g, (long) i);
        } else if (this.d.c()) {
            o b2 = this.d.b();
            b2.b().a(this.c.g, b2.c(), (long) i, true);
        }
    }

    public void realmSet$versionName(String str) {
        if (!this.d.f()) {
            this.d.a().e();
            if (str == null) {
                this.d.b().c(this.c.f);
            } else {
                this.d.b().a(this.c.f, str);
            }
        } else if (this.d.c()) {
            o b2 = this.d.b();
            if (str == null) {
                b2.b().a(this.c.f, b2.c(), true);
            } else {
                b2.b().a(this.c.f, b2.c(), str, true);
            }
        }
    }

    public String toString() {
        if (!bj.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder sb = new StringBuilder("ProcessInfo = proxy[");
        sb.append("{processId:");
        sb.append(realmGet$processId());
        sb.append("}");
        sb.append(",");
        sb.append("{name:");
        sb.append(realmGet$name() != null ? realmGet$name() : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{applicationLabel:");
        sb.append(realmGet$applicationLabel() != null ? realmGet$applicationLabel() : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{isSystemApp:");
        sb.append(realmGet$isSystemApp());
        sb.append("}");
        sb.append(",");
        sb.append("{importance:");
        sb.append(realmGet$importance() != null ? realmGet$importance() : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{versionName:");
        sb.append(realmGet$versionName() != null ? realmGet$versionName() : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{versionCode:");
        sb.append(realmGet$versionCode());
        sb.append("}");
        sb.append(",");
        sb.append("{installationPkg:");
        sb.append(realmGet$installationPkg() != null ? realmGet$installationPkg() : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{appPermissions:");
        sb.append("RealmList<AppPermission>[").append(realmGet$appPermissions().size()).append("]");
        sb.append("}");
        sb.append(",");
        sb.append("{appSignatures:");
        sb.append("RealmList<AppSignature>[").append(realmGet$appSignatures().size()).append("]");
        sb.append("}");
        sb.append("]");
        return sb.toString();
    }
}
