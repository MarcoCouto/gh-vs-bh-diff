package io.realm;

import io.realm.internal.OsList;
import java.util.Locale;

final class m extends al<Boolean> {
    m(e eVar, OsList osList, Class<Boolean> cls) {
        super(eVar, osList, cls);
    }

    /* renamed from: a */
    public Boolean b(int i) {
        return (Boolean) this.f4548b.f((long) i);
    }

    public void a(int i, Object obj) {
        this.f4548b.a((long) i, ((Boolean) obj).booleanValue());
    }

    /* access modifiers changed from: protected */
    public void a(Object obj) {
        if (obj != null && !(obj instanceof Boolean)) {
            throw new IllegalArgumentException(String.format(Locale.ENGLISH, "Unacceptable value type. Acceptable: %1$s, actual: %2$s .", new Object[]{"java.lang.Boolean", obj.getClass().getName()}));
        }
    }

    /* access modifiers changed from: protected */
    public void b(int i, Object obj) {
        this.f4548b.b((long) i, ((Boolean) obj).booleanValue());
    }

    public void b(Object obj) {
        this.f4548b.a(((Boolean) obj).booleanValue());
    }
}
