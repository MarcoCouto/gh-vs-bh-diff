package io.realm;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.realm.internal.f;
import io.realm.internal.m;
import io.realm.internal.o;

public abstract class bj implements bh {
    static final String MSG_DELETED_OBJECT = "the object is already deleted.";
    static final String MSG_DYNAMIC_OBJECT = "the object is an instance of DynamicRealmObject. Use DynamicRealmObject.getDynamicRealm() instead.";
    static final String MSG_NULL_OBJECT = "'model' is null.";

    public static <E extends bh> void addChangeListener(E e, bd<E> bdVar) {
        addChangeListener(e, (bk<E>) new b<E>(bdVar));
    }

    public static <E extends bh> void addChangeListener(E e, bk<E> bkVar) {
        if (e == null) {
            throw new IllegalArgumentException("Object should not be null");
        } else if (bkVar == null) {
            throw new IllegalArgumentException("Listener should not be null");
        } else if (e instanceof m) {
            m mVar = (m) e;
            e a2 = mVar.d().a();
            a2.e();
            a2.e.capabilities.a("Listeners cannot be used on current thread.");
            mVar.d().a(bkVar);
        } else {
            throw new IllegalArgumentException("Cannot add listener from this unmanaged RealmObject (created outside of Realm)");
        }
    }

    public static <E extends bh> Observable<Object<E>> asChangesetObservable(E e) {
        if (e instanceof m) {
            e a2 = ((m) e).d().a();
            if (a2 instanceof bb) {
                return a2.d.o().b((bb) a2, e);
            }
            if (a2 instanceof aa) {
                return a2.d.o().b((aa) a2, (ab) e);
            }
            throw new UnsupportedOperationException(a2.getClass() + " does not support RxJava. See https://realm.io/docs/java/latest/#rxjava for more details.");
        }
        throw new IllegalArgumentException("Cannot create Observables from unmanaged RealmObjects");
    }

    public static <E extends bh> Flowable<E> asFlowable(E e) {
        if (e instanceof m) {
            e a2 = ((m) e).d().a();
            if (a2 instanceof bb) {
                return a2.d.o().a((bb) a2, e);
            }
            if (a2 instanceof aa) {
                return a2.d.o().a((aa) a2, (ab) e);
            }
            throw new UnsupportedOperationException(a2.getClass() + " does not support RxJava. See https://realm.io/docs/java/latest/#rxjava for more details.");
        }
        throw new IllegalArgumentException("Cannot create Observables from unmanaged RealmObjects");
    }

    public static <E extends bh> void deleteFromRealm(E e) {
        if (!(e instanceof m)) {
            throw new IllegalArgumentException("Object not managed by Realm, so it cannot be removed.");
        }
        m mVar = (m) e;
        if (mVar.d().b() == null) {
            throw new IllegalStateException("Object malformed: missing object in Realm. Make sure to instantiate RealmObjects with Realm.createObject()");
        } else if (mVar.d().a() == null) {
            throw new IllegalStateException("Object malformed: missing Realm. Make sure to instantiate RealmObjects with Realm.createObject()");
        } else {
            mVar.d().a().e();
            o b2 = mVar.d().b();
            b2.b().d(b2.c());
            mVar.d().a((o) f.INSTANCE);
        }
    }

    public static bb getRealm(bh bhVar) {
        if (bhVar == null) {
            throw new IllegalArgumentException(MSG_NULL_OBJECT);
        } else if (bhVar instanceof ab) {
            throw new IllegalStateException(MSG_DYNAMIC_OBJECT);
        } else if (!(bhVar instanceof m)) {
            return null;
        } else {
            e a2 = ((m) bhVar).d().a();
            a2.e();
            if (isValid(bhVar)) {
                return (bb) a2;
            }
            throw new IllegalStateException(MSG_DELETED_OBJECT);
        }
    }

    public static <E extends bh> boolean isLoaded(E e) {
        if (!(e instanceof m)) {
            return true;
        }
        m mVar = (m) e;
        mVar.d().a().e();
        return mVar.d().h();
    }

    public static <E extends bh> boolean isManaged(E e) {
        return e instanceof m;
    }

    public static <E extends bh> boolean isValid(E e) {
        if (!(e instanceof m)) {
            return true;
        }
        o b2 = ((m) e).d().b();
        return b2 != null && b2.d();
    }

    public static <E extends bh> boolean load(E e) {
        if (isLoaded(e)) {
            return true;
        }
        if (!(e instanceof m)) {
            return false;
        }
        ((m) e).d().i();
        return true;
    }

    public static <E extends bh> void removeAllChangeListeners(E e) {
        if (e instanceof m) {
            m mVar = (m) e;
            e a2 = mVar.d().a();
            a2.e();
            a2.e.capabilities.a("Listeners cannot be used on current thread.");
            mVar.d().e();
            return;
        }
        throw new IllegalArgumentException("Cannot remove listeners from this unmanaged RealmObject (created outside of Realm)");
    }

    public static <E extends bh> void removeChangeListener(E e, bd<E> bdVar) {
        removeChangeListener(e, (bk) new b(bdVar));
    }

    public static <E extends bh> void removeChangeListener(E e, bk bkVar) {
        if (e == null) {
            throw new IllegalArgumentException("Object should not be null");
        } else if (bkVar == null) {
            throw new IllegalArgumentException("Listener should not be null");
        } else if (e instanceof m) {
            m mVar = (m) e;
            e a2 = mVar.d().a();
            a2.e();
            a2.e.capabilities.a("Listeners cannot be used on current thread.");
            mVar.d().b(bkVar);
        } else {
            throw new IllegalArgumentException("Cannot remove listener from this unmanaged RealmObject (created outside of Realm)");
        }
    }

    public final <E extends bh> void addChangeListener(bd<E> bdVar) {
        addChangeListener((E) this, bdVar);
    }

    public final <E extends bh> void addChangeListener(bk<E> bkVar) {
        addChangeListener((E) this, bkVar);
    }

    public final <E extends bj> Observable<Object<E>> asChangesetObservable() {
        return asChangesetObservable(this);
    }

    public final <E extends bj> Flowable<E> asFlowable() {
        return asFlowable(this);
    }

    public final void deleteFromRealm() {
        deleteFromRealm(this);
    }

    public bb getRealm() {
        return getRealm(this);
    }

    public final boolean isLoaded() {
        return isLoaded(this);
    }

    public boolean isManaged() {
        return isManaged(this);
    }

    public final boolean isValid() {
        return isValid(this);
    }

    public final boolean load() {
        return load(this);
    }

    public final void removeAllChangeListeners() {
        removeAllChangeListeners(this);
    }

    public final void removeChangeListener(bd bdVar) {
        removeChangeListener((E) this, bdVar);
    }

    public final void removeChangeListener(bk bkVar) {
        removeChangeListener((E) this, bkVar);
    }
}
