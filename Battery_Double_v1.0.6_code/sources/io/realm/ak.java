package io.realm;

import io.realm.internal.OsList;
import java.util.Locale;

final class ak<T> extends al<T> {
    ak(e eVar, OsList osList, Class<T> cls) {
        super(eVar, osList, cls);
    }

    public void a(int i, Object obj) {
        this.f4548b.c((long) i, ((Number) obj).longValue());
    }

    /* access modifiers changed from: protected */
    public void a(Object obj) {
        if (obj != null && !(obj instanceof Number)) {
            throw new IllegalArgumentException(String.format(Locale.ENGLISH, "Unacceptable value type. Acceptable: %1$s, actual: %2$s .", new Object[]{"java.lang.Long, java.lang.Integer, java.lang.Short, java.lang.Byte", obj.getClass().getName()}));
        }
    }

    public T b(int i) {
        T t = (Long) this.f4548b.f((long) i);
        if (t == null) {
            return null;
        }
        if (this.c == Long.class) {
            return t;
        }
        if (this.c == Integer.class) {
            return this.c.cast(Integer.valueOf(t.intValue()));
        }
        if (this.c == Short.class) {
            return this.c.cast(Short.valueOf(t.shortValue()));
        }
        if (this.c == Byte.class) {
            return this.c.cast(Byte.valueOf(t.byteValue()));
        }
        throw new IllegalStateException("Unexpected element type: " + this.c.getName());
    }

    /* access modifiers changed from: protected */
    public void b(int i, Object obj) {
        this.f4548b.d((long) i, ((Number) obj).longValue());
    }

    public void b(Object obj) {
        this.f4548b.e(((Number) obj).longValue());
    }
}
