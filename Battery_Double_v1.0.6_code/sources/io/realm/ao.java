package io.realm;

import io.realm.internal.OsObjectStore;
import io.realm.internal.Table;
import java.util.Locale;

class ao extends bl {
    ao(e eVar, bo boVar, Table table) {
        super(eVar, boVar, table, new a(table));
    }

    private void a(String str, ae[] aeVarArr) {
        if (aeVarArr != null) {
            try {
                if (aeVarArr.length > 0) {
                    if (a(aeVarArr, ae.INDEXED)) {
                        b(str);
                    }
                    if (a(aeVarArr, ae.PRIMARY_KEY)) {
                        c(str);
                    }
                }
            } catch (Exception e) {
                long g = g(str);
                if (0 != 0) {
                    this.e.j(g);
                }
                throw ((RuntimeException) e);
            }
        }
    }

    static boolean a(ae[] aeVarArr, ae aeVar) {
        if (aeVarArr == null || aeVarArr.length == 0) {
            return false;
        }
        for (ae aeVar2 : aeVarArr) {
            if (aeVar2 == aeVar) {
                return true;
            }
        }
        return false;
    }

    private void c() {
        if (this.d.d.s()) {
            throw new UnsupportedOperationException("'addPrimaryKey' is not supported by synced Realms.");
        }
    }

    private void h(String str) {
        e(str);
        i(str);
    }

    private void i(String str) {
        if (this.e.a(str) != -1) {
            throw new IllegalArgumentException("Field already exists in '" + a() + "': " + str);
        }
    }

    public bl a(String str) {
        this.d.f();
        e(str);
        if (!d(str)) {
            throw new IllegalStateException(str + " does not exist.");
        }
        long g = g(str);
        String a2 = a();
        if (str.equals(OsObjectStore.a(this.d.e, a2))) {
            OsObjectStore.a(this.d.e, a2, str);
        }
        this.e.a(g);
        return this;
    }

    public bl a(String str, Class<?> cls, ae... aeVarArr) {
        boolean z = false;
        b bVar = (b) f4588a.get(cls);
        if (bVar != null) {
            if (a(aeVarArr, ae.PRIMARY_KEY)) {
                c();
            }
            h(str);
            boolean z2 = bVar.c;
            if (!a(aeVarArr, ae.REQUIRED)) {
                z = z2;
            }
            long a2 = this.e.a(bVar.f4591a, str, z);
            try {
                a(str, aeVarArr);
                return this;
            } catch (Exception e) {
                this.e.a(a2);
                throw e;
            }
        } else if (f4589b.containsKey(cls)) {
            throw new IllegalArgumentException("Use addRealmObjectField() instead to add fields that link to other RealmObjects: " + str);
        } else {
            throw new IllegalArgumentException(String.format(Locale.US, "Realm doesn't support this field type: %s(%s)", new Object[]{str, cls}));
        }
    }

    public bl b(String str) {
        e(str);
        f(str);
        long g = g(str);
        if (this.e.k(g)) {
            throw new IllegalStateException(str + " already has an index.");
        }
        this.e.i(g);
        return this;
    }

    public bl c(String str) {
        c();
        e(str);
        f(str);
        String a2 = OsObjectStore.a(this.d.e, a());
        if (a2 != null) {
            throw new IllegalStateException(String.format(Locale.ENGLISH, "Field '%s' has been already defined as primary key.", new Object[]{a2}));
        }
        long g = g(str);
        if (!this.e.k(g)) {
            this.e.i(g);
        }
        OsObjectStore.a(this.d.e, a(), str);
        return this;
    }
}
