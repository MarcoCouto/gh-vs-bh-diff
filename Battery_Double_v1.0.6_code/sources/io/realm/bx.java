package io.realm;

import io.realm.internal.OsList;
import java.util.Locale;

final class bx extends al<String> {
    bx(e eVar, OsList osList, Class<String> cls) {
        super(eVar, osList, cls);
    }

    /* renamed from: a */
    public String b(int i) {
        return (String) this.f4548b.f((long) i);
    }

    public void a(int i, Object obj) {
        this.f4548b.a((long) i, (String) obj);
    }

    /* access modifiers changed from: protected */
    public void a(Object obj) {
        if (obj != null && !(obj instanceof String)) {
            throw new IllegalArgumentException(String.format(Locale.ENGLISH, "Unacceptable value type. Acceptable: %1$s, actual: %2$s .", new Object[]{"java.lang.String", obj.getClass().getName()}));
        }
    }

    /* access modifiers changed from: protected */
    public void b(int i, Object obj) {
        this.f4548b.b((long) i, (String) obj);
    }

    public void b(Object obj) {
        this.f4548b.a((String) obj);
    }
}
