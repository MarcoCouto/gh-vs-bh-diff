package io.realm;

import com.hmatalonga.greenhub.models.data.Message;
import io.realm.exceptions.RealmException;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.Table;
import io.realm.internal.c;
import io.realm.internal.m;
import io.realm.internal.o;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class am extends Message implements an, m {

    /* renamed from: a reason: collision with root package name */
    private static final OsObjectSchemaInfo f4549a = e();

    /* renamed from: b reason: collision with root package name */
    private static final List<String> f4550b;
    private a c;
    private ba<Message> d;

    static final class a extends c {

        /* renamed from: a reason: collision with root package name */
        long f4551a;

        /* renamed from: b reason: collision with root package name */
        long f4552b;
        long c;
        long d;
        long e;
        long f;

        a(OsSchemaInfo osSchemaInfo) {
            super(6);
            OsObjectSchemaInfo a2 = osSchemaInfo.a("Message");
            this.f4551a = a("id", a2);
            this.f4552b = a("type", a2);
            this.c = a("title", a2);
            this.d = a("body", a2);
            this.e = a("date", a2);
            this.f = a("read", a2);
        }

        /* access modifiers changed from: protected */
        public final void a(c cVar, c cVar2) {
            a aVar = (a) cVar;
            a aVar2 = (a) cVar2;
            aVar2.f4551a = aVar.f4551a;
            aVar2.f4552b = aVar.f4552b;
            aVar2.c = aVar.c;
            aVar2.d = aVar.d;
            aVar2.e = aVar.e;
            aVar2.f = aVar.f;
        }
    }

    static {
        ArrayList arrayList = new ArrayList(6);
        arrayList.add("id");
        arrayList.add("type");
        arrayList.add("title");
        arrayList.add("body");
        arrayList.add("date");
        arrayList.add("read");
        f4550b = Collections.unmodifiableList(arrayList);
    }

    am() {
        this.d.g();
    }

    static Message a(bb bbVar, Message message, Message message2, Map<bh, m> map) {
        an anVar = message;
        an anVar2 = message2;
        anVar.realmSet$type(anVar2.realmGet$type());
        anVar.realmSet$title(anVar2.realmGet$title());
        anVar.realmSet$body(anVar2.realmGet$body());
        anVar.realmSet$date(anVar2.realmGet$date());
        anVar.realmSet$read(anVar2.realmGet$read());
        return message;
    }

    /* JADX INFO: finally extract failed */
    public static Message a(bb bbVar, Message message, boolean z, Map<bh, m> map) {
        boolean z2;
        am amVar;
        if ((message instanceof m) && ((m) message).d().a() != null) {
            e a2 = ((m) message).d().a();
            if (a2.c != bbVar.c) {
                throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
            } else if (a2.g().equals(bbVar.g())) {
                return message;
            }
        }
        io.realm.e.a aVar = (io.realm.e.a) e.f.get();
        m mVar = (m) map.get(message);
        if (mVar != null) {
            return (Message) mVar;
        }
        if (z) {
            Table b2 = bbVar.b(Message.class);
            long a3 = b2.a(((a) bbVar.k().c(Message.class)).f4551a, (long) message.realmGet$id());
            if (a3 == -1) {
                z2 = false;
                amVar = null;
            } else {
                try {
                    aVar.a(bbVar, b2.f(a3), bbVar.k().c(Message.class), false, Collections.emptyList());
                    amVar = new am();
                    map.put(message, amVar);
                    aVar.f();
                    z2 = z;
                } catch (Throwable th) {
                    aVar.f();
                    throw th;
                }
            }
        } else {
            z2 = z;
            amVar = null;
        }
        return z2 ? a(bbVar, (Message) amVar, message, map) : b(bbVar, message, z, map);
    }

    public static a a(OsSchemaInfo osSchemaInfo) {
        return new a(osSchemaInfo);
    }

    public static Message b(bb bbVar, Message message, boolean z, Map<bh, m> map) {
        m mVar = (m) map.get(message);
        if (mVar != null) {
            return (Message) mVar;
        }
        Message message2 = (Message) bbVar.a(Message.class, Integer.valueOf(message.realmGet$id()), false, Collections.emptyList());
        map.put(message, (m) message2);
        an anVar = message;
        an anVar2 = message2;
        anVar2.realmSet$type(anVar.realmGet$type());
        anVar2.realmSet$title(anVar.realmGet$title());
        anVar2.realmSet$body(anVar.realmGet$body());
        anVar2.realmSet$date(anVar.realmGet$date());
        anVar2.realmSet$read(anVar.realmGet$read());
        return message2;
    }

    public static OsObjectSchemaInfo b() {
        return f4549a;
    }

    public static String c() {
        return "class_Message";
    }

    private static OsObjectSchemaInfo e() {
        io.realm.internal.OsObjectSchemaInfo.a aVar = new io.realm.internal.OsObjectSchemaInfo.a("Message", 6, 0);
        aVar.a("id", RealmFieldType.INTEGER, true, true, true);
        aVar.a("type", RealmFieldType.STRING, false, false, false);
        aVar.a("title", RealmFieldType.STRING, false, false, false);
        aVar.a("body", RealmFieldType.STRING, false, false, false);
        aVar.a("date", RealmFieldType.STRING, false, false, false);
        aVar.a("read", RealmFieldType.BOOLEAN, false, false, true);
        return aVar.a();
    }

    public void a() {
        if (this.d == null) {
            io.realm.e.a aVar = (io.realm.e.a) e.f.get();
            this.c = (a) aVar.c();
            this.d = new ba<>(this);
            this.d.a(aVar.a());
            this.d.a(aVar.b());
            this.d.a(aVar.d());
            this.d.a(aVar.e());
        }
    }

    public ba<?> d() {
        return this.d;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        am amVar = (am) obj;
        String g = this.d.a().g();
        String g2 = amVar.d.a().g();
        if (g == null ? g2 != null : !g.equals(g2)) {
            return false;
        }
        String g3 = this.d.b().b().g();
        String g4 = amVar.d.b().b().g();
        if (g3 == null ? g4 != null : !g3.equals(g4)) {
            return false;
        }
        return this.d.b().c() == amVar.d.b().c();
    }

    public int hashCode() {
        int i = 0;
        String g = this.d.a().g();
        String g2 = this.d.b().b().g();
        long c2 = this.d.b().c();
        int hashCode = ((g != null ? g.hashCode() : 0) + 527) * 31;
        if (g2 != null) {
            i = g2.hashCode();
        }
        return ((i + hashCode) * 31) + ((int) ((c2 >>> 32) ^ c2));
    }

    public String realmGet$body() {
        this.d.a().e();
        return this.d.b().l(this.c.d);
    }

    public String realmGet$date() {
        this.d.a().e();
        return this.d.b().l(this.c.e);
    }

    public int realmGet$id() {
        this.d.a().e();
        return (int) this.d.b().g(this.c.f4551a);
    }

    public boolean realmGet$read() {
        this.d.a().e();
        return this.d.b().h(this.c.f);
    }

    public String realmGet$title() {
        this.d.a().e();
        return this.d.b().l(this.c.c);
    }

    public String realmGet$type() {
        this.d.a().e();
        return this.d.b().l(this.c.f4552b);
    }

    public void realmSet$body(String str) {
        if (!this.d.f()) {
            this.d.a().e();
            if (str == null) {
                this.d.b().c(this.c.d);
            } else {
                this.d.b().a(this.c.d, str);
            }
        } else if (this.d.c()) {
            o b2 = this.d.b();
            if (str == null) {
                b2.b().a(this.c.d, b2.c(), true);
            } else {
                b2.b().a(this.c.d, b2.c(), str, true);
            }
        }
    }

    public void realmSet$date(String str) {
        if (!this.d.f()) {
            this.d.a().e();
            if (str == null) {
                this.d.b().c(this.c.e);
            } else {
                this.d.b().a(this.c.e, str);
            }
        } else if (this.d.c()) {
            o b2 = this.d.b();
            if (str == null) {
                b2.b().a(this.c.e, b2.c(), true);
            } else {
                b2.b().a(this.c.e, b2.c(), str, true);
            }
        }
    }

    public void realmSet$id(int i) {
        if (!this.d.f()) {
            this.d.a().e();
            throw new RealmException("Primary key field 'id' cannot be changed after object was created.");
        }
    }

    public void realmSet$read(boolean z) {
        if (!this.d.f()) {
            this.d.a().e();
            this.d.b().a(this.c.f, z);
        } else if (this.d.c()) {
            o b2 = this.d.b();
            b2.b().a(this.c.f, b2.c(), z, true);
        }
    }

    public void realmSet$title(String str) {
        if (!this.d.f()) {
            this.d.a().e();
            if (str == null) {
                this.d.b().c(this.c.c);
            } else {
                this.d.b().a(this.c.c, str);
            }
        } else if (this.d.c()) {
            o b2 = this.d.b();
            if (str == null) {
                b2.b().a(this.c.c, b2.c(), true);
            } else {
                b2.b().a(this.c.c, b2.c(), str, true);
            }
        }
    }

    public void realmSet$type(String str) {
        if (!this.d.f()) {
            this.d.a().e();
            if (str == null) {
                this.d.b().c(this.c.f4552b);
            } else {
                this.d.b().a(this.c.f4552b, str);
            }
        } else if (this.d.c()) {
            o b2 = this.d.b();
            if (str == null) {
                b2.b().a(this.c.f4552b, b2.c(), true);
            } else {
                b2.b().a(this.c.f4552b, b2.c(), str, true);
            }
        }
    }

    public String toString() {
        if (!bj.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder sb = new StringBuilder("Message = proxy[");
        sb.append("{id:");
        sb.append(realmGet$id());
        sb.append("}");
        sb.append(",");
        sb.append("{type:");
        sb.append(realmGet$type() != null ? realmGet$type() : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{title:");
        sb.append(realmGet$title() != null ? realmGet$title() : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{body:");
        sb.append(realmGet$body() != null ? realmGet$body() : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{date:");
        sb.append(realmGet$date() != null ? realmGet$date() : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{read:");
        sb.append(realmGet$read());
        sb.append("}");
        sb.append("]");
        return sb.toString();
    }
}
