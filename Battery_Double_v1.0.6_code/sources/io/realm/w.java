package io.realm;

import io.realm.internal.OsList;
import java.util.Date;
import java.util.Locale;

final class w extends al<Date> {
    w(e eVar, OsList osList, Class<Date> cls) {
        super(eVar, osList, cls);
    }

    /* renamed from: a */
    public Date b(int i) {
        return (Date) this.f4548b.f((long) i);
    }

    public void a(int i, Object obj) {
        this.f4548b.a((long) i, (Date) obj);
    }

    /* access modifiers changed from: protected */
    public void a(Object obj) {
        if (obj != null && !(obj instanceof Date)) {
            throw new IllegalArgumentException(String.format(Locale.ENGLISH, "Unacceptable value type. Acceptable: %1$s, actual: %2$s .", new Object[]{"java.util.Date", obj.getClass().getName()}));
        }
    }

    /* access modifiers changed from: protected */
    public void b(int i, Object obj) {
        this.f4548b.b((long) i, (Date) obj);
    }

    public void b(Object obj) {
        this.f4548b.a((Date) obj);
    }
}
