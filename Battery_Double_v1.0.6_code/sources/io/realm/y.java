package io.realm;

public interface y {
    String realmGet$brand();

    int realmGet$isRoot();

    String realmGet$kernelVersion();

    String realmGet$manufacturer();

    String realmGet$model();

    String realmGet$osVersion();

    String realmGet$product();

    String realmGet$uuId();

    void realmSet$brand(String str);

    void realmSet$isRoot(int i);

    void realmSet$kernelVersion(String str);

    void realmSet$manufacturer(String str);

    void realmSet$model(String str);

    void realmSet$osVersion(String str);

    void realmSet$product(String str);

    void realmSet$uuId(String str);
}
