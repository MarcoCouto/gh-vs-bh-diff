package io.realm.log;

import android.util.Log;
import java.util.Locale;

public final class RealmLog {

    /* renamed from: a reason: collision with root package name */
    private static String f4711a = "REALM_JAVA";

    public static int a() {
        return nativeGetLogLevel();
    }

    private static void a(int i, Throwable th, String str, Object... objArr) {
        if (i >= a()) {
            StringBuilder sb = new StringBuilder();
            if (!(str == null || objArr == null || objArr.length <= 0)) {
                str = String.format(Locale.US, str, objArr);
            }
            if (th != null) {
                sb.append(Log.getStackTraceString(th));
            }
            if (str != null) {
                if (th != null) {
                    sb.append("\n");
                }
                sb.append(str);
            }
            nativeLog(i, f4711a, th, sb.toString());
        }
    }

    public static void a(String str, Object... objArr) {
        a(null, str, objArr);
    }

    public static void a(Throwable th, String str, Object... objArr) {
        a(5, th, str, objArr);
    }

    public static void b(String str, Object... objArr) {
        c(null, str, objArr);
    }

    public static void b(Throwable th, String str, Object... objArr) {
        a(6, th, str, objArr);
    }

    public static void c(Throwable th, String str, Object... objArr) {
        a(7, th, str, objArr);
    }

    private static native int nativeGetLogLevel();

    private static native void nativeLog(int i, String str, Throwable th, String str2);
}
