package io.realm;

import android.content.Context;
import android.os.SystemClock;
import io.realm.exceptions.RealmException;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectStore;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.SharedRealm;
import io.realm.internal.Table;
import io.realm.internal.b;
import io.realm.internal.i;
import io.realm.internal.l;
import io.realm.internal.m;
import io.realm.internal.n;
import io.realm.log.RealmLog;
import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class bb extends e {
    private static final Object g = new Object();
    private static be h;
    private final bo i;

    public interface a {
        void a(bb bbVar);
    }

    private bb(bc bcVar) {
        super(bcVar, a(bcVar.a().h()));
        this.i = new ah(this, new b(this.d.h(), this.e.getSchemaInfo()));
        if (this.d.p()) {
            n h2 = this.d.h();
            for (Class a2 : h2.b()) {
                String a3 = h2.a(a2);
                if (!this.e.hasTable(a3)) {
                    this.e.close();
                    throw new RealmMigrationNeededException(this.d.m(), String.format(Locale.US, "Cannot open the read only Realm. '%s' is missing.", new Object[]{Table.b(a3)}));
                }
            }
        }
    }

    private bb(SharedRealm sharedRealm) {
        super(sharedRealm);
        this.i = new ah(this, new b(this.d.h(), sharedRealm.getSchemaInfo()));
    }

    static bb a(bc bcVar) {
        return new bb(bcVar);
    }

    static bb a(SharedRealm sharedRealm) {
        return new bb(sharedRealm);
    }

    private <E extends bh> E a(E e, boolean z, Map<bh, m> map) {
        e();
        return this.d.h().a(this, e, z, map);
    }

    private static OsSchemaInfo a(n nVar) {
        return new OsSchemaInfo(nVar.a().values());
    }

    public static synchronized void a(Context context) {
        synchronized (bb.class) {
            if (e.f4615a == null) {
                if (context == null) {
                    throw new IllegalArgumentException("Non-null context required.");
                }
                b(context);
                l.a(context);
                b(new io.realm.be.a(context).a());
                i.a().a(context);
                if (context.getApplicationContext() != null) {
                    e.f4615a = context.getApplicationContext();
                } else {
                    e.f4615a = context;
                }
                SharedRealm.initialize(new File(context.getFilesDir(), ".realm.temp"));
            }
        }
    }

    private static void b(Context context) {
        File filesDir = context.getFilesDir();
        if (filesDir != null) {
            if (!filesDir.exists()) {
                try {
                    filesDir.mkdirs();
                } catch (SecurityException e) {
                }
            } else {
                return;
            }
        }
        if (filesDir == null || !filesDir.exists()) {
            long[] jArr = {1, 2, 5, 10, 16};
            long j = 0;
            int i2 = -1;
            do {
                if (context.getFilesDir() != null && context.getFilesDir().exists()) {
                    break;
                }
                i2++;
                long j2 = jArr[Math.min(i2, jArr.length - 1)];
                SystemClock.sleep(j2);
                j += j2;
            } while (j <= 200);
        }
        if (context.getFilesDir() == null || !context.getFilesDir().exists()) {
            throw new IllegalStateException("Context.getFilesDir() returns " + context.getFilesDir() + " which is not an existing directory. See https://issuetracker.google.com/issues/36918154");
        }
    }

    public static void b(be beVar) {
        if (beVar == null) {
            throw new IllegalArgumentException("A non-null RealmConfiguration must be provided");
        }
        synchronized (g) {
            h = beVar;
        }
    }

    private <E extends bh> void c(E e) {
        if (e == null) {
            throw new IllegalArgumentException("Null objects cannot be copied into Realm.");
        }
    }

    private void c(Class<? extends bh> cls) {
        if (this.e.getSchemaInfo().a(this.d.h().b(cls)).a() == null) {
            throw new IllegalArgumentException("A RealmObject with no @PrimaryKey cannot be updated: " + cls.toString());
        }
    }

    public static bb m() {
        be n = n();
        if (n != null) {
            return (bb) bc.a(n, bb.class);
        }
        if (e.f4615a == null) {
            throw new IllegalStateException("Call `Realm.init(Context)` before calling this method.");
        }
        throw new IllegalStateException("Set default configuration by using `Realm.setDefaultConfiguration(RealmConfiguration)`.");
    }

    public static be n() {
        be beVar;
        synchronized (g) {
            beVar = h;
        }
        return beVar;
    }

    public static Object o() {
        String str = "io.realm.DefaultRealmModule";
        try {
            Constructor constructor = Class.forName(str).getDeclaredConstructors()[0];
            constructor.setAccessible(true);
            return constructor.newInstance(new Object[0]);
        } catch (ClassNotFoundException e) {
            return null;
        } catch (InvocationTargetException e2) {
            throw new RealmException("Could not create an instance of " + str, e2);
        } catch (InstantiationException e3) {
            throw new RealmException("Could not create an instance of " + str, e3);
        } catch (IllegalAccessException e4) {
            throw new RealmException("Could not create an instance of " + str, e4);
        }
    }

    public <E extends bh> E a(E e) {
        c(e);
        return a(e, false, (Map<bh, m>) new HashMap<bh,m>());
    }

    /* access modifiers changed from: 0000 */
    public <E extends bh> E a(Class<E> cls, Object obj, boolean z, List<String> list) {
        Table a2 = this.i.a(cls);
        return this.d.h().a(cls, this, OsObject.createWithPrimaryKey(a2, obj), this.i.c(cls), z, list);
    }

    /* access modifiers changed from: 0000 */
    public <E extends bh> E a(Class<E> cls, boolean z, List<String> list) {
        Table a2 = this.i.a(cls);
        if (OsObjectStore.a(this.e, this.d.h().b(cls)) != null) {
            throw new RealmException(String.format(Locale.US, "'%s' has a primary key, use 'createObject(Class<E>, Object)' instead.", new Object[]{a2.h()}));
        }
        return this.d.h().a(cls, this, OsObject.create(a2), this.i.c(cls), z, list);
    }

    public <E extends bh> bm<E> a(Class<E> cls) {
        e();
        return bm.a(this, cls);
    }

    public void a(a aVar) {
        if (aVar == null) {
            throw new IllegalArgumentException("Transaction should not be null");
        }
        b();
        try {
            aVar.a(this);
            c();
        } catch (Throwable th) {
            if (a()) {
                d();
            } else {
                RealmLog.a("Could not cancel transaction, not currently in a transaction.", new Object[0]);
            }
            throw th;
        }
    }

    public /* bridge */ /* synthetic */ boolean a() {
        return super.a();
    }

    public <E extends bh> E b(E e) {
        c(e);
        c(e.getClass());
        return a(e, true, (Map<bh, m>) new HashMap<bh,m>());
    }

    /* access modifiers changed from: 0000 */
    public Table b(Class<? extends bh> cls) {
        return this.i.a(cls);
    }

    public /* bridge */ /* synthetic */ void b() {
        super.b();
    }

    public /* bridge */ /* synthetic */ void c() {
        super.c();
    }

    public /* bridge */ /* synthetic */ void close() {
        super.close();
    }

    public /* bridge */ /* synthetic */ void d() {
        super.d();
    }

    public /* bridge */ /* synthetic */ String g() {
        return super.g();
    }

    public /* bridge */ /* synthetic */ be h() {
        return super.h();
    }

    public /* bridge */ /* synthetic */ boolean j() {
        return super.j();
    }

    public bo k() {
        return this.i;
    }
}
