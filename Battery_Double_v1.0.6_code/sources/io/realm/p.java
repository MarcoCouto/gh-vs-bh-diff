package io.realm;

import com.hmatalonga.greenhub.models.data.CallMonth;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.c;
import io.realm.internal.m;
import io.realm.internal.o;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class p extends CallMonth implements m, q {

    /* renamed from: a reason: collision with root package name */
    private static final OsObjectSchemaInfo f4716a = e();

    /* renamed from: b reason: collision with root package name */
    private static final List<String> f4717b;
    private a c;
    private ba<CallMonth> d;

    static final class a extends c {

        /* renamed from: a reason: collision with root package name */
        long f4718a;

        /* renamed from: b reason: collision with root package name */
        long f4719b;
        long c;
        long d;
        long e;

        a(OsSchemaInfo osSchemaInfo) {
            super(5);
            OsObjectSchemaInfo a2 = osSchemaInfo.a("CallMonth");
            this.f4718a = a("totalCallInNum", a2);
            this.f4719b = a("totalCallOutNum", a2);
            this.c = a("totalMissedCallNum", a2);
            this.d = a("totalCallInDur", a2);
            this.e = a("totalCallOutDur", a2);
        }

        /* access modifiers changed from: protected */
        public final void a(c cVar, c cVar2) {
            a aVar = (a) cVar;
            a aVar2 = (a) cVar2;
            aVar2.f4718a = aVar.f4718a;
            aVar2.f4719b = aVar.f4719b;
            aVar2.c = aVar.c;
            aVar2.d = aVar.d;
            aVar2.e = aVar.e;
        }
    }

    static {
        ArrayList arrayList = new ArrayList(5);
        arrayList.add("totalCallInNum");
        arrayList.add("totalCallOutNum");
        arrayList.add("totalMissedCallNum");
        arrayList.add("totalCallInDur");
        arrayList.add("totalCallOutDur");
        f4717b = Collections.unmodifiableList(arrayList);
    }

    p() {
        this.d.g();
    }

    public static CallMonth a(bb bbVar, CallMonth callMonth, boolean z, Map<bh, m> map) {
        if ((callMonth instanceof m) && ((m) callMonth).d().a() != null) {
            e a2 = ((m) callMonth).d().a();
            if (a2.c != bbVar.c) {
                throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
            } else if (a2.g().equals(bbVar.g())) {
                return callMonth;
            }
        }
        io.realm.e.a aVar = (io.realm.e.a) e.f.get();
        m mVar = (m) map.get(callMonth);
        return mVar != null ? (CallMonth) mVar : b(bbVar, callMonth, z, map);
    }

    public static a a(OsSchemaInfo osSchemaInfo) {
        return new a(osSchemaInfo);
    }

    public static CallMonth b(bb bbVar, CallMonth callMonth, boolean z, Map<bh, m> map) {
        m mVar = (m) map.get(callMonth);
        if (mVar != null) {
            return (CallMonth) mVar;
        }
        CallMonth callMonth2 = (CallMonth) bbVar.a(CallMonth.class, false, Collections.emptyList());
        map.put(callMonth, (m) callMonth2);
        q qVar = callMonth;
        q qVar2 = callMonth2;
        qVar2.realmSet$totalCallInNum(qVar.realmGet$totalCallInNum());
        qVar2.realmSet$totalCallOutNum(qVar.realmGet$totalCallOutNum());
        qVar2.realmSet$totalMissedCallNum(qVar.realmGet$totalMissedCallNum());
        qVar2.realmSet$totalCallInDur(qVar.realmGet$totalCallInDur());
        qVar2.realmSet$totalCallOutDur(qVar.realmGet$totalCallOutDur());
        return callMonth2;
    }

    public static OsObjectSchemaInfo b() {
        return f4716a;
    }

    public static String c() {
        return "class_CallMonth";
    }

    private static OsObjectSchemaInfo e() {
        io.realm.internal.OsObjectSchemaInfo.a aVar = new io.realm.internal.OsObjectSchemaInfo.a("CallMonth", 5, 0);
        aVar.a("totalCallInNum", RealmFieldType.INTEGER, false, false, true);
        aVar.a("totalCallOutNum", RealmFieldType.INTEGER, false, false, true);
        aVar.a("totalMissedCallNum", RealmFieldType.INTEGER, false, false, true);
        aVar.a("totalCallInDur", RealmFieldType.INTEGER, false, false, true);
        aVar.a("totalCallOutDur", RealmFieldType.INTEGER, false, false, true);
        return aVar.a();
    }

    public void a() {
        if (this.d == null) {
            io.realm.e.a aVar = (io.realm.e.a) e.f.get();
            this.c = (a) aVar.c();
            this.d = new ba<>(this);
            this.d.a(aVar.a());
            this.d.a(aVar.b());
            this.d.a(aVar.d());
            this.d.a(aVar.e());
        }
    }

    public ba<?> d() {
        return this.d;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        p pVar = (p) obj;
        String g = this.d.a().g();
        String g2 = pVar.d.a().g();
        if (g == null ? g2 != null : !g.equals(g2)) {
            return false;
        }
        String g3 = this.d.b().b().g();
        String g4 = pVar.d.b().b().g();
        if (g3 == null ? g4 != null : !g3.equals(g4)) {
            return false;
        }
        return this.d.b().c() == pVar.d.b().c();
    }

    public int hashCode() {
        int i = 0;
        String g = this.d.a().g();
        String g2 = this.d.b().b().g();
        long c2 = this.d.b().c();
        int hashCode = ((g != null ? g.hashCode() : 0) + 527) * 31;
        if (g2 != null) {
            i = g2.hashCode();
        }
        return ((i + hashCode) * 31) + ((int) ((c2 >>> 32) ^ c2));
    }

    public long realmGet$totalCallInDur() {
        this.d.a().e();
        return this.d.b().g(this.c.d);
    }

    public int realmGet$totalCallInNum() {
        this.d.a().e();
        return (int) this.d.b().g(this.c.f4718a);
    }

    public long realmGet$totalCallOutDur() {
        this.d.a().e();
        return this.d.b().g(this.c.e);
    }

    public int realmGet$totalCallOutNum() {
        this.d.a().e();
        return (int) this.d.b().g(this.c.f4719b);
    }

    public int realmGet$totalMissedCallNum() {
        this.d.a().e();
        return (int) this.d.b().g(this.c.c);
    }

    public void realmSet$totalCallInDur(long j) {
        if (!this.d.f()) {
            this.d.a().e();
            this.d.b().a(this.c.d, j);
        } else if (this.d.c()) {
            o b2 = this.d.b();
            b2.b().a(this.c.d, b2.c(), j, true);
        }
    }

    public void realmSet$totalCallInNum(int i) {
        if (!this.d.f()) {
            this.d.a().e();
            this.d.b().a(this.c.f4718a, (long) i);
        } else if (this.d.c()) {
            o b2 = this.d.b();
            b2.b().a(this.c.f4718a, b2.c(), (long) i, true);
        }
    }

    public void realmSet$totalCallOutDur(long j) {
        if (!this.d.f()) {
            this.d.a().e();
            this.d.b().a(this.c.e, j);
        } else if (this.d.c()) {
            o b2 = this.d.b();
            b2.b().a(this.c.e, b2.c(), j, true);
        }
    }

    public void realmSet$totalCallOutNum(int i) {
        if (!this.d.f()) {
            this.d.a().e();
            this.d.b().a(this.c.f4719b, (long) i);
        } else if (this.d.c()) {
            o b2 = this.d.b();
            b2.b().a(this.c.f4719b, b2.c(), (long) i, true);
        }
    }

    public void realmSet$totalMissedCallNum(int i) {
        if (!this.d.f()) {
            this.d.a().e();
            this.d.b().a(this.c.c, (long) i);
        } else if (this.d.c()) {
            o b2 = this.d.b();
            b2.b().a(this.c.c, b2.c(), (long) i, true);
        }
    }

    public String toString() {
        if (!bj.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder sb = new StringBuilder("CallMonth = proxy[");
        sb.append("{totalCallInNum:");
        sb.append(realmGet$totalCallInNum());
        sb.append("}");
        sb.append(",");
        sb.append("{totalCallOutNum:");
        sb.append(realmGet$totalCallOutNum());
        sb.append("}");
        sb.append(",");
        sb.append("{totalMissedCallNum:");
        sb.append(realmGet$totalMissedCallNum());
        sb.append("}");
        sb.append(",");
        sb.append("{totalCallInDur:");
        sb.append(realmGet$totalCallInDur());
        sb.append("}");
        sb.append(",");
        sb.append("{totalCallOutDur:");
        sb.append(realmGet$totalCallOutDur());
        sb.append("}");
        sb.append("]");
        return sb.toString();
    }
}
