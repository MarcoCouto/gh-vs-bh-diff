package io.realm;

import io.realm.internal.OsList;
import io.realm.internal.f;
import io.realm.internal.m;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.ConcurrentModificationException;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;

public class bf<E> extends AbstractList<E> implements OrderedRealmCollection<E> {

    /* renamed from: a reason: collision with root package name */
    protected Class<E> f4584a;

    /* renamed from: b reason: collision with root package name */
    protected String f4585b;
    final al<E> c;
    protected final e d;
    private List<E> e;

    private class a implements Iterator<E> {

        /* renamed from: a reason: collision with root package name */
        int f4586a;

        /* renamed from: b reason: collision with root package name */
        int f4587b;
        int c;

        private a() {
            this.f4586a = 0;
            this.f4587b = -1;
            this.c = bf.this.modCount;
        }

        /* access modifiers changed from: 0000 */
        public final void a() {
            if (bf.this.modCount != this.c) {
                throw new ConcurrentModificationException();
            }
        }

        public boolean hasNext() {
            bf.this.d();
            a();
            return this.f4586a != bf.this.size();
        }

        public E next() {
            bf.this.d();
            a();
            int i = this.f4586a;
            try {
                E e = bf.this.get(i);
                this.f4587b = i;
                this.f4586a = i + 1;
                return e;
            } catch (IndexOutOfBoundsException e2) {
                a();
                throw new NoSuchElementException("Cannot access index " + i + " when size is " + bf.this.size() + ". Remember to check hasNext() before using next().");
            }
        }

        public void remove() {
            bf.this.d();
            if (this.f4587b < 0) {
                throw new IllegalStateException("Cannot call remove() twice. Must call next() in between.");
            }
            a();
            try {
                bf.this.remove(this.f4587b);
                if (this.f4587b < this.f4586a) {
                    this.f4586a--;
                }
                this.f4587b = -1;
                this.c = bf.this.modCount;
            } catch (IndexOutOfBoundsException e) {
                throw new ConcurrentModificationException();
            }
        }
    }

    private class b extends a implements ListIterator<E> {
        b(int i) {
            super();
            if (i < 0 || i > bf.this.size()) {
                throw new IndexOutOfBoundsException("Starting location must be a valid index: [0, " + (bf.this.size() - 1) + "]. Index was " + i);
            }
            this.f4586a = i;
        }

        public void add(E e2) {
            bf.this.d.e();
            a();
            try {
                int i = this.f4586a;
                bf.this.add(i, e2);
                this.f4587b = -1;
                this.f4586a = i + 1;
                this.c = bf.this.modCount;
            } catch (IndexOutOfBoundsException e3) {
                throw new ConcurrentModificationException();
            }
        }

        public boolean hasPrevious() {
            return this.f4586a != 0;
        }

        public int nextIndex() {
            return this.f4586a;
        }

        public E previous() {
            a();
            int i = this.f4586a - 1;
            try {
                E e2 = bf.this.get(i);
                this.f4586a = i;
                this.f4587b = i;
                return e2;
            } catch (IndexOutOfBoundsException e3) {
                a();
                throw new NoSuchElementException("Cannot access index less than zero. This was " + i + ". Remember to check hasPrevious() before using previous().");
            }
        }

        public int previousIndex() {
            return this.f4586a - 1;
        }

        public void set(E e2) {
            bf.this.d.e();
            if (this.f4587b < 0) {
                throw new IllegalStateException();
            }
            a();
            try {
                bf.this.set(this.f4587b, e2);
                this.c = bf.this.modCount;
            } catch (IndexOutOfBoundsException e3) {
                throw new ConcurrentModificationException();
            }
        }
    }

    public bf() {
        this.d = null;
        this.c = null;
        this.e = new ArrayList();
    }

    bf(Class<E> cls, OsList osList, e eVar) {
        this.f4584a = cls;
        this.c = a(eVar, osList, cls, null);
        this.d = eVar;
    }

    private al<E> a(e eVar, OsList osList, Class<E> cls, String str) {
        if (cls == null || a(cls)) {
            return new bi(eVar, osList, cls, str);
        }
        if (cls == String.class) {
            return new bx(eVar, osList, cls);
        }
        if (cls == Long.class || cls == Integer.class || cls == Short.class || cls == Byte.class) {
            return new ak(eVar, osList, cls);
        }
        if (cls == Boolean.class) {
            return new m(eVar, osList, cls);
        }
        if (cls == byte[].class) {
            return new l(eVar, osList, cls);
        }
        if (cls == Double.class) {
            return new z(eVar, osList, cls);
        }
        if (cls == Float.class) {
            return new af(eVar, osList, cls);
        }
        if (cls == Date.class) {
            return new w(eVar, osList, cls);
        }
        throw new IllegalArgumentException("Unexpected value class: " + cls.getName());
    }

    private static boolean a(Class<?> cls) {
        return bh.class.isAssignableFrom(cls);
    }

    private boolean b() {
        return this.c != null && this.c.a();
    }

    /* access modifiers changed from: private */
    public void d() {
        this.d.e();
    }

    public boolean a() {
        return this.d != null;
    }

    public void add(int i, E e2) {
        if (a()) {
            d();
            this.c.c(i, e2);
        } else {
            this.e.add(i, e2);
        }
        this.modCount++;
    }

    public boolean add(E e2) {
        if (a()) {
            d();
            this.c.c((Object) e2);
        } else {
            this.e.add(e2);
        }
        this.modCount++;
        return true;
    }

    public boolean c() {
        return true;
    }

    public void clear() {
        if (a()) {
            d();
            this.c.c();
        } else {
            this.e.clear();
        }
        this.modCount++;
    }

    public boolean contains(Object obj) {
        if (!a()) {
            return this.e.contains(obj);
        }
        this.d.e();
        if (!(obj instanceof m) || ((m) obj).d().b() != f.INSTANCE) {
            return super.contains(obj);
        }
        return false;
    }

    public E get(int i) {
        if (!a()) {
            return this.e.get(i);
        }
        d();
        return this.c.b(i);
    }

    public Iterator<E> iterator() {
        return a() ? new a() : super.iterator();
    }

    public ListIterator<E> listIterator() {
        return listIterator(0);
    }

    public ListIterator<E> listIterator(int i) {
        return a() ? new b(i) : super.listIterator(i);
    }

    public E remove(int i) {
        E remove;
        if (a()) {
            d();
            remove = get(i);
            this.c.e(i);
        } else {
            remove = this.e.remove(i);
        }
        this.modCount++;
        return remove;
    }

    public boolean remove(Object obj) {
        if (!a() || this.d.a()) {
            return super.remove(obj);
        }
        throw new IllegalStateException("Objects can only be removed from inside a write transaction.");
    }

    public boolean removeAll(Collection<?> collection) {
        if (!a() || this.d.a()) {
            return super.removeAll(collection);
        }
        throw new IllegalStateException("Objects can only be removed from inside a write transaction.");
    }

    public E set(int i, E e2) {
        if (!a()) {
            return this.e.set(i, e2);
        }
        d();
        return this.c.d(i, e2);
    }

    public int size() {
        if (!a()) {
            return this.e.size();
        }
        d();
        return this.c.b();
    }

    public String toString() {
        int i = 0;
        String str = ",";
        StringBuilder sb = new StringBuilder();
        if (!a()) {
            sb.append("RealmList<?>@[");
            int size = size();
            for (int i2 = 0; i2 < size; i2++) {
                Object obj = get(i2);
                if (obj instanceof bh) {
                    sb.append(System.identityHashCode(obj));
                } else if (obj instanceof byte[]) {
                    sb.append("byte[").append(((byte[]) obj).length).append("]");
                } else {
                    sb.append(obj);
                }
                sb.append(",");
            }
            if (size() > 0) {
                sb.setLength(sb.length() - ",".length());
            }
            sb.append("]");
        } else {
            sb.append("RealmList<");
            if (this.f4585b != null) {
                sb.append(this.f4585b);
            } else if (a(this.f4584a)) {
                sb.append(this.d.k().b(this.f4584a).a());
            } else if (this.f4584a == byte[].class) {
                sb.append(this.f4584a.getSimpleName());
            } else {
                sb.append(this.f4584a.getName());
            }
            sb.append(">@[");
            if (!b()) {
                sb.append("invalid");
            } else if (a(this.f4584a)) {
                while (true) {
                    int i3 = i;
                    if (i3 >= size()) {
                        break;
                    }
                    sb.append(((m) get(i3)).d().b().c());
                    sb.append(",");
                    i = i3 + 1;
                }
                if (size() > 0) {
                    sb.setLength(sb.length() - ",".length());
                }
            } else {
                while (true) {
                    int i4 = i;
                    if (i4 >= size()) {
                        break;
                    }
                    Object obj2 = get(i4);
                    if (obj2 instanceof byte[]) {
                        sb.append("byte[").append(((byte[]) obj2).length).append("]");
                    } else {
                        sb.append(obj2);
                    }
                    sb.append(",");
                    i = i4 + 1;
                }
                if (size() > 0) {
                    sb.setLength(sb.length() - ",".length());
                }
            }
            sb.append("]");
        }
        return sb.toString();
    }
}
