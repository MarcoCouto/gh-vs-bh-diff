package io.realm;

import com.hmatalonga.greenhub.models.data.NetworkDetails;
import com.hmatalonga.greenhub.models.data.NetworkStatistics;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.c;
import io.realm.internal.m;
import io.realm.internal.o;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class aq extends NetworkDetails implements ar, m {

    /* renamed from: a reason: collision with root package name */
    private static final OsObjectSchemaInfo f4553a = e();

    /* renamed from: b reason: collision with root package name */
    private static final List<String> f4554b;
    private a c;
    private ba<NetworkDetails> d;

    static final class a extends c {

        /* renamed from: a reason: collision with root package name */
        long f4555a;

        /* renamed from: b reason: collision with root package name */
        long f4556b;
        long c;
        long d;
        long e;
        long f;
        long g;
        long h;
        long i;
        long j;
        long k;
        long l;
        long m;
        long n;

        a(OsSchemaInfo osSchemaInfo) {
            super(14);
            OsObjectSchemaInfo a2 = osSchemaInfo.a("NetworkDetails");
            this.f4555a = a("networkType", a2);
            this.f4556b = a("mobileNetworkType", a2);
            this.c = a("mobileDataStatus", a2);
            this.d = a("mobileDataActivity", a2);
            this.e = a("roamingEnabled", a2);
            this.f = a("wifiStatus", a2);
            this.g = a("wifiSignalStrength", a2);
            this.h = a("wifiLinkSpeed", a2);
            this.i = a("networkStatistics", a2);
            this.j = a("wifiApStatus", a2);
            this.k = a("networkOperator", a2);
            this.l = a("simOperator", a2);
            this.m = a("mcc", a2);
            this.n = a("mnc", a2);
        }

        /* access modifiers changed from: protected */
        public final void a(c cVar, c cVar2) {
            a aVar = (a) cVar;
            a aVar2 = (a) cVar2;
            aVar2.f4555a = aVar.f4555a;
            aVar2.f4556b = aVar.f4556b;
            aVar2.c = aVar.c;
            aVar2.d = aVar.d;
            aVar2.e = aVar.e;
            aVar2.f = aVar.f;
            aVar2.g = aVar.g;
            aVar2.h = aVar.h;
            aVar2.i = aVar.i;
            aVar2.j = aVar.j;
            aVar2.k = aVar.k;
            aVar2.l = aVar.l;
            aVar2.m = aVar.m;
            aVar2.n = aVar.n;
        }
    }

    static {
        ArrayList arrayList = new ArrayList(14);
        arrayList.add("networkType");
        arrayList.add("mobileNetworkType");
        arrayList.add("mobileDataStatus");
        arrayList.add("mobileDataActivity");
        arrayList.add("roamingEnabled");
        arrayList.add("wifiStatus");
        arrayList.add("wifiSignalStrength");
        arrayList.add("wifiLinkSpeed");
        arrayList.add("networkStatistics");
        arrayList.add("wifiApStatus");
        arrayList.add("networkOperator");
        arrayList.add("simOperator");
        arrayList.add("mcc");
        arrayList.add("mnc");
        f4554b = Collections.unmodifiableList(arrayList);
    }

    aq() {
        this.d.g();
    }

    public static NetworkDetails a(bb bbVar, NetworkDetails networkDetails, boolean z, Map<bh, m> map) {
        if ((networkDetails instanceof m) && ((m) networkDetails).d().a() != null) {
            e a2 = ((m) networkDetails).d().a();
            if (a2.c != bbVar.c) {
                throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
            } else if (a2.g().equals(bbVar.g())) {
                return networkDetails;
            }
        }
        io.realm.e.a aVar = (io.realm.e.a) e.f.get();
        m mVar = (m) map.get(networkDetails);
        return mVar != null ? (NetworkDetails) mVar : b(bbVar, networkDetails, z, map);
    }

    public static a a(OsSchemaInfo osSchemaInfo) {
        return new a(osSchemaInfo);
    }

    public static NetworkDetails b(bb bbVar, NetworkDetails networkDetails, boolean z, Map<bh, m> map) {
        m mVar = (m) map.get(networkDetails);
        if (mVar != null) {
            return (NetworkDetails) mVar;
        }
        NetworkDetails networkDetails2 = (NetworkDetails) bbVar.a(NetworkDetails.class, false, Collections.emptyList());
        map.put(networkDetails, (m) networkDetails2);
        ar arVar = networkDetails;
        ar arVar2 = networkDetails2;
        arVar2.realmSet$networkType(arVar.realmGet$networkType());
        arVar2.realmSet$mobileNetworkType(arVar.realmGet$mobileNetworkType());
        arVar2.realmSet$mobileDataStatus(arVar.realmGet$mobileDataStatus());
        arVar2.realmSet$mobileDataActivity(arVar.realmGet$mobileDataActivity());
        arVar2.realmSet$roamingEnabled(arVar.realmGet$roamingEnabled());
        arVar2.realmSet$wifiStatus(arVar.realmGet$wifiStatus());
        arVar2.realmSet$wifiSignalStrength(arVar.realmGet$wifiSignalStrength());
        arVar2.realmSet$wifiLinkSpeed(arVar.realmGet$wifiLinkSpeed());
        NetworkStatistics realmGet$networkStatistics = arVar.realmGet$networkStatistics();
        if (realmGet$networkStatistics == null) {
            arVar2.realmSet$networkStatistics(null);
        } else {
            NetworkStatistics networkStatistics = (NetworkStatistics) map.get(realmGet$networkStatistics);
            if (networkStatistics != null) {
                arVar2.realmSet$networkStatistics(networkStatistics);
            } else {
                arVar2.realmSet$networkStatistics(as.a(bbVar, realmGet$networkStatistics, z, map));
            }
        }
        arVar2.realmSet$wifiApStatus(arVar.realmGet$wifiApStatus());
        arVar2.realmSet$networkOperator(arVar.realmGet$networkOperator());
        arVar2.realmSet$simOperator(arVar.realmGet$simOperator());
        arVar2.realmSet$mcc(arVar.realmGet$mcc());
        arVar2.realmSet$mnc(arVar.realmGet$mnc());
        return networkDetails2;
    }

    public static OsObjectSchemaInfo b() {
        return f4553a;
    }

    public static String c() {
        return "class_NetworkDetails";
    }

    private static OsObjectSchemaInfo e() {
        io.realm.internal.OsObjectSchemaInfo.a aVar = new io.realm.internal.OsObjectSchemaInfo.a("NetworkDetails", 14, 0);
        aVar.a("networkType", RealmFieldType.STRING, false, false, false);
        aVar.a("mobileNetworkType", RealmFieldType.STRING, false, false, false);
        aVar.a("mobileDataStatus", RealmFieldType.STRING, false, false, false);
        aVar.a("mobileDataActivity", RealmFieldType.STRING, false, false, false);
        aVar.a("roamingEnabled", RealmFieldType.INTEGER, false, false, true);
        aVar.a("wifiStatus", RealmFieldType.STRING, false, false, false);
        aVar.a("wifiSignalStrength", RealmFieldType.INTEGER, false, false, true);
        aVar.a("wifiLinkSpeed", RealmFieldType.INTEGER, false, false, true);
        aVar.a("networkStatistics", RealmFieldType.OBJECT, "NetworkStatistics");
        aVar.a("wifiApStatus", RealmFieldType.STRING, false, false, false);
        aVar.a("networkOperator", RealmFieldType.STRING, false, false, false);
        aVar.a("simOperator", RealmFieldType.STRING, false, false, false);
        aVar.a("mcc", RealmFieldType.STRING, false, false, false);
        aVar.a("mnc", RealmFieldType.STRING, false, false, false);
        return aVar.a();
    }

    public void a() {
        if (this.d == null) {
            io.realm.e.a aVar = (io.realm.e.a) e.f.get();
            this.c = (a) aVar.c();
            this.d = new ba<>(this);
            this.d.a(aVar.a());
            this.d.a(aVar.b());
            this.d.a(aVar.d());
            this.d.a(aVar.e());
        }
    }

    public ba<?> d() {
        return this.d;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        aq aqVar = (aq) obj;
        String g = this.d.a().g();
        String g2 = aqVar.d.a().g();
        if (g == null ? g2 != null : !g.equals(g2)) {
            return false;
        }
        String g3 = this.d.b().b().g();
        String g4 = aqVar.d.b().b().g();
        if (g3 == null ? g4 != null : !g3.equals(g4)) {
            return false;
        }
        return this.d.b().c() == aqVar.d.b().c();
    }

    public int hashCode() {
        int i = 0;
        String g = this.d.a().g();
        String g2 = this.d.b().b().g();
        long c2 = this.d.b().c();
        int hashCode = ((g != null ? g.hashCode() : 0) + 527) * 31;
        if (g2 != null) {
            i = g2.hashCode();
        }
        return ((i + hashCode) * 31) + ((int) ((c2 >>> 32) ^ c2));
    }

    public String realmGet$mcc() {
        this.d.a().e();
        return this.d.b().l(this.c.m);
    }

    public String realmGet$mnc() {
        this.d.a().e();
        return this.d.b().l(this.c.n);
    }

    public String realmGet$mobileDataActivity() {
        this.d.a().e();
        return this.d.b().l(this.c.d);
    }

    public String realmGet$mobileDataStatus() {
        this.d.a().e();
        return this.d.b().l(this.c.c);
    }

    public String realmGet$mobileNetworkType() {
        this.d.a().e();
        return this.d.b().l(this.c.f4556b);
    }

    public String realmGet$networkOperator() {
        this.d.a().e();
        return this.d.b().l(this.c.k);
    }

    public NetworkStatistics realmGet$networkStatistics() {
        this.d.a().e();
        if (this.d.b().a(this.c.i)) {
            return null;
        }
        return (NetworkStatistics) this.d.a().a(NetworkStatistics.class, this.d.b().n(this.c.i), false, Collections.emptyList());
    }

    public String realmGet$networkType() {
        this.d.a().e();
        return this.d.b().l(this.c.f4555a);
    }

    public int realmGet$roamingEnabled() {
        this.d.a().e();
        return (int) this.d.b().g(this.c.e);
    }

    public String realmGet$simOperator() {
        this.d.a().e();
        return this.d.b().l(this.c.l);
    }

    public String realmGet$wifiApStatus() {
        this.d.a().e();
        return this.d.b().l(this.c.j);
    }

    public int realmGet$wifiLinkSpeed() {
        this.d.a().e();
        return (int) this.d.b().g(this.c.h);
    }

    public int realmGet$wifiSignalStrength() {
        this.d.a().e();
        return (int) this.d.b().g(this.c.g);
    }

    public String realmGet$wifiStatus() {
        this.d.a().e();
        return this.d.b().l(this.c.f);
    }

    public void realmSet$mcc(String str) {
        if (!this.d.f()) {
            this.d.a().e();
            if (str == null) {
                this.d.b().c(this.c.m);
            } else {
                this.d.b().a(this.c.m, str);
            }
        } else if (this.d.c()) {
            o b2 = this.d.b();
            if (str == null) {
                b2.b().a(this.c.m, b2.c(), true);
            } else {
                b2.b().a(this.c.m, b2.c(), str, true);
            }
        }
    }

    public void realmSet$mnc(String str) {
        if (!this.d.f()) {
            this.d.a().e();
            if (str == null) {
                this.d.b().c(this.c.n);
            } else {
                this.d.b().a(this.c.n, str);
            }
        } else if (this.d.c()) {
            o b2 = this.d.b();
            if (str == null) {
                b2.b().a(this.c.n, b2.c(), true);
            } else {
                b2.b().a(this.c.n, b2.c(), str, true);
            }
        }
    }

    public void realmSet$mobileDataActivity(String str) {
        if (!this.d.f()) {
            this.d.a().e();
            if (str == null) {
                this.d.b().c(this.c.d);
            } else {
                this.d.b().a(this.c.d, str);
            }
        } else if (this.d.c()) {
            o b2 = this.d.b();
            if (str == null) {
                b2.b().a(this.c.d, b2.c(), true);
            } else {
                b2.b().a(this.c.d, b2.c(), str, true);
            }
        }
    }

    public void realmSet$mobileDataStatus(String str) {
        if (!this.d.f()) {
            this.d.a().e();
            if (str == null) {
                this.d.b().c(this.c.c);
            } else {
                this.d.b().a(this.c.c, str);
            }
        } else if (this.d.c()) {
            o b2 = this.d.b();
            if (str == null) {
                b2.b().a(this.c.c, b2.c(), true);
            } else {
                b2.b().a(this.c.c, b2.c(), str, true);
            }
        }
    }

    public void realmSet$mobileNetworkType(String str) {
        if (!this.d.f()) {
            this.d.a().e();
            if (str == null) {
                this.d.b().c(this.c.f4556b);
            } else {
                this.d.b().a(this.c.f4556b, str);
            }
        } else if (this.d.c()) {
            o b2 = this.d.b();
            if (str == null) {
                b2.b().a(this.c.f4556b, b2.c(), true);
            } else {
                b2.b().a(this.c.f4556b, b2.c(), str, true);
            }
        }
    }

    public void realmSet$networkOperator(String str) {
        if (!this.d.f()) {
            this.d.a().e();
            if (str == null) {
                this.d.b().c(this.c.k);
            } else {
                this.d.b().a(this.c.k, str);
            }
        } else if (this.d.c()) {
            o b2 = this.d.b();
            if (str == null) {
                b2.b().a(this.c.k, b2.c(), true);
            } else {
                b2.b().a(this.c.k, b2.c(), str, true);
            }
        }
    }

    public void realmSet$networkStatistics(NetworkStatistics networkStatistics) {
        if (!this.d.f()) {
            this.d.a().e();
            if (networkStatistics == null) {
                this.d.b().o(this.c.i);
                return;
            }
            this.d.a((bh) networkStatistics);
            this.d.b().b(this.c.i, ((m) networkStatistics).d().b().c());
        } else if (this.d.c() && !this.d.d().contains("networkStatistics")) {
            NetworkStatistics networkStatistics2 = (networkStatistics == null || bj.isManaged(networkStatistics)) ? networkStatistics : (NetworkStatistics) ((bb) this.d.a()).a(networkStatistics);
            o b2 = this.d.b();
            if (networkStatistics2 == null) {
                b2.o(this.c.i);
                return;
            }
            this.d.a((bh) networkStatistics2);
            b2.b().b(this.c.i, b2.c(), ((m) networkStatistics2).d().b().c(), true);
        }
    }

    public void realmSet$networkType(String str) {
        if (!this.d.f()) {
            this.d.a().e();
            if (str == null) {
                this.d.b().c(this.c.f4555a);
            } else {
                this.d.b().a(this.c.f4555a, str);
            }
        } else if (this.d.c()) {
            o b2 = this.d.b();
            if (str == null) {
                b2.b().a(this.c.f4555a, b2.c(), true);
            } else {
                b2.b().a(this.c.f4555a, b2.c(), str, true);
            }
        }
    }

    public void realmSet$roamingEnabled(int i) {
        if (!this.d.f()) {
            this.d.a().e();
            this.d.b().a(this.c.e, (long) i);
        } else if (this.d.c()) {
            o b2 = this.d.b();
            b2.b().a(this.c.e, b2.c(), (long) i, true);
        }
    }

    public void realmSet$simOperator(String str) {
        if (!this.d.f()) {
            this.d.a().e();
            if (str == null) {
                this.d.b().c(this.c.l);
            } else {
                this.d.b().a(this.c.l, str);
            }
        } else if (this.d.c()) {
            o b2 = this.d.b();
            if (str == null) {
                b2.b().a(this.c.l, b2.c(), true);
            } else {
                b2.b().a(this.c.l, b2.c(), str, true);
            }
        }
    }

    public void realmSet$wifiApStatus(String str) {
        if (!this.d.f()) {
            this.d.a().e();
            if (str == null) {
                this.d.b().c(this.c.j);
            } else {
                this.d.b().a(this.c.j, str);
            }
        } else if (this.d.c()) {
            o b2 = this.d.b();
            if (str == null) {
                b2.b().a(this.c.j, b2.c(), true);
            } else {
                b2.b().a(this.c.j, b2.c(), str, true);
            }
        }
    }

    public void realmSet$wifiLinkSpeed(int i) {
        if (!this.d.f()) {
            this.d.a().e();
            this.d.b().a(this.c.h, (long) i);
        } else if (this.d.c()) {
            o b2 = this.d.b();
            b2.b().a(this.c.h, b2.c(), (long) i, true);
        }
    }

    public void realmSet$wifiSignalStrength(int i) {
        if (!this.d.f()) {
            this.d.a().e();
            this.d.b().a(this.c.g, (long) i);
        } else if (this.d.c()) {
            o b2 = this.d.b();
            b2.b().a(this.c.g, b2.c(), (long) i, true);
        }
    }

    public void realmSet$wifiStatus(String str) {
        if (!this.d.f()) {
            this.d.a().e();
            if (str == null) {
                this.d.b().c(this.c.f);
            } else {
                this.d.b().a(this.c.f, str);
            }
        } else if (this.d.c()) {
            o b2 = this.d.b();
            if (str == null) {
                b2.b().a(this.c.f, b2.c(), true);
            } else {
                b2.b().a(this.c.f, b2.c(), str, true);
            }
        }
    }

    public String toString() {
        if (!bj.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder sb = new StringBuilder("NetworkDetails = proxy[");
        sb.append("{networkType:");
        sb.append(realmGet$networkType() != null ? realmGet$networkType() : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{mobileNetworkType:");
        sb.append(realmGet$mobileNetworkType() != null ? realmGet$mobileNetworkType() : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{mobileDataStatus:");
        sb.append(realmGet$mobileDataStatus() != null ? realmGet$mobileDataStatus() : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{mobileDataActivity:");
        sb.append(realmGet$mobileDataActivity() != null ? realmGet$mobileDataActivity() : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{roamingEnabled:");
        sb.append(realmGet$roamingEnabled());
        sb.append("}");
        sb.append(",");
        sb.append("{wifiStatus:");
        sb.append(realmGet$wifiStatus() != null ? realmGet$wifiStatus() : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{wifiSignalStrength:");
        sb.append(realmGet$wifiSignalStrength());
        sb.append("}");
        sb.append(",");
        sb.append("{wifiLinkSpeed:");
        sb.append(realmGet$wifiLinkSpeed());
        sb.append("}");
        sb.append(",");
        sb.append("{networkStatistics:");
        sb.append(realmGet$networkStatistics() != null ? "NetworkStatistics" : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{wifiApStatus:");
        sb.append(realmGet$wifiApStatus() != null ? realmGet$wifiApStatus() : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{networkOperator:");
        sb.append(realmGet$networkOperator() != null ? realmGet$networkOperator() : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{simOperator:");
        sb.append(realmGet$simOperator() != null ? realmGet$simOperator() : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{mcc:");
        sb.append(realmGet$mcc() != null ? realmGet$mcc() : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{mnc:");
        sb.append(realmGet$mnc() != null ? realmGet$mnc() : "null");
        sb.append("}");
        sb.append("]");
        return sb.toString();
    }
}
