package io.realm;

import com.hmatalonga.greenhub.models.data.StorageDetails;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.c;
import io.realm.internal.m;
import io.realm.internal.o;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class bv extends StorageDetails implements bw, m {

    /* renamed from: a reason: collision with root package name */
    private static final OsObjectSchemaInfo f4608a = e();

    /* renamed from: b reason: collision with root package name */
    private static final List<String> f4609b;
    private a c;
    private ba<StorageDetails> d;

    static final class a extends c {

        /* renamed from: a reason: collision with root package name */
        long f4610a;

        /* renamed from: b reason: collision with root package name */
        long f4611b;
        long c;
        long d;
        long e;
        long f;
        long g;
        long h;

        a(OsSchemaInfo osSchemaInfo) {
            super(8);
            OsObjectSchemaInfo a2 = osSchemaInfo.a("StorageDetails");
            this.f4610a = a("free", a2);
            this.f4611b = a("total", a2);
            this.c = a("freeExternal", a2);
            this.d = a("totalExternal", a2);
            this.e = a("freeSystem", a2);
            this.f = a("totalSystem", a2);
            this.g = a("freeSecondary", a2);
            this.h = a("totalSecondary", a2);
        }

        /* access modifiers changed from: protected */
        public final void a(c cVar, c cVar2) {
            a aVar = (a) cVar;
            a aVar2 = (a) cVar2;
            aVar2.f4610a = aVar.f4610a;
            aVar2.f4611b = aVar.f4611b;
            aVar2.c = aVar.c;
            aVar2.d = aVar.d;
            aVar2.e = aVar.e;
            aVar2.f = aVar.f;
            aVar2.g = aVar.g;
            aVar2.h = aVar.h;
        }
    }

    static {
        ArrayList arrayList = new ArrayList(8);
        arrayList.add("free");
        arrayList.add("total");
        arrayList.add("freeExternal");
        arrayList.add("totalExternal");
        arrayList.add("freeSystem");
        arrayList.add("totalSystem");
        arrayList.add("freeSecondary");
        arrayList.add("totalSecondary");
        f4609b = Collections.unmodifiableList(arrayList);
    }

    bv() {
        this.d.g();
    }

    public static StorageDetails a(bb bbVar, StorageDetails storageDetails, boolean z, Map<bh, m> map) {
        if ((storageDetails instanceof m) && ((m) storageDetails).d().a() != null) {
            e a2 = ((m) storageDetails).d().a();
            if (a2.c != bbVar.c) {
                throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
            } else if (a2.g().equals(bbVar.g())) {
                return storageDetails;
            }
        }
        io.realm.e.a aVar = (io.realm.e.a) e.f.get();
        m mVar = (m) map.get(storageDetails);
        return mVar != null ? (StorageDetails) mVar : b(bbVar, storageDetails, z, map);
    }

    public static a a(OsSchemaInfo osSchemaInfo) {
        return new a(osSchemaInfo);
    }

    public static StorageDetails b(bb bbVar, StorageDetails storageDetails, boolean z, Map<bh, m> map) {
        m mVar = (m) map.get(storageDetails);
        if (mVar != null) {
            return (StorageDetails) mVar;
        }
        StorageDetails storageDetails2 = (StorageDetails) bbVar.a(StorageDetails.class, false, Collections.emptyList());
        map.put(storageDetails, (m) storageDetails2);
        bw bwVar = storageDetails;
        bw bwVar2 = storageDetails2;
        bwVar2.realmSet$free(bwVar.realmGet$free());
        bwVar2.realmSet$total(bwVar.realmGet$total());
        bwVar2.realmSet$freeExternal(bwVar.realmGet$freeExternal());
        bwVar2.realmSet$totalExternal(bwVar.realmGet$totalExternal());
        bwVar2.realmSet$freeSystem(bwVar.realmGet$freeSystem());
        bwVar2.realmSet$totalSystem(bwVar.realmGet$totalSystem());
        bwVar2.realmSet$freeSecondary(bwVar.realmGet$freeSecondary());
        bwVar2.realmSet$totalSecondary(bwVar.realmGet$totalSecondary());
        return storageDetails2;
    }

    public static OsObjectSchemaInfo b() {
        return f4608a;
    }

    public static String c() {
        return "class_StorageDetails";
    }

    private static OsObjectSchemaInfo e() {
        io.realm.internal.OsObjectSchemaInfo.a aVar = new io.realm.internal.OsObjectSchemaInfo.a("StorageDetails", 8, 0);
        aVar.a("free", RealmFieldType.INTEGER, false, false, true);
        aVar.a("total", RealmFieldType.INTEGER, false, false, true);
        aVar.a("freeExternal", RealmFieldType.INTEGER, false, false, true);
        aVar.a("totalExternal", RealmFieldType.INTEGER, false, false, true);
        aVar.a("freeSystem", RealmFieldType.INTEGER, false, false, true);
        aVar.a("totalSystem", RealmFieldType.INTEGER, false, false, true);
        aVar.a("freeSecondary", RealmFieldType.INTEGER, false, false, true);
        aVar.a("totalSecondary", RealmFieldType.INTEGER, false, false, true);
        return aVar.a();
    }

    public void a() {
        if (this.d == null) {
            io.realm.e.a aVar = (io.realm.e.a) e.f.get();
            this.c = (a) aVar.c();
            this.d = new ba<>(this);
            this.d.a(aVar.a());
            this.d.a(aVar.b());
            this.d.a(aVar.d());
            this.d.a(aVar.e());
        }
    }

    public ba<?> d() {
        return this.d;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        bv bvVar = (bv) obj;
        String g = this.d.a().g();
        String g2 = bvVar.d.a().g();
        if (g == null ? g2 != null : !g.equals(g2)) {
            return false;
        }
        String g3 = this.d.b().b().g();
        String g4 = bvVar.d.b().b().g();
        if (g3 == null ? g4 != null : !g3.equals(g4)) {
            return false;
        }
        return this.d.b().c() == bvVar.d.b().c();
    }

    public int hashCode() {
        int i = 0;
        String g = this.d.a().g();
        String g2 = this.d.b().b().g();
        long c2 = this.d.b().c();
        int hashCode = ((g != null ? g.hashCode() : 0) + 527) * 31;
        if (g2 != null) {
            i = g2.hashCode();
        }
        return ((i + hashCode) * 31) + ((int) ((c2 >>> 32) ^ c2));
    }

    public int realmGet$free() {
        this.d.a().e();
        return (int) this.d.b().g(this.c.f4610a);
    }

    public int realmGet$freeExternal() {
        this.d.a().e();
        return (int) this.d.b().g(this.c.c);
    }

    public int realmGet$freeSecondary() {
        this.d.a().e();
        return (int) this.d.b().g(this.c.g);
    }

    public int realmGet$freeSystem() {
        this.d.a().e();
        return (int) this.d.b().g(this.c.e);
    }

    public int realmGet$total() {
        this.d.a().e();
        return (int) this.d.b().g(this.c.f4611b);
    }

    public int realmGet$totalExternal() {
        this.d.a().e();
        return (int) this.d.b().g(this.c.d);
    }

    public int realmGet$totalSecondary() {
        this.d.a().e();
        return (int) this.d.b().g(this.c.h);
    }

    public int realmGet$totalSystem() {
        this.d.a().e();
        return (int) this.d.b().g(this.c.f);
    }

    public void realmSet$free(int i) {
        if (!this.d.f()) {
            this.d.a().e();
            this.d.b().a(this.c.f4610a, (long) i);
        } else if (this.d.c()) {
            o b2 = this.d.b();
            b2.b().a(this.c.f4610a, b2.c(), (long) i, true);
        }
    }

    public void realmSet$freeExternal(int i) {
        if (!this.d.f()) {
            this.d.a().e();
            this.d.b().a(this.c.c, (long) i);
        } else if (this.d.c()) {
            o b2 = this.d.b();
            b2.b().a(this.c.c, b2.c(), (long) i, true);
        }
    }

    public void realmSet$freeSecondary(int i) {
        if (!this.d.f()) {
            this.d.a().e();
            this.d.b().a(this.c.g, (long) i);
        } else if (this.d.c()) {
            o b2 = this.d.b();
            b2.b().a(this.c.g, b2.c(), (long) i, true);
        }
    }

    public void realmSet$freeSystem(int i) {
        if (!this.d.f()) {
            this.d.a().e();
            this.d.b().a(this.c.e, (long) i);
        } else if (this.d.c()) {
            o b2 = this.d.b();
            b2.b().a(this.c.e, b2.c(), (long) i, true);
        }
    }

    public void realmSet$total(int i) {
        if (!this.d.f()) {
            this.d.a().e();
            this.d.b().a(this.c.f4611b, (long) i);
        } else if (this.d.c()) {
            o b2 = this.d.b();
            b2.b().a(this.c.f4611b, b2.c(), (long) i, true);
        }
    }

    public void realmSet$totalExternal(int i) {
        if (!this.d.f()) {
            this.d.a().e();
            this.d.b().a(this.c.d, (long) i);
        } else if (this.d.c()) {
            o b2 = this.d.b();
            b2.b().a(this.c.d, b2.c(), (long) i, true);
        }
    }

    public void realmSet$totalSecondary(int i) {
        if (!this.d.f()) {
            this.d.a().e();
            this.d.b().a(this.c.h, (long) i);
        } else if (this.d.c()) {
            o b2 = this.d.b();
            b2.b().a(this.c.h, b2.c(), (long) i, true);
        }
    }

    public void realmSet$totalSystem(int i) {
        if (!this.d.f()) {
            this.d.a().e();
            this.d.b().a(this.c.f, (long) i);
        } else if (this.d.c()) {
            o b2 = this.d.b();
            b2.b().a(this.c.f, b2.c(), (long) i, true);
        }
    }

    public String toString() {
        if (!bj.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder sb = new StringBuilder("StorageDetails = proxy[");
        sb.append("{free:");
        sb.append(realmGet$free());
        sb.append("}");
        sb.append(",");
        sb.append("{total:");
        sb.append(realmGet$total());
        sb.append("}");
        sb.append(",");
        sb.append("{freeExternal:");
        sb.append(realmGet$freeExternal());
        sb.append("}");
        sb.append(",");
        sb.append("{totalExternal:");
        sb.append(realmGet$totalExternal());
        sb.append("}");
        sb.append(",");
        sb.append("{freeSystem:");
        sb.append(realmGet$freeSystem());
        sb.append("}");
        sb.append(",");
        sb.append("{totalSystem:");
        sb.append(realmGet$totalSystem());
        sb.append("}");
        sb.append(",");
        sb.append("{freeSecondary:");
        sb.append(realmGet$freeSecondary());
        sb.append("}");
        sb.append(",");
        sb.append("{totalSecondary:");
        sb.append(realmGet$totalSecondary());
        sb.append("}");
        sb.append("]");
        return sb.toString();
    }
}
