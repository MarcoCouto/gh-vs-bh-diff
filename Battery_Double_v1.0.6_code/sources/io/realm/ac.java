package io.realm;

import com.hmatalonga.greenhub.models.data.Feature;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.c;
import io.realm.internal.m;
import io.realm.internal.o;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class ac extends Feature implements ad, m {

    /* renamed from: a reason: collision with root package name */
    private static final OsObjectSchemaInfo f4538a = e();

    /* renamed from: b reason: collision with root package name */
    private static final List<String> f4539b;
    private a c;
    private ba<Feature> d;

    static final class a extends c {

        /* renamed from: a reason: collision with root package name */
        long f4540a;

        /* renamed from: b reason: collision with root package name */
        long f4541b;

        a(OsSchemaInfo osSchemaInfo) {
            super(2);
            OsObjectSchemaInfo a2 = osSchemaInfo.a("Feature");
            this.f4540a = a("key", a2);
            this.f4541b = a("value", a2);
        }

        /* access modifiers changed from: protected */
        public final void a(c cVar, c cVar2) {
            a aVar = (a) cVar;
            a aVar2 = (a) cVar2;
            aVar2.f4540a = aVar.f4540a;
            aVar2.f4541b = aVar.f4541b;
        }
    }

    static {
        ArrayList arrayList = new ArrayList(2);
        arrayList.add("key");
        arrayList.add("value");
        f4539b = Collections.unmodifiableList(arrayList);
    }

    ac() {
        this.d.g();
    }

    public static Feature a(bb bbVar, Feature feature, boolean z, Map<bh, m> map) {
        if ((feature instanceof m) && ((m) feature).d().a() != null) {
            e a2 = ((m) feature).d().a();
            if (a2.c != bbVar.c) {
                throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
            } else if (a2.g().equals(bbVar.g())) {
                return feature;
            }
        }
        io.realm.e.a aVar = (io.realm.e.a) e.f.get();
        m mVar = (m) map.get(feature);
        return mVar != null ? (Feature) mVar : b(bbVar, feature, z, map);
    }

    public static a a(OsSchemaInfo osSchemaInfo) {
        return new a(osSchemaInfo);
    }

    public static Feature b(bb bbVar, Feature feature, boolean z, Map<bh, m> map) {
        m mVar = (m) map.get(feature);
        if (mVar != null) {
            return (Feature) mVar;
        }
        Feature feature2 = (Feature) bbVar.a(Feature.class, false, Collections.emptyList());
        map.put(feature, (m) feature2);
        ad adVar = feature;
        ad adVar2 = feature2;
        adVar2.realmSet$key(adVar.realmGet$key());
        adVar2.realmSet$value(adVar.realmGet$value());
        return feature2;
    }

    public static OsObjectSchemaInfo b() {
        return f4538a;
    }

    public static String c() {
        return "class_Feature";
    }

    private static OsObjectSchemaInfo e() {
        io.realm.internal.OsObjectSchemaInfo.a aVar = new io.realm.internal.OsObjectSchemaInfo.a("Feature", 2, 0);
        aVar.a("key", RealmFieldType.STRING, false, false, false);
        aVar.a("value", RealmFieldType.STRING, false, false, false);
        return aVar.a();
    }

    public void a() {
        if (this.d == null) {
            io.realm.e.a aVar = (io.realm.e.a) e.f.get();
            this.c = (a) aVar.c();
            this.d = new ba<>(this);
            this.d.a(aVar.a());
            this.d.a(aVar.b());
            this.d.a(aVar.d());
            this.d.a(aVar.e());
        }
    }

    public ba<?> d() {
        return this.d;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        ac acVar = (ac) obj;
        String g = this.d.a().g();
        String g2 = acVar.d.a().g();
        if (g == null ? g2 != null : !g.equals(g2)) {
            return false;
        }
        String g3 = this.d.b().b().g();
        String g4 = acVar.d.b().b().g();
        if (g3 == null ? g4 != null : !g3.equals(g4)) {
            return false;
        }
        return this.d.b().c() == acVar.d.b().c();
    }

    public int hashCode() {
        int i = 0;
        String g = this.d.a().g();
        String g2 = this.d.b().b().g();
        long c2 = this.d.b().c();
        int hashCode = ((g != null ? g.hashCode() : 0) + 527) * 31;
        if (g2 != null) {
            i = g2.hashCode();
        }
        return ((i + hashCode) * 31) + ((int) ((c2 >>> 32) ^ c2));
    }

    public String realmGet$key() {
        this.d.a().e();
        return this.d.b().l(this.c.f4540a);
    }

    public String realmGet$value() {
        this.d.a().e();
        return this.d.b().l(this.c.f4541b);
    }

    public void realmSet$key(String str) {
        if (!this.d.f()) {
            this.d.a().e();
            if (str == null) {
                this.d.b().c(this.c.f4540a);
            } else {
                this.d.b().a(this.c.f4540a, str);
            }
        } else if (this.d.c()) {
            o b2 = this.d.b();
            if (str == null) {
                b2.b().a(this.c.f4540a, b2.c(), true);
            } else {
                b2.b().a(this.c.f4540a, b2.c(), str, true);
            }
        }
    }

    public void realmSet$value(String str) {
        if (!this.d.f()) {
            this.d.a().e();
            if (str == null) {
                this.d.b().c(this.c.f4541b);
            } else {
                this.d.b().a(this.c.f4541b, str);
            }
        } else if (this.d.c()) {
            o b2 = this.d.b();
            if (str == null) {
                b2.b().a(this.c.f4541b, b2.c(), true);
            } else {
                b2.b().a(this.c.f4541b, b2.c(), str, true);
            }
        }
    }

    public String toString() {
        if (!bj.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder sb = new StringBuilder("Feature = proxy[");
        sb.append("{key:");
        sb.append(realmGet$key() != null ? realmGet$key() : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{value:");
        sb.append(realmGet$value() != null ? realmGet$value() : "null");
        sb.append("}");
        sb.append("]");
        return sb.toString();
    }
}
