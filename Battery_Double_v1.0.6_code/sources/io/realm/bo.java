package io.realm;

import io.realm.internal.Table;
import io.realm.internal.Util;
import io.realm.internal.b;
import io.realm.internal.c;
import java.util.HashMap;
import java.util.Map;

public abstract class bo {

    /* renamed from: a reason: collision with root package name */
    final e f4595a;

    /* renamed from: b reason: collision with root package name */
    private final Map<String, Table> f4596b = new HashMap();
    private final Map<Class<? extends bh>, Table> c = new HashMap();
    private final Map<Class<? extends bh>, bl> d = new HashMap();
    private final Map<String, bl> e = new HashMap();
    private final b f;

    bo(e eVar, b bVar) {
        this.f4595a = eVar;
        this.f = bVar;
    }

    private boolean a(Class<? extends bh> cls, Class<? extends bh> cls2) {
        return cls.equals(cls2);
    }

    private void c() {
        if (!a()) {
            throw new IllegalStateException("Attempt to use column index before set.");
        }
    }

    public abstract bl a(String str);

    /* access modifiers changed from: 0000 */
    public Table a(Class<? extends bh> cls) {
        Table table = (Table) this.c.get(cls);
        if (table == null) {
            Class a2 = Util.a(cls);
            if (a(a2, cls)) {
                table = (Table) this.c.get(a2);
            }
            if (table == null) {
                table = this.f4595a.l().getTable(this.f4595a.h().h().a(a2));
                this.c.put(a2, table);
            }
            if (a(a2, cls)) {
                this.c.put(cls, table);
            }
        }
        return table;
    }

    /* access modifiers changed from: 0000 */
    public void a(String str, String str2) {
        if (str == null || str.isEmpty()) {
            throw new IllegalArgumentException(str2);
        }
    }

    /* access modifiers changed from: 0000 */
    public final boolean a() {
        return this.f != null;
    }

    /* access modifiers changed from: 0000 */
    public bl b(Class<? extends bh> cls) {
        bl blVar = (bl) this.d.get(cls);
        if (blVar == null) {
            Class a2 = Util.a(cls);
            if (a(a2, cls)) {
                blVar = (bl) this.d.get(a2);
            }
            if (blVar == null) {
                blVar = new ag(this.f4595a, this, a(cls), c(a2));
                this.d.put(a2, blVar);
            }
            if (a(a2, cls)) {
                this.d.put(cls, blVar);
            }
        }
        return blVar;
    }

    /* access modifiers changed from: 0000 */
    public Table b(String str) {
        String c2 = Table.c(str);
        Table table = (Table) this.f4596b.get(c2);
        if (table != null) {
            return table;
        }
        Table table2 = this.f4595a.l().getTable(c2);
        this.f4596b.put(c2, table2);
        return table2;
    }

    /* access modifiers changed from: 0000 */
    public void b() {
        if (this.f != null) {
            this.f.a();
        }
        this.f4596b.clear();
        this.c.clear();
        this.d.clear();
        this.e.clear();
    }

    /* access modifiers changed from: 0000 */
    public final c c(Class<? extends bh> cls) {
        c();
        return this.f.a(cls);
    }

    /* access modifiers changed from: protected */
    public final c c(String str) {
        c();
        return this.f.a(str);
    }
}
