package io.realm;

import io.realm.internal.OsList;
import java.util.Locale;

final class l extends al<byte[]> {
    l(e eVar, OsList osList, Class<byte[]> cls) {
        super(eVar, osList, cls);
    }

    public void a(int i, Object obj) {
        this.f4548b.a((long) i, (byte[]) obj);
    }

    /* access modifiers changed from: protected */
    public void a(Object obj) {
        if (obj != null && !(obj instanceof byte[])) {
            throw new IllegalArgumentException(String.format(Locale.ENGLISH, "Unacceptable value type. Acceptable: %1$s, actual: %2$s .", new Object[]{"byte[]", obj.getClass().getName()}));
        }
    }

    /* renamed from: a */
    public byte[] b(int i) {
        return (byte[]) this.f4548b.f((long) i);
    }

    /* access modifiers changed from: protected */
    public void b(int i, Object obj) {
        this.f4548b.b((long) i, (byte[]) obj);
    }

    public void b(Object obj) {
        this.f4548b.a((byte[]) obj);
    }
}
