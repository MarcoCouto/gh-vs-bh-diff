package io.realm;

import com.hmatalonga.greenhub.models.data.Device;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.c;
import io.realm.internal.m;
import io.realm.internal.o;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class x extends Device implements m, y {

    /* renamed from: a reason: collision with root package name */
    private static final OsObjectSchemaInfo f4730a = e();

    /* renamed from: b reason: collision with root package name */
    private static final List<String> f4731b;
    private a c;
    private ba<Device> d;

    static final class a extends c {

        /* renamed from: a reason: collision with root package name */
        long f4732a;

        /* renamed from: b reason: collision with root package name */
        long f4733b;
        long c;
        long d;
        long e;
        long f;
        long g;
        long h;

        a(OsSchemaInfo osSchemaInfo) {
            super(8);
            OsObjectSchemaInfo a2 = osSchemaInfo.a("Device");
            this.f4732a = a("uuId", a2);
            this.f4733b = a("model", a2);
            this.c = a("manufacturer", a2);
            this.d = a("brand", a2);
            this.e = a("product", a2);
            this.f = a("osVersion", a2);
            this.g = a("kernelVersion", a2);
            this.h = a("isRoot", a2);
        }

        /* access modifiers changed from: protected */
        public final void a(c cVar, c cVar2) {
            a aVar = (a) cVar;
            a aVar2 = (a) cVar2;
            aVar2.f4732a = aVar.f4732a;
            aVar2.f4733b = aVar.f4733b;
            aVar2.c = aVar.c;
            aVar2.d = aVar.d;
            aVar2.e = aVar.e;
            aVar2.f = aVar.f;
            aVar2.g = aVar.g;
            aVar2.h = aVar.h;
        }
    }

    static {
        ArrayList arrayList = new ArrayList(8);
        arrayList.add("uuId");
        arrayList.add("model");
        arrayList.add("manufacturer");
        arrayList.add("brand");
        arrayList.add("product");
        arrayList.add("osVersion");
        arrayList.add("kernelVersion");
        arrayList.add("isRoot");
        f4731b = Collections.unmodifiableList(arrayList);
    }

    x() {
        this.d.g();
    }

    public static Device a(bb bbVar, Device device, boolean z, Map<bh, m> map) {
        if ((device instanceof m) && ((m) device).d().a() != null) {
            e a2 = ((m) device).d().a();
            if (a2.c != bbVar.c) {
                throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
            } else if (a2.g().equals(bbVar.g())) {
                return device;
            }
        }
        io.realm.e.a aVar = (io.realm.e.a) e.f.get();
        m mVar = (m) map.get(device);
        return mVar != null ? (Device) mVar : b(bbVar, device, z, map);
    }

    public static a a(OsSchemaInfo osSchemaInfo) {
        return new a(osSchemaInfo);
    }

    public static Device b(bb bbVar, Device device, boolean z, Map<bh, m> map) {
        m mVar = (m) map.get(device);
        if (mVar != null) {
            return (Device) mVar;
        }
        Device device2 = (Device) bbVar.a(Device.class, false, Collections.emptyList());
        map.put(device, (m) device2);
        y yVar = device;
        y yVar2 = device2;
        yVar2.realmSet$uuId(yVar.realmGet$uuId());
        yVar2.realmSet$model(yVar.realmGet$model());
        yVar2.realmSet$manufacturer(yVar.realmGet$manufacturer());
        yVar2.realmSet$brand(yVar.realmGet$brand());
        yVar2.realmSet$product(yVar.realmGet$product());
        yVar2.realmSet$osVersion(yVar.realmGet$osVersion());
        yVar2.realmSet$kernelVersion(yVar.realmGet$kernelVersion());
        yVar2.realmSet$isRoot(yVar.realmGet$isRoot());
        return device2;
    }

    public static OsObjectSchemaInfo b() {
        return f4730a;
    }

    public static String c() {
        return "class_Device";
    }

    private static OsObjectSchemaInfo e() {
        io.realm.internal.OsObjectSchemaInfo.a aVar = new io.realm.internal.OsObjectSchemaInfo.a("Device", 8, 0);
        aVar.a("uuId", RealmFieldType.STRING, false, false, false);
        aVar.a("model", RealmFieldType.STRING, false, false, false);
        aVar.a("manufacturer", RealmFieldType.STRING, false, false, false);
        aVar.a("brand", RealmFieldType.STRING, false, false, false);
        aVar.a("product", RealmFieldType.STRING, false, false, false);
        aVar.a("osVersion", RealmFieldType.STRING, false, false, false);
        aVar.a("kernelVersion", RealmFieldType.STRING, false, false, false);
        aVar.a("isRoot", RealmFieldType.INTEGER, false, false, true);
        return aVar.a();
    }

    public void a() {
        if (this.d == null) {
            io.realm.e.a aVar = (io.realm.e.a) e.f.get();
            this.c = (a) aVar.c();
            this.d = new ba<>(this);
            this.d.a(aVar.a());
            this.d.a(aVar.b());
            this.d.a(aVar.d());
            this.d.a(aVar.e());
        }
    }

    public ba<?> d() {
        return this.d;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        x xVar = (x) obj;
        String g = this.d.a().g();
        String g2 = xVar.d.a().g();
        if (g == null ? g2 != null : !g.equals(g2)) {
            return false;
        }
        String g3 = this.d.b().b().g();
        String g4 = xVar.d.b().b().g();
        if (g3 == null ? g4 != null : !g3.equals(g4)) {
            return false;
        }
        return this.d.b().c() == xVar.d.b().c();
    }

    public int hashCode() {
        int i = 0;
        String g = this.d.a().g();
        String g2 = this.d.b().b().g();
        long c2 = this.d.b().c();
        int hashCode = ((g != null ? g.hashCode() : 0) + 527) * 31;
        if (g2 != null) {
            i = g2.hashCode();
        }
        return ((i + hashCode) * 31) + ((int) ((c2 >>> 32) ^ c2));
    }

    public String realmGet$brand() {
        this.d.a().e();
        return this.d.b().l(this.c.d);
    }

    public int realmGet$isRoot() {
        this.d.a().e();
        return (int) this.d.b().g(this.c.h);
    }

    public String realmGet$kernelVersion() {
        this.d.a().e();
        return this.d.b().l(this.c.g);
    }

    public String realmGet$manufacturer() {
        this.d.a().e();
        return this.d.b().l(this.c.c);
    }

    public String realmGet$model() {
        this.d.a().e();
        return this.d.b().l(this.c.f4733b);
    }

    public String realmGet$osVersion() {
        this.d.a().e();
        return this.d.b().l(this.c.f);
    }

    public String realmGet$product() {
        this.d.a().e();
        return this.d.b().l(this.c.e);
    }

    public String realmGet$uuId() {
        this.d.a().e();
        return this.d.b().l(this.c.f4732a);
    }

    public void realmSet$brand(String str) {
        if (!this.d.f()) {
            this.d.a().e();
            if (str == null) {
                this.d.b().c(this.c.d);
            } else {
                this.d.b().a(this.c.d, str);
            }
        } else if (this.d.c()) {
            o b2 = this.d.b();
            if (str == null) {
                b2.b().a(this.c.d, b2.c(), true);
            } else {
                b2.b().a(this.c.d, b2.c(), str, true);
            }
        }
    }

    public void realmSet$isRoot(int i) {
        if (!this.d.f()) {
            this.d.a().e();
            this.d.b().a(this.c.h, (long) i);
        } else if (this.d.c()) {
            o b2 = this.d.b();
            b2.b().a(this.c.h, b2.c(), (long) i, true);
        }
    }

    public void realmSet$kernelVersion(String str) {
        if (!this.d.f()) {
            this.d.a().e();
            if (str == null) {
                this.d.b().c(this.c.g);
            } else {
                this.d.b().a(this.c.g, str);
            }
        } else if (this.d.c()) {
            o b2 = this.d.b();
            if (str == null) {
                b2.b().a(this.c.g, b2.c(), true);
            } else {
                b2.b().a(this.c.g, b2.c(), str, true);
            }
        }
    }

    public void realmSet$manufacturer(String str) {
        if (!this.d.f()) {
            this.d.a().e();
            if (str == null) {
                this.d.b().c(this.c.c);
            } else {
                this.d.b().a(this.c.c, str);
            }
        } else if (this.d.c()) {
            o b2 = this.d.b();
            if (str == null) {
                b2.b().a(this.c.c, b2.c(), true);
            } else {
                b2.b().a(this.c.c, b2.c(), str, true);
            }
        }
    }

    public void realmSet$model(String str) {
        if (!this.d.f()) {
            this.d.a().e();
            if (str == null) {
                this.d.b().c(this.c.f4733b);
            } else {
                this.d.b().a(this.c.f4733b, str);
            }
        } else if (this.d.c()) {
            o b2 = this.d.b();
            if (str == null) {
                b2.b().a(this.c.f4733b, b2.c(), true);
            } else {
                b2.b().a(this.c.f4733b, b2.c(), str, true);
            }
        }
    }

    public void realmSet$osVersion(String str) {
        if (!this.d.f()) {
            this.d.a().e();
            if (str == null) {
                this.d.b().c(this.c.f);
            } else {
                this.d.b().a(this.c.f, str);
            }
        } else if (this.d.c()) {
            o b2 = this.d.b();
            if (str == null) {
                b2.b().a(this.c.f, b2.c(), true);
            } else {
                b2.b().a(this.c.f, b2.c(), str, true);
            }
        }
    }

    public void realmSet$product(String str) {
        if (!this.d.f()) {
            this.d.a().e();
            if (str == null) {
                this.d.b().c(this.c.e);
            } else {
                this.d.b().a(this.c.e, str);
            }
        } else if (this.d.c()) {
            o b2 = this.d.b();
            if (str == null) {
                b2.b().a(this.c.e, b2.c(), true);
            } else {
                b2.b().a(this.c.e, b2.c(), str, true);
            }
        }
    }

    public void realmSet$uuId(String str) {
        if (!this.d.f()) {
            this.d.a().e();
            if (str == null) {
                this.d.b().c(this.c.f4732a);
            } else {
                this.d.b().a(this.c.f4732a, str);
            }
        } else if (this.d.c()) {
            o b2 = this.d.b();
            if (str == null) {
                b2.b().a(this.c.f4732a, b2.c(), true);
            } else {
                b2.b().a(this.c.f4732a, b2.c(), str, true);
            }
        }
    }

    public String toString() {
        if (!bj.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder sb = new StringBuilder("Device = proxy[");
        sb.append("{uuId:");
        sb.append(realmGet$uuId() != null ? realmGet$uuId() : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{model:");
        sb.append(realmGet$model() != null ? realmGet$model() : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{manufacturer:");
        sb.append(realmGet$manufacturer() != null ? realmGet$manufacturer() : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{brand:");
        sb.append(realmGet$brand() != null ? realmGet$brand() : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{product:");
        sb.append(realmGet$product() != null ? realmGet$product() : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{osVersion:");
        sb.append(realmGet$osVersion() != null ? realmGet$osVersion() : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{kernelVersion:");
        sb.append(realmGet$kernelVersion() != null ? realmGet$kernelVersion() : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{isRoot:");
        sb.append(realmGet$isRoot());
        sb.append("}");
        sb.append("]");
        return sb.toString();
    }
}
