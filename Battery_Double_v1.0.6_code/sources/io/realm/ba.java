package io.realm;

import io.realm.bh;
import io.realm.internal.OsObject;
import io.realm.internal.UncheckedRow;
import io.realm.internal.j;
import io.realm.internal.k;
import io.realm.internal.m;
import io.realm.internal.o;
import java.util.List;

public final class ba<E extends bh> implements io.realm.internal.k.a {
    private static a i = new a();

    /* renamed from: a reason: collision with root package name */
    private E f4569a;

    /* renamed from: b reason: collision with root package name */
    private boolean f4570b = true;
    private o c;
    private OsObject d;
    private e e;
    private boolean f;
    private List<String> g;
    private j<io.realm.internal.OsObject.b> h = new j<>();

    private static class a implements io.realm.internal.j.a<io.realm.internal.OsObject.b> {
        private a() {
        }

        public void a(io.realm.internal.OsObject.b bVar, Object obj) {
            bVar.a((bh) obj, null);
        }
    }

    static class b<T extends bh> implements bk<T> {

        /* renamed from: a reason: collision with root package name */
        private final bd<T> f4571a;

        b(bd<T> bdVar) {
            if (bdVar == null) {
                throw new IllegalArgumentException("Listener should not be null");
            }
            this.f4571a = bdVar;
        }

        public void a(T t, au auVar) {
            this.f4571a.a(t);
        }

        public boolean equals(Object obj) {
            return (obj instanceof b) && this.f4571a == ((b) obj).f4571a;
        }

        public int hashCode() {
            return this.f4571a.hashCode();
        }
    }

    public ba() {
    }

    public ba(E e2) {
        this.f4569a = e2;
    }

    private void j() {
        this.h.a((io.realm.internal.j.a<T>) i);
    }

    private void k() {
        if (this.e.e != null && !this.e.e.isClosed() && this.c.d() && this.d == null) {
            this.d = new OsObject(this.e.e, (UncheckedRow) this.c);
            this.d.setObserverPairs(this.h);
            this.h = null;
        }
    }

    public e a() {
        return this.e;
    }

    public void a(bh bhVar) {
        if (!bj.isValid(bhVar) || !bj.isManaged(bhVar)) {
            throw new IllegalArgumentException("'value' is not a valid managed object.");
        } else if (((m) bhVar).d().a() != a()) {
            throw new IllegalArgumentException("'value' belongs to a different Realm.");
        }
    }

    public void a(bk<E> bkVar) {
        if (this.c instanceof k) {
            this.h.a(new io.realm.internal.OsObject.b(this.f4569a, bkVar));
        } else if (this.c instanceof UncheckedRow) {
            k();
            if (this.d != null) {
                this.d.addListener(this.f4569a, bkVar);
            }
        }
    }

    public void a(e eVar) {
        this.e = eVar;
    }

    public void a(o oVar) {
        this.c = oVar;
    }

    public void a(List<String> list) {
        this.g = list;
    }

    public void a(boolean z) {
        this.f = z;
    }

    public o b() {
        return this.c;
    }

    public void b(bk<E> bkVar) {
        if (this.d != null) {
            this.d.removeListener(this.f4569a, bkVar);
        } else {
            this.h.a(this.f4569a, bkVar);
        }
    }

    public void b(o oVar) {
        this.c = oVar;
        j();
        if (oVar.d()) {
            k();
        }
    }

    public boolean c() {
        return this.f;
    }

    public List<String> d() {
        return this.g;
    }

    public void e() {
        if (this.d != null) {
            this.d.removeListener(this.f4569a);
        } else {
            this.h.b();
        }
    }

    public boolean f() {
        return this.f4570b;
    }

    public void g() {
        this.f4570b = false;
        this.g = null;
    }

    public boolean h() {
        return !(this.c instanceof k);
    }

    public void i() {
        if (this.c instanceof k) {
            ((k) this.c).e();
        }
    }
}
