package io.realm;

import io.realm.internal.Table;
import io.realm.internal.c;

class ag extends bl {
    ag(e eVar, bo boVar, Table table, c cVar) {
        super(eVar, boVar, table, cVar);
    }

    public bl a(String str) {
        throw new UnsupportedOperationException("This 'RealmObjectSchema' is immutable. Please use 'DynamicRealm.getSchema() to get a mutable instance.");
    }

    public bl a(String str, Class<?> cls, ae... aeVarArr) {
        throw new UnsupportedOperationException("This 'RealmObjectSchema' is immutable. Please use 'DynamicRealm.getSchema() to get a mutable instance.");
    }
}
