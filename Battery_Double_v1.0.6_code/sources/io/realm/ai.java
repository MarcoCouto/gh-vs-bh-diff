package io.realm;

import com.hmatalonga.greenhub.models.data.LocationProvider;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.c;
import io.realm.internal.m;
import io.realm.internal.o;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class ai extends LocationProvider implements aj, m {

    /* renamed from: a reason: collision with root package name */
    private static final OsObjectSchemaInfo f4544a = e();

    /* renamed from: b reason: collision with root package name */
    private static final List<String> f4545b;
    private a c;
    private ba<LocationProvider> d;

    static final class a extends c {

        /* renamed from: a reason: collision with root package name */
        long f4546a;

        a(OsSchemaInfo osSchemaInfo) {
            super(1);
            this.f4546a = a("provider", osSchemaInfo.a("LocationProvider"));
        }

        /* access modifiers changed from: protected */
        public final void a(c cVar, c cVar2) {
            ((a) cVar2).f4546a = ((a) cVar).f4546a;
        }
    }

    static {
        ArrayList arrayList = new ArrayList(1);
        arrayList.add("provider");
        f4545b = Collections.unmodifiableList(arrayList);
    }

    ai() {
        this.d.g();
    }

    public static LocationProvider a(bb bbVar, LocationProvider locationProvider, boolean z, Map<bh, m> map) {
        if ((locationProvider instanceof m) && ((m) locationProvider).d().a() != null) {
            e a2 = ((m) locationProvider).d().a();
            if (a2.c != bbVar.c) {
                throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
            } else if (a2.g().equals(bbVar.g())) {
                return locationProvider;
            }
        }
        io.realm.e.a aVar = (io.realm.e.a) e.f.get();
        m mVar = (m) map.get(locationProvider);
        return mVar != null ? (LocationProvider) mVar : b(bbVar, locationProvider, z, map);
    }

    public static a a(OsSchemaInfo osSchemaInfo) {
        return new a(osSchemaInfo);
    }

    public static LocationProvider b(bb bbVar, LocationProvider locationProvider, boolean z, Map<bh, m> map) {
        m mVar = (m) map.get(locationProvider);
        if (mVar != null) {
            return (LocationProvider) mVar;
        }
        LocationProvider locationProvider2 = (LocationProvider) bbVar.a(LocationProvider.class, false, Collections.emptyList());
        map.put(locationProvider, (m) locationProvider2);
        locationProvider2.realmSet$provider(locationProvider.realmGet$provider());
        return locationProvider2;
    }

    public static OsObjectSchemaInfo b() {
        return f4544a;
    }

    public static String c() {
        return "class_LocationProvider";
    }

    private static OsObjectSchemaInfo e() {
        io.realm.internal.OsObjectSchemaInfo.a aVar = new io.realm.internal.OsObjectSchemaInfo.a("LocationProvider", 1, 0);
        aVar.a("provider", RealmFieldType.STRING, false, false, false);
        return aVar.a();
    }

    public void a() {
        if (this.d == null) {
            io.realm.e.a aVar = (io.realm.e.a) e.f.get();
            this.c = (a) aVar.c();
            this.d = new ba<>(this);
            this.d.a(aVar.a());
            this.d.a(aVar.b());
            this.d.a(aVar.d());
            this.d.a(aVar.e());
        }
    }

    public ba<?> d() {
        return this.d;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        ai aiVar = (ai) obj;
        String g = this.d.a().g();
        String g2 = aiVar.d.a().g();
        if (g == null ? g2 != null : !g.equals(g2)) {
            return false;
        }
        String g3 = this.d.b().b().g();
        String g4 = aiVar.d.b().b().g();
        if (g3 == null ? g4 != null : !g3.equals(g4)) {
            return false;
        }
        return this.d.b().c() == aiVar.d.b().c();
    }

    public int hashCode() {
        int i = 0;
        String g = this.d.a().g();
        String g2 = this.d.b().b().g();
        long c2 = this.d.b().c();
        int hashCode = ((g != null ? g.hashCode() : 0) + 527) * 31;
        if (g2 != null) {
            i = g2.hashCode();
        }
        return ((i + hashCode) * 31) + ((int) ((c2 >>> 32) ^ c2));
    }

    public String realmGet$provider() {
        this.d.a().e();
        return this.d.b().l(this.c.f4546a);
    }

    public void realmSet$provider(String str) {
        if (!this.d.f()) {
            this.d.a().e();
            if (str == null) {
                this.d.b().c(this.c.f4546a);
            } else {
                this.d.b().a(this.c.f4546a, str);
            }
        } else if (this.d.c()) {
            o b2 = this.d.b();
            if (str == null) {
                b2.b().a(this.c.f4546a, b2.c(), true);
            } else {
                b2.b().a(this.c.f4546a, b2.c(), str, true);
            }
        }
    }

    public String toString() {
        if (!bj.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder sb = new StringBuilder("LocationProvider = proxy[");
        sb.append("{provider:");
        sb.append(realmGet$provider() != null ? realmGet$provider() : "null");
        sb.append("}");
        sb.append("]");
        return sb.toString();
    }
}
