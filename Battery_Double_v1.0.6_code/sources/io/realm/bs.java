package io.realm;

import com.hmatalonga.greenhub.models.data.Settings;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.c;
import io.realm.internal.m;
import io.realm.internal.o;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class bs extends Settings implements bt, m {

    /* renamed from: a reason: collision with root package name */
    private static final OsObjectSchemaInfo f4602a = e();

    /* renamed from: b reason: collision with root package name */
    private static final List<String> f4603b;
    private a c;
    private ba<Settings> d;

    static final class a extends c {

        /* renamed from: a reason: collision with root package name */
        long f4604a;

        /* renamed from: b reason: collision with root package name */
        long f4605b;
        long c;
        long d;
        long e;
        long f;
        long g;

        a(OsSchemaInfo osSchemaInfo) {
            super(7);
            OsObjectSchemaInfo a2 = osSchemaInfo.a("Settings");
            this.f4604a = a("bluetoothEnabled", a2);
            this.f4605b = a("locationEnabled", a2);
            this.c = a("powersaverEnabled", a2);
            this.d = a("flashlightEnabled", a2);
            this.e = a("nfcEnabled", a2);
            this.f = a("unknownSources", a2);
            this.g = a("developerMode", a2);
        }

        /* access modifiers changed from: protected */
        public final void a(c cVar, c cVar2) {
            a aVar = (a) cVar;
            a aVar2 = (a) cVar2;
            aVar2.f4604a = aVar.f4604a;
            aVar2.f4605b = aVar.f4605b;
            aVar2.c = aVar.c;
            aVar2.d = aVar.d;
            aVar2.e = aVar.e;
            aVar2.f = aVar.f;
            aVar2.g = aVar.g;
        }
    }

    static {
        ArrayList arrayList = new ArrayList(7);
        arrayList.add("bluetoothEnabled");
        arrayList.add("locationEnabled");
        arrayList.add("powersaverEnabled");
        arrayList.add("flashlightEnabled");
        arrayList.add("nfcEnabled");
        arrayList.add("unknownSources");
        arrayList.add("developerMode");
        f4603b = Collections.unmodifiableList(arrayList);
    }

    bs() {
        this.d.g();
    }

    public static Settings a(bb bbVar, Settings settings, boolean z, Map<bh, m> map) {
        if ((settings instanceof m) && ((m) settings).d().a() != null) {
            e a2 = ((m) settings).d().a();
            if (a2.c != bbVar.c) {
                throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
            } else if (a2.g().equals(bbVar.g())) {
                return settings;
            }
        }
        io.realm.e.a aVar = (io.realm.e.a) e.f.get();
        m mVar = (m) map.get(settings);
        return mVar != null ? (Settings) mVar : b(bbVar, settings, z, map);
    }

    public static a a(OsSchemaInfo osSchemaInfo) {
        return new a(osSchemaInfo);
    }

    public static Settings b(bb bbVar, Settings settings, boolean z, Map<bh, m> map) {
        m mVar = (m) map.get(settings);
        if (mVar != null) {
            return (Settings) mVar;
        }
        Settings settings2 = (Settings) bbVar.a(Settings.class, false, Collections.emptyList());
        map.put(settings, (m) settings2);
        bt btVar = settings;
        bt btVar2 = settings2;
        btVar2.realmSet$bluetoothEnabled(btVar.realmGet$bluetoothEnabled());
        btVar2.realmSet$locationEnabled(btVar.realmGet$locationEnabled());
        btVar2.realmSet$powersaverEnabled(btVar.realmGet$powersaverEnabled());
        btVar2.realmSet$flashlightEnabled(btVar.realmGet$flashlightEnabled());
        btVar2.realmSet$nfcEnabled(btVar.realmGet$nfcEnabled());
        btVar2.realmSet$unknownSources(btVar.realmGet$unknownSources());
        btVar2.realmSet$developerMode(btVar.realmGet$developerMode());
        return settings2;
    }

    public static OsObjectSchemaInfo b() {
        return f4602a;
    }

    public static String c() {
        return "class_Settings";
    }

    private static OsObjectSchemaInfo e() {
        io.realm.internal.OsObjectSchemaInfo.a aVar = new io.realm.internal.OsObjectSchemaInfo.a("Settings", 7, 0);
        aVar.a("bluetoothEnabled", RealmFieldType.BOOLEAN, false, false, true);
        aVar.a("locationEnabled", RealmFieldType.BOOLEAN, false, false, true);
        aVar.a("powersaverEnabled", RealmFieldType.BOOLEAN, false, false, true);
        aVar.a("flashlightEnabled", RealmFieldType.BOOLEAN, false, false, true);
        aVar.a("nfcEnabled", RealmFieldType.BOOLEAN, false, false, true);
        aVar.a("unknownSources", RealmFieldType.INTEGER, false, false, true);
        aVar.a("developerMode", RealmFieldType.INTEGER, false, false, true);
        return aVar.a();
    }

    public void a() {
        if (this.d == null) {
            io.realm.e.a aVar = (io.realm.e.a) e.f.get();
            this.c = (a) aVar.c();
            this.d = new ba<>(this);
            this.d.a(aVar.a());
            this.d.a(aVar.b());
            this.d.a(aVar.d());
            this.d.a(aVar.e());
        }
    }

    public ba<?> d() {
        return this.d;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        bs bsVar = (bs) obj;
        String g = this.d.a().g();
        String g2 = bsVar.d.a().g();
        if (g == null ? g2 != null : !g.equals(g2)) {
            return false;
        }
        String g3 = this.d.b().b().g();
        String g4 = bsVar.d.b().b().g();
        if (g3 == null ? g4 != null : !g3.equals(g4)) {
            return false;
        }
        return this.d.b().c() == bsVar.d.b().c();
    }

    public int hashCode() {
        int i = 0;
        String g = this.d.a().g();
        String g2 = this.d.b().b().g();
        long c2 = this.d.b().c();
        int hashCode = ((g != null ? g.hashCode() : 0) + 527) * 31;
        if (g2 != null) {
            i = g2.hashCode();
        }
        return ((i + hashCode) * 31) + ((int) ((c2 >>> 32) ^ c2));
    }

    public boolean realmGet$bluetoothEnabled() {
        this.d.a().e();
        return this.d.b().h(this.c.f4604a);
    }

    public int realmGet$developerMode() {
        this.d.a().e();
        return (int) this.d.b().g(this.c.g);
    }

    public boolean realmGet$flashlightEnabled() {
        this.d.a().e();
        return this.d.b().h(this.c.d);
    }

    public boolean realmGet$locationEnabled() {
        this.d.a().e();
        return this.d.b().h(this.c.f4605b);
    }

    public boolean realmGet$nfcEnabled() {
        this.d.a().e();
        return this.d.b().h(this.c.e);
    }

    public boolean realmGet$powersaverEnabled() {
        this.d.a().e();
        return this.d.b().h(this.c.c);
    }

    public int realmGet$unknownSources() {
        this.d.a().e();
        return (int) this.d.b().g(this.c.f);
    }

    public void realmSet$bluetoothEnabled(boolean z) {
        if (!this.d.f()) {
            this.d.a().e();
            this.d.b().a(this.c.f4604a, z);
        } else if (this.d.c()) {
            o b2 = this.d.b();
            b2.b().a(this.c.f4604a, b2.c(), z, true);
        }
    }

    public void realmSet$developerMode(int i) {
        if (!this.d.f()) {
            this.d.a().e();
            this.d.b().a(this.c.g, (long) i);
        } else if (this.d.c()) {
            o b2 = this.d.b();
            b2.b().a(this.c.g, b2.c(), (long) i, true);
        }
    }

    public void realmSet$flashlightEnabled(boolean z) {
        if (!this.d.f()) {
            this.d.a().e();
            this.d.b().a(this.c.d, z);
        } else if (this.d.c()) {
            o b2 = this.d.b();
            b2.b().a(this.c.d, b2.c(), z, true);
        }
    }

    public void realmSet$locationEnabled(boolean z) {
        if (!this.d.f()) {
            this.d.a().e();
            this.d.b().a(this.c.f4605b, z);
        } else if (this.d.c()) {
            o b2 = this.d.b();
            b2.b().a(this.c.f4605b, b2.c(), z, true);
        }
    }

    public void realmSet$nfcEnabled(boolean z) {
        if (!this.d.f()) {
            this.d.a().e();
            this.d.b().a(this.c.e, z);
        } else if (this.d.c()) {
            o b2 = this.d.b();
            b2.b().a(this.c.e, b2.c(), z, true);
        }
    }

    public void realmSet$powersaverEnabled(boolean z) {
        if (!this.d.f()) {
            this.d.a().e();
            this.d.b().a(this.c.c, z);
        } else if (this.d.c()) {
            o b2 = this.d.b();
            b2.b().a(this.c.c, b2.c(), z, true);
        }
    }

    public void realmSet$unknownSources(int i) {
        if (!this.d.f()) {
            this.d.a().e();
            this.d.b().a(this.c.f, (long) i);
        } else if (this.d.c()) {
            o b2 = this.d.b();
            b2.b().a(this.c.f, b2.c(), (long) i, true);
        }
    }

    public String toString() {
        if (!bj.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder sb = new StringBuilder("Settings = proxy[");
        sb.append("{bluetoothEnabled:");
        sb.append(realmGet$bluetoothEnabled());
        sb.append("}");
        sb.append(",");
        sb.append("{locationEnabled:");
        sb.append(realmGet$locationEnabled());
        sb.append("}");
        sb.append(",");
        sb.append("{powersaverEnabled:");
        sb.append(realmGet$powersaverEnabled());
        sb.append("}");
        sb.append(",");
        sb.append("{flashlightEnabled:");
        sb.append(realmGet$flashlightEnabled());
        sb.append("}");
        sb.append(",");
        sb.append("{nfcEnabled:");
        sb.append(realmGet$nfcEnabled());
        sb.append("}");
        sb.append(",");
        sb.append("{unknownSources:");
        sb.append(realmGet$unknownSources());
        sb.append("}");
        sb.append(",");
        sb.append("{developerMode:");
        sb.append(realmGet$developerMode());
        sb.append("}");
        sb.append("]");
        return sb.toString();
    }
}
