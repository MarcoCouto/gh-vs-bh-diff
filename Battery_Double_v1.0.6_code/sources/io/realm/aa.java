package io.realm;

import io.realm.internal.OsObjectStore;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.SharedRealm;

public class aa extends e {
    private final bo g = new ap(this);

    private aa(final bc bcVar) {
        super(bcVar, (OsSchemaInfo) null);
        bc.a(bcVar.a(), (a) new a() {
            public void a(int i) {
                if (i <= 0 && !bcVar.a().p() && OsObjectStore.a(aa.this.e) == -1) {
                    aa.this.e.beginTransaction();
                    if (OsObjectStore.a(aa.this.e) == -1) {
                        OsObjectStore.a(aa.this.e, -1);
                    }
                    aa.this.e.commitTransaction();
                }
            }
        });
    }

    private aa(SharedRealm sharedRealm) {
        super(sharedRealm);
    }

    static aa a(bc bcVar) {
        return new aa(bcVar);
    }

    static aa a(SharedRealm sharedRealm) {
        return new aa(sharedRealm);
    }

    public /* bridge */ /* synthetic */ boolean a() {
        return super.a();
    }

    public /* bridge */ /* synthetic */ void b() {
        super.b();
    }

    public /* bridge */ /* synthetic */ void c() {
        super.c();
    }

    public /* bridge */ /* synthetic */ void close() {
        super.close();
    }

    public /* bridge */ /* synthetic */ void d() {
        super.d();
    }

    public /* bridge */ /* synthetic */ String g() {
        return super.g();
    }

    public /* bridge */ /* synthetic */ be h() {
        return super.h();
    }

    public /* bridge */ /* synthetic */ boolean j() {
        return super.j();
    }

    public bo k() {
        return this.g;
    }
}
