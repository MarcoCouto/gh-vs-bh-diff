package io.realm;

import com.hmatalonga.greenhub.models.data.AppSignature;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.m;
import io.realm.internal.o;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class c extends AppSignature implements d, m {

    /* renamed from: a reason: collision with root package name */
    private static final OsObjectSchemaInfo f4612a = e();

    /* renamed from: b reason: collision with root package name */
    private static final List<String> f4613b;
    private a c;
    private ba<AppSignature> d;

    static final class a extends io.realm.internal.c {

        /* renamed from: a reason: collision with root package name */
        long f4614a;

        a(OsSchemaInfo osSchemaInfo) {
            super(1);
            this.f4614a = a("signature", osSchemaInfo.a("AppSignature"));
        }

        /* access modifiers changed from: protected */
        public final void a(io.realm.internal.c cVar, io.realm.internal.c cVar2) {
            ((a) cVar2).f4614a = ((a) cVar).f4614a;
        }
    }

    static {
        ArrayList arrayList = new ArrayList(1);
        arrayList.add("signature");
        f4613b = Collections.unmodifiableList(arrayList);
    }

    c() {
        this.d.g();
    }

    public static AppSignature a(bb bbVar, AppSignature appSignature, boolean z, Map<bh, m> map) {
        if ((appSignature instanceof m) && ((m) appSignature).d().a() != null) {
            e a2 = ((m) appSignature).d().a();
            if (a2.c != bbVar.c) {
                throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
            } else if (a2.g().equals(bbVar.g())) {
                return appSignature;
            }
        }
        io.realm.e.a aVar = (io.realm.e.a) e.f.get();
        m mVar = (m) map.get(appSignature);
        return mVar != null ? (AppSignature) mVar : b(bbVar, appSignature, z, map);
    }

    public static a a(OsSchemaInfo osSchemaInfo) {
        return new a(osSchemaInfo);
    }

    public static AppSignature b(bb bbVar, AppSignature appSignature, boolean z, Map<bh, m> map) {
        m mVar = (m) map.get(appSignature);
        if (mVar != null) {
            return (AppSignature) mVar;
        }
        AppSignature appSignature2 = (AppSignature) bbVar.a(AppSignature.class, false, Collections.emptyList());
        map.put(appSignature, (m) appSignature2);
        appSignature2.realmSet$signature(appSignature.realmGet$signature());
        return appSignature2;
    }

    public static OsObjectSchemaInfo b() {
        return f4612a;
    }

    public static String c() {
        return "class_AppSignature";
    }

    private static OsObjectSchemaInfo e() {
        io.realm.internal.OsObjectSchemaInfo.a aVar = new io.realm.internal.OsObjectSchemaInfo.a("AppSignature", 1, 0);
        aVar.a("signature", RealmFieldType.STRING, false, false, false);
        return aVar.a();
    }

    public void a() {
        if (this.d == null) {
            io.realm.e.a aVar = (io.realm.e.a) e.f.get();
            this.c = (a) aVar.c();
            this.d = new ba<>(this);
            this.d.a(aVar.a());
            this.d.a(aVar.b());
            this.d.a(aVar.d());
            this.d.a(aVar.e());
        }
    }

    public ba<?> d() {
        return this.d;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        c cVar = (c) obj;
        String g = this.d.a().g();
        String g2 = cVar.d.a().g();
        if (g == null ? g2 != null : !g.equals(g2)) {
            return false;
        }
        String g3 = this.d.b().b().g();
        String g4 = cVar.d.b().b().g();
        if (g3 == null ? g4 != null : !g3.equals(g4)) {
            return false;
        }
        return this.d.b().c() == cVar.d.b().c();
    }

    public int hashCode() {
        int i = 0;
        String g = this.d.a().g();
        String g2 = this.d.b().b().g();
        long c2 = this.d.b().c();
        int hashCode = ((g != null ? g.hashCode() : 0) + 527) * 31;
        if (g2 != null) {
            i = g2.hashCode();
        }
        return ((i + hashCode) * 31) + ((int) ((c2 >>> 32) ^ c2));
    }

    public String realmGet$signature() {
        this.d.a().e();
        return this.d.b().l(this.c.f4614a);
    }

    public void realmSet$signature(String str) {
        if (!this.d.f()) {
            this.d.a().e();
            if (str == null) {
                this.d.b().c(this.c.f4614a);
            } else {
                this.d.b().a(this.c.f4614a, str);
            }
        } else if (this.d.c()) {
            o b2 = this.d.b();
            if (str == null) {
                b2.b().a(this.c.f4614a, b2.c(), true);
            } else {
                b2.b().a(this.c.f4614a, b2.c(), str, true);
            }
        }
    }

    public String toString() {
        if (!bj.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder sb = new StringBuilder("AppSignature = proxy[");
        sb.append("{signature:");
        sb.append(realmGet$signature() != null ? realmGet$signature() : "null");
        sb.append("}");
        sb.append("]");
        return sb.toString();
    }
}
