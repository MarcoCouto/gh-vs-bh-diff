package io.realm;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import io.realm.internal.Collection;
import io.realm.internal.UncheckedRow;
import io.realm.internal.f;
import io.realm.internal.m;
import java.util.AbstractList;
import java.util.Iterator;
import java.util.ListIterator;

abstract class ax<E> extends AbstractList<E> implements OrderedRealmCollection<E> {

    /* renamed from: a reason: collision with root package name */
    final e f4561a;

    /* renamed from: b reason: collision with root package name */
    final Class<E> f4562b;
    final String c;
    @SuppressFBWarnings({"SS_SHOULD_BE_STATIC"})
    final boolean d;
    final Collection e;

    private class a extends io.realm.internal.Collection.a<E> {
        a() {
            super(ax.this.e);
        }

        /* access modifiers changed from: protected */
        public E a(UncheckedRow uncheckedRow) {
            return ax.this.f4561a.a(ax.this.f4562b, ax.this.c, uncheckedRow);
        }
    }

    private class b extends io.realm.internal.Collection.b<E> {
        b(int i) {
            super(ax.this.e, i);
        }

        /* access modifiers changed from: protected */
        public E a(UncheckedRow uncheckedRow) {
            return ax.this.f4561a.a(ax.this.f4562b, ax.this.c, uncheckedRow);
        }
    }

    ax(e eVar, Collection collection, Class<E> cls) {
        this(eVar, collection, cls, null);
    }

    private ax(e eVar, Collection collection, Class<E> cls, String str) {
        this.d = false;
        this.f4561a = eVar;
        this.e = collection;
        this.f4562b = cls;
        this.c = str;
    }

    ax(e eVar, Collection collection, String str) {
        this(eVar, collection, null, str);
    }

    private E a(boolean z, E e2) {
        UncheckedRow c2 = this.e.c();
        if (c2 != null) {
            return this.f4561a.a(this.f4562b, this.c, c2);
        }
        if (!z) {
            return e2;
        }
        throw new IndexOutOfBoundsException("No results were found.");
    }

    public E a() {
        return a(true, null);
    }

    @Deprecated
    public void add(int i, E e2) {
        throw new UnsupportedOperationException("This method is not supported by 'RealmResults' or 'OrderedRealmCollectionSnapshot'.");
    }

    @Deprecated
    public boolean add(E e2) {
        throw new UnsupportedOperationException("This method is not supported by 'RealmResults' or 'OrderedRealmCollectionSnapshot'.");
    }

    @Deprecated
    public boolean addAll(int i, java.util.Collection<? extends E> collection) {
        throw new UnsupportedOperationException("This method is not supported by 'RealmResults' or 'OrderedRealmCollectionSnapshot'.");
    }

    @Deprecated
    public boolean addAll(java.util.Collection<? extends E> collection) {
        throw new UnsupportedOperationException("This method is not supported by 'RealmResults' or 'OrderedRealmCollectionSnapshot'.");
    }

    public boolean b() {
        this.f4561a.e();
        if (size() <= 0) {
            return false;
        }
        this.e.e();
        return true;
    }

    @Deprecated
    public void clear() {
        throw new UnsupportedOperationException("This method is not supported by 'RealmResults' or 'OrderedRealmCollectionSnapshot'.");
    }

    public boolean contains(Object obj) {
        if (c()) {
            if ((obj instanceof m) && ((m) obj).d().b() == f.INSTANCE) {
                return false;
            }
            Iterator it = iterator();
            while (it.hasNext()) {
                if (it.next().equals(obj)) {
                    return true;
                }
            }
        }
        return false;
    }

    public E get(int i) {
        this.f4561a.e();
        return this.f4561a.a(this.f4562b, this.c, this.e.a(i));
    }

    public Iterator<E> iterator() {
        return new a();
    }

    public ListIterator<E> listIterator() {
        return new b(0);
    }

    public ListIterator<E> listIterator(int i) {
        return new b(i);
    }

    @Deprecated
    public E remove(int i) {
        throw new UnsupportedOperationException("This method is not supported by 'RealmResults' or 'OrderedRealmCollectionSnapshot'.");
    }

    @Deprecated
    public boolean remove(Object obj) {
        throw new UnsupportedOperationException("This method is not supported by 'RealmResults' or 'OrderedRealmCollectionSnapshot'.");
    }

    @Deprecated
    public boolean removeAll(java.util.Collection<?> collection) {
        throw new UnsupportedOperationException("This method is not supported by 'RealmResults' or 'OrderedRealmCollectionSnapshot'.");
    }

    @Deprecated
    public boolean retainAll(java.util.Collection<?> collection) {
        throw new UnsupportedOperationException("This method is not supported by 'RealmResults' or 'OrderedRealmCollectionSnapshot'.");
    }

    @Deprecated
    public E set(int i, E e2) {
        throw new UnsupportedOperationException("This method is not supported by 'RealmResults' or 'OrderedRealmCollectionSnapshot'.");
    }

    public int size() {
        if (!c()) {
            return 0;
        }
        long d2 = this.e.d();
        if (d2 > 2147483647L) {
            return Integer.MAX_VALUE;
        }
        return (int) d2;
    }
}
