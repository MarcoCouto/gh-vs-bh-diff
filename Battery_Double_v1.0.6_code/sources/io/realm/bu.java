package io.realm;

public enum bu {
    ASCENDING(true),
    DESCENDING(false);
    
    private final boolean c;

    private bu(boolean z) {
        this.c = z;
    }

    public boolean a() {
        return this.c;
    }
}
