package io.realm;

import io.realm.internal.m;
import io.realm.internal.o;
import java.util.Arrays;
import java.util.Locale;

public class ab extends bj implements m {

    /* renamed from: a reason: collision with root package name */
    private final ba<ab> f4536a = new ba<>(this);

    ab(e eVar, o oVar) {
        this.f4536a.a(eVar);
        this.f4536a.a(oVar);
        this.f4536a.g();
    }

    public void a() {
    }

    public String[] b() {
        this.f4536a.a().e();
        String[] strArr = new String[((int) this.f4536a.b().a())];
        for (int i = 0; i < strArr.length; i++) {
            strArr[i] = this.f4536a.b().e((long) i);
        }
        return strArr;
    }

    public String c() {
        this.f4536a.a().e();
        return this.f4536a.b().b().h();
    }

    public ba d() {
        return this.f4536a;
    }

    public boolean equals(Object obj) {
        boolean z = true;
        this.f4536a.a().e();
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        ab abVar = (ab) obj;
        String g = this.f4536a.a().g();
        String g2 = abVar.f4536a.a().g();
        if (g != null) {
            if (!g.equals(g2)) {
                return false;
            }
        } else if (g2 != null) {
            return false;
        }
        String g3 = this.f4536a.b().b().g();
        String g4 = abVar.f4536a.b().b().g();
        if (g3 != null) {
            if (!g3.equals(g4)) {
                return false;
            }
        } else if (g4 != null) {
            return false;
        }
        if (this.f4536a.b().c() != abVar.f4536a.b().c()) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        int i = 0;
        this.f4536a.a().e();
        String g = this.f4536a.a().g();
        String g2 = this.f4536a.b().b().g();
        long c = this.f4536a.b().c();
        int hashCode = ((g != null ? g.hashCode() : 0) + 527) * 31;
        if (g2 != null) {
            i = g2.hashCode();
        }
        return ((i + hashCode) * 31) + ((int) ((c >>> 32) ^ c));
    }

    public String toString() {
        String[] b2;
        this.f4536a.a().e();
        if (!this.f4536a.b().d()) {
            return "Invalid object";
        }
        StringBuilder sb = new StringBuilder(this.f4536a.b().b().h() + " = dynamic[");
        for (String str : b()) {
            long a2 = this.f4536a.b().a(str);
            RealmFieldType f = this.f4536a.b().f(a2);
            sb.append("{");
            sb.append(str).append(":");
            switch (f) {
                case BOOLEAN:
                    sb.append(this.f4536a.b().b(a2) ? "null" : Boolean.valueOf(this.f4536a.b().h(a2)));
                    break;
                case INTEGER:
                    sb.append(this.f4536a.b().b(a2) ? "null" : Long.valueOf(this.f4536a.b().g(a2)));
                    break;
                case FLOAT:
                    sb.append(this.f4536a.b().b(a2) ? "null" : Float.valueOf(this.f4536a.b().i(a2)));
                    break;
                case DOUBLE:
                    sb.append(this.f4536a.b().b(a2) ? "null" : Double.valueOf(this.f4536a.b().j(a2)));
                    break;
                case STRING:
                    sb.append(this.f4536a.b().l(a2));
                    break;
                case BINARY:
                    sb.append(Arrays.toString(this.f4536a.b().m(a2)));
                    break;
                case DATE:
                    sb.append(this.f4536a.b().b(a2) ? "null" : this.f4536a.b().k(a2));
                    break;
                case OBJECT:
                    sb.append(this.f4536a.b().a(a2) ? "null" : this.f4536a.b().b().e(a2).h());
                    break;
                case LIST:
                    sb.append(String.format(Locale.US, "RealmList<%s>[%s]", new Object[]{this.f4536a.b().b().e(a2).h(), Long.valueOf(this.f4536a.b().d(a2).c())}));
                    break;
                case INTEGER_LIST:
                    sb.append(String.format(Locale.US, "RealmList<Long>[%s]", new Object[]{Long.valueOf(this.f4536a.b().a(a2, f).c())}));
                    break;
                case BOOLEAN_LIST:
                    sb.append(String.format(Locale.US, "RealmList<Boolean>[%s]", new Object[]{Long.valueOf(this.f4536a.b().a(a2, f).c())}));
                    break;
                case STRING_LIST:
                    sb.append(String.format(Locale.US, "RealmList<String>[%s]", new Object[]{Long.valueOf(this.f4536a.b().a(a2, f).c())}));
                    break;
                case BINARY_LIST:
                    sb.append(String.format(Locale.US, "RealmList<byte[]>[%s]", new Object[]{Long.valueOf(this.f4536a.b().a(a2, f).c())}));
                    break;
                case DATE_LIST:
                    sb.append(String.format(Locale.US, "RealmList<Date>[%s]", new Object[]{Long.valueOf(this.f4536a.b().a(a2, f).c())}));
                    break;
                case FLOAT_LIST:
                    sb.append(String.format(Locale.US, "RealmList<Float>[%s]", new Object[]{Long.valueOf(this.f4536a.b().a(a2, f).c())}));
                    break;
                case DOUBLE_LIST:
                    sb.append(String.format(Locale.US, "RealmList<Double>[%s]", new Object[]{Long.valueOf(this.f4536a.b().a(a2, f).c())}));
                    break;
                default:
                    sb.append("?");
                    break;
            }
            sb.append("},");
        }
        sb.replace(sb.length() - 1, sb.length(), "");
        sb.append("]");
        return sb.toString();
    }
}
