package io.realm;

import com.hmatalonga.greenhub.models.data.BatteryDetails;
import com.hmatalonga.greenhub.models.data.CallInfo;
import com.hmatalonga.greenhub.models.data.CpuStatus;
import com.hmatalonga.greenhub.models.data.Feature;
import com.hmatalonga.greenhub.models.data.LocationProvider;
import com.hmatalonga.greenhub.models.data.NetworkDetails;
import com.hmatalonga.greenhub.models.data.ProcessInfo;
import com.hmatalonga.greenhub.models.data.Settings;
import com.hmatalonga.greenhub.models.data.StorageDetails;

public interface bq {
    BatteryDetails realmGet$batteryDetails();

    double realmGet$batteryLevel();

    String realmGet$batteryState();

    CallInfo realmGet$callInfo();

    String realmGet$countryCode();

    CpuStatus realmGet$cpuStatus();

    int realmGet$database();

    double realmGet$distanceTraveled();

    bf<Feature> realmGet$features();

    int realmGet$id();

    bf<LocationProvider> realmGet$locationProviders();

    int realmGet$memoryActive();

    int realmGet$memoryFree();

    int realmGet$memoryInactive();

    int realmGet$memoryUser();

    int realmGet$memoryWired();

    NetworkDetails realmGet$networkDetails();

    String realmGet$networkStatus();

    bf<ProcessInfo> realmGet$processInfos();

    int realmGet$screenBrightness();

    int realmGet$screenOn();

    Settings realmGet$settings();

    StorageDetails realmGet$storageDetails();

    String realmGet$timeZone();

    long realmGet$timestamp();

    String realmGet$triggeredBy();

    String realmGet$uuId();

    int realmGet$version();

    void realmSet$batteryDetails(BatteryDetails batteryDetails);

    void realmSet$batteryLevel(double d);

    void realmSet$batteryState(String str);

    void realmSet$callInfo(CallInfo callInfo);

    void realmSet$countryCode(String str);

    void realmSet$cpuStatus(CpuStatus cpuStatus);

    void realmSet$database(int i);

    void realmSet$distanceTraveled(double d);

    void realmSet$memoryActive(int i);

    void realmSet$memoryFree(int i);

    void realmSet$memoryInactive(int i);

    void realmSet$memoryUser(int i);

    void realmSet$memoryWired(int i);

    void realmSet$networkDetails(NetworkDetails networkDetails);

    void realmSet$networkStatus(String str);

    void realmSet$screenBrightness(int i);

    void realmSet$screenOn(int i);

    void realmSet$settings(Settings settings);

    void realmSet$storageDetails(StorageDetails storageDetails);

    void realmSet$timeZone(String str);

    void realmSet$timestamp(long j);

    void realmSet$triggeredBy(String str);

    void realmSet$uuId(String str);

    void realmSet$version(int i);
}
