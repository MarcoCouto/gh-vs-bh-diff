package io.realm;

import io.realm.internal.OsList;
import java.util.Locale;

final class z extends al<Double> {
    z(e eVar, OsList osList, Class<Double> cls) {
        super(eVar, osList, cls);
    }

    /* renamed from: a */
    public Double b(int i) {
        return (Double) this.f4548b.f((long) i);
    }

    public void a(int i, Object obj) {
        this.f4548b.a((long) i, ((Number) obj).doubleValue());
    }

    /* access modifiers changed from: protected */
    public void a(Object obj) {
        if (obj != null && !(obj instanceof Number)) {
            throw new IllegalArgumentException(String.format(Locale.ENGLISH, "Unacceptable value type. Acceptable: %1$s, actual: %2$s .", new Object[]{"java.lang.Number", obj.getClass().getName()}));
        }
    }

    /* access modifiers changed from: protected */
    public void b(int i, Object obj) {
        this.f4548b.b((long) i, ((Number) obj).doubleValue());
    }

    public void b(Object obj) {
        this.f4548b.a(((Number) obj).doubleValue());
    }
}
