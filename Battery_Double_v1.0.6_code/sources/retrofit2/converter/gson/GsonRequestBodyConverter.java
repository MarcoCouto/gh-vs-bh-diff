package retrofit2.converter.gson;

import a.aa;
import a.u;
import b.c;
import com.google.a.f;
import com.google.a.v;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import retrofit2.Converter;

final class GsonRequestBodyConverter<T> implements Converter<T, aa> {
    private static final u MEDIA_TYPE = u.a("application/json; charset=UTF-8");
    private static final Charset UTF_8 = Charset.forName("UTF-8");
    private final v<T> adapter;
    private final f gson;

    GsonRequestBodyConverter(f fVar, v<T> vVar) {
        this.gson = fVar;
        this.adapter = vVar;
    }

    public aa convert(T t) throws IOException {
        c cVar = new c();
        com.google.a.d.c a2 = this.gson.a((Writer) new OutputStreamWriter(cVar.c(), UTF_8));
        this.adapter.a(a2, t);
        a2.close();
        return aa.create(MEDIA_TYPE, cVar.o());
    }
}
