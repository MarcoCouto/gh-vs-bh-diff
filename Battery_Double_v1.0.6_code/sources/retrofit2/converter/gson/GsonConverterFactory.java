package retrofit2.converter.gson;

import a.aa;
import a.ac;
import com.google.a.c.a;
import com.google.a.f;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import retrofit2.Converter;
import retrofit2.Converter.Factory;
import retrofit2.Retrofit;

public final class GsonConverterFactory extends Factory {
    private final f gson;

    private GsonConverterFactory(f fVar) {
        if (fVar == null) {
            throw new NullPointerException("gson == null");
        }
        this.gson = fVar;
    }

    public static GsonConverterFactory create() {
        return create(new f());
    }

    public static GsonConverterFactory create(f fVar) {
        return new GsonConverterFactory(fVar);
    }

    public Converter<?, aa> requestBodyConverter(Type type, Annotation[] annotationArr, Annotation[] annotationArr2, Retrofit retrofit) {
        return new GsonRequestBodyConverter(this.gson, this.gson.a(a.a(type)));
    }

    public Converter<ac, ?> responseBodyConverter(Type type, Annotation[] annotationArr, Retrofit retrofit) {
        return new GsonResponseBodyConverter(this.gson, this.gson.a(a.a(type)));
    }
}
