package retrofit2.converter.gson;

import a.ac;
import com.google.a.f;
import com.google.a.v;
import java.io.IOException;
import retrofit2.Converter;

final class GsonResponseBodyConverter<T> implements Converter<ac, T> {
    private final v<T> adapter;
    private final f gson;

    GsonResponseBodyConverter(f fVar, v<T> vVar) {
        this.gson = fVar;
        this.adapter = vVar;
    }

    public T convert(ac acVar) throws IOException {
        try {
            return this.adapter.b(this.gson.a(acVar.charStream()));
        } finally {
            acVar.close();
        }
    }
}
