package retrofit2;

import a.ab;
import a.ac;
import a.e;
import a.f;
import a.u;
import a.z;
import b.c;
import b.h;
import b.l;
import b.s;
import java.io.IOException;

final class OkHttpCall<T> implements Call<T> {
    private final Object[] args;
    private volatile boolean canceled;
    private Throwable creationFailure;
    private boolean executed;
    private e rawCall;
    private final ServiceMethod<T> serviceMethod;

    static final class ExceptionCatchingRequestBody extends ac {
        private final ac delegate;
        IOException thrownException;

        ExceptionCatchingRequestBody(ac acVar) {
            this.delegate = acVar;
        }

        public void close() {
            this.delegate.close();
        }

        public long contentLength() {
            return this.delegate.contentLength();
        }

        public u contentType() {
            return this.delegate.contentType();
        }

        public b.e source() {
            return l.a((s) new h(this.delegate.source()) {
                public long read(c cVar, long j) throws IOException {
                    try {
                        return super.read(cVar, j);
                    } catch (IOException e) {
                        ExceptionCatchingRequestBody.this.thrownException = e;
                        throw e;
                    }
                }
            });
        }

        /* access modifiers changed from: 0000 */
        public void throwIfCaught() throws IOException {
            if (this.thrownException != null) {
                throw this.thrownException;
            }
        }
    }

    static final class NoContentResponseBody extends ac {
        private final long contentLength;
        private final u contentType;

        NoContentResponseBody(u uVar, long j) {
            this.contentType = uVar;
            this.contentLength = j;
        }

        public long contentLength() {
            return this.contentLength;
        }

        public u contentType() {
            return this.contentType;
        }

        public b.e source() {
            throw new IllegalStateException("Cannot read raw response body of a converted body.");
        }
    }

    OkHttpCall(ServiceMethod<T> serviceMethod2, Object[] objArr) {
        this.serviceMethod = serviceMethod2;
        this.args = objArr;
    }

    private e createRawCall() throws IOException {
        e a2 = this.serviceMethod.callFactory.a(this.serviceMethod.toRequest(this.args));
        if (a2 != null) {
            return a2;
        }
        throw new NullPointerException("Call.Factory returned null.");
    }

    public void cancel() {
        e eVar;
        this.canceled = true;
        synchronized (this) {
            eVar = this.rawCall;
        }
        if (eVar != null) {
            eVar.c();
        }
    }

    public OkHttpCall<T> clone() {
        return new OkHttpCall<>(this.serviceMethod, this.args);
    }

    public void enqueue(final Callback<T> callback) {
        Throwable th;
        e eVar;
        if (callback == null) {
            throw new NullPointerException("callback == null");
        }
        synchronized (this) {
            if (this.executed) {
                throw new IllegalStateException("Already executed.");
            }
            this.executed = true;
            e eVar2 = this.rawCall;
            th = this.creationFailure;
            if (eVar2 == null && th == null) {
                try {
                    eVar = createRawCall();
                    this.rawCall = eVar;
                } catch (Throwable th2) {
                    th = th2;
                    this.creationFailure = th;
                    eVar = eVar2;
                }
            } else {
                eVar = eVar2;
            }
        }
        if (th != null) {
            callback.onFailure(this, th);
            return;
        }
        if (this.canceled) {
            eVar.c();
        }
        eVar.a(new f() {
            private void callFailure(Throwable th) {
                try {
                    callback.onFailure(OkHttpCall.this, th);
                } catch (Throwable th2) {
                    th2.printStackTrace();
                }
            }

            private void callSuccess(Response<T> response) {
                try {
                    callback.onResponse(OkHttpCall.this, response);
                } catch (Throwable th) {
                    th.printStackTrace();
                }
            }

            public void onFailure(e eVar, IOException iOException) {
                try {
                    callback.onFailure(OkHttpCall.this, iOException);
                } catch (Throwable th) {
                    th.printStackTrace();
                }
            }

            public void onResponse(e eVar, ab abVar) throws IOException {
                try {
                    callSuccess(OkHttpCall.this.parseResponse(abVar));
                } catch (Throwable th) {
                    callFailure(th);
                }
            }
        });
    }

    public Response<T> execute() throws IOException {
        e eVar;
        synchronized (this) {
            if (this.executed) {
                throw new IllegalStateException("Already executed.");
            }
            this.executed = true;
            if (this.creationFailure == null) {
                eVar = this.rawCall;
                if (eVar == null) {
                    try {
                        eVar = createRawCall();
                        this.rawCall = eVar;
                    } catch (IOException | RuntimeException e) {
                        this.creationFailure = e;
                        throw e;
                    }
                }
            } else if (this.creationFailure instanceof IOException) {
                throw ((IOException) this.creationFailure);
            } else {
                throw ((RuntimeException) this.creationFailure);
            }
        }
        if (this.canceled) {
            eVar.c();
        }
        return parseResponse(eVar.b());
    }

    public boolean isCanceled() {
        return this.canceled;
    }

    public synchronized boolean isExecuted() {
        return this.executed;
    }

    /* access modifiers changed from: 0000 */
    public Response<T> parseResponse(ab abVar) throws IOException {
        ac g = abVar.g();
        ab a2 = abVar.h().a((ac) new NoContentResponseBody(g.contentType(), g.contentLength())).a();
        int b2 = a2.b();
        if (b2 < 200 || b2 >= 300) {
            try {
                return Response.error(Utils.buffer(g), a2);
            } finally {
                g.close();
            }
        } else if (b2 == 204 || b2 == 205) {
            return Response.success(null, a2);
        } else {
            ExceptionCatchingRequestBody exceptionCatchingRequestBody = new ExceptionCatchingRequestBody(g);
            try {
                return Response.success(this.serviceMethod.toResponse(exceptionCatchingRequestBody), a2);
            } catch (RuntimeException e) {
                exceptionCatchingRequestBody.throwIfCaught();
                throw e;
            }
        }
    }

    public synchronized z request() {
        z a2;
        e eVar = this.rawCall;
        if (eVar != null) {
            a2 = eVar.a();
        } else if (this.creationFailure == null) {
            try {
                e createRawCall = createRawCall();
                this.rawCall = createRawCall;
                a2 = createRawCall.a();
            } catch (RuntimeException e) {
                this.creationFailure = e;
                throw e;
            } catch (IOException e2) {
                this.creationFailure = e2;
                throw new RuntimeException("Unable to create request.", e2);
            }
        } else if (this.creationFailure instanceof IOException) {
            throw new RuntimeException("Unable to create request.", this.creationFailure);
        } else {
            throw ((RuntimeException) this.creationFailure);
        }
        return a2;
    }
}
