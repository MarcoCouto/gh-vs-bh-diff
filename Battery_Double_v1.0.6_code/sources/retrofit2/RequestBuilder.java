package retrofit2;

import a.aa;
import a.p.a;
import a.r;
import a.s;
import a.u;
import a.v;
import a.v.b;
import a.z;
import b.c;
import b.d;
import java.io.IOException;

final class RequestBuilder {
    private static final char[] HEX_DIGITS = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
    private static final String PATH_SEGMENT_ALWAYS_ENCODE_SET = " \"<>^`{}|\\?#";
    private final s baseUrl;
    private aa body;
    private u contentType;
    private a formBuilder;
    private final boolean hasBody;
    private final String method;
    private v.a multipartBuilder;
    private String relativeUrl;
    private final z.a requestBuilder = new z.a();
    private s.a urlBuilder;

    private static class ContentTypeOverridingRequestBody extends aa {
        private final u contentType;
        private final aa delegate;

        ContentTypeOverridingRequestBody(aa aaVar, u uVar) {
            this.delegate = aaVar;
            this.contentType = uVar;
        }

        public long contentLength() throws IOException {
            return this.delegate.contentLength();
        }

        public u contentType() {
            return this.contentType;
        }

        public void writeTo(d dVar) throws IOException {
            this.delegate.writeTo(dVar);
        }
    }

    RequestBuilder(String str, s sVar, String str2, r rVar, u uVar, boolean z, boolean z2, boolean z3) {
        this.method = str;
        this.baseUrl = sVar;
        this.relativeUrl = str2;
        this.contentType = uVar;
        this.hasBody = z;
        if (rVar != null) {
            this.requestBuilder.a(rVar);
        }
        if (z2) {
            this.formBuilder = new a();
        } else if (z3) {
            this.multipartBuilder = new v.a();
            this.multipartBuilder.a(v.e);
        }
    }

    private static String canonicalizeForPath(String str, boolean z) {
        int length = str.length();
        int i = 0;
        while (i < length) {
            int codePointAt = str.codePointAt(i);
            if (codePointAt < 32 || codePointAt >= 127 || PATH_SEGMENT_ALWAYS_ENCODE_SET.indexOf(codePointAt) != -1 || (!z && (codePointAt == 47 || codePointAt == 37))) {
                c cVar = new c();
                cVar.a(str, 0, i);
                canonicalizeForPath(cVar, str, i, length, z);
                return cVar.p();
            }
            i += Character.charCount(codePointAt);
        }
        return str;
    }

    private static void canonicalizeForPath(c cVar, String str, int i, int i2, boolean z) {
        c cVar2 = null;
        while (i < i2) {
            int codePointAt = str.codePointAt(i);
            if (!z || !(codePointAt == 9 || codePointAt == 10 || codePointAt == 12 || codePointAt == 13)) {
                if (codePointAt < 32 || codePointAt >= 127 || PATH_SEGMENT_ALWAYS_ENCODE_SET.indexOf(codePointAt) != -1 || (!z && (codePointAt == 47 || codePointAt == 37))) {
                    if (cVar2 == null) {
                        cVar2 = new c();
                    }
                    cVar2.a(codePointAt);
                    while (!cVar2.f()) {
                        byte i3 = cVar2.i() & 255;
                        cVar.i(37);
                        cVar.i((int) HEX_DIGITS[(i3 >> 4) & 15]);
                        cVar.i((int) HEX_DIGITS[i3 & 15]);
                    }
                } else {
                    cVar.a(codePointAt);
                }
            }
            i += Character.charCount(codePointAt);
        }
    }

    /* access modifiers changed from: 0000 */
    public void addFormField(String str, String str2, boolean z) {
        if (z) {
            this.formBuilder.b(str, str2);
        } else {
            this.formBuilder.a(str, str2);
        }
    }

    /* access modifiers changed from: 0000 */
    public void addHeader(String str, String str2) {
        if ("Content-Type".equalsIgnoreCase(str)) {
            u a2 = u.a(str2);
            if (a2 == null) {
                throw new IllegalArgumentException("Malformed content type: " + str2);
            }
            this.contentType = a2;
            return;
        }
        this.requestBuilder.b(str, str2);
    }

    /* access modifiers changed from: 0000 */
    public void addPart(r rVar, aa aaVar) {
        this.multipartBuilder.a(rVar, aaVar);
    }

    /* access modifiers changed from: 0000 */
    public void addPart(b bVar) {
        this.multipartBuilder.a(bVar);
    }

    /* access modifiers changed from: 0000 */
    public void addPathParam(String str, String str2, boolean z) {
        if (this.relativeUrl == null) {
            throw new AssertionError();
        }
        this.relativeUrl = this.relativeUrl.replace("{" + str + "}", canonicalizeForPath(str2, z));
    }

    /* access modifiers changed from: 0000 */
    public void addQueryParam(String str, String str2, boolean z) {
        if (this.relativeUrl != null) {
            this.urlBuilder = this.baseUrl.d(this.relativeUrl);
            if (this.urlBuilder == null) {
                throw new IllegalArgumentException("Malformed URL. Base: " + this.baseUrl + ", Relative: " + this.relativeUrl);
            }
            this.relativeUrl = null;
        }
        if (z) {
            this.urlBuilder.b(str, str2);
        } else {
            this.urlBuilder.a(str, str2);
        }
    }

    /* access modifiers changed from: 0000 */
    public z build() {
        s c;
        s.a aVar = this.urlBuilder;
        if (aVar != null) {
            c = aVar.c();
        } else {
            c = this.baseUrl.c(this.relativeUrl);
            if (c == null) {
                throw new IllegalArgumentException("Malformed URL. Base: " + this.baseUrl + ", Relative: " + this.relativeUrl);
            }
        }
        aa aaVar = this.body;
        if (aaVar == null) {
            if (this.formBuilder != null) {
                aaVar = this.formBuilder.a();
            } else if (this.multipartBuilder != null) {
                aaVar = this.multipartBuilder.a();
            } else if (this.hasBody) {
                aaVar = aa.create((u) null, new byte[0]);
            }
        }
        u uVar = this.contentType;
        if (uVar != null) {
            if (aaVar != null) {
                aaVar = new ContentTypeOverridingRequestBody(aaVar, uVar);
            } else {
                this.requestBuilder.b("Content-Type", uVar.toString());
            }
        }
        return this.requestBuilder.a(c).a(this.method, aaVar).a();
    }

    /* access modifiers changed from: 0000 */
    public void setBody(aa aaVar) {
        this.body = aaVar;
    }

    /* access modifiers changed from: 0000 */
    public void setRelativeUrl(Object obj) {
        if (obj == null) {
            throw new NullPointerException("@Url parameter is null.");
        }
        this.relativeUrl = obj.toString();
    }
}
