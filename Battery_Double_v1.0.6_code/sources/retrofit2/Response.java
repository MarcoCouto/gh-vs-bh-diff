package retrofit2;

import a.ab;
import a.ab.a;
import a.ac;
import a.r;
import a.x;
import a.z;

public final class Response<T> {
    private final T body;
    private final ac errorBody;
    private final ab rawResponse;

    private Response(ab abVar, T t, ac acVar) {
        this.rawResponse = abVar;
        this.body = t;
        this.errorBody = acVar;
    }

    public static <T> Response<T> error(int i, ac acVar) {
        if (i >= 400) {
            return error(acVar, new a().a(i).a(x.HTTP_1_1).a(new z.a().a("http://localhost/").a()).a());
        }
        throw new IllegalArgumentException("code < 400: " + i);
    }

    public static <T> Response<T> error(ac acVar, ab abVar) {
        if (acVar == null) {
            throw new NullPointerException("body == null");
        } else if (abVar == null) {
            throw new NullPointerException("rawResponse == null");
        } else if (!abVar.c()) {
            return new Response<>(abVar, null, acVar);
        } else {
            throw new IllegalArgumentException("rawResponse should not be successful response");
        }
    }

    public static <T> Response<T> success(T t) {
        return success(t, new a().a(200).a("OK").a(x.HTTP_1_1).a(new z.a().a("http://localhost/").a()).a());
    }

    public static <T> Response<T> success(T t, ab abVar) {
        if (abVar == null) {
            throw new NullPointerException("rawResponse == null");
        } else if (abVar.c()) {
            return new Response<>(abVar, t, null);
        } else {
            throw new IllegalArgumentException("rawResponse must be successful response");
        }
    }

    public static <T> Response<T> success(T t, r rVar) {
        if (rVar != null) {
            return success(t, new a().a(200).a("OK").a(x.HTTP_1_1).a(rVar).a(new z.a().a("http://localhost/").a()).a());
        }
        throw new NullPointerException("headers == null");
    }

    public T body() {
        return this.body;
    }

    public int code() {
        return this.rawResponse.b();
    }

    public ac errorBody() {
        return this.errorBody;
    }

    public r headers() {
        return this.rawResponse.f();
    }

    public boolean isSuccessful() {
        return this.rawResponse.c();
    }

    public String message() {
        return this.rawResponse.d();
    }

    public ab raw() {
        return this.rawResponse;
    }
}
